package in.gocibo.foodie.interfaces;


import in.gocibo.foodie.models.BuyerDetails;

/**
 * Created by venkatesh on 12/1/16.
 */
public interface RegistrationCallback {
    void onComplete(boolean status, String token, BuyerDetails buyerDetails);
}
