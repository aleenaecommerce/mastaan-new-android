package in.gocibo.foodie.pushnotifications;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import in.gocibo.foodie.R;
import in.gocibo.foodie.constants.Constants;

/**
 * Created by venkatesh on 29/6/15.
 */

public class GCMRegistration {

    Context context;
    GoogleCloudMessaging googleCloudMessaging;
    String SENDER_ID;
    String registrationID;

    public interface RegisterToGCMCallback{
        void onComplete(boolean status, String message, String gcmRegistrationID);
    }

    public GCMRegistration(Context context){
        this.context = context;
        SENDER_ID = context.getResources().getString(R.string.google_senderid);

        if ( checkPlayServices() ) {
            Log.d(Constants.LOG_TAG, "\nGCM-Registration : Device Supports GCM");
            googleCloudMessaging = GoogleCloudMessaging.getInstance(context);
        }
        else {
            Log.d(Constants.LOG_TAG, "\nGCM-Registration : Device NOT Supports GCM");
        }
    }

    private boolean checkPlayServices() {  // Checking Google Play Services exists in Device

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if ( resultCode != ConnectionResult.SUCCESS ) {         // ON FAILURE.
            return false;
        }
        return true;
    }

    public void registerToGCM(final RegisterToGCMCallback callback){

        unRegisterToGCM();      //  UnRegisteringBefore Registering

        if (googleCloudMessaging != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        registrationID = googleCloudMessaging.register(SENDER_ID);

                        if ( registrationID != null && registrationID.length() > 0 ) {
                            Log.d(Constants.LOG_TAG, "\nGCM-Registration : Success , RegID = " + registrationID);
                            if ( callback != null ){
                                callback.onComplete(true, "Success", registrationID);
                            }
                        }else{
                            Log.d(Constants.LOG_TAG, "\nGCM-Registration : Failure");
                            if ( callback != null ){
                                callback.onComplete(false, "Failure", null);
                            }
                        }
                    }catch (Exception e){
                        Log.d(Constants.LOG_TAG, "\nGCM-Registration : Error , Msg = "+e);
                        if ( callback != null ){
                            callback.onComplete(false, "Error", null);
                        }
                    }
                    return null;
                }
            }.execute();
        }else{
            if ( callback != null ){
                callback.onComplete(false, "Error", null);
            }
        }
    }

    public void unRegisterToGCM(){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    googleCloudMessaging.unregister();
                }catch (Exception e){
                    Log.d(Constants.LOG_TAG, "\nGCM-UnRegistration : Error , Msg = "+e);
                }
                return null;
            }
        }.execute();
    }

}
