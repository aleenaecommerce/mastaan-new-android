package in.gocibo.foodie.pushnotifications;

import android.content.Context;
import android.content.Intent;
import androidx.core.app.TaskStackBuilder;
import androidx.legacy.content.WakefulBroadcastReceiver;

import android.util.Log;

import in.gocibo.foodie.activities.OrderDetailsActivity;
import in.gocibo.foodie.constants.Constants;

import com.aleena.common.widgets.vNotification;

/**
 * Created by venkatesh on 29/6/15.
 */

public class GCMPushNotificationsReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String type = intent.getStringExtra("type");
        String id = intent.getStringExtra("id");
        boolean status = intent.getBooleanExtra("status", false);
        String message = intent.getStringExtra("message");

        Log.d(Constants.LOG_TAG, "\nGCM-onMessageReceive : Type = "+type+" , Message = "+message);

        if ( type != null && type.equalsIgnoreCase("order_deliver") ){

            vNotification notification = new vNotification(context, "GoCibo", message);
            Intent resultIntent = new Intent(context, OrderDetailsActivity.class);
            resultIntent.putExtra("orderID", id);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(OrderDetailsActivity.class);            // Adds the Intent to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            notification.setStackBuilder(stackBuilder);
            notification.show();
        }
    }

}