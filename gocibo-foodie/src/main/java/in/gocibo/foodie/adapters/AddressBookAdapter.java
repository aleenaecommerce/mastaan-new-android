package in.gocibo.foodie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.models.AddressBookItemDetails;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;

public class AddressBookAdapter extends RecyclerView.Adapter<AddressBookAdapter.ViewHolder> {

    Context context;
    int layourResourceID;
    List<AddressBookItemDetails> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public interface CallBack {
        void onItemClick(int position);
        void onItemMenuClick(int position, View view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        FrameLayout itemSelector;
        FrameLayout menuOptions;
        ImageView type_icon;
        TextView place_title;
        TextView place_name;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = (FrameLayout) itemLayoutView.findViewById(R.id.itemSelector);
            menuOptions = (FrameLayout) itemLayoutView.findViewById(R.id.menuOptions);
            type_icon = (ImageView) itemLayoutView.findViewById(R.id.type_icon);
            place_title = (TextView) itemLayoutView.findViewById(R.id.place_title);
            place_name = (TextView) itemLayoutView.findViewById(R.id.place_name);
        }
    }

    public AddressBookAdapter(Context context, int layourResourceID, List<AddressBookItemDetails> itemsList, CallBack callBack) {
        this.context = context;
        this.layourResourceID = layourResourceID;
        this.itemsList = itemsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AddressBookAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(layourResourceID, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final AddressBookItemDetails addressBookItemDetails = itemsList.get(position);    // Getting Food Items Details using Position...

        if ( addressBookItemDetails.getType().equalsIgnoreCase("history") ){
            viewHolder.type_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_previous));
        }else{
            viewHolder.type_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favourite));
        }
        if ( addressBookItemDetails.getTitle() != null && addressBookItemDetails.getTitle().length() > 0 ){
            viewHolder.place_title.setText(addressBookItemDetails.getTitle());
            viewHolder.place_title.setVisibility(View.VISIBLE);
        }else{
            viewHolder.place_title.setVisibility(View.GONE);
        }
        viewHolder.place_name.setText(addressBookItemDetails.getFullAddress());

        if ( callBack != null ){
            viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemClick(position);
                }
            });
            viewHolder.menuOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemMenuClick(position, viewHolder.menuOptions);
                }
            });
        }

    }

    //----------------

    public void setItems(List<AddressBookItemDetails> items){

        itemsList.clear();
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                if ( items.get(i).getTitle() == null ) {
                    items.get(i).setTitle("");
                }
                itemsList.add(items.get(i));
            }
            sortItems();
        }
        notifyDataSetChanged();
    }

    public void addItems(List<AddressBookItemDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                if ( items.get(i).getTitle() == null ) {
                    items.get(i).setTitle("");
                }
                itemsList.add(items.get(i));
            }
            sortItems();

            notifyDataSetChanged();
        }
    }

    private void sortItems(){

        for (int i=0;i<itemsList.size();i++){
            for (int j=0;j<itemsList.size();j++){
                if ( itemsList.get(i).getTitle().trim().compareToIgnoreCase(itemsList.get(j).getTitle().trim()) < 0) {
                    AddressBookItemDetails temp = itemsList.get(i);
                    itemsList.set(i, itemsList.get(j));
                    itemsList.set(j, temp);
                }
            }
        }
    }

    public void addItem(AddressBookItemDetails addressBookItemDetails){
        itemsList.add(addressBookItemDetails);
        sortItems();
        notifyDataSetChanged();
    }

    public void updateItem(int postiion, AddressBookItemDetails modifiedAddressBookItemDetails){
        itemsList.set(postiion, modifiedAddressBookItemDetails);
        sortItems();
        notifyDataSetChanged();
    }

    public void deleteItem(int position){
        itemsList.remove(position);
        notifyDataSetChanged();
    }

    public List<AddressBookItemDetails> getAllItems(){
        List<AddressBookItemDetails> items = new ArrayList<>();
        for (int i=0;i<itemsList.size();i++){
            items.add(itemsList.get(i));
        }
        return items;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public AddressBookItemDetails getItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}