package in.gocibo.foodie.adapters;

import android.content.Context;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import in.gocibo.foodie.fragments.AddressBookFragment;
import in.gocibo.foodie.fragments.CuisinesFragment;
import in.gocibo.foodie.fragments.FavouritesFragment;
import in.gocibo.foodie.fragments.LoyaltyCenterFragment;
import com.aleena.common.fragments.NotYetFragment;
import in.gocibo.foodie.fragments.OrderHistoryFragment;
import in.gocibo.foodie.fragments.PrivacyPolicyFragment;
import in.gocibo.foodie.fragments.ProfileAllergiesFragment;
import in.gocibo.foodie.fragments.ProfileSummaryFragment;
import in.gocibo.foodie.fragments.TermsAndConditionsFragment;

public class TabsFragmentAdapter extends FragmentPagerAdapter {

    Context context;
    String[] itemsList;

    public TabsFragmentAdapter(Context context, FragmentManager fragmentManager, String[] itemsList) {
        super(fragmentManager);
        this.context = context;
        this.itemsList =  itemsList;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        if ( itemsList[position].equalsIgnoreCase("Profile") ){
            fragment = new ProfileSummaryFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Address Book") ){
            fragment = new AddressBookFragment().newInstance(context, false, null);
        }
        else if ( itemsList[position].equalsIgnoreCase("Bio") ){
            fragment = new ProfileSummaryFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Order History") ){
            fragment = new OrderHistoryFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Cuisines") ){
            fragment = new CuisinesFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Favorites") ){
            fragment = new FavouritesFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Loyalty center") ){
            fragment = new LoyaltyCenterFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Allergies") ){
            fragment = new ProfileAllergiesFragment();
        }

        else if ( itemsList[position].equalsIgnoreCase("Terms & Conditions") ){
            fragment = new TermsAndConditionsFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Privacy policy") ){
            fragment = new PrivacyPolicyFragment();
        }

        else{
            fragment = new NotYetFragment();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return itemsList.length;
    }

}
