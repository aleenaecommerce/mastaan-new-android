package in.gocibo.foodie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.models.ChefDetails;
import com.aleena.common.widgets.vCircularImageView;
import com.aleena.common.methods.CommonMethods;

public class ChefsListAdapter extends RecyclerView.Adapter<ChefsListAdapter.ViewHolder> {

    Context context;
    List<ChefDetails> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        Button chefSelector;
        vCircularImageView chef_image;
        TextView chef_name;
        TextView chef_nativity;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            chefSelector = (Button) itemLayoutView.findViewById(R.id.chefSelector);
            chef_image = (vCircularImageView) itemLayoutView.findViewById(R.id.chef_image);
            chef_name = (TextView) itemLayoutView.findViewById(R.id.chef_name);
            chef_nativity = (TextView) itemLayoutView.findViewById(R.id.chef_nativity);
        }
    }

    public ChefsListAdapter(Context context, List<ChefDetails> chefsList, CallBack callBack) {
        this.context = context;
        this.itemsList = chefsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ChefsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_chef, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final ChefDetails chefDetails = itemsList.get(position);    // Getting Food Items Details using Position...

        viewHolder.chef_name.setText(new CommonMethods().capitalizeStringWords(chefDetails.getFullName()));
        Picasso.get()
                .load(chefDetails.getThumbnailImageURL())
                .placeholder(R.drawable.ic_chef)
                .error(R.drawable.ic_chef)
                .fit()
                .centerCrop()
                .tag(context)
                .into(viewHolder.chef_image);

        viewHolder.chef_nativity.setText(chefDetails.getNativity());

        if ( callBack != null && viewHolder.chefSelector != null ){
            viewHolder.chefSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemClick(position);
                }
            });
        }else{
            if ( viewHolder.chefSelector != null ){    viewHolder.chefSelector.setClickable(false);   }
        }
    }

    public void addItems(List<ChefDetails> chefs){

        for(int i=0;i<chefs.size();i++){
            itemsList.add(chefs.get(i));
        }
        notifyDataSetChanged();
    }

    public List<ChefDetails> getAllChefs(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public ChefDetails getChefDetails(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}