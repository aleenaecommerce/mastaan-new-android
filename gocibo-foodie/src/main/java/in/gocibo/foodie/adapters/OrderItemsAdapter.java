package in.gocibo.foodie.adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vCircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.models.OrderItemDetails;

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder> {

    Context context;
    CommonMethods commonMethods;
    List<OrderItemDetails> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        TextView item_name;
        ImageView item_image;
        TextView item_price;
        TextView item_quantity;
        ImageView item_type_indicator;

        TextView delivery_slot;
        ImageView chef_image;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            cardView = (CardView) itemLayoutView.findViewById(R.id.cardView);
            item_name = (TextView) itemLayoutView.findViewById(R.id.item_name);
            item_image = (ImageView) itemLayoutView.findViewById(R.id.item_image);
            item_price = (TextView) itemLayoutView.findViewById(R.id.item_price);
            item_quantity = (TextView) itemLayoutView.findViewById(R.id.item_quantity);
            item_type_indicator = (ImageView) itemLayoutView.findViewById(R.id.item_type_indicator);

            delivery_slot = (TextView) itemLayoutView.findViewById(R.id.delivery_slot);
            chef_image = (vCircularImageView) itemLayoutView.findViewById(R.id.chef_image);
        }
    }

    public OrderItemsAdapter(Context context, List<OrderItemDetails> orderItems, CallBack callBack) {
        this.context = context;
        this.itemsList = orderItems;
        this.callBack = callBack;
        this.commonMethods = new CommonMethods();
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public OrderItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_food_item_order, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        try {
            final OrderItemDetails orderItemDetails = itemsList.get(position);    // Getting Food Items Details using Position...

            viewHolder.item_name.setText(new CommonMethods().capitalizeFirstLetter(orderItemDetails.getDishName()));

            Picasso.get()
                    .load(orderItemDetails.getDishImage())
                    .placeholder(R.drawable.image_default)
                    .error(R.drawable.image_default)
                    .fit()
                    .centerCrop()
                    .tag(context)
                    .into(viewHolder.item_image);

            viewHolder.item_price.setText(context.getResources().getString(R.string.Rs) + " " + commonMethods.getInDecimalFormat(orderItemDetails.getQuantity() * orderItemDetails.getDishPrice()));

            if (orderItemDetails.getDishQuantityType().equalsIgnoreCase("n")) {
                viewHolder.item_quantity.setText(commonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " qty.");
            } else if (orderItemDetails.getDishQuantityType().equalsIgnoreCase("w")) {
                viewHolder.item_quantity.setText(commonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " kgs");
                if (orderItemDetails.getQuantity() == 1) {
                    viewHolder.item_quantity.setText(commonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " kg");
                }
            } else if (orderItemDetails.getDishQuantityType().equalsIgnoreCase("v")) {
                viewHolder.item_quantity.setText(commonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " L");
            }

            if (orderItemDetails.getDishType().equalsIgnoreCase("veg")) {
                viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_veg);
            } else if (orderItemDetails.getDishType().equalsIgnoreCase("non-veg")) {
                viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_non_veg);
            } else if (orderItemDetails.getDishType().equalsIgnoreCase("jain")) {
                viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_jain);
            }

            viewHolder.delivery_slot.setText(orderItemDetails.getDBY());
            Picasso.get()
                    .load(orderItemDetails.getDishChefImage())
                    .placeholder(R.drawable.ic_chef)
                    .error(R.drawable.ic_chef)
                    .fit()
                    .centerCrop()
                    .tag(context)
                    .into(viewHolder.chef_image);

            /*if ( callBack != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callBack.onItemClick(position);
                    }
                });
            }else{
                viewHolder.itemSelector.setClickable(false);
            }*/
        }catch (Exception e){e.printStackTrace();}
    }

    public void addItems(List<OrderItemDetails> items){

        for(int i=0;i<items.size();i++){
            itemsList.add(items.get(i));
        }
        notifyDataSetChanged();
    }

    public void deleteAllItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<OrderItemDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public OrderItemDetails getItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}