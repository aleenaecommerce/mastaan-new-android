package in.gocibo.foodie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vCircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.models.CategoryDetails;
import in.gocibo.foodie.models.FoodItemDetails;

public class FoodItemsAdapter extends RecyclerView.Adapter<FoodItemsAdapter.ViewHolder> {

    ViewGroup parent;
    Context context;

    LocalStorageData localStorageData;
    CommonMethods commonMethods;
    String RS;

    List<FoodItemDetails> itemsList;
    CategoryDetails categoryDetails;
    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onChefClick(int position);
        void onCartClick(int position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        FrameLayout itemSelector;
        TextView item_name;
        TextView item_price;
        ImageView item_image;

        ImageView item_type_indicator;
        TextView available_info;
        FrameLayout item_details_pan;
        FrameLayout sold_out_pan;
        TextView sold_out_text;
        LinearLayout sold_out_fader;

        TextView chef_name;
        TextView item_quantity;
        vCircularImageView chef_image;
        Button chefSelector;

        FrameLayout add_to_cart;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = (FrameLayout) itemLayoutView.findViewById(R.id.itemSelector);
            item_name = (TextView) itemLayoutView.findViewById(R.id.item_name);
            item_price = (TextView) itemLayoutView.findViewById(R.id.item_price);
            available_info = (TextView) itemLayoutView.findViewById(R.id.available_info);
            item_image = (ImageView) itemLayoutView.findViewById(R.id.item_image);

            item_type_indicator = (ImageView) itemLayoutView.findViewById(R.id.item_type_indicator);
            item_details_pan = (FrameLayout) itemLayoutView.findViewById(R.id.item_details_pan);
            sold_out_pan = (FrameLayout) itemLayoutView.findViewById(R.id.sold_out_pan);
            sold_out_text = (TextView) itemLayoutView.findViewById(R.id.sold_out_text);
            sold_out_fader = (LinearLayout) itemLayoutView.findViewById(R.id.sold_out_fader);

            chef_name = (TextView) itemLayoutView.findViewById(R.id.chef_name);
            item_quantity = (TextView) itemLayoutView.findViewById(R.id.item_quantity);
            chef_image = (vCircularImageView) itemLayoutView.findViewById(R.id.chef_image);
            chefSelector = (Button) itemLayoutView.findViewById(R.id.chefSelector);

            add_to_cart = (FrameLayout) itemLayoutView.findViewById(R.id.add_to_cart);
        }
    }

    public FoodItemsAdapter(Context context, List<FoodItemDetails> itemsList, CategoryDetails categoryDetails, Callback callback) {
        this.context = context;
        this.localStorageData = new LocalStorageData(context);
        this.commonMethods = new CommonMethods();
        this.RS = context.getResources().getString(R.string.Rs);

        this.itemsList = itemsList;
        this.categoryDetails = categoryDetails;
        this.callback = callback;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public FoodItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_food_item_home, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final FoodItemDetails foodItemDetails = getItem(position);

        viewHolder.item_name.setText(commonMethods.capitalizeFirstLetter(foodItemDetails.getName()) + " (" + commonMethods.capitalizeFirstLetter(foodItemDetails.getCuisineName()) + ")");

        Picasso.get()
                .load(foodItemDetails.getImageURL())
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .fit()
                .centerCrop()
                .tag(context)
                .into(viewHolder.item_image);

        if ( foodItemDetails.getType().equalsIgnoreCase("veg") ){
            viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_veg);
        }else if ( foodItemDetails.getType().equalsIgnoreCase("non-veg") ){
            viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_non_veg);
        }else if ( foodItemDetails.getType().equalsIgnoreCase("jain") ){
            viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_jain);
        }

        viewHolder.chef_name.setText(commonMethods.capitalizeFirstLetter(foodItemDetails.getChefFirstName()));
        Picasso.get()
                .load(foodItemDetails.getChefThumbnailImageURL())
                .placeholder(R.drawable.ic_chef)
                .error(R.drawable.ic_chef)
                .fit()
                .centerCrop()
                .tag(context)
                .into(viewHolder.chef_image);


        if ( foodItemDetails.getCurrentAvailability() == 0 ){

            if ( foodItemDetails.getQuantityType().equalsIgnoreCase("n")){
                viewHolder.sold_out_text.setText(commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability()) + " SERVINGS SOLD OUT");
                if ( foodItemDetails.getTotalAvailability() == 1 ){
                    viewHolder.sold_out_text.setText(commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability()) + " SERVING SOLD OUT");
                }
            }
            else if ( foodItemDetails.getQuantityType().equalsIgnoreCase("w")){
                viewHolder.sold_out_text.setText(commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability()) + " KGS SOLD OUT");
                if ( foodItemDetails.getTotalAvailability() == 1 ){
                    viewHolder.sold_out_text.setText(commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability()) + " KG SOLD OUT");
                }
            }
            else if ( foodItemDetails.getQuantityType().equalsIgnoreCase("v")){
                viewHolder.sold_out_text.setText(commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability()) + " LITRES SOLD OUT");
                if ( foodItemDetails.getTotalAvailability() == 1 ){
                    viewHolder.sold_out_text.setText(commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability()) + " LITRE SOLD OUT");
                }
            }
            viewHolder.sold_out_fader.setVisibility(View.VISIBLE);
            viewHolder.sold_out_pan.setVisibility(View.VISIBLE);
            viewHolder.item_details_pan.setVisibility(View.INVISIBLE);
        }
        else{
            String totalInfo="";

            if (foodItemDetails.getQuantityType().equalsIgnoreCase("n")) {
                viewHolder.item_price.setText(RS + " " + new CommonMethods().getInDecimalFormat(foodItemDetails.getProfitPrice()));
                totalInfo = "of " + commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability())+" servings";
                viewHolder.available_info.setText(commonMethods.getInDecimalFormat(foodItemDetails.getCurrentAvailability()) + " avl.");
            }
            else if (foodItemDetails.getQuantityType().equalsIgnoreCase("w")) {
                viewHolder.item_price.setText(RS + " " + new CommonMethods().getInDecimalFormat(foodItemDetails.getProfitPrice())+"/kg");
                totalInfo = "of " + commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability()) + " kgs";
                viewHolder.available_info.setText(commonMethods.getInDecimalFormat(foodItemDetails.getCurrentAvailability()) + " kgs avl.");

                if (foodItemDetails.getCurrentAvailability() == 1) {
                    totalInfo = "of " + commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability()) + " kg";
                    viewHolder.available_info.setText(commonMethods.getInDecimalFormat(foodItemDetails.getCurrentAvailability()) + " kg avl.");
                }
            } else if (foodItemDetails.getQuantityType().equalsIgnoreCase("v")) {
                viewHolder.item_price.setText(RS + " " + new CommonMethods().getInDecimalFormat(foodItemDetails.getProfitPrice())+"/L");
                totalInfo = "of total " + commonMethods.getInDecimalFormat(foodItemDetails.getTotalAvailability())+" L";
                viewHolder.available_info.setText(commonMethods.getInDecimalFormat(foodItemDetails.getCurrentAvailability()) + " L avl.");
            }


            boolean isAvailable = true;

            if ( DateMethods.compareDates(commonMethods.getReadableTimeFromUTC(foodItemDetails.getAvailableTime()), localStorageData.getServerTime()) > 0 ) {
                viewHolder.available_info.append(" from " + DateMethods.getTimeIn12HrFormat(commonMethods.getReadableTimeFromUTC(foodItemDetails.getAvailableTime())).toUpperCase());
            }
            else {
                if ( foodItemDetails.getAvailabilityEndTime() != null && foodItemDetails.getAvailabilityEndTime().length() > 0 ){
                    if ( DateMethods.compareDates(localStorageData.getServerTime(), commonMethods.getReadableTimeFromUTC(foodItemDetails.getAvailabilityEndTime())) > 0 ) {
                        viewHolder.sold_out_text.setText("NOT AVAILABLE");
                        isAvailable = false;                                // ON ITEM NOT AVAILABLE
                    }else {
                        //viewHolder.available_info.append(" upto "+commonMethods.getOnlyTimeIn12HrFormat(commonMethods.getReadableTimeFromUTC(foodItemDetails.getAvailabilityEndTime())).toUpperCase());
                        viewHolder.available_info.append(totalInfo);
                    }
                }
                else{
                    viewHolder.available_info.append(totalInfo);
                }
            }

            if ( isAvailable ) {
                viewHolder.item_details_pan.setVisibility(View.VISIBLE);
                viewHolder.sold_out_pan.setVisibility(View.INVISIBLE);
                viewHolder.sold_out_fader.setVisibility(View.GONE);
            }else{
                viewHolder.sold_out_fader.setVisibility(View.VISIBLE);
                viewHolder.sold_out_pan.setVisibility(View.VISIBLE);
                viewHolder.item_details_pan.setVisibility(View.INVISIBLE);
            }
        }

        //----------------------------------------------------

        viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onItemClick(position);
                }
            }
        });

        viewHolder.chefSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onChefClick(position);
                }
            }
        });

        viewHolder.add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    if (foodItemDetails.getCurrentAvailability() > 0) {
                        callback.onCartClick(position);
                    }
                }
            }
        });
    }

    //===================================================

    public void updateItemAvailability(String dishID, double newAvailability){//final int viewNo){
        int itemIndex = isContails(dishID);
        if ( itemIndex != -1 ){
            setItemNewAvailability(itemIndex, newAvailability);
        }
        notifyDataSetChanged();
    }

    public int isContails(String dishID){
        int status = -1;
        for(int i=0;i< itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(dishID)){
                status = i;
                break;
            }
        }
        return status;
    }

    public void updateItemsAvailabilityFromFoodItems(List<FoodItemDetails> toUpdateItems){

        if ( toUpdateItems != null ){
            for (int i=0;i<toUpdateItems.size();i++){
                int itemIndex = isContails(toUpdateItems.get(i).getID());
                if ( itemIndex != -1 ){
                    setItemNewAvailability(itemIndex, toUpdateItems.get(i).getCurrentAvailability());
                }
            }
            notifyDataSetChanged();
        }
    }

    public void setItemNewAvailability(int index, double newAvailability){

        FoodItemDetails foodItemDetails = itemsList.get(index);
        foodItemDetails.setAvailability(newAvailability);
        itemsList.set(index, foodItemDetails);
    }

    //====================================================

    public void addItems(List<FoodItemDetails> items){

        if ( items != null ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<FoodItemDetails> getAllItems(){
        return itemsList;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public FoodItemDetails getItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}