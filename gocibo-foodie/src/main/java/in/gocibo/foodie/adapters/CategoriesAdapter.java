package in.gocibo.foodie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.models.CategoryDetails;

public class CategoriesAdapter extends ArrayAdapter<CategoryDetails> {

    Context context;

    List<CategoryDetails> itemsList;
    Callback callback;

    public interface Callback {
        void onItemClick(int position);
    }

    class ViewHolder {          // Food Item View Holder

        Button itemSelector;
        TextView category_name;
        TextView category_uploads_count;
        ImageView category_image;
        View category_overlay;
    }


    public CategoriesAdapter(Context context, List<CategoryDetails> itemsList, Callback callback) {
        super(context, R.layout.view_category, itemsList);

        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if ( convertView == null ) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.view_category, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.itemSelector = (Button) convertView.findViewById(R.id.itemSelector);
            viewHolder.category_name = (TextView) convertView.findViewById(R.id.category_name);
            viewHolder.category_uploads_count = (TextView) convertView.findViewById(R.id.category_uploads_count);
            viewHolder.category_image = (ImageView) convertView.findViewById(R.id.category_image);
            viewHolder.category_overlay = convertView.findViewById(R.id.category_overlay);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final CategoryDetails categoryDetails = getItem(position);

        viewHolder.category_name.setText(categoryDetails.getName());

        if ( categoryDetails.getUploadsCount() > 0 ){
            viewHolder.category_uploads_count.setVisibility(View.VISIBLE);
            viewHolder.category_overlay.setVisibility(View.GONE);
            viewHolder.category_uploads_count.setText(categoryDetails.getUploadsCount()+" items available today");
            if ( categoryDetails.getUploadsCount() == 1 ){
                viewHolder.category_uploads_count.setText(categoryDetails.getUploadsCount()+" item available today");
            }
        } else{
            viewHolder.category_uploads_count.setVisibility(View.VISIBLE);
            viewHolder.category_overlay.setVisibility(View.VISIBLE);
            viewHolder.category_uploads_count.setText("No items available yet");
        }

        Picasso.get()
                .load(categoryDetails.getImageURL())
                .placeholder(R.drawable.image_default_gocibo)
                .error(R.drawable.image_default_gocibo)
                .tag(context)
                .into(viewHolder.category_image);

        if ( callback != null ){
            viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onItemClick(position);
                }
            });
        }

        return convertView;
    }

    public void updateCategoryUploadsCount(String categoryID, int uploadsCount){
        if ( uploadsCount >= 0 ) {
            for (int i = 0; i < itemsList.size(); i++) {
                if (itemsList.get(i).getID().equals(categoryID)) {
                    itemsList.get(i).setUploadsCount(uploadsCount);
                    notifyDataSetChanged();
                    break;
                }
            }
        }
    }

    //=======================================================

    public void setItems(List<CategoryDetails> items){
        if ( items != null ){
            itemsList.clear();
            addItems(items);
        }
    }

    public void addItems(List<CategoryDetails> items){
        if ( items != null && items.size() > 0 ){
            for (int i=0;i<items.size();i++){
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    @Override
    public CategoryDetails getItem(int position) { // << subclasses can use subtypes in overridden methods!
        return itemsList.get(position);
    }

    public List<CategoryDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){ return itemsList.size();    }
}