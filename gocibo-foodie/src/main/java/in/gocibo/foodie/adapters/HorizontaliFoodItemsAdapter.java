package in.gocibo.foodie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.aleena.common.methods.CommonMethods;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.models.FoodItemDetails;

public class HorizontaliFoodItemsAdapter extends ArrayAdapter<FoodItemDetails> {

    Context context;
    LayoutInflater inflater;
    ViewGroup parent;
    int layoutResourceId;

    List<FoodItemDetails> foodItems;

    String RS;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        //void onChefClick(int position);
    }

    class ViewHolder {          // Food Item View Holder
        CardView cardView;
        TextView item_name;
        ImageView item_image;
        TextView item_price;
        ImageView item_type_indicator;
    }


    public HorizontaliFoodItemsAdapter(Context context, int layoutResourceId, List<FoodItemDetails> foodItems, Callback callback) {
        super(context, layoutResourceId, foodItems);

        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layoutResourceId = layoutResourceId;

        this.foodItems = foodItems;

        this.RS = context.getResources().getString(R.string.Rs);

        this.callback = callback;
    }

    @Override
    public FoodItemDetails getItem(int position) { // << subclasses can use subtypes in overridden methods!
        return foodItems.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        this.parent = parent;

        ViewHolder viewHolder;

        if ( convertView == null ) {
            convertView = inflater.inflate(layoutResourceId, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.cardView = (CardView) convertView.findViewById(R.id.cardView);
            viewHolder.item_name = (TextView) convertView.findViewById(R.id.item_name);
            viewHolder.item_image = (ImageView) convertView.findViewById(R.id.item_image);
            viewHolder.item_price = (TextView) convertView.findViewById(R.id.item_price);
            viewHolder.item_type_indicator = (ImageView) convertView.findViewById(R.id.item_type_indicator);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //-------------------------------------------------

        final FoodItemDetails foodItemDetails = getItem(position);

        viewHolder.item_name.setText(new CommonMethods().capitalizeFirstLetter(foodItemDetails.getName()));
        viewHolder.item_price.setText(context.getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(foodItemDetails.getProfitPrice()));

        Picasso.get()
                .load(foodItemDetails.getThumbnailImageURL())
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .fit()
                .centerCrop()
                .tag(context)
                .into(viewHolder.item_image);

        if ( foodItemDetails.getType().equalsIgnoreCase("veg") ){
            viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_veg);
        }else if ( foodItemDetails.getType().equalsIgnoreCase("non-veg") ){
            viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_non_veg);
        }else if ( foodItemDetails.getType().equalsIgnoreCase("jain") ){
            viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_jain);
        }

        if ( callback != null ){
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(position);
                }
            });
        }else{
            viewHolder.cardView.setClickable(false);
        }

        return convertView;
    }

    public void clearItems(){
        foodItems.clear();
        notifyDataSetChanged();
    }

    private void sortItems(){

        for (int i=0;i<foodItems.size();i++){
            for (int j=0;j<foodItems.size();j++){
                if ( foodItems.get(i).getName().trim().compareToIgnoreCase(foodItems.get(j).getName().trim()) < 0) {
                    FoodItemDetails temp = foodItems.get(i);
                    foodItems.set(i, foodItems.get(j));
                    foodItems.set(j, temp);
                }
            }
        }
    }

    public void addItems(List<FoodItemDetails> foodItems){

        if ( foodItems != null && foodItems.size() > 0 ) {
            for (int i = 0; i < foodItems.size(); i++) {
                this.foodItems.add(foodItems.get(i));
            }
            sortItems();
            notifyDataSetChanged();
        }
    }

    public List<FoodItemDetails> getItemsExcluding(List<FoodItemDetails> foodItems, FoodItemDetails excludeItemDetails){
        List<FoodItemDetails> items = new ArrayList<>();
        for(int i=0;i<foodItems.size();i++){
            if ( foodItems.get(i).getID().equals(excludeItemDetails.getID()) == false ){
                items.add(foodItems.get(i));
            }
        }
        return items;
    }

    public List<FoodItemDetails> getAllFoodItems(){
        return foodItems;
    }

    public int getItemsCount(){
        return foodItems.size();
    }

    public FoodItemDetails getFoodItem(int position){
        if ( position < foodItems.size() ) {
            return foodItems.get(position);
        }
        return null;
    }
}