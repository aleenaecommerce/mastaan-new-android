package in.gocibo.foodie.adapters;

import android.content.Context;
import android.text.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.models.OrderDetails;
import in.gocibo.foodie.models.OrderItemDetails;

public class OrdersHistoryAdapter extends RecyclerView.Adapter<OrdersHistoryAdapter.ViewHolder> {

    Context context;
    List<OrderDetails> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        Button orderSelector;
        View order_status;
        TextView order_date;
        TextView order_id;
        TextView order_items;
        TextView order_cost;
        View separator;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            orderSelector = (Button) itemLayoutView.findViewById(R.id.orderSelector);
            order_status = (View) itemLayoutView.findViewById(R.id.order_status);
            order_date = (TextView) itemLayoutView.findViewById(R.id.order_date);
            order_id = (TextView) itemLayoutView.findViewById(R.id.order_id);
            order_items = (TextView) itemLayoutView.findViewById(R.id.order_items);
            order_cost = (TextView) itemLayoutView.findViewById(R.id.order_cost);
            separator = (View) itemLayoutView.findViewById(R.id.separator);

        }
    }

    public OrdersHistoryAdapter(Context context, List<OrderDetails> itemsList, CallBack callBack) {
        this.context = context;
        this.itemsList = itemsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public OrdersHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_history_item, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        try{
            final OrderDetails orderDetails = itemsList.get(position);    // Getting Food Items Details using Position...

            viewHolder.order_date.setText(DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()), DateConstants.MMM_DD_YYYY_HH_MM_A));
            viewHolder.order_id.setText("Order ID : "+orderDetails.getOrderID());
            viewHolder.order_cost.setText(context.getResources().getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(orderDetails.getTotalAmount()));

            if ( orderDetails.getOrderStaus().equalsIgnoreCase("pending") ){
                viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.order_pending_color));
            }else if ( orderDetails.getOrderStaus().equalsIgnoreCase("delivered") ){
                viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.order_delivered_color));
            }else if ( orderDetails.getOrderStaus().equalsIgnoreCase("failure") || orderDetails.getOrderStaus().equalsIgnoreCase("failed")){
                viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.order_cancelled_color));
            }

            List<OrderItemDetails> orderItems = orderDetails.getOrderItems();
            if ( orderItems != null ){
                for(int j=0;j<orderItems.size();j++){
                    if ( j == 0 ){
                        viewHolder.order_items.setText(new CommonMethods().capitalizeFirstLetter(orderItems.get(j).getDishName()));
                    }else {
                        viewHolder.order_items.append(", " + new CommonMethods().capitalizeFirstLetter(orderItems.get(j).getDishName()));
                    }
                }
            }else{
                viewHolder.order_items.setText("");
            }

            if ( viewHolder.orderSelector != null ){
                if ( callBack != null ){
                    viewHolder.orderSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callBack.onItemClick(position);
                        }
                    });
                }else{
                    viewHolder.orderSelector.setClickable(false);
                }
                viewHolder.orderSelector.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboardManager.setText(orderDetails.getOrderID());
                        Toast.makeText(context, "Order ID ( "+orderDetails.getOrderID()+" ) is copied", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                });
            }
        }catch (Exception e){e.printStackTrace();}
    }

    private void sortByDate(){

        for (int i=0;i<itemsList.size();i++){
            for (int j=i;j<itemsList.size();j++){
                if ( itemsList.get(i).getCreatedDate().compareTo(itemsList.get(j).getCreatedDate()) < 0 ){    // If itemList[i] > itemList[j]
                    OrderDetails temp = itemsList.get(i);
                    itemsList.set(i, itemsList.get(j));
                    itemsList.set(j, temp);
                }
            }
        }
    }

    public void updateOrderStatus(String orderID, String orderStatus){
        if ( itemsList != null && orderID != null && orderStatus != null ){
            for ( int i=0;i<itemsList.size();i++){
                if ( itemsList.get(i).getID().equals(orderID) ){
                    itemsList.get(i).setOrderStaus(orderStatus);
                }
            }
            notifyDataSetChanged();
        }
    }

    public void addItems(List<OrderDetails> items){

        for(int i=items.size()-1;i>=0;i--){
            itemsList.add(items.get(i));
        }
        sortByDate();
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<OrderDetails> getAllOrderItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public OrderDetails getOrderItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}