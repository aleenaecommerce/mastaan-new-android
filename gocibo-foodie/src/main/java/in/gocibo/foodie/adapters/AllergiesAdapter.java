package in.gocibo.foodie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.models.AllergyDetails;
import com.aleena.common.methods.CommonMethods;

public class AllergiesAdapter extends ArrayAdapter<AllergyDetails> {

    Context context;
    LayoutInflater inflater;
    ViewGroup parent;
    int layoutResourceId;

    List<AllergyDetails> itemsList;

    List<AllergyDetails> deletedItemsLists;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onDelete(int position);
    }

    class ViewHolder {          // Food Item View Holder
        TextView allergy_name;
        FrameLayout delete_allergy;
    }


    public AllergiesAdapter(Context context, int layoutResourceId, List<AllergyDetails> itemsList, Callback callback) {
        super(context, layoutResourceId, itemsList);

        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layoutResourceId = layoutResourceId;
        this.itemsList = itemsList;

        this.deletedItemsLists = new ArrayList<AllergyDetails>();
    }

    @Override
    public AllergyDetails getItem(int position) { // << subclasses can use subtypes in overridden methods!
        return itemsList.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        this.parent = parent;

        ViewHolder viewHolder;

        if ( convertView == null ) {
            convertView = inflater.inflate(layoutResourceId, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.allergy_name = (TextView) convertView.findViewById(R.id.allergy_name);
            //viewHolder.delete_allergy = (FrameLayout) convertView.findViewById(R.id.delete_allergy);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //-------------------------------------------------

        final AllergyDetails allergyDetails = getItem(position);

        viewHolder.allergy_name.setText(new CommonMethods().capitalizeFirstLetter(allergyDetails.getName()));

        /*final View itemView = convertView;
        viewHolder.delete_allergy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemView.setVisibility(View.GONE);
                deleteAllergy(position);
                if ( callback != null ){
                    callback.onDelete(position);
                }
            }
        });*/

        return convertView;
    }

    public void deleteAllergy(int position){
        deletedItemsLists.add(itemsList.get(position));
        itemsList.remove(position);
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItems(List<AllergyDetails> items){

        if ( itemsList == null ){   itemsList = new ArrayList<>();  }
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public List<AllergyDetails> getAllAllergies(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public AllergyDetails getAllergyItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }
}