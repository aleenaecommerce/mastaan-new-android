package in.gocibo.foodie.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.CommonMethods;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.models.CartItemDetails;

public class CartItemsAdapter extends RecyclerView.Adapter<CartItemsAdapter.ViewHolder> {

    Context context;
    List<CartItemDetails> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public interface CallBack {
        void onItemMenuClick(int position, View view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView item_name;
        TextView item_quantity;
        TextView item_price;
        TextView delivery_slot;
        Button menuOptions;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            item_name = (TextView) itemLayoutView.findViewById(R.id.item_name);
            item_quantity = (TextView) itemLayoutView.findViewById(R.id.item_quantity);
            item_price = (TextView) itemLayoutView.findViewById(R.id.item_price);
            delivery_slot = (TextView) itemLayoutView.findViewById(R.id.delivery_slot);
            menuOptions = (Button) itemLayoutView.findViewById(R.id.menuOptions);
        }
    }

    public CartItemsAdapter(Context context, List<CartItemDetails> itemsList, CallBack callBack) {
        this.context = context;
        this.itemsList = itemsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public CartItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_cart_item, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final CartItemDetails cartItemDetails = itemsList.get(position);    // Getting Cart Item Details using Position...

        viewHolder.item_name.setText(CommonMethods.capitalizeFirstLetter(cartItemDetails.getDishName()));

        viewHolder.delivery_slot.setText(CommonMethods.capitalizeFirstLetter(cartItemDetails.getDBY()));

        if ( cartItemDetails.getDishQuantityType().equalsIgnoreCase("n")){
            viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(cartItemDetails.getQuantity())+" servings");
            if ( cartItemDetails.getQuantity() == 1 ){
                viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(cartItemDetails.getQuantity())+" serving");
            }
        }
        else if ( cartItemDetails.getDishQuantityType().equalsIgnoreCase("w")){
            viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(cartItemDetails.getQuantity())+" kgs");
            if ( cartItemDetails.getQuantity() == 1 ){
                viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(cartItemDetails.getQuantity())+" kg");
            }
        }
        else if ( cartItemDetails.getDishQuantityType().equalsIgnoreCase("v")){
            viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(cartItemDetails.getQuantity())+" L");
        }

        viewHolder.item_price.setText(context.getResources().getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(cartItemDetails.getQuantity() * cartItemDetails.getDishPrice()));

        if ( callBack != null && viewHolder.menuOptions != null ){
            viewHolder.menuOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemMenuClick(position, viewHolder.menuOptions);
                }
            });
        }
    }

    public void addItems(List<CartItemDetails> items){

        for(int i=items.size()-1;i>=0;i--){
            itemsList.add(items.get(i));
        }
        notifyDataSetChanged();
    }

    public void deleteCartItem(int position){
        if ( itemsList.size() > position ){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }
    public void updateCartItem(int position, CartItemDetails updatedCartItemDetailsDetails){
        if ( itemsList.size() > position ){
            itemsList.set(position, updatedCartItemDetailsDetails);
            notifyDataSetChanged();
        }
    }

    public void deleteAllCartItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<CartItemDetails> getAllCartItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public CartItemDetails getCartItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}