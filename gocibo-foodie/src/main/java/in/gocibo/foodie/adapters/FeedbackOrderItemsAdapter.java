package in.gocibo.foodie.adapters;

import android.content.Context;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.fragments.FoodItemFeedbackFragment;
import in.gocibo.foodie.models.OrderItemDetails;

import com.aleena.common.widgets.vLinearLayout;

public class FeedbackOrderItemsAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {

    private float onFocusViewScale = 1.0f;
    private float offFocusViewScale = 0.7f;
    private float focusDiff = 0.3f;

    Callback callback;

    private FragmentManager fragmentManager;
    private Context context;
    private int pagerID;
    private List<OrderItemDetails> itemsList;

    public interface Callback{
        void onPageScrolled(int pageNumber);
    }

    public FeedbackOrderItemsAdapter(Context context, FragmentManager fragmentManager, int pagerID, List<OrderItemDetails> orderItems, Callback callback) {
        super(fragmentManager);
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.pagerID = pagerID;
        this.itemsList = orderItems;
        this.callback = callback;
    }

    @Override
    public Fragment getItem(int position) {
        return FoodItemFeedbackFragment.newInstance(context, position == 0?1.0f:0.7f, itemsList.get(position));
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    public void addItems(List<OrderItemDetails> items){
        if ( items != null ){
            for (int i=0;i<items.size();i++){
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        if ( getCount() > 0 ) {
            if (positionOffset >= 0f && positionOffset <= 1f) {
                vLinearLayout currentView = getRootView(position);
                currentView.setScaleBoth(onFocusViewScale - focusDiff * positionOffset);

                if (position < getCount() - 1) {
                    vLinearLayout nextView = getRootView(position + 1);
                    nextView.setScaleBoth(offFocusViewScale + focusDiff * positionOffset);
                }
            }
        }
    }

    @Override
    public void onPageSelected(int position) {
        if ( getCount() > 0 ) {
            vLinearLayout currentView = getRootView(position);
            currentView.findViewById(R.id.fader).setVisibility(View.GONE);
            if (position < getCount() - 1) {
                vLinearLayout nextView = getRootView(position + 1);
                nextView.findViewById(R.id.fader).setVisibility(View.VISIBLE);
            }
            if (position > 0) {
                vLinearLayout prevView = getRootView(position - 1);
                prevView.findViewById(R.id.fader).setVisibility(View.VISIBLE);
            }

            if (callback != null) {
                callback.onPageScrolled(position);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {}

    private vLinearLayout getRootView(int position) {
        return (vLinearLayout) fragmentManager.findFragmentByTag(this.getFragmentTag(position)).getView().findViewById(R.id.carouselHolder);   // noinspection ConstantConditions
    }

    private String getFragmentTag(int position) {
        return "android:switcher:" + pagerID + ":" + position;
    }
}

