package in.gocibo.foodie.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;

public class CuisinesFragment extends GociboFragment {

    LinearLayout cuisinesHolder;

    List<String> selectedCuisines = new ArrayList<String>();

    public CuisinesFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cuisines, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cuisinesHolder = (LinearLayout) view.findViewById(R.id.cuisinesHolder);

        setCuisinesList();

    }
    public void setCuisinesList(){

        final String[] cuisinesList = new String[0];

        for(int i=0;i<cuisinesList.length;i++){

            final int index = i;

            View cuisineView = inflater.inflate(R.layout.view_check_item, null, false);
            cuisinesHolder.addView(cuisineView);
            final CheckBox checkbox = (CheckBox) cuisineView.findViewById(R.id.checkbox);
            checkbox.setText(cuisinesList[i]);

            cuisineView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkbox.isChecked() == false) {
                        selectedCuisines.add(cuisinesList[index]);
                        checkbox.setChecked(true);
                    }
                    else {
                        selectedCuisines.remove(cuisinesList[index]);
                        checkbox.setChecked(false);
                    }
                }
            });
        }
    }

}
