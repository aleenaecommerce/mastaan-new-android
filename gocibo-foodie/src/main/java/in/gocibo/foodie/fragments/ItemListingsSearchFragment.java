package in.gocibo.foodie.fragments;

import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.activities.ChefDetailsActivity;
import in.gocibo.foodie.activities.FoodItemDetailsActivity;
import in.gocibo.foodie.activities.ItemListingsActivity;
import in.gocibo.foodie.adapters.FoodItemsAdapter;
import in.gocibo.foodie.backend.FoodItemsAPI;
import in.gocibo.foodie.backend.model.RequestSearchUploads;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.CategoryDetails;
import in.gocibo.foodie.models.CuisineDetails;
import in.gocibo.foodie.models.FoodItemDetails;

public class ItemListingsSearchFragment extends GociboFragment {

    ItemListingsActivity itemListingsActivity;
    CategoryDetails categoryDetails;

    CardView info_layout;
    TextView no_of_results;
    Button clear_filter;

    vRecyclerView foodItemsHolder;
    FoodItemsAdapter foodItemsAdapter;
    FoodItemsAdapter.Callback foodItemsCallback;

    RequestSearchUploads requestSearchUploads;

    public ItemListingsSearchFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_listings_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        itemListingsActivity = (ItemListingsActivity) getActivity();
        categoryDetails = itemListingsActivity.getCategoryDetails();

        //----------------

        info_layout = (CardView) view.findViewById(R.id.info_layout);

        no_of_results = (TextView) view.findViewById(R.id.no_of_results);
        clear_filter = (Button) view.findViewById(R.id.clear_filter);
        clear_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemListingsActivity.switchToHome();
            }
        });

        foodItemsHolder = (vRecyclerView) view.findViewById(R.id.foodItemsHolder);
        setupFoodItemsHolder();

        //---------------------

    }

    public void setupFoodItemsHolder(){

        foodItemsHolder.setHasFixedSize(true);
        foodItemsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        foodItemsHolder.setItemAnimator(new DefaultItemAnimator());

        foodItemsCallback = new FoodItemsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                FoodItemDetails foodItemDetails = foodItemsAdapter.getItem(position);

                Intent foddItemDetailsAct = new Intent(context, FoodItemDetailsActivity.class);
                foddItemDetailsAct.putExtra("dishID", foodItemDetails.getID());
                foddItemDetailsAct.putExtra("categoryDetails", new Gson().toJson(itemListingsActivity.getCategoryDetails()));
                //foddItemDetailsAct.putExtra("hasStock", (foodItemDetails.getCurrentAvailability() > 0));
                startActivityForResult(foddItemDetailsAct, Constants.HOME_ACTIVITY_CODE);
            }

            @Override
            public void onChefClick(int position) {
                Intent chefAct = new Intent(context, ChefDetailsActivity.class);
                chefAct.putExtra("chefID", foodItemsAdapter.getItem(position).getChefID());
                startActivity(chefAct);
            }

            @Override
            public void onCartClick(int position) {
                itemListingsActivity.addItemToCart(foodItemsAdapter.getItem(position));
            }
        };

        foodItemsAdapter = new FoodItemsAdapter(context, new ArrayList<FoodItemDetails>(), categoryDetails, foodItemsCallback);
        foodItemsHolder.setAdapter(foodItemsAdapter);

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        searchFoodItems(requestSearchUploads);
    }

    public void searchFoodItems(RequestSearchUploads requestObject){

        this.requestSearchUploads = requestObject;

        foodItemsAdapter.clearItems();
        showLoadingIndicator("Searching with Filters, wait..");

        getBackendAPIs().getFoodItemsAPI().getFoodItems(requestSearchUploads, new FoodItemsAPI.FoodItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<FoodItemDetails> foodItems, List<CuisineDetails> availableCuisines) {

                if (status == true) {

                    if (message.equalsIgnoreCase("OK")) {
                        switchToContentPage();

                        if (foodItems.size() > 0) {
                            no_of_results.setText(foodItems.size() + " results found");
                            if (foodItems.size() == 1) {
                                no_of_results.setText(foodItems.size() + " result found");
                            }
                            foodItemsAdapter.addItems(foodItems);
                            info_layout.setVisibility(View.GONE);
                        } else {
                            no_of_results.setText("No results");
                            info_layout.setVisibility(View.VISIBLE);
                            TextView title = (TextView) info_layout.findViewById(R.id.title);
                            TextView subtitle = (TextView) info_layout.findViewById(R.id.subtitle);
                            title.setText("Nothing to show.");
                            subtitle.setText("Your search did not return any results. Please try changing your search parameters.");
                        }
                    } else if (message.equalsIgnoreCase("slot_closed")) {
                        //homeActivity.onSlotEnded();
                    }
                } else {
                    showReloadIndicator(statusCode, "Unable to get Search results, try again!");
                }
            }
        });

    }

    //====================

    public String getSearchName(){
        return requestSearchUploads.getSearchName();
    }

    public int getItemsCount(){
        return foodItemsAdapter.getItemsCount();
    }

    public void updateFoodItemAvailability(String dishID, double newAvailability){
        foodItemsAdapter.updateItemAvailability(dishID, newAvailability);
    }

    public void updateFoodItemsAvailabilityFromFoodItems(List<FoodItemDetails> toUpdateItems){
        foodItemsAdapter.updateItemsAvailabilityFromFoodItems(toUpdateItems);
    }

    //==========================

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if (requestCode == Constants.HOME_ACTIVITY_CODE && resultCode == Constants.HOME_ACTIVITY_CODE) {
                itemListingsActivity.showItemAddedInfo(data.getStringExtra("item_name"), data.getStringExtra("item_quantity_type"), data.getDoubleExtra("item_quantity", 0), data.getStringExtra("item_slot"));
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

}
