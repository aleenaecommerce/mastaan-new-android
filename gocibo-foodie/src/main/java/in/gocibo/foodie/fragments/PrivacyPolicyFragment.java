package in.gocibo.foodie.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import in.gocibo.foodie.R;
import com.aleena.common.widgets.vWebViewProgressIndicator;

public class PrivacyPolicyFragment extends GociboFragment {

    WebView webview_privacy_policy;

    public PrivacyPolicyFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_privacy_policy, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        webview_privacy_policy = (WebView) view.findViewById(R.id.webview_privacy_policy);
        webview_privacy_policy.getSettings().setJavaScriptEnabled(true);
        webview_privacy_policy.setWebChromeClient(new vWebViewProgressIndicator(new vWebViewProgressIndicator.CallBack() {
            @Override
            public void onLoading(int progress) {
                if ( progress == 100 ){
                    switchToContentPage();
                }
            }
        }));

        webview_privacy_policy.loadUrl("file:///android_asset/html/GoCiboPrivacyPolicy.html");
        showLoadingIndicator("Loading GoCibo Privacy poicy, wait..");

    }
}
