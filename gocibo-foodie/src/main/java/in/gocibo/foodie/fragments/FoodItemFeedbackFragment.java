package in.gocibo.foodie.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vCircularImageView;
import com.aleena.common.widgets.vLinearLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import in.gocibo.foodie.R;
import in.gocibo.foodie.models.OrderItemDetails;

public class FoodItemFeedbackFragment extends GociboFragment {

    float viewScale;
    OrderItemDetails orderItemDetails;

    Context context;
    vLinearLayout carouselHolder;
    ImageView item_image;
    vCircularImageView chef_image;
    View item_type_indicator;
    TextView item_name, chef_name;

    public static Fragment newInstance(Context context, float viewScale, OrderItemDetails orderItemDetails) {
        Bundle bundle = new Bundle();
        bundle.putFloat("viewScale", viewScale);
        bundle.putString("orderItemDetails", new Gson().toJson(orderItemDetails));
        return Fragment.instantiate(context, FoodItemFeedbackFragment.class.getName(), bundle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_feedback_food_item, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle data) {
        super.onViewCreated(view, data);
        this.context = getActivity();

        viewScale = getArguments().getFloat("viewScale");
        orderItemDetails = new Gson().fromJson(getArguments().getString("orderItemDetails"), OrderItemDetails.class);
        carouselHolder = (vLinearLayout) view.findViewById(R.id.carouselHolder);
        carouselHolder.setScaleBoth(viewScale);

        item_image = (ImageView) view.findViewById(R.id.item_image);
        chef_image = (vCircularImageView) view.findViewById(R.id.chef_image);
        item_type_indicator = (View) view.findViewById(R.id.item_type_indicator);
        item_name = (TextView) view.findViewById(R.id.item_name);
        chef_name = (TextView) view.findViewById(R.id.chef_name);

        item_name.setText(CommonMethods.capitalizeFirstLetter(orderItemDetails.getDishName()));
        chef_name.setText(CommonMethods.capitalizeStringWords(orderItemDetails.getDishChefFullName()));

        Picasso.get()
                .load(orderItemDetails.getDishImage())
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .fit()
                .centerCrop()
                .tag(context)
                .into(item_image);

        Picasso.get()
                .load(orderItemDetails.getDishChefImage())
                .placeholder(R.drawable.ic_chef)
                .error(R.drawable.ic_chef)
                .fit()
                .centerCrop()
                .tag(context)
                .into(chef_image);

        if ( orderItemDetails.getDishType().equalsIgnoreCase("veg") ){
            item_type_indicator.setBackgroundColor(context.getResources().getColor(R.color.veg_item_color));
        }else if ( orderItemDetails.getDishType().equalsIgnoreCase("non-veg") ){
            item_type_indicator.setBackgroundColor(context.getResources().getColor(R.color.nonveg_item_color));
        }

    }
}