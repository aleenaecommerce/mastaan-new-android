package in.gocibo.foodie.fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;
import com.google.gson.Gson;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.activities.ProfileActivity;
import in.gocibo.foodie.activities.ProfileEditActivity;
import in.gocibo.foodie.backend.ProfileAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.AllergyDetails;
import in.gocibo.foodie.models.BuyerDetails;
import in.gocibo.foodie.models.ProfileSummaryDetails;

public class ProfileSummaryFragment extends GociboFragment implements View.OnClickListener{

    ProfileActivity profileActivity;

    TextView user_name, join_date, user_email, user_phone, user_dob, user_anniversary, no_of_orders, no_of_allergies;
    FrameLayout orders, allergies;

    BuyerDetails buyerDetails;

    public ProfileSummaryFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_profile_summary, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        profileActivity = (ProfileActivity) getActivity();      // Accessing Activity from Fragment..

        //------------------------------------------------------------

        user_name = (TextView) view.findViewById(R.id.user_name);
        join_date= (TextView) view.findViewById(R.id.join_date);
        user_email = (TextView) view.findViewById(R.id.user_email);
        user_phone = (TextView) view.findViewById(R.id.user_phone);
        user_dob = (TextView) view.findViewById(R.id.user_dob);
        user_anniversary = (TextView) view.findViewById(R.id.user_anniversary);
        no_of_orders = (TextView) view.findViewById(R.id.no_of_orders);
        no_of_allergies = (TextView) view.findViewById(R.id.no_of_allergies);

        orders = (FrameLayout) view.findViewById(R.id.orders);
        orders.setOnClickListener(this);
        allergies = (FrameLayout) view.findViewById(R.id.allergies);
        allergies.setOnClickListener(this);

        //--------------

        getProfileDetails();        //  Getting User Profile Details.

    }

    @Override
    public void onClick(View view) {

        if ( view == orders ){
            profileActivity.changePage("Order history");
        }
        else if ( view == allergies ){
            profileActivity.changePage("Allergies");
        }
    }

    public void toggleEditButton() {
        if ( buyerDetails != null ){
            toggleEditButton(true);
        }else {
            toggleEditButton(false);
        }
    }

    private void toggleEditButton(boolean status){
        if ( profileActivity != null ){
            if ( profileActivity.profileItems[profileActivity.pageNumber].equalsIgnoreCase("Profile") ){
                profileActivity.setFABVisibility(status);
            }
        }
    }

    private void displayProfileDetails(ProfileSummaryDetails profileSummaryDetails){

        no_of_orders.setText("" + profileSummaryDetails.getNoOfOrders());
        switchToContentPage();

        displayBuyerDetails(profileSummaryDetails.getBuyerDetails());

    }

    private void displayBuyerDetails(BuyerDetails buyerDetails){

        localStorageData.setBuyerDetails(buyerDetails);
        this.buyerDetails = buyerDetails;

        user_name.setText(buyerDetails.getName());
        join_date.setText("Joined on "+ DateMethods.getDateInFormat(buyerDetails.getJoinedDate(), DateConstants.MMM_DD));
        user_email.setText(buyerDetails.getEmail());
        user_phone.setText(buyerDetails.getMobile());
        user_dob.setText(DateMethods.getDateInFormat(buyerDetails.getDOB(), DateConstants.MMM_DD));

        if ( buyerDetails.getMatrialStatus() ){
            user_anniversary.setText(DateMethods.getDateInFormat(buyerDetails.getAnniversary(), DateConstants.MMM_DD));
        }else {
            user_anniversary.setText("");
        }

        if ( buyerDetails.getAllergies() != null ) {
            no_of_allergies.setText("" + buyerDetails.getAllergies().size());
        }
        switchToContentPage();
    }

    public void updateBuyerAllergeis(List<AllergyDetails> buyerAllergies){
        if ( buyerAllergies != null ) {
            buyerDetails.setAllergies(buyerAllergies);
            no_of_allergies.setText(""+buyerAllergies.size());
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getProfileDetails();
    }

    private void getProfileDetails(){

        toggleEditButton(false);
        showLoadingIndicator("Loading your profile, wait..");
        getBackendAPIs().getProfileAPI().getProfileDetails(new ProfileAPI.ProfileDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, ProfileSummaryDetails profileSummaryDetails) {
                if (status == true) {
                    displayProfileDetails(profileSummaryDetails);
                    toggleEditButton(true);
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load your profile, try again!");
                }
            }
        });

    }

    @Override
    public void onFABPressed() {
        super.onFABPressed();
        Intent profileEditAct = new Intent(getActivity(), ProfileEditActivity.class);
        profileEditAct.putExtra("buyer_details_json", new Gson().toJson(buyerDetails));
        startActivityForResult(profileEditAct, Constants.PROFILE_ACTIVITY_CODE);
    }

    //======================

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);

        try{
            if ( resultCode == Constants.PROFILE_ACTIVITY_CODE && requestCode == Constants.PROFILE_ACTIVITY_CODE ){

                String buyer_details_json = resultData.getStringExtra("profile_data_json");
                BuyerDetails updatedBuyerDetails = new Gson().fromJson(buyer_details_json, BuyerDetails.class);

                displayBuyerDetails(updatedBuyerDetails);

                if (profileActivity != null) {
                    showSnackbarMessage(profileActivity.activityFAB, "Profile updated successfully.", null, null);
                }else{
                    showSnackbarMessage("Profile updated successfully.", null, null);
                }
            }

        }catch (Exception e){   e.printStackTrace();    }
    }

}
