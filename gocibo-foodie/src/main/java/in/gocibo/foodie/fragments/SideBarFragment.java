package in.gocibo.foodie.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.freshdesk.hotline.FaqOptions;
import com.freshdesk.hotline.Hotline;
import com.freshdesk.hotline.HotlineConfig;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.gocibo.foodie.R;
import in.gocibo.foodie.activities.BecomeChefActivity;
import in.gocibo.foodie.activities.ChefListActivity;
import in.gocibo.foodie.activities.LegalActivity;
import in.gocibo.foodie.activities.LocationSelectionActivity;
import in.gocibo.foodie.activities.MapLocationSelectionActivity;
import in.gocibo.foodie.activities.OrderHistoryActivity;
import in.gocibo.foodie.activities.ProfileActivity;
import in.gocibo.foodie.activities.ReferFriendActivity;
import in.gocibo.foodie.activities.SettingsActivity;
import in.gocibo.foodie.backend.CommonAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.interfaces.RegistrationCallback;
import in.gocibo.foodie.models.BootStrapDetails;
import in.gocibo.foodie.models.BuyerDetails;

public class SideBarFragment extends GociboFragment implements View.OnClickListener{

    String[] drawerListItems;

    LinearLayout header_location;
    ImageView header_logo;
    LinearLayout itemsHolder;
    View []itemViewHolders;

    AlertDialog supportDialog;
    Button deliver_location;
    Button helpdesk, send_message, call_us;

    SideBarCallback callback;

    public interface SideBarCallback{
        void onItemSelected(int position);
        void onDeliverLocationChange(PlaceDetails deliverLocationDetails);
    }

    public SideBarFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sidebar, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //MobihelpConfig config = new MobihelpConfig(getString(R.string.mobihelp_app_url), getString(R.string.mobihelp_app_key), getString(R.string.mobihelp_app_secret_key));
        //Mobihelp.init(context, config);

        HotlineConfig hotlineConfig=new HotlineConfig(getString(R.string.hotline_id), getString(R.string.hotline_key));
        hotlineConfig.setVoiceMessagingEnabled(false);
        hotlineConfig.setVoiceMessagingEnabled(true);
        hotlineConfig.setCameraCaptureEnabled(true);
        hotlineConfig.setPictureMessagingEnabled(true);
        Hotline.getInstance(context).init(hotlineConfig);

        registerFragmentReceiver(Constants.SIDEBAR_RECEIVER);

        //.......................................

        supportDialog = new AlertDialog.Builder(context).create();
        LayoutInflater inflater = LayoutInflater.from(context);
        View supportView = inflater.inflate(R.layout.dialog_support, null);
        supportDialog.setView(supportView);

        header_location = (LinearLayout) view.findViewById(R.id.header_location);
        header_logo = (ImageView) view.findViewById(R.id.header_logo);

        deliver_location = (Button) view.findViewById(R.id.deliver_location);
        deliver_location.setOnClickListener(this);

        helpdesk = (Button) supportView.findViewById(R.id.helpdesk);
        helpdesk.setOnClickListener(this);
        send_message = (Button) supportView.findViewById(R.id.send_message);
        send_message.setOnClickListener(this);
        call_us = (Button) supportView.findViewById(R.id.call_us);
        call_us.setOnClickListener(this);

        itemsHolder = (LinearLayout) view.findViewById(R.id.itemsHolder);

        //--------------------------------------------------

        displayDeliveryLocation();
        setListItems();     // Set Items List...

    }

    @Override
    public void onClick(View view) {

        if ( view == deliver_location ){

            if ( callback != null ){
                callback.onItemSelected(-1);
            }

            if ( localStorageData.isBuyerHasFavourites() ){
                Intent locationSelectionAct = new Intent(context, LocationSelectionActivity.class);
                locationSelectionAct.putExtra("isForActivityResult", true);
                startActivityForResult(locationSelectionAct, Constants.LOCATION_SELECTOR_ACTIVITY_CODE);
            }else{
                Intent mapLocationSelectionAct = new Intent(context, MapLocationSelectionActivity.class);
                mapLocationSelectionAct.putExtra("isForActivityResult", true);
                startActivityForResult(mapLocationSelectionAct, Constants.LOCATION_SELECTOR_ACTIVITY_CODE);
            }
        }
        else if ( view == helpdesk ){
            supportDialog.dismiss();

            //Mobihelp.showSolutions(context);
            FaqOptions faqOptions = new FaqOptions()
                    .showFaqCategoriesAsGrid(true)
                    .showContactUsOnAppBar(true)
                    .showContactUsOnFaqScreens(false)
                    .showContactUsOnFaqNotHelpful(false)
                    .filterByTags(CommonMethods.getStringListFromStringArray(new String[]{"payment"}), "Payment");
            Hotline.showFAQs(context);//, faqOptions);
        }
        else if ( view == send_message ){
            supportDialog.dismiss();

            //Mobihelp.showConversations(context);
            Hotline.showConversations(context);
        }
        else if ( view == call_us ){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
            startActivity(intent);
            supportDialog.dismiss();
        }
    }

    public void displayDeliveryLocation(){

        if( localStorageData.getDeliveryLocation() != null && localStorageData.getDeliveryLocation().getFullAddress() != null ) {
            header_location.setVisibility(View.VISIBLE);
            header_logo.setVisibility(View.GONE);
            deliver_location.setText(localStorageData.getDeliveryLocation().getFullAddress());
        }
        else{
            header_location.setVisibility(View.GONE);
            header_logo.setVisibility(View.VISIBLE);
        }
    }

    public void setListItems(){

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        drawerListItems =  new String[]{"You", "Our Chefs", "Order history", "Refer a friend", "Become a GoCibo Chef", "Support", "Feedback", "Rate our app", "Settings", "Legal", "Login/Register"};//, "Logout"};//getResources().getStringArray(R.array.drawerListItems);
        int[] listImages = new int[]{R.drawable.ic_profile, R.drawable.ic_chef, R.drawable.ic_previous, R.drawable.ic_refer_friend, R.drawable.ic_silverware_grey, R.drawable.ic_support, R.drawable.ic_flower_grey, R.drawable.ic_rate, R.drawable.ic_settings, R.drawable.ic_gavel_grey, R.drawable.ic_login_grey};//, R.drawable.ic_logout};//R.drawable.ic_profile,R.drawable.ic_favourite_grey, R.drawable.ic_previous_orders, R.drawable.ic_loyality_center, R.drawable.ic_settings
        List<Map<String, Object>> listItems = new ArrayList<>();

        for (int i = 0; i < drawerListItems.length; i++) {
            Map<String, Object> item = new HashMap<>();
            item.put("itemName", drawerListItems[i]);
            item.put("itemImage", listImages[i]);
            listItems.add(item);
        }

        itemViewHolders = new View[listItems.size()];

        for(int i=0;i<listItems.size();i++){
            final int position = i;
            View itemView = inflater.inflate(R.layout.view_sidebar_item, null, false);
            itemViewHolders [i] = itemView;
            itemsHolder.addView(itemView);

            ImageView image = (ImageView) itemView.findViewById(R.id.image);
            image.setBackgroundResource((int) listItems.get(position).get("itemImage"));
            TextView title = (TextView) itemView.findViewById(R.id.title);
            title.setText(listItems.get(position).get("itemName").toString());

            Button itemSelector = (Button) itemView.findViewById(R.id.itemSelector);
            itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sideBarItemSelect(position);
                }
            });
        }

        updateSidebarBasedOnSessioin(false);

    }

    @Override
    public void onFragmentReceiverMessage(String receiverName, Intent receivedData) {
        super.onFragmentReceiverMessage(receiverName, receivedData);

        try{
            String type = receivedData.getStringExtra("type");

            if ( type.equalsIgnoreCase("session_update") ){
                updateSidebarBasedOnSessioin(true);
            }
            else if ( type.equalsIgnoreCase("update_delivery_address_name") ){
                displayDeliveryLocation();
            }
        }catch (Exception e){}
    }

    public void updateSidebarBasedOnSessioin(boolean loadBootstrap){

        if ( localStorageData.getSessionFlag() == true ){

            if ( loadBootstrap == true ){
                getUserBootStrapDetails(0);
            }

            for (int i=0;i<drawerListItems.length;i++){
                if ( drawerListItems[i].equalsIgnoreCase("You") || drawerListItems[i].equalsIgnoreCase("Order history") || drawerListItems[i].equalsIgnoreCase("Refer a friend") ){//|| drawerListItems[i].equalsIgnoreCase("Logout") ){
                    itemViewHolders[i].setVisibility(View.VISIBLE);
                }
                if ( drawerListItems[i].equalsIgnoreCase("Login/Register") ){
                    itemViewHolders[i].setVisibility(View.GONE);
                }
            }
        }
        else{
            for (int i=0;i<drawerListItems.length;i++){
                if ( drawerListItems[i].equalsIgnoreCase("You") || drawerListItems[i].equalsIgnoreCase("Order history") || drawerListItems[i].equalsIgnoreCase("Refer a friend") ){//|| drawerListItems[i].equalsIgnoreCase("Logout") ){
                    itemViewHolders[i].setVisibility(View.GONE);
                }
                if ( drawerListItems[i].equalsIgnoreCase("Login/Register") ){
                    itemViewHolders[i].setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void getUserBootStrapDetails(final int retryCount){

        if ( retryCount < 5 && localStorageData.getSessionFlag() == true ) {

            getBackendAPIs().getCommonAPI().getBootStrap(getAppVersionCode(), new CommonAPI.BootStrapCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, final BootStrapDetails bootstrapObject) {

                    if (status == true) {
                        localStorageData.setBuyerDetails(bootstrapObject.getBuyerDetails());
                        localStorageData.setBuyerAllergies(bootstrapObject.getBuyerDetails().getAllergies());
                        localStorageData.setBuyerPendingOrders(bootstrapObject.getPendingOrders());
                        localStorageData.setCartCount(bootstrapObject.getCartItemsCount());
                        localStorageData.setCartRemainingTime(bootstrapObject.getCartRemainingTime());

                        if ( localStorageData.getNoOfPendingOrders() > 0 ){
                            context.startService(firebaseService);
                        }

                        if ( bootstrapObject.getCartItemsCount() > 0 ){
                            AddressBookItemDetails deliveryLocation = localStorageData.getDeliveryLocation();
                            float distance = -1;
                            if ( deliveryLocation != null ){
                                distance = LatLngMethods.getDistanceBetweenLatLng(deliveryLocation.getLatLng(), bootstrapObject.getCartDeliveryAddressLatLng());
                            }

                            if ( distance >= 0 && distance <= 200 ){
                                deliveryLocation.setFullName(bootstrapObject.getCartDeliveryAddressFullName());
                                localStorageData.setDeliveryLocation(deliveryLocation);
                            }else{
                                Log.d(Constants.LOG_TAG, "Distance between cartDeliveryLocation and currentDeliveryLocation is " + distance + " is > 100m or NOT SAME, Fetching CartDeliveryLocation details");
                                getGooglePlacesAPI().getPlaceDetailsByLatLng(bootstrapObject.getCartDeliveryAddressLatLng(), new GooglePlacesAPI.PlaceDetailsCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                                        if (status == true) {
                                            placeDetails.setFullName(bootstrapObject.getCartDeliveryAddressFullName());
                                            localStorageData.setDeliveryLocation(new AddressBookItemDetails(placeDetails));
                                            displayDeliveryLocation();
                                            Toast.makeText(context, "Your Delivery location is changed to match your cart items delivery location", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                        }

                        Intent homeIntent = new Intent(Constants.HOME_RECEIVER);
                        homeIntent.putExtra("type", "display_user_session_info");
                        context.sendBroadcast(homeIntent);
                    }
                    else {
                        if ( statusCode == 403 ){
                            clearUserSession("Your session has expired.");
                        }else{
                            new CountDownTimer(3000, 1000) {
                                @Override
                                public void onTick(long l) {}
                                @Override
                                public void onFinish() {
                                    getUserBootStrapDetails(retryCount + 1);
                                }
                            }.start();
                        }
                    }
                }
            });
        }
    }

    public void sideBarItemSelect(final int position) {

        if ( callback != null ){
            callback.onItemSelected(position);
        }

        if ( drawerListItems[position].equalsIgnoreCase("Home") ){
            /*Intent profileAct = new Intent(context, ProfileActivity.class);
            startActivity(profileAct);*/
        }
        else if ( drawerListItems[position].equalsIgnoreCase("You") ){
            Intent profileAct = new Intent(context, ProfileActivity.class);
            startActivity(profileAct);
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Our chefs") ){
            Intent chefListAct = new Intent(context, ChefListActivity.class);
            startActivity(chefListAct);
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Order history") ){
            Intent orderHistoryAct = new Intent(context, OrderHistoryActivity.class);
            startActivity(orderHistoryAct);
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Refer a friend") ){

            String shareSubject = "GoCibo";
            String shareMessage = "Check out GoCibo (http://www.gocibo.in). They are delivering amazing homemade food...";
            if ( localStorageData.getSessionFlag() == true ){
                String buyerReferralID = localStorageData.getBuyerReferralID();
                if (  buyerReferralID != null && buyerReferralID.length() > 0 ){
                    shareMessage = "Check out GoCibo (http://www.gocibo.in/invite/?r="+buyerReferralID+"). They are delivering amazing homemade food...";
                }
            }

            Intent referFriendAct = new Intent(context, ReferFriendActivity.class);
            referFriendAct.putExtra("shareSubject", shareSubject);
            referFriendAct.putExtra("shareMessage", shareMessage);
            startActivity(referFriendAct);
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Become a Gocibo chef") ){
            Intent becomeChefAct = new Intent(context, BecomeChefActivity.class);
            startActivity(becomeChefAct);
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Legal") ){
            Intent legalAct = new Intent(context, LegalActivity.class);
            startActivity(legalAct);
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Support") ){
            supportDialog.show();
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Feedback") ){
            //Mobihelp.showFeedback(context);
            Hotline.showConversations(context);

            /*Intent feedbackAct = new Intent(context, FoodItemFeedbackActivity.class);
            startActivity(feedbackAct);*/
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Rate our app") ){
            Log.d(Constants.LOG_TAG, "Going to Rate App with Package Name = " + context.getPackageName());
            Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
            }
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Settings") ){
            Intent settingsAct = new Intent(context, SettingsActivity.class);
            startActivity(settingsAct);
        }
        else if ( drawerListItems[position].equalsIgnoreCase("Login/Register") ){
            goToRegistration(new RegistrationCallback() {
                @Override
                public void onComplete(boolean status, String token, BuyerDetails buyerDetails) {
                    if ( status ){
                        updateSidebarBasedOnSessioin(true);
                        showToastMessage("Logged in successfully.");
                    }
                }
            });
        }
        else{
            showSnackbarMessage(drawerListItems[position] + " [ Under Construction ]", null, null);
        }
    }

    //-----------------------------------------

    public void setCallback(SideBarCallback callback){
        this.callback = callback;
    }

    public void changeDeliveryLocation(String deliveryLocationName){
        deliver_location.setText(deliveryLocationName);
    }

    //==========================================

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if ( requestCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE && resultCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE ) {      // If It is from PlaceSearch Activity
                String deliver_location_json = data.getStringExtra("deliver_location_json");
                PlaceDetails deliverLocationDetails = new Gson().fromJson(deliver_location_json, PlaceDetails.class);

                if ( callback != null && deliverLocationDetails != null ){
                    callback.onDeliverLocationChange(deliverLocationDetails);
                }
            }
        }catch (Exception e){e.printStackTrace();}

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
