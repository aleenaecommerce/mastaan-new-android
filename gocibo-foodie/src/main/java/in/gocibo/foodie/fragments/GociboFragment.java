package in.gocibo.foodie.fragments;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.IntentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleena.common.fragments.vFragment;
import com.google.gson.Gson;

import in.gocibo.foodie.GoCiboApplication;
import in.gocibo.foodie.R;
import in.gocibo.foodie.activities.LaunchActivity;
import in.gocibo.foodie.activities.RegisterWithMobileActivity;
import in.gocibo.foodie.backend.BackendAPIs;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.interfaces.RegistrationCallback;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.localdata.LocationDatabase;
import in.gocibo.foodie.methods.BroadcastReceiversMethods;
import in.gocibo.foodie.models.BuyerDetails;
import in.gocibo.foodie.services.FireBaseService;
import in.gocibo.foodie.services.TimedCartService;

public class GociboFragment extends vFragment {

    Context context;
    GoCiboApplication goCiboApplication;        // GoCibo Application Class

    BroadcastReceiversMethods broadcastReceiversMethods;
    LocalStorageData localStorageData;

    Intent timedCartService;
    Intent firebaseService;

    RegistrationCallback registrationCallback;

    public String GOCIBO_BASE_URL;
    private BackendAPIs backendAPIs;

    public GociboFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();

        goCiboApplication = (GoCiboApplication) getActivity().getApplication();       // Accessing Application Class;

        GOCIBO_BASE_URL = getString(R.string.gocibo_base_url);

        broadcastReceiversMethods = new BroadcastReceiversMethods(context);
        localStorageData = new LocalStorageData(context);
        backendAPIs = new BackendAPIs(getActivity());

        timedCartService = new Intent(context, TimedCartService.class);
        firebaseService = new Intent(context, FireBaseService.class);

    }

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(context);
        }
        return backendAPIs;
    }

    public void checkSessionValidity(int statusCode){
        if ( localStorageData.getSessionFlag() == true && statusCode == 403 ){
            clearUserSession("Your session has expired.");
        }
    }

    public void goToRegistration(RegistrationCallback registrationCallback){

        this.registrationCallback = registrationCallback;

        Intent registerAct = new Intent(context, RegisterWithMobileActivity.class);
        registerAct.putExtra("baseURL", GOCIBO_BASE_URL);
        startActivityForResult(registerAct, Constants.REGISTER_ACTIVITY_CODE);
    }


    public void clearUserSession(String messageToDisplay){

        localStorageData.clearBuyerSession();        // Clearing Session
        localStorageData.setFirstCartItemFlag(false);
        LocationDatabase locationDatabase = new LocationDatabase(context);
        locationDatabase.openDatabase();
        locationDatabase.deleteAllPlaces();

        showToastMessage(messageToDisplay);

        Intent loadingAct = new Intent(context, LaunchActivity.class);
        ComponentName componentName = loadingAct.getComponent();
//        Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
//        startActivity(mainIntent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goCiboApplication = (GoCiboApplication) getActivity().getApplication();       // Accessing Application Class;
    }

    //==============

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if ( requestCode == Constants.REGISTER_ACTIVITY_CODE ) {

                if ( resultCode == Activity.RESULT_OK ) {

                    localStorageData.storeBuyerSession(data.getStringExtra("token"), new Gson().fromJson(data.getStringExtra("buyerDetails"), BuyerDetails.class));

                    Intent sidebarReceiver = new Intent(Constants.SIDEBAR_RECEIVER);
                    sidebarReceiver.putExtra("type", "session_update");
                    sidebarReceiver.putExtra("mobileNumber", data.getStringExtra("mobileNumber"));
                    context.sendBroadcast(sidebarReceiver);

                    if (registrationCallback != null) {
                        registrationCallback.onComplete(true, localStorageData.getAccessToken(), localStorageData.getBuyerDetails());
                    }
                }else{
                    if (registrationCallback != null) {
                        registrationCallback.onComplete(false, null, null);
                    }
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }


}
