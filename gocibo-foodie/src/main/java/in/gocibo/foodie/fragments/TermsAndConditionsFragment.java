package in.gocibo.foodie.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.aleena.common.widgets.vWebViewProgressIndicator;

import in.gocibo.foodie.R;

public class TermsAndConditionsFragment extends GociboFragment {

    WebView webview_terms_and_conditions;

    public TermsAndConditionsFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        //-------

        webview_terms_and_conditions = (WebView) view.findViewById(R.id.webview_terms_and_conditions);
        webview_terms_and_conditions.getSettings().setJavaScriptEnabled(true);
        webview_terms_and_conditions.setWebChromeClient(new vWebViewProgressIndicator(new vWebViewProgressIndicator.CallBack() {
            @Override
            public void onLoading(int progress) {
                if ( progress == 100 ) {
                    switchToContentPage();
                }
            }
        }));

        webview_terms_and_conditions.loadUrl("file:///android_asset/html/GociboTermsAndConditions.html");//"file://html/GociboTermsAndConditions.html");
        showLoadingIndicator("Loading GoCibo Terms & Conditions, wait..");

    }
}
