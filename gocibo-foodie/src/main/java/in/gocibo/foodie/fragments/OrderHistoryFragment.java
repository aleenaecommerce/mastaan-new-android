package in.gocibo.foodie.fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import com.aleena.common.widgets.vRecyclerView;
import in.gocibo.foodie.R;
import in.gocibo.foodie.activities.OrderDetailsActivity;
import in.gocibo.foodie.adapters.OrdersHistoryAdapter;
import in.gocibo.foodie.backend.OrdersAPI;
import in.gocibo.foodie.models.OrderDetails;
import in.gocibo.foodie.constants.Constants;

public class OrderHistoryFragment extends GociboFragment {

    vRecyclerView ordersListHolder;
    OrdersHistoryAdapter ordersListAdapter;
    OrdersHistoryAdapter.CallBack ordersListCallback;

    public OrderHistoryFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();
        hasSwipeRefresh();

        registerFragmentReceiver(Constants.ORDER_HISTORY_RECEIVER);

        //--------------------------------------------------------

        ordersListHolder = (vRecyclerView) view.findViewById(R.id.ordersListHolder);
        ordersListHolder.setHasFixedSize(true);
        ordersListHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        ordersListHolder.setItemAnimator(new DefaultItemAnimator());

        ordersListCallback = new OrdersHistoryAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
                orderDetailsAct.putExtra("orderID", ordersListAdapter.getOrderItem(position).getID());
                startActivity(orderDetailsAct);
            }
        };

        ordersListAdapter = new OrdersHistoryAdapter(context, new ArrayList<OrderDetails>(), ordersListCallback);
        ordersListHolder.setAdapter(ordersListAdapter);

        getOrders();      // GetOrders

    }

    @Override
    public void onFragmentReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onFragmentReceiverMessage(activityReceiverName, receivedData);
        try {
            if (receivedData.getStringExtra("type").equalsIgnoreCase("update_order_status")) {
                String orderID = receivedData.getStringExtra("orderID");
                String orderStatus = receivedData.getStringExtra("orderStatus");

                Log.d(Constants.LOG_TAG, "Checking to update OrderDetails in OrderHistoryPage, for oID = " + orderID + " ,  with DS = " + orderStatus);

                ordersListAdapter.updateOrderStatus(orderID, orderStatus);
            }
        }catch (Exception e){}
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getOrders();      // GetMyOrders
    }

    public void getOrders(){

        ordersListAdapter.clearItems();

        showLoadingIndicator("Loading your orders, wait..");
        getBackendAPIs().getOrdersAPI().getOrders(new OrdersAPI.OrdersListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<OrderDetails> previousOrders) {

                if (status == true) {
                    if (previousOrders.size() > 0) {
                        ordersListAdapter.addItems(previousOrders);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("You are yet to try \nour tasty homemade food!!! ");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load your Orders, try again!");
                }
            }
        });
    }

}
