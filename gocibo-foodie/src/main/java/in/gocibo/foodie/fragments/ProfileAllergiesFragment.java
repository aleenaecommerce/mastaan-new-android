package in.gocibo.foodie.fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleena.common.widgets.vFlowLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.activities.AllergiesEditActivity;
import in.gocibo.foodie.activities.ProfileActivity;
import in.gocibo.foodie.adapters.AllergiesAdapter;
import in.gocibo.foodie.backend.ProfileAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.AllergyDetails;

public class ProfileAllergiesFragment extends GociboFragment {

    ProfileActivity profileActivity;
    vFlowLayout allergiesHolder;
    AllergiesAdapter allergiesAdapter;

    public ProfileAllergiesFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_allergies, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        profileActivity = (ProfileActivity) getActivity();

        allergiesHolder = (vFlowLayout) view.findViewById(R.id.allergiesHolder);
        allergiesAdapter = new AllergiesAdapter(context, R.layout.view_allergy_item, null, null);

        getAllergies();     //  Getting Buyer Allergies

    }

    @Override
    public void onFABPressed() {
        super.onFABPressed();
        ArrayList<String> buyer_allergies_jsons = new ArrayList<>();
        for(int i=0;i<allergiesAdapter.getItemsCount();i++){
            buyer_allergies_jsons.add(new Gson().toJson(allergiesAdapter.getAllergyItem(i)));
        }
        Intent allergiesEditAct = new Intent(getActivity(), AllergiesEditActivity.class);
        allergiesEditAct.putStringArrayListExtra("buyer_allergies_jsons", buyer_allergies_jsons);
        startActivityForResult(allergiesEditAct, Constants.PROFILE_ACTIVITY_CODE);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getAllergies();
    }

    public void getAllergies(){

        toggleEditButton(false);
        showLoadingIndicator("Loading your allergies, wait..");
        getBackendAPIs().getProfileAPI().getBuyerAllergies(new ProfileAPI.AllergiesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<AllergyDetails> buyerAllergies) {

                if (status == true) {

                    localStorageData.setBuyerAllergies(buyerAllergies);
                    profileActivity.setBuyerAllergies(buyerAllergies);
                    allergiesAdapter.addItems(new ArrayList<AllergyDetails>());

                    if (buyerAllergies.size() > 0) {
                        allergiesAdapter.addItems(buyerAllergies);
                        for (int i = 0; i < allergiesAdapter.getItemsCount(); i++) {
                            final View allergyView = allergiesAdapter.getView(i, null, null);
                            allergiesHolder.addView(allergyView);
                        }
                        switchToContentPage();
                    } else {
                        localStorageData.setBuyerAllergies(new ArrayList<AllergyDetails>());
                        profileActivity.setBuyerAllergies(new ArrayList<AllergyDetails>());
                        showNoDataIndicator("You haven't listed any allergies ");
                    }
                    toggleEditButton(true);
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load your Allergies, try again!");
                }
            }
        });
    }

    public void toggleEditButton() {
        if ( allergiesAdapter.getAllAllergies() != null ){
            toggleEditButton(true);
        }else {
            toggleEditButton(false);
        }
    }

    private void toggleEditButton(boolean status){
        if ( profileActivity != null ){
            if ( profileActivity.profileItems[profileActivity.pageNumber].equalsIgnoreCase("Allergies") ){
                profileActivity.setFABVisibility(status);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);

        try{
            if ( resultCode == Constants.PROFILE_ACTIVITY_CODE && requestCode == Constants.PROFILE_ACTIVITY_CODE ) {
                //String buyer_details_json = resultData.getStringExtra("profile_data_json");
                ArrayList<String> buyer_allergies_jsons = resultData.getStringArrayListExtra("buyer_allergies_jsons");
                List<AllergyDetails> selectedAllergies = new ArrayList<>();
                if (buyer_allergies_jsons != null) {
                    for (int i = 0; i < buyer_allergies_jsons.size(); i++) {
                        selectedAllergies.add(new Gson().fromJson(buyer_allergies_jsons.get(i), AllergyDetails.class));
                    }
                }
                allergiesAdapter.clearItems();
                allergiesHolder.removeAllViews();

                if (selectedAllergies.size() > 0) {
                    localStorageData.setBuyerAllergies(selectedAllergies);
                    allergiesAdapter.addItems(selectedAllergies);
                    for (int i = 0; i < allergiesAdapter.getItemsCount(); i++) {
                        final View allergyView = allergiesAdapter.getView(i, null, null);
                        allergiesHolder.addView(allergyView);
                    }
                    switchToContentPage();
                } else {
                    showNoDataIndicator("You haven't listed any allergies ");
                }

                //profileActivity.updateBuyerAllergies(selectedAllergies);
                if (profileActivity != null) {
                    showSnackbarMessage(profileActivity.activityFAB, "Allergies updated successfully.", null, null);
                }else{
                    showSnackbarMessage("Allergies updated successfully.", null, null);
                }
            }

        }catch (Exception e){   e.printStackTrace();    }

    }

}