package in.gocibo.foodie.fragments;

import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.activities.ChefDetailsActivity;
import in.gocibo.foodie.activities.FoodItemDetailsActivity;
import in.gocibo.foodie.activities.ItemListingsActivity;
import in.gocibo.foodie.adapters.FoodItemsAdapter;
import in.gocibo.foodie.backend.FoodItemsAPI;
import in.gocibo.foodie.backend.model.RequestSearchUploads;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.CategoryDetails;
import in.gocibo.foodie.models.CuisineDetails;
import in.gocibo.foodie.models.FoodItemDetails;

public class ItemListingsFragment extends GociboFragment{

    ItemListingsActivity itemListingsActivity;
    CategoryDetails categoryDetails;

    vRecyclerView foodItemsHolder;
    FoodItemsAdapter foodItemsAdapter;
    FoodItemsAdapter.Callback foodItemsCallback;

    public ItemListingsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);                    // Bcoz of Extending Custom Fragment
        return inflater.inflate(R.layout.fragment_item_listings, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();
        hasSwipeRefresh();

        itemListingsActivity = (ItemListingsActivity) getActivity();
        categoryDetails = itemListingsActivity.getCategoryDetails();

        //--------------------------------------------------------

        foodItemsHolder = (vRecyclerView) view.findViewById(R.id.foodItemsHolder);
        setupFoodItemsHolder();

        //------------

        //getFoodItems();       // It is called from ItemListingsActivity after Menu created.

    }

    public void setupFoodItemsHolder(){

        foodItemsHolder.setHasFixedSize(true);
        foodItemsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        foodItemsHolder.setItemAnimator(new DefaultItemAnimator());

        foodItemsCallback = new FoodItemsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                FoodItemDetails foodItemDetails = foodItemsAdapter.getItem(position);

                Intent foddItemDetailsAct = new Intent(context, FoodItemDetailsActivity.class);
                foddItemDetailsAct.putExtra("dishID", foodItemDetails.getID());
                foddItemDetailsAct.putExtra("categoryDetails", new Gson().toJson(itemListingsActivity.getCategoryDetails()));
                startActivityForResult(foddItemDetailsAct, Constants.HOME_ACTIVITY_CODE);
            }

            @Override
            public void onChefClick(int position) {
                Intent chefAct = new Intent(context, ChefDetailsActivity.class);
                chefAct.putExtra("chefID", foodItemsAdapter.getItem(position).getChefID());
                startActivity(chefAct);
            }

            @Override
            public void onCartClick(int position) {
                itemListingsActivity.addItemToCart(foodItemsAdapter.getItem(position));
            }
        };

        foodItemsAdapter = new FoodItemsAdapter(context, new ArrayList<FoodItemDetails>(), categoryDetails, foodItemsCallback);
        foodItemsHolder.setAdapter(foodItemsAdapter);

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        foodItemsAdapter.clearItems();
        getFoodItems();
    }

    public void displayFoodItems(List<FoodItemDetails> foodItems, List<CuisineDetails> cuisines){

        Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                .putExtra("type", "update_category_uploads_count")
                .putExtra("categoryID", categoryDetails.getID())
                .putExtra("uploadsCount", foodItems.size());
        context.sendBroadcast(homeReceiverIntent);

        foodItemsAdapter.addItems(foodItems);
        itemListingsActivity.updateFilterMenu(cuisines);
        itemListingsActivity.toggleSearchFilterIcons(foodItemsAdapter.getItemsCount());
        switchToContentPage();

    }
    public void getFoodItems(){

        itemListingsActivity.hideMenuItems();
        showLoadingIndicator("Loading " + categoryDetails.getName() + ", wait...");
        getBackendAPIs().getFoodItemsAPI().getFoodItems(new RequestSearchUploads(null, categoryDetails.getValue(), localStorageData.getDeliveryLocation().getLatLng(), null, null), new FoodItemsAPI.FoodItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<FoodItemDetails> foodItems, List<CuisineDetails> availableCuisines) {
                if (status) {
                    if (foodItems.size() > 0) {
                        displayFoodItems(foodItems, availableCuisines);
                    } else {
                        showNoDataIndicator("No items in "+categoryDetails.getName().toLowerCase());
                        String msg = "We couldn't fetch you any of our delicious food items. This could be because..."
                                + "\n\n"
                                + "\t 1. Our chefs haven’t uploaded any dishes yet (do check back again)"
                                + "\n\t 2. All our items are past their availability time"
                                + "\n\n"
                                + "We are currently serving at " + localStorageData.getServingAt();
                        showNotificationDialog(false, "Nothing to show!", msg, new ActionCallback() {
                            @Override
                            public void onAction() {
                                getActivity().onBackPressed();
                            }
                        });
                    }
                } else {
                    showReloadIndicator(statusCode, "Error in loading " + categoryDetails.getName() + ", try again!");
                }
            }
        });
    }

    //====================

    public int getItemsCount(){
        return foodItemsAdapter.getItemsCount();
    }

    public List<String> getLoadedItemsIDs(){
        List<FoodItemDetails> foodItems = foodItemsAdapter.getAllItems();
        List<String> foodItemsIDs = new ArrayList<>();
        if ( foodItems != null && foodItems.size() > 0 ){
            for (int i=0;i<foodItems.size();i++){
                foodItemsIDs.add(foodItems.get(i).getID());
            }
        }
        return foodItemsIDs;
    }

    public void updateFoodItemAvailability(String dishID, double newAvailability){
        foodItemsAdapter.updateItemAvailability(dishID, newAvailability);
    }

    public void updateFoodItemsAvailabilityFromFoodItems(List<FoodItemDetails> toUpdateItems){
        foodItemsAdapter.updateItemsAvailabilityFromFoodItems(toUpdateItems);
    }

    //==========================

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if (requestCode == Constants.HOME_ACTIVITY_CODE && resultCode == Constants.HOME_ACTIVITY_CODE) {
                itemListingsActivity.showItemAddedInfo(data.getStringExtra("item_name"), data.getStringExtra("item_quantity_type"), data.getDoubleExtra("item_quantity", 0), data.getStringExtra("item_slot"));
            }
        }catch (Exception e){   e.printStackTrace();    }
    }


}
