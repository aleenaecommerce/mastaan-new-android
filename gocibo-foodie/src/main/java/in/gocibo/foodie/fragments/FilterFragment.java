package in.gocibo.foodie.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.activities.ItemListingsActivity;
import in.gocibo.foodie.models.CuisineDetails;

public class FilterFragment extends GociboFragment implements View.OnClickListener{

    ItemListingsActivity itemListingsActivity;
    View layoutView;

    String []foodTypes = new String[]{"Veg", "Non Veg", "All"};

    RadioGroup type;
    RadioButton vegItems, nonVegItems, allItems;
    LinearLayout cuisinesHolder;
    Button filter;

    ArrayList<View> cuisinesViews = new ArrayList<>();

    List<String> selectedCuisines = new ArrayList<String>();

    public FilterFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        //view.setFitsSystemWindows(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutView = view;
        hasLoadingView();

        itemListingsActivity = (ItemListingsActivity) getActivity();

        //-----------------------

        type = (RadioGroup) layoutView.findViewById(R.id.type);
        vegItems = (RadioButton) layoutView.findViewById(R.id.vegItems);
        nonVegItems = (RadioButton) layoutView.findViewById(R.id.nonVegItems);
        allItems = (RadioButton) layoutView.findViewById(R.id.allItems);
        cuisinesHolder = (LinearLayout) layoutView.findViewById(R.id.cuisinesHolder);

        filter = (Button) layoutView.findViewById(R.id.filter);
        filter.setOnClickListener(this);

        //-----------------------

        displayCuisinesList(null);
    }

    @Override
    public void onClick(View view) {

        if ( view == filter ){
            boolean done = false;
            int selectedFoodTypeIndex = type.indexOfChild(type.findViewById(type.getCheckedRadioButtonId()));

            if (selectedFoodTypeIndex != -1) {
                done = true;
            } else {
                selectedFoodTypeIndex = 0;
            }

            if (selectedCuisines.size() != 0) {
                done = true;
            }

            if (done == true) {
                if (itemListingsActivity != null) {
                    String[] selCuisines = new String[selectedCuisines.size()];
                    for (int i = 0; i < selectedCuisines.size(); i++) {
                        selCuisines[i] = selectedCuisines.get(i);
                    }
                    itemListingsActivity.onFilterPressed(foodTypes[selectedFoodTypeIndex], selCuisines);
                }
            } else {
                showSnackbarMessage("Please select Food type (or) atleast 1 Cuisine to Filter", null, null);
            }
        }
    }

    public void displayCuisinesList(final List<CuisineDetails> availableCuisines){

        switchToContentPage();

        if ( availableCuisines != null && availableCuisines.size() > 0 ){

            selectedCuisines.clear();
            cuisinesHolder.removeAllViews();
            layoutView.findViewById(R.id.no_cuisines_info).setVisibility(View.GONE);
            allItems.setChecked(true);

            for(int i=0;i<availableCuisines.size();i++){

                final CuisineDetails cuisineDetails = availableCuisines.get(i);

                View cuisineView = inflater.inflate(R.layout.view_check_item, null, false);
                cuisinesViews.add(cuisineView);
                cuisinesHolder.addView(cuisineView);

                final CheckBox checkbox = (CheckBox) cuisineView.findViewById(R.id.checkbox);
                checkbox.setText(cuisineDetails.getValue()+" ("+cuisineDetails.getCount()+")");

                cuisineView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if ( checkbox.isChecked() == false ) {
                            checkbox.setChecked(true);
                        } else {
                            checkbox.setChecked(false);
                        }
                    }
                });

                checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                        if ( isChecked == true ) {
                            if (selectedCuisines.contains(cuisineDetails.getValue()) == false) {
                                selectedCuisines.add(cuisineDetails.getValue());
                            }
                        }else{
                            selectedCuisines.remove(cuisineDetails.getValue());
                        }
                    }
                });
            }

        }else{
            layoutView.findViewById(R.id.no_cuisines_info).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
