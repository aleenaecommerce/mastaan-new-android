package in.gocibo.foodie.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import in.gocibo.foodie.R;


public class SplashActivity extends Activity {//implements View.OnClickListener {

    Context context;

    /*SliderLayout imageSlider;
    FloatingActionButton skipSlider;
    Animation enterAnim, exitAnim;*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);     //enable transitions
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_splash_screen);

        context = this;

        //.......................................................

        /*enterAnim = AnimationUtils.loadAnimation(this, R.anim.fab_enter_anim);
        exitAnim = AnimationUtils.loadAnimation(this, R.anim.fab_exit_anim);

        skipSlider = (FloatingActionButton) findViewById(R.id.skipSlider);
        skipSlider.startAnimation(enterAnim);
        skipSlider.setOnClickListener(this);

        imageSlider = (SliderLayout) findViewById(R.id.imageSlider);

        final int[] images = new int[]{R.drawable.gocibo_main_background_potrait, R.drawable.gocibo_main_background_potrait, R.drawable.gocibo_main_background_potrait};
        final String[] imagesTitles = new String[]{"Direct from homes", "Secrets passed over generations", "Let your taste buds savor the best"};
        final String[] imagesSubTitles = new String[]{"Your dish is being cooked as you browse", "Awesomeness and quality in every bite", "It is a B-E-A-Utiful life"};

        imageSlider.stopAutoCycle();

        for (int i = 0; i < images.length; i++) {
            final int position = i;
            try {
                imageSlider.addSlider(new BaseSliderView(getApplicationContext()) {
                    @Override
                    public View getView() {
                        LayoutInflater cInflater = LayoutInflater.from(getApplicationContext());
                        View itemView = cInflater.inflate(R.layout.view_image_slider_item, null);

                        final ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
                        final TextView title = (TextView) itemView.findViewById(R.id.title);
                        final TextView subtitle = (TextView) itemView.findViewById(R.id.subtitle);

                        imageView.setImageResource(images[position]);
                        title.setText(imagesTitles[position]);
                        subtitle.setText(imagesSubTitles[position]);

                        return itemView;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                imageSlider.setDuration(4000);
                imageSlider.startAutoCycle();
            }
        }.start();*/

    }
    /*
    @Override
    public void onClick(View view) {
        if (view == skipSlider) {
            //skipSlider.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_anim));
            imageSlider.stopAutoCycle();
            Intent tcActivity = new Intent(context, TermsAndConditionsActivity.class);
            startActivity(tcActivity);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imageSlider.stopAutoCycle();
    }*/

}