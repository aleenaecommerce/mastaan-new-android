package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.rey.material.widget.FloatingActionButton;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.CommonAPI;
import in.gocibo.foodie.backend.model.RequestAllergiesUpdate;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.AllergyDetails;

public class AllergiesEditActivity extends GociboToolBarActivity {

    LinearLayout allergies_holder;

    FloatingActionButton doneEdit;

    List<AllergyDetails> selectedAllergies = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allergies_edit);
        hasLoadingView();

        ArrayList<String> buyer_allergies_jsons = getIntent().getStringArrayListExtra("buyer_allergies_jsons");
        if ( buyer_allergies_jsons != null ){
            for (int i=0;i<buyer_allergies_jsons.size();i++){
                selectedAllergies.add(new Gson().fromJson(buyer_allergies_jsons.get(i), AllergyDetails.class));
            }
        }

        allergies_holder = (LinearLayout) findViewById(R.id.allergies_holder);

        doneEdit = (FloatingActionButton) findViewById(R.id.doneEdit);
        doneEdit.startAnimation(fabEnterAnim);
        doneEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(context, "Done Edit " + selectedAllergies.size(), Toast.LENGTH_LONG).show();
                updateAllergies();
            }
        });

        getAvailableAllergies();     //  GetAllergies
    }

    public void updateAllergies(){

        showLoadingDialog("Updating Allergies");
        getBackendAPIs().getProfileAPI().updateBuyerAllergies(new RequestAllergiesUpdate(selectedAllergies), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {

                closeLoadingDialog();

                if (status == true) {

                    for (int i = 0; i < selectedAllergies.size(); i++) {
                        for (int j = 0; j < selectedAllergies.size(); j++) {
                            if (selectedAllergies.get(i).getName().trim().compareToIgnoreCase(selectedAllergies.get(j).getName().trim()) < 0) {
                                AllergyDetails temp = selectedAllergies.get(i);
                                selectedAllergies.set(i, selectedAllergies.get(j));
                                selectedAllergies.set(j, temp);
                            }
                        }
                    }

                    ArrayList<String> buyer_allergies_jsons = new ArrayList<>();
                    for (int i = 0; i < selectedAllergies.size(); i++) {
                        buyer_allergies_jsons.add(new Gson().toJson(selectedAllergies.get(i)));
                    }

                    Intent modifiedData = new Intent();
                    modifiedData.putStringArrayListExtra("buyer_allergies_jsons", buyer_allergies_jsons);
                    setResult(Constants.PROFILE_ACTIVITY_CODE, modifiedData);
                    finish();//finishing activity

                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage(doneEdit, "Unable to update Allergies", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateAllergies();
                        }
                    });
                }
            }
        });
    }

    public void showAllergiesWithSelected(final List<AllergyDetails> availableAllergies){

        for(int i=0;i<availableAllergies.size();i++){
            View allergyView = inflater.inflate(R.layout.view_check_item, null, false);
            allergies_holder.addView(allergyView);

            final CheckBox checkbox = (CheckBox) allergyView.findViewById(R.id.checkbox);
            checkbox.setText(availableAllergies.get(i).getName());

            if ( isSelectedAllergy(availableAllergies.get(i)) ){
                checkbox.setChecked(true);
            }

            final int position = i;
            //final boolean isChecked = false;
            allergyView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkbox.isChecked() == false) {
                        selectedAllergies.add(availableAllergies.get(position));
                        checkbox.setChecked(true);
                    } else {
                        deleteAllergy(availableAllergies.get(position));
                        checkbox.setChecked(false);
                    }
                }
            });

        }

        switchToContentPage();
    }

    public void deleteAllergy(AllergyDetails allergyDetails){

        for (int i=0;i<selectedAllergies.size();i++){
            if ( selectedAllergies.get(i).getID().equals(allergyDetails.getID()) ){
                selectedAllergies.remove(i);
                break;
            }
        }
    }

    public boolean isSelectedAllergy(AllergyDetails allergyDetails){
        boolean status = false;
        for(int i=0;i<selectedAllergies.size();i++){
            if ( selectedAllergies.get(i).getID().equals(allergyDetails.getID()) ){
                status = true;
                break;
            }
        }
        return status;
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getAvailableAllergies();
    }

    public void getAvailableAllergies() {

        showLoadingIndicator("Loading allergies, wait..");
        getBackendAPIs().getCommonAPI().getAvailableAllergies(new CommonAPI.AllergiesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<AllergyDetails> availableAllergies) {
                if (status == true) {
                    showAllergiesWithSelected(availableAllergies);
                } else {
                    showReloadIndicator(statusCode, "Unable to load allergies, try again!");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
    }
}
