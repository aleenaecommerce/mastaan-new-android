package in.gocibo.foodie.activities;

import android.os.Bundle;
import android.view.MenuItem;

import in.gocibo.foodie.R;
import in.gocibo.foodie.fragments.OrderHistoryFragment;

public class OrderHistoryActivity extends GociboToolBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_order_history);

        getSupportFragmentManager().beginTransaction().replace(R.id.ordersListHolder, new OrderHistoryFragment()).commit();

    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
    }
}
