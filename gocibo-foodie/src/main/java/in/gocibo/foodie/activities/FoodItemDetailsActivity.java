package in.gocibo.foodie.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.IntentCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vCircularImageView;
import com.aleena.common.widgets.vFlowLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.HorizontaliFoodItemsAdapter;
import in.gocibo.foodie.backend.CartAPI;
import in.gocibo.foodie.backend.FoodItemsAPI;
import in.gocibo.foodie.backend.ProfileAPI;
import in.gocibo.foodie.backend.model.RequestSearchUploads;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.interfaces.RegistrationCallback;
import in.gocibo.foodie.models.AllergyDetails;
import in.gocibo.foodie.models.CategoryDetails;
import in.gocibo.foodie.models.CuisineDetails;
import in.gocibo.foodie.models.FoodItemDetails;
import in.gocibo.foodie.models.IngradientDetails;

public class FoodItemDetailsActivity extends GociboToolBarActivity implements View.OnClickListener{

    Menu menu;
    CollapsingToolbarLayout collapsingToolbar;

    ImageView item_image;
    vCircularImageView chef_image;
    Button chefSelector;
    FrameLayout item_stats_pan;
    TextView item_quantity, item_price, chef_name, item_description, item_cuisine;
    MenuItem item_type_indicator;
    vFlowLayout ingradientsHolder;

    Button addToCart;

    TextView chefItemsTitle;
    LinearLayout other_dishes_details;
    LinearLayout otherChefItemsHolder;
    HorizontaliFoodItemsAdapter.Callback otherChefItemsCallback;
    HorizontaliFoodItemsAdapter otherChefItemsAdapter;

    FoodItemDetails foodItemDetails;
    //CategoryDetails categoryDetails;

    String dishID;
    String slotType;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_item_details);
        hasLoadingView();
        hasBackButton();        //  To Show while loading

        dishID = getIntent().getStringExtra("dishID");
        //categoryDetails = new Gson().fromJson(getIntent().getStringExtra("categoryDetails"), CategoryDetails.class)

        //---------------

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(getString(R.string.title_activity_food_item_details).toUpperCase());

        addToCart = (Button) findViewById(R.id.addToCart);
        addToCart.setOnClickListener(this);

        item_image = (ImageView) findViewById(R.id.item_image);
        chef_image = (vCircularImageView) findViewById(R.id.chef_image);
        chefSelector = (Button) findViewById(R.id.chefSelector);
        chefSelector.setOnClickListener(this);

        item_stats_pan = (FrameLayout) findViewById(R.id.item_stats_pan);
        item_quantity = (TextView) findViewById(R.id.item_quantity);
        item_price = (TextView) findViewById(R.id.item_price);
        chef_name = (TextView) findViewById(R.id.chef_name);
        item_description = (TextView) findViewById(R.id.item_description);
        item_cuisine = (TextView) findViewById(R.id.item_cuisine);

        ingradientsHolder = (vFlowLayout) findViewById(R.id.ingradientsHolder);

        chefItemsTitle = (TextView) findViewById(R.id.chefItemsTitle);

        otherChefItemsCallback = new HorizontaliFoodItemsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                Intent foddItemDetailsAct = new Intent(context, FoodItemDetailsActivity.class);
                foddItemDetailsAct.putExtra("dishID", otherChefItemsAdapter.getFoodItem(position).getID());
                foddItemDetailsAct.putExtra("categoryDetails", new Gson().toJson(getCategoryDetails()));
                startActivityForResult(foddItemDetailsAct, Constants.HOME_ACTIVITY_CODE);
            }
        };

        other_dishes_details = (LinearLayout) findViewById(R.id.other_dishes_details);
        otherChefItemsHolder = (LinearLayout) findViewById(R.id.otherChefItemsHolder);
        otherChefItemsAdapter = new HorizontaliFoodItemsAdapter(context, R.layout.view_food_item_mini_with_price, new ArrayList<FoodItemDetails>(), otherChefItemsCallback);

        //------------------------------------

        getFoodItemDetails();      // Load Food Item Details..

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getFoodItemDetails();
    }

    public void getFoodItemDetails(){

        showLoadingIndicator("Loading food item details, wait..");
        getBackendAPIs().getFoodItemsAPI().getFoodItemDetails(dishID, new FoodItemsAPI.FoodItemDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, FoodItemDetails foodItemDetails) {
                if (status == true) {
                    displayFoodItemDetails(foodItemDetails);
                } else {
                    showReloadIndicator(statusCode, "Unable to load food item details, try again!");
                }
            }
        });
    }

    // Display Food Item Details...
    public void displayFoodItemDetails(FoodItemDetails foodItemDetails){

        try {
            this.foodItemDetails = foodItemDetails;
            switchToContentPage();

            collapsingToolbar.setTitle(foodItemDetails.getName().toUpperCase());

            //ViewCompat.animate(item_image).setDuration(1000).scaleYBy(1).scaleXBy(1).start();
            Picasso.get()
                    .load(foodItemDetails.getImageURL())
                    .placeholder(R.drawable.image_default)
                    .error(R.drawable.image_default)
                    .fit()
                    .centerCrop()
                    .tag(context)
                    .into(item_image);

            item_price.setText(context.getResources().getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(foodItemDetails.getProfitPrice()));
            if (foodItemDetails.getQuantityType().equalsIgnoreCase("n")) {
                item_price.setText(context.getResources().getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(foodItemDetails.getProfitPrice()));
            } else if (foodItemDetails.getQuantityType().equalsIgnoreCase("w")) {
                item_price.setText(context.getResources().getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(foodItemDetails.getProfitPrice()) + "/kg");
            } else if (foodItemDetails.getQuantityType().equalsIgnoreCase("v")) {
                item_price.setText(context.getResources().getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(foodItemDetails.getProfitPrice()) + "/L");
            }
            chef_name.setText(CommonMethods.capitalizeStringWords(foodItemDetails.getChefFullName()));
            item_description.setText(CommonMethods.capitalizeFirstLetter(foodItemDetails.getDescription()));
            item_cuisine.setText(CommonMethods.capitalizeFirstLetter(foodItemDetails.getCuisineName()));

            if (item_type_indicator == null) {
                if (menu != null) {
                    item_type_indicator = menu.getItem(0);
                }
            }
            if (item_type_indicator != null) {
                if (foodItemDetails.getType().equalsIgnoreCase("veg")) {
                    item_type_indicator.setIcon(R.drawable.ic_veg);
                } else if (foodItemDetails.getType().equalsIgnoreCase("non-veg")) {
                    item_type_indicator.setIcon(R.drawable.ic_non_veg);
                }else if ( foodItemDetails.getType().equalsIgnoreCase("jain") ){
                    item_type_indicator.setIcon(R.drawable.ic_jain2);
                }
            }

            Picasso.get()
                    .load(foodItemDetails.getChefThumbnailImageURL())
                    .placeholder(R.drawable.ic_chef)
                    .error(R.drawable.ic_chef)
                    .fit()
                    .centerCrop()
                    .tag(context)
                    .into(chef_image);

            displayItemAvailability(foodItemDetails.getCurrentAvailability());

            //Ingradients
            List<IngradientDetails> ingradients = this.foodItemDetails.getIngradients();

            for (int i = 0; i < ingradients.size(); i++) {
                final int position = i;
                View ingradientView = inflater.inflate(R.layout.view_ingredient_item, null, false);
                ingradientsHolder.addView(ingradientView);
                TextView ingradient_name = (TextView) ingradientView.findViewById(R.id.ingradient_name);
                ingradient_name.setText(ingradients.get(position).getName());
            }

            chefItemsTitle.setText("OTHER DISHES ON SALE BY " + foodItemDetails.getChefFullName());
            getChefOtherDishesOnSale();        // Loading Chef Food Items..

        }catch (Exception e){e.printStackTrace();}

    }

    public void getChefOtherDishesOnSale(){

        RequestSearchUploads requestObject = new RequestSearchUploads(foodItemDetails.getChefID(), getCategoryDetails().getValue(), localStorageData.getDeliveryLocation().getLatLng(), null, null);

        findViewById(R.id.list_loading_indicator).setVisibility(View.VISIBLE);
        findViewById(R.id.list_reload_indicator).setVisibility(View.GONE);

        getBackendAPIs().getFoodItemsAPI().getFoodItems(requestObject, new FoodItemsAPI.FoodItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<FoodItemDetails> foodItems, List<CuisineDetails> availableCuisines) {

                findViewById(R.id.list_loading_indicator).setVisibility(View.GONE);

                if (status == true) {

                    List<FoodItemDetails> chefItems = otherChefItemsAdapter.getItemsExcluding(foodItems, foodItemDetails);
                    if (chefItems.size() > 0) {
                        otherChefItemsAdapter.addItems(chefItems);
                        for (int i = 0; i < otherChefItemsAdapter.getItemsCount(); i++) {
                            otherChefItemsHolder.addView(otherChefItemsAdapter.getView(i, null, null));
                        }
                    } else {
                        other_dishes_details.setVisibility(View.GONE);
                    }
                } else {
                    findViewById(R.id.list_reload_indicator).setVisibility(View.VISIBLE);
                    findViewById(R.id.list_reload_indicator).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getChefOtherDishesOnSale();
                        }
                    });
                }
            }
        });
    }

    public void displayItemAvailability(double availability){

        foodItemDetails.setAvailability(availability);
        broadcastReceiversMethods.updateFoodItemAvailability(foodItemDetails.getID(), foodItemDetails.getCurrentAvailability());

        item_stats_pan.setVisibility(View.VISIBLE);
        if ( availability == 0 ){
            item_quantity.setText("SOLD OUT");
            addToCart.setVisibility(View.GONE);
        }
        else if  ( DateMethods.compareDates(localStorageData.getServerTime(), DateMethods.getReadableDateFromUTC(foodItemDetails.getAvailabilityEndTime())) > 0  ){
            item_quantity.setText("NOT AVAILABLE");
            addToCart.setVisibility(View.GONE);
        }
        else {
            String totalInfo = CommonMethods.getInDecimalFormat(availability) + " servings";
            if (foodItemDetails.getQuantityType().equalsIgnoreCase("n")) {
                totalInfo = CommonMethods.getInDecimalFormat(availability) + " servings";
                if (availability == 1) {
                    totalInfo = CommonMethods.getInDecimalFormat(availability) + " serving";
                }
            } else if (foodItemDetails.getQuantityType().equalsIgnoreCase("w")) {
                totalInfo = CommonMethods.getInDecimalFormat(availability) + " kgs";
                if (availability == 1) {
                    totalInfo = CommonMethods.getInDecimalFormat(availability) + " kg";
                }
            } else if (foodItemDetails.getQuantityType().equalsIgnoreCase("v")) {
                totalInfo = CommonMethods.getInDecimalFormat(availability) + " L";
            }

            if ( DateMethods.compareDates(localStorageData.getServerTime(), DateMethods.getReadableDateFromUTC(foodItemDetails.getAvailableTime())) < 0 ) {
                item_quantity.setText(totalInfo+ " avl. from "+DateMethods.getTimeIn12HrFormat(DateMethods.getReadableDateFromUTC(foodItemDetails.getAvailableTime())).toUpperCase());
            }else {
                item_quantity.setText("Only " + totalInfo+" left");
            }
        }
    }

    @Override
    public void onClick(View view) {

        if ( view == addToCart ){
            addItemToCart();
        }
        else if ( view == chefSelector ){
            Intent chefAct = new Intent(context, ChefDetailsActivity.class);
            chefAct.putExtra("chefID", foodItemDetails.getChefID());
            startActivity(chefAct);
        }
    }

    //-----------  ADD TO CART METHODS -------------------

    public void addItemToCart(){

        if ( localStorageData.getSessionFlag() ){

            if ( localStorageData.getBuyerAllergies() != null ){
                new Add_or_Edit_Cart_Item_Flow(activity).addFlow(getCategoryDetails(), foodItemDetails, new Add_or_Edit_Cart_Item_Flow.Add_or_Edit_Cart_Item_Flow_Callback() {
                    @Override
                    public void onComplete(double quantity, String deliverySlot) {
                        addToCart(quantity, deliverySlot);
                    }
                });
            }else{
                showLoadingDialog("Checking your allergies, wait..");
                getBackendAPIs().getProfileAPI().getBuyerAllergies(new ProfileAPI.AllergiesCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, List<AllergyDetails> buyerAllergies) {
                        closeLoadingDialog();
                        checkSessionValidity(statusCode);
                        localStorageData.setBuyerAllergies(buyerAllergies);

                        new Add_or_Edit_Cart_Item_Flow(activity).addFlow(getCategoryDetails(), foodItemDetails, new Add_or_Edit_Cart_Item_Flow.Add_or_Edit_Cart_Item_Flow_Callback() {
                            @Override
                            public void onComplete(double quantity, String deliverySlot) {
                                addToCart(quantity, deliverySlot);
                            }
                        });
                    }
                });
            }
        }
        else {      // IF NO SESSION
            goToRegistration(new RegistrationCallback() {
                @Override
                public void onComplete(boolean status, String token, in.gocibo.foodie.models.BuyerDetails buyerDetails) {
                    if (status) {
                        addItemToCart();       // Adding Item To Cart..
                    }
                }
            });
        }
    }

    //REQUEST

    private void addToCart(final double quantity, final String slot){

        showLoadingDialog("Adding item to your food basket, wait...");
        getBackendAPIs().getCartAPI().addToCart(foodItemDetails.getID(), quantity, slot, new CartAPI.AddCartItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, boolean isAlreayPresent, double latestAvailability) {

                closeLoadingDialog();

                if (status == true) {

                    if (message.equalsIgnoreCase("OK")) {

                        displayItemAvailability(latestAvailability);

                        if (isAlreayPresent == false || localStorageData.getCartCount() == 0) {
                            localStorageData.setCartCount(localStorageData.getCartCount() + 1);
                        }

                        if (localStorageData.getCartCount() == 1) {
                            broadcastReceiversMethods.startTimerCart();
                        }

                        Intent addedItemData = new Intent();
                        addedItemData.putExtra("item_name", foodItemDetails.getName());
                        addedItemData.putExtra("item_quantity_type", foodItemDetails.getQuantityType());
                        addedItemData.putExtra("item_quantity", quantity);
                        addedItemData.putExtra("item_slot", slot);
                        setResult(Constants.HOME_ACTIVITY_CODE, addedItemData);
                        finish();
                    }
                    else if (message.equalsIgnoreCase("slot_closed")) {
                        showToastMessage("Sorry Current Slot is Ended");
                        Intent closedAct = new Intent(context, ClosedActivity.class);
                        ComponentName componentName = closedAct.getComponent();
                      //  Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
                      //  startActivity(mainIntent);
                    }
                    else if (message.equalsIgnoreCase("cart_add_item_availability_exceeded")) {
                        displayItemAvailability(latestAvailability);
                        showSnackbarMessage("Requested item quantity exceeds the availability", null, null);
                    }
                    else if ( message.equalsIgnoreCase("slot_type_should_be_same")){
                        showNotificationDialog("Can't add to your food basket!", "You can't combine meal items with sweets/snacks in a single order.");
                    }

                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to Add Item to Cart!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            addToCart(quantity, slot);
                        }
                    });
                }
            }
        });
    }

    //---------------------------------

    public CategoryDetails getCategoryDetails() {
        return new Gson().fromJson(getIntent().getStringExtra("categoryDetails"), CategoryDetails.class);
    }

    //=================================

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_food_item_details, menu);
        this.menu = menu;
        item_type_indicator = menu.getItem(0);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() != R.id.item_type_indicator ) {
            onBackPressed();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if ( requestCode == Constants.HOME_ACTIVITY_CODE && resultCode == Constants.HOME_ACTIVITY_CODE ) {
                setResult(Constants.HOME_ACTIVITY_CODE, data);
                finish();
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

}