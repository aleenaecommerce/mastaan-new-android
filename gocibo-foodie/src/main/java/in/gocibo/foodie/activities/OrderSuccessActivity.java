package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.widgets.vCollageImageView;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.OrderItemsAdapter;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.OrderDetails;
import in.gocibo.foodie.models.OrderItemDetails;

public class OrderSuccessActivity extends GociboToolBarActivity implements View.OnClickListener{

    vCollageImageView collage_images;
    TextView order_id;
    TextView order_cost;
    TextView name;
    TextView payment_mode;
    TextView order_coupon;
    TextView delivery_address;
    Button trackOrder;

    vRecyclerView orderItemsHolder;
    OrderItemsAdapter orderItemsAdapter;

    OrderDetails orderDetails;
    boolean isImmediate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_order_success);

        String order_details_json = getIntent().getStringExtra("order_details_json");
        orderDetails = new Gson().fromJson(order_details_json, OrderDetails.class);
        isImmediate = getIntent().getBooleanExtra("isImmediate", false);

        int previousPendingOrdersCount = localStorageData.getNoOfPendingOrders();
        addToPendingOrders(orderDetails);

        if (  previousPendingOrdersCount > 0 ){
            Intent firebaseReceiver = new Intent(Constants.FIREBASE_RECEIVER)
                    .putExtra("type", "add_new_pending_order")
                    .putExtra("order_details_json", order_details_json);
            context.sendBroadcast(firebaseReceiver);
        }else{
            context.startService(firebaseService);
        }

        Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                .putExtra("type", "display_pending_orders_info");
        context.sendBroadcast(homeReceiverIntent);

        //.....................................................

        collage_images = (vCollageImageView) findViewById(R.id.collage_images);
        order_id = (TextView) findViewById(R.id.order_id);
        order_cost = (TextView) findViewById(R.id.order_cost);
        name = (TextView) findViewById(R.id.name);
        payment_mode = (TextView) findViewById(R.id.payment_mode);
        order_coupon = (TextView) findViewById(R.id.order_coupon);
        delivery_address = (TextView) findViewById(R.id.delivery_address);
        trackOrder = (Button) findViewById(R.id.trackOrder);
        trackOrder.setOnClickListener(this);

        orderItemsHolder = (vRecyclerView) findViewById(R.id.orderItemsHolder);

        //.....................................................

        ArrayList<String> imageURLs = new ArrayList<>();
        if ( orderDetails.getOrderItems() != null ) {
            for (int i=0;i<orderDetails.getOrderItems().size();i++){
                imageURLs.add(orderDetails.getOrderItems().get(i).getDishImage());
            }
            collage_images.addImageURLs(imageURLs);
        }

        order_id.setText(orderDetails.getOrderID());
        name.setText(orderDetails.getUserName());
        order_cost.setText(RS+" "+ CommonMethods.getInDecimalFormat(orderDetails.getTotalAmount()));
        payment_mode.setText(orderDetails.getPaymentType());

        delivery_address.setText(orderDetails.getDeliveryAddress());

        //....................................

        orderItemsHolder.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        orderItemsHolder.setLayoutManager(manager);
        orderItemsHolder.setItemAnimator(new DefaultItemAnimator());

        orderItemsAdapter = new OrderItemsAdapter(context, new ArrayList<OrderItemDetails>(), null);
        orderItemsHolder.setAdapter(orderItemsAdapter);

        orderItemsAdapter.addItems(orderDetails.getOrderItems());      // Display Order Items.

        //-------

        storeDeliveryAddressLocally();
    }

    public void storeDeliveryAddressLocally(){

        List<AddressBookItemDetails> addressBookList = localStorageData.getBuyerAddressBookList();

        // IF DELIVERY ADDRESS IS ALREADY A FAVOURITE OR HISTORY ITEM THEN UPDATING IT..
        if ( orderDetails.getDeliveryAddressID() != null && orderDetails.getDeliveryAddressID().length() > 0 && orderDetails.getDeliveryAddressID().equalsIgnoreCase("DUMMY_ID") == false ){
            for (int i=0;i<addressBookList.size();i++){
                AddressBookItemDetails addressBookItemDetails = addressBookList.get(i);
                // COMPARING WITH ADDRESS ID IF ALREADY PRESENT
                if ( addressBookItemDetails.getID().equals(orderDetails.getDeliveryAddressID()) ){
                    addressBookItemDetails.setPremise(orderDetails.getDeliveryAddressPremise());
                    addressBookItemDetails.setSublocalityLevel2(orderDetails.getDeliveryAddressSubLocalityLevel2());
                    addressBookItemDetails.setLandmark(orderDetails.getDeliveryAddressLandmark());

                    localStorageData.setDeliveryLocation(addressBookItemDetails);
                    addressBookList.set(i, addressBookItemDetails);
                    break;
                }
            }
        }
        // IF DELIVERY ADDRESS IS NEW ONE THEN STORING IT
        else{
            boolean isAlreadyPresent = false;
            for (int i=0;i<addressBookList.size();i++){
                AddressBookItemDetails addressBookItemDetails = addressBookList.get(i);
                // COMPARING WITH LATLNG IF ALREADY PRESENT
                if (addressBookItemDetails.getLatLng().equals(orderDetails.getDeliveryAddressLatLng()) ){
                    addressBookItemDetails.setPremise(orderDetails.getDeliveryAddressPremise());
                    addressBookItemDetails.setSublocalityLevel2(orderDetails.getDeliveryAddressSubLocalityLevel2());
                    addressBookItemDetails.setLandmark(orderDetails.getDeliveryAddressLandmark());

                    isAlreadyPresent = true;
                    localStorageData.setDeliveryLocation(addressBookItemDetails);
                    addressBookList.set(i, addressBookItemDetails);
                    break;
                }
            }

            if ( isAlreadyPresent == false ){
                AddressBookItemDetails addressBookItemDetails = new AddressBookItemDetails(orderDetails.getDeliveryAddressLatLng(), orderDetails.getDeliveryAddressPremise(), orderDetails.getDeliveryAddressSubLocalityLevel2(), orderDetails.getDeliveryAddressSubLocalityLevel1(), orderDetails.getDeliveryAddressRoadNo(),  orderDetails.getDeliveryAddressLandmark(), orderDetails.getDeliveryAddressLocality(), orderDetails.getDeliveryAddressState(), orderDetails.getDeliveryAddressCountry(), orderDetails.getDeliveryAddressPinCode());
                addressBookItemDetails.setID(orderDetails.getDeliveryAddressID());

                localStorageData.setDeliveryLocation(addressBookItemDetails);
                addressBookList.add(addressBookItemDetails);
            }
        }

        localStorageData.storeBuyerAddressBookList(addressBookList);    // UPDATING IN LOCAL DATABASE

        Intent sidebarReceiver = new Intent(Constants.SIDEBAR_RECEIVER);
        sidebarReceiver.putExtra("type", "update_delivery_address_name");
        context.sendBroadcast(sidebarReceiver);

    }


    @Override
    public void onClick(View view) {

        if ( view == trackOrder ){
            Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
            orderDetailsAct.putExtra("orderID", orderDetails.getID());
            orderDetailsAct.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);;
            orderDetailsAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(orderDetailsAct);
        }

    }

    //===========================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent homeAct = new Intent(context, HomeActivity.class);
        homeAct.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);;
        homeAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeAct);

    }
}
