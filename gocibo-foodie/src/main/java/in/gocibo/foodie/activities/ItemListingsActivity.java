package in.gocibo.foodie.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import androidx.drawerlayout.widget.DrawerLayout;

import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.google.gson.Gson;
import com.rey.material.widget.ProgressView;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.CartAPI;
import in.gocibo.foodie.backend.FoodItemsAPI;
import in.gocibo.foodie.backend.ProfileAPI;
import in.gocibo.foodie.backend.model.RequestSearchUploads;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.fragments.FilterFragment;
import in.gocibo.foodie.fragments.ItemListingsFragment;
import in.gocibo.foodie.fragments.ItemListingsSearchFragment;
import in.gocibo.foodie.interfaces.RegistrationCallback;
import in.gocibo.foodie.models.AllergyDetails;
import in.gocibo.foodie.models.BuyerDetails;
import in.gocibo.foodie.models.CategoryDetails;
import in.gocibo.foodie.models.CuisineDetails;
import in.gocibo.foodie.models.FoodItemDetails;

public class ItemListingsActivity extends GociboToolBarActivity implements View.OnClickListener{

    //Menu menu;
    MenuItem filterMenuITem;
    MenuItem searchViewMenuITem;
    SearchView searchView;

    DrawerLayout drawerLayout;

    String currentView = "FoodListings";
    ViewSwitcher viewSwitcher;

    FilterFragment filterFragment;

    ItemListingsFragment itemListingsFragment;
    ItemListingsSearchFragment itemListingsSearchFragment;

    FrameLayout cartInfo;
    ImageButton cartFAB;
    TextView cart_count;
    ProgressView cart_countdown_indicator;

    //CategoryDetails categoryDetails;
    FoodItemDetails selectedFoodItemDetails;

    //public ResponseSearchUploads responseSearchUploads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_listings);

        filterFragment = new FilterFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();

        itemListingsFragment = new ItemListingsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.itemListingsFragment, itemListingsFragment).commit();

        itemListingsSearchFragment = new ItemListingsSearchFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.itemListingsSearchFragment, itemListingsSearchFragment).commit();

        // categoryDetails = new Gson().fromJson(getIntent().getStringExtra("categoryDetails"), CategoryDetails.class);
        actionBar.setTitle(getCategoryDetails().getName());

        registerActivityReceiver(Constants.HOME_RECEIVER);

        //-------------------

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        disableRightNavigationDrawer();

        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);

        cartInfo = (FrameLayout) findViewById(R.id.cartInfo);
        cart_count = (TextView) findViewById(R.id.cart_count);
        cart_countdown_indicator = (ProgressView) findViewById(R.id.cart_countdown_indicator);
        cartFAB = (ImageButton) findViewById(R.id.cartFAB);
        cartFAB.setOnClickListener(this);

        //---------------------

        displayUserSessionInfo();

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();
        //itemListingsFragment.displayFoodItems(responseSearchUploads.getFoodItems(), responseSearchUploads.getCuisines());
        itemListingsFragment.getFoodItems();
    }

    @Override
    public void onClick(View view) {
        if ( view == cartFAB){
            Intent cartAct = new Intent(context, CartActivity.class);
            startActivity(cartAct);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        long differenceTime = DateMethods.getDifferenceToCurrentTime(localStorageData.getHomePageLastLoadedTime());
        if ( differenceTime > 600000 ) {     // If greater than 10 mins (600,000 millisec) InActiveTime
            Log.d(Constants.LOG_TAG, "Reloading App as InActive time > 10min");
            Intent launchActivity = new Intent(context, LaunchActivity.class);
            startActivity(launchActivity);
            finish();
        }
    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);

        try{
            if ( receivedData.getStringExtra("type").equalsIgnoreCase("display_user_session_info") ){
                displayUserSessionInfo();
            }
            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("update_food_item_availability")) {
                updateFoodItemAvailability(receivedData.getStringExtra("dishID"), receivedData.getDoubleExtra("latestAvailability", -1));
            }

            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("start_timed_cart") ){
                startTimedCart();
            }

            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("update_cart_count") ){
                checkCartStatus("");
            }
            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("update_timer_cart_countdown_indicator") ){
                if ( cart_countdown_indicator.getVisibility() == View.INVISIBLE || cart_countdown_indicator.getVisibility() == View.GONE ){
                    cart_countdown_indicator.setVisibility(View.VISIBLE);
                }
                cart_countdown_indicator.setProgress(receivedData.getFloatExtra("progress", cart_countdown_indicator.getProgress()));
            }
            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("stop_timed_cart") ){
                localStorageData.setCartCount(0);
                localStorageData.setCartStartTime("'");

                context.stopService(timedCartService);
                checkCartStatus("");
                cart_countdown_indicator.setVisibility(View.INVISIBLE);
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    //--------------------

    public void updateItemsAvailabilityAfterTimeOut(){

        Log.d(Constants.LOG_TAG, "Updating Items Availability after CartTimerFinish");
        getBackendAPIs().getFoodItemsAPI().getItemsAvailability(itemListingsFragment.getLoadedItemsIDs(), new FoodItemsAPI.FoodItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<FoodItemDetails> foodItems, List<CuisineDetails> availableCuisines) {
                if (status == true) {
                    itemListingsFragment.updateFoodItemsAvailabilityFromFoodItems(foodItems);
                    itemListingsSearchFragment.updateFoodItemsAvailabilityFromFoodItems(foodItems);
                }
            }
        });
    }

    public void updateFilterMenu(List<CuisineDetails> availableCuisines){
        filterFragment.displayCuisinesList(availableCuisines);
    }

    public void startTimedCart(){

        context.startService(timedCartService);
        cart_countdown_indicator.setVisibility(View.VISIBLE);
        cart_countdown_indicator.setProgress(0.0f);
    }

    public void displayUserSessionInfo(){

        if ( localStorageData.getCartCount() > 0 ){
            if ( localStorageData.getCartRemainingTime() > 0 ) {
                if (cart_countdown_indicator.getVisibility() == View.INVISIBLE || cart_countdown_indicator.getVisibility() == View.GONE) {
                    startTimedCart();
                }
            }else{
                localStorageData.setCartCount(0);
                localStorageData.setCartRemainingTime(0);
            }
        }else{
            context.stopService(timedCartService);
        }
        checkCartStatus("");
    }

    //-----------  ADD TO CART METHODS -------------------

    public void addItemToCart(final FoodItemDetails selectedItemDetails){

        this.selectedFoodItemDetails = selectedItemDetails;

        if ( localStorageData.getSessionFlag() ){

            if ( localStorageData.getBuyerAllergies() != null ){
                new Add_or_Edit_Cart_Item_Flow(activity).addFlow(getCategoryDetails(), selectedFoodItemDetails, new Add_or_Edit_Cart_Item_Flow.Add_or_Edit_Cart_Item_Flow_Callback() {
                    @Override
                    public void onComplete(double quantity, String deliverySlot) {
                        addToCart(quantity, deliverySlot);
                    }
                });
            }else{
                showLoadingDialog("Checking your allergies, wait..");
                getBackendAPIs().getProfileAPI().getBuyerAllergies(new ProfileAPI.AllergiesCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, List<AllergyDetails> buyerAllergies) {
                        closeLoadingDialog();
                        checkSessionValidity(statusCode);
                        localStorageData.setBuyerAllergies(buyerAllergies);

                        new Add_or_Edit_Cart_Item_Flow(activity).addFlow(getCategoryDetails(), selectedFoodItemDetails, new Add_or_Edit_Cart_Item_Flow.Add_or_Edit_Cart_Item_Flow_Callback() {
                            @Override
                            public void onComplete(double quantity, String deliverySlot) {
                                addToCart(quantity, deliverySlot);
                            }
                        });
                    }
                });
            }
        }
        else {      // IF NO SESSION
            goToRegistration(new RegistrationCallback() {
                @Override
                public void onComplete(boolean status, String token, BuyerDetails buyerDetails) {
                    if (status) {
                        addItemToCart(selectedItemDetails);
                    }
                }
            });
        }
    }

    //REQUEST

    private void addToCart(final double quantity, final String slot){

        showLoadingDialog("Adding item to your food basket, wait...");
        getBackendAPIs().getCartAPI().addToCart(selectedFoodItemDetails.getID(), quantity, slot, new CartAPI.AddCartItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, boolean isAlreayPresent, double latestAvailability) {

                closeLoadingDialog();

                if (status == true) {

                    if (message.equalsIgnoreCase("OK")) {

                        selectedFoodItemDetails.setAvailability((selectedFoodItemDetails.getCurrentAvailability() - quantity));

                        if (isAlreayPresent == false || localStorageData.getCartCount() == 0) {
                            localStorageData.setCartCount(localStorageData.getCartCount() + 1);
                        }

                        if (localStorageData.getCartCount() == 1) {
                            localStorageData.setCartStartTime(DateMethods.getCurrentDateAndTime());
                            Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                                    .putExtra("type", "start_timed_cart");
                            context.sendBroadcast(homeReceiverIntent);
                        }
                        updateFoodItemAvailability(selectedFoodItemDetails.getID(), latestAvailability);

                        showItemAddedInfo(selectedFoodItemDetails.getName(), selectedFoodItemDetails.getQuantityType(), quantity, slot);
                    } else if (message.equalsIgnoreCase("slot_closed")) {
                        onSlotEnded();
                    } else if (message.equalsIgnoreCase("cart_add_item_availability_exceeded")) {
                        showSnackbarMessage(cartInfo, "Requested item quantity exceeds the availability", null, null);
                        updateFoodItemAvailability(selectedFoodItemDetails.getID(), latestAvailability);
                    } else if (message.equalsIgnoreCase("slot_type_should_be_same")) {
                        showNotificationDialog("Can't add to your food basket!", "You can't combine meal items with sweets/snacks in a single order.");
                    }

                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage(cartInfo, "Unable to Add Item to Cart!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            addToCart(quantity, slot);
                        }
                    });
                }
            }
        });
    }

    //=================================

    public CategoryDetails getCategoryDetails() {
        return new Gson().fromJson(getIntent().getStringExtra("categoryDetails"), CategoryDetails.class);
    }

    public void updateFoodItemAvailability(String dishID, double latestAvailability){
        itemListingsFragment.updateFoodItemAvailability(dishID, latestAvailability);
        itemListingsSearchFragment.updateFoodItemAvailability(dishID, latestAvailability);
    }

    public void onSlotEnded(){
        showToastMessage("Sorry Current Slot is Ended");
        Intent closedAct = new Intent(context, ClosedActivity.class);
        startActivity(closedAct);
        finish();
    }

    public void showItemAddedInfo(String itemName, final String quantityType, double itemQuantity, String slot){

        if ( slot != null && slot.equalsIgnoreCase("immediate") ){
            slot = ".";
        }else{
            slot = " for "+slot + " slot.";
        }

        if ( localStorageData.isFirstCartItemAdded() == false ){
            localStorageData.setFirstCartItemFlag(true);
            String cartFirstItemMessage = "You've added your first item to your food basket. Since the items are limited, and the demand high, you'll have <b>"+ DateMethods.getTimeFromMilliSeconds(localStorageData.getCartValidTime())+"</b> to place your order, after which we'll empty your basket.";
            showNotificationDialog("Congratulations!", Html.fromHtml(cartFirstItemMessage));
        }
        String addedMessage = "";
        if ( quantityType.equalsIgnoreCase("n")) {
            addedMessage = "Added "+ CommonMethods.getInDecimalFormat(itemQuantity) + " servings of " + itemName + slot;
            if ( itemQuantity == 1 ) {
                addedMessage = "Added "+CommonMethods.getInDecimalFormat(itemQuantity) + " serving of " + itemName + slot;
            }
        }
        else if ( quantityType.equalsIgnoreCase("w")) {
            addedMessage = "Added "+CommonMethods.getInDecimalFormat(itemQuantity) + " kgs of " + itemName + slot;
            if ( itemQuantity == 1 ) {
                addedMessage = "Added "+CommonMethods.getInDecimalFormat(itemQuantity) + " kg of " + itemName + slot;
            }
        }
        else if ( quantityType.equalsIgnoreCase("l")) {
            addedMessage = "Added "+CommonMethods.getInDecimalFormat(itemQuantity) + "L of " + itemName + slot;
        }
        checkCartStatus(addedMessage);
    }

    public void checkCartStatus(final String message){

        fabEnterAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (message != null && message.length() > 0) {
                    showSnackbarMessage(cartInfo, message, null, null);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        int cartCount = localStorageData.getCartCount();

        if ( cartCount > 0 ){
            cart_count.setText("" + cartCount);
            if ( cartInfo.getVisibility() == View.INVISIBLE  || cartInfo.getVisibility() == View.GONE ){
                cartInfo.setVisibility(View.VISIBLE);
                cartInfo.startAnimation(fabEnterAnim);
            }else{
                if (message != null && message.length() > 0) {
                    showSnackbarMessage(cartInfo, message, null, null);
                }
            }
        }
        else {
            localStorageData.setCartRemainingTime(0);
            if ( cartInfo.getVisibility() == View.VISIBLE ){
                cartInfo.setVisibility(View.INVISIBLE);
                cartInfo.startAnimation(fabExitAnim);
            }
        }
    }

    //==================================

    private void switchView(String view){

        if ( view.equalsIgnoreCase("FoodListings") && currentView.equalsIgnoreCase(view) == false ){
            currentView = "FoodListings";
            viewSwitcher.setDisplayedChild(0);;
        }
        else if ( view.equalsIgnoreCase("FoodSearch") && currentView.equalsIgnoreCase(view) == false ){
            currentView = "FoodSearch";
            viewSwitcher.setDisplayedChild(1);;
        }
    }

    public void switchToHome(){
        switchView("FoodListings");
    }

    public void onFilterPressed(String selectedFoodType, String []selectedCuisines) {

        drawerLayout.closeDrawer(Gravity.RIGHT);

        if ( currentView.equalsIgnoreCase("FoodSearch") ){
            itemListingsSearchFragment.searchFoodItems(new RequestSearchUploads(itemListingsSearchFragment.getSearchName(), getCategoryDetails().getValue(), localStorageData.getDeliveryLocation().getLatLng(), selectedFoodType, selectedCuisines));
        }else{
            itemListingsSearchFragment.searchFoodItems(new RequestSearchUploads(null, getCategoryDetails().getValue(), localStorageData.getDeliveryLocation().getLatLng(), selectedFoodType, selectedCuisines));
        }
        switchView("FoodSearch");
    }

    public void toggleSearchFilterIcons(int loadedItemsCount){

        int minToggleCount = 50;
        if ( getString(R.string.app_mode).equalsIgnoreCase("development") ){
            minToggleCount = 1;
        }

        if (loadedItemsCount < minToggleCount) {
            disableRightNavigationDrawer();
            searchViewMenuITem.setVisible(false);
            filterMenuITem.setVisible(false);
        } else {
            enableRightNavigationDrawer();
            searchViewMenuITem.setVisible(true);
            filterMenuITem.setVisible(true);
            //showCaseSequence.showSingeView(menu.findItem(R.id.action_search).getActionView(), "Press here to filter food items.", "GOT IT", "FILTER_BUTTON_SHOWCASE");
        }
    }

    //===================================

    public void enableRightNavigationDrawer(){
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(R.id.right_drawer));
    }

    public void disableRightNavigationDrawer(){
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
    }

    public void hideMenuItems(){
        searchViewMenuITem.setVisible(false);
        filterMenuITem.setVisible(false);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_listings, menu);
        //this.menu = menu;

        filterMenuITem = menu.findItem(R.id.action_filter);
        searchViewMenuITem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchViewMenuITem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //searchView.setIconifiedByDefault(false);

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if (currentView.equalsIgnoreCase("FoodSearch")) {
                    switchView("FoodListings");
                    toggleSearchFilterIcons(itemListingsFragment.getItemsCount());
                }
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                itemListingsSearchFragment.searchFoodItems(new RequestSearchUploads(query, getCategoryDetails().getValue(), localStorageData.getDeliveryLocation().getLatLng(), null, null));
                switchView("FoodSearch");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus == false) {
                    if (currentView.equalsIgnoreCase("FoodListings")) {
                        searchViewMenuITem.collapseActionView();
                    } else if (currentView.equalsIgnoreCase("FoodSearch")) {
                        //searchViewMenuITem.collapseActionView();
                        //switchView("FoodListings");
                    }
                }
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){

        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if ( drawerLayout.isDrawerOpen(Gravity.RIGHT) ) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }else{
            super.onBackPressed();
        }
//        if ( currentView.equalsIgnoreCase("FoodSearch") ){
//            switchView("FoodListings");
//        }
    }
}
