package in.gocibo.foodie.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.aleena.common.widgets.vWebViewProgressIndicator;

import in.gocibo.foodie.R;

public class TermsAndConditionsActivity extends GociboToolBarActivity implements View.OnClickListener{

    Button decline;
    Button accept;

    WebView webview_terms_and_conditions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        hasLoadingView();

        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);

        //-----------------

        decline = (Button) findViewById(R.id.decline);
        decline.setOnClickListener(this);
        accept = (Button) findViewById(R.id.accept);
        accept.setOnClickListener(this);

        webview_terms_and_conditions = (WebView) findViewById(R.id.webview_terms_and_conditions);

        //------------------

        webview_terms_and_conditions.getSettings().setJavaScriptEnabled(true);
        webview_terms_and_conditions.setWebChromeClient(new vWebViewProgressIndicator(new vWebViewProgressIndicator.CallBack() {
            @Override
            public void onLoading(int progress) {
                if (progress == 100) {
                    switchToContentPage();
                }
            }
        }));

        webview_terms_and_conditions.loadUrl("file:///android_asset/html/GociboTermsAndConditions.html");//"file://html/GociboTermsAndConditions.html");
        showLoadingIndicator("Loading GoCibo Terms & Conditions, wait..");

    }

    @Override
    public void onClick(View view) {

        if ( view == accept ){
            localStorageData.setTCFlag(true);
            setResult(RESULT_OK);
            finish();
        }
        else if ( view == decline ){
            onBackPressed();
        }
    }

}
