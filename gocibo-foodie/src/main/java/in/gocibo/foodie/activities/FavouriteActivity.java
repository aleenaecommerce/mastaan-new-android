package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.AddressBookAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.methods.BroadcastReceiversMethods;


public class FavouriteActivity extends GociboToolBarActivity {

    GoogleMap map;

    vTextInputLayout title;
    vTextInputLayout premise;
    vTextInputLayout sublocality;
    vTextInputLayout landmark;
    vTextInputLayout address;

    String mode;
    AddressBookItemDetails addressBookItemDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_favourite);

        mode = getIntent().getStringExtra("mode");
        if ( mode != null && mode.equalsIgnoreCase("Edit") ){
            actionBar.setTitle("Edit favourite");
        }

        String address_book_item_json = getIntent().getStringExtra("address_book_item_json");
        addressBookItemDetails = new Gson().fromJson(address_book_item_json, AddressBookItemDetails.class);

        //......................................................

        title = (vTextInputLayout) findViewById(R.id.title);
        premise = (vTextInputLayout) findViewById(R.id.premise);
        sublocality = (vTextInputLayout) findViewById(R.id.sublocality);
        landmark = (vTextInputLayout) findViewById(R.id.landmark);
        address = (vTextInputLayout) findViewById(R.id.address);

        //map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                title.getEditText().setText(addressBookItemDetails.getTitle());
                premise.setText(addressBookItemDetails.getPremise());
                sublocality.setText(addressBookItemDetails.getSublocalityLevel_2());
                landmark.setText(addressBookItemDetails.getLandmark());
                if ( addressBookItemDetails.getLandmark() == null || addressBookItemDetails.getLandmark().length() == 0 ){
                    landmark.setText(addressBookItemDetails.getRoute());
                }
                address.setText(CommonMethods.getStringFromStringArray(new String[]{addressBookItemDetails.getSublocalityLevel_1(), addressBookItemDetails.getLocality(), addressBookItemDetails.getState(), addressBookItemDetails.getCountry(), addressBookItemDetails.getPostalCode()}));

                map.moveCamera(CameraUpdateFactory.newLatLng(addressBookItemDetails.getLatLng()));
                map.animateCamera(CameraUpdateFactory.zoomTo(17));
                map.addMarker(new MarkerOptions().position(addressBookItemDetails.getLatLng()).title(addressBookItemDetails.getFullAddress()).snippet("(Lat: " + addressBookItemDetails.getLatLng().latitude + ", Lng: " + addressBookItemDetails.getLatLng().longitude + ")").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_red)));
                map.getUiSettings().setScrollGesturesEnabled(false);
                map.getUiSettings().setZoomGesturesEnabled(false);
            }
        });

    }

    public void updateFavourite(){

        showLoadingDialog("Updating your favourite, wait..");
        getBackendAPIs().getAddressBookAPI().updateAddress(addressBookItemDetails, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {

                closeLoadingDialog();

                if (status == true) {

                    List<AddressBookItemDetails> addressBookList = localStorageData.getBuyerAddressBookList();
                    for (int i=0;i<addressBookList.size();i++){
                        if ( addressBookList.get(i).getID().equals(addressBookItemDetails.getID()) ){
                            addressBookItemDetails.setType("favourite");
                            addressBookList.set(i, addressBookItemDetails);
                            break;
                        }
                    }
                    localStorageData.storeBuyerAddressBookList(addressBookList);    // UPDATING IN LOCAL DATABASE
                    new BroadcastReceiversMethods(context).updateAddressBookList();

                    Intent placeData = new Intent();
                    placeData.putExtra("mode", mode);
                    placeData.putExtra("address_book_item_json", new Gson().toJson(addressBookItemDetails));
                    setResult(Constants.FAVOURITE_ACTIVITY_CODE, placeData);
                    finish();
                    //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to Update your Favourite , try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateFavourite();
                        }
                    });
                }
            }
        });
    }

    public void addFavourite(){

        showLoadingDialog("Adding your favourite, wait..");
        getBackendAPIs().getAddressBookAPI().addAddress(addressBookItemDetails, new AddressBookAPI.AddAddressCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String addressID) {

                closeLoadingDialog();

                if ( status == true ){

                    addressBookItemDetails.setID(addressID);

                    List<AddressBookItemDetails> addressBookList = localStorageData.getBuyerAddressBookList();
                    addressBookList.add(addressBookItemDetails);
                    localStorageData.storeBuyerAddressBookList(addressBookList);    // ADDING TO LOCAL DATABASE
                    new BroadcastReceiversMethods(context).updateAddressBookList();

                    Intent placeData = new Intent();
                    placeData.putExtra("mode", mode);
                    placeData.putExtra("address_book_item_json", new Gson().toJson(addressBookItemDetails));
                    setResult(Constants.FAVOURITE_ACTIVITY_CODE, placeData);
                    finish();
                    //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation.
                }else{
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to Save your Favourite , try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            addFavourite();
                        }
                    });
                }
            }
        });
    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        if (menuItem.getItemId() == R.id.action_save) {

            inputMethodManager.hideSoftInputFromWindow(title.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(premise.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(sublocality.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(landmark.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(address.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..

            title.checkError("*Enter your Favourite name");
            final String oTitle = title.getText();
            final String oPremise = premise.getText();
            final String oSublocality = sublocality.getText();
            final String oLandmark = landmark.getText();

            if ( oTitle.length() > 0 ){//&& oLandmark.length() > 0 ) {
                addressBookItemDetails.setTitle(oTitle);
                addressBookItemDetails.setPremise(oPremise);
                addressBookItemDetails.setSublocalityLevel2(oSublocality);
                addressBookItemDetails.setLandmark(oLandmark);

                if ( mode.equalsIgnoreCase("Edit") ){
                    updateFavourite();
                }
                else{
                    addFavourite();
                }
            }
        }
        else {
            onBackPressed();
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_favourite, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
