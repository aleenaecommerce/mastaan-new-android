package in.gocibo.foodie.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.IntentCompat;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.SoftKeyBoardActionsCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vTextInputLayout;
import com.aleena.common.widgets.vWrappingLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.CartItemsAdapter;
import in.gocibo.foodie.backend.CartAPI;
import in.gocibo.foodie.backend.CouponsAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.CartItemDetails;
import in.gocibo.foodie.models.CartTotalDetails;
import in.gocibo.foodie.models.CategoryDetails;

public class CartActivity extends GociboToolBarActivity implements View.OnClickListener{

    MenuItem cartTimer;

    vRecyclerView cartItemsHolder;
    CartItemsAdapter cartItemsAdapter;
    CartItemsAdapter.CallBack cartItemsCallback;

    LinearLayout total_summary;
    TextView cart_total;
    TextView cart_subtotal;
    TextView cart_government_taxes;
    TextView cart_service_charges;

    RelativeLayout addCoupon;
    TextView coupon_code;
    TextView coupon_discount;
    Button removeCoupon;

    Button checkOut;

    CartItemDetails selectedCartItemDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_cart);
        hasLoadingView();

        registerActivityReceiver(Constants.CART_RECEIVER);

        //------------------

        total_summary = (LinearLayout) findViewById(R.id.total_summary);
        cart_total = (TextView) findViewById(R.id.total);
        cart_subtotal = (TextView) findViewById(R.id.subtotal);
        cart_government_taxes = (TextView) findViewById(R.id.government_taxes);
        cart_service_charges = (TextView) findViewById(R.id.service_charges);

        addCoupon = (RelativeLayout) findViewById(R.id.addCoupon);
        addCoupon.setOnClickListener(this);
        coupon_code = (TextView) findViewById(R.id.coupon_code);
        coupon_discount = (TextView) findViewById(R.id.coupon_discount);
        removeCoupon = (Button) findViewById(R.id.removeCoupon);
        removeCoupon.setOnClickListener(this);

        checkOut = (Button) findViewById(R.id.checkOut);
        checkOut.setOnClickListener(this);

        cartItemsHolder = (vRecyclerView) findViewById(R.id.cartItemsHolder);
        setupCartItemsHolder();

        //---------------------

        getCartItems();        // Load Cart Items.

    }

    @Override
    public void onClick(View view) {

        if ( view == addCoupon ){
            showCouponDialog();
        }

        else if ( view == removeCoupon ){
            //removeCoupon();
        }

        else if ( view==checkOut ){
            Intent orderBookingAct = new Intent(context, OrderBookingActivity.class);
            startActivity(orderBookingAct);
        }
    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);

        try {
            if (receivedData.getStringExtra("type").equalsIgnoreCase("update_cart_remail_valid_time")) {
                cartTimer.setTitle(receivedData.getStringExtra("remain_time"));
            }
            else if (receivedData.getStringExtra("type").equalsIgnoreCase("clear_cart")) {
                cartItemsAdapter.deleteAllCartItems();
                cartTimer.setTitle("");
                displayTotalInfo(0, 0, 0);
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getCartItems();
    }

    public void setupCartItemsHolder(){

        //cartItemsHolder.setHasFixedSize(true);
        cartItemsHolder.setLayoutManager(new vWrappingLinearLayoutManager(context));
        //cartItemsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        //cartItemsHolder.setItemAnimator(new DefaultItemAnimator());

        cartItemsCallback = new CartItemsAdapter.CallBack() {
            @Override
            public void onItemMenuClick(final int position, View view) {
                PopupMenu popup = new PopupMenu(getSupportActionBar().getThemedContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_address_item, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new android.widget.PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getItemId() == R.id.action_edit) {

                            selectedCartItemDetails = cartItemsAdapter.getCartItem(position);

                            new Add_or_Edit_Cart_Item_Flow(activity).editFlow(getCategoryDetailsOfDish(selectedCartItemDetails.getDishCategoryID()), selectedCartItemDetails, new Add_or_Edit_Cart_Item_Flow.Add_or_Edit_Cart_Item_Flow_Callback() {
                                @Override
                                public void onComplete(double quantity, String deliverySlot) {
                                    updateCartItme(position, quantity, deliverySlot);
                                }
                            });
                        }
                        else if (item.getItemId() == R.id.action_delete) {
                            deleteCartItem(position);
                        }

                        return true;
                    }
                });
            }
        };

        cartItemsAdapter = new CartItemsAdapter(context, new ArrayList<CartItemDetails>(), cartItemsCallback);
        cartItemsHolder.setAdapter(cartItemsAdapter);
    }

    public void showCouponDialog(){

        View couponDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_coupon, null);
        final vTextInputLayout coupon = (vTextInputLayout) couponDialogView.findViewById(R.id.coupon_code);
        final Button applyCoupon = (Button) couponDialogView.findViewById(R.id.applyCoupon);
        final ProgressBar progress_bar = (ProgressBar) couponDialogView.findViewById(R.id.progress_bar);

        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        coupon.setText(coupon_code.getText().toString());
        coupon.setSoftKeyBoardListener(new SoftKeyBoardActionsCallback() {
            @Override
            public void onDonePressed(String text) {
                applyCoupon.performClick();
            }
        });

        applyCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String couponCode = coupon.getText().toString();
                coupon.checkError("* Enter coupon");

                if (couponCode.length() > 0) {

                    if (couponCode.length() >= 5) {
                        coupon.hideError();
                        inputMethodManager.hideSoftInputFromWindow(coupon.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
                        applyCoupon.setVisibility(View.INVISIBLE);//setEnabled(false);
                        progress_bar.setVisibility(View.VISIBLE);

                        getBackendAPIs().getCouponsAPI().applyCoupon(couponCode, new CouponsAPI.AddOrRemoveCouponCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, CartTotalDetails cartTotalDetails) {
                                applyCoupon.setVisibility(View.VISIBLE);//setEnabled(true);
                                progress_bar.setVisibility(View.GONE);

                                if (status) {
                                    closeCustomDialog();

                                    //displayTotalInfo(cartTotalDetails);

                                    if (message != null && message.length() > 0) {
                                        showNotificationDialog("Success", message);
                                    } else {
                                        showSnackbarMessage("Coupon applied successfully.");
                                    }
                                } else {
                                    if (message != null && message.length() > 0) {
                                        showNotificationDialog("Alert!", message);
                                    } else {
                                        coupon.showError("* Something went wrong , try again!");
                                    }
                                }
                            }
                        });
                    } else {
                        coupon.showError("* Enter valid coupon");
                    }
                }

            }
        });
        showCustomDialog(couponDialogView);

    }

    public void displayTotalInfo(double cartTotalAmount, double cartTotalServiceFees, double cartTotalGovtTaxes){

        double cartSubtotal = cartTotalAmount-cartTotalServiceFees-cartTotalGovtTaxes;

        if ( cartSubtotal > 0 ){
            cart_total.setText(getResources().getString(R.string.Rs)+" "+ CommonMethods.getInDecimalFormat(cartTotalAmount));
            cart_subtotal.setText(getResources().getString(R.string.Rs)+" "+ CommonMethods.getInDecimalFormat(cartSubtotal));
            cart_service_charges.setText(getResources().getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(cartTotalServiceFees));
            cart_government_taxes.setText(getResources().getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(cartTotalGovtTaxes));

            total_summary.setVisibility(View.VISIBLE);
            checkOut.setEnabled(true);
        }else{
            total_summary.setVisibility(View.GONE);
            checkOut.setEnabled(false);
        }
    }

    //-----------------------

    public void getCartItems(){

        showLoadingIndicator("Loading cart items, wait..");
        getBackendAPIs().getCartAPI().getCartItems(new CartAPI.GetCartItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String serverTime, List<CartItemDetails> cartItems, double cartTotalAmount, double cartTotalServiceCharges, double cartTotalGovtTaxes) {

                if (status) {

                    localStorageData.setServerTime(DateMethods.getReadableDateFromUTC(serverTime));

                    if (cartItems.size() > 0) {
                        cartItemsAdapter.addItems(cartItems);
                        displayTotalInfo(cartTotalAmount, cartTotalServiceCharges, cartTotalGovtTaxes);
                        switchToContentPage();
                    } else {
                        context.stopService(timedCartService);
                        cartTimer.setVisible(false);
                        broadcastReceiversMethods.stopTimerCart();
                        showNoDataIndicator("No items in your cart.");
                        showNotificationDialog(false, "Alert!", "Sorry there is no items in your cart.", new ActionCallback() {
                            @Override
                            public void onAction() {
                                onBackPressed();
                            }
                        });
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load cart items, try again!");
                }
            }
        });
    }

    public void updateCartItme(final int position, final double newQuantity, final String newDBY){

        final CartItemDetails cartItemDetails = cartItemsAdapter.getCartItem(position);

        showLoadingDialog("Updating item, wait..");
        getBackendAPIs().getCartAPI().updateCartItem(cartItemDetails.getDishUploadID(), cartItemDetails.getQuantity(), newQuantity, newDBY, new CartAPI.UpdateCartItemCallck() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, boolean isAlreadyPresent, double latestAvailability, double cartTotalAmount, double cartTotalServiceCharges, double cartTotalGovtTaxes) {
                closeLoadingDialog();

                if (status == true) {

                    if (message.equalsIgnoreCase("OK")) {

                        broadcastReceiversMethods.updateFoodItemAvailability(cartItemDetails.getDishID(), latestAvailability);

                        cartItemDetails.setQuantity(newQuantity);
                        cartItemDetails.setDishCurrentAvailability(latestAvailability);
                        cartItemDetails.setDBY(newDBY);
                        cartItemsAdapter.updateCartItem(position, cartItemDetails);
                        displayTotalInfo(cartTotalAmount, cartTotalServiceCharges, cartTotalGovtTaxes);

                        showItemUpdateInfo(cartItemDetails.getDishName(), cartItemDetails.getDishQuantityType(), newQuantity, newDBY);

                    } else if (message.equalsIgnoreCase("slot_closed")) {
                        Toast.makeText(context, "Sorry Current Slot is Ended", Toast.LENGTH_LONG).show();
                        Intent closedAct = new Intent(context, ClosedActivity.class);
                        ComponentName componentName = closedAct.getComponent();
                       // Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
                       // startActivity(mainIntent);
                    } else if (message.equalsIgnoreCase("cart_add_item_availability_exceeded")) {
                        broadcastReceiversMethods.updateFoodItemAvailability(cartItemDetails.getDishID(), latestAvailability);
                        showSnackbarMessage("Requested item quantity exceeds the availability", null, null);
                    }

                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to update the item, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateCartItme(position, newQuantity, newDBY);
                        }
                    });
                }
            }
        });
    }

    public void deleteCartItem(final int position){

        final CartItemDetails cartItemDetailsDetails = cartItemsAdapter.getCartItem(position);

        showLoadingDialog("Removing item from your food basket, wait..");
        getBackendAPIs().getCartAPI().removeFromCart(cartItemDetailsDetails.getDishUploadID(), cartItemDetailsDetails.getQuantity(), cartItemDetailsDetails.getDBY(), new CartAPI.RemoveCartItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, double latestAvailability, double cartTotalAmount, double cartTotalServiceCharges, double cartTotalGovtTaxes) {

                closeLoadingDialog();

                if (status == true) {

                    if (message.equalsIgnoreCase("OK")) {

                        broadcastReceiversMethods.updateCartCount(localStorageData.getCartCount() - 1);
                        broadcastReceiversMethods.updateFoodItemAvailability(cartItemDetailsDetails.getDishID(), latestAvailability);

                        cartItemDetailsDetails.setDishCurrentAvailability(latestAvailability);
                        cartItemDetailsDetails.setQuantity(0);
                        cartItemsAdapter.deleteCartItem(position);
                        displayTotalInfo(cartTotalAmount, cartTotalServiceCharges, cartTotalGovtTaxes);

                        if (localStorageData.getCartCount() == 0) {
                            cartTimer.setVisible(false);
                            broadcastReceiversMethods.stopTimerCart();
                            onBackPressed();
                        }

                        showSnackbarMessage(cartItemDetailsDetails.getDishName() + " deleted from basket.", null, null);
                    } else if (message.equalsIgnoreCase("slot_closed")) {
                        Toast.makeText(context, "Sorry Current Slot is Ended", Toast.LENGTH_LONG).show();
                        Intent closedAct = new Intent(context, ClosedActivity.class);
                        ComponentName componentName = closedAct.getComponent();
                       // Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
                       // startActivity(mainIntent);
                    }

                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to remove the item from you food basket, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            deleteCartItem(position);
                        }
                    });
                }
            }
        });
    }

    public void removeCoupon(){

        showLoadingDialog("Removing coupon, wait...");
        getBackendAPIs().getCouponsAPI().removeCoupon(new CouponsAPI.AddOrRemoveCouponCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, CartTotalDetails cartTotalDetails) {
                closeLoadingDialog();
                if (status) {
                    showSnackbarMessage("Coupon removed successfully.");
                    //displayTotalInfo(cartTotalDetails);
                } else {
                    showChoiceSelectionDialog("Failure!", "Something went wrong while removing coupon.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                removeCoupon();
                            }
                        }
                    });
                }
            }
        });
    }

    //----------------------------

    public CategoryDetails getCategoryDetailsOfDish(String dishCategoryID) {

        CategoryDetails categoryDetails = new CategoryDetails();

        List<CategoryDetails> categories = localStorageData.getCategories();
        for (int i=0;i<categories.size();i++){
            if ( categories.get(i).getID().equalsIgnoreCase(dishCategoryID)){
                categoryDetails = categories.get(i);
                break;
            }
        }
        return categoryDetails;
    }

    //-----------------------------

    public void showItemUpdateInfo(String itemName, final String quantityType, double itemQuantity, String slot){

        if ( slot != null && slot.equalsIgnoreCase("immediate") ){
            slot = ".";
        }else{
            slot = " for "+slot + " slot.";
        }
        String updatedMessage = "";
        if ( quantityType.equalsIgnoreCase("n")) {
            updatedMessage = itemName+" updated to "+CommonMethods.getInDecimalFormat(itemQuantity) + " servings"  + slot;
            if ( itemQuantity == 1 ) {
                updatedMessage = itemName+" updated to "+CommonMethods.getInDecimalFormat(itemQuantity) + " serving"  + slot;
            }
        }
        else if ( quantityType.equalsIgnoreCase("w")) {
            updatedMessage = itemName+" updated to "+CommonMethods.getInDecimalFormat(itemQuantity) + " kgs" + slot;
            if ( itemQuantity == 1 ) {
                updatedMessage = itemName+" updated to "+CommonMethods.getInDecimalFormat(itemQuantity) + " kg" + slot;
            }
        }
        else if ( quantityType.equalsIgnoreCase("l")) {
            updatedMessage = itemName+" updated to "+CommonMethods.getInDecimalFormat(itemQuantity) + "L" + slot;
        }
        showSnackbarMessage(updatedMessage, null, null);
    }

    //=============================

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cart_timer, menu);
        cartTimer = menu.getItem(0);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.action_timer) {
            onBackPressed();
        }
        return true;
    }
}