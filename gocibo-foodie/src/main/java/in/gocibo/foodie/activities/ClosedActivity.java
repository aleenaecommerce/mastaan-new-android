package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.PlaceDetails;

import in.gocibo.foodie.R;
import in.gocibo.foodie.fragments.SideBarFragment;
import in.gocibo.foodie.constants.Constants;


public class ClosedActivity extends GociboNavigationDrawerActivity {

    SideBarFragment sideBarFragment;

    ImageButton openNavigationDrawer;

    TextView title;
    TextView subtitle;
    TextView slots_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);                                                             // Bcoz of Extending Custom Activity
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_closed);

        sideBarFragment = new SideBarFragment();
        sideBarFragment.setCallback(new SideBarFragment.SideBarCallback() {
            @Override
            public void onItemSelected(int position) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }

            @Override
            public void onDeliverLocationChange(PlaceDetails deliverLocationDetails) {

            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();

        //------------------------------

        title = (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitle);

        title.setText(localStorageData.getClosedPageTitle());
        subtitle.setText(localStorageData.getClosedPageSubTitle());

        openNavigationDrawer = (ImageButton) findViewById(R.id.openNavigationDrawer);
        openNavigationDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        long differenceTime = DateMethods.getDifferenceToCurrentTime(localStorageData.getHomePageLastLoadedTime());
        if ( differenceTime > 600000 ) {     // If greater than 10 mins InActiveTime
            Log.d(Constants.LOG_TAG, "Reloading App as InActive time > 10min");
            Intent launchActivity = new Intent(context, LaunchActivity.class);
            startActivity(launchActivity);
            finish();
        }
    }
}
