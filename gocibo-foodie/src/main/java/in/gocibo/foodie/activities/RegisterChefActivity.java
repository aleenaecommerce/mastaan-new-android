package in.gocibo.foodie.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.aleena.common.interfaces.ContactPickerCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;

import in.gocibo.foodie.R;
import in.gocibo.foodie.constants.Constants;

public class RegisterChefActivity extends GociboToolBarActivity implements View.OnClickListener{

    ViewSwitcher viewSwitcher;
    LinearLayout register_layout, success_layout, error_layout;
    ImageButton pickFromContacts;
    TextView success_info;
    TextView error_message;

    vTextInputLayout phone;
    Button register;
    Button reTry;

    String mobileNumber="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_register_chef);

        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);

        register_layout = (LinearLayout) findViewById(R.id.register_layout);
        pickFromContacts = (ImageButton) findViewById(R.id.pickFromContacts);
        pickFromContacts.setOnClickListener(this);
        success_layout = (LinearLayout) findViewById(R.id.success_layout);
        success_info = (TextView) findViewById(R.id.success_info);
        error_layout = (LinearLayout) findViewById(R.id.error_layout);
        error_message = (TextView) findViewById(R.id.error_message);

        phone = (vTextInputLayout) findViewById(R.id.phone);
        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(this);

        reTry = (Button) findViewById(R.id.reTry);
        reTry.setOnClickListener(this);

        if ( getIntent().getStringExtra("registerChefType").equalsIgnoreCase("me") ){
            register_layout.setVisibility(View.INVISIBLE);
            registerChef(localStorageData.getBuyerMobile());
        }else{
            register_layout.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View view) {

        if ( view == register ){
            phone.checkError("*Enter Phone number");
            String oPhone = phone.getText();

            if ( CommonMethods.isValidPhoneNumber(oPhone) ){
                registerChef(oPhone);
            }else {
                phone.showError("*Enter a valid phone number (10 digits)");
            }
        }
        else if ( view == reTry ){
            registerChef(mobileNumber);
        }
        else if ( view == pickFromContacts ){

            phone.setText("");
            pickFromContacts(new ContactPickerCallback() {
                @Override
                public void onComplete(boolean status, String contactName, String contactNumber) {
                    if ( status ){
                        phone.setText(CommonMethods.removeSpacesFromString(contactNumber));
                    }
                }
            });
        }
    }

    public void registerChef(String mobileNumber){

        this.mobileNumber = mobileNumber;

        showLoadingDialog("Registering wait...");
        getBackendAPIs().getChefsAPI().registerChef(mobileNumber, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                register.setEnabled(true);
                if (status == true) {
                    showView("Success");
                } else {
                    error_message.setText("Oops, Something went wrong while registering!");
                    if ( statusCode == -1 ){
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if ( isInternetAvailable ){
                            error_message.setText("Not able to connect to Server. \nPlease try again after a while");
                        }else{
                            error_message.setText("No Internet connection!");
                        }
                        error_message.setText("No Internet connection!");
                    }
                    showView("Error");
                }
            }
        });
    }

    public void showView(String view){

        if ( view.equalsIgnoreCase("Success") ){
            viewSwitcher.setDisplayedChild(1);
            if ( getIntent().getStringExtra("registerChefType").equalsIgnoreCase("me") ) {
                success_info.setText("Our talent search team will contact you within 24 hours.");
            }else{
                success_info.setText("Our talent search team will contact your friend within 24 hours.");
            }
            success_layout.setVisibility(View.VISIBLE);
            error_layout.setVisibility(View.GONE);
        }
        else if ( view.equalsIgnoreCase("Error") ){
            viewSwitcher.setDisplayedChild(1);
            success_layout.setVisibility(View.GONE);
            error_layout.setVisibility(View.VISIBLE);
        }
    }

    //======================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        inputMethodManager.hideSoftInputFromWindow(phone.getWindowToken(), 0);    // Hides Key Board After Item Select..
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent chefIntent = new Intent();
        chefIntent.putExtra("mobile", mobileNumber);
        setResult(Constants.REGISTER_CHEF_ACTIVITY_CODE, chefIntent);
        finish();
    }
}
