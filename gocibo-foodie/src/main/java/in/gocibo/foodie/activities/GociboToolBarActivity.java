package in.gocibo.foodie.activities;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.core.content.IntentCompat;
import android.util.Log;
import android.widget.Toast;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.methods.DateMethods;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.GoCiboApplication;
import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.BackendAPIs;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.interfaces.RegistrationCallback;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.localdata.LocationDatabase;
import in.gocibo.foodie.localdata.OrdersFeedbackDatabase;
import in.gocibo.foodie.methods.BroadcastReceiversMethods;
import in.gocibo.foodie.models.BuyerDetails;
import in.gocibo.foodie.models.OrderDetails;
import in.gocibo.foodie.services.FireBaseService;
import in.gocibo.foodie.services.TimedCartService;


public class GociboToolBarActivity extends vToolBarActivity {

    public GoCiboApplication goCiboApplication;        // GoCibo Application Class

    public Activity activity;
    public Context context;

    public BroadcastReceiversMethods broadcastReceiversMethods;
    public LocalStorageData localStorageData;

    public Typeface kaushanFont;

    Intent timedCartService;
    Intent firebaseService;

    RegistrationCallback registrationCallback;

    public String GOCIBO_BASE_URL;
    private BackendAPIs backendAPIs;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        context = this;
        activity = this;

        goCiboApplication = (GoCiboApplication) getApplication();       // Accessing Application Class;
        goCiboApplication.setCurrentActivityContext(context);           // Setting Current Class Context.

        broadcastReceiversMethods = new BroadcastReceiversMethods(context);

        localStorageData = new LocalStorageData(context);

        GOCIBO_BASE_URL = getString(R.string.gocibo_base_url);

        timedCartService = new Intent(context, TimedCartService.class);
        firebaseService = new Intent(context, FireBaseService.class);

        //-------

        kaushanFont = Typeface.createFromAsset(getAssets(), "fonts/KaushanScript-Regular.ttf");

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        goCiboApplication = (GoCiboApplication) getApplication();       // Accessing Application Class;
    }

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(context);
        }
        return backendAPIs;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ( goCiboApplication != null ){
            goCiboApplication.setCurrentActivityContext(context);
        }

        long differenceTime = DateMethods.getDifferenceToCurrentTime(new LocalStorageData(this).getHomePageLastLoadedTime());
        if ( differenceTime > 600000 ) {     // If greater than 10 mins (600,000 millisec) InActiveTime
            Log.d(Constants.LOG_TAG, "Reloading App as InActive time > 10min");

            Intent launchActivity = new Intent(this, LaunchActivity.class);
            ComponentName componentName = launchActivity.getComponent();
           // Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
           // startActivity(mainIntent);
        }
    }

    @Override
    protected void onPause() {
        try {
            localStorageData.setHomePageLastLoadedTime(DateMethods.getCurrentDateAndTime());
        }catch (Exception e){}

        super.onPause();
    }

    public void checkSessionValidity(int statusCode){
        if ( localStorageData.getSessionFlag() == true && statusCode == 403 ){
            clearUserSession("Your session has expired.");
        }
    }

    public void addToPendingOrders(OrderDetails orderDetails){
        if ( orderDetails != null ){
            List<OrderDetails> pendingOrders = localStorageData.getBuyerPendingOrders();
            if ( pendingOrders == null ){
                pendingOrders = new ArrayList<>();
            }
            pendingOrders.add(orderDetails);
            localStorageData.setBuyerPendingOrders(pendingOrders);
        }
    }

    public void deleteFromPendingOrders(String orderID){
        if ( orderID != null && orderID.length() > 0 ){
            boolean isDeleted = false;
            List<OrderDetails> pendingOrders = localStorageData.getBuyerPendingOrders();
            for (int i=0;i<pendingOrders.size();i++){
                OrderDetails pendingOrderDetails = pendingOrders.get(i);
                if ( pendingOrderDetails.getID().equals(orderID) ){
                    pendingOrders.remove(i);
                    isDeleted = true;
                    break;
                }
            }
            if ( isDeleted == true ){
                localStorageData.setBuyerPendingOrders(pendingOrders);
            }
        }
    }

    //==============  CUSTOM DIALOGS METHODS  ==============//

    public void clearUserSession(String messageToDisplay){

        try {
            context.stopService(firebaseService);
        }catch (Exception e){}

        localStorageData.clearBuyerSession();        // Clearing Session
        localStorageData.setFirstCartItemFlag(false);

        LocationDatabase locationDatabase = new LocationDatabase(context);
        locationDatabase.openDatabase();
        locationDatabase.deleteAllPlaces();     // Deleteing all the previously stored locations

        OrdersFeedbackDatabase ordersFeedbackDatabase = new OrdersFeedbackDatabase(context);
        ordersFeedbackDatabase.openDatabase();
        ordersFeedbackDatabase.deleteAllFeedbackItems();
        //ordersFeedbackDatabase.closeDatabase();

        Toast.makeText(context, messageToDisplay, Toast.LENGTH_LONG).show();

        Intent loadingAct = new Intent(context, LaunchActivity.class);
        ComponentName componentName = loadingAct.getComponent();
      //  Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
       // startActivity(mainIntent);
    }

    public void goToRegistration(RegistrationCallback registrationCallback){

        this.registrationCallback = registrationCallback;

        Intent registerAct = new Intent(context, RegisterWithMobileActivity.class);
        registerAct.putExtra("baseURL", GOCIBO_BASE_URL);
        startActivityForResult(registerAct, Constants.REGISTER_ACTIVITY_CODE);
    }

    //===============

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if ( requestCode == Constants.REGISTER_ACTIVITY_CODE ) {

                if ( resultCode == RESULT_OK ) {

                    localStorageData.storeBuyerSession(data.getStringExtra("token"), new Gson().fromJson(data.getStringExtra("buyerDetails"), BuyerDetails.class));

                    Intent sidebarReceiver = new Intent(Constants.SIDEBAR_RECEIVER);
                    sidebarReceiver.putExtra("type", "session_update");
                    sidebarReceiver.putExtra("mobileNumber", data.getStringExtra("mobileNumber"));
                    context.sendBroadcast(sidebarReceiver);

                    if (registrationCallback != null) {
                        registrationCallback.onComplete(true, localStorageData.getAccessToken(), localStorageData.getBuyerDetails());
                    }
                }else{
                    if (registrationCallback != null) {
                        registrationCallback.onComplete(false, null, null);
                    }
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }
}
