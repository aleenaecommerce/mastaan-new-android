package in.gocibo.foodie.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.FeedbackOrderItemsAdapter;
import in.gocibo.foodie.backend.model.RequestOrderFeedback;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.OrdersFeedbackDatabase;
import in.gocibo.foodie.models.OrderDetails;
import in.gocibo.foodie.models.OrderFeedback;
import in.gocibo.foodie.models.OrderItemDetails;

public class OrderFeedbackActivity extends GociboToolBarActivity implements View.OnClickListener{

    OrdersFeedbackDatabase ordersFeedbackDatabase;

    TextView order_id, order_date;
    Button sad, neutral, happy;

    TextView tap_down_hit;
    RelativeLayout comment_pan;
    AppCompatEditText comments;
    FloatingActionButton submit;

    LinearLayout layout_fader, happy_fader, sad_fader, neutral_fader;

    public ViewPager orderItemsHolder;
    FeedbackOrderItemsAdapter orderItemsAdapter;

    boolean showNextIndicator = true;
    OrderDetails orderDetails;
    List<OrderItemDetails> feedbackItems;
    int currentOrderFoodItemIndex;

    RequestOrderFeedback requestOrderFeedback;

    OrderFeedback currentOrderFeedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_feedback);
        actionBar.setDisplayHomeAsUpEnabled(false);

        showNextIndicator = getIntent().getBooleanExtra("showNextIndicator", true);
        String order_details_json = getIntent().getStringExtra("order_details_json");
        orderDetails = new Gson().fromJson(order_details_json, OrderDetails.class);
        feedbackItems = orderDetails.getOrderItemsExcludingSubmittedFeedbackItemsAndFailedItems();

        requestOrderFeedback = new RequestOrderFeedback();

        ordersFeedbackDatabase = new OrdersFeedbackDatabase(context);

        //--------------------------------------

        order_id = (TextView) findViewById(R.id.order_id);
        order_date = (TextView) findViewById(R.id.order_date);

        sad = (Button) findViewById(R.id.sad);
        sad.setOnClickListener(this);
        neutral = (Button) findViewById(R.id.neutral);
        neutral.setOnClickListener(this);
        happy = (Button) findViewById(R.id.happy);
        happy.setOnClickListener(this);

        tap_down_hit = (TextView) findViewById(R.id.tap_down_hit);
        comment_pan = (RelativeLayout) findViewById(R.id.comment_pan);
        comments = (AppCompatEditText) findViewById(R.id.comments);
        submit = (FloatingActionButton) findViewById(R.id.submit);
        submit.setOnClickListener(this);

        layout_fader= (LinearLayout) findViewById(R.id.layout_fader);
        layout_fader.setOnClickListener(this);

        sad_fader= (LinearLayout) findViewById(R.id.sad_fader);
        sad_fader.setOnClickListener(this);

        neutral_fader= (LinearLayout) findViewById(R.id.neutral_fader);
        neutral_fader.setOnClickListener(this);

        happy_fader= (LinearLayout) findViewById(R.id.happy_fader);
        happy_fader.setOnClickListener(this);

        orderItemsHolder = (ViewPager) findViewById(R.id.orderItemsHolder);
        //orderItemsAdapter = new FeedbackOrderItemsAdapter(context, getSupportFragmentManager(), orderItemsHolder.getDishID(), new ArrayList<CartItemDetails>(), null);
        //orderItemsHolder.setAdapter(orderItemsAdapter);
        //orderItemsHolder.addOnPageChangeListener(orderItemsAdapter);
        orderItemsHolder.setOffscreenPageLimit(1);  // Necessary or the pager will only have one extra page to show make this at least however many pages you can see
        orderItemsHolder.setClipToPadding(false);   // Set margin for pages as a negative number, so a part of next and previous pages will be showed
        //orderItemsHolder.setPageMargin(-1 * this.getResources().getDimensionPixelOffset(R.dimen.carousel_margin));    // REMOVED TEMPORARILY

        //-----------------------------------------

        order_id.setText("Order ID : "+orderDetails.getOrderID().toUpperCase());
        order_date.setText("Ordered on " + DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()), DateConstants.MMM_DD_YYYY));
        //orderItemsAdapter.clearItems();
        //orderItemsAdapter.setItems(currentOrderDetails.getOrderItemsExcludingFailed());
        orderItemsAdapter = new FeedbackOrderItemsAdapter(context, getSupportFragmentManager(), orderItemsHolder.getId(), feedbackItems, null);
        orderItemsHolder.setAdapter(orderItemsAdapter);
        orderItemsHolder.addOnPageChangeListener(orderItemsAdapter);
        showFoodItem(0);

    }

    @Override
    public void onClick(View view) {

        if ( view == layout_fader ){
            tap_down_hit.setVisibility(View.VISIBLE);
            comment_pan.setVisibility(View.GONE);
            toggleFaderVisibility(layout_fader, View.GONE);
            sad_fader.setVisibility(View.VISIBLE);
            toggleFaderVisibility(sad_fader, View.GONE);
            neutral_fader.setVisibility(View.VISIBLE);
            toggleFaderVisibility(neutral_fader, View.GONE);
            happy_fader.setVisibility(View.VISIBLE);
            toggleFaderVisibility(happy_fader, View.GONE);
        }
        if ( view == sad || view == sad_fader ){
            comments.setText("");
            comments.setHint("Too bad you don't like it. How can we improve?");
            comment_pan.setBackgroundColor(Color.parseColor("#d50000"));
            if ( comment_pan.getVisibility() == View.INVISIBLE || comment_pan.getVisibility() == View.GONE ) {
                comment_pan.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in));
            }
            comment_pan.setVisibility(View.VISIBLE);
            tap_down_hit.setVisibility(View.GONE);

            toggleFaderVisibility(layout_fader, View.VISIBLE);
            toggleFaderVisibility(sad_fader, View.GONE);
            toggleFaderVisibility(neutral_fader, View.VISIBLE);
            toggleFaderVisibility(happy_fader, View.VISIBLE);

            submit.setImageResource(R.drawable.ic_send_red);

            currentOrderFeedback = new OrderFeedback(feedbackItems.get(currentOrderFoodItemIndex).getID(), feedbackItems.get(currentOrderFoodItemIndex).getDishID(), 1, "");
            //showFoodItem(currentOrderFoodItemIndex + 1);
        }
        else if ( view == neutral || view ==  neutral_fader){
            comments.setText("");
            comments.setHint("Ok! We'll improve. Tap to add any comments");
            comment_pan.setBackgroundColor(Color.parseColor("#2979ff"));
            if ( comment_pan.getVisibility() == View.INVISIBLE || comment_pan.getVisibility() == View.GONE ) {
                comment_pan.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in));
            }
            comment_pan.setVisibility(View.VISIBLE);
            tap_down_hit.setVisibility(View.GONE);

            toggleFaderVisibility(layout_fader, View.VISIBLE);
            toggleFaderVisibility(sad_fader, View.VISIBLE);
            toggleFaderVisibility(neutral_fader, View.GONE);
            toggleFaderVisibility(happy_fader, View.VISIBLE);

            submit.setImageResource(R.drawable.ic_send_blue);

            currentOrderFeedback = new OrderFeedback(feedbackItems.get(currentOrderFoodItemIndex).getID(), feedbackItems.get(currentOrderFoodItemIndex).getDishID(), 3, "");
            //showFoodItem(currentOrderFoodItemIndex+1);
        }
        else if ( view == happy || view == happy_fader ){
            comments.setText("");
            comments.setHint("Awesome! Tap to write your appreciation");
            comment_pan.setBackgroundColor(Color.parseColor("#00c853"));
            if ( comment_pan.getVisibility() == View.INVISIBLE || comment_pan.getVisibility() == View.GONE ) {
                comment_pan.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in));
            }
            comment_pan.setVisibility(View.VISIBLE);
            tap_down_hit.setVisibility(View.GONE);

            toggleFaderVisibility(layout_fader, View.VISIBLE);
            toggleFaderVisibility(sad_fader, View.VISIBLE);
            toggleFaderVisibility(neutral_fader, View.VISIBLE);
            toggleFaderVisibility(happy_fader, View.GONE);

            submit.setImageResource(R.drawable.ic_send_green);

            currentOrderFeedback = new OrderFeedback(feedbackItems.get(currentOrderFoodItemIndex).getID(), feedbackItems.get(currentOrderFoodItemIndex).getDishID(), 5, "");
            //showFoodItem(currentOrderFoodItemIndex+1);
        }
        else if ( view == submit ){

            inputMethodManager.hideSoftInputFromWindow(comments.getWindowToken(), 0);    // Hides Key Board After Item Select..

            currentOrderFeedback.setComments(comments.getText().toString());
            requestOrderFeedback.addFeedback(currentOrderFeedback);
            showFoodItem(currentOrderFoodItemIndex + 1);
        }

    }

    public void toggleFaderVisibility(View faderView , int visibility){
        if ( visibility == View.VISIBLE ){
            if ( faderView.getVisibility() == View.GONE || faderView.getVisibility() == View.INVISIBLE ){
                faderView.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in));
            }
            faderView.setVisibility(View.VISIBLE);
        }else if ( visibility == View.GONE || visibility == View.INVISIBLE ){
            if ( faderView.getVisibility() == View.VISIBLE ){
                faderView.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_out));
            }
            faderView.setVisibility(View.GONE);
        }
    }

    public void showFoodItem(int index){

        if ( feedbackItems.size() > index ){
            currentOrderFoodItemIndex = index;
            orderItemsHolder.setCurrentItem(currentOrderFoodItemIndex);

            comments.setText("");
            comment_pan.setVisibility(View.GONE);
            tap_down_hit.setVisibility(View.VISIBLE);
            layout_fader.setVisibility(View.GONE);
            sad_fader.setVisibility(View.GONE);
            neutral_fader.setVisibility(View.GONE);
            happy_fader.setVisibility(View.GONE);
        }else {
            if (feedbackItems.size() > 0){
                submitFeedback(false);
            }else{
                Intent resultData = new Intent();
                setResult(Constants.ORDER_FEEDBACK_ACTIVITY_CODE, resultData);
                finish();
            }
        }
    }

    public void submitFeedback(boolean remindLater){

        requestOrderFeedback.setRemindLater(remindLater);
        getBackendAPIs().getOrdersAPI().postOrderFeedback(orderDetails.getID(), requestOrderFeedback, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status == false) {
                    if (statusCode == 403) {
                        clearUserSession("Your session has expired.");
                    } else {
                        ordersFeedbackDatabase.openDatabase();
                        ordersFeedbackDatabase.addFeedbackItem("Submit", orderDetails.getID(), localStorageData.getBuyerMobile(), requestOrderFeedback, new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                Log.d(Constants.LOG_TAG, "Unable to Submit Feedback for OrderID=" + " , Storing it for Future");
                            }
                        });
                    }
                }
            }
        });

        if ( requestOrderFeedback.getFeedbacks().size() > 0 ){
            finishFeedback(true);
        }else{
            finishFeedback(false);
        }

    }

    public void skipFeedback(){

        getBackendAPIs().getOrdersAPI().skipOrderFeedback(orderDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {

                if (status == false ){
                    if ( statusCode == 403 ){
                        clearUserSession("Your session has expired.");
                    }
                    else {
                        ordersFeedbackDatabase.openDatabase();
                        ordersFeedbackDatabase.addFeedbackItem("Skip", orderDetails.getID(), localStorageData.getBuyerMobile(), requestOrderFeedback, new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                Log.d(Constants.LOG_TAG, "Unable to Skip Feedback for OrderID=" + " , Storing it for Future");
                            }
                        });
                    }
                }
            }
        });

        finishFeedback(false);
    }

    public void finishFeedback(final boolean isSubmittedFeedback){

        if ( showNextIndicator == true ){
            showLoadingDialog("Loading Next order details, wait..");
            new CountDownTimer(180, 80) {
                public void onTick(long millisUntilFinished) {}
                public void onFinish() {
                    Intent resultData = new Intent();
                    resultData.putExtra("isSubmittedFeedback", isSubmittedFeedback);
                    setResult(Constants.ORDER_FEEDBACK_ACTIVITY_CODE, resultData);
                    finish();
                }
            }.start();
        }else{
            Intent resultData = new Intent();
            resultData.putExtra("isSubmittedFeedback", isSubmittedFeedback);
            setResult(Constants.ORDER_FEEDBACK_ACTIVITY_CODE, resultData);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_feedback, menu);

        if ( orderDetails.isAlreadyRemindedFeedbackForLater() ){
            menu.findItem(R.id.action_remind_later).setVisible(false);
        }else{
            menu.findItem(R.id.action_remind_later).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_skip ) {
            skipFeedback();
        }
        else if ( menuItem.getItemId() == R.id.action_remind_later ) {
            submitFeedback(true);
        }
        return true;
    }
}
