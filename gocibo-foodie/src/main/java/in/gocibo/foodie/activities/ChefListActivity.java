package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.ChefsListAdapter;
import in.gocibo.foodie.backend.ChefsAPI;
import in.gocibo.foodie.models.ChefDetails;
import com.aleena.common.widgets.vRecyclerView;

public class ChefListActivity extends GociboToolBarActivity {

    vRecyclerView chefsListHolder;
    ChefsListAdapter chefsListAdapter;
    ChefsListAdapter.CallBack chefsListCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_chef_list);
        hasLoadingView();

        //........................................................

        chefsListHolder = (vRecyclerView) findViewById(R.id.chefsListHolder);
        chefsListHolder.setHasFixedSize(true);
        chefsListHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        chefsListHolder.setItemAnimator(new DefaultItemAnimator());

        chefsListCallback = new ChefsListAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Intent chefAct = new Intent(context, ChefDetailsActivity.class);
                chefAct.putExtra("chefID", chefsListAdapter.getChefDetails(position).getID());
                startActivity(chefAct);
            }
        };

        chefsListAdapter = new ChefsListAdapter(context, new ArrayList<ChefDetails>(), chefsListCallback);
        chefsListHolder.setAdapter(chefsListAdapter);

        getChefsList();        // GetChefs

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getChefsList();
    }

    public void getChefsList(){

        showLoadingIndicator("Loading our chefs list, wait..");
        getBackendAPIs().getChefsAPI().getChefsList(new ChefsAPI.ChefsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<ChefDetails> chefsList) {
                if ( status == true ){
                    if ( chefsList.size() > 0 ){
                        chefsListAdapter.addItems(chefsList);
                        switchToContentPage();
                    }else {
                        showNoDataIndicator("No Chefs yet!");
                    }
                }else {
                    showReloadIndicator(statusCode, "Unable to load our chefs list, try again!");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
    }

}
