package in.gocibo.foodie.activities;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.gson.Gson;

import com.aleena.common.models.PlaceDetails;
import in.gocibo.foodie.R;
import in.gocibo.foodie.fragments.AddressBookFragment;


public class AddressBookActivity extends GociboToolBarActivity {

    PlaceDetails faouriteLocationDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_address_book);

        //...........................................................

        try {
            String favourite_location_json = getIntent().getStringExtra("favourite_location_json");
            faouriteLocationDetails = new Gson().fromJson(favourite_location_json, PlaceDetails.class);
        }catch (Exception e){}

        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, new AddressBookFragment().newInstance(context, true, faouriteLocationDetails)).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
    }

}
