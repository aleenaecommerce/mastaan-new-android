package in.gocibo.foodie.activities;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.OrderItemsAdapter;
import in.gocibo.foodie.backend.OrdersAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.OrderDetails;
import in.gocibo.foodie.models.OrderItemDetails;

public class OrderDetailsActivity extends GociboToolBarActivity implements View.OnClickListener{

    String orderID;
    OrderDetails orderDetails;
    List<OrderItemDetails> orderItems;

    TextView order_id;
    TextView order_name;
    TextView order_payment_mode;
    TextView order_deliver_address;
    TextView order_deliver_date;
    TextView order_coupon;
    TextView order_cost;
    LinearLayout order_items_status_holder;
    Button call_us;

    vRecyclerView orderItemsHolder;
    OrderItemsAdapter orderItemsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_order_details);
        hasLoadingView();

        orderID = getIntent().getStringExtra("orderID");
        registerActivityReceiver(Constants.ORDER_DETAILS_RECEIVER);

        //-------------------------------------------------

        order_id = (TextView) findViewById(R.id.order_id);
        order_name = (TextView) findViewById(R.id.order_name);
        order_payment_mode = (TextView) findViewById(R.id.order_payment_mode);
        order_deliver_address = (TextView) findViewById(R.id.order_deliver_address);
        order_deliver_date = (TextView) findViewById(R.id.order_deliver_date);
        order_coupon = (TextView) findViewById(R.id.order_coupon);
        order_cost = (TextView) findViewById(R.id.order_cost);

        order_items_status_holder = (LinearLayout) findViewById(R.id.order_items_status_holder);
        call_us = (Button) findViewById(R.id.call_us);
        call_us.setOnClickListener(this);

        //....................................

        orderItemsHolder = (vRecyclerView) findViewById(R.id.orderItemsHolder);
        orderItemsHolder.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        orderItemsHolder.setLayoutManager(manager);
        orderItemsHolder.setItemAnimator(new DefaultItemAnimator());

        orderItemsAdapter = new OrderItemsAdapter(context, new ArrayList<OrderItemDetails>(), null);
        orderItemsHolder.setAdapter(orderItemsAdapter);

        //---------------------------------------------------

        getOrderDetails();              //   Loadign Order Details

    }

    @Override
    public void onClick(View view) {

        if ( view == call_us ){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
            startActivity(intent);
        }
    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);

        try {
            if (receivedData.getStringExtra("type").equalsIgnoreCase("update_order_status")) {
                String orderID = receivedData.getStringExtra("orderID");
                String orderStatus = receivedData.getStringExtra("orderStatus");

                Log.d(Constants.LOG_TAG, "Checking to update OrderDetails in OrderDetailsPage, for oID = "+orderID+" ,  with DS = "+orderStatus);

                if ( orderDetails.getID().equals(orderID) ){
                    orderDetails.setOrderStaus(orderStatus);
                    displayOrderDetails();
                }
            }
            else if (receivedData.getStringExtra("type").equalsIgnoreCase("update_suborder_status")) {
                String orderID = receivedData.getStringExtra("orderID");
                String subOrderID = receivedData.getStringExtra("subOrderID");
                int subOrderStatus = receivedData.getIntExtra("subOrderStatus", -1);

                Log.d(Constants.LOG_TAG, "Checking to update SubOrderDetails in OrderDetailsPage , for oID = "+orderID+" , sOID = "+subOrderID+" with DS = "+subOrderStatus);

                if ( orderDetails.getID().equals(orderID) ){
                    for (int i=0;i< orderItems.size();i++){
                        if ( orderItems.get(i).getID().equals(subOrderID) ){
                            orderItems.get(i).setDeliverStatus(subOrderStatus);
                        }
                    }
                    displayOrderDetails();
                }
            }


        }catch (Exception e){}
    }

    public void displayOrderDetails(){

        switchToContentPage();

        order_items_status_holder.removeAllViews();
        orderItemsAdapter.deleteAllItems();

        order_id.setText(this.orderDetails.getOrderID());
        order_name.setText(CommonMethods.capitalizeFirstLetter(this.orderDetails.getUserName()));
        order_payment_mode.setText(this.orderDetails.getPaymentType());
        order_deliver_address.setText(this.orderDetails.getDeliveryAddress());
        order_deliver_date.setText(DateMethods.getDateInFormat(CommonMethods.getReadableTimeFromUTC(this.orderDetails.getCreatedDate()), DateConstants.MMM_DD_YYYY_HH_MM_A));
        order_coupon.setText("");
        order_cost.setText(getResources().getString(R.string.Rs) + " " + CommonMethods.getInDecimalFormat(this.orderDetails.getTotalAmount()));

        orderItemsAdapter.addItems(this.orderDetails.getOrderItems());      // Display Order Items.

        if ( orderDetails.getOrderStaus().equalsIgnoreCase("processing") || orderDetails.getOrderStaus().equalsIgnoreCase("pending")){

            call_us.setVisibility(View.VISIBLE);
            call_us.setText("Tap to call "+localStorageData.getSupportContactNumber()+" for order status/cancellation");

            /*orderedFoodItems.add(new CartItemDetails("ID", "Egg Curry", "", 20, 1, 2, 2, "cid", "Venkatesh Uppu", "", 1));
            orderedFoodItems.add(new CartItemDetails("ID", "Egg Curry", "", 20, 1, 2, 2, "cid", "Venkatesh Uppu", "", 4));
            orderedFoodItems.add(new CartItemDetails("ID", "Egg Curry", "", 20, 1, 2, 2, "cid", "Venkatesh Uppu", "", 2));
            orderedFoodItems.add(new CartItemDetails("ID", "Egg Curry", "", 20, 1, 2, 2, "cid", "Venkatesh Uppu", "", 3));
            orderedFoodItems.add(new CartItemDetails("ID", "Egg Curry", "", 20, 1, 2, 2, "cid", "Venkatesh Uppu", "", 5));*/

            for (int i=0;i< orderItems.size();i++){

                OrderItemDetails orderItemDetails = orderItems.get(i);

                View orderItemView = inflater.inflate(R.layout.view_order_status_item, null, false);
                order_items_status_holder.addView(orderItemView);
                TextView item_name = (TextView) orderItemView.findViewById(R.id.item_name);
                TextView item_portions = (TextView) orderItemView.findViewById(R.id.item_portions);
                TextView delivery_slot = (TextView) orderItemView.findViewById(R.id.delivery_slot);

                View order_status = orderItemView.findViewById(R.id.order_status);
                TextView processing = (TextView) orderItemView.findViewById(R.id.processing);
                TextView pickup_pending = (TextView) orderItemView.findViewById(R.id.pickup_pending);
                TextView on_the_way = (TextView) orderItemView.findViewById(R.id.on_the_way);
                TextView delivered = (TextView) orderItemView.findViewById(R.id.delivered);
                TextView failed = (TextView) orderItemView.findViewById(R.id.failed);

                item_name.setText(CommonMethods.capitalizeFirstLetter(orderItemDetails.getDishName())+" by "+CommonMethods.capitalizeFirstLetter(orderItemDetails.getDishChefFirstName()));
                delivery_slot.setText(orderItemDetails.getDBY());

                if ( orderItemDetails.getDishQuantityType().equalsIgnoreCase("n")){
                    item_portions.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity())+" servings, "+RS+" "+CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity() * orderItemDetails.getDishPrice()));
                    if ( orderItemDetails.getQuantity() == 1 ){
                        item_portions.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity())+" serving, "+RS+" "+CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity() * orderItemDetails.getDishPrice()));
                    }
                }
                else if ( orderItemDetails.getDishQuantityType().equalsIgnoreCase("w")){
                    item_portions.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity())+" kgs, "+RS+" "+CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity() * orderItemDetails.getDishPrice()));
                    if ( orderItemDetails.getQuantity() == 1 ){
                        item_portions.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity())+" kg, "+RS+" "+CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity() * orderItemDetails.getDishPrice()));
                    }
                }
                else if ( orderItemDetails.getDishQuantityType().equalsIgnoreCase("v")){
                    item_portions.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity())+"L, "+RS+" "+CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity() * orderItemDetails.getDishPrice()));
                }


                GradientDrawable gradientDrawable;

                int deliveryStatus = orderItemDetails.getDeliverStatus();

                if ( deliveryStatus == 0 || deliveryStatus == 1 || deliveryStatus == 7 ){       //  Processing
                    gradientDrawable = (GradientDrawable) processing.getBackground();
                    gradientDrawable.setColor(getResources().getColor(R.color.order_processing_color));

                    order_status.setBackgroundColor(context.getResources().getColor(R.color.order_processing_color));

                    processing.setVisibility(View.VISIBLE);
                    orderItemView.findViewById(R.id.pickup_pending_icon).setVisibility(View.VISIBLE);
                    orderItemView.findViewById(R.id.on_the_way_pending_icon).setVisibility(View.VISIBLE);
                    orderItemView.findViewById(R.id.delivered_pending_icon).setVisibility(View.VISIBLE);
                }
                else if ( deliveryStatus == 2 ){        //  Pickup Pending
                    gradientDrawable = (GradientDrawable) pickup_pending.getBackground();
                    gradientDrawable.setColor(getResources().getColor(R.color.order_pickup_pending_color));

                    order_status.setBackgroundColor(context.getResources().getColor(R.color.order_pickup_pending_color));

                    orderItemView.findViewById(R.id.processing_done_icon).setVisibility(View.VISIBLE);
                    pickup_pending.setVisibility(View.VISIBLE);
                    orderItemView.findViewById(R.id.on_the_way_pending_icon).setVisibility(View.VISIBLE);
                    orderItemView.findViewById(R.id.delivered_pending_icon).setVisibility(View.VISIBLE);
                }
                else if ( deliveryStatus == 3 ){        //  On the Way
                    gradientDrawable = (GradientDrawable) on_the_way.getBackground();
                    gradientDrawable.setColor(getResources().getColor(R.color.order_on_the_way_color));

                    order_status.setBackgroundColor(context.getResources().getColor(R.color.order_on_the_way_color));

                    orderItemView.findViewById(R.id.processing_done_icon).setVisibility(View.VISIBLE);
                    orderItemView.findViewById(R.id.pickup_done_icon).setVisibility(View.VISIBLE);
                    on_the_way.setVisibility(View.VISIBLE);
                    orderItemView.findViewById(R.id.delivered_pending_icon).setVisibility(View.VISIBLE);
                }

                else if ( deliveryStatus == 4 ){        //  Delivered
                    gradientDrawable = (GradientDrawable) delivered.getBackground();
                    gradientDrawable.setColor(getResources().getColor(R.color.order_delivered_color));

                    order_status.setBackgroundColor(context.getResources().getColor(R.color.order_delivered_color));

                    orderItemView.findViewById(R.id.processing_done_icon).setVisibility(View.VISIBLE);
                    orderItemView.findViewById(R.id.pickup_done_icon).setVisibility(View.VISIBLE);
                    orderItemView.findViewById(R.id.on_the_way_done_icon).setVisibility(View.VISIBLE);
                    delivered.setVisibility(View.VISIBLE);
                }

                else if ( deliveryStatus == 5 || deliveryStatus == 6 ){        //  Failed
                    GradientDrawable failed_drawable = (GradientDrawable) failed.getBackground();
                    failed_drawable.setColor(getResources().getColor(R.color.order_cancelled_color));
                    order_status.setBackgroundColor(context.getResources().getColor(R.color.order_cancelled_color));
                    failed.setVisibility(View.VISIBLE);
                }
            }
        }else{
            call_us.setVisibility(View.GONE);
            order_items_status_holder.setVisibility(View.GONE);
            deleteFromPendingOrders(orderID);
        }

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getOrderDetails();
    }

    public void getOrderDetails(){

        showLoadingIndicator("Loading order details, wait..");
        getBackendAPIs().getOrdersAPI().getOrderDetails(orderID, new OrdersAPI.OrderDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, OrderDetails order_details) {
                if ( status == true ){
                    orderDetails = order_details;
                    orderItems = order_details.getOrderItems();
                    if ( orderItems == null ){   orderItems = new ArrayList<OrderItemDetails>();   }
                    displayOrderDetails();
                }
                else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load order details, try again!");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }
}
