package in.gocibo.foodie.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.google.gson.Gson;
import com.rey.material.widget.FloatingActionButton;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.OrdersAPI;
import in.gocibo.foodie.backend.model.RequestPlaceOrder;
import in.gocibo.foodie.models.OrderDetails;

public class OrderErrorActivity extends GociboToolBarActivity implements View.OnClickListener{

    TextView gocibo_contact_no;
    ImageView header_image;
    Button reTry;
    FloatingActionButton callUs;

    RequestPlaceOrder requestPlaceOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_order_error);

        //orderStausCode = getIntent().getIntExtra("status_code", 504);
        String request_object_place_order = getIntent().getStringExtra("request_object_place_order");
        requestPlaceOrder = new Gson().fromJson(request_object_place_order, RequestPlaceOrder.class);

        //.......................................

        header_image = (ImageView) findViewById(R.id.header_image);

        gocibo_contact_no = (TextView) findViewById(R.id.gocibo_contact_no);
        gocibo_contact_no.setText("Call us at "+localStorageData.getSupportContactNumber()+"\nto place your order.");

        reTry = (Button) findViewById(R.id.reTry);
        reTry.setOnClickListener(this);

        callUs = (FloatingActionButton) findViewById(R.id.callUs);
        callUs.setOnClickListener(this);

        /*Picasso.with(context)
                .load(cartItems.get(0).getThumbnailImageURL())
                .placeHolder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .fit()
                .centerCrop()
                .tag(context)
                .into(collage_images);*/
    }

    @Override
    public void onClick(View view) {

        if ( view == reTry ){
            placeOrder();
        }
        else if ( view == callUs ){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
            startActivity(intent);
        }
    }

    public void placeOrder(){

        showLoadingDialog("Placing your order, wait..");
        getBackendAPIs().getOrdersAPI().placeOrder(requestPlaceOrder, new OrdersAPI.PlaceOrderCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, OrderDetails orderDetails) {

                closeLoadingDialog();

                if ( status == true ){

                    localStorageData.setCartCount(0);
                    broadcastReceiversMethods.stopTimerCart();

                    Intent orderSuccessAct = new Intent(context, OrderSuccessActivity.class);
                    orderSuccessAct.putExtra("order_details_json", new Gson().toJson(orderDetails));
                    if ( requestPlaceOrder.getDeliverBy().equalsIgnoreCase("immediate")){
                        orderSuccessAct.putExtra("isImmediate", true);
                    }
                    startActivity(orderSuccessAct);
                    finish();
                }
                else {
                    if ( statusCode == -1 ){
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if ( isInternetAvailable == false ){
                            showSnackbarMessage("No internet connection!", null, null);
                        }
                    }
                    else if ( statusCode == 403 ){
                        clearUserSession("Your session has expired.");
                    }
                    else{
                        showSnackbarMessage("Unable to Place your Order!", "RETRY", new ActionCallback() {
                            @Override
                            public void onAction() {
                                placeOrder();
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        /*showLoadingDialog("Clearing your Cart, wait..");
        getBackendAPIs().getCartAPI().clearCartItems(new CartAPI.ClearCartItemsCallback() {
            @Override
            public void onComplete(boolean status, List<CartItemDetails> cartItems) {
                closeLoadingDialog();
                if (status == true) {
                    onBackPressed();
                    broadcastReceiversMethods.clearShoppingCart(cartItems);
                }
            }
        });*/

        onBackPressed();

        return true;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..

        Intent homeAct = new Intent(context, HomeActivity.class);
        homeAct.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        homeAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeAct);

    }
}
