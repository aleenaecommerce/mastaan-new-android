package in.gocibo.foodie.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.adapters.SearchPlacesAdapter;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vEditText;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.FoodItemsAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.CategoryDetails;


public class PlaceSearchActivity extends GociboToolBarActivity implements View.OnClickListener{

    boolean isForActivityResult = true;
    String searchName;

    vEditText searchLocation;
    ImageView clearSearch;
    LinearLayout addressBook;
    ImageView addressBookClickIcon;

    vRecyclerView searchPlacesHolder;
    SearchPlacesAdapter searchPlacesAdapter;
    SearchPlacesAdapter.CallBack searchPlacesCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_place_search);
        hasLoadingView();
        switchToContentPage();

        isForActivityResult = getIntent().getBooleanExtra("isForActivityResult", false);
        if ( isForActivityResult ){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }else{
            actionBar.setDisplayHomeAsUpEnabled(false);
        }

        //............................................................

        addressBookClickIcon = (ImageView) findViewById(R.id.addressBookClickIcon);
        addressBook = (LinearLayout) findViewById(R.id.addressBook);
        if ( localStorageData.getSessionFlag() == true ){
            addressBook.setOnClickListener(this);
        }else{
            addressBook.setVisibility(View.GONE);
        }

        clearSearch = (ImageView) findViewById(R.id.clearSearch);
        clearSearch.setOnClickListener(this);

        searchLocation = (vEditText) findViewById(R.id.searchLocation);
        searchLocation.setTextChangeListener(new vEditText.TextChangeCallback() {
            @Override
            public void onTextChangeFinish(final String text) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (text != null && text.length() > 0) {
                            getPlacePredictions(text);
                        } else {
                            clearSearch.setVisibility(View.INVISIBLE);
                            searchPlacesAdapter.clearItems();
                        }
                    }
                });
            }
        });

        searchPlacesHolder = (vRecyclerView) findViewById(R.id.searchPlacesHolder);
        searchPlacesHolder.setHasFixedSize(true);
        searchPlacesHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        searchPlacesHolder.setItemAnimator(new DefaultItemAnimator());

        searchPlacesCallback = new SearchPlacesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                fetchPlaceDetailsByIDAndProceed(searchPlacesAdapter.getPlaceDetails(position).get("place_id"), searchPlacesAdapter.getPlaceDetails(position).get("description").toString());
            }
        };

        searchPlacesAdapter = new SearchPlacesAdapter(context, new ArrayList<Map<String, String>>(), searchPlacesCallback);
        searchPlacesHolder.setAdapter(searchPlacesAdapter);

    }

    @Override
    public void onClick(View view) {

        if ( view == clearSearch ){
            switchToContentPage();
            searchLocation.setText("");
            searchPlacesAdapter.clearItems();
        }
        else if ( view == addressBook ){
            addressBookClickIcon.setVisibility(View.VISIBLE);
            new CountDownTimer(150, 50) {
                public void onTick(long millisUntilFinished) {}
                public void onFinish() {
                    addressBookClickIcon.setVisibility(View.GONE);
                    Intent addressBookSaveActivity = new Intent(context, AddressBookActivity.class);
                    addressBookSaveActivity.putExtra("favourite_location_json", new Gson().toJson(""));
                    startActivityForResult(addressBookSaveActivity, Constants.ADDRESS_BOOK_ACTIVITY_CODE);
                }
            }.start();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPlacePredictions(searchName);
    }

    public void getPlacePredictions(String qName){

        searchName = qName;

        clearSearch.setVisibility(View.VISIBLE);
        searchPlacesAdapter.clearItems();

        showLoadingIndicator("Searching, wait..");
        getGooglePlacesAPI().getPlacePredictions(qName, "17.3850051,78.4845113", 300*1000, new GooglePlacesAPI.PlacePredictionsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String queryName, List<Map<String, String>> placePredictions) {

                if (queryName.equalsIgnoreCase(searchName)) {
                    switchToContentPage();
                    if (status == true) {
                        if (placePredictions.size() > 0) {
                            searchPlacesAdapter.addItems(placePredictions);
                        } else {
                            showNoDataIndicator("Nothing found ");
                        }
                    } else {
                        showReloadIndicator(statusCode, "Unable to retrieve places, try again!");
                    }
                }
            }
        });
    }

    public void fetchPlaceDetailsByIDAndProceed(final String placeID, final String placeFullName){

        showLoadingDialog("Fetching place details, please wait...");
        getGooglePlacesAPI().getPlaceDetailsByPlaceID(placeID, new GooglePlacesAPI.PlaceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, LatLng location, final String placeID, PlaceDetails placeDetails) {

                if (status == true) {

                    if (placeDetails.getLocality() != null && placeDetails.getState() != null && placeDetails.getCountry() != null && placeDetails.getPostal_code() != null) {
                        closeLoadingDialog();
                        placeDetails.setFullName(placeFullName);
                        onDone(placeDetails);
                    } else {
                        Log.d(Constants.LOG_TAG, "Loading Place Details with LatLng for PlaceID = " + placeID + " (" + placeFullName + ") with LatLng = " + placeDetails.getLatLng());
                        fetchPlaceDetailsByLatLngAndProceed(false, placeDetails.getLatLng(), placeFullName);
                    }
                } else {
                    closeLoadingDialog();
                    showSnackbarMessage("Unable to Fetch Place Details!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            fetchPlaceDetailsByIDAndProceed(placeID, placeFullName);
                        }
                    });
                }
            }
        });

    }

    public void fetchPlaceDetailsByLatLngAndProceed(final boolean showLoadingDialog, final LatLng latLng, final String placeFullName){

        if ( showLoadingDialog ){
            showLoadingDialog("Fetching place details, please wait...");
        }
        getGooglePlacesAPI().getPlaceDetailsByLatLng(latLng, new GooglePlacesAPI.PlaceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, LatLng location, final String placeID, PlaceDetails placeDetails) {

                if (status == true) {
                    placeDetails.setFullName(placeFullName);
                    onDone(placeDetails);
                } else {
                    closeLoadingDialog();
                    showSnackbarMessage("Unable to Fetch Place Details!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            fetchPlaceDetailsByLatLngAndProceed(true, latLng, placeFullName);
                        }
                    });
                }
            }
        });

    }


    public void onDone(PlaceDetails deliverLocationDetails){

        if ( deliverLocationDetails != null ) {

            inputMethodManager.hideSoftInputFromWindow(searchLocation.getWindowToken(), 0);    // Hides Key Board After Item Select..

            if (isForActivityResult == true) {
                String deliver_location_json = new Gson().toJson(deliverLocationDetails);
                Intent placeData = new Intent();
                placeData.putExtra("deliver_location_json", deliver_location_json);
                setResult(Constants.PLACE_SEARCH_ACTIVITY_CODE, placeData);
                finish();
            }
            else {
                checkAvailabilityAtLocationAndProceedToHome(new AddressBookItemDetails(deliverLocationDetails));
            }
        }else{
            showSnackbarMessage("Delivery loaction not available.");
        }
    }

    public void checkAvailabilityAtLocationAndProceedToHome(final AddressBookItemDetails deliveryLocation){

        inputMethodManager.hideSoftInputFromWindow(searchLocation.getWindowToken(), 0);    // Hides Key Board After Item Select..

        showLoadingDialog("Loading, please wait..");
        getBackendAPIs().getFoodItemsAPI().getCategoriesList(deliveryLocation.getLatLng(), new FoodItemsAPI.CategoriesListCallback() {

            @Override
            public void onComplete(boolean status, int statusCode, String message, long total, List<CategoryDetails> categoriesList) {
                if (status) {
                    if ( total > 0 ) {
                        localStorageData.setDeliveryLocation(deliveryLocation);
                        localStorageData.storeCategories(categoriesList);

                        Intent homeAct = new Intent(context, HomeActivity.class);
                        //ComponentName componentName = homeAct.getComponent();
                        //Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
                        startActivity(homeAct);
                        finish();
                    }else{
                        closeLoadingDialog();
                        String msg = "We couldn't fetch you any of our delicious food items. This could be because..."
                                + "\n\n"
                                + "\t 1. Our chefs haven’t uploaded any dishes yet (do check back again)"
                                + "\n\t 2. All our items are past their availability time"
                                + "\n\n"
                                + "We are currently serving at " + localStorageData.getServingAt();
                        showNotificationDialog("Nothing to show!", msg);
                    }
                }
                else {
                    closeLoadingDialog();
                    showChoiceSelectionDialog(true, "Error!", "Something went wrong while checking availability at selected location.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                checkAvailabilityAtLocationAndProceedToHome(deliveryLocation);
                            }
                        }
                    });
                }
            }
        });
    }

    //=============================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if (resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE && resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE) {      // If It is from PlaceSearch Activity
                String address_item_json = data.getStringExtra("address_item_json");
                PlaceDetails deliverLocationDetails = new PlaceDetails(new Gson().fromJson(address_item_json, AddressBookItemDetails.class));
                onDone(deliverLocationDetails);
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(searchLocation.getWindowToken(), 0);    // Hides Key Board After Item Select..
    }
}