package in.gocibo.foodie.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.AuthenticationAPI;
import in.gocibo.foodie.backend.model.ResponseValidateMobile;
import in.gocibo.foodie.backend.model.ResponseVerifyMobile;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.BuyerDetails;


public class RegisterWithMobileActivity extends GociboToolBarActivity implements View.OnClickListener{

    long countDownTimeLimit = 45000;

    ResponseValidateMobile responseValidateMobile;

    ViewSwitcher viewSwitcher;

    LinearLayout register_layout;
    vTextInputLayout phone;
    Button register;
    ProgressBar validatingIndicator;
    TextView validationStatus;

    boolean autoVerify = true;
    FrameLayout auto_verifying_layout;
    TextView auto_verify_title, countdonwn_time;
    Button not_this_device;

    FrameLayout not_received_otp;
    FrameLayout send_otp_to_email;

    vTextInputLayout otp;
    Button verify;
    ProgressBar verifyingIndicator;
    TextView verficationStatus;

    String mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_register_with_mobile);

        registerActivityReceiver(Constants.OTP_RECEIVER);

        //......................................................

        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);

        register_layout = (LinearLayout) findViewById(R.id.register_layout);
        phone = (vTextInputLayout) findViewById(R.id.phone);
        validatingIndicator = (ProgressBar) findViewById(R.id.validatingIndicator);
        validationStatus = (TextView) findViewById(R.id.validationStatus);
        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(this);

        otp = (vTextInputLayout) findViewById(R.id.otp);
        verifyingIndicator = (ProgressBar) findViewById(R.id.verifyingIndicator);
        verficationStatus = (TextView) findViewById(R.id.verficationStatus);
        verify = (Button) findViewById(R.id.verify);
        verify.setOnClickListener(this);

        auto_verifying_layout = (FrameLayout) findViewById(R.id.auto_verifying_layout);
        auto_verify_title = (TextView) findViewById(R.id.auto_verify_title);
        countdonwn_time = (TextView) findViewById(R.id.countdonwn_time);
        not_this_device = (Button) findViewById(R.id.not_this_device);
        not_this_device.setOnClickListener(this);

        not_received_otp = (FrameLayout) findViewById(R.id.not_received_otp);
        not_received_otp.setOnClickListener(this);

        send_otp_to_email = (FrameLayout) findViewById(R.id.send_otp_to_email);
        send_otp_to_email.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        inputMethodManager.hideSoftInputFromWindow(phone.getWindowToken(), 0);    // Hides Key Board After Item Select..
        inputMethodManager.hideSoftInputFromWindow(otp.getWindowToken(), 0);    // Hides Key Board After Item Select..

        if ( view == register ){

            validationStatus.setText("");
            phone.checkError("*Enter Phone number");
            mobileNumber = phone.getText();

            if ( CommonMethods.isValidPhoneNumber(mobileNumber) ){
                register.setEnabled(false);
                validatingIndicator.setVisibility(View.VISIBLE);

                validateNumber(mobileNumber);

            }else {
                phone.showError("*Enter a valid phone number (10 digits)");
            }
        }

        else if ( view == not_received_otp ){

            if ( autoVerify == true ){
                register.setEnabled(true);
                validatingIndicator.setVisibility(View.GONE);
                register_layout.setVisibility(View.VISIBLE);
                auto_verifying_layout.setVisibility(View.GONE);
                viewSwitcher.setDisplayedChild(0);
            }else{
                showLoadingDialog("Resending OTP, wait..");
            }

            mobileNumber = phone.getText();
            validateNumber(mobileNumber);
        }

        else  if ( view == send_otp_to_email ){
            Intent emailRegisterAct = new Intent(context, RegisterWithEmailActivity.class);
            startActivityForResult(emailRegisterAct, Constants.REGISTER_ACTIVITY_CODE);
        }

        else if ( view == not_this_device ){
            autoVerify = false;
            verficationStatus.setText("");
            viewSwitcher.setDisplayedChild(1);
        }

        else if ( view == verify ){
            verficationStatus.setText("");
            otp.checkError("*Enter OTP sent to you");
            String oOTP = otp.getText();

            if (oOTP.length() > 0) {
                verifyNumber(responseValidateMobile.getSmsID(), oOTP);
            }
        }
    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {

        try{
            if ( autoVerify == true ) {
                String otpService = receivedData.getStringExtra("otpService");
                String otpCode = receivedData.getStringExtra("otp");

                otp.setText(otpCode);
                verify.performClick();
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    public void autoVerfityNumber(final ResponseValidateMobile responseValidateMobile){

        this.responseValidateMobile = responseValidateMobile;

        auto_verify_title.setText("We'll send an SMS to " + phone.getText() + ". If this device has the number, we'll auto verify it.");
        inputMethodManager.hideSoftInputFromWindow(phone.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        register_layout.setVisibility(View.GONE);
        auto_verifying_layout.setVisibility(View.VISIBLE);                        // Showing AutoVerifying Dialog.

        countdonwn_time.setText(DateMethods.getTimeFromMilliSeconds(countDownTimeLimit));
        new CountDownTimer(countDownTimeLimit, 1000) {                                           // Staring 40sec Duration for AutoVerfication.
            public void onTick(long millisUntilFinished) {
                String cTime = "( "+((millisUntilFinished)/60000)+" min "+((millisUntilFinished%60000)/1000)+" sec )";
                countdonwn_time.setText(cTime);
            }
            public void onFinish() {
                if ( autoVerify ) {
                    verficationStatus.setText("Unable to your number automatically, Try manually by entering the OTP sent to you...");
                    viewSwitcher.setDisplayedChild(1);
                }
            }
        }.start();

    }

    private void validateNumber(String mobileNumber){

        getBackendAPIs().getAuthenticationAPI().validatePhoneNumber(mobileNumber, new AuthenticationAPI.MobileValidationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, ResponseValidateMobile validationDetails) {
                closeLoadingDialog();
                if (status == true) {
                    responseValidateMobile = validationDetails;
                    if (autoVerify == true) {
                        autoVerfityNumber(validationDetails);
                    } else {
                        verficationStatus.setText("");
                        viewSwitcher.setDisplayedChild(1);
                    }
                } else {
                    validationStatus.setText("Something went wrong while sending otp , try again!");
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            validationStatus.setText("No Internet connection!");
                        } else {
                            validationStatus.setText("Not able to connect to Server. \nPlease try again after a while");
                        }
                    }

                    register.setEnabled(true);
                    validatingIndicator.setVisibility(View.GONE);
                }
            }
        });
    }

    public void verifyNumber(String smsID, String otp){         // For Manually Verifying by Entering OTP...

        verify.setEnabled(false);
        verifyingIndicator.setVisibility(View.VISIBLE);

        getBackendAPIs().getAuthenticationAPI().verifyPhoneNumber(smsID, otp, new AuthenticationAPI.MobileVerificationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, ResponseVerifyMobile responseVerifyMobile) {

                if (status == true) {
                    getBuyerDetails(responseVerifyMobile.getToken());
                } else {
                    verficationStatus.setText("Verification failure, check OTP & try again.");
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            verficationStatus.setText("No Internet connection!");
                        } else {
                            verficationStatus.setText("Verification failure. \nPlease try again after a while");
                        }
                    }
                    verify.setEnabled(true);
                    verifyingIndicator.setVisibility(View.GONE);
                    viewSwitcher.setDisplayedChild(1);
                }
            }
        });

    }

    public void getBuyerDetails(final String token){

        getBackendAPIs().getAuthenticationAPI().getBuyerDetails(token, new AuthenticationAPI.BuyerDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, BuyerDetails buyerDetails) {

                if (status == true) {

                    Log.d(Constants.LOG_TAG, "Mobile Verification Completed, For Mobile = " + buyerDetails.getMobile());

                    Intent verificationIntent = new Intent();
                    verificationIntent.putExtra("token", token);
                    verificationIntent.putExtra("mobileNumber", mobileNumber);
                    verificationIntent.putExtra("buyerDetails", new Gson().toJson(buyerDetails));
                    setResult(RESULT_OK, verificationIntent);
                    finish();
                } else {
                    verficationStatus.setText("Something went wrong while getting details in verfication, try again.");
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            verficationStatus.setText("No Internet connection!");
                        } else {
                            verficationStatus.setText("Not able to connect to Server. \nPlease try again after a while");
                        }
                    }

                    verify.setEnabled(true);
                    verifyingIndicator.setVisibility(View.GONE);
                    viewSwitcher.setDisplayedChild(1);
                }
            }
        });

    }

    //=========================================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ( requestCode == Constants.REGISTER_ACTIVITY_CODE ) {      // If It is from Register Activity
            if ( resultCode == RESULT_OK ) {
                setResult(Constants.REGISTER_ACTIVITY_CODE, data);
                finish();
            }else {
                onBackPressed();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        inputMethodManager.hideSoftInputFromWindow(phone.getWindowToken(), 0);    // Hides Key Board After Item Select..
        inputMethodManager.hideSoftInputFromWindow(otp.getWindowToken(), 0);    // Hides Key Board After Item Select..
        onBackPressed();
        return true;
    }
}
