package in.gocibo.foodie.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vMapFragment;
import com.aleena.common.widgets.vMapWrapperLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.rey.material.widget.ProgressView;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.FoodItemsAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.fragments.SideBarFragment;
import in.gocibo.foodie.interfaces.RegistrationCallback;
import in.gocibo.foodie.methods.ShowCaseSequenceMethods;
import in.gocibo.foodie.models.CategoryDetails;


public class MapLocationSelectionActivity extends GociboNavigationDrawerActivity implements View.OnClickListener {

    boolean isForAddFavourite;

    SideBarFragment sideBarFragment;

    FrameLayout deliverFAB;
    ImageButton deliverHere;
    LinearLayout addFavourite;
    ImageView addFavouriteClickIcon;

    ProgressView fetching_name_indicator;

    GoogleMap map;
    ImageButton locateMe;
    boolean fetchingLoc;

    LatLng selectedLocationLatLng;
    PlaceDetails selectedLocationDetails;

    FrameLayout searchLocation;
    TextView deliverLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_map_location_selection);

        isForAddFavourite = getIntent().getBooleanExtra("isForAddFavourite", false);

        // IF LAUNCHED FOR RESULT LIKE ADDING_FAVOURITE/DELIVERY_LCOATION_SELECTION
        if (getCallingActivity() != null) {
            disableNavigationDrawer();
        }
        // SHOWING SIDE IF LAUNCHED FROM LAUNCH ACTIVITY
        else {
            sideBarFragment = new SideBarFragment();
            sideBarFragment.setCallback(new SideBarFragment.SideBarCallback() {
                @Override
                public void onItemSelected(int position) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                }

                @Override
                public void onDeliverLocationChange(PlaceDetails deliverLocationDetails) {

                }
            });
            getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();
        }

        if (isForAddFavourite) {
            actionBar.setTitle(getIntent().getStringExtra("activityTitle"));
            if (actionBar.getTitle() == null || actionBar.getTitle().length() == 0) {
                actionBar.setTitle("Add/Edit Favourite");
            }
        }

        //.........................

        final ShowCaseSequenceMethods ShowCaseSequenceMethods = new ShowCaseSequenceMethods(this, "LOACTION_ACTIVITY_SHOWCASE");

        fetching_name_indicator = findViewById(R.id.fetching_name_indicator);

        deliverFAB = findViewById(R.id.deliverFAB);
        deliverHere = findViewById(R.id.deliverHere);
        deliverHere.startAnimation(fabEnterAnim);
        deliverHere.setOnClickListener(this);
        ShowCaseSequenceMethods.addShowCaseView(deliverHere, "Press here to confirm your delivery location.");

        addFavouriteClickIcon = findViewById(R.id.addFavouriteClickIcon);
        addFavourite = findViewById(R.id.addFavourite);
        addFavourite.setOnClickListener(this);
        ShowCaseSequenceMethods.addShowCaseView(addFavourite, "Press here to view your address book.");
        if (isForAddFavourite) {
            addFavourite.setVisibility(View.GONE);
            deliverHere.setImageResource(R.drawable.ic_add_white);
        }

        locateMe = findViewById(R.id.locateMe);
        locateMe.setOnClickListener(this);

        searchLocation = findViewById(R.id.searchLocation);
        searchLocation.setOnClickListener(this);
        deliverLocation = findViewById(R.id.deliverLocation);

        //map=((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
        final vMapFragment vMapFragment = ((vMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        vMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                //map = vMapFragment.getMap();
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                map.setMyLocationEnabled(true);

                vMapFragment.setOnDragListener(new vMapWrapperLayout.OnDragListener() {
                    @Override
                    public void onDrag(MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                            deliverLocation.setText("release map to set place..");
                        } else {
                            getPlaceName(map.getCameraPosition().target);       // on Completed Moving Map..
                        }
                    }
                });

                ShowCaseSequenceMethods.startShowCase();

                //...............................................

                try {
                    AddressBookItemDetails deliveryLocation = localStorageData.getDeliveryLocation();
                    if (deliveryLocation != null) {
                        selectedLocationLatLng = deliveryLocation.getLatLng();
                        selectedLocationDetails = new PlaceDetails(deliveryLocation);
                    } else {
                        String current_location_details = getIntent().getStringExtra("current_location_details");
                        selectedLocationDetails = new Gson().fromJson(current_location_details, PlaceDetails.class);
                    }

                    // IF SELECTED/CURRENT LOCATION IS NOT NULL
                    if (selectedLocationDetails != null) {
                        moveCamera(selectedLocationLatLng, 500, false);         // Move & Animate Camera with adjustAnimateTime.
                        deliverLocation.setText(selectedLocationDetails.getFullAddress());
                    }
                    //IF SELECTED/CURRENT LOCATION IS NOT PRESENT
                    else {
                        fetchCurrentLocation(500, false);          // Fetch Location with cameraMoveAnimate Time, with NoAdjustAnimateTIme..
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    fetchCurrentLocation(500, false);          // Fetch Location with cameraMoveAnimate Time, with NoAdjustAnimateTIme..
                }
            }
        });

    }

    @Override
    public void onClick(View view) {

        if (view == locateMe) {
            checkLocationAccess(new CheckLocatoinAccessCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message) {
                    if (status) {
                        fetchCurrentLocation(2500, true);          // Fetch Location with cameraMoveAnimate Time..
                    } else {
                        showToastMessage("Location access cancelled by user");
                    }
                }
            });
        } else if (view == searchLocation) {
            Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
            searchActivity.putExtra("isForActivityResult", true);
            startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);
        } else if (view == addFavourite) {

            addFavouriteClickIcon.setVisibility(View.VISIBLE);
            new CountDownTimer(150, 50) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    addFavouriteClickIcon.setVisibility(View.GONE);

                    if (localStorageData.getSessionFlag() == true) {
                        Intent addressBookAct = new Intent(context, AddressBookActivity.class);
                        addressBookAct.putExtra("favourite_location_json", new Gson().toJson(selectedLocationDetails));
                        startActivityForResult(addressBookAct, Constants.ADDRESS_BOOK_ACTIVITY_CODE);
                    } else {
                        goToRegistration(new RegistrationCallback() {
                            @Override
                            public void onComplete(boolean status, String token, in.gocibo.foodie.models.BuyerDetails buyerDetails) {
                                if (status) {
                                    Intent addressBookAct = new Intent(context, AddressBookActivity.class);
                                    addressBookAct.putExtra("favourite_location_json", new Gson().toJson(selectedLocationDetails));
                                    startActivityForResult(addressBookAct, Constants.ADDRESS_BOOK_ACTIVITY_CODE);
                                }
                            }
                        });
                    }
                }
            }.start();
        } else if (view == deliverHere) {

            if (selectedLocationDetails != null) {

                if (isForAddFavourite) {
                    String deliver_location_json = new Gson().toJson(selectedLocationDetails);
                    Intent placeData = new Intent();
                    placeData.putExtra("deliver_location_json", deliver_location_json);
                    setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, placeData);
                    finish();
                } else {
                    checkAvailabilityAtLocationAndProceedToHome(new AddressBookItemDetails(selectedLocationDetails));
                }
            } else {
                showSnackbarMessage(deliverFAB, "Deliver location not available, Please enter it to continue.", "ENTER", new ActionCallback() {
                    @Override
                    public void onAction() {
                        Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
                        searchActivity.putExtra("isForActivityResult", true);
                        startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);
                    }
                });
            }
        }
    }

    //-------------

    public void fetchCurrentLocation(final int animateTime, final boolean adjustAnimateTime) {

        checkLocationAccess(new CheckLocatoinAccessCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {

                if (status) {

                    showLoadingDialog(true, "Fetching your current location, please wait..");
                    new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                        @Override
                        public void onComplete(Location location) {

                            closeLoadingDialog();

                            if (location != null) {
                                selectedLocationLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                                moveCamera(selectedLocationLatLng, animateTime, adjustAnimateTime);         // Move & Animate Camera with adjustAnimateTime..
                                getPlaceName(new LatLng(location.getLatitude(), location.getLongitude()));

                            } else {
                                showChoiceSelectionDialog(true, "Failure", "Unable to Fetch your current location.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                                    @Override
                                    public void onSelect(int choiceNo, String choiceName) {
                                        if (choiceName.equalsIgnoreCase("YES")) {
                                            fetchCurrentLocation(animateTime, adjustAnimateTime);          // Fetch Location with cameraMoveAnimate Time with adjustAnimateTime..
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    public void getPlaceName(LatLng location) {

        selectedLocationLatLng = location;
        selectedLocationDetails = null;

        deliverLocation.setText("Fetching place name, wait..");
        fetching_name_indicator.setVisibility(View.VISIBLE);
        deliverHere.setEnabled(false);
        getGooglePlacesAPI().getPlaceDetailsByLatLng(selectedLocationLatLng, new GooglePlacesAPI.PlaceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                fetching_name_indicator.setVisibility(View.INVISIBLE);
                deliverHere.setEnabled(true);
                if (status == true) {
                    selectedLocationDetails = placeDetails;
                    deliverLocation.setText(placeDetails.getFullAddress());
                } else {
                    deliverLocation.setText("Error in Fetching Location, try again!");
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            deliverLocation.setText("No Internet connection, Unable to load place details , tryagain!");
                        }
                    }
                }
            }
        });
    }

    //------------

    public void checkAvailabilityAtLocationAndProceedToHome(final AddressBookItemDetails deliveryLocation) {

        showLoadingDialog("Loading, please wait..");
        getBackendAPIs().getFoodItemsAPI().getCategoriesList(deliveryLocation.getLatLng(), new FoodItemsAPI.CategoriesListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, long total, List<CategoryDetails> categoriesList) {

                if (status) {

                    if (total > 0) {
                        //localStorageData.setDeliveryLocation(deliveryLocation);
                        localStorageData.storeCategories(categoriesList);

                        if (getCallingActivity() != null) {
                            String deliver_location_json = new Gson().toJson(selectedLocationDetails);
                            Intent placeData = new Intent();
                            placeData.putExtra("deliver_location_json", deliver_location_json);
                            setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, placeData);
                            finish();
                        } else {
                            Intent homeActivity = new Intent(context, HomeActivity.class);
                            startActivity(homeActivity);
                            finish();
                        }
                    } else {
                        closeLoadingDialog();
                        String msg = "We couldn't fetch you any of our delicious food items. This could be because..."
                                + "\n\n"
                                + "\t 1. Our chefs haven’t uploaded any dishes yet (do check back again)"
                                + "\n\t 2. All our items are past their availability time"
                                + "\n\n"
                                + "We are currently serving at " + localStorageData.getServingAt();
                        showNotificationDialog("Nothing to show!", msg);
                    }
                } else {
                    closeLoadingDialog();
                    showChoiceSelectionDialog(true, "Error!", "Something went wrong while checking availability at selected location.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                checkAvailabilityAtLocationAndProceedToHome(deliveryLocation);
                            }
                        }
                    });
                }
            }
        });
    }

    //---------- UI MEthods

    public void moveCamera(LatLng targetLocationLatLng, int animateTime, boolean adjustAnimateTime) {

        if (adjustAnimateTime == true) {
            boolean contains = map.getProjection().getVisibleRegion().latLngBounds.contains(targetLocationLatLng);      // Check Given Point VIsible in Current View

            if (contains == false) {       // If Not Visible in Current ZoomLevel / View...
                Location currentLocation = new Location("current");
                LatLng curLoc = map.getCameraPosition().target;
                currentLocation.setLatitude(curLoc.latitude);
                currentLocation.setLongitude(curLoc.longitude);

                Location targetLocation = new Location("target");
                targetLocation.setLatitude(targetLocationLatLng.latitude);
                targetLocation.setLongitude(targetLocationLatLng.longitude);

                float distance = currentLocation.distanceTo(targetLocation);
                if (distance < 500) {
                    animateTime = 400;
                } else if (distance < 1000) {
                    animateTime = 800;
                } else if (distance < 5000) {
                    animateTime = 1500;
                } else {
                    animateTime = 2500;
                }
            } else {                       // If Visible in CUrrent ZoomLevel / View....
                animateTime = 200;
            }
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(targetLocationLatLng)      // Sets the center of the map to Mountain View
                .zoom(17)                           // Sets the zoom
                .bearing(0)                         // Sets the orientation of the camera to east
                .tilt(0)                            // Sets the tilt of the camera to 30 degrees
                .build();                           // Creates a CameraPosition from the builder

        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), animateTime, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
            }

            @Override
            public void onCancel() {
            }
        });
    }

    //=============================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == Constants.PLACE_SEARCH_ACTIVITY_CODE && resultCode == Constants.PLACE_SEARCH_ACTIVITY_CODE) {      // If It is from PlaceSearch Activity
                String deliver_location_json = data.getStringExtra("deliver_location_json");
                PlaceDetails deliverLocationDetails = new Gson().fromJson(deliver_location_json, PlaceDetails.class);

                if (deliverLocationDetails != null) {
                    this.selectedLocationDetails = deliverLocationDetails;
                    selectedLocationLatLng = deliverLocationDetails.getLatLng();
                    deliverLocation.setText(deliverLocationDetails.getFullAddress());
                    moveCamera(deliverLocationDetails.getLatLng(), 1200, false);         // Move & Animate Camera..
                }
            } else if (resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE && resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE) {      // If It is from PlaceSearch Activity
                String address_item_json = data.getStringExtra("address_item_json");
                PlaceDetails deliverLocationDetails = new PlaceDetails(new Gson().fromJson(address_item_json, AddressBookItemDetails.class));

                if (deliverLocationDetails != null) {
                    this.selectedLocationDetails = deliverLocationDetails;
                    selectedLocationLatLng = deliverLocationDetails.getLatLng();
                    deliverLocation.setText(deliverLocationDetails.getFullAddress());
                    moveCamera(deliverLocationDetails.getLatLng(), 1200, false);         // Move & Animate Camera..
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (getCallingActivity() != null) {
            onBackPressed();
        } else {
            if (drawerListener.onOptionsItemSelected(item)) {
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
