package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.HorizontaliFoodItemsAdapter;
import in.gocibo.foodie.backend.ChefsAPI;
import in.gocibo.foodie.models.ChefDetails;
import in.gocibo.foodie.models.FoodItemDetails;

public class ChefDetailsActivity extends GociboToolBarActivity {

    CollapsingToolbarLayout collapsingToolbar;

    FrameLayout default_chef_image_view;
    ImageView chef_image;
    TextView chef_join_date, chef_about, chef_nativity;

    TextView chefItemsTitle;
    LinearLayout chefItemsHolder;
    HorizontaliFoodItemsAdapter chefItemsAdapter;

    ChefDetails chefDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_chef_details);
        hasLoadingView();
        hasBackButton();

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(getString(R.string.title_activity_chef_details).toUpperCase());

        //...........................................................

        default_chef_image_view = (FrameLayout) findViewById(R.id.default_chef_image_view);
        chef_image = (ImageView) findViewById(R.id.chef_image);
        chef_join_date = (TextView) findViewById(R.id.chef_join_date);
        chef_about = (TextView) findViewById(R.id.chef_about);
        chef_nativity = (TextView) findViewById(R.id.chef_nativity);

        chefItemsTitle = (TextView) findViewById(R.id.chefItemsTitle);

        chefItemsHolder = (LinearLayout) findViewById(R.id.chefItemsHolder);

        chefItemsAdapter = new HorizontaliFoodItemsAdapter(context, R.layout.view_food_item_mini, new ArrayList<FoodItemDetails>(), new HorizontaliFoodItemsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                Intent chefItemDetailsAct = new Intent(context, ChefItemDetailsActivity.class);
                chefItemDetailsAct.putExtra("dishID", chefItemsAdapter.getItem(position).getID());
                startActivity(chefItemDetailsAct);
            }
        });

        //------------------------

        getChefDetails();      // Load Chef Details..

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getChefDetails();
    }

    public void getChefDetails(){

        showLoadingIndicator("Loading chef details, wait..");
        getBackendAPIs().getChefsAPI().getChefDetails(getIntent().getStringExtra("chefID"), new ChefsAPI.ChefDetailsCallback()
        {
            @Override
            public void onComplete(boolean status, int statusCode, ChefDetails chefDetails) {
                if (status == true) {
                    displayChefDetails(chefDetails);
                } else {
                    showReloadIndicator(statusCode, "Unable to load chef details, try again!");
                }
            }
        });
    }


    public void displayChefDetails(ChefDetails chefDetails){

        this.chefDetails = chefDetails;

        collapsingToolbar.setTitle(chefDetails.getFullName().toUpperCase());

        Picasso.get()
                .load(chefDetails.getImageURL())
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .fit()
                .centerCrop()
                .tag(context)
                .into(chef_image, new Callback() {
                    @Override
                    public void onSuccess() {}

                    @Override
                    public void onError(Exception e) {
                        default_chef_image_view.setVisibility(View.VISIBLE);

                    }


                });


        chef_join_date.setText("Joined on "+ DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(chefDetails.getJoinDate()), DateConstants.MMM_DD_YYYY));
        if ( chefDetails.getAbout() != null && chefDetails.getAbout().length() > 0 ){
            chef_about.setText(CommonMethods.capitalizeFirstLetter(chefDetails.getAbout()));
        }else{
            chef_about.setText("Yet to interview our chef");
        }

        if ( chefDetails.getNativity() != null && chefDetails.getNativity().length() > 0 ){
            chef_nativity.setText(chefDetails.getNativity());
        }else{
            findViewById(R.id.nativityView).setVisibility(View.GONE);
        }

        getChefItems(0);        // Loading Dishes Made by Chef

        chefItemsTitle.setText("CHEF " + chefDetails.getFirstName().toUpperCase() + "'s COOKBOOK");

        switchToContentPage();

    }

    public void getChefItems(final int from){

        findViewById(R.id.list_loading_indicator).setVisibility(View.VISIBLE);
        findViewById(R.id.list_reload_indicator).setVisibility(View.GONE);

        getBackendAPIs().getChefsAPI().getChefDishes(chefDetails.getID(), new ChefsAPI.ChefFoodItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<FoodItemDetails> chefFoodItems) {

                findViewById(R.id.list_loading_indicator).setVisibility(View.GONE);

                if (status == true) {
                    if (chefFoodItems.size() > 0) {
                        chefItemsAdapter.addItems(chefFoodItems);
                        for (int i = 0; i < chefItemsAdapter.getItemsCount(); i++) {
                            chefItemsHolder.addView(chefItemsAdapter.getView(i, null, null));
                        }
                    } else {
                        findViewById(R.id.no_chef_dishes_info).setVisibility(View.VISIBLE);
                    }
                } else {
                    findViewById(R.id.list_reload_indicator).setVisibility(View.VISIBLE);
                    findViewById(R.id.list_reload_indicator).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getChefItems(from);
                        }
                    });
                }
            }
        });
    }

    //.......................

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }
}
