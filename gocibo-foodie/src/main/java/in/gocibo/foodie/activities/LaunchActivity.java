package in.gocibo.foodie.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.rey.material.widget.FloatingActionButton;
import com.rey.material.widget.ProgressView;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.CommonAPI;
import in.gocibo.foodie.backend.FoodItemsAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.OrdersFeedbackDatabase;
import in.gocibo.foodie.models.BootStrapDetails;
import in.gocibo.foodie.models.CategoryDetails;
import in.gocibo.foodie.pushnotifications.GCMRegistration;


public class LaunchActivity extends GociboToolBarActivity{

    Location currentLocation;
    PlaceDetails currentLocationDetails;

    LinearLayout loading_layout;
    ProgressView loading_indicator;
    FloatingActionButton reload_indicator;
    TextView loading_message;

    OrdersFeedbackDatabase ordersFeedbackDatabase;

    BootStrapDetails bootStrapDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        Log.d(Constants.LOG_TAG, "AppMode = " + getResources().getString(R.string.app_mode));
        //Log.d(Constants.LOG_TAG, "Buyer ID = " + localStorageData.getBuyerID());
        Log.d(Constants.LOG_TAG, "AccessToken = " + localStorageData.getAccessToken());
        Log.d(Constants.LOG_TAG, "GCMRegID = " + localStorageData.getGCMRegistrationID());
        Log.d(Constants.LOG_TAG, "FireBase AccessToken = " + localStorageData.getFireBaseToken());

        localStorageData.setAppCurrentVersion();
        localStorageData.setDeviceID(getDeviceID());
        localStorageData.storeCategories(new ArrayList<CategoryDetails>());

        localStorageData.setHomePageLastLoadedTime(DateMethods.getCurrentDateAndTime());

        ordersFeedbackDatabase = new OrdersFeedbackDatabase(context);
        ordersFeedbackDatabase.openDatabase();

        //-------------------------------------------

        loading_layout = (LinearLayout) findViewById(R.id.loading_layout);
        loading_indicator = (ProgressView) findViewById(R.id.loading_indicator);
        loading_message = (TextView) findViewById(R.id.loading_message);
        reload_indicator = (FloatingActionButton) findViewById(R.id.reload_indicator);
        reload_indicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getBootStrapDetails();
            }
        });

        //...........................................

        checkInternetConnection(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status == true) {
                    loading_layout.setVisibility(View.VISIBLE);
                    // IF SESSION PRESENT THEN SKITPPING LOCATION ACCESS CHECKING
                    if ( localStorageData.getSessionFlag() ){
                        checkGCMRegistrationStatusAndProceedToBootstrap();
                    }
                    // IF SESSION NOT PRESENT THEN CHECKING LOCATION ACCESS
                    else{
                        checkLocationAccessAndProceedToBootstrap();
                    }
                } else {
                    loading_layout.setVisibility(View.GONE);
                    //showToastMessage("No Internet connection!");
                }
            }
        });
    }

    public void checkGCMRegistrationStatusAndProceedToBootstrap(){

        if ( localStorageData.getGCMRegistrationID() != null && localStorageData.getGCMRegistrationID().length() > 0 ){

            Log.d(Constants.LOG_TAG, "Already Registered to GCM , RegID = " + localStorageData.getGCMRegistrationID());

            if ( localStorageData.getPushNotificationsRegistratinStatus() ){
                Log.d(Constants.LOG_TAG, "Already Registered For Push Notifications");
            }else{
                Log.d(Constants.LOG_TAG, "Not Registered For Push Notifications");

                if (localStorageData.getSessionFlag() ) {       // REGISTER FOR BUYER PUSH NOTIFICATIONS
                    getBackendAPIs().getCommonAPI().registerBuyerForPushNotifications(new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if (status) {
                                localStorageData.setPushNotificationsRegistratinStatus(true);
                            }else{
                                localStorageData.setPushNotificationsRegistratinStatus(false);
                            }
                        }
                    });
                }
                else {                                          // REGISTER FOR PUSH NOTIFICATIONS
                    getBackendAPIs().getCommonAPI().registerForPushNotifications(new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if (status) {
                                localStorageData.setPushNotificationsRegistratinStatus(true);
                            }else{
                                localStorageData.setPushNotificationsRegistratinStatus(false);
                            }
                        }
                    });    // Registering TO Server For Push Notifications
                }
            }
        }
        else {      // IF NOT REGISTERED TO GCM
            Log.d(Constants.LOG_TAG, "Not Registered to GCM");
            new GCMRegistration(context).registerToGCM(new GCMRegistration.RegisterToGCMCallback() {
                @Override
                public void onComplete(boolean status, String message, String gcmRegistrationID) {
                    if (status) {
                        localStorageData.storeGCMRegistrationID(gcmRegistrationID);
                        getBackendAPIs().getCommonAPI().registerForPushNotifications(new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    localStorageData.setPushNotificationsRegistratinStatus(true);
                                }else{
                                    localStorageData.setPushNotificationsRegistratinStatus(false);
                                }
                            }
                        });    // Registering TO Server For Push Notifications
                    }
                }
            });
        }

        getBootStrapDetails();  // Go TO BOOTSTRAP
    }

    public void checkLocationAccessAndProceedToBootstrap(){

        checkLocationAccess(new CheckLocatoinAccessCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {

                Log.d(Constants.LOG_TAG, "LOCATION_SETTINGS : " + message);

                if (status) {
                    new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                        @Override
                        public void onComplete(Location location) {
                            Log.d(ConstantsCommonLibrary.LOG_TAG, "Location = " + location);
                            if (location != null) {
                                currentLocation = location;
                                getGooglePlacesAPI().getPlaceDetailsByLatLng(new LatLng(location.getLatitude(), location.getLongitude()), new GooglePlacesAPI.PlaceDetailsCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                                        currentLocationDetails = placeDetails;
                                        Log.d(ConstantsCommonLibrary.LOG_TAG, "PLACE DETAILS = " + status + " , DET = " + currentLocationDetails);
                                        checkGCMRegistrationStatusAndProceedToBootstrap();
                                    }
                                });
                            } else {
                                checkGCMRegistrationStatusAndProceedToBootstrap();
                            }
                        }
                    });
                } else {
                    checkGCMRegistrationStatusAndProceedToBootstrap();
                }
            }
        });
    }

    //---------

    public void getBootStrapDetails(){

        loading_indicator.setVisibility(View.VISIBLE);
        reload_indicator.setVisibility(View.GONE);
        loading_message.setText("TALKING TO SERVER");
        loading_message.setVisibility(View.VISIBLE);

        getBackendAPIs().getCommonAPI().getBootStrap(getAppVersionCode(), new CommonAPI.BootStrapCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final BootStrapDetails boot_strap_details) {

                if (status == true) {
                    bootStrapDetails = boot_strap_details;
                    checkUpdatesAndProceed();
                } else {        // ON ERROR
                    if (statusCode == 503 && message != null && message.length() > 0) { // FOR MAINTAINANCE
                        loading_indicator.setVisibility(View.INVISIBLE);
                        reload_indicator.setVisibility(View.VISIBLE);
                        loading_message.setText(message);
                    } else {
                        showReload(statusCode);
                    }
                }
            }
        });
    }

    public void showReload(int statusCode){

        loading_layout.setVisibility(View.VISIBLE);
        loading_indicator.setVisibility(View.INVISIBLE);
        reload_indicator.setVisibility(View.VISIBLE);
        loading_message.setText("ERROR CONNECTING. PLEASE TRY AGAIN!");
        if ( statusCode == -1 ){
            boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
            if ( isInternetAvailable ){
                loading_message.setText("NOT ABLE TO CONNECT. \nPLEASE TRY AGAIN AFTER A WHILE!");
            }else{
                loading_message.setText("NO INTERNET CONNECTION!");
            }
        }
    }

    //======================

    public void checkUpdatesAndProceed(){

        if ( bootStrapDetails.isUpgradeAvailable() ){
            if ( bootStrapDetails.isCompulsoryUpgrade() ){
                showChoiceSelectionDialog(false, "Update Available!", "This version is no longer compatible.\nPlease update to new version tocontinue.", "EXIT", "UPDATE", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if (choiceName.equalsIgnoreCase("UPDATE")) {
                            CommonMethods.openInPlaystore(context, context.getPackageName());
                            System.exit(0);
                        }else{
                            System.exit(0);
                        }
                    }
                });

            }else{
////                if ( localStorageData.getSkipAppUpdateVersion() != bootStrapDetails.getLatestVersion() ) {
////                    showChoiceSelectionDialog(false, "Update Available!", "New version of app is available.", "NOT NOW", "UPDATE", new ChoiceSelectionCallback() {
////                        @Override
////                        public void onSelect(int choiceNo, String choiceName) {
////                            if (choiceName.equalsIgnoreCase("YES")) {
////                                CommonMethods.openInPlaystore(context, context.getPackageName());
////                                System.exit(0);
////                            } else {
////                                localStorageData.setSkipAppUpdateVersion(bootStrapDetails.getLatestVersion());
////                                useBootstrapAndProceed();
////                            }
////                        }
////                    });
//                }else{
//                    useBootstrapAndProceed();
//                }
            }
        }else{
            useBootstrapAndProceed();
        }
    }

    public void useBootstrapAndProceed() {

        //localStorageData.storeCategories(bootStrapDetails.getCategoriesList());

        localStorageData.setServerTime(DateMethods.getReadableDateFromUTC(bootStrapDetails.getServerTime()));
        localStorageData.setSupportContactNumber(bootStrapDetails.getSupportContactNumber());
        localStorageData.storeServingAt(bootStrapDetails.getServingAt());
        localStorageData.setCartValidTime(bootStrapDetails.getCartValidTime());
        localStorageData.setClosedPageTitle(bootStrapDetails.getClosedPageTitle());
        localStorageData.setClosedPageSubTitle(bootStrapDetails.getClosedPageSubTitle());

        // USER SPECIFIC
        localStorageData.setCartCount(bootStrapDetails.getCartItemsCount());
        localStorageData.setCartRemainingTime(bootStrapDetails.getCartRemainingTime());
        localStorageData.setBuyerDetails(bootStrapDetails.getBuyerDetails());
        localStorageData.setBuyerAllergies(bootStrapDetails.getBuyerDetails().getAllergies());
        localStorageData.setBuyerPendingOrders(bootStrapDetails.getPendingOrders());

        localStorageData.storeBuyerAddressBookList(bootStrapDetails.getAddressBookList());

        // IF NO SESSION (or) SESSION EXPIRED (or) INVALID SESSION
        if (bootStrapDetails.getBuyerDetails() == null || (bootStrapDetails.getBuyerDetails().getMobile() == null && bootStrapDetails.getBuyerDetails().getEmail() == null)) {
            localStorageData.clearBuyerSession();

            localStorageData.setDeliveryLocation(null);

            goToHome();     //  GOING TO HOME

        }
        // IF VALID SESSION
        else{

            if ( bootStrapDetails.getPendingOrders() != null && bootStrapDetails.getPendingOrders().size() > 0 ){
                context.startService(firebaseService);      //  Starting FIREBASE SERVICE FOR PENDING ORDERS UPDATES
            }

            // IF CART_COUNT > 0
            if ( bootStrapDetails.getCartItemsCount() > 0 ){

                context.startService(timedCartService);         // STARTING CART TIMER

                localStorageData.storeCategories(new ArrayList<CategoryDetails>());

                // STORING EXISTED DELIVERY LOCATION FROM BOOTSTRAP
                AddressBookItemDetails deliveryLocation = localStorageData.getDeliveryLocation();

                float distance = -1;
                if ( deliveryLocation != null ){
                    distance = LatLngMethods.getDistanceBetweenLatLng(deliveryLocation.getLatLng(), bootStrapDetails.getCartDeliveryAddressLatLng());
                }

                if ( deliveryLocation.getID().equalsIgnoreCase(bootStrapDetails.getCartDeliveryAddressID()) || (distance >= 0 && distance <= 100 ) ){
                    Log.d(Constants.LOG_TAG, "Distance between cartDeliveryLocation and currentDeliveryLocation is " + distance + " is < 100m , so going to Home Page");
                    deliveryLocation.setFullName(bootStrapDetails.getCartDeliveryAddressFullName());
                    localStorageData.setDeliveryLocation(deliveryLocation);

                    if (bootStrapDetails.getPendingFeedbackOrders() != null && bootStrapDetails.getPendingFeedbackOrders().size() > 0) {
                        goToOrdersPendingFeedbacks();
                    }else {
                        ordersFeedbackDatabase.deleteAllFeedbackItems();    // CLEARING ALL EXISTED STORED FEEDBACKS
                        goToHome();     //  GOING TO HOME
                    }

                }
                else{
                    Log.d(Constants.LOG_TAG, "Distance between cartDeliveryLocation and currentDeliveryLocation is "+distance+" is > 100m or NOT SAME, Fetching CartDeliveryLocation details");
                    getGooglePlacesAPI().getPlaceDetailsByLatLng(bootStrapDetails.getCartDeliveryAddressLatLng(), new GooglePlacesAPI.PlaceDetailsCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                            if (status == true) {
                                placeDetails.setFullName(bootStrapDetails.getCartDeliveryAddressFullName());
                                localStorageData.setDeliveryLocation(new AddressBookItemDetails(placeDetails));
                            } else {
                                localStorageData.setDeliveryLocation(null);
                            }

                            if (bootStrapDetails.getPendingFeedbackOrders() != null && bootStrapDetails.getPendingFeedbackOrders().size() > 0) {
                                goToOrdersPendingFeedbacks();
                            }else {
                                ordersFeedbackDatabase.deleteAllFeedbackItems();    // CLEARING ALL EXISTED STORED FEEDBACKS
                                goToHome();     //  GOING TO HOME
                            }
                        }
                    });
                }
            }
            // IF NO ITEMS IN CART
            else{
                // RESETTING DELIVERY LOCATION IF NO ITEMS IN CART
                localStorageData.setDeliveryLocation(null);

                if (bootStrapDetails.getPendingFeedbackOrders() != null && bootStrapDetails.getPendingFeedbackOrders().size() > 0) {
                    goToOrdersPendingFeedbacks();
                }else {
                    ordersFeedbackDatabase.deleteAllFeedbackItems();    // CLEARING ALL EXISTED STORED FEEDBACKS
                    goToHome();     //  GOING TO HOME
                }
            }
        }
    }

    public void goToOrdersPendingFeedbacks(){

        ArrayList<String> pendingFeedbackOrders = new ArrayList<String>();
        for (int i = 0; i < bootStrapDetails.getPendingFeedbackOrders().size(); i++) {
            pendingFeedbackOrders.add(new Gson().toJson(bootStrapDetails.getPendingFeedbackOrders().get(i)));
        }
        Intent pendingFeedbackOrdersAct = new Intent(context, PendingFeedbacksActivity.class);
        pendingFeedbackOrdersAct.putStringArrayListExtra("pendingFeedbackOrders", pendingFeedbackOrders);
        startActivityForResult(pendingFeedbackOrdersAct, Constants.PENDING_FEEDBACK_ORDERS_ACTIVITY_CODE);
    }

    public void goToHome(){

        if ( localStorageData.getTCFlag() ) {                   // IF Seen Terms&Conditions or Accepeted TC..
            if ( bootStrapDetails.isOpen() ){
                //GOING TO HOME PAGE
                if ( localStorageData.getDeliveryLocation() != null ){
                    Intent homeAct = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(homeAct);
                    finish();
                }
                // GOING TO LOCATION PAGE
                else{
                    goToHomeCheckAddressBookAndConfirmLocation();
                }
            }else{
                Intent closedAct = new Intent(context, ClosedActivity.class);
                startActivity(closedAct);
                finish();
            }
        }
        else{
            Intent tcActivity = new Intent(context, TermsAndConditionsActivity.class);
            startActivityForResult(tcActivity, Constants.TERMS_AND_CONDITIONS_ACTIVITY_CODE);
        }

        //ordersFeedbackDatabase.closeDatabase();
    }

    public void goToHomeCheckAddressBookAndConfirmLocation(){

        // USER SESSION PRESENT
        if ( localStorageData.getSessionFlag() ){

            // USER HAS SAVED LOCATIONS >> DIRECTLY GOING TO LOCATION SELECTOR PAGE
            if ( bootStrapDetails.getAddressBookList() != null && bootStrapDetails.getAddressBookList().size() > 0 ){

                /*ArrayList<String> addressBookLocations = new ArrayList<String>();
                for (int i = 0; i < bootStrapDetails.getAddressBookList().size(); i++) {
                    addressBookLocations.add(new Gson().toJson(bootStrapDetails.getAddressBookList().get(i)));
                }*/

                Intent locationSelctionAct = new Intent(context, LocationSelectionActivity.class);
                //locationSelctionAct.putStringArrayListExtra("addressBookLocations", addressBookLocations);
                startActivity(locationSelctionAct);
                finish();
            }
            // USER HAS NO SAVED LCOATIONS
            else{
                checkLocationAccess(new CheckLocatoinAccessCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        Log.d(Constants.LOG_TAG, "LOCATION_SETTINGS : " + message);

                        if (status) {
                            new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                                @Override
                                public void onComplete(Location location) {
                                    Log.d(ConstantsCommonLibrary.LOG_TAG, "Location = " + location);
                                    if (location != null) {
                                        currentLocation = location;
                                        getGooglePlacesAPI().getPlaceDetailsByLatLng(new LatLng(location.getLatitude(), location.getLongitude()), new GooglePlacesAPI.PlaceDetailsCallback() {
                                            @Override
                                            public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                                                currentLocationDetails = placeDetails;
                                                Log.d(ConstantsCommonLibrary.LOG_TAG, "PLACE DETAILS = " + status + " , DET = " + currentLocationDetails);
                                                goToHomeConfirmLocation();
                                            }
                                        });
                                    } else {
                                        goToHomeConfirmLocation();
                                    }
                                }
                            });
                        } else {
                            goToHomeConfirmLocation();
                        }
                    }
                });
            }
        }
        // NO USER SESSION PRESENT
        else{
            goToHomeConfirmLocation();
        }
    }

    public void goToHomeConfirmLocation(){

        loading_indicator.setVisibility(View.VISIBLE);
        loading_message.setVisibility(View.VISIBLE);
        loading_message.setText("CHECKING YOUR LOCATION");

        // IF CURRENT LOCATION ACCESS ENABLED
        if ( isLocationAccessEnabled() ){

            // IF CURRENT LOCATION FOUND
            if ( currentLocation != null ){

                // IF CURRENT LOCATION DETAILS FETCHED
                if ( currentLocationDetails != null ){

                    loading_indicator.setVisibility(View.INVISIBLE);
                    loading_message.setVisibility(View.INVISIBLE);

                    showChoiceSelectionDialog(false, "Confirm your location", currentLocationDetails.getFullAddress(), "CHANGE", "DONE", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {

                            // IF USER CONFIRMS HIS LOCATION
                            if (choiceName.equalsIgnoreCase("DONE")) {
                                checkAvailabilityAtLocationAndProceedToHome(new AddressBookItemDetails(currentLocationDetails));
                            }
                            // IF USER WANT TO ENTER MANUALLY
                            else {
                                Intent mapLocationSelectionAct = new Intent(context, MapLocationSelectionActivity.class);
                                mapLocationSelectionAct.putExtra("current_location_details", new Gson().toJson(currentLocationDetails));
                                //startActivityForResult(locationAct, Constants.PLACE_SEARCH_ACTIVITY_CODE);
                                startActivity(mapLocationSelectionAct);
                                finish();
                            }
                        }
                    });
                }
                // IF CURRENT LOCATION DETAILS NOT FETCHED
                else{

                    loading_indicator.setVisibility(View.INVISIBLE);
                    loading_message.setVisibility(View.INVISIBLE);
                    
                    showChoiceSelectionDialog(false, "Failed!", "Something went wrong while getting your current place details.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {

                            // IF TO WANT TRY AGAIN TO FETCH CURRENT LOCATION DETAILS..
                            if (choiceName.equalsIgnoreCase("YES")) {

                                getGooglePlacesAPI().getPlaceDetailsByLatLng(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), new GooglePlacesAPI.PlaceDetailsCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                                        if (status) {
                                            currentLocationDetails = placeDetails;
                                        }
                                        goToHomeConfirmLocation();
                                    }
                                });
                            }
                            // IF DON'T WANT TRY AGAIN TO FETCH CURRENT LOCATION DETAILS..
                            else {
                                Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
                                startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);
                                finish();
                            }
                        }
                    });

                }
            }
            // IF CURRENT LOCATION NOT FOUND
            else{

                showChoiceSelectionDialog(false, "Failed!", "Something went wrong while getting your current location.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {

                        // IF TO WANT TRY AGAIN TO FETCH CURRENT LOCATION..
                        if (choiceName.equalsIgnoreCase("YES")) {

                            new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                                @Override
                                public void onComplete(Location location) {
                                    Log.d(ConstantsCommonLibrary.LOG_TAG, "Location = " + location);
                                    if (location != null) {
                                        currentLocation = location;
                                    }
                                    goToHomeConfirmLocation();
                                }
                            });
                        }
                        // IF DON'T WANT TRY AGAIN TO FETCH CURRENT LOCATION..
                        else {
                            Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
                            startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);
                            finish();
                        }
                    }
                });
            }
        }
        // IF CURRENT LOCATION ACCESS DISABLED
        else{
            Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
            startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);
            finish();
        }
    }

    public void checkAvailabilityAtLocationAndProceedToHome(final AddressBookItemDetails deliveryLocation){

        loading_indicator.setVisibility(View.VISIBLE);
        loading_message.setVisibility(View.VISIBLE);
        loading_message.setText("TALKING TO SERVER");

        getBackendAPIs().getFoodItemsAPI().getCategoriesList(deliveryLocation.getLatLng(), new FoodItemsAPI.CategoriesListCallback() {

            @Override
            public void onComplete(boolean status, int statusCode, String message, long total, List<CategoryDetails> categoriesList) {
                if (status) {
                    if (total > 0) {
                        localStorageData.setDeliveryLocation(deliveryLocation);
                        localStorageData.storeCategories(categoriesList);

                        Intent homeAct = new Intent(context, HomeActivity.class);
                        startActivity(homeAct);
                        finish();
                    } else {
                        String msg = "We couldn't fetch you any of our delicious food items. This could be because..."
                                + "\n\n"
                                + "\t 1. Our chefs haven’t uploaded any dishes yet (do check back again)"
                                + "\n\t 2. All our items are past their availability time"
                                + "\n\n"
                                + "We are currently serving at " + localStorageData.getServingAt();
                        showChoiceSelectionDialog(false, "Nothing to show!", msg, "CHANGE LOCATION", "EXIT", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("EXIT")) {
                                    onBackPressed();
                                } else {
                                    Intent mapLocationSelectionAct = new Intent(context, MapLocationSelectionActivity.class);
                                    mapLocationSelectionAct.putExtra("current_location_details", new Gson().toJson(currentLocationDetails));
                                    //startActivityForResult(locationAct, Constants.PLACE_SEARCH_ACTIVITY_CODE);
                                    startActivity(mapLocationSelectionAct);
                                    finish();
                                }
                            }
                        });
                    }
                } else {
                    showChoiceSelectionDialog(true, "Error!", "Something went wrong while checking availability at selected location.\n\nPlease try again!", "EXIT", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                checkAvailabilityAtLocationAndProceedToHome(deliveryLocation);
                            } else {
                                onBackPressed();
                            }
                        }
                    });
                }
            }
        });
    }

    //=================================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == Constants.TERMS_AND_CONDITIONS_ACTIVITY_CODE ){
                if ( resultCode == Activity.RESULT_OK ) {
                    goToHome();
                }else{
                    onBackPressed();
                }
            }
            else if (requestCode == Constants.PENDING_FEEDBACK_ORDERS_ACTIVITY_CODE ){
                if ( resultCode == Constants.PENDING_FEEDBACK_ORDERS_ACTIVITY_CODE) {
                    goToHome();
                }else{
                    onBackPressed();
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }
}
