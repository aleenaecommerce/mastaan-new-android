package in.gocibo.foodie.activities;

import android.app.Activity;
import android.text.Html;

import com.aleena.common.dialogs.ChoiceSelectionDialog;
import com.aleena.common.dialogs.NumberPickerDialog;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.NumberPickerCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.models.AllergyDetails;
import in.gocibo.foodie.models.CartItemDetails;
import in.gocibo.foodie.models.CategoryDetails;
import in.gocibo.foodie.models.DeliverySlotDetails;
import in.gocibo.foodie.models.FoodItemDetails;
import in.gocibo.foodie.models.IngradientDetails;
import in.gocibo.foodie.models.SlotDetails;

/**
 * Created by venkatesh on 15/12/15.
 */
public class Add_or_Edit_Cart_Item_Flow {

    Activity activity;
    Add_or_Edit_Cart_Item_Flow_Callback callback;

    LocalStorageData localStorageData;
    CommonMethods commonMethods;

    String serverTime;
    CategoryDetails categoryDetails;
    List<IngradientDetails> itemIngradients;
    String foodItemAvailTime;
    String availabilityEndTime;
    String quantityType;
    String slotType;
    double currentAvailability;

    double selectedQuantity;
    String seletedDeliverySlot;

    public interface Add_or_Edit_Cart_Item_Flow_Callback{
        void onComplete(double quantity, String deliverySlot);
    }

    public Add_or_Edit_Cart_Item_Flow(Activity activity){

        this.activity = activity;
        localStorageData = new LocalStorageData(activity);
        commonMethods = new CommonMethods();

        serverTime = DateMethods.getTimeIn24HrFormat(localStorageData.getServerTime());

    }

    public void addFlow(CategoryDetails categoryDetails, FoodItemDetails selectedFoodItemDetails, Add_or_Edit_Cart_Item_Flow_Callback callback) {

        this.callback = callback;

        this.categoryDetails = categoryDetails;
        foodItemAvailTime = DateMethods.getTimeIn24HrFormat(commonMethods.getReadableTimeFromUTC(selectedFoodItemDetails.getAvailableTime()));
        availabilityEndTime = DateMethods.getTimeIn24HrFormat(commonMethods.getReadableTimeFromUTC(selectedFoodItemDetails.getAvailabilityEndTime()));
        itemIngradients = selectedFoodItemDetails.getIngradients();
        quantityType = selectedFoodItemDetails.getQuantityType();
        slotType = selectedFoodItemDetails.getSlotType();
        currentAvailability = selectedFoodItemDetails.getCurrentAvailability();

        checkAllergiesAndProceedToAddCart();        // CHECKING ALLERGIES BEFORE ADDING IN ADD FLOW
    }

    public void editFlow(CategoryDetails categoryDetails, CartItemDetails cartItemDetails, Add_or_Edit_Cart_Item_Flow_Callback callback) {

        this.callback = callback;

        this.categoryDetails = categoryDetails;
        foodItemAvailTime = DateMethods.getTimeIn24HrFormat(commonMethods.getReadableTimeFromUTC(cartItemDetails.getDishAvailableTime()));
        availabilityEndTime = DateMethods.getTimeIn24HrFormat(commonMethods.getReadableTimeFromUTC(cartItemDetails.getDishAvailabilityEndTime()));
        quantityType = cartItemDetails.getDishQuantityType();
        slotType = cartItemDetails.getDishSlotType();
        currentAvailability = cartItemDetails.getDishCurrentAvailability()+ cartItemDetails.getQuantity();

        selectedQuantity = cartItemDetails.getQuantity();
        seletedDeliverySlot = cartItemDetails.getDBY();

        addToCartSelectQuantity();        // DIRECTLY GOING TO SELECT QUANTITY
    }

    public void checkAllergiesAndProceedToAddCart(){

        boolean isContainsMyAllergies = false;
        List<String> containedAllergies = new ArrayList<>();

        if ( itemIngradients != null && localStorageData.getBuyerAllergies() != null ){
            List<AllergyDetails> buyerAllergies = localStorageData.getBuyerAllergies();
            for ( int i=0;i<itemIngradients.size();i++){
                for (int j=0;j<buyerAllergies.size();j++){
                    if ( itemIngradients.get(i).getName().equalsIgnoreCase(buyerAllergies.get(j).getName()) == true ){
                        isContainsMyAllergies = true;
                        containedAllergies.add(buyerAllergies.get(j).getName());
                    }
                }
            }
        }

        if ( isContainsMyAllergies == true ){
            String allergiesMessage = "This dish contains <b>"+commonMethods.getStringFromStringList(containedAllergies)+".</b><br><br>"+"You are allergic to them. Are you sure you want to add it to your food basket?";
            if ( containedAllergies.size() == 1 ){
                allergiesMessage = "This dish contains <b>"+commonMethods.getStringFromStringList(containedAllergies)+".</b><br><br>"+"You are allergic to it. Are you sure you want to add it to your food basket?";
            }

            new ChoiceSelectionDialog(activity).showDialog(false, "Allergies Alert!", Html.fromHtml(allergiesMessage), "NO", "YES", new ChoiceSelectionCallback() {
                @Override
                public void onSelect(int choiceNo, String choiceName) {
                    if (choiceName.equalsIgnoreCase("YES")) {
                        addToCartSelectQuantity();
                    }
                }
            });

        }else{
            addToCartSelectQuantity();
        }
    }

    public void addToCartSelectQuantity(){

        String dialogTitle = "Select no.of servings";
        final List<Double> quantiy = new ArrayList<Double>();
        List<String> quantityStrings = new ArrayList<String>();

        if ( quantityType.equalsIgnoreCase("n")){
            dialogTitle = "Select no.of servings";
            for(int i=1;i<=currentAvailability;i++){
                quantiy.add(i*1.0);
                quantityStrings.add(i+"");
            }
        }
        else if ( quantityType.equalsIgnoreCase("w")){
            dialogTitle = "Select quantity";
            for(double i=0.25;i<=currentAvailability;i=i+0.25){
                quantiy.add(i);
                if ( i == 1 ){
                    quantityStrings.add(commonMethods.getInDecimalFormat(i)+" kg");
                }else{
                    quantityStrings.add(commonMethods.getInDecimalFormat(i)+" kgs");
                }
            }
        }
        else if ( quantityType.equalsIgnoreCase("v")){
            dialogTitle = "Select quantity";
            for(double i=0.25;i<=currentAvailability;i=i+0.25){
                quantiy.add(i);
                quantityStrings.add(commonMethods.getInDecimalFormat(i)+" L");
            }
        }

        if ( quantiy.size() == 1 ){
            addToCartSelectSlot(quantiy.get(0));
        }else{

            int selectedQuantityIndex=0;
            for (int i=0;i<quantiy.size();i++){
                if ( quantiy.get(i) == selectedQuantity ){
                    selectedQuantityIndex = i;
                    break;
                }
            }

            new NumberPickerDialog(activity).showDialog(dialogTitle, quantityStrings, selectedQuantityIndex, 0, quantityStrings.size() - 1, new NumberPickerCallback() {
                @Override
                public void onSelect(int selectedNumber) {
                    addToCartSelectSlot(quantiy.get(selectedNumber));
                }
            });
        }
    }

    //

    public void addToCartSelectSlot(final double quantity){


        if (slotType.equalsIgnoreCase("tb")) {

            SlotDetails belongsSlot = null;
            final List<DeliverySlotDetails> availableSlots = new ArrayList<>();
            final List<String> availableSlotsStrings = new ArrayList<>();

            List<SlotDetails> timebasedSlots = categoryDetails.getTimeBasedSlots();
            for (int i = 0; i < timebasedSlots.size(); i++) {      // FINDING SLOT TO WHICH ITEM BELONGS
                String deliveryHoursStartTime = timebasedSlots.get(i).getDeliveringHours().getStartTime();
                String deliveryHoursEndTime = timebasedSlots.get(i).getDeliveringHours().getEndTime();

                if (DateMethods.compareDates(deliveryHoursStartTime, foodItemAvailTime) >= 0 || DateMethods.compareDates(deliveryHoursEndTime, foodItemAvailTime) >= 0) {
                    belongsSlot = timebasedSlots.get(i);
                    break;
                }
            }

            if ( belongsSlot != null ){
                List<DeliverySlotDetails> allSlots = belongsSlot.getDeliverySlots();
                for (int i = 0; i < allSlots.size(); i++) {
                    DeliverySlotDetails deliverySlotDetails = allSlots.get(i);

                    if ( availabilityEndTime != null && availabilityEndTime.length() > 0 ){
                        if ( DateMethods.compareDates(deliverySlotDetails.getStartTime(), foodItemAvailTime) >= 0
                                && DateMethods.compareDates(availabilityEndTime, deliverySlotDetails.getStartTime()) >= 0 ) {
                            deliverySlotDetails.setDisplayText(DateMethods.getTimeIn12HrFormat(deliverySlotDetails.getStartTime()) + " to " + DateMethods.getTimeIn12HrFormat(deliverySlotDetails.getEndTime()));
                            availableSlots.add(deliverySlotDetails);
                            availableSlotsStrings.add(deliverySlotDetails.getDisplayText());
                        }
                    }
                    else{
                        if (DateMethods.compareDates(deliverySlotDetails.getStartTime(), foodItemAvailTime) >= 0) {
                            deliverySlotDetails.setDisplayText(DateMethods.getTimeIn12HrFormat(deliverySlotDetails.getStartTime()) + " to " + DateMethods.getTimeIn12HrFormat(deliverySlotDetails.getEndTime()));
                            availableSlots.add(deliverySlotDetails);
                            availableSlotsStrings.add(deliverySlotDetails.getDisplayText());
                        }
                    }
                }

                if (availableSlots.size() == 0) {
                    sendCallback(quantity, "immediate");
                } else if (availableSlots.size() == 1) {
                    sendCallback(quantity, availableSlots.get(0).getDisplayText());
                } else {
                    // IF SERVER TIMER >= FOOD ITEM AVAILABLILITY
                    if (DateMethods.compareDates(serverTime, foodItemAvailTime) > 0) {
                        availableSlots.add(0, new DeliverySlotDetails("immediate", "immediate", "immediate", "immediate", "immediate", "immediate", "immediate"));
                        availableSlotsStrings.add(0, "Immediate (1 hour)");
                    }

                    int selectedSlotIndex = 0;
                    if ( seletedDeliverySlot != null && seletedDeliverySlot.length() > 0 ) {
                        for (int i = 0; i < availableSlots.size(); i++) {
                            if (availableSlots.get(i).getDisplayText().equalsIgnoreCase(seletedDeliverySlot)) {
                                selectedSlotIndex = i;
                                break;
                            }
                        }
                    }

                    new NumberPickerDialog(activity).showDialog("Select delivery slot", availableSlotsStrings, selectedSlotIndex, 0, availableSlotsStrings.size() - 1, new NumberPickerCallback() {
                        @Override
                        public void onSelect(int selectedNumber) {
                            sendCallback(quantity, availableSlots.get(selectedNumber).getDisplayText());
                        }
                    });
                }
            }
            else{
                sendCallback(quantity, "immediate");
            }
        }
        else if (slotType.equalsIgnoreCase("ad")) {

            String[] availableSlots = commonMethods.getTimeSlots(serverTime, categoryDetails.getAllDayDeliverySlotsEndTime(), true);
            String[] availableSlotsStrings = availableSlots;

            if (DateMethods.compareDates(foodItemAvailTime, serverTime) > 0) {
                availableSlots = commonMethods.getTimeSlots(foodItemAvailTime, categoryDetails.getAllDayDeliverySlotsEndTime(), true);
                availableSlotsStrings = availableSlots;
            }

            if (availableSlots.length == 0) {
                sendCallback(quantity, "immediate");
            } else if (availableSlots.length == 1) {
                sendCallback(quantity, availableSlots[0]);
            } else {
                if (DateMethods.compareDates(serverTime, foodItemAvailTime) >= 0) {
                    availableSlots = commonMethods.addStringArrays(new String[]{"immediate"}, availableSlots);
                    availableSlotsStrings = commonMethods.addStringArrays(new String[]{"Immediately (1 hour)"}, availableSlotsStrings);
                }

                int selectedSlotIndex = 0;
                if ( seletedDeliverySlot != null && seletedDeliverySlot.length() > 0 ) {
                    for (int i = 0; i < availableSlots.length; i++) {
                        if (availableSlots[i].equalsIgnoreCase(seletedDeliverySlot)) {
                            selectedSlotIndex = i;
                            break;
                        }
                    }
                }
                final String[] availableSlotss = availableSlots;
                new NumberPickerDialog(activity).showDialog("Select delivery slot", availableSlotsStrings, selectedSlotIndex, 0, availableSlots.length - 1, new NumberPickerCallback() {
                    @Override
                    public void onSelect(int selectedNumber) {
                        sendCallback(quantity, availableSlotss[selectedNumber]);
                    }
                });
            }
        }
    }

    public void sendCallback(double quanity, String deliverySlot){
        if ( callback != null ){
            callback.onComplete(quanity, deliverySlot);
        }
    }
}
