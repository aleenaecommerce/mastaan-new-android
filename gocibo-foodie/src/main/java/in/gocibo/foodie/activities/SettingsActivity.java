package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.rey.material.widget.Switch;

import in.gocibo.foodie.R;

public class SettingsActivity extends GociboToolBarActivity implements View.OnClickListener{

    LinearLayout notifications, credits, about;
    LinearLayout logoutView, logout;

    TextView app_version;
    Switch notifications_switch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_settings);

        notifications = (LinearLayout) findViewById(R.id.notifications);
        notifications.setOnClickListener(this);
        credits = (LinearLayout) findViewById(R.id.credits);
        credits.setOnClickListener(this);
        about = (LinearLayout) findViewById(R.id.credits);
        about.setOnClickListener(this);
        app_version = (TextView) findViewById(R.id.app_version);
        app_version.setText("Version "+getAppVersionName());

        logoutView = (LinearLayout) findViewById(R.id.logoutView);
        logout = (LinearLayout) findViewById(R.id.logout);

        if ( localStorageData.getSessionFlag() ){
            logoutView.setVisibility(View.VISIBLE);
            logout.setOnClickListener(this);
        }

        notifications_switch = (Switch) findViewById(R.id.notifications_switch);

        if ( localStorageData.getSessionFlag() == true ){
            notifications.setVisibility(View.VISIBLE);
            notifications_switch.setChecked(localStorageData.getNotificationsFlag());

            notifications_switch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(Switch aSwitch, boolean isChecked) {
                    localStorageData.setNotificationsFlag(isChecked);
                }
            });
        }

    }

    @Override
    public void onClick(View view) {

        if ( view == notifications ){
            if ( notifications_switch.isChecked() ){
                notifications_switch.setChecked(false);
            }else{
                notifications_switch.setChecked(true);
            }
        }
        else if ( view == credits ){
            Intent creditsAct = new Intent(context, CreditsActivity.class);
            startActivity(creditsAct);
        }
        else if ( view == about ){

        }

        else if ( view == logout ){
            if ( localStorageData.getCartCount() > 0 ) {
                showChoiceSelectionDialog(false, "Alert!", "Items you added to your food basket will be removed if you log out. Continue?", "YES", "NO", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        closeLoadingDialog();
                        if (choiceName.equalsIgnoreCase("YES")) {
                            logOut();
                        }
                    }
                });
            }else{
                logOut();
            }
        }

    }

    public void logOut(){

        showLoadingDialog("Logging out, wait..");
        getBackendAPIs().getAuthenticationAPI().confirmLogout(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status == true ){
                    clearUserSession("Logged out successfully.");
                    //updateSidebarBasedOnSessioin();          // Updating Sidebar
                }
                else{
                    clearUserSession("Logged out successfully.");
                    showSnackbarMessage("Unable to Logout!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            logOut();
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
