package in.gocibo.foodie.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vWrappingLinearLayoutManager;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.AddressBookAdapter;
import in.gocibo.foodie.backend.AddressBookAPI;
import in.gocibo.foodie.backend.FoodItemsAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.fragments.SideBarFragment;
import in.gocibo.foodie.models.CategoryDetails;

public class LocationSelectionActivity extends GociboNavigationDrawerActivity implements View.OnClickListener{

    String searchQuery;
    MenuItem searchViewMenuITem;
    SearchView searchView;

    boolean isForActivityResult;
    SideBarFragment sideBarFragment;

    vRecyclerView addressBookHolder;
    AddressBookAdapter addressBookAdapter;

    Button selectLocation;

    int selectedItemIndex;

    List<AddressBookItemDetails> originalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selection);
        hasLoadingView();

        sideBarFragment = new SideBarFragment();
        sideBarFragment.setCallback(new SideBarFragment.SideBarCallback() {
            @Override
            public void onItemSelected(int position) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }

            @Override
            public void onDeliverLocationChange(PlaceDetails deliverLocationDetails) {

            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();

        isForActivityResult = getIntent().getBooleanExtra("isForActivityResult", false);

        if ( isForActivityResult ) {
            disableNavigationDrawer();
        }

        registerActivityReceiver(Constants.LOCATION_SELECTION_RECEIVER);

        //---------

        selectLocation = (Button) findViewById(R.id.selectLocation);
        selectLocation.setOnClickListener(this);

        addressBookHolder = (vRecyclerView) findViewById(R.id.addressBookHolder);
        setupHolder();

        //--------

        showLoadingIndicator("Loading address book, wait...");


    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        getFavourites();    // GetFavourites
    }

    @Override
    public void onClick(View view) {

        if ( view == selectLocation ){
            Intent mapLocationSelectionAct = new Intent(context, MapLocationSelectionActivity.class);
            mapLocationSelectionAct.putExtra("isForActivityResult", true);
            startActivityForResult(mapLocationSelectionAct, Constants.LOCATION_SELECTOR_ACTIVITY_CODE);
        }
    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);
        try{
            String type = receivedData.getStringExtra("type");
            if ( type == null ){ type = ""; }

            if ( type.equalsIgnoreCase("update_address_book") ){
                getFavourites();
            }
        }catch (Exception e){e.printStackTrace();}
    }

    public void setupHolder(){
        addressBookHolder.setLayoutManager(new vWrappingLinearLayoutManager(context));//new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        addressBookHolder.setHasFixedSize(false);
        addressBookHolder.setNestedScrollingEnabled(false);
        addressBookHolder.setItemAnimator(new DefaultItemAnimator());

        addressBookAdapter = new AddressBookAdapter(context, R.layout.view_address_book_item2, new ArrayList<AddressBookItemDetails>(), new AddressBookAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                checkAvailabilityAtLocationAndProceedToHome(addressBookAdapter.getItem(position));
            }

            @Override
            public void onItemMenuClick(final int position, final View view) {
                selectedItemIndex = position;

                PopupMenu popup = new PopupMenu(context, view);;//new PopupMenu(getActivity().getSupportActionBar().getThemedContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_address_item, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        if ( menuItem.getItemId() == R.id.action_edit ) {
                            Intent addFavActivity = new Intent(context, FavouriteActivity.class);
                            addFavActivity.putExtra("mode", "Edit");
                            addFavActivity.putExtra("address_book_item_json", new Gson().toJson(addressBookAdapter.getItem(position)));
                            startActivityForResult(addFavActivity, Constants.FAVOURITE_ACTIVITY_CODE);
                        }
                        else if ( menuItem.getItemId() == R.id.action_delete ) {
                            deleteFavourite(position, addressBookAdapter.getItem(position));
                        }
                        return true;
                    }
                });
            }
        });
        addressBookHolder.setAdapter(addressBookAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getFavourites();
    }

    public void getFavourites(){

        showLoadingIndicator("Loading your addresses...");

        List<AddressBookItemDetails> addressBookList = localStorageData.getBuyerAddressBookList();
        if ( addressBookList != null ){
            // REMOVING ITEMS WITHOUD IDs
            List<AddressBookItemDetails> addressBookListItems = new ArrayList<>();
            for (int i=0;i<addressBookList.size();i++){
                if ( addressBookList.get(i).getID() != null && addressBookList.get(i).getID().length() >0 ) {
                    addressBookListItems.add(addressBookList.get(i));
                }
            }
            addressBookAdapter.setItems(addressBookListItems);
            originalList = addressBookAdapter.getAllItems();
            if ( addressBookAdapter.getItemCount() >= 10 ){ showSearchView();   }
            else{ hideSearchView(); }

            switchToContentPage();
        }
        else{
            getBackendAPIs().getAddressBookAPI().getFavourites(new AddressBookAPI.FavouritesCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, List<AddressBookItemDetails> addressBookItems) {
                    if (status == true) {
                        localStorageData.storeBuyerAddressBookList(addressBookItems);
                        addressBookAdapter.setItems(addressBookItems);
                        if ( addressBookAdapter.getItemCount() >= 10 ){ showSearchView();   }
                        else{ hideSearchView(); }

                        switchToContentPage();
                    } else {
                        checkSessionValidity(statusCode);
                        showReloadIndicator(statusCode, "Unable to load your favourites, try again!");
                    }
                }
            });
        }
    }

    public void deleteFavourite(final int position, final AddressBookItemDetails  addressBookItemDetails){

        showLoadingDialog("Deleting favourite, please wait...");
        getBackendAPIs().getAddressBookAPI().deleteAddress(addressBookItemDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {

                closeLoadingDialog();

                if (status == true) {
                    //REMOVING FROM ADAPTER
                    addressBookAdapter.deleteItem(position);
                    //REMOVING FROM ORIGINAL LIST
                    for (int i=0;i<originalList.size();i++){
                        if ( originalList.get(i).getID().equals(addressBookItemDetails.getID())){
                            originalList.remove(i);
                            break;
                        }
                    }
                    if ( addressBookAdapter.getItemCount() >= 10 ){ showSearchView();   }
                    else{ hideSearchView(); }

                    //REMOVING FROM LOCAL STORAGE
                    List<AddressBookItemDetails> addressBookList = localStorageData.getBuyerAddressBookList();
                    for (int i = 0; i < addressBookList.size(); i++) {
                        if (addressBookList.get(i).getID().equals(addressBookItemDetails.getID())) {
                            addressBookList.remove(i);
                            break;
                        }
                    }
                    localStorageData.storeBuyerAddressBookList(addressBookList);   // DELETING FROM LOCAL DATABASE

                    showSnackbarMessage("Favourite Deleted!", null, null);
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to Delete Favourite , try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            deleteFavourite(position, addressBookItemDetails);
                        }
                    });
                }
            }
        });
    }

    public void checkAvailabilityAtLocationAndProceedToHome(final AddressBookItemDetails deliveryLocation){

        showLoadingDialog("Loading, please wait..");
        getBackendAPIs().getFoodItemsAPI().getCategoriesList(deliveryLocation.getLatLng(), new FoodItemsAPI.CategoriesListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, long total, List<CategoryDetails> categoriesList) {
                if (status) {
                    if (total > 0) {
                        localStorageData.storeCategories(categoriesList);

                        if ( isForActivityResult == true ) {
                            String deliver_location_json = new Gson().toJson(new PlaceDetails(deliveryLocation));
                            Intent placeData = new Intent();
                            placeData.putExtra("deliver_location_json", deliver_location_json);
                            setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, placeData);
                            finish();
                        } else {
                            localStorageData.setDeliveryLocation(deliveryLocation);

                            Intent homeActivity = new Intent(context, HomeActivity.class);
                            startActivity(homeActivity);
                            finish();
                        }

                        Intent homeActivity = new Intent(context, HomeActivity.class);
                        startActivity(homeActivity);
                        finish();
                    }
                    else {
                        closeLoadingDialog();
                        String msg = "We couldn't fetch you any of our delicious food items. This could be because..."
                                + "\n\n"
                                + "\t 1. Our chefs haven’t uploaded any dishes yet (do check back again)"
                                + "\n\t 2. All our items are past their availability time"
                                + "\n\n"
                                + "We are currently serving at " + localStorageData.getServingAt();
                        showNotificationDialog("Nothing to show!", msg);
                    }
                } else {
                    closeLoadingDialog();
                    showChoiceSelectionDialog(true, "Error!", "Something went wrong while checking availability at selected location.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                checkAvailabilityAtLocationAndProceedToHome(deliveryLocation);
                            }
                        }
                    });
                }
            }
        });
    }

    //----------------

    public void onPerformSearch(String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchQuery = search_query.toLowerCase();

            List<AddressBookItemDetails> filteredList = new ArrayList<>();
            for (int i=0;i<originalList.size();i++){
                AddressBookItemDetails addressBookItemDetails = originalList.get(i);
                if ( addressBookItemDetails.getFullAddress().toLowerCase().contains(searchQuery) || addressBookItemDetails.getTitle().toLowerCase().contains(searchQuery) ){
                    filteredList.add(originalList.get(i));
                }
            }

            if ( filteredList.size() > 0 ){
                addressBookAdapter.setItems(filteredList);
                switchToContentPage();
            }else{
                showNoDataIndicator("Nothing found for ("+searchQuery+")");
            }
        }
    }

    public void onClearSearch(){
        if ( searchQuery != null && searchQuery.length() > 0 ) {
            setupHolder();
            addressBookAdapter.setItems(originalList);
            if (addressBookAdapter.getItemsCount() > 0) {
                switchToContentPage();
            } else {
                showNoDataIndicator("No items");
            }
        }
    }

    //============

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE && resultCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE) {      // If It is from PlaceSearch Activity

                String deliver_location_json = data.getStringExtra("deliver_location_json");
                PlaceDetails deliveryLocation = new Gson().fromJson(deliver_location_json, PlaceDetails.class);

                if (deliveryLocation != null) {

                    if (isForActivityResult) {
                        Intent placeData = new Intent();
                        placeData.putExtra("deliver_location_json", deliver_location_json);
                        setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, placeData);
                        finish();
                    } else {
                        localStorageData.setDeliveryLocation(new AddressBookItemDetails(deliveryLocation));

                        Intent homeActivity = new Intent(context, HomeActivity.class);
                        startActivity(homeActivity);
                        finish();
                    }
                }
            }
            else if (requestCode == Constants.FAVOURITE_ACTIVITY_CODE && resultCode == Constants.FAVOURITE_ACTIVITY_CODE) {
                /*
                // NOT NEEDED AS ACTIVITY RECEIVER DOING THIS WORK
                String address_book_item_json = data.getStringExtra("address_book_item_json");
                AddressBookItemDetails addressBookItemDetails = new Gson().fromJson(address_book_item_json, AddressBookItemDetails.class);

                if ( addressBookItemDetails != null ){
                    addressBookItemDetails.setType("favourite");
                    addressBookAdapter.updateItem(selectedItemIndex, addressBookItemDetails);
                    showSnackbarMessage("Favourite Location (" + addressBookItemDetails.getTitle() + ") Updated.", null, null);
                }*/
            }
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    public void showSearchView(){
        try{    searchViewMenuITem.setVisible(true); }catch (Exception e){}
    }

    public void hideSearchView(){
        try{    searchViewMenuITem.setVisible(false); }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){

        }
        else {
            if ( isForActivityResult == true ){
                onBackPressed();
            }else{
                if (drawerListener.onOptionsItemSelected(item)) {
                    return true;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
