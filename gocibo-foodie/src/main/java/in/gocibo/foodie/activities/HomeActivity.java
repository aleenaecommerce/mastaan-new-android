package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vGridView;
import com.google.gson.Gson;
import com.rey.material.widget.ProgressView;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.CategoriesAdapter;
import in.gocibo.foodie.backend.CartAPI;
import in.gocibo.foodie.backend.FoodItemsAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.fragments.SideBarFragment;
import in.gocibo.foodie.models.CartItemDetails;
import in.gocibo.foodie.models.CategoryDetails;

public class HomeActivity extends GociboNavigationDrawerActivity implements View.OnClickListener{

    MenuItem pendingOrdersInfo;

    SideBarFragment sideBarFragment;

    FrameLayout cartInfo;
    ImageButton cartFAB;
    TextView cart_count;
    ProgressView cart_countdown_indicator;

    vGridView categoriesHolder;
    CategoriesAdapter categoriesAdapter;
    CategoriesAdapter.Callback categoriesCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        hasLoadingView();
        hasSwipeRefresh();

        sideBarFragment = new SideBarFragment();
        sideBarFragment.setCallback(new SideBarFragment.SideBarCallback() {
            @Override
            public void onItemSelected(int position) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }

            @Override
            public void onDeliverLocationChange(PlaceDetails deliverLocationDetails) {
                //drawerLayout.closeDrawer(Gravity.LEFT);
                if (localStorageData.getCartCount() > 0) {
                    showClearCartDialogOnDeliveryLocationChange(deliverLocationDetails);
                } else {
                    localStorageData.setDeliveryLocation(new AddressBookItemDetails(deliverLocationDetails));
                    sideBarFragment.changeDeliveryLocation(deliverLocationDetails.getFullAddress());
                    getCategories();
                }
            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();

        registerActivityReceiver(Constants.HOME_RECEIVER);

        //-----------------------

        cartInfo = (FrameLayout) findViewById(R.id.cartInfo);
        cart_count = (TextView) findViewById(R.id.cart_count);
        cart_countdown_indicator = (ProgressView) findViewById(R.id.cart_countdown_indicator);
        cartFAB = (ImageButton) findViewById(R.id.cartFAB);
        cartFAB.setOnClickListener(this);

        categoriesHolder = (vGridView) findViewById(R.id.categoriesHolder);
        setupCategoriesHolder();

        //-----------------------

        displayUserSessionInfo();       // Displaying User info

        List<CategoryDetails> categoriesList = localStorageData.getCategories();
        if ( categoriesList != null && categoriesList.size() > 0 ){
            displayCategories(categoriesList);
        }else{
            getCategories();
        }

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();
        displayUserSessionInfo();
    }

    @Override
    public void onClick(View view) {
       if ( view == cartFAB){
            Intent cartAct = new Intent(context, CartActivity.class);
            startActivity(cartAct);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkCartStatus("");
    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
            super.onActivityReceiverMessage(activityReceiverName, receivedData);

        try{
            if ( receivedData.getStringExtra("type").equalsIgnoreCase("display_user_session_info") ){
                displayUserSessionInfo();
            }
            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("display_pending_orders_info") ){
                displayUserSessionInfo();
            }
            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("on_order_done") ){
                deleteFromPendingOrders(receivedData.getStringExtra("orderID"));
                displayUserSessionInfo();
            }
            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("update_category_uploads_count") ){
                categoriesAdapter.updateCategoryUploadsCount(receivedData.getStringExtra("categoryID"), receivedData.getIntExtra("uploadsCount", -1));
            }
            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("start_timed_cart") ){
                startTimedCart();
            }

            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("update_cart_count") ){
                checkCartStatus("");
            }

            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("update_timer_cart_countdown_indicator") ){
                if ( cart_countdown_indicator.getVisibility() == View.INVISIBLE || cart_countdown_indicator.getVisibility() == View.GONE ){
                    cart_countdown_indicator.setVisibility(View.VISIBLE);
                }
                cart_countdown_indicator.setProgress(receivedData.getFloatExtra("progress", cart_countdown_indicator.getProgress()));
            }

            else if ( receivedData.getStringExtra("type").equalsIgnoreCase("stop_timed_cart") ){
                localStorageData.setCartCount(0);
                localStorageData.setCartStartTime("'");

                context.stopService(timedCartService);
                checkCartStatus("");
                cart_countdown_indicator.setVisibility(View.INVISIBLE);
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getCategories();
    }

    //-------------

    public void setupCategoriesHolder(){

        categoriesCallback = new CategoriesAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                Intent itemListingsAct = new Intent(context, ItemListingsActivity.class);
                itemListingsAct.putExtra("categoryDetails", new Gson().toJson(categoriesAdapter.getItem(position)));
                startActivity(itemListingsAct);
            }
        };

        //categoriesHolder.setExpanded(true);           // To Make GridView Collapsible
        categoriesAdapter = new CategoriesAdapter(context, new ArrayList<CategoryDetails>(), categoriesCallback);
        categoriesHolder.setAdapter(categoriesAdapter);
    }

    public void displayCategories(List<CategoryDetails> categoriesList){

        if ( categoriesList != null ){

            localStorageData.storeCategories(categoriesList);   // Storing locally

            if ( categoriesList.size() > 0 ){
                categoriesAdapter.setItems(categoriesList);
                switchToContentPage();
            }else{
                showNoDataIndicator("No categories available.");
            }
        }else{
            showReloadIndicator("Unable to load categories");
            getCategories();
        }
    }

    //------------

    public void getCategories(){

        showLoadingIndicator("Loading categories, wait..");
        getBackendAPIs().getFoodItemsAPI().getCategoriesList(localStorageData.getDeliveryLocation().getLatLng(), new FoodItemsAPI.CategoriesListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, long total, List<CategoryDetails> categoriesList) {
                if (status) {
                    displayCategories(categoriesList);
                }else{
                    showReloadIndicator(statusCode, "Unable to load categories, try again!");
                }
            }
        });
    }

    //=========================

    public void startTimedCart(){

        context.startService(timedCartService);
        cart_countdown_indicator.setVisibility(View.VISIBLE);
        cart_countdown_indicator.setProgress(0.0f);
    }

    public void displayUserSessionInfo(){

        if ( pendingOrdersInfo != null ) {
            if ( localStorageData.getNoOfPendingOrders() > 0 ){
                pendingOrdersInfo.setVisible(true);
                pendingOrdersInfo.setTitle(localStorageData.getNoOfPendingOrders() + " PENDING ORDERS");
                if ( localStorageData.getNoOfPendingOrders() == 1 ){
                    pendingOrdersInfo.setTitle("1 PENDING ORDER");
                }
            }else{
                pendingOrdersInfo.setVisible(false);
                pendingOrdersInfo.setTitle("");
            }
        }

        if ( localStorageData.getCartCount() > 0 ){
            if ( localStorageData.getCartRemainingTime() > 0 ) {
                if (cart_countdown_indicator.getVisibility() == View.INVISIBLE || cart_countdown_indicator.getVisibility() == View.GONE) {
                    startTimedCart();
                }
            }else{
                localStorageData.setCartCount(0);
                localStorageData.setCartRemainingTime(0);
            }
        }else{
            context.stopService(timedCartService);
        }
        checkCartStatus("");
    }

    //==========================

    public void onDeliverLocationUpdate(){
        //homeFoodListingsFragment.getWhatsCookingFoodItems();
    }

    public void showClearCartDialogOnDeliveryLocationChange(final PlaceDetails newDeliverLocation){

        showChoiceSelectionDialog(true, "Clear food basket!", "Sorry, changing the delivery location will clear your food basket.\n\nDo you want to change your delivery location?", "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {

                closeChoiceSelectionDialog();

                if (choiceName.equalsIgnoreCase("YES")) {

                    localStorageData.setDeliveryLocation(new AddressBookItemDetails(newDeliverLocation));
                    sideBarFragment.changeDeliveryLocation(newDeliverLocation.getFullAddress());

                    showLoadingDialog("Clearing your cart, wait..");
                    getBackendAPIs().getCartAPI().clearCartItems(new CartAPI.ClearCartItemsCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, List<CartItemDetails> cartItems) {
                            closeLoadingDialog();

                            if (status == true) {
                                localStorageData.setCartCount(0);
                                localStorageData.setCartRemainingTime(0);
                                checkCartStatus("");
                                broadcastReceiversMethods.clearShoppingCart(null);

                                displayCategories(localStorageData.getCategories());
                            }
                            else {
                                checkSessionValidity(statusCode);
                                showSnackbarMessage("Error in clearing your cart, TryAgain", null, null);
                                showClearCartDialogOnDeliveryLocationChange(newDeliverLocation);
                            }
                        }
                    });
                }
            }
        });
    }

    //==========================

    public void checkCartStatus(final String message){

        fabEnterAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (message != null && message.length() > 0) {
                    showSnackbarMessage(cartInfo, message, null, null);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        int cartCount = localStorageData.getCartCount();

        if ( cartCount > 0 ){
            cart_count.setText(""+cartCount);
            if ( cartInfo.getVisibility() == View.INVISIBLE  || cartInfo.getVisibility() == View.GONE ){
                cartInfo.setVisibility(View.VISIBLE);
                cartInfo.startAnimation(fabEnterAnim);
            }else{
                if (message != null && message.length() > 0) {
                    showSnackbarMessage(cartInfo, message, null, null);
                }
            }
        }
        else {
            localStorageData.setCartRemainingTime(0);
            if ( cartInfo.getVisibility() == View.VISIBLE ){
                cartInfo.setVisibility(View.INVISIBLE);
                cartInfo.startAnimation(fabExitAnim);
            }
        }
    }

    //===========================

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        pendingOrdersInfo = menu.findItem(R.id.action_pending_orders);
        onAfterMenuCreated();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void disableRightNavigationDrawer() {
        super.disableRightNavigationDrawer();
    }

    @Override
    public void enableRightNavigationDrawer() {
        super.enableRightNavigationDrawer();
    }

    public void closeLeftNavigationDrawer(){
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if ( drawerListener.onOptionsItemSelected(item) ) {
            return true;
        }
        else if (item.getItemId() == R.id.action_pending_orders) {
            if ( localStorageData.getNoOfPendingOrders() == 1 ) {
                Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
                orderDetailsAct.putExtra("orderID", localStorageData.getBuyerPendingOrders().get(0).getID());
                startActivity(orderDetailsAct);
            }else{
                Intent pendingOrdersAct = new Intent(context, PendingOrdersActivity.class);
                startActivity(pendingOrdersAct);
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
