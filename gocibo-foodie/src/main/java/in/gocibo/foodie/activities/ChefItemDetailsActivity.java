package in.gocibo.foodie.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vCircularImageView;
import com.aleena.common.widgets.vFlowLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.ChefsAPI;
import in.gocibo.foodie.models.DishDetails;
import in.gocibo.foodie.models.IngradientDetails;

public class ChefItemDetailsActivity extends GociboToolBarActivity{

    Menu menu;
    CollapsingToolbarLayout collapsingToolbar;

    ImageView item_image;
    vCircularImageView chef_image;
    Button chefSelector;
    TextView chef_name;
    TextView item_description;
    TextView item_cuisine;
    MenuItem item_type_indicator;
    vFlowLayout ingradientsHolder;

    String dishID;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chef_item_details);
        hasLoadingView();
        hasBackButton();        //  To Show while loading

        dishID = getIntent().getStringExtra("dishID");

        //---------------

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(getString(R.string.title_activity_food_item_details).toUpperCase());

        //.................................

        item_image = (ImageView) findViewById(R.id.item_image);
        chef_image = (vCircularImageView) findViewById(R.id.chef_image);
        chefSelector = (Button) findViewById(R.id.chefSelector);

        chef_name = (TextView) findViewById(R.id.chef_name);
        item_description = (TextView) findViewById(R.id.item_description);
        item_cuisine = (TextView) findViewById(R.id.item_cuisine);

        ingradientsHolder = (vFlowLayout) findViewById(R.id.ingradientsHolder);

        //------------------------------------

        getItemDetails();      // Load Food Item Details..

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getItemDetails();
    }

    public void getItemDetails(){

        showLoadingIndicator("Loading food item details, wait..");
        getBackendAPIs().getChefsAPI().getChefItemDetails(dishID, new ChefsAPI.ChefItemDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, DishDetails dishDetails) {
                if (status == true) {
                    displayItemDetails(dishDetails);
                } else {
                    showReloadIndicator(statusCode, "Unable to load food item details, try again!");
                }
            }
        });
    }

    // Display Food Item Details...
    public void displayItemDetails(DishDetails dishDetails){

        switchToContentPage();

        collapsingToolbar.setTitle(dishDetails.getName().toUpperCase());

        //ViewCompat.animate(item_image).setDuration(1000).scaleYBy(1).scaleXBy(1).start();
        Picasso.get()
                .load(dishDetails.getImageURL())
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .fit()
                .centerCrop()
                .tag(context)
                .into(item_image);

        chef_name.setText(CommonMethods.capitalizeStringWords(dishDetails.getChefFullName()));
        item_description.setText(CommonMethods.capitalizeFirstLetter(dishDetails.getDescription()));
        item_cuisine.setText(CommonMethods.capitalizeFirstLetter(dishDetails.getCuisineName()));

        if ( item_type_indicator == null ){
            if ( menu != null ) {
                item_type_indicator = menu.getItem(0);
            }
        }
        if ( item_type_indicator != null ){
            if ( dishDetails.getType().equalsIgnoreCase("veg") ){
                item_type_indicator.setIcon(R.drawable.ic_veg);
            }else if ( dishDetails.getType().equalsIgnoreCase("non-veg") ){
                item_type_indicator.setIcon(R.drawable.ic_non_veg);
            }else if ( dishDetails.getType().equalsIgnoreCase("jain") ){
                item_type_indicator.setIcon(R.drawable.ic_jain);
            }
        }

        Picasso.get()
                .load(dishDetails.getChefThumbnailImageURL())
                .placeholder(R.drawable.ic_chef)
                .error(R.drawable.ic_chef)
                .fit()
                .centerCrop()
                .tag(context)
                .into(chef_image);

        List<IngradientDetails> ingradients = dishDetails.getIngradients();
        for(int i=0;i<ingradients.size();i++){
            final int position = i;
            View ingradientView = inflater.inflate(R.layout.view_ingredient_item, null, false);
            ingradientsHolder.addView(ingradientView);

            TextView ingradient_name = (TextView) ingradientView.findViewById(R.id.ingradient_name);
            ingradient_name.setText(ingradients.get(position).getName());
        }
    }

    //.........................

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() != R.id.item_type_indicator ) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_food_item_details, menu);
        this.menu = menu;
        item_type_indicator = menu.getItem(0);
        return super.onCreateOptionsMenu(menu);
    }

}