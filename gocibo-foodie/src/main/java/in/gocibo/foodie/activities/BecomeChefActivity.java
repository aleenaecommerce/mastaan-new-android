package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import in.gocibo.foodie.R;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.interfaces.RegistrationCallback;

public class BecomeChefActivity extends GociboToolBarActivity implements View.OnClickListener {

    Toolbar toolbar;
    ImageButton backButton;

    Button become_gocibo_chef, refer_someone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_become_chef);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        backButton = (ImageButton) findViewById(R.id.backButton);
        backButton.setOnClickListener(this);

        become_gocibo_chef = (Button) findViewById(R.id.become_gocibo_chef);
        become_gocibo_chef.setOnClickListener(this);
        refer_someone = (Button) findViewById(R.id.refer_someone);
        refer_someone.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if ( view == backButton ){
            onBackPressed();
        }
        else if ( view == become_gocibo_chef ){
            if ( localStorageData.getSessionFlag() == true ){
                Intent registerChefAct = new Intent(context, RegisterChefActivity.class);
                registerChefAct.putExtra("registerChefType", "me");
                startActivityForResult(registerChefAct, Constants.REGISTER_CHEF_ACTIVITY_CODE);
            }else{
                goToRegistration(new RegistrationCallback() {
                    @Override
                    public void onComplete(boolean status, String token, in.gocibo.foodie.models.BuyerDetails buyerDetails) {
                        if (status) {
                            Intent registerChefAct = new Intent(context, RegisterChefActivity.class);
                            registerChefAct.putExtra("registerChefType", "me");
                            startActivityForResult(registerChefAct, Constants.REGISTER_CHEF_ACTIVITY_CODE);
                        }
                    }
                });
            }

        }
        else if ( view == refer_someone ){
            Intent registerChefAct = new Intent(context, RegisterChefActivity.class);
            registerChefAct.putExtra("registerChefType", "other");
            startActivityForResult(registerChefAct, Constants.REGISTER_CHEF_ACTIVITY_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if ( requestCode == Constants.REGISTER_CHEF_ACTIVITY_CODE && resultCode == Constants.REGISTER_CHEF_ACTIVITY_CODE ) {
                String mobileNumber = data.getStringExtra("mobile");
                if ( mobileNumber != null && mobileNumber.length() > 0  ){
                    onBackPressed();
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
    }

}
