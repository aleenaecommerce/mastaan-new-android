package in.gocibo.foodie.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;

import android.view.Gravity;
import android.view.View;

import in.gocibo.foodie.GoCiboApplication;
import in.gocibo.foodie.R;

public class GociboNavigationDrawerActivity extends GociboToolBarActivity {

    Context context;
    public DrawerLayout drawerLayout;
    public ActionBarDrawerToggle drawerListener;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        context = this;
        setupNavigationDrawer();
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        goCiboApplication = (GoCiboApplication) getApplication();       // Accessing Application Class;
    }

    public void setupNavigationDrawer(){

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerListener = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {//, R.drawable.ic_drawer
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                drawerListener.setDrawerIndicatorEnabled(false);
                drawerListener.syncState();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                drawerListener.setDrawerIndicatorEnabled(true);
                drawerListener.syncState();
            }
        };

        drawerLayout.setDrawerListener(drawerListener);
    }

    public DrawerLayout getDrawerLayout(){
        return drawerLayout;
    }
    public ActionBarDrawerToggle getDrawerListener(){
        return drawerListener;
    }

    public void disableNavigationDrawer(){
        //drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //drawerListener.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerListener.setDrawerIndicatorEnabled(false);
        drawerListener.syncState();
    }

    public void enableLeftNavigationDrawer(){
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(R.id.left_drawer));
    }
    public void disableLeftNavigationDrawer(){
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.left_drawer));
    }

    public void enableRightNavigationDrawer(){
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(R.id.right_drawer));
    }
    public void disableRightNavigationDrawer(){
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerListener.syncState();             // Sync the toggle state after onRestoreInstanceState has occurred.
    }

    @Override
    public void onBackPressed() {
        if ( drawerLayout.isDrawerOpen(Gravity.LEFT) ) {
            drawerLayout.closeDrawer(Gravity.LEFT);
        }
        else if ( drawerLayout.isDrawerOpen(Gravity.RIGHT) ) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }
        else if ( drawerLayout.isDrawerOpen(GravityCompat.START) ) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else if ( drawerLayout.isDrawerOpen(GravityCompat.END) ) {
            drawerLayout.closeDrawer(GravityCompat.END);
        }
        else {
            super.onBackPressed();
        }
        onBackButonPressed();
    }

    public void onBackButonPressed(){}
}
