package in.gocibo.foodie.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.DefaultItemAnimator;

import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vWrappingLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.OrdersHistoryAdapter;
import in.gocibo.foodie.backend.OrdersAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.OrderDetails;

public class PendingOrdersActivity extends GociboToolBarActivity implements View.OnClickListener{

    Button call_us;

    vRecyclerView ordersListHolder;
    OrdersHistoryAdapter ordersListAdapter;
    OrdersHistoryAdapter.CallBack ordersListCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_pending_orders);
        hasLoadingView();
        hasSwipeRefresh();

        //-------------------------------------

        call_us = (Button) findViewById(R.id.call_us);
        call_us.setOnClickListener(this);
        call_us.setText("Tap to call " + localStorageData.getSupportContactNumber() + " for order status/cancellation");

        ordersListHolder = (vRecyclerView) findViewById(R.id.ordersListHolder);
        setupOrderListHolder();

        //--------------------------------------

        getPendingOrders();

    }

    @Override
    public void onClick(View view) {

        if ( view == call_us ){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
            startActivity(intent);
        }
    }

    public void setupOrderListHolder(){

        ordersListHolder.setLayoutManager(new vWrappingLinearLayoutManager(context));//new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        ordersListHolder.setHasFixedSize(false);
        ordersListHolder.setNestedScrollingEnabled(false);
        ordersListHolder.setItemAnimator(new DefaultItemAnimator());

        ordersListCallback = new OrdersHistoryAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
                orderDetailsAct.putExtra("orderID", ordersListAdapter.getOrderItem(position).getID());
                startActivity(orderDetailsAct);
            }
        };

        ordersListAdapter = new OrdersHistoryAdapter(context, new ArrayList<OrderDetails>(), ordersListCallback);
        ordersListHolder.setAdapter(ordersListAdapter);

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPendingOrders();      // GetMyPendingOrders
    }

    public void getPendingOrders(){

        ordersListAdapter.clearItems();

        showLoadingIndicator("Loading your pending orders, wait..");
        getBackendAPIs().getOrdersAPI().getPendingOrders(new OrdersAPI.PendingOrdersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<OrderDetails> ordersList) {

                if (status == true) {
                    if (ordersList.size() > 0) {
                        ordersListAdapter.addItems(ordersList);
                        switchToContentPage();

                        localStorageData.setBuyerPendingOrders(ordersList);
                        Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                                .putExtra("type", "display_user_session_info");
                        context.sendBroadcast(homeReceiverIntent);

                    } else {
                        showNoDataIndicator("No Pending orders!!! ");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load your pending orders, try again!");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }
}
