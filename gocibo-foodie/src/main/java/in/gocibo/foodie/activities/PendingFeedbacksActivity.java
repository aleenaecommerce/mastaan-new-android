package in.gocibo.foodie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import com.aleena.common.interfaces.StatusCallback;
import in.gocibo.foodie.R;
import in.gocibo.foodie.localdata.LocalDBOrderFeedback;
import in.gocibo.foodie.localdata.OrdersFeedbackDatabase;
import in.gocibo.foodie.models.OrderDetails;
import in.gocibo.foodie.constants.Constants;

public class PendingFeedbacksActivity extends GociboToolBarActivity {

    OrdersFeedbackDatabase ordersFeedbackDatabase;

    LinearLayout thank_you_view;
    Button continuee;

    boolean isSubmittedAnyFeedback = false;

    int currentOrderIndex = 0;
    List<OrderDetails> allOrders = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_feedbacks);

        ArrayList<String> pendingFeedbackOrders = getIntent().getStringArrayListExtra("pendingFeedbackOrders");
        if ( pendingFeedbackOrders != null ){
            for ( int i=0;i<pendingFeedbackOrders.size();i++){
                allOrders.add(new Gson().fromJson(pendingFeedbackOrders.get(i), OrderDetails.class));
            }
        }

        ordersFeedbackDatabase = new OrdersFeedbackDatabase(context);
        ordersFeedbackDatabase.openDatabase();

        //-------------------------------

        thank_you_view = (LinearLayout) findViewById(R.id.thank_you_view);
        continuee = (Button) findViewById(R.id.continuee);
        continuee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ordersFeedbackDatabase.closeDatabase();
                setResult(Constants.PENDING_FEEDBACK_ORDERS_ACTIVITY_CODE, new Intent());
                finish();
            }
        });
        //-------------------------------

        displayOrderFeedback(0);
    }

    public void displayOrderFeedback(int index){

        if ( allOrders.size() > index ){

            currentOrderIndex = index;

            ordersFeedbackDatabase.getFeedbackItem(allOrders.get(currentOrderIndex).getID(), new OrdersFeedbackDatabase.GetOrderFeedbackItemCallback() {
                @Override
                public void onComplete(final LocalDBOrderFeedback localDBOrderFeedback) {

                    if ( localDBOrderFeedback == null || localDBOrderFeedback.getOrderID() == null ) {  //  If Not Given Feedback.

                        Intent orderFeedbackAct = new Intent(context, OrderFeedbackActivity.class);
                        orderFeedbackAct.putExtra("order_details_json", new Gson().toJson(allOrders.get(currentOrderIndex)));
                        if (currentOrderIndex == allOrders.size() - 1) {
                            orderFeedbackAct.putExtra("showNextIndicator", false);
                        }
                        startActivityForResult(orderFeedbackAct, Constants.ORDER_FEEDBACK_ACTIVITY_CODE);
                    }
                    else{
                        Log.d(Constants.LOG_TAG, "Already Had Feedback for OrderID="+localDBOrderFeedback.getOrderID()+" of Type="+localDBOrderFeedback.getType()+" , with Feedback : "+localDBOrderFeedback.getOrderFeedbackJSON()+" , Submitting it in the Background");
                        displayOrderFeedback(currentOrderIndex + 1);    //  Showing Next FeedbackItem

                        if ( localDBOrderFeedback.getBuyerMobile().equalsIgnoreCase(localStorageData.getBuyerMobile()) ){

                            if ( localDBOrderFeedback.getType().equalsIgnoreCase("Submit") ){
                                getBackendAPIs().getOrdersAPI().postOrderFeedback(localDBOrderFeedback.getOrderID(), localDBOrderFeedback.getOrderFeedbackObject(), new StatusCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message) {
                                        if (status == true || statusCode == 500) {
                                            ordersFeedbackDatabase.deleteFeedbackItem(localDBOrderFeedback.getOrderID(), null);
                                        }
                                        else if ( statusCode == 403 ){
                                            clearUserSession("Your session has expired.");
                                        }
                                        else {
                                            if ( localDBOrderFeedback.getRetryCount() < 8 ) {
                                                ordersFeedbackDatabase.increaseOrderFeedbackRetryCount(localDBOrderFeedback, null);
                                            }else{
                                                ordersFeedbackDatabase.deleteFeedbackItem(localDBOrderFeedback.getOrderID(), null);
                                            }
                                        }
                                    }
                                });
                            }
                            else if ( localDBOrderFeedback.getType().equalsIgnoreCase("Skip") ){
                                getBackendAPIs().getOrdersAPI().skipOrderFeedback(localDBOrderFeedback.getOrderID(), new StatusCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message) {
                                        if (status == true || statusCode == 500 ) {
                                            ordersFeedbackDatabase.deleteFeedbackItem(localDBOrderFeedback.getOrderID(), null);
                                        }
                                        else if ( statusCode == 403 ){
                                            clearUserSession("Your session has expired.");
                                        }
                                        else {
                                            if ( localDBOrderFeedback.getRetryCount() < 8 ) {
                                                ordersFeedbackDatabase.increaseOrderFeedbackRetryCount(localDBOrderFeedback, null);
                                            }else{
                                                ordersFeedbackDatabase.deleteFeedbackItem(localDBOrderFeedback.getOrderID(), null);
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

        }
        else{
            if ( isSubmittedAnyFeedback == true ){
                thank_you_view.setVisibility(View.VISIBLE);
            }else{
                //ordersFeedbackDatabase.closeDatabase();
                setResult(Constants.PENDING_FEEDBACK_ORDERS_ACTIVITY_CODE, new Intent());
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == Constants.ORDER_FEEDBACK_ACTIVITY_CODE ){
                if ( resultCode == Constants.ORDER_FEEDBACK_ACTIVITY_CODE) {
                    if ( data.getBooleanExtra("isSubmittedFeedback", false)  ){
                        isSubmittedAnyFeedback = true;
                    }
                    displayOrderFeedback(currentOrderIndex + 1);
                }else{
                    onBackPressed();
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }
}
