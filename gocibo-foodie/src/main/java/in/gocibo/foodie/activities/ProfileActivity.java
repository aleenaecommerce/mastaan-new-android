package in.gocibo.foodie.activities;

import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.R;
import in.gocibo.foodie.adapters.TabsFragmentAdapter;
import in.gocibo.foodie.fragments.AddressBookFragment;
import in.gocibo.foodie.fragments.ProfileAllergiesFragment;
import in.gocibo.foodie.fragments.ProfileSummaryFragment;
import in.gocibo.foodie.models.AllergyDetails;

public class ProfileActivity extends GociboToolBarActivity {

    public String[] profileItems = new String[]{"Profile", "Address Book", "Order History", "Allergies"};

    List<AllergyDetails> buyerAllergies;
    List<AllergyDetails> availableAllergies = new ArrayList<>();

    public int pageNumber;
    TabLayout tabLayout;
    ViewPager viewPager;
    TabsFragmentAdapter tabsFragmentAdapter;

    public FloatingActionButton activityFAB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_profile);

        //-----------

        activityFAB = (FloatingActionButton) findViewById(R.id.activityFAB);
        activityFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profileItems[pageNumber].equalsIgnoreCase("Profile")) {
                    ProfileSummaryFragment profileSummaryFragment = (ProfileSummaryFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + pageNumber);
                    profileSummaryFragment.onFABPressed();
                } else if (profileItems[pageNumber].equalsIgnoreCase("Allergies")) {
                    ProfileAllergiesFragment profileAllergiesFragment = (ProfileAllergiesFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + pageNumber);
                    profileAllergiesFragment.onFABPressed();
                } else if (profileItems[pageNumber].equalsIgnoreCase("Address Book")) {
                    AddressBookFragment addressBookFragment = (AddressBookFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + pageNumber);
                    addressBookFragment.onFABPressed();
                }
            }
        });


        // Adding Tabs..

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabTextColors(getResources().getColor(R.color.normal_tab_text_color), getResources().getColor(R.color.selected_tab_text_color));

        for(int i=0;i<profileItems.length;i++) {
            TabLayout.Tab tab = tabLayout.newTab();
            //tab.setText(profileItems[i]);
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.view_tab_list_item, null, false);
            TextView tabTitle = (TextView) rowView.findViewById(R.id.tabTitle);
            tabTitle.setText(profileItems[i]);
            tab.setCustomView(rowView);
            tabLayout.addTab(tab);
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pageNumber = tab.getPosition();
                viewPager.setCurrentItem(pageNumber);

                if (profileItems[pageNumber].equalsIgnoreCase("Address Book")) {
                    activityFAB.setImageResource(R.drawable.ic_add_white);
                }else{
                    activityFAB.setImageResource(R.drawable.ic_edit);
                }

                if (profileItems[pageNumber].equalsIgnoreCase("Profile")) {//|| profileItems[pageNumber].equalsIgnoreCase("Allergies") ){
                    ProfileSummaryFragment profileSummaryFragment = (ProfileSummaryFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + pageNumber);
                    profileSummaryFragment.toggleEditButton();
                } else if (profileItems[pageNumber].equalsIgnoreCase("Allergies")) {
                    ProfileAllergiesFragment profileAllergiesFragment = (ProfileAllergiesFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + pageNumber);
                    profileAllergiesFragment.toggleEditButton();
                } else if (profileItems[pageNumber].equalsIgnoreCase("Address Book")) {
                    setFABVisibility(true);
                } else {
                    setFABVisibility(false);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });

        // View Pager...

        viewPager = (ViewPager) findViewById(R.id.pager);
        tabsFragmentAdapter = new TabsFragmentAdapter(context, getSupportFragmentManager(), profileItems);
        viewPager.setAdapter(tabsFragmentAdapter);

        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                /*final float normalizedposition = Math.abs(Math.abs(position) - 1);
                page.setAlpha(normalizedposition);*/

                /*final float normalizedposition = Math.abs(Math.abs(position) - 1);
                page.setScaleX(normalizedposition / 2 + 0.5f);
                page.setScaleY(normalizedposition / 2 + 0.5f);*/

                //page.setRotationY(position * -30);
            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));  // To Sync ViewPager with Tabs.
    }

    public void setFABVisibility(boolean visibility){
        //showSnackbarMessage("Page = "+pageName+" Visibility = "+visibility, null, null);
        if ( visibility == true ){
            activityFAB.setVisibility(View.VISIBLE);
            activityFAB.startAnimation(fabEnterAnim);
        }else{
            activityFAB.setVisibility(View.INVISIBLE);
        }
    }

    public void changePage(String pageName) {

        int position = -1;
        for (int i = 0; i < profileItems.length; i++) {
            if (profileItems[i].equalsIgnoreCase(pageName) == true) {
                position = i;
                break;
            }
        }
        if (position != -1) {
            viewPager.setCurrentItem(position);
        }
    }

    //-------------------------------------------

    public void setAvailableAllergies(List<AllergyDetails> availableAllergies){
        this.availableAllergies = availableAllergies;
    }

    public List<AllergyDetails> getAvailableAllergies() {
        return availableAllergies;
    }

    public List<AllergyDetails> getBuyerAllergies() {
        return buyerAllergies;
    }

    public void setBuyerAllergies(List<AllergyDetails> buyerAllergies) {

        if ( buyerAllergies != null ) {
            this.buyerAllergies = buyerAllergies;
            for (int i=0;i<profileItems.length;i++){
                if ( profileItems[i].equalsIgnoreCase("Summary") ){
                    ProfileSummaryFragment profileSummaryFragment = (ProfileSummaryFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + i);
                    profileSummaryFragment.updateBuyerAllergeis(buyerAllergies);
                    break;
                }
            }
        }
    }

    //............................................

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
    }
}
