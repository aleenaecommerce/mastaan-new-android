package in.gocibo.foodie.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import in.gocibo.foodie.R;

public class FoodItemFeedbackActivity extends GociboToolBarActivity {

    ViewSwitcher viewSwitcher;

    boolean isLiked;
    FrameLayout like_selector_holder;
    TextView yes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_food_item_feedback);

        //.............................................................

        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);

        like_selector_holder = (FrameLayout) findViewById(R.id.like_selector_holder);
        yes = (TextView) findViewById(R.id.yes);
        findViewById(R.id.like_selector).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( isLiked ){
                    isLiked = false;
                    like_selector_holder.setBackgroundColor(context.getResources().getColor(R.color.whiteColor));
                    yes.setTextColor(context.getResources().getColor(R.color.greyColor));
                }else {
                    isLiked = true;
                    like_selector_holder.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryMain));
                    yes.setTextColor(context.getResources().getColor(R.color.whiteColor));
                }
            }
        });





    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
    }

}
