package in.gocibo.foodie.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.interfaces.SoftKeyBoardActionsCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.widgets.vScrollView;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;

import in.gocibo.foodie.R;
import in.gocibo.foodie.backend.OrdersAPI;
import in.gocibo.foodie.backend.model.RequestPlaceOrder;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.BuyerDetails;
import in.gocibo.foodie.models.OrderDetails;

public class OrderBookingActivity extends GociboToolBarActivity implements View.OnClickListener{

    MenuItem cartTimer;

    BuyerDetails sessionBuyerDetails;
    AddressBookItemDetails deliverLocationDetails;

    vScrollView scrollView;

    vTextInputLayout name;
    vTextInputLayout phone;
    vTextInputLayout email;
    AppCompatAutoCompleteTextView emailAutoCompleteTextview;
    ListArrayAdapter registeredEmailsAdapter;

    vTextInputLayout premise;
    vTextInputLayout sublocality;
    vTextInputLayout landmark;
    vTextInputLayout address;

    vTextInputLayout instructions;

    Button placeOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_order_booking);
        hasLoadingView();

        registerActivityReceiver(Constants.CART_RECEIVER);

        //.............................................................

        scrollView = (vScrollView) findViewById(R.id.scrollView);

        name = (vTextInputLayout) findViewById(R.id.name);

        phone = (vTextInputLayout) findViewById(R.id.phone);
        email = (vTextInputLayout) findViewById(R.id.email);
        emailAutoCompleteTextview = (AppCompatAutoCompleteTextView) findViewById(R.id.emailAutoCompleteTextview);
        emailAutoCompleteTextview.setThreshold(1);
        registeredEmailsAdapter = new ListArrayAdapter(context, CommonMethods.getRegisteredEmails(context));
        emailAutoCompleteTextview.setAdapter(registeredEmailsAdapter);

        premise = (vTextInputLayout) findViewById(R.id.premise);
        sublocality = (vTextInputLayout) findViewById(R.id.sublocality);
        landmark = (vTextInputLayout) findViewById(R.id.landmark);
        address = (vTextInputLayout) findViewById(R.id.address);

        instructions = (vTextInputLayout) findViewById(R.id.instructions);
        instructions.setSoftKeyBoardListener(new SoftKeyBoardActionsCallback() {
            @Override
            public void onDonePressed(String text) {
                placeOrder.performClick();
            }
        });

        placeOrder = (Button) findViewById(R.id.placeOrder);
        placeOrder.setOnClickListener(this);

        //...........................................

        sessionBuyerDetails = localStorageData.getBuyerDetails();
        deliverLocationDetails = localStorageData.getDeliveryLocation();

        phone.setText(CommonMethods.removeCountryCodeFromNumber(localStorageData.getBuyerMobile()));
        email.setText(localStorageData.getBuyerEmail());
        name.setText(localStorageData.getBuyerName());

        if ( deliverLocationDetails.getID() != null && deliverLocationDetails.getID().length() > 0 ) {
            premise.setText(deliverLocationDetails.getPremise());
        }
        sublocality.setText(deliverLocationDetails.getSublocalityLevel_2());
        landmark.setText(deliverLocationDetails.getLandmark());
        if ( deliverLocationDetails.getLandmark() == null || deliverLocationDetails.getLandmark().length() == 0 ){
            landmark.setText(deliverLocationDetails.getRoute());
        }
        address.setText(CommonMethods.getStringFromStringArray(new String[]{deliverLocationDetails.getSublocalityLevel_1(), deliverLocationDetails.getLocality(), deliverLocationDetails.getState(), deliverLocationDetails.getCountry(), deliverLocationDetails.getPostalCode()}));

        //-----------

    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);

        try {
            if (receivedData.getStringExtra("type").equalsIgnoreCase("update_cart_remail_valid_time")) {
                try {
                    cartTimer.setTitle(receivedData.getStringExtra("remain_time"));
                } catch (Exception e) {}
            } else if (receivedData.getStringExtra("type").equalsIgnoreCase("clear_cart")) {
                placeOrder.setEnabled(false);
                cartTimer.setTitle("");
                //showSnackbarMessage("Your Cart Items Removed!", null, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

        if ( view == placeOrder ){

            hideSoftKeyboard();

            final String oName = name.getText().trim();
            name.checkError("* Enter Name");
            final String oEmail = email.getText().trim();
            email.checkError("* Enter Email");
            final String oPhone = phone.getText().trim();
            phone.checkError("* Enter Phone number");

            final String oPremise = premise.getText().trim();
            premise.checkError("* Enter Flat No/Appartment name");
            final String oSublocality = sublocality.getText().trim();
            sublocality.checkError("* Enter Locality/Colony name");
            final String oAddress = address.getText().trim();
            address.checkError("* Enter Address");
            final String oLandmark = landmark.getText().trim();

            final String oInstructions = instructions.getText().trim();

            //MOVING SCROLLVIEW TO NOT ENTERED COMPULSARY FIELDS
            vTextInputLayout []compulsaryInputFields = new vTextInputLayout[]{address, sublocality, premise, phone, email, name};
            for (int i=0;i<compulsaryInputFields.length;i++){
                if ( compulsaryInputFields[i].getText().length() == 0 ){
                    scrollView.focusToViewTop(compulsaryInputFields[i]);         // FOCUSING VIEW
                }
            }

            if ( oName.length() > 0 && oEmail.length() > 0 && oPremise.length() > 0 && oSublocality.length() > 0 && oAddress.length() > 0 && oPhone.length() > 0 ){//&& oSlot.length() > 0 ){

                if ( CommonMethods.isValidPhoneNumber(oPhone) ){

                    if ( CommonMethods.isValidEmailAddress(oEmail) == true ){

                        final String serverTime = localStorageData.getServerTime();

                        deliverLocationDetails.setPremise(oPremise);
                        deliverLocationDetails.setSublocalityLevel2(oSublocality);
                        deliverLocationDetails.setLandmark(oLandmark);

                        final RequestPlaceOrder requestPlaceOrder = new RequestPlaceOrder(deliverLocationDetails.getID(), oName, oPhone, oEmail, 0, oInstructions, deliverLocationDetails.getLatLng(), "", oPremise, oSublocality, deliverLocationDetails.getSublocalityLevel_1(), deliverLocationDetails.getLocality(), oLandmark, oAddress, deliverLocationDetails.getState(), deliverLocationDetails.getCountry(), deliverLocationDetails.getPostalCode());
                        placeOrder(requestPlaceOrder);

                    } else {
                        email.showError("*Enter a valid email address");
                    }
                }else{
                    phone.showError("*Enter a valid phone number (10 digits)");
                }
            }else{
                showToastMessage("* Enter all necessary fields");
            }
        }
    }

    public void placeOrder(final RequestPlaceOrder requestPlaceOrder) {

        showLoadingDialog("Placing your order, wait..");
        getBackendAPIs().getOrdersAPI().placeOrder(requestPlaceOrder, new OrdersAPI.PlaceOrderCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, OrderDetails orderDetails) {

                if (status == true) {

                    localStorageData.setBuyerName(requestPlaceOrder.getUserName());
                    localStorageData.setBuyerEmail(requestPlaceOrder.getUserEmail());
                    localStorageData.setBuyerMobile(requestPlaceOrder.getUserMobile());

                    localStorageData.setCartCount(0);
                    broadcastReceiversMethods.stopTimerCart();

                    orderDetails.setDeliveryAddress(requestPlaceOrder.getDeliveryAddress());
                    orderDetails.setDeliveryAddressLandmark(requestPlaceOrder.getLocality());

                    Intent orderSuccessAct = new Intent(context, OrderSuccessActivity.class);
                    orderSuccessAct.putExtra("order_details_json", new Gson().toJson(orderDetails));
                    if (requestPlaceOrder.getDeliverBy().equalsIgnoreCase("immediate")) {
                        orderSuccessAct.putExtra("isImmediate", true);
                    }
                    startActivity(orderSuccessAct);
                } else {

                    closeLoadingDialog();
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            showSnackbarMessage("No internet connection!", null, null);
                        }
                    } else if (statusCode == 403) {
                        clearUserSession("Your session has expired.");
                    } else {
                        Intent orderErrorAct = new Intent(context, OrderErrorActivity.class);
                        orderErrorAct.putExtra("status_code", statusCode);
                        orderErrorAct.putExtra("request_object_place_order", new Gson().toJson(requestPlaceOrder));
                        startActivity(orderErrorAct);
                    }
                }
            }
        });
    }


    //------ UI Methods

    public void hideSoftKeyboard() {
        inputMethodManager.hideSoftInputFromWindow(name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(email.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(premise.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(sublocality.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(landmark.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(address.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(phone.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
    }

    //==========

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cart_timer, menu);
        cartTimer = menu.getItem(0);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        hideSoftKeyboard();
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if ( name.getText().length() > 0 ){
            localStorageData.setBuyerName(name.getText());
        }
        if ( email.getText().length() > 0 ){
            localStorageData.setBuyerEmail(email.getText());
        }
        if ( phone.getText().length() > 0 ){
            localStorageData.setBuyerMobile(phone.getText());
        }

        if ( premise.getText().length() > 0 ){
            deliverLocationDetails.setPremise(premise.getText());
        }
        if ( landmark.getText().length() > 0 ){
            deliverLocationDetails.setLandmark(landmark.getText());
        }
        if (sublocality.getText().length() > 0 ){
            deliverLocationDetails.setSublocalityLevel2(sublocality.getText());
        }
        localStorageData.setDeliveryLocation(deliverLocationDetails);
    }

}
