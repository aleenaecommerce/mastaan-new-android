package in.gocibo.foodie.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import com.aleena.common.methods.CommonMethods;

import java.util.List;

import in.gocibo.foodie.GoCiboApplication;
import in.gocibo.foodie.backend.BackendAPIs;
import in.gocibo.foodie.backend.CartAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.BroadcastReceiversMethods;
import in.gocibo.foodie.models.CartItemDetails;

public class TimedCartService extends Service {

    GoCiboApplication goCiboApplication;
    Context context;

    CommonMethods commonMethods;
    LocalStorageData localStorageData;

    BroadcastReceiversMethods broadcastReceiversMethods;

    boolean sendBroadcast = true;
    CountDownTimer cartCountdownTimer;

    public TimedCartService() {}

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        goCiboApplication = (GoCiboApplication) getApplication();       // Accessing Application Class;
        context = getApplicationContext();

        commonMethods = new CommonMethods();
        localStorageData = new LocalStorageData(context);

        broadcastReceiversMethods = new BroadcastReceiversMethods(context);

        //cartValidTime = getResources().getInteger(R.integer.cart_valid_time);
        //cartValidTime = ;
        //Toast.makeText(context, "Valid Time = " + cartValidTime, Toast.LENGTH_LONG).show();

        cartCountdownTimer = new CountDownTimer(localStorageData.getCartRemainingTime(), 1000) {
            public void onTick(long millisUntilFinished) {

                if ( sendBroadcast == true ){
                    //Log.d(Constants.LOG_TAG, "Cart Remain TIme = "+(cartValidTime-millisUntilFinished)/1000);
                    broadcastReceiversMethods.updateCartRemainTime(millisUntilFinished);
                }
            }
            public void onFinish() {

                if ( sendBroadcast == true ){
                    try {
                        localStorageData.setCartCount(0);
                        localStorageData.setCartRemainingTime(0);
                        localStorageData.setCartStartTime("'");
                        broadcastReceiversMethods.clearShoppingCart(null);
                        goCiboApplication.showNotificationDialog("Timeout", "Sorry! your time is up. We had to empty your food basket.");

                        new BackendAPIs(context).getCartAPI().clearCartItems(new CartAPI.ClearCartItemsCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, List<CartItemDetails> cartItemDetailses) {
                                Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                                        .putExtra("type", "update_items_availability_on_cart_time_out");
                                context.sendBroadcast(homeReceiverIntent);
                            }
                        });
                    }catch (Exception e){}
                    //stopSelf();
                }
            }
        };
        cartCountdownTimer.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.LOG_TAG, "TIMED_CART Service is STARTED");
        return Service.START_STICKY;
    }

    //=====================

    @Override
    public void onDestroy() {
        super.onDestroy();
        sendBroadcast = false;
        Log.d(Constants.LOG_TAG, "TIMED_CART Service is FINISHED");
    }
}
