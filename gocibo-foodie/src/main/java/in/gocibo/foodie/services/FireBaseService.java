package in.gocibo.foodie.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vNotification;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.GoCiboApplication;
import in.gocibo.foodie.R;
import in.gocibo.foodie.activities.OrderDetailsActivity;
import in.gocibo.foodie.backend.BackendAPIs;
import in.gocibo.foodie.backend.CommonAPI;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.models.OrderDetails;

public class FireBaseService extends Service {

    BroadcastReceiver firebaseReceiver;
    boolean isActive = true;

    String firebaseToken;
    Firebase firebase;
    ArrayList<String> firebaseChilds = new ArrayList<>();

    GoCiboApplication goCiboApplication;
    Context context;

    CommonMethods commonMethods;
    LocalStorageData localStorageData;

    BackendAPIs backendAPIs;

    int notificationIconID;
    int notificationIndex = 0;

    public FireBaseService() {}

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.LOG_TAG, "FIRE_BASE Service is STARTED");

        Firebase.setAndroidContext(this);
        registerServiceReceiver();

        //-------------------

        goCiboApplication = (GoCiboApplication) getApplication();       // Accessing Application Class;
        context = getApplicationContext();

        commonMethods = new CommonMethods();
        localStorageData = new LocalStorageData(context);

        backendAPIs = new BackendAPIs(context);

        firebaseToken = localStorageData.getFireBaseToken();
        if (firebaseToken.length() == 0) {
            backendAPIs.getCommonAPI().getFirebaseToken(new CommonAPI.FirebaseTokenCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String firebase_token) {
                    if (status) {
                        firebaseToken = firebase_token;
                        localStorageData.setFireBaseToken(firebaseToken);
                        setupFireBase();
                    }
                }
            });
        } else {
            setupFireBase();
        }

        notificationIconID = R.drawable.ic_app_notification;
        /*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationIconID = R.drawable.ic_app_notification;
        }*/

        return Service.START_STICKY;
    }

    public void registerServiceReceiver(){

        IntentFilter intentFilter = new IntentFilter(Constants.FIREBASE_RECEIVER);
        firebaseReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent receivedData) {
                if ( isActive == true ) {
                    try {
                        if (receivedData.getStringExtra("type").equalsIgnoreCase("add_new_pending_order")) {
                            Log.d(Constants.LOG_TAG, "FIRE_BASE : Setting up Firebase updates for newly place order");
                            String order_details_json = receivedData.getStringExtra("order_details_json");
                            OrderDetails orderDetails = new Gson().fromJson(order_details_json, OrderDetails.class);
                            setupFirebaseUpdatesForOrder(orderDetails);
                        }
                    } catch (Exception e) {e.printStackTrace();}
                }
            }
        };
        this.registerReceiver(firebaseReceiver, intentFilter);  //registering receiver
    }

    public void setupFireBase() {

        firebase = new Firebase(getString(R.string.firebase_url));
        firebase.authWithCustomToken(firebaseToken, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                Log.d(Constants.LOG_TAG, "FIRE_BASE : Authentication is SUCCESS with Token = " + firebaseToken);

                List<OrderDetails> pendingOrders = localStorageData.getBuyerPendingOrders();
                for (int i = 0; i < pendingOrders.size(); i++) {
                    setupFirebaseUpdatesForOrder(pendingOrders.get(i)); //  processing, pending , pending , delivered , failure
                }
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                Log.d(Constants.LOG_TAG, "FIRE_BASE : Authentication is FAILED with Token = " + firebaseToken);
            }
        });
        //firebase.child("message").setValue("Data from Firebase2.");
    }

    public void setupFirebaseUpdatesForOrder(final OrderDetails orderDetails){

        if ( orderDetails != null ) {
            final String orderID = orderDetails.getID();
            //final int notificationIndex = ++ntfIndex;
            ++notificationIndex;

            Log.d(Constants.LOG_TAG, "FIRE_BASE :  Listening Updates for OrderID = " + orderID + " with NtfIndex = " + notificationIndex);

            firebaseChilds.add(localStorageData.getBuyerID() + "/o-" + orderID);
            firebase.child(localStorageData.getBuyerID() + "/o-" + orderID).addValueEventListener(new ValueEventListener() {

                boolean listenForUpdates;

                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if ( isActive == true ){
                        if (dataSnapshot.getValue() != null && listenForUpdates == true) {

                            String orderStatus = dataSnapshot.getValue().toString();

                            if (orderStatus.equalsIgnoreCase("delivered")) {
                                vNotification notification = new vNotification(context, notificationIconID, "Your order is delivered", "We look forward to your feedback.");
                                notification.setLaunchActivity(new Intent(context, OrderDetailsActivity.class).putExtra("orderID", orderID));
                                notification.show(notificationIndex);

                                firebase.child(localStorageData.getBuyerID() + "/o-" + orderID).removeEventListener(this);

                                updateOrderDetails(orderID, orderStatus);
                            } else if (orderStatus.equalsIgnoreCase("failure") || orderStatus.equalsIgnoreCase("failed")) {
                                vNotification notification = new vNotification(context, notificationIconID, "Sorry, your order's delivery has failed", "Please contact our support for more details.");
                                notification.setLaunchActivity(new Intent(context, OrderDetailsActivity.class).putExtra("orderID", orderID));
                                notification.show(notificationIndex);

                                firebase.child(localStorageData.getBuyerID() + "/o-" + orderID).removeEventListener(this);

                                updateOrderDetails(orderID, orderStatus);
                            }
                            Log.d(Constants.LOG_TAG, "FIRE_BASE :  New Status for OID = " + orderID + " is = " + orderStatus);
                        }
                        listenForUpdates = true;
                    }
                }

                @Override
                public void onCancelled(FirebaseError error) {
                }
            });

            // FOR SUB ORDER ITEMS......

            if (orderDetails.getOrderItems() != null && orderDetails.getOrderItems().size() > 0) {

                for (int j = 0; j < orderDetails.getOrderItems().size(); j++) {
                    if ( orderDetails.getOrderItems().get(j) != null ){
                        final String subOrderID = orderDetails.getOrderItems().get(j).getID();
                        //final int notificationIndexx = ++ntfIndex;
                        ++notificationIndex;

                        String portionMessage = orderDetails.getOrderItems().get(j).getQuantity() + " servings of ";

                        if ( orderDetails.getOrderItems().get(j).getDishQuantityType().equalsIgnoreCase("n")){
                            portionMessage = commonMethods.getInDecimalFormat(orderDetails.getOrderItems().get(j).getQuantity()) + " servings of ";
                            if ( orderDetails.getOrderItems().get(j).getQuantity() == 1 ){
                                portionMessage = "1 serving of ";
                            }
                        }
                        else if ( orderDetails.getOrderItems().get(j).getDishQuantityType().equalsIgnoreCase("w")){
                            portionMessage = commonMethods.getInDecimalFormat(orderDetails.getOrderItems().get(j).getQuantity()) + " kgs of ";
                            if ( orderDetails.getOrderItems().get(j).getQuantity() == 1 ){
                                portionMessage = "1 kg of ";
                            }
                        }
                        else if ( orderDetails.getOrderItems().get(j).getDishQuantityType().equalsIgnoreCase("v")){
                            portionMessage = commonMethods.getInDecimalFormat(orderDetails.getOrderItems().get(j).getQuantity()) + "L of ";
                        }

                        final String notificationMessage = portionMessage + commonMethods.capitalizeFirstLetter(orderDetails.getOrderItems().get(j).getDishName()) + " made by " + commonMethods.capitalizeFirstLetter(orderDetails.getOrderItems().get(j).getDishChefFullName()) + ".";

                        Log.d(Constants.LOG_TAG, "FIRE_BASE :  Listening Updates for subOrderID = " + subOrderID + " with NtfIndex = " + notificationIndex);

                        firebaseChilds.add(localStorageData.getBuyerID() + "/oi-" + subOrderID);
                        firebase.child(localStorageData.getBuyerID() + "/oi-" + subOrderID).addValueEventListener(new ValueEventListener() {

                            boolean listenForUpdates;

                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if ( isActive == true ){
                                    if (dataSnapshot.getValue() != null && listenForUpdates == true) {
                                        int orderDeliveryStatus = -1;
                                        try {
                                            orderDeliveryStatus = Integer.parseInt("" + dataSnapshot.getValue().toString());
                                        } catch (Exception e) {
                                        }

                                        if (orderDeliveryStatus == 2) {
                                            vNotification notification = new vNotification(context, notificationIconID, "Ready for pickup", notificationMessage);
                                            notification.setLaunchActivity(new Intent(context, OrderDetailsActivity.class).putExtra("orderID", orderID));
                                            notification.show(notificationIndex);

                                            updateSubOrderDetails(orderID, subOrderID, orderDeliveryStatus);
                                        } else if (orderDeliveryStatus == 3) {
                                            vNotification notification = new vNotification(context, notificationIconID, "Picked up by our delivery boy", notificationMessage);
                                            notification.setLaunchActivity(new Intent(context, OrderDetailsActivity.class).putExtra("orderID", orderID));
                                            notification.show(notificationIndex);

                                            updateSubOrderDetails(orderID, subOrderID, orderDeliveryStatus);
                                        } else if (orderDeliveryStatus == 4) {
                                            vNotification notification = new vNotification(context, notificationIconID, "Enjoy your food", notificationMessage);
                                            notification.setLaunchActivity(new Intent(context, OrderDetailsActivity.class).putExtra("orderID", orderID));
                                            notification.show(notificationIndex);

                                            firebase.child(localStorageData.getBuyerID() + "/oi-" + subOrderID).removeEventListener(this);

                                            updateSubOrderDetails(orderID, subOrderID, orderDeliveryStatus);
                                        } else if (orderDeliveryStatus == 5 || orderDeliveryStatus == 6) {
                                            vNotification notification = new vNotification(context, notificationIconID, "Order failed", notificationMessage);
                                            notification.setLaunchActivity(new Intent(context, OrderDetailsActivity.class).putExtra("orderID", orderID));
                                            notification.show(notificationIndex);

                                            firebase.child(localStorageData.getBuyerID() + "/oi-" + subOrderID).removeEventListener(this);

                                            updateSubOrderDetails(orderID, subOrderID, orderDeliveryStatus);
                                        }
                                        Log.d(Constants.LOG_TAG, "FIRE_BASE :  New Status for sOID = " + subOrderID + " is = " + orderDeliveryStatus);
                                    }
                                    listenForUpdates = true;
                                }
                            }

                            @Override
                            public void onCancelled(FirebaseError error) {}

                        });
                    }
                }
            }
        }
    }

    public void updateOrderDetails(String orderID, String orderStatus){

        Intent orderDetailsIntent = new Intent(Constants.ORDER_DETAILS_RECEIVER)
                .putExtra("type", "update_order_status")
                .putExtra("orderID", orderID)
                .putExtra("orderStatus", orderStatus);
        context.sendBroadcast(orderDetailsIntent);

        Intent orderHistoryIntent = new Intent(Constants.ORDER_HISTORY_RECEIVER)
                .putExtra("type", "update_order_status")
                .putExtra("orderID", orderID)
                .putExtra("orderStatus", orderStatus);
        context.sendBroadcast(orderHistoryIntent);

        if ( orderStatus.equalsIgnoreCase("delivered") || orderStatus.equalsIgnoreCase("failed") || orderStatus.equalsIgnoreCase("failure")){
            deleteFromPendingOrders(orderID);
            Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                    .putExtra("type", "display_user_session_info");
            context.sendBroadcast(homeReceiverIntent);
        }
    }
    public void updateSubOrderDetails(String orderID, String subOrderID, int subOrderStatus){
        Intent orderDetailsIntent = new Intent(Constants.ORDER_DETAILS_RECEIVER)
                .putExtra("type", "update_suborder_status")
                .putExtra("orderID", orderID)
                .putExtra("subOrderID", subOrderID)
                .putExtra("subOrderStatus", subOrderStatus);
        context.sendBroadcast(orderDetailsIntent);
    }

    public void deleteFromPendingOrders(String orderID){
        if ( orderID != null && orderID.length() > 0 ){
            boolean isDeleted = false;
            List<OrderDetails> pendingOrders = localStorageData.getBuyerPendingOrders();
            for (int i=0;i<pendingOrders.size();i++){
                OrderDetails pendingOrderDetails = pendingOrders.get(i);
                if ( pendingOrderDetails.getID().equals(orderID) ){
                    pendingOrders.remove(i);
                    isDeleted = true;
                    break;
                }
            }
            if ( isDeleted == true ){
                localStorageData.setBuyerPendingOrders(pendingOrders);
            }
        }
    }

    //==================

    @Override
    public void onDestroy() {
        super.onDestroy();
        isActive = false;
        try{
            context.unregisterReceiver(firebaseReceiver);
        }catch (Exception e){}
        Log.d(Constants.LOG_TAG, "FIRE_BASE Service is FINISHED");
    }
}
