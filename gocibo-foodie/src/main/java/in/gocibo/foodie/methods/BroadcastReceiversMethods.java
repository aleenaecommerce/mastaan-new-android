package in.gocibo.foodie.methods;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.models.CartItemDetails;
import in.gocibo.foodie.services.TimedCartService;

/**
 * Created by venkatesh on 4/8/15.
 */

public class BroadcastReceiversMethods {

    Context context;
    LocalStorageData localStorageData;

    Intent timedCartService;
    long cartValidTime;


    public BroadcastReceiversMethods(Context context){

        this.context = context;
        localStorageData = new LocalStorageData(context);

        timedCartService = new Intent(context, TimedCartService.class);
        cartValidTime = localStorageData.getCartValidTime();

    }

    public void updateAddressBookList() {
        Intent locationSelectionActivityReceiver = new Intent(Constants.LOCATION_SELECTION_RECEIVER)
                .putExtra("type", "update_address_book");
        context.sendBroadcast(locationSelectionActivityReceiver);
    }

    public void updateFoodItemAvailability(String  dishID, double latestAvailability){

        Intent updateFoodItemAvailabilityIntent = new Intent(Constants.HOME_RECEIVER)
                .putExtra("type", "update_food_item_availability")
                .putExtra("dishID", dishID)
                .putExtra("latestAvailability", latestAvailability);
        context.sendBroadcast(updateFoodItemAvailabilityIntent);
    }

    public void updateCartCount(int newCartCount){

        localStorageData.setCartCount(newCartCount);

        Intent updateCartCount = new Intent(Constants.HOME_RECEIVER)
                .putExtra("type", "update_cart_count");
        context.sendBroadcast(updateCartCount);
    }

    public void startTimerCart(){

        Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                .putExtra("type", "start_timed_cart");
        context.sendBroadcast(homeReceiverIntent);
    }

    public void updateCartRemainTime(long milliSecondsLeft){

        float progress = (cartValidTime-milliSecondsLeft)/(float)cartValidTime;

        String cTime = "";
        if ( ((milliSecondsLeft)/60000) == 0 ){
            cTime = ((milliSecondsLeft%60000)/1000)+"s";
        }else{
            cTime = ((milliSecondsLeft)/60000)+"m "+((milliSecondsLeft%60000)/1000)+"s";
        }
        //localStorageData.setCartRemainingTime(milliSecondsLeft);

        Intent cartReceiverIntent = new Intent(Constants.CART_RECEIVER)
                .putExtra("type", "update_cart_remail_valid_time")
                .putExtra("remain_time", cTime);
        context.sendBroadcast(cartReceiverIntent);

        Intent orderBookingIntent = new Intent(Constants.ORDER_BOOKING_RECEIVER)
                .putExtra("type", "update_cart_remail_valid_time")
                .putExtra("remain_time", cTime);
        context.sendBroadcast(orderBookingIntent);

        Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                .putExtra("type", "update_timer_cart_countdown_indicator")
                .putExtra("progress", progress);
        context.sendBroadcast(homeReceiverIntent);
    }

    public void stopTimerCart(){

        context.stopService(timedCartService);

        Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                .putExtra("type", "stop_timed_cart");
        context.sendBroadcast(homeReceiverIntent);
    }

    public void clearShoppingCart(List<CartItemDetails> cartItems){

        ArrayList<Map<String, Object>> cart_items = new ArrayList<Map<String, Object>>();
        if ( cartItems != null ) {
            for (int i = 0; i < cartItems.size(); i++) {
                Map<String, Object> cart_item = new HashMap<String, Object>();
                cart_item.put("dishID", cartItems.get(i).getDishID());
                cart_item.put("newAvailability", cartItems.get(i).getDishCurrentAvailability() + cartItems.get(i).getQuantity());
                cart_items.add(cart_item);
            }
        }

        Intent orderBookingIntent = new Intent(Constants.ORDER_BOOKING_RECEIVER)
                .putExtra("type", "clear_cart");
        context.sendBroadcast(orderBookingIntent);

        Intent cartReceiverIntent = new Intent(Constants.CART_RECEIVER)
                .putExtra("type", "clear_cart");
        context.sendBroadcast(cartReceiverIntent);

        Intent homeReceiverIntent = new Intent(Constants.HOME_RECEIVER)
                .putExtra("type", "stop_timed_cart")
                .putExtra("cart_items", cart_items);
        context.sendBroadcast(homeReceiverIntent);

    }
}
