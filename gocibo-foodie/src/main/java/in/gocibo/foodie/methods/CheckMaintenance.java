package in.gocibo.foodie.methods;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import androidx.core.content.IntentCompat;


import org.json.JSONObject;

import in.gocibo.foodie.activities.LaunchActivity;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;

/**
 * Created by venkatesh on 25/3/16.
 */
public  class CheckMaintenance {

    Context context;

    public CheckMaintenance(Context context){
        this.context = context;
    }

    public boolean check(RetrofitError error){

        Activity activity = (Activity) context;
        try {
            String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
            JSONObject jsonResponse = new JSONObject(response_json_string);
            if ( jsonResponse.getBoolean("maintenance")) {

                Intent launchAct = new Intent(context, LaunchActivity.class);
                ComponentName componentName = launchAct.getComponent();
              //  Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
               // activity.startActivity(mainIntent);

                return true;
            }
        }catch (Exception e){}

        return false;
    }
}
