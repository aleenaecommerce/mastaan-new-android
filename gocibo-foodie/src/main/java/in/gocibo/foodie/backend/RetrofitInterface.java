package in.gocibo.foodie.backend;

import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.ResponseStatus;

import java.util.List;

import in.gocibo.foodie.backend.model.RequestAddAddress;
import in.gocibo.foodie.backend.model.RequestAddToCart;
import in.gocibo.foodie.backend.model.RequestAllergiesUpdate;
import in.gocibo.foodie.backend.model.RequestGetCategories;
import in.gocibo.foodie.backend.model.RequestGetItemsAvailability;
import in.gocibo.foodie.backend.model.RequestGetUploadsCount;
import in.gocibo.foodie.backend.model.RequestOrderFeedback;
import in.gocibo.foodie.backend.model.RequestPlaceOrder;
import in.gocibo.foodie.backend.model.RequestProfileUpdate;
import in.gocibo.foodie.backend.model.RequestRegisterPushNotifications;
import in.gocibo.foodie.backend.model.RequestSearchUploads;
import in.gocibo.foodie.backend.model.RequestUpdateCartItem;
import in.gocibo.foodie.backend.model.ResponseAddCartItem;
import in.gocibo.foodie.backend.model.ResponseAddorRemoveCoupon;
import in.gocibo.foodie.backend.model.ResponseChangeCartSlot;
import in.gocibo.foodie.backend.model.ResponseChefDishes;
import in.gocibo.foodie.backend.model.ResponseGetCartItems;
import in.gocibo.foodie.backend.model.ResponseGetCategories;
import in.gocibo.foodie.backend.model.ResponseGetCurrentSlot;
import in.gocibo.foodie.backend.model.ResponseGetFirebaseToken;
import in.gocibo.foodie.backend.model.ResponseGetUploadsCount;
import in.gocibo.foodie.backend.model.ResponseRemoveCartItem;
import in.gocibo.foodie.backend.model.ResponseSearchUploads;
import in.gocibo.foodie.backend.model.ResponseUpdateCartItem;
import in.gocibo.foodie.models.AllergyDetails;
import in.gocibo.foodie.models.BootStrapDetails;
import in.gocibo.foodie.models.BuyerDetails;
import in.gocibo.foodie.models.ChefDetails;
import in.gocibo.foodie.models.DishDetails;
import in.gocibo.foodie.backend.model.ResponseValidateEmail;
import in.gocibo.foodie.backend.model.ResponseVerifyEmail;
import in.gocibo.foodie.models.FoodItemDetails;
import in.gocibo.foodie.backend.model.ResponseValidateMobile;
import in.gocibo.foodie.backend.model.ResponseVerifyMobile;
import in.gocibo.foodie.models.OrderDetails;
import in.gocibo.foodie.models.ProfileSummaryDetails;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by venkatesh on 29/6/15.
 */

public interface RetrofitInterface {

    //-------------- General API Calls --------------//

    @GET("/buyer/currentslot")
    void getCurrentSlot(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<ResponseGetCurrentSlot> callback);

    @GET("/buyer/bootstrap")
    void getBootStrap(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Query("appVersion") int appVersion, Callback<BootStrapDetails> callback);

    @GET("/general/allergens")
    void getAvailableAllergies(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<List<AllergyDetails>> callback);

    @GET("/buyer/getrttoken")
    void getFireBaseToken(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<ResponseGetFirebaseToken> callback);

    //-------------- Authentication API Calls --------------//

    @GET("/general/validatenumber1/{phonenumber}")
    void validatePhoneNumber1(@Query("i") String deviceID, @Path("phonenumber") String phonenumber, Callback<ResponseValidateMobile> callback);

    @GET("/general/verifynumber1/buyer/{smsid}/{otp}")
    void verifyPhoneNumber1(@Query("i") String deviceID, @Path("smsid") String smsid, @Path("otp") String otp, Callback<ResponseVerifyMobile> callback);

    @GET("/buyer/verifyemail/{email}")
    void validateEmail(@Query("i") String deviceID, @Path("email") String emailAddress, Callback<ResponseValidateEmail> callback);

    @GET("/buyer/validateemail/{pinid}/{pin}")
    void verifyEmail(@Query("i") String deviceID, @Path("pinid") String pinid, @Path("pin") String pin, Callback<ResponseVerifyEmail> callback);


    @GET("/buyer/connect")
    void getBuyerDetails(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<BuyerDetails> callback);

    @GET("/general/disconnect")
    void confirmLogout(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<ResponseStatus> callback);

    //-------------- Push Notifications -----------------//

    @POST("/buyer/registerpush")
    void registerForPushNotifications(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestRegisterPushNotifications requestObj, Callback<ResponseStatus> callback);

    @POST("/buyer/registerbuyerpush")
    void registerBuyerForPushNotifications(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestRegisterPushNotifications requestObj, Callback<ResponseStatus> callback);

    //-------------- AddressBook API Calls --------------//

    @GET("/buyer/addressbook")
    void getFavourites(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<List<AddressBookItemDetails>> callback);

    @POST("/buyer/addaddress")
    void addAddress(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestAddAddress userObj, Callback<RequestAddAddress> callback);

    @POST("/buyer/updateaddress")
    void updateAddress(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestAddAddress userObj, Callback<ResponseStatus> callback);

    @GET("/buyer/deleteaddress/{addr_id}")
    void deleteAddress(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("addr_id") String addr_id, Callback<ResponseStatus> callback);

    //-------------- FoodItems API Calls --------------//

    @POST("/buyer/searchuploads")
    void searchUploads(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestSearchUploads requestSearchUploads, Callback<ResponseSearchUploads> callback);

    @GET("/buyer/uploaddetails/{dish_id}")
    void getFoodItemDetails(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("dish_id") String dish_id, Callback<FoodItemDetails> callback);

    @POST("/buyer/getdishavailability")
    void getItemsAvailability(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestGetItemsAvailability requestGetItemsAvailability, Callback<List<FoodItemDetails>> callback);

    @POST("/buyer/categories")
    void getCategories(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestGetCategories requestGetCategories, Callback<ResponseGetCategories> callback);

    @POST("/buyer/countuploads")
    void getUploadsCount(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestGetUploadsCount requestGetUploadsCount, Callback<ResponseGetUploadsCount> callback);

    //-------------- Chefs API Calls --------------//

    @GET("/buyer/registerchef/{phoneNo}")
    void registerChef(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("phoneNo") String phoneNo, Callback<ResponseStatus> callback);

    @GET("/general/chefs")
    void getChefsList(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<List<ChefDetails>> callback);

    @GET("/general/chefdetails/{chef_id}")
    void getChefDetails(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("chef_id") String chef_id, Callback<ChefDetails> callback);

    @GET("/buyer/chefdishes/{chef_id}")
    void getChefDishes(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("chef_id") String chef_id, Callback<List<ResponseChefDishes>> callback);

    @GET("/buyer/dishdetails/{dish_id}")
    void getChefItemDetails(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("dish_id") String dish_id, Callback<DishDetails> callback);

    //-------------- Cart API Calls --------------//

    @GET("/buyer/cart")
    void getCartItems(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<ResponseGetCartItems> callback);

    @GET("/buyer/clearcart/true")
    void clearCartItems(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<ResponseStatus> callback);

    @POST("/buyer/addtocart/{dish_id}/{num}")
    void addToCart(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestAddToCart addObject, @Path("dish_id") String dish_id, @Path("num") double num, Callback<ResponseAddCartItem> callback);

    @POST("/buyer/updatecart")
    void updateCartItem(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestUpdateCartItem updateObj, Callback<ResponseUpdateCartItem> callback);

    @GET("/buyer/removefromcart/{dish_id}/{num}/{dby}")
    void removeFromCart(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("dish_id") String dish_id, @Path("num") double num, @Path("dby") String dby, Callback<ResponseRemoveCartItem> callback);

    @GET("/buyer/changecartslot/{new_slot}")
    void changeCartSlot(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("new_slot") String new_slot, Callback<ResponseChangeCartSlot> callback);

    @GET("/buyer/confirmchangecartslot/{new_slot}")
    void removeMisMatchItemsAndChangeCartSlot(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("new_slot") String new_slot, Callback<ResponseChangeCartSlot> callback);

    //-------------- COUPONS API CALLS -----------//

    @GET("/buyer/applycouponcode/{code}")
    void applyCoupon(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("code") String couponCode, Callback<ResponseAddorRemoveCoupon> callback);

    @GET("/buyer/clearcouponcode")
    void removeCoupon(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<ResponseAddorRemoveCoupon> callback);

    //-------------- Order API Calls --------------//

    @POST("/buyer/placeorder")
    void placeOrder(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestPlaceOrder orderObj, Callback<OrderDetails> callback);

    @GET("/buyer/orderdetails/{order_Id}")
    void getOrderDetails(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("order_Id") String order_Id, Callback<OrderDetails> callback);

    @GET("/buyer/orderhistory")
    void getOrders(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<List<OrderDetails>> callback);

    @GET("/buyer/pendingorders")
    void getPendingOrders(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<List<OrderDetails>> callback);

    //-------------- FeedBack API Calls --------------//

    @POST("/buyer/postfeedback/{order_id}")
    void postOrderFeedback(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestOrderFeedback requestOrderFeedback, @Path("order_id") String order_id, Callback<ResponseStatus> callback);

    @GET("/buyer/skipfeedback/{order_id}")
    void skipOrderFeedback(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Path("order_id") String order_id, Callback<ResponseStatus> callback);


    //-------------- Profile API Calls --------------//

    @GET("/buyer/profile")
    void getProfileDetails(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<ProfileSummaryDetails> callback);

    @POST("/buyer/update")
    void updateProfile(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestProfileUpdate profileUpdateRequest, Callback<ResponseStatus> callback);

    @GET("/buyer/allergies")
    void getBuyerAllergies(@Query("i") String deviceID, @Query("accessToken") String accessToken, Callback<List<AllergyDetails>> callback);

    @POST("/buyer/updateallergies")
    void updateBuyerAllergies(@Query("i") String deviceID, @Query("accessToken") String accessToken, @Body RequestAllergiesUpdate allergiesUpdateRequest, Callback<ResponseStatus> callback);

    //-----------------------------------------------//

}