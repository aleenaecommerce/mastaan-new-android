package in.gocibo.foodie.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;

import in.gocibo.foodie.backend.model.ResponseValidateEmail;
import in.gocibo.foodie.backend.model.ResponseValidateMobile;
import in.gocibo.foodie.backend.model.ResponseVerifyEmail;
import in.gocibo.foodie.backend.model.ResponseVerifyMobile;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.CheckMaintenance;
import in.gocibo.foodie.models.BuyerDetails;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by venkatesh on 13/7/15.
 */

public class AuthenticationAPI {

    Context context;
    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface MobileValidationCallback {
        void onComplete(boolean status, int statusCode, ResponseValidateMobile responseValidateMobile);
    }

    public interface MobileVerificationCallback {
        void onComplete(boolean status, int statusCode, ResponseVerifyMobile responseVerifyMobile);
    }

    public interface EmailValidationCallback {
        void onComplete(boolean status, int statusCode, String message, ResponseValidateEmail responseValidateEmail);
    }

    public interface EmailVerificationCallback {
        void onComplete(boolean status, int statusCode, String message, ResponseVerifyEmail responseVerifyEmail);
    }

    public interface BuyerDetailsCallback {
        void onComplete(boolean status, int statusCode, BuyerDetails buyerDetails);
    }

    public AuthenticationAPI(Context context, RestAdapter restAdapter) {
        this.context = context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void validatePhoneNumber(final String mobileNumber, final MobileValidationCallback callback){

        retrofitInterface.validatePhoneNumber1(localStorageData.getDeviceID(), mobileNumber, new Callback<ResponseValidateMobile>() {
            @Override
            public void success(ResponseValidateMobile responseValidateMobile, Response response) {

                if (responseValidateMobile.getError() != null && responseValidateMobile.getError().length() > 0) {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-validateNumber1 : Success , FailureMsg=" + responseValidateMobile.getError());
                    callback.onComplete(false, 200, responseValidateMobile);
                } else {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-validateNumber1 : Success , SMSID=" + responseValidateMobile.getSmsID());
                    callback.onComplete(true, 500, responseValidateMobile);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-validateNumber1 : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), new ResponseValidateMobile());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-validateNumber1 : Error = " + error.getKind());
                        callback.onComplete(false, -1, new ResponseValidateMobile());
                    }
                }
            }
        });
    }

    public void verifyPhoneNumber(final String smsID, final String otp, final MobileVerificationCallback callback){

        retrofitInterface.verifyPhoneNumber1(localStorageData.getDeviceID(), smsID, otp, new Callback<ResponseVerifyMobile>() {
            @Override
            public void success(ResponseVerifyMobile responseVerifyMobile, Response response) {

                if (responseVerifyMobile.getToken() != null && responseVerifyMobile.getToken().length() > 0) {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-verifyNumber1 : Success , Token=" + responseVerifyMobile.getToken());
                    callback.onComplete(true, 200, responseVerifyMobile);
                } else {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-verifyNumber1 : Failure");
                    callback.onComplete(false, 500, responseVerifyMobile);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-verifyNumber1 : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-verifyNumber1 : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    //================================

    public void validateEmail(final String emailAddress, final EmailValidationCallback callback){

        retrofitInterface.validateEmail(localStorageData.getDeviceID(), emailAddress, new Callback<ResponseValidateEmail>() {
            @Override
            public void success(ResponseValidateEmail responseValidateEmail, Response response) {

                if (responseValidateEmail != null && responseValidateEmail.getPinID() != null && responseValidateEmail.getPinID().length() > 0) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Success , PINID=" + responseValidateEmail.getPinID());
                    callback.onComplete(true, 200, "OK", responseValidateEmail);
                } else {
                    if (responseValidateEmail != null && responseValidateEmail.getCode() != null && responseValidateEmail.getCode().length() > 0) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Failure, Message = " + responseValidateEmail.getCode());
                        callback.onComplete(false, 500, responseValidateEmail.getCode(), null);
                    } else {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Failure");
                        callback.onComplete(false, 500, "Failure", null);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", new ResponseValidateEmail());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", new ResponseValidateEmail());
                    }
                }
            }
        });
    }

    public void verifyEmail(final String pinID, final String pin, final EmailVerificationCallback callback){

        retrofitInterface.verifyEmail(localStorageData.getDeviceID(), pinID, pin, new Callback<ResponseVerifyEmail>() {
            @Override
            public void success(ResponseVerifyEmail responseVerifyEmail, Response response) {

                if (responseVerifyEmail != null && responseVerifyEmail.getToken() != null && responseVerifyEmail.getToken().length() > 0 && responseVerifyEmail.getBuyerDetails() != null) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Success , Token=" + responseVerifyEmail.getToken());
                    callback.onComplete(true, 200, "OK", responseVerifyEmail);
                } else {
                    if (responseVerifyEmail != null && responseVerifyEmail.getCode() != null && responseVerifyEmail.getCode().length() > 0) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Failure, Message = " + responseVerifyEmail.getCode());
                        callback.onComplete(false, 500, responseVerifyEmail.getCode(), responseVerifyEmail);
                    } else {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Failure");
                        callback.onComplete(false, 500, "Failure", responseVerifyEmail);
                    }
                }//5616253d732e6a53433250a3
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", new ResponseVerifyEmail());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", new ResponseVerifyEmail());
                    }
                }
            }
        });
    }

    public void getBuyerDetails(final String accessToken, final BuyerDetailsCallback callback){

        retrofitInterface.getBuyerDetails(localStorageData.getDeviceID(), accessToken, new Callback<BuyerDetails>() {
            @Override
            public void success(BuyerDetails buyerDetails, Response response) {

                if (buyerDetails != null && buyerDetails.getMobile() != null && buyerDetails.getMobile().length() > 0) {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-buyerDetails : Success , Mobile=" + buyerDetails.getMobile() + " , for ID = " + buyerDetails.getID());
                    //localStorageData.setPushNotificationsRegistratinStatus(false);      // Clearing For BuyerPush from JustPush
                    callback.onComplete(true, 200, new BuyerDetails(buyerDetails.getID(), buyerDetails.getJoinedDate(), buyerDetails.getMobile(), buyerDetails.getName(), buyerDetails.getEmail(), buyerDetails.getDOB(), buyerDetails.getMatrialStatus(), buyerDetails.getAnniversary(), buyerDetails.getReferralID()));
                } else {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-buyerDetails : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-buyerDetails : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-buyerDetails : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void confirmLogout(final StatusCallback callback){

        retrofitInterface.confirmLogout(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ LOGOUT ]  >>  Success");
                    localStorageData.setPushNotificationsRegistratinStatus(false);      // Clearing For JustPush from BuyerPush
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "nCOMMON_API : [ LOGOUT ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ LOGOUT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ LOGOUT ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


}
