package in.gocibo.foodie.backend.model;

import com.aleena.common.methods.CommonMethods;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.models.CuisineDetails;
import in.gocibo.foodie.models.FoodItemDetails;

/**
 * Created by venkatesh on 15/7/15.
 */

public class ResponseSearchUploads {

    String message;
    List<FoodItemDetails> results;
    List<CuisineDetails> cuisines;

    String code;
    int statusCode;
    boolean retryable;
    int retryDelay;

    public ResponseSearchUploads(List<FoodItemDetails> foodItems, List<CuisineDetails> cuisines){
        this.results = foodItems;
        this.cuisines = cuisines;
    }

    public List<FoodItemDetails> getFoodItems() {
        return results;
    }

    public List<CuisineDetails> getCuisines() {
        return cuisines;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getRetryDelay() {
        return retryDelay;
    }

    public int getStatusCode() {
        return statusCode;
    }

}
