package in.gocibo.foodie.backend.model;

/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestProfileUpdate {

    String f;
    String l;
    String e;
    String m;
    String d;
    boolean mr;
    String a;

    public RequestProfileUpdate(String firstName, String lastName, String emailAddress, String mobileNumber, String birthday, boolean matrialStatus, String anniversary){
        this.f = firstName;
        this.l = lastName;
        this.e = emailAddress;
        this.m = mobileNumber;
        this.d = birthday;
        this.mr = matrialStatus;
        this.a = anniversary;
    }

    public String getMobile() {      return m;   }

    public String getEmail() {
        return e;
    }

    public String getName() {
        return f;
    }

    public String getDOB() {
        return d;
    }

    public boolean getMatrialStaus() {
        return mr;
    }

    public String getAnniversary() {
        return a;
    }

}
