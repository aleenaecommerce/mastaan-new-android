package in.gocibo.foodie.backend.model;

/**
 * Created by venkatesh on 9/11/15.
 */
public class ResponseAddCartItem {

    String code;
    boolean alreadyPresent;
    double latestAvailability;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
    public boolean isAlreadyPresent(){  return  alreadyPresent; }

    public double getLatestAvailability() {
        return latestAvailability;
    }


}
