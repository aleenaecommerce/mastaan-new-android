package in.gocibo.foodie.backend.model;

/**
 * Created by venkatesh on 8/10/15.
 */
public class ResponseValidateEmail {

    String code;
    String pinid;

    public String getPinID() {
        return pinid;
    }

    public String getCode() {
        return code;
    }
}
