package in.gocibo.foodie.backend.model;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.models.OrderFeedback;

/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestOrderFeedback {

    //String buyer_mobile;
    //String order_id;
    List<OrderFeedback> feedbacks;
    boolean remind;

    public RequestOrderFeedback(){
        feedbacks = new ArrayList<>();
        remind = false;
    }

    public void addFeedback(String orderID, String dishID, float rating, String comments){
        feedbacks.add(new OrderFeedback(orderID, dishID, rating, comments));
    }

    public void addFeedback(OrderFeedback orderFeedback){
        feedbacks.add(orderFeedback);
    }

    public void setRemindLater(boolean remindLater) {
        this.remind = remindLater;
    }

    public List<OrderFeedback> getFeedbacks() {
        if ( feedbacks == null ){
            return  new ArrayList<>();
        }
        return feedbacks;
    }

}
