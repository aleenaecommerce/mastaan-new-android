package in.gocibo.foodie.backend.model;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.models.CartItemDetails;

/**
 * Created by venkatesh on 15/7/15.
 */

public class ResponseGetCartItems {

    int processingFees; // In Rupees
    String serverTime;              //  Servertime

    double govtTaxes;
    double serviceCharges;
    double ta;

    List<CartItemDetails> cart;


    public int getProcessingFees() {
        return processingFees;
    }

    public List<CartItemDetails> getCartItems() {
        if ( cart == null ){ cart = new ArrayList<>();   }
        return cart;
    }

    public String getServerTime() {
        return serverTime;
    }

    public double getCartGovtTaxes() {
        return govtTaxes;
    }

    public double getCartServiceCharges() {
        return serviceCharges;
    }

    public double getCartTotalAmount() {
        return ta;
    }


}
