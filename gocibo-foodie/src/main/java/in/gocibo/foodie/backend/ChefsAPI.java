package in.gocibo.foodie.backend;


import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.backend.model.ResponseChefDishes;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.CheckMaintenance;
import in.gocibo.foodie.models.ChefDetails;
import in.gocibo.foodie.models.DishDetails;
import in.gocibo.foodie.models.FoodItemDetails;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ChefsAPI {

    Context context;
    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface ChefsListCallback {
        void onComplete(boolean status, int statusCode, List<ChefDetails> chefsList);
    }

    public interface ChefDetailsCallback {
        void onComplete(boolean status, int statusCode, ChefDetails chefDetails);
    }

    public interface ChefFoodItemsCallback {
        void onComplete(boolean status, int statusCode, List<FoodItemDetails> chefFoodItems);
    }

    public interface ChefItemDetailsCallback{
        void onComplete(boolean status, int statusCode, DishDetails dishDetails);
    }

    public ChefsAPI(Context context, RestAdapter restAdapter) {
        this.context = context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void registerChef(final String phoneNo, final StatusCallback callback){

        retrofitInterface.registerChef(localStorageData.getDeviceID(), localStorageData.getAccessToken(), phoneNo, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nCHEFS_API - RegisterChef : Success , For = " + phoneNo);
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nCHEFS_API - RegisterChef : Failure , For = " + phoneNo);
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCHEFS_API - RegisterChef : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " For = " + phoneNo);
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCHEFS_API - RegisterChef : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getChefsList(final ChefsListCallback callback){

        retrofitInterface.getChefsList(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new Callback<List<ChefDetails>>() {
            @Override
            public void success(List<ChefDetails> chefsList, Response response) {

                if (chefsList != null) {
                    Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefsList : Success , Found = " + chefsList.size());
                    callback.onComplete(true, 200, chefsList);
                } else {
                    Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefsList : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefsList : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefsList : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getChefDetails(final String chefID, final ChefDetailsCallback callback){

        retrofitInterface.getChefDetails(localStorageData.getDeviceID(), localStorageData.getAccessToken(), chefID, new Callback<ChefDetails>() {
            @Override
            public void success(ChefDetails chefDetails, Response response) {

                if (chefDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefsDetails : Success , For " + chefDetails.getFullName());
                    callback.onComplete(true, 200, chefDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefsDetails : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefsDetails : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefsDetails : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getChefDishes(final String chefID, final ChefFoodItemsCallback callback){

        retrofitInterface.getChefDishes(localStorageData.getDeviceID(), localStorageData.getAccessToken(), chefID, new Callback<List<ResponseChefDishes>>() {
            @Override
            public void success(List<ResponseChefDishes> responseChefDishs, Response response) {

                if (responseChefDishs != null) {
                    List<FoodItemDetails> chefDishes = new ArrayList<FoodItemDetails>();
                    for (int i = 0; i < responseChefDishs.size(); i++) {
                        ResponseChefDishes chefDish = responseChefDishs.get(i);
                        chefDishes.add(new FoodItemDetails(chefDish._id, chefDish.n, 0, chefDish.p, chefDish.th, chefDish.t, 0, chefDish.c, "", ""));
                    }

                    Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefDishes : Success , Found = " + responseChefDishs.size() + " , for (chef_id = " + chefID + " )");
                    callback.onComplete(true, 200, chefDishes);
                } else {
                    Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefDishes : Failure , for (chef_id = " + chefID + " )");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefDishes : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCHEFS_API - ChefDishes : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });

    }

    // Get Food Item Details..
    public void getChefItemDetails(final String dishID, final ChefItemDetailsCallback callback){

        retrofitInterface.getChefItemDetails(localStorageData.getDeviceID(), localStorageData.getAccessToken(), dishID, new Callback<DishDetails>() {
            @Override
            public void success(DishDetails dishDetails, Response response) {
                if (dishDetails != null && dishDetails.getID() != null) {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - FoodItemDetails : Success , Found = " + dishDetails.getName());
                    callback.onComplete(true, 200, dishDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - FoodItemDetails : Failure ,for (id=" + dishID + ")");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - FoodItemDetails : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " ,for (id=" + dishID + ")");
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - FoodItemDetails : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }



}