package in.gocibo.foodie.backend.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 10/12/15.
 */
public class RequestGetUploadsCount {
    String l;

    public RequestGetUploadsCount(LatLng location){
        if ( location != null ){
            l = location.latitude+","+location.longitude;
        }
    }
}
