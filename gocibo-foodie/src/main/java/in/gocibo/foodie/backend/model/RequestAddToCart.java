package in.gocibo.foodie.backend.model;

import com.aleena.common.models.AddressBookItemDetails;

/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestAddToCart {

    String adid;    //  Delivery Address ID
    String pos;     //  Delivery Address LatLng
    String ad;      //  Delivery Address Full Name

    String dby;     //  Delivery Slot

    public RequestAddToCart(String deliveryAddressLatLng, String deliveryAddressFullName){
        this.pos = deliveryAddressLatLng;
        this.ad = deliveryAddressFullName;
    }

    public RequestAddToCart(AddressBookItemDetails deliveryLocation, String deliverySlot){
        if ( deliveryLocation != null ){
            if ( deliveryLocation.getLatLng() != null ){
                this.pos = deliveryLocation.getLatLng().latitude+","+deliveryLocation.getLatLng().longitude;
            }else{
                this.pos = "0,0";
            }
            this.ad = deliveryLocation.getFullAddress();
            this.adid = deliveryLocation.getID();
        }
        this.dby = deliverySlot;
    }

}
