package in.gocibo.foodie.backend.model;

/**
 * Created by venkatesh on 15/7/15.
 */

public class ResponseGetFirebaseToken {

    String code;
    String data;

    public String getCode() {
        return code;
    }

    public String getFirebaseToken() {
        return data;
    }
}
