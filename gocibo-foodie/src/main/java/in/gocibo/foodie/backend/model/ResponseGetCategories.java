package in.gocibo.foodie.backend.model;

import java.util.List;

import in.gocibo.foodie.models.CategoryDetails;

/**
 * Created by venkatesh on 11/12/15.
 */
public class ResponseGetCategories {

    long total;
    List<CategoryDetails> categories;

    public long getTotal() {
        return total;
    }

    public List<CategoryDetails> getCategories() {
        return categories;
    }
}
