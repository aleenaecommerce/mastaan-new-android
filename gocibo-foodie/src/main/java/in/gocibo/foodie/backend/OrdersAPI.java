package in.gocibo.foodie.backend;


import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;

import java.util.List;

import in.gocibo.foodie.backend.model.RequestOrderFeedback;
import in.gocibo.foodie.backend.model.RequestPlaceOrder;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.CheckMaintenance;
import in.gocibo.foodie.models.OrderDetails;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class OrdersAPI {

    Context context;
    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;


    public interface PlaceOrderCallback {
        void onComplete(boolean status, int statusCode, OrderDetails orderDetails);
    }

    public interface OrderDetailsCallback {
        void onComplete(boolean status, int statusCode, OrderDetails orderDetails);
    }

    public interface OrdersListCallback {
        void onComplete(boolean status, int statusCode, List<OrderDetails> previousOrders);
    }

    public interface PendingOrdersCallback {
        void onComplete(boolean status, int statusCode, List<OrderDetails> pendingOrders);
    }

    public OrdersAPI(Context context, RestAdapter restAdapter) {
        this.context = context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }


    public void placeOrder(final RequestPlaceOrder placeOrderObject, final PlaceOrderCallback callback){

        retrofitInterface.placeOrder(localStorageData.getDeviceID(), localStorageData.getAccessToken(), placeOrderObject, new retrofit.Callback<OrderDetails>() {
            @Override
            public void success(OrderDetails orderDetails, Response response) {

                if (orderDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - -PlaceOrder : Success , OrderID = " + orderDetails.getOrderID());
                    callback.onComplete(true, 200, orderDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - PlaceOrder : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - PlaceOrder : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - PlaceOrder : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getOrderDetails(final String orderID, final OrderDetailsCallback callback){

        retrofitInterface.getOrderDetails(localStorageData.getDeviceID(), localStorageData.getAccessToken(), orderID, new Callback<OrderDetails>() {
            @Override
            public void success(OrderDetails orderDetails, Response response) {

                if (orderDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderDetails : Success , for (order_id = " + orderID + " )");
                    callback.onComplete(true, 200, orderDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderDetails : Failure , for (order_id = " + orderID + " )");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderDetails : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " , for (order_id = " + orderID + " )");
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderDetails : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getOrders(final OrdersListCallback callback){

        retrofitInterface.getOrders(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new retrofit.Callback<List<OrderDetails>>() {
            @Override
            public void success(List<OrderDetails> myOrders, Response response) {

                if (myOrders != null) {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - MyOrders : Success , Found = " + myOrders.size());
                    callback.onComplete(true, 200, myOrders);
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - MyOrders : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - MyOrders : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - MyOrders : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });

    }

    public void getPendingOrders(final PendingOrdersCallback callback){

        retrofitInterface.getPendingOrders(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new retrofit.Callback<List<OrderDetails>>() {
            @Override
            public void success(List<OrderDetails> pendingOrders, Response response) {

                if (pendingOrders != null) {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - PendingOrders : Success , Found = " + pendingOrders.size());
                    callback.onComplete(true, 200, pendingOrders);
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - PendingOrders : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - PendingOrders : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - PendingOrders : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });

    }

    public void postOrderFeedback(final String orderID, RequestOrderFeedback requestOrderFeedback, final StatusCallback callback){

        retrofitInterface.postOrderFeedback(localStorageData.getDeviceID(), localStorageData.getAccessToken(), requestOrderFeedback, orderID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {

                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderFeedBack : Success , for order_id = " + orderID);
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderFeedBack : Failure , for order_id = " + orderID);
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderFeedBack : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderFeedBack : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void skipOrderFeedback(final String orderID, final StatusCallback callback){

        retrofitInterface.skipOrderFeedback(localStorageData.getDeviceID(), localStorageData.getAccessToken(), orderID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {

                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderFeedBack-SKIP : Success , for order_id = " + orderID);
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderFeedBack-SKIP : Failure , for order_id = " + orderID);
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderFeedBack-SKIP : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - OrderFeedBack-SKIP : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


}