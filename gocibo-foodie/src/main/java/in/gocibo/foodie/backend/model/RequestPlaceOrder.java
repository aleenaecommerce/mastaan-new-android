package in.gocibo.foodie.backend.model;

import com.aleena.common.methods.CommonMethods;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestPlaceOrder {

    public String n;
    public String m;
    public String e;

    public int pt;

    public String si;       // Slot

    public String dby;      //  DeliveryBy

    public String ad;       // AddressBookItemID
    public String da;       // Full Address
    public String pos;      // LatLng
    public String lm;       // Landmark
    public String p;        //  Premise
    public String s1;       //  SubLocality-1
    public String s2;       //  SubLocality-2
    public String r;        //  Road No
    public String l;        //  Locality
    public String s;        //  State
    public String c;        //  Country
    public  String pin;     // PinCode

    public RequestPlaceOrder(String addresBookItemID, String name, String mobile, String email, int paymentType, String instructions, LatLng latLng, String deliverySlot, String premise, String sublocality2, String sublocality1, String locality, String landmark, String address, String state, String country, String pincode){

        if ( addresBookItemID != null && addresBookItemID.equalsIgnoreCase("DUMMY_ID") == false ){
            this.ad = addresBookItemID;
        }

        this.n = name;
        this.m = mobile;
        this.e = email;
        this.pt = paymentType;

        if ( latLng != null ) {
            this.pos = latLng.latitude + "," + latLng.longitude;
        }

        this.si = instructions;

        this.dby = deliverySlot;

        this.p = premise;
        this.s1 = sublocality1;
        this.s2 = sublocality2;
        this.l = locality;
        this.lm = landmark;
        this.da = new CommonMethods().getStringFromStringArray(new String[]{premise, s2, lm, s1, l, s, c, pin});//premise+", "+sublocality2+", "+address;
        this.s = state;
        this.c = country;
        this.pin = pincode;
    }

    public void setDeliverySlot(String deliverySlot) {
        this.dby = deliverySlot;
    }

    public String getDeliverBy() {
        return dby;
    }

    public String getUserName() {
        return n;
    }

    public String getUserEmail() {
        return e;
    }

    public String getUserMobile() {
        return m;
    }

    public String getPremise() {
        return p;
    }

    public String getSubLocality1() {
        return s1;
    }

    public String getLocality() {
        return l;
    }

    public String getDeliveryAddress() {
        return da;
    }
}
