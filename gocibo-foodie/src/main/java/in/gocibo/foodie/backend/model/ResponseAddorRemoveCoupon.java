package in.gocibo.foodie.backend.model;

import java.util.List;

/**
 * Created by venkatesh on 15/3/16.
 */

public class ResponseAddorRemoveCoupon {

    String msg;                     // Message

    float ta;                      // Total Amount
    float govtTaxes;                  //
    float processingFees;             //
    float ccd;                     // CouponCode Discount

    String cc;                      // CouponCode
    List<String> ed;       // CouponCodeWorkingDays

    public String getMessage() {
        return msg;
    }

    public float getTotal() {
        return ta;
    }

    public float getSubTotal() {
        return (ta-govtTaxes-processingFees+ccd);
    }

    public float getGovtTaxes() {
        return govtTaxes;
    }

    public float getProcessingFees() {
        return processingFees;
    }

    public float getCouponDiscount() {
        return ccd;
    }

    public String getCouponCode() {
        return cc;
    }

    public List<String> getCouponCodeWorkingDays() {
        return ed;
    }
}
