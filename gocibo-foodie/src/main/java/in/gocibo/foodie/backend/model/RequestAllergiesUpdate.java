package in.gocibo.foodie.backend.model;

import java.util.List;

import in.gocibo.foodie.models.AllergyDetails;

/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestAllergiesUpdate {

    String []al;

    public RequestAllergiesUpdate(String[] allergiesList){
        this.al = allergiesList;
    }

    public RequestAllergiesUpdate(List<AllergyDetails> allergiesList){

        if ( allergiesList != null ) {
            String[] allergies = new String[allergiesList.size()];
            for ( int i=0;i<allergiesList.size();i++){
                allergies[i] = allergiesList.get(i).getID();
            }
            this.al = allergies;
        }
    }

}
