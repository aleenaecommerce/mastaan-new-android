package in.gocibo.foodie.backend.model;


import java.io.Serializable;

import in.gocibo.foodie.models.BuyerDetails;

public class ResponseVerifyEmail implements Serializable{

    public String token;
    public String code;
    public String error;

    BuyerDetails user;

    public String getToken(){  return token;  }
    public String getCode(){  return code;  }
    public String getError(){  return error;  }

    public BuyerDetails getBuyerDetails() {
        return user;
    }
}
