package in.gocibo.foodie.backend.model;

import java.util.List;

/**
 * Created by venkatesh on 10/12/15.
 */
public class ResponseGetUploadsCount {

    List<CategoryUploadsCountDetails> categories;

    public class CategoryUploadsCountDetails{
        String value;
        int count;

        public String getValue() {
            return value;
        }

        public int getCount() {
            return count;
        }
    }

    public List<CategoryUploadsCountDetails> getCategoriesUploadsCount() {
        return categories;
    }
}
