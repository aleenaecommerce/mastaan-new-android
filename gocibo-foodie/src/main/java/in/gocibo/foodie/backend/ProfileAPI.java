package in.gocibo.foodie.backend;


import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;

import java.util.List;

import in.gocibo.foodie.backend.model.RequestAllergiesUpdate;
import in.gocibo.foodie.backend.model.RequestProfileUpdate;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.CheckMaintenance;
import in.gocibo.foodie.models.AllergyDetails;
import in.gocibo.foodie.models.ProfileSummaryDetails;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ProfileAPI {

    Context context;
    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface ProfileDetailsCallback{
        void onComplete(boolean status, int statusCode, ProfileSummaryDetails profileSummaryDetails);
    }

    public interface AllergiesCallback{
        void onComplete(boolean status, int statusCode, List<AllergyDetails> availableAllergies);
    }

    public ProfileAPI(Context context, RestAdapter restAdapter) {
        this.context = context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getProfileDetails(final ProfileDetailsCallback callback){

        retrofitInterface.getProfileDetails(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new Callback<ProfileSummaryDetails>() {
            @Override
            public void success(ProfileSummaryDetails profileSummaryDetails, Response response) {

                if (profileSummaryDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - ProfileDetails : Success");
                    callback.onComplete(true, 200, profileSummaryDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - ProfileDetails : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - ProfileDetails : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - ProfileDetails : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void updateProfile(RequestProfileUpdate requestProfileUpdate, final StatusCallback callback){

        retrofitInterface.updateProfile(localStorageData.getDeviceID(), localStorageData.getAccessToken(), requestProfileUpdate, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("ok")) {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateProfile : Success");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateProfile : Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - FoodItemDetails : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - FoodItemDetails : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getBuyerAllergies(final AllergiesCallback callback){

        retrofitInterface.getBuyerAllergies(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new Callback<List<AllergyDetails>>() {
            @Override
            public void success(List<AllergyDetails> buyerAllergies, Response response) {

                if (buyerAllergies != null) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : BuyerAllergies , Success , Found = " + buyerAllergies.size());
                    callback.onComplete(true, 200, buyerAllergies);
                } else {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : BuyerAllergies , Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : BuyerAllergies , Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : BuyerAllergies , Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void updateBuyerAllergies(RequestAllergiesUpdate requestAllergiesUpdate, final StatusCallback callback){

        retrofitInterface.updateBuyerAllergies(localStorageData.getDeviceID(), localStorageData.getAccessToken(), requestAllergiesUpdate, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("ok")) {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateAllergies : Success");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateAllergies : Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateAllergies : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateAllergies : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


}