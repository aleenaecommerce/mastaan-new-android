package in.gocibo.foodie.backend.model;

import com.google.android.gms.maps.model.LatLng;

import com.aleena.common.methods.CommonMethods;

/**
 * Created by venkatesh on 02/07/15.
 */

public class RequestSearchUploads {
    public String q;    // Query Name.
    public String cat;  //  Category
    public String st;   //  Slot Type
    public  String l;   // Location LatLng
    public String t;       // 1-Veg, 2-NonVeg , 0-All
    public String c;    // Cuisines

    public RequestSearchUploads(){}

    public RequestSearchUploads(String query, String category, LatLng location, String type, String[] cuisines){

        q = query;
        cat = category;
        //st = slotType;
        if ( location != null ){
            l = location.latitude+","+location.longitude;
        }

        if ( type != null && type.equalsIgnoreCase("veg")){ t = ""+0;   }
        else if ( type != null && (type.equalsIgnoreCase("nonveg") || type.equalsIgnoreCase("Non Veg") || type.equalsIgnoreCase("non-veg")) ){ t = ""+1;  }

        if ( cuisines != null && cuisines.length > 0 ){
            c = new CommonMethods().getStringFromStringArray(cuisines);
        }

    }

    public RequestSearchUploads(String query, String category, LatLng location, int type, String[] cuisines){
        q = query;
        cat = category;
        //st = slotType;
        if ( location != null ){
            l = location.latitude+","+location.longitude;
        }

        if ( type != 0 ){   t = ""+type;   }

        if ( cuisines != null && cuisines.length > 0 ){
            c = new CommonMethods().getStringFromStringArray(cuisines);
        }
    }

    public String getSearchName() {
        return q;
    }

    public String getLocation() {
        return l;
    }

    public String getFoodType() {
        return t;
    }

    public String getCuisines() {
        return c;
    }
}
