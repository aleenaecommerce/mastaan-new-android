package in.gocibo.foodie.backend.model;

/**
 * Created by venkatesh on 9/11/15.
 */
public class ResponseRemoveCartItem {

    String code;
    boolean alreadyPresent;
    double latestAvailability;

    double govtTaxes;
    double serviceCharges;
    double ta;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
    public boolean isAlreadyPresent(){  return  alreadyPresent; }

    public double getLatestAvailability() {
        return latestAvailability;
    }

    public double getCartTotalGovtTaxes() {
        return govtTaxes;
    }

    public double getCartTotalServiceCharges() {
        return serviceCharges;
    }

    public double getCartTotalAmount() {
        return ta;
    }

}
