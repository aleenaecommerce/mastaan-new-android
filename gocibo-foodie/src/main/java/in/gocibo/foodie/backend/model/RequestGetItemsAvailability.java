package in.gocibo.foodie.backend.model;

import java.util.List;

/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestGetItemsAvailability {

    public List<String> dishes;

    public RequestGetItemsAvailability(List<String> dishes){
        this.dishes =  dishes;
    }

    public void setDishes(List<String> dishes) {
        this.dishes = dishes;
    }

    public List<String> getDishes() {
        return dishes;
    }
}
