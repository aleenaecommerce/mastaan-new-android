package in.gocibo.foodie.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;

import org.json.JSONObject;

import java.util.List;

import in.gocibo.foodie.backend.model.RequestRegisterPushNotifications;
import in.gocibo.foodie.backend.model.ResponseGetFirebaseToken;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.CheckMaintenance;
import in.gocibo.foodie.models.AllergyDetails;
import in.gocibo.foodie.models.BootStrapDetails;
import in.gocibo.foodie.models.CuisineDetails;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by venkatesh on 13/7/15.
 */

public class CommonAPI {

    Context context;
    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface BootStrapCallback{
        void onComplete(boolean status, int statusCode, String message, BootStrapDetails bootStrapObject);
    }

    public interface AllergiesCallback{
        void onComplete(boolean status, int statusCode, List<AllergyDetails> availableAllergies);
    }

    public interface CuisinesCallback{
        void onComplete(boolean status, int statusCode, List<CuisineDetails> availableCuisines);
    }

    public interface FirebaseTokenCallback{
        void onComplete(boolean status, int statusCode, String firebaseToken);
    }

    public CommonAPI(Context context, RestAdapter restAdapter) {
        this.context = context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getFirebaseToken(final FirebaseTokenCallback callback){

        retrofitInterface.getFireBaseToken(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new Callback<ResponseGetFirebaseToken>() {
            @Override
            public void success(ResponseGetFirebaseToken responseFirebase, Response response) {
                if (responseFirebase != null && responseFirebase.getFirebaseToken() != null && responseFirebase.getFirebaseToken().length() > 0) {
                    callback.onComplete(true, 200, responseFirebase.getFirebaseToken());
                    Log.d(Constants.LOG_TAG, "\nFIREBASE_API:  GetToken : Success , Token = " + responseFirebase.getFirebaseToken());
                } else {
                    callback.onComplete(false, 500, null);
                    Log.d(Constants.LOG_TAG, "\nFIREBASE_API:  GetToken : Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFIREBASE_API:  GetToken : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFIREBASE_API:  GetToken : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getBootStrap(final int appVersion, final BootStrapCallback callback){

        retrofitInterface.getBootStrap(localStorageData.getDeviceID(), localStorageData.getAccessToken(), appVersion, new retrofit.Callback<BootStrapDetails>() {
            @Override
            public void success(BootStrapDetails bootStrapObject, Response response) {

                if (bootStrapObject != null) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API - BootStrap : Success , inTiming = " + bootStrapObject.isOpen());
                    callback.onComplete(true, 200, "Success", bootStrapObject);
                } else {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API - BootStrap : Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    String message = "";
                    try {
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);
                        if (jsonResponse.getBoolean("maintenance")) {
                            message = jsonResponse.getString("message");
                        }
                    } catch (Exception e) {
                    }
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API - BootStrap : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " , MessageFromServer = " + message);
                    callback.onComplete(false, error.getResponse().getStatus(), message, null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API - BootStrap : Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", null);
                }
            }
        });
    }

    public void getAvailableAllergies(final AllergiesCallback callback){

        retrofitInterface.getAvailableAllergies(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new Callback<List<AllergyDetails>>() {
            @Override
            public void success(List<AllergyDetails> availableAllergies, Response response) {
                if (availableAllergies != null) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : AvailableAllergies , Success , Found = " + availableAllergies.size());
                    callback.onComplete(true, 200, availableAllergies);
                } else {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : AvailableAllergies , Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nGoCibo-CurrentSlot : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : AvailableAllergies , Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void registerForPushNotifications(final StatusCallback callback){

        retrofitInterface.registerForPushNotifications(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new RequestRegisterPushNotifications(localStorageData.getGCMRegistrationID()), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK") == true) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : PushNotificationsRegistration : Success , for (gcmRegID=" + localStorageData.getGCMRegistrationID() + ")");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : PushNotificationsRegistration : Failure ,  for (gcmRegID=" + localStorageData.getGCMRegistrationID() + ")");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : PushNotificationsRegistration : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : PushNotificationsRegistration : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void registerBuyerForPushNotifications(final StatusCallback callback){

        retrofitInterface.registerBuyerForPushNotifications(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new RequestRegisterPushNotifications(localStorageData.getGCMRegistrationID()), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK") == true) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : BuyerPushNotificationsRegistration : Success , for (gcmRegID=" + localStorageData.getGCMRegistrationID() + ")");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : BuyerPushNotificationsRegistration : Failure ,  for (gcmRegID=" + localStorageData.getGCMRegistrationID() + ")");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : BuyerPushNotificationsRegistration : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : BuyerPushNotificationsRegistration : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}