package in.gocibo.foodie.backend;


import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.backend.model.RequestGetCategories;
import in.gocibo.foodie.backend.model.RequestGetItemsAvailability;
import in.gocibo.foodie.backend.model.RequestGetUploadsCount;
import in.gocibo.foodie.backend.model.RequestSearchUploads;
import in.gocibo.foodie.backend.model.ResponseGetCategories;
import in.gocibo.foodie.backend.model.ResponseGetUploadsCount;
import in.gocibo.foodie.backend.model.ResponseSearchUploads;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.CheckMaintenance;
import in.gocibo.foodie.models.CategoryDetails;
import in.gocibo.foodie.models.CuisineDetails;
import in.gocibo.foodie.models.FoodItemDetails;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FoodItemsAPI {

    Context context;
    String appMode;
    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface CategoriesListCallback{
        void onComplete(boolean status, int statusCode, String message, long total, List<CategoryDetails> categoriesList);
    }

    public interface FoodItemsListCallback {
        void onComplete(boolean status, int statusCode, String message, List<FoodItemDetails> foodItems, List<CuisineDetails> availableCuisines);
    }

    public interface FoodItemDetailsCallback {
        void onComplete(boolean status, int statusCode, FoodItemDetails foodItemDetails);
    }

    public interface CountUploadsCallback {
        void onComplete(boolean status, int statusCode, ResponseGetUploadsCount responseGetUploadsCount);
    }

    public FoodItemsAPI(Context context, String appMode, RestAdapter restAdapter) {
        this.context = context;
        this.appMode = appMode;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getCategoriesList(final LatLng deliveryLocationLatLng, final CategoriesListCallback callback){

        retrofitInterface.getCategories(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new RequestGetCategories(deliveryLocationLatLng), new Callback<ResponseGetCategories>() {

            @Override
            public void success(ResponseGetCategories responseGetCategories, Response response) {

                if (responseGetCategories != null) {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - Categories : Success");
                    callback.onComplete(true, 200, "Success", responseGetCategories.getTotal(), responseGetCategories.getCategories());
                } else {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - Categories : Failure");
                    callback.onComplete(false, 500, "Failure", 0, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - Categories : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", 0, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - Categories : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", 0, null);
                    }
                }
            }
        });
    }

    // Get Uploads Count
    public void getUploadsCount(final CountUploadsCallback callback){

        retrofitInterface.getUploadsCount(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new RequestGetUploadsCount(localStorageData.getDeliveryLocation().getLatLng()), new Callback<ResponseGetUploadsCount>() {

            @Override
            public void success(ResponseGetUploadsCount responseGetUploadsCount, Response response) {
                if (responseGetUploadsCount != null && responseGetUploadsCount.getCategoriesUploadsCount() != null) {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - CountUploads : Success");
                    callback.onComplete(true, 200, responseGetUploadsCount);
                } else {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - CountUploads : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - CountUploads : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - CountUploads : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    // Get Food Item Details..
    public void getFoodItemDetails(final String dishID, final FoodItemDetailsCallback callback){

        retrofitInterface.getFoodItemDetails(localStorageData.getDeviceID(), localStorageData.getAccessToken(), dishID, new Callback<FoodItemDetails>() {
            @Override
            public void success(FoodItemDetails foodItemDetails, Response response) {
                if (foodItemDetails != null && foodItemDetails.getID() != null) {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - FoodItemDetails : Success , Found = " + foodItemDetails.getName());
                    callback.onComplete(true, 200, foodItemDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - FoodItemDetails : Failure ,for (id=" + dishID + ")");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - FoodItemDetails : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " ,for (id=" + dishID + ")");
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - FoodItemDetails : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getFoodItems(RequestSearchUploads requestSearchUploads, final FoodItemsListCallback callback){

        retrofitInterface.searchUploads(localStorageData.getDeviceID(), localStorageData.getAccessToken(), requestSearchUploads, new retrofit.Callback<ResponseSearchUploads>() {

            @Override
            public void success(ResponseSearchUploads responseObject, Response response) {

                if (responseObject != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    if (responseObject.getFoodItems() != null) {
                        /*List<FoodItemDetails> foodItems = new ArrayList<FoodItemDetails>();
                        // FILTERING EXPIRED ITEMS
                        for (int i=0;i<responseObject.getFoodItems().size();i++){
                            if (responseObject.getFoodItems().get(i).getAvailabilityEndTime().equals("") || DateMethods.compareDates(DateMethods.getReadableDateFromUTC(responseObject.getFoodItems().get(i).getAvailabilityEndTime()), localStorageData.getServerTime()) > 0 ) {
                                foodItems.add(responseObject.getFoodItems().get(i));
                            }
                        }*/
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - WhatsCookingItems : Success , Found=" + responseObject.getFoodItems().size());
                        callback.onComplete(true, 200, "OK", responseObject.getFoodItems(), responseObject.getCuisines());
                    } else {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - WhatsCookingItems : Failure");
                        callback.onComplete(false, 500, "FAILURE", null, null);
                    }
                } else if (responseObject != null && responseObject.getCode().equalsIgnoreCase("slot_closed")) {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - WhatsCookingItems : Success with SOME PROBLEM ( " + responseObject.getCode());
                    callback.onComplete(true, 200, "slot_closed", new ArrayList<FoodItemDetails>(), null);
                } else {
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - WhatsCookingItems : Failure");
                    callback.onComplete(false, 500, "FAILURE", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - WhatsCookingItems : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "ERROR", null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - WhatsCookingItems : Error = " + error.getKind());
                        callback.onComplete(false, -1, "ERROR", null, null);
                    }
                }
            }
        });

    }

    public void getItemsAvailability(final List<String> itemsIDs, final FoodItemsListCallback callback){

        retrofitInterface.getItemsAvailability(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new RequestGetItemsAvailability(itemsIDs), new Callback<List<FoodItemDetails>>() {
            @Override
            public void success(List<FoodItemDetails> foodItems, Response response) {
                if ( foodItems != null ){
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - GetItemsAvailability : Success , for = "+ foodItems.size());
                    callback.onComplete(true, 200, "OK", foodItems, null);
                }else{
                    Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - GetItemsAvailability : Failure , for = "+ foodItems.size());
                    callback.onComplete(true, 500, "FAILURE", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - GetItemsAvailability : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "ERROR", null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOOD_ITEMS_API - GetItemsAvailability : Error = " + error.getKind());
                        callback.onComplete(false, -1, "ERROR", null, null);
                    }
                }
            }
        });
    }


}