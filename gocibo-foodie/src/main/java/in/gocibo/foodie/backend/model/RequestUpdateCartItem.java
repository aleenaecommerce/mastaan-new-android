package in.gocibo.foodie.backend.model;

/**
 * Created by venkatesh on 16/07/15.
 */

public class RequestUpdateCartItem {

    public String dish_id;
    public double diff;
    public String dby;
    //public int old_num;
    //public int new_num;

    public RequestUpdateCartItem(String dishID, double oldQuantity, double newQuantity, String dby){
        this.dish_id = dishID;
        //this.old_num = oldQuantity;
        //this.new_num = newQuantity;
        this.diff = newQuantity-oldQuantity;
        this.dby = dby;
    }
}
