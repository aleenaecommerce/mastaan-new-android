package in.gocibo.foodie.backend;


import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.ResponseStatus;

import java.util.List;

import in.gocibo.foodie.backend.model.RequestAddAddress;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.CheckMaintenance;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddressBookAPI {

    Context context;
    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface AddAddressCallback{
        void onComplete(boolean status, int statusCode, String addressID);
    }

    public interface FavouritesCallback {
        void onComplete(boolean status, int statusCode, List<AddressBookItemDetails> addressBookItems);
    }

    public AddressBookAPI(Context context, RestAdapter restAdapter) {
        this.context = context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getFavourites(final FavouritesCallback callback){

        retrofitInterface.getFavourites(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new Callback<List<AddressBookItemDetails>>() {
            @Override
            public void success(List<AddressBookItemDetails> addressBookItems, Response response) {

                if (addressBookItems != null) {
                    Log.d(Constants.LOG_TAG, "\nAddressBook-GetFavourites : Success , Found = " + addressBookItems.size());
                    callback.onComplete(true, 200, addressBookItems);
                } else {
                    Log.d(Constants.LOG_TAG, "\nAddressBook-GetFavourites : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAddressBook-GetFavourites : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAddressBook-GetFavourites : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void addAddress(final AddressBookItemDetails addressBookItemDetails, final  AddAddressCallback callback){

        retrofitInterface.addAddress(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new RequestAddAddress(addressBookItemDetails), new Callback<RequestAddAddress>() {
            @Override
            public void success(RequestAddAddress responseObject, Response response) {

                if (responseObject != null && responseObject.get_id() != null && responseObject.get_id().length() > 0) {
                    Log.d(Constants.LOG_TAG, "\nAddressBook-AddAddress : Success , AddressID = " + responseObject.get_id());
                    callback.onComplete(true, 200, responseObject.get_id());
                } else {
                    Log.d(Constants.LOG_TAG, "\nAddressBook-AddAddress : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAddressBook-AddAddress : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAddressBook-AddAddress : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void updateAddress(final AddressBookItemDetails addressBookItemDetails, final StatusCallback callback){

        retrofitInterface.updateAddress(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new RequestAddAddress(addressBookItemDetails), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nAddressBook-UpdateAddress : Success , for ID = " + addressBookItemDetails.getID());
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nAddressBook-UpdateAddress : Failure , for ID = " + addressBookItemDetails.getID());
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAddressBook-UpdateAddress : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for ID = " + addressBookItemDetails.getID());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAddressBook-UpdateAddress : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void deleteAddress(final String addressID, final StatusCallback callback){

        retrofitInterface.deleteAddress(localStorageData.getDeviceID(), localStorageData.getAccessToken(), addressID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nAddressBook-DeleteAddress : Success , for ID = " + addressID);
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nAddressBook-DeleteAddress : Failure , for ID = " + addressID);
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAddressBook-DeleteAddress : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " , for ID = " + addressID);
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAddressBook-DeleteAddress : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}