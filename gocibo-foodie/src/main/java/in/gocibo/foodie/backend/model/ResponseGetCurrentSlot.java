package in.gocibo.foodie.backend.model;

import java.util.List;

import in.gocibo.foodie.models.SlotDetails;

/**
 * Created by venkatesh on 13/7/15.
 */

public class ResponseGetCurrentSlot {
    boolean open;

    String title;
    String subTitle;

    List<SlotDetails> slots;

    public boolean isOpen(){
        return open;
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public List<SlotDetails> getAvailableSlots() {
        return slots;
    }

    public void setAvailableSlots(List<SlotDetails> slots) {
        this.slots = slots;
    }
}
