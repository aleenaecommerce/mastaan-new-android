package in.gocibo.foodie.backend.model;

/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestRegisterPushNotifications {

    public String rid;      // GCM Registration ID

    public RequestRegisterPushNotifications(String gcmRegID){
        rid = gcmRegID;
    }
}
