package in.gocibo.foodie.backend.model;

import java.io.Serializable;

public class ResponseVerifyMobile implements Serializable{

    public String token;
    public String code;
    public String error;

    public String getToken(){  return token;  }
    public String getCode(){  return code;  }
    public String getError(){  return error;  }

}
