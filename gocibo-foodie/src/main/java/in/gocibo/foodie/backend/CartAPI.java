package in.gocibo.foodie.backend;


import android.content.Context;
import android.util.Log;

import com.aleena.common.models.ResponseStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import in.gocibo.foodie.backend.model.RequestAddToCart;
import in.gocibo.foodie.backend.model.RequestUpdateCartItem;
import in.gocibo.foodie.backend.model.ResponseAddCartItem;
import in.gocibo.foodie.backend.model.ResponseGetCartItems;
import in.gocibo.foodie.backend.model.ResponseRemoveCartItem;
import in.gocibo.foodie.backend.model.ResponseUpdateCartItem;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.CheckMaintenance;
import in.gocibo.foodie.models.CartItemDetails;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CartAPI {

    Context context;
    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface AddCartItemCallback {
        void onComplete(boolean status, int statusCode, String message, boolean isAlreadyPresent, double latestAvailability);
    }
    public interface UpdateCartItemCallck {
        void onComplete(boolean status, int statusCode, String message, boolean isAlreadyPresent, double latestAvailability, double cartTotalAmount, double cartTotalServiceCharges, double cartTotalGovtTaxes);
    }
    public interface RemoveCartItemCallback {
        void onComplete(boolean status, int statusCode, String message, double latestAvailability, double cartTotalAmount, double cartTotalServiceCharges, double cartTotalGovtTaxes);
    }

    public interface GetCartItemsCallback {
        void onComplete(boolean status, int statusCode, String serverTime, List<CartItemDetails> cartItems, double cartTotalAmount, double cartTotalServiceCharges, double cartTotalGovtTaxes);
    }

    public interface ClearCartItemsCallback {
        void onComplete(boolean status, int statusCode, List<CartItemDetails> cartItemDetailses);
    }

    public interface ChangeCartSlotCallback{
        void onComplete(boolean status, int statusCode, String message, List<Map<String, String>> mismatchedCartItems);
    }

    public CartAPI(Context context, RestAdapter restAdapter) {
        this.context = context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getCartItems(final GetCartItemsCallback callback){

        retrofitInterface.getCartItems(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new retrofit.Callback<ResponseGetCartItems>() {
            @Override
            public void success(ResponseGetCartItems responseGetCartItems, Response response) {
                Log.d(Constants.LOG_TAG, "\nCART-GetItems : Success , Found = " + responseGetCartItems.getCartItems().size());
                callback.onComplete(true, 200, responseGetCartItems.getServerTime(), responseGetCartItems.getCartItems(), responseGetCartItems.getCartTotalAmount(), responseGetCartItems.getCartServiceCharges(), responseGetCartItems.getCartGovtTaxes());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART-GetItems : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null, null, 0, 0, 0);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART-GetItems : Error = " + error.getKind());
                        callback.onComplete(false, -1, null, null, 0, 0, 0);
                    }
                }
            }
        });
    }

    public void clearCartItems(final ClearCartItemsCallback callback){

        retrofitInterface.clearCartItems(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, new ArrayList<CartItemDetails>());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART-ClearCart : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART-ClearCart : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });

    }

    public void addToCart(final String dishID, final double quantity, final String deliverySlot, final AddCartItemCallback callback){

        retrofitInterface.addToCart(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new RequestAddToCart(localStorageData.getDeliveryLocation(), deliverySlot), dishID, quantity, new retrofit.Callback<ResponseAddCartItem>() {
            @Override
            public void success(ResponseAddCartItem responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null) {
                    //responseObject.setCode("cart_add_item_availability_exceeded");//slot_closed
                    if (responseObject.getCode().equalsIgnoreCase("OK")) {
                        Log.d(Constants.LOG_TAG, "\nCART-AddToCart : Success [AlreadyExists = " + responseObject.isAlreadyPresent() + " for (dish_id=" + dishID + ")");
                        callback.onComplete(true, 200, responseObject.getCode(), responseObject.isAlreadyPresent(), responseObject.getLatestAvailability());
                    } else {
                        Log.d(Constants.LOG_TAG, "\nCART-AddToCart : Success with SOME PROBLEM ( " + responseObject.getCode() + " ) [AlreadyExists = " + responseObject.isAlreadyPresent() + " for (dish_id=" + dishID + ")");
                        callback.onComplete(true, 200, responseObject.getCode(), responseObject.isAlreadyPresent(), responseObject.getLatestAvailability());
                    }
                } else {
                    Log.d(Constants.LOG_TAG, "\nCART-AddToCart : Failure for (dish_id=" + dishID + ")");
                    callback.onComplete(false, 500, "FAILURE", false, 0);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART-AddToCart : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (dish_id=" + dishID + ")");
                        callback.onComplete(false, error.getResponse().getStatus(), "ERROR", false, 0);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART-AddToCart : Error = " + error.getKind());
                        callback.onComplete(false, -1, "ERROR", false, 0);
                    }
                }
            }
        });
    }

    public void updateCartItem(final String dishID, final double oldQuantity, final double newQuantity, final String dby, final UpdateCartItemCallck callback){

        RequestUpdateCartItem requestUpdateCartItem = new RequestUpdateCartItem(dishID, oldQuantity, newQuantity, dby);

        retrofitInterface.updateCartItem(localStorageData.getDeviceID(), localStorageData.getAccessToken(), requestUpdateCartItem, new retrofit.Callback<ResponseUpdateCartItem>() {
            @Override
            public void success(ResponseUpdateCartItem responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null) {

                    if (responseObject.getCode().equalsIgnoreCase("OK")) {
                        Log.d(Constants.LOG_TAG, "\nCART-UpdateCartItem : Success , for (dish_id=" + dishID + ")");
                        callback.onComplete(true, 200, responseObject.getCode(), true, responseObject.getLatestAvailability(), responseObject.getCartTotalAmount(), responseObject.getCartTotalServiceCharges(), responseObject.getCartTotalGovtTaxes());
                    } else {
                        Log.d(Constants.LOG_TAG, "\nCART-UpdateCartItem : Success with SOME PROBLEM ( " + responseObject.getCode() + " )  for (dish_id=" + dishID + ")");
                        callback.onComplete(true, 200, responseObject.getCode(), responseObject.isAlreadyPresent(), responseObject.getLatestAvailability(), responseObject.getCartTotalAmount(), responseObject.getCartTotalServiceCharges(), responseObject.getCartTotalGovtTaxes());
                    }
                } else {
                    Log.d(Constants.LOG_TAG, "\nCART-UpdateCartItem : Failure = for (dish_id=" + dishID + ")");
                    callback.onComplete(false, 500, "FAILURE", false, 0, 0, 0, 0);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART-UpdateCartItem : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (dish_id=" + dishID + ")");
                        callback.onComplete(false, error.getResponse().getStatus(), "ERROR", false, 0, 0, 0, 0);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART-UpdateCartItem : Error = " + error.getKind());
                        callback.onComplete(false, -1, "ERROR", false, 0, 0, 0, 0);
                    }
                }
            }
        });
    }

    public void removeFromCart(final String dishID, final double quantity, final String dby, final RemoveCartItemCallback callback){

        retrofitInterface.removeFromCart(localStorageData.getDeviceID(), localStorageData.getAccessToken(), dishID, quantity, dby, new retrofit.Callback<ResponseRemoveCartItem>() {
            @Override
            public void success(ResponseRemoveCartItem responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null) {
                    if (responseObject.getCode().equalsIgnoreCase("OK")) {
                        Log.d(Constants.LOG_TAG, "\nCART-RemoveFromCart : Success , for (dish_id=" + dishID + ")");
                        callback.onComplete(true, 200, responseObject.getCode(), responseObject.getLatestAvailability(), responseObject.getCartTotalAmount(), responseObject.getCartTotalServiceCharges(), responseObject.getCartTotalGovtTaxes());
                    } else {
                        Log.d(Constants.LOG_TAG, "\nCART-RemoveFromCart : Failure = for (dish_id=" + dishID + ")");
                        callback.onComplete(false, 500, "FAILURE", 0, 0, 0, 0);
                    }
                } else {
                    Log.d(Constants.LOG_TAG, "\nCART-RemoveFromCart : Failure = for (dish_id=" + dishID + ")");
                    callback.onComplete(false, 500, "FAILURE", 0, 0, 0, 0);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART-RemoveFromCart : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (dish_id=" + dishID + ")");
                        callback.onComplete(false, error.getResponse().getStatus(), "ERROR", 0, 0, 0, 0);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART-RemoveFromCart : Error = " + error.getKind());
                        callback.onComplete(false, -1, "ERROR", 0, 0, 0, 0);
                    }
                }
            }
        });
    }



}