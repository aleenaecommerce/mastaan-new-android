package in.gocibo.foodie.backend.model;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by venkatesh on 15/7/15.
 */

public class ResponseChangeCartSlot {

    String code;
    List<MistMatchCartItem> mismatches;

    class MistMatchCartItem{
        String _id;
        String n;

        public String getID() {
            return _id;
        }

        public String getName() {
            return n;
        }
    }

    public String getCode() {
        return code;
    }

    public List<Map<String, String>> getMismatches() {

        List<Map<String, String>> mismatchItems = new ArrayList<>();

        if ( mismatches != null && mismatches.size() > 0 ){
            for (int i=0;i<mismatches.size();i++){
                Map<String, String> mismatchItem = new HashMap<>();
                mismatchItem.put("id", mismatches.get(i).getID());
                mismatchItem.put("name", mismatches.get(i).getName());
                mismatchItems.add(mismatchItem);
            }
            return mismatchItems;
        }
        return mismatchItems;
    }
}
