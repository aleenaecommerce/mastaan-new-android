package in.gocibo.foodie.backend;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;

import in.gocibo.foodie.backend.model.ResponseAddorRemoveCoupon;
import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.localdata.LocalStorageData;
import in.gocibo.foodie.methods.CheckMaintenance;
import in.gocibo.foodie.models.CartTotalDetails;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by venkatesh on 13/7/15.
 */

public class CouponsAPI {

    Context context;
    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface AddOrRemoveCouponCallback {
        void onComplete(boolean status, int statusCode, String message, CartTotalDetails cartTotalDetails);
    }

    public CouponsAPI(Context context, RestAdapter restAdapter) {
        this.context = context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void applyCoupon(final String couponCode, final AddOrRemoveCouponCallback callback){

        retrofitInterface.applyCoupon(localStorageData.getDeviceID(), localStorageData.getAccessToken(), couponCode, new Callback<ResponseAddorRemoveCoupon>() {
            @Override
            public void success(ResponseAddorRemoveCoupon responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ APPLY_COUPON ]  >>  Success");
                callback.onComplete(true, 200, responseObject.getMessage(), new CartTotalDetails(responseObject.getTotal(), responseObject.getGovtTaxes(), responseObject.getProcessingFees(), responseObject.getCouponDiscount(), responseObject.getCouponCode(), responseObject.getCouponCodeWorkingDays()));
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);

                        String msg = "";
                        try {
                            msg = jsonResponse.getString("msg");
                        } catch (Exception e) {
                        }

                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ APPLY_COUPON ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), msg, null);

                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ APPLY_COUPON ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "", null);
                    }
                }
            }
        });
    }

    public void removeCoupon(final AddOrRemoveCouponCallback callback){

        retrofitInterface.removeCoupon(localStorageData.getDeviceID(), localStorageData.getAccessToken(), new Callback<ResponseAddorRemoveCoupon>() {
            @Override
            public void success(ResponseAddorRemoveCoupon responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ REMOVE_COUPON ]  >>  Success");
                callback.onComplete(true, 200, "Success", new CartTotalDetails(responseObject.getTotal(), responseObject.getGovtTaxes(), responseObject.getProcessingFees(), responseObject.getCouponDiscount(), responseObject.getCouponCode(), responseObject.getCouponCodeWorkingDays()));
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckMaintenance(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ REMOVE_COUPON ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ REMOVE_COUPON ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }
}
