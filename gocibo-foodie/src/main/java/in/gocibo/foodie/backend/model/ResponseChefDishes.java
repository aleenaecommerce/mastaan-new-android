package in.gocibo.foodie.backend.model;

/**
 * Created by venkatesh on 15/7/15.
 */

public class ResponseChefDishes {

    public String _id;          // dishID
    public String n;            // dishName
    public String d;            // dishDescription
    public int t;               // dishType   { veg / nonveg }
    public String p;            // dishImageURL
    public String th;           // dishThumbnailURL

    public String c;            // chefID
}
