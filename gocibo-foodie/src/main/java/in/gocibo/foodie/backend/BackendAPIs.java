package in.gocibo.foodie.backend;

import android.content.Context;

import in.gocibo.foodie.R;
import retrofit.RestAdapter;

/**
 * Created by venkatesh on 21/7/15.
 */
public class BackendAPIs {

    Context context;

    RestAdapter restAdapter;
    //RetrofitInterface retrofitInterface;

    AuthenticationAPI authenticationAPI;
    CommonAPI commonAPI;
    ProfileAPI profileAPI;
    AddressBookAPI addressBookAPI;
    ChefsAPI chefsAPI;
    FoodItemsAPI foodItemsAPI;
    CartAPI cartAPI;
    CouponsAPI couponsAPI;
    OrdersAPI ordersAPI;

    public BackendAPIs(Context context){
        this.context = context;
    }

    private RestAdapter getRestAdapter(){
        if ( restAdapter == null ){
            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(context.getString(R.string.gocibo_base_url))
                    /*.setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            request.addHeader("x-access-token", new LocalStorageData(context).getAccessToken());
                        }
                    })*/
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            //retrofitInterface = restAdapter.create(RetrofitInterface.class);
        }
        return restAdapter;
    }

    public CommonAPI getCommonAPI() {
        if ( commonAPI == null ){
            commonAPI = new CommonAPI(context, getRestAdapter());
        }
        return commonAPI;
    }

    public AuthenticationAPI getAuthenticationAPI() {
        if ( authenticationAPI == null ){
            authenticationAPI = new AuthenticationAPI(context, getRestAdapter());
        }
        return authenticationAPI;
    }

    public ProfileAPI getProfileAPI() {
        if ( profileAPI == null ){
            profileAPI = new ProfileAPI(context, getRestAdapter());
        }
        return profileAPI;
    }

    public AddressBookAPI getAddressBookAPI() {
        if ( addressBookAPI == null ){
            addressBookAPI = new AddressBookAPI(context, getRestAdapter());
        }
        return addressBookAPI;
    }

    public ChefsAPI getChefsAPI() {
        if ( chefsAPI == null ){
            chefsAPI = new ChefsAPI(context, getRestAdapter());
        }
        return chefsAPI;
    }

    public FoodItemsAPI getFoodItemsAPI() {
        if ( foodItemsAPI == null ){
            foodItemsAPI = new FoodItemsAPI(context, context.getResources().getString(R.string.app_mode), getRestAdapter());
        }
        return foodItemsAPI;
    }

    public CartAPI getCartAPI() {
        if ( cartAPI == null ){
            cartAPI = new CartAPI(context, getRestAdapter());
        }
        return cartAPI;
    }

    public CouponsAPI getCouponsAPI() {
        if ( couponsAPI == null ){
            couponsAPI = new CouponsAPI(context, getRestAdapter());
        }
        return couponsAPI;
    }

    public OrdersAPI getOrdersAPI() {
        if ( ordersAPI == null ){
            ordersAPI = new OrdersAPI(context, getRestAdapter());
        }
        return ordersAPI;
    }

}
