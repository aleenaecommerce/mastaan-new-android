package in.gocibo.foodie.constants;

import android.view.View;

/**
 * Created by venkatesh on 8/9/15.
 */

public final class Constants {

    public static final String LOG_TAG =  "GoCiboLogs";

    public static final int TERMS_AND_CONDITIONS_ACTIVITY_CODE = 100;
    public static final int REGISTER_ACTIVITY_CODE = 101;
    public static final int LOCATION_SELECTOR_ACTIVITY_CODE = 102;
    public static final int PLACE_SEARCH_ACTIVITY_CODE = 103;
    public static final int ADDRESS_BOOK_ACTIVITY_CODE = 104;
    public static final int FAVOURITE_ACTIVITY_CODE = 105;
    public static final int HOME_ACTIVITY_CODE = 106;
    public static final int PROFILE_ACTIVITY_CODE = 107;
    public static final int PENDING_FEEDBACK_ORDERS_ACTIVITY_CODE = 108;
    public static final int ORDER_FEEDBACK_ACTIVITY_CODE = 1079;
    public static final int REGISTER_CHEF_ACTIVITY_CODE = 110;

    public static final String OTP_RECEIVER = "OTP_RECEIVER";
    public static final String SIDEBAR_RECEIVER = "SIDEBAR_RECEIVER";
    public static final String LOCATION_SELECTION_RECEIVER = "LOCATION_SELECTION_RECEIVER";
    public static final String HOME_RECEIVER = "HOME_RECEIVER";
    public static final String CART_RECEIVER = "CART_RECEIVER";
    public static final String ORDER_BOOKING_RECEIVER = "ORDER_BOOKING_RECEIVER";
    public static final String ORDER_HISTORY_RECEIVER =  "ORDER_HISTORY_RECEIVER";
    public static final String ORDER_DETAILS_RECEIVER = "ORDER_DETAILS_RECEIVER";
    public static final String FIREBASE_RECEIVER = "FIREBASE_RECEIVER";


}
