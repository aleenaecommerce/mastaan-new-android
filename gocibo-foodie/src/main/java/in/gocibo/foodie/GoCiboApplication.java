package in.gocibo.foodie;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 ** Created by venkatesh on 7/8/15.
 */

public class GoCiboApplication extends Application{//MultiDexApplication {

    Context currentActivityContext;

    AlertDialog notificationDialog;
    View notificationDialogView;

    /*int cartCount;
    List<CartItemDetails> cartItems;
    int noOfPendingOrders;
    AddressBookItemDetails deliveryLocation;

    SlotDetails currentSlot;

    BuyerDetails buyerDetails;
    List<AllergyDetails> buyerAllergies;

    List<CuisineDetails> availableCuisines = new ArrayList<>();
    List<AllergyDetails> availableAllergies = new ArrayList<>();
    List<SlotDetails> availableSlots = new ArrayList<>();*/

    // uncaught exception handler variable
    private Thread.UncaughtExceptionHandler defaultUEH;

    // handler listener
    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable exception) {

                    String appVersionName="";
                    int appVersionNumber=0;
                    PackageInfo packageInfo = null;
                    try {
                        packageInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
                        appVersionName = packageInfo.versionName;
                        appVersionNumber = packageInfo.versionCode;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }

                    String crashStackTrace = Log.getStackTraceString(exception);
                    String crashCause =  exception.getCause().toString();
                    int crashLineNumber = exception.getCause().getStackTrace()[0].getLineNumber();
                    String crashMethod = exception.getCause().getStackTrace()[0].getMethodName();
                    String crashClass = exception.getCause().getStackTrace()[0].getClassName();

                    Log.d("GoCiboCrashes", "\n[ APP_CRASH in v"+appVersionName+" (vn"+appVersionNumber+") ] =>  Cause : " + crashCause+" , Stack -> { @Line : "+crashLineNumber+" , @Method : "+crashMethod+" , @Class : "+crashClass+" }");

                }
            };

    public GoCiboApplication() {
        //defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        //Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler); // setup handler for uncaught exception
    }

    public void setCurrentActivityContext(Context context){
        currentActivityContext = context;
    }

    public void showNotificationDialog(String notificationTitle, String notificationSubtitle){
        try{
            if ( notificationDialog == null ){
                notificationDialog = new AlertDialog.Builder(currentActivityContext).create();
                notificationDialogView = LayoutInflater.from(currentActivityContext).inflate(R.layout.dialog_notifcation, null);
                notificationDialog.setView(notificationDialogView);
                notificationDialog.setCancelable(true);
            }
            TextView title = (TextView) notificationDialogView.findViewById(R.id.title);
            TextView subtitle = (TextView) notificationDialogView.findViewById(R.id.subtitle);
            title.setText(notificationTitle);
            subtitle.setText(notificationSubtitle);

            notificationDialog.show();

            notificationDialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( notificationDialog != null ){
                        notificationDialog.dismiss();
                    }
                }
            });
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), notificationTitle+" : "+notificationSubtitle, Toast.LENGTH_LONG).show();
        }
    }
    public void closeNotificationDialog(){
        if ( notificationDialog != null ){
            notificationDialog.dismiss();
        }
    }

    /*public void setBuyerDetails(BuyerDetails buyerDetails) {
        if ( buyerDetails != null ) {
            this.buyerDetails = buyerDetails;
        }
    }

    public BuyerDetails getBuyerDetails() {
        return buyerDetails;
    }*/

    /*public void setCartCount(int cartCount) {
        this.cartCount = cartCount;
    }

    public int getCartCount() {
        return cartCount;
    }*/

    /*public void setCartItems(List<CartItemDetails> cartItems) {
        this.cartItems = cartItems;
    }

    public List<CartItemDetails> getCartItems() {
        return cartItems;
    }*/

    /*public void setNoOfPendingOrders(int noOfPendingOrders) {
        this.noOfPendingOrders = noOfPendingOrders;
    }

    public int getNoOfPendingOrders() {
        return noOfPendingOrders;
    }*/

    /*public void setDeliveryLocation(AddressBookItemDetails deliveryLocation) {
        if (deliveryLocation != null){
            this.deliveryLocation = deliveryLocation;
        }
    }

    public AddressBookItemDetails getDeliveryLocation() {
        return deliveryLocation;
    }*/

    /*public void setCurrentSlot(SlotDetails currentSlot) {
        this.currentSlot = currentSlot;
    }

    public SlotDetails getCurrentSlot() {
        if ( currentSlot != null ){
            return currentSlot;
        }
        return new SlotDetails();
    }*/

    /*public void setAvailableSlots(List<SlotDetails> availableSlots) {
        this.availableSlots = availableSlots;
        if ( availableSlots != null && availableSlots.size() > 0 ){
            //setCurrentSlot(availableSlots.get(0));
        }
    }

    public List<SlotDetails> getAvailableSlots() {
        return availableSlots;
    }*/


    /*public void setAvailableAllergies(List<AllergyDetails> availableAllergies){
        this.availableAllergies = availableAllergies;
    }

    public List<AllergyDetails> getAvailableAllergies() {
        return availableAllergies;
    }

    public void setAvailableCuisines(List<CuisineDetails> availableCuisines) {
        this.availableCuisines = availableCuisines;
    }

    public List<CuisineDetails> getCuisines() {
        return availableCuisines;
    }

    public List<AllergyDetails> getBuyerAllergies() {
        return buyerAllergies;
    }

    public void setBuyerAllergies(List<AllergyDetails> buyerAllergies) {
        if ( buyerAllergies != null ) {
            this.buyerAllergies = buyerAllergies;
        }
    }*/
}
