package in.gocibo.foodie.localdata;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.foodie.constants.Constants;
import in.gocibo.foodie.models.AllergyDetails;
import in.gocibo.foodie.models.BuyerDetails;
import in.gocibo.foodie.models.CategoryDetails;
import in.gocibo.foodie.models.OrderDetails;

public class LocalStorageData {

    Context context;
    CommonMethods commonMethods;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public LocalStorageData(Context context) {
        this.context = context;
        commonMethods = new CommonMethods();
        sharedPreferences =PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    public void setFirstCartItemFlag(boolean status){
        editor.putBoolean("firstCartItemFlag", status);
        editor.commit();
    }
    public boolean isFirstCartItemAdded(){
        return sharedPreferences.getBoolean("firstCartItemFlag", false);
    }

    public void setTCFlag(boolean status){
        editor.putBoolean("seenTC", status);
        editor.commit();
    }
    public boolean getTCFlag(){
        return sharedPreferences.getBoolean("seenTC", false);
    }

    public void storeServingAt(String servingAt){
        editor.putString("serving_at", servingAt);
        editor.commit();
    }

    public String getServingAt(){
        return sharedPreferences.getString("serving_at", "");
    }

    public void setGovtTaxes(int govtTaxes) {
        editor.putInt("govt_taxes", govtTaxes);
        editor.commit();
    }

    public int getGovtTaxes() {
        return  sharedPreferences.getInt("govt_taxes", 0);
    }

    public void setProcessingFees(int processingFee) {
        editor.putInt("processing_fee", processingFee);
        editor.commit();
    }

    public int getProcessingFees() {
        return  sharedPreferences.getInt("processing_fee", 0);
    }

    //---------------------------------------------------------

    public void storeBuyerSession(String accessToken, BuyerDetails buyerDetails){

        editor.putBoolean("sessionFlag", true);

        editor.putString("access_token", accessToken);
        editor.putString("buyer_id", buyerDetails.getID());
        editor.putString("buyer_referral_id", buyerDetails.getReferralID());

        setBuyerDetails(buyerDetails);

        editor.commit();
    }

    public void clearBuyerSession(){
        editor.putBoolean("sessionFlag", false);
        editor.putBoolean("notificationsFlag", true);
        editor.putString("access_token", "");
        editor.putString("buyer_mobile", "");
        editor.putString("buyer_email", "");
        editor.putString("buyer_name", "");
        editor.putString("buyer_id", "");
        editor.putString("buyer_details", "");
        editor.putString("buyer_referral_id", "");
        editor.putInt("buyer_pending_orders_count", 0);
        editor.putString("buyer_pending_orders", "");
        editor.putBoolean("is_buyer_has_favourites", false);
        editor.putString("buyer_favourites_list", "[]");
        editor.putInt("cart_count", 0);
        editor.putString("cart_started_by", "");
        editor.putString("cart_start_time", "");
        editor.putString("deliver_location", "");
        editor.putString("firebase_token", "");
        editor.putString("firebase_token_generated_time", "");
        editor.putBoolean("gcm_registration_id_sent_to_server_status", false);
        editor.commit();
    }

    public boolean getSessionFlag(){
        return sharedPreferences.getBoolean("sessionFlag", false);
    }

    public String getAccessToken(){
        return sharedPreferences.getString("access_token", "");
    }

    public String getBuyerMobile(){
        return sharedPreferences.getString("buyer_mobile", "");
    }

    public void setBuyerMobile(String mobile){
        editor.putString("buyer_mobile", mobile);
        editor.commit();
    }

    public String getBuyerID(){
        return sharedPreferences.getString("buyer_id", "");
    }

    public String getBuyerReferralID(){
        return sharedPreferences.getString("buyer_referral_id", "");
    }

    public String getBuyerName(){
        return sharedPreferences.getString("buyer_name", "");
    }

    public void setBuyerName(String name){
        editor.putString("buyer_name", name);
        editor.commit();
    }

    public void setBuyerEmail(String email){
        editor.putString("buyer_email", email);
        editor.commit();
    }

    public String getBuyerEmail(){
        return sharedPreferences.getString("buyer_email", "");
    }

    //------

    public void storeBuyerAddressBookList(List<AddressBookItemDetails> favouritesList) {
        if ( favouritesList != null && favouritesList.size() > 0 ){
            List<String> favourites_jsons_list = new ArrayList<String>();
            for (int i = 0; i < favouritesList.size(); i++) {
                favourites_jsons_list.add(new Gson().toJson(favouritesList.get(i)));
            }
            editor.putBoolean("is_buyer_has_favourites", true);
            editor.putString("buyer_favourites_list", commonMethods.getJSONArryString(favourites_jsons_list));

        }else{
            editor.putBoolean("is_buyer_has_favourites", false);
            editor.putString("buyer_favourites_list", "[]");
        }
        editor.commit();
    }

    public boolean isBuyerHasFavourites(){
        return sharedPreferences.getBoolean("is_buyer_has_favourites", false);
    }

    public List<AddressBookItemDetails> getBuyerAddressBookList() {
        List<AddressBookItemDetails> favouritesList = new ArrayList<>();
        try {
            JSONArray favourites_jsons_list = new JSONArray(sharedPreferences.getString("buyer_favourites_list", "[]"));
            for (int i = 0; i < favourites_jsons_list.length(); i++) {
                favouritesList.add(new Gson().fromJson(favourites_jsons_list.get(i).toString(), AddressBookItemDetails.class));
            }
        } catch (Exception e) {}

        return favouritesList;
    }

    //-------

    public void setFireBaseToken(String fireBaseToken){
        editor.putString("firebase_token", fireBaseToken);
        editor.putString("firebase_token_generated_time", DateMethods.getCurrentDateAndTime());
        editor.commit();
    }

    public String getFireBaseToken(){
        long firebaseTokenElapsedTime = DateMethods.getDifferenceToCurrentTime(sharedPreferences.getString("firebase_token_generated_time", ""));
        if ( firebaseTokenElapsedTime == -1 || firebaseTokenElapsedTime >= 86400000 ){
            return "";
        }else{
            return sharedPreferences.getString("firebase_token", "");
        }
    }

    public void setNotificationsFlag(boolean notificationsFlag) {
        editor.putBoolean("notificationsFlag", notificationsFlag);
        editor.commit();
    }
    public boolean getNotificationsFlag(){
        return sharedPreferences.getBoolean("notificationsFlag", true);
    }


    //------- Payment Details -----

    public void setPayUKey(String payUKey){
        editor.putString("payu_key", payUKey);
        editor.commit();
    }

    public String getPayUKey() {
        return sharedPreferences.getString("payu_key", "");
    }

    public void setPayUSalt(String payUSalt){
        editor.putString("payu_salt", payUSalt);
        editor.commit();
    }

    public String getPayUSalt() {
        return sharedPreferences.getString("payu_salt", "");
    }

    public void setPayUSuccessURL(String payUSuccessURL){
        editor.putString("payu_success_url", payUSuccessURL);
        editor.commit();
    }
    public String getPayUSuccessURL() {
        return sharedPreferences.getString("payu_success_url", "");
    }

    public void setPayUFailureURL(String payUFailureURL){
        editor.putString("payu_failure_url", payUFailureURL);
        editor.commit();
    }

    public String getPayUFailureURL() {
        return sharedPreferences.getString("payu_failure_url", "");
    }

    //---------------------------------------------

    public void setBuyerDetails(BuyerDetails buyerDetails) {
        if ( buyerDetails != null ) {
            editor.putString("buyer_details", new Gson().toJson(buyerDetails));

            if ( buyerDetails.getMobile() != null && buyerDetails.getMobile().length() > 0 ){
                editor.putString("buyer_mobile", buyerDetails.getMobile());
            }
            if ( buyerDetails.getEmail() != null && buyerDetails.getEmail().length() > 0 ){
                editor.putString("buyer_email", buyerDetails.getEmail());
            }
            if ( buyerDetails.getName() != null && buyerDetails.getName().length() > 0 ){
                editor.putString("buyer_name", buyerDetails.getName());
            }
            editor.commit();
        }else{
            clearBuyerSession();
        }
    }

    public BuyerDetails getBuyerDetails() {
        String buyer_details_json = sharedPreferences.getString("buyer_details", "");
        BuyerDetails buyerDetails = new BuyerDetails();
        buyerDetails = new Gson().fromJson(buyer_details_json, BuyerDetails.class);
        return buyerDetails;
    }

    public void setBuyerAllergies(List<AllergyDetails> buyerAllergies) {
        if ( buyerAllergies != null ){
            BuyerDetails buyerDetails = getBuyerDetails();
            buyerDetails.setAllergies(buyerAllergies);
            setBuyerDetails(buyerDetails);
        }
    }

    public List<AllergyDetails> getBuyerAllergies() {
        try {
            return getBuyerDetails().getAllergies();
        }catch (Exception e){
            return  null;
        }
    }

    public void storeCategories(List<CategoryDetails> categoriesList) {
        if ( categoriesList != null ){
            List<String> categories_jsons_list = new ArrayList<String>();
            for (int i = 0; i < categoriesList.size(); i++) {
                categories_jsons_list.add(new Gson().toJson(categoriesList.get(i)));
            }
            editor.putString("categories_list", commonMethods.getJSONArryString(categories_jsons_list));
            editor.commit();
        }
    }

    public List<CategoryDetails> getCategories() {
        List<CategoryDetails> categoriesList = new ArrayList<>();
        try {
            JSONArray categories_jsons_list = new JSONArray(sharedPreferences.getString("categories_list", "[]"));
            for (int i = 0; i < categories_jsons_list.length(); i++) {
                categoriesList.add(new Gson().fromJson(categories_jsons_list.get(i).toString(), CategoryDetails.class));
            }
        } catch (Exception e) {}

        return categoriesList;
    }


    //------------------------------------------

    public void setCartValidTime(long cartValidTime){
        editor.putLong("cart_valid_time", cartValidTime);
        editor.commit();
    }

    public long getCartValidTime(){
        return  sharedPreferences.getLong("cart_valid_time", 240000);
    }

    public void setCartRemainingTime(long remainingTime){
        editor.putLong("cart_remain_time", remainingTime);
        editor.commit();
    }

    public long getCartRemainingTime(){
        long cartRemainTime = sharedPreferences.getLong("cart_remain_time", 0);
        if ( cartRemainTime <= 0 ){
            cartRemainTime = getCartValidTime();
        }
        return cartRemainTime;
    }

    public void setCartCount(int cartCount){
        editor.putInt("cart_count", cartCount);
        if ( cartCount == 0 ){
            editor.putString("cart_started_by", "");
            editor.putString("cart_start_time", "");
        }
        editor.commit();
    }

    public int getCartCount(){
        return  sharedPreferences.getInt("cart_count", 0);
    }
    public void setCartStartTime(String cart_start_time){
        editor.putString("cart_started_by", getBuyerMobile());
        editor.putString("cart_start_time", cart_start_time);
        editor.commit();
    }

    //------------------------------------------

    public void setDeliveryLocation(AddressBookItemDetails addressBookItemDetails){
        if ( addressBookItemDetails != null ) {
            String deliver_location_json = new Gson().toJson(addressBookItemDetails);
            editor.putString("deliver_location", deliver_location_json);
        }else{
            editor.putString("deliver_location", "");
        }
        editor.commit();
    }

    public AddressBookItemDetails getDeliveryLocation(){
        String deliver_location_json = sharedPreferences.getString("deliver_location", "");
        AddressBookItemDetails deliverLocationDetails = new AddressBookItemDetails();
        deliverLocationDetails = new Gson().fromJson(deliver_location_json, AddressBookItemDetails.class);
        return deliverLocationDetails;
    }

    //---------------------------------------------

    public void setServerTime(String serverTime){
        editor.putString("server_time", serverTime);
        editor.commit();
    }

    public String getServerTime(){
        return sharedPreferences.getString("server_time", "");
    }

    public void setSupportContactNumber(String supportContactNumber){
        editor.putString("support_contact_number", supportContactNumber);
        editor.commit();
    }

    public String getSupportContactNumber(){
        return sharedPreferences.getString("support_contact_number", "");
    }

    public void setClosedPageTitle(String closedPageTitle){
        editor.putString("closed_page_title", closedPageTitle);
        editor.commit();
    }

    public String getClosedPageTitle(){
        return sharedPreferences.getString("closed_page_title", "Closed");
    }

    public void setClosedPageSubTitle(String closedPageSubTitle){
        editor.putString("closed_page_subtitle", closedPageSubTitle);
        editor.commit();
    }

    public String getClosedPageSubTitle(){
        return sharedPreferences.getString("closed_page_subtitle", "");
    }

    //--------------------------------------------

    public int getNoOfPendingOrders(){
        return  sharedPreferences.getInt("buyer_pending_orders_count", 0);
    }

    public void setBuyerPendingOrders(List<OrderDetails> pendingOrders) {
        if ( pendingOrders != null ){
            List<String> pending_orders_jsons = new ArrayList<String>();
            for (int i = 0; i < pendingOrders.size(); i++) {
                pending_orders_jsons.add(new Gson().toJson(pendingOrders.get(i)));
            }
            editor.putInt("buyer_pending_orders_count", pendingOrders.size());
            editor.putString("buyer_pending_orders", commonMethods.getJSONArryString(pending_orders_jsons));
            editor.commit();
        }
    }

    public List<OrderDetails> getBuyerPendingOrders() {
        List<OrderDetails> pendingOrders = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(sharedPreferences.getString("buyer_pending_orders", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                pendingOrders.add(new Gson().fromJson(jsonArray.get(i).toString(), OrderDetails.class));
            }
        } catch (Exception e) {}

        return pendingOrders;
    }

    //=== APP SPECIFIC DETAILS ===//

    public void setDeviceID(String deviceID){
        editor.putString("device_id", deviceID);
        editor.commit();
    }

    public String getDeviceID(){
        return sharedPreferences.getString("device_id", "");
    }

    public void setAppCurrentVersion(){
        int appVersion=0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appVersion = pInfo.versionCode;
        }catch (Exception e){}

        editor.putInt("appVersion", appVersion);
        editor.commit();
    }

    public int getAppVersion(){
        return sharedPreferences.getInt("appVersion", 0);
    }

    public void storeGCMRegistrationID(String registrationID){
        Log.d(Constants.LOG_TAG, "GCM Registration ID = " + registrationID);

        int appVersion=0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appVersion = pInfo.versionCode;
        }catch (Exception e){}

        editor.putString("gcmRegID", registrationID);
        editor.putInt("appVersion", appVersion);
        editor.commit();
    }

    public String getGCMRegistrationID(){
        int currentVersion=0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            currentVersion = pInfo.versionCode;
        }catch (Exception e){}

        if ( currentVersion == getAppVersion() ) {
            return sharedPreferences.getString("gcmRegID", "");
        }else{
            return "";
        }
    }

    public void setSkipAppUpdateVersion(int versionNo){
        editor.putInt("skip_app_update_version", versionNo);
        editor.commit();
    }

    public int getSkipAppUpdateVersion(){
        return sharedPreferences.getInt("skip_app_update_version", -1);
    }

    //-----------------------------------------------------------

    public void setPushNotificationsRegistratinStatus(boolean status){
        editor.putBoolean("gcm_registration_id_sent_to_server_status", status);
        editor.commit();
    }

    public boolean getPushNotificationsRegistratinStatus(){
        return sharedPreferences.getBoolean("gcm_registration_id_sent_to_server_status", false);
    }

    //===========================//

    public void setHomePageLastLoadedTime(String homePageLastLoadedTime){
        editor.putString("home_page_last_loaded_time", homePageLastLoadedTime);
        editor.commit();
    }

    public String getHomePageLastLoadedTime(){
        return  sharedPreferences.getString("home_page_last_loaded_time", "NA");
    }

    //=====================================

}