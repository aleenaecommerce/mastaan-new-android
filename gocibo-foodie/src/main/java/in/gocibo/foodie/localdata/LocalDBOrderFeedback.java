package in.gocibo.foodie.localdata;

import com.google.gson.Gson;

import in.gocibo.foodie.backend.model.RequestOrderFeedback;

/**
 * Created by venkatesh on 9/9/15.
 */

public class LocalDBOrderFeedback {

    String type;
    String orderID;
    String buyerMobile;
    int retryCount;
    String order_feedback_json;

    public LocalDBOrderFeedback(String type, String orderID, String buyerMobile, int retryCount, String order_feedback_json){
        this.type = type;
        this.orderID = orderID;
        this.buyerMobile = buyerMobile;
        this.retryCount = retryCount;
        this.order_feedback_json = order_feedback_json;
    }

    public String getOrderID() {
        return orderID;
    }

    public String getType() {
        return type;
    }

    public String getBuyerMobile() {
        return buyerMobile;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public String getOrderFeedbackJSON() {
        return order_feedback_json;
    }

    public RequestOrderFeedback getOrderFeedbackObject() {
        return new Gson().fromJson(order_feedback_json, RequestOrderFeedback.class);
    }
}
