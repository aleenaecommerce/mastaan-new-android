package in.gocibo.foodie.localdata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;

import in.gocibo.foodie.constants.Constants;

/**
 * Created by venkatesh on 10/7/15.
 */

public class LocationDatabase extends SQLiteOpenHelper {

    Context context;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;

    private static final String DB_NAME = "LocationDatabase";
    private static final int DB_VERSION = 1;

    private static final String LOCATION_TABLE = "locationTable";

    private static final String LATLNG = "latlng";
    private static final String PREMISE = "premise";
    private static final String SUBLOCALITY = "sublocality";
    private static final String LANDMARK = "landmark";
    private static final String LOCALITY = "locality";
    private static final String STATE = "state";
    private static final String COUNTRY = "country";
    private static final String PINCODE = "pin";

    public interface StoredPlaceDetailsCallback{
        void onComplete(boolean isPresent, PlaceDetails placeDetails);
    }

    public interface GetAllStoredPlacesCallback{
        void onComplete(List<PlaceDetails> places);
    }

    private final String CREATE_TABLE="CREATE TABLE IF NOT EXISTS "+ LOCATION_TABLE+" ( "+
            LATLNG+" TEXT ,"+
            PREMISE+" TEXT ,"+
            SUBLOCALITY+" TEXT ,"+
            LANDMARK+" TEXT ,"+
            LOCALITY+" TEXT ,"+
            STATE+" TEXT ,"+
            COUNTRY+" TEXT ,"+
            PINCODE+" TEXT ,"+
            "PRIMARY KEY ("+LATLNG+")"+
            ")";

    public LocationDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+LOCATION_TABLE);
        onCreate(sqLiteDatabase);
    }

    public void openDatabase(){
        sqLiteDatabase = this.getWritableDatabase();
    }

    public void closeDatabase(){
        sqLiteDatabase.close();
    }

    public void addLocation (PlaceDetails placeDetails){
        addLocation(placeDetails.getLatLng(), placeDetails.getPremise(), placeDetails.getSublocalityLevel_2(), placeDetails.getLandmark(), placeDetails.getLocality(), placeDetails.getState(), placeDetails.getCountry(), placeDetails.getPostal_code());
    }

    public void addLocation (AddressBookItemDetails placeDetails){
        addLocation(placeDetails.getLatLng(), placeDetails.getPremise(), placeDetails.getSublocalityLevel_2(), placeDetails.getLandmark(), placeDetails.getLocality(), placeDetails.getState(), placeDetails.getCountry(), placeDetails.getPostalCode());
    }

    public void addLocation (final LatLng latLng, final String premise, final String sublocality, final String landmark, final String locality, final String state, final String country, final String pincode){

        getPlaceDetailsIfPresent(latLng, new LocationDatabase.StoredPlaceDetailsCallback() {
            @Override
            public void onComplete(boolean isPresent, PlaceDetails placeDetails) {

                if (isPresent == false) {
                    Log.d(Constants.LOG_TAG, "LocationDatabase : AddLocation (" + locality.toString() + ") not exists, Adding It");

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {

                            ContentValues contentValues = new ContentValues();
                            contentValues.put(LATLNG, latLng.latitude + "," + latLng.longitude);
                            contentValues.put(PREMISE, premise);
                            contentValues.put(SUBLOCALITY, sublocality);
                            contentValues.put(LANDMARK, landmark);
                            contentValues.put(LOCALITY, locality);
                            contentValues.put(STATE, state);
                            contentValues.put(COUNTRY, country);
                            contentValues.put(PINCODE, pincode);

                            try {
                                sqLiteDatabase.insertOrThrow(LOCATION_TABLE, null, contentValues);
                                Log.d(Constants.LOG_TAG, "LocationDatabase : AddLocation , Success for (LatLng = " + latLng + ")");
                            } catch (Exception e) {
                                Log.d(Constants.LOG_TAG, "LocationDatabase : AddLocation , Error ");
                            }
                            return null;
                        }
                    }.execute();
                } else {
                    Log.d(Constants.LOG_TAG, "LocationDatabase : AddLocation (" + locality.toString() + ") Already exists");
                }
            }
        });

    }

    public void getPlaceDetailsIfPresent(final LatLng latLng, final StoredPlaceDetailsCallback callback){

        final Location targetLocation = new Location("targetLocation");
        targetLocation.setLatitude(latLng.latitude);
        targetLocation.setLongitude(latLng.longitude);

        getAllPlaces(new GetAllStoredPlacesCallback() {
            @Override
            public void onComplete(List<PlaceDetails> places) {

                if (places != null && places.size() > 0) {

                    boolean isPresent = false;
                    PlaceDetails placeDetails = null;

                    for (int i = 0; i < places.size(); i++) {
                        Location currentLocation = new Location("currentLocation");
                        currentLocation.setLatitude(places.get(i).getLatLng().latitude);
                        currentLocation.setLongitude(places.get(i).getLatLng().longitude);

                        if (targetLocation.distanceTo(currentLocation) < 50) {
                            isPresent = true;
                            placeDetails = places.get(i);
                            Log.d(Constants.LOG_TAG, "LocationDatabase : GetStoredLocationDetails (" + latLng.toString() + " , Premise = " + placeDetails.getPremise() + ") Exists");
                            break;
                        }
                    }
                    callback.onComplete(isPresent, placeDetails);

                } else {
                    callback.onComplete(false, null);
                }
            }
        });
    }

    public int getPlacesCount(){
        return ((int) DatabaseUtils.longForQuery(sqLiteDatabase, "SELECT COUNT(*) FROM "+LOCATION_TABLE, null));
    }

    public void getAllPlaces(final GetAllStoredPlacesCallback callback){

        new AsyncTask<Void, Void, Void>() {

            List<PlaceDetails> places = new ArrayList<>();

            @Override
            protected Void doInBackground(Void... voids) {

                cursor = sqLiteDatabase.query(LOCATION_TABLE, new String[]{LATLNG, PREMISE, SUBLOCALITY, LANDMARK, LOCALITY, STATE, COUNTRY, PINCODE}, null, null, null, null, null);

                if ( cursor != null ){
                    if( cursor.moveToFirst() ){
                        do{
                            PlaceDetails placeDetails = new PlaceDetails(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7));
                            places.add(placeDetails);
                        }while( cursor.moveToNext() );
                    }
                    cursor.close();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d(Constants.LOG_TAG, "LocationDatabase : GetStoredLocations , Found = "+places.size());
                callback.onComplete(places);
            }
        }.execute();

    }

    public void deleteAllPlaces(){
        sqLiteDatabase.execSQL("delete from "+ LOCATION_TABLE);
    }

}