package in.gocibo.foodie.models;

/**
 * Created by venkatesh on 7/12/15.
 */
public class CartItemDetails {

    String _id;
    String dby;
    double n;
    ItemDetails d;

    class ItemDetails{
        public String _id;
        public double pr;       // Item Price
        public double opr;      //  Profit Price
        public double cnum;        // Item Quantity

        DishDetails d;
        ChefDetails c;
        AvailDetails a;

        class AvailDetails{
            public String date;
            public String slot;         // Available time
            public String slote;        //  Item End Time if Any
            public double num;         // dishTotal Availability
        }
    }

    //---------------------------

    public String getID() {
        return _id;
    }

    public String getDBY(){
        return  dby;
    }

    public void setDBY(String dby){
        this.dby = dby;
    }

    public double getQuantity() {    return n;     }

    public void setQuantity(double n) {
        this.n = n;
    }

    public String getDishUploadID() {
        return d._id;
    }

    public String getDishID() {
        return d._id;
    }

    public String getDishName(){    return d.d.getName();    }

    public String getDishImage(){    return d.d.getImageURL();    }

    public double getDishPrice(){
        return d.opr;
    }

    public String getDishType() {   return d.d.getType();    }

    public String getDishSlotType() {   return d.d.getSlotType();    }

    public String getDishQuantityType() {
        return d.d.getQuantityType();
    }

    public String getDishAvailableTime(){
        return d.a.slot;
    }

    public String getDishAvailabilityEndTime() {
        if ( d.a.slote != null ){
            return d.a.slote;
        }
        return "";
        //return "2016-01-19T07:29:50.083Z";//d.a.slote;
    }

    public double getDishTotalAvailability(){   return d.a.num;    }

    public double getDishCurrentAvailability(){
        return d.cnum;
    }


    public void setDishCurrentAvailability(double newCurrentAvailability){
        this.d.cnum = newCurrentAvailability;
    }

    public String getDishChefID() {
        return d.c.getID();
    }

    public String getDishChefName() {
        return d.c.getFullName();
    }

    public String getDishChefImageURL() {
        return d.c.getImageURL();
    }

    public String getDishCategoryValue() {
        return d.d.getCategoryValue();
    }
    public String getDishCategoryID() {
        return d.d.getCategoryID();
    }
    public String getDishCategoryName() {
        return d.d.getCategoryName();
    }

}
