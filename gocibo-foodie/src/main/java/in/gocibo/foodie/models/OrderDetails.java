package in.gocibo.foodie.models;

import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class OrderDetails {

    public String _id;      // OrderAccessID
    public String oid;      //  Order ID
    public String cd;       // Created Date

    public double ta;       // Total Amount
    public int pt;          // Payment Type {   0-CashOnDelivery    }

    public String dby;      // DeliverSlot

    public String os;       // Order Status

    public String n;        // User Name
    public String e;        // User Email

    public String ad;       // AddressBookItemID
    public String da;       // Full Address
    public String pos;      // LatLng
    public String lm;       // Landmark
    public String p;        //  Premise
    public String s1;       //  SubLocality-1
    public String s2;       //  SubLocality-2
    public String r;        //  Road No
    public String l;        //  Locality
    public String s;        //  State
    public String c;        //  Country
    public String pin;      // PinCode

    public List<OrderItemDetails> i;        // Ordered Food Items.

    public String mfd;      // MovedFeedbackDate

    //----------------------------------------

    public String getID() {
        return _id;
    }

    public String getOrderID() {
        return oid;
    }

    public void setOrderStaus(String orderStaus) {
        this.os = orderStaus;
    }

    public String getOrderStaus() {
        return os;
    }

    public double getTotalAmount() {
        return ta;
    }

    public String getPaymentType() {
        if ( pt == 0 ){
            return "Cash on delivery";
        }else{
            return "";
        }
    }

    public String getCreatedDate() {
        return cd;
    }

    public String getDeliveredBy() {
        if ( dby != null ) {
            return dby;
        }
        return "";
    }

    public String getUserName() {
        return n;
    }

    public String getUserEmail() {
        return e;
    }

    public List<OrderItemDetails> getOrderItems(){
       if ( i == null ){ return new ArrayList<>();  }
        return i;
    }

//    public List<OrderItemDetails> getOrderItemsExcludingFailed(){
//        List<OrderItemDetails> orderItems = new ArrayList<>();
//        if ( i != null && i.size() > 0 ){
//            for (int a=0;a<i.size();a++){
//                if ( i != null && i.get(a).getDeliverStatus() != 5 && i.get(a).getDeliverStatus() != 6 ){
//                    orderItems.add(i.get(a));
//                }
//            }
//        }
//        return orderItems;
//    }

    public List<OrderItemDetails> getOrderItemsExcludingSubmittedFeedbackItemsAndFailedItems() {
        List<OrderItemDetails> orderItems = new ArrayList<>();
        if ( i != null && i.size() > 0 ){
            for (int a=0;a<i.size();a++){
                if ( i != null && i.get(a).isSubmittedFeedback() == false && i.get(a).getDeliverStatus() != 5 && i.get(a).getDeliverStatus() != 6 ){
                    orderItems.add(i.get(a));
                }
            }
        }
        return orderItems;
    }

    public String getMovedFeedbackDate() {
        return mfd;
    }

    public boolean isAlreadyRemindedFeedbackForLater(){
        if ( mfd != null && mfd.length() > 0 ){
            return true;
        }
        return false;
    }

    //---

    public String getDeliveryAddressID() {
        if ( ad == null ){ ad = "DUMMY_ID"; }
        return ad;
    }

    public LatLng getDeliveryAddressLatLng() {
        return LatLngMethods.getLatLngFromString(pos);
    }

    public String getDeliveryAddressPremise() {
        return p;
    }

    public String getDeliveryAddress() {
        return da;
    }

    public void setDeliveryAddress(String delivery_address) {
        this.da = delivery_address;
    }

    public String getDeliveryAddressSubLocalityLevel1() {
        return s1;
    }

    public String getDeliveryAddressSubLocalityLevel2() {
        return s2;
    }

    public String getDeliveryAddressRoadNo() {
        return r;
    }

    public String getDeliveryAddressLandmark() {
        return lm;
    }

    public void setDeliveryAddressLandmark(String delivery_landmark) {
        this.lm = delivery_landmark;
    }

    public String getDeliveryAddressLocality() {
        return l;
    }

    public String getDeliveryAddressState() {
        return s;
    }

    public String getDeliveryAddressCountry() {
        return c;
    }

    public String getDeliveryAddressPinCode() {
        return pin;
    }
}
