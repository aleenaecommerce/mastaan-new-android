package in.gocibo.foodie.models;

import java.util.List;

/**
 * Created by aleena on 7/11/15.
 */
public class SimilarSlotTypes {
    List<String> tb;
    List<String> ad;
    List<String> md;

    public List<String> getAllDaySlotTypes() {
        return ad;
    }

    public List<String> getMultiDaySlotTypes() {
        return md;
    }

    public List<String> getTimeBasedSlotTypes() {
        return tb;
    }
}
