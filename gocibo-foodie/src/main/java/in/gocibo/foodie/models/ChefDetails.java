package in.gocibo.foodie.models;

import java.io.Serializable;

public class ChefDetails implements Serializable{

    public String _id;      //  Chef ID
    public String f;        //  Chef First Name
    public String l;        //  Chef Last Name
    public String p;        //  Chef Picture URL
    public String t;        //  Chef Thumbnail Picture URL
    public String b;        //  Chef About(Bio)
    public String e;        //  Chef Email
    public NativityDetails n;        //  Chef Nativity
    public String m;
    public String g;
    public String d;
    public String a;
    public String a1;
    public String a2;
    public String lmk;
    public String ar;
    public String ci;
    public String pin;
    public String s;
    public String z;
    public String et;
    public Integer V;
    public Boolean st;
    public String loc;
    public String cd;
    public String ud;

    class NativityDetails{
        public String n;       //  Nativity Name
        public String m;
        public String s;
    }

    public String getID(){  return _id;  }
    public String getFullName(){
        if ( l == null && f != null ){   return f;   }
        else if ( f == null & l != null ){  return l;   }
        else if ( f == null && l == null ){  return "";  }

        return f+" "+l;
    }
    public String getFirstName(){
        if ( f != null ){
            return f;
        }else{
            return "";
        }
    }
    public String getThumbnailImageURL() {
        return "http:"+t;
    }
    public String getImageURL() {
        return "http:"+p;
    }

    public String getNativity() {
        if ( n != null ) {
            return n.n;
        }
        return "";
    }

    public String getJoinDate(){   return cd;   }
    public String getAbout(){
        return b;
    }
    public int getDishesServed(){
        return 100;
    }
    public int getLikes(){
        return 100;
    }

}
