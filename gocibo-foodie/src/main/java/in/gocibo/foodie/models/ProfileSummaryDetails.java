package in.gocibo.foodie.models;

/**
 * Created by venkatesh on 15/8/15.
 */

public class ProfileSummaryDetails {

    BuyerDetails user;
    Stats stats;

    class Stats{
        int orders;              //  No. of Orders
    }

    public BuyerDetails getBuyerDetails() {
        return user;
    }

    public int getNoOfOrders() {
        return stats.orders;
    }
}
