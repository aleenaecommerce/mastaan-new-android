package in.gocibo.foodie.models;

/**
 * Created by venkatesh on 15/8/15.
 */

public class AllergyDetails {

    String _id;         //  ID
    String n;           //  Name
    String cd;          //  CreatedDate
    String up;          //  UpdatedDate

    public String getID() {
        return _id;
    }

    public String getName() {
        return n;
    }

    public String getCreatedDate() {
        return cd;
    }
}
