package in.gocibo.foodie.models;

import com.aleena.common.methods.DateMethods;

import java.io.Serializable;
import java.util.List;

public class BuyerDetails implements Serializable{

    String token;   //  Token

    String _id;     //  ID
    String m;       // Mobile Number
    String e;       //  Email Address
    String f;       //  First Name
    String l;       //  Last Name
    String d;       //  DateOfBirth
    boolean mr;     //  Matrial Status
    String a;       //  Anniversary Date if Married

    List<AllergyDetails> al;    //  Allergies

    String r;       // Referral ID

    boolean sb;     // SuperBuyer

    String cd;
    String ud;

    public BuyerDetails(){}

    public BuyerDetails(String ID, String joinedDate, String mobileNumber, String name, String email, String dateOfBirth, boolean matrialStatus, String anniversaryDate, String referralID){
        this._id = ID;
        this.cd = joinedDate;
        this.m = mobileNumber;
        this.f = name;
        this.e = email;
        this.d = dateOfBirth;
        this.mr = matrialStatus;
        this.a = anniversaryDate;
        this.r = referralID;
    }

    public String getID() {    return _id;     }
    public String getMobile() {      return m;   }

    public String getEmail() {
        return e;
    }
    public void setEmail(String email){  this.e = email;   }

    public String getName() {
        return f;
    }
    public void setName(String name){  this.f = name;   }

    public String getDOB() {
        return DateMethods.getReadableDateFromUTC(d);
    }
    public void setDOB(String dob){  this.d = dob;   }

    public boolean getMatrialStatus() {
        return mr;
    }
    public void setMatrialStatus(boolean mr) {
        this.mr = mr;
    }

    public String getAnniversary() {
        return DateMethods.getReadableDateFromUTC(a);
    }
    public void setAnniversary(String anniversary){  this.a = anniversary;   }

    public void setAllergies(List<AllergyDetails> al) {
        this.al = al;
    }

    public List<AllergyDetails> getAllergies() {
        return al;
    }

    public String getReferralID() {
        return r;
    }

    public boolean isSuperBuyer() {
        return sb;
    }

    public String getJoinedDate() {     return DateMethods.getReadableDateFromUTC(cd);      }
}
