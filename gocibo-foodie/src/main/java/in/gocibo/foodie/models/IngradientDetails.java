package in.gocibo.foodie.models;

/**
 * Created by venkatesh on 28/11/15.
 */
public class IngradientDetails {
    public String _id;      // Ingradient ID
    public String n;        // Ingradient Name

    public String getID() {
        return _id;
    }

    public String getName() {
        return n;
    }
}
