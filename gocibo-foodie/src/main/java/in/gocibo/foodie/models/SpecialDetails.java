package in.gocibo.foodie.models;

/**
 * Created by aleena on 7/11/15.
 */
public class SpecialDetails {
    boolean open;
    boolean available;
    String title;
    String subTitle;
    String icon;

    OperatingHours operatingHours;
    DeliveringHours deliveringHours;

    String range;

    class OperatingHours{
        String fromHour;
        String fromMinutes;
        String toHour;
        String toMinutes;

        public String getFromTime() {
            return fromHour+":"+fromMinutes;
        }

        public String getEndTime() {
            return toHour+":"+toMinutes;
        }
    }

    class DeliveringHours{
        String fromHour;
        String fromMinutes;
        String toHour;
        String toMinutes;

        public String getFromTime() {
            return fromHour+":"+fromMinutes;
        }

        public String getEndTime() {
            return toHour+":"+toMinutes;
        }
    }

    public boolean isOpen() {
        return open;
    }

    public boolean isAvailable() {
        return available;
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public String getImageURL() {
        return icon;
    }

    public String getDeliverySlotsStartTime(){ return deliveringHours.getFromTime();}

    public String getDeliverySlotsEndTime(){
        return deliveringHours.getEndTime();
    }

    public String getOperationsStartTime(){
        return operatingHours.getFromTime();
    }

    public String getOperationsEndTime(){
        return operatingHours.getEndTime();
    }

    public String getRange() {
        return range;
    }
}
