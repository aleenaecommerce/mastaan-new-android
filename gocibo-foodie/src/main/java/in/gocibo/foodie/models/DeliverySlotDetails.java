package in.gocibo.foodie.models;

/**
 * Created by venkatesh on 29/9/15.
 */
public class DeliverySlotDetails {

    public String displayText;
    public String headerDisplay;

    public String fromHour;
    public String fromMinutes;
    public String toHour;
    public String toMinutes;

    public String value;

    public DeliverySlotDetails(){}

    public DeliverySlotDetails(String displayText, String headerDisplay,String fromHour, String fromMinutes, String toHour, String toMinutes, String value){
        this.displayText = displayText;
        this.headerDisplay = headerDisplay;
        this.fromHour = fromHour;
        this.fromMinutes = fromMinutes;
        this.toHour = toHour;
        this.toMinutes = toMinutes;
        this.value = value;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText){
        this.displayText = displayText;
    }

    public String getHeaderDisplayText() {
        return headerDisplay;
    }

    public String getStartTime() {
        return fromHour+":"+fromMinutes;
    }

    public String getEndTime() {
        return toHour+":"+toMinutes;
    }

    public String getValue() {
        return value;
    }

}
