package in.gocibo.foodie.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 28/11/15.
 */
public class DishDetails {

    String _id;                 //  dishID

    public String n;            // dishName
    public String d;            // dishDescription
    public int t;               // dishType   { veg / nonveg }
    public String p;            // dishImageURL
    public String th;           // dishImageThubnailURL
    public String qt;           //  QuantityTYpe

    ChefDetails c;              //  chefDetails
    public CuisineDetails cu;   //  Cuisine Details
    List<IngradientDetails> i;             // Ingradients Details

    CategoryDetails cat;

    String st;                  //  Slot Type

    public String getID(){  return _id;  }

    public String getName(){
        if ( n != null ) {
            return n;
        }
        return "";
    }

    public String getDescription(){     return  d;    }

    public String getImageURL(){
        String str = "http:";
        if (p != null) {
            str = str + p;
            return str;
        }
        return null;
    }

    public String getThumbnailImageURL() {
        String str = "http:";
        if (th != null) {
            str = str + th;
            return str;
        }
        return null;
    }

    public String getType(){
        if ( t == 1 ){
            return "non-veg";
        }
        else if ( t== 2 ){
            return "jain";
        }
        return "veg";
    }

    public String getCuisineName(){
        return cu.getName();
    }

    public String getQuantityType(){  return qt;    }

    public String getChefID(){  return  c.getID(); }

    public String getChefFullName(){    return c.getFullName();    }

    public String getChefFirstName(){    return c.getFirstName();    }

    public String getChefImageURL(){
        return c.getImageURL();
    }

    public String getChefThumbnailImageURL(){
        return c.getThumbnailImageURL();
    }

    public List<IngradientDetails> getIngradients(){
        if ( i != null ) {
            return i;
        }
        return new ArrayList<>();
    }

    public int getLikes(){  return 10;  }

    public String getCategoryValue() {
        return cat.getValue();
    }
    public String getCategoryID() {
        return cat.getID();
    }
    public String getCategoryName() {
        return cat.getName();
    }

    public String getSlotType() {
        return st;
    }
}
