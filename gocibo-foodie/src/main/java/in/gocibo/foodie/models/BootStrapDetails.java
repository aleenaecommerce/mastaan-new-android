package in.gocibo.foodie.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by venkatesh on 13/7/15.
 */

public class BootStrapDetails {

    boolean upgrade;
    boolean compulsoryUpgrade;
    int latestVersion;

    String serverTime;
    String servingAt;
    long cartTimeout;    //  Cart Valid Time
    SlotDetails slotDetails;

    String supportContactNumber;

    List<CategoryDetails> categories;


    BuyerDetails user;

    long cartRemainingTime;
    int cartItems;              // No of Items in Cart
    String cartAddressID;       // CartDeliveryAddress ID
    String cartPos;             //  CartDeliveryAddress LatLng
    String cartAddress;         //  CartDeliveryAddress FullName

    List<AddressBookItemDetails> addressBook;

    List<OrderDetails> pendingOrders;     //  Pending Orders
    List<OrderDetails> pendingFeedback;     //  Pending Feedback Orders

    String pmKey;
    String pmSalt;
    String paymentSuccessURL;
    String paymentFailureURL;

    class SlotDetails {
        boolean open;
        String title;
        String subTitle;

        public boolean isOpen() {
            return open;
        }
        public String getTitle() {
            return title;
        }
        public String getSubTitle() {
            return subTitle;
        }
    }

    public boolean isUpgradeAvailable() {
        return upgrade;
    }

    public boolean isCompulsoryUpgrade() {
        return compulsoryUpgrade;
    }

    public int getLatestVersion() {
        return latestVersion;
    }

    public boolean isOpen(){
        return slotDetails.isOpen();
    }

    public String getClosedPageTitle(){
        return slotDetails.getTitle();
    }

    public String getClosedPageSubTitle(){
        return slotDetails.getSubTitle();
    }


    public String getServerTime() {
        return serverTime;
    }

    public String getSupportContactNumber() {
        return supportContactNumber;
    }

    public String getServingAt() {
        return servingAt;
    }

    public long getCartValidTime() {
        return cartTimeout;
    }

    public int getCartItemsCount() {
        return cartItems;
    }

    public String getCartDeliveryAddressID() {
        if ( cartAddressID == null ){
            return "";
        }
        return cartAddressID;
    }

    public String getCartDeliveryAddressFullName() {
        return cartAddress;
    }

    public LatLng getCartDeliveryAddressLatLng() {
        return LatLngMethods.getLatLngFromString(cartPos);
    }


    public long getCartRemainingTime() {
        return cartRemainingTime;
    }

    public BuyerDetails getBuyerDetails() {
        return user;
    }

    public List<OrderDetails> getPendingFeedbackOrders(){
        return pendingFeedback;
    }

    public List<AddressBookItemDetails> getAddressBookList() {
        return addressBook;
    }

    public List<OrderDetails> getPendingOrders() {
        return pendingOrders;
    }
    public List<CategoryDetails> getCategoriesList() {
        return categories;
    }

    public String getPayUKey() {
        return pmKey;
    }

    public String getPayUSalt() {
        return pmSalt;
    }

    public String getPayUSuccessURL() {
        return paymentSuccessURL;
    }

    public String getPayUFailureURL() {
        return paymentFailureURL;
    }
}
