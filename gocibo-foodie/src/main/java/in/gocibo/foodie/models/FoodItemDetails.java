package in.gocibo.foodie.models;

import java.util.List;

public class FoodItemDetails{

    public String _id;          // foodItemID
    public double pr;           // dishPrice
    public double opr;          // dishProfitPrice
    public double cnum;            // dish Current Availability
    public String p;            // dishImageURL
    public String t;           // dishImageThubnailURL

    public String st;           //  Slot Type

    public DishDetails d;       // Dish Details
    public AvailDetails a;      // Availability Details

    class AvailDetails{
        public String date;
        public String slot;         // Available time
        public String slote;        //  Item End Time if Any
        public double num;         // dishTotal Availability
    }

    public String getID(){  return _id;  }

    public String getName(){
        return d.getName();
    }
    public String getDescription(){     return  d.getDescription();    }

    public double getPrice(){   return  opr;      }

    public double getProfitPrice(){   return  opr;      }

    public String getImageURL(){
        if (p != null) {
            return "http:" + p;
        }
        return d.getImageURL();
    }

    public String getThumbnailImageURL() {
        if(t != null) {
            return "http:" + t;
        }
        return d.getThumbnailImageURL();
    }
    public String getType(){
        return d.getType();
    }

    public String getSlotType() {
        return st;
    }

    public String getAvailabilityEndTime() {
        if ( a.slote != null ){
            return a.slote;
        }
        return "";
        //return "2016-01-19T07:29:50.083Z";//a.slote;
    }

    public String getCuisineName(){
        return d.getCuisineName();
    }

    public String getQuantityType(){  return d.getQuantityType();    }

    public String getAvailableTime(){
        return a.slot;
    }
    public double getCurrentAvailability(){   return cnum;    }
    public double getTotalAvailability(){   return a.num;    }
    public void setAvailability(double newAvailability) {
        this.cnum = newAvailability;
    }

    public String getChefID(){  return  d.getChefID(); }
    public String getChefFullName(){    return d.getChefFullName() ;   }
    public String getChefFirstName(){    return d.getChefFirstName();    }
    public String getChefImageURL(){
        return d.getChefImageURL();
    }
    public String getChefThumbnailImageURL(){
        return d.getChefThumbnailImageURL();
    }

    public List<IngradientDetails> getIngradients(){
        return d.getIngradients();
    }

    public String getCategoryValue() {
        return d.cat.getValue();
    }
    public String getCategoryID() {
        return d.cat.getID();
    }
    public String getCategoryName() {
        return d.cat.getName();
    }

    // Not Required when Backend Integration is DONE..

    public FoodItemDetails(){}

    public FoodItemDetails(String id, String name, double price, String imageURL, String thumbURL, int type, int quantity, String chefID, String chefName, String chefImageURL){

        this.d = new DishDetails();
        this.a = new AvailDetails();
        this.d.c = new ChefDetails();

        this._id = id;
        this.d.n = name;
        this.pr = price;
        this.d.p = imageURL;
        this.d.t = type;
        this.d.th = thumbURL;

        this.cnum = quantity;

        this.d.c._id = chefID;
        this.d.c.f = chefName;
        this.d.c.p = chefImageURL;
    }
}
