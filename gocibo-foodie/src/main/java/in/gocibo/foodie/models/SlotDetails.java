package in.gocibo.foodie.models;

import java.util.List;

/**
 * Created by venkatesh on 4/9/15.
 */

public class SlotDetails{
    public boolean current;
    public String name;
    public String range;
    public String start;
    public String end;

    public  DeliverySlotDetails deliveringHours;

    public List<DeliverySlotDetails> deliverySlots;


    public String getName() {
        return name;
    }

    public String getRange() {
        return range;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public boolean isCurrentSlot() {
        return current;
    }

    public List<DeliverySlotDetails> getDeliverySlots() {
        return deliverySlots;
    }

    public DeliverySlotDetails getDeliveringHours() {
        return deliveringHours;
    }
}

