package in.gocibo.foodie.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 16/3/16.
 */
public class CartTotalDetails {

    //String msg;                     // Message
    float ta;                       // Total Amount
    float govtTaxes;                //
    float processingFees;           //
    float ccd;                      // CouponCode Discount

    String cc;                      // CouponCode
    List<String> ed;                // CouponCodeWorkingDays

    public CartTotalDetails(float cartTotal, float cartGovtTaxes, float cartProcessingFees, float cartCouponDiscount, String couponCode, List<String> couponEnabledDays){
        //this.msg = message;
        this.ta = cartTotal;
        this.govtTaxes = cartGovtTaxes;
        this.processingFees = cartProcessingFees;
        this.ccd = cartCouponDiscount;
        this.cc = couponCode;
        this.ed = couponEnabledDays;

    }

    /*public String getMessage() {
        return msg;
    }*/

    public float getTotal() {
        return ta;
    }

    public float getSubTotal() {
        return (ta-govtTaxes-processingFees+ccd);
    }

    public float getGovtTaxes() {
        return govtTaxes;
    }

    public float getProcessingFees() {
        return processingFees;
    }

    public float getCouponDiscount() {
        return ccd;
    }

    public String getCouponCode() {
        return cc;
    }

    public List<String> getCouponWorkingDays() {
        if ( ed == null ){ ed = new ArrayList<>();  }
        return ed;
    }
}
