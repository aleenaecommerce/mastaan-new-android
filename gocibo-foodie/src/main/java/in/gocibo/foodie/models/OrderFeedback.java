package in.gocibo.foodie.models;

/**
 * Created by venkatesh on 5/10/15.
 */

public class OrderFeedback {
    String o;   //  OrderID
    String d;   //  DishID
    float r;   //  Rating
    String c;   //  Comments

    public OrderFeedback(String orderID, String dishID, float rating, String comments){
        this.o = orderID;
        this.d = dishID;
        this.r = rating;
        this.c = comments;
    }

    public void setComments(String comments) {
        this.c = comments;
    }
}
