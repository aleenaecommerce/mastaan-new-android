package in.gocibo.foodie.models;

/**
 * Created by venkatesh on 31/8/15.
 */

public class CuisineDetails {
    String _id;
    String n;
    String value;
    int count;

    public String getID() {
        return _id;
    }

    public String getName() {
        return n;
    }

    public String getValue() {
        return value;
    }

    public int getCount() {
        return count;
    }
}
