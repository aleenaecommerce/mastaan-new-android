package in.gocibo.foodie.models;

import java.util.List;

/**
 * Created by venkatesh on 30/11/15.
 */

public class CategoryDetails {

    String _id;
    String v;
    String n;
    String th;

    long count = -1;  //  Local Value

    List<SlotDetails> timeBasedSlots;
    AllDaySlotsDetails allDaySlots;

    class AllDaySlotsDetails{
        OperatingHours operatingHours;
        DeliveringHours deliveringHours;

        class OperatingHours{
            String fromHour;
            String fromMinutes;
            String toHour;
            String toMinutes;

            public String getFromTime() {
                return fromHour+":"+fromMinutes;
            }

            public String getEndTime() {
                return toHour+":"+toMinutes;
            }
        }

        class DeliveringHours{
            String fromHour;
            String fromMinutes;
            String toHour;
            String toMinutes;

            public String getFromTime() {
                return fromHour+":"+fromMinutes;
            }

            public String getEndTime() {
                return toHour+":"+toMinutes;
            }
        }

        public String getDeliverySlotsStartTime(){ return deliveringHours.getFromTime();}

        public String getDeliverySlotsEndTime(){
            return deliveringHours.getEndTime();
        }

        public String getOperationsStartTime(){
            return operatingHours.getFromTime();
        }

        public String getOperationsEndTime(){
            return operatingHours.getEndTime();
        }

    }


    public CategoryDetails(){}

    public CategoryDetails(String categoryID, String categoryName, String imageURL){
        this._id = categoryID;
        this.v = categoryID;
        this.n = categoryName;
        this.th = imageURL;
    }

    public String getID() {
        return _id;
    }

    public String getValue() {
        return v;
    }

    public String getName() {
        if ( n == null ){   n = ""; }
        return n;
    }

    public String getImageURL() {
        return "http:"+th;
    }

    public List<SlotDetails> getTimeBasedSlots() {
        return timeBasedSlots;
    }

    public SlotDetails getCurrentTimeBasedSlot(){
        SlotDetails currentTimeBasedSlot = new SlotDetails();
        if ( timeBasedSlots != null ){
            for (int i = 0; i < timeBasedSlots.size(); i++) {
                if ( timeBasedSlots.get(i).isCurrentSlot() ){
                    currentTimeBasedSlot = timeBasedSlots.get(i);
                }
            }
        }
        return currentTimeBasedSlot;
    }

    public String getAllDaySlotDeliverySlotsStartTime(){ return allDaySlots.getDeliverySlotsStartTime();}

    public String getAllDayDeliverySlotsEndTime(){
        return allDaySlots.getDeliverySlotsEndTime();
    }

    public String getAllDayOperationsStartTime(){
        return allDaySlots.getOperationsStartTime();
    }

    public String getAllDayOperationsEndTime(){
        return allDaySlots.getOperationsEndTime();
    }

    public long getUploadsCount() {
        return count;
    }

    public void setUploadsCount(long uploads_count) {
        this.count = uploads_count;
    }
}
