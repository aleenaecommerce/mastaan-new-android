package in.gocibo.foodie.models;

/**
 * Created by venkatesh on 7/12/15.
 */
public class OrderItemDetails {

    public String _id;              // subOrderID
    public double num;              // Quantity
    public String dby;
    public int status;          // Item Delivery Status

    String f;               // FeedbackID;

    Dish dish;

    class Dish {
        public String _id;
        DishDetails d;
        public double pr;       // Item Price
        public double opr;      //  Profit Price
    }


    //----------------------------

    public String getID() {
        return _id;
    }

    public String getDBY(){
        return  dby;
    }

    public int getDeliverStatus() {
        return status;
    }

    public void setDeliverStatus(int deliverStatus) {
        this.status = deliverStatus;
    }

    public double getQuantity() {    return num;     }

    public String getDishID(){    return dish._id;    }

    public String getDishName(){    return dish.d.getName();    }

    public String getDishImage(){
        return dish.d.getImageURL();
    }

    public double getDishPrice(){
        return dish.opr;
    }

    public String getDishType() {   return dish.d.getType();    }

    public String getDishQuantityType() {
        return dish.d.getQuantityType();
    }

    public String getDishChefID() {
        return dish.d.c.getID();
    }

    public String getDishChefFullName() {
        return dish.d.c.getFullName();
    }

    public String getDishChefFirstName() {
        return dish.d.c.getFirstName();
    }


    public String getDishChefImage() {
        return dish.d.c.getImageURL();
    }

    public String getFeedbackID() {
        return f;
    }

    public boolean isSubmittedFeedback(){
        if ( f != null && f.length() > 0 ){
            return true;
        }
        return false;
    }
}
