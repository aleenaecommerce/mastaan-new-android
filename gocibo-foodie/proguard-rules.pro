# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/venkatesh/adt-bundle-linux-x86_64-20140702/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and orderItemDetails by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

########### ANNOTIONS ##########




########### DEPENDENCIES #######

-keepattributes Signature

-dontwarn com.aleena.common.** { *; }
-keep class com.aleena.common.** { *; }
-keep interface com.aleena.common.** { *; }
-keepnames com.aleena.common.** { *; }

-dontwarn org.w3c.dom.**

-keep class com.google.gson.** { *; }

-dontwarn org.codehaus.jackson.**
-keep  class com.fasterxml.jackson.** {*;}
-keep  class com.fasterxml.jackson.annotation.** {*;}
-keep  class com.fasterxml.jackson.core.** {*;}
-keep  class com.fasterxml.jackson.databind.** {*;}

-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }

-dontwarn com.google.android.gms.**
-keep class com.google.android.gms.**
-keep interface com.google.android.gms.** { *; }

-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn retrofit.**
-dontwarn retrofit.appengine.UrlFetchClient
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}

-dontwarn com.squareup.picasso.**
-keep class com.squareup.picasso.** { *; }
-keep interface com.squareup.picasso.** { *; }

-dontwarn com.firebase.**
-keep class com.firebase.** { *; }
-keep interface com.firebase.** { *; }

-dontwarn com.freshdesk.mobihelp.**
-keep class com.freshdesk.mobihelp.** { *; }
-keep interface com.freshdesk.mobihelp.** { *; }

-dontwarn com.rey.material.**
-keep class com.rey.material.** { *; }
-keep interface com.rey.material.** { *; }

-dontwarn com.melnykov.fab.**
-keep class com.melnykov.fab.** { *; }
-keep interface com.melnykov.fab.** { *; }

-dontwarn uk.co.deanwild.materialshowcaseview.**
-keep class uk.co.deanwild.materialshowcaseview.** { *; }
-keep interface uk.co.deanwild.materialshowcaseview.** { *; }
