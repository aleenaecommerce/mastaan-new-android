# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/venkatesh/adt-bundle-linux-x86_64-20140702/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and orderItemDetails by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# =========================

-keepattributes SourceFile,LineNumberTable
-keep class * extends android.app.Application
-keep class * implements android.os.Parcelable { public static final android.os.Parcelable$Creator *; }

# ======= COMMONLIB =======

-keepclassmembers class com.aleena.common.models.** { *; }
-keepclassmembers class com.aleena.common.location.model.** { *; }
#-keep class com.aleena.common.** { *; }

# ======= MASTAAN =========

-keepclassmembers class com.mastaan.buyer.models.** { *; }
-keepclassmembers class com.mastaan.buyer.backend.models.** { *; }
#-keep class com.mastaan.mastaan.** { *; }

# ======= APPCOMPAT =======

-keep class android.support.v7.widget.SearchView { *; }

# ======== RAZORPAY =======

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

-keepattributes JavascriptInterface
-keepattributes *Annotation*

-dontwarn com.razorpay.**
-keep class com.razorpay.** {*;}

-optimizations !method/inlining/*

-keepclasseswithmembers class * {
  public void onPayment*(...);
}

# ===== RETROFIT & PICASSO ======

-keep class retrofit.** { *; }
-dontwarn retrofit.**
-dontwarn okio.**
-dontwarn com.squareup.okhttp.**
-dontwarn com.squareup.okhttp.**

# ======== HOTLINE =========

-keep class com.freshdesk.hotline.** { *; }
-keep class com.demach.konotor.** { *; }
-keep class uk.co.chrisjenx.calligraphy.** { *; }
-dontwarn uk.co.chrisjenx.calligraphy.**

# ========= JACKSON LIB ======

-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}
-keepnames class com.fasterxml.jackson.** { *; }
-dontwarn com.fasterxml.jackson.databind.**

# =====  FIREBASE DATABASE ====

-keep class com.firebase.** { *; }
-dontwarn com.firebase.**
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }
-dontwarn org.w3c.dom.**
-dontwarn org.joda.time.**
-dontwarn org.shaded.apache.**
-dontwarn org.ietf.jgss.**
-dontnote com.firebase.client.core.GaePlatform

