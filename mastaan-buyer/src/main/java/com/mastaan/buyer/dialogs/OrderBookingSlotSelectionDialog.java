package com.mastaan.buyer.dialogs;

/**
 * Created by venkatesh on 17/12/15.
 */
public class OrderBookingSlotSelectionDialog{/*} implements View.OnClickListener{*/

    /*CustomDialog customDialog;

    vToolBarActivity activity;
    Context context;
    LocalStorageData localStorageData;
    BackendAPIs backendAPIs;

    //DIALOG
    View dialogDeliverySlotView;
    ViewSwitcher dateTimeSwitcher;
    ViewSwitcher viewSwitcher;
    ProgressBar loading_indicator;
    FloatingActionButton reload_indicator;
    TextView loading_message;
    TextView no_data_message;
    //
    Button dateView;
    Button slotView;
    RobotoCondensedTextView selectedDateView;
    RobotoCondensedTextView selectedSlotView;
    TextView pre_order_items_count;
    MaterialCalendarView calendarView;
    TextView cart_prep_time;
    NumberPicker timePicker;
    Button done;
    Button cancel;

    double maxCartPreTime;
    int preOrderItemsCount;

    List<DayDetails> nonServingDates = new ArrayList<>();
    List<String> nonCouponDates = new ArrayList<>();
    SimpleDateFormat simpleDateFormat;
    String serverTime;

    List<String> showingSlots;

    OrderBookingSlotSelectionFlowCallback callback;

    public interface OrderBookingSlotSelectionFlowCallback{
        void onComplete(String selectedDate);
    }

    public OrderBookingSlotSelectionDialog(final vToolBarActivity activity){
        this.activity = activity;
        this.context = activity;
        localStorageData = new LocalStorageData(context);
        backendAPIs = new BackendAPIs(context, activity.getDeviceID(), activity.getAppVersionCode());

        nonServingDates = localStorageData.getDisabledDates();
        simpleDateFormat= new SimpleDateFormat("dd-MM-yyyy");
        serverTime = localStorageData.getServerTime();

        //--------- DIALOG

        dialogDeliverySlotView =  LayoutInflater.from(context).inflate(R.layout.dialog_delivery_slot, null);
        dateTimeSwitcher = (ViewSwitcher) dialogDeliverySlotView.findViewById(R.id.dateTimeSwitcher);
        viewSwitcher = (ViewSwitcher) dialogDeliverySlotView.findViewById(R.id.viewSwitcher);
        loading_indicator = (ProgressBar) dialogDeliverySlotView.findViewById(R.id.loading_indicator);
        reload_indicator = (FloatingActionButton) dialogDeliverySlotView.findViewById(R.id.reload_indicator);
        loading_message = (TextView) dialogDeliverySlotView.findViewById(R.id.loading_message);
        no_data_message = (TextView) dialogDeliverySlotView.findViewById(R.id.no_data_message);

        dateView = (Button) dialogDeliverySlotView.findViewById(R.id.dateView);
        dateView.setOnClickListener(this);
        slotView = (Button) dialogDeliverySlotView.findViewById(R.id.slotView);
        slotView.setOnClickListener(this);
        selectedDateView = (RobotoCondensedTextView) dialogDeliverySlotView.findViewById(R.id.selectedDateView);
        selectedSlotView = (RobotoCondensedTextView) dialogDeliverySlotView.findViewById(R.id.selectedSlotView);
        pre_order_items_count = (TextView) dialogDeliverySlotView.findViewById(R.id.pre_order_items_count);
        calendarView = (MaterialCalendarView) dialogDeliverySlotView.findViewById(R.id.calendarView);
        cart_prep_time = (TextView) dialogDeliverySlotView.findViewById(R.id.cart_prep_time);
        timePicker = (NumberPicker) dialogDeliverySlotView.findViewById(R.id.timePicker);

        done = (Button) dialogDeliverySlotView.findViewById(R.id.done);
        done.setOnClickListener(this);
        cancel = (Button) dialogDeliverySlotView.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(MaterialCalendarView widget, CalendarDay selectedDate, boolean selected) {

                String selDate = String.format("%02d", selectedDate.getDay()) + "-" + String.format("%02d", (selectedDate.getMonth() + 1)) + "-" + selectedDate.getYear();

                // IF SERVING DATE
                if ( isServingDate(selDate) ) {

                    // IF COUPON APPLICABLE DATE
                    if ( isCouponWokringDate(selDate) ){
                        selectedSlotView.setText("SELECT TIME");
                        done.setText("SELECT TIME");

                        selectedDateView.setText(DateMethods.getDateInFormat(selDate, DateConstants.MMM_DD_YYYY));
                        getSlotsForDate(selectedDateView.getText().toString());         // LOADING SLOTS FROM BACKEND
                    }
                    // IF COUPON NOT APPLICABLE DATE
                    else{
                        calendarView.setSelectedDate(DateMethods.getCalendarFromDate(selectedDateView.getText().toString()));
                        Toast.makeText(context, "Coupon not applicable on " + DateMethods.getDateInFormat(selDate, DateConstants.MMM_DD_YYYY), Toast.LENGTH_LONG).show();
                    }
                }
                // IF NOT SERVING DATE
                else {
                    calendarView.setSelectedDate(DateMethods.getCalendarFromDate(selectedDateView.getText().toString()));
                    Toast.makeText(context, "Not serving on " + DateMethods.getDateInFormat(selDate, DateConstants.MMM_DD_YYYY), Toast.LENGTH_LONG).show();
                }
            }
        });

        timePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                selectedSlotView.setText(timePicker.getDisplayedValues()[newVal]);
            }
        });


    }

    @Override
    public void onClick(View view) {

        if ( view == dateView ){
            dateTimeSwitcher.setDisplayedChild(0);
            done.setText("SELECT TIME");
            selectedSlotView.setText("SELECT TIME");
        }

        else if ( view == slotView ){
            switchToSlotSelection();
        }

        else if ( view == cancel ){
            closeCustomDialog();
        }

        else if ( view == done ){

            if (done.getText().toString().equalsIgnoreCase("DONE")) {

                if (selectedSlotView.getText().toString().equalsIgnoreCase("SELECT TIME") == false) {

                    closeCustomDialog();

                    String selectedDeliverySlotDate = String.format("%02d", calendarView.getSelectedDate().getDay()) + "-" + String.format("%02d", (calendarView.getSelectedDate().getMonth() + 1)) + "-" + calendarView.getSelectedDate().getYear();
                    String[] splitSlot = showingSlots.get(timePicker.getValue()).split("\\s+");
                    String selectedDeliverySlotTime = splitSlot[splitSlot.length - 2] + " " + splitSlot[splitSlot.length - 1];

                    String deliverySlot = selectedDeliverySlotDate + ", " + selectedDeliverySlotTime;
                    if (callback != null) {
                        callback.onComplete(deliverySlot);
                    }
                } else {
                    Toast.makeText(context, "Select slot", Toast.LENGTH_LONG).show();
                }
            }
            else if (done.getText().toString().equalsIgnoreCase("SELECT TIME")) {
                switchToSlotSelection();
            }
        }

    }

    private void switchToSlotSelection(){

        if ( isServingDate(selectedDateView.getText().toString()) ){
            done.setText("DONE");
            dateTimeSwitcher.setDisplayedChild(1);
            if (timePicker.getDisplayedValues() != null && timePicker.getDisplayedValues().length > 0) {
                selectedSlotView.setText(timePicker.getDisplayedValues()[0]);
            }else {
                selectedSlotView.setText("SELECT TIME");
            }
        }
    }

    public OrderBookingSlotSelectionDialog set(double maxCartPreTime, int preOrderItemsCount, List<String> couponWorkingDays){
        this.maxCartPreTime = maxCartPreTime;
        this.preOrderItemsCount = preOrderItemsCount;

        // IF COUPON APPLIED THEN CHECKING DAYS
        if ( couponWorkingDays != null && couponWorkingDays.size() > 0 ) {
            for (int i=0;i<localStorageData.getDaysToEnableSlots();i++){

                String date = DateMethods.addToDateInDays(serverTime, i);

                boolean isCouponWorkingDate = false;
                for (int j=0;j<couponWorkingDays.size();j++){
                    if ( DateMethods.compareWeekDays(couponWorkingDays.get(j), DateMethods.getDayOfWeekFromDate(date)) ){
                        isCouponWorkingDate = true;
                        break;
                    }
                }

                // IF COUPON NOT WORKING DATE THEN ADDING NOCOUPON DATES
                if ( isCouponWorkingDate == false ) {
                    nonCouponDates.add(DateMethods.getOnlyDate(date));
                }
            }
        }

        return this;
    }

    //=================

    public void start(OrderBookingSlotSelectionFlowCallback callback){
        this.callback = callback;

        Calendar minDate = DateMethods.getCalendarFromDate(serverTime);
        Calendar maxDate = DateMethods.getCalendarFromDate(DateMethods.addToDateInDays(serverTime, localStorageData.getDaysToEnableSlots() - 1));

        // IF DELIVERY SLOTS ENDS THEN DEFAULT DATE IS TOMORROW
//        if ( DateMethods.compareDates(DateMethods.getTimeIn24HrFormat(serverTime), localStorageData.getDeliverySlotsEndTime()) >= 0 ){
//            minDate.add(Calendar.DATE, 1);
//        }

        // IF PREORDER ITEMS PRESENT THEN ADDING 1 DAY
        if ( preOrderItemsCount > 0 ){
            boolean isAddedOneDayForPreOrderItems = false;
            // IF FIRST SERVING DATE IS SAME AS TODAY THEN ADDING 1 DAY FOR PREORDER ITEMS
            if ( DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getDateFromCalendar(minDate)), DateMethods.getOnlyDate(serverTime)) == 0 ){
                // IF DELIVERY SLOTS STARTED
                //if ( commonMethods.compareDates(commonMethods.getOnlyTimeIn24HrFormat(serverTime), localStorageData.getDeliverySlotsStartTime()) >=  0 ){
                minDate.add(Calendar.DATE, 1);
                isAddedOneDayForPreOrderItems = true;
                //}
            }

            if ( isAddedOneDayForPreOrderItems ) {
                pre_order_items_count.setText(Html.fromHtml("Your cart contains <b>" + preOrderItemsCount + "</b> items that can only be pre-ordered. So your default delivery date is set to next available date"));
                if ( preOrderItemsCount == 1 ){
                    pre_order_items_count.setText(Html.fromHtml("Your cart contains <b>" + preOrderItemsCount + "</b> preorder item. So your default delivery date is set to next available date"));
                }
                pre_order_items_count.setVisibility(View.VISIBLE);
            }
        }

        // STARTING WITH SERVING DATE
        for (int i=0;i<localStorageData.getDaysToEnableSlots();i++){
            if ( isServingDate(DateMethods.getDateFromCalendar(minDate)) && isCouponWokringDate(DateMethods.getDateFromCalendar(minDate)) ){
                //Toast.makeText(context, "First Serving Date : "+DateMethods.getDateFromCalendar(calendar), Toast.LENGTH_LONG).show();
                break;
            }else{
                minDate.add(Calendar.DATE, 1);
            }
        }

        if ( maxCartPreTime > 0 ){
            cart_prep_time.setVisibility(View.VISIBLE);
            cart_prep_time.setText(Html.fromHtml("Your cart contains item(s) with extra prepartion time of <b>"+DateMethods.getTimeStringFromMinutes((int) (maxCartPreTime * 60))+"</b>. Your slots are adjusted accordingly."));
        }

        //------------------

        timePicker.setWrapSelectorWheel(false);

        //calendarView.setShowOtherDates(MaterialCalendarView.SHOW_DECORATED_DISABLED|MaterialCalendarView.SHOW_OUT_OF_RANGE);
        calendarView.setMinimumDate(minDate);
        calendarView.setSelectedDate(minDate);
        calendarView.setMaximumDate(maxDate);

        final ArrayList<CalendarDay> disabledDates = new ArrayList<>();
        for (int i=0;i< nonServingDates.size();i++){
            disabledDates.add(new CalendarDay(DateMethods.getDateFromString(nonServingDates.get(i).getDate())));
        }
        for (int i=0;i< nonCouponDates.size();i++){
            disabledDates.add(new CalendarDay(DateMethods.getDateFromString(nonCouponDates.get(i))));
        }

        calendarView.addDecorator(new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay day) {
                return disabledDates.contains(day);
            }

            @Override
            public void decorate(DayViewFacade view) {
                view.setDaysDisabled(true);
            }
        });//new EventDecorator(dates));

        // IF ATLEAST 1 DAY AVAILABLE
        if ( calendarView.getSelectedDate() != null ) {
            String initialDate = String.format("%02d", calendarView.getSelectedDate().getDay()) + "-" + String.format("%02d", (calendarView.getSelectedDate().getMonth() + 1)) + "-" + calendarView.getSelectedDate().getYear();
            selectedDateView.setText(DateMethods.getDateInFormat(initialDate, DateConstants.MMM_DD_YYYY));
            getSlotsForDate(initialDate);

            showCustomDialog(dialogDeliverySlotView);
        }
        // IF NO DATES AVAILABLE
        else{
            activity.showNotificationDialog("No Delivery Dates", "Sorry no delivery dates available.\nTry changing coupon if applied.");
        }

    }


    //-----------------------

    private void displaySlots(List<SlotDetails> slotsList){

        String deliverySlotsStartTime = "";
        String deliverySlotsEndTime = "";
        if ( slotsList.size() > 0 ){
            deliverySlotsStartTime = slotsList.get(0).getStartTime();
            deliverySlotsEndTime = slotsList.get(slotsList.size()-1).getStartTime();
        }

        showingSlots = new ArrayList<>();

        // IF TODAY SHOWING ONLY AVAILABLE SLOTS
        if ( DateMethods.compareDates(selectedDateView.getText().toString(), DateMethods.getOnlyDate(serverTime)) == 0 ){
            String compareTime = DateMethods.addToDateInMinutes(serverTime, (int) (maxCartPreTime * 60));
            if ( DateMethods.compareDates(deliverySlotsStartTime, DateMethods.getTimeIn24HrFormat(serverTime)) >= 0 ){
                compareTime = DateMethods.addToDateInMinutes(deliverySlotsStartTime, (int) (maxCartPreTime * 60));
            }

            for (int i=0;i<slotsList.size();i++){
                //CHECKING SLOT START TIME WITH (SERVER_TIME+CART_PREP_TIME)
                if ( DateMethods.compareDates(slotsList.get(i).getStartTime(), DateMethods.getTimeIn24HrFormat(compareTime)) >= 0 ){
                    showingSlots.add(slotsList.get(i).getDisplayText());
                }
            }
        }
        // IF NOT TODAY
        else{
            String deliverySlotsStartTimeAfterAddingCartPrepTime = DateMethods.addToDateInMinutes(deliverySlotsStartTime, (int) (maxCartPreTime * 60));

            for (int i=0;i<slotsList.size();i++){
                //CHECKING SLOT START TIME WITH (DELIVERY_SLOTS_START_TIME+CART_PREP_TIME)
                if ( DateMethods.compareDates(slotsList.get(i).getStartTime(), DateMethods.getTimeIn24HrFormat(deliverySlotsStartTimeAfterAddingCartPrepTime)) >= 0 ){
                    showingSlots.add(slotsList.get(i).getDisplayText());
                }
            }
        }

        if ( showingSlots.size() > 0 ){
            timePicker.setDisplayedValues(null);
            timePicker.setMinValue(0);
            timePicker.setMaxValue(showingSlots.size() - 1);
            timePicker.setDisplayedValues(CommonMethods.getStringArrayFromStringList(showingSlots));
            timePicker.setValue(0);

            if ( dateTimeSwitcher.getDisplayedChild() == 1) {
                selectedSlotView.setText(timePicker.getDisplayedValues()[0]);
            }
            switchToContentPage();
        }
        else{
            // IF TODAY SHOWING ONLY AVAILABLE SLOTS
            if ( DateMethods.compareDates(selectedDateView.getText().toString(), DateMethods.getOnlyDate(serverTime)) == 0 ){
                Toast.makeText(context, "No slots available today.", Toast.LENGTH_LONG).show();
                showNoDataIndicator("No slots available today.");
                // STARTING WITH WORKING DATE
                for (int i=1;i<=localStorageData.getDaysToEnableSlots();i++){
                    try {
                        String date = DateMethods.addToDateInDays(selectedDateView.getText().toString(), i);
                        if (isServingDate(date)) {
                            calendarView.setSelectedDate(DateMethods.getCalendarFromDate(date));
                            calendarView.setMinimumDate(DateMethods.getCalendarFromDate(date));
                            calendarView.setSelectedDate(DateMethods.getCalendarFromDate(date));

                            selectedDateView.setText(DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY));
                            getSlotsForDate(date);
                            break;
                        }
                    }catch (Exception e){e.printStackTrace();}
                }
            }else {
                showNoDataIndicator("No slots available.");
            }
        }
    }

    //--------- BACKEND CODE

    private boolean isServingDate(String date){

        if ( date != null ){
            date = DateMethods.getOnlyDate(date);
            if ( nonServingDates != null && nonServingDates.size() > 0 ){
                for (int i=0;i< nonServingDates.size();i++){
                    if ( DateMethods.compareDates(date, nonServingDates.get(i).getDate()) == 0 ){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean isCouponWokringDate(String date){

        if ( date != null ){
            date = DateMethods.getOnlyDate(date);
            if ( nonCouponDates != null && nonCouponDates.size() > 0 ){
                for (int i=0;i< nonCouponDates.size();i++){
                    if ( DateMethods.compareDates(date, nonCouponDates.get(i)) == 0 ){
                        return false;
                    }
                }
            }
        }
        return true;
    }


    private void getSlotsForDate(String date){

        timePicker.setDisplayedValues(null);

        // IF PRESENT LOCALLY
        if ( getSlots(date) != null ){
            showLoadingIndicator("Loading slots for "+date);
            displaySlots(getSlots(date));
        }
        // LOADING FROM SERVER
        else{
            showLoadingIndicator("Loading slots for " + date);
            backendAPIs.getGeneralAPI().getSlotsForDate(date, new RequestSlotsForDate(), new GeneralAPI.SlotsForDateCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String loadedDate, List<SlotDetails> slotsList) {
                    saveDaySlots(loadedDate, slotsList);
                    if (DateMethods.compareDates(selectedDateView.getText().toString(), loadedDate) == 0) {
                        if (status) {
                            displaySlots(slotsList);
                        } else {
                            showReloadIndicator("Error in loading slots, try again!");
                        }
                    }
                }
            });
        }
    }

    //--------- ENHANCEMENT

    List<DaySlots> daySlots = new ArrayList<>();

    private void saveDaySlots(String date, List<SlotDetails> slots){
        if ( daySlots != null ){ daySlots.add(new DaySlots(date, slots));}
    }

    private List<SlotDetails> getSlots(String date){

        if ( daySlots == null ){ daySlots = new ArrayList<>();}

        for (int i=0;i<daySlots.size();i++){
            if ( DateMethods.compareDates(daySlots.get(i).getDate(), date) == 0 ){
                return daySlots.get(i).getSlots();
            }
        }
        return  null;
    }
    class DaySlots{
        String date;
        List<SlotDetails> slots;
        DaySlots(String date, List<SlotDetails> slots){
            this.date = date;
            this.slots = slots;
        }

        public String getDate() {
            return date;
        }

        public List<SlotDetails> getSlots() {
            return slots;
        }
    }

    //---------- UI Methods

    private void showCustomDialog(View dialogView){
        customDialog = new CustomDialog(context);
        customDialog.showDialog(dialogView);
    }
    private void closeCustomDialog(){
        if ( customDialog != null ){
            customDialog.closeDialog();
        }
    }

    private void showLoadingIndicator(String loadiingMessage) {
        try {
            viewSwitcher.setDisplayedChild(0);
            loading_indicator.setVisibility(View.VISIBLE);
            reload_indicator.setVisibility(View.GONE);
            loading_message.setText(loadiingMessage);
            loading_message.setVisibility(View.VISIBLE);
            no_data_message.setVisibility(View.GONE);
        } catch (Exception e) {}
    }

    private void showReloadIndicator(String reloadMessage) {
        try {
            loading_indicator.setVisibility(View.GONE);
            reload_indicator.setVisibility(View.VISIBLE);
            loading_message.setText(reloadMessage);
            loading_message.setVisibility(View.VISIBLE);
            no_data_message.setVisibility(View.GONE);
            reload_indicator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getSlotsForDate(selectedDateView.getText().toString());
                }
            });
            viewSwitcher.setDisplayedChild(0);
        } catch (Exception e) {}
    }

    private void showNoDataIndicator(String noDataMessage) {
        try {
            viewSwitcher.setDisplayedChild(0);
            loading_indicator.setVisibility(View.GONE);
            reload_indicator.setVisibility(View.GONE);
            loading_message.setVisibility(View.GONE);
            no_data_message.setVisibility(View.VISIBLE);
            no_data_message.setText(noDataMessage);
        } catch (Exception e) {}
    }

    private void switchToContentPage() {
        try {
            viewSwitcher.setDisplayedChild(1);
        } catch (Exception e) {}
    }*/
}