package com.mastaan.buyer.dialogs;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by Venkatesh Uppu on 01/03/19.
 */

public class ImageDialog implements View.OnClickListener{

    MastaanToolBarActivity activity;

    View dialogView;
    View container;
    ImageView image;
    View close;


    public ImageDialog(final MastaanToolBarActivity activity){
        try{
            this.activity = activity;

            dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_image, null);
            container = dialogView.findViewById(R.id.container);
            container.setOnClickListener(this);
            image = dialogView.findViewById(R.id.image);
            image.setOnClickListener(this);
            close = dialogView.findViewById(R.id.close);
            close.setOnClickListener(this);
        }catch (Exception e){}
    }

    @Override
    public void onClick(View view) {
        if ( view == container || view == image || view == close ){
            activity.closeCustomDialog();
        }
    }

    public void show(@NonNull final String imageURL) {

        if (imageURL != null && imageURL.trim().length() > 0){
            Picasso.get()
                    .load(imageURL)
                    .placeholder(R.drawable.image_default)
                    .error(R.drawable.image_default)
                    .into(image);

            activity.showCustomDialog(dialogView, true, true);
        }
    }

}