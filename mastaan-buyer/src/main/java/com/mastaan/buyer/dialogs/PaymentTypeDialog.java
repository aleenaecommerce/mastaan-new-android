package com.mastaan.buyer.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.adapters.MessagesAdapter;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.interfaces.MessagesCallback;
import com.mastaan.buyer.models.MessageDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 22/12/15.
 */
public class PaymentTypeDialog implements View.OnClickListener{
    MastaanToolBarActivity activity;
    View dialogView;
    TextView title;
    TextView message;
    View cashOnDelivery;
    View onlinePayment;
    //View paytmWallet;
    //View paytm_qr_hint;
    vRecyclerView messagesHolder;

    boolean isCancellable = true;

    String dialogTitle;
    String dialogMessage;

    MessagesAdapter messagesAdapter;

    Callback callback;

    public interface Callback{
        void onSelect(String paymentType);
    }


    public PaymentTypeDialog setCallback(Callback callback) {
        this.callback = callback;
        return this;
    }

    public PaymentTypeDialog(final MastaanToolBarActivity activity){
        try{
            this.activity = activity;

            dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_payment_type, null);
            title = dialogView.findViewById(R.id.title);
            message = dialogView.findViewById(R.id.message);
            cashOnDelivery = dialogView.findViewById(R.id.cashOnDelivery);
            cashOnDelivery.setOnClickListener(this);
            onlinePayment = dialogView.findViewById(R.id.onlinePayment);
            onlinePayment.setOnClickListener(this);
            //paytmWallet = dialogView.findViewById(R.id.paytmWallet);
            //paytmWallet.setOnClickListener(this);
            //paytm_qr_hint = dialogView.findViewById(R.id.paytm_qr_hint);
            messagesHolder = dialogView.findViewById(R.id.messagesHolder);
            messagesHolder.setupVerticalOrientation();
            messagesHolder.setNestedScrollingEnabled(false);
            messagesAdapter = new MessagesAdapter(activity/*, R.layout.view_message_without_card*/, new ArrayList<MessageDetails>(), new MessagesAdapter.Callback() {
                @Override
                public void onItemClick(int position) {
                    activity.handleMessageClick(messagesAdapter.getItem(position));
                }
            });
            messagesHolder.setAdapter(messagesAdapter);
        }catch (Exception e){}
    }

    public PaymentTypeDialog setCashOnDeliveryVisibility(boolean visibility){
        try{
            if ( visibility ){
                cashOnDelivery.setVisibility(View.VISIBLE);
                //paytm_qr_hint.setVisibility(View.VISIBLE);
            }else{
                cashOnDelivery.setVisibility(View.GONE);
                //paytm_qr_hint.setVisibility(View.GONE);
            }
        }catch (Exception e){}
        return this;
    }

    public PaymentTypeDialog setCancellable(boolean cancellable) {
        isCancellable = cancellable;
        return this;
    }

    public PaymentTypeDialog setTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
        return this;
    }

    public PaymentTypeDialog setMessage(String dialogMessage) {
        this.dialogMessage = dialogMessage;
        return this;
    }

    public void show(){
        try{
            if ( dialogTitle != null && dialogTitle.length() > 0 ){
                title.setText(dialogTitle);
            }

            if ( dialogMessage != null && dialogMessage.length() > 0 ){
                message.setVisibility(View.VISIBLE);
                message.setText(dialogMessage);
            }else{
                message.setVisibility(View.GONE);
            }

            activity.getMastaanApplication().getAlertMessages("payments", new MessagesCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<MessageDetails> messagesList) {
                    if ( status ){
                        messagesAdapter.setItems(messagesList);
                    }
                }
            });

            activity.showCustomDialog(dialogView, isCancellable);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        activity.closeCustomDialog();
        if ( callback != null ){
            if ( view == cashOnDelivery ){
                callback.onSelect(Constants.CASH_ON_DELIVERY);
            }
            else if ( view == onlinePayment ){
                callback.onSelect(Constants.ONLINE_PAYMENT);
            }
            //else if ( view == paytmWallet ){
            //    callback.onSelect(Constants.PAYTM_WALLET);
            //}
        }
    }

}