package com.mastaan.buyer.dialogs;

/**
 * Created by venkatesh on 17/12/15.
 */
public class SlotSelectionDialog {/*implements View.OnClickListener{

    CustomDialog customDialog;

    vToolBarActivity activity;
    Context context;
    LocalStorageData localStorageData;
    BackendAPIs backendAPIs;

    //DIALOG
    ViewSwitcher dateTimeSwitcher;
    View dialogSlotSelectionView;
    ViewSwitcher viewSwitcher;
    ProgressBar loading_indicator;
    FloatingActionButton reload_indicator;
    TextView loading_message;
    TextView no_data_message;
    //
    Button dateView;
    Button slotView;
    RobotoCondensedTextView selectedDateView;
    RobotoCondensedTextView selectedSlotView;
    TextView pre_order_items_count;
    MaterialCalendarView calendarView;
    TextView cart_prep_time;
    NumberPicker timePicker;
    Button done;
    Button cancel;

    SimpleDateFormat simpleDateFormat;

    List<String> showingSlots;

    Callback callback;

    public interface Callback{
        void onComplete(String selectedDate);
    }

    public SlotSelectionDialog(final vToolBarActivity activity){
        this.activity = activity;
        this.context = activity;
        localStorageData = new LocalStorageData(context);
        backendAPIs = new BackendAPIs(context, activity.getDeviceID(), activity.getAppVersionCode());

        simpleDateFormat= new SimpleDateFormat("dd-MM-yyyy");
    }

    public SlotSelectionDialog setupUI(){

        dialogSlotSelectionView =  LayoutInflater.from(context).inflate(R.layout.dialog_date_slot_selection, null);

        dateTimeSwitcher = (ViewSwitcher) dialogSlotSelectionView.findViewById(R.id.dateTimeSwitcher);
        viewSwitcher = (ViewSwitcher) dialogSlotSelectionView.findViewById(R.id.viewSwitcher);
        loading_indicator = (ProgressBar) dialogSlotSelectionView.findViewById(R.id.loading_indicator);
        reload_indicator = (FloatingActionButton) dialogSlotSelectionView.findViewById(R.id.reload_indicator);
        loading_message = (TextView) dialogSlotSelectionView.findViewById(R.id.loading_message);
        no_data_message = (TextView) dialogSlotSelectionView.findViewById(R.id.no_data_message);

        dateView = (Button) dialogSlotSelectionView.findViewById(R.id.dateView);
        dateView.setOnClickListener(this);
        slotView = (Button) dialogSlotSelectionView.findViewById(R.id.slotView);
        slotView.setOnClickListener(this);
        selectedDateView = (RobotoCondensedTextView) dialogSlotSelectionView.findViewById(R.id.selectedDateView);
        selectedSlotView = (RobotoCondensedTextView) dialogSlotSelectionView.findViewById(R.id.selectedSlotView);
        pre_order_items_count = (TextView) dialogSlotSelectionView.findViewById(R.id.pre_order_items_count);
        calendarView = (MaterialCalendarView) dialogSlotSelectionView.findViewById(R.id.calendarView);
        cart_prep_time = (TextView) dialogSlotSelectionView.findViewById(R.id.cart_prep_time);
        timePicker = (NumberPicker) dialogSlotSelectionView.findViewById(R.id.timePicker);

        done = (Button) dialogSlotSelectionView.findViewById(R.id.done);
        done.setOnClickListener(this);
        cancel = (Button) dialogSlotSelectionView.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(MaterialCalendarView widget, CalendarDay selectedDate, boolean selected) {

                String selDate = String.format("%02d", selectedDate.getDay()) + "-" + String.format("%02d", (selectedDate.getMonth() + 1)) + "-" + selectedDate.getYear();

            }
        });

        timePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                selectedSlotView.setText(timePicker.getDisplayedValues()[newVal]);
            }
        });

        return this;
    }

    @Override
    public void onClick(View view) {

        if ( view == dateView ){
            dateTimeSwitcher.setDisplayedChild(0);
            done.setText("SELECT TIME");
            selectedSlotView.setText("SELECT TIME");
        }

        else if ( view == slotView ){
            switchToSlotSelection();
        }

        else if ( view == cancel ){
//            closeCustomDialog();
        }

        else if ( view == done ){

            if (done.getText().toString().equalsIgnoreCase("DONE")) {

                if (selectedSlotView.getText().toString().equalsIgnoreCase("SELECT TIME") == false) {

//                    closeCustomDialog();

                    String selectedDeliverySlotDate = String.format("%02d", calendarView.getSelectedDate().getDay()) + "-" + String.format("%02d", (calendarView.getSelectedDate().getMonth() + 1)) + "-" + calendarView.getSelectedDate().getYear();
                    String[] splitSlot = showingSlots.get(timePicker.getValue()).split("\\s+");
                    String selectedDeliverySlotTime = splitSlot[splitSlot.length - 2] + " " + splitSlot[splitSlot.length - 1];

                    String deliverySlot = selectedDeliverySlotDate + ", " + selectedDeliverySlotTime;
                    if (callback != null) {
                        callback.onComplete(deliverySlot);
                    }
                } else {
                    Toast.makeText(context, "Select slot", Toast.LENGTH_LONG).show();
                }
            }
            else if (done.getText().toString().equalsIgnoreCase("SELECT TIME")) {
                switchToSlotSelection();
            }
        }

    }

    private void switchToSlotSelection(){

        if ( isAvailableOnDate(selectedDateView.getText().toString()) ){
            done.setText("DONE");
            dateTimeSwitcher.setDisplayedChild(1);
            if (timePicker.getDisplayedValues() != null && timePicker.getDisplayedValues().length > 0) {
                selectedSlotView.setText(timePicker.getDisplayedValues()[0]);
            }else {
                selectedSlotView.setText("SELECT TIME");
            }
        }
    }


    //---------------------

    WarehouseMeatItemDetails warehouseMeatItemDetails;
    List<AvailabilityDetails> availabilitiesList = new ArrayList<>();

    public SlotSelectionDialog set(WarehouseMeatItemDetails warehouseMeatItemDetails){
        this.warehouseMeatItemDetails = warehouseMeatItemDetails;
        this.availabilitiesList = warehouseMeatItemDetails.getFullAvlabiltiesList(DateMethods.getOnlyDate(localStorageData.getServerTime()), localStorageData.getDaysToEnableSlots(), localStorageData.getDisabledDates());
        return this;
    }


    public void start(Callback callback){
        start(null, null, callback);
    }

    public void start(String prefillDate, String prefillTime, Callback callback){
        this.callback = callback;

        //calendarView.setShowOtherDates(MaterialCalendarView.SHOW_DECORATED_DISABLED|MaterialCalendarView.SHOW_OUT_OF_RANGE);
        calendarView.setMinimumDate(DateMethods.getCalendarFromDate(availabilitiesList.get(0).getDate()));
        calendarView.setMaximumDate(DateMethods.getCalendarFromDate(availabilitiesList.get(availabilitiesList.size()-1).getDate()));

        final ArrayList<CalendarDay> unavailableDates = new ArrayList<>();
        for (int i=0;i<availabilitiesList.size();i++){
            if ( availabilitiesList.get(i).isAvailable() == false ) {
                unavailableDates.add(new CalendarDay(DateMethods.getDateFromString(availabilitiesList.get(i).getDate())));
            }
        }

        calendarView.addDecorator(new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay day) {
                return unavailableDates.contains(day);
            }

            @Override
            public void decorate(DayViewFacade view) {
                view.setDaysDisabled(true);
            }
        });//new EventDecorator(dates));

        if ( prefillDate != null && prefillDate.length() > 0 ){
            calendarView.setSelectedDate(DateMethods.getCalendarFromDate(prefillDate));
        }


        activity.showCustomDialog(dialogSlotSelectionView);
    }



    //--------------------------------

    private List<String> getUnavailableDates(){
        List<String> unavailableDates = new ArrayList<>();
        for (int i=0;i<availabilitiesList.size();i++){
            if ( availabilitiesList.get(i).isAvailable() == false ) {
                unavailableDates.add(availabilitiesList.get(i).getDate());
            }
        }
        return unavailableDates;
    }

    private boolean isAtleastOneAvailableDay(){
        for (int i=0;i<availabilitiesList.size();i++){
            if ( availabilitiesList.get(i).isAvailable() ){
                return true;
            }
        }
        return false;
    }

    private boolean isAvailableOnDate(String date){
        for (int i=0;i<availabilitiesList.size();i++){
            if ( DateMethods.compareDates(date, availabilitiesList.get(i).getDate()) == 0 ) {
                return availabilitiesList.get(i).isAvailable();
            }
        }
        return false;
    }






    //============================ ENHANCEMENT

    List<DaySlots> daySlots = new ArrayList<>();

    private void saveDaySlots(String date, List<SlotDetails> slots){
        if ( daySlots == null ){ daySlots = new ArrayList<>();}
        if ( date != null && slots != null ){ daySlots.add(new DaySlots(date, slots));}
    }

    private List<SlotDetails> getSlots(String date){
        if ( daySlots == null ){ daySlots = new ArrayList<>();}
        for (int i=0;i<daySlots.size();i++){
            if ( DateMethods.compareDates(daySlots.get(i).getDate(), date) == 0 ){
                if ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(daySlots.get(i).getLoadedTime(), DateMethods.getCurrentDateAndTime()) <= 5*60*1000 ){
                    return daySlots.get(i).getSlots();
                }else{
                    daySlots.remove(i);
                    break;
                }
            }
        }
        return  null;
    }
    class DaySlots{
        String date;
        List<SlotDetails> slots;
        String loadedTime;
        DaySlots(String date, List<SlotDetails> slots){
            this.date = date;
            this.slots = slots;
            this.loadedTime = DateMethods.getCurrentDateAndTime();
        }

        public String getLoadedTime() {
            return loadedTime;
        }

        public String getDate() {
            return date;
        }

        public List<SlotDetails> getSlots() {
            return slots;
        }
    }

    */

}