package com.mastaan.buyer.dialogs;

import android.os.AsyncTask;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.models.MessageDetails;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by Venkatesh Uppu on 19/02/18.
 */

public class LaunchMessageDialog implements View.OnClickListener{

    MastaanToolBarActivity activity;

    View dialogView;
    View detailsHolder;
    View imageHolder;
    TextView title;
    TextView details;
    ImageView image;
    Button action;
    TextView actionImage;
    View close;

    MessageDetails messageDetails;


    public LaunchMessageDialog(final MastaanToolBarActivity activity){
        try{
            this.activity = activity;

            dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_launch_message, null);
            detailsHolder = dialogView.findViewById(R.id.detailsHolder);
            imageHolder = dialogView.findViewById(R.id.imageHolder);
            title = dialogView.findViewById(R.id.title);
            details = dialogView.findViewById(R.id.details);
            image = dialogView.findViewById(R.id.image);
            image.setOnClickListener(this);
            action = dialogView.findViewById(R.id.action);
            action.setOnClickListener(this);
            actionImage = dialogView.findViewById(R.id.actionImage);
            actionImage.setOnClickListener(this);
            close = dialogView.findViewById(R.id.close);
            close.setOnClickListener(this);
        }catch (Exception e){}
    }

    @Override
    public void onClick(View view) {
        if ( view == action || view == actionImage || view == image ){
            activity.closeCustomDialog();
            activity.handleMessageClick(messageDetails);
        }
        else if ( view == close ){
            activity.closeCustomDialog();
        }
    }

    public void show(@NonNull final MessageDetails messageDetails){
        this.messageDetails = messageDetails;

        if ( messageDetails != null ){
            new AsyncTask<Void, Void, Void>() {
                boolean show;

                @Override
                protected Void doInBackground(Void... voids) {
                    try{
                        /*String startDate = messageDetails.getStartDate();
                        String endDate = messageDetails.getEndDate();*/
                        String serverTime = activity.getLocalStorageData().getServerTime();

                        if ( messageDetails.getStatus() && messageDetails.isValidMessage()
                                /*&& (startDate == null || startDate.trim().length() == 0 || DateMethods.compareDates(serverTime, startDate) >= 0 )
                                && (endDate == null || endDate.trim().length() == 0 || DateMethods.compareDates(endDate, serverTime) >= 0 )*/
                                ) {
                            MessageDetails lastMessage = activity.getLocalStorageData().getLastLaunchMessage();

                            // IF LAST & CURRENT MESSAGES ARE SAME
                            if (compareMessages(messageDetails, lastMessage)) {
                                // IF CURRENT MSG SHOW ONCE == FALSE & LAST SHOWN 6 HRS BACK
                                if (messageDetails.showOnce() == false && DateMethods.getDifferenceBetweenDatesInMilliSeconds(serverTime, activity.getLocalStorageData().getLastLaunchMessageShownTime()) >= 4 * 60 * 60 * 1000) {
                                    show = true;
                                }
                            }
                            // IF LAST & CURRENT MESSAGES ARE DIFFERENT
                            else {
                                show = true;
                            }

                            //---------

                            // STORING LOCALLY
                            if (show){
                                activity.getLocalStorageData().setLastLaunchMessage(messageDetails);
                            }
                        }
                    }catch (Exception e){e.printStackTrace();}
                    return null;
                }

                private boolean compareMessages(MessageDetails message1, MessageDetails message2){
                    if ( message1 != null && message2 != null
                            && message1.getStatus() == message2.getStatus()
                            && CommonMethods.compareStrings(message1.getTitle(), message2.getTitle())
                            && CommonMethods.compareStrings(message1.getDetails(), message2.getDetails())
                            && CommonMethods.compareStrings(message1.getImage(), message2.getImage())
                            && CommonMethods.compareStrings(message1.getAction(), message2.getAction())
                            && CommonMethods.compareStrings(message1.getURL(), message2.getURL())
                            && CommonMethods.compareStrings(message1.getAppURL(), message2.getAppURL())
                            && CommonMethods.compareStrings(message1.getStartDate(), message2.getStartDate())
                            && CommonMethods.compareStrings(message1.getEndDate(), message2.getEndDate())
                            ){
                        return true;
                    }
                    return false;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    try{
                        if ( show ){
                            // Image View
                            if ( messageDetails.getImage() != null && messageDetails.getImage().trim().length() > 0 ){
                                imageHolder.setVisibility(View.VISIBLE);
                                detailsHolder.setVisibility(View.GONE);

                                if ( messageDetails.getAction() != null && messageDetails.getAction().trim().length() > 0 ){
                                    actionImage.setText(messageDetails.getAction());
                                }else{  actionImage.setVisibility(View.GONE);    }

                                Picasso.get()
                                        .load(messageDetails.getImage())
                                        .into(image, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                                activity.showCustomDialog(dialogView, true, true);
                                            }

                                            @Override
                                            public void onError(Exception e) {

                                            }


                                        });
                            }
                            // Details View
                            else if ( (messageDetails.getTitle() != null && messageDetails.getTitle().trim().length() > 0)
                                    || (messageDetails.getDetails() != null && messageDetails.getDetails().trim().length() > 0) ){
                                detailsHolder.setVisibility(View.VISIBLE);
                                imageHolder.setVisibility(View.GONE);

                                if ( messageDetails.getTitle() != null && messageDetails.getTitle().trim().length() > 0 ){
                                    title.setVisibility(View.VISIBLE);
                                    title.setText(CommonMethods.fromHtml(messageDetails.getTitle()));
                                }else{  title.setVisibility(View.GONE); }

                                if ( messageDetails.getDetails() != null && messageDetails.getDetails().trim().length() > 0 ){
                                    details.setVisibility(View.VISIBLE);
                                    details.setText(CommonMethods.fromHtml(messageDetails.getDetails()));
                                }else{  details.setVisibility(View.GONE); }

                                if ( messageDetails.getAction() != null && messageDetails.getAction().trim().length() > 0 ){
                                    action.setText(messageDetails.getAction());
                                }

                                activity.showCustomDialog(dialogView, true, true);
                            }
                        }
                    }catch (Exception e){e.printStackTrace();}
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

}