package com.mastaan.buyer.flows;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.aleena.common.interfaces.StatusCallback;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.localdata.LocalStorageData;

/**
 * Created by Venkatesh Uppu on 08/09/2018.
 */

public class RateOnPlaystoreFlow {

    MastaanToolBarActivity activity;

    public RateOnPlaystoreFlow(MastaanToolBarActivity activity){
        this.activity = activity;
    }

    public void check(final StatusCallback callback){
        new AsyncTask<Void, Void, Void>() {
            boolean askFeedback;
            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    LocalStorageData localStorageData = new LocalStorageData(activity);
                    if ( activity.getMastaanApplication().isSubmittedHighRatedFeedback()
                            && localStorageData.getSessionFlag()
                            && localStorageData.isBuyerRatedOnPlaystore() == false ){
                        askFeedback = true;
                    }
                }catch (Exception e){}
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                try{
                    if ( askFeedback ){
                        View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_rate_on_playstore, null);
                        final CheckBox doNotShowAgain = dialogView.findViewById(R.id.doNotShowAgain);
                        final Button later = dialogView.findViewById(R.id.later);
                        final Button rate = dialogView.findViewById(R.id.rate);

                        doNotShowAgain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                try {
                                    rate.setTextColor(ContextCompat.getColor(activity, isChecked ? R.color.grey : R.color.action_button_color));
                                    later.setText(isChecked ? "Skip" : "Later");
                                    rate.setClickable(isChecked ? false : true);
                                }catch (Exception e){}
                            }
                        });
                        later.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try{
                                    if ( callback != null ){
                                        callback.onComplete(false, -1, "Skipped");
                                    }

                                    if ( doNotShowAgain.isChecked() ){
                                        // Storing buyer rated on store in server
                                        activity.getBackendAPIs().getProfileAPI().markRatedOnStore(null);
                                    }
                                    activity.closeCustomDialog();
                                }catch (Exception e){}
                            }
                        });
                        rate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try{
                                    if ( callback != null ){
                                        callback.onComplete(true, 200, "Success");
                                    }

                                    // Storing buyer rated on store in server
                                    activity.getBackendAPIs().getProfileAPI().markRatedOnStore(null);

                                    activity.closeCustomDialog();

                                    Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
                                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                    try {
                                        activity.startActivity(goToMarket);
                                    } catch (ActivityNotFoundException e) {
                                        activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
                                    }
                                }catch (Exception e){}
                            }
                        });
                        activity.showCustomDialog(dialogView, false);
                    }
                    else{
                        if ( callback != null ){
                            callback.onComplete(false, -1, "Alreay rated/skipped");
                        }
                    }
                }catch (Exception e){
                    if ( callback != null ){
                        callback.onComplete(false, 500, "Error");
                    }
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

}
