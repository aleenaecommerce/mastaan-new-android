package com.mastaan.buyer.flows;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mastaan.buyer.activities.OrderErrorActivity;
import com.mastaan.buyer.activities.OrderSuccessActivity;
import com.mastaan.buyer.backend.models.RequestPlaceOrder;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.GroupedOrders;
import com.mastaan.buyer.models.OrderDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public final class GoToPage {

    public static final void goToOrderSuccessPage(final Context context, final List<OrderDetails> ordersList){

        if ( ordersList != null && ordersList.size() > 0 && ordersList.get(0) != null ){
            new AsyncTask<Void, Void, Void>() {
                String orders_list_json = "";
                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        OrderDetails orderDetails = ordersList.get(0);

                        LocalStorageData localStorageData = new LocalStorageData(context);
                        localStorageData.setCartCount(0);
                        localStorageData.setBuyerName(orderDetails.getCustomerName());
                        localStorageData.setBuyerEmail(orderDetails.getCustomerEmail());
                        localStorageData.setBuyerMobile(orderDetails.getCustomerMobile());
                        localStorageData.setBuyerAlternateMobile(orderDetails.getCustomerAlternateMobile());

                        orders_list_json = new Gson().toJson(new GroupedOrders("", ordersList));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

//                    ArrayList<String> imageURLs = new ArrayList<>();
//                    for (int i=0;i<ordersList.size();i++){
//                        OrderDetails orderDetails = ordersList.get(i);
//                        if ( orderDetails != null && orderDetails.getOrderItems() != null ) {
//                            for (int j = 0; j < orderDetails.getOrderItems().size(); j++) {
//                                imageURLs.add(orderDetails.getOrderItems().get(j).getMeatItemDetails().getThumbnailImageURL());
//                            }
//                        }
//                    }

                    Intent orderSuccessAct = new Intent(context, OrderSuccessActivity.class);
//                    orderSuccessAct.putStringArrayListExtra("imageURLs", imageURLs);
                    orderSuccessAct.putExtra(Constants.GROUPED_ORDERS, orders_list_json);
                    context.startActivity(orderSuccessAct);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else{
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    public static final void goToOrderFailurePage(Context context, RequestPlaceOrder requestPlaceOrder){
        Intent orderErrorAct = new Intent(context, OrderErrorActivity.class);
        orderErrorAct.putExtra(Constants.REQUEST_PLACE_ORDER, new Gson().toJson(requestPlaceOrder));
        context.startActivity(orderErrorAct);
    }

}
