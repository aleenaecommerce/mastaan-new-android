package com.mastaan.buyer.flows;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.backend.models.RequestUpdateInstallSource;
import com.mastaan.buyer.models.BuyerDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 11/09/2018.
 */

public class AskInstallSourceFlow {

    MastaanToolBarActivity activity;

    public AskInstallSourceFlow(MastaanToolBarActivity activity){
        this.activity = activity;
    }

    public void check(final StatusCallback callback){
        new AsyncTask<Void, Void, Void>() {
            boolean ask = false;
            List<String> installSources = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    BuyerDetails buyerDetails = activity.localStorageData.getBuyerDetails();
                    if ( activity.localStorageData.getSessionFlag() && (buyerDetails != null && buyerDetails.getID() != null && buyerDetails.getID().length() > 0 )
                            && ( buyerDetails.getInstallSource() == null || buyerDetails.getInstallSource().trim().length() == 0 ) ){
                        ask = true;
                        installSources = activity.localStorageData.getInstallSources();
                    }
                }catch (Exception e){}
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                try{
                    installSources.add("Others");
                    if ( ask ){
                        activity.showListChooserDialog(activity.getString(R.string.app_type).equalsIgnoreCase("internal")?true:false, "How did you hear about Mastaan?", installSources, -1, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                try{
                                    if ( installSources.get(position).equalsIgnoreCase("Others") ){
                                        View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_install_source, null);
                                        final vTextInputLayout comments = (vTextInputLayout) dialogView.findViewById(R.id.comments);
                                        dialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                String eComments = comments.getText().trim();
                                                comments.checkError("* Enter comments");

                                                if ( eComments.length() == 0 ) {
                                                    return;
                                                }
                                                if ( eComments.length() < 4 ){
                                                    comments.showError("* Input must be at least 4 characters");
                                                    return;
                                                }
                                                if ( eComments.matches(".*[a-zA-Z]+.*") == false ){
                                                    comments.showError("* Invalid input");
                                                    return;
                                                }

                                                if ( callback != null ){
                                                    callback.onComplete(true, 200, "Success");
                                                }
                                                activity.closeCustomDialog();
                                                continueWithAskInstallSource("Others", eComments);
                                            }
                                        });
                                        activity.showCustomDialog(dialogView, false);
                                    }
                                    else{
                                        if ( callback != null ) {
                                            callback.onComplete(true, 200, "Success");
                                        }
                                        continueWithAskInstallSource(installSources.get(position), "");
                                    }
                                }catch (Exception e){}
                            }
                        });
                    }else{
                        if ( callback != null ) {
                            callback.onComplete(true, 200, "Success");
                        }
                    }
                }catch (Exception e){
                    if ( callback != null ) {
                        callback.onComplete(false, 500, "Error");
                    }
                }
            }
            private void continueWithAskInstallSource(final String installSource, String installSourceDetails){
                try{
                    activity.getBackendAPIs().getProfileAPI().updateInstallSource(new RequestUpdateInstallSource(installSource, installSourceDetails), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            try{
                                if ( status ){
                                    activity.showToastMessage("Thanks for your input");
                                    activity.getGoogleAnalyticsMethods().sendEvent("Install Source", installSource, activity.localStorageData.getBuyerMobileOrEmail());
                                }
                            }catch (Exception e){}
                        }
                    });
                }catch (Exception e){}
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

}
