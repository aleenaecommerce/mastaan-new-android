package com.mastaan.buyer.localdata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.google.gson.Gson;
import com.mastaan.buyer.backend.models.RequestSubmitOrderFeedback;
import com.mastaan.buyer.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 10/7/15.
 */

public class OrdersFeedbackDatabase extends SQLiteOpenHelper {

    Context context;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;

    private static final String DB_NAME = "OrdersFeedbackDatabase";
    private static final int DB_VERSION = 1;

    private static final String FEEDBACK_TABLE = "feedbackTable";

    private static final String BUYER_MOBILE = "buyer_mobile";
    private static final String TYPE = "type";
    private static final String RETRY_COUNT = "retry_count";
    private static final String ORDER_ID = "order_id";
    private static final String ORDER_FEEDBACK_JSON = "order_feedback_json";

    public interface GetOrderFeedbackItemCallback{
        void onComplete(LocalDBOrderFeedback localDBOrderFeedback);
    }

    public interface GetAllOrderFeedbacksCallback{
        void onComplete(List<LocalDBOrderFeedback> feedbackItems);
    }

    private final String CREATE_TABLE="CREATE TABLE IF NOT EXISTS "+ FEEDBACK_TABLE +" ( "+
            TYPE+" TEXT ,"+
            ORDER_ID+" TEXT ,"+
            BUYER_MOBILE+" TEXT ,"+
            RETRY_COUNT+" INTEGER ,"+
            ORDER_FEEDBACK_JSON+" TEXT ,"+
            "PRIMARY KEY ("+ORDER_ID+")"+
            ")";


    public OrdersFeedbackDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ FEEDBACK_TABLE);
        onCreate(sqLiteDatabase);
    }

    public void openDatabase(){
        sqLiteDatabase = this.getWritableDatabase();
    }

    public void closeDatabase(){
        sqLiteDatabase.close();
    }

    public void addFeedbackItem(final String type, final String orderID, final String buyerMobile, final RequestSubmitOrderFeedback requestSubmitOrderFeedback, final StatusCallback callback){

        new AsyncTask<Void, Void, Void>() {
            boolean status;

            @Override
            protected Void doInBackground(Void... voids) {

                ContentValues contentValues = new ContentValues();
                contentValues.put(TYPE, type);
                contentValues.put(ORDER_ID, orderID);
                contentValues.put(BUYER_MOBILE, buyerMobile);
                contentValues.put(RETRY_COUNT, 0);
                contentValues.put(ORDER_FEEDBACK_JSON, new Gson().toJson(requestSubmitOrderFeedback));

                try {
                    sqLiteDatabase.insertOrThrow(FEEDBACK_TABLE, null, contentValues);
                    status = true;
                    Log.d(Constants.LOG_TAG, "FeedbackDatabase : addFeedbackItem , Success for orderID = "+orderID);
                }catch (Exception e){
                    Log.d(Constants.LOG_TAG, "OrderFeedbackDatabase : AddFeedbackItem , Error for orderID = "+orderID);
                    status = false;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, 200, "");
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void increaseOrderFeedbackRetryCount(final LocalDBOrderFeedback localDBOrderFeedback, final StatusCallback callback){

        new AsyncTask<Void, Void, Void>() {
            boolean status;

            @Override
            protected Void doInBackground(Void... voids) {

                ContentValues contentValues = new ContentValues();
                contentValues.put(TYPE, localDBOrderFeedback.getType());
                contentValues.put(ORDER_ID, localDBOrderFeedback.getOrderID());
                contentValues.put(BUYER_MOBILE, localDBOrderFeedback.getBuyerMobile());
                contentValues.put(RETRY_COUNT, localDBOrderFeedback.getRetryCount()+1);
                contentValues.put(ORDER_FEEDBACK_JSON, localDBOrderFeedback.getOrderFeedbackJSON());

                try {
                    sqLiteDatabase.update(FEEDBACK_TABLE, contentValues, ORDER_ID + " = " + "\"" + localDBOrderFeedback.getOrderID() + "\"", null);
                    status = true;
                    Log.d(Constants.LOG_TAG, "FeedbackDatabase : UpdateItem , Success for orderID = "+localDBOrderFeedback.getOrderID());
                }catch (Exception e){
                    Log.d(Constants.LOG_TAG, "CartDatabase : UpdateItem , Error for orderID = "+localDBOrderFeedback.getOrderID());
                    status = false;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, 200, "");
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void deleteFeedbackItem(final String orderID, final StatusCallback callback){

        new AsyncTask<Void, Void, Void>() {

            boolean status;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    sqLiteDatabase.execSQL("delete from " + FEEDBACK_TABLE + " where " + ORDER_ID + " = " + "\"" + orderID + "\"");
                    status = true;
                    Log.d(Constants.LOG_TAG, "FeedbackDatabase : DeleteItem , Success for orderID = "+orderID);
                }catch (Exception e){
                    Log.d(Constants.LOG_TAG, "FeedbackDatabase : DeleteItem , Error for orderID = "+orderID);
                    status = false;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, 200, "");
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void getFeedbackItem(final String orderID, final GetOrderFeedbackItemCallback callback){

        new AsyncTask<Void, Void, Void>() {

            LocalDBOrderFeedback localDBOrderFeedback = null;

            @Override
            protected Void doInBackground(Void... voids) {
                cursor = sqLiteDatabase.rawQuery("select " + TYPE + " , " + ORDER_ID + " , " + BUYER_MOBILE + " , " + RETRY_COUNT + " , " + ORDER_FEEDBACK_JSON +  " from " + FEEDBACK_TABLE + " where " + ORDER_ID + " = " + "\"" + orderID + "\"", null);

                if(cursor!=null){
                    if( cursor.moveToFirst() ) {
                        localDBOrderFeedback = new LocalDBOrderFeedback(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4));
                    }
                    cursor.close();
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ) {
                    callback.onComplete(localDBOrderFeedback);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public int getFeedbackItemsCount(){
        return ((int) DatabaseUtils.longForQuery(sqLiteDatabase, "SELECT COUNT(*) FROM "+ FEEDBACK_TABLE, null));
    }

    public void getAllFeedbackItems(final GetAllOrderFeedbacksCallback callback){

        new AsyncTask<Void, Void, Void>() {

            List<LocalDBOrderFeedback> feedbackItems = new ArrayList<>();

            @Override
            protected Void doInBackground(Void... voids) {

                cursor = sqLiteDatabase.query(FEEDBACK_TABLE, new String[]{TYPE, ORDER_ID, BUYER_MOBILE, RETRY_COUNT, ORDER_FEEDBACK_JSON}, null, null, null, null, null);

                if ( cursor != null ){
                    if( cursor.moveToFirst() ){
                        do{
                            LocalDBOrderFeedback localDBOrderFeedback = new LocalDBOrderFeedback(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4));
                            feedbackItems.add(localDBOrderFeedback);
                        }while( cursor.moveToNext() );
                    }
                    cursor.close();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(feedbackItems);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void deleteAllFeedbackItems(){
        sqLiteDatabase.execSQL("delete from "+ FEEDBACK_TABLE);
        Log.d(Constants.LOG_TAG, "FeedbackDatabase : DeleteALL , Success");
    }
}