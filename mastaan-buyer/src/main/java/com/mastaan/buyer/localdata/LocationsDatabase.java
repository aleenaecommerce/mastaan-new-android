package com.mastaan.buyer.localdata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 10/7/15.
 */

public class LocationsDatabase extends SQLiteOpenHelper {

    Context context;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;

    private static final String DB_NAME = "LocationsDatabase";
    private static final int DB_VERSION = 1;

    private static final String LOCATION_TABLE = "locationTable";

    private static final String LATLNG = "latlng";
    private static final String PREMISE = "premise";
    private static final String SUBLOCALITY = "sublocality";
    private static final String LANDMARK = "landmark";
    private static final String LOCALITY = "locality";
    private static final String STATE = "state";
    private static final String COUNTRY = "country";
    private static final String PINCODE = "pin";

    public interface StoredPlaceDetailsCallback{
        void onComplete(boolean isPresent, AddressBookItemDetails placeDetails);
    }

    public interface GetAllStoredPlacesCallback{
        void onComplete(List<AddressBookItemDetails> places);
    }

    private final String CREATE_TABLE="CREATE TABLE IF NOT EXISTS "+ LOCATION_TABLE+" ( "+
            LATLNG+" TEXT ,"+
            PREMISE+" TEXT ,"+
            SUBLOCALITY+" TEXT ,"+
            LANDMARK+" TEXT ,"+
            LOCALITY+" TEXT ,"+
            STATE+" TEXT ,"+
            COUNTRY+" TEXT ,"+
            PINCODE+" TEXT ,"+
            "PRIMARY KEY ("+LATLNG+")"+
            ")";

    public LocationsDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+LOCATION_TABLE);
        onCreate(sqLiteDatabase);
    }

    public void openDatabase(){
        sqLiteDatabase = this.getWritableDatabase();
    }

    public void closeDatabase(){
        sqLiteDatabase.close();
    }

    public void addLocations (List<AddressBookItemDetails> placesList){
        if ( placesList != null && placesList.size() > 0 ) {
            for (int i=0;i<placesList.size();i++) {
                AddressBookItemDetails placeDetails = placesList.get(i);
                if ( placeDetails != null ) {
                    addLocation(placeDetails.getLatLng(), placeDetails.getPremise(), placeDetails.getSublocalityLevel_2(), placeDetails.getLandmark(), placeDetails.getLocality(), placeDetails.getState(), placeDetails.getCountry(), placeDetails.getPostalCode());
                }
            }
        }
    }

    public void addLocation (PlaceDetails placeDetails){
        addLocation(placeDetails.getLatLng(), placeDetails.getPremise(), placeDetails.getSublocalityLevel_2(), placeDetails.getLandmark(), placeDetails.getLocality(), placeDetails.getState(), placeDetails.getCountry(), placeDetails.getPostal_code());
    }

    public void addLocation (AddressBookItemDetails placeDetails){
        addLocation(placeDetails.getLatLng(), placeDetails.getPremise(), placeDetails.getSublocalityLevel_2(), placeDetails.getLandmark(), placeDetails.getLocality(), placeDetails.getState(), placeDetails.getCountry(), placeDetails.getPostalCode());
    }

    public void addLocation (final LatLng latLng, final String premise, final String sublocality, final String landmark, final String locality, final String state, final String country, final String pincode){

        getPlaceDetailsIfPresent(latLng, new LocationsDatabase.StoredPlaceDetailsCallback() {
            @Override
            public void onComplete(boolean isPresent, AddressBookItemDetails placeDetails) {

                if (isPresent == false) {
                    Log.d("GociboLogs", "LocationsDatabase : AddLocation (" + locality.toString() + ") not exists, Adding It");

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {

                            ContentValues contentValues = new ContentValues();
                            contentValues.put(LATLNG, latLng.latitude + "," + latLng.longitude);
                            contentValues.put(PREMISE, premise);
                            contentValues.put(SUBLOCALITY, sublocality);
                            contentValues.put(LANDMARK, landmark);
                            contentValues.put(LOCALITY, locality);
                            contentValues.put(STATE, state);
                            contentValues.put(COUNTRY, country);
                            contentValues.put(PINCODE, pincode);

                            try {
                                sqLiteDatabase.insertOrThrow(LOCATION_TABLE, null, contentValues);
                                Log.d("GociboLogs", "LocationsDatabase : AddLocation , Success for (LatLng = " + latLng + ")");
                            } catch (Exception e) {
                                Log.d("GociboLogs", "LocationsDatabase : AddLocation , Error ");
                            }
                            return null;
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    Log.d("GociboLogs", "LocationsDatabase : AddLocation (" + locality.toString() + ") Already exists");
                }
            }
        });

    }

    public void getPlaceDetailsIfPresent(final LatLng latLng, final StoredPlaceDetailsCallback callback){

        final Location targetLocation = new Location("targetLocation");
        targetLocation.setLatitude(latLng.latitude);
        targetLocation.setLongitude(latLng.longitude);

        getAllPlaces(new GetAllStoredPlacesCallback() {
            @Override
            public void onComplete(List<AddressBookItemDetails> places) {

                if (places != null && places.size() > 0) {

                    boolean isPresent = false;
                    AddressBookItemDetails placeDetails = null;

                    for (int i = 0; i < places.size(); i++) {
                        Location currentLocation = new Location("currentLocation");
                        currentLocation.setLatitude(places.get(i).getLatLng().latitude);
                        currentLocation.setLongitude(places.get(i).getLatLng().longitude);

                        if (targetLocation.distanceTo(currentLocation) < 50) {
                            isPresent = true;
                            placeDetails = places.get(i);
                            Log.d("GociboLogs", "LocationsDatabase : GetStoredLocationDetails (" + latLng.toString() + " , Premise = " + placeDetails.getPremise() + ") Exists");
                            break;
                        }
                    }
                    callback.onComplete(isPresent, placeDetails);

                } else {
                    callback.onComplete(false, null);
                }
            }
        });
    }

    public void getAllPlaces(final GetAllStoredPlacesCallback callback){

        new AsyncTask<Void, Void, Void>() {

            List<AddressBookItemDetails> places = new ArrayList<>();

            @Override
            protected Void doInBackground(Void... voids) {

                cursor = sqLiteDatabase.query(LOCATION_TABLE, new String[]{LATLNG, PREMISE, SUBLOCALITY, LANDMARK, LOCALITY, STATE, COUNTRY, PINCODE}, null, null, null, null, null);

                if ( cursor != null ){
                    if( cursor.moveToFirst() ){
                        do{
                            AddressBookItemDetails addressBookItemDetails = new AddressBookItemDetails(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7));
                            places.add(addressBookItemDetails);
                        }while( cursor.moveToNext() );
                    }
                    cursor.close();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d("GociboLogs", "LocationsDatabase : GetStoredLocations , Found = "+places.size());
                callback.onComplete(places);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public int getLocationsCount(){
        return ((int) DatabaseUtils.longForQuery(sqLiteDatabase, "SELECT COUNT(*) FROM "+LOCATION_TABLE, null));
    }

    public void deleteAllLocations(){
        sqLiteDatabase.execSQL("delete from "+ LOCATION_TABLE);
    }

}