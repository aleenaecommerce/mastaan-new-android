package com.mastaan.buyer.localdata;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.google.gson.Gson;
import com.mastaan.buyer.models.BuyerDetails;
import com.mastaan.buyer.models.CategoryDetails;
import com.mastaan.buyer.models.DayDetails;
import com.mastaan.buyer.models.MessageDetails;
import com.mastaan.buyer.models.OrderDetails;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class LocalStorageData {

    Context context;
    CommonMethods commonMethods;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public LocalStorageData(Context context) {
        this.context = context;
        commonMethods = new CommonMethods();
        sharedPreferences =PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }


    //-------------- APP DATA ---------------//

    public void setTCFlag(boolean status){
        editor.putBoolean("seenTC", status);
        editor.commit();
    }
    public boolean getTCFlag(){
        return sharedPreferences.getBoolean("seenTC", false);
    }

    public void setNotificationsFlag(boolean notificationsFlag) {
        editor.putBoolean("notificationsFlag", notificationsFlag);
        editor.commit();
    }
    public boolean getNotificationsFlag(){
        return sharedPreferences.getBoolean("notificationsFlag", true);
    }

    //--------

    public void setDeviceID(String deviceID){
        editor.putString("device_id", deviceID);
        editor.commit();
    }

    public String getDeviceID(){
        return sharedPreferences.getString("device_id", "");
    }

    public void setAppCurrentVersion(){
        int appVersion=0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appVersion = pInfo.versionCode;
        }catch (Exception e){}

        editor.putInt("appVersion", appVersion);
        editor.commit();
    }

    public int getAppVersion(){
        return sharedPreferences.getInt("appVersion", 0);
    }

    public void setSkipAppUpdateVersion(int versionNo){
        editor.putInt("skip_app_update_version", 71);
        editor.commit();
    }

    public int getSkipAppUpdateVersion(){
        return sharedPreferences.getInt("skip_app_update_version", -1);
    }

    //-----------------------------------------------------------

    public void setPushNotificationsRegistratinStatus(boolean status){
        editor.putBoolean("gcm_registration_id_sent_to_server_status", status);
        editor.commit();
    }

    public boolean getPushNotificationsRegistratinStatus(){
        return sharedPreferences.getBoolean("gcm_registration_id_sent_to_server_status", false);
    }




    //-------------- MASTAAN  DATA ---------------//

    public void setServingAt(String servingAt){
        editor.putString("serving_at", servingAt);
        editor.commit();
    }

    public String getServingAt(){
        return sharedPreferences.getString("serving_at", "");
    }

    public void storeNoDeliveryDates(List<DayDetails> noDeliveryDates) {
        if ( noDeliveryDates != null && noDeliveryDates.size() > 0 ){
            List<String> nodeliveries_jsons_list = new ArrayList<String>();
            for (int i = 0; i < noDeliveryDates.size(); i++) {
                nodeliveries_jsons_list.add(new Gson().toJson(noDeliveryDates.get(i)));
            }
            editor.putString("no_delivery_dates", commonMethods.getJSONArryString(nodeliveries_jsons_list));
        }else{
            editor.putString("no_delivery_dates", "[]");
        }
        editor.commit();
    }

    public List<DayDetails> getDisabledDates() {
        List<DayDetails> noDeliveryDates = new ArrayList<>();
        try {
            JSONArray jsons_list = new JSONArray(sharedPreferences.getString("no_delivery_dates", "[]"));
            for (int i = 0; i < jsons_list.length(); i++) {
                noDeliveryDates.add(new Gson().fromJson(jsons_list.get(i).toString(), DayDetails.class));
            }
        } catch (Exception e) {}

        return noDeliveryDates;
    }

    public void setGovtTaxes(float govtTaxes) {
        editor.putFloat("govt_taxes", govtTaxes);
        editor.commit();
    }

    public float getGovtTaxes() {
        return  sharedPreferences.getFloat("govt_taxes", 0);
    }

    public void setProcessingFees(float processingFee) {
        editor.putFloat("processing_fee", processingFee);
        editor.commit();
    }

    public float getProcessingFees() {
        return  sharedPreferences.getFloat("processing_fee", 0);
    }

    public void setSupportContactNumber(String supportContactNumber){
        editor.putString("support_contact_number", supportContactNumber);
        editor.commit();
    }

    public String getSupportContactNumber(){
        return sharedPreferences.getString("support_contact_number", "");
    }

    //---------- TOKENS DATA -------------//

    public void setFireBaseToken(String fireBaseToken){
        editor.putString("firebase_token", fireBaseToken);
        editor.putString("firebase_token_generated_time", DateMethods.getCurrentDateAndTime());
        editor.commit();
    }

    public String getFireBaseToken(){
        long firebaseTokenElapsedTime = DateMethods.getDifferenceToCurrentTime(sharedPreferences.getString("firebase_token_generated_time", ""));
        if ( firebaseTokenElapsedTime == -1 || firebaseTokenElapsedTime >= 86400000 ){
            return "";
        }else{
            return sharedPreferences.getString("firebase_token", "");
        }
    }

    //---------- USER DATA ------------//

    public boolean getSessionFlag(){
        return sharedPreferences.getBoolean("sessionFlag", false);
    }

    public void storeBuyerSession(String accessToken, BuyerDetails buyerDetails){

        editor.putBoolean("sessionFlag", true);

        editor.putString("access_token", accessToken);

        editor.putString("buyer_id", buyerDetails.getID());

        editor.putString("buyer_mobile", buyerDetails.getMobile());
        editor.putString("buyer_email", buyerDetails.getEmail());
        editor.putString("buyer_name", buyerDetails.getName());

        editor.putString("buyer_referral_id", buyerDetails.getReferralID());

        setBuyerDetails(buyerDetails);
        editor.commit();
    }

    public void setBuyerDetails(final BuyerDetails buyerDetails, final StatusCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                setBuyerDetails(buyerDetails);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    public void setBuyerDetails(BuyerDetails buyerDetails) {

        if ( buyerDetails != null ) {

            editor.putBoolean("sessionFlag", true);

            editor.putString("buyer_details", new Gson().toJson(buyerDetails));

            if ( buyerDetails.getMobile() != null && buyerDetails.getMobile().length() > 0 ){
                editor.putString("buyer_mobile", buyerDetails.getMobile());
            }
            if ( buyerDetails.getEmail() != null && buyerDetails.getEmail().length() > 0 ){
                editor.putString("buyer_email", buyerDetails.getEmail());
            }
            if ( buyerDetails.getName() != null && buyerDetails.getName().length() > 0 ){
                editor.putString("buyer_name", buyerDetails.getName());
            }
            editor.putString("buyer_membership", buyerDetails.getMembershipType());
            editor.commit();
        }else{
            clearBuyerSession();
        }
    }

    public BuyerDetails getBuyerDetails() {
        try {
            String buyer_details_json = sharedPreferences.getString("buyer_details", "");
            BuyerDetails buyerDetails = new Gson().fromJson(buyer_details_json, BuyerDetails.class);
            return buyerDetails;
        }catch (Exception e){}
        return null;
    }

    public void clearBuyerSession(){
        clearBuyerSession(true);
    }

    public void clearBuyerSession(boolean clearAccessToken){
        editor.putBoolean("sessionFlag", false);
        editor.putBoolean("notificationsFlag", true);
        if ( clearAccessToken ) {
            editor.putString("access_token", "");
        }
        editor.putString("buyer_mobile", "");
        editor.putString("buyer_email", "");
        editor.putString("buyer_name", "");
        editor.putString("buyer_membership", "");
        editor.putString("buyer_id", "");
        editor.putString("buyer_details", "");
        editor.putString("buyer_referral_id", "");
        editor.putInt("buyer_pending_orders_count", 0);
        editor.putString("buyer_pending_orders", "");
        editor.putBoolean("is_buyer_has_favourites", false);
        editor.putString("buyer_favourites_list", "[]");
        editor.putInt("cart_count", 0);
        editor.putString("deliver_location", "");
        editor.putString("firebase_token", "");
        editor.putString("firebase_token_generated_time", "");
        editor.putBoolean("gcm_registration_id_sent_to_server_status", false);
        editor.commit();
    }

    public void setAccessToken(String accessToken) {
        editor.putString("access_token", accessToken);
        editor.commit();
    }
    public String getAccessToken(){
        return sharedPreferences.getString("access_token", "");
    }

    public String getBuyerMobile(){
        return sharedPreferences.getString("buyer_mobile", "");
    }
    public void setBuyerMobile(String mobile){
        editor.putString("buyer_mobile", mobile);
        editor.commit();
    }

    public String getBuyerMobileOrEmail(){
        String mobile = getBuyerMobile();
        if ( mobile != null && mobile.trim().length() > 0 ){
            return mobile;
        }else{
            return getBuyerEmail();
        }
    }

    public String getBuyerAlternateMobile(){
        return sharedPreferences.getString("buyer_alternate_mobile", "");
    }

    public void setBuyerAlternateMobile(String alternateMobile){
        editor.putString("buyer_alternate_mobile", alternateMobile);
        editor.commit();
    }

    public String getBuyerID(){
        return sharedPreferences.getString("buyer_id", "");
    }

    public String getBuyerReferralID(){
        return sharedPreferences.getString("buyer_referral_id", "");
    }

    public String getBuyerName(){
        return sharedPreferences.getString("buyer_name", "");
    }

    public String getBuyerMembershipType(){
        return sharedPreferences.getString("buyer_membership", "");
    }

    public void setBuyerName(String name){
        editor.putString("buyer_name", name);
        editor.commit();
    }

    public void setBuyerEmail(String email){
        editor.putString("buyer_email", email);
        editor.commit();
    }

    public String getBuyerEmail(){
        return sharedPreferences.getString("buyer_email", "");
    }


    public void setBuyerRatedOnPlaystore(boolean ratedOnPlaystore){
        editor.putBoolean("buyer_rated_on_playstore", ratedOnPlaystore);
        editor.commit();
    }
    public boolean isBuyerRatedOnPlaystore(){
        return sharedPreferences.getBoolean("buyer_rated_on_playstore", false);
    }

    //------

    public void setHasLocationHistory(boolean hasLocationHistory){
        editor.putBoolean("has_location_history", hasLocationHistory);
    }
    public boolean isHasLocationHistory(){
        return sharedPreferences.getBoolean("has_location_history", false);
    }

    //-------------- SERVER DATA -----------------//


    public void setServerTime(String serverTime){
        editor.putString("server_time", serverTime);
        editor.commit();
    }

    public String getServerTime(){
        return sharedPreferences.getString("server_time", "");
    }

    public void setDaysToEnableSlots(int daysToEnableSlots){
        editor.putInt("daysToEnableSlots", daysToEnableSlots);
        editor.commit();
    }

    public int getDaysToEnableSlots(){
        return sharedPreferences.getInt("daysToEnableSlots", 0);
    }

    public void setDeliveringHoursStartTime(String deliveringHoursStartTime){
        editor.putString("delivering_hours_start_time", deliveringHoursStartTime);
        editor.commit();
    }
    public String getDeliveringHoursStartTime(){
        return sharedPreferences.getString("delivering_hours_start_time", "");
    }


    public void setDeliveringHoursEndTime(String deliveringHoursEndTime){
        editor.putString("delivering_hours_end_time", deliveringHoursEndTime);
        editor.commit();
    }
    public String getDeliveringHoursEndTime(){
        return sharedPreferences.getString("delivering_hours_end_time", "");
    }

    //--------------- CURRENT DATA ---------------//

    public void setDeliveryLocation(AddressBookItemDetails addressBookItemDetails){
        if ( addressBookItemDetails != null ) {
            editor.putString("deliver_location", new Gson().toJson(addressBookItemDetails));
        }else{
            editor.putString("deliver_location", "");
        }
        editor.commit();
    }
    public AddressBookItemDetails getDeliveryLocation(){
        String deliver_location_json = sharedPreferences.getString("deliver_location", "");
        AddressBookItemDetails deliverLocationDetails = new Gson().fromJson(deliver_location_json, AddressBookItemDetails.class);
        return deliverLocationDetails;
    }
    public boolean isDeliveryLocationExists(){
        String deliver_location = sharedPreferences.getString("deliver_location", "");
        if ( deliver_location != null && deliver_location.trim().length() > 0 ){
            return true;
        }
        return false;
    }

    public void setCartCount(int cartCount){
        editor.putInt("cart_count", cartCount);
        editor.commit();
        if ( cartCount == 0 ){
            setMeatItemPrefillDate("");
        }
    }

    public int getCartCount(){
        return  sharedPreferences.getInt("cart_count", 0);
    }

    public void storeCategories(List<CategoryDetails> categoriesList) {
        if ( categoriesList != null ){
            List<String> categories_jsons_list = new ArrayList<String>();
            for (int i = 0; i < categoriesList.size(); i++) {
                categories_jsons_list.add(new Gson().toJson(categoriesList.get(i)));
            }
            editor.putString("categories_list", commonMethods.getJSONArryString(categories_jsons_list));
        }else{
            editor.putString("categories_list", "[]");
        }
        editor.commit();
    }

    public List<CategoryDetails> getCategories() {
        List<CategoryDetails> categoriesList = new ArrayList<>();
        try {
            JSONArray categories_jsons_list = new JSONArray(sharedPreferences.getString("categories_list", "[]"));
            for (int i = 0; i < categories_jsons_list.length(); i++) {
                categoriesList.add(new Gson().fromJson(categories_jsons_list.get(i).toString(), CategoryDetails.class));
            }
        } catch (Exception e) {}

        return categoriesList;
    }

    public void storeMeatItemWeights(List<Double> meatItemWeights) {
        if ( meatItemWeights != null && meatItemWeights.size() > 0 ){
            List<String> meat_item_weights_jsons_list = new ArrayList<String>();
            for (int i = 0; i < meatItemWeights.size(); i++) {
                meat_item_weights_jsons_list.add(new Gson().toJson(meatItemWeights.get(i)));
            }
            editor.putString("meat_item_weights", commonMethods.getJSONArryString(meat_item_weights_jsons_list));
        }else {
            editor.putString("meat_item_weights", "[]");
        }
        editor.commit();
    }

    public List<Double> getMeatItemWeights() {
        List<Double> meatItemWeights = new ArrayList<>();
        try {
            JSONArray meat_item_weights_jsons_list = new JSONArray(sharedPreferences.getString("meat_item_weights", "[]"));
            for (int i = 0; i < meat_item_weights_jsons_list.length(); i++) {
                meatItemWeights.add(new Gson().fromJson(meat_item_weights_jsons_list.get(i).toString(), Double.class));
            }
        } catch (Exception e) {}

        return meatItemWeights;
    }

    public void storeMeatItemSets(List<Double> meatItemSets) {
        if ( meatItemSets != null && meatItemSets.size() > 0 ){
            List<String> meat_item_sets_jsons_list = new ArrayList<String>();
            for (int i = 0; i < meatItemSets.size(); i++) {
                meat_item_sets_jsons_list.add(new Gson().toJson(meatItemSets.get(i)));
            }
            editor.putString("meat_item_sets", commonMethods.getJSONArryString(meat_item_sets_jsons_list));
        }else{
            editor.putString("meat_item_sets", "[]");
        }
        editor.commit();
    }

    public List<Double> getMeatItemSets() {
        List<Double> meatItemSets = new ArrayList<>();
        try {
            JSONArray meat_item_sets_jsons_list = new JSONArray(sharedPreferences.getString("meat_item_sets", "[]"));
            for (int i = 0; i < meat_item_sets_jsons_list.length(); i++) {
                meatItemSets.add(new Gson().fromJson(meat_item_sets_jsons_list.get(i).toString(), Double.class));
            }
        } catch (Exception e) {}

        return meatItemSets;
    }

    public void storeMeatItemPieces(List<Double> meatItemPieces) {
        if ( meatItemPieces != null && meatItemPieces.size() > 0  ){
            List<String> meat_item_pieces_jsons_list = new ArrayList<String>();
            for (int i = 0; i < meatItemPieces.size(); i++) {
                meat_item_pieces_jsons_list.add(new Gson().toJson(meatItemPieces.get(i)));
            }
            editor.putString("meat_item_pieces", commonMethods.getJSONArryString(meat_item_pieces_jsons_list));
        }else{
            editor.putString("meat_item_pieces", "[]");
        }
        editor.commit();
    }

    public List<Double> getMeatItemPieces() {
        List<Double> meatItemPieces = new ArrayList<>();
        try {
            JSONArray meat_item_pieces_jsons_list = new JSONArray(sharedPreferences.getString("meat_item_pieces", "[]"));
            for (int i = 0; i < meat_item_pieces_jsons_list.length(); i++) {
                meatItemPieces.add(new Gson().fromJson(meat_item_pieces_jsons_list.get(i).toString(), Double.class));
            }
        } catch (Exception e) {}

        return meatItemPieces;
    }

    public int getNoOfPendingOrders(){
        return  sharedPreferences.getInt("buyer_pending_orders_count", 0);
    }

    public void setBuyerPendingOrders(List<OrderDetails> pendingOrders) {
        if ( pendingOrders != null && pendingOrders.size() > 0 ){
            List<String> pending_orders_jsons = new ArrayList<String>();
            for (int i = 0; i < pendingOrders.size(); i++) {
                pending_orders_jsons.add(new Gson().toJson(pendingOrders.get(i)));
            }
            editor.putInt("buyer_pending_orders_count", pendingOrders.size());
            editor.putString("buyer_pending_orders", commonMethods.getJSONArryString(pending_orders_jsons));

        }else{
            editor.putInt("buyer_pending_orders_count", 0);
            editor.putString("buyer_pending_orders", "[]");
        }
        editor.commit();
    }

    public List<OrderDetails> getBuyerPendingOrders() {
        List<OrderDetails> pendingOrders = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(sharedPreferences.getString("buyer_pending_orders", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                pendingOrders.add(new Gson().fromJson(jsonArray.get(i).toString(), OrderDetails.class));
            }
        } catch (Exception e) {}
        if ( pendingOrders == null ){   pendingOrders = new ArrayList<>();  }

        return pendingOrders;
    }

    public void storeInstallSources(List<String> installMediums){
        List<String> failure_reasons_list = new ArrayList<String>();
        for (int i = 0; i < installMediums.size(); i++) {
            failure_reasons_list.add(new Gson().toJson(installMediums.get(i)));
        }
        editor.putString("install_sources", CommonMethods.getJSONArryString(failure_reasons_list));
        editor.commit();
    }

    public List<String> getInstallSources(){
        List<String> installMediums = new ArrayList<>();
        try {
            JSONArray install_mediums_list = new JSONArray(sharedPreferences.getString("install_sources", "[]"));
            for (int i = 0; i < install_mediums_list.length(); i++) {
                installMediums.add(new Gson().fromJson(install_mediums_list.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return installMediums;
    }



    //--------------- UX IMPROVE DATA --------------//

    public void setHomePageLastLoadedTime(String homePageLastLoadedTime){
        editor.putString("home_page_last_loaded_time", homePageLastLoadedTime);
        editor.commit();
    }

    public String getHomePageLastLoadedTime(){
        return  sharedPreferences.getString("home_page_last_loaded_time", "NA");
    }

    public void setMeatItemPrefillDate(String prefillDate){
        editor.putString("meat_item_prefill_date", prefillDate);
        editor.commit();
    }

    public String getMeatItemPrefillDate(){
        return  sharedPreferences.getString("meat_item_prefill_date", "");
    }



    //=======

    public void setLastShownDeliveriesAlertMessage(String lastShownAlertMessage){
        editor.putString("last_shown_deliveries_alert_message_time", DateMethods.getCurrentDateAndTime());
        editor.putString("last_shown_deliveries_alert_message", lastShownAlertMessage);
        editor.commit();
    }

    public String getLastShownDeliveriesAlertMessage(){
        String last_shown_deliveries_alert_message_time = sharedPreferences.getString("last_shown_deliveries_alert_message_time", "");
        String last_shown_deliveries_alert_message = sharedPreferences.getString("last_shown_deliveries_alert_message", "");
        if ( last_shown_deliveries_alert_message.length() > 0
                && last_shown_deliveries_alert_message_time.length() > 0 ) {
            try {
                long differenceTime = DateMethods.getDifferenceToCurrentTime(last_shown_deliveries_alert_message_time);
                if (differenceTime > 6*60*60*1000 ) {
                    setLastShownDeliveriesAlertMessage("");
                    return "";
                }
            } catch (Exception e) {}
        }
        return last_shown_deliveries_alert_message;
    }

    public void setLastShownBootstrapAlertMessage(String lastShownBootstrapAlertMessage){
        editor.putString("last_shown_bootstrap_alert_message_time", DateMethods.getCurrentDateAndTime());
        editor.putString("last_shown_bootstrap_alert_message", lastShownBootstrapAlertMessage);
        editor.commit();
    }

    public String getLastShownBootstrapAlertMessage(){
        String last_shown_bootstrap_alert_message_time = sharedPreferences.getString("last_shown_bootstrap_alert_message_time", "");
        String last_shown_bootstrap_alert_message = sharedPreferences.getString("last_shown_bootstrap_alert_message", "");
        if ( last_shown_bootstrap_alert_message.length() > 0
                && last_shown_bootstrap_alert_message_time.length() > 0 ) {
            try {
                long differenceTime = DateMethods.getDifferenceToCurrentTime(last_shown_bootstrap_alert_message_time);
                if (differenceTime > 48*60*60*1000 ) {
                    setLastShownDeliveriesAlertMessage("");
                    return "";
                }
            } catch (Exception e) {}
        }
        return last_shown_bootstrap_alert_message;
    }

    //  LAUNCH MESSAGE

    public void setLastLaunchMessage(MessageDetails launchMessage){
        editor.putString("last_launch_message", new Gson().toJson(launchMessage));
        editor.putString("last_launch_message_shown_time", DateMethods.getCurrentDateAndTime());
        editor.commit();
    }
    public MessageDetails getLastLaunchMessage(){
        return new Gson().fromJson(sharedPreferences.getString("last_launch_message", ""), MessageDetails.class);
    }
    public String getLastLaunchMessageShownTime(){
        return sharedPreferences.getString("last_launch_message_shown_time", "");
    }

    //===================== INTERNAL APP ===============//

    public String getInternalAppInitiatedEmployeeID(){
        return sharedPreferences.getString("internal_app_initiated_employee_id", null);
    }
    public void setInternalAppInitiatedEmployeeID(String internalAppInitiatedEmployeeID){
        editor.putString("internal_app_initiated_employee_id", internalAppInitiatedEmployeeID);
        editor.commit();
    }

}

/*

public void storeGCMRegistrationID(String registrationID){
        Log.d(Constants.LOG_TAG, "GCM Registration ID = " + registrationID);

        int appVersion=0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            appVersion = pInfo.versionCode;
        }catch (Exception e){}

        editor.putString("gcmRegID", registrationID);
        editor.putInt("appVersion", appVersion);
        editor.commit();
    }

    public String getGCMRegistrationID(){
        int currentVersion=0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            currentVersion = pInfo.versionCode;
        }catch (Exception e){}

        if ( currentVersion == getAppVersion() ) {
            return sharedPreferences.getString("gcmRegID", "");
        }else{
            return "";
        }
    }

 */