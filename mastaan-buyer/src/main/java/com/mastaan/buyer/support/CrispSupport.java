package com.mastaan.buyer.support;

import android.content.Context;
import android.os.AsyncTask;
import androidx.annotation.NonNull;

import com.mastaan.buyer.R;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.BuyerDetails;

import im.crisp.sdk.Crisp;

/**
 * Created in Mastaan Buyer on 13/12/17.
 * @author Venkatesh Uppu
 */

public class CrispSupport {
    Context context;

    public CrispSupport(@NonNull Context context){
        this.context = context;
    }

    public void setup(){
        try{
            Crisp.initialize(context);
            Crisp.getInstance().setWebsiteId(context.getString(R.string.crisp_app_id));

            updateUserSession();
        }catch (Exception e){
            e.printStackTrace();
            //Crashlytics.logException(e);
        }
    }

    public void updateUserSession(){
        try{
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    try{
                        BuyerDetails buyerDetails = new LocalStorageData(context).getBuyerDetails();
                        if ( buyerDetails == null ){
                            buyerDetails = new BuyerDetails("", "Guest User");
                        }

                        Crisp.User.setNickname(buyerDetails.getName());
                        Crisp.User.setPhone(buyerDetails.getMobile());
                        Crisp.User.setEmail(buyerDetails.getEmail());
                    }catch (Exception e){
                        e.printStackTrace();
                        //Crashlytics.logException(e);
                    }
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }catch (Exception e){}
    }

    public void clearUserSession(){
        try{
            Crisp.Session.reset();
        }catch (Exception e){
            e.printStackTrace();
           // Crashlytics.logException(e);
        }
    }
}
