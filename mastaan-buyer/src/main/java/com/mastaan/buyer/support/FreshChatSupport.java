package com.mastaan.buyer.support;

import android.content.Context;
import androidx.annotation.NonNull;

/**
 * Created in Mastaan Buyer on 13/12/17.
 * @author Venkatesh Uppu
 */

public class FreshChatSupport {
    Context context;

    public FreshChatSupport(@NonNull Context context){
        this.context = context;
    }

    public void setup(){
        /*try{
            FreshchatConfig freshchatConfig = new FreshchatConfig(context.getString(R.string.freshchat_app_id), context.getString(R.string.freshchat_app_key));
            freshchatConfig.setCameraCaptureEnabled(true);
            freshchatConfig.setGallerySelectionEnabled(true);
            freshchatConfig.setTeamMemberInfoVisible(true);
            Freshchat.getInstance(context).init(freshchatConfig);

            FreshchatNotificationConfig freshchatNotificationConfig = new FreshchatNotificationConfig()
                    .setNotificationSoundEnabled(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(R.mipmap.ic_launcher)
                    .setPriority(NotificationCompat.PRIORITY_HIGH);

            Freshchat.getInstance(context).setNotificationConfig(freshchatNotificationConfig);

            updateUserSession();
        }catch (Exception e){
            e.printStackTrace();
            Crashlytics.logException(e);
        }*/
    }

    public void updateUserSession(){
        /*try{
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    try{
                        BuyerDetails buyerDetails = new LocalStorageData(context).getBuyerDetails();
                        if ( buyerDetails == null ){
                            buyerDetails = new BuyerDetails("", "Guest User");
                        }

                        FreshchatUser freshchatUser = Freshchat.getInstance(context).getUser();
                        freshchatUser.setFirstName(buyerDetails.getName());
                        freshchatUser.setEmail(buyerDetails.getEmail());
                        freshchatUser.setExternalId(buyerDetails.getID());
                        freshchatUser.setPhone("", buyerDetails.getMobile());
                        Freshchat.getInstance(context).setUser(freshchatUser);

                        Freshchat.getInstance(context).setPushRegistrationToken(FirebaseInstanceId.getInstance().getToken());
                    }catch (Exception e){
                        e.printStackTrace();
                        Crashlytics.logException(e);
                    }
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }catch (Exception e){}*/
    }

    public void clearUserSession(){
        /*try{
            Freshchat.resetUser(context);
        }catch (Exception e){
            e.printStackTrace();
            Crashlytics.logException(e);
        }*/
    }

}
