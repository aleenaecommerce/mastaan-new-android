package com.mastaan.buyer.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by venkatesh on 21/12/18.
 */

public class FredokaOneTextView extends TextView {

    Typeface font;

    public FredokaOneTextView (Context context) {
        super(context);
        font = Typeface.createFromAsset(context.getAssets(), "fonts/FredokaOne-Regular.ttf");
        this.setTypeface(font);
    }

    public FredokaOneTextView (Context context, AttributeSet attrs) {
        super(context, attrs);
        font = Typeface.createFromAsset(context.getAssets(), "fonts/FredokaOne-Regular.ttf");
        this.setTypeface(font);
    }

    public FredokaOneTextView (Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        font = Typeface.createFromAsset(context.getAssets(), "fonts/FredokaOne-Regular.ttf");
        this.setTypeface(font);
    }
}
