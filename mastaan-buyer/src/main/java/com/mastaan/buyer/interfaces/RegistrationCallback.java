package com.mastaan.buyer.interfaces;


import com.mastaan.buyer.models.BuyerDetails;

/**
 * Created by venkatesh on 12/1/16.
 */
public interface RegistrationCallback {
    void onComplete(boolean status, String token, BuyerDetails buyerDetails);
}
