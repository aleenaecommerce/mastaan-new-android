package com.mastaan.buyer.interfaces;

import com.mastaan.buyer.models.MessageDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 03/03/2018.
 */

public interface MessagesCallback {
    void onComplete(boolean status, int statusCode, String message, List<MessageDetails> messagesList);
}
