package com.mastaan.buyer.interfaces;

/**
 * Created by venkatesh on 12/1/16.
 */
public interface AttributeOptionCallbak{
    void onSelect(int previousPosition, int currentPosition);
}
