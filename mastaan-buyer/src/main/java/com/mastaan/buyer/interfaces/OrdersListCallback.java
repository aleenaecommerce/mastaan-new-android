package com.mastaan.buyer.interfaces;

import com.mastaan.buyer.models.OrderDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public interface OrdersListCallback {
    void onComplete(boolean status, int statusCode, List<OrderDetails> ordersList);
}
