package com.mastaan.buyer.interfaces;

import com.mastaan.buyer.models.MembershipDetails;
import com.mastaan.buyer.models.MessageDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 17/01/19.
 */

public interface MembershipsCallback {
    void onComplete(boolean status, int statusCode, String message, List<MembershipDetails> membershipDetails);
}
