package com.mastaan.buyer.interfaces;

import com.mastaan.buyer.models.MessageDetails;

/**
 * Created by Venkatesh Uppu on 03/03/2018.
 */

public interface MessageCallback {
    void onComplete(boolean status, int statusCode, String message, MessageDetails messageDetails);
}
