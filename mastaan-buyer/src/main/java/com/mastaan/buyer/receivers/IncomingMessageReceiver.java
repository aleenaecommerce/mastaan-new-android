package com.mastaan.buyer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.mastaan.buyer.constants.Constants;

/**
 * Created by venkatesh on 8/7/15.
 */

public class IncomingMessageReceiver extends BroadcastReceiver{

    String senderNum;
    String message;

    String otpService;
    String otp;

    @Override
    public void onReceive(Context context, Intent intent) {
        try{
            otpService = "";
            otp = "";

            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    senderNum = currentMessage.getDisplayOriginatingAddress();;
                    message = currentMessage.getDisplayMessageBody();

                    String[] splitedMessageBySpace = message.split("\\s+");

                    if ( splitedMessageBySpace[0].equalsIgnoreCase("Your") ){
                        otpService = "Service1";
                        otp = splitedMessageBySpace[splitedMessageBySpace.length-1];
                    }
                    else if (splitedMessageBySpace[splitedMessageBySpace.length-1].equalsIgnoreCase("code") ){
                        otpService = "Service2";
                        otp = splitedMessageBySpace[0];
                    }
                }
            }

            if ( message.toLowerCase().contains("Mastaan".toLowerCase()) && message.toLowerCase().contains("validation".toLowerCase()) ){

                Log.d(Constants.LOG_TAG, "Authentication : OTP Message Received , OTP = " + otp+" from = "+otpService);

                if ( otp != null && otp.length() > 0 ){
                    Intent verificationActivityIntent = new Intent(Constants.OTP_RECEIVER)
                            .putExtra(Constants.OTP_SERVICE, otpService)
                            .putExtra(Constants.OTP_CODE, otp);
                    context.sendBroadcast(verificationActivityIntent);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
