package com.mastaan.buyer.handlers;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mastaan.buyer.BuildConfig;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created in Mastaan on 19/08/18.
 * @author Venkatesh Uppu
 */

public class PermissionsHandler {

    MastaanToolBarActivity activity;

    public static final String internetPermissionDetails = "<b>Internet:</b> For login, order placing etc.";
    public static final String vibrationPermissionDetails = "<b>Vibration:</b> For notifications";
    public static final String storagePermissionDetails = "<b>Storage:</b> For adding attachments from storage in customer support";
    public static final String cameraPermissionDetails = "<b>Camera:</b> For adding attachments using camera in customer support";
    public static final String locationPermissionDetails = "<b>Location:</b> To get current location";
    public static final String readSMSPermissionDetails = "<b>SMS:</b> To read login OTP";


    public PermissionsHandler(MastaanToolBarActivity activity){
        this.activity = activity;
    }


    public String getPermissionsDetails(@NonNull List<String> permissionsList){
        String permissionsDetails = "";
        try{
            if ( permissionsList != null && permissionsList.size() > 0 ){
                for (int i=0;i<permissionsList.size();i++){
                    if ( permissionsList.get(i) != null ){
                        String details = getPermissionDetails(permissionsList.get(i));
                        if ( permissionsDetails.toLowerCase().contains(details.toLowerCase()) == false ){
                            if ( permissionsDetails.length() > 0 ){ permissionsDetails += "<br>"; }
                            permissionsDetails += details;
                        }
                    }
                }
            }
        }catch (Exception e){}

        return permissionsDetails;
    }

    public final String getPermissionDetails(String permission){
        try{
            if ( permission.equalsIgnoreCase(Manifest.permission.INTERNET) || permission.equalsIgnoreCase(Manifest.permission.ACCESS_NETWORK_STATE) ){
                return internetPermissionDetails;
            }
            else if ( permission.equalsIgnoreCase(Manifest.permission.VIBRATE) ){
                return vibrationPermissionDetails;
            }
            else if ( permission.equalsIgnoreCase(Manifest.permission.READ_EXTERNAL_STORAGE) || permission.equalsIgnoreCase(Manifest.permission.WRITE_EXTERNAL_STORAGE) ){
                return storagePermissionDetails;
            }
            else if ( permission.equalsIgnoreCase(Manifest.permission.CAMERA) ){
                return cameraPermissionDetails;
            }
            else if ( permission.equalsIgnoreCase(Manifest.permission.ACCESS_FINE_LOCATION) || permission.equalsIgnoreCase(Manifest.permission.ACCESS_COARSE_LOCATION) ){
                return locationPermissionDetails;
            }
            else if ( permission.equalsIgnoreCase(Manifest.permission.READ_SMS) || permission.equalsIgnoreCase(Manifest.permission.RECEIVE_SMS) ){
                return readSMSPermissionDetails;
            }
        }catch (Exception e){}
        return "";
    }

    //-----------

    public void checkExternalStoragePermission(StatusCallback callback){
        checkPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, callback);
    }
    public void checkCameraPermission(StatusCallback callback){
        checkPermissions(new String[]{Manifest.permission.CAMERA}, callback);
    }
    public void checkCameraAndExternalStoragePermissions(StatusCallback callback){
        checkPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, callback);
    }
    public void checkLocationPermission(StatusCallback callback){
        checkPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, callback);
    }
    public void checkSMSPermission(StatusCallback callback){
        checkPermissions(new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS}, callback);
    }

    String []checkPermissions;
    StatusCallback checkPermissionsCallback;
    public void checkPermissions(@NonNull final String[] permissions, final StatusCallback callback){
        checkPermissions(permissions, true, callback);
    }
    public void checkPermissions(@NonNull final String[] permissions, final boolean goToSettings, final StatusCallback callback){
        this.checkPermissions = permissions;
        this.checkPermissionsCallback = callback;

        Dexter.withActivity(activity).withPermissions(permissions).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if ( report.areAllPermissionsGranted() ){
                    if ( callback != null ){
                        callback.onComplete(true, 200, "Success");
                    }
                }
                else{
                    if ( goToSettings && report.isAnyPermissionPermanentlyDenied() ){
                        List<String> deniedPermissions = new ArrayList<>();
                        for (int i=0;i<report.getDeniedPermissionResponses().size();i++) {
                            deniedPermissions.add(report.getDeniedPermissionResponses().get(i).getPermissionName());
                        }

                        String message = "Following permission(s) are required<br><br>"
                                + getPermissionsDetails(deniedPermissions)
                                +"<br><br>Do you want to enable?";

                        activity.showChoiceSelectionDialog(false, "Alert", CommonMethods.fromHtml(message), "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("YES") ){
                                    try{
                                        Intent settingsAct = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
                                        activity.startActivityForResult(settingsAct, Constants.APP_SETTINGS_ACTIVITY_CODE);
                                        activity.showToastMessage("Click on Permissions in App Settings page");
                                    }
                                    catch (Exception e){
                                        activity.showToastMessage("Something went wrong");
                                        if ( callback != null ){
                                            callback.onComplete(false, 500, "Failure");
                                        }
                                    }
                                }
                                else{
                                    if ( callback != null ){
                                        callback.onComplete(false, 500, "Failure");
                                    }
                                }
                            }
                        });
                    }
                    else{
                        if ( callback != null ){
                            callback.onComplete(false, 500, "Failure");
                        }
                    }
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, final PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    //=========

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if ( requestCode == Constants.APP_SETTINGS_ACTIVITY_CODE ){
                checkPermissions(checkPermissions, false, checkPermissionsCallback);
            }
        }catch (Exception e){}
    }

}
