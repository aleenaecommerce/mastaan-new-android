package com.mastaan.buyer.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;

import com.aleena.common.methods.CommonMethods;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.mastaan.buyer.MastaanApplication;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.NotificationDetailsActivity;
import com.mastaan.buyer.activities.OrderDetailsActivity;
import com.mastaan.buyer.activities.TransactionDetailsActivity;
import com.mastaan.buyer.analytics.GoogleAnalyticsMethods;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;
import com.mastaan.buyer.models.NotificationDetails;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Random;

/**
 * Created in Mastaan Buyer on 13/12/17.
 *
 * @author Venkatesh Uppu
 */

public class FirebaseMessageService extends FirebaseMessagingService {

    String TAG = "FIREBASE_MESSAGING_SERVICE : ";
    Context context;

    String analyticsCategoryName = "Firebase Messaging";


    final String NOTIFICATION_TITLE = "gcm.notification.title";
    final String NOTIFICATION_DETAILS = "gcm.notification.body";
    final String ID = "id";
    final String TITLE = "title";
    final String DETAILS = "details";
    final String IMAGE = "image";
    final String ICON = "icon";
    final String ACTION = "action";
    final String USER = "user";
    final String URL = "url";
    final String ORDER = "order";
    final String TRANSACTION = "transaction";


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(Constants.LOG_TAG, TAG+"Created");
        context = getApplicationContext();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try{
            Log.d(Constants.LOG_TAG, TAG+"Message Received, Msg: "+remoteMessage.getData());

            /*if ( Freshchat.isFreshchatNotification(remoteMessage) ) {
                Freshchat.getInstance(this).handleFcmMessage(remoteMessage);
            }*/

            int icon = R.drawable.ic_app_notification;
            int iconBackground = ContextCompat.getColor(context, R.color.app_icon_color);

            final String type = remoteMessage.getData().get("type");//channel_id
            String title = remoteMessage.getNotification() != null ? remoteMessage.getNotification().getTitle() : "";
            String details = remoteMessage.getNotification() != null ? remoteMessage.getNotification().getBody() : "";

            if ( remoteMessage.getData().get(TITLE) != null ){
                title = remoteMessage.getData().get(TITLE);
            }
            if ( remoteMessage.getData().get(DETAILS) != null ){
                details = remoteMessage.getData().get(DETAILS);
            }

            if ( type != null && (title != null || details != null) ){
                // ORDER
                if ( type.trim().equalsIgnoreCase(Constants.NOTIFICATION_TYPES.ORDER) ){
                    Log.d(Constants.LOG_TAG, TAG+ "Order message");

                    String order = remoteMessage.getData().get(ORDER);

                    Intent clickActivity = null;
                    if ( order != null && order.trim().length() > 0 ){
                        clickActivity = new Intent(context, OrderDetailsActivity.class);
                        clickActivity.putExtra(Constants.ID, order);
                    }
                    // Showing notification
                    displayNotification(type, new Random().nextInt(), title, details, null, clickActivity);

                    // Refreshing order details page if it is in stack
                    new BroadcastReceiversMethods(context).reloadOrderDetails(order);

                    sendGoogleAnalyticsEvent("Order", "Success");
                }

                // TRANSACTION
                else if ( type.trim().equalsIgnoreCase(Constants.NOTIFICATION_TYPES.TRANSACTION) ){
                    Log.d(Constants.LOG_TAG, TAG+ "Transaction message");

                    String transaction = remoteMessage.getData().get(TRANSACTION);

                    Intent clickActivity = null;
                    if ( transaction != null && transaction.trim().length() > 0 ){
                        clickActivity = new Intent(context, TransactionDetailsActivity.class);
                        clickActivity.putExtra(Constants.ID, transaction);
                    }
                    // Showing notification
                    displayNotification(type, new Random().nextInt(), title, details, null, clickActivity);

                    sendGoogleAnalyticsEvent("Transaction", "Success");
                }

                // GENERAL / PROMOTION
                else if ( type.equalsIgnoreCase(Constants.NOTIFICATION_TYPES.GENERAL) || type.equalsIgnoreCase(Constants.NOTIFICATION_TYPES.PROMOTION) ){
                    if ( new LocalStorageData(getApplicationContext()).getNotificationsFlag() ){
                        final NotificationDetails notificationDetails = new NotificationDetails(remoteMessage.getData().get(ID)
                                , type
                                , title
                                , details
                                , remoteMessage.getData().get(IMAGE)
                                , remoteMessage.getData().get(ICON)
                                , remoteMessage.getData().get(ACTION)
                                , remoteMessage.getData().get(URL)
                        );
                        String analyticsActionName = CommonMethods.capitalizeStringWords(notificationDetails.getType())+(notificationDetails.getID()!=null&&notificationDetails.getID().trim().length()>0?" - "+notificationDetails.getID().trim():"");

                        final Intent clickActivity = new Intent(context, NotificationDetailsActivity.class)
                                .putExtra(Constants.NOTIFICATION_DETAILS, new Gson().toJson(notificationDetails))
                                .putExtra(Constants.ANALYTICS_EVENT_CATEGORY, analyticsCategoryName)
                                .putExtra(Constants.ANALYTICS_EVENT_ACTION, analyticsActionName);

                        // IF IMAGE PRESENT
                        if ( notificationDetails.getImage() != null && notificationDetails.getImage().trim().length() > 0 ){
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Picasso.get()
                                            .load(notificationDetails.getImage())
                                            .into(new Target() {
                                                @Override
                                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                                    displayNotification(type, new Random().nextInt(), notificationDetails.getTitle(), notificationDetails.getDetails(), bitmap, clickActivity);
                                                }

                                                @Override
                                                public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                                                    displayNotification(type, new Random().nextInt(), notificationDetails.getTitle(), notificationDetails.getDetails(), null, clickActivity);
                                                }

                                                @Override
                                                public void onPrepareLoad(Drawable placeHolderDrawable) {}
                                            });
                                }
                            });
                        }
                        // IF NO IMAGE PRESENT
                        else{
                            displayNotification(type, new Random().nextInt(), title, details, null, clickActivity);
                        }

                        sendGoogleAnalyticsEvent(analyticsActionName, "Success");
                    }
                }

                // OTHER
                else{
                    Log.d(Constants.LOG_TAG, TAG+ "Other message");

                    displayNotification(type, new Random().nextInt(), title, details, null, null);

                    sendGoogleAnalyticsEvent("Other", "Success");
                }
            }
        }catch (Exception e){
            Log.d(Constants.LOG_TAG, TAG+"Message Received, Msg: "+e.getMessage());
            e.printStackTrace();
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    private void sendGoogleAnalyticsEvent(String actionName, String labelName){
        try{
            new GoogleAnalyticsMethods(((MastaanApplication)getApplication()).getAnalyticsTracker()).sendEvent(analyticsCategoryName, actionName, labelName);
        }catch (Exception e){}
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
        Log.d(Constants.LOG_TAG, TAG+ "Message Received with handleIntent, D: "+intent.getExtras());
    }

    /*@Override
    public void handleIntent(Intent intent) {
        try{
            Log.d(Constants.LOG_TAG, TAG+ "Message Received with handleIntent, D: "+intent.getExtras());

            int icon = R.drawable.ic_app_notification;
            int iconBackground = ContextCompat.getColor(context, R.color.app_icon_color);

            final String type = intent.getStringExtra("type") != null ? intent.getStringExtra("type") : "";//channel_id
            String title = intent.getStringExtra(NOTIFICATION_TITLE);
            String details = intent.getStringExtra(NOTIFICATION_DETAILS);

            if ( title != null || details != null ){
                // ORDER
                if ( type.trim().equalsIgnoreCase(Constants.NOTIFICATION_TYPES.ORDER) ){
                    Log.d(Constants.LOG_TAG, TAG+ "Order message");

                    String order = intent.getStringExtra(Constants.NOTIFICATION_TYPES.ORDER);

                    Intent clickActivity = null;
                    if ( order != null && order.trim().length() > 0 ){
                        clickActivity = new Intent(context, OrderDetailsActivity.class);
                        clickActivity.putExtra(Constants.ID, order);
                    }
                    // Showing notification
                    displayNotification(type, new Random().nextInt(), title, details, null, clickActivity);

                    // Refreshing order details page if it is in stack
                    new BroadcastReceiversMethods(context).reloadOrderDetails(order);

                    sendGoogleAnalyticsEvent("Order", "Success");
                }

                // TRANSACTION
                else if ( type.trim().equalsIgnoreCase(Constants.NOTIFICATION_TYPES.TRANSACTION) ){
                    Log.d(Constants.LOG_TAG, TAG+ "Transaction message");

                    String transaction = intent.getStringExtra(Constants.NOTIFICATION_TYPES.TRANSACTION);

                    Intent clickActivity = null;
                    if ( transaction != null && transaction.trim().length() > 0 ){
                        clickActivity = new Intent(context, TransactionDetailsActivity.class);
                        clickActivity.putExtra(Constants.ID, transaction);
                    }
                    // Showing notification
                    displayNotification(type, new Random().nextInt(), title, details, null, clickActivity);

                    sendGoogleAnalyticsEvent("Transaction", "Success");
                }

                // GENERAL / PROMOTION
                else if ( type.equalsIgnoreCase(Constants.NOTIFICATION_TYPES.GENERAL) || type.equalsIgnoreCase(Constants.NOTIFICATION_TYPES.PROMOTION) ){
                    if ( new LocalStorageData(getApplicationContext()).getNotificationsFlag() ){
                        final NotificationDetails notificationDetails = new NotificationDetails(intent.getStringExtra(ID)
                                , type
                                , intent.getStringExtra(NOTIFICATION_TITLE)
                                , intent.getStringExtra(NOTIFICATION_DETAILS)
                                , intent.getStringExtra(IMAGE)
                                , intent.getStringExtra(ICON)
                                , intent.getStringExtra(ACTION)
                                , intent.getStringExtra(URL)
                        );
                        String analyticsActionName = CommonMethods.capitalizeStringWords(notificationDetails.getType())+(notificationDetails.getID()!=null&&notificationDetails.getID().trim().length()>0?" - "+notificationDetails.getID().trim():"");

                        final Intent clickActivity = new Intent(context, NotificationDetailsActivity.class)
                                .putExtra(Constants.NOTIFICATION_DETAILS, new Gson().toJson(notificationDetails))
                                .putExtra(Constants.ANALYTICS_EVENT_CATEGORY, analyticsCategoryName)
                                .putExtra(Constants.ANALYTICS_EVENT_ACTION, analyticsActionName);

                        // IF IMAGE PRESENT
                        if ( notificationDetails.getImage() != null && notificationDetails.getImage().trim().length() > 0 ){
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Picasso.with(context)
                                            .load(notificationDetails.getImage())
                                            .into(new Target() {
                                                @Override
                                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                                    displayNotification(type, new Random().nextInt(), notificationDetails.getTitle(), notificationDetails.getDetails(), bitmap, clickActivity);
                                                }

                                                @Override
                                                public void onBitmapFailed(Drawable errorDrawable) {
                                                    displayNotification(type, new Random().nextInt(), notificationDetails.getTitle(), notificationDetails.getDetails(), null, clickActivity);
                                                }

                                                @Override
                                                public void onPrepareLoad(Drawable placeHolderDrawable) {}
                                            });
                                }
                            });
                        }
                        // IF NO IMAGE PRESENT
                        else{
                            displayNotification(type, new Random().nextInt(), title, details, null, clickActivity);
                        }

                        sendGoogleAnalyticsEvent(analyticsActionName, "Success");
                    }
                }

                // OTHER
                else{
                    Log.d(Constants.LOG_TAG, TAG+ "Other message");

                    displayNotification(type, new Random().nextInt(), title, details, null, null);

                    sendGoogleAnalyticsEvent("Other", "Success");
                }
            }
        }
        catch (Exception e){
            Log.d(Constants.LOG_TAG, TAG+"Message Received with handleIntent, Parse Exception, Msg: "+e.getMessage());
            FirebaseCrashlytics.getInstance().recordException(e);    // Sending To Firebse Crash  FirebaseCrash.log("Activity created");
            e.printStackTrace();
        }
    }*/


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(Constants.LOG_TAG, TAG+"Destroyed");
    }


    public void displayNotification(String channel, int index, String title, String details, Bitmap image, Intent activity){
        try{
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channel)
                    .setContentTitle(title!=null?title:"")
                    .setContentText(details!=null?details:"")
                    .setSmallIcon(R.drawable.ic_app_notification)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(Notification.PRIORITY_MAX);

            if ( image != null ){
                notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(image)
                        .setBigContentTitle(title)
                        .setSummaryText(details));
            }else{
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle(title)
                        .bigText(details));
            }

            if ( activity != null ){
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, activity, PendingIntent.FLAG_UPDATE_CURRENT);
                notificationBuilder.setContentIntent(pendingIntent);
            }

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(index, notificationBuilder.build());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

}