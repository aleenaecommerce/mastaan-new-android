package com.mastaan.buyer.fcm;

import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.mastaan.buyer.MastaanApplication;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;

/**
 * Created in Mastaan Buyer on 13/12/17.
 * @author Venkatesh Uppu
 */

public class FirebaseTokenService extends FirebaseInstanceIdService {

    String TAG = "FIREBASE_TOKEN_SERVICE : ";
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(Constants.LOG_TAG, TAG+"Created");
        context = this;
    }

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        try {
            Log.d(Constants.LOG_TAG, TAG + "Token refresh , T: " + FirebaseInstanceId.getInstance().getToken());

            /*Freshchat.getInstance(this).setPushRegistrationToken(FirebaseInstanceId.getInstance().getToken());*/

            // Linking buyer to FCM
            if ( new LocalStorageData(context).getSessionFlag() ) {
                ((MastaanApplication) getApplication()).getCurrentActivity().getBackendAPIs().getProfileAPI().linkToFCM(FirebaseInstanceId.getInstance().getToken(), null);
            }
        }catch (Exception e){
            e.printStackTrace();
            //Crashlytics.logException(e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(Constants.LOG_TAG, TAG+"Destroyed");
    }

}
