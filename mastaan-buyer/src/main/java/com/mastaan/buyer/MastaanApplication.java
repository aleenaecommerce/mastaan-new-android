package com.mastaan.buyer;

import android.app.AlertDialog;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aleena.common.methods.AppMethods;
import com.aleena.common.methods.DeviceMethods;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.backend.BackendAPIs;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.interfaces.MembershipsCallback;
import com.mastaan.buyer.interfaces.MessagesCallback;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.MembershipDetails;
import com.mastaan.buyer.models.MessageDetails;
import com.mastaan.buyer.models.ReferralOfferDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by venkatesh on 15/9/15.
 */

public class MastaanApplication extends Application{//MultiDexApplication {

    MastaanToolBarActivity currentActivity;

    AlertDialog notificationDialog;
    View notificationDialogView;

    private Tracker analyticsTracker;


    BackendAPIs backendAPIs;
    LocalStorageData localStorageData;


    @Override
    public void onCreate() {
        super.onCreate();

        try {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            NotificationManager notificationManager = getSystemService(NotificationManager.class);

                            NotificationChannel generalNotificationsChannel = new NotificationChannel(Constants.NOTIFICATION_TYPES.GENERAL, "General Notifications", NotificationManager.IMPORTANCE_DEFAULT);
                            generalNotificationsChannel.setDescription("General Notifications");
                            notificationManager.createNotificationChannel(generalNotificationsChannel);

                            NotificationChannel promotionalNotificationsChannel = new NotificationChannel(Constants.NOTIFICATION_TYPES.PROMOTION, "Promotional Notifications", NotificationManager.IMPORTANCE_DEFAULT);
                            generalNotificationsChannel.setDescription("Promotional Notifications");
                            notificationManager.createNotificationChannel(promotionalNotificationsChannel);

                            NotificationChannel orderNotificationsChannel = new NotificationChannel(Constants.NOTIFICATION_TYPES.ORDER, "Order Notifications", NotificationManager.IMPORTANCE_HIGH);
                            generalNotificationsChannel.setDescription("Order Notifications");
                            notificationManager.createNotificationChannel(orderNotificationsChannel);

                            NotificationChannel transactionNotificationsChannel = new NotificationChannel(Constants.NOTIFICATION_TYPES.TRANSACTION, "Transaction Notifications", NotificationManager.IMPORTANCE_HIGH);
                            generalNotificationsChannel.setDescription("Transaction Notifications");
                            notificationManager.createNotificationChannel(transactionNotificationsChannel);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        FirebaseCrashlytics.getInstance().recordException(e);
                    }
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }catch (Exception e){}catch (Error e){}
    }

    public void initializeAnalyticsTracker() {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);//analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
        analyticsTracker = analytics.newTracker(R.xml.global_tracker);
        String buyerID = getLocalStorageData().getBuyerID();
        if ( buyerID != null && buyerID.length() > 0 ){
            analyticsTracker.set("&uid", buyerID);
        }
    }

    synchronized public Tracker getAnalyticsTracker() {
        if (analyticsTracker == null) {
            initializeAnalyticsTracker();
        }
        return analyticsTracker;
    }


    public void setCurrentActivity(MastaanToolBarActivity activity){
        currentActivity = activity;
    }

    public MastaanToolBarActivity getCurrentActivity() {
        return currentActivity;
    }

    public void showNotificationDialog(String notificationTitle, String notificationSubtitle){
        try{
            if ( notificationDialog == null ){
                notificationDialog = new AlertDialog.Builder(currentActivity).create();
                notificationDialogView = LayoutInflater.from(currentActivity).inflate(R.layout.dialog_notifcation, null);
                notificationDialog.setView(notificationDialogView);
                notificationDialog.setCancelable(true);
            }
            TextView title = (TextView) notificationDialogView.findViewById(R.id.title);
            TextView subtitle = (TextView) notificationDialogView.findViewById(R.id.subtitle);
            title.setText(notificationTitle);
            subtitle.setText(notificationSubtitle);

            notificationDialog.show();

            notificationDialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( notificationDialog != null ){
                        notificationDialog.dismiss();
                    }
                }
            });
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), notificationTitle + " : " + notificationSubtitle, Toast.LENGTH_LONG).show();
        }
    }
    public void closeNotificationDialog(){
        if ( notificationDialog != null ){
            notificationDialog.dismiss();
        }
    }

    //-----

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(this, DeviceMethods.getDeviceID(getBaseContext()), AppMethods.getAppVersionCode(getBaseContext()));
        }
        return backendAPIs;
    }

    public LocalStorageData getLocalStorageData() {
        if ( localStorageData == null ){    localStorageData = new LocalStorageData(getApplicationContext());   }
        return localStorageData;
    }

    List<MessageDetails> alertMessages;
    private void getAlertMessages(final MessagesCallback callback) {
        if ( alertMessages != null ){
            if ( callback != null ){
                callback.onComplete(true, 200, "Success", alertMessages);
            }
        }else{
            getBackendAPIs().getGeneralAPI().getAlertMessages(new MessagesCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<MessageDetails> messagesList) {
                    if ( status ){
                        alertMessages = messagesList;
                    }
                    if ( callback != null ) {
                        callback.onComplete(true, 200, "Success", messagesList);
                    }
                }
            });
        }
    }
    public void resetAlertMessages(){
        this.alertMessages = null;
    }

    public void getAlertMessages(final String messageType, final MessagesCallback callback){
        getAlertMessages(new MessagesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, final String message, final List<MessageDetails> messagesList) {
                if ( status ){
                    new AsyncTask<Void, Void, Void>() {
                        List<MessageDetails> filteredMessages = new ArrayList<>();
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try{
                                /*String finalMessageType = messageType;
                                if ( finalMessageType.equalsIgnoreCase(Constants.SEA_WATER_SF) ){   finalMessageType = "seawaterseafood";   }
                                else if ( finalMessageType.equalsIgnoreCase(Constants.FRESH_WATER_SF) ){   finalMessageType = "freshwaterseafood";   }*/

                                if ( messageType.equalsIgnoreCase("launchmessage") ){
                                    for (int i=0;i<messagesList.size();i++){
                                        if ( messagesList.get(i) != null && messagesList.get(i).getType().trim().equalsIgnoreCase(messageType.trim()) ){
                                            filteredMessages.add(messagesList.get(i));
                                            break;
                                        }
                                    }
                                }else{
                                    for (int i=0;i<messagesList.size();i++){
                                        if ( messagesList.get(i) != null && ( messagesList.get(i).getType().trim().toLowerCase().contains(messageType.trim().toLowerCase()) || messagesList.get(i).getType().toLowerCase().contains("all") ) ){
                                            filteredMessages.add(messagesList.get(i));
                                        }
                                    }
                                }
                            }catch (Exception e){e.printStackTrace();}
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if ( callback != null ){
                                callback.onComplete(true, 200, "Success", filteredMessages);
                            }
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }else{
                    if ( callback != null ) {
                        callback.onComplete(status, statusCode, message, null);
                    }
                }
            }
        });
    }

    ReferralOfferDetails referralOfferDetails;
    public void getReferralOfferDetails(final GeneralAPI.ReferralOfferCallback callback){
        if ( referralOfferDetails != null ){
            if ( callback != null ) {
                callback.onComplete(true, 200, "Success", referralOfferDetails);
            }
        }
        else{
            getBackendAPIs().getGeneralAPI().getReferralOfferDetails(new GeneralAPI.ReferralOfferCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, ReferralOfferDetails referralOffer) {
                    if ( status ){
                        referralOfferDetails = referralOffer;
                    }
                    if ( callback != null ) {
                        callback.onComplete(status, statusCode, message, referralOffer);
                    }
                }
            });
        }
    }

    List<MembershipDetails> membershipsList;
    public void getMemberships(final MembershipsCallback callback){
        if ( membershipsList != null ){
            if ( callback != null ) {
                callback.onComplete(true, 200, "Success", membershipsList);
            }
        }
        else{
            getBackendAPIs().getGeneralAPI().getMemberships(new MembershipsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<MembershipDetails> memberships) {
                    if ( status ){
                        membershipsList = memberships;
                    }
                    if ( callback != null ) {
                        callback.onComplete(status, statusCode, message, memberships);
                    }
                }
            });
        }
    }
    public void resetMemberships(){
        this.membershipsList = null;
    }

    //-----

    boolean submittedHighRatedFeedback;
    public void setSubmittedHighRatedFeedback(boolean submittedHighRatedFeedback) {
        this.submittedHighRatedFeedback = submittedHighRatedFeedback;
    }
    public boolean isSubmittedHighRatedFeedback() {
        return submittedHighRatedFeedback;
    }

}


/*

// uncaught exception handler variable
    private Thread.UncaughtExceptionHandler defaultUEH;

    // handler listener
    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable exception) {

                    String appVersionName="";
                    int appVersionNumber=0;
                    PackageInfo packageInfo = null;
                    try {
                        packageInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
                        appVersionName = packageInfo.versionName;
                        appVersionNumber = packageInfo.versionCode;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }

                    String crashStackTrace = Log.getStackTraceString(exception);
                    String crashCause =  exception.getCause().toString();
                    int crashLineNumber = exception.getCause().getStackTrace()[0].getLineNumber();
                    String crashMethod = exception.getCause().getStackTrace()[0].getMethodName();
                    String crashClass = exception.getCause().getStackTrace()[0].getClassName();

                    Log.d("GoCiboCrashes", "\n[ APP_CRASH in v"+appVersionName+" (vn"+appVersionNumber+") ] =>  Cause : " + crashCause+" , Stack -> { @Line : "+crashLineNumber+" , @Method : "+crashMethod+" , @Class : "+crashClass+" }");

                }
            };

    public MastaanApplication() {
        //defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        //Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler); // setup handler for uncaught exception
    }

 */