package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.aleena.common.widgets.vGridView;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.AttributeOptionsIconGridAdapter;
import com.mastaan.buyer.models.AttributeOptionDetails;

import java.util.ArrayList;
import java.util.List;

public class IconGridAttributeOptionsActivity extends MastaanToolBarActivity {

    vGridView optionsHolder;

    String attributeName;
    List<AttributeOptionDetails> attributeOptions = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icon_grid_attribute_options);
        getGoogleAnalyticsMethods().sendScreenName("Icon Grid Attribute Options");       // Send ScreenNames to Analytics

        attributeName = getIntent().getStringExtra("attributeName");
        ArrayList<String> attribute_options_jsons = getIntent().getStringArrayListExtra("attribute_options_jsons");
        if ( attribute_options_jsons != null ){
            for (int i=0;i<attribute_options_jsons.size();i++){
                attributeOptions.add(new Gson().fromJson(attribute_options_jsons.get(i), AttributeOptionDetails.class));
            }
        }

        //---------

        setToolBarTitle("Select " + attributeName + " ");

        optionsHolder = (vGridView) findViewById(R.id.optionsHolder);
        optionsHolder.setExpanded(true);
        optionsHolder.setAdapter(new AttributeOptionsIconGridAdapter(context, R.layout.view_attribute_option_icongrid1, attributeOptions, new AttributeOptionsIconGridAdapter.Callback() {
            @Override
            public void onItemClick(int position, AttributeOptionDetails optionDetails) {
                Intent resultData = new Intent();
                resultData.putExtra("position", position);
                setResult(RESULT_OK, resultData);
                finish();
            }
        }));
    }

    //=============

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
