package com.mastaan.buyer.activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;

import com.mastaan.buyer.R;
import com.mastaan.buyer.constants.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class NutritionsActivity extends MastaanToolBarActivity {

    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutritions);
        hasLoadingView();

        getGoogleAnalyticsMethods().sendScreenName("Nutritional facts");   // Send ScreenNames to Analytics

        try{
            if ( getIntent().getStringExtra(Constants.COLOR_PRIMARY) != null  ){
                setThemeColors(Color.parseColor(getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK)!=null?getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK):getIntent().getStringExtra(Constants.COLOR_PRIMARY))
                        , Color.parseColor(getIntent().getStringExtra(Constants.COLOR_PRIMARY))
                );
            }
        }catch (Exception e){}

        //--------

        image = findViewById(R.id.image);

        showLoadingIndicator("Loading...");
        Picasso.get()
                .load(getIntent().getStringExtra(Constants.URL))
                .into(image, new Callback() {
                    @Override
                    public void onSuccess() {
                        switchToContentPage();
                    }

                    @Override
                    public void onError(Exception e) {
                        showReloadIndicator("Something went wrong, try again!");

                    }


                });

    }

    //=========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}

