package com.mastaan.buyer.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.adapters.SearchPlacesAdapter;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.location.HereMapsPlacesAPI;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vEditText;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.constants.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PlaceHereMapSearchActivity extends MastaanToolBarActivity implements View.OnClickListener {
    boolean isForActivityResult = true;
    LatLng latLng;
    vEditText searchLocation;
    vRecyclerView searchPlacesHolder;
    SearchPlacesAdapter searchPlacesAdapter;
    SearchPlacesAdapter.CallBack searchPlacesCallback;
    String searchName;
    ImageView clearSearch;
    LinearLayout addressBook;
    ImageView addressBookClickIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_search);
        // getGoogleAnalyticsMethods().sendScreenName("Place Search");       // Send ScreenNames to Analytics

        searchLocation = findViewById(R.id.searchLocation);

        addressBookClickIcon = findViewById(R.id.addressBookClickIcon);
        addressBook = findViewById(R.id.addressBook);
        if (localStorageData.getSessionFlag() == true) {
            addressBook.setOnClickListener(this);
        } else {
            addressBook.setVisibility(View.GONE);
        }

        clearSearch = findViewById(R.id.clearSearch);
        clearSearch.setOnClickListener(this);

        searchLocation.setTextChangeListener(new vEditText.TextChangeCallback() {
            @Override
            public void onTextChangeFinish(final String text) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (text != null && text.length() > 0) {
                            getPlacePredictions(text);
                        } else {
                            clearSearch.setVisibility(View.INVISIBLE);
                            searchPlacesAdapter.clearItems();
                        }
                    }
                });
            }
        });

        searchPlacesHolder = findViewById(R.id.searchPlacesHolder);

        searchPlacesHolder.setHasFixedSize(true);
        searchPlacesHolder.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, GridLayoutManager.VERTICAL, false));
        searchPlacesHolder.setItemAnimator(new DefaultItemAnimator());

        searchPlacesCallback = new SearchPlacesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Map<String, String> placeDetails = searchPlacesAdapter.getPlaceDetails(position);
                if (placeDetails != null) {
                    Log.e("itemclick ddd", "" + position);

                    fetchPlaceDetailsByIDAndProceed(placeDetails.get("title"));
                }
            }
        };

        searchPlacesAdapter = new SearchPlacesAdapter(getApplicationContext(), new ArrayList<Map<String, String>>(), searchPlacesCallback);
        searchPlacesHolder.setAdapter(searchPlacesAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPlacePredictions(searchName);
    }

    @Override
    public void onClick(View view) {

        if (view == clearSearch) {
            switchToContentPage();
            searchLocation.setText("");
            searchPlacesAdapter.clearItems();
        } else if (view == addressBook) {
            addressBookClickIcon.setVisibility(View.VISIBLE);
            new CountDownTimer(150, 50) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    addressBookClickIcon.setVisibility(View.GONE);
                    Intent addressBookSaveActivity = new Intent(context, AddressBookActivity.class);
                    addressBookSaveActivity.putExtra("favourite_location_json", new Gson().toJson(""));
                    startActivityForResult(addressBookSaveActivity, Constants.ADDRESS_BOOK_ACTIVITY_CODE);
                }
            }.start();
        }
    }


    public void fetchPlaceDetailsByIDAndProceed(final String searchName) {

        Log.e("itemclick", searchName);
        showLoadingDialog("Fetching place details, please wait...");
//        getHereMapsSearchAPI().getPlaceDetailsBySearch("bR9X8dKi_hfjCIzXLAodaLUc5D6Hd54OxlrOwAmH9rQ", searchName, new HereMapsPlacesAPI1.PlaceDetailsCallback() {
//
//
//            @Override
//            public void onComplete(boolean status, int statusCode, String queryName, PlaceHereMapsByplaceIdLatLng placeDetails) {
//
//                if (status == true) {
//
//                    //   Log.e("placehere", placeDetails.label);
//
////                    if (placeDetails.getAddress_location().address.getCountry() != null && placeDetails.getAddress_location().address.getState() != null && placeDetails.getAddress_location().address.getCountry() != null && placeDetails.getAddress_location().address.getPostalCode() != null) {
////                        closeLoadingDialog();
////                        placeDetails.setLabel(placeDetails.getAddress_location().address.getLabel());
////                        onDone(placeDetails);
////                        Log.e("placehere", placeDetails.getAddress_location().address.getLabel());
////                    } else {
//                    Log.d(Constants.LOG_TAG, "Loading Place Details with LatLng for PlaceID = " + placeDetails + " (" + searchName + ") with LatLng = ");
//                    fetchPlaceDetailsByLatLngAndProceed(false, searchName);
////                    }
//                } else {
//                    closeLoadingDialog();
//                    showSnackbarMessage("Unable to Fetch Place Details!", "RETRY", new ActionCallback() {
//                        @Override
//                        public void onAction() {
//                            fetchPlaceDetailsByIDAndProceed(searchName);
//                        }
//                    });
//                }
//            }
//
//
//        });


    }

    public void fetchPlaceDetailsByLatLngAndProceed(final boolean showLoadingDialog, final String placeFullName) {

        if (showLoadingDialog) {
            showLoadingDialog("Fetching place details, please wait...");
        }
//        getHereMapsSearchAPI().getPlaceDetailsByPlaceID("bR9X8dKi_hfjCIzXLAodaLUc5D6Hd54OxlrOwAmH9rQ", placeFullName, new HereMapsPlacesAPI1.PlacesIDCallback() {
//
//
//            @Override
//            public void onComplete(boolean status, int statusCode, String queryName, PlaceHereMapsByplaceIdLatLng placeHereMapsSearchDataByLatLng) {
//
//
//                if (status == true) {
//                    // placeHereMapsSearchDataByLatLng.se(placeFullName);
//                  //  onDone(placeHereMapsSearchDataByLatLng);
//                } else {
//                    closeLoadingDialog();
//                    showSnackbarMessage("Unable to Fetch Place Details!", "RETRY", new ActionCallback() {
//                        @Override
//                        public void onAction() {
//                            fetchPlaceDetailsByLatLngAndProceed(true, placeFullName);
//                        }
//                    });
//                }
//            }
//
//
//        });

    }

    public void onDone(final PlaceDetails deliverLocationDetails) {

        if (deliverLocationDetails != null) {

            inputMethodManager.hideSoftInputFromWindow(searchLocation.getWindowToken(), 0);    // Hides Key Board After Item Select..

            if (isForActivityResult == true) {
                String deliver_location_json = new Gson().toJson(deliverLocationDetails);
                Intent placeData = new Intent();
                placeData.putExtra("deliver_location_json", deliver_location_json);
                setResult(Constants.PLACE_SEARCH_ACTIVITY_CODE, placeData);
                finish();
            } else {
                if (getResources().getString(R.string.app_type).equalsIgnoreCase("internal")) {
                    showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to continue with selected location <b>" + "</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                checkAvailabilityAtLocationAndProceed(deliverLocationDetails);
                            }
                        }
                    });
                } else {
                    checkAvailabilityAtLocationAndProceed(deliverLocationDetails);
                }
            }
        } else {
            showSnackbarMessage("Delivery location not available.");
        }
    }


    public void getPlacePredictions(String qName) {

        try {


            searchName = qName;

            //clearSearch.setVisibility(View.VISIBLE);
            searchPlacesAdapter.clearItems();

            showLoadingIndicator("Searching, wait..");
            getHerePlacesAPI().getHerePlacePredictions("17.3850051,78.4845113", qName, 300 * 1000, "dj6OLxVaoEv2n0s47QrB", "3p5ylTFzJf8-y3KN9snOzQ", "plain", "city=Hyderabad", new HereMapsPlacesAPI.PlacePredictionsCallback() {

                @Override
                public void onComplete(boolean status, int statusCode, String queryName, List<Map<String, String>> placePredictions) {

                    try {


                        if (queryName.equalsIgnoreCase(searchName)) {
                            switchToContentPage();
                            if (status == true) {
                                if (placePredictions.size() > 0) {
                                    searchPlacesAdapter.addItems(placePredictions);
                                } else {
                                    showNoDataIndicator("Nothing found ");
                                }
                            } else {
                                showReloadIndicator(statusCode, "Unable to retrieve places, try again!");
                            }
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void checkAvailabilityAtLocationAndProceed(final PlaceDetails placeDetails) {

        inputMethodManager.hideSoftInputFromWindow(searchLocation.getWindowToken(), 0);    // Hides Key Board After Item Select..

//        latLng=placeDetails.getAddress_location().displayPosition.getLatitude()+","+placeDetails.getAddress_location().displayPosition.getLongitude();
        showLoadingDialog("Launching Home, please wait..");
        getBackendAPIs().getGeneralAPI().checkAvailabilityAtLocation(placeDetails.getId(), placeDetails.getLatLng(), new GeneralAPI.CheckAvailabilityAtLocationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation) {


                if (status) {

                    if (responseCheckAvailabilityAtLocation.isAvailable()) {
                        //launchHomePage(new AddressBookItemDetails(placeDetails), responseCheckAvailabilityAtLocation);           // LAUNCHING HOME PAGE
                    } else {
                        closeLoadingDialog();
                        //  showNoServiceDialog(new AddressBookItemDetails(placeDetails), null);
                        showNotificationDialog("No Service!", Html.fromHtml("We are not serving at the selected location. We are currently serving at <b>" + localStorageData.getServingAt() + "</b>"));
                    }
                } else {
                    closeLoadingDialog();
                    showChoiceSelectionDialog(true, "Error!", "Something went wrong while checking availability at selected location.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                checkAvailabilityAtLocationAndProceed(placeDetails);
                            }
                        }
                    });
                }

            }


        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE && resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE) {      // If It is from PlaceSearch Activity
                String address_item_json = data.getStringExtra("address_item_json");
                PlaceDetails deliverLocationDetails = new PlaceDetails(new Gson().fromJson(address_item_json, AddressBookItemDetails.class));
                onDone(deliverLocationDetails);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(searchLocation.getWindowToken(), 0);    // Hides Key Board After Item Select..
    }


}