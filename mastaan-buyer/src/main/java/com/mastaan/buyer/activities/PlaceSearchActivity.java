package com.mastaan.buyer.activities;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.adapters.SearchPlacesAdapter;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.location.HereMapsPlacesAPI;
import com.aleena.common.location.HereMapsPlacesAPI1;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vEditText;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.constants.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class PlaceSearchActivity extends MastaanToolBarActivity implements View.OnClickListener {

    public Double latitude;
    public Double longtitude;
    boolean isForActivityResult = true;
    String searchName;
    LatLng selectedLocationLatLng;
    Location location;
    vEditText searchLocation;
    ImageView clearSearch;
    LinearLayout addressBook;
    ImageView addressBookClickIcon;
    vRecyclerView searchPlacesHolder;
    SearchPlacesAdapter searchPlacesAdapter;
    SearchPlacesAdapter.CallBack searchPlacesCallback;
    private GpsTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_place_search);
        getGoogleAnalyticsMethods().sendScreenName("Place Search");       // Send ScreenNames to Analytics
        hasLoadingView();
        switchToContentPage();

        isForActivityResult = getIntent().getBooleanExtra("isForActivityResult", false);
        if (isForActivityResult) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else {
            actionBar.setTitle(" " + actionBar.getTitle());
            actionBar.setDisplayHomeAsUpEnabled(false);
        }

        //............................................................

        addressBookClickIcon = (ImageView) findViewById(R.id.addressBookClickIcon);
        addressBook = (LinearLayout) findViewById(R.id.addressBook);
        if (localStorageData.getSessionFlag() == true) {
            addressBook.setOnClickListener(this);
        } else {
            addressBook.setVisibility(View.GONE);
        }

        clearSearch = (ImageView) findViewById(R.id.clearSearch);
        clearSearch.setOnClickListener(this);

        searchLocation = (vEditText) findViewById(R.id.searchLocation);
        searchLocation.setTextChangeListener(new vEditText.TextChangeCallback() {
            @Override
            public void onTextChangeFinish(final String text) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (text != null && text.length() > 0) {
                            getPlacePredictions(text);
                        } else {
                            clearSearch.setVisibility(View.INVISIBLE);
                            searchPlacesAdapter.clearItems();
                        }
                    }
                });
            }
        });

        searchPlacesHolder = (vRecyclerView) findViewById(R.id.searchPlacesHolder);
        searchPlacesHolder.setHasFixedSize(true);
        searchPlacesHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        searchPlacesHolder.setItemAnimator(new DefaultItemAnimator());

        searchPlacesCallback = new SearchPlacesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Map<String, String> placeDetails = searchPlacesAdapter.getPlaceDetails(position);
                if (placeDetails != null) {

                    fetchPlaceDetailsByIDAndProceed(placeDetails.get("title"), placeDetails.get("vicinity"));
                    Log.e("searchname", "" + placeDetails.get("title"));

                }
            }
        };

        searchPlacesAdapter = new SearchPlacesAdapter(context, new ArrayList<Map<String, String>>(), searchPlacesCallback);
        searchPlacesHolder.setAdapter(searchPlacesAdapter);

    }

    @Override
    public void onClick(View view) {

        if (view == clearSearch) {
            switchToContentPage();
            searchLocation.setText("");
            searchPlacesAdapter.clearItems();
        } else if (view == addressBook) {
            addressBookClickIcon.setVisibility(View.VISIBLE);
            new CountDownTimer(150, 50) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    addressBookClickIcon.setVisibility(View.GONE);
                    Intent addressBookSaveActivity = new Intent(context, AddressBookActivity.class);
                    addressBookSaveActivity.putExtra("favourite_location_json", new Gson().toJson(""));
                    startActivityForResult(addressBookSaveActivity, Constants.ADDRESS_BOOK_ACTIVITY_CODE);
                }
            }.start();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPlacePredictions(searchName);
    }

    public void getPlacePredictions(String qName) {

        searchName = qName;

        clearSearch.setVisibility(View.VISIBLE);
        searchPlacesAdapter.clearItems();

        showLoadingIndicator("Searching, wait..");
        gpsTracker = new GpsTracker(PlaceSearchActivity.this);
        latitude = gpsTracker.getLatitude();
        longtitude = gpsTracker.getLongitude();
        String lat = latitude + "," + longtitude;
        Log.e("lat search", lat);
//        getHerePlacesAPI().getHerePlacePredictions(lat, qName, 300 * 1000, "dj6OLxVaoEv2n0s47QrB", "3p5ylTFzJf8-y3KN9snOzQ",  new HereMapsPlacesAPI.PlacePredictionsCallback() {
        getHerePlacesAPI().getHerePlacePredictions(lat, qName, 300 * 1000, "dj6OLxVaoEv2n0s47QrB", "3p5ylTFzJf8-y3KN9snOzQ", "plain", "city=Hyderabad", new HereMapsPlacesAPI.PlacePredictionsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String queryName, List<Map<String, String>> placePredictions) {
                try {
                    if (queryName.equalsIgnoreCase(searchName)) {
                        switchToContentPage();
                        if (status) {

                            if (placePredictions.size() > 0) {
                                searchPlacesAdapter.addItems(placePredictions);
                            } else {
                                showNoDataIndicator("Nothing found ");
                            }
                        } else {
                            showReloadIndicator(statusCode, "Unable to retrieve places, try again!");
                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
    }


//    public void fetchPlaceDetailsByIDAndProceed(final String placeID, final String placeFullName){
//        showLoadingDialog("Fetching place details, please wait...");
//        getGooglePlacesAPI().getPlaceDetailsByPlaceID(placeID, new GooglePlacesAPI.PlaceDetailsCallback() {
//            @Override
//            public void onComplete(boolean status, int statusCode, LatLng location, final String placeID, PlaceDetails placeDetails) {
//                if (status == true) {
//                    if ( placeDetails.getLocality() != null && placeDetails.getState() != null && placeDetails.getCountry() != null && placeDetails.getPostal_code() != null ){
//                        closeLoadingDialog();
//                        placeDetails.setFullName(placeFullName);
//                        onDone(placeDetails);
//                    }else{
//                        fetchPlaceDetailsByLatLngAndProceed(false, placeDetails.getLatLng(), placeFullName);
//                    }
//                }
//                else {
//                    closeLoadingDialog();
//                    showSnackbarMessage("Unable to Fetch Place Details!", "RETRY", new ActionCallback() {
//                        @Override
//                        public void onAction() {
//                            fetchPlaceDetailsByIDAndProceed(placeID, placeFullName);
//                        }
//                    });
//                }
//            }
//        });
//    }

    public void fetchPlaceDetailsByIDAndProceed(final String qryname, final String placeFullName) {
        Log.e("itemclick", qryname + placeFullName);
        showLoadingDialog("Fetching place details, please wait...");
        getHereMapsSearchAPI().getPlaceDetailsByPlaceID1("bR9X8dKi_hfjCIzXLAodaLUc5D6Hd54OxlrOwAmH9rQ",   placeFullName, new HereMapsPlacesAPI1.PlaceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String location, String placeID, PlaceDetails placeDetails) {

                if (placeDetails.getTitle() != null) {
                    closeLoadingDialog();
                    placeDetails.setFullName(qryname + placeFullName);
                    placeDetails.setLatitude(placeDetails.getLatitude());
                    placeDetails.setLongtitude(placeDetails.getLongtitude());
                    placeDetails.setLatLng(placeDetails.getLatLng());

                    onDone(placeDetails);
                    Log.e("placehere", placeDetails.getFullName());
                } else {
                    Log.d(Constants.LOG_TAG, "Loading Place Details with LatLng for PlaceID = " + placeDetails.getLatLng() + " (" + placeFullName + ") with LatLng = ");
                    fetchPlaceDetailsByLatLngAndProceed(false, placeDetails.getLatLng(), qryname + placeFullName);

//
                }
            }


        });
    }

    public void fetchPlaceDetailsByLatLngAndProceed(final boolean showLoadingDialog, final LatLng latLng, final String placeFullName) {

        if (showLoadingDialog) {
            showLoadingDialog("Fetching place details, please wait...");
        }
        getGooglePlacesAPI().getPlaceDetailsByLatLng(latLng, new GooglePlacesAPI.PlaceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, LatLng location, final String placeID, PlaceDetails placeDetails) {

                if (status == true) {
                    placeDetails.setFullName(placeFullName);
                    placeDetails.setLatitude(placeDetails.getLatitude());
                    placeDetails.setLongtitude(placeDetails.getLongtitude());
                    onDone(placeDetails);
                } else {
                    closeLoadingDialog();
                    showSnackbarMessage("Unable to Fetch Place Details!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            fetchPlaceDetailsByLatLngAndProceed(true, latLng, placeFullName);
                        }
                    });
                }
            }
        });

    }

    public void onDone(final PlaceDetails deliverLocationDetails) {

        if (deliverLocationDetails != null) {

            inputMethodManager.hideSoftInputFromWindow(searchLocation.getWindowToken(), 0);    // Hides Key Board After Item Select..

            if (isForActivityResult == true) {
                String deliver_location_json = new Gson().toJson(deliverLocationDetails);
                Intent placeData = new Intent();
                placeData.putExtra("deliver_location_json", deliver_location_json);
                setResult(Constants.PLACE_SEARCH_ACTIVITY_CODE, placeData);
                Log.e("deliver_location_json", "" + deliverLocationDetails.getLongtitude());
                finish();
            } else {
                if (getResources().getString(R.string.app_type).equalsIgnoreCase("internal")) {
                    showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to continue with selected location <b>" + deliverLocationDetails.getFullAddress() + "</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                checkAvailabilityAtLocationAndProceed(deliverLocationDetails);
                            }
                        }
                    });
                } else {
                    checkAvailabilityAtLocationAndProceed(deliverLocationDetails);
                }
            }
        } else {
            showSnackbarMessage("Delivery location not available.");
        }
    }

    public void checkAvailabilityAtLocationAndProceed(final PlaceDetails placeDetails) {

        inputMethodManager.hideSoftInputFromWindow(searchLocation.getWindowToken(), 0);    // Hides Key Board After Item Select..

        showLoadingDialog("Launching Home, please wait..");
        getBackendAPIs().getGeneralAPI().checkAvailabilityAtLocation(placeDetails.getId(), placeDetails.getLatLng(), new GeneralAPI.CheckAvailabilityAtLocationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation) {

                if (status) {

                    if (responseCheckAvailabilityAtLocation.isAvailable()) {
                        launchHomePage(new AddressBookItemDetails(placeDetails), responseCheckAvailabilityAtLocation);           // LAUNCHING HOME PAGE
                    } else {
                        closeLoadingDialog();
                        showNoServiceDialog(new AddressBookItemDetails(placeDetails), null);
                        //showNotificationDialog("No Service!", Html.fromHtml("We are not serving at the selected location. We are currently serving at <b>" + localStorageData.getServingAt() + "</b>"));
                    }
                } else {
                    closeLoadingDialog();
                    showChoiceSelectionDialog(true, "Error!", "Something went wrong while checking availability at selected location.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                checkAvailabilityAtLocationAndProceed(placeDetails);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE && resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE) {      // If It is from PlaceSearch Activity
                String address_item_json = data.getStringExtra("address_item_json");
                PlaceDetails deliverLocationDetails = new PlaceDetails(new Gson().fromJson(address_item_json, AddressBookItemDetails.class));
                onDone(deliverLocationDetails);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(searchLocation.getWindowToken(), 0);    // Hides Key Board After Item Select..
    }
}