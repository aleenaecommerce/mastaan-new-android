package com.mastaan.buyer.activities;

import android.os.Bundle;
import android.view.MenuItem;

import com.mastaan.buyer.R;
import com.mastaan.buyer.fragments.OrderHistoryFragment;


public class OrderHistoryActivity extends MastaanToolBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        getGoogleAnalyticsMethods().sendScreenName("Order History");       // Send ScreenNames to Analytics

        getSupportFragmentManager().beginTransaction().replace(R.id.ordersListHolder, new OrderHistoryFragment()).commit();

    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }
}
