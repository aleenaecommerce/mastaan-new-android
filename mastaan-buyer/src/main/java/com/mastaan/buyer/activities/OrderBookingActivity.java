package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.SoftKeyBoardActionsCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.widgets.vScrollView;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.OrdersAPI;
import com.mastaan.buyer.backend.models.RequestPlaceOrder;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.flows.GoToPage;
import com.mastaan.buyer.models.BuyerDetails;
import com.mastaan.buyer.models.CartTotalDetails;
import com.mastaan.buyer.models.OrderDetails;

import java.util.List;


public class OrderBookingActivity extends MastaanToolBarActivity implements View.OnClickListener {

    CartTotalDetails cartTotalDetails;
    String transactionID;
    double maxCartPreTime;
    int preOrderItemsCount;

    BuyerDetails sessionBuyerDetails;
    AddressBookItemDetails deliverLocationDetails;

    vScrollView scrollView;

    vTextInputLayout name;
    vTextInputLayout mobile;
    vTextInputLayout alternate_mobile;
    vTextInputLayout email;
    AppCompatAutoCompleteTextView emailAutoCompleteTextview;
    ListArrayAdapter registeredEmailsAdapter;
    vTextInputLayout premise;
    vTextInputLayout sublocality;
    vTextInputLayout landmark;
    vTextInputLayout address;
    vTextInputLayout instructions;
    CheckBox deliverEarlyIfPossible;
    CheckBox billNotRequired;

    Button placeOrder;

    RequestPlaceOrder requestPlaceOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_booking);
        getGoogleAnalyticsMethods().sendScreenName("Order Booking");       // Send ScreenNames to Analytics
        hasLoadingView();

        transactionID = getIntent().getStringExtra("transaction_id");
        maxCartPreTime = (getIntent().getDoubleExtra("maxCartPreTime", 0));
        cartTotalDetails = new Gson().fromJson(getIntent().getStringExtra("cartTotalDetails"), CartTotalDetails.class);
        preOrderItemsCount = (getIntent().getIntExtra("preOrderItemsCount", 0));

        //------------------------------

        scrollView = findViewById(R.id.scrollView);

        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        alternate_mobile = findViewById(R.id.alternate_mobile);
        email = findViewById(R.id.email);
        emailAutoCompleteTextview = (AppCompatAutoCompleteTextView) findViewById(R.id.emailAutoCompleteTextview);
        emailAutoCompleteTextview.setThreshold(1);
        registeredEmailsAdapter = new ListArrayAdapter(context, CommonMethods.getRegisteredEmails(context));
        emailAutoCompleteTextview.setAdapter(registeredEmailsAdapter);

        premise = findViewById(R.id.premise);
        sublocality = findViewById(R.id.sublocality);
        landmark = findViewById(R.id.landmark);
        address = findViewById(R.id.address);

        instructions = findViewById(R.id.instructions);
        instructions.setSoftKeyBoardListener(new SoftKeyBoardActionsCallback() {
            @Override
            public void onDonePressed(String text) {
                placeOrder.performClick();
            }
        });

        deliverEarlyIfPossible = findViewById(R.id.deliverEarlyIfPossible);
        billNotRequired = findViewById(R.id.billNotRequired);

        placeOrder = findViewById(R.id.placeOrder);
        placeOrder.setOnClickListener(this);

        //...........................................

        sessionBuyerDetails = localStorageData.getBuyerDetails();
        deliverLocationDetails = localStorageData.getDeliveryLocation();

        mobile.setText(CommonMethods.removeCountryCodeFromNumber(localStorageData.getBuyerMobile()));
        if ( localStorageData.getBuyerAlternateMobile().equalsIgnoreCase(localStorageData.getBuyerMobile()) == false ) {
            alternate_mobile.setText(CommonMethods.removeCountryCodeFromNumber(localStorageData.getBuyerAlternateMobile()));
        }
        email.setText(localStorageData.getBuyerEmail());
        name.setText(localStorageData.getBuyerName());
        if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") ) {
            if ( mobile.getText().toString().length() > 0 && CommonMethods.isValidPhoneNumber(mobile.getText().toString()) ){
                mobile.getEditText().setFocusable(false);
            }
            if ( email.getText().toString().length() > 0 && CommonMethods.isValidEmailAddress(email.getText().toString()) ){
                email.getEditText().setFocusable(false);
            }
        }

        premise.setText(deliverLocationDetails.getPremise());
        sublocality.setText(deliverLocationDetails.getSublocalityLevel_2());
        landmark.setText(deliverLocationDetails.getLandmark());
        if ( deliverLocationDetails.getLandmark() == null || deliverLocationDetails.getLandmark().length() == 0 ){
            landmark.setText(deliverLocationDetails.getRoute());
        }
        address.setText(CommonMethods.getStringFromStringArray(new String[]{deliverLocationDetails.getSublocalityLevel_1(), deliverLocationDetails.getLocality(), deliverLocationDetails.getState(), deliverLocationDetails.getCountry(), deliverLocationDetails.getPostalCode()}));

    }

    @Override
    public void onClick(View view) {

        if (view == placeOrder) {

            hideSoftKeyboard();

            String oName = name.getText().trim();
            name.checkError("* Enter Name");
            String oEmail = email.getText().trim();
            email.checkError("* Enter Email");
            String oPhone = mobile.getText().trim();
            mobile.checkError("* Enter Phone number");
            String oAlternatePhone = alternate_mobile.getText().trim();
            if ( oPhone.equalsIgnoreCase(oAlternatePhone) ){    oAlternatePhone = "";   }
            alternate_mobile.hideError();

            String oPremise = premise.getText().trim();
            premise.checkError("* Enter Flat No/Appartment name");
            String oSublocality = sublocality.getText().trim();
            sublocality.checkError("* Enter Locality/Colony name");
            String oAddress = address.getText().trim();
            address.checkError("* Enter Address");
            String oLandmark = landmark.getText().trim();
            landmark.checkError("* Enter Landmark");

            String oInstructions = instructions.getText().trim();

            //MOVING SCROLLVIEW TO NOT ENTERED COMPULSARY FIELDS
            vTextInputLayout []compulsaryInputFields = new vTextInputLayout[]{address, landmark, sublocality, premise, mobile, email, name};
            for (int i=0;i<compulsaryInputFields.length;i++){
                if ( compulsaryInputFields[i].getText().length() == 0 ){
                    scrollView.focusToViewTop(compulsaryInputFields[i]);         // FOCUSING VIEW
                }
            }

            if (oName.length() > 0 && oEmail.length() > 0 && oPremise.length() > 0 && oSublocality.length() > 0 && oLandmark.length() > 0 && oAddress.length() > 0 && oPhone.length() > 0) {//&& oSlot.length() > 0 ){
                if (CommonMethods.isValidPhoneNumber(oPhone)) {
                    if ( oAlternatePhone == null || oAlternatePhone.length() == 0 || CommonMethods.isValidPhoneNumber(oAlternatePhone)) {
                        if (CommonMethods.isValidEmailAddress(oEmail) == true) {
                            if ( oPremise.matches(".*\\d+.*") ){
                                deliverLocationDetails.setPremise(oPremise);
                                deliverLocationDetails.setLandmark(oLandmark);
                                deliverLocationDetails.setSublocalityLevel2(oSublocality);

                                requestPlaceOrder = new RequestPlaceOrder(deliverLocationDetails.getID(), oName, oPhone, oAlternatePhone, oEmail, oInstructions, cartTotalDetails.getCouponCode(), /*selectedDate*/"", maxCartPreTime, deliverLocationDetails.getLatLng(), oPremise, oSublocality, deliverLocationDetails.getSublocalityLevel_1(), deliverLocationDetails.getLocality(), oLandmark, oAddress, deliverLocationDetails.getState(), deliverLocationDetails.getCountry(), deliverLocationDetails.getPostalCode(), deliverEarlyIfPossible.isChecked(), billNotRequired.isChecked(), getIntent().getBooleanExtra(Constants.USE_WALLET_BALANCE, false));
                                requestPlaceOrder.setTotal(cartTotalDetails.getTotal());
                                requestPlaceOrder.setTransactionID(transactionID);
                                if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") ) {
                                    requestPlaceOrder.setCreatedByEmployeeID(localStorageData.getInternalAppInitiatedEmployeeID());
                                }
                                //------

                                requestPlaceOrder.setPaymentType(Constants.CASH_ON_DELIVERY_CODE);    //  1 - For Cash on Delivery
                                requestPlaceOrder.setPaymentID("");
                                placeOrder();
                            }
                            else{
                                premise.showError("*Enter flat no/house no");
                                scrollView.focusToViewTop(premise);         // FOCUSING VIEW
                            }
                        } else {
                            email.showError("*Enter a valid email address");
                            scrollView.focusToViewTop(email);         // FOCUSING VIEW
                        }
                    }else{
                        alternate_mobile.showError("*Enter a valid mobile number (10 digits)");
                        scrollView.focusToViewTop(alternate_mobile);         // FOCUSING VIEW
                    }
                } else {
                    mobile.showError("*Enter a valid mobile number (10 digits)");
                    scrollView.focusToViewTop(mobile);         // FOCUSING VIEW
                }
            }else{
                showToastMessage("* Enter all necessary fields");
            }
        }
    }

    public void placeOrder() {

        showLoadingDialog("Placing your order...");
        getBackendAPIs().getOrdersAPI().placeOrder(requestPlaceOrder, new OrdersAPI.PlaceOrderCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final String errorCode, List<OrderDetails> ordersList) {
                if (status) {
                    GoToPage.goToOrderSuccessPage(activity, ordersList);
                }
                else {
                    closeLoadingDialog();
                    checkSessionValidity(statusCode);
                    if ( errorCode != null && errorCode.trim().length() > 0 ){
                        showNotificationDialog(false, "Failure", CommonMethods.fromHtml(message), new ActionCallback() {
                            @Override
                            public void onAction() {
                                if ( errorCode.toLowerCase().contains("stock") || errorCode.toLowerCase().contains("cart") ){
                                    Intent cartAct = new Intent(context, CartActivity.class);
                                    cartAct.putExtra(Constants.RELOAD_DATA, true);
                                    cartAct.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    cartAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(cartAct);
                                    finish();
                                }
                            }
                        });
                    }else{
                        //GoToPage.goToOrderFailurePage(activity, requestPlaceOrder);
                        showSnackbarMessage("Something went wrong, please try again!");
                    }
                }
            }
        });
    }

    //===================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        hideSoftKeyboard();
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if ( name.getText().length() > 0 ){
            localStorageData.setBuyerName(name.getText());
        }
        if ( email.getText().length() > 0 ){
            localStorageData.setBuyerEmail(email.getText());
        }
        if ( mobile.getText().length() > 0 && CommonMethods.isValidPhoneNumber(mobile.getText())){
            localStorageData.setBuyerMobile(mobile.getText());
        }
        if ( alternate_mobile.getText().length() > 0 && CommonMethods.isValidPhoneNumber(alternate_mobile.getText())){
            localStorageData.setBuyerAlternateMobile(alternate_mobile.getText());
        }

        if ( premise.getText().length() > 0 ){
            deliverLocationDetails.setPremise(premise.getText());
        }
        if ( landmark.getText().length() > 0 ){
            deliverLocationDetails.setLandmark(landmark.getText());
        }
        if (sublocality.getText().length() > 0 ){
            deliverLocationDetails.setSublocalityLevel2(sublocality.getText());
        }
        localStorageData.setDeliveryLocation(deliverLocationDetails);
    }

    //------ UI Methods

    public void hideSoftKeyboard() {
        inputMethodManager.hideSoftInputFromWindow(name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(email.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(premise.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(sublocality.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(landmark.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(address.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(mobile.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
    }

}