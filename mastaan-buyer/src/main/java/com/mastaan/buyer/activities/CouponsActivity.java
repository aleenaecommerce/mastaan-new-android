package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.CouponsAdapter;
import com.mastaan.buyer.backend.CouponsAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.CouponDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 14/7/18.
 */

public class CouponsActivity extends MastaanToolBarActivity implements View.OnClickListener {

    EditText coupon_code;
    View applyCoupon;

    View availableCouponsView;
    vRecyclerView couponsHolder;
    CouponsAdapter couponsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons);
        getGoogleAnalyticsMethods().sendScreenName("Coupons");       // Send ScreenNames to Analytics
        hasLoadingView();
        hasSwipeRefresh();

        //--------------

        coupon_code = findViewById(R.id.coupon_code);
        applyCoupon = findViewById(R.id.applyCoupon);
        applyCoupon.setOnClickListener(this);

        availableCouponsView = findViewById(R.id.availableCouponsView);
        couponsHolder = findViewById(R.id.couponsHolder);
        couponsHolder.setNestedScrollingEnabled(false);
        couponsHolder.setupVerticalOrientation();
        couponsAdapter = new CouponsAdapter(context, new ArrayList<CouponDetails>(), new CouponsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                if (getCallingActivity() != null) {
                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.COUPON_CODE, couponsAdapter.getItem(position).getCouponCode());
                    setResult(RESULT_OK, resultData);
                    finish();
                }
            }
        });
        couponsHolder.setAdapter(couponsAdapter);

        //----

        getCoupons();

    }

    @Override
    public void onClick(View view) {
        if (view == applyCoupon) {
            String eCouponCode = coupon_code.getText().toString();

            if (eCouponCode.length() == 0) {
                showSnackbarMessage("* Enter coupon code");
                return;
            }

            Intent resultData = new Intent();
            resultData.putExtra(Constants.COUPON_CODE, eCouponCode);
            setResult(RESULT_OK, resultData);
            finish();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getCoupons();
    }

    public void getCoupons() {
        showLoadingIndicator("Loading...");
        getBackendAPIs().getCouponsAPI().getCoupons(new CouponsAPI.CouponsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<CouponDetails> couponsList) {
                if (status && couponsList.size() > 0) {
                    availableCouponsView.setVisibility(View.VISIBLE);
                    couponsAdapter.setItems(couponsList);
                } else {
                    availableCouponsView.setVisibility(View.GONE);
                }

                switchToContentPage();
            }
        });
    }

    //=========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
