package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.TransactionsAdapter;
import com.mastaan.buyer.backend.WalletAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.TransactionDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 11/03/19.
 */

public class WalletStatementActivity extends MastaanToolBarActivity implements View.OnClickListener{

    TextView month;
    View changeMonth;

    vRecyclerView transactionsHolder;

    TransactionsAdapter transactionsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_statement);
        hasLoadingView();
        hasSwipeRefresh();

        getGoogleAnalyticsMethods().sendScreenName("Wallet Statement");       // Send ScreenNames to Analytics

        //-----------

        month = findViewById(R.id.month);
        changeMonth = findViewById(R.id.changeMonth);
        changeMonth.setOnClickListener(this);

        transactionsHolder = findViewById(R.id.transactionsHolder);
        transactionsHolder.setupVerticalOrientation();
        transactionsAdapter = new TransactionsAdapter(context, new ArrayList<TransactionDetails>(), new TransactionsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                Intent transactionDetailsAct = new Intent(context, TransactionDetailsActivity.class);
                transactionDetailsAct.putExtra(Constants.ID, transactionsAdapter.getItem(position).getID());
                startActivity(transactionDetailsAct);
            }

            @Override
            public void onLinkClick(int position) {
                TransactionDetails transactionDetails = transactionsAdapter.getItem(position);
                if ( transactionDetails.getOrderID() != null ){
                    Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
                    orderDetailsAct.putExtra(Constants.ID, transactionDetails.getOrderID());
                    startActivity(orderDetailsAct);
                }
            }
        });
        transactionsHolder.setAdapter(transactionsAdapter);

        //-----------

        month.setText(DateMethods.getCurrentDate(DateConstants.MMM_YYYY));
        getTransactions(month.getText().toString());

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getTransactions(month.getText().toString());
    }

    @Override
    public void onClick(View view) {
        if ( view == changeMonth ){
            String prefillMonth = month.getText().toString();
            if ( prefillMonth == null || prefillMonth.length() == 0 || prefillMonth.trim().equalsIgnoreCase("month") ){
                prefillMonth = DateMethods.getDateInFormat(localStorageData.getServerTime(), DateConstants.MMM_YYYY);
            }
            showDatePickerDialog("Select month", null, null, prefillMonth, true, false, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int sDay, int sMonth, int sYear) {
                    month.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_YYYY));
                    getTransactions(month.getText().toString());
                }
            });
        }
    }

    public void getTransactions(String forMonth){
        if ( forMonth == null || forMonth.length() == 0 || forMonth.trim().equalsIgnoreCase("month") ){
            showNoDataIndicator("Select month");
            return;
        }

        showLoadingIndicator("Loading...");
        changeMonth.setClickable(false);
        getBackendAPIs().getWalletAPI().getWalletStatement(forMonth, new WalletAPI.WalletStatementCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String month, List<TransactionDetails> transactionsList) {
                changeMonth.setClickable(true);
                if ( status ){
                    if ( transactionsList.size() > 0 ){
                        switchToContentPage();
                        transactionsAdapter.setItems(transactionsList);
                        transactionsHolder.scrollToPosition(0);
                    }else{
                        showNoDataIndicator("No transactions to show");
                    }
                }else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }


    //========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
