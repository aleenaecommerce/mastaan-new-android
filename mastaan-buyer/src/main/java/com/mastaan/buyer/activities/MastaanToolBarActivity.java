package com.mastaan.buyer.activities;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.widgets.vAppCompatEditText;
import com.mastaan.buyer.MastaanApplication;
import com.mastaan.buyer.R;
import com.mastaan.buyer.analytics.GoogleAnalyticsMethods;
import com.mastaan.buyer.backend.BackendAPIs;
import com.mastaan.buyer.backend.models.RequestRegisterForAvailabilityAtLocationUpdates;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.handlers.PermissionsHandler;
import com.mastaan.buyer.interfaces.RegistrationCallback;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.localdata.OrdersFeedbackDatabase;
import com.mastaan.buyer.models.CategoryDetails;
import com.mastaan.buyer.models.DayDetails;
import com.mastaan.buyer.models.MessageDetails;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.payment.razorpay.RazorpayPayment;
import com.mastaan.buyer.support.CrispSupport;
import com.razorpay.ExternalWalletListener;
import com.razorpay.PaymentData;

import java.util.ArrayList;
import java.util.List;


public class MastaanToolBarActivity extends vToolBarActivity implements ExternalWalletListener {

    public Context context;
    MastaanToolBarActivity activity;
    private MastaanApplication mastaanApplication;
    private PermissionsHandler permissionsHandler;
    private GoogleAnalyticsMethods googleAnalyticsMethods;
    TextView customActionBarTitle;

    public LocalStorageData localStorageData;

    //Intent firebaseService;

    RegistrationCallback registrationCallback;

    String MASTAAN_BASE_URL;
    private BackendAPIs backendAPIs;

    private RazorpayPayment razorpayPayment;


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        context = this;
        activity = this;

        //----------------------------------

        try {
            View customActionbarView = inflater.inflate(R.layout.custom_actionbar_layout, null);
            customActionBarTitle = customActionbarView.findViewById(R.id.toolbar_title);
            customActionBarTitle.setText(actionBar.getTitle() + " ");
            actionBar.setTitle("");
            toolbar.addView(customActionbarView);
        }catch (Exception e){}

        //-----------------------------------

        localStorageData = new LocalStorageData(context);
        MASTAAN_BASE_URL = getString(R.string.mastaan_base_url);

        //firebaseService = new Intent(context, FireBaseService.class);

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        mastaanApplication = (MastaanApplication) getApplication();       // Accessing Application Class;
    }

    public MastaanApplication getMastaanApplication() {
        if ( mastaanApplication == null){
            mastaanApplication = (MastaanApplication) getApplication();       // Accessing Application Class;
        }
        return mastaanApplication;
    }

    public PermissionsHandler getPermissionsHandler() {
        if ( permissionsHandler == null ){  permissionsHandler = new PermissionsHandler(activity);  }
        return permissionsHandler;
    }

    public LocalStorageData getLocalStorageData() {
        if ( localStorageData == null ){    localStorageData = new LocalStorageData(context);   }
        return localStorageData;
    }

    public GoogleAnalyticsMethods getGoogleAnalyticsMethods() {
        if ( googleAnalyticsMethods == null ){
            googleAnalyticsMethods = new GoogleAnalyticsMethods(getMastaanApplication().getAnalyticsTracker());
        }
        return googleAnalyticsMethods;
    }

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(this, getDeviceID(), getAppVersionCode());
        }
        return backendAPIs;
    }

    public RazorpayPayment getRazorpayPayment() {
        if ( razorpayPayment == null ){
            razorpayPayment = new RazorpayPayment(activity);
        }
        return razorpayPayment;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Storing Current Activity Context in Application class
        getMastaanApplication().setCurrentActivity(this);
        // Sending current Screen Name to Google Analytics
        //getGoogleAnalyticsMethods().sendScreenName(this.getClass().getSimpleName());

        long differenceTime = DateMethods.getDifferenceToCurrentTime(new LocalStorageData(this).getHomePageLastLoadedTime());
        if ( differenceTime > 600000 ) {     // If greater than 10 mins (600,000 millisec) InActiveTime
            Log.d(Constants.LOG_TAG, "Reloading App as InActive time > 10min");

            Intent launchActivity = new Intent(this, LaunchActivity.class);
            ComponentName componentName = launchActivity.getComponent();
            Intent mainIntent = Intent.makeRestartActivityTask(componentName);
            startActivity(mainIntent);
        }
    }

    @Override
    protected void onPause() {
        try {
            localStorageData.setHomePageLastLoadedTime(DateMethods.getCurrentDateAndTime());
        }catch (Exception e){}

        super.onPause();
    }


    //------------

    public void showNoServiceDialog(final AddressBookItemDetails location, final ActionCallback callback){
        showNotificationDialog("No Service!", Html.fromHtml("We are currently serving at <b>" + localStorageData.getServingAt() + "</b><br>more locations to come..."), new ActionCallback() {
            @Override
            public void onAction() {
                registerForAvailabilityAtLocationUpdatesDialog(location, callback);
            }
        });
    }

    public void registerForAvailabilityAtLocationUpdatesDialog(final AddressBookItemDetails location, final ActionCallback callback){

        View registerForAvailabilityAtLocationUpdatesDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_register_for_availability_at_location_updates, null);
        TextView message = (TextView) registerForAvailabilityAtLocationUpdatesDialogView.findViewById(R.id.message);
        final vAppCompatEditText mobile_or_email = (vAppCompatEditText) registerForAvailabilityAtLocationUpdatesDialogView.findViewById(R.id.mobile_or_email);

        registerForAvailabilityAtLocationUpdatesDialogView.findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(mobile_or_email.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..

                String mobileOrEmail = mobile_or_email.getText().toString();
                mobile_or_email.checkError("* Enter mobile or email");

                if (mobileOrEmail.length() > 0) {
                    if (CommonMethods.isValidPhoneNumber(mobileOrEmail)){
                        closeCustomDialog();
                        registerForAvailabilityAtLocationUpdates(mobileOrEmail, "", location, callback);
                    }
                    else if (CommonMethods.isValidEmailAddress(mobileOrEmail)){
                        closeCustomDialog();
                        registerForAvailabilityAtLocationUpdates("", mobileOrEmail, location, callback);
                    }
                    else{
                        mobile_or_email.showError("* Enter valid mobile or email");
                    }
                }
            }
        });

        registerForAvailabilityAtLocationUpdatesDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(mobile_or_email.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
                closeCustomDialog();
                if ( callback != null ){
                    callback.onAction();
                }
            }
        });

        showCustomDialog(registerForAvailabilityAtLocationUpdatesDialogView, false);
    }

    private void registerForAvailabilityAtLocationUpdates(final String mobile, final String email, final  AddressBookItemDetails location, final ActionCallback callback){

        RequestRegisterForAvailabilityAtLocationUpdates requestRegisterForAvailabilityAtLocationUpdates = new RequestRegisterForAvailabilityAtLocationUpdates(mobile, email, location);

        showLoadingDialog("Registering...");
        getBackendAPIs().getGeneralAPI().registerForAvailabilityAtLocationUpdates(requestRegisterForAvailabilityAtLocationUpdates, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showNotificationDialog(false, "Success!", "Thanks for your interest. We'll get in touch when we launch in your area.", new ActionCallback() {
                        @Override
                        public void onAction() {
                            if ( callback != null ){
                                callback.onAction();
                            }
                        }
                    });
                }else{
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong. Please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            registerForAvailabilityAtLocationUpdates(mobile, email, location, callback);
                        }
                    });
                }
            }
        });
    }

    public void goToRegistration(RegistrationCallback registrationCallback){

        this.registrationCallback = registrationCallback;

        Intent registerAct = new Intent(context, RegisterWithMobileActivity.class);
        startActivityForResult(registerAct, Constants.REGISTER_ACTIVITY_CODE);
    }

    public void setToolBarTitle(String title){
        customActionBarTitle.setText(title+" ");
    }

    public void setThemeColors(String statucBarColor, String toolBarColor){
        try{
            actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(toolBarColor)));
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(Color.parseColor(statucBarColor));
            }
        }catch (Exception e){}
    }

    public void setThemeColors(int statusBarColor, int toolBarColor){
        try{
            actionBar.setBackgroundDrawable(new ColorDrawable(toolBarColor));
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(statusBarColor);
            }
        }catch (Exception e){}
    }

    //===============

    public int getAppCurrentVersion(){
        int currentVersion=0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            currentVersion = pInfo.versionCode;
        }catch (Exception e){}
        return currentVersion;
    }

    //------------------------------------------

    public void checkSessionValidity(int statusCode){
        if ( localStorageData.getSessionFlag() == true && statusCode == 403 ){
            clearUserSession("Your session has expired, please login to continue.");
        }
    }

    /*public void checkMaintainance(int statusCode){
        if (statusCode == 503 ){
            clearUserSession("Under maintainance.");
            Intent loadingAct = new Intent(context, LaunchActivity.class);
            ComponentName componentName = loadingAct.getComponent();
            Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
            startActivity(mainIntent);
        }
    }*/

    public void clearUserSession(String messageToDisplay){

        localStorageData.clearBuyerSession();        // Clearing Session
        /*LocationsDatabase locationsDatabase = new LocationsDatabase(context);
        locationsDatabase.openDatabase();
        locationsDatabase.deleteAllLocations();*/

        // ReInitializing Analytics Tracker with UserID
        getMastaanApplication().initializeAnalyticsTracker();
        googleAnalyticsMethods = new GoogleAnalyticsMethods(getMastaanApplication().getAnalyticsTracker());

        OrdersFeedbackDatabase ordersFeedbackDatabase = new OrdersFeedbackDatabase(context);
        ordersFeedbackDatabase.openDatabase();
        ordersFeedbackDatabase.deleteAllFeedbackItems();
        //ordersFeedbackDatabase.closeDatabase();

        Toast.makeText(context, messageToDisplay, Toast.LENGTH_LONG).show();

        Intent loadingAct = new Intent(context, LaunchActivity.class);
        ComponentName componentName = loadingAct.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
        startActivity(mainIntent);
    }

    //-------------------------

    public void addToPendingOrders(OrderDetails orderDetails){
        if ( orderDetails != null ){
            List<OrderDetails> pendingOrders = localStorageData.getBuyerPendingOrders();
            if ( pendingOrders == null ){
                pendingOrders = new ArrayList<>();
            }
            pendingOrders.add(orderDetails);
            localStorageData.setBuyerPendingOrders(pendingOrders);
        }
    }

    public void deleteFromPendingOrders(final String orderID){

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                if ( orderID != null && orderID.length() > 0 ){
                    boolean isDeleted = false;
                    List<OrderDetails> pendingOrders = localStorageData.getBuyerPendingOrders();
                    for (int i=0;i<pendingOrders.size();i++){
                        OrderDetails pendingOrderDetails = pendingOrders.get(i);
                        if ( pendingOrderDetails.getID().equals(orderID) ){
                            pendingOrders.remove(i);
                            isDeleted = true;
                            break;
                        }
                    }
                    if ( isDeleted == true ){
                        localStorageData.setBuyerPendingOrders(pendingOrders);
                    }
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    //-------------

    public void launchHomePage(){
        launchHomePage(false, null, null);
    }
    public void launchHomePage(final AddressBookItemDetails deliveryLocationDetails, final ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation){
        launchHomePage(false, deliveryLocationDetails, responseCheckAvailabilityAtLocation);
    }
    public void launchHomePage(final boolean showDialog, final AddressBookItemDetails deliveryLocationDetails, final ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                if ( deliveryLocationDetails != null ){ localStorageData.setDeliveryLocation(deliveryLocationDetails);  }

                if ( responseCheckAvailabilityAtLocation != null ) {
                    //localStorageData.storeCategories(responseCheckAvailabilityAtLocation.getCategories());
                    localStorageData.storeNoDeliveryDates(responseCheckAvailabilityAtLocation.getNoDeliveryDates());
                    localStorageData.setDaysToEnableSlots(responseCheckAvailabilityAtLocation.getDaysToEnableSlots());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                isTodayServingDay(new StatusCallback() {
                    @Override
                    public void onComplete(boolean isServingDay, int statusCode, String alertMessage) {
                        if ( isServingDay || DateMethods.compareDates(DateMethods.getTimeIn24HrFormat(localStorageData.getServerTime()), localStorageData.getDeliveringHoursEndTime()) > 0 ){
                            if ( showDialog ) {
                                showLoadingDialog("Launching Home...");
                            }
                            Intent homeAct = new Intent(getApplicationContext(), HomeActivity.class);
                            startActivity(homeAct);
                            finish();
                        }
                        else{
                            showNotificationDialog(false, "Alert!", Html.fromHtml(alertMessage), new ActionCallback() {
                                @Override
                                public void onAction() {
                                    Intent homeAct = new Intent(getApplicationContext(), HomeActivity.class);
                                    startActivity(homeAct);
                                    finish();
                                }
                            });
                        }
                    }
                });
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void isTodayServingDay(final StatusCallback callback){
        new AsyncTask<Void, Void, Void>() {
            boolean isTodayServingDay = true;
            String alertMessage = "";

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    String todaysDate = DateMethods.getOnlyDate(localStorageData.getServerTime());

                    List<DayDetails> noDeliveryDates = localStorageData.getDisabledDates();
                    for (int i=0;i< noDeliveryDates.size();i++){
                        if ( DateMethods.compareDates(todaysDate, noDeliveryDates.get(i).getDate()) == 0 ){
                            isTodayServingDay = false;
                        }
                    }

                    if ( isTodayServingDay == false ) {
                        String message = "";
                        for (int i = 0; i < noDeliveryDates.size(); i++) {
                            DayDetails dayDetails = noDeliveryDates.get(i);
                            if (DateMethods.compareDates(todaysDate, dayDetails.getDate()) == 0) {
                                if (dayDetails.getCategories().size() == 0) {
                                    message = "<b>today.</b> Reason - " + dayDetails.getMessage();
                                } else {
                                    message = "<b>today</b> for the following categories <b>";
                                    for (int j = 0; j < dayDetails.getCategories().size(); j++) {
                                        CategoryDetails categoryDetails = dayDetails.getCategories().get(j);
                                        message += "<br>   " + SpecialCharacters.DOT + " " + CommonMethods.capitalizeFirstLetter(categoryDetails.getName() /*+ " " + categoryDetails.getParentCategoryName()*/);
                                    }
                                    message += "</b><br> Reason - " + dayDetails.getMessage();
                                }
                            }
                        }
                        alertMessage = "We are closed for deliveries " + message + "<br>Apologies for the inconvenience caused.";
                    }
                }catch (Exception e){}
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(isTodayServingDay, 200, alertMessage);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    //-------

    public void openURLInBrowser(String url){
        try {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(browserIntent);
        }catch (Exception e){
            showToastMessage("Something went wrong, try again!");
        }
    }

    //-------

    public void openAppInPlayStore(){
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public void handleMessageClick(MessageDetails messageDetails){
        try{
            if ( messageDetails != null ){
                // APP URL
                if ( messageDetails.getAppURL() != null && messageDetails.getAppURL().trim().length() > 0 ){
                    if ( messageDetails.getAppURL().equalsIgnoreCase(Constants.PROFILE) ){
                        if ( getLocalStorageData().getSessionFlag() ){
                            Intent profileAct = new Intent(context, ProfileActivity.class);
                            startActivity(profileAct);
                        }
                    }
                    else if ( messageDetails.getAppURL().equalsIgnoreCase(Constants.REFERRAL) ){
                        Intent referFriendAct = new Intent(context, ReferFriendActivity.class);
                        startActivity(referFriendAct);
                    }
                    else if ( messageDetails.getAppURL().equalsIgnoreCase(Constants.LOYALTY) ){
                        Intent loyaltyProgramAct = new Intent(context, LoyaltyProgramActivity.class);
                        startActivity(loyaltyProgramAct);
                    }
                    else if ( messageDetails.getAppURL().equalsIgnoreCase(Constants.PLAYSTORE) ){
                        openAppInPlayStore();
                    }
                    else if ( messageDetails.getAppURL().toLowerCase().contains(Constants.MEAT_ITEMS.toLowerCase()) ){
                        String []split = messageDetails.getAppURL().replaceAll(Constants.MEAT_ITEMS+",", "").trim().split(",");
                        if ( split.length == 3 ){
                            Intent meatItemsAct = new Intent(context, MeatItemsActivity.class);
                            meatItemsAct.putExtra(Constants.CATEGORY_ID, split[0]);
                            meatItemsAct.putExtra(Constants.CATEGORY_VALUE, split[1]);
                            meatItemsAct.putExtra(Constants.CATEGORY_NAME, split[2]);
                            startActivityForResult(meatItemsAct, Constants.ITEM_LISTINGS_ACTIVITY_CODE);
                        }
                    }

                    else if ( messageDetails.getAppURL().equalsIgnoreCase(Constants.SETTINGS) ){
                        Intent settingsAct = new Intent(context, SettingsActivity.class);
                        startActivity(settingsAct);
                    }
                    else if ( messageDetails.getAppURL().equalsIgnoreCase(Constants.LEGAL) ){
                        Intent legalAct = new Intent(context, LegalActivity.class);
                        startActivity(legalAct);
                    }
                }
                // URL
                else if ( messageDetails.getURL() != null && messageDetails.getURL().trim().length() > 0 ){
                    if ( messageDetails.getURL() != null && messageDetails.getURL().trim().length() > 0 ){
                        openURLInBrowser(messageDetails.getFormattedURL());
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //============================

    @Override
    public void onExternalWalletSelected(String walletName, PaymentData paymentData) {
        if ( razorpayPayment != null ){
            razorpayPayment.onExternalWalletSelected(walletName, paymentData);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (permissionsHandler != null) { permissionsHandler.onActivityResult(requestCode, resultCode, data);   }

            if ( requestCode == Constants.REGISTER_ACTIVITY_CODE ) {
                if ( resultCode == RESULT_OK ) {
                    // ReInitializing Analytics Tracker with UserID
                    getMastaanApplication().initializeAnalyticsTracker();
                    googleAnalyticsMethods = new GoogleAnalyticsMethods(getMastaanApplication().getAnalyticsTracker());

                    // Updating Freshchat session user details
                    new CrispSupport(context).updateUserSession();

                    if (registrationCallback != null) {
                        registrationCallback.onComplete(true, localStorageData.getAccessToken(), localStorageData.getBuyerDetails());
                    }
                }else{
                    if (registrationCallback != null) {
                        registrationCallback.onComplete(false, null, null);
                    }
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
