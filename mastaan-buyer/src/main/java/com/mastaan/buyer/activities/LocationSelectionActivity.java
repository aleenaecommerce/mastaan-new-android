package com.mastaan.buyer.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.appcompat.widget.SearchView;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vWrappingLinearLayoutManager;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.AddressBookAdapter;
import com.mastaan.buyer.backend.AddressBookAPI;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.fragments.SideBarFragment;

import java.util.ArrayList;
import java.util.List;

public class LocationSelectionActivity extends MastaanNavigationDrawerActivity implements View.OnClickListener{

    String searchQuery;
    MenuItem searchViewMenuITem;
    SearchView searchView;

    boolean isForActivityResult;
    SideBarFragment sideBarFragment;

    vRecyclerView addressBookHolder;
    AddressBookAdapter addressBookAdapter;

    Button selectLocation;

    int selectedItemIndex;

    List<AddressBookItemDetails> originalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selection);
        getGoogleAnalyticsMethods().sendScreenName("Location Selection");       // Send ScreenNames to Analytics
        hasLoadingView(R.color.white);

        sideBarFragment = new SideBarFragment();
        sideBarFragment.setCallback(new SideBarFragment.SideBarCallback() {
            @Override
            public void onItemSelected(int position) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }

            @Override
            public void onDeliverLocationChange(PlaceDetails deliverLocationDetails, ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation) {

            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();
        if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") ) {
            disableLeftNavigationDrawer();
        }

        isForActivityResult = getIntent().getBooleanExtra("isForActivityResult", false);

        if ( isForActivityResult ) {
            disableNavigationDrawer();
        }

        registerActivityReceiver(Constants.LOCATION_SELECTION_RECEIVER);

        //---------

        selectLocation = (Button) findViewById(R.id.selectLocation);
        selectLocation.setOnClickListener(this);

        addressBookHolder = (vRecyclerView) findViewById(R.id.addressBookHolder);
        setupHolder();

        //--------

        showLoadingIndicator("Loading address book...");

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        getLocations();        // Get Favourites
    }

    @Override
    public void onClick(View view) {

        if ( view == selectLocation ){
            Intent mapLocationSelectionAct = new Intent(context, MapLocationSelectionActivity.class);
            mapLocationSelectionAct.putExtra("isForActivityResult", true);
            startActivityForResult(mapLocationSelectionAct, Constants.LOCATION_SELECTOR_ACTIVITY_CODE);
        }
    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);
        try{
            String type = receivedData.getStringExtra(Constants.ACTION_TYPE);
            if ( type.equalsIgnoreCase(Constants.DISPLAY_ADDRESS_BOOK) ){
                getLocations();
            }
        }catch (Exception e){e.printStackTrace();}
    }

    public void setupHolder(){
        addressBookHolder.setLayoutManager(new vWrappingLinearLayoutManager(context));//new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        addressBookHolder.setHasFixedSize(false);
        addressBookHolder.setNestedScrollingEnabled(false);
        addressBookHolder.setItemAnimator(new DefaultItemAnimator());

        addressBookAdapter = new AddressBookAdapter(context, R.layout.view_address_book_item2, new ArrayList<AddressBookItemDetails>(), new AddressBookAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                final AddressBookItemDetails addressBookItemDetails = addressBookAdapter.getItem(position);

                if ( getCallingActivity() != null ){
                    Intent placeData = new Intent();
                    placeData.putExtra("deliver_location_json", new Gson().toJson(new PlaceDetails(addressBookItemDetails)));
                    setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, placeData);
                    finish();
                }else{
                    if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") ) {
                        showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to continue with selected location <b>" + addressBookItemDetails.getFullAddress() + "</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("YES") ){
                                    checkAvailabilityAtLocationAndProceed(addressBookItemDetails);
                                }
                            }
                        });
                    }
                    else{
                        checkAvailabilityAtLocationAndProceed(addressBookItemDetails);
                    }
                }
            }

            @Override
            public void onItemMenuClick(final int position, final View view) {
                //if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") == false ) {
                    selectedItemIndex = position;

                    PopupMenu popup = new PopupMenu(context, view);;//new PopupMenu(getActivity().getSupportActionBar().getThemedContext(), view);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.menu_options, popup.getMenu());
                    popup.show();

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {

                            if ( menuItem.getItemId() == R.id.action_edit ) {
                                Intent addFavActivity = new Intent(context, FavouriteActivity.class);
                                addFavActivity.putExtra("mode", "Edit");
                                addFavActivity.putExtra("address_book_item_json", new Gson().toJson(addressBookAdapter.getItem(position)));
                                startActivityForResult(addFavActivity, Constants.FAVOURITE_ACTIVITY_CODE);
                            }
                            else if ( menuItem.getItemId() == R.id.action_delete ) {
                                deleteFavourite(position, addressBookAdapter.getItem(position));
                            }
                            return true;
                        }
                    });
                //}
            }
        });
        addressBookHolder.setAdapter(addressBookAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getLocations();
    }

    public void getLocations(){

        showLoadingIndicator("Loading your addresses...");
        getBackendAPIs().getAddressBookAPI().getLocationHistory(new AddressBookAPI.FavouritesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, final List<AddressBookItemDetails> addressBookItems) {
                if (status) {
                    if ( addressBookItems.size() > 0 ) {
                        switchToContentPage();
                        showSearchView();
                        addressBookAdapter.setItems(addressBookItems);
                        originalList = addressBookAdapter.getAllItems();
                    }else{
                        showNoDataIndicator("No addresses to show");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load your addresses, try again!");
                }
            }
        });
    }

    public void deleteFavourite(final int position, final AddressBookItemDetails  addressBookItemDetails){

        showLoadingDialog("Deleting address, please wait...");
        getBackendAPIs().getAddressBookAPI().deleteFavourie(addressBookItemDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    //REMOVING FROM ADAPTER
                    addressBookAdapter.deleteItem(position);

                    //REMOVING FROM ORIGINAL LIST
                    for (int i=0;i<originalList.size();i++){
                        if ( originalList.get(i).getID().equals(addressBookItemDetails.getID())){
                            originalList.remove(i);
                            break;
                        }
                    }

                    //
                    if ( addressBookAdapter.getItemCount() == 0 ){
                        localStorageData.setHasLocationHistory(false);
                    }
                    showSnackbarMessage("Address deleted successfully", null, null);
                }
                else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to delete address , try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            deleteFavourite(position, addressBookItemDetails);
                        }
                    });
                }
            }
        });
    }

    public void checkAvailabilityAtLocationAndProceed(final AddressBookItemDetails placeDetails){

        showLoadingDialog("Launching home...");
        getBackendAPIs().getGeneralAPI().checkAvailabilityAtLocation(placeDetails.getID(), placeDetails.getLatLng(), new GeneralAPI.CheckAvailabilityAtLocationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation) {

                if (status) {
                    if (responseCheckAvailabilityAtLocation.isAvailable()) {

                        if (isForActivityResult == true) {
                            Intent placeData = new Intent();
                            placeData.putExtra("deliver_location_json", new Gson().toJson(new PlaceDetails(placeDetails)));
                            placeData.putExtra(Constants.RESPONSE_CHECK_AVAILABILITY_AT_LOCATION, new Gson().toJson(responseCheckAvailabilityAtLocation));
                            setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, placeData);
                            finish();
                        }
                        else {
                            launchHomePage(placeDetails, responseCheckAvailabilityAtLocation);           // LAUNCHING HOME PAGE
                        }
                    } else {
                        closeLoadingDialog();
                        showNoServiceDialog(placeDetails, null);
                    }
                } else {
                    closeLoadingDialog();
                    showSnackbarMessage("Something went wrong while checking availability at selected location.", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            checkAvailabilityAtLocationAndProceed(placeDetails);
                        }
                    });
                }
            }
        });
    }

    //----------------

    public void onPerformSearch(String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchQuery = search_query.toLowerCase();

            List<AddressBookItemDetails> filteredList = new ArrayList<>();
            for (int i=0;i<originalList.size();i++){
                AddressBookItemDetails addressBookItemDetails = originalList.get(i);
                if ( addressBookItemDetails.getFullAddress().toLowerCase().contains(searchQuery) || addressBookItemDetails.getTitle().toLowerCase().contains(searchQuery) ){
                    filteredList.add(originalList.get(i));
                }
            }

            if ( filteredList.size() > 0 ){
                addressBookAdapter.setItems(filteredList);
                switchToContentPage();
            }else{
                showNoDataIndicator("Nothing found for ("+searchQuery+")");
            }
        }
    }

    public void onClearSearch(){
        if ( searchQuery != null && searchQuery.length() > 0 ) {
            setupHolder();
            addressBookAdapter.setItems(originalList);
            if (addressBookAdapter.getItemsCount() > 0) {
                switchToContentPage();
            } else {
                showNoDataIndicator("No items");
            }
        }
    }

    //============

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE && resultCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE) {      // If It is from PlaceSearch Activity
                if ( getCallingActivity() != null ){
                    setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, data);
                    finish();
                }else{
                    String deliver_location_json = data.getStringExtra("deliver_location_json");
                    PlaceDetails deliveryLocation = new Gson().fromJson(deliver_location_json, PlaceDetails.class);
                    /*String response_check_availability_json = data.getStringExtra(Constants.RESPONSE_CHECK_AVAILABILITY_AT_LOCATION);

                    if (deliveryLocation != null) {

                        if (isForActivityResult) {
                            Intent placeData = new Intent();
                            placeData.putExtra("deliver_location_json", deliver_location_json);
                            placeData.putExtra(Constants.RESPONSE_CHECK_AVAILABILITY_AT_LOCATION, response_check_availability_json);
                            setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, placeData);
                            finish();
                        } else {
                            launchHomePage(true, new AddressBookItemDetails(deliveryLocation), new Gson().fromJson(response_check_availability_json, ResponseCheckAvailabilityAtLocation.class));           // LAUNCHING HOME PAGE
                        }
                    }*/
                    checkAvailabilityAtLocationAndProceed(new AddressBookItemDetails(deliveryLocation));
                }
            }
            else if (requestCode == Constants.FAVOURITE_ACTIVITY_CODE && resultCode == Constants.FAVOURITE_ACTIVITY_CODE) {
                /*
                // NOT NEEDED AS ACTIVITY RECEIVER DOING THIS WORK
                String address_book_item_json = data.getStringExtra("address_book_item_json");
                AddressBookItemDetails addressBookItemDetails = new Gson().fromJson(address_book_item_json, AddressBookItemDetails.class);

                if ( addressBookItemDetails != null ){
                    addressBookItemDetails.setType("favourite");
                    addressBookAdapter.updateItem(selectedItemIndex, addressBookItemDetails);
                    showSnackbarMessage("Favourite Location (" + addressBookItemDetails.getTitle() + ") Updated.", null, null);
                }*/
            }
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    public void showSearchView(){
        try{    searchViewMenuITem.setVisible(true); }catch (Exception e){}
    }

    public void hideSearchView(){
        try{    searchViewMenuITem.setVisible(false); }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){

        }
        else {
            if ( isForActivityResult == true ){
                onBackPressed();
            }else{
                if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") == false ) {
                    if (drawerListener.onOptionsItemSelected(item)) {
                        return true;
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
