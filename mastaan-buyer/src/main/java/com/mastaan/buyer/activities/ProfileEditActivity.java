package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.models.RequestProfileUpdate;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.BuyerDetails;
import com.mastaan.buyer.support.CrispSupport;


public class ProfileEditActivity extends MastaanToolBarActivity implements View.OnClickListener{

    vTextInputLayout user_name, user_email;//, user_phone, user_dob, user_anniversary;
    //CheckBox marital_status;

    View doneEdit;

    BuyerDetails buyerDetails;

    RequestProfileUpdate requestProfileUpdate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        getGoogleAnalyticsMethods().sendScreenName("Edit Profile");       // Send ScreenNames to Analytics

        String buyer_details_json = getIntent().getStringExtra("buyer_details_json");
        buyerDetails = new Gson().fromJson(buyer_details_json, BuyerDetails.class);

        //................................................

        user_name = (vTextInputLayout) findViewById(R.id.user_name);
        user_email = (vTextInputLayout) findViewById(R.id.user_email);
        /*user_phone = (vTextInputLayout) findViewById(R.id.user_phone);
        user_dob = (vTextInputLayout) findViewById(R.id.user_dob);
        user_dob.getEditText().setOnClickListener(this);
        marital_status = (CheckBox) findViewById(R.id.marital_status);
        user_anniversary = (vTextInputLayout) findViewById(R.id.user_anniversary);
        user_anniversary.getEditText().setOnClickListener(this);*/

        user_name.setText(buyerDetails.getName());
        user_email.setText(buyerDetails.getEmail());

        /*user_dob.setText(DateMethods.getDateInFormat(buyerDetails.getDOB(), DateConstants.MMM_DD));
        if ( buyerDetails.getMatrialStatus() == true ){
            user_anniversary.setVisibility(View.VISIBLE);
            user_anniversary.setText(DateMethods.getDateInFormat(buyerDetails.getAnniversary(), DateConstants.MMM_DD));
            marital_status.setChecked(true);
        }else{
            user_anniversary.setVisibility(View.GONE);
        }*/

        doneEdit = findViewById(R.id.doneEdit);
        doneEdit.setOnClickListener(this);
        doneEdit.startAnimation(fabEnterAnim);

        /*marital_status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if ( isChecked ){
                    user_anniversary.setVisibility(View.VISIBLE);
                    user_anniversary.hideError();
                }else{
                    user_anniversary.setText("");
                    user_anniversary.setVisibility(View.GONE);
                }
            }
        });*/

    }

    @Override
    public void onClick(View view) {

        /*if ( view == user_dob.getEditText() ){
            String userDOB = user_dob.getText().toString();
            showDatePickerDialog("Select Date", userDOB+", 2014", false, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    user_dob.setText(months[month - 1] + " " + String.format("%02d", day));
                }
            });
        }

        else if ( view == user_anniversary.getEditText() ){
            String userAnniversary = user_anniversary.getText().toString();
            showDatePickerDialog("Select Date", userAnniversary+", 2014", false, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    user_anniversary.setText(months[month-1]+" "+ String.format("%02d", day));
                }
            });
        }

        else */if ( view == doneEdit ){

            boolean validationStatus = true;
            //user_name.checkError("*Enter Name");
            String userName = user_name.getText();
            //user_phone.checkError("*Enter Phone number");
            //String userPhone = user_phone.getText();
            //user_email.checkError("*Enter an Email address");
            String userEmail = user_email.getText().trim();
            /*String userDOB = user_dob.getText().toString();
            boolean matrialStatus = marital_status.isChecked();
            String userAnniversary = user_anniversary.getText().toString();*/

            /*if ( matrialStatus == false ){  userAnniversary = "";   }
            if ( matrialStatus == true && (userAnniversary == null || userAnniversary.length() == 0 ) ){
                user_anniversary.showError("*Enter Anniversary date");
                validationStatus = false;
            }*/

            if ( userEmail.length() > 0 ){
                if ( CommonMethods.isValidEmailAddress(userEmail) == false ) {
                    user_email.showError("*Enter a valid email address");
                    validationStatus = false;
                }
            }

            if ( validationStatus == true ){
                requestProfileUpdate = new RequestProfileUpdate(userName, null, userEmail, buyerDetails.getMobile(), null, false, null);//userDOB, matrialStatus, userAnniversary);
                updateProfile();
            }
        }
    }

    public void updateProfile(){

        getBackendAPIs().getProfileAPI().updateProfile(requestProfileUpdate, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {

                if (status == true) {

                    buyerDetails.setEmail(requestProfileUpdate.getEmail());
                    buyerDetails.setName(requestProfileUpdate.getName());
                    buyerDetails.setDOB(requestProfileUpdate.getDOB());
                    buyerDetails.setMatrialStatus(requestProfileUpdate.getMatrialStaus());
                    buyerDetails.setAnniversary(requestProfileUpdate.getAnniversary());

                    localStorageData.setBuyerDetails(buyerDetails);

                    // Updating Freshchat session user details
                    new CrispSupport(context).updateUserSession();

                    Intent modifiedData = new Intent();
                    modifiedData.putExtra("profile_data_json", new Gson().toJson(buyerDetails));
                    setResult(Constants.PROFILE_ACTIVITY_CODE, modifiedData);
                    finish();//finishing activity

                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage(doneEdit, "Unable to update profile", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateProfile();
                        }
                    });
                }
            }
        });
    }

    //============

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        inputMethodManager.hideSoftInputFromWindow(user_name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(user_name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        onBackPressed();
        return true;
    }


}
