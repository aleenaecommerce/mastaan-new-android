package com.mastaan.buyer.activities;

import android.os.Bundle;
import android.os.PersistableBundle;

import com.mastaan.buyer.R;

/**
 * Created in Mastaan Buyer on 27/08/18.
 * @author Venkatesh Uppu
 */

public class CrispActivity extends MastaanToolBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crisp);

    }

}
