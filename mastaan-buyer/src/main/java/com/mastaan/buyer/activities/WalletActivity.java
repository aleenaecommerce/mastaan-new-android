package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.WalletAPI;
import com.mastaan.buyer.models.WalletDetails;

/**
 * Created by Venkatesh Uppu on 11/03/19.
 */

public class WalletActivity extends MastaanToolBarActivity implements View.OnClickListener{

    TextView balance;
    View showStatement;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        hasLoadingView();
        hasSwipeRefresh();

        getGoogleAnalyticsMethods().sendScreenName("Wallet");       // Send ScreenNames to Analytics

        //-----------

        balance = findViewById(R.id.balance);
        showStatement = findViewById(R.id.showStatement);
        showStatement.setOnClickListener(this);

        //-----------

        getWalletDetails();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getWalletDetails();
    }

    @Override
    public void onClick(View view) {
        if ( view == showStatement ){
            Intent walletStatementAct = new Intent(context, WalletStatementActivity.class);
            startActivity(walletStatementAct);
        }
    }

    public void getWalletDetails(){
        showLoadingIndicator("Loading...");
        getBackendAPIs().getWalletAPI().getWalletDetails(new WalletAPI.WalletCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, WalletDetails walletDetails) {
                if ( status ){
                    switchToContentPage();
                    balance.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(walletDetails.getBalance()));
                }else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }


    //======

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
