package com.mastaan.buyer.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.OrdersHistoryAdapter;
import com.mastaan.buyer.backend.OrdersAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;
import com.mastaan.buyer.models.OrderDetails;

import java.util.ArrayList;
import java.util.List;


public class PendingOrdersActivity extends MastaanToolBarActivity implements View.OnClickListener{

    Button call_us;

    vRecyclerView ordersListHolder;
    OrdersHistoryAdapter ordersListAdapter;
    OrdersHistoryAdapter.CallBack ordersListCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_orders);
        getGoogleAnalyticsMethods().sendScreenName("Pending Orders");       // Send ScreenNames to Analytics
        hasLoadingView();
        //hasSwipeRefresh();

        //-------------------------------------

        call_us = findViewById(R.id.call_us);
        call_us.setOnClickListener(this);
        call_us.setText("Tap to call " + localStorageData.getSupportContactNumber() + " for order status/cancellation");

        ordersListHolder = (vRecyclerView) findViewById(R.id.ordersListHolder);
        setupOrderListHolder();

        //------------------

        getPendingOrders();

    }

    @Override
    public void onClick(View view) {

        if ( view == call_us ){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
            startActivity(intent);
        }
    }

    public void setupOrderListHolder(){

//        ordersListHolder.setLayoutManager(new vWrappingLinearLayoutManager(context));//new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
//        ordersListHolder.setHasFixedSize(false);
//        ordersListHolder.setNestedScrollingEnabled(false);
//        ordersListHolder.setItemAnimator(new DefaultItemAnimator());

        ordersListHolder.setHasFixedSize(true);
        ordersListHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        ordersListHolder.setItemAnimator(new DefaultItemAnimator());

        ordersListCallback = new OrdersHistoryAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
                orderDetailsAct.putExtra(Constants.ID, ordersListAdapter.getOrderItem(position).getID());
                startActivity(orderDetailsAct);
            }
        };

        ordersListAdapter = new OrdersHistoryAdapter(context, new ArrayList<OrderDetails>(), ordersListCallback);
        ordersListHolder.setAdapter(ordersListAdapter);

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPendingOrders();      // GetMyPendingOrders
    }

    public void getPendingOrders(){

        ordersListAdapter.clearItems();

        showLoadingIndicator("Loading your pending orders, wait..");
        getBackendAPIs().getOrdersAPI().getPendingOrders(new OrdersAPI.PendingOrdersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<OrderDetails> ordersList) {

                if (status == true) {
                    if (ordersList.size() > 0) {
                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                localStorageData.setBuyerPendingOrders(ordersList);
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                new BroadcastReceiversMethods(context).displayUserSessionInfo();
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        ordersListAdapter.addItems(ordersList);
                        switchToContentPage();

                    } else {
                        showNoDataIndicator("No Pending orders!!! ");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load your pending orders, try again!");
                }
            }
        });
    }

    //==============

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }
}