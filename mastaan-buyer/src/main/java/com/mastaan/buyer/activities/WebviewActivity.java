package com.mastaan.buyer.activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mastaan.buyer.R;
import com.mastaan.buyer.constants.Constants;


public class WebviewActivity extends MastaanToolBarActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        hasLoadingView();

        setToolBarTitle(getIntent().getStringExtra(Constants.TITLE)!=null?getIntent().getStringExtra(Constants.TITLE):"");

        if ( getIntent().getStringExtra(Constants.ANALYTICS_TITLE) != null && getIntent().getStringExtra(Constants.ANALYTICS_TITLE).trim().length() > 0 ){
            getGoogleAnalyticsMethods().sendScreenName(getIntent().getStringExtra(Constants.ANALYTICS_TITLE));   // Send ScreenNames to Analytics
        }

        try{
            if ( getIntent().getStringExtra(Constants.COLOR_PRIMARY) != null  ){
                setThemeColors(Color.parseColor(getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK)!=null?getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK):getIntent().getStringExtra(Constants.COLOR_PRIMARY))
                        , Color.parseColor(getIntent().getStringExtra(Constants.COLOR_PRIMARY))
                );
            }
        }catch (Exception e){}

        //--------

        webView = findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebViewClient(new WebViewClient());
        //webView.setWebContentsDebuggingEnabled(true);

        webView.setWebChromeClient(new WebChromeClient(){
            boolean loadedOnce;
            boolean loadedOtherURL;
            boolean clearedHistory;
            boolean ignoreBlank;
            public void onProgressChanged(WebView view, int progress) {
                showLoadingIndicator("Loading...");
                if( progress == 100) {
                    switchToContentPage();

                    //Log.d(Constants.LOG_TAG, "=========> "+webView.getUrl());
                    // Handling back press for webview loaded first time with html content
                    try{
                        if ( ignoreBlank ){
                            ignoreBlank = false;
                        }
                        else{
                            if ( webView.getUrl().equalsIgnoreCase("about:blank") ){
                                if ( clearedHistory ){  finish();   }
                                if ( loadedOnce ){
                                    if ( loadedOtherURL ){
                                        webView.clearHistory();
                                        clearedHistory = true;
                                        ignoreBlank = true;
                                        webView.loadDataWithBaseURL("file:///android_asset/", getIntent().getStringExtra(Constants.CONTENT), "text/html", "UTF-8", null);
                                    }
                                }
                                loadedOtherURL = false;
                            }else{
                                loadedOtherURL = true;
                                clearedHistory = false;
                            }
                        }
                    }catch (Exception e){}
                    loadedOnce = true;
                }
            }
        });

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView webView, String url) {
                try {
                    webView.loadUrl("javascript:(function(){ document.body.style.padding = '12px' })();");
                }catch (Exception e){}
            }
        });

        /*if ( getIntent().getBooleanExtra(Constants.FORCE_DESKTOP_VERSION, false) ) {
            webView.getSettings().setUserAgentString("Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0");
        }*/

        if ( getIntent().getStringExtra(Constants.URL) != null && getIntent().getStringExtra(Constants.URL).trim().length() > 0 ){
            String linkURL = getIntent().getStringExtra(Constants.URL);
            if ( linkURL.toLowerCase().contains("http://") == false && linkURL.toLowerCase().contains("https://") == false ){
                linkURL = "http://"+linkURL;
            }
            webView.loadUrl(linkURL); // its working
        }
        else if ( getIntent().getStringExtra(Constants.CONTENT) != null && getIntent().getStringExtra(Constants.CONTENT).trim().length() > 0 ){
            webView.loadDataWithBaseURL("file:///android_asset/", getIntent().getStringExtra(Constants.CONTENT), "text/html", "UTF-8", null);
        }

    }

    //=========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if ( webView.canGoBack() ){
            webView.goBack();
        }else{
            super.onBackPressed();
        }
    }

}

