package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.widgets.vSwitch;
import com.mastaan.buyer.R;
import com.mastaan.buyer.support.CrispSupport;


public class SettingsActivity extends MastaanToolBarActivity implements View.OnClickListener{

    LinearLayout notifications, credits, about;
    LinearLayout logoutView, logout;

    TextView app_version;
    vSwitch notifications_switch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_settings);
        getGoogleAnalyticsMethods().sendScreenName("Settings");       // Send ScreenNames to Analytics

        notifications = findViewById(R.id.notifications);
        notifications.setOnClickListener(this);
        notifications_switch = findViewById(R.id.notifications_switch);
        credits = findViewById(R.id.credits);
        credits.setOnClickListener(this);
        about = findViewById(R.id.credits);
        about.setOnClickListener(this);
        app_version = findViewById(R.id.app_version);
        app_version.setText("Version "+getAppVersionName());

        logoutView = findViewById(R.id.logoutView);
        logout =  findViewById(R.id.logout);

        if ( localStorageData.getSessionFlag() ){
            logoutView.setVisibility(View.VISIBLE);
            logout.setOnClickListener(this);
        }

        if ( localStorageData.getSessionFlag() == true ){
            notifications.setVisibility(View.VISIBLE);
            notifications_switch.setChecked(localStorageData.getNotificationsFlag());

            notifications_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, final boolean isChecked) {
                    showLoadingDialog("Updating your preference...");
                    getBackendAPIs().getProfileAPI().updatePromotionalAlertsPreference(isChecked, new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            closeLoadingDialog();
                            if ( status ){
                                localStorageData.setNotificationsFlag(isChecked);
                            }else{
                                showToastMessage("Something went wrong, try again!");
                                notifications_switch.setCheckedWithoutCallback(!isChecked);
                            }
                        }
                    });
                }
            });
        }

    }

    @Override
    public void onClick(View view) {

        if ( view == notifications ){
            if ( notifications_switch.isChecked() ){
                notifications_switch.setChecked(false);
            }else{
                notifications_switch.setChecked(true);
            }
        }
        else if ( view == credits ){
            Intent creditsAct = new Intent(context, CreditsActivity.class);
            startActivity(creditsAct);
        }
        else if ( view == about ){

        }

        else if ( view == logout ){
            if ( localStorageData.getCartCount() > 0 ) {
                showChoiceSelectionDialog(false, "Alert!", "Items you added to your food basket will be removed if you log out. Continue?", "YES", "NO", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        closeLoadingDialog();
                        if (choiceName.equalsIgnoreCase("YES")) {
                            getBackendAPIs().getCartAPI().clearCart(new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                }
                            });
                            logOut();
                        }
                    }
                });
            }else{
                logOut();
            }
        }

    }

    public void logOut(){

        showLoadingDialog("Logging out, wait..");
        getBackendAPIs().getAuthenticationAPI().confirmLogout(localStorageData.getAccessToken(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();

                // Clearing Freshchat session user details
                new CrispSupport(context).clearUserSession();

                if (status == true) {
                    clearUserSession("Logged out successfully.");
                    //updateSidebarUIBasedOnSessioin();          // Updating Sidebar
                } else {
                    clearUserSession("Logged out.");
                    showSnackbarMessage("Unable to Logout!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            logOut();
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
