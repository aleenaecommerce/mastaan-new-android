package com.mastaan.buyer.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.aleena.common.widgets.vWebViewProgressIndicator;
import com.mastaan.buyer.R;

public class TermsAndConditionsActivity extends MastaanToolBarActivity {//} implements View.OnClickListener{

    //Button decline;
    //Button accept;

    WebView webview_terms_and_conditions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        getGoogleAnalyticsMethods().sendScreenName("Terms and Conditions");       // Send ScreenNames to Analytics
        hasLoadingView();

        //actionBar.setHomeButtonEnabled(false);
        //actionBar.setDisplayHomeAsUpEnabled(false);

        //-----------------

        /*decline = (Button) findViewById(R.id.decline);
        decline.setOnClickListener(this);
        accept = (Button) findViewById(R.id.accept);
        accept.setOnClickListener(this);*/

        webview_terms_and_conditions = (WebView) findViewById(R.id.webview_terms_and_conditions);

        //------------------

        webview_terms_and_conditions.getSettings().setJavaScriptEnabled(true);
        webview_terms_and_conditions.setWebChromeClient(new vWebViewProgressIndicator(new vWebViewProgressIndicator.CallBack() {
            @Override
            public void onLoading(int progress) {
                if (progress == 100) {
                    switchToContentPage();
                }
            }
        }));

        webview_terms_and_conditions.loadUrl("file:///android_asset/html/MastaanTermsAndConditions.html");//"file://html/MastaanTermsAndConditions.html");
        showLoadingIndicator("Loading Mastaan Terms & Conditions, wait..");

    }

    /*@Override
    public void onClick(View view) {

        if ( view == accept ){
            localStorageData.setTCFlag(true);
            setResult(RESULT_OK);
            finish();
        }
        else if ( view == decline ){
            onBackPressed();
        }
    }*/

    //=========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
