package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vCheckBox;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.CartItemsAdapter;
import com.mastaan.buyer.backend.CartAPI;
import com.mastaan.buyer.backend.CouponsAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;
import com.mastaan.buyer.methods.GroupingMethods;
import com.mastaan.buyer.models.CartItemDetails;
import com.mastaan.buyer.models.CartTotalDetails;
import com.mastaan.buyer.models.GlobalItemDetails;
import com.mastaan.buyer.models.GroupedCartItems;
import com.mastaan.buyer.models.WalletDetails;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CartActivity extends MastaanToolBarActivity implements View.OnClickListener {

    int selectedCartItemIndex = -1;
    CartItemDetails selectedCartItemDetails;

    vRecyclerView cartItemsHolder;
    CartItemsAdapter cartItemsAdapter;

    TextView sub_total;
    TextView delivery_charges;
    TextView delivery_charges_message;
    TextView service_charges;
    View surChargesDetails;
    TextView surcharges;
    TextView government_taxes;
    TextView total_amount;
    TextView total_amount_message;

    View addCoupon;
    TextView coupon_code;
    TextView coupon_discount;
    Button removeCoupon;
    vCheckBox useWalletBalance;
    Button checkOut;
    CartTotalDetails cartTotalDetails;
    WalletDetails walletDetails;
    String transactionID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_cart);
        hasLoadingView();
        hasSwipeRefresh();

        getGoogleAnalyticsMethods().sendScreenName("Cart");       // Send ScreenNames to Analytics

        //------------------

        sub_total = findViewById(R.id.sub_total);
        service_charges = findViewById(R.id.service_charges);
        delivery_charges = findViewById(R.id.delivery_charges);
        delivery_charges_message = findViewById(R.id.delivery_charges_message);
        surChargesDetails = findViewById(R.id.surChargesDetails);
        surcharges = findViewById(R.id.surcharges);
        government_taxes = findViewById(R.id.government_taxes);
        total_amount = findViewById(R.id.total_amount);
        total_amount_message = findViewById(R.id.total_amount_message);

        addCoupon = findViewById(R.id.addCoupon);
        addCoupon.setOnClickListener(this);
        coupon_code = findViewById(R.id.coupon_code);
        coupon_discount = findViewById(R.id.coupon_discount);
        removeCoupon = findViewById(R.id.removeCoupon);
        removeCoupon.setOnClickListener(this);

        useWalletBalance = findViewById(R.id.useWalletBalance);
        useWalletBalance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                displayCheckoutAmount();
            }
        });

        checkOut = findViewById(R.id.checkOut);
        checkOut.setOnClickListener(this);

        cartItemsHolder = findViewById(R.id.cartItemsHolder);
        setupHolder();

        //-----------

        getCart();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getBooleanExtra(Constants.RELOAD_DATA, false)) {
            onReloadPressed();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getCart();
    }

    @Override
    public void onClick(View view) {

        if (view == addCoupon) {
            if (removeCoupon.getVisibility() != View.VISIBLE) {
                Intent couponsAct = new Intent(context, CouponsActivity.class);
                startActivityForResult(couponsAct, Constants.COUPONS_ACTIVITY_CODE);
            }
        } else if (view == removeCoupon) {
            removeCoupon();
        } else if (view == checkOut) {
            checkoutCart();
        }

    }

    public void setupHolder() {

        //cartItemsHolder.setHasFixedSize(true);
        /*cartItemsHolder.setLayoutManager(new vWrappingLinearLayoutManager(context));*/
        //cartItemsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        //cartItemsHolder.setItemAnimator(new DefaultItemAnimator());

        cartItemsHolder.setupVerticalOrientation();
        cartItemsHolder.setNestedScrollingEnabled(false);
        cartItemsAdapter = new CartItemsAdapter(context, new ArrayList<GlobalItemDetails>(), new CartItemsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                selectedCartItemIndex = position;
                selectedCartItemDetails = cartItemsAdapter.getItem(position).getCartItemDetails();

                Intent editCartItemAct = new Intent(context, AddorEditCartItemActivity.class);
                editCartItemAct.putExtra("type", "Update");
                editCartItemAct.putExtra(Constants.CATEGORY_ID, selectedCartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getCategoryID());
                editCartItemAct.putExtra(Constants.CATEGORY_VALUE, selectedCartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getCategoryValue());
                editCartItemAct.putExtra("meatItemName", selectedCartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getNameWithCategoryAndSize());
                editCartItemAct.putExtra("cartItemDetails", new Gson().toJson(selectedCartItemDetails));
                if (selectedCartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getCategoryDetails() != null) {
                    editCartItemAct.putExtra(Constants.COLOR_PRIMARY, selectedCartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getCategoryDetails().getBackgroundColor());
                    editCartItemAct.putExtra(Constants.COLOR_PRIMARY_DARK, selectedCartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getCategoryDetails().getStatusBarColor());
                }
                startActivityForResult(editCartItemAct, Constants.UPDATE_CART_ITEM_ACTIVITY_CODE);
            }

            @Override
            public void onAddOrUpdateSpecialInstructions(final int position) {
                CartItemDetails cartItemDetails = cartItemsAdapter.getItem(position).getCartItemDetails();
                showInputTextDialog(cartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getName()
                        , "Special instructions", cartItemDetails.getSpecialInstructions(), false, new TextInputCallback() {
                            @Override
                            public void onComplete(String inputText) {
                                selectedCartItemIndex = position;
                                selectedCartItemDetails = cartItemsAdapter.getItem(position).getCartItemDetails();
                                updateCartItemSpecialInstructions(inputText);
                            }
                        });
            }
            @Override
            public void onItemMenuClick(int position, View view) {
                selectedCartItemIndex = position;
                selectedCartItemDetails = cartItemsAdapter.getItem(position).getCartItemDetails();

                PopupMenu popup = new PopupMenu(getSupportActionBar().getThemedContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_options, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getItemId() == R.id.action_edit) {
                            Intent addorEditCartItemActivity = new Intent(context, AddorEditCartItemActivity.class);
                            addorEditCartItemActivity.putExtra("type", "Update");
                            addorEditCartItemActivity.putExtra(Constants.CATEGORY_ID, selectedCartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getCategoryID());
                            addorEditCartItemActivity.putExtra(Constants.CATEGORY_VALUE, selectedCartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getCategoryValue());
                            addorEditCartItemActivity.putExtra("meatItemName", selectedCartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getNameWithCategoryAndSize());
                            addorEditCartItemActivity.putExtra("cartItemDetails", new Gson().toJson(selectedCartItemDetails));
                            startActivityForResult(addorEditCartItemActivity, Constants.UPDATE_CART_ITEM_ACTIVITY_CODE);
                        } else if (item.getItemId() == R.id.action_delete) {
                            deleteCartItem();
                        }

                        return true;
                    }
                });
            }
        });
        cartItemsHolder.setAdapter(cartItemsAdapter);
    }

    public void displayCart(final List<CartItemDetails> cartItemsList, final CartTotalDetails cartTotalDetails, final WalletDetails walletDetails, final String transactionID) {
        this.cartTotalDetails = cartTotalDetails;
        this.transactionID = transactionID;
        this.walletDetails = walletDetails;

        //showLoadingIndicator("Displaying cart items...");
        GroupingMethods.groupCartItems(cartItemsList, new GroupingMethods.GroupedCartItemsCallback() {
            @Override
            public void onComplete(List<GroupedCartItems> groupedCartItems) {
                switchToContentPage();

                cartItemsAdapter.setItems(GroupingMethods.getGlobalModelsList(groupedCartItems));

                if (walletDetails != null && walletDetails.getBalance() >= 1) {
                    useWalletBalance.setVisibility(View.VISIBLE);
                    useWalletBalance.setText("Use wallet balance (" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(walletDetails.getBalance()) + ")");
                    useWalletBalance.setCheckedWithoutCallback(true);
                } else {
                    useWalletBalance.setVisibility(View.GONE);
                    useWalletBalance.setCheckedWithoutCallback(false);
                }

                displayTotal(cartTotalDetails);
            }
        });
    }

    public void displayTotal(CartTotalDetails cartTotalDetails) {
        try {
            this.cartTotalDetails = cartTotalDetails;

            sub_total.setText(RS + " " + new DecimalFormat("0.00").format(cartTotalDetails.getSubTotal()));
            delivery_charges.setText(RS + " " + new DecimalFormat("0.00").format(cartTotalDetails.getDeliveryCharges()));
            service_charges.setText(RS + " " + new DecimalFormat("0.00").format(cartTotalDetails.getServiceCharges()));
            if (cartTotalDetails.getDeliveryChargesMessage() != null && cartTotalDetails.getDeliveryChargesMessage().trim().length() > 0) {
                delivery_charges_message.setVisibility(View.VISIBLE);
                delivery_charges_message.setText(CommonMethods.fromHtml(cartTotalDetails.getDeliveryChargesMessage().replaceAll("Rs.", SpecialCharacters.RS)));
            } else {
                delivery_charges_message.setVisibility(View.GONE);
            }
            if (cartTotalDetails.getSurCharges() > 0) {
                surChargesDetails.setVisibility(View.VISIBLE);
                surcharges.setText(RS + " " + new DecimalFormat("0.00").format(cartTotalDetails.getSurCharges()));
            } else {
                surChargesDetails.setVisibility(View.GONE);
            }

            government_taxes.setText(RS + " " + new DecimalFormat("0.00").format(cartTotalDetails.getGovtTaxes()));

            if (cartTotalDetails.getCouponCode() != null && cartTotalDetails.getCouponCode().length() > 0) {
                coupon_code.setText(cartTotalDetails.getCouponCode().toUpperCase().replaceAll(localStorageData.getBuyerID().toUpperCase(), ""));
                if (cartTotalDetails.getCouponCashback() > 0) {
                    coupon_discount.setText("Cashback: " + SpecialCharacters.RS + " " + new DecimalFormat("0.00").format(cartTotalDetails.getCouponCashback()));
                } else {
                    coupon_discount.setText("- " + SpecialCharacters.RS + " " + new DecimalFormat("0.00").format(cartTotalDetails.getCouponDiscount()));
                }
                removeCoupon.setVisibility(View.VISIBLE);
            } else {
                coupon_code.setText("");
                coupon_discount.setText("APPLY");
                removeCoupon.setVisibility(View.GONE);
            }

            total_amount.setText(RS + " " + new DecimalFormat("0.00").format(cartTotalDetails.getTotal()));

            if (cartTotalDetails.getTotalMessage() != null && cartTotalDetails.getTotalMessage().trim().length() > 0) {
                total_amount_message.setVisibility(View.VISIBLE);
                total_amount_message.setText(CommonMethods.fromHtml(cartTotalDetails.getTotalMessage().replaceAll("Rs.", SpecialCharacters.RS)));
            } else {
                total_amount_message.setVisibility(View.GONE);
            }

            // Checkout Amount
            displayCheckoutAmount();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayCheckoutAmount() {
        double checkoutAmount = cartTotalDetails.getTotal();
        if (walletDetails != null && walletDetails.getBalance() >= 1 && useWalletBalance.getVisibility() == View.VISIBLE && useWalletBalance.isChecked()) {
            checkoutAmount = cartTotalDetails.getTotal() - walletDetails.getBalance();
        }
        if (checkoutAmount < 0) {
            checkoutAmount = 0;
        }
        checkOut.setText("CHECKOUT (" + RS + " " + new DecimalFormat("0.00").format(checkoutAmount) + ")");
    }

    //-------------

    public void getCart() {

        showLoadingIndicator("Loading cart items...");
        getBackendAPIs().getCartAPI().getCart(new CartAPI.CartCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String serverTime, List<CartItemDetails> cartItemsList, CartTotalDetails cartTotalDetails, WalletDetails walletDetails, String transactionID) {
                if (status) {
                    if (cartItemsList.size() > 0) {
                        if (serverTime != null && serverTime.trim().length() > 0) {
                            localStorageData.setServerTime(DateMethods.getReadableDateFromUTC(serverTime));
                        }
                        localStorageData.setCartCount(cartItemsList.size());

                        displayCart(cartItemsList, cartTotalDetails, walletDetails, transactionID);
                    } else {
                        localStorageData.setCartCount(0);
                        showNoDataIndicator("No items in cart.");
                        if (message == null || message.length() == 0) {
                            message = "Sorry there is no items in your cart.";
                        }
                        showNotificationDialog(false, "Alert!", message, new ActionCallback() {
                            @Override
                            public void onAction() {
                                onBackPressed();
                            }
                        });
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load cart items, try again!");
                }
            }
        });
    }

    public void updateCartItemSpecialInstructions(final String specialInstructions) {
        showLoadingDialog("Updating special instructions, wait...");
        getBackendAPIs().getCartAPI().updateCartItemSpecialInstructions(selectedCartItemDetails.getID(), specialInstructions, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    cartItemsAdapter.updateSpecialInstructions(selectedCartItemIndex, specialInstructions);
                } else {
                    showSnackbarMessage("Unable to update special instructions, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateCartItemSpecialInstructions(specialInstructions);
                        }
                    });
                }
            }
        });
    }

    public void deleteCartItem() {

        showLoadingDialog("Deleting item, wait..");
        getBackendAPIs().getCartAPI().deleteCartItem(selectedCartItemDetails.getID(), new CartAPI.DeleteCartItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, CartTotalDetails cartTotalDetails) {
                closeLoadingDialog();

                if (status) {
                    localStorageData.setCartCount(localStorageData.getCartCount() - 1);
                    cartItemsAdapter.deleteItem(selectedCartItemIndex);

                    if (localStorageData.getCartCount() > 0) {
                        displayCart(cartItemsAdapter.getAllCartItems(), cartTotalDetails, walletDetails, transactionID);
                        if (message != null && message.length() > 0) {
                            showNotificationDialog("Alert!", message);
                        }
                    } else {
                        onBackPressed();
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to delete item, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            deleteCartItem();
                        }
                    });
                }
            }
        });
    }

    public void applyCoupon(String couponCode) {

        showLoadingDialog("Applying coupon...");
        getBackendAPIs().getCouponsAPI().applyCoupon(couponCode, (status, statusCode, message, cartTotalDetails) -> {
            closeLoadingDialog();
            Log.e("coupon ", "" + cartTotalDetails);

            if (status) {
                displayTotal(cartTotalDetails);
                // Log.e("coupon sucess", "" + cartTotalDetails.getTotalMessage());

                if (message != null && message.length() > 0) {
                    showNotificationDialog("Success", CommonMethods.fromHtml(message));

                } else {
                    showSnackbarMessage("Coupon applied successfully.");
                }
            } else {

                //getCart();
//                showSnackbarMessage("Coupon applied successfully.");

                if (message != null && message.length() > 0) {
                    //  showNotificationDialog("Success", "OK");
                     showNotificationDialog("Failure", CommonMethods.fromHtml(message));
                    Log.e("coupon fail", "" + cartTotalDetails);
                   // onReloadPressed();
                    // getCart();
                } else {
                    showToastMessage("* Something went wrong , try again!");
                }
            }
        });
    }

    public void removeCoupon() {

        showLoadingDialog("Removing coupon...");
        getBackendAPIs().getCouponsAPI().removeCoupon(new CouponsAPI.AddOrRemoveCouponCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, CartTotalDetails cartTotalDetails) {
                closeLoadingDialog();
                if (status) {
                    showSnackbarMessage("Coupon removed successfully.");
                    displayTotal(cartTotalDetails);
                } else {
                    showChoiceSelectionDialog("Failure!", "Something went wrong while removing coupon.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                removeCoupon();
                                onReloadPressed();
                            }
                        }
                    });
                }
            }
        });
    }

    public void checkoutCart() {

        showLoadingDialog("Checking items availability...");
        getBackendAPIs().getCartAPI().checkoutCart(new CartAPI.CheckoutCartCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final boolean refreshCart, String cartMessage) {
                closeLoadingDialog();
                if (status) {
                    int preOrderItemsCount = 0;
                    List<Double> itemsPrepTimes = new ArrayList<>();
                    for (int i = 0; i < cartItemsAdapter.getItemCount(); i++) {
                        CartItemDetails cartItemDetails = cartItemsAdapter.getItem(i).getCartItemDetails();
                        if (cartItemDetails != null) {
                            itemsPrepTimes.add(cartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getPreparationHours());
                            if (cartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().isPreOrderable()) {
                                preOrderItemsCount++;
                            }
                        }
                    }

                    Intent orderBookingAct = new Intent(context, OrderBookingActivity.class);
                    orderBookingAct.putExtra("maxCartPreTime", CommonMethods.getMaxValueFromDoubleList(itemsPrepTimes));
                    orderBookingAct.putExtra("preOrderItemsCount", preOrderItemsCount);
                    orderBookingAct.putExtra("cartTotalDetails", new Gson().toJson(cartTotalDetails));
                    orderBookingAct.putExtra("transaction_id", transactionID);
                    orderBookingAct.putExtra(Constants.USE_WALLET_BALANCE, useWalletBalance.getVisibility() == View.VISIBLE && useWalletBalance.isChecked());
                    startActivity(orderBookingAct);
                } else {
                    if (cartMessage != null && cartMessage.length() > 0) {
                        showNotificationDialog("Alert", CommonMethods.fromHtml(cartMessage), new ActionCallback() {
                            @Override
                            public void onAction() {
                                if (refreshCart) {
                                    onReloadPressed();
                                }
                            }
                        });
                    } else {
                        showChoiceSelectionDialog("Failure", "Something went wrong, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                    checkoutCart();
                                }
                            }
                        });
                    }
                }
            }
        });
    }


    //=====================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == Constants.UPDATE_CART_ITEM_ACTIVITY_CODE && resultCode == RESULT_OK) {
                onReloadPressed();
            } else if (requestCode == Constants.COUPONS_ACTIVITY_CODE && resultCode == RESULT_OK) {
                applyCoupon(data.getStringExtra(Constants.COUPON_CODE));
            }

        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        try {
            new BroadcastReceiversMethods(activity).checkCartStatus();
        } catch (Exception e) {
        }
        super.onBackPressed();
    }

}

/*

public void showCouponDialog(){

        View couponDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_or_edit_coupon, null);
        final vTextInputLayout coupon = (vTextInputLayout) couponDialogView.findViewById(R.id.coupon_code);
        final Button applyCoupon = (Button) couponDialogView.findViewById(R.id.applyCoupon);
        final ProgressBar progress_bar = (ProgressBar) couponDialogView.findViewById(R.id.progress_bar);

        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        coupon.setText(coupon_code.getText().toString());
        coupon.setSoftKeyBoardListener(new SoftKeyBoardActionsCallback() {
            @Override
            public void onDonePressed(String text) {
                applyCoupon.performClick();
            }
        });

        applyCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String couponCode = coupon.getText().toString();
                if (couponCode.length() > 0) {
                    //if (couponCode.length() >= 5) {
                        coupon.hideError();
                        inputMethodManager.hideSoftInputFromWindow(coupon.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
                        applyCoupon.setVisibility(View.INVISIBLE);//setEnabled(false);
                        progress_bar.setVisibility(View.VISIBLE);

                        getBackendAPIs().getCouponsAPI().applyCoupon(couponCode, new CouponsAPI.AddOrRemoveCouponCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, CartTotalDetails cartTotalDetails) {
                                applyCoupon.setVisibility(View.VISIBLE);//setEnabled(true);
                                progress_bar.setVisibility(View.GONE);

                                if (status) {
                                    closeCustomDialog();

                                    displayTotal(cartTotalDetails);

                                    if (message != null && message.length() > 0) {
                                        showNotificationDialog("Success", message);
                                    } else {
                                        showSnackbarMessage("Coupon applied successfully.");
                                    }
                                } else {
                                    if (message != null && message.length() > 0) {
                                        showNotificationDialog("Alert!", message);
                                    } else {
                                        coupon.showError("* Something went wrong , try again!");
                                    }
                                }
                            }
                        });
                    //} else {
                    //    coupon.showError("* Enter valid coupon");
                    //}
                }else{
                    coupon.showError("* Enter coupon code");
                }

            }
        });
        showCustomDialog(couponDialogView);
    }

 */