package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.aleena.common.interfaces.StatusCallback;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalDBOrderFeedback;
import com.mastaan.buyer.localdata.OrdersFeedbackDatabase;
import com.mastaan.buyer.models.OrderDetails;

import java.util.ArrayList;
import java.util.List;

public class PendingFeedbacksActivity extends MastaanToolBarActivity {

    OrdersFeedbackDatabase ordersFeedbackDatabase;

    LinearLayout thank_you_view;
    Button continuee;

    boolean isSubmittedAnyFeedback = false;

    int currentOrderIndex = 0;
    List<OrderDetails> allOrders = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_feedbacks);
        hasLoadingView();
        actionBar.setDisplayHomeAsUpEnabled(false);
        getGoogleAnalyticsMethods().sendScreenName("Pending Feedbacks");       // Send ScreenNames to Analytics

        ordersFeedbackDatabase = new OrdersFeedbackDatabase(context);
        ordersFeedbackDatabase.openDatabase();

        //-------------------------------

        thank_you_view = (LinearLayout) findViewById(R.id.thank_you_view);
        continuee = (Button) findViewById(R.id.continuee);
        continuee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ordersFeedbackDatabase.closeDatabase();
                setResult(Constants.PENDING_FEEDBACK_ORDERS_ACTIVITY_CODE, new Intent());
                finish();
            }
        });

        //-------------------------------

        showLoadingIndicator("Loading...");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                ArrayList<String> pendingFeedbackOrders = getIntent().getStringArrayListExtra("pendingFeedbackOrders");
                if ( pendingFeedbackOrders != null ){
                    for ( int i=0;i<pendingFeedbackOrders.size();i++){
                        allOrders.add(new Gson().fromJson(pendingFeedbackOrders.get(i), OrderDetails.class));
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                displayOrderFeedback(0);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void displayOrderFeedback(int index){

        if ( allOrders.size() > index ){

            currentOrderIndex = index;

            ordersFeedbackDatabase.getFeedbackItem(allOrders.get(currentOrderIndex).getID(), new OrdersFeedbackDatabase.GetOrderFeedbackItemCallback() {
                @Override
                public void onComplete(final LocalDBOrderFeedback localDBOrderFeedback) {

                    if ( localDBOrderFeedback == null || localDBOrderFeedback.getOrderID() == null ) {  //  If Not Given Feedback.

                        Intent orderFeedbackAct = new Intent(context, OrderFeedbackActivity.class);
                        orderFeedbackAct.putExtra("order_details_json", new Gson().toJson(allOrders.get(currentOrderIndex)));
                        if (currentOrderIndex == allOrders.size() - 1) {
                            orderFeedbackAct.putExtra("showNextIndicator", false);
                        }
                        startActivityForResult(orderFeedbackAct, Constants.ORDER_FEEDBACK_ACTIVITY_CODE);
                    }
                    else{
                        Log.d(Constants.LOG_TAG, "Already Had Feedback for OrderID="+localDBOrderFeedback.getOrderID()+" of Type="+localDBOrderFeedback.getType()+" , with Feedback : "+localDBOrderFeedback.getOrderFeedbackJSON()+" , Submitting it in the Background");
                        displayOrderFeedback(currentOrderIndex + 1);    //  Showing Next FeedbackItem

                        if ( localDBOrderFeedback.getBuyerMobile().equalsIgnoreCase(localStorageData.getBuyerMobile()) ){

                            if ( localDBOrderFeedback.getType().equalsIgnoreCase("Submit") ){
                                getBackendAPIs().getFeedbacksAPI().submitFeedback(localDBOrderFeedback.getOrderID(), localDBOrderFeedback.getOrderFeedbackObject(), new StatusCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message) {
                                        if (status == true || statusCode == 500) {
                                            ordersFeedbackDatabase.deleteFeedbackItem(localDBOrderFeedback.getOrderID(), null);
                                        } else if (statusCode == 403) {
                                            clearUserSession("Your session has expired.");
                                        } else {
                                            if (localDBOrderFeedback.getRetryCount() < 8) {
                                                ordersFeedbackDatabase.increaseOrderFeedbackRetryCount(localDBOrderFeedback, null);
                                            } else {
                                                ordersFeedbackDatabase.deleteFeedbackItem(localDBOrderFeedback.getOrderID(), null);
                                            }
                                        }
                                    }
                                });
                            }
                            else if ( localDBOrderFeedback.getType().equalsIgnoreCase("Skip") ){
                                getBackendAPIs().getFeedbacksAPI().skipFeedback(localDBOrderFeedback.getOrderID(), new StatusCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message) {
                                        if (status == true || statusCode == 500 ) {
                                            ordersFeedbackDatabase.deleteFeedbackItem(localDBOrderFeedback.getOrderID(), null);
                                        }
                                        else if ( statusCode == 403 ){
                                            clearUserSession("Your session has expired.");
                                        }
                                        else {
                                            if ( localDBOrderFeedback.getRetryCount() < 8 ) {
                                                ordersFeedbackDatabase.increaseOrderFeedbackRetryCount(localDBOrderFeedback, null);
                                            }else{
                                                ordersFeedbackDatabase.deleteFeedbackItem(localDBOrderFeedback.getOrderID(), null);
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

        }
        else{
            if ( isSubmittedAnyFeedback == true ){
                switchToContentPage();
            }else{
                //ordersFeedbackDatabase.closeDatabase();
                setResult(Constants.PENDING_FEEDBACK_ORDERS_ACTIVITY_CODE, new Intent());
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == Constants.ORDER_FEEDBACK_ACTIVITY_CODE ){
                if ( resultCode == Constants.ORDER_FEEDBACK_ACTIVITY_CODE) {
                    if ( data.getBooleanExtra("isSubmittedFeedback", false)  ){
                        isSubmittedAnyFeedback = true;
                    }
                    displayOrderFeedback(currentOrderIndex + 1);
                }else{
                    onBackPressed();
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }
}
