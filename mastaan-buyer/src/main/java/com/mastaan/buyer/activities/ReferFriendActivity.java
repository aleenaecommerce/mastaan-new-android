package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.BuyerDetails;
import com.mastaan.buyer.models.ReferralOfferDetails;

public class ReferFriendActivity extends MastaanToolBarActivity {

    ImageView image;
    TextView details;
    TextView expiry_date;
    View referAndEarn;

    BuyerDetails buyerDetails;
    ReferralOfferDetails referralOfferDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_friend);
        getGoogleAnalyticsMethods().sendScreenName("Refer Friend");       // Send Screen Name to Analytics
        hasLoadingView();

        buyerDetails = localStorageData.getBuyerDetails();

        //--------

        image = findViewById(R.id.image);

        details = findViewById(R.id.details);
        expiry_date = findViewById(R.id.expiry_date);
        referAndEarn = findViewById(R.id.referAndEarn);
        referAndEarn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( localStorageData.getSessionFlag() ){
                    String subject = getDefaultShareSubject();
                    String message = getDefaultShareMessage();

                    if ( referralOfferDetails != null && buyerDetails != null && buyerDetails.getReferralID() != null && buyerDetails.getReferralID().length() > 0 ){
                        message = "Dekho idar meat mast fresh hai, try Mastaan (https://www.mastaan.com), use coupon "+buyerDetails.getReferralID().toUpperCase()+" and get "+referralOfferDetails.getReceiverOffer()
                                +(referralOfferDetails.getReceiverMinimumOrder()>0?" on min order of Rs."+CommonMethods.getIndianFormatNumber(referralOfferDetails.getReceiverMinimumOrder()):"")+".";
                    }

                    Intent shareAppsAct = new Intent(context, ShareAppsActivity.class);
                    shareAppsAct.putExtra(Constants.SUBJECT, subject);
                    shareAppsAct.putExtra(Constants.MESSAGE, message);
                    startActivity(shareAppsAct);
                }else{
                    showToastMessage("Please login to continue...");
                }
            }
        });

        //--------

        getReferralDetails();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getReferralDetails();
    }

    public void getReferralDetails(){

        if ( localStorageData.getSessionFlag() ) {
            showLoadingIndicator("Loading...");
            getMastaanApplication().getReferralOfferDetails(new GeneralAPI.ReferralOfferCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, ReferralOfferDetails referralOfferDetails) {
                    if (status) {
                        display(referralOfferDetails);
                    } else {
                        showReloadIndicator("Something went wrong, try again!");
                    }
                }
            });
        }else{
            display(null);
        }
    }

    public void display(ReferralOfferDetails referralOfferDetails){

        if ( referralOfferDetails != null && referralOfferDetails.getReceiverDiscountValue() > 0 && referralOfferDetails.getSenderDiscountValue() > 0){
            this.referralOfferDetails = referralOfferDetails;

            details.setText(CommonMethods.fromHtml(referralOfferDetails.getFormatterReferralOfferDetails()));
            if ( referralOfferDetails.getExpiryDate() != null && referralOfferDetails.getExpiryDate().length() > 0 ){
                expiry_date.setVisibility(View.VISIBLE);
                expiry_date.setText("Valid till "+DateMethods.getReadableDateFromUTC(referralOfferDetails.getExpiryDate(), DateConstants.MMM_DD_YYYY));
            }else{
                expiry_date.setVisibility(View.GONE);
            }
            switchToContentPage();
        }
        else{
            Intent shareAppsAct = new Intent(context, ShareAppsActivity.class);
            shareAppsAct.putExtra(Constants.ANALYTICS_TITLE, "Refer Friend");
            shareAppsAct.putExtra(Constants.SUBJECT, getDefaultShareSubject());
            shareAppsAct.putExtra(Constants.MESSAGE, getDefaultShareMessage());
            startActivity(shareAppsAct);
            finish();
        }
    }

    public String getDefaultShareSubject(){
        return "Mastaan";
    }
    public String getDefaultShareMessage() {
        return "Check out Mastaan (https://www.mastaan.com). They deliver fresh meat to your doorstep.";
    }


    //========

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
