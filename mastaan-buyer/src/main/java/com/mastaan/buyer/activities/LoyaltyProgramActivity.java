package com.mastaan.buyer.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vNestedScrollView;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.MembershipsAdapter;
import com.mastaan.buyer.backend.ProfileAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.interfaces.MembershipsCallback;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;
import com.mastaan.buyer.models.MembershipDetails;
import com.mastaan.buyer.models.ProfileSummaryDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 17/01/19.
 */

public class LoyaltyProgramActivity extends MastaanToolBarActivity {

    vNestedScrollView scrollview;

    View membershipDetails;
    TextView membership;
    TextView membership_delivery_charges_savings;
    TextView membership_orders_savings;

    View ordersAmountView;
    TextView orders_total;

    vRecyclerView membershipsHolder;
    MembershipsAdapter membershipsAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loyalty_program);
        getGoogleAnalyticsMethods().sendScreenName("Loyalty Program");       // Send ScreenNames to Analytics
        hasLoadingView();

        //--------

        scrollview = findViewById(R.id.scrollview);

        membershipDetails = findViewById(R.id.membershipDetails);
        membership = findViewById(R.id.membership);
        membership_delivery_charges_savings = findViewById(R.id.membership_delivery_charges_savings);
        membership_orders_savings = findViewById(R.id.membership_orders_savings);

        ordersAmountView = findViewById(R.id.ordersAmountView);
        orders_total = findViewById(R.id.orders_total);

        membershipsHolder = findViewById(R.id.membershipsHolder);
        membershipsHolder.setupVerticalOrientation();
        membershipsHolder.setNestedScrollingEnabled(false);
        membershipsAdapter = new MembershipsAdapter(context, new ArrayList<MembershipDetails>(), new MembershipsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
            }
        }).setUiCallback(new MembershipsAdapter.UICallback() {
            @Override
            public void onExpand(int position, View view) {
                if ( position >= membershipsAdapter.getItemCount()-2 ) {
                    scrollview.focusToViewBottom(view);
                }
            }

            @Override
            public void onCollapse(int position, View view) {

            }
        });
        membershipsHolder.setAdapter(membershipsAdapter);

        //--------

        getData();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getData();
    }

    //-------

    private void getData(){

        showLoadingIndicator("Loading...");
        getMastaanApplication().getMemberships(new MembershipsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<MembershipDetails> membershipsList) {
                if ( status ){
                    if ( localStorageData.getSessionFlag() ){
                        getBackendAPIs().getProfileAPI().getProfileDetails(new ProfileAPI.ProfileDetailsCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, ProfileSummaryDetails profileSummaryDetails) {
                                if ( status ){
                                    display(profileSummaryDetails, membershipsList);
                                }else{
                                    showReloadIndicator("Something went wrong, try again!");
                                }
                            }
                        });
                    }
                    else{
                        display(null, membershipsList);
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    private void display(ProfileSummaryDetails profileSummaryDetails, List<MembershipDetails> membershipsList){
        if ( membershipsList == null || membershipsList.size() == 0 ){
            showToastMessage("Currently, we do not have any active Loyalty Program");
            onBackPressed();
            return;
        }

        Collections.sort(membershipsList, new Comparator<MembershipDetails>() {
            @Override
            public int compare(MembershipDetails item1, MembershipDetails item2) {
                return item2.getBuyerTotalOrdersMinimumAmount().compareTo(item1.getBuyerTotalOrdersMinimumAmount());
            }
        });

        switchToContentPage();
        membershipsAdapter.setItems(membershipsList);

        if ( profileSummaryDetails != null && profileSummaryDetails.getBuyerDetails() != null ){
            String membershipType = profileSummaryDetails.getBuyerDetails().getMembershipType();

            if ( membershipType.length() > 0 && membershipType.trim().equalsIgnoreCase(Constants.BRONZE_MEMBER) == false ){
                membershipDetails.setVisibility(View.VISIBLE);

                membership.setText(CommonMethods.capitalizeStringWords(membershipType));

                membership_delivery_charges_savings.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(profileSummaryDetails.getMembershipDeliveryDiscount())));
                membership_orders_savings.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(profileSummaryDetails.getMembershipOrderDiscount())));
            }
            else{
                membershipDetails.setVisibility(View.GONE);
            }

            orders_total.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(profileSummaryDetails.getFinalTotal())));

            localStorageData.setBuyerDetails(profileSummaryDetails.getBuyerDetails(), new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message) {
                    new BroadcastReceiversMethods(context).displayMembershipDetailsInSidebar();
                }
            });
        }else{
            membershipDetails.setVisibility(View.GONE);
            ordersAmountView.setVisibility(View.GONE);
        }

    }

    //==============

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
