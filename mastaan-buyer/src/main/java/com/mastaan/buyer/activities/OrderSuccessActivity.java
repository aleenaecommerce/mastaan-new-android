package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.OrientationHelper;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vCollageImageView;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vWrappingLinearLayoutManager;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.OrdersAdapter;
import com.mastaan.buyer.backend.models.RequestAddOnlinePayment;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.dialogs.PaymentTypeDialog;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;
import com.mastaan.buyer.models.GroupedOrders;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.payment.paytm.PaytmPayment;
import com.mastaan.buyer.payment.razorpay.RazorpayPayment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrderSuccessActivity extends MastaanToolBarActivity{

    vCollageImageView collage_images;
    View orderIDView;
    TextView order_id;
    TextView buyer_name;
    TextView buyer_contact;
    TextView delivery_address;
    TextView deliverySlot;
    View paymentModeView;
    TextView payment_mode;
    View makeOnlinePayment;
    TextView final_amount;
    TextView sub_total;
    View discountDetails;
    TextView discount_name;
    TextView discount_amount;
    TextView delivery_charges;
    View serviceChargesDetails;
    TextView service_charges;
    View surChargesDetails;
    TextView surcharges;
    View governmentTaxesDetails;
    TextView government_taxes;
    View membershipOrderDiscountDetails;
    TextView membership_order_discount;
    View membershipDeliveryDiscountDetails;
    TextView membership_delivery_discount;
    View cashbackDetails;
    TextView cashback_name;
    TextView cashback_amount;

    vRecyclerView ordersHolder;
    OrdersAdapter ordersAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_success);
        hasLoadingView();
        //getGoogleAnalyticsMethods().sendScreenName("Order Success");       // Sending with e-commerce transaction

        localStorageData.setMeatItemPrefillDate("");

        //-----------

        collage_images = findViewById(R.id.collage_images);
        orderIDView = findViewById(R.id.orderIDView);
        order_id = findViewById(R.id.order_id);
        buyer_name = findViewById(R.id.buyer_name);
        buyer_contact = findViewById(R.id.buyer_contact);
        delivery_address = findViewById(R.id.delivery_address);
        deliverySlot = findViewById(R.id.deliverySlot);
        paymentModeView = findViewById(R.id.paymentModeView);
        payment_mode = findViewById(R.id.payment_mode);
        final_amount = findViewById(R.id.final_amount);
        sub_total = findViewById(R.id.sub_total);
        discountDetails = findViewById(R.id.discountDetails);
        discount_name = findViewById(R.id.discount_name);
        discount_amount = findViewById(R.id.discount_amount);
        delivery_charges = findViewById(R.id.delivery_charges);
        serviceChargesDetails = findViewById(R.id.serviceChargesDetails);
        service_charges = findViewById(R.id.service_charges);
        surChargesDetails = findViewById(R.id.surChargesDetails);
        surcharges = findViewById(R.id.surcharges);
        governmentTaxesDetails = findViewById(R.id.governmentTaxesDetails);
        government_taxes = findViewById(R.id.government_taxes);
        membershipOrderDiscountDetails = findViewById(R.id.membershipOrderDiscountDetails);
        membership_order_discount = findViewById(R.id.membership_order_discount);
        membershipDeliveryDiscountDetails = findViewById(R.id.membershipDeliveryDiscountDetails);
        membership_delivery_discount = findViewById(R.id.membership_delivery_discount);
        cashbackDetails = findViewById(R.id.cashbackDetails);
        cashback_name = findViewById(R.id.cashback_name);
        cashback_amount = findViewById(R.id.cashback_amount);

        makeOnlinePayment = findViewById(R.id.makeOnlinePayment);
        makeOnlinePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proceedToPayment(true, null, null, false);
            }
        });

        ordersHolder = findViewById(R.id.ordersHolder);
        vWrappingLinearLayoutManager vWrappingLinearLayoutManager = new vWrappingLinearLayoutManager(context);
        vWrappingLinearLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        ordersHolder.setLayoutManager(vWrappingLinearLayoutManager);//new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        ordersHolder.setHasFixedSize(false);
        ordersHolder.setNestedScrollingEnabled(false);
        ordersHolder.setItemAnimator(new DefaultItemAnimator());

        //-----------

        ArrayList<String> imageURLs = getIntent().getStringArrayListExtra("imageURLs");
        collage_images.setDefaultImage(R.drawable.image_default_mastaan);
        collage_images.addImageURLs(imageURLs);

        showLoadingIndicator("Loading order details...");
        new AsyncTask<Void, Void, Void>() {
            List<OrderDetails> ordersList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... voids) {
                String grouped_orders_list_json = getIntent().getStringExtra(Constants.GROUPED_ORDERS);
                ordersList = (new Gson().fromJson(grouped_orders_list_json, GroupedOrders.class)).getOrdersList();
                //isImmediate = getIntent().getBooleanExtra("isImmediate", false);

                Collections.sort(ordersList, new Comparator<OrderDetails>() {
                    public int compare(OrderDetails item1, OrderDetails item2) {
                        return item1.getDeliveryDateInISTFormat().compareToIgnoreCase(item2.getDeliveryDateInISTFormat());
                    }
                });
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                switchToContentPage();

                updateDBAndReceivers(ordersList);
                displayOrderDetails(ordersList);

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    String couponCode = "";
    double totalAmount = 0, totalOPAmount = 0, totalWalletAmount = 0, totalCODAmount = 0, totalCouponDiscount=0, totalCouponCashback = 0, totalDeliveryCharges=0, totalServiceCharges=0, totalSurCharges=0, totalGovtTaxes=0, totalMembershipOrderDiscount=0, totalMembershipDeliveryDiscount=0;
    public void displayOrderDetails(List<OrderDetails> ordersList){
        try{
            // Sending e-commerce transaction details to google analytics
            getGoogleAnalyticsMethods().sendOrderTransaction("Order Success", ordersList);

            OrderDetails orderDetails = ordersList.get(0);      //TEMP

            couponCode = orderDetails.getCouponcode();
            for (int i=0;i<ordersList.size();i++){
                totalAmount += ordersList.get(i).getTotalAmount();
                totalOPAmount += ordersList.get(i).getOnlinePaymentAmount();
                totalWalletAmount += ordersList.get(i).getWalletAmount();
                totalCODAmount += ordersList.get(i).getCODAmount();
                totalCouponDiscount += ordersList.get(i).getCouponDiscount();
                totalCouponCashback += ordersList.get(i).getCouponCashback();
                totalDeliveryCharges += ordersList.get(i).getDeliveryCharges();
                totalServiceCharges += ordersList.get(i).getServiceCharges();
                totalSurCharges += ordersList.get(i).getSurcharges();
                totalGovtTaxes += ordersList.get(i).getGovernmentTaxes();
                totalMembershipOrderDiscount += ordersList.get(i).getMembershipOrderDiscount();
                totalMembershipDeliveryDiscount += ordersList.get(i).getMembershipDeliveryDiscount();
            }

            buyer_name.setText(orderDetails.getCustomerName());
            buyer_contact.setText(orderDetails.getFormattedContactDetails());
            delivery_address.setText(orderDetails.getDeliveryAddress());

            displayPaymentDetails();

            //----

            if ( ordersList.size() == 1 ) {
                ordersAdapter = new OrdersAdapter(context, R.layout.view_order_without_details, new ArrayList<OrderDetails>(), null);
                orderIDView.setVisibility(View.VISIBLE);
                deliverySlot.setVisibility(View.VISIBLE);
                order_id.setText(orderDetails.getOrderID());
                deliverySlot.setText("Your order will be delivered by\n" + orderDetails.getFormattedDeliveryDate());//DateMethods.getDateInFormat(orderDetails.getDeliveryDateInISTFormat(), DateConstants.MMM_DD_YYYY_HH_MM_A));
            }else{
                ordersAdapter = new OrdersAdapter(context, new ArrayList<OrderDetails>(), null);
                orderIDView.setVisibility(View.GONE);
                deliverySlot.setVisibility(View.GONE);
            }

            ordersHolder.setAdapter(ordersAdapter);
            ordersAdapter.addItems(ordersList);      // Display Order Items.

            //---------

            if ( totalCODAmount > 0 ) {
                paymentModeView.setVisibility(View.GONE);       // Hiding Temporarily until Payment Type selected
                proceedToPayment(false, "Thank you", "Your order is placed, kindly choose a payment method.", true);
            }else{
                paymentModeView.setVisibility(View.VISIBLE);
                makeOnlinePayment.setVisibility(View.GONE);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    private void displayPaymentDetails(){
        String paymentModeString = "";
        if (totalWalletAmount > 0) {
            paymentModeString += "Wallet"+(totalOPAmount>0||totalCODAmount>0?" (" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(totalWalletAmount) + ")":"");
        }
        if (totalOPAmount > 0) {
            if ( paymentModeString.length() > 0 ){   paymentModeString += "\n";  }
            paymentModeString += "Online payment"+(totalWalletAmount>0||totalCODAmount>0?" (" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(totalOPAmount) + ")":"");

        }
        if (totalCODAmount > 0) {
            if ( paymentModeString.length() > 0 ){   paymentModeString += "\n";  }
            paymentModeString += "Cash on delivery"+(totalWalletAmount>0||totalOPAmount>0?" (" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(totalCODAmount) + ")":"");
        }
        payment_mode.setText(paymentModeString);

        final_amount.setText(SpecialCharacters.RS + " " + new DecimalFormat("0.00").format(totalAmount));

        sub_total.setText(SpecialCharacters.RS + " " + new DecimalFormat("0.00").format(totalAmount-totalDeliveryCharges-totalServiceCharges-totalSurCharges-totalGovtTaxes+totalCouponDiscount+totalMembershipOrderDiscount+totalMembershipDeliveryDiscount));

        if ( totalCouponDiscount > 0 ){
            discountDetails.setVisibility(View.VISIBLE);
            if ( couponCode != null && couponCode.length() > 0 ){
                discount_name.setText(Html.fromHtml("Discount<br>(<b>"+couponCode.toUpperCase()+"</b>)"));
            }
            discount_amount.setText("- "+SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(totalCouponDiscount));
        }else{  discountDetails.setVisibility(View.GONE);  }

        if ( totalMembershipOrderDiscount > 0 ){
            membershipOrderDiscountDetails.setVisibility(View.VISIBLE);
            membership_order_discount.setText("- "+SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(totalMembershipOrderDiscount));
        }else{  membershipOrderDiscountDetails.setVisibility(View.GONE); }

        if ( totalMembershipDeliveryDiscount > 0 ){
            membershipDeliveryDiscountDetails.setVisibility(View.VISIBLE);
            membership_delivery_discount.setText("- "+SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(totalMembershipDeliveryDiscount));
        }else{  membershipDeliveryDiscountDetails.setVisibility(View.GONE); }

        delivery_charges.setText(SpecialCharacters.RS + " " + new DecimalFormat("0.00").format(totalDeliveryCharges));

        if ( totalServiceCharges > 0 ){
            serviceChargesDetails.setVisibility(View.VISIBLE);
            service_charges.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(totalServiceCharges));
        }else{  serviceChargesDetails.setVisibility(View.GONE);  }

        if ( totalSurCharges > 0 ){
            surChargesDetails.setVisibility(View.VISIBLE);
            surcharges.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(totalSurCharges));
        }else{  surChargesDetails.setVisibility(View.GONE);  }

        if ( totalGovtTaxes > 0 ){
            governmentTaxesDetails.setVisibility(View.VISIBLE);
            government_taxes.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(totalGovtTaxes));
        }else{  governmentTaxesDetails.setVisibility(View.GONE);  }

        if ( totalCouponCashback > 0 ){
            cashbackDetails.setVisibility(View.VISIBLE);
            if ( couponCode != null && couponCode.length() > 0 ){
                cashback_name.setText(Html.fromHtml("Cashback<br>(<b>"+couponCode.toUpperCase()+"</b>)"));
            }
            cashback_amount.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(totalCouponCashback));
        }else{  cashbackDetails.setVisibility(View.GONE);  }
    }

    public void proceedToPayment(final boolean isCancellable, final String dialogTitle, final String dialogMessage, final boolean showCashOnDelivery){
        PaymentTypeDialog.Callback paymentCallback = new PaymentTypeDialog.Callback() {
            @Override
            public void onSelect(String paymentType) {
                // OP
                if ( paymentType == Constants.ONLINE_PAYMENT ) {
                    RazorpayPayment.Callback razorpayPaymentCallback = new RazorpayPayment.Callback() {
                        @Override
                        public void onPaymentSuccess(String message, int paymentType, double amount, String transactionID, String paymentID) {
                            if ( ordersAdapter.getItemCount() > 1 ){
                                addOnlinePaymentForGroupOrder(ordersAdapter.getOrder(0).getGroupOrderID(), new RequestAddOnlinePayment(paymentType, amount, transactionID, paymentID));
                            }else{
                                addOnlinePaymentForOrder(ordersAdapter.getOrder(0).getID(), new RequestAddOnlinePayment(paymentType, amount, transactionID, paymentID));
                            }
                        }

                        @Override
                        public void onPaymentFailure(String message, double amount, String newTransactioinID) {
                            showChoiceSelectionDialog(false, "Failure!", "Your payment is failed. If the money is debited from your account, it will be refunded.\n\nDo you want to retry payment?", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if ( choiceName.equalsIgnoreCase("RETRY")){
                                        proceedToPayment(isCancellable, "Retry payment", null, showCashOnDelivery);
                                    }else{
                                        paymentModeView.setVisibility(View.VISIBLE);
                                        makeOnlinePayment.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                        }

                        @Override
                        public void onPaymentCancel() {
                            paymentModeView.setVisibility(View.VISIBLE);
                            makeOnlinePayment.setVisibility(View.VISIBLE);
                            showToastMessage("Payment cancelled");
                        }

                        @Override
                        public void onExternalWalletSelect(String walletName, double amount) {
                            if ( walletName.equalsIgnoreCase("paytm") ){
                                new PaytmPayment(activity).proceed(ordersAdapter.getOrder(0), amount, new PaytmPayment.Callback() {
                                    @Override
                                    public void onPaymentSuccess(String message, int paymentType, double amount, String transactionID, String paymentID) {
                                        if ( ordersAdapter.getItemCount() > 1 ){
                                            addOnlinePaymentForGroupOrder(ordersAdapter.getOrder(0).getGroupOrderID(), new RequestAddOnlinePayment(paymentType, amount, transactionID, paymentID));
                                        }else{
                                            addOnlinePaymentForOrder(ordersAdapter.getOrder(0).getID(), new RequestAddOnlinePayment(paymentType, amount, transactionID, paymentID));
                                        }
                                    }

                                    @Override
                                    public void onPaymentFailure(String message, double amount, String newTransactioinID) {
                                        showChoiceSelectionDialog(false, "Failure!", "Your payment is failed. If the money is debited from your account, it will be refunded.\n\nDo you want to retry payment?", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                                            @Override
                                            public void onSelect(int choiceNo, String choiceName) {
                                                if ( choiceName.equalsIgnoreCase("RETRY")){
                                                    proceedToPayment(isCancellable, "Retry payment", null, showCashOnDelivery);
                                                }else{
                                                    paymentModeView.setVisibility(View.VISIBLE);
                                                    makeOnlinePayment.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        });
                                    }

                                    @Override
                                    public void onPaymentCancel() {
                                        paymentModeView.setVisibility(View.VISIBLE);
                                        makeOnlinePayment.setVisibility(View.VISIBLE);
                                        showToastMessage("Payment cancelled");
                                    }
                                });
                            }else{
                                showToastMessage("Something went wrong, try again!");
                            }
                        }
                    };
                    if ( ordersAdapter.getItemCount() > 1 ) {
                        getRazorpayPayment().proceedForGroupedOrder(ordersAdapter.getAllOrders(), razorpayPaymentCallback);
                    }
                    else {
                        getRazorpayPayment().proceedForOrder(ordersAdapter.getOrder(0), ordersAdapter.getOrder(0).getCODAmount(), razorpayPaymentCallback);
                    }
                }
                // COD
                else{
                    paymentModeView.setVisibility(View.VISIBLE);
                    makeOnlinePayment.setVisibility(View.VISIBLE);
                    showNotificationDialog("Request", "Kindly tender the exact amount at the time of delivery, we are on a cashless mission and our delivery boys do not carry change. You can always pay for your order online.\n\nThank you.");
                }
            }
        };

        if ( showCashOnDelivery ){
            PaymentTypeDialog paymentTypeDialog = new PaymentTypeDialog(activity);
            paymentTypeDialog.setCancellable(isCancellable);
            paymentTypeDialog.setCashOnDeliveryVisibility(showCashOnDelivery);
            paymentTypeDialog.setCallback(paymentCallback).setTitle(dialogTitle).setMessage(dialogMessage).show();
        }else{
            paymentCallback.onSelect(Constants.ONLINE_PAYMENT);
        }
    }

    public void addOnlinePaymentForGroupOrder(final String groupOrderID, final RequestAddOnlinePayment requestAddOnlinePayment){
        showLoadingDialog("Processing payment...");
        getBackendAPIs().getPaymentAPI().addOnlinePaymentForGroupedOrder(groupOrderID, requestAddOnlinePayment, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Payment successful");

                    paymentModeView.setVisibility(View.VISIBLE);
                    totalCODAmount -= requestAddOnlinePayment.getAmount();
                    totalOPAmount += requestAddOnlinePayment.getAmount();
                    displayPaymentDetails();
                    makeOnlinePayment.setVisibility(View.GONE);
                }
                else{
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while processing payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                addOnlinePaymentForGroupOrder(groupOrderID, requestAddOnlinePayment);
                            }else{
                                paymentModeView.setVisibility(View.VISIBLE);
                                makeOnlinePayment.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
        });
    }

    public void addOnlinePaymentForOrder(final String orderID, final RequestAddOnlinePayment requestAddOnlinePayment){
        showLoadingDialog("Processing payment, wait...");
        getBackendAPIs().getPaymentAPI().addOnlinePaymentForOrder(orderID, requestAddOnlinePayment, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Payment successful");

                    paymentModeView.setVisibility(View.VISIBLE);
                    totalCODAmount -= requestAddOnlinePayment.getAmount();
                    totalOPAmount += requestAddOnlinePayment.getAmount();
                    displayPaymentDetails();
                    makeOnlinePayment.setVisibility(View.GONE);
                }
                else{
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while processing payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                addOnlinePaymentForOrder(orderID, requestAddOnlinePayment);
                            }else{
                                paymentModeView.setVisibility(View.VISIBLE);
                                makeOnlinePayment.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
        });
    }

    //-------------

    public void updateDBAndReceivers(final List<OrderDetails> ordersList){

        localStorageData.setCartCount(0);
        new BroadcastReceiversMethods(context).displayUserSessionInfo();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                int previousPendingOrdersCount = localStorageData.getNoOfPendingOrders();
                List<OrderDetails> pendingOrders = localStorageData.getBuyerPendingOrders();
                for (int i=0;i<ordersList.size();i++){
                    if ( ordersList.get(i) != null ){   pendingOrders.add(ordersList.get(i));   }
                }
                localStorageData.setBuyerPendingOrders(pendingOrders);

                new BroadcastReceiversMethods(context).displayUserSessionInfo();

                if (  previousPendingOrdersCount > 0 ){
                    for (int i=0;i<ordersList.size();i++) {
                        new BroadcastReceiversMethods(context).setupFirebaseForPendingOrder(ordersList.get(i));
                    }
                }/*else{
                    context.startService(firebaseService);
                }*/

                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    //=======================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") ) {
            finishAffinity();   // Closing app completely
        }else{
            super.onBackPressed();
            new BroadcastReceiversMethods(context).checkCartStatus();;

            Intent homeAct = new Intent(context, HomeActivity.class);
            homeAct.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            homeAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeAct);
        }
    }
}
