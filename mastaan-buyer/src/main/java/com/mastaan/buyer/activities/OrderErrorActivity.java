package com.mastaan.buyer.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.methods.CommonMethods;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.OrdersAPI;
import com.mastaan.buyer.backend.models.RequestPlaceOrder;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.flows.GoToPage;
import com.mastaan.buyer.models.OrderDetails;

import java.util.List;

public class OrderErrorActivity extends MastaanToolBarActivity implements View.OnClickListener{

    //LocationsDatabase locationsDatabase;

    TextView mastaan_contact_no;
    ImageView header_image;
    Button reTry;
    View callUs;

    RequestPlaceOrder requestPlaceOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_error);
        getGoogleAnalyticsMethods().sendScreenName("Order Failure");       // Send ScreenNames to Analytics

        String request_object_place_order = getIntent().getStringExtra("request_object_place_order");
        requestPlaceOrder = new Gson().fromJson(request_object_place_order, RequestPlaceOrder.class);

        //locationsDatabase = new LocationsDatabase(context);
        //locationsDatabase.openDatabase();

        //.......................................

        header_image = (ImageView) findViewById(R.id.header_image);

        mastaan_contact_no = (TextView) findViewById(R.id.mastaan_contact_no);
        mastaan_contact_no.setText("Call us at "+localStorageData.getSupportContactNumber()+"\nto place your order.");

        reTry = (Button) findViewById(R.id.reTry);
        reTry.setOnClickListener(this);

        callUs = findViewById(R.id.callUs);
        callUs.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if ( view == reTry ){
            placeOrder();
        }
        else if ( view == callUs ){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
            startActivity(intent);
        }
    }

    public void placeOrder(){

        showLoadingDialog("Placing your order...");
        getBackendAPIs().getOrdersAPI().placeOrder(requestPlaceOrder, new OrdersAPI.PlaceOrderCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final String errorCode, List<OrderDetails> ordersList) {
                if (status) {
                    GoToPage.goToOrderSuccessPage(activity, ordersList);
                }
                else {
                    closeLoadingDialog();
                    checkSessionValidity(statusCode);
                    if ( errorCode != null && errorCode.trim().length() > 0 ){
                        showNotificationDialog(false, "Failure", CommonMethods.fromHtml(message), new ActionCallback() {
                            @Override
                            public void onAction() {
                                if ( errorCode.toLowerCase().contains("stock") || errorCode.toLowerCase().contains("cart") ){
                                    Intent cartAct = new Intent(context, CartActivity.class);
                                    cartAct.putExtra(Constants.RELOAD_DATA, true);
                                    cartAct.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    cartAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(cartAct);
                                    finish();
                                }
                            }
                        });
                    }else{
                        //GoToPage.goToOrderFailurePage(activity, requestPlaceOrder);
                        showSnackbarMessage("Something went wrong, please try again!");
                    }
                }
            }
        });
    }

    //--------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent homeAct = new Intent(context, HomeActivity.class);
        homeAct.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        homeAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeAct);

    }
}


/*

public void goToOrderSuccessPage(final List<OrderDetails> ordersList){

        localStorageData.setCartCount(0);

        localStorageData.setBuyerName(requestPlaceOrder.getUserName());
        localStorageData.setBuyerEmail(requestPlaceOrder.getUserEmail());
        localStorageData.setBuyerMobile(requestPlaceOrder.getUserMobile());

        new AsyncTask<Void, Void, Void>() {
            String orders_list_json = "";
            @Override
            protected Void doInBackground(Void... voids) {
                orders_list_json = new Gson().toJson(new GroupedOrders("", ordersList));
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                ArrayList<String> imageURLs = new ArrayList<>();
                for (int i=0;i<ordersList.size();i++){
                    OrderDetails orderDetails = ordersList.get(i);
                    if ( orderDetails != null && orderDetails.getOrderItems() != null ) {
                        for (int j = 0; j < orderDetails.getOrderItems().size(); j++) {
                            imageURLs.add(orderDetails.getOrderItems().get(j).getMeatItemDetails().getThumbnailImageURL());
                        }
                    }
                }

                Intent orderSuccessAct = new Intent(context, OrderSuccessActivity.class);
                orderSuccessAct.putStringArrayListExtra("imageURLs", imageURLs);
                orderSuccessAct.putExtra(Constants.GROUPED_ORDERS, orders_list_json);
                if (requestPlaceOrder.getDeliverBy().equalsIgnoreCase("immediate")) {
                    orderSuccessAct.putExtra("isImmediate", true);
                }
                startActivity(orderSuccessAct);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
 */