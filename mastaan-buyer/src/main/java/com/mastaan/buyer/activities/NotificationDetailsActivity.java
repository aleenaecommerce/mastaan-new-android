package com.mastaan.buyer.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.NotificationDetails;
import com.squareup.picasso.Picasso;

/**
 * Created by Venkatesh Uppu on 07/09/18.
 */

public class NotificationDetailsActivity extends MastaanToolBarActivity {

    NotificationDetails notificationDetails;

    ImageView image;
    ImageView icon;
    TextView title;
    TextView details;
    TextView action;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_details);
        hasLoadingView();

        getGoogleAnalyticsMethods().sendScreenName("Notification Details");     // Google Analytics Screen Name

        //----------

        image = findViewById(R.id.image);
        icon = findViewById(R.id.icon);
        title = findViewById(R.id.title);
        details = findViewById(R.id.details);
        action = findViewById(R.id.action);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if ( notificationDetails.getURL() != null && notificationDetails.getURL().trim().length() > 0 ) {
                        if ( getIntent().getStringExtra(Constants.ANALYTICS_EVENT_CATEGORY) != null && getIntent().getStringExtra(Constants.ANALYTICS_EVENT_ACTION) != null ){
                            getGoogleAnalyticsMethods().sendEvent(getIntent().getStringExtra(Constants.ANALYTICS_EVENT_CATEGORY), getIntent().getStringExtra(Constants.ANALYTICS_EVENT_ACTION), "Action Click");
                        }

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(notificationDetails.getFormattedURL()));
                        startActivity(browserIntent);
                    }
                }catch (Exception e){showToastMessage("Something went wrong, try again!");}
            }
        });

        //----------

        showLoadingIndicator("Loading details...");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    if ( getIntent().getStringExtra(Constants.NOTIFICATION_DETAILS) != null ) {
                        notificationDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.NOTIFICATION_DETAILS), NotificationDetails.class);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                  //  Crashlytics.logException(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( getIntent().getStringExtra(Constants.ANALYTICS_EVENT_CATEGORY) != null && getIntent().getStringExtra(Constants.ANALYTICS_EVENT_ACTION) != null ){
                    getGoogleAnalyticsMethods().sendEvent(getIntent().getStringExtra(Constants.ANALYTICS_EVENT_CATEGORY), getIntent().getStringExtra(Constants.ANALYTICS_EVENT_ACTION), "Open");
                }
                display();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void display(){

        if ( notificationDetails != null ){
            switchToContentPage();

            if ( notificationDetails.getImage() != null && notificationDetails.getImage().length() > 0 ){
                Picasso.get()
                        .load(notificationDetails.getImage())
                        .placeholder(R.drawable.image_default)
                        .error(R.drawable.image_default)
                        .fit()
                        .centerCrop()
                        .tag(context)
                        .into(image);
            }else{  image.setVisibility(View.GONE); }

            if ( notificationDetails.getIcon() != null && notificationDetails.getIcon().length() > 0 ){
                Picasso.get()
                        .load(notificationDetails.getIcon())
                        .placeholder(R.drawable.image_default)
                        .error(R.drawable.image_default)
                        .fit()
                        .centerCrop()
                        .tag(context)
                        .into(icon);
            }else{  icon.setVisibility(View.GONE); }

            title.setText(CommonMethods.fromHtml(notificationDetails.getTitle()));
            details.setText(CommonMethods.fromHtml(notificationDetails.getDetails()));

            if ( notificationDetails.getURL() != null && notificationDetails.getURL().trim().length() > 0
                    && notificationDetails.getAction() != null && notificationDetails.getAction().trim().length() > 0
                    ){
                action.setVisibility(View.VISIBLE);
                action.setText(notificationDetails.getAction());
            }else{
                action.setVisibility(View.GONE);
            }
        }
    }

    //=============

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
