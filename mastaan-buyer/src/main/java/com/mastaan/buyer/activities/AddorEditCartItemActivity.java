package com.mastaan.buyer.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.Response;
import com.aleena.common.widgets.vGridView;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vScrollView;
import com.aleena.common.widgets.vSpinner;
import com.google.gson.Gson;
import com.mastaan.buyer.BuildConfig;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.AttributeOptionsAdapter;
import com.mastaan.buyer.adapters.AttributeOptionsIconGridAdapter;
import com.mastaan.buyer.adapters.DateSelectionAdapter;
import com.mastaan.buyer.backend.CartAPI;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.backend.MeatItemsAPI;
import com.mastaan.buyer.backend.StockAPI;
import com.mastaan.buyer.backend.models.RequestAddToCart;
import com.mastaan.buyer.backend.models.RequestSlotsForDate;
import com.mastaan.buyer.backend.models.RequestUpdateCartItem;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.interfaces.AttributeOptionCallbak;
import com.mastaan.buyer.interfaces.RegistrationCallback;
import com.mastaan.buyer.models.AttributeDetails;
import com.mastaan.buyer.models.AttributeOptionDetails;
import com.mastaan.buyer.models.AvailabilityDetails;
import com.mastaan.buyer.models.BuyerDetails;
import com.mastaan.buyer.models.CartItemDetails;
import com.mastaan.buyer.models.CartTotalDetails;
import com.mastaan.buyer.models.DayDetails;
import com.mastaan.buyer.models.MeatItemDetails;
import com.mastaan.buyer.models.SelectedAttributeDetails;
import com.mastaan.buyer.models.SlotDetails;
import com.mastaan.buyer.models.WarehouseMeatItemDetails;
import com.mastaan.buyer.models.WarehouseMeatItemStockDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AddorEditCartItemActivity extends MastaanToolBarActivity implements View.OnClickListener{

    String actionType ="";

    BuyerDetails buyerDetails;
    String serverTime;

    String categoryID;
    String categoryValue;

    CartItemDetails cartItemDetails;
    WarehouseMeatItemDetails warehouseMeatItemDetails;
    WarehouseMeatItemStockDetails warehouseMeatItemStockDetails;
    //List<OptionAvailabilities> attributeOptionsAvailabilitiesList;

    //---------

    vScrollView scrollView;

    ImageView item_image;
    TextView highlight;
    View pre_order_status;
    View add_on;
    TextView preparation_time;
    TextView weight_vary_info;
    View showDetails;
    View showNutritionalFacts;
    View showRecipes;

    View disableDateSelection;
    LinearLayout dateHolder;
    View attributesLoadingIndicator;
    View selectionHolder;
    View optionsHolder;
    LinearLayout weightHolder;
    LinearLayout attributesHolder;
    //View enterSpecialInstrutions;
    //TextView special_instructions;

    View actionsHolder;
    Button moreOptions;
    FrameLayout confirmButton;
    TextView confirmActionName;
    TextView total_price;

    //------

    List<View> savedAttributesViews = new ArrayList<>();

    double meatItemBasePrice;
    String selectedDate;
    double selectedQuantity;
    double totalPrice;
    double totalAttributesPrice;
    double totalWeightDifference;
    String weightQuantityType = "";
    String weightQuantityUnit = "";

    View dateSelectionDialogView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_cart_item);
        hasLoadingView();

        try{
            if ( getIntent().getStringExtra(Constants.COLOR_PRIMARY) != null  ){
                setThemeColors(Color.parseColor(getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK)!=null?getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK):getIntent().getStringExtra(Constants.COLOR_PRIMARY))
                        , Color.parseColor(getIntent().getStringExtra(Constants.COLOR_PRIMARY))
                );
            }
        }catch (Exception e){}

        buyerDetails = localStorageData.getBuyerDetails();
        serverTime = localStorageData.getServerTime();

        dateSelectionDialogView = inflater.inflate(R.layout.dialog_date_selection, null);

        //-----------

        item_image = findViewById(R.id.item_image);
        highlight = findViewById(R.id.highlight);
        pre_order_status = findViewById(R.id.pre_order_status);
        add_on = findViewById(R.id.add_on);
        preparation_time = findViewById(R.id.preparation_time);
        weight_vary_info = findViewById(R.id.weight_vary_info);
        showDetails = findViewById(R.id.showDetails);
        showDetails.setOnClickListener(this);
        showNutritionalFacts = findViewById(R.id.showNutritionalFacts);
        showNutritionalFacts.setOnClickListener(this);
        showRecipes = findViewById(R.id.showRecipes);
        showRecipes.setOnClickListener(this);

        disableDateSelection = findViewById(R.id.disableDateSelection);
        dateHolder = findViewById(R.id.dateHolder);
        attributesLoadingIndicator = findViewById(R.id.attributesLoadingIndicator);
        selectionHolder = findViewById(R.id.selectionHolder);
        optionsHolder = findViewById(R.id.optionsHolder);
        weightHolder = findViewById(R.id.weightHolder);
        attributesHolder = findViewById(R.id.attributesHolder);
        //enterSpecialInstrutions = findViewById(R.id.enterSpecialInstrutions);
        //enterSpecialInstrutions.setOnClickListener(this);
        //special_instructions = findViewById(R.id.special_instructions);

        total_price = findViewById(R.id.total_price);

        actionsHolder = findViewById(R.id.actionsHolder);

        moreOptions = (Button) findViewById(R.id.moreOptions);
        moreOptions.setOnClickListener(this);

        confirmButton = findViewById(R.id.addToCart);
        confirmButton.setOnClickListener(this);
        confirmActionName = findViewById(R.id.confirmActionName);

        //---------

        if ( getIntent().getStringExtra("meatItemName") != null ){//warehouseMeatItemDetails != null && warehouseMeatItemDetails.getMeatItemDetais().getName() != null && warehouseMeatItemDetails.getMeatItemDetais().getName().length() > 0 ){
            setToolBarTitle(getIntent().getStringExtra("meatItemName"));
        }

        categoryID = getIntent().getStringExtra(Constants.CATEGORY_ID);
        categoryValue = getIntent().getStringExtra(Constants.CATEGORY_VALUE);

        //-------------

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                actionType = getIntent().getStringExtra("type");
                if ( actionType == null ){ actionType ="Add"; }

                if ( actionType.equalsIgnoreCase("Add") ) {
                    String meat_item_details_json = getIntent().getStringExtra("warehouseMeatItemDetails");
                    warehouseMeatItemDetails = new Gson().fromJson(meat_item_details_json, WarehouseMeatItemDetails.class);

                    getGoogleAnalyticsMethods().sendScreenName("MeatItem");//warehouseMeatItemDetails.getName());       // Send ScreenNames to Analytics
                }
                else if ( actionType.equalsIgnoreCase("Update") ) {
                    String cart_item_details_json = getIntent().getStringExtra("cartItemDetails");
                    cartItemDetails = new Gson().fromJson(cart_item_details_json, CartItemDetails.class);
                    if ( cartItemDetails != null ) {
                        selectedDate = DateMethods.getReadableDateFromUTC(cartItemDetails.getDeliveryByDate());
                        warehouseMeatItemDetails = cartItemDetails.getWarehouseMeatItemDetails();
                    }

                    getGoogleAnalyticsMethods().sendScreenName("Edit "+"MeatItem");//warehouseMeatItemDetails.getName());       // Send ScreenNames to Analytics
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if ( warehouseMeatItemDetails != null ) {
                    confirmButton.setEnabled(true);
                    if ( actionType.equalsIgnoreCase("Add") ) {
                        confirmActionName.setText("ADD TO CART");
//                        getMeatItemPreferences();
                    }else if ( actionType.equalsIgnoreCase("Update") ) {
                        confirmActionName.setText("UPDATE ITEM");
//                        displayItemDetails();
                    }
                    getPreferencesAndProceed();
                }
                else{
                    confirmButton.setEnabled(false);
                    showToastMessage("Something went wrong");
                }

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        //-----------------------------

        scrollView = findViewById(R.id.scrollView);
        scrollView.setScrollViewListener(new vScrollView.ScrollViewListener() {
            boolean shownAllAttributes = false;
            @Override
            public void onScrollChange(vScrollView scrollView, boolean isScrollEnd, int x, int y, int oldx, int oldy) {
                if ( shownAllAttributes || isScrollEnd || scrollView.isScrollable() == false ){
                    shownAllAttributes = true;
                    toggleAddToCart(true);
                }else{
                    toggleAddToCart(false);
                }
            }
        });

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPreferencesAndProceed();
    }

    @Override
    public void onClick(View view) {

        if ( view == showDetails /*|| view == showNutritionalFacts*/ || view == showRecipes ){
            Intent webviewAct = new Intent(context, WebviewActivity.class);
            webviewAct.putExtra(Constants.COLOR_PRIMARY_DARK, getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK));
            webviewAct.putExtra(Constants.COLOR_PRIMARY, getIntent().getStringExtra(Constants.COLOR_PRIMARY));

            String url = "https://www.mastaan.com";
            if ( BuildConfig.PRODUCTION == false ){
                url = "http://d19scr6f6kme6j.cloudfront.net";
            }

            if ( view == showDetails ){
                webviewAct.putExtra(Constants.TITLE, "Product information");
                webviewAct.putExtra(Constants.ANALYTICS_TITLE, "Product Information");
                url += "/meatitem/details.html?id="+warehouseMeatItemDetails.getMeatItemDetais().getID();
            }/*else if ( view == showNutritionalFacts ){
                webviewAct.putExtra(Constants.TITLE, "Nutritional facts");
                webviewAct.putExtra(Constants.ANALYTICS_TITLE, "Product Nutritions");
                url += "/meatitem/nutritions.html?id="+warehouseMeatItemDetails.getMeatItemDetais().getID();
            }*/else if ( view == showRecipes ){
                webviewAct.putExtra(Constants.TITLE, "Recipes from our customers");
                webviewAct.putExtra(Constants.ANALYTICS_TITLE, "Product Recipes");
                url += "/meatitem/recipes.html?id="+warehouseMeatItemDetails.getMeatItemDetais().getID();
            }

            /*String content = "";
            String stylesheet = "";
            if ( view == showDetails ){
                webviewAct.putExtra(Constants.TITLE, "Product information");
                webviewAct.putExtra(Constants.ANALYTICS_TITLE, "Product Information");
                content = warehouseMeatItemDetails.getMeatItemDetais().getDetails();
                stylesheet = "details_styles.css";
            }else if ( view == showNutritionalFacts ){
                webviewAct.putExtra(Constants.TITLE, "Nutritional facts");
                webviewAct.putExtra(Constants.ANALYTICS_TITLE, "Product Nutritional Facts");
                content = warehouseMeatItemDetails.getMeatItemDetais().getNutritionsImage();
                stylesheet = "nutrition_styles.css";
            }else if ( view == showRecipes ){
                webviewAct.putExtra(Constants.TITLE, "Recipes from our customers");
                webviewAct.putExtra(Constants.ANALYTICS_TITLE, "Product Recipes");
                content = warehouseMeatItemDetails.getMeatItemDetais().getRecipes();
                stylesheet = "details_styles.css";
            }
            webviewAct.putExtra(Constants.CONTENT, "<html>" +
                    "<head><link href=\""+stylesheet+"\" type=\"text/css\" rel=\"stylesheet\"/></head>" +
                    "<body>" + content + "</body>" +
                    "</html>");*/

            webviewAct.putExtra(Constants.URL, url);
            startActivity(webviewAct);
        }

        else if ( view == showNutritionalFacts ){
            //new ImageDialog(activity).show(warehouseMeatItemDetails.getMeatItemDetais().getNutritionsImage());
            Intent nutritionsAct = new Intent(context, NutritionsActivity.class);
            nutritionsAct.putExtra(Constants.URL, warehouseMeatItemDetails.getMeatItemDetais().getNutritionsImage());
            nutritionsAct.putExtra(Constants.COLOR_PRIMARY_DARK, getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK));
            nutritionsAct.putExtra(Constants.COLOR_PRIMARY, getIntent().getStringExtra(Constants.COLOR_PRIMARY));
            startActivity(nutritionsAct);
        }
        /*else if ( view == enterSpecialInstrutions){
            showInputTextDialog("Special instructions", "Special instructions", special_instructions.getText().toString(), false, new TextInputCallback() {
                @Override
                public void onComplete(String inputText) {
                    special_instructions.setText(inputText);
                }
            });
        }*/

        else if ( view == moreOptions ){
            getGoogleAnalyticsMethods().sendEvent("MeatItem", "More options", ""/*"More options tap for " + warehouseMeatItemDetails.getMeatItemDetais().getName()*/);    // Send Event to Analytics
            toggleAddToCart(true);
            scrollView.scrollToEnd();
        }

        else if ( view == confirmButton){
            if ( localStorageData.getSessionFlag() ){
                if ( selectedDate != null && selectedDate.trim().length() > 0 && selectedDate.equalsIgnoreCase("select") == false ) {
                    if ( actionType.equalsIgnoreCase("Add") && warehouseMeatItemDetails.getMeatItemDetais().getSize() != null && warehouseMeatItemDetails.getMeatItemDetais().getSize().trim().length() > 0 ){
                        showChoiceSelectionDialog("Confirm", CommonMethods.fromHtml(warehouseMeatItemDetails.getMeatItemDetais().getName() + " is available in <b>" + warehouseMeatItemDetails.getMeatItemDetais().getSize() + "</b> size variant. Do you want to continue?"), "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("YES") ){
                                    getSlotsForDateAndProceed(selectedDate);
                                }
                            }
                        });
                    }else{
                        getSlotsForDateAndProceed(selectedDate);
                    }
                }else{
                    showSnackbarMessage("* Please select date");
                }
            }else{
                goToRegistration(new RegistrationCallback() {
                    @Override
                    public void onComplete(boolean status, String token, BuyerDetails buyerDetails) {
                        if (status) {
                            confirmButton.performClick();
                        }
                    }
                });
            }
        }
    }

    public void toggleAddToCart(boolean show){
        if ( show ){
            moreOptions.setVisibility(View.GONE);
            confirmButton.setVisibility(View.VISIBLE);
        }else{
            confirmButton.setVisibility(View.GONE);
            moreOptions.setVisibility(View.VISIBLE);
        }
    }
    public void toggleAttributes(boolean show){
        if ( show ){
            selectionHolder.setVisibility(View.VISIBLE);
            actionsHolder.setVisibility(View.VISIBLE);
            toggleAddToCart(false);
            new CountDownTimer(900, 100) {
                @Override
                public void onTick(long l) {
                }

                @Override
                public void onFinish() {
                    if ( scrollView.isScrollEnded() == false){
                        toggleAddToCart(false);
                    }else{ toggleAddToCart(true);  }
                }
            }.start();
        }
        else{
            selectionHolder.setVisibility(View.GONE);
            actionsHolder.setVisibility(View.GONE);
            if ( savedAttributesViews.size() > 0 ){ scrollView.focusToViewTop(savedAttributesViews.get(0));}
        }
    }

    //-------- UI Methods

    public void displayItemDetails(){

        MeatItemDetails meatItemDetails = warehouseMeatItemDetails.getMeatItemDetais();

        selectionHolder.setVisibility(View.GONE);
        actionsHolder.setVisibility(View.GONE);

        meatItemBasePrice = meatItemDetails.getSellingPrice();
        totalAttributesPrice = 0;
        totalPrice = meatItemBasePrice + totalAttributesPrice;
        selectedQuantity = 1;

        setToolBarTitle(meatItemDetails.getNameWithCategoryAndSize()+" ");

        if ( meatItemDetails.getPreparationHours() > 0 ){
            preparation_time.setText(DateMethods.getTimeStringFromMinutes((int) (meatItemDetails.getPreparationHours() * 60), "hr", "min"));
            findViewById(R.id.preparationTimeView).setVisibility(View.VISIBLE);
        }

        if ( warehouseMeatItemDetails.getHighlightText() != null && warehouseMeatItemDetails.getHighlightText().trim().length() > 0 ){
            highlight.setVisibility(View.VISIBLE);
            highlight.setText(warehouseMeatItemDetails.getHighlightText());
        }else{  highlight.setVisibility(View.GONE); }

        pre_order_status.setVisibility(meatItemDetails.isPreOrderable()?View.VISIBLE:View.GONE);
        add_on.setVisibility(meatItemDetails.isAddOn()?View.VISIBLE:View.GONE);

        Picasso.get()
                .load(meatItemDetails.getImageURL())
                .placeholder(R.drawable.image_default_mastaan)
                .error(R.drawable.image_default_mastaan)
                .tag(context)
                .into(item_image);

        showDetails.setVisibility((meatItemDetails.getDetails() != null && meatItemDetails.getDetails().trim().length() > 0)?View.VISIBLE:View.GONE);
        showNutritionalFacts.setVisibility((meatItemDetails.getNutritionsImage() != null && meatItemDetails.getNutritionsImage().trim().length() > 0)?View.VISIBLE:View.GONE);
        showRecipes.setVisibility((meatItemDetails.getRecipes() != null && meatItemDetails.getRecipes().trim().length() > 0)?View.VISIBLE:View.GONE);

        //--------------------------------

        List<Double> tempWeights = new ArrayList<>();
        if ( meatItemDetails.getQuantityType().equalsIgnoreCase("n") ){
            tempWeights = localStorageData.getMeatItemPieces();
            weightQuantityType = "No. of pieces";
            weightQuantityUnit = "piece";
        }
        else if ( meatItemDetails.getQuantityType().equalsIgnoreCase("w") ){
            tempWeights = localStorageData.getMeatItemWeights();
            weightQuantityType = "Weight";
            weightQuantityUnit = "kg";
        }
        else if ( meatItemDetails.getQuantityType().equalsIgnoreCase("s") ){
            tempWeights = localStorageData.getMeatItemSets();
            weightQuantityType = "No. of sets";
            weightQuantityUnit = "set";
        }

        final List<Double> weights = new ArrayList<>();
        final List<AttributeOptionDetails> weightsStrings = new ArrayList<>();

        // Getting max weight from stock for limited stock item in Add mode
        double maxWeight = -1;
        if ( actionType.equalsIgnoreCase("Add") && warehouseMeatItemStockDetails != null
                && (warehouseMeatItemStockDetails.getType().equalsIgnoreCase("l") || warehouseMeatItemStockDetails.getType().equalsIgnoreCase("d")) ){
                //&& warehouseMeatItemStockDetails.getQuantity() >= meatItemDetails.getMinimumWeightToAddToCart() ){
            maxWeight = warehouseMeatItemStockDetails.getQuantity();
        }

        for ( int w=0;w<tempWeights.size();w++){
            if ( tempWeights.get(w) >= meatItemDetails.getMinimumWeightToAddToCart()
                    && (maxWeight == -1 || tempWeights.get(w) <= maxWeight) ) {
                weights.add(tempWeights.get(w));
                weightsStrings.add(new AttributeOptionDetails("", "", CommonMethods.getInDecimalFormat(tempWeights.get(w)) + " " + weightQuantityUnit + (tempWeights.get(w)==1?"":"s")));
            }
        }

        //=========== DATE VIEW ==========//

        int selectedDateIndex = 0;
        String selectedCartItemDate = "";
        if ( cartItemDetails != null ){selectedCartItemDate=DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(cartItemDetails.getDeliveryByDate()));}
        if ( selectedCartItemDate == null || selectedCartItemDate.length() == 0 ){  selectedCartItemDate = DateMethods.getOnlyDate(localStorageData.getMeatItemPrefillDate()); }

        final List<AttributeOptionDetails> datesList = new ArrayList<>();
        datesList.add(new AttributeOptionDetails("select", "Select", "", 0, true));

        List<DayDetails> disabledDates = localStorageData.getDisabledDates();
        try {
            if (meatItemDetails.isPreOrderable() && meatItemDetails.getPreOrderCutoffTime() != null && meatItemDetails.getPreOrderCutoffTime().trim().length() > 0) {
                // Disabling tomorrow if pre order cut off time reached
                if (DateMethods.compareDates(DateMethods.getOnlyTime(localStorageData.getServerTime()), meatItemDetails.getPreOrderCutoffTime()) > 0) {
                    disabledDates.add(new DayDetails(DateMethods.getOnlyDate(DateMethods.addToDateInDays(localStorageData.getServerTime(), 1)), "Unavailable because you can only order the item by " + DateMethods.getTimeIn12HrFormat(meatItemDetails.getPreOrderCutoffTime()) + " for next day delivery", 0, 0));
                }
            }
        }catch (Exception e){}
        final List<AvailabilityDetails> availableDates = warehouseMeatItemDetails.getFullAvailabilitiesList(localStorageData.getServerTime(), localStorageData.getDaysToEnableSlots(), disabledDates, actionType.equalsIgnoreCase("Add")?warehouseMeatItemStockDetails:null, serverTime, localStorageData.getDeliveringHoursEndTime());
        for (int i=0;i<availableDates.size();i++){
            datesList.add(new AttributeOptionDetails(availableDates.get(i), serverTime));
            if ( (selectedCartItemDate != null && selectedCartItemDate.length() > 0) && selectedDateIndex == 0 && DateMethods.compareDates(availableDates.get(i).getDate(), selectedCartItemDate) == 0 ){
                selectedDateIndex = i+1;
            }
        }

        if ( selectedDateIndex != 0 ){ toggleAttributes(true);}
        dateHolder.addView(getAttributeView(null, "dsd", "Delivery date", false, weightQuantityUnit, datesList, "", selectedDateIndex, new AttributeOptionCallbak() {
            @Override
            public void onSelect(int previousPosition, int currentPosition) {
                selectedDate = datesList.get(currentPosition).getDate();

                if ( selectedDate == null || selectedDate.length() == 0 || selectedDate.equalsIgnoreCase("select")  ){
                    toggleAttributes(false);
                }else {
                    toggleAttributes(true);
                    //getSlotsForDate(selectedDate, false);

                    displayAttributesForDate(selectedDate);

                    meatItemBasePrice = datesList.get(currentPosition).getPriceDifference();//weights.get(currentPosition);
                    updatePrice();
                }
            }
        }));


        //=========== WEIGHT VIEW ==========//

        int selectedKgIndex = 0;
        if ( cartItemDetails != null ) {
            for (int w = 0; w < weights.size(); w++) {
                if (cartItemDetails.getWeight() == weights.get(w)) {
                    selectedKgIndex = w;
                }
            }
        }

        weightHolder.addView(getAttributeView(null, "dd", weightQuantityType, false, "", weightsStrings, "", selectedKgIndex, new AttributeOptionCallbak() {
            @Override
            public void onSelect(int previousPosition, int currentPosition) {
                selectedQuantity = weights.get(currentPosition);
                updatePrice();
            }
        }));

        /*if ( cartItemDetails != null ){
            special_instructions.setText(cartItemDetails.getSpecialInstructions());
        }*/

        switchToContentPage();
    }

    public void displayAttributesForDate(final String date){
        savedAttributesViews.clear();
        attributesHolder.removeAllViews();

        disableDateSelection.setVisibility(View.VISIBLE);
        attributesLoadingIndicator.setVisibility(View.VISIBLE);
        optionsHolder.setVisibility(View.GONE);
        actionsHolder.setVisibility(View.GONE);
        new AsyncTask<Void, Void, Void>() {
            List<AttributeDetails> attributesForDate = new ArrayList<>();

            @Override
            protected Void doInBackground(Void... voids) {
                List<AttributeDetails> allAttributes = warehouseMeatItemDetails.getMeatItemDetais().getAttributes();
                for (int i=0;i<allAttributes.size();i++){
                    AttributeDetails availableAttributeDetails = allAttributes.get(i);
                    AttributeDetails attributeDetailsForDate = new AttributeDetails(availableAttributeDetails.getID(), availableAttributeDetails.getName(), availableAttributeDetails.isItemLevelAttribute(), new ArrayList<AttributeOptionDetails>(), availableAttributeDetails.getOptionsDisplayType());

                    List<AttributeOptionDetails> allAttributeOptions = availableAttributeDetails.getAvailableOptions();
                    for (int j=0;j<allAttributeOptions.size();j++){
                        AttributeOptionDetails optionDetailsForDate = allAttributeOptions.get(j).getOptionDetailsForDate(date);
                        attributeDetailsForDate.addOption(optionDetailsForDate);
                    }

                    attributesForDate.add(attributeDetailsForDate);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                disableDateSelection.setVisibility(View.GONE);
                attributesLoadingIndicator.setVisibility(View.GONE);
                optionsHolder.setVisibility(View.VISIBLE);
                actionsHolder.setVisibility(View.VISIBLE);

                for (int i = 0; i< attributesForDate.size(); i++){
                    final AttributeDetails attributeDetails = attributesForDate.get(i);
                    attributesHolder.addView(getAttributeView(attributeDetails.getID(), attributeDetails.getOptionsDisplayType(), attributeDetails.getName(), attributeDetails.isItemLevelAttribute(), weightQuantityUnit, attributeDetails.getAvailableOptions(), "+ ", getSelectedOptionIndex(attributeDetails), new AttributeOptionCallbak() {
                        @Override
                        public void onSelect(int previousPosition, int currentPosition) {
                            if (previousPosition != -1) {
                                updateAttributes(attributeDetails.getAvailableOptions().get(currentPosition).getDisabledAttributes(), attributeDetails.getAvailableOptions().get(currentPosition).getDisabledAttributeOptions(), attributeDetails.getAvailableOptions().get(previousPosition).getDisabledAttributes(), attributeDetails.getAvailableOptions().get(previousPosition).getDisabledAttributeOptions());
                                updatePrice();
                            } else {
                                updateAttributes(attributeDetails.getAvailableOptions().get(currentPosition).getDisabledAttributes(), attributeDetails.getAvailableOptions().get(currentPosition).getDisabledAttributeOptions(), new ArrayList<String>(), new ArrayList<String>());
                                updatePrice();
                            }
                        }
                    }));
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void updateAttributes(List<String> disabledAttributes, List<String> disabledAttributeOptions, List<String> reEnableAttributes, List<String> reEnableAttributeOptions){

        for (int i = 0; i< savedAttributesViews.size(); i++){

            final AttributeDetails attributeDetails = warehouseMeatItemDetails.getMeatItemDetais().getAttributes().get(i);
            final List<AttributeOptionDetails> attributeOptions = warehouseMeatItemDetails.getMeatItemDetais().getAttributes().get(i).getAvailableOptions();

            final View attributeView = savedAttributesViews.get(i);
            final TextView name = attributeView.findViewById(R.id.name);
            final View disabledIndicator = attributeView.findViewById(R.id.disabledIndicator);
            final View optionsView = attributeView.findViewById(R.id.optionsView);
            final vSpinner options = attributeView.findViewById(R.id.options);
            final View not_applicable = attributeView.findViewById(R.id.not_applicable);

            for (int a=0;a<reEnableAttributes.size();a++){
                if ( attributeDetails.getID().equals(reEnableAttributes.get(a)) ){
                    name.setTextColor(context.getResources().getColor(R.color.black));
                    optionsView.setVisibility(View.VISIBLE);
                    attributeView.findViewById(R.id.not_applicable).setVisibility(View.GONE);
                    disabledIndicator.setVisibility(View.GONE);
                    break;
                }
            }

            for (int a=0;a<disabledAttributes.size();a++){
                if ( attributeDetails.getID().equals(disabledAttributes.get(a)) ){
                    name.setTextColor(context.getResources().getColor(R.color.disabled_color));
                    optionsView.setVisibility(View.INVISIBLE);
                    attributeView.findViewById(R.id.not_applicable).setVisibility(View.VISIBLE);
                    disabledIndicator.setVisibility(View.VISIBLE);
                    break;
                }
            }

            AttributeOptionsAdapter adapter = (AttributeOptionsAdapter) options.getAdapter();
            for (int ao=0;ao<attributeOptions.size();ao++){
                if ( reEnableAttributeOptions.contains(attributeOptions.get(ao).getID()) ){
                    adapter.enableItem(attributeOptions.get(ao));
                    // SELECTING FIRST ENABLED OPTION IF CURRENT SELECTED OPTION IS DISABLED AFTER CHANGES
                    if ( adapter.getItem(options.getSelectedItemPosition()).isAvailable() == false/*adapter.getItem(options.getSelectedItemPosition()).getStatus() == false
                            || adapter.getItem(options.getSelectedItemPosition()).isEnabled() == false*/ ){
                        options.setSelection(adapter.getFirstEnabledItemIndex());
                    }
                }
            }

            for (int ao=0;ao<attributeOptions.size();ao++){
                if ( disabledAttributeOptions.contains(attributeOptions.get(ao).getID()) ){
                    adapter.disableItem(attributeOptions.get(ao).getID());
                    // SELECTING FIRST ENABLED OPTION IF CURRENT SELECTED OPTION IS DISABLED AFTER CHANGES
                    if ( adapter.getItem(options.getSelectedItemPosition()).isAvailable() == false/*adapter.getItem(options.getSelectedItemPosition()).getStatus() == false
                        || adapter.getItem(options.getSelectedItemPosition()).isEnabled() == false*/ ){
                        options.setSelection(adapter.getFirstEnabledItemIndex());
                    }
                }
            }

            //------

            // DISABLING ATTRIBUTE IF ALL OPTIONS ARE UNAVAILABLE AFTER CHANGES
            if ( reEnableAttributes.contains(attributeDetails.getID())
                    && disabledAttributes.contains(attributeDetails.getID()) == false ) {
                if (adapter.isAtleastOneOptionAvailable()) {
                    name.setTextColor(context.getResources().getColor(R.color.black));
                    optionsView.setVisibility(View.VISIBLE);
                    not_applicable.setVisibility(View.GONE);
                    disabledIndicator.setVisibility(View.GONE);
                } else {
                    name.setTextColor(context.getResources().getColor(R.color.disabled_color));
                    optionsView.setVisibility(View.GONE);
                    not_applicable.setVisibility(View.VISIBLE);
                    disabledIndicator.setVisibility(View.VISIBLE);
                }
            }

        }
    }

    public void updatePrice(){

        totalAttributesPrice = 0;
        totalWeightDifference = 0;

        for (int i = 0; i< savedAttributesViews.size(); i++){
            final View attributeView = savedAttributesViews.get(i);
            TextView name = attributeView.findViewById(R.id.name);
            if ( attributeView.findViewById(R.id.disabledIndicator).getVisibility() == View.GONE ){
                try {
                    final vSpinner options = (vSpinner) attributeView.findViewById(R.id.options);
                    AttributeOptionDetails optionDetails = (AttributeOptionDetails) options.getSelectedItem();
                    totalAttributesPrice += optionDetails.getPriceDifference();
                    totalWeightDifference += optionDetails.getWeightDifferencePercentage();
                }catch (Exception e){
                    e.printStackTrace();
                    Log.d(Constants.LOG_TAG, "Error for " + name.getText().toString().toUpperCase());
                }
            }
        }
        totalPrice = (meatItemBasePrice + totalAttributesPrice)* selectedQuantity;
        total_price.setText("(" + RS + " " + CommonMethods.getInDecimalFormat(Math.round(totalPrice)) + ")");

        // Updating Weight Difference Info
        if (totalWeightDifference > 0) {
            weight_vary_info.setVisibility(View.VISIBLE);
            weight_vary_info.setText("The final weight of the item will be approximately " + (selectedQuantity - ((selectedQuantity * totalWeightDifference)) / 100) + " kgs");
        } else {
            if ( warehouseMeatItemDetails.getMeatItemDetais().getCategoryValue().equals("freshwatersf") || warehouseMeatItemDetails.getMeatItemDetais().getCategoryValue().equals("seawatersf")) {
                weight_vary_info.setVisibility(View.VISIBLE);
                weight_vary_info.setText("Final weight of the item will be reduced after cleaning and processing");
            } else {
                weight_vary_info.setVisibility(View.GONE);
            }
        }

        Log.d(Constants.LOG_TAG, "Final price is "+ totalPrice);
    }

    public View getAttributeView(final String attributeID, final String optionsViewType, final String attributeName, final boolean itemLevelAttribute, final String weightQuantityUnit, final List<AttributeOptionDetails> attributeOptions, final String priceIndicatorText, final int startValue, final AttributeOptionCallbak callback){

        List<String> availableOptionsNames = new ArrayList<>();
        for (int j=0;j<attributeOptions.size();j++){
            String optionName = attributeOptions.get(j).getName();
            if ( attributeOptions.get(j).getPriceDifference() > 0 ) {
                optionName = attributeOptions.get(j).getName() + " ("+priceIndicatorText+"" + RS + CommonMethods.getInDecimalFormat(attributeOptions.get(j).getPriceDifference()) + "/"+weightQuantityUnit+")";
            }
            availableOptionsNames.add(optionName);
        }

        //UI

        final View attributeView = inflater.inflate(R.layout.view_attribute_item, null);
        TextView id = attributeView.findViewById(R.id.id);
        TextView item_level_attribute = attributeView.findViewById(R.id.item_level_attribute);
        TextView name = attributeView.findViewById(R.id.name);
        final View optionsView = attributeView.findViewById(R.id.optionsView);
        final vSpinner options = attributeView.findViewById(R.id.options);
        final View not_applicable = attributeView.findViewById(R.id.not_applicable);
        final View disabledIndicator = attributeView.findViewById(R.id.disabledIndicator);

        id.setText(attributeID);
        name.setText(attributeName);
        item_level_attribute.setText(itemLevelAttribute?"Yes":"No");

        //-------

        final AttributeOptionsAdapter attributeOptionsAdapter = new AttributeOptionsAdapter(context, attributeOptions, weightQuantityUnit, new AttributeOptionsAdapter.Callback() {
            @Override
            public void onShowDisabledReason(int position) {
                if ( attributeOptions.get(position).getAvailabilityReason() != null && attributeOptions.get(position).getAvailabilityReason().trim().length() > 0 ) {
                    showNotificationDialog("Message", attributeOptions.get(position).getAvailabilityReason());
                }
            }
        });//new ArrayAdapter<String> (this,android.R.layout.simple_dropdown_item_1line, attributeOptions);
        attributeOptionsAdapter.setPriceIndicatorText(priceIndicatorText);
        //attributeOptionsAdapter.setDropDownViewResource(R.layout.view_popup_list_item);
        options.setAdapter(attributeOptionsAdapter);//

        if ( attributeOptionsAdapter.isAtleastOneOptionAvailable() ) {
            name.setTextColor(context.getResources().getColor(R.color.black));
            optionsView.setVisibility(View.VISIBLE); not_applicable.setVisibility(View.GONE); disabledIndicator.setVisibility(View.GONE);

            if (attributeOptionsAdapter.getItem(startValue).isAvailable()) {//isEnabled() ) {
                options.setSelection(startValue);
            } else {
                options.setSelection(attributeOptionsAdapter.getFirstEnabledItemIndex());
            }
        }else{
            name.setTextColor(context.getResources().getColor(R.color.disabled_color));
            optionsView.setVisibility(View.GONE); not_applicable.setVisibility(View.VISIBLE); disabledIndicator.setVisibility(View.VISIBLE);
        }

        options.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previousPosition = -1;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( attributeOptionsAdapter.getItem(position).isAvailable() ) {
                    callback.onSelect(previousPosition, position);
                    previousPosition = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        options.setSpinnerEventsListener(new vSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened() {
                attributeView.findViewById(R.id.opened_indicator).setVisibility(View.VISIBLE);
            }

            @Override
            public void onSpinnerClosed() {
                attributeView.findViewById(R.id.opened_indicator).setVisibility(View.GONE);
            }
        });

        if ( attributeView.findViewById(R.id.attributeSelector) != null ) {

            attributeView.findViewById(R.id.attributeSelector).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if ( optionsViewType.equalsIgnoreCase("dd") ) { //DropDown
                        options.performClick();
                    }
                    else if ( optionsViewType.equalsIgnoreCase("rb") ){    //  RadiobButtons
                        options.performClick();
                    }
                    else if ( optionsViewType.equalsIgnoreCase("ig") ){    // IconGrid

                        //IF ATTRIBUTE OPTIONS LESS THAN 5 THEN SHOWING ATCITVITY
                        if ( attributeOptions.size() > 4 ){
                            AttributeOptionsAdapter attributeOptionsAdapter = (AttributeOptionsAdapter) options.getAdapter();
                            startIconGridActivity(attributeName, attributeOptionsAdapter.getAllItems(), new AttributeOptionCallbak() {
                                @Override
                                public void onSelect(int previousPosition, int currentPosition) {
                                    options.setSelection(currentPosition);
                                }
                            });
                        }

                        // IF ATTRIBUTE OPTIONS LESS THAN 5 THEN SHOWING DIALOG
                        else{
                            View iconGridAttributeOptionsDialogView = inflater.inflate(R.layout.dialog_icongrid_attribute_options, null);
                            TextView title = iconGridAttributeOptionsDialogView.findViewById(R.id.name);
                            title.setText("Select " + attributeName);

                            vGridView optionsHolder = (vGridView) iconGridAttributeOptionsDialogView.findViewById(R.id.optionsHolder);
                            optionsHolder.setExpanded(true);
                            optionsHolder.setAdapter(new AttributeOptionsIconGridAdapter(context, R.layout.view_attribute_option_icongrid, attributeOptions, new AttributeOptionsIconGridAdapter.Callback() {
                                @Override
                                public void onItemClick(int position, AttributeOptionDetails optionDetails) {
                                    options.setSelection(position);
                                    closeCustomDialog();
                                }
                            }));
                            showCustomDialog(iconGridAttributeOptionsDialogView, true);
                        }

                    }
                    else if ( optionsViewType.equalsIgnoreCase("dsd") ) {    // DateSelectionDialog
                        vRecyclerView datesHolder = (vRecyclerView) dateSelectionDialogView.findViewById(R.id.datesHolder);
                        datesHolder.setHasFixedSize(true);
                        datesHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
                        datesHolder.setItemAnimator(new DefaultItemAnimator());

                        List<AttributeOptionDetails> optionsList = new ArrayList<>();
                        for (int i=1;i<attributeOptions.size();i++){
                            optionsList.add(attributeOptions.get(i));
                        }
                        datesHolder.setAdapter(new DateSelectionAdapter(context, optionsList, new DateSelectionAdapter.Callback() {
                            @Override
                            public void onItemClick(int position) {
                                options.setSelection(position+1);
                                closeCustomDialog();
                            }
                        }));
                        showCustomDialog(dateSelectionDialogView);
                    }
                }
            });
        }

        savedAttributesViews.add(attributeView);

        return attributeView;
    }

    AttributeOptionCallbak attributeOptionCallbak;
    public void startIconGridActivity(String attributeName, List<AttributeOptionDetails> attributeOptions, AttributeOptionCallbak attributeOptionCallbak){
        this.attributeOptionCallbak = attributeOptionCallbak;

        ArrayList<String> attribute_options_jsons = new ArrayList<String>();
        for (int i = 0; i < attributeOptions.size(); i++) {
            attribute_options_jsons.add(new Gson().toJson(attributeOptions.get(i)));
        }

        Intent iconGridAttributeOptionsAct = new Intent(context, IconGridAttributeOptionsActivity.class);
        iconGridAttributeOptionsAct.putExtra("attributeName", attributeName);
        iconGridAttributeOptionsAct.putStringArrayListExtra("attribute_options_jsons", attribute_options_jsons);
        startActivityForResult(iconGridAttributeOptionsAct, Constants.ICONGRID_ATTRIBUTE_OPTIONS_ACTIVITY_CODE);

    }

    //---------------- GetMethods

    public List<SelectedAttributeDetails>  getSelectedAttributes(){
        List<SelectedAttributeDetails> selectedAttributes = new ArrayList<>();

        for (int i = 0; i< savedAttributesViews.size(); i++){
            final View attributeView = savedAttributesViews.get(i);
            if ( attributeView.findViewById(R.id.disabledIndicator).getVisibility() == View.GONE ){
                TextView id = attributeView.findViewById(R.id.id);
                vSpinner options = attributeView.findViewById(R.id.options);
                AttributeOptionDetails optionDetails = (AttributeOptionDetails) options.getSelectedItem();
                selectedAttributes.add(new SelectedAttributeDetails(/*warehouseMeatItemDetails.getMeatItemDetais().getAttributes().get(i).getID()*/id.getText().toString(), optionDetails.getID()));
            }
        }
        return selectedAttributes;
    }

    public List<String>  getSelectedItemLevelAttributesSelectedOptions(){
        List<String> selectedItemLevelAttributesOptions = new ArrayList<>();
        for (int i = 0; i< savedAttributesViews.size(); i++){
            final View attributeView = savedAttributesViews.get(i);
            if ( attributeView.findViewById(R.id.disabledIndicator).getVisibility() == View.GONE ){
                TextView item_level_attribute = attributeView.findViewById(R.id.item_level_attribute);
                if ( item_level_attribute.getText().toString().equalsIgnoreCase("Yes") ){
                    vSpinner options = attributeView.findViewById(R.id.options);
                    AttributeOptionDetails optionDetails = (AttributeOptionDetails) options.getSelectedItem();
                    selectedItemLevelAttributesOptions.add(optionDetails.getID());
                }
            }
        }
        return selectedItemLevelAttributesOptions;
    }

    public int getSelectedOptionIndex(AttributeDetails attributeDetails){

        int index = 0;

        if ( cartItemDetails != null && cartItemDetails.getSelectedAttributes() != null && cartItemDetails.getSelectedAttributes().size() > 0 ) {
            for (int i = 0; i < cartItemDetails.getSelectedAttributes().size(); i++) {
                if (attributeDetails.getID().equals(cartItemDetails.getSelectedAttributes().get(i).getAttributeID())) {
                    String selectedOptionID = cartItemDetails.getSelectedAttributes().get(i).getOptinID();
                    for (int j = 0; j < attributeDetails.getAvailableOptions().size(); j++) {
                        if (selectedOptionID.equals(attributeDetails.getAvailableOptions().get(j).getValue()) || selectedOptionID.equals(attributeDetails.getAvailableOptions().get(j).getID())) {
                            index = j;
                            break;
                        }
                    }
                }
            }
        }
        return index;
    }

    public void displaySlotsForDateAndProceed(final String date, final List<SlotDetails> slotsList){

        if ( slotsList != null && slotsList.size() > 0 ) {

            String prefillTime = null;
            if ( cartItemDetails != null && DateMethods.compareDates(date, DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(cartItemDetails.getDeliveryByDate()))) == 0 ){
                prefillTime = DateMethods.getTimeIn24HrFormat(DateMethods.getReadableDateFromUTC(cartItemDetails.getDeliveryByDate()));
            }

            int prefillSlotIndex = -1;
            final List<String> slotStrings = new ArrayList<>();
            for (int i = 0; i < slotsList.size(); i++) {
                if (buyerDetails.isSuperBuyer()) {
                    slotStrings.add(slotsList.get(i).getDisplayText() + " (" + slotsList.get(i).getOrdersCount() + "/" + slotsList.get(i).getMaximumAllowedOrdersCount() + ")");
                } else {
                    slotStrings.add(slotsList.get(i).getDisplayText());
                }

                if ( prefillTime != null && prefillSlotIndex == -1
                        && DateMethods.compareDates(slotsList.get(i).getEndTime(), prefillTime) == 0 ){
                    prefillSlotIndex = i;
                }
            }
            slotStrings.add("Need help ordering?\nCall "+localStorageData.getSupportContactNumber());

            showListChooserDialog("Select slot on " + DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY), slotStrings, prefillSlotIndex, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( position == slotsList.size() ){
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
                        startActivity(intent);
                    }
                    else {
                        String deliveryDateAndTime = DateMethods.getDateInFormat(date, DateConstants.DD_MM_YYYY) + ", " + DateMethods.getTimeIn12HrFormat(slotsList.get(position).getEndTime());
                        String slotID = slotsList.get(position).getID();

                        if (actionType.equalsIgnoreCase("Add")) {
                            addToCart(slotID, deliveryDateAndTime);
                        } else if (actionType.equalsIgnoreCase("Update")) {
                            updateCartItem(slotID, deliveryDateAndTime);
                        }
                    }
                }
            });
        }
        else{
            showNotificationDialog("Alert!", Html.fromHtml("There are no delivery slots available on <b>"+DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY)+"</b>. Please select another date."));
        }
    }

    //------------------ Backend

    public void getPreferencesAndProceed(){
        showLoadingIndicator("Loading "+ warehouseMeatItemDetails.getMeatItemDetais().getName()+" details, wait..");
        /*getBackendAPIs().getStockAPI().getWarehouseMeatItemStock(warehouseMeatItemDetails.getID(), new ArrayList<String>(), new StockAPI.WarehouseMeatItemStockCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemStockDetails stockDetails) {
                if ( status ){
                    warehouseMeatItemStockDetails = stockDetails;*/

                    getBackendAPIs().getMeatItemsAPI().getMeatItemPreferences(warehouseMeatItemDetails.getMeatItemID(), new MeatItemsAPI.MeatItemPreferencesCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<AttributeDetails> attributesList, CartItemDetails previousCartItemDetails) {
                            if ( status ){
                                //attributeOptionsAvailabilitiesList = attributeOptions_AvailabilitiesList;
                                warehouseMeatItemDetails.getMeatItemDetais().setAttributes(attributesList);
                                if ( actionType.equalsIgnoreCase("Add") ) {
                                    cartItemDetails = previousCartItemDetails;
                                }
                                /*displayItemDetails();*/
                            }/*else{
                                showReloadIndicator(statusCode, "Unable to load "+warehouseMeatItemDetails.getMeatItemDetais().getName()+"details, try again!");
                            }*/
                            displayItemDetails();
                        }
                    });

                /*}else{
                    showReloadIndicator(statusCode, "Unable to load "+warehouseMeatItemDetails.getMeatItemDetais().getName()+"details, try again!");
                }
            }
        });*/
    }

    private void getSlotsForDateAndProceed(final String date){

        showLoadingDialog("Loading slots for " + date);
        getBackendAPIs().getStockAPI().getWarehouseMeatItemStock(warehouseMeatItemDetails.getID(), getSelectedItemLevelAttributesSelectedOptions(), new StockAPI.WarehouseMeatItemStockCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemStockDetails warehouseMeatItemStockDetails) {
                if ( status ){
                    Response<String> stockAvailableResponse = warehouseMeatItemStockDetails.isAvailableForDate(date, warehouseMeatItemDetails.getMeatItemDetais().getMinimumWeightToAddToCart());
                    if ( stockAvailableResponse.getStatus() ){
                        continueWithSlots(false);
                    }else{
                        closeLoadingDialog();
                        showSnackbarMessage(stockAvailableResponse.getMessage());//"Item is not available on selected date");
                    }
                }
                else{
                    closeLoadingDialog();
                    showSnackbarMessage("Something went wrong while checking stock availability, please try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            getSlotsForDateAndProceed(date);
                        }
                    });
                }
            }

            private void continueWithSlots(boolean showDialog){
                RequestSlotsForDate requestSlotsForDate = new RequestSlotsForDate(warehouseMeatItemDetails.getCategoryID(), warehouseMeatItemDetails.getID(), warehouseMeatItemDetails.getMeatItemID());
                if ( showDialog ){
                    showLoadingDialog("Loading slots for " + date);
                }
                getBackendAPIs().getGeneralAPI().getSlotsForDate(date, requestSlotsForDate, new GeneralAPI.SlotsForDateCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, String loadedDate, List<SlotDetails> slotsList) {
                        if (DateMethods.compareDates(date, loadedDate) == 0) {
                            closeLoadingDialog();
                            if (status) {
                                displaySlotsForDateAndProceed(date, slotsList);
                            } else {
                                showSnackbarMessage("Something went wrong while loading slots, please try again!", "RETRY", new ActionCallback() {
                                    @Override
                                    public void onAction() {
                                        continueWithSlots(true);
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    }

    public void addToCart(final String slotID, final String deliveryDateAndTime){

        RequestAddToCart requestAddToCart = new RequestAddToCart(warehouseMeatItemDetails.getID(), slotID, deliveryDateAndTime, selectedQuantity, getSelectedAttributes(), localStorageData.getDeliveryLocation(), /*special_instructions.getText().toString()*/null);

        showLoadingDialog("Adding item to your basket, please wait...");
        getBackendAPIs().getCartAPI().addToCart(requestAddToCart, new CartAPI.AddToCartCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, boolean isAlreadyPresent) {
                closeLoadingDialog();
                if (status) {
                    if (isAlreadyPresent == false) {
                        localStorageData.setCartCount(localStorageData.getCartCount() + 1);
                    }
                    localStorageData.setMeatItemPrefillDate(DateMethods.getOnlyDate(deliveryDateAndTime));

                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("status", true);
                    resultIntent.putExtra("cartMessage", message);
                    resultIntent.putExtra("displayMessage", (warehouseMeatItemDetails.getMeatItemDetais().getName() + " added to your basket."));
                    setResult(Constants.ADD_CART_ITEM_ACTIVITY_CODE, resultIntent);
                    finish();
                } else {
                    checkSessionValidity(statusCode);
                    if ( message != null && message.length() > 0 ){
                        showNotificationDialog("Failure", message);
                    }else {
                        showSnackbarMessage("Unable to add item.", "RETRY", new ActionCallback() {
                            @Override
                            public void onAction() {
                                addToCart(slotID, deliveryDateAndTime);
                            }
                        });
                    }
                }
            }
        });
    }

    public void updateCartItem(final String slotID, final String deliveryDateAndTime){

        RequestUpdateCartItem requestUpdateCartItem = new RequestUpdateCartItem(cartItemDetails.getWarehouseMeatItemDetails().getID(), cartItemDetails.getHash(), slotID, deliveryDateAndTime, selectedQuantity, getSelectedAttributes(), /*special_instructions.getText().toString()*/null);

        showLoadingDialog("Updating item, please wait..");
        getBackendAPIs().getCartAPI().updateCartItem(cartItemDetails.getID(), requestUpdateCartItem, new CartAPI.UpdateCartItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String newHash, String newDeliveryDate, double newPrice, CartTotalDetails cartTotalDetails) {
                closeLoadingDialog();

                if (status) {
                    cartItemDetails.setDeliveryByDate(newDeliveryDate);
                    cartItemDetails.setSelectedAttributes(getSelectedAttributes());
                    cartItemDetails.setWeight(selectedQuantity);
                    cartItemDetails.setHash(newHash);
                    cartItemDetails.setTotal(newPrice);

                    localStorageData.setMeatItemPrefillDate(DateMethods.getOnlyDate(deliveryDateAndTime));

                    /*Intent resultIntent = new Intent();
                    resultIntent.putExtra("status", true);
                    resultIntent.putExtra("cartItemDetails", new Gson().toJson(cartItemDetails));
                    resultIntent.putExtra("cartTotalDetails", new Gson().toJson(cartTotalDetails));
                    resultIntent.putExtra("cartMessage", message);
                    resultIntent.putExtra("displayMessage", (warehouseMeatItemDetails.getMeatItemDetais().getName() + " updated successfully."));*/
                    setResult(RESULT_OK, new Intent());
                    finish();
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to update item, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateCartItem(slotID, deliveryDateAndTime);
                        }
                    });
                }
            }
        });
    }




    //====================================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if ( requestCode == Constants.ICONGRID_ATTRIBUTE_OPTIONS_ACTIVITY_CODE && resultCode == RESULT_OK ){
                int selectedPosition = data.getIntExtra("position", -1);
                if ( selectedPosition != -1 && attributeOptionCallbak != null ){
                    attributeOptionCallbak.onSelect(-1, selectedPosition);
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
























    //============================ ENHANCEMENT

//    List<DaySlots> daySlots = new ArrayList<>();
//
//    private void saveDaySlots(String date, List<SlotDetails> slots){
//        if ( daySlots == null ){ daySlots = new ArrayList<>();}
//        if ( date != null && slots != null ){ daySlots.add(new DaySlots(date, slots));}
//    }
//
//    private List<SlotDetails> getSlots(String date){
//        if ( daySlots == null ){ daySlots = new ArrayList<>();}
//        for (int i=0;i<daySlots.size();i++){
//            if ( DateMethods.compareDates(daySlots.get(i).getDate(), date) == 0 ){
//                if ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(daySlots.get(i).getLoadedTime(), DateMethods.getCurrentDateAndTime()) <= 5*60*1000 ){
//                    return daySlots.get(i).getSlots();
//                }else{
//                    daySlots.remove(i);
//                    break;
//                }
//            }
//        }
//        return  null;
//    }
//    class DaySlots{
//        String date;
//        List<SlotDetails> slots;
//        String loadedTime;
//        DaySlots(String date, List<SlotDetails> slots){
//            this.date = date;
//            this.slots = slots;
//            this.loadedTime = DateMethods.getCurrentDateAndTime();
//        }
//
//        public String getLoadedTime() {
//            return loadedTime;
//        }
//
//        public String getDate() {
//            return date;
//        }
//
//        public List<SlotDetails> getSlots() {
//            return slots;
//        }
//    }


}