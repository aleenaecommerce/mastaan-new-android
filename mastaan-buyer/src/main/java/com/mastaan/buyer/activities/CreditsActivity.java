package com.mastaan.buyer.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.mastaan.buyer.R;
import com.squareup.picasso.Picasso;

public class CreditsActivity extends MastaanToolBarActivity implements View.OnClickListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_credits);
        getGoogleAnalyticsMethods().sendScreenName("Credits");       // Send ScreenNames to Analytics

        ImageView shrimp_image = (ImageView) findViewById(R.id.shrimp_image);
        ImageView prawns_image = (ImageView) findViewById(R.id.prawns_image);
        ImageView refer_image = (ImageView) findViewById(R.id.refer_image);

        Picasso.get()
                .load("https://s3-eu-west-1.amazonaws.com/mastaanappassets/prawn_with_shell.png")
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .into(shrimp_image);

        Picasso.get()
                .load("https://s3-eu-west-1.amazonaws.com/mastaanappassets/prawns_without_shell.png")
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .into(prawns_image);

        Picasso.get()
                .load(R.drawable.image_refer_friend)
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .into(refer_image);

    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.back_activity_enter_anim, R.anim.back_activity_exit_anim);     // Back Transition Animation..
    }

}
