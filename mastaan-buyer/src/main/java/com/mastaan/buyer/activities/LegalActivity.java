package com.mastaan.buyer.activities;

import android.content.Context;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.TabsFragmentAdapter;

public class LegalActivity extends MastaanToolBarActivity {

    TabLayout tabLayout;

    String[] legalItems;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_legal);
        getGoogleAnalyticsMethods().sendScreenName("Legal");       // Send ScreenNames to Analytics

        legalItems =  new String[]{"Terms & Conditions", "Privacy Policy"};//getResources().getStringArray(R.array.legalItems);

        // TabLayout...

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabTextColors(getResources().getColor(R.color.normal_tab_text_color), getResources().getColor(R.color.selected_tab_text_color));

        for(int i=0;i<legalItems.length;i++) {
            TabLayout.Tab tab = tabLayout.newTab();
            //tab.setText(profileItems[i]);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.view_tab_list_item, null, false);
            TextView tabTitle = (TextView) rowView.findViewById(R.id.tabTitle);
            tabTitle.setText(legalItems[i]);
            tab.setCustomView(rowView);
            tabLayout.addTab(tab);
        }

        getGoogleAnalyticsMethods().sendEvent("General", "Legal", legalItems[0]);         // Send Event to Analytics
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getGoogleAnalyticsMethods().sendEvent("General", "Legal", legalItems[tab.getPosition()]);         // Send Event to Analytics
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });

        // View Pager...

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new TabsFragmentAdapter(context, getSupportFragmentManager(), legalItems));

        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                //page.setRotationY(position * -30);
            }
        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));  // To Sync ViewPager with Tabs.


    }

    //========

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }
}
