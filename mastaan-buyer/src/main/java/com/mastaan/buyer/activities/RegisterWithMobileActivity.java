package com.mastaan.buyer.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.AuthenticationAPI;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.backend.models.ResponeValidateMobile;
import com.mastaan.buyer.backend.models.ResponseConnectBuyer;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;
import com.mastaan.buyer.models.BootStrapDetails;
import com.mastaan.buyer.models.BuyerDetails;


public class RegisterWithMobileActivity extends MastaanToolBarActivity implements View.OnClickListener{

    //long countDownTimeLimit = 46000;

    ResponeValidateMobile responeValidateMobile;

    ViewSwitcher viewSwitcher;

    LinearLayout register_layout;
    vTextInputLayout phone;
    Button register;
    ProgressBar validatingIndicator;
    CheckBox acceptTnC;
    View showTnC;
    TextView validationStatus;

    /*boolean autoVerify = true;
    FrameLayout auto_verifying_layout;
    TextView auto_verify_title, countdonwn_time;
    Button not_this_device;*/

    FrameLayout not_received_otp;
    FrameLayout send_otp_to_email;

    vTextInputLayout otp;
    Button verify;
    ProgressBar verifyingIndicator;
    TextView verficationStatus;

    String mobileNumber;

    String accessToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_register_with_mobile);
        getGoogleAnalyticsMethods().sendScreenName("Register with Mobile");       // Send ScreenNames to Analytics

        /*registerActivityReceiver(Constants.OTP_RECEIVER);*/

        //......................................................

        viewSwitcher = findViewById(R.id.viewSwitcher);

        register_layout = findViewById(R.id.register_layout);
        phone = findViewById(R.id.phone);
        validatingIndicator = findViewById(R.id.validatingIndicator);
        validationStatus = findViewById(R.id.validationStatus);
        register = findViewById(R.id.register);
        register.setOnClickListener(this);
        acceptTnC = findViewById(R.id.acceptTnC);
        showTnC = findViewById(R.id.showTnC);
        showTnC.setOnClickListener(this);
        acceptTnC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                register.setEnabled(isChecked);
            }
        });

        otp = findViewById(R.id.otp);
        verifyingIndicator = findViewById(R.id.verifyingIndicator);
        verficationStatus = findViewById(R.id.verficationStatus);
        verify = findViewById(R.id.verify);
        verify.setOnClickListener(this);

        /*auto_verifying_layout = findViewById(R.id.auto_verifying_layout);
        auto_verify_title = findViewById(R.id.auto_verify_title);
        countdonwn_time = findViewById(R.id.countdonwn_time);
        not_this_device = findViewById(R.id.not_this_device);
        not_this_device.setOnClickListener(this);*/

        not_received_otp = findViewById(R.id.not_received_otp);
        not_received_otp.setOnClickListener(this);

        send_otp_to_email = findViewById(R.id.send_otp_to_email);
        send_otp_to_email.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        try {
            inputMethodManager.hideSoftInputFromWindow(phone.getWindowToken(), 0);    // Hides Key Board After Item Select..
            inputMethodManager.hideSoftInputFromWindow(otp.getWindowToken(), 0);    // Hides Key Board After Item Select..
        }catch (Exception e){}


        if ( view == showTnC ){
            Intent tncAct = new Intent(context, TermsAndConditionsActivity.class);
            startActivity(tncAct);
        }
        else if ( view == register ){

            validationStatus.setText("");
            phone.checkError("*Enter phone number");
            mobileNumber = phone.getText();

            if ( CommonMethods.isValidPhoneNumber(mobileNumber) ){
                register.setEnabled(false);
                validatingIndicator.setVisibility(View.VISIBLE);

                /*getPermissionsHandler().checkSMSPermission(new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        autoVerify = status?true:false;*/     // Auto verify if SMS permission granted
                        validateNumber(mobileNumber);
                    /*}
                });*/
            }
            else {
                phone.showError("*Enter a valid phone number (10 digits)");
            }
        }

        else if ( view == not_received_otp ){
            getGoogleAnalyticsMethods().sendEvent("Auth", "OTP not received", ""/*"OTP not received for mobile " + phone.getText()*/);    // Send Event to Analytics

            /*if ( autoVerify == true ){
                register.setEnabled(true);
                validatingIndicator.setVisibility(View.GONE);
                register_layout.setVisibility(View.VISIBLE);
                auto_verifying_layout.setVisibility(View.GONE);
                viewSwitcher.setDisplayedChild(0);
            }else{*/
                showLoadingDialog("Resending OTP, please wait..");
            /*}*/

            mobileNumber = phone.getText();
            validateNumber(mobileNumber);
        }

        else  if ( view == send_otp_to_email ){
            Intent emailRegisterAct = new Intent(context, RegisterWithEmailActivity.class);
            startActivityForResult(emailRegisterAct, Constants.REGISTER_ACTIVITY_CODE);
        }

        /*else if ( view == not_this_device ){
            autoVerify = false;
            verficationStatus.setText("");
            viewSwitcher.setDisplayedChild(1);
        }*/

        else if ( view == verify ){
            verficationStatus.setText("");
            otp.checkError("*Enter OTP sent to you");
            String oOTP = otp.getText();

            if (oOTP.length() > 0) {
                // FOR HANDLING ERROR IN BOOTSTRAP URL AFTER VERIFICATION COMPLETE
                if ( localStorageData.getSessionFlag() ){
                    getBootstrapDetails();
                }
                // FOR HANDLING ERROAR IN BUYER CONNECT URL
                else if ( accessToken != null && accessToken.length() > 0 ){
                    connectBuyer();
                }
                // NORMAL VERIFICATION URL WITH OTP
                else {
                    verifyNumber(responeValidateMobile.getSmsID(), oOTP);
                }
            }
        }
    }

    /*@Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {

        try{
            if ( autoVerify == true ) {
                //getGoogleAnalyticsMethods().sendEvent(false, "Auth", "OTP SMS read", "OTP SMS read for mobile " + phone.getText() + " with OTP: " + receivedData.getStringExtra("otp"));    // Send Event to Analytics
                String otpService = receivedData.getStringExtra(Constants.OTP_SERVICE);
                String otpCode = receivedData.getStringExtra(Constants.OTP_CODE);

                otp.setText(otpCode);
                verify.performClick();
            }
        }catch (Exception e){   e.printStackTrace();    }
    }*/

    /*public void autoVerifyNumber(final ResponeValidateMobile responeValidateMobile){

        this.responeValidateMobile = responeValidateMobile;

        auto_verify_title.setText("We'll send an SMS to " + phone.getText() + ". If this device has the number, we'll auto verify it.");
        inputMethodManager.hideSoftInputFromWindow(phone.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        register_layout.setVisibility(View.GONE);
        auto_verifying_layout.setVisibility(View.VISIBLE);                        // Showing AutoVerifying Dialog.

        countdonwn_time.setText(DateMethods.getTimeFromMilliSeconds(countDownTimeLimit));
        new CountDownTimer(countDownTimeLimit, 1000) {                                           // Staring 40sec Duration for AutoVerfication.
            public void onTick(long millisUntilFinished) {
                long minutes = millisUntilFinished/60000;
                long seconds = (millisUntilFinished%60000)/1000;
                String cTime =  seconds + " sec";
                if (minutes > 0) {
                    cTime = minutes + " min, " + seconds + " sec";
                }
                countdonwn_time.setText(cTime);
            }
            public void onFinish() {
                if ( autoVerify ) {
                    //getGoogleAnalyticsMethods().sendEvent(false, "Auth", "OTP SMS read timeout", "OTP SMS read timeout for mobile " + phone.getText());    // Send Event to Analytics
                    verficationStatus.setText("Unable to auto verify the number, Try manually by using OTP Sent to you...");
                    viewSwitcher.setDisplayedChild(1);
                }
            }
        }.start();

    }*/

    private void validateNumber(String mobileNumber){

        getBackendAPIs().getAuthenticationAPI().validatePhoneNumber(mobileNumber, new AuthenticationAPI.MobileValidationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, ResponeValidateMobile validationDetails) {
                closeLoadingDialog();
                if (status == true) {
                    responeValidateMobile = validationDetails;
                    /*if (autoVerify == true) {
                        autoVerifyNumber(validationDetails);
                    } else {*/
                        verficationStatus.setText("");
                        viewSwitcher.setDisplayedChild(1);
                    /*}*/
                } else {
                    validationStatus.setText("Something went wrong while sending otp , try again!");
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            validationStatus.setText("No Internet connection!");
                        } else {
                            validationStatus.setText("Not able to connect to Server. \nPlease try again after a while");
                        }
                    }

                    register.setEnabled(true);
                    validatingIndicator.setVisibility(View.GONE);
                }
            }
        });
    }

    public void verifyNumber(String smsID, String otp){         // For Manually Verifying by Entering OTP...

        verify.setEnabled(false);
        verifyingIndicator.setVisibility(View.VISIBLE);
        verficationStatus.setText("Verifying number..");
        getBackendAPIs().getAuthenticationAPI().verifyPhoneNumber(smsID, otp, new AuthenticationAPI.MobileVerificationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String access_Token) {
                if (status == true) {
                    accessToken = access_Token;
                    connectBuyer();
                } else {
                    verficationStatus.setText("Verification failure, check OTP & try again.");
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            verficationStatus.setText("No Internet connection!");
                        } else {
                            verficationStatus.setText("Verification failure. \nPlease try again after a while");
                        }
                    }

                    verify.setEnabled(true);
                    verifyingIndicator.setVisibility(View.GONE);
                    viewSwitcher.setDisplayedChild(1);
                }
            }
        });

    }

    public void connectBuyer(){

        verify.setEnabled(false);
        verifyingIndicator.setVisibility(View.VISIBLE);
        verficationStatus.setText("Verifying number...");
        getBackendAPIs().getAuthenticationAPI().connectBuyer(accessToken, new AuthenticationAPI.BuyerConnectCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ResponseConnectBuyer responseConnectBuyer) {
                if (status) {
                    getGoogleAnalyticsMethods().sendEvent("Auth", responseConnectBuyer.isSignUp()?"Signup":"Signin", responseConnectBuyer.getBuyerDetails().getMobile());

                    Log.d(Constants.LOG_TAG, "Mobile Verification Completed, For Mobile = " + responseConnectBuyer.getBuyerDetails().getMobile());
                    localStorageData.storeBuyerSession(accessToken, responseConnectBuyer.getBuyerDetails());        // STORING USER SESSION

                    // Linking buyer to FCM
                    try {
                        getBackendAPIs().getProfileAPI().linkToFCM(FirebaseInstanceId.getInstance().getToken(), null);
                    }catch (Exception e){}

                    getBootstrapDetails();
                } else {
                    showVerifcationRetryView(statusCode);
                }
            }
        });
    }

    private void showVerifcationRetryView(int statusCode){

        verficationStatus.setText("Something went wrong while verifying number, try again.");
        if (statusCode == -1) {
            boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
            if (isInternetAvailable == false) {
                verficationStatus.setText("No Internet connection!");
            } else {
                verficationStatus.setText("Not able to connect to Server. \nPlease try again after a while");
            }
        }

        verify.setEnabled(true);
        verifyingIndicator.setVisibility(View.GONE);
        viewSwitcher.setDisplayedChild(1);
    }

    public void getBootstrapDetails(){

        verify.setEnabled(false);
        verifyingIndicator.setVisibility(View.VISIBLE);
        verficationStatus.setText("Verifying number....");
        getBackendAPIs().getGeneralAPI().getBootStrap(new GeneralAPI.BootStrapCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final BootStrapDetails bootStrapDetails) {
                if ( status ){

                    new AsyncTask<Void, Void, Void>() {
                        boolean showDeliveryAddressChangeAlert = false;
                        @Override
                        protected Void doInBackground(Void... voids) {
                            localStorageData.setServerTime(CommonMethods.getReadableTimeFromUTC(bootStrapDetails.getServerTime()));

                            localStorageData.setServingAt(bootStrapDetails.getServingAt());
                            localStorageData.setSupportContactNumber(bootStrapDetails.getSupportContactNumber());

                            localStorageData.setDeliveringHoursStartTime(bootStrapDetails.getDeliveringHoursStartTime());
                            localStorageData.setDeliveringHoursEndTime(bootStrapDetails.getDeliveringHoursEndTime());

                            localStorageData.storeInstallSources(bootStrapDetails.getInstallSources());

                            localStorageData.storeMeatItemWeights(bootStrapDetails.getMeatItemWeights());
                            localStorageData.storeMeatItemSets(bootStrapDetails.getMeatItemSets());
                            localStorageData.storeMeatItemPieces(bootStrapDetails.getMeatItemPieces());

                            //-----

                            localStorageData.setBuyerDetails(bootStrapDetails.getBuyerDetails());

                            localStorageData.setNotificationsFlag(bootStrapDetails.getBuyerDetails().getPromotionalAlertsPreference());

                            localStorageData.setCartCount(bootStrapDetails.getCartItemsCount());
                            localStorageData.setBuyerPendingOrders(bootStrapDetails.getPendingOrders());

                            if ( bootStrapDetails.getCartItemsCount() > 0 ) {
                                AddressBookItemDetails storedDeliveryAddressDetails = localStorageData.getDeliveryLocation();
                                if ( storedDeliveryAddressDetails.getID().equals(bootStrapDetails.getCartDeliveryAddress().getID()) == false
                                        && storedDeliveryAddressDetails.getFullAddress().equalsIgnoreCase(bootStrapDetails.getCartDeliveryAddress().getFullAddress()) == false ){
                                    showDeliveryAddressChangeAlert = true;
                                }
                                localStorageData.setDeliveryLocation(bootStrapDetails.getCartDeliveryAddress());
                            }
                            localStorageData.setHasLocationHistory(bootStrapDetails.getAddressBookList()!=null&&bootStrapDetails.getAddressBookList().size()>0?true:false);

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);

                            new BroadcastReceiversMethods(context).updateUserSessionInfo();

                            if ( showDeliveryAddressChangeAlert ){
                                showToastMessage("Your delivery location is changed to match your cart items delivery location");
                            }

                            /*if ( localStorageData.getNoOfPendingOrders() > 0 ){
                                context.startService(firebaseService);
                            }*/

                            //--------

                            Intent verificationIntent = new Intent();
                            setResult(RESULT_OK, verificationIntent);
                            finish();
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }else{
                    showVerifcationRetryView(statusCode);
                }
            }
        });

    }

    //=========================================


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ( requestCode == Constants.REGISTER_ACTIVITY_CODE ) {      // If It is from Register Activity
            if ( resultCode == RESULT_OK ) {
                setResult(RESULT_OK, data);
                finish();
            }else {
                onBackPressed();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        inputMethodManager.hideSoftInputFromWindow(phone.getWindowToken(), 0);    // Hides Key Board After Item Select..
        inputMethodManager.hideSoftInputFromWindow(otp.getWindowToken(), 0);    // Hides Key Board After Item Select..
        onBackPressed();
        return true;
    }
}
