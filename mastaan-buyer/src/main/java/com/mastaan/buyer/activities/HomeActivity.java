package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.CategoriesAdapter;
import com.mastaan.buyer.adapters.MessagesAdapter;
import com.mastaan.buyer.adapters.OrderFeedbacksAdapter;
import com.mastaan.buyer.backend.FeedbacksAPI;
import com.mastaan.buyer.backend.MeatItemsAPI;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.fragments.SideBarFragment;
import com.mastaan.buyer.interfaces.MessagesCallback;
import com.mastaan.buyer.models.CategoryDetails;
import com.mastaan.buyer.models.MessageDetails;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.services.MastaanService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 08/12/18.
 */

public class HomeActivity extends MastaanNavigationDrawerActivity implements View.OnClickListener{

    MenuItem pendingOrdersInfoMenuItem;

    SideBarFragment sideBarFragment;

    FrameLayout cartInfo;
    ImageButton cartFAB;
    TextView cart_count;

    View messagesView;
    vRecyclerView messagesHolder;
    TextView messagesPagerIndicator;
    MessagesAdapter messagesAdapter;

    vRecyclerView categoriesHolder;
    CategoriesAdapter categoriesAdapter;

    View feedbacksView;
    View skipFeedbacks;
    vRecyclerView feedbacksHolder;
    TextView feedbacksPagerIndicator;
    OrderFeedbacksAdapter feedbacksAdapter;
    int selectedFeedbackPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getGoogleAnalyticsMethods().sendScreenName("Categories");       // Send ScreenNames to Analytics
        hasLoadingView();

        //----------

        sideBarFragment = new SideBarFragment();
        sideBarFragment.setCallback(new SideBarFragment.SideBarCallback() {
            @Override
            public void onItemSelected(int position) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }

            @Override
            public void onDeliverLocationChange(final PlaceDetails deliverLocationDetails, final ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation) {
                getMastaanApplication().resetAlertMessages();
                getMastaanApplication().resetMemberships();
                displayAlertMessages();

                /*if ( localStorageData.getCartCount() > 0 ) {
                    showClearCartDialogOnDeliveryLocationChange(deliverLocationDetails, responseCheckAvailabilityAtLocation);
                }else{*/
                    showLoadingIndicator("Loading, wait..");
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            localStorageData.setMeatItemPrefillDate("");
                            localStorageData.setCartCount(0);

                            localStorageData.setDeliveryLocation(new AddressBookItemDetails(deliverLocationDetails));

                            localStorageData.storeNoDeliveryDates(responseCheckAvailabilityAtLocation.getNoDeliveryDates());
                            localStorageData.setDaysToEnableSlots(responseCheckAvailabilityAtLocation.getDaysToEnableSlots());
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);

                            checkCartStatus("");

                            isTodayServingDay(new StatusCallback() {
                                @Override
                                public void onComplete(boolean isTodayServingDay, int statusCode, String alertMessage) {
                                    closeLoadingDialog();
                                    sideBarFragment.changeDeliveryLocation(deliverLocationDetails.getFullAddress());
                                    getCategories();

                                    if ( isTodayServingDay == false ){
                                        showNotificationDialog("Alert!", Html.fromHtml(alertMessage));
                                    }
                                }
                            });
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                /*}*/
            }
        });
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();
        if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") ) {
            disableLeftNavigationDrawer();
        }

        registerActivityReceiver(Constants.HOME_RECEIVER);

        startService(new Intent(context, MastaanService.class));    // Starting Mastaan Service

        //------------

        cartInfo = findViewById(R.id.cartInfo);
        cart_count = findViewById(R.id.cart_count);
        cartFAB = findViewById(R.id.cartFAB);
        cartFAB.setOnClickListener(this);

        messagesView = findViewById(R.id.messagesView);
        messagesPagerIndicator = findViewById(R.id.messagesPagerIndicator);
        messagesHolder = findViewById(R.id.messagesHolder);
        messagesHolder.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        final SnapHelper messagesSnapHelper = new PagerSnapHelper();
        messagesSnapHelper.attachToRecyclerView(messagesHolder);
        messagesAdapter = new MessagesAdapter(context, R.layout.view_message_home, new ArrayList<MessageDetails>(), new MessagesAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                activity.handleMessageClick(messagesAdapter.getItem(position));
            }
        });
        messagesHolder.setAdapter(messagesAdapter);
        messagesHolder.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int position = 0;
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try{
                    int newPosition = messagesHolder.getLayoutManager().getPosition(messagesSnapHelper.findSnapView(messagesHolder.getLayoutManager()));
                    if ( newPosition != position ){
                        position = newPosition;
                        displayMessagesPagerIndicator(position);
                    }
                }catch (Exception e){}
            }
        });

        categoriesHolder = findViewById(R.id.categoriesHolder);
        categoriesHolder.setupVerticalGridOrientation(2);
        categoriesHolder.setNestedScrollingEnabled(false);
        categoriesAdapter = new CategoriesAdapter(activity, new ArrayList<CategoryDetails>(), new CategoriesAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                CategoryDetails categoryDetails = categoriesAdapter.getItem(position);

                Intent meatItemsAct = new Intent(context, MeatItemsActivity.class);
                meatItemsAct.putExtra(Constants.CATEGORY_ID, categoryDetails.getID());
                meatItemsAct.putExtra(Constants.CATEGORY_VALUE, categoryDetails.getValue());
                meatItemsAct.putExtra(Constants.CATEGORY_NAME, categoryDetails.getName()+" "+categoryDetails.getParentCategoryName());
                meatItemsAct.putExtra(Constants.COLOR_PRIMARY, categoryDetails.getBackgroundColor());
                meatItemsAct.putExtra(Constants.COLOR_PRIMARY_DARK, categoryDetails.getStatusBarColor());
                startActivityForResult(meatItemsAct, Constants.ITEM_LISTINGS_ACTIVITY_CODE);
            }

            @Override
            public void onShowDisabledInfo(int position) {
                showNotificationDialog(categoriesAdapter.getItem(position).getName(), categoriesAdapter.getItem(position).getDisabledMessage());
            }
        });
        categoriesHolder.setAdapter(categoriesAdapter);

        feedbacksView = findViewById(R.id.feedbacksView);
        skipFeedbacks = findViewById(R.id.skipFeedbacks);
        skipFeedbacks.setOnClickListener(this);
        feedbacksPagerIndicator = findViewById(R.id.feedbacksPagerIndicator);
        feedbacksHolder = findViewById(R.id.feedbacksHolder);
        feedbacksHolder.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        final SnapHelper feedbacksSnapHelper = new PagerSnapHelper();
        feedbacksSnapHelper.attachToRecyclerView(feedbacksHolder);
        feedbacksAdapter = new OrderFeedbacksAdapter(context, new ArrayList<OrderDetails>(), new OrderFeedbacksAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                selectedFeedbackPosition = position;

                Intent orderFeedbackAct = new Intent(context, OrderFeedbackActivity.class);
                orderFeedbackAct.putExtra(Constants.ORDER_DETAILS, new Gson().toJson(feedbacksAdapter.getItem(position)));
                startActivityForResult(orderFeedbackAct, Constants.ORDER_FEEDBACK_ACTIVITY_CODE);
            }
        });
        feedbacksHolder.setAdapter(feedbacksAdapter);
        feedbacksHolder.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int position = 0;
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try{
                    int newPosition = feedbacksHolder.getLayoutManager().getPosition(feedbacksSnapHelper.findSnapView(feedbacksHolder.getLayoutManager()));
                    if ( newPosition != position ){
                        position = newPosition;
                        displayFeedbacksPagerIndicator(position);
                    }
                }catch (Exception e){}
            }
        });

        //------------

        displayAlertMessages();

        getCategories();

        displayPendingFeedbacks();

    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);

        try{
            String actionType = receivedData.getStringExtra(Constants.ACTION_TYPE);

            if ( actionType.equalsIgnoreCase(Constants.DISPLAY_USER_SESSION_INFO) ){
                displayUserSessionInfo();
            }
            else if ( actionType.equalsIgnoreCase(Constants.CHECK_CART_STATUS) ){
                checkCartStatus("");
            }
            else if ( actionType.equalsIgnoreCase(Constants.DISPLAY_PENDING_FEEDBACKS) ){
                displayPendingFeedbacks();
            }
        }catch (Exception e){}
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getCategories();
    }

    @Override
    public void onClick(View view) {
        if ( view == cartFAB){
            Intent cartAct = new Intent(context, CartActivity.class);
            startActivityForResult(cartAct, Constants.CART_ACTIVITY_CODE);
        }

        else if ( view == skipFeedbacks ){
            feedbacksView.setVisibility(View.GONE);
            try{
                for (int i=0;i<feedbacksAdapter.getItemCount();i++){
                    if ( feedbacksAdapter.getItem(i) != null ){
                        getBackendAPIs().getFeedbacksAPI().skipFeedback(feedbacksAdapter.getItem(i).getID(), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                //showToastMessage("Skipped "+System.currentTimeMillis());
                            }
                        });
                    }
                }
            }catch (Exception e){}
        }
    }

    private void displayMessagesPagerIndicator(int visibleItemPosition){
        try {
            if ( messagesAdapter.getItemCount() > 1 ) {
                if (visibleItemPosition < 0) {  visibleItemPosition = 0;    }
                String pagerIndicatorString = "";
                for (int i = 0; i < messagesAdapter.getItemCount(); i++) {
                    if (visibleItemPosition == i) {
                        pagerIndicatorString += " " + (Build.VERSION.SDK_INT <= 19 ? "\u25CF" : "\u2B24") + " ";
                    } else {
                        pagerIndicatorString += " " + (Build.VERSION.SDK_INT <= 19 ? "\u25CB" : "\u26AC") + " ";
                    }
                }
                messagesPagerIndicator.setText(CommonMethods.fromHtml(pagerIndicatorString));
            }else{
                messagesPagerIndicator.setText("");
            }
        }catch (Exception e){}
    }

    private void displayFeedbacksPagerIndicator(int visibleItemPosition){
        try {
            if ( feedbacksAdapter.getItemCount() > 1 ) {
                if (visibleItemPosition < 0) {  visibleItemPosition = 0;    }
                String pagerIndicatorString = "";
                for (int i = 0; i < feedbacksAdapter.getItemCount(); i++) {
                    if (visibleItemPosition == i) {
                        pagerIndicatorString += " " + (Build.VERSION.SDK_INT <= 19 ? "\u25CF" : "\u2B24") + " ";
                    } else {
                        pagerIndicatorString += " " + (Build.VERSION.SDK_INT <= 19 ? "\u25CB" : "\u26AC") + " ";
                    }
                }
                feedbacksPagerIndicator.setText(CommonMethods.fromHtml(pagerIndicatorString));
            }else{
                feedbacksPagerIndicator.setText("");
            }
        }catch (Exception e){}
    }

    public void checkCartStatus(final String message){

        fabEnterAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (message != null && message.length() > 0) {
                    showSnackbarMessage(cartInfo, message, null, null);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        int cartCount = localStorageData.getCartCount();

        if ( cartCount > 0 ){
            cart_count.setText("" + cartCount);
            if ( cartInfo.getVisibility() == View.INVISIBLE  || cartInfo.getVisibility() == View.GONE ){
                cartInfo.setVisibility(View.VISIBLE);
                cartInfo.startAnimation(fabEnterAnim);
            }else{
                if (message != null && message.length() > 0) {
                    showSnackbarMessage(cartInfo, message, null, null);
                }
            }
        }
        else {
            if ( cartInfo.getVisibility() == View.VISIBLE ){
                cartInfo.setVisibility(View.INVISIBLE);
                cartInfo.startAnimation(fabExitAnim);
            }
        }
    }

    //---------

    public void getCategories(){
        showLoadingIndicator("Loading...");
        getBackendAPIs().getMeatItemsAPI().getCategoriesList(new MeatItemsAPI.CategoriesListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<CategoryDetails> categories_list) {
                if ( status ){
                    List<CategoryDetails> categoriesList = new ArrayList<>();
                    for (int i=0;i<categories_list.size();i++){
                        if ( categories_list.get(i) != null && categories_list.get(i).getSubCategories() != null ){
                            for (int j=0;j<categories_list.get(i).getSubCategories().size();j++){
                                if ( categories_list.get(i).getSubCategories().get(j) != null ){
                                    categoriesList.add(categories_list.get(i).getSubCategories().get(j));
                                }
                            }
                        }
                    }

                    if ( categories_list.size() > 0 ) {
                        categoriesAdapter.setItems(categoriesList);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("Nothing to show");
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    public void displayAlertMessages(){
        messagesView.setVisibility(View.GONE);
        getMastaanApplication().getAlertMessages("home", new MessagesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<MessageDetails> messagesList) {
                if ( status && messagesList.size() > 0 ){
                    messagesView.setVisibility(View.VISIBLE);
                    messagesAdapter.setItems(messagesList);
                    displayMessagesPagerIndicator(0);
                }
            }
        });
    }

    public void displayPendingFeedbacks(){
        feedbacksView.setVisibility(View.GONE);
        if ( getString(R.string.app_type).equalsIgnoreCase("internal") == false ) {
            getBackendAPIs().getFeedbacksAPI().getPendingFeedbackOrders(new FeedbacksAPI.PendingFeedbackOrdersCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> pendingFeedbackOrders) {
                    if ( status && pendingFeedbackOrders.size() > 0 ){
                        feedbacksView.setVisibility(View.VISIBLE);
                        feedbacksAdapter.setItems(pendingFeedbackOrders);
                        displayFeedbacksPagerIndicator(0);
                    }
                }
            });
        }

    }

    //----------

    public void displayUserSessionInfo(){

        checkCartStatus("");

        if ( pendingOrdersInfoMenuItem != null ) {
            if ( localStorageData.getNoOfPendingOrders() > 0 ){
                pendingOrdersInfoMenuItem.setVisible(true);
                pendingOrdersInfoMenuItem.setTitle(localStorageData.getNoOfPendingOrders() + " PENDING ORDERS");
                if ( localStorageData.getNoOfPendingOrders() == 1 ){
                    pendingOrdersInfoMenuItem.setTitle("1 PENDING ORDER");
                }
            }else{
                pendingOrdersInfoMenuItem.setVisible(false);
                pendingOrdersInfoMenuItem.setTitle("");
            }
        }
    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();
        displayUserSessionInfo();
    }

    /*public void showClearCartDialogOnDeliveryLocationChange(final PlaceDetails newDeliverLocation, final ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation){

        showChoiceSelectionDialog(false, "Clear meat basket!", "Sorry, changing the delivery location will clear your meat basket.\n\nDo you want to change your delivery location?", "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {

                closeChoiceSelectionDialog();

                if (choiceName.equalsIgnoreCase("YES")) {

                    showLoadingDialog("Clearing your cart, please wait..");
                    getBackendAPIs().getCartAPI().clearCart(new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            closeLoadingDialog();
                            if (status == true) {
                                localStorageData.setMeatItemPrefillDate("");
                                localStorageData.setCartCount(0);
                                checkCartStatus("");

                                showLoadingIndicator("Loading, wait..");
                                new AsyncTask<Void, Void, Void>() {
                                    @Override
                                    protected Void doInBackground(Void... voids) {
                                        localStorageData.setDeliveryLocation(new AddressBookItemDetails(newDeliverLocation));

                                        localStorageData.storeNoDeliveryDates(responseCheckAvailabilityAtLocation.getNoDeliveryDates());
                                        localStorageData.setDaysToEnableSlots(responseCheckAvailabilityAtLocation.getDaysToEnableSlots());

                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Void aVoid) {
                                        super.onPostExecute(aVoid);
                                        closeLoadingDialog();
                                        sideBarFragment.changeDeliveryLocation(newDeliverLocation.getFullAddress());
                                        getCategories();
                                    }
                                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                            } else {
                                checkSessionValidity(statusCode);
                                showSnackbarMessage("Error in clearing your cart, try again!", null, null);
                                showClearCartDialogOnDeliveryLocationChange(newDeliverLocation, responseCheckAvailabilityAtLocation);
                            }
                        }
                    });
                }
            }
        });
    }*/

    //===================================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if ( requestCode == Constants.ADD_CART_ITEM_ACTIVITY_CODE || requestCode == Constants.ITEM_LISTINGS_ACTIVITY_CODE ){
                if ( data != null ){
                    boolean addedStatus = data.getBooleanExtra("status", false);
                    if ( addedStatus ) {
                        String displayMessage = data.getStringExtra("displayMessage");
                        checkCartStatus(displayMessage);

                        String cartMessage = data.getStringExtra("cartMessage");
                        if ( cartMessage != null && cartMessage.length() > 0 ){
                            showNotificationDialog("Alert!", cartMessage);
                        }
                    }else{
                        checkCartStatus("");
                    }
                }else{
                    checkCartStatus("");
                }
            }
            else if ( requestCode == Constants.ORDER_FEEDBACK_ACTIVITY_CODE && resultCode == RESULT_OK ){
                feedbacksAdapter.deleteItem(selectedFeedbackPosition);
                if ( feedbacksAdapter.getItemCount() > 0 ) {
                    displayFeedbacksPagerIndicator(selectedFeedbackPosition);
                }else{
                    feedbacksView.setVisibility(View.GONE);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        pendingOrdersInfoMenuItem = menu.findItem(R.id.action_pending_orders);
        onAfterMenuCreated();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") == false ) {
            if ( drawerListener.onOptionsItemSelected(item) ) {
                return true;
            }
            else if (item.getItemId() == R.id.action_pending_orders) {
                if ( localStorageData.getNoOfPendingOrders() == 1 ) {
                    Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
                    orderDetailsAct.putExtra(Constants.ID, localStorageData.getBuyerPendingOrders().get(0).getID());
                    startActivity(orderDetailsAct);
                }else{
                    Intent pendingOrdersAct = new Intent(context, PendingOrdersActivity.class);
                    startActivity(pendingOrdersAct);
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
