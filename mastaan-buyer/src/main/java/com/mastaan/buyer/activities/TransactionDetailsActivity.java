package com.mastaan.buyer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.WalletAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.TransactionDetails;

/**
 * Created by Venkatesh Uppu on 22/03/19.
 */

public class TransactionDetailsActivity extends MastaanToolBarActivity implements View.OnClickListener{

    String transactionID;
    TransactionDetails transactionDetails;

    TextView id;
    TextView date;
    TextView type;
    TextView amount;
    View linkView;
    TextView link_name;
    TextView link;
    View commentsView;
    TextView comments;
    TextView closing_balance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_details);
        getGoogleAnalyticsMethods().sendScreenName("Transaction Details");       // Send ScreenNames to Analytics
        hasLoadingView();
        hasSwipeRefresh();

        transactionID = getIntent().getStringExtra(Constants.ID);

        //------------

        id = findViewById(R.id.id);
        date = findViewById(R.id.date);
        type = findViewById(R.id.type);
        amount = findViewById(R.id.amount);
        linkView = findViewById(R.id.linkView);
        link_name = findViewById(R.id.link_name);
        link = findViewById(R.id.link);
        link.setOnClickListener(this);
        commentsView = findViewById(R.id.commentsView);
        comments = findViewById(R.id.comments);
        closing_balance = findViewById(R.id.closing_balance);

        //----------

        getTransactionDetails();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ( getIntent().getStringExtra(Constants.ID) != null ){
            transactionID = getIntent().getStringExtra(Constants.ID);
            onReloadPressed();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getTransactionDetails();
    }

    @Override
    public void onClick(View view) {
        if ( view == link ){
            Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
            orderDetailsAct.putExtra(Constants.ID, transactionDetails.getOrderID());
            startActivity(orderDetailsAct);
        }
    }

    public void getTransactionDetails(){

        showLoadingIndicator("Loading transaction details, wait..");
        getBackendAPIs().getWalletAPI().getTransactionDetails(transactionID, new WalletAPI.TransactionCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, TransactionDetails transactionDetails) {
                if ( status ){
                    display(transactionDetails);
                }else{
                    showReloadIndicator(statusCode, "Unable to load order details, try again!");
                }
            }
        });
    }

    public void display(TransactionDetails transactionDetails){
        this.transactionDetails = transactionDetails;
        try{
            switchToContentPage();

            id.setText(transactionDetails.getID());
            
            date.setText(DateMethods.getReadableDateFromUTC(transactionDetails.getCreatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A));
            
            type.setText(transactionDetails.getTypeString());

            amount.setText((transactionDetails.getAmount()<0?"Debit: ":"Credit: ")+ SpecialCharacters.RS+" "+ CommonMethods.getIndianFormatNumber(Math.abs(transactionDetails.getAmount())));

            if ( transactionDetails.getOrderID() != null ){
                linkView.setVisibility(View.VISIBLE);
                link_name.setText("Order");
                link.setText(transactionDetails.getOrderReadableID());
            }else{
                linkView.setVisibility(View.GONE);
            }

            if ( transactionDetails.getComments() != null && transactionDetails.getComments().trim().length() > 0 ){
                commentsView.setVisibility(View.VISIBLE);
                comments.setText(transactionDetails.getComments().trim());
            }else{  commentsView.setVisibility(View.GONE);    }

            closing_balance.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(transactionDetails.getEndingBalance()));
            
        }catch (Exception e){}
    }

    //==============

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
