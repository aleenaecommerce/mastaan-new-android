package com.mastaan.buyer.activities;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.AppCompatEditText;
import android.view.MenuItem;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.FeedbackOrderItemsAdapter;
import com.mastaan.buyer.backend.models.RequestSubmitOrderFeedback;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.models.OrderItemDetails;
import com.mastaan.buyer.models.OrderItemFeedback;

import java.util.List;


public class OrderFeedbackActivity extends MastaanToolBarActivity implements View.OnClickListener{

    TextView title;
    TextView details;

    View feedbackView;
    RatingBar rating_bar;
    AppCompatEditText comments;
    FloatingActionButton submit;

    ViewPager orderItemsHolder;
    FeedbackOrderItemsAdapter orderItemsAdapter;

    OrderDetails orderDetails;
    List<OrderItemDetails> orderItems;

    int currentOrderFoodItemIndex;

    RequestSubmitOrderFeedback requestSubmitOrderFeedback = new RequestSubmitOrderFeedback();

    OrderItemFeedback currentOrderItemFeedback;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_feedback);
        hasLoadingView();

        getGoogleAnalyticsMethods().sendScreenName("Order Feedback");       // Send ScreenNames to Analytics

        //---------------

        title = findViewById(R.id.title);
        details = findViewById(R.id.details);

        feedbackView = findViewById(R.id.feedbackView);
        comments = findViewById(R.id.comments);
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(this);
        rating_bar = findViewById(R.id.rating_bar);
        rating_bar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean b) {
                if ( rating <= 0 ){
                    feedbackView.setBackgroundColor(Color.parseColor("#d1d1d1"));
                    comments.setText("");
                    comments.setHint("Comments");
                    submit.setImageResource(R.drawable.ic_send_black);
                }
                else{
                    if ( rating < 3 ){
                        feedbackView.setBackgroundColor(Color.parseColor("#d50000"));
                        //comments.setText("");
                        comments.setHint("Too bad you don't like it. How can we improve?");
                        submit.setImageResource(R.drawable.ic_send_red);

                        currentOrderItemFeedback = new OrderItemFeedback(orderItems.get(currentOrderFoodItemIndex).getID(), orderItems.get(currentOrderFoodItemIndex).getMeatItemDetails().getID(), rating_bar.getRating(), "");
                    }
                    else if ( rating >= 3 && rating < 4 ){
                        feedbackView.setBackgroundColor(Color.parseColor("#2979ff"));
                        //comments.setText("");
                        comments.setHint("Ok! We'll improve. Tap to add any comments");
                        submit.setImageResource(R.drawable.ic_send_blue);

                        currentOrderItemFeedback = new OrderItemFeedback(orderItems.get(currentOrderFoodItemIndex).getID(), orderItems.get(currentOrderFoodItemIndex).getMeatItemDetails().getID(), rating_bar.getRating(), "");
                    }
                    else if ( rating >= 4 ){
                        feedbackView.setBackgroundColor(Color.parseColor("#00c853"));
                        //comments.setText("");
                        comments.setHint("Awesome! Tap to write your appreciation");
                        submit.setImageResource(R.drawable.ic_send_green);

                        currentOrderItemFeedback = new OrderItemFeedback(orderItems.get(currentOrderFoodItemIndex).getID(), orderItems.get(currentOrderFoodItemIndex).getMeatItemDetails().getID(), rating_bar.getRating(), "");
                    }
                }
            }
        });

        orderItemsHolder = findViewById(R.id.orderItemsHolder);
        //orderItemsAdapter = new FeedbackOrderItemsAdapter(context, getSupportFragmentManager(), orderItemsHolder.getMeatItemID(), new ArrayList<CartItemDetails>(), null);
        //orderItemsHolder.setAdapter(orderItemsAdapter);
        //orderItemsHolder.addOnPageChangeListener(orderItemsAdapter);
        orderItemsHolder.setOffscreenPageLimit(1);  // Necessary or the pager will only have one extra page to show make this at least however many pages you can see
        orderItemsHolder.setClipToPadding(false);   // Set margin for pages as a negative number, so a part of next and previous pages will be showed
        //orderItemsHolder.setPageMargin(-1 * this.getResources().getDimensionPixelOffset(R.dimen.carousel_margin));    // REMOVED TEMPORARILY

        //-------------

        showLoadingIndicator("Loading...");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                orderDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.ORDER_DETAILS), OrderDetails.class);
                orderItems = orderDetails.getOrderItemsExcludingSubmittedFeedbackItemsAndFailedItems();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if ( orderDetails != null ) {
                    if ( orderItems.size() > 0 ){
                        switchToContentPage();

                        //order_id.setText("Order ID : " + orderDetails.getOrderID().toUpperCase());
                        //order_date.setText("Ordered on " + DateMethods.getDateInFormat(orderDetails.getCreatedDate(), DateConstants.MMM_DD_YYYY));
                        title.setText(orderDetails.getOrderID().toUpperCase()+" ("+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalAmount())+")");
                        details.setText(orderDetails.getFormattedDeliveryDate()+"\n"+orderDetails.getOrderItemsNames());
                        orderItemsAdapter = new FeedbackOrderItemsAdapter(context, getSupportFragmentManager(), orderItemsHolder.getId(), orderItems, null);
                        orderItemsHolder.setAdapter(orderItemsAdapter);
                        orderItemsHolder.addOnPageChangeListener(orderItemsAdapter);

                        showItem(0);
                    }
                    else{
                        setResult(RESULT_OK);
                        finish();
                    }
                }else{
                    finish();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public void onClick(View view) {
        if ( view == submit ){
            inputMethodManager.hideSoftInputFromWindow(comments.getWindowToken(), 0);    // Hides Key Board After Item Select..

            if ( currentOrderItemFeedback != null ){
                currentOrderItemFeedback.setComments(comments.getText().toString().trim());
                requestSubmitOrderFeedback.addOrUpdateFeedback(currentOrderItemFeedback);
                showItem(currentOrderFoodItemIndex + 1);
            } else{
                showToastMessage("* Please give your rating");
            }
        }
    }

    public void showItem(int index){

        if ( orderItems.size() > index ){
            currentOrderFoodItemIndex = index;
            orderItemsHolder.setCurrentItem(currentOrderFoodItemIndex);

            currentOrderItemFeedback = null;
            rating_bar.setRating(0);
            comments.setText("");
        }
        else {
            submitFeedback();
        }
    }

    public void submitFeedback(){
        
        showLoadingDialog("Submitting feedback...");
        getBackendAPIs().getFeedbacksAPI().submitFeedback(orderDetails.getID(), requestSubmitOrderFeedback, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Thanks for your feedback");
                    setResult(RESULT_OK);
                    finish();
                }else{
                    if (statusCode == 403) {
                        clearUserSession("Your session has expired.");
                    }else{
                        showToastMessage("Something went wrong, please try again!");
                    }
                }
            }
        });
    }
    
    //=========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    
}
