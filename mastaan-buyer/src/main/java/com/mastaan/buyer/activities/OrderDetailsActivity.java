package com.mastaan.buyer.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.OrderItemsAdapter;
import com.mastaan.buyer.adapters.OrderItemsWithStatusAdapter;
import com.mastaan.buyer.backend.OrdersAPI;
import com.mastaan.buyer.backend.models.RequestAddOnlinePayment;
import com.mastaan.buyer.backend.models.RequestAssignToCustomerSupport;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.models.OrderItemDetails;
import com.mastaan.buyer.payment.paytm.PaytmPayment;
import com.mastaan.buyer.payment.razorpay.RazorpayPayment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class OrderDetailsActivity extends MastaanToolBarActivity implements View.OnClickListener{

    String orderID;
    OrderDetails orderDetails;

    View callUs;
    View requestSupport;
    View cancelOrder;

    TextView order_id;
    View orderStatusHolder;
    TextView order_status;
    TextView order_date;
    TextView buyer_name;
    TextView buyer_contact;
    TextView delivery_slot;
    TextView payment_mode;
    View makeOnlinePayment;
    TextView delivery_address;
    View initialAmountDetails;
    TextView initial_amount;
    TextView final_amount;
    TextView sub_total;
    View discountDetails;
    TextView discount_name;
    TextView discount_amount;
    TextView delivery_charges;
    View serviceChargesDetails;
    TextView service_charges;
    View surChargesDetails;
    TextView surcharges;
    View governmentTaxesDetails;
    TextView government_taxes;
    View membershipOrderDiscountDetails;
    TextView membership_order_discount;
    View membershipDeliveryDiscountDetails;
    TextView membership_delivery_discount;
    View cashbackDetails;
    TextView cashback_name;
    TextView cashback_amount;
    View refundAmountDetails;
    TextView refund_amount_title;
    TextView refund_amount;
    View instructionsView;
    TextView instructions;

    vRecyclerView orderItemsWithStatusHolder;
    vRecyclerView orderItemsHolder;

    View giveFeedback;

    OrderItemsWithStatusAdapter orderItemsWithStatusAdapter;
    OrderItemsAdapter orderItemsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        getGoogleAnalyticsMethods().sendScreenName("Order Details");       // Send ScreenNames to Analytics
        hasLoadingView();
        hasSwipeRefresh();

        orderID = getIntent().getStringExtra(Constants.ID);
        registerActivityReceiver(Constants.ORDER_DETAILS_RECEIVER);

        //------------

        callUs =  findViewById(R.id.callUs);
        callUs.setOnClickListener(this);

        requestSupport =  findViewById(R.id.requestSupport);
        requestSupport.setOnClickListener(this);
        cancelOrder =  findViewById(R.id.cancelOrder);
        cancelOrder.setOnClickListener(this);

        order_id = findViewById(R.id.order_id);
        orderStatusHolder = findViewById(R.id.orderStatusHolder);
        order_status = findViewById(R.id.order_status);
        order_date = findViewById(R.id.order_date);
        buyer_name = findViewById(R.id.buyer_name);
        buyer_contact = findViewById(R.id.buyer_contact);
        delivery_slot = findViewById(R.id.delivery_slot);
        payment_mode = findViewById(R.id.payment_mode);
        makeOnlinePayment = findViewById(R.id.makeOnlinePayment);
        makeOnlinePayment.setOnClickListener(this);
        delivery_address = findViewById(R.id.delivery_address);
        initialAmountDetails = findViewById(R.id.initialAmountDetails);
        initial_amount = findViewById(R.id.initial_amount);
        final_amount = findViewById(R.id.final_amount);
        sub_total = findViewById(R.id.sub_total);
        discountDetails = findViewById(R.id.discountDetails);
        discount_name = findViewById(R.id.discount_name);
        discount_amount = findViewById(R.id.discount_amount);
        delivery_charges = findViewById(R.id.delivery_charges);
        serviceChargesDetails = findViewById(R.id.serviceChargesDetails);
        service_charges = findViewById(R.id.service_charges);
        surChargesDetails = findViewById(R.id.surChargesDetails);
        surcharges = findViewById(R.id.surcharges);
        governmentTaxesDetails = findViewById(R.id.governmentTaxesDetails);
        government_taxes = findViewById(R.id.government_taxes);
        membershipOrderDiscountDetails = findViewById(R.id.membershipOrderDiscountDetails);
        membership_order_discount = findViewById(R.id.membership_order_discount);
        membershipDeliveryDiscountDetails = findViewById(R.id.membershipDeliveryDiscountDetails);
        membership_delivery_discount = findViewById(R.id.membership_delivery_discount);
        cashbackDetails = findViewById(R.id.cashbackDetails);
        cashback_name = findViewById(R.id.cashback_name);
        cashback_amount = findViewById(R.id.cashback_amount);
        refundAmountDetails = findViewById(R.id.refundAmountDetails);
        refund_amount_title = findViewById(R.id.refund_amount_title);
        refund_amount = findViewById(R.id.refund_amount);
        instructionsView = findViewById(R.id.instructionsView);
        instructions = findViewById(R.id.instructions);

        orderItemsWithStatusHolder = findViewById(R.id.orderItemsWithStatusHolder);
        orderItemsHolder = findViewById(R.id.orderItemsHolder);

        giveFeedback = findViewById(R.id.giveFeedback);
        giveFeedback.setOnClickListener(this);

        //----------------

        orderItemsWithStatusHolder.setupVerticalOrientation();
        orderItemsWithStatusAdapter = new OrderItemsWithStatusAdapter(context, new ArrayList<OrderItemDetails>(), new OrderItemsWithStatusAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Intent orderItemDetailsAct = new Intent(context, OrderItemDetailsActivity.class);
                orderItemDetailsAct.putExtra(Constants.ORDER_ITEM_DETAILS, new Gson().toJson(orderItemsWithStatusAdapter.getItem(position)));
                startActivity(orderItemDetailsAct);
            }
        });
        orderItemsWithStatusHolder.setAdapter(orderItemsWithStatusAdapter);

        orderItemsHolder.setupHorizontalOrientation();
        orderItemsAdapter = new OrderItemsAdapter(context, new ArrayList<OrderItemDetails>(), new OrderItemsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Intent orderItemDetailsAct = new Intent(context, OrderItemDetailsActivity.class);
                orderItemDetailsAct.putExtra(Constants.ORDER_ITEM_DETAILS, new Gson().toJson(orderItemsAdapter.getItem(position)));
                startActivity(orderItemDetailsAct);
            }
        });
        orderItemsHolder.setAdapter(orderItemsAdapter);

        //----------------

        getOrderDetails();              //   Loadign Order Details

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ( getIntent().getStringExtra(Constants.ID) != null ){
            orderID = getIntent().getStringExtra(Constants.ID);
            onReloadPressed();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getOrderDetails();
    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);
        try {
            if ( receivedData.getStringExtra(Constants.ACTION_TYPE).equalsIgnoreCase(Constants.RELOAD_DATA) ){
                if ( receivedData.getStringExtra(Constants.ID) != null && receivedData.getStringExtra(Constants.ID).equals(orderID) ){
                    onReloadPressed();
                }
            }
            else if (receivedData.getStringExtra(Constants.ACTION_TYPE).equalsIgnoreCase(Constants.DISPLAY_ORDER_STATUS)) {
                getOrderDetails(true);
            }
        }catch (Exception e){}
    }

    @Override
    public void onClick(View view) {

        if ( view == callUs ){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
            startActivity(intent);
        }

        else if ( view == requestSupport ){
            final String[] optionsList = new String[]{"With order", "With item"};
            ListChooserCallback callback = new ListChooserCallback() {
                @Override
                public void onSelect(final int position) {
                    if ( optionsList[position].equalsIgnoreCase("With order") ){
                        showInputTextDialog("How can we help you?", Html.fromHtml("Kindly add your query below and our customer support executive will call you at the earliest. Order cancellations cannot be accepted through this support, kindly call us at <b>"+localStorageData.getSupportContactNumber()+"</b>."), "Query", "", new TextInputCallback() {
                            @Override
                            public void onComplete(String inputText) {
                                RequestAssignToCustomerSupport requestObject = new RequestAssignToCustomerSupport(orderDetails.getOrderItems(), inputText);
                                assignToCustomerSupport(requestObject);
                            }
                        });
                    }
                    else if ( optionsList[position].equalsIgnoreCase("With item") ){
                        final List<OrderItemDetails> orderItemsList = orderDetails.getOrderItems();
                        final List<String> selectedItems = new ArrayList<>();

                        View itemsPickerDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_order_items_picker, null);
                        LinearLayout itemsHolder = (LinearLayout) itemsPickerDialogView.findViewById(R.id.itemsHolder);

                        for (int i=0;i< orderItemsList.size();i++){
                            final OrderItemDetails orderItemDetails = orderItemsList.get(i);
                            if ( orderItemDetails != null ){
                                View itemView = LayoutInflater.from(context).inflate(R.layout.view_checkbox_order_item, null);
                                final CheckBox check_box = (CheckBox) itemView.findViewById(R.id.check_box);
                                itemsHolder.addView(itemView);

                                check_box.setText(orderItemDetails.getMeatItemDetails().getName()
                                        +"\n"
                                        + RS + CommonMethods.getInDecimalFormat(orderItemDetails.getTotal())
                                        + ", "+CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " " + orderItemDetails.getMeatItemDetails().getQuantityUnit()+(orderItemDetails.getQuantity()!=1?"s":""));

                                itemView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if ( check_box.isChecked() ){
                                            check_box.setChecked(false);
                                            selectedItems.remove(orderItemDetails.getID());
                                        }else{
                                            check_box.setChecked(true);
                                            selectedItems.add(orderItemDetails.getID());
                                        }
                                    }
                                });
                            }
                        }

                        itemsPickerDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                closeCustomDialog();
                            }
                        });


                        itemsPickerDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if ( selectedItems != null && selectedItems.size() > 0 ){
                                    closeCustomDialog();
                                    showInputTextDialog("How can we help you?", Html.fromHtml("Kindly add your query below and our customer support executive will call you at the earliest.<br>Item cancellations cannot be accepted through this support, kindly call us at <b>"+localStorageData.getSupportContactNumber()+"</b>."), "Query", "", new TextInputCallback() {
                                        @Override
                                        public void onComplete(String inputText) {
                                            RequestAssignToCustomerSupport requestObject = new RequestAssignToCustomerSupport(inputText, selectedItems);
                                            assignToCustomerSupport(requestObject);
                                        }
                                    });
                                }else{
                                    showToastMessage("* Please select atleast one item");
                                }
                            }
                        });

                        showCustomDialog(itemsPickerDialogView);
                    }
                }
            };
            if ( orderDetails.getOrderItems().size() == 1 ){
                callback.onSelect(0);
            }else{
                showListChooserDialog("Support", optionsList, callback);
            }
        }

        else if ( view == cancelOrder ){
            final String[] optionsList = new String[]{"Cancel order", "Cancel item"};
            ListChooserCallback callback = new ListChooserCallback() {
                @Override
                public void onSelect(final int position) {
                    if ( optionsList[position].equalsIgnoreCase("Cancel order") ){
                        showInputTextDialog("Why do you want to cancel?", "Reason", new TextInputCallback() {
                            @Override
                            public void onComplete(String inputText) {
                                cancelOrder(inputText);
                            }
                        });
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Cancel item") ){
                        final List<OrderItemDetails> orderItemsList = new ArrayList<>();
                        for (int i=0;i<orderDetails.getOrderItems().size();i++){
                            if ( orderDetails.getOrderItems().get(i).getStatusString().equalsIgnoreCase(Constants.FAILED) == false
                                    && orderDetails.getOrderItems().get(i).getStatusString().equalsIgnoreCase(Constants.REJECTED) == false ){
                                orderItemsList.add(orderDetails.getOrderItems().get(i));
                            }
                        }
                        if ( orderItemsList.size() > 0 ){
                            List<String> orderItemsStrings = new ArrayList<>();
                            for (int i=0;i<orderItemsList.size();i++){
                                OrderItemDetails orderItemDetails = orderItemsList.get(i);
                                orderItemsStrings.add(orderItemDetails.getMeatItemDetails().getName()
                                        +"\n"
                                        + RS + CommonMethods.getInDecimalFormat(orderItemDetails.getTotal())
                                        + ", "+CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " " + orderItemDetails.getMeatItemDetails().getQuantityUnit()+(orderItemDetails.getQuantity()!=1?"s":""));
                            }

                            showListChooserDialog("Select item", orderItemsStrings, new ListChooserCallback() {
                                @Override
                                public void onSelect(final int position) {
                                    showInputTextDialog("Why do you want to cancel?", "Reason", new TextInputCallback() {
                                        @Override
                                        public void onComplete(String inputText) {
                                            checkForCancelOrderItemAndProceedToCancelOrderItem(orderItemsList.get(position), inputText);
                                        }
                                    });
                                }
                            });
                        }
                        else{
                            showToastMessage("No items to cancel");
                        }
                    }
                }
            };
            if ( orderDetails.getOrderItems().size() == 1 ){
                callback.onSelect(0);
            }else{
                showListChooserDialog(optionsList, callback);
            }

        }

        else if ( view == makeOnlinePayment ){
            View dialogMakeOnlinePaymentView = LayoutInflater.from(context).inflate(R.layout.dialog_make_online_payment, null);
            final vTextInputLayout amount = dialogMakeOnlinePaymentView.findViewById(R.id.amount);
            amount.setText(CommonMethods.getInDecimalFormat(orderDetails.getCODAmount())+"");

            dialogMakeOnlinePaymentView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeCustomDialog();
                }
            });
            dialogMakeOnlinePaymentView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String oAmount = amount.getText();
                    amount.checkError("* Enter amount");
                    if ( oAmount.length() > 0 ){
                        inputMethodManager.hideSoftInputFromWindow(amount.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
                        closeCustomDialog();

                        getRazorpayPayment().proceedForOrder(orderDetails, Double.parseDouble(oAmount), new RazorpayPayment.Callback() {
                            @Override
                            public void onPaymentSuccess(String message, int paymentType, double amount, String transactionID, String paymentID) {
                                addOnlinePaymentForOrder(new RequestAddOnlinePayment(paymentType, amount, transactionID, paymentID));
                            }

                            @Override
                            public void onPaymentFailure(String message, double amount, String newTransactioinID) {
                                showSnackbarMessage("Payment failed");
                                showNotificationDialog(false, "Failure!", "Your payment is failed. If the money is debited from your account, it will be refunded.", new ActionCallback() {
                                    @Override
                                    public void onAction() {
                                        getOrderDetails();
                                    }
                                });
                            }

                            @Override
                            public void onPaymentCancel() {
                                showSnackbarMessage("Payment Cancelled");
                                getOrderDetails();
                            }

                            @Override
                            public void onExternalWalletSelect(String walletName, double amount) {
                                if ( walletName.equalsIgnoreCase("paytm") ){
                                    new PaytmPayment(activity).proceed(orderDetails, amount, new PaytmPayment.Callback() {
                                        @Override
                                        public void onPaymentSuccess(String message, int paymentType, double amount, String transactionID, String paymentID) {
                                            addOnlinePaymentForOrder(new RequestAddOnlinePayment(paymentType, amount, transactionID, paymentID));
                                        }

                                        @Override
                                        public void onPaymentFailure(String message, double amount, String newTransactioinID) {
                                            showNotificationDialog(false, "Failure!", "Your payment is failed. If the money is debited from your account, it will be refunded.", new ActionCallback() {
                                                @Override
                                                public void onAction() {
                                                    getOrderDetails();
                                                }
                                            });
                                        }

                                        @Override
                                        public void onPaymentCancel() {
                                            showSnackbarMessage("Payment Cancelled");
                                            getOrderDetails();
                                        }
                                    });
                                }else{
                                    showToastMessage("Something went wrong, try again!");
                                }
                            }
                        });
                    }
                }
            });
            showCustomDialog(dialogMakeOnlinePaymentView);
        }

        else if ( view == giveFeedback ){
            Intent feedbackAct = new Intent(context, OrderFeedbackActivity.class);
            feedbackAct.putExtra(Constants.ORDER_DETAILS, new Gson().toJson(orderDetails));
            startActivityForResult(feedbackAct, Constants.FEEDBACK_ACTIVITY_CODE);
        }

    }

    public void displayOrderDetails(){

        try{
            switchToContentPage();

            order_id.setText(orderDetails.getOrderID());
            buyer_name.setText(CommonMethods.capitalizeFirstLetter(orderDetails.getCustomerName()));
            buyer_contact.setText(orderDetails.getFormattedContactDetails());

            delivery_slot.setText(orderDetails.getFormattedDeliveryDate());//DateMethods.getDateInFormat(orderDetails.getDeliveryDateInISTFormat(), DateConstants.MMM_DD_YYYY_HH_MM_A));

            payment_mode.setText(orderDetails.getPaymentString());

            if ( orderDetails.getCODAmount() > 0
                    && ( orderDetails.getStatus().equalsIgnoreCase("pending") || orderDetails.getStatus().equalsIgnoreCase("delivering")) ){
                makeOnlinePayment.setVisibility(View.VISIBLE);
            }else{
                makeOnlinePayment.setVisibility(View.GONE);
            }

            delivery_address.setText(orderDetails.getDeliveryAddress());
            order_date.setText(DateMethods.getDateInFormat(orderDetails.getCreatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A));

            if ( orderDetails.getInitialTotalAmount() > 0 && orderDetails.getTotalAmount() != orderDetails.getInitialTotalAmount() ){
                initialAmountDetails.setVisibility(View.VISIBLE);
                initial_amount.setText(SpecialCharacters.RS + " " + new DecimalFormat("0.00").format(orderDetails.getInitialTotalAmount()));
            }else{  initialAmountDetails.setVisibility(View.GONE);  }

            //------

            final_amount.setText(SpecialCharacters.RS + " " + new DecimalFormat("0.00").format(orderDetails.getTotalAmount()));

            sub_total.setText(SpecialCharacters.RS + " " + new DecimalFormat("0.00").format(orderDetails.getSubTotal()));

            if ( orderDetails.getCouponDiscount() > 0 ){
                discountDetails.setVisibility(View.VISIBLE);
                if ( orderDetails.getCouponcode() != null && orderDetails.getCouponcode().length() > 0 ){
                    discount_name.setText(Html.fromHtml("Discount<br>(<b>"+orderDetails.getCouponcode().toUpperCase()+"</b>)"));
                }
                discount_amount.setText("- "+SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(orderDetails.getCouponDiscount()));
            }else{  discountDetails.setVisibility(View.GONE);  }


            if ( orderDetails.getMembershipDiscount() > 0 ){
                membershipOrderDiscountDetails.setVisibility(View.VISIBLE);
                membership_order_discount.setText("- "+SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(orderDetails.getMembershipOrderDiscount()));
            }else{  membershipOrderDiscountDetails.setVisibility(View.GONE); }

            if ( orderDetails.getMembershipDeliveryDiscount() > 0 ){
                membershipDeliveryDiscountDetails.setVisibility(View.VISIBLE);
                membership_delivery_discount.setText("- "+SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(orderDetails.getMembershipDeliveryDiscount()));
            }else{  membershipDeliveryDiscountDetails.setVisibility(View.GONE); }

            delivery_charges.setText(SpecialCharacters.RS + " " + new DecimalFormat("0.00").format(orderDetails.getDeliveryCharges()));

            if ( orderDetails.getServiceCharges() > 0 ){
                serviceChargesDetails.setVisibility(View.VISIBLE);
                service_charges.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(orderDetails.getServiceCharges()));
            }else{  serviceChargesDetails.setVisibility(View.GONE);  }

            if ( orderDetails.getSurcharges() > 0 ){
                surChargesDetails.setVisibility(View.VISIBLE);
                surcharges.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(orderDetails.getSurcharges()));
            }else{  surChargesDetails.setVisibility(View.GONE);  }

            if ( orderDetails.getGovernmentTaxes() > 0 ){
                governmentTaxesDetails.setVisibility(View.VISIBLE);
                government_taxes.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(orderDetails.getGovernmentTaxes()));
            }else{  governmentTaxesDetails.setVisibility(View.GONE);  }

            if ( orderDetails.getCouponCashback() > 0 ){
                cashbackDetails.setVisibility(View.VISIBLE);
                if ( orderDetails.getCouponcode() != null && orderDetails.getCouponcode().length() > 0 ){
                    cashback_name.setText(Html.fromHtml("Cashback<br>(<b>"+orderDetails.getCouponcode().toUpperCase()+"</b>)"));
                }
                cashback_amount.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(orderDetails.getCouponCashback()));
            }else{  cashbackDetails.setVisibility(View.GONE);  }

            //------

            if ( orderDetails.getCODAmount() < 0 || orderDetails.getRefundedAmount() > 0 ){
                refundAmountDetails.setVisibility(View.VISIBLE);
                if ( orderDetails.getRefundedAmount() > 0 ){
                    refund_amount_title.setText("Refunded amount");
                    refund_amount.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(Math.abs(orderDetails.getRefundedAmount()))
                        +(orderDetails.getCODAmount()<0?"\n(Remain: "+SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(Math.abs(orderDetails.getCODAmount()))+")":""));
                }
                else{
                    refund_amount_title.setText("Refund amount");
                    refund_amount.setText(SpecialCharacters.RS+" "+new DecimalFormat("0.00").format(Math.abs(orderDetails.getCODAmount())));
                }
            }else{  refundAmountDetails.setVisibility(View.GONE);   }

            //-------

            String deliveryDate = DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate(), DateConstants.DD_MM_YYYY);
            String todaysDate = DateMethods.getOnlyDate(localStorageData.getServerTime());

            requestSupport.setVisibility(DateMethods.compareDates(deliveryDate, todaysDate) == 0?View.VISIBLE:View.GONE);
            cancelOrder.setVisibility((orderDetails.getStatus().equalsIgnoreCase(Constants.PENDING) && DateMethods.compareDates(deliveryDate, todaysDate) > 0)
                    ?
                    View.VISIBLE:View.GONE);

            //--------

            if ( orderDetails.getSpecialInstructions() != null && orderDetails.getSpecialInstructions().trim().length() > 0 ){
                instructionsView.setVisibility(View.VISIBLE);
                instructions.setText(orderDetails.getSpecialInstructions());
            }else{  instructionsView.setVisibility(View.GONE);  }

            order_status.setText(CommonMethods.capitalizeFirstLetter(orderDetails.getStatus()));

            if ( orderDetails.getStatus().equalsIgnoreCase(Constants.DELIVERED) == false
                    && orderDetails.getStatus().equalsIgnoreCase(Constants.FAILED) == false ){
                orderItemsWithStatusHolder.setVisibility(View.VISIBLE);
                orderItemsWithStatusAdapter.setItems(orderDetails.getOrderItems());
                orderStatusHolder.setVisibility(View.GONE);
                orderItemsHolder.setVisibility(View.GONE);
            }
            else{
                orderItemsHolder.setVisibility(View.VISIBLE);
                orderItemsAdapter.setItems(orderDetails.getOrderItems());
                orderItemsWithStatusHolder.setVisibility(View.GONE);
                orderStatusHolder.setVisibility(View.VISIBLE);
            }

            //-------

            giveFeedback.setVisibility(orderDetails.getStatus().equalsIgnoreCase(Constants.DELIVERED) && orderDetails.isGivenFeedback() == false ?View.VISIBLE:View.GONE);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void getOrderDetails(){
        getOrderDetails(false);
    }
    public void getOrderDetails(final boolean loadInBackground){

        if ( loadInBackground == false ){   showLoadingIndicator("Loading order details, wait..");  }
        getBackendAPIs().getOrdersAPI().getOrderDetails(orderID, new OrdersAPI.OrderDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, OrderDetails order_details) {
                if ( status == true ){
                    orderDetails = order_details;
                    displayOrderDetails();
                }
                else {
                    checkSessionValidity(statusCode);
                    if ( loadInBackground == false ){  showReloadIndicator(statusCode, "Unable to load order details, try again!"); }
                }
            }
        });
    }

    public void addOnlinePaymentForOrder(final RequestAddOnlinePayment requestAddOnlinePayment){

        showLoadingDialog("Processing payment, wait...");
        getBackendAPIs().getPaymentAPI().addOnlinePaymentForOrder(orderDetails.getID(), requestAddOnlinePayment, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Payment successfull");
                    getOrderDetails();
                }else{
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while processing payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                addOnlinePaymentForOrder(requestAddOnlinePayment);
                            }
                        }
                    });
                }
            }
        });
    }

    public void cancelOrder(final String cancellationReason){

        showLoadingDialog("Cancelling order, wait...");
        getBackendAPIs().getOrdersAPI().cancelOrder(orderID, cancellationReason, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    // send e-commerce refund transaction to google analytics
                    getGoogleAnalyticsMethods().sendOrderRefundTransaction(orderDetails);

                    showNotificationDialog(false, "Success", "Your order has been cancelled."
                            + (orderDetails.getOnlinePaymentAmount() > 0 ? " If you have made an online payment, a refund will be processed shortly and may take 3-5 business days to reflect in your account." : ""), new ActionCallback() {
                        @Override
                        public void onAction() {
                            getOrderDetails();
                        }
                    });
                }else{
                    if ( message.equalsIgnoreCase("Error") ){
                        showChoiceSelectionDialog(false, "Failure", "Something went wrong while cancelling order.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("RETRY") ){
                                    cancelOrder(cancellationReason);
                                }
                            }
                        });
                    }else{
                        showNotificationDialog("Sorry, cancel failed", message);
                    }

                }
            }
        });

    }

    public void checkForCancelOrderItemAndProceedToCancelOrderItem(final OrderItemDetails orderItemDetails, final String cancellationReason){

        showLoadingDialog("Cancelling order, wait...");
        getBackendAPIs().getOrdersAPI().checkForCancelOrderItem(orderItemDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if ( status ){
                    cancelOrderItem(false, orderItemDetails, cancellationReason);
                }
                else{
                    closeLoadingDialog();
                    if ( statusCode == 500 ){
                        showChoiceSelectionDialog(false, "Confirm", message, "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("YES") ){
                                    cancelOrderItem(true, orderItemDetails, cancellationReason);
                                }
                            }
                        });
                    }else{
                        showChoiceSelectionDialog(false, "Failure", "Something went wrong while canceling order item. Please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("RETRY") ){
                                    checkForCancelOrderItemAndProceedToCancelOrderItem(orderItemDetails, cancellationReason);
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    public void cancelOrderItem(final boolean showDialog, final OrderItemDetails orderItemDetails, final String cancellationReason){

        if ( showDialog ){  showLoadingDialog("Cancelling order item, wait..."); }
        getBackendAPIs().getOrdersAPI().cancelOrderItem(orderItemDetails.getID(), cancellationReason, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    // send e-commerce refund transaction to google analytics
                    getGoogleAnalyticsMethods().sendOrderItemRefundTransaction(orderID, orderItemDetails);

                    showNotificationDialog(false, "Success", "Your order item has been cancelled."
                            + (orderDetails.getOnlinePaymentAmount() > 0 ? " If you have made an online payment, a refund will be processed shortly and may take 3-5 business days to reflect in your account." : ""), new ActionCallback() {
                        @Override
                        public void onAction() {
                            getOrderDetails();
                        }
                    });
                }else{
                    if ( message.equalsIgnoreCase("Error") ){
                        showChoiceSelectionDialog(false, "Failure", "Something went wrong while cancelling order item.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("RETRY") ){
                                    cancelOrderItem(showDialog, orderItemDetails, cancellationReason);
                                }
                            }
                        });
                    }else{
                        showNotificationDialog("Sorry, cancel failed", message);
                    }

                }
            }
        });
    }

    public void assignToCustomerSupport(final RequestAssignToCustomerSupport requestObject){

        showLoadingDialog("Assigning to customer support, wait...");
        getBackendAPIs().getCustomerSupportAPI().assignToCustomerSupport(orderID, requestObject, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    getGoogleAnalyticsMethods().sendEvent("Customer Support", "Assign to customer support", "Success");         // Send Event to Analytics

                    showToastMessage("Assigned to customer support successfully");
                    getOrderDetails();
                }else{
                    getGoogleAnalyticsMethods().sendEvent("Customer Support", "Assign to customer support", "Failure");         // Send Event to Analytics

                    showChoiceSelectionDialog(false, "Failure", "Something went wrong while cancelling order item.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                assignToCustomerSupport(requestObject);
                            }
                        }
                    });
                }
            }
        });
    }


    //==============

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.FEEDBACK_ACTIVITY_CODE && resultCode == RESULT_OK ){
                giveFeedback.setVisibility(View.GONE);
                new BroadcastReceiversMethods(context).displayPendingFeedbacksInHome();
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
