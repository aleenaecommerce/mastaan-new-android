package com.mastaan.buyer.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.AuthenticationAPI;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.backend.models.ResponseValidateEmail;
import com.mastaan.buyer.backend.models.ResponseVerifyEmail;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;
import com.mastaan.buyer.models.BootStrapDetails;


public class RegisterWithEmailActivity extends MastaanToolBarActivity implements View.OnClickListener{

    String pinID;

    ViewSwitcher viewSwitcher;
    LinearLayout register_layout;
    vTextInputLayout email;
    AppCompatAutoCompleteTextView emailAutoCompleteTextview;
    ListArrayAdapter registeredEmailsAdapter;

    Button register;
    ProgressBar validatingIndicator;
    CheckBox acceptTnC;
    View showTnC;
    TextView validationStatus;

    vTextInputLayout otp;
    Button verify;
    ProgressBar verifyingIndicator;
    TextView verficationStatus;

    boolean onResend;
    FrameLayout not_received_otp;

    String emailAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_register_with_email);
        getGoogleAnalyticsMethods().sendScreenName("Register with Email");       // Send ScreenNames to Analytics

        //......................................................

        viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);

        register_layout = (LinearLayout) findViewById(R.id.register_layout);

        email = (vTextInputLayout) findViewById(R.id.email);
        emailAutoCompleteTextview = (AppCompatAutoCompleteTextView) findViewById(R.id.emailAutoCompleteTextview);
        emailAutoCompleteTextview.setThreshold(1);
        registeredEmailsAdapter = new ListArrayAdapter(context, CommonMethods.getRegisteredEmails(context));
        emailAutoCompleteTextview.setAdapter(registeredEmailsAdapter);

        validatingIndicator = (ProgressBar) findViewById(R.id.validatingIndicator);
        validationStatus = (TextView) findViewById(R.id.validationStatus);
        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(this);
        acceptTnC = (CheckBox) findViewById(R.id.acceptTnC);
        showTnC = findViewById(R.id.showTnC);
        showTnC.setOnClickListener(this);
        acceptTnC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                register.setEnabled(isChecked);
            }
        });

        otp = (vTextInputLayout) findViewById(R.id.otp);
        verifyingIndicator = (ProgressBar) findViewById(R.id.verifyingIndicator);
        verficationStatus = (TextView) findViewById(R.id.verficationStatus);
        verify = (Button) findViewById(R.id.verify);
        verify.setOnClickListener(this);

        not_received_otp = (FrameLayout) findViewById(R.id.not_received_otp);
        not_received_otp.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        try {
            inputMethodManager.hideSoftInputFromWindow(email.getWindowToken(), 0);    // Hides Key Board After Item Select..
            inputMethodManager.hideSoftInputFromWindow(otp.getWindowToken(), 0);    // Hides Key Board After Item Select..
        }catch (Exception e){}

        if ( view == showTnC ){
            Intent tncAct = new Intent(context, TermsAndConditionsActivity.class);
            startActivity(tncAct);
        }
        else if ( view == register ){

            validationStatus.setText("");
            email.checkError("*Enter Email number");
            emailAddress = email.getText();

            if ( CommonMethods.isValidEmailAddress(emailAddress) ){
                validateEmail();
            }else {
                email.showError("*Enter a valid email address");
            }
        }

        else if ( view == verify ){
            verficationStatus.setText("");
            otp.checkError("*Enter OTP sent to you");
            String oOTP = otp.getText();

            if (oOTP.length() > 0) {
                if ( localStorageData.getSessionFlag() ){
                    getBootstrapDetails();
                }
                // NORMAT VERIFICATION URL WITH OTP
                else {
                    verifyEmail(oOTP);
                }
            }
        }
        else if ( view == not_received_otp ){
            showLoadingIndicator("Resending OTP, wait..");
            onResend = true;
            validateEmail();
        }


    }

    public void validateEmail(){

        register.setEnabled(false);
        validatingIndicator.setVisibility(View.VISIBLE);

        getBackendAPIs().getAuthenticationAPI().validateEmail(emailAddress, new AuthenticationAPI.EmailValidationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ResponseValidateEmail responseValidateEmail) {

                if (status == true) {
                    Toast.makeText(context, "Enter the OTP sent to your email " + emailAddress, Toast.LENGTH_LONG).show();
                    pinID = responseValidateEmail.getPinID();
                    viewSwitcher.setDisplayedChild(1);

                } else {
                    if (onResend) {
                        Toast.makeText(context, "Something went wrong while sending otp , try again!", Toast.LENGTH_LONG).show();
                        closeLoadingDialog();
                    }
                    validationStatus.setText("Something went wrong while sending otp , try again!");
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            validationStatus.setText("No Internet connection!");
                        } else {
                            validationStatus.setText("Not able to connect to Server. \nPlease try again after a while");
                        }
                    } else if (statusCode == 500) {
                        if (message.equalsIgnoreCase("problem_pin_email")) {
                            validationStatus.setText("There was a problem sending PIN to your email address. Please try again");
                        }
                    }
                    register.setEnabled(true);
                    validatingIndicator.setVisibility(View.GONE);
                }
            }
        });
    }

    public void verifyEmail(String otp){

        verify.setEnabled(false);
        verifyingIndicator.setVisibility(View.VISIBLE);

        getBackendAPIs().getAuthenticationAPI().verifyEmail(pinID, otp, new AuthenticationAPI.EmailVerificationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ResponseVerifyEmail responseVerifyEmail) {
                if (status) {
                    getGoogleAnalyticsMethods().sendEvent("Auth", responseVerifyEmail.isSignUp()?"Signup":"Signin", responseVerifyEmail.getBuyerDetails().getEmail());

                    Log.d(Constants.LOG_TAG, "Email Verification Completed, For Email = " + responseVerifyEmail.getBuyerDetails().getEmail());
                    localStorageData.storeBuyerSession(responseVerifyEmail.getToken(), responseVerifyEmail.getBuyerDetails());        // STORING USER SESSION

                    // Linking buyer to FCM
                    try {
                        getBackendAPIs().getProfileAPI().linkToFCM(FirebaseInstanceId.getInstance().getToken(), null);
                    }catch (Exception e){}

                    getBootstrapDetails();
                } else {
                    showVerificationRetryView(statusCode, message);
                }
            }
        });
    }

    private void showVerificationRetryView(int statusCode, String message){

        verficationStatus.setText("We couldn't log you in. Please try again later.");
        if (statusCode == -1) {
            boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
            if (isInternetAvailable == false) {
                verficationStatus.setText("No Internet connection!");
            } else {
                verficationStatus.setText("Not able to connect to Server. \nPlease try again after a while");
            }
        } else if (statusCode == 500) {
            if (message.equalsIgnoreCase("incorrect_pin")) {
                verficationStatus.setText("The PIN you entered is wrong. Please try again!");
            } else if (message.equalsIgnoreCase("buyer_not_found")) {
                verficationStatus.setText("We could not find your account details. Please contact our customer support if you continue facing this problem.");
            } else if (message.equalsIgnoreCase("session_create_error")) {
                verficationStatus.setText("We couldn't log you in. Please try again later.");
            }
        }
        verify.setEnabled(true);
        verifyingIndicator.setVisibility(View.GONE);
        viewSwitcher.setDisplayedChild(1);
    }

    public void getBootstrapDetails(){

        getBackendAPIs().getGeneralAPI().getBootStrap(new GeneralAPI.BootStrapCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final BootStrapDetails bootStrapDetails) {
                if ( status ){

                    new AsyncTask<Void, Void, Void>() {
                        boolean showDeliveryAddressChangeAlert = false;
                        @Override
                        protected Void doInBackground(Void... voids) {
                            localStorageData.setServerTime(CommonMethods.getReadableTimeFromUTC(bootStrapDetails.getServerTime()));

                            localStorageData.setServingAt(bootStrapDetails.getServingAt());
                            localStorageData.setSupportContactNumber(bootStrapDetails.getSupportContactNumber());

                            localStorageData.setDeliveringHoursStartTime(bootStrapDetails.getDeliveringHoursStartTime());
                            localStorageData.setDeliveringHoursEndTime(bootStrapDetails.getDeliveringHoursEndTime());

                            localStorageData.storeInstallSources(bootStrapDetails.getInstallSources());

                            localStorageData.storeMeatItemWeights(bootStrapDetails.getMeatItemWeights());
                            localStorageData.storeMeatItemSets(bootStrapDetails.getMeatItemSets());
                            localStorageData.storeMeatItemPieces(bootStrapDetails.getMeatItemPieces());

                            //----

                            localStorageData.setBuyerDetails(bootStrapDetails.getBuyerDetails());

                            localStorageData.setNotificationsFlag(bootStrapDetails.getBuyerDetails().getPromotionalAlertsPreference());

                            localStorageData.setCartCount(bootStrapDetails.getCartItemsCount());
                            localStorageData.setBuyerPendingOrders(bootStrapDetails.getPendingOrders());

                            if ( bootStrapDetails.getCartItemsCount() > 0 ) {
                                AddressBookItemDetails storedDeliveryAddressDetails = localStorageData.getDeliveryLocation();
                                if ( storedDeliveryAddressDetails.getID().equals(bootStrapDetails.getCartDeliveryAddress().getID()) == false
                                        && storedDeliveryAddressDetails.getFullAddress().equalsIgnoreCase(bootStrapDetails.getCartDeliveryAddress().getFullAddress()) == false ){
                                    showDeliveryAddressChangeAlert = true;
                                }
                                localStorageData.setDeliveryLocation(bootStrapDetails.getCartDeliveryAddress());
                            }
                            localStorageData.setHasLocationHistory(bootStrapDetails.getAddressBookList()!=null&&bootStrapDetails.getAddressBookList().size()>0?true:false);

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);

                            new BroadcastReceiversMethods(context).updateUserSessionInfo();

                            if ( showDeliveryAddressChangeAlert ){
                                showToastMessage("Your delivery location is changed to match your cart items delivery location");
                            }

                            /*if ( localStorageData.getNoOfPendingOrders() > 0 ){
                                context.startService(firebaseService);
                            }*/

                            //--------

                            Intent verificationIntent = new Intent();
                            setResult(RESULT_OK, verificationIntent);
                            finish();
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }else{
                    showVerificationRetryView(statusCode, "");
                }
            }
        });

    }

    //=========================================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        //inputMethodManager.hideSoftInputFromWindow(mobile.getWindowToken(), 0);    // Hides Key Board After Item Select..
        //inputMethodManager.hideSoftInputFromWindow(otp.getWindowToken(), 0);    // Hides Key Board After Item Select..
        onBackPressed();
        return true;
    }

}
