package com.mastaan.buyer.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.GlobalItemsAdapter;
import com.mastaan.buyer.adapters.MeatItemsAdapter;
import com.mastaan.buyer.adapters.MessagesAdapter;
import com.mastaan.buyer.backend.MeatItemsAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.interfaces.MessagesCallback;
import com.mastaan.buyer.interfaces.RegistrationCallback;
import com.mastaan.buyer.models.BuyerDetails;
import com.mastaan.buyer.models.GlobalItemDetails;
import com.mastaan.buyer.models.MessageDetails;
import com.mastaan.buyer.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

public class MeatItemsActivity extends MastaanToolBarActivity implements View.OnClickListener{

    String searchQuery;
    MenuItem searchViewMenuITem;
    SearchView searchView;

    String categoryID;
    String categoryValue;
    String categoryName;

    FrameLayout cartInfo;
    ImageButton cartFAB;
    TextView cart_count;

    TextView call_us;

    vRecyclerView itemsHolder;
    GlobalItemsAdapter globalItemsAdapter;

    List<WarehouseMeatItemDetails> originalMeatItemsList = new ArrayList<>();
    List<MessageDetails> messagesList = new ArrayList<>();

    WarehouseMeatItemDetails selectedWarehouseMeatItemDetails;


    public interface SearchResultsCallback{
        void onComplete(List<WarehouseMeatItemDetails> searchList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meat_items);
        hasLoadingView();
        hasSwipeRefresh();

        categoryID = getIntent().getStringExtra(Constants.CATEGORY_ID);
        categoryValue = getIntent().getStringExtra(Constants.CATEGORY_VALUE);
        categoryName = getIntent().getStringExtra(Constants.CATEGORY_NAME);
        if ( categoryName == null ){    categoryName = "Meat Items";  }

        getGoogleAnalyticsMethods().sendScreenName(categoryName);       // Send ScreenNames to Analytics

        setToolBarTitle(categoryName+" ");

        try{
            if ( getIntent().getStringExtra(Constants.COLOR_PRIMARY) != null  ){
                setThemeColors(Color.parseColor(getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK)!=null?getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK):getIntent().getStringExtra(Constants.COLOR_PRIMARY))
                        , Color.parseColor(getIntent().getStringExtra(Constants.COLOR_PRIMARY))
                );
            }
        }catch (Exception e){}

        registerActivityReceiver(Constants.MEAT_ITEMS_RECEIVER);

        //-------------------------------------

        cartInfo = findViewById(R.id.cartInfo);
        cart_count = findViewById(R.id.cart_count);
        cartFAB = findViewById(R.id.cartFAB);
        cartFAB.setOnClickListener(this);

        call_us = findViewById(R.id.call_us);
        call_us.setOnClickListener(this);
        call_us.setText("Need help ordering? Call "+localStorageData.getSupportContactNumber());

        itemsHolder = findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        globalItemsAdapter = new GlobalItemsAdapter(context, new ArrayList<GlobalItemDetails>()).setMeatItemsCallback(new MeatItemsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                proceedToAddToCart(globalItemsAdapter.getItem(position).getWarehouseMeatItemDetails());
            }

            @Override
            public void onCartClick(int position) {
                proceedToAddToCart(globalItemsAdapter.getItem(position).getWarehouseMeatItemDetails());
            }

            @Override
            public void onNotifyClick(final int position) {
                if ( localStorageData.getSessionFlag() ){
                    registerForMeatItemAvailabilityAlerts(position);
                }
                else {
                    goToRegistration(new RegistrationCallback() {
                        @Override
                        public void onComplete(boolean status, String token, BuyerDetails buyerDetails) {
                            if (status) {
                                onNotifyClick(position);
                            }else{
                                showToastMessage("* Please login to get notified.");
                            }
                        }
                    });
                }
            }
        }).setMessagesCallback(new MessagesAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                handleMessageClick(globalItemsAdapter.getItem(position).getMessageDetails());
            }
        });
        itemsHolder.setAdapter(globalItemsAdapter);

        //-------------------------

        checkCartStatus("");

        getMeatItems();

    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);
        try{
            if ( receivedData.getStringExtra(Constants.ACTION_TYPE).equalsIgnoreCase(Constants.DISPLAY_USER_SESSION_INFO) ){
                checkCartStatus("");
            }
            else if ( receivedData.getStringExtra(Constants.ACTION_TYPE).equalsIgnoreCase(Constants.CHECK_CART_STATUS) ){
                checkCartStatus("");
            }
        }catch (Exception e){}
    }

    @Override
    public void onClick(View view) {

        if ( view == cartFAB){
            Intent cartAct = new Intent(context, CartActivity.class);
            startActivityForResult(cartAct, Constants.CART_ACTIVITY_CODE);
        }
        else if ( view == call_us ){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
            startActivity(intent);
        }
    }

    public void proceedToAddToCart(WarehouseMeatItemDetails warehouseMeatItemDetails){

        selectedWarehouseMeatItemDetails = warehouseMeatItemDetails;

        if ( localStorageData.getSessionFlag() ){
            Intent addCartItemAct = new Intent(context, AddorEditCartItemActivity.class);
            addCartItemAct.putExtra("type", "Add");
            addCartItemAct.putExtra(Constants.CATEGORY_ID, categoryID);
            addCartItemAct.putExtra(Constants.CATEGORY_VALUE, categoryValue);
            addCartItemAct.putExtra("meatItemName", selectedWarehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize());
            addCartItemAct.putExtra("warehouseMeatItemDetails", new Gson().toJson(selectedWarehouseMeatItemDetails));
            addCartItemAct.putExtra(Constants.COLOR_PRIMARY, getIntent().getStringExtra(Constants.COLOR_PRIMARY));
            addCartItemAct.putExtra(Constants.COLOR_PRIMARY_DARK, getIntent().getStringExtra(Constants.COLOR_PRIMARY_DARK));
            startActivityForResult(addCartItemAct, Constants.ADD_CART_ITEM_ACTIVITY_CODE);
        }else{
            goToRegistration(new RegistrationCallback() {
                @Override
                public void onComplete(boolean status, String token, BuyerDetails buyerDetails) {
                    if (status) {
                        proceedToAddToCart(selectedWarehouseMeatItemDetails);
                    } else {
                        if (globalItemsAdapter.getItemsCount() == 1) {
                            finish();
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getMeatItems();
    }

    public void getMeatItems(){
        hideSearchView();
        showLoadingIndicator("Loading " + categoryName.toLowerCase() + " items...");
        getBackendAPIs().getMeatItemsAPI().getMeatItems(categoryID, new MeatItemsAPI.MeatItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<WarehouseMeatItemDetails> meatItemsList) {
                if (status == true) {
                    originalMeatItemsList = meatItemsList;
                    if (meatItemsList.size() > 0) {
                        showSearchView();
                        globalItemsAdapter.setWarehouseMeatItems(meatItemsList);

                        if (meatItemsList.size() == 1) {
                            proceedToAddToCart(meatItemsList.get(0));
                        } else {
                            switchToContentPage();
                            getMastaanApplication().getAlertMessages(categoryValue, new MessagesCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message, List<MessageDetails> messages) {
                                    if ( status ){
                                        messagesList = messages;
                                        globalItemsAdapter.addMessagesAtBeginning(messagesList);
                                    }
                                }
                            });
                        }
                    } else {
                        showNoDataIndicator("No items");
                    }
                } else {
                    showReloadIndicator(statusCode, "Error in loading meat items, try again!");
                }
            }
        });
    }

    public void registerForMeatItemAvailabilityAlerts(final int position){

        showLoadingDialog("Registering for availability alerts...");
        getBackendAPIs().getGeneralAPI().registerForMeatItemAvailabilityUpdates(globalItemsAdapter.getItem(position).getWarehouseMeatItemDetails().getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showSnackbarMessage("Successfully registered for availability alerts.");
                }else{
                    if ( statusCode == 409 ){
                        showSnackbarMessage("Already registered for availability alerts.");
                    }else {
                        showSnackbarMessage("Something went wrong, try again!", "RETRY", new ActionCallback() {
                            @Override
                            public void onAction() {
                                registerForMeatItemAvailabilityAlerts(position);
                            }
                        });
                    }
                }
            }
        });
    }

    //------------------------------------------

    public void checkCartStatus(final String message){

        fabEnterAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (message != null && message.length() > 0) {
                    revealCartFAB(message);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        int cartCount = localStorageData.getCartCount();

        if ( cartCount > 0 ){
            cart_count.setText(""+cartCount);
            if ( cartInfo.getVisibility() == View.INVISIBLE  || cartInfo.getVisibility() == View.GONE ){
                cartInfo.setVisibility(View.VISIBLE);
                cartInfo.startAnimation(fabEnterAnim);
            }else{
                if (message != null && message.length() > 0) {
                    revealCartFAB(message);
                }
            }
        }
        else {
            if ( cartInfo.getVisibility() == View.VISIBLE ){
                cartInfo.setVisibility(View.INVISIBLE);
                cartInfo.startAnimation(fabExitAnim);
            }
        }
    }


    public void revealCartFAB(String message){

        /*revelaFABMessage.setText(message);      //  Displaying Message

        fabRevealLayout.revealFAB(fabRevealLayout, getResources().getColor(R.color.action_button_color), 2000, new FABRevealLayout.RevealFABCallback() {
            @Override
            public void onCollapse() {
                revelaFABMessage.setText("");
                //ShowCaseSequenceMethods.showSingeView(cartInfo, "Press here to checkout your order after you are done shopping.", "GOT IT", "HOME_FAB_SHOWCASE2244");
            }

            @Override
            public void onExpand() {}
        });*/
        showSnackbarMessage(cartInfo, message, null, null);
    }

    //----------

    public void onPerformSearch(final String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchQuery = search_query.toLowerCase();

            showLoadingIndicator("Searching for ("+searchQuery+")");
            getSearchList(searchQuery, new SearchResultsCallback() {
                @Override
                public void onComplete(List<WarehouseMeatItemDetails> searchList) {
                    if (searchList.size() > 0) {
                        globalItemsAdapter.setWarehouseMeatItems(searchList);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No results found for (" + searchQuery + ")");
                    }
                }
            });
        }else{
            onClearSearch();
        }
    }

    private void getSearchList(final String searchQuery, final SearchResultsCallback callback){

        new AsyncTask<Void, Void, Void>() {
            List<WarehouseMeatItemDetails> filteredList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {

                if ( originalMeatItemsList != null && originalMeatItemsList.size() > 0 ){
                    for (int i=0;i< originalMeatItemsList.size();i++){
                        WarehouseMeatItemDetails warehouseMeatItemDetails = originalMeatItemsList.get(i);
                        if ( warehouseMeatItemDetails.getMeatItemDetais().getCombinedEnglishNamesString().toLowerCase().contains(searchQuery) || warehouseMeatItemDetails.getMeatItemDetais().getCombinedEnglishNamesString().toLowerCase().contains(searchQuery) ){
                            filteredList.add(warehouseMeatItemDetails);
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(filteredList);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void onClearSearch(){
        globalItemsAdapter.setWarehouseMeatItems(originalMeatItemsList);
        if ( globalItemsAdapter.getItemCount() > 0 ){
            switchToContentPage();
            globalItemsAdapter.addMessagesAtBeginning(messagesList);
        }else{
            showNoDataIndicator("No items");
        }
    }

    //-------

    public void showSearchView(){
        try{    searchViewMenuITem.setVisible(true);    }catch (Exception e){}
    }

    public void hideSearchView(){
        try{
            searchViewMenuITem.setVisible(false);
            searchViewMenuITem.collapseActionView();
        }catch (Exception e){}
    }

    //=============================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if ( requestCode == Constants.CART_ACTIVITY_CODE || resultCode == Constants.CART_ACTIVITY_CODE){
                checkCartStatus("");
            }
            else if ( requestCode == Constants.ADD_CART_ITEM_ACTIVITY_CODE ){
                if ( resultCode == Constants.ADD_CART_ITEM_ACTIVITY_CODE ) {
                    if ( globalItemsAdapter.getItemsCount() == 1 ){
                        setResult(Constants.ITEM_LISTINGS_ACTIVITY_CODE, data);
                        finish();
                    }else{
                        boolean addedStatus = data.getBooleanExtra("status", false);
                        if ( addedStatus ) {
                            String displayMessage = data.getStringExtra("displayMessage");
                            checkCartStatus(displayMessage);

                            String cartMessage = data.getStringExtra("cartMessage");
                            if ( cartMessage != null && cartMessage.length() > 0 ){
                                showNotificationDialog("Alert!", cartMessage);
                            }
                        }
                    }
                }else{
                    if ( globalItemsAdapter.getItemsCount() == 1 ) {
                        finish();
                    }
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){

        }else {
            onBackPressed();
        }
        return true;
    }

}
