package com.mastaan.buyer.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.FeedbacksAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.AttributeDetails;
import com.mastaan.buyer.models.FeedbackDetails;
import com.mastaan.buyer.models.MeatItemDetails;
import com.mastaan.buyer.models.OrderItemDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OrderItemDetailsActivity extends MastaanToolBarActivity{

    OrderItemDetails orderItemDetails;

    ImageView item_image;
    TextView item_status;
    TextView item_price;
    TextView item_quantity;
    TextView item_preferences;
    View instructionsView;
    TextView instructions;

    View feedbackView;
    RatingBar feedback_rating;
    TextView feedback_comments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item_details);
        hasLoadingView();
        getGoogleAnalyticsMethods().sendScreenName("Order Item Details");       // Send ScreenName to Analytics

        //----------

        item_image = findViewById(R.id.item_image);
        item_status = findViewById(R.id.item_status);
        item_price = findViewById(R.id.item_price);
        item_quantity = findViewById(R.id.item_quantity);
        item_preferences = findViewById(R.id.item_preferences);
        instructionsView = findViewById(R.id.instructionsView);
        instructions = findViewById(R.id.instructions);

        feedbackView = findViewById(R.id.feedbackView);
        feedback_rating = findViewById(R.id.feedback_rating);
        feedback_comments = findViewById(R.id.feedback_comments);

        //------

        displayItemDetails(new Gson().fromJson(getIntent().getStringExtra(Constants.ORDER_ITEM_DETAILS), OrderItemDetails.class));

    }

    public void displayItemDetails(OrderItemDetails orderItemDetails){
        this.orderItemDetails = orderItemDetails;
        MeatItemDetails meatItemDetails = orderItemDetails.getMeatItemDetails();

        switchToContentPage();

        setToolBarTitle(meatItemDetails.getNameWithCategoryAndSize());

        item_status.setText(orderItemDetails.getStatusString());

        Picasso.get()
                .load(meatItemDetails.getThumbnailImageURL())
                .placeholder(R.drawable.image_default_mastaan)
                .error(R.drawable.image_default_mastaan)
                .tag(context)
                .into(item_image);

        item_price.setText(RS + CommonMethods.getInDecimalFormat(orderItemDetails.getTotal()));
        item_quantity.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " " + meatItemDetails.getQuantityUnit()+(orderItemDetails.getQuantity()!=1?"s":""));

        List<String> selectedOptions = new ArrayList<>();
        for (int a = 0; a < orderItemDetails.getSelectedAttributes().size(); a++) {
            String attributeID = orderItemDetails.getSelectedAttributes().get(a).getAttributeID();
            String attributeOptionID = orderItemDetails.getSelectedAttributes().get(a).getOptinID();
            for (int i=0;i<meatItemDetails.getAttributes().size();i++){
                if ( attributeID.equals(meatItemDetails.getAttributes().get(i).getID())) {
                    AttributeDetails attributeDetails = meatItemDetails.getAttributes().get(i);
                    for (int j=0;j<attributeDetails.getAvailableOptions().size();j++){
                        if ( attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getValue()) || attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getID()) ){
                            selectedOptions.add(attributeDetails.getAvailableOptions().get(j).getName());
                            break;
                        }
                    }
                }
            }
        }
        item_preferences.setText(CommonMethods.getStringFromStringList(selectedOptions));

        if ( orderItemDetails.getSpecialInstructions() != null && orderItemDetails.getSpecialInstructions().trim().length() > 0 ){
            instructionsView.setVisibility(View.VISIBLE);
            instructions.setText(orderItemDetails.getSpecialInstructions());
        }else{  instructionsView.setVisibility(View.GONE);  }

        feedbackView.setVisibility(View.GONE);
        getBackendAPIs().getFeedbacksAPI().getOrderItemFeedback(orderItemDetails.getID(), new FeedbacksAPI.FeedbackCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, FeedbackDetails feedbackDetails) {
                if ( status && feedbackDetails.getRating() > 0 ){
                    feedbackView.setVisibility(View.VISIBLE);
                    feedback_rating.setRating(feedbackDetails.getRating());
                    if ( feedbackDetails.getComments() != null && feedbackDetails.getComments().trim().length() > 0 ){
                        feedback_comments.setVisibility(View.VISIBLE);
                        feedback_comments.setText(feedbackDetails.getComments().trim());
                    }else{  feedback_comments.setVisibility(View.GONE); }
                }else{
                    feedbackView.setVisibility(View.GONE);
                }
            }
        });

    }

    //========

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
