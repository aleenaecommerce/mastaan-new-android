package com.mastaan.buyer.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.methods.AppMethods;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.OrdersFeedbackDatabase;
import com.mastaan.buyer.models.BootStrapDetails;
import com.mastaan.buyer.models.BuyerDetails;

public class LaunchActivity extends MastaanToolBarActivity implements View.OnClickListener { //v2.4.39

    Location currentLocation;
    PlaceDetails currentLocationDetails;

    TextView version;
    LinearLayout loading_layout;
    View loading_indicator;
    View reload_indicator;
    TextView loading_message;

    OrdersFeedbackDatabase ordersFeedbackDatabase;

    BootStrapDetails bootStrapDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        //TestFairy.begin(this, getString(R.string.testfairy_app_token));    // Initializing Testfairy

        getGoogleAnalyticsMethods().sendScreenName("Launch Screen");       // Send ScreenNames to Analytics
//        getAnalyticsTracker().sendScreenName(this.getClass().getSimpleName());
//        getAnalyticsTracker().send(new HitBuilders.ScreenViewBuilder().build());
        Log.d(Constants.LOG_TAG, "AppMode = " + getResources().getString(R.string.app_mode));

        try{
            String []topics = new String[]{Constants.NOTIFICATION_TYPES.GENERAL, Constants.NOTIFICATION_TYPES.PROMOTION, Constants.NOTIFICATION_TYPES.ORDER, Constants.NOTIFICATION_TYPES.TRANSACTION};
            for (int i=0;i<topics.length;i++) {
                FirebaseMessaging.getInstance().subscribeToTopic(topics[i]);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        localStorageData.setAppCurrentVersion();
        localStorageData.setDeviceID(getDeviceID());
        Log.e("deviceid", "" + getDeviceID());

        localStorageData.setHomePageLastLoadedTime(DateMethods.getCurrentDateAndTime());
        localStorageData.setMeatItemPrefillDate("");
        localStorageData.setDeliveryLocation(null);

        ordersFeedbackDatabase = new OrdersFeedbackDatabase(context);
        ordersFeedbackDatabase.openDatabase();

        if (getString(R.string.app_type).equalsIgnoreCase("internal")) {
            localStorageData.clearBuyerSession();
            localStorageData.setAccessToken(getIntent().getStringExtra(Constants.ACCESS_TOKEN));
            localStorageData.setInternalAppInitiatedEmployeeID(getIntent().getStringExtra(Constants.EMPLOYEE_ID));
        } else {
            localStorageData.setInternalAppInitiatedEmployeeID("");
        }

        //-------------------------------------------

        version = findViewById(R.id.version);
        loading_layout = findViewById(R.id.loading_layout);
        loading_indicator = findViewById(R.id.loading_indicator);
        loading_message = findViewById(R.id.loading_message);
        reload_indicator = findViewById(R.id.reload_indicator);
        reload_indicator.setOnClickListener(this);

        //-------------------------------------------

        version.setText(AppMethods.getAppVersionName(context));

        checkInternetConnection(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status == true) {
                    loading_layout.setVisibility(View.VISIBLE);
                    // IF SESSION PRESENT THEN SKITPPING LOCATION ACCESS CHECKING
                    if (localStorageData.getSessionFlag()) {
                        //checkGCMRegistrationStatusAndProceedToBootstrap();
                        getBootStrapDetails();  // Go TO BOOTSTRAP
                    }
                    // IF SESSION NOT PRESENT THEN CHECKING LOCATION ACCESS
                    else {
                        checkLocationAccessAndProceedToBootstrap();
                    }
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view == reload_indicator) {
            getGoogleAnalyticsMethods().sendEvent("General", "Refresh button in splash screen tap", "");    // Send Event to Analytics
            getBootStrapDetails();
        }
    }

    public void showReload(int statusCode) {

        loading_layout.setVisibility(View.VISIBLE);
        loading_indicator.setVisibility(View.GONE);
        reload_indicator.setVisibility(View.VISIBLE);
        loading_message.setText("ERROR CONNECTING. PLEASE TRY AGAIN!");
        if (statusCode == -1) {
            @SuppressLint("MissingPermission") boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
            if (isInternetAvailable) {
                loading_message.setText("NOT ABLE TO CONNECT. \nPLEASE TRY AGAIN AFTER A WHILE!");
            } else {
                loading_message.setText("NO INTERNET CONNECTION!");
            }
        }
    }

    //-------------

    public void checkLocationAccessAndProceedToBootstrap() {

        getPermissionsHandler().checkLocationPermission(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status) {
                    checkLocationAccess(new CheckLocatoinAccessCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            // IF LOCATION ACCESS ENABLED THEN GETTING CURRENT LOCATION IN BACKGROUND
                            if (status) {
                                new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                                    @Override
                                    public void onComplete(Location location) {
                                        if (location != null) {
                                            currentLocation = location;
                                            getGooglePlacesAPI().getPlaceDetailsByLatLng(new LatLng(location.getLatitude(), location.getLongitude()), new GooglePlacesAPI.PlaceDetailsCallback() {
                                                @Override
                                                public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                                                    Log.d(ConstantsCommonLibrary.LOG_TAG, "PLACE DETAILS = " + status + " , DET = " + currentLocationDetails);
                                                    currentLocationDetails = placeDetails;
                                                    getBootStrapDetails();
                                                }
                                            });
                                        } else {
                                            getBootStrapDetails();
                                        }
                                    }
                                });
                            } else {
                                getBootStrapDetails();
                            }
                        }
                    });
                } else {
                    getBootStrapDetails();
                }
            }
        });
    }

    //--------

    public void getBootStrapDetails() {

        loading_indicator.setVisibility(View.VISIBLE);
        reload_indicator.setVisibility(View.GONE);
        loading_message.setText("TALKING TO SERVER");

        getBackendAPIs().getGeneralAPI().getBootStrap(new GeneralAPI.BootStrapCallback() {


            @Override
            public void onComplete(boolean status, int statusCode, String message, BootStrapDetails boot_strap_details) {

                if (status) {
                    bootStrapDetails = boot_strap_details;
                    checkUpdatesAndProceed();
                } else {
                    if (statusCode == 503 && message != null && message.length() > 0) {
                        loading_indicator.setVisibility(View.INVISIBLE);
                        reload_indicator.setVisibility(View.VISIBLE);
                        loading_message.setText(message);
                    } else {
                        showReload(statusCode);
                    }
                }
            }
        });
    }

    //----------------------------------------

    public void checkUpdatesAndProceed() {

        if (bootStrapDetails.isUpgradeAvailable()) {
            if (bootStrapDetails.isCompulsoryUpgrade()) {
                showChoiceSelectionDialog(false, "Update Available!", "This version is no longer compatible.\nPlease update to new version to continue.", "EXIT", "UPDATE", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if (choiceName.equalsIgnoreCase("UPDATE")) {
                            CommonMethods.openInPlaystore(context, context.getPackageName());
                            System.exit(0);
                        } else {
                            System.exit(0);
                        }
                    }
                });

            } else {
                if (localStorageData.getSkipAppUpdateVersion() != bootStrapDetails.getLatestVersion()) {
                    showChoiceSelectionDialog(false, "Update Available!", "New version of app is available.", "NOT NOW", "UPDATE", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                CommonMethods.openInPlaystore(context, context.getPackageName());
                                System.exit(0);
                            } else {
                                localStorageData.setSkipAppUpdateVersion(bootStrapDetails.getLatestVersion());
                                checkAlertMessagesAndProceed();
                            }
                        }
                    });
                } else {
                    checkAlertMessagesAndProceed();
                }
            }
        } else {
            checkAlertMessagesAndProceed();
        }
    }

    public void checkAlertMessagesAndProceed() {
        if (bootStrapDetails.getAlertMessage() != null && bootStrapDetails.getAlertMessage().length() > 0
                && localStorageData.getLastShownBootstrapAlertMessage().equalsIgnoreCase(bootStrapDetails.getAlertMessage()) == false) {

            localStorageData.setLastShownBootstrapAlertMessage(bootStrapDetails.getAlertMessage());
            showNotificationDialog("Alert!", Html.fromHtml(bootStrapDetails.getAlertMessage()), new ActionCallback() {
                @Override
                public void onAction() {
                    useBootstrapAndProceed();
                }
            });
        } else {
            useBootstrapAndProceed();
        }
    }

    public void useBootstrapAndProceed() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                localStorageData.setServerTime(CommonMethods.getReadableTimeFromUTC(bootStrapDetails.getServerTime()));

                localStorageData.setServingAt(bootStrapDetails.getServingAt());
                localStorageData.setSupportContactNumber(bootStrapDetails.getSupportContactNumber());

                localStorageData.setDeliveringHoursStartTime(bootStrapDetails.getDeliveringHoursStartTime());
                localStorageData.setDeliveringHoursEndTime(bootStrapDetails.getDeliveringHoursEndTime());

                localStorageData.storeInstallSources(bootStrapDetails.getInstallSources());

                localStorageData.storeMeatItemWeights(bootStrapDetails.getMeatItemWeights());
                localStorageData.storeMeatItemSets(bootStrapDetails.getMeatItemSets());
                localStorageData.storeMeatItemPieces(bootStrapDetails.getMeatItemPieces());

                //------

                localStorageData.setBuyerDetails(bootStrapDetails.getBuyerDetails());

                localStorageData.setNotificationsFlag(bootStrapDetails.getBuyerDetails().getPromotionalAlertsPreference());

                localStorageData.setCartCount(bootStrapDetails.getCartItemsCount());
                localStorageData.setDeliveryLocation(bootStrapDetails.getCartDeliveryAddress());
                localStorageData.setBuyerPendingOrders(bootStrapDetails.getPendingOrders());
                localStorageData.setHasLocationHistory(bootStrapDetails.getAddressBookList() != null && bootStrapDetails.getAddressBookList().size() > 0);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                // IF OPEN
                if (bootStrapDetails.isOpen()) {

                    // IF NO SESSION (or) SESSION EXPIRED (or) INVALID SESSION
                    if (bootStrapDetails.getBuyerDetails() == null || (bootStrapDetails.getBuyerDetails().getMobile() == null && bootStrapDetails.getBuyerDetails().getEmail() == null)) {
                        localStorageData.clearBuyerSession(false);

                        if (bootStrapDetails.getAccessToken() != null && bootStrapDetails.getAccessToken().length() > 0) {
                            localStorageData.setAccessToken(bootStrapDetails.getAccessToken());
                        }

                        checkDeliveryLocationAndProceed();     //  GOING TO HOME
                    }
                    // IF VALID SESSION
                    else {
                        localStorageData.setBuyerRatedOnPlaystore(bootStrapDetails.getBuyerDetails().isRatedOnPlaystore());

                        checkBuyerLinkedToFCM(bootStrapDetails.getBuyerDetails());    // Checking buyer linked to FCM

                        checkDeliveryLocationAndProceed();     //  GOING TO HOME
                    }
                }
                // IF CLOSED
                else {
                    loading_indicator.setVisibility(View.GONE);
                    loading_message.setText("  " + bootStrapDetails.getClosedMessage() + "  ");
                    loading_message.setBackgroundColor(Color.parseColor("#0094DC"));//R.drawable.app_theme_button);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void checkBuyerLinkedToFCM(BuyerDetails buyerDetails) {
        try {
            if (buyerDetails != null && FirebaseInstanceId.getInstance().getToken() != null) {
                if (buyerDetails.getFCMRegistrationID() == null || buyerDetails.getFCMRegistrationID().length() == 0
                        || buyerDetails.getFCMRegistrationID().equals(FirebaseInstanceId.getInstance().getToken()) == false) {
                    getBackendAPIs().getProfileAPI().linkToFCM(FirebaseInstanceId.getInstance().getToken(), null);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Crashlytics.logException(e);
        }
    }

    /*public void goToOrdersPendingFeedbacks(){
        try{
            PendingFeedbacksFragment pendingFeedbacksFragment = new PendingFeedbacksFragment().setCallback(new PendingFeedbacksFragment.Callback() {
                @Override
                public void onComplete() {
                    checkDeliveryLocationAndProceed();
                }
            });
            getSupportFragmentManager().beginTransaction().replace(R.id.feedback_fragment, pendingFeedbacksFragment).commitAllowingStateLoss();
            OrdersFeedbackDatabase ordersFeedbackDatabase = new OrdersFeedbackDatabase(context);
            ordersFeedbackDatabase.openDatabase();
            pendingFeedbacksFragment.displayFeedbacks(ordersFeedbackDatabase, bootStrapDetails.getPendingFeedbackOrders());
        }catch (Exception e){
            e.printStackTrace();
            // IF SOMETHING WRONG THEN GOING FORWARD
            checkDeliveryLocationAndProceed();      // TODO Handle this
        }
    }*/


    public void checkDeliveryLocationAndProceed() {
        //GOING TO HOME PAGE IF DELIVERY LOCATION LIKE WHEN ITEMS IN CART
        if (localStorageData.getDeliveryLocation() != null) {
            launchHomePage();
        }
        // GOING TO LOCATION PAGE
        else {
            // USER HAS SAVED LOCATIONS >> DIRECTLY GOING TO LOCATION SELECTOR PAGE
            if (bootStrapDetails.getAddressBookList() != null && bootStrapDetails.getAddressBookList().size() > 0) {
                Intent locationSelctionAct = new Intent(context, LocationSelectionActivity.class);
                startActivity(locationSelctionAct);
                finish();
            }
            // USER HAS NO SAVED LCOATIONS OR SESSION NOT PRESENT
            else {
                confirmDeliveryLocationAndProceed();
            }
        }
    }


    public void confirmDeliveryLocationAndProceed() {

        // IF LOCATION ACCESS ENABLED
        if (isLocationAccessEnabled()) {

            loading_indicator.setVisibility(View.VISIBLE);
            loading_message.setVisibility(View.VISIBLE);
            loading_message.setText("CHECKING YOUR LOCATION");

            // IF CURRENT LOCATION FOUND
            if (currentLocation != null) {
                // IF CURRENT LOCATION DETAILS FETCHED
                loading_indicator.setVisibility(View.INVISIBLE);
                loading_message.setVisibility(View.INVISIBLE);
                if (currentLocationDetails != null) {
                    showChoiceSelectionDialog(false, "Confirm your location", currentLocationDetails.getFullAddress(), "CHANGE", "DONE", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            // IF USER CONFIRMS HIS LOCATION
                            if (choiceName.equalsIgnoreCase("DONE")) {
                                localStorageData.setDeliveryLocation(new AddressBookItemDetails(currentLocationDetails));
                                checkAvailabilityAtLocationAndGoToHome();
                            }
                            // IF USER WANT TO ENTER MANUALLY
                            else {
                                Intent mapLocationSelectionAct = new Intent(context, MapLocationSelectionActivity.class);
                                mapLocationSelectionAct.putExtra("current_location_details", new Gson().toJson(currentLocationDetails));
                                startActivity(mapLocationSelectionAct);
                                finish();
                            }
                        }
                    });
                }
                // IF CURRENT LOCATION DETAILS NOT FETCHED
                else {
                    showChoiceSelectionDialog(false, "Failed!", "Something went wrong while getting your current place details.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            // IF TO WANT TRY AGAIN TO FETCH CURRENT LOCATION DETAILS..
                            if (choiceName.equalsIgnoreCase("YES")) {
                                getGooglePlacesAPI().getPlaceDetailsByLatLng(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), new GooglePlacesAPI.PlaceDetailsCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                                        currentLocationDetails = placeDetails;
                                        confirmDeliveryLocationAndProceed();
                                    }
                                });
                            }
                            // IF DON'T WANT TRY AGAIN TO FETCH CURRENT LOCATION DETAILS..
                            else {
                                Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
                                startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);
                                finish();
                            }
                        }
                    });
                }
            }
            // IF CURRENT LOCATION NOT FOUND
            else {
                /*showChoiceSelectionDialog(false, "Failed!", "Something went wrong while getting your current location.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        // IF TO WANT TRY AGAIN TO FETCH CURRENT LOCATION..
                        if (choiceName.equalsIgnoreCase("YES")) {

                            new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                                @Override
                                public void onComplete(Location location) {
                                    Log.d(ConstantsCommonLibrary.LOG_TAG, "Location = " + location);
                                    if (location != null) {
                                        currentLocation = location;
                                    }
                                    confirmDeliveryLocationAndProceed();
                                }
                            });
                        }
                        // IF DON'T WANT TRY AGAIN TO FETCH CURRENT LOCATION..
                        else {*/ //TODO?rp Keep or Remove?
                Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
                startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);
                finish();
                        /*}
                    }
                });*/
            }
        }
        // IF LOCATION ACCESS DISABLED
        else {
            Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
            startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);
            finish();
        }
    }

    public void checkAvailabilityAtLocationAndGoToHome() {

        loading_indicator.setVisibility(View.VISIBLE);
        loading_message.setText("TALKING TO SERVER");
        loading_message.setVisibility(View.VISIBLE);

        getBackendAPIs().getGeneralAPI().checkAvailabilityAtLocation(null, new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), new GeneralAPI.CheckAvailabilityAtLocationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation) {
                if (status) {
                    if (responseCheckAvailabilityAtLocation.isAvailable()) {
                        launchHomePage(null, responseCheckAvailabilityAtLocation);
                    } else {
                        closeLoadingDialog();
                        showChoiceSelectionDialog(false, "No Service!", Html.fromHtml("We are currently serving at <b>" + localStorageData.getServingAt() + "</b><br><br>Do you want to change location?"), "EXIT", "CHANGE LCOATION", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("EXIT")) {
                                    registerForAvailabilityAtLocationUpdatesDialog(new AddressBookItemDetails(currentLocationDetails), new ActionCallback() {
                                        @Override
                                        public void onAction() {
                                            System.exit(0);
                                        }
                                    });
                                } else {
                                    Intent mapLocationSelectionAct = new Intent(context, MapLocationSelectionActivity.class);
                                    mapLocationSelectionAct.putExtra("current_location_details", new Gson().toJson(currentLocationDetails));
                                    //startActivityForResult(mapLocationSelectionAct, Constants.PLACE_SEARCH_ACTIVITY_CODE);
                                    startActivity(mapLocationSelectionAct);
                                    finish();
                                }
                            }
                        });
                    }
                } else {
                    closeLoadingDialog();
                    showChoiceSelectionDialog(true, "Error!", "Something went wrong while checking availability at selected location.\n\nPlease try again!", "EXIT", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                checkAvailabilityAtLocationAndGoToHome();
                            } else {
                                onBackPressed();
                            }
                        }
                    });
                }
            }
        });
    }


}






/*

public void checkGCMRegistrationStatusAndProceedToBootstrap(){

        if ( localStorageData.getGCMRegistrationID() != null && localStorageData.getGCMRegistrationID().length() > 0 ){
            Log.d(Constants.LOG_TAG, "Already Registered to GCM");// , RegID = " + localStorageData.getGCMRegistrationID());
            if ( localStorageData.getPushNotificationsRegistratinStatus() ){
                Log.d(Constants.LOG_TAG, "Already Registered For Push Notifications");
            }else{
                Log.d(Constants.LOG_TAG, "Not Registered For Push Notifications");
                if (localStorageData.getSessionFlag() ) {       // REGISTER FOR BUYER PUSH NOTIFICATIONS
                    getBackendAPIs().getGeneralAPI().registerBuyerForPushNotifications(new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if (status) {
                                localStorageData.setPushNotificationsRegistratinStatus(true);
                            } else {
                                localStorageData.setPushNotificationsRegistratinStatus(false);
                            }
                        }
                    });
                }
                else {                                          // REGISTER FOR PUSH NOTIFICATIONS
                    getBackendAPIs().getGeneralAPI().registerForPushNotifications(new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if (status) {
                                localStorageData.setPushNotificationsRegistratinStatus(true);
                            } else {
                                localStorageData.setPushNotificationsRegistratinStatus(false);
                            }
                        }
                    });    // Registering TO Server For Push Notifications
                }
            }
        }
        else {      // IF NOT REGISTERED TO GCM
            Log.d(Constants.LOG_TAG, "Not Registered to GCM");
            new GCMRegistration(context).registerToGCM(new GCMRegistration.RegisterToGCMCallback() {
                @Override
                public void onComplete(boolean status, String message, String gcmRegistrationID) {
                    if (status) {
                        localStorageData.storeGCMRegistrationID(gcmRegistrationID);
                        getBackendAPIs().getGeneralAPI().registerForPushNotifications(new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    localStorageData.setPushNotificationsRegistratinStatus(true);
                                } else {
                                    localStorageData.setPushNotificationsRegistratinStatus(false);
                                }
                            }
                        });    // Registering TO Server For Push Notifications
                    }
                }
            });
        }

        getBootStrap
        Details();  // Go TO BOOTSTRAP
    }

 */