package com.mastaan.buyer.activities;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mastaan.buyer.R;
import com.mastaan.buyer.fragments.SideBarFragment;


public class ClosedActivity extends MastaanNavigationDrawerActivity {

    SideBarFragment sideBarFragment;

    ImageButton openNavigationDrawer;

    TextView slots_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);                                                             // Bcoz of Extending Custom Activity
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_closed);
        getGoogleAnalyticsMethods().sendScreenName("Closed Page");       // Send ScreenNames to Analytics

        sideBarFragment = new SideBarFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();


        //------------------------------

        openNavigationDrawer = (ImageButton) findViewById(R.id.openNavigationDrawer);
        openNavigationDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
