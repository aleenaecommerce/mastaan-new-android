package com.mastaan.buyer.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.here.sdk.core.GeoCoordinates;
import com.here.sdk.core.Point2D;
import com.here.sdk.gestures.TapListener;
import com.here.sdk.mapview.MapError;
import com.here.sdk.mapview.MapScene;
import com.here.sdk.mapview.MapScheme;
import com.here.sdk.mapview.MapView;
import com.mastaan.buyer.R;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.fragments.SideBarFragment;
import com.mastaan.buyer.interfaces.RegistrationCallback;
import com.mastaan.buyer.methods.ShowCaseSequenceMethods;
import com.mastaan.buyer.models.BuyerDetails;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapLocationSelectionActivity extends MastaanNavigationDrawerActivity implements View.OnClickListener {

    private static final int REQUEST_LOCATION = 1;
    //    private final GestureMapAnimator gestureMapAnimator;
    // Default animation should take 2 seconds.
    // Tip: Use a fixed duration, no matter how far to move.
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] RUNTIME_PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE
    };
    // Only one instance allowed.
    private static AnimatorSet animatorSet;
    //    private final MapCamera camera;
    private final List<Animator> valueAnimatorList = new ArrayList<>();
    public Double latitude;
    public Double longtitude;
    boolean isForAddFavourite;
    SideBarFragment sideBarFragment;
    String current_location_details;
    FrameLayout deliverFAB;
    ImageButton deliverHere;
    LinearLayout addFavourite;
    ImageView addFavouriteClickIcon;
    View fetching_name_indicator;
    //    GoogleMap map;
    ImageButton locateMe;
    LatLng selectedLocationLatLng;
    PlaceDetails selectedLocationDetails;
    FrameLayout searchLocation;
    TextView deliverLocation;
    private MapView mapView;
    private GpsTracker gpsTracker;
    private TimeInterpolator timeInterpolator;
    private LocationManager locationManager;


    /**
     * Only when the app's target SDK is 23 or higher, it requests each dangerous permissions it
     * needs when the app is running.
     */
    private static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
//        });

//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_location_selection);
        getGoogleAnalyticsMethods().sendScreenName("Map Location Selection");       // Send ScreenNames to Analytics

        isForAddFavourite = getIntent().getBooleanExtra("isForAddFavourite", false);

        // IF LAUNCHED FOR RESULT LIKE ADDING_FAVOURITE/DELIVERY_LCOATION_SELECTION
        if (getCallingActivity() != null) {
            disableNavigationDrawer();
        }
        // SHOWING SIDE IF LAUNCHED FROM LAUNCH ACTIVITY
        else {
            sideBarFragment = new SideBarFragment();
            sideBarFragment.setCallback(new SideBarFragment.SideBarCallback() {
                @Override
                public void onItemSelected(int position) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                }

                @Override
                public void onDeliverLocationChange(PlaceDetails deliverLocationDetails, ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation) {

                }
            });
            getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();
        }

        if (isForAddFavourite) {
            setToolBarTitle(getIntent().getStringExtra("activityTitle"));
            if (actionBar.getTitle() == null || actionBar.getTitle().length() == 0) {
                setToolBarTitle("Add/Edit Favourite");
            }
        }
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getLocation();
        }
        if (hasPermissions(this, RUNTIME_PERMISSIONS)) {
            setupMapFragmentView();
        } else {
            ActivityCompat
                    .requestPermissions(this, RUNTIME_PERMISSIONS, REQUEST_CODE_ASK_PERMISSIONS);
        }
        //...............................................

        final ShowCaseSequenceMethods ShowCaseSequenceMethods = new ShowCaseSequenceMethods(this, "LOACTION_ACTIVITY_SHOWCASE");

        fetching_name_indicator = (View) findViewById(R.id.fetching_name_indicator);

        deliverFAB = (FrameLayout) findViewById(R.id.deliverFAB);
        deliverHere = (ImageButton) findViewById(R.id.deliverHere);
        deliverHere.startAnimation(fabEnterAnim);
        deliverHere.setOnClickListener(this);
        ShowCaseSequenceMethods.addShowCaseView(deliverHere, "Press here to confirm your delivery location.");

        addFavouriteClickIcon = (ImageView) findViewById(R.id.addFavouriteClickIcon);
        addFavourite = (LinearLayout) findViewById(R.id.addFavourite);
        addFavourite.setOnClickListener(this);
        ShowCaseSequenceMethods.addShowCaseView(addFavourite, "Press here to view your address book.");
        if (isForAddFavourite) {
            addFavourite.setVisibility(View.GONE);
            deliverHere.setImageResource(R.drawable.ic_add_white);
        }

        locateMe = (ImageButton) findViewById(R.id.locateMe);
        locateMe.setOnClickListener(this);

        searchLocation = (FrameLayout) findViewById(R.id.searchLocation);
        searchLocation.setOnClickListener(this);
        deliverLocation = (TextView) findViewById(R.id.deliverLocation);

        mapView = findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);
        mapView.setOnReadyListener(new MapView.OnReadyListener() {
            @Override
            public void onMapViewReady() {
                // This will be called each time after this activity is resumed.
                // It will not be called before the first map scene was loaded.
                // Any code that requires map data may not work as expected beforehand.
                Log.d("Curent Maps", "HERE Rendering Engine attached.");


            }
        });


        mapView.getMapScene().loadScene(MapScheme.NORMAL_DAY, new MapScene.LoadSceneCallback() {
            @Override
            public void onLoadScene(@Nullable MapError mapError) {
                if (mapError == null) {
                    double distanceInMeters = 2000;

                    gpsTracker = new GpsTracker(MapLocationSelectionActivity.this);
                    latitude = gpsTracker.getLatitude();
                    longtitude = gpsTracker.getLongitude();
                    if (gpsTracker.canGetLocation()) {
//                        current_location = getIntent().getStringExtra("current_location_");
//
//                        deliverLocation.setText(current_location);
                        Log.e("location", "" + latitude + longtitude);
                    } else {
                        gpsTracker.showSettingsAlert();
                    }


                    mapView.getCamera().lookAt(new GeoCoordinates(latitude, longtitude), 2000);


                    mapView.getGestures().setTapListener(new TapListener() {
                        @Override
                        public void onTap(@NonNull Point2D touchPoint) {
                            StringBuilder result = new StringBuilder();

                            GeoCoordinates geoCoordinates = mapView.viewToGeoCoordinates(touchPoint);
                            Log.e("hjs", "Tap at: " + geoCoordinates.latitude);

                            try {
                                Geocoder geocoder = new Geocoder(MapLocationSelectionActivity.this, Locale.getDefault());
                                List<Address> addresses = geocoder.getFromLocation(geoCoordinates.latitude, geoCoordinates.longitude, 1);
                                if (addresses.size() > 0) {
                                    Address address = addresses.get(0);
                                    result.append(address.getLocality()).append("\n");
                                    result.append(address.getSubLocality()).append("\n");
                                    result.append(address.getAdminArea()).append("\n");
                                    result.append(address.getCountryName());
                                    result.append(address.getPostalCode());
                                    result.append(address.getFeatureName());
                                    String currentadd = address.getAddressLine(0);
//                currentadd=address.getAddressLine(0)+","+address.getSubLocality()+address.ge
//                tLocality()+address.getAdminArea()+address.getCountryName()+address.getPostalCode();
                                    Log.e("address", currentadd);
                                    deliverLocation.setText("" + currentadd);
                                    getPlaceName(new LatLng(geoCoordinates.latitude, geoCoordinates.longitude));
                                    Log.e("placess", "" + geoCoordinates.latitude);


                                }
                            } catch (IOException e) {
                                Log.e("tag", e.getMessage());
                            }
                        }
                    });


                } else {
                    Log.d("jdshjs", "Loading map failed: mapError: " + mapError.name());
                }
            }
        });
        //map=((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
//        final vMapFragment vMapFragment = ((vMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
//        vMapFragment.getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(GoogleMap googleMap) {
//                map = googleMap;
//                //map = vMapFragment.getMap();
//                //map.setMyLocationEnabled(true);   TODO?rp Keep or Remove?
//
//                vMapFragment.setOnDragListener(new vMapWrapperLayout.OnDragListener() {
//                    @Override
//                    public void onDrag(MotionEvent motionEvent) {
//                        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
//                            deliverLocation.setText("release map to set place..");
//                        } else {
//                            getPlaceName(map.getCameraPosition().target);       // on Completed Moving Map..
//                        }
//                    }
//                });
//
//                ShowCaseSequenceMethods.startShowCase();
//
//                //...............................................
//
        try {
            AddressBookItemDetails deliveryLocation = localStorageData.getDeliveryLocation();
            if (deliveryLocation != null) {
                selectedLocationLatLng = deliveryLocation.getLatLng();
                selectedLocationDetails = new PlaceDetails(deliveryLocation);

            } else {
                current_location_details = getIntent().getStringExtra("current_location_details");
                selectedLocationDetails = new Gson().fromJson(current_location_details, PlaceDetails.class);
                deliverLocation.setText(current_location_details);
                Log.e("current_location_", "" + new Gson().toJson(current_location_details));


            }

            // IF SELECTED/CURRENT LOCATION IS NOT NULL
            if (selectedLocationDetails != null) {
                String search_location_details = getIntent().getStringExtra("search_location_details");

                deliverLocation.setText(selectedLocationDetails.getFullAddress());

            }

            //IF SELECTED/CURRENT LOCATION IS NOT PRESENT
            else {
                fetchCurrentLocation(500, false);          // Fetch Location with cameraMoveAnimate Time, with NoAdjustAnimateTIme..
            }
        } catch (Exception e) {
            e.printStackTrace();
            fetchCurrentLocation(500, false);          // Fetch Location with cameraMoveAnimate Time, with NoAdjustAnimateTIme..
        }
    }

    //-------------

    @Override
    public void onClick(View view) {

        if (view == locateMe) {
            fetchCurrentLocation(2500, true);          // Fetch Location with cameraMoveAnimate Time..
        } else if (view == searchLocation) {
            Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
            searchActivity.putExtra("isForActivityResult", true);
            startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);

        } else if (view == addFavourite) {

            addFavouriteClickIcon.setVisibility(View.VISIBLE);
            new CountDownTimer(150, 50) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    addFavouriteClickIcon.setVisibility(View.GONE);

                    if (localStorageData.getSessionFlag() == true) {
                        Intent addressBookAct = new Intent(context, AddressBookActivity.class);
                        addressBookAct.putExtra("favourite_location_json", new Gson().toJson(selectedLocationDetails));
                        startActivityForResult(addressBookAct, Constants.ADDRESS_BOOK_ACTIVITY_CODE);
                    } else {
                        goToRegistration(new RegistrationCallback() {
                            @Override
                            public void onComplete(boolean status, String token, BuyerDetails buyerDetails) {
                                if (status) {
                                    Intent addressBookAct = new Intent(context, AddressBookActivity.class);
                                    addressBookAct.putExtra("favourite_location_json", new Gson().toJson(selectedLocationDetails));
                                    startActivityForResult(addressBookAct, Constants.ADDRESS_BOOK_ACTIVITY_CODE);
                                }
                            }
                        });
                    }
                }
            }.start();
        } else if (view == deliverHere) {

            if (selectedLocationDetails != null) {
                if (getCallingActivity() != null) {//isForAddFavourite ){
                    Intent placeData = new Intent();
                    placeData.putExtra("deliver_location_json", new Gson().toJson(selectedLocationDetails));
                    setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, placeData);
                    finish();
                } else {
                    if (getResources().getString(R.string.app_type).equalsIgnoreCase("internal")) {
                        showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to continue with selected location <b>" + selectedLocationDetails.getFullAddress() + "</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("YES")) {
                                    checkAvailabilityAtLocationAndProceedToHome();
                                }
                            }
                        });
                    } else {
                        checkAvailabilityAtLocationAndProceedToHome();
                    }
                }
            } else {
                showSnackbarMessage(deliverFAB, "Deliver location not available, Please enter it to continue.", "ENTER", new ActionCallback() {
                    @Override
                    public void onAction() {
                        Intent searchActivity = new Intent(context, PlaceSearchActivity.class);
                        searchActivity.putExtra("isForActivityResult", true);
                        startActivityForResult(searchActivity, Constants.PLACE_SEARCH_ACTIVITY_CODE);
                    }
                });
            }
        }
    }

    public void fetchCurrentLocation(final int animateTime, final boolean adjustAnimateTime) {
        getPermissionsHandler().checkLocationPermission(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status) {
                    checkLocationAccess(new CheckLocatoinAccessCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if (status) {
                                showLoadingDialog(true, "Fetching your current location, please wait..");
                                new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                                    @Override
                                    public void onComplete(Location location) {
                                        closeLoadingDialog();
                                        if (location != null) {
                                            selectedLocationLatLng = new LatLng(location.getLatitude(), location.getLongitude());
//

                                            //cameraAnimator.moveTo(new GeoCoordinates(location.getLatitude(),location.getLongitude()),8000);
//                                            moveCamera(selectedLocationLatLng, animateTime, adjustAnimateTime);         // Move & Animate Camera with adjustAnimateTime..
                                            getPlaceName(new LatLng(location.getLatitude(), location.getLongitude()));
                                            mapView.getCamera().lookAt(
                                                    new GeoCoordinates(location.getLatitude(), location.getLongitude()), 2000);
                                        } else {
                                            showChoiceSelectionDialog(true, "Failure", "Unable to Fetch your current location.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                                                @Override
                                                public void onSelect(int choiceNo, String choiceName) {
                                                    if (choiceName.equalsIgnoreCase("YES")) {
                                                        fetchCurrentLocation(animateTime, adjustAnimateTime);          // Fetch Location with cameraMoveAnimate Time with adjustAnimateTime..
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }


    //----------

    public void getPlaceName(LatLng location) {

        selectedLocationLatLng = location;
        selectedLocationDetails = null;

        deliverLocation.setText("Fetching place name, wait..");
        fetching_name_indicator.setVisibility(View.VISIBLE);
        deliverHere.setEnabled(false);
        getGooglePlacesAPI().getPlaceDetailsByLatLng(selectedLocationLatLng, new GooglePlacesAPI.PlaceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                fetching_name_indicator.setVisibility(View.INVISIBLE);
                deliverHere.setEnabled(true);
                if (status == true) {
                    selectedLocationDetails = placeDetails;
                    deliverLocation.setText(placeDetails.getFullAddress());

                } else {
                    deliverLocation.setText("Error in Fetching Location, try again!");
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            deliverLocation.setText("No Internet connection, Unable to load place details , tryagain!");
                        }
                    }
                }
            }
        });
    }

    //------ UI MEthods

//    public void moveCamera(LatLng targetLocationLatLng, int animateTime, boolean adjustAnimateTime) {
//
//        if (adjustAnimateTime == true) {
//            boolean contains = map.getProjection().getVisibleRegion().latLngBounds.contains(targetLocationLatLng);      // Check Given Point VIsible in Current View
//
//            if (contains == false) {       // If Not Visible in Current ZoomLevel / View...
//                Location currentLocation = new Location("current");
//                LatLng curLoc = map.getCameraPosition().target;
//                currentLocation.setLatitude(curLoc.latitude);
//                currentLocation.setLongitude(curLoc.longitude);
//
//                Location targetLocation = new Location("target");
//                targetLocation.setLatitude(targetLocationLatLng.latitude);
//                targetLocation.setLongitude(targetLocationLatLng.longitude);
//
//                float distance = currentLocation.distanceTo(targetLocation);
//                if (distance < 500) {
//                    animateTime = 400;
//                } else if (distance < 1000) {
//                    animateTime = 800;
//                } else if (distance < 5000) {
//                    animateTime = 1500;
//                } else {
//                    animateTime = 2500;
//                }
//            } else {                       // If Visible in CUrrent ZoomLevel / View....
//                animateTime = 200;
//            }
//        }
//
//        CameraPosition cameraPosition = new CameraPosition.Builder()
//                .target(targetLocationLatLng)      // Sets the center of the map to Mountain View
//                .zoom(17)                           // Sets the zoom
//                .bearing(0)                         // Sets the orientation of the camera to east
//                .tilt(0)                            // Sets the tilt of the camera to 30 degrees
//                .build();                           // Creates a CameraPosition from the builder
//
//        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), animateTime, new GoogleMap.CancelableCallback() {
//            @Override
//            public void onFinish() {
//            }
//
//            @Override
//            public void onCancel() {
//            }
//        });
//    }

    //==========================

    public void checkAvailabilityAtLocationAndProceedToHome() {

        showLoadingDialog("Launching Home...");
        getBackendAPIs().getGeneralAPI().checkAvailabilityAtLocation(selectedLocationDetails.getId(), selectedLocationDetails.getLatLng(), new GeneralAPI.CheckAvailabilityAtLocationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation) {
                if (status) {
                    if (responseCheckAvailabilityAtLocation.isAvailable()) {

                        if (getCallingActivity() != null) {
                            Intent placeData = new Intent();
                            placeData.putExtra("deliver_location_json", new Gson().toJson(selectedLocationDetails));
                            placeData.putExtra(Constants.RESPONSE_CHECK_AVAILABILITY_AT_LOCATION, new Gson().toJson(responseCheckAvailabilityAtLocation));
                            setResult(Constants.LOCATION_SELECTOR_ACTIVITY_CODE, placeData);
                            finish();

                        } else {
                            launchHomePage(new AddressBookItemDetails(selectedLocationDetails), responseCheckAvailabilityAtLocation);           // LAUNCHING HOME PAGE
                        }
                    } else {
                        closeLoadingDialog();
                        showNoServiceDialog(new AddressBookItemDetails(selectedLocationDetails), null);
                        //showNotificationDialog("No Service!", Html.fromHtml("We are not serving at the selected location. We are currently serving at <b>" + localStorageData.getServingAt() + "</b>"));
                    }
                } else {
                    closeLoadingDialog();
                    showChoiceSelectionDialog(true, "Error!", "Something went wrong while checking availability at selected location.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                checkAvailabilityAtLocationAndProceedToHome();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (getCallingActivity() != null) {
            onBackPressed();
        } else {
            if (drawerListener.onOptionsItemSelected(item)) {
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                MapLocationSelectionActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                MapLocationSelectionActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
//                latitude = String.valueOf(lat);
//                longtitude = String.valueOf(longi);
                Log.e("fd: ", "Lat: " + lat + "Long: " + longi);
                StringBuilder result = new StringBuilder();
                try {
                    Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                    List<Address> addresses = geocoder.getFromLocation(lat, longi, 1);
                    if (addresses.size() > 0) {
                        Address address = addresses.get(0);
                        result.append(address.getLocality()).append("\n");
                        result.append(address.getSubLocality()).append("\n");
                        result.append(address.getAdminArea()).append("\n");
                        result.append(address.getCountryName());
                        result.append(address.getPostalCode());
                        result.append(address.getFeatureName());
//                        currentadd = address.getAddressLine(0);
//                        Log.e("address", currentadd);
                        deliverLocation.setText(selectedLocationDetails.getFullName());
                    }
                } catch (IOException e) {
                    Log.e("tag", e.getMessage());
                } catch (NullPointerException e) {
                    Log.e("tag", e.getMessage());
                }
            } else {
                // Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setupMapFragmentView() {
        // All permission requests are being handled. Create map fragment view. Please note
        // the HERE Mobile SDK requires all permissions defined above to operate properly.
        invalidateOptionsMenu();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == Constants.LOCATION_ENABLER_ACTIVITY_CODE || resultCode == Constants.LOCATION_ENABLER_ACTIVITY_CODE) {     // From Location Settings..
                boolean status = false;
                LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
                if (locationManager != null) {
                    status = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (status == false) {
                        status = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                    }
                }
                if (status == true) {
                    fetchCurrentLocation(500, true);
                }
            } else if (resultCode == Constants.PLACE_SEARCH_ACTIVITY_CODE && resultCode == Constants.PLACE_SEARCH_ACTIVITY_CODE) {      // If It is from PlaceSearch Activity
                String deliver_location_json = data.getStringExtra("deliver_location_json");
                PlaceDetails deliverLocationDetails = new Gson().fromJson(deliver_location_json, PlaceDetails.class);
                deliverLocation.setText(deliver_location_json);
                Log.e("deliver_locajsonmap", "" + deliverLocationDetails.getFullName());

                if (deliverLocationDetails != null) {
                    this.selectedLocationDetails = deliverLocationDetails;
//                    selectedLocationLatLng = selectedLocationDetails.latLng;
                    deliverLocation.setText(deliverLocationDetails.getFullName());
                    Log.e("address llll", "" + deliverLocationDetails.getLatLng());

                    mapView.getMapScene().loadScene(MapScheme.NORMAL_DAY, new MapScene.LoadSceneCallback() {
                        @Override
                        public void onLoadScene(@Nullable MapError mapError) {
                            if (mapError == null) {


                                mapView.getCamera().lookAt(
                                        new GeoCoordinates(deliverLocationDetails.getLatitude(), deliverLocationDetails.getLongtitude()), 2000);
                                Log.e("map load", "" + deliverLocationDetails.getLatitude() + "," + deliverLocationDetails.getLongtitude());
//
                            } else {
                                Log.d("jdshjs", "Loading map failed: mapError: " + mapError.name());
                            }
                        }
                    });

                    // loadMapScene(deliverLocationDetails.getLatitude(), deliverLocationDetails.getLongtitude());
                    //loadMapScene(17.306159, 78.473887);


                    //moveCamera(deliverLocationDetails.getLatLng(), 1200, false);         // Move & Animate Camera..
                }
            } else if (resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE && resultCode == Constants.ADDRESS_BOOK_ACTIVITY_CODE) {      // If It is from PlaceSearch Activity
                String address_item_json = data.getStringExtra("address_item_json");

                PlaceDetails deliverLocationDetails = new PlaceDetails(new Gson().fromJson(address_item_json, AddressBookItemDetails.class));
                if (deliverLocationDetails != null) {
                    this.selectedLocationDetails = deliverLocationDetails;
                    selectedLocationLatLng = deliverLocationDetails.getLatLng();
                    deliverLocation.setText(deliverLocationDetails.getFullAddress());
                    // moveCamera(deliverLocationDetails.getLatLng(), 1200, false);

//                PlaceHereMapsSearchDataByLatLng deliverLocationDetails = new PlaceHereMapsSearchDataByLatLng(new Gson().fromJson(address_item_json, AddressBookItemDetails.class));
//                if (deliverLocationDetails != null) {
//                    this.selectedLocationDetails = deliverLocationDetails;
//                    selectedLocationLatLng = selectedLocationDetails.getLatLng();
//                    deliverLocation.setText(deliverLocationDetails.getFullAddress());
//                    // moveCamera(deliverLocationDetails.getLatLng(), 1200, false);         // Move & Animate Camera..
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
