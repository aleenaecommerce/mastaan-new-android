package com.mastaan.buyer.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.MenuItem;

import com.aleena.common.adapters.ShareAppsAdapter;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.buyer.R;
import com.mastaan.buyer.constants.Constants;

import java.util.ArrayList;
import java.util.List;

public class ShareAppsActivity extends MastaanToolBarActivity {

    String shareSubject, shareMessage;

    vRecyclerView shareAppsHolder;
    ShareAppsAdapter shareAppsAdapter;
    ShareAppsAdapter.CallBack shareAppsCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_apps);
        hasLoadingView();

        if ( getIntent().getStringExtra(Constants.ANALYTICS_TITLE) != null ){
            getGoogleAnalyticsMethods().sendScreenName(getIntent().getStringExtra(Constants.ANALYTICS_TITLE));       // Send ScreenNames to Analytics
        }
        shareSubject = getIntent().getStringExtra(Constants.SUBJECT);
        shareMessage = getIntent().getStringExtra(Constants.MESSAGE);

        //----------------

        shareAppsHolder = (vRecyclerView) findViewById(R.id.shareAppsHolder);
        shareAppsHolder.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        shareAppsHolder.setLayoutManager(layoutManager);
        shareAppsHolder.setItemAnimator(new DefaultItemAnimator());

        shareAppsCallback = new ShareAppsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                ResolveInfo appInfo = shareAppsAdapter.getAppInfo(position);

                getGoogleAnalyticsMethods().sendEvent("Growth", "Referral channel", appInfo.loadLabel(context.getPackageManager()).toString());

                ActivityInfo activity = appInfo.activityInfo;
                ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                //shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSubject);
                shareIntent.putExtra(Intent.EXTRA_TEXT, (shareMessage));
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                shareIntent.setComponent(name);
                startActivity(shareIntent);             //  Opening Sharing App
            }
        };

        shareAppsAdapter = new ShareAppsAdapter(context, new ArrayList<ResolveInfo>(), shareAppsCallback);
        shareAppsHolder.setAdapter(shareAppsAdapter);

        getShareAppsList();         //      Get Share Apps

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getShareAppsList();
    }

    public void getShareAppsList(){

        showLoadingIndicator("Loading...");
        CommonMethods.getShareAppsList(context, new CommonMethods.ShareAppsListCallback(){
            @Override
            public void onComplete(List<ResolveInfo> shareAppsList) {
                if ( shareAppsList != null ) {
                    if (shareAppsList.size() > 0) {
                        shareAppsAdapter.addItems(shareAppsList);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No sharing apps found ");
                    }
                }else{
                    showReloadIndicator("Something went wrong, try again!");
                }

            }
        });
    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}


/*  NOT REQURIED
    public void shareVia(String packageName, String subject, String message, String url){

        // packageName = {"facebook", "com.whatsapp", "twitter", "android.gm"}

        boolean isPackageAvailable = false;

        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, (message+"\n"+"visit : "+url));
        //shareIntent.putExtra(Intent.EXTRA_STREAM, url);

        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);

        for (final ResolveInfo app : activityList){
            //Log.d("shareIntent", ""+(app.activityInfo.name));
            if ( (app.activityInfo.name).contains(packageName) ) {
                final ActivityInfo activity = app.activityInfo;
                final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                shareIntent.setComponent(name);
                startActivity(shareIntent);

                isPackageAvailable = true;
                break;
            }
        }

        if ( isPackageAvailable == false ){
            Toast.makeText(context, "Sorry , Seems like ["+packageName+"] is Not Installed in your Mobile", Toast.LENGTH_LONG).show();
        }
    }

    public void openAllShares(String subject, String message, String url){

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sendIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendIntent.putExtra(Intent.EXTRA_STREAM, url);
        startActivity(sendIntent);
    }
    */