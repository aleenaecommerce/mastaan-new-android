package com.mastaan.buyer.constants;

/**
 * Created by venkatesh on 8/9/15.
 */

public final class Constants {

    public static final String LOG_TAG =  "MastaanLogs";

    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";

    public static final String COLOR_PRIMARY = "COLOR_PRIMARY";
    public static final String COLOR_PRIMARY_DARK = "COLOR_PRIMARY_DARK";

    public static final int REGISTER_ACTIVITY_CODE = 190;

    public static final int APP_SETTINGS_ACTIVITY_CODE = 5;
    public static final int LOCATION_ENABLER_ACTIVITY_CODE = 6;
    public static final int LOCATION_SELECTOR_ACTIVITY_CODE = 7;
    public static final int PLACE_SEARCH_ACTIVITY_CODE = 8;


    public static final int ADDRESS_BOOK_ACTIVITY_CODE = 9;
    public static final int CART_ACTIVITY_CODE = 10;
    public static final int ITEM_LISTINGS_ACTIVITY_CODE = 107;
    public static final int FAVOURITE_ACTIVITY_CODE = 11;
    public static final int ADD_CART_ITEM_ACTIVITY_CODE = 12;
    public static final int UPDATE_CART_ITEM_ACTIVITY_CODE = 13;
    public static final int PENDING_FEEDBACK_ORDERS_ACTIVITY_CODE = 15;
    public static final int ORDER_FEEDBACK_ACTIVITY_CODE = 16;
    public static final int COUPONS_ACTIVITY_CODE = 26;

    public static final int TERMS_AND_CONDITIONS_ACTIVITY_CODE = 17;
    public static final int PROFILE_ACTIVITY_CODE = 18;

    public static final int PAYMENT_TYPE_ACTIVITY_CODE = 20;
    public static final int CREDIT_DEBIT_CARD_ACTIVITY_CODE = 21;
    public static final int INTERNET_BANKING_ACTIVITY_CODE = 22;
    public static final int PAYUBIZ_ACTIVITY_CODE = 23;

    public static final int ICONGRID_ATTRIBUTE_OPTIONS_ACTIVITY_CODE = 25;

    public static final int FEEDBACK_ACTIVITY_CODE = 30;

    public static final String OTP_RECEIVER = "OTP_RECEIVER";
    public static final String SIDEBAR_RECEIVER = "SIDEBAR_RECEIVER";
    public static final String LOCATION_SELECTION_RECEIVER = "LOCATION_SELECTION_RECEIVER";
    public static final String HOME_RECEIVER = "HOME_RECEIVER";
    public static final String MEAT_ITEMS_RECEIVER = "MEAT_ITEMS_RECEIVER";
    public static final String ORDER_DETAILS_RECEIVER = "ORDER_DETAILS_RECEIVER";

    public static final String FIREBASE_SERVICE_RECEIVER = "FIREBASE_SERVICE_RECEIVER";

    public static final String ACTION_TYPE = "type";
    public static final String DISPLAY_MEMBERSHIP_DETAILS = "DISPLAY_MEMBERSHIP_DETAILS";
    public static final String DISPLAY_DELIVERY_LOCATION = "DISPLAY_DELIVERY_LOCATION";
    public static final String DISPLAY_ADDRESS_BOOK = "DISPLAY_ADDRESS_BOOK";
    public static final String RELOAD_DATA = "RELOAD_DATA";
    public static final String DISPLAY_ORDER_STATUS = "DISPLAY_ORDER_STATUS";
    public static final String DISPLAY_USER_SESSION_INFO = "DISPLAY_USER_SESSION_INFO";
    public static final String ADD_NEW_PENDING_ORDER = "ADD_NEW_PENDING_ORDER";
    public static final String CHECK_CART_STATUS = "CHECK_CART_STATUS";
    public static final String DISPLAY_PENDING_FEEDBACKS = "DISPLAY_PENDING_FEEDBACKS";

    public static final String OTP_SERVICE = "OTP_SERVICE";
    public static final String OTP_CODE = "OTP_CODE";

    //-------

    public static final String ID = "ID";
    public static final String TYPE = "TYPE";
    public static final String STATUS = "STATUS";
    public static final String NAME = "NAME";
    public static final String DETAILS = "DETAILS";

    public static final String BRONZE_MEMBER = "Bronze Member";
    public static final String SILVER_MEMBER = "Silver Member";
    public static final String GOLD_MEMBER = "Gold Member";
    public static final String DIAMOND_MEMBER = "Diamond Member";
    public static final String PLATINUM_MEMBER = "Platinum Member";

    public static final String PROFILE = "profile";
    public static final String REFERRAL = "referral";
    public static final String LOYALTY = "loyalty";
    public static final String SETTINGS = "settings";
    public static final String LEGAL = "legal";
    public static final String PLAYSTORE = "playstore";

    public static final String MEAT_ITEMS = "meatitems";

    public static final String CHICKEN = "chicken";
    public static final String FARM_CHICKEN = "farmChicken";
    public static final String COUNTRY_CHICKEN = "countrychicken";

    public static final String MUTTON = "mutton";
    public static final String SHEEP_MUTTON = "sheepmutton";
    public static final String GOAT_MUTTON = "goatmutton";

    public static final String SEAFOOD = "seafood";
    public static final String FRESH_WATER_SF = "freshwatersf";
    public static final String SEA_WATER_SF = "seawatersf";

    //--------

    public static final String UNLIMITED = "ul";
    public static final String LIMITED = "l";
    public static final String DAILY = "d";

    public static final String PENDING = "Pending";
    public static final String ORDERED = "Ordered";
    public static final String ACKNOWLEDGED = "Acknowledged";
    public static final String PROCESSING = "Processing";
    public static final String PROCESSED = "Processed";
    public static final String ASSIGNED = "Assigned";
    public static final String PICKED_UP = "Picked up";
    //public static final String PICKUP_PENDING = "Pickup Pending";
    public static final String DELIVERING = "Delivering";
    public static final String DELIVERED = "Delivered";
    public static final String REJECTED = "Rejected";
    public static final String FAILED = "Failed";

    public static final int CASH_ON_DELIVERY_CODE = 1;
    public static final String CASH_ON_DELIVERY = "Cash on delivery";
    public static final int ONLINE_PAYMENT_CODE = 7;
    public static final String ONLINE_PAYMENT = "Online payment";
    public static final String DEBIT_CARD = "Debit card";
    public static final String CREDIT_CARD = "Credit card";
    public static final String NET_BANKING = "Net banking";
    public static final int PAYTM_WALLET_CODE = 8;
    public static final String PAYTM_WALLET = "PAYTM_WALLET";

    //--------

    public static final int DATE_VIEW_TYPE = -210;
    public static final int MEAT_ITEM_VIEW_TYPE = -212;
    public static final int CART_ITEM_VIEW_TYPE = -213;
    public static final int MESSAGE_VIEW_TYPE = -220;

    public static final String GROUPED_ORDERS = "GROUPED_ORDERS";


    public static final String RESPONSE_CHECK_AVAILABILITY_AT_LOCATION = "RESPONSE_CHECK_AVAILABILITY_AT_LOCATION";

    public static final String EMPLOYEE_ID = "EMPLOYEE_ID";
    public static final String CATEGORY_ID = "categoryID";
    public static final String CATEGORY_VALUE = "categoryValue";
    public static final String CATEGORY_NAME = "categoryName";
    public static final String ORDER_DETAILS= "ORDER_DETAILS";
    public static final String ORDER_ITEM_DETAILS= "ORDER_ITEM_DETAILS";
    public static final String COUPON_DETAILS= "COUPON_DETAILS";
    public static final String COUPON_CODE= "COUPON_CODE";
    public static final String NOTIFICATION_DETAILS= "NOTIFICATION_DETAILS";


    public static final String REQUEST_PLACE_ORDER = "request_object_place_order";

    public static final String USE_WALLET_BALANCE = "USE_WALLET_BALANCE";


    public static final String TITLE = "TITLE";
    public static final String ANALYTICS_TITLE = "ANALYTICS_TITLE";
    public static final String URL = "URL";
    public static final String CONTENT = "CONTENT";
    public static final String SUBJECT = "SUBJECT";
    public static final String MESSAGE = "MESSAGE";

    public static final String ORDER = "Order";
    public static final String CASHBACK = "Cashback";
    public static final String REFUND = "Refund";

    public static final String ANALYTICS_EVENT_CATEGORY = "ANALYTICS_EVENT_CATEGORY";
    public static final String ANALYTICS_EVENT_ACTION = "ANALYTICS_EVENT_ACTION";
    public static final String ANALYTICS_EVENT_LABEL = "ANALYTICS_EVENT_LABEL";

    public static final class NOTIFICATION_TYPES{
        public static final String ORDER = "order";
        public static final String TRANSACTION = "transaction";
        public static final String GENERAL = "general";
        public static final String PROMOTION = "promotion";
    }

}
