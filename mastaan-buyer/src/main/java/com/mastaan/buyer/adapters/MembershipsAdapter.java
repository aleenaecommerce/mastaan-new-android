package com.mastaan.buyer.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.MembershipDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 17/01/19.
 */

public class MembershipsAdapter extends RecyclerView.Adapter<MembershipsAdapter.ViewHolder> {
    Context context;
    List<MembershipDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemSelector;
        View container;
        TextView name;
        TextView details;
        View actionExpandCollapse;
        ImageView iconExpandCollapse;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            container = itemLayoutView.findViewById(R.id.container);
            name = itemLayoutView.findViewById(R.id.name);
            details = itemLayoutView.findViewById(R.id.details);
            actionExpandCollapse = itemLayoutView.findViewById(R.id.actionExpandCollapse);
            iconExpandCollapse = itemLayoutView.findViewById(R.id.iconExpandCollapse);
        }
    }

    public MembershipsAdapter(Context context, List<MembershipDetails> membershipsList, CallBack callBack) {
        this.context = context;
        this.itemsList = membershipsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MembershipsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_membership, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final MembershipDetails membershipDetails = itemsList.get(position);

        try{
            viewHolder.actionExpandCollapse.setRotation(0);
            viewHolder.details.setVisibility(View.GONE);

            if ( membershipDetails != null ){
                try{
                    viewHolder.container.setBackgroundColor(Color.parseColor(membershipDetails.getBackgroundColor()));
                }catch (Exception e){   viewHolder.container.setBackgroundColor(Color.WHITE);   }

                try{
                    int textColor = Color.parseColor(membershipDetails.getTextColor());
                    viewHolder.name.setTextColor(textColor);
                    viewHolder.details.setTextColor(textColor);
                    viewHolder.iconExpandCollapse.setColorFilter(textColor);
                }catch (Exception e){
                    viewHolder.name.setTextColor(Color.BLACK);
                    viewHolder.details.setTextColor(Color.BLACK);
                    viewHolder.iconExpandCollapse.setColorFilter(Color.BLACK);
                }

                try{
                    int backgroundColor = Color.parseColor(membershipDetails.getBackgroundColor());
                    int textColor = Color.parseColor(membershipDetails.getTextColor());
                    viewHolder.container.setBackgroundColor(backgroundColor);
                    viewHolder.name.setTextColor(textColor);
                    viewHolder.details.setTextColor(textColor);
                    viewHolder.iconExpandCollapse.setColorFilter(textColor);
                }catch (Exception e){}

                viewHolder.name.setText(membershipDetails.getTypeName());

                viewHolder.details.setText(CommonMethods.fromHtml(membershipDetails.getFormattedDetails()));

                viewHolder.actionExpandCollapse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if ( viewHolder.details.getVisibility() == View.VISIBLE ){
                            viewHolder.iconExpandCollapse.setRotation(0);
                            viewHolder.details.setVisibility(View.GONE);
                            if ( uiCallback != null ){
                                uiCallback.onCollapse(position, viewHolder.itemView);
                            }
                        }else{
                            viewHolder.iconExpandCollapse.setRotation(180);
                            viewHolder.details.setVisibility(View.VISIBLE);
                            if ( uiCallback != null ){
                                uiCallback.onExpand(position, viewHolder.itemView);
                            }
                        }
                    }
                });

                if ( callBack != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callBack.onItemClick(position);
                        }
                    });
                }
            }
        }catch (Exception e){}

    }

    public UICallback uiCallback;
    public MembershipsAdapter setUiCallback(UICallback uiCallback) {
        this.uiCallback = uiCallback;
        return this;
    }

    public interface UICallback{
        void onExpand(int position, View view);
        void onCollapse(int position, View view);
    }


    //---------------

    public void setItems(List<MembershipDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<MembershipDetails> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }
    public void addItem(MembershipDetails membershipDetails){
        itemsList.add(membershipDetails);
        notifyDataSetChanged();
    }
    public void addItem(int position, MembershipDetails membershipDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.add(position, membershipDetails);
        }else{
            itemsList.add(membershipDetails);
        }
        notifyDataSetChanged();
    }

    public void setItem(int position, MembershipDetails membershipDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, membershipDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public MembershipDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<MembershipDetails> getAllItems(){
        return itemsList;
    }

}