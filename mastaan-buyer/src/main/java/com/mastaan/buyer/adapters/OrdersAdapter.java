package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    Context context;
    int layoutView;
    List<OrderDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView order_id;
        TextView delivery_date;
        vRecyclerView orderItemsHolder;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            order_id = (TextView) itemLayoutView.findViewById(R.id.order_id);
            delivery_date = (TextView) itemLayoutView.findViewById(R.id.delivery_date);
            orderItemsHolder = (vRecyclerView) itemLayoutView.findViewById(R.id.orderItemsHolder);
        }
    }

    public OrdersAdapter(Context context, List<OrderDetails> itemsList, CallBack callBack) {
        this.context = context;
        this.layoutView = R.layout.view_order;
        this.itemsList = itemsList;
        this.callBack = callBack;
    }

    public OrdersAdapter(Context context, int view, List<OrderDetails> itemsList, CallBack callBack) {
        this.context = context;
        this.layoutView = view;
        this.layoutView = view;
        this.itemsList = itemsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(layoutView, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final OrderDetails orderDetails = itemsList.get(position);    // Getting Food Items Details using Position...

        if ( orderDetails != null ) {
            viewHolder.order_id.setText("Order ID : " + orderDetails.getOrderID());
            viewHolder.delivery_date.setText(orderDetails.getFormattedDeliveryDate());//DateMethods.getDateInFormat(orderDetails.getDeliveryDateInISTFormat(), DateConstants.MMM_DD_YYYY_HH_MM_A));

            viewHolder.orderItemsHolder.setHasFixedSize(true);
            viewHolder.orderItemsHolder.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            viewHolder.orderItemsHolder.setItemAnimator(new DefaultItemAnimator());
            OrderItemsAdapter orderItemsAdapter = new OrderItemsAdapter(context, new ArrayList<OrderItemDetails>(), null);
            viewHolder.orderItemsHolder.setAdapter(orderItemsAdapter);

            orderItemsAdapter.addItems(orderDetails.getOrderItems());
        }
    }

    //-----------------

    public void addItems(List<OrderDetails> items){

        if ( items != null ) {
            for (int i = items.size() - 1; i >= 0; i--) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<OrderDetails> getAllOrders(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public OrderDetails getOrder(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}