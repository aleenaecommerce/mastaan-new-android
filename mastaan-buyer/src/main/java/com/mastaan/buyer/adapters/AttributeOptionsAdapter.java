package com.mastaan.buyer.adapters;

import android.content.Context;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.AttributeOptionDetails;

import java.util.ArrayList;
import java.util.List;

public class AttributeOptionsAdapter extends ArrayAdapter<AttributeOptionDetails>{

    Context context;
    List<AttributeOptionDetails> itemsList;
    List<AttributeOptionDetails> originalItemsList = new ArrayList<>();

    String priceIndicatorText="";
    String weightQuantityUnit="";

    Callback callback;

    public void setPriceIndicatorText(String priceIndicatorText) {
        this.priceIndicatorText = priceIndicatorText;
    }

    public interface Callback{
        void onShowDisabledReason(int position);
    }

    class ViewHolder {          // Food Item View Holder
        TextView name;
        TextView info;
        View disabledIndicator;
    }

    public AttributeOptionsAdapter(Context context, List<AttributeOptionDetails> itemsList, String weightQuantityUnit, Callback callback) {
        super(context, R.layout.view_list_item, itemsList);
        this.context = context;
        this.itemsList = itemsList;
        for (int i=0;i<itemsList.size();i++){
            originalItemsList.add(itemsList.get(i));
        }
        this.weightQuantityUnit = weightQuantityUnit;
        this.callback = callback;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if ( convertView == null ) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_attribute_option, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.disabledIndicator = convertView.findViewById(R.id.disabledIndicator);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        setupView(position, viewHolder);        // Display Details

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if ( convertView == null ) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_attribute_option_popup, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.info = (TextView) convertView.findViewById(R.id.info);
            viewHolder.disabledIndicator = convertView.findViewById(R.id.disabledIndicator);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        setupView(position, viewHolder);        // Display Details

        return convertView;
    }

    public void setupView(final int position, ViewHolder viewHolder){

        AttributeOptionDetails optionDetails = getItem(position);

        if ( optionDetails.getPriceDifference() > 0 ) {
            viewHolder.name.setText(optionDetails.getName() + " ("+priceIndicatorText + SpecialCharacters.RS + CommonMethods.getInDecimalFormat(optionDetails.getPriceDifference()) + "/"+weightQuantityUnit+")");
            if ( optionDetails.getActualPriceDifference() > optionDetails.getPriceDifference() ){
                try{
                    viewHolder.name.setText(optionDetails.getName() + " ("+priceIndicatorText+" ", TextView.BufferType.SPANNABLE);
                    int strikeStart = viewHolder.name.getText().toString().length();
                    viewHolder.name.append(SpecialCharacters.RS + CommonMethods.getInDecimalFormat(optionDetails.getActualPriceDifference())+" ");
                    ((Spannable) viewHolder.name.getText()).setSpan(new StrikethroughSpan(), strikeStart, viewHolder.name.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    viewHolder.name.append(" "+SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(optionDetails.getPriceDifference())+ "/"+weightQuantityUnit+")");
                }catch (Exception e){}
            }
        }else{
            viewHolder.name.setText(optionDetails.getName());
        }

        if ( optionDetails.isAvailable() ){
            viewHolder.name.setTextColor(context.getResources().getColor(R.color.black));
            if ( viewHolder.info != null ){
                viewHolder.info.setVisibility(View.GONE);
            }
            if ( viewHolder.disabledIndicator != null ){
                viewHolder.disabledIndicator.setVisibility(View.GONE);
            }
        }else{
            viewHolder.name.setTextColor(context.getResources().getColor(R.color.disabled_color));
            viewHolder.name.setText(optionDetails.getName());

            if ( viewHolder.info != null ){
                if ( optionDetails.getDisabledTag() != null && optionDetails.getDisabledTag().length() > 0 ){
                    viewHolder.info.setVisibility(View.VISIBLE);
                    viewHolder.info.setText(optionDetails.getDisabledTag());
                }else{
                    viewHolder.info.setVisibility(View.GONE);
                }
            }
            if ( viewHolder.disabledIndicator != null ) {
                viewHolder.disabledIndicator.setVisibility(View.VISIBLE);
            }
        }

        if ( callback != null && viewHolder.disabledIndicator != null ){
            viewHolder.disabledIndicator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onShowDisabledReason(position);
                }
            });
        }
    }

    public int getFirstEnabledItemIndex(){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).isAvailable() ){
                return i;
            }
        }
        return 0;
    }

    public boolean isAtleastOneOptionAvailable(){
        boolean atleastOneOptionAvailable = false;
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).isAvailable() ){
                atleastOneOptionAvailable = true;
                break;
            }
        }
        return atleastOneOptionAvailable;
    }

    public void setItems(List<AttributeOptionDetails> listItems) {
        this.itemsList = listItems;
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void enableItem(AttributeOptionDetails item){
        /*itemsList.add(item);
        List<AttributeOptionDetails> sortedItemsList = new ArrayList<>();
        for (int i=0;i<originalItemsList.size();i++){
            if ( isContains(originalItemsList.get(i).getMeatItemID()) ){
                sortedItemsList.add(originalItemsList.get(i));
            }
        }
        itemsList.clear();
        for (int i=0;i<sortedItemsList.size();i++){
            itemsList.add(sortedItemsList.get(i));
        }*/
        String optionID = item.getID();
        for (int i=0;i< itemsList.size();i++){
            if ( itemsList.get(i) != null && itemsList.get(i).getID() != null ){
                if ( itemsList.get(i).getID().equals(optionID) ){
                    //itemsList.remove(i);
                    itemsList.get(i).setStatus(true);
                    notifyDataSetChanged();
                }
            }
        }
        notifyDataSetChanged();
    }

    private boolean isContains(String optionID){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(optionID) ){
                return true;
            }
        }
        return false;
    }

    public void disableItem(int position){
        itemsList.remove(position);
        notifyDataSetChanged();
    }

    public void disableItem(AttributeOptionDetails item){
        itemsList.remove(item);
        notifyDataSetChanged();
    }

    public void disableItem(String optionID){
        if ( optionID != null && optionID.length() > 0 ){
            for (int i=0;i< itemsList.size();i++){
                if ( itemsList.get(i) != null && itemsList.get(i).getID() != null ){
                    if ( itemsList.get(i).getID().equals(optionID) ){
                        //itemsList.remove(i);
                        itemsList.get(i).setStatus(false);
                        notifyDataSetChanged();
                    }
                }
            }
        }
    }

    //-------

    public AttributeOptionDetails getItem(int position){
        if ( itemsList != null && position < itemsList.size() ){
            return itemsList.get(position);
        }
        return null;
    }

    public List<AttributeOptionDetails> getAllItems() {
        return itemsList;
    }
}
