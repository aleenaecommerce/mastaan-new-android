package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.CouponDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 14/7/18.
 */

public class CouponsAdapter extends RecyclerView.Adapter<CouponsAdapter.ViewHolder> {

    Context context;
    List<CouponDetails> itemsList;
    Callback callback;

    public interface Callback {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View itemSelector;
        TextView code;
        TextView details;
        TextView expiry_date;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            code = (TextView) itemLayoutView.findViewById(R.id.code);
            details = (TextView) itemLayoutView.findViewById(R.id.details);
            expiry_date = (TextView) itemLayoutView.findViewById(R.id.expiry_date);
        }
    }

    public CouponsAdapter(Context context, List<CouponDetails> itemsList, Callback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public CouponsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_coupon, null);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(CouponsAdapter.ViewHolder viewHolder, final int position) {
        try{
            CouponDetails couponDetails = getItem(position);

            if ( couponDetails != null ){
                viewHolder.code.setText(couponDetails.getCouponCode());

                String details = "";
                if ( couponDetails.getDiscountType().equalsIgnoreCase("v") ){
                    details += "Rs."+ CommonMethods.getIndianFormatNumber(couponDetails.getDiscountValue())+" off";
                }else if ( couponDetails.getDiscountType().equalsIgnoreCase("p") ){
                    details += CommonMethods.getInDecimalFormat(couponDetails.getDiscountValue())+"% off"+(couponDetails.getMaximumDiscount()>0?" (max Rs."+CommonMethods.getIndianFormatNumber(couponDetails.getMaximumDiscount())+")":"");
                }
                if ( couponDetails.getMinimumOrderValue() > 0 ) {
                    details += " on min order of Rs." + CommonMethods.getIndianFormatNumber(couponDetails.getMinimumOrderValue());
                }
                if ( couponDetails.isForPreOrders() ){
                    if ( details.length() > 0 ){    details += "<br>";  }
                    details += "Only for pre-orders";
                }
                if ( couponDetails.isForFirstTimeOrder() ){
                    if ( details.length() > 0 ){    details += "<br>";  }
                    details += "Only for first order";
                }

                if ( details.length() > 0 ){
                    viewHolder.details.setVisibility(View.VISIBLE);
                    viewHolder.details.setText(CommonMethods.fromHtml(details));
                }else{  viewHolder.details.setVisibility(View.GONE);    }

                if ( couponDetails.getExpiryDate() != null && couponDetails.getExpiryDate().length() > 0 ){
                    viewHolder.expiry_date.setVisibility(View.VISIBLE);
                    viewHolder.expiry_date.setText("Valid till "+DateMethods.getReadableDateFromUTC(couponDetails.getExpiryDate(), DateConstants.MMM_DD_YYYY));
                }else{  viewHolder.expiry_date.setVisibility(View.GONE);    }

                if ( callback != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callback.onItemClick(position);
                        }
                    });
                }
            }
        }catch (Exception e){}
    }

    //======

    public void setItems(List<CouponDetails> items) {
        itemsList.clear();
        ;
        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {//for (int i = items.size() - 1; i >= 0; i--) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public List<CouponDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public CouponDetails getItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}