package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.viewholders.DateViewHolder;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.AttributeDetails;
import com.mastaan.buyer.models.CartItemDetails;
import com.mastaan.buyer.models.GlobalItemDetails;
import com.mastaan.buyer.models.MeatItemDetails;
import com.mastaan.buyer.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

public class CartItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<GlobalItemDetails> itemsList;
    CallBack callBack;

    String RS;

    ViewGroup parent;

    //List<CategoryDetails> categoriesList;

    public interface CallBack {
        void onItemClick(int position);
        void onAddOrUpdateSpecialInstructions(int position);
        void onItemMenuClick(int position, View view);
    }

    public class CartItemViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout itemSelector;
        TextView item_name;
        TextView item_details;
        //TextView addOrUpdateSpecialInstructions;
        TextView item_price;
        Button menuOptions;

        public CartItemViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = (RelativeLayout) itemLayoutView.findViewById(R.id.itemSelector);
            item_name = (TextView) itemLayoutView.findViewById(R.id.item_name);
            item_details = (TextView) itemLayoutView.findViewById(R.id.item_details);
            //addOrUpdateSpecialInstructions = (TextView) itemLayoutView.findViewById(R.id.addOrUpdateSpecialInstructions);
            item_price = (TextView) itemLayoutView.findViewById(R.id.item_price);
            menuOptions = (Button) itemLayoutView.findViewById(R.id.menuOptions);
        }
    }

    public CartItemsAdapter(Context context, List<GlobalItemDetails> itemsList, CallBack callBack) {
        this.context = context;
        this.itemsList = itemsList;
        this.callBack = callBack;
        RS = context.getResources().getString(R.string.Rs);
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if ( position >= 0 ){
            if ( itemsList.get(position).getDayDetails() != null ) {
                return Constants.DATE_VIEW_TYPE;
            }
            else if ( itemsList.get(position).getCartItemDetails() != null ) {
                return Constants.CART_ITEM_VIEW_TYPE;
            }
        }
        return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if ( viewType == Constants.DATE_VIEW_TYPE) {
            View viewPost = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_date_item, null);
            return new DateViewHolder(viewPost);
        }
        else if ( viewType == Constants.CART_ITEM_VIEW_TYPE) {
            View viewISupport = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_cart_item, null);
            return new CartItemViewHolder(viewISupport);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        try{
            if ( viewHolder.getItemViewType() == Constants.DATE_VIEW_TYPE){
                DateViewHolder dateViewHolder = (DateViewHolder) viewHolder;
                dateViewHolder.displayDetails(context, position, itemsList.get(position).getDayDetails());
            }
            else if ( viewHolder.getItemViewType() == Constants.CART_ITEM_VIEW_TYPE){
                final CartItemViewHolder cartItemViewHolder = (CartItemViewHolder) viewHolder;
                final CartItemDetails cartItemDetails = itemsList.get(position).getCartItemDetails();    // Getting Cart Item Details using Position...

                if ( cartItemDetails != null ){
                    WarehouseMeatItemDetails warehouseMeatItemDetails = cartItemDetails.getWarehouseMeatItemDetails();
                    MeatItemDetails meatItemDetails = warehouseMeatItemDetails.getMeatItemDetais();

                    cartItemViewHolder.item_name.setText(CommonMethods.capitalizeFirstLetter(meatItemDetails.getNameWithSize()));

                    List<String> selectedOptions = new ArrayList<>();
                    for (int i=0;i<cartItemDetails.getSelectedAttributes().size();i++){
                        selectedOptions.add(getOptionName(position, cartItemDetails.getSelectedAttributes().get(i).getAttributeID(), cartItemDetails.getSelectedAttributes().get(i).getOptinID()));//+"("+cartItemDetails.getSelectedAttributes().get(i).getName()+")");
                    }

                    String detailsString = CommonMethods.capitalizeStringWords(meatItemDetails.getCategoriesList().get(0).getName());//getCategoryName(meatItemDetails.getCategoriesList().get(0).getValue());
                    detailsString += ", "+cartItemDetails.getWeightString();
                    detailsString += selectedOptions.size()>0?"\n" + CommonMethods.getStringFromStringList(selectedOptions):"";
                    /*if ( cartItemDetails.getSpecialInstructions() != null ){
                        detailsString += "\n" + cartItemDetails.getSpecialInstructions();
                        cartItemViewHolder.addOrUpdateSpecialInstructions.setVisibility(View.GONE);
                        //cartItemViewHolder.addOrUpdateSpecialInstructions.setText(Html.fromHtml("<u>Update special instructions</u>"));
                    }else{
                        cartItemViewHolder.addOrUpdateSpecialInstructions.setVisibility(View.VISIBLE);
                        cartItemViewHolder.addOrUpdateSpecialInstructions.setText(Html.fromHtml("<u>Add special instructions</u>"));
                    }*/
                    cartItemViewHolder.item_details.setText(detailsString);


                    cartItemViewHolder.item_price.setText( RS+ CommonMethods.getInDecimalFormat(cartItemDetails.getTotal()));


                    if ( callBack != null) {
                        cartItemViewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callBack.onItemClick(position);
                            }
                        });
                        /*cartItemViewHolder.addOrUpdateSpecialInstructions.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callBack.onAddOrUpdateSpecialInstructions(position);
                            }
                        });*/
                        cartItemViewHolder.menuOptions.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callBack.onItemMenuClick(position, cartItemViewHolder.menuOptions);
                            }
                        });
                    }
                }
            }
        }catch (Exception e){}
    }

    public String getOptionName(int postion, String attributeID, String attributeOptionID){

        String attributeOptionName = "";

        CartItemDetails cartItemDetails = getItem(postion).getCartItemDetails();

        for (int i = 0; i<cartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getAttributes().size(); i++){
            if ( attributeID.equals(cartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getAttributes().get(i).getID())) {
                AttributeDetails attributeDetails = cartItemDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getAttributes().get(i);
                for (int j=0;j<attributeDetails.getAvailableOptions().size();j++){
                    if ( attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getValue()) || attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getID()) ){
                        attributeOptionName = attributeDetails.getAvailableOptions().get(j).getName();
                        break;
                    }
                }
            }
        }
        return attributeOptionName;
    }

    /*public String getCategoryName(String categoryValue){
        String categoryName = "";
        if ( categoriesList != null ){

            for (int i=0;i<categoriesList.size();i++){
                CategoryDetails categoryDetails = categoriesList.get(i);
                if ( categoryDetails.getValue().equalsIgnoreCase(categoryValue) ){
                    categoryName = categoryDetails.getName();
                    break;
                }
                if ( categoryDetails.getSubCategoriesCount() > 0 ){
                    for (int j=0;j<categoryDetails.getSubCategories().size();j++){
                        CategoryDetails subCategoryDetails = categoryDetails.getSubCategories().get(j);
                        if ( subCategoryDetails.getValue().equalsIgnoreCase(categoryValue) ){
                            categoryName = subCategoryDetails.getName() + " "+categoryDetails.getName();
                            if ( subCategoryDetails.getValue().equals("seawatersf") || subCategoryDetails.getValue().equals("freshwatersf") ){
                                categoryName = subCategoryDetails.getName();
                            }
                        }
                    }
                }
            }
        }
        return categoryName;
    }*/

    public void updateSpecialInstructions(int position, String specialInstructions){
        if ( position >= 0 && itemsList.size() > position && itemsList.get(position).getCartItemDetails() != null ){
            itemsList.get(position).getCartItemDetails().setSpecialInstructions(specialInstructions);
            notifyDataSetChanged();
        }
    }


    //======

    public void setItems(List<GlobalItemDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i <items.size(); i++) {//for (int i = items.size() - 1; i >= 0; i--) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void deleteItem(int position){
        if ( itemsList.size() > position ){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }
    public void updateItem(int position, GlobalItemDetails globalItemDetails){

        //Toast.makeText(context, position+" = "+globalItemDetails.getQuantity(), Toast.LENGTH_LONG).show();
        if ( itemsList.size() > position && position >= 0 ){
            if ( globalItemDetails != null ) {
                itemsList.set(position, globalItemDetails);
                notifyDataSetChanged();
            }
        }
    }

    public void deleteAllItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<GlobalItemDetails> getAllItems(){
        return itemsList;
    }

    public List<CartItemDetails> getAllCartItems(){
        List<CartItemDetails> cartItems = new ArrayList<>();
        if ( itemsList != null && itemsList.size() > 0 ) {
            for (int i = 0; i < itemsList.size(); i++) {
                if (itemsList.get(i) != null && itemsList.get(i).getCartItemDetails() != null) {
                    cartItems.add(itemsList.get(i).getCartItemDetails());
                }
            }
        }
        return cartItems;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public GlobalItemDetails getItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}