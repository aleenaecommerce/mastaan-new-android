package com.mastaan.buyer.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.AttributeDetails;
import com.mastaan.buyer.models.MeatItemDetails;
import com.mastaan.buyer.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

public class OrderItemsWithStatusAdapter extends RecyclerView.Adapter<OrderItemsWithStatusAdapter.ViewHolder> {

    Context context;
    List<OrderItemDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View itemSelector;
        TextView item_name;
        TextView item_quantity_amount;
        TextView item_details;

        View order_status;

        TextView processing;
        View processing_done_icon;

        TextView picked_up;
        View pickup_pending_icon;
        View pickup_done_icon;

        TextView on_the_way;
        View on_the_way_pending_icon;
        View on_the_way_done_icon;

        TextView delivered;
        View delivered_pending_icon;
        
        TextView failed;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            item_name = (TextView) itemLayoutView.findViewById(R.id.item_name);
            item_quantity_amount = (TextView) itemLayoutView.findViewById(R.id.item_quantity_amount);
            item_details = (TextView) itemLayoutView.findViewById(R.id.item_details);

            order_status = itemLayoutView.findViewById(R.id.order_status);

            processing = (TextView) itemLayoutView.findViewById(R.id.processing);
            processing_done_icon = itemLayoutView.findViewById(R.id.processing_done_icon);

            picked_up = (TextView) itemLayoutView.findViewById(R.id.picked_up);
            pickup_pending_icon = itemLayoutView.findViewById(R.id.pickup_pending_icon);
            pickup_done_icon = itemLayoutView.findViewById(R.id.pickup_done_icon);

            on_the_way = (TextView) itemLayoutView.findViewById(R.id.on_the_way);
            on_the_way_pending_icon = itemLayoutView.findViewById(R.id.on_the_way_pending_icon);
            on_the_way_done_icon = itemLayoutView.findViewById(R.id.on_the_way_done_icon);

            delivered = (TextView) itemLayoutView.findViewById(R.id.delivered);
            delivered_pending_icon = itemLayoutView.findViewById(R.id.delivered_pending_icon);

            failed = (TextView) itemLayoutView.findViewById(R.id.failed);
        }
    }

    public OrderItemsWithStatusAdapter(Context context, List<OrderItemDetails> itemsList, CallBack callBack) {
        this.context = context;
        this.itemsList = itemsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public OrderItemsWithStatusAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_item_with_status, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        try {
            final OrderItemDetails orderItemDetails = itemsList.get(position);
            MeatItemDetails meatItemDetails = orderItemDetails.getMeatItemDetails();

            if ( orderItemDetails != null ){

                viewHolder.item_name.setText(meatItemDetails.getNameWithCategoryAndSize());
                viewHolder.item_quantity_amount.setText(CommonMethods.getIndianFormatNumber(orderItemDetails.getQuantity())+" "+meatItemDetails.getQuantityUnit()+(orderItemDetails.getQuantity()!=1?"s":"")
                                                        +", "+SpecialCharacters.RS+CommonMethods.getIndianFormatNumber(orderItemDetails.getTotal()));

                List<String> selectedOptions = new ArrayList<>();
                for (int i = 0; i < orderItemDetails.getSelectedAttributes().size(); i++) {
                    selectedOptions.add(getOptionName(position, orderItemDetails.getSelectedAttributes().get(i).getAttributeID(), orderItemDetails.getSelectedAttributes().get(i).getOptinID()));
                }
                String detailsString = selectedOptions.size()>0?CommonMethods.getStringFromStringList(selectedOptions):"";
                if ( detailsString.length() > 0 ) {
                    viewHolder.item_details.setVisibility(View.VISIBLE);
                    viewHolder.item_details.setText(detailsString);
                }else{  viewHolder.item_details.setVisibility(View.GONE);   }


                GradientDrawable gradientDrawable;
                String status = orderItemDetails.getStatusString();

                if ( status.equalsIgnoreCase(Constants.ORDERED)
                        || status.equalsIgnoreCase(Constants.ACKNOWLEDGED) || status.equalsIgnoreCase(Constants.PROCESSING)
                        || status.equalsIgnoreCase(Constants.PROCESSED) || status.equalsIgnoreCase(Constants.ASSIGNED) ){       //  viewHolder.processing
                    gradientDrawable = (GradientDrawable) viewHolder.processing.getBackground();
                    gradientDrawable.setColor(context.getResources().getColor(R.color.order_processing_color));

                    viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.order_processing_color));
                    viewHolder.processing.setText(status.toUpperCase());

                    toggleViewsVisibility(new View[]{viewHolder.processing, viewHolder.pickup_pending_icon, viewHolder.on_the_way_pending_icon, viewHolder.delivered_pending_icon}, View.VISIBLE);
                    toggleViewsVisibility(new View[]{viewHolder.processing_done_icon, viewHolder.picked_up, viewHolder.pickup_done_icon, viewHolder.on_the_way, viewHolder.on_the_way_done_icon, viewHolder.delivered, viewHolder.failed}, View.GONE);
                }
                else if ( status.equalsIgnoreCase(Constants.PICKED_UP) ){        //  Pickup Pending
                    gradientDrawable = (GradientDrawable) viewHolder.picked_up.getBackground();
                    gradientDrawable.setColor(context.getResources().getColor(R.color.order_pickup_pending_color));

                    viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.order_pickup_pending_color));

                    toggleViewsVisibility(new View[]{viewHolder.processing_done_icon, viewHolder.picked_up, viewHolder.on_the_way_pending_icon, viewHolder.delivered_pending_icon}, View.VISIBLE);
                    toggleViewsVisibility(new View[]{viewHolder.processing, viewHolder.pickup_pending_icon, viewHolder.pickup_done_icon, viewHolder.on_the_way, viewHolder.on_the_way_done_icon, viewHolder.delivered, viewHolder.failed}, View.GONE);
                }
                else if ( status.equalsIgnoreCase(Constants.DELIVERING) ){        //  On the Way
                    gradientDrawable = (GradientDrawable) viewHolder.on_the_way.getBackground();
                    gradientDrawable.setColor(context.getResources().getColor(R.color.order_on_the_way_color));

                    viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.order_on_the_way_color));

                    toggleViewsVisibility(new View[]{viewHolder.processing_done_icon, viewHolder.pickup_done_icon, viewHolder.on_the_way, viewHolder.delivered_pending_icon}, View.VISIBLE);
                    toggleViewsVisibility(new View[]{viewHolder.processing, viewHolder.picked_up, viewHolder.pickup_pending_icon, viewHolder.on_the_way_pending_icon, viewHolder.on_the_way_done_icon, viewHolder.delivered, viewHolder.failed}, View.GONE);
                }

                else if ( status.equalsIgnoreCase(Constants.DELIVERED) ){        //  Delivered
                    gradientDrawable = (GradientDrawable) viewHolder.delivered.getBackground();
                    gradientDrawable.setColor(context.getResources().getColor(R.color.order_delivered_color));

                    viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.order_delivered_color));


                    toggleViewsVisibility(new View[]{viewHolder.processing_done_icon, viewHolder.pickup_done_icon, viewHolder.on_the_way_done_icon, viewHolder.delivered}, View.VISIBLE);
                    toggleViewsVisibility(new View[]{viewHolder.processing, viewHolder.picked_up, viewHolder.pickup_pending_icon, viewHolder.on_the_way, viewHolder.on_the_way_pending_icon, viewHolder.delivered_pending_icon, viewHolder.failed}, View.GONE);
                }

                else if ( status.equalsIgnoreCase(Constants.FAILED) || status.equalsIgnoreCase(Constants.REJECTED) ){        //  Failed
                    GradientDrawable failed_drawable = (GradientDrawable) viewHolder.failed.getBackground();
                    failed_drawable.setColor(context.getResources().getColor(R.color.order_failed_color));
                    viewHolder.order_status.setBackgroundColor(context.getResources().getColor(R.color.order_failed_color));

                    toggleViewsVisibility(new View[]{viewHolder.failed}, View.VISIBLE);
                    toggleViewsVisibility(new View[]{viewHolder.processing, viewHolder.processing_done_icon, viewHolder.picked_up, viewHolder.pickup_pending_icon, viewHolder.pickup_done_icon, viewHolder.on_the_way, viewHolder.on_the_way_pending_icon, viewHolder.on_the_way_done_icon, viewHolder.delivered, viewHolder.delivered_pending_icon}, View.GONE);
                }

                //----------

                if ( callBack != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callBack.onItemClick(position);
                        }
                    });
                }
            }

        }catch (Exception e){}

    }

    public String getOptionName(int position, String attributeID, String attributeOptionID){

        String attributeOptionName = "";

        OrderItemDetails orderItemDetails = getItem(position);

        for (int i=0;i<orderItemDetails.getMeatItemDetails().getAttributes().size();i++){
            if ( attributeID.equals(orderItemDetails.getMeatItemDetails().getAttributes().get(i).getID())) {
                AttributeDetails attributeDetails = orderItemDetails.getMeatItemDetails().getAttributes().get(i);
                for (int j=0;j<attributeDetails.getAvailableOptions().size();j++){
                    if ( attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getValue()) || attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getID()) ){
                        attributeOptionName = attributeDetails.getAvailableOptions().get(j).getName();
                        break;
                    }
                }
            }
        }
        return attributeOptionName;
    }


    private void toggleViewsVisibility(View[]views, int visibiity){
        if ( views != null && views.length > 0 ){
            for (int i=0;i<views.length;i++){
                if ( views[i] != null ){    views[i].setVisibility(visibiity); }
            }
        }
    }

    //-----------

    public void setItems(List<OrderItemDetails> items){
        if ( items == null ){   items = new ArrayList<>();  }
        itemsList = items;
        notifyDataSetChanged();
    }
    public void addItems(List<OrderItemDetails> items){

        for(int i=items.size()-1;i>=0;i--){
            itemsList.add(items.get(i));
        }
        notifyDataSetChanged();
    }

    public void deleteCartItem(int position){
        if ( itemsList.size() > position ){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }
    public void updateCartItem(int position, OrderItemDetails updatedItem){
        if ( itemsList.size() > position){
            itemsList.set(position, updatedItem);
            notifyDataSetChanged();
        }
    }

    public void deleteAllCartItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<OrderItemDetails> getAllCartItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public OrderItemDetails getItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}