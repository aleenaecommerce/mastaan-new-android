package com.mastaan.buyer.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.TransactionDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 11/03/19.
 */

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder> {

    Context context;
    List<TransactionDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onLinkClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View itemSelector;
        TextView type;
        TextView date;
        TextView amount;
        TextView details;
        TextView link;
        TextView ending_balance;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            type = itemLayoutView.findViewById(R.id.type);
            date = itemLayoutView.findViewById(R.id.date);
            amount = itemLayoutView.findViewById(R.id.amount);
            details = itemLayoutView.findViewById(R.id.details);
            link = itemLayoutView.findViewById(R.id.link);
            ending_balance = itemLayoutView.findViewById(R.id.ending_balance);
        }
    }

    public TransactionsAdapter(Context context, List<TransactionDetails> itemsList, Callback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public TransactionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_transaction, null);
        return (new ViewHolder(view));
    }

    @Override
    public void onBindViewHolder(final TransactionsAdapter.ViewHolder viewHolder, final int position) {
        try{
            TransactionDetails transactionDetails = getItem(position);
            if ( transactionDetails != null ){
                try{
                    if ( transactionDetails.getAmount() < 0 ){
                        ((GradientDrawable) viewHolder.amount.getBackground().mutate()).setColor(Color.parseColor("#F44336"));
                    }else {
                        ((GradientDrawable) viewHolder.amount.getBackground().mutate()).setColor(Color.parseColor("#4CAF50"));
                    }
                }catch (Exception e){}

                viewHolder.type.setText(transactionDetails.getTypeString());

                viewHolder.date.setText(DateMethods.getReadableDateFromUTC(transactionDetails.getCreatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A));

                viewHolder.amount.setText((transactionDetails.getAmount()<0?"- ":"")+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.abs(transactionDetails.getAmount())));

                if ( transactionDetails.getComments() != null && transactionDetails.getComments().trim().length() > 0 ){
                    viewHolder.details.setVisibility(View.VISIBLE);
                    viewHolder.details.setText(transactionDetails.getComments().trim());
                }else{  viewHolder.details.setVisibility(View.GONE);    }

                if ( transactionDetails.getLink() != null ){
                    viewHolder.link.setVisibility(View.VISIBLE);
                    viewHolder.link.setText(transactionDetails.getLink());
                }else{  viewHolder.link.setVisibility(View.GONE);   }

                viewHolder.ending_balance.setText(CommonMethods.fromHtml("Closing balance: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(transactionDetails.getEndingBalance())+"</b>"));

                //------

                if ( callback != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onItemClick(position);
                        }
                    });
                    viewHolder.link.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onLinkClick(position);
                        }
                    });
                }
            }
        }catch (Exception e){}
    }

    //------------

    public void setItems(List<TransactionDetails> items){
        itemsList.clear();
        addItems(items);
    }

    public void addItems(List<TransactionDetails> items){
        if ( items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public TransactionDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public List<TransactionDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

}