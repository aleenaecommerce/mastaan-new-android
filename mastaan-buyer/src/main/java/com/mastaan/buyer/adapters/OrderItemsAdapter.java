package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.AttributeDetails;
import com.mastaan.buyer.models.MeatItemDetails;
import com.mastaan.buyer.models.OrderItemDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder> {

    Context context;
    List<OrderItemDetails> itemsList;
    CallBack callBack;

    String RS;

    ViewGroup parent;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View itemSelector;
        TextView item_name;
        ImageView item_image;
        TextView item_details;
        TextView item_price;
        View failed_indicator;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            item_name = (TextView) itemLayoutView.findViewById(R.id.item_name);
            item_image = (ImageView) itemLayoutView.findViewById(R.id.item_image);
            item_details = (TextView) itemLayoutView.findViewById(R.id.item_details);
            item_price = (TextView) itemLayoutView.findViewById(R.id.item_price);
            failed_indicator = itemLayoutView.findViewById(R.id.failed_indicator);
        }
    }

    public OrderItemsAdapter(Context context, List<OrderItemDetails> itemsList, CallBack callBack) {
        this.context = context;
        this.itemsList = itemsList;
        this.callBack = callBack;
        RS = context.getResources().getString(R.string.Rs);
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public OrderItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_item, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        try {
            final OrderItemDetails orderItemDetails = itemsList.get(position);    // Getting Cart Item Details using Position...
            MeatItemDetails meatItemDetails = orderItemDetails.getMeatItemDetails();

            viewHolder.item_name.setText(CommonMethods.capitalizeFirstLetter(meatItemDetails.getNameWithCategoryAndSize()));

            Picasso.get()
                    .load(meatItemDetails.getThumbnailImageURL())
                    .placeholder(R.drawable.image_default_mastaan)
                    .error(R.drawable.image_default_mastaan)
                    .fit()
                    .centerCrop()
                    .tag(context)
                    .into(viewHolder.item_image);

            List<String> selectedOptions = new ArrayList<>();
            for (int i = 0; i < orderItemDetails.getSelectedAttributes().size(); i++) {
                selectedOptions.add(getOptionName(position, orderItemDetails.getSelectedAttributes().get(i).getAttributeID(), orderItemDetails.getSelectedAttributes().get(i).getOptinID()));
            }

            String weightQuantityUnit = "";
            if (meatItemDetails.getQuantityType().equalsIgnoreCase("n")) {
                weightQuantityUnit = "piece";
            } else if (meatItemDetails.getQuantityType().equalsIgnoreCase("w")) {
                weightQuantityUnit = "kg";
            } else if (meatItemDetails.getQuantityType().equalsIgnoreCase("s")) {
                weightQuantityUnit = "set";
            }
            viewHolder.item_details.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity()));
            if (orderItemDetails.getQuantity() == 1) {
                viewHolder.item_details.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " " + weightQuantityUnit);
            } else {
                viewHolder.item_details.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " " + weightQuantityUnit + "s");
            }

            if (selectedOptions.size() > 0) {
                viewHolder.item_details.append(", ");
            }
            viewHolder.item_details.append(CommonMethods.getStringFromStringList(selectedOptions));

            viewHolder.item_price.setText(RS + CommonMethods.getInDecimalFormat(orderItemDetails.getTotal()));

            viewHolder.failed_indicator.setVisibility(orderItemDetails.getStatusString().equalsIgnoreCase(Constants.FAILED)?View.VISIBLE:View.GONE);


            if ( callBack != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callBack.onItemClick(position);
                    }
                });
            }

        }catch (Exception e){}

    }

    public String getOptionName(int postion, String attributeID, String attributeOptionID){

        String attributeOptionName = "";

        OrderItemDetails orderItemDetails = getItem(postion);

        for (int i=0;i<orderItemDetails.getMeatItemDetails().getAttributes().size();i++){
            if ( attributeID.equals(orderItemDetails.getMeatItemDetails().getAttributes().get(i).getID())) {
                AttributeDetails attributeDetails = orderItemDetails.getMeatItemDetails().getAttributes().get(i);
                for (int j=0;j<attributeDetails.getAvailableOptions().size();j++){
                    if ( attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getValue()) || attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getID()) ){
                        attributeOptionName = attributeDetails.getAvailableOptions().get(j).getName();
                        break;
                    }
                }
            }
        }
        return attributeOptionName;
    }


    //-----------

    public void setItems(List<OrderItemDetails> items){
        if ( items == null ){   items = new ArrayList<>();  }
        itemsList = items;
        notifyDataSetChanged();
    }
    public void addItems(List<OrderItemDetails> items){

        for(int i=items.size()-1;i>=0;i--){
            itemsList.add(items.get(i));
        }
        notifyDataSetChanged();
    }

    public void deleteCartItem(int position){
        if ( itemsList.size() > position ){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }
    public void updateCartItem(int position, OrderItemDetails updatedItem){
        if ( itemsList.size() > position){
            itemsList.set(position, updatedItem);
            notifyDataSetChanged();
        }
    }

    public void deleteAllCartItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<OrderItemDetails> getAllCartItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public OrderItemDetails getItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}