package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.AttributeOptionDetails;

import java.util.ArrayList;
import java.util.List;

public class DateSelectionAdapter extends RecyclerView.Adapter<DateSelectionAdapter.ViewHolder> {

    Context context;
    String todayDate;
    String tomorrowDate;
    List<AttributeOptionDetails> itemsList;

    Callback callback;

    public interface Callback{
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        TextView date;
        TextView info;
        TextView price;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);

            date = (TextView) itemLayoutView.findViewById(R.id.date);
            info = (TextView) itemLayoutView.findViewById(R.id.info);
            price = (TextView) itemLayoutView.findViewById(R.id.price);
        }
    }

    public DateSelectionAdapter(Context context, List<AttributeOptionDetails> itemList, Callback callback) {
        this.context = context;
        this.todayDate = DateMethods.getOnlyDate(new LocalStorageData(context).getServerTime());
        this.tomorrowDate = DateMethods.addToDateInDays(todayDate, 1);
        this.itemsList = itemList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public DateSelectionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_date_selection_item, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        AttributeOptionDetails availabilityDetails = getItem(position);

        if ( availabilityDetails != null ){

            if ( DateMethods.compareDates(todayDate, availabilityDetails.getDate()) == 0 ){
                viewHolder.date.setText("Today ("+DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD_YYYY)+")");
            }else if ( DateMethods.compareDates(tomorrowDate, availabilityDetails.getDate()) == 0 ){
                viewHolder.date.setText("Tomorrow ("+DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD_YYYY)+")");
            }else{
                viewHolder.date.setText(DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD_YYYY));
            }

            if ( availabilityDetails.isAvailable() ){
                viewHolder.itemSelector.setVisibility(View.VISIBLE);
                viewHolder.date.setTextColor(context.getResources().getColor(R.color.black));
                viewHolder.price.setVisibility(View.VISIBLE);
                viewHolder.info.setVisibility(View.GONE);

                viewHolder.price.setText(SpecialCharacters.RS+" "+ CommonMethods.getIndianFormatNumber(availabilityDetails.getPriceDifference()));
                if ( availabilityDetails.getActualPriceDifference() > availabilityDetails.getPriceDifference() ){
                    try{
                        viewHolder.price.setText(" "+SpecialCharacters.RS + " " +CommonMethods.getInDecimalFormat(availabilityDetails.getActualPriceDifference())+" ", TextView.BufferType.SPANNABLE);
                        ((Spannable) viewHolder.price.getText()).setSpan(new StrikethroughSpan(), 0, viewHolder.price.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        viewHolder.price.append(" "+SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(availabilityDetails.getPriceDifference()));
                    }catch (Exception e){}
                }
            }else{
                viewHolder.itemSelector.setVisibility(View.GONE);
                viewHolder.date.setTextColor(context.getResources().getColor(R.color.disabled_color));
                viewHolder.price.setVisibility(View.GONE);
                viewHolder.info.setVisibility(View.VISIBLE);
                viewHolder.info.setText(availabilityDetails.getAvailabilityReason());
            }

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position);
                    }
                });
            }

        }

    }

    //------------

    public void setItems(List<AttributeOptionDetails> items){
        if ( items == null ){ items = new ArrayList<>();    }
        itemsList = items;
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItems(List<AttributeOptionDetails> items){
        if ( items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public AttributeOptionDetails getItem(int position){
        return itemsList.get(position);
    }

    public List<AttributeOptionDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

}