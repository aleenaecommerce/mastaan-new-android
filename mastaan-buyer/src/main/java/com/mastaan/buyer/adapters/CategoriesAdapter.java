package com.mastaan.buyer.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.CategoryDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 08/12/18.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {
    Context context;
    List<CategoryDetails> itemsList;
    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onShowDisabledInfo(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View itemSelector;
        View container;
        ImageView image;
        TextView name;
        TextView disabled_info;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            container = itemLayoutView.findViewById(R.id.container);
            image = itemLayoutView.findViewById(R.id.image);
            name = itemLayoutView.findViewById(R.id.name);
            disabled_info = itemLayoutView.findViewById(R.id.disabled_info);
        }
    }
    
    
    public CategoriesAdapter(Context context, List<CategoryDetails> itemsList, Callback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_category, null);
        return (new CategoriesAdapter.ViewHolder(view));
    }

    @Override
    public void onBindViewHolder(final CategoriesAdapter.ViewHolder viewHolder, final int position) {
        try{
            final CategoryDetails categoryDetails = getItem(position);
            if ( categoryDetails != null ){
                try {
                    GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                            new int[]{CommonMethods.parseColor(categoryDetails.getBackgroundColor()), CommonMethods.parseColor(categoryDetails.getGradientColor())});
                    gradientDrawable.setCornerRadius(0f);
                    viewHolder.container.setBackground(gradientDrawable);
                }catch (Exception e){
                    viewHolder.container.setBackgroundColor(CommonMethods.parseColor(categoryDetails.getBackgroundColor()));
                }

                try {
                    Picasso.get()
                            .load(categoryDetails.getIconURL())
                            .tag(context)
                            .into(viewHolder.image);
                }catch (Exception e){}

                viewHolder.name.setText(" "+categoryDetails.getName().toUpperCase()+"  ");

                if ( categoryDetails.getStatus() == false ){
                    viewHolder.disabled_info.setVisibility(View.VISIBLE);
                    viewHolder.disabled_info.setText(categoryDetails.getDisabledTitle());
                }else{
                    viewHolder.disabled_info.setVisibility(View.GONE);
                }

                //------

                if ( callback != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if ( categoryDetails.getStatus() ) {
                                callback.onItemClick(position);
                            }else{
                                callback.onShowDisabledInfo(position);
                            }
                        }
                    });
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //------------

    public void setItems(List<CategoryDetails> items){
        itemsList.clear();
        addItems(items);
    }

    public void addItems(List<CategoryDetails> items){
        if ( items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public CategoryDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public List<CategoryDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

}

/*int iconResourceID = -1;
                if ( categoryDetails.getValue().equalsIgnoreCase("chicken") ){
                    iconResourceID = R.drawable.icon_chicken;
                } else if ( categoryDetails.getValue().equalsIgnoreCase("mutton") ){
                    iconResourceID = R.drawable.icon_mutton;
                } else if ( categoryDetails.getValue().equalsIgnoreCase("seafood") ){
                    iconResourceID = R.drawable.icon_seafood;
                }

                if ( iconResourceID != -1 ){
                    Picasso.with(context)
                            .load(iconResourceID)
                            .tag(context)
                            .into(viewHolder.image);
                    //viewHolder.image.setImageResource(iconResourceID);
                }else{
                    Picasso.with(context)
                            .load(categoryDetails.getIconURL())
                            .tag(context)
                            .into(viewHolder.image);
                }*/