package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.AttributeOptionDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AttributeOptionsIconGridAdapter extends ArrayAdapter<AttributeOptionDetails> {

    Context context;
    int layoutResourceId;
    List<AttributeOptionDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position, AttributeOptionDetails optionDetails);
    }

    class ViewHolder {          // Food Item View Holder
        CardView cardView;
        Button optionSelector;
        View disabledIndicator;
        TextView option_name;
        ImageView option_image;
    }


    public AttributeOptionsIconGridAdapter(Context context, int layoutResourceId, List<AttributeOptionDetails> itemsList, Callback callback) {
        super(context, R.layout.view_attribute_option_icongrid, itemsList);

        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.itemsList = itemsList;
        this.callback = callback;
    }

    @Override
    public AttributeOptionDetails getItem(int position) { // << subclasses can use subtypes in overridden methods!
        return itemsList.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if ( convertView == null ) {
            convertView = LayoutInflater.from(context).inflate(layoutResourceId, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.cardView = (CardView) convertView.findViewById(R.id.cardView);
            viewHolder.option_name = (TextView) convertView.findViewById(R.id.option_name);
            viewHolder.option_image = (ImageView) convertView.findViewById(R.id.option_image);
            viewHolder.optionSelector = (Button) convertView.findViewById(R.id.optionSelector);
            viewHolder.disabledIndicator = convertView.findViewById(R.id.disabledIndicator);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //-------

        final AttributeOptionDetails optionDetails = getItem(position);

        viewHolder.option_name.setText(optionDetails.getName());
        if ( optionDetails.getPriceDifference() > 0 ) {
            viewHolder.option_name.setText(optionDetails.getName()+ " (+ " + SpecialCharacters.RS + CommonMethods.getInDecimalFormat(optionDetails.getPriceDifference()) + ")");
        }

        if ( optionDetails.isAvailable() ){
            viewHolder.option_image.setAlpha(1.0f);
            viewHolder.option_name.setTextColor(context.getResources().getColor(R.color.black));
            viewHolder.disabledIndicator.setVisibility(View.GONE);
        }else{
            if ( viewHolder.cardView != null ) {
                viewHolder.cardView.setCardElevation(0);
            }
            viewHolder.option_image.setAlpha(0.6f);
            viewHolder.option_name.setTextColor(context.getResources().getColor(R.color.disabled_color));
            viewHolder.disabledIndicator.setVisibility(View.VISIBLE);
        }

        Picasso.get()
                .load(optionDetails.getImageURL())
                .placeholder(R.drawable.image_default_mastaan)
                .error(R.drawable.image_default_mastaan)
                .tag(context)
                .into(viewHolder.option_image);

        if ( callback != null ){
            viewHolder.optionSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(position, optionDetails);
                }
            });
        }

        return convertView;
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItems(List<AttributeOptionDetails> items){

        if ( items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public List<AttributeOptionDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }
}