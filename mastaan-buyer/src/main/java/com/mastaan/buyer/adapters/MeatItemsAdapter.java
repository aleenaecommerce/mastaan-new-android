package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.viewholders.MeatItemViewHolder;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.WarehouseMeatItemDetails;

import java.util.List;

public class MeatItemsAdapter extends RecyclerView.Adapter<MeatItemViewHolder> {

    Context context;
    List<WarehouseMeatItemDetails> itemsList;

    String todaysDate="";
    boolean isSuperBuyer;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onCartClick(int position);
        void onNotifyClick(int position);
    }

    public MeatItemsAdapter(Context context, List<WarehouseMeatItemDetails> itemsList, Callback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;

        this.todaysDate = DateMethods.getOnlyDate(new LocalStorageData(context).getServerTime());
        //BuyerDetails buyerDetails = new LocalStorageData(context).getBuyerDetails();
        //if ( buyerDetails != null ){    isSuperBuyer = buyerDetails.isSuperBuyer(); }
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MeatItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_meat_item, null);
        return (new MeatItemViewHolder(view));
    }

    @Override
    public void onBindViewHolder(final MeatItemViewHolder viewHolder, final int position) {
        try{
            viewHolder.setTodaysDate(todaysDate).displayDetails(getItem(position), new MeatItemViewHolder.Callback() {
                @Override
                public void onItemClick() {
                    if ( callback != null ){
                        callback.onItemClick(position);
                    }
                }

                @Override
                public void onCartClick() {
                    if ( callback != null ){
                        callback.onCartClick(position);
                    }
                }

                @Override
                public void onNotifyClick() {
                    if ( callback != null ){
                        callback.onNotifyClick(position);
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //------------

    public void setItems(List<WarehouseMeatItemDetails> items){
        itemsList.clear();
        addItems(items);
    }

    public void addItems(List<WarehouseMeatItemDetails> items){
        if ( items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public WarehouseMeatItemDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public List<WarehouseMeatItemDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

}