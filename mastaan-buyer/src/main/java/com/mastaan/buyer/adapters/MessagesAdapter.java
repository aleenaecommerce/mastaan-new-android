package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.viewholders.MessageViewHolder;
import com.mastaan.buyer.models.MessageDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 22/11/18.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessageViewHolder> {

    Context context;
    int viewID = R.layout.view_message;
    List<MessageDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
    }

    public MessagesAdapter(Context context, List<MessageDetails> itemsList, Callback callback) {
        this(context, R.layout.view_message, itemsList, callback);
    }
    public MessagesAdapter(Context context, int viewID, List<MessageDetails> itemsList, Callback callback) {
        this.context = context;
        this.viewID = viewID;
        this.itemsList = itemsList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewID, null);
        if ( viewID == R.layout.view_message_home ) {
            view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
        return (new MessageViewHolder(view));
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder viewHolder, final int position) {
        try{
            viewHolder.displayDetails(getItem(position), new MessageViewHolder.Callback() {
                @Override
                public void onItemClick() {
                    if ( callback != null ){
                        callback.onItemClick(position);
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //------------

    public void setItems(List<MessageDetails> items){
        itemsList.clear();
        addItems(items);
    }

    public void addItems(List<MessageDetails> items){
        if ( items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public MessageDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public List<MessageDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

}