package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.adapters.viewholders.CartItemViewHolder;
import com.mastaan.buyer.adapters.viewholders.DateViewHolder;
import com.mastaan.buyer.adapters.viewholders.MeatItemViewHolder;
import com.mastaan.buyer.adapters.viewholders.MessageViewHolder;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.GlobalItemDetails;
import com.mastaan.buyer.models.MessageDetails;
import com.mastaan.buyer.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 22/11/18.
 */

public class GlobalItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<GlobalItemDetails> itemsList;

    int messageViewID = R.layout.view_message;
    public GlobalItemsAdapter setMessageViewID(int messageViewID) {
        this.messageViewID = messageViewID;
        return this;
    }

    String todaysDate="";

    MeatItemsAdapter.Callback meatItemsCallback;
    MessagesAdapter.Callback messagesCallback;

    public GlobalItemsAdapter(Context context, List<GlobalItemDetails> itemsList) {
        this.context = context;
        this.itemsList = itemsList;

        this.todaysDate = DateMethods.getOnlyDate(new LocalStorageData(context).getServerTime());
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if ( position >= 0 ){
            if ( itemsList.get(position).getWarehouseMeatItemDetails() != null ) {
                return Constants.MEAT_ITEM_VIEW_TYPE;
            }
            else if ( itemsList.get(position).getDayDetails() != null ) {
                return Constants.DATE_VIEW_TYPE;
            }
            else if ( itemsList.get(position).getCartItemDetails() != null ) {
                return Constants.CART_ITEM_VIEW_TYPE;
            }
            else if ( itemsList.get(position).getMessageDetails() != null ) {
                return Constants.MESSAGE_VIEW_TYPE;
            }
        }
        return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if ( viewType == Constants.MEAT_ITEM_VIEW_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_meat_item, null);
            return new MeatItemViewHolder(view);
        }
        else if ( viewType == Constants.DATE_VIEW_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_date_item, null);
            return new DateViewHolder(view);
        }
        else if ( viewType == Constants.CART_ITEM_VIEW_TYPE) {
            View veiw = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_cart_item, null);
            return new CartItemViewHolder(veiw);
        }
        else if ( viewType == Constants.MESSAGE_VIEW_TYPE) {
            View veiw = LayoutInflater.from(parent.getContext()).inflate(messageViewID, null);
            return new MessageViewHolder(veiw);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        try{
            // Meat Item
            if ( viewHolder.getItemViewType() == Constants.MEAT_ITEM_VIEW_TYPE ){
                ((MeatItemViewHolder) viewHolder).setTodaysDate(todaysDate).displayDetails(getItem(position).getWarehouseMeatItemDetails(), new MeatItemViewHolder.Callback() {
                    @Override
                    public void onItemClick() {
                        if ( meatItemsCallback != null ){
                            meatItemsCallback.onItemClick(position);
                        }
                    }

                    @Override
                    public void onCartClick() {
                        if ( meatItemsCallback != null ){
                            meatItemsCallback.onCartClick(position);
                        }
                    }

                    @Override
                    public void onNotifyClick() {
                        if ( meatItemsCallback != null ){
                            meatItemsCallback.onNotifyClick(position);
                        }
                    }
                });
            }
            // Date
            else if ( viewHolder.getItemViewType() == Constants.DATE_VIEW_TYPE ){
                DateViewHolder dateViewHolder = (DateViewHolder) viewHolder;
                dateViewHolder.displayDetails(context, position, itemsList.get(position).getDayDetails());
            }
            // Cart Item
            else if ( viewHolder.getItemViewType() == Constants.CART_ITEM_VIEW_TYPE ){
                CartItemViewHolder cartItemViewHolder = (CartItemViewHolder) viewHolder;

            }
            // Message
            else if ( viewHolder.getItemViewType() == Constants.MESSAGE_VIEW_TYPE ){
                ((MessageViewHolder) viewHolder).displayDetails(getItem(position).getMessageDetails(), new MessageViewHolder.Callback() {
                    @Override
                    public void onItemClick() {
                        if ( messagesCallback != null ){
                            messagesCallback.onItemClick(position);
                        }
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public GlobalItemsAdapter setMeatItemsCallback(MeatItemsAdapter.Callback meatItemsCallback) {
        this.meatItemsCallback = meatItemsCallback;
        return this;
    }
    public GlobalItemsAdapter setMessagesCallback(MessagesAdapter.Callback messagesCallback) {
        this.messagesCallback = messagesCallback;
        return this;
    }

    //==========

    private List<GlobalItemDetails> getGlobalItemsFromMessages(List<MessageDetails> items){
        List<GlobalItemDetails> itemsList = new ArrayList<>();
        for (int i=0;i<items.size();i++){
            itemsList.add(new GlobalItemDetails().setMessageDetails(items.get(i)));
        }
        return itemsList;
    }
    private List<GlobalItemDetails> getGlobalItemsFromWarehouseMeatItems(List<WarehouseMeatItemDetails> items){
        List<GlobalItemDetails> itemsList = new ArrayList<>();
        for (int i=0;i<items.size();i++){
            itemsList.add(new GlobalItemDetails().setWarehouseMeatItemDetails(items.get(i)));
        }
        return itemsList;
    }

    public void setWarehouseMeatItems(List<WarehouseMeatItemDetails> meatItemsList){
        setItems(getGlobalItemsFromWarehouseMeatItems(meatItemsList));
    }

    public void setMessages(List<MessageDetails> messagesList){
        setItems(getGlobalItemsFromMessages(messagesList));
    }
    public void addMessagesAtBeginning(List<MessageDetails> messagesList){
        addItemsAtBeginning(getGlobalItemsFromMessages(messagesList));
    }

    public void setItems(List<GlobalItemDetails> itemsList){
        if ( this.itemsList == null ){   this.itemsList = new ArrayList<>();  }
        this.itemsList.clear();;
        if ( itemsList != null && itemsList.size() > 0 ) {
            for (int i = 0; i <itemsList.size(); i++) {
                this.itemsList.add(itemsList.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItemsAtBeginning(List<GlobalItemDetails> itemsList){
        addItems(itemsList, true);
    }
    public void addItems(List<GlobalItemDetails> itemsList){
        addItems(itemsList, false);
    }
    private void addItems(List<GlobalItemDetails> itemsList, boolean atBeginning){
        if ( this.itemsList == null ){   this.itemsList = new ArrayList<>();  }
        if ( itemsList != null && itemsList.size() > 0 ) {
            if ( atBeginning && itemsList.size() > 0 ){
                for (int i = itemsList.size()-1;i >=0;i--) {
                    this.itemsList.add(0, itemsList.get(i));
                }
            }else{
                for (int i = 0; i <itemsList.size(); i++) {
                    this.itemsList.add(itemsList.get(i));
                }
            }
            notifyDataSetChanged();
        }
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public GlobalItemDetails getItem(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

    public void updateItem(int position, GlobalItemDetails globalItemDetails){
        if ( itemsList.size() > position && position >= 0 ){
            if ( globalItemDetails != null ) {
                itemsList.set(position, globalItemDetails);
                notifyDataSetChanged();
            }
        }
    }

    public void deleteItem(int position){
        if ( itemsList.size() > position ){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<GlobalItemDetails> getAllItems(){
        return itemsList;
    }

    public void deleteAllItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

}