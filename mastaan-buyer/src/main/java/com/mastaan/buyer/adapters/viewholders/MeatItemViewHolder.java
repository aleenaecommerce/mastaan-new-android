package com.mastaan.buyer.adapters.viewholders;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.AvailabilityDetails;
import com.mastaan.buyer.models.MeatItemDetails;
import com.mastaan.buyer.models.WarehouseMeatItemDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 22/11/18.
 */

public class MeatItemViewHolder extends RecyclerView.ViewHolder {
    Context context;

    View itemSelector;
    TextView item_name;
    TextView highlight;
    View pre_order_status;
    View add_on;
    TextView source;
    TextView multi_item_name;
    ImageView item_image;
    TextView item_price;
    TextView preparation_time;
    View item_price_pan;
    View not_available_pan;
    View actionAddToCart;
    View actionNotify;
    View fader;


    public interface Callback {
        void onItemClick();
        void onCartClick();
        void onNotifyClick();
    }

    public MeatItemViewHolder(View itemLayoutView) {
        super(itemLayoutView);
        context = itemLayoutView.getContext();

        itemSelector = itemLayoutView.findViewById(R.id.itemSelector);

        item_name = itemLayoutView.findViewById(R.id.item_name);
        highlight = itemLayoutView.findViewById(R.id.highlight);
        pre_order_status =  itemLayoutView.findViewById(R.id.pre_order_status);
        add_on = itemLayoutView.findViewById(R.id.add_on);
        source = itemLayoutView.findViewById(R.id.source);
        multi_item_name = itemLayoutView.findViewById(R.id.multi_item_name);
        item_image = itemLayoutView.findViewById(R.id.item_image);
        preparation_time = itemLayoutView.findViewById(R.id.preparation_time);
        item_price = itemLayoutView.findViewById(R.id.item_price);
        item_price_pan = itemLayoutView.findViewById(R.id.item_price_pan);
        not_available_pan = itemLayoutView.findViewById(R.id.not_available_pan);
        actionAddToCart = itemLayoutView.findViewById(R.id.actionAddToCart);
        actionNotify = itemLayoutView.findViewById(R.id.actionNotify);
        fader = itemLayoutView.findViewById(R.id.fader);
    }

    public void displayDetails(final WarehouseMeatItemDetails warehouseMeatItemDetails, final Callback callback){
        try{
            if ( warehouseMeatItemDetails != null && warehouseMeatItemDetails.getMeatItemDetais() != null ) {
                MeatItemDetails meatItemDetails = warehouseMeatItemDetails.getMeatItemDetais();

                item_name.setText(CommonMethods.capitalizeFirstLetter(meatItemDetails.getName())+(meatItemDetails.getSize()!=null&&meatItemDetails.getSize().trim().length()>0?" ("+meatItemDetails.getSize()+")":""));

                if ( warehouseMeatItemDetails.isAvailable() && warehouseMeatItemDetails.getHighlightText() != null && warehouseMeatItemDetails.getHighlightText().trim().length() > 0 ){
                    highlight.setVisibility(View.VISIBLE);
                    highlight.setText(warehouseMeatItemDetails.getHighlightText());
                }else{  highlight.setVisibility(View.GONE);  }

                pre_order_status.setVisibility(meatItemDetails.isPreOrderable()?View.VISIBLE:View.GONE);
                add_on.setVisibility(meatItemDetails.isAddOn()?View.VISIBLE:View.GONE);

                if ( warehouseMeatItemDetails.isAvailable() && warehouseMeatItemDetails.getSource() != null && warehouseMeatItemDetails.getSource().trim().length() > 0 ){
                    source.setVisibility(View.VISIBLE);
                    source.setText(CommonMethods.fromHtml("Sourced from <b>"+warehouseMeatItemDetails.getSource().trim()+"</b>"));
                }else{  source.setVisibility(View.GONE); }

                AvailabilityDetails availabilityDetails = warehouseMeatItemDetails.getAvailabilityDetailsForDate(getTodaysDate());
                item_price.setText(SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(availabilityDetails.getSellingPrice())+"/"+meatItemDetails.getQuantityUnit());
                if ( availabilityDetails.getActualSellingPrice() > availabilityDetails.getSellingPrice() ){
                    try{
                        item_price.setText(" "+SpecialCharacters.RS + " " +CommonMethods.getInDecimalFormat(availabilityDetails.getActualSellingPrice())+" ", TextView.BufferType.SPANNABLE);
                        ((Spannable) item_price.getText()).setSpan(new StrikethroughSpan(), 0, item_price.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        item_price.append(" "+SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(availabilityDetails.getSellingPrice())+"/"+meatItemDetails.getQuantityUnit());
                    }catch (Exception e){}
                }

                List<String> namesList = meatItemDetails.getMultiLingualNamesList();
                if ( isSuperBuyer() ){
                    namesList = meatItemDetails.getEnglishNamesList();
                }

                if ( namesList.size() > 1 ){
                    List<String> remainNames = new ArrayList<>();
                    for (int i =1 ;i<namesList.size();i++){
                        remainNames.add(namesList.get(i));
                    }
                    multi_item_name.setVisibility(View.VISIBLE);
                    multi_item_name.setText(CommonMethods.getStringFromStringList(remainNames));
                }else{
                    multi_item_name.setVisibility(View.GONE);
                }

                Picasso.get()
                        .load(meatItemDetails.getThumbnailImageURL())
                        .placeholder(R.drawable.image_default_mastaan)
                        .error(R.drawable.image_default_mastaan)
                        .fit()
                        .centerCrop()
                        .tag(context)
                        .into(item_image);

                if ( meatItemDetails.getPreparationHours() > 0 ){
                    preparation_time.setText("Prep. time: " + DateMethods.getTimeStringFromMinutes((int) (meatItemDetails.getPreparationHours() * 60), "hr", "min"));
                }else{
                    preparation_time.setText("");
                }

                if ( warehouseMeatItemDetails.isAvailable() ){
                    item_price_pan.setVisibility(View.VISIBLE);
                    preparation_time.setVisibility(View.VISIBLE);
                    not_available_pan.setVisibility(View.GONE);
                    actionNotify.setVisibility(View.GONE);
                    fader.setVisibility(View.GONE);
                }else{
                    not_available_pan.setVisibility(View.VISIBLE);
                    actionNotify.setVisibility(View.VISIBLE);
                    fader.setVisibility(View.VISIBLE);
                    item_price_pan.setVisibility(View.INVISIBLE);
                    preparation_time.setVisibility(View.INVISIBLE);
                }

                //-----------------

                if ( callback != null ){
                    itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onItemClick();
                        }
                    });
                    actionAddToCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onCartClick();
                        }
                    });
                    actionNotify.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onNotifyClick();
                        }
                    });
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //------

    boolean isSuperBuyer;
    public MeatItemViewHolder setSuperBuyer(boolean superBuyer) {
        isSuperBuyer = superBuyer;
        return this;
    }
    public boolean isSuperBuyer() {
        return isSuperBuyer;
    }

    String todaysDate;
    public MeatItemViewHolder setTodaysDate(String todaysDate) {
        this.todaysDate = todaysDate;
        return this;
    }
    public String getTodaysDate() {
        if ( todaysDate == null || todaysDate.trim().length() == 0 ){
            todaysDate = DateMethods.getOnlyDate(new LocalStorageData(context).getServerTime());
        }
        return todaysDate;
    }

}
