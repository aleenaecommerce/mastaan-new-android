package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.OrderDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 27/02/19.
 */

public class OrderFeedbacksAdapter extends RecyclerView.Adapter<OrderFeedbacksAdapter.ViewHolder> {

    Context context;
    List<OrderDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View itemSelector;
        TextView title;
        TextView details;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            title = itemLayoutView.findViewById(R.id.title);
            details = itemLayoutView.findViewById(R.id.details);
        }
    }

    public OrderFeedbacksAdapter(Context context, List<OrderDetails> itemsList, Callback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public OrderFeedbacksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_feedback, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return (new OrderFeedbacksAdapter.ViewHolder(view));
    }

    @Override
    public void onBindViewHolder(final OrderFeedbacksAdapter.ViewHolder viewHolder, final int position) {
        try{
            OrderDetails orderDetails = getItem(position);
            
            if ( orderDetails != null ) {
                viewHolder.title.setText(orderDetails.getFormattedDeliveryDate()+" ("+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalAmount())+")");

                viewHolder.details.setText(orderDetails.getOrderItemsNames());
                
                if (callback != null) {
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onItemClick(position);
                        }
                    });
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //------------

    public void setItems(List<OrderDetails> items){
        itemsList.clear();
        addItems(items);
    }

    public void addItems(List<OrderDetails> items){
        if ( items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public OrderDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<OrderDetails> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

}