package com.mastaan.buyer.adapters.viewholders;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.mastaan.buyer.models.WarehouseMeatItemDetails;

/**
 * Created by Venkatesh Uppu on 22/11/18.
 */

public class CartItemViewHolder extends RecyclerView.ViewHolder {
    Context context;

    public interface Callback {
        void onItemClick();
        void onCartClick();
        void onNotifyClick();
    }

    public CartItemViewHolder(View itemLayoutView) {
        super(itemLayoutView);
        context = itemLayoutView.getContext();


    }

    public void displayDetails(final WarehouseMeatItemDetails warehouseMeatItemDetails, final Callback callback){
        try{
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
