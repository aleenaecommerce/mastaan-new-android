package com.mastaan.buyer.adapters.viewholders;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.MessageDetails;
import com.squareup.picasso.Picasso;

/**
 * Created by Venkatesh Uppu on 22/11/18.
 */

public class MessageViewHolder extends RecyclerView.ViewHolder {
    Context context;

    View itemSelector;
    ImageView image;
    View detailsView;
    TextView title;
    TextView details;

    public interface Callback {
        void onItemClick();
    }

    public MessageViewHolder(View itemLayoutView) {
        super(itemLayoutView);
        context = itemLayoutView.getContext();

        itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
        /*container = itemLayoutView.findViewById(R.id.container);*/
        image = itemLayoutView.findViewById(R.id.image);
        detailsView = itemLayoutView.findViewById(R.id.detailsView);
        title =  itemLayoutView.findViewById(R.id.title);
        details = itemLayoutView.findViewById(R.id.details);
    }

    public void displayDetails(final MessageDetails messageDetails, final Callback callback){
        try{
            if ( messageDetails != null ) {
                if ( messageDetails.getImage() != null && messageDetails.getImage().trim().length() > 0 ){
                    image.setVisibility(View.VISIBLE);
                    detailsView.setVisibility(View.GONE);
                    Picasso.get()
                            .load(messageDetails.getImage())
                            .placeholder(R.drawable.image_default)
                            .error(R.drawable.image_default)
                            .into(image, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError(Exception e) {

                                }




                            });
                }
                else if ( (messageDetails.getTitle() != null && messageDetails.getTitle().trim().length() > 0) || (messageDetails.getDetails() != null && messageDetails.getDetails().trim().length() > 0) ){
                    detailsView.setVisibility(View.VISIBLE);
                    image.setVisibility(View.GONE);

                    if ( messageDetails.getTitle() != null && messageDetails.getTitle().trim().length() > 0 ){
                        title.setVisibility(View.VISIBLE);
                        title.setText(CommonMethods.fromHtml(messageDetails.getTitle()));
                    }else{  title.setVisibility(View.GONE); }

                    if ( messageDetails.getDetails() != null && messageDetails.getDetails().trim().length() > 0 ){
                        details.setVisibility(View.VISIBLE);
                        details.setText(CommonMethods.fromHtml(messageDetails.getDetails()));
                    }else{  details.setVisibility(View.GONE); }
                }

                /*try{
                    if ( messageDetails.getColor() != null && messageDetails.getColor().toLowerCase().contains("#") ){
                        container.setBackgroundColor(Color.parseColor(messageDetails.getColor()));
                    }else{
                        container.setBackgroundColor(Color.WHITE);
                    }
                }catch (Exception e){}*/

                //-----------------

                if ( callback != null ){
                    itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onItemClick();
                        }
                    });
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
