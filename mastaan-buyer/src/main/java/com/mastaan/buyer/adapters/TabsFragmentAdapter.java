package com.mastaan.buyer.adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aleena.common.fragments.NotYetFragment;
import com.mastaan.buyer.fragments.AddressBookFragment;
import com.mastaan.buyer.fragments.OrderHistoryFragment;
import com.mastaan.buyer.fragments.PrivacyPolicyFragment;
import com.mastaan.buyer.fragments.ProfileSummaryFragment;
import com.mastaan.buyer.fragments.TermsAndConditionsFragment;

public class TabsFragmentAdapter extends FragmentPagerAdapter {

    Context context;
    String[] itemsList;

    public TabsFragmentAdapter(Context context, FragmentManager fragmentManager, String[] itemsList) {
        super(fragmentManager);
        this.context = context;
        this.itemsList =  itemsList;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        if ( itemsList[position].equalsIgnoreCase("Profile") ){
            fragment = new ProfileSummaryFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Address Book") ){
            fragment = new AddressBookFragment().newInstance(context, false, null);
        }
        else if ( itemsList[position].equalsIgnoreCase("Order History") ){
            fragment = new OrderHistoryFragment();
        }
        /*
        else if ( itemsList[position].equalsIgnoreCase("Favorites") ){
            fragment = new FavouritesFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Loyalty center") ){
            fragment = new LoyaltyCenterFragment();
        }*/
        else if ( itemsList[position].equalsIgnoreCase("Terms & Conditions") ){
            fragment = new TermsAndConditionsFragment();
        }
        else if ( itemsList[position].equalsIgnoreCase("Privacy policy") ){
            fragment = new PrivacyPolicyFragment();
        }

        else{
            fragment = new NotYetFragment();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return itemsList.length;
    }

}
