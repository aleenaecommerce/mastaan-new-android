package com.mastaan.buyer.adapters.viewholders;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.R;
import com.mastaan.buyer.models.DayDetails;

/**
 * Created by venkatesh on 3/6/16.
 */
public class DateViewHolder extends RecyclerView.ViewHolder {

    TextView date;

    public DateViewHolder(View itemLayoutView) {
        super(itemLayoutView);

        date = (TextView) itemLayoutView.findViewById(R.id.date);
    }

    public void displayDetails(final Context context, final int position, final DayDetails dayDetails){

        if ( dayDetails != null ){
            //String fullDate = DateMethods.getReadableDateFromUTC(dayDetails.getDate());
            //String time = DateMethods.getTimeIn24HrFormat(fullDate);
            //String displayText = DateMethods.getDateInFormat(fullDate, "dd MMM")+", "+/*DateMethods.getTimeIn12HrFormat(DateMethods.addToDateInHours(time, -1))+" to "+*/DateMethods.getTimeIn12HrFormat(time);
            //displayText = displayText.toUpperCase();

            String displayText = dayDetails.getFormattedDate();//.toUpperCase();

            if ( dayDetails.getCount() > 0 ){
                if ( dayDetails.getCount() == 1 ){
                    displayText += " - "+dayDetails.getCount()+" item";
                }else{
                    displayText += " - "+dayDetails.getCount()+" items";
                }
            }
            if ( dayDetails.getAmount() > 0 ){
                displayText += " ("+ SpecialCharacters.RS+" "+ CommonMethods.getIndianFormatNumber(dayDetails.getAmount())+")";
            }
            date.setText(displayText);
        }
    }

}
