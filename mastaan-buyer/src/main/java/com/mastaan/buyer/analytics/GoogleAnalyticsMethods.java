package com.mastaan.buyer.analytics;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.models.OrderItemDetails;

import java.util.List;

/**
 * Created by venkatesh on 7/6/16.
 */
public class GoogleAnalyticsMethods {

    private Tracker analyticsTracker;

    public GoogleAnalyticsMethods(Tracker analyticsTracker){
        this.analyticsTracker = analyticsTracker;
    }

    public Tracker getAnalyticsTracker() {
        return analyticsTracker;
    }

    public void sendScreenName(String screenName){
        try {
            if ( analyticsTracker != null ) {
                analyticsTracker.setScreenName(screenName);
                analyticsTracker.send(new HitBuilders.ScreenViewBuilder().build());
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    public void sendEvent(String categoryID, String actionName, String labelName){
        sendEvent(true, categoryID, actionName, labelName);
    }

    public void sendEvent(boolean isUserInteractive, String categoryID, String actionName, String labelName){
        if ( categoryID == null ){ categoryID = ""; }
        if ( actionName == null ){  actionName = "";    }
        if ( labelName == null ){   labelName = ""; }

        try {
            if ( analyticsTracker != null ) {
                if (isUserInteractive) {
                    analyticsTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(categoryID)
                            .setAction(actionName)
                            .setLabel(labelName)
                            .build());
                } else {
                    analyticsTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(categoryID)
                            .setAction(actionName)
                            .setLabel(labelName)
                            .setNonInteraction(true)
                            .build());
                }
            }
        }catch (Exception e){ e.printStackTrace();  }
    }


    public void sendOrderTransaction(List<OrderDetails> ordersList){
        sendOrderTransaction(ordersList);
    }
    public void sendOrderTransaction(String screenName, List<OrderDetails> ordersList){
        if ( ordersList != null && ordersList.size() > 0 ){
            for (int i=0;i<ordersList.size();i++){
                sendOrderTransaction(screenName, ordersList.get(i));
            }
        }
    }
    public void sendOrderTransaction(OrderDetails orderDetails){
        sendOrderTransaction(null, orderDetails);
    }
    public void sendOrderTransaction(String screenName, OrderDetails orderDetails){
        try{
            if ( analyticsTracker != null ) {
                if (orderDetails != null) {
                    HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder();
                    for (int i = 0; i < orderDetails.getOrderItems().size(); i++) {
                        OrderItemDetails orderItemDetails = orderDetails.getOrderItems().get(i);

                        int quantity = Math.round(orderItemDetails.getMeatItemDetails().getQuantityUnit().equalsIgnoreCase("kg") ? orderItemDetails.getQuantity() * 1000/*kgs to grams*/ : orderItemDetails.getQuantity());
                        double unitPrice = orderItemDetails.getTotal() / quantity;

                        // Adding product
                        builder.addProduct(new Product()
                                .setId(orderItemDetails.getMeatItemDetails().getID())
                                .setName(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())
                                .setCategory(orderItemDetails.getMeatItemDetails().getCategoryDetails().getName())
                                .setPrice(unitPrice)
                                .setQuantity(quantity));
                    }

                    // Adding transaction
                    ProductAction productAction = new ProductAction(ProductAction.ACTION_PURCHASE)
                            .setTransactionId(orderDetails.getOrderID())
                            .setTransactionAffiliation("Mastaan Android")
                            .setTransactionRevenue(orderDetails.getTotalAmount() + orderDetails.getCouponDiscount())
                            .setTransactionTax(orderDetails.getGovernmentTaxes())
                            .setTransactionShipping(orderDetails.getDeliveryCharges());
                    if (orderDetails.getCouponcode() != null && orderDetails.getCouponcode().trim().length() > 0) {
                        productAction.setTransactionCouponCode(orderDetails.getCouponcode());
                    }
                    builder.setProductAction(productAction);

                    // Sending
                    if (screenName != null && screenName.trim().length() > 0) {
                        analyticsTracker.setScreenName(screenName);
                    }
                    analyticsTracker.set("&cu", "INR");
                    analyticsTracker.send(builder.build());
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }


    public void sendOrderRefundTransaction(OrderDetails orderDetails){
        try{
            if ( analyticsTracker != null ) {
                if (orderDetails != null) {
                    HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder();

                    builder.setProductAction(new ProductAction(ProductAction.ACTION_REFUND)
                            .setTransactionId(orderDetails.getOrderID()));

                    analyticsTracker.setScreenName("Cancel Order");
                    analyticsTracker.send(builder.build());
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }
    public void sendOrderItemRefundTransaction(String orderID, OrderItemDetails orderItemDetails){
        try{
            if ( analyticsTracker != null ) {
                if (orderItemDetails != null) {
                    HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder();

                    builder.addProduct(new Product().setId(orderItemDetails.getMeatItemDetails().getID())
                            .setQuantity(Math.round(orderItemDetails.getMeatItemDetails().getQuantityUnit().equalsIgnoreCase("kg") ? orderItemDetails.getQuantity() * 1000/*kgs to grams*/ : orderItemDetails.getQuantity())));

                    builder.setProductAction(new ProductAction(ProductAction.ACTION_REFUND)
                            .setTransactionId(orderID));

                    analyticsTracker.setScreenName("Cancel Order Item");
                    analyticsTracker.send(builder.build());
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }


}
