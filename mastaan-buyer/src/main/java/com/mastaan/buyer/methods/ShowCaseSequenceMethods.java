package com.mastaan.buyer.methods;

import android.app.Activity;
import android.view.View;

import com.aleena.common.R;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

/**
 * Created by venkatesh on 24/9/15.
 */

public class ShowCaseSequenceMethods {

    Activity activity;
    String showCaseUniqueID;
    String defaultActionName = "GOT IT";
    ShowcaseConfig showcaseConfig;
    MaterialShowcaseSequence showcaseSequence;

    public ShowCaseSequenceMethods(Activity activity, String showCaseUniqueID){
        this.activity = activity;
        this.showCaseUniqueID = showCaseUniqueID;
        showcaseConfig = new ShowcaseConfig();
        showcaseConfig.setDelay(100);
        showcaseConfig.setMaskColor(activity.getResources().getColor(R.color.showcase_mask_color));
        showcaseConfig.setContentTextColor(activity.getResources().getColor(R.color.showcase_hint_message_color));
        showcaseConfig.setDismissTextColor(activity.getResources().getColor(R.color.showcase_action_message_color));
        showcaseSequence = new MaterialShowcaseSequence(activity, showCaseUniqueID);
        showcaseSequence.setConfig(showcaseConfig);
    }

    public ShowCaseSequenceMethods(Activity activity, String showCaseUniqueID, int maskColor, int hintTextColor, int dismissTextColor){
        this.activity = activity;
        this.showCaseUniqueID = showCaseUniqueID;
        showcaseConfig = new ShowcaseConfig();
        showcaseConfig.setDelay(100);
        showcaseConfig.setMaskColor(maskColor);
        showcaseConfig.setContentTextColor(hintTextColor);
        showcaseConfig.setDismissTextColor(dismissTextColor);
        showcaseSequence = new MaterialShowcaseSequence(activity, showCaseUniqueID);
        showcaseSequence.setConfig(showcaseConfig);
    }

    public ShowCaseSequenceMethods(Activity activity, String showCaseUniqueID, String defaultActionName){
        this.activity = activity;
        this.showCaseUniqueID = showCaseUniqueID;
        this.defaultActionName = defaultActionName;
        showcaseConfig = new ShowcaseConfig();
        showcaseConfig.setDelay(100);
        showcaseConfig.setMaskColor(activity.getResources().getColor(R.color.showcase_mask_color));
        showcaseConfig.setContentTextColor(activity.getResources().getColor(R.color.showcase_hint_message_color));
        showcaseConfig.setDismissTextColor(activity.getResources().getColor(R.color.showcase_action_message_color));
        showcaseSequence = new MaterialShowcaseSequence(activity, showCaseUniqueID);
        showcaseSequence.setConfig(showcaseConfig);
    }

    public ShowCaseSequenceMethods(Activity activity, String showCaseUniqueID, int maskColor, int hintTextColor, int dismissTextColor, String defaultActionName){
        this.activity = activity;
        this.showCaseUniqueID = showCaseUniqueID;
        this.defaultActionName = defaultActionName;
        showcaseConfig = new ShowcaseConfig();
        showcaseConfig.setDelay(100);
        showcaseConfig.setMaskColor(maskColor);
        showcaseConfig.setContentTextColor(hintTextColor);
        showcaseConfig.setDismissTextColor(dismissTextColor);
        showcaseSequence = new MaterialShowcaseSequence(activity, showCaseUniqueID);
        showcaseSequence.setConfig(showcaseConfig);
    }

    public void addShowCaseView(View view, String hintMessage, String actionMessage){
        if ( actionMessage == null || actionMessage.length() == 0 ){
            showcaseSequence.addSequenceItem(view, hintMessage, defaultActionName);
        }else{
            showcaseSequence.addSequenceItem(view, hintMessage, actionMessage);
        }
    }
    public void addShowCaseView(View view, String hintMessage){
        showcaseSequence.addSequenceItem(view, hintMessage, defaultActionName);
    }

    public void startShowCase(){
        if ( showcaseSequence != null ){
            showcaseSequence.start();
        }
    }

    public void showSingeView(View view, String hintMessage, String actionMessage, String showCaseUniqueID){
        showSingeView( view, activity.getResources().getColor(R.color.showcase_mask_color), hintMessage, activity.getResources().getColor(R.color.showcase_hint_message_color), actionMessage, activity.getResources().getColor(R.color.showcase_action_message_color), showCaseUniqueID);
    }

    public void showSingeView(View view, int maskColor, String hintMessage, int hintTextColor, String actionMessage, int actionTextColor, String showCaseUniqueID){

        new MaterialShowcaseView.Builder(activity)
                .setTarget(view)
                .setMaskColour(maskColor)
                .setContentTextColor(hintTextColor)
                .setDismissTextColor(actionTextColor)
                .setContentText(hintMessage)
                .setDismissText(actionMessage)
                .setDelay(100)
                .singleUse(showCaseUniqueID)
                .show();
    }
}
