package com.mastaan.buyer.methods;

import com.mastaan.buyer.models.MeatItemDetails;

/**
 * Created by valueduser on 24/8/16.
 */
public final class MeatItemMethods {

    public static final String weightQuantityType(MeatItemDetails meatItemDetails){
        String weightQuantityType = "";
        if ( meatItemDetails.getQuantityType().equalsIgnoreCase("n") ){
            weightQuantityType = "No. of pieces";
        }
        else if ( meatItemDetails.getQuantityType().equalsIgnoreCase("w") ){
            weightQuantityType = "Weight";
        }
        else if ( meatItemDetails.getQuantityType().equalsIgnoreCase("s") ){
            weightQuantityType = "No. of sets";
        }
        return weightQuantityType;
    }

    public static final String weightQuantityUnit(MeatItemDetails meatItemDetails){
        String weightQuantityUnit = "";
        if ( meatItemDetails.getQuantityType().equalsIgnoreCase("n") ){
            weightQuantityUnit = "piece";
        }
        else if ( meatItemDetails.getQuantityType().equalsIgnoreCase("w") ){
            weightQuantityUnit = "kg";
        }
        else if ( meatItemDetails.getQuantityType().equalsIgnoreCase("s") ){
            weightQuantityUnit = "set";
        }
        return weightQuantityUnit;
    }
}
