package com.mastaan.buyer.methods;

import android.os.AsyncTask;

import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.models.CartItemDetails;
import com.mastaan.buyer.models.DayDetails;
import com.mastaan.buyer.models.GlobalItemDetails;
import com.mastaan.buyer.models.GroupedCartItems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Venkatesh Uppu on 25/8/16.
 */
public final class GroupingMethods {

    public interface GroupedCartItemsCallback{
        void onComplete(List<GroupedCartItems> groupedCartItems);
    }

    public static final void groupCartItems(final List<CartItemDetails> cartItems, final GroupedCartItemsCallback callback){
        new AsyncTask<Void, Void, Void>() {
            List<GroupedCartItems> groupedCartItemsList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... voids) {
                groupedCartItemsList = groupCartItems(cartItems);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(groupedCartItemsList);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public static final List<GroupedCartItems> groupCartItems(final List<CartItemDetails> cartItems){
        List<GroupedCartItems> groupedCartItemsList = new ArrayList<>();

        Map<String, GroupedCartItems> keyMapSet = new HashMap<>();
        for (int i=0;i<cartItems.size();i++){
            final CartItemDetails cartItemDetails = cartItems.get(i);
            if ( cartItemDetails != null ){
                String slot = cartItemDetails.getDeliveryStartByDate()+":"+cartItemDetails.getDeliveryByDate();
                if ( keyMapSet.containsKey(slot) ) {
                    keyMapSet.get(slot).addCartItem(cartItemDetails);
                }
                else {
                    GroupedCartItems groupedCartItems = new GroupedCartItems(cartItemDetails.getDeliveryByDate(), new ArrayList<CartItemDetails>()).setFormattedDate(cartItemDetails.getFormattedDeliveryDate());
                    keyMapSet.put(slot, groupedCartItems);
                    keyMapSet.get(slot).addCartItem(cartItemDetails);
                }
            }
        }

        for (String key : keyMapSet.keySet()) {
            groupedCartItemsList.add(keyMapSet.get(key));
        }

        Collections.sort(groupedCartItemsList, new Comparator<GroupedCartItems>() {
            public int compare(GroupedCartItems item1, GroupedCartItems item2) {
                return item1.getDate().compareToIgnoreCase(item2.getDate());
            }
        });

        return groupedCartItemsList;
    }


    //=============

    public static final List<GlobalItemDetails> getGlobalModelsList(List<GroupedCartItems> groupdCartItemsList){
        List<GlobalItemDetails> globalModelsList = new ArrayList<>();

        if ( groupdCartItemsList != null && groupdCartItemsList.size() > 0 ){
            for (int i=0;i<groupdCartItemsList.size();i++){
                globalModelsList.add(new GlobalItemDetails().setDayDetails(new DayDetails(DateMethods.getReadableDateFromUTC(groupdCartItemsList.get(i).getDate()), "", groupdCartItemsList.get(i).getItemsCount(), groupdCartItemsList.get(i).getTotalAmount()).setFormattedDate(groupdCartItemsList.get(i).getFormattedDate())));
                for (int j=0;j<groupdCartItemsList.get(i).getCartItems().size();j++){
                    globalModelsList.add(new GlobalItemDetails().setCartItemDetails(groupdCartItemsList.get(i).getCartItems().get(j)));
                }
            }
        }

        return globalModelsList;
    }
}
