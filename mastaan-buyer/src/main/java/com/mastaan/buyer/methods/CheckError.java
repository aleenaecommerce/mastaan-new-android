package com.mastaan.buyer.methods;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.Html;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.activities.LaunchActivity;
import com.mastaan.buyer.activities.MastaanToolBarActivity;

import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;

/**
 * Created by venkatesh on 25/3/16.
 */
public  class CheckError {

    Context context;
    MastaanToolBarActivity activity;

    boolean ignoreMaintenance;
    boolean ignoreUpgrade;

    public CheckError(Context context){
        this.context = context;
        try{    activity = (MastaanToolBarActivity) context;  }catch (Exception e){}
    }

    public CheckError ignoreMaintenance(){
        this.ignoreMaintenance = true;
        return this;
    }

    public CheckError ignoreUpgrade(){
        this.ignoreUpgrade = true;
        return this;
    }

    public boolean check(RetrofitError error){

        try {
            String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
            JSONObject jsonResponse = new JSONObject(response_json_string);
            boolean maintainance = false;
            try{    maintainance = jsonResponse.getBoolean("maintenance");  }catch (Exception e){}
            boolean upgrade = false;
            try{    upgrade = jsonResponse.getBoolean("upgrade");  }catch (Exception e){}

            if ( !ignoreMaintenance && maintainance ) {
                if ( activity != null ) {
                    Intent launchAct = new Intent(context, LaunchActivity.class);
                    ComponentName componentName = launchAct.getComponent();
                    Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                    activity.startActivity(mainIntent);
                }
                return true;
            }
            else if ( !ignoreUpgrade && upgrade) {
                if ( activity != null ){
                    activity.showChoiceSelectionDialog(false, "Update Available!", Html.fromHtml("This version is no longer compatible.<br>Please update to new version to continue."), "EXIT", "UPDATE", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("UPDATE")) {
                                CommonMethods.openInPlaystore(context, context.getPackageName());
                                activity.finishAffinity();
                                System.exit(0);
                            } else {
                                activity.finishAffinity();
                                System.exit(0);
                            }
                        }
                    });
                }
                return true;
            }
        }catch (Exception e){e.printStackTrace();}

        return false;
    }
}
