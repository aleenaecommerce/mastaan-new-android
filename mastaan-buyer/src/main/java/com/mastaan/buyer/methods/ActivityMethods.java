package com.mastaan.buyer.methods;

import android.app.Activity;

import com.mastaan.buyer.activities.HomeActivity;
import com.mastaan.buyer.activities.LocationSelectionActivity;

/**
 * Created by Venkatesh Uppu on 25/01/19.
 */

public class ActivityMethods {

    public static final boolean isLocationSelectionActivity(Activity activity){
        try{
            LocationSelectionActivity locationSelectionActivity = (LocationSelectionActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isHomeActivity(Activity activity){
        try{
            HomeActivity homeActivity = (HomeActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

}
