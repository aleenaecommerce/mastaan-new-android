package com.mastaan.buyer.methods;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.OrderDetails;

/**
 * Created by venkatesh on 4/8/15.
 */

public class BroadcastReceiversMethods {

    Context context;

    public BroadcastReceiversMethods(Context context) {
        this.context = context;

    }

    public BroadcastReceiversMethods updateUserSessionInfo(){
        updateUserSessionInfoInSidebarPage();
        displayUserSessionInfo();
        return this;
    }
    public BroadcastReceiversMethods updateUserSessionInfoInSidebarPage(){
        Intent sidebarReceiver = new Intent(Constants.SIDEBAR_RECEIVER);
        sidebarReceiver.putExtra(Constants.ACTION_TYPE, Constants.DISPLAY_USER_SESSION_INFO);
        context.sendBroadcast(sidebarReceiver);
        return this;
    }

    public BroadcastReceiversMethods displayMembershipDetailsInSidebar() {
        Intent sidebarReceiver = new Intent(Constants.SIDEBAR_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.DISPLAY_MEMBERSHIP_DETAILS);
        context.sendBroadcast(sidebarReceiver);
        return this;
    }

    public BroadcastReceiversMethods displayDeliveryLocationInSidebar() {
        Intent sidebarReceiver = new Intent(Constants.SIDEBAR_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.DISPLAY_DELIVERY_LOCATION);
        context.sendBroadcast(sidebarReceiver);
        return this;
    }

    public BroadcastReceiversMethods displayAddressBookListInAddressBookPage() {
        Intent locationSelectionReceiver = new Intent(Constants.LOCATION_SELECTION_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.DISPLAY_ADDRESS_BOOK);
        context.sendBroadcast(locationSelectionReceiver);
        return this;
    }

    public BroadcastReceiversMethods reloadOrderDetails(String orderID){
        Intent orderDetailsReceiver = new Intent(Constants.ORDER_DETAILS_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.RELOAD_DATA)
                .putExtra(Constants.ID, orderID);
        context.sendBroadcast(orderDetailsReceiver);
        return this;
    }

    public BroadcastReceiversMethods displayOrderStatus(String orderID, String orderStatus){
        Intent orderDetailsReceiver = new Intent(Constants.ORDER_DETAILS_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.DISPLAY_ORDER_STATUS)
                .putExtra(Constants.ID, orderID)
                .putExtra(Constants.STATUS, orderStatus);
        context.sendBroadcast(orderDetailsReceiver);
        return this;
    }

    public BroadcastReceiversMethods setupFirebaseForPendingOrder(OrderDetails orderDetails){
        Intent firebaseReceiver = new Intent(Constants.FIREBASE_SERVICE_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.ADD_NEW_PENDING_ORDER)
                .putExtra(Constants.ORDER_DETAILS, new Gson().toJson(orderDetails));
        context.sendBroadcast(firebaseReceiver);
        return this;
    }

    public BroadcastReceiversMethods displayUserSessionInfo(){
        Intent homeReceiver = new Intent(Constants.HOME_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.DISPLAY_USER_SESSION_INFO);
        context.sendBroadcast(homeReceiver);

        Intent meatItemsReceiver = new Intent(Constants.MEAT_ITEMS_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.DISPLAY_USER_SESSION_INFO);
        context.sendBroadcast(meatItemsReceiver);
        return this;
    }

    public BroadcastReceiversMethods checkCartStatus(){
        Intent homeReceiver = new Intent(Constants.HOME_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.CHECK_CART_STATUS);
        context.sendBroadcast(homeReceiver);

        Intent meatItemsReceiver = new Intent(Constants.MEAT_ITEMS_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.CHECK_CART_STATUS);
        context.sendBroadcast(meatItemsReceiver);

        return this;
    }

    public BroadcastReceiversMethods displayPendingFeedbacksInHome(){
        Intent homeReceiver = new Intent(Constants.HOME_RECEIVER)
                .putExtra(Constants.ACTION_TYPE, Constants.DISPLAY_PENDING_FEEDBACKS);
        context.sendBroadcast(homeReceiver);
        return this;
    }

}
