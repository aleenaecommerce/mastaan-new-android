package com.mastaan.buyer.models;

import com.mastaan.buyer.constants.Constants;

import java.io.Serializable;

public class BuyerDetails implements Serializable{

    String _id;     //  ID
    String m;       // Mobile Number
    String e;       //  Email Address
    String f;       //  First Name
    String l;       //  Last Name
    String d;       //  DateOfBirth
    boolean mr;     //  Matrial Status
    String a;       //  Anniversary Date if Married

    long lp;        //  Loyalty Points
    String r;       // Referral ID

    double amt;     //  Outstanding Amount

    boolean sb;     // SuperBuyer

    String mt;      // Membership Type
    double mdi;     // Membership Discount
    double mdcdi;    // Membership Delivery Charges Discount
    double modi;     // Membership Order Discount

    String is;      // Install Source Ex: Friend, News Paper etc.

    String fcm;     // FCM Registration id

    boolean ros;    // Rated on Playstore

    boolean palt;   // Promotional Alerts Preference

    String cd;
    String ud;

    
    public BuyerDetails(){}

    public BuyerDetails(String id, String name){
        this._id = id;
        this.f = name;
    }

    public BuyerDetails(String ID, String joinedDate, String mobileNumber, String name, String email, String dateOfBirth, boolean matrialStatus, String anniversaryDate, String referralID){
        this._id = ID;
        this.cd = joinedDate;
        this.m = mobileNumber;
        this.f = name;
        this.e = email;
        this.d = dateOfBirth;
        this.mr = matrialStatus;
        this.a = anniversaryDate;
        this.r = referralID;
    }

    public String getID() {    return _id;     }
    public String getMobile() {      return m;   }

    public String getEmail() {
        return e;
    }
    public void setEmail(String email){  this.e = email;   }

    public String getName() {
        return f;
    }
    public void setName(String name){  this.f = name;   }

    public String getDOB() {
        return d;
    }
    public void setDOB(String dob){  this.d = dob;   }

    public boolean getMatrialStatus() {
        return mr;
    }
    public void setMatrialStatus(boolean mr) {
        this.mr = mr;
    }

    public String getAnniversary() {
        return a;
    }
    public void setAnniversary(String anniversary){  this.a = anniversary;   }

    public long getLoyaltyPoints() {
        return lp;
    }

    public String getReferralID() {
        return r;
    }

    public boolean isSuperBuyer() {
        return sb;
    }

    public String getMembershipType() {
        if ( mt != null ){
            if ( mt.equalsIgnoreCase("sm") ){
                return Constants.SILVER_MEMBER;
            }else if ( mt.equalsIgnoreCase("gm") ){
                return Constants.GOLD_MEMBER;
            }else if ( mt.equalsIgnoreCase("dm") ){
                return Constants.DIAMOND_MEMBER;
            }else if ( mt.equalsIgnoreCase("pm") ){
                return Constants.PLATINUM_MEMBER;
            }
        }
        return Constants.BRONZE_MEMBER;
    }

    public double getMembershipDiscout() {
        return mdi;
    }

    public double getMembershipDeliveryChargesDiscount() {
        return mdcdi;
    }

    public double getMembershipOrderDiscount() {
        return modi;
    }

    public String getJoinedDate() {     return cd;      }

    public String getFCMRegistrationID() {
        return fcm;
    }

    public String getInstallSource() {
        if ( is == null ){  is = "";    }
        return is;
    }

    public boolean isRatedOnPlaystore() {
        return ros;
    }

    public boolean getPromotionalAlertsPreference() {
        return palt;
    }

    public double getOutstandingAmount() {
        return amt;
    }
}
