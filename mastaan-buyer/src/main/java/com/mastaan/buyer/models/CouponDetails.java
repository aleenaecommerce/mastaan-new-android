package com.mastaan.buyer.models;

/**
 * Created by Venkatesh Uppu on 16/3/16.
 */

public class CouponDetails {

    String _id;     //  ID
    String cc;      //  CouponCode

    String std;     // StartDate
    String exd;     // ExpiredDate

    double mo;      // Minimum order value
    double di;      // Discount value
    String dt;      // Discount value type v - value (Rs. 20), p - percentage (20%)
    double mdi;     // Maximum discount

    boolean pr;     // Pre-order
    boolean ft;     // First time order


    public String getID() {
        return _id;
    }

    public String getCouponCode() {
        return cc;
    }

    public String getStartDate() {
        return std;
    }

    public String getExpiryDate() {
        return exd;
    }

    public double getMinimumOrderValue() {
        return mo;
    }

    public double getDiscountValue() {
        return di;
    }

    public String getDiscountType() {
        return dt!=null?dt:"";
    }

    public double getMaximumDiscount() {
        return mdi;
    }

    public boolean isForPreOrders() {
        return pr;
    }

    public boolean isForFirstTimeOrder() {
        return ft;
    }
}
