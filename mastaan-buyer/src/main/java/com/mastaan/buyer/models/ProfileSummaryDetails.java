package com.mastaan.buyer.models;

/**
 * Created by venkatesh on 15/8/15.
 */

public class ProfileSummaryDetails {

    BuyerDetails user;
    Stats stats;

    class Stats{
        int count;                                      // Orders Count
        double total;                                   // Total
        double final_total;                             // Final Total

        double discount;                                // Discount
        double coupon_discount;                         // Coupon Discount
        double membership_discount;                     // Membership Total Discount
        double membership_delivery_discount;     // Membership Delivery Charges Discount
        double membership_order_discount;               // Membership Order Discount
    }

    public BuyerDetails getBuyerDetails() {
        return user;
    }

    public int getOrdersCount() {
        return stats!=null?stats.count:0;
    }

    public double getFinalTotal(){
        return stats!=null?stats.final_total:0;
    }

    public double getCouponDiscount(){
        return stats!=null?stats.coupon_discount:0;
    }

    public double getMembershipDiscount(){
        return stats!=null?stats.membership_discount:0;
    }

    public double getMembershipDeliveryDiscount(){
        return stats!=null?stats.membership_delivery_discount:0;
    }

    public double getMembershipOrderDiscount(){
        return stats!=null?stats.membership_order_discount:0;
    }

}
