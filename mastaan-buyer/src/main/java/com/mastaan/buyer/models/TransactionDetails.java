package com.mastaan.buyer.models;

import com.mastaan.buyer.constants.Constants;

/**
 * Created by venkatesh on 11/03/19.
 */

public class TransactionDetails {

    String _id;     // ID
    String cd;      // Created Date

    String ty;      // Type (o - Order, cb - Cashback, ref - Refund)

    double amt;     // Amount

    String cmt;     // Comments

    Order o;        // Linked Order

    double eb;      // Ending Balance

    class Order{
        String _id;
        String oid;
    }


    public String getID() {
        return _id;
    }

    public String getCreatedDate() {
        return cd;
    }

    public String getType() {
        return ty!=null?ty:"";
    }
    public String getTypeString(){
        String type = getType();
        if ( type.equalsIgnoreCase("o") ){
            return Constants.ORDER;
        }else if ( type.equalsIgnoreCase("cb") ){
            return Constants.CASHBACK;
        }else if ( type.equalsIgnoreCase("ref") ){
            return Constants.REFUND;
        }
        return type;
    }

    public double getAmount() {
        return amt;
    }

    public String getComments() {
        return cmt;
    }

    public String getOrderID() {
        return o!=null?o._id:null;
    }
    public String getOrderReadableID() {
        return o!=null?(o.oid!=null?o.oid:o._id):null;
    }

    public String getLink(){
        if ( o != null ){
            return o.oid!=null?o.oid:o._id;
        }
        return null;
    }

    public double getEndingBalance() {
        return eb;
    }

}
