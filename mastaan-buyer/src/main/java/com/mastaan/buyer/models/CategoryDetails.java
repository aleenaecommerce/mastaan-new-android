package com.mastaan.buyer.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 28/9/15.
 */

public class CategoryDetails {

    String _id;         //  CategoryID

    String c;           //  CategoryValue
    String n;           //  CategoryName
    String col1;        //  CategoryStatusBarColor
    String col2;        //  CategoryBackgroundColor
    String col3;        //  CategoryGradientColor
    String bg;          //  CategoryBackgroundPattern
    String ic;          //  CategoryIcon

    boolean en;         //  CategoryCurrentAvailableStatus
    String dt;          //
    String dm;          //

    List<CategoryDetails> s;     //  SubCategories
    List<CategoryDetails> p;     //  ParentCategories


    public CategoryDetails(){}

    public CategoryDetails(String id, String name, String imageURL, String iconURL, List<CategoryDetails> subCategories){
        this._id = id;
        this.c = id;
        this.n = name;
        this.ic = iconURL;
        this.bg = imageURL;
        this.s = subCategories;
    }

    public String getID() {
        return _id;
    }

    public String getValue() {
        return c;
    }

    public String getName() {
        return n;
    }

    public String getStatusBarColor() {
        return col1!=null?col1:getBackgroundColor();
    }

    public String getBackgroundColor() {
        return col2!=null?col2:"#D1D1D1";
    }

    public String getGradientColor() {
        return col3!=null?col3:getStatusBarColor();
    }

    public String getPatternURL() {
        return bg;
    }

    public String getIconURL() {
        return ic;
    }

    public int getSubCategoriesCount() {
        if ( s != null ) {
            return s.size();
        }else{
            return 0;
        }
    }

    public List<CategoryDetails> getSubCategories() {
        if ( s == null ){   return new ArrayList<>();   }
        return s;
    }

    public String getParentCategoryName() {
        return (p == null || p.size() == 0) ? "" : p.get(0).getName();
    }

    public boolean getStatus(){
        return en;
    }

    public String getDisabledTitle() {
        return dt;
    }

    public String getDisabledMessage() {
        return dm;
    }
}
