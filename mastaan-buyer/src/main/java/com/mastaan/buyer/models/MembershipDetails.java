package com.mastaan.buyer.models;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.constants.Constants;

/**
 * Created by Venkatesh Uppu on 10/01/19.
 */

public class MembershipDetails {

    String _id;

    boolean st;         // Status

    String ty;          // Type

    Double bo_min;      // Buyer total orders minimum amount
    Double bo_max;      // Buyer total orders maximum amount

    Double o_di;        // order - discount percent
    Double o_min;       // order - minimum  amount
    Double o_max_di;    // order - maximum discount

    Double dc_di;       // delivery charge - discount percent
    Double dc_o_min;    // delivery charge - order - minimum amount

    String col1;        // Background Color
    String col2;        // Text Color


    public MembershipDetails(boolean status, String type, double buyerTotalOrdersMinimumAmount, double buyerTotalOrdersMaximumAmount, double orderDiscount, double orderMinimumAmount, double orderMaximumDiscount, double deliveryChargesDiscount, double deliveryChargesOrderMinimumAmount){
        this.st = status;
        this.ty = type;
        this.bo_min = buyerTotalOrdersMinimumAmount;
        this.bo_max = buyerTotalOrdersMaximumAmount;
        this.o_di = orderDiscount;
        this.o_min = orderMinimumAmount;
        this.o_max_di = orderMaximumDiscount;
        this.dc_di = deliveryChargesDiscount;
        this.dc_o_min = deliveryChargesOrderMinimumAmount;
    }

    public MembershipDetails setID(String id) {
        this._id = id;
        return this;
    }
    public String getID() {
        return _id;
    }

    public boolean getStatus() {
        return st;
    }

    public String getType() {
        return ty!=null?ty:"";
    }

    public String getTypeName() {
        if ( ty != null && ty.length() > 0 ){
            if ( ty.equalsIgnoreCase("bm") ){
                return Constants.BRONZE_MEMBER;
            }else if ( ty.equalsIgnoreCase("sm") ){
                return Constants.SILVER_MEMBER;
            }else if ( ty.equalsIgnoreCase("gm") ){
                return Constants.GOLD_MEMBER;
            }else if ( ty.equalsIgnoreCase("dm") ){
                return Constants.DIAMOND_MEMBER;
            }else if ( ty.equalsIgnoreCase("pm") ){
                return Constants.PLATINUM_MEMBER;
            }
        }
        return "-";
    }

    public String getBackgroundColor() {
        if ( col1 != null && col1.trim().length() > 0 ){
            if ( col1.charAt(0) != '#' ){
                col1 = "#"+col1;
            }
            return col1;
        }
        return "#ffffff";
    }
    public String getTextColor(){
        if ( col2 != null && col2.trim().length() > 0 ){
            if ( col2.charAt(0) != '#' ){
                col2 = "#"+col2;
            }
            return col2;
        }
        return "#000000";
    }

    public Double getBuyerTotalOrdersMinimumAmount() {
        return bo_min;
    }
    public Double getBuyerTotalOrdersMaximumAmount() {
        return bo_max;
    }

    public Double getOrderDiscount() {
        return o_di;
    }

    public Double getOrderMinimumAmount() {
        return o_min;
    }

    public Double getOrderMaximumDiscount() {
        return o_max_di;
    }

    public Double getDeliveryChargesDiscount() {
        return dc_di;
    }

    public Double getDeliveryChargesOrderMinimumAmount() {
        return dc_o_min;
    }


    public String getFormattedDetails(){
        String detailsString = "";

        if ( getBuyerTotalOrdersMinimumAmount() > 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + "<b>Eligibility</b><br>";

            if ( getBuyerTotalOrdersMaximumAmount() > 0 ){
                detailsString += "Total ordered value between <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getBuyerTotalOrdersMinimumAmount())+"</b>"
                        + " and " + "<b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getBuyerTotalOrdersMaximumAmount())+"</b>";
            }else{
                detailsString += "Total ordered value exceeds <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getBuyerTotalOrdersMinimumAmount())+"</b>";
            }
        }

        if ( getDeliveryChargesDiscount() > 0 || getOrderDiscount() > 0 ){
            detailsString += (detailsString.length()>0?"<br><br>":"") + "<b>Benefits</b><br>";

            if ( getDeliveryChargesDiscount() > 0 ){
                detailsString += "<b>#</b> Delivery Charges: <b>"+CommonMethods.getInDecimalFormat(getDeliveryChargesDiscount())+"%"+"</b> off"
                        + (getDeliveryChargesOrderMinimumAmount()>0?" (min order: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getDeliveryChargesOrderMinimumAmount())+"</b>)":"")
                ;
            }

            if ( getOrderDiscount() > 0 ){
                detailsString += (getDeliveryChargesDiscount()>0?"<br>":"") + "<b>#</b> Discount: <b>"+CommonMethods.getInDecimalFormat(getOrderDiscount())+"%"+"</b> off";
                if ( getOrderMinimumAmount() > 0 || getOrderMaximumDiscount() > 0 ){
                    detailsString += " ("
                            + (getOrderMinimumAmount()>0?"min order: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getOrderMinimumAmount())+"</b>":"")
                            + (getOrderMaximumDiscount()>0?(getOrderMinimumAmount()>0?", ":"")+"max discount: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getOrderMaximumDiscount())+"</b>":"")
                            + ")";
                }
            }
        }

        return detailsString;
    }

}
