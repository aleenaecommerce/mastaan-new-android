package com.mastaan.buyer.models;

import com.aleena.common.methods.CommonMethods;

/**
 * Created by Venkatesh Uppu on 14/7/18.
 */

public class ReferralOfferDetails {

    String std;     // StartDate
    String exd;     // ExpiredDate

    double rmo;      // Receiver Minimum order value
    double rdi;      // Receiver Discount value
    String rdt;      // Receiver Discount value type v - value (Rs. 20), p - percentage (20%)
    double rmdi;     // Receiver Maximum discount

    double smo;      // Sender Minimum order value
    double sdi;      // Sender Discount value
    String sdt;      // Sender Discount value type v - value (Rs. 20), p - percentage (20%)
    double smdi;     // Sender Maximum discount


    public String getStartDate() {
        return std;
    }

    public String getExpiryDate() {
        return exd;
    }

    public double getReceiverMinimumOrder() {
        return rmo;
    }

    public double getReceiverDiscountValue() {
        return rdi;
    }

    public String getReceiverDiscountValueType() {
        return rdt!=null?rdt:"";
    }

    public double getReceiverMaximumDiscount() {
        return rmdi;
    }


    public double getSenderMinimumOrder() {
        return smo;
    }

    public double getSenderDiscountValue() {
        return sdi;
    }

    public String getSenderDiscountValueType() {
        return sdt!=null?sdt:"";
    }

    public double getSenderMaximumDiscount() {
        return smdi;
    }


    public String getReceiverOffer() {
        return getReceiverDiscountValueType().equalsIgnoreCase("p")
                ?CommonMethods.getInDecimalFormat(getReceiverDiscountValue())+"% off"+(getReceiverMaximumDiscount()>0?" (max Rs."+CommonMethods.getIndianFormatNumber(getReceiverMaximumDiscount())+")":"")
                :
                "Rs."+CommonMethods.getIndianFormatNumber(getReceiverDiscountValue())+" off";
    }
    public String getSenderOffer() {
        return getSenderDiscountValueType().equalsIgnoreCase("p")
                ?CommonMethods.getInDecimalFormat(getSenderDiscountValue())+"% off"+(getSenderMaximumDiscount()>0?" (max Rs."+CommonMethods.getIndianFormatNumber(getSenderMaximumDiscount())+")":"")
                :
                "Rs."+CommonMethods.getIndianFormatNumber(getSenderDiscountValue())+" off";
    }

    public String getFormatterReferralOfferDetails(){
        String receiverOffer = getReceiverOffer();
        String senderOffer = getSenderOffer();

        if ( getReceiverMinimumOrder() == getSenderMinimumOrder() ){
            return "Refer a friend & we'll give you "+senderOffer+" & your buddy "+receiverOffer+" on your next orders above Rs."+CommonMethods.getIndianFormatNumber(getReceiverMinimumOrder());
        }
        else{
            return "Refer a friend & we'll give you "+senderOffer+" on min order of Rs."+CommonMethods.getIndianFormatNumber(getSenderMinimumOrder())+" & your buddy "+receiverOffer+" on min order of Rs."+CommonMethods.getIndianFormatNumber(getReceiverMinimumOrder());
        }
    }

}
