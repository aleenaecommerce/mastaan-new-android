package com.mastaan.buyer.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 25/8/16.
 */
public class GroupedOrders {
    String date;
    List<OrderDetails> ordersList;

    public GroupedOrders(String date, List<OrderDetails> ordersList){
        this.date = date;
        this.ordersList = ordersList;
    }

    public String getDate() {
        if ( date == null ){    date = "";  }
        return date;
    }

    public void setOrdersList(List<OrderDetails> ordersList) {
        this.ordersList = ordersList;
    }

    public void addCartItem(OrderDetails orderDetails) {
        if ( ordersList == null ){    ordersList = new ArrayList<>();  }
        this.ordersList.add(orderDetails);
    }

    public List<OrderDetails> getOrdersList() {
        if ( ordersList == null ){   ordersList = new ArrayList<>();  }
        return ordersList;
    }
}
