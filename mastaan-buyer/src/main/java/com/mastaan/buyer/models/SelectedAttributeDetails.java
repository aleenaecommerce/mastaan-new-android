package com.mastaan.buyer.models;

/**
 * Created by aleena on 17/10/15.
 */
public class SelectedAttributeDetails {
    String at;      //  AttributeID
    String n;       //  Attribute Name
    String opt;     //  OptionID
    String _id;     // OptionAccessID

    public SelectedAttributeDetails(String attributeID, String attributeOptionID){
        this.at = attributeID;
        this.opt = attributeOptionID;
    }

    public String getAttributeID() {
        return at;
    }

    public String getOptinID() {
        return opt;
    }

    public String getOptionAccessID() {
        return _id;
    }

    public String getName() {
        return n;
    }
}
