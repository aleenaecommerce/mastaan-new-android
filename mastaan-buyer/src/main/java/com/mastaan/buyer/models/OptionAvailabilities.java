package com.mastaan.buyer.models;

import java.util.List;

/**
 * Created by Venkatesh on 19/10/16.
 */
public class OptionAvailabilities {
    String opt;
    List<AttributeOptionAvailabilityDetails> avl;


    public String getAttributeOptionID() {
        return opt;
    }

    public List<AttributeOptionAvailabilityDetails> getAvailabilitiesList() {
        return avl;
    }
}
