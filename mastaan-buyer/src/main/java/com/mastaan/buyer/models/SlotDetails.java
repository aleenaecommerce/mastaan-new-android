package com.mastaan.buyer.models;

import com.aleena.common.methods.DateMethods;

/**
 * Created by Venkatesh Uppu on 01/02/16.
 */

public class SlotDetails {

    String _id;

    public String displayText;

    public int sh;
    public int sm;
    public int eh;
    public int em;

    boolean en;

    long mnum;          // Maximum orders count
    long inum;          // Orders count

    public SlotDetails(int fromHour, int fromMinutes, int toHour, int toMinutes){
        this.sh = fromHour;
        this.sm = fromMinutes;
        this.eh = toHour;
        this.em = toMinutes;
    }

    public String getID() {
        return _id;
    }

    public String getStartTime() {
        return String.format("%02d", sh)+":"+String.format("%02d", sm);
    }

    public String getEndTime() {
        return String.format("%02d", eh)+":"+String.format("%02d", em);
    }

    public String getDisplayText() {
        if ( displayText == null ){
//            return DateMethods.getTimeIn12HrFormat(getEndTime());
            return DateMethods.getTimeIn12HrFormat(getStartTime())+" to "+DateMethods.getTimeIn12HrFormat(getEndTime());
            //return DateMethods.getTimeIn12HrFormat(fromHour + ":" + fromMinutes)+" to "+DateMethods.getTimeIn12HrFormat(toHour + ":" + toMinutes);
        }
        return displayText;
    }

    public long getMaximumAllowedOrdersCount() {
        return mnum;
    }

    public long getOrdersCount() {
        return inum;
    }

}
