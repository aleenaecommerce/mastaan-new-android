package com.mastaan.buyer.models;

import com.aleena.common.models.AddressBookItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 15/10/15.
 */

public class BootStrapDetails {

    String token;

    String serverTime;
    String msg;

    boolean upgrade;
    boolean compulsoryUpgrade;
    int latestVersion;

    boolean open;
    String closedMsg;

    String servingAt;
    String supportContactNumber;

    BuyerDetails user;

    int cartItems;              // No of Items in Cart
    AddressBookItemDetails cartAddress;     //  CartDeliveryAddress

    List<Double> meatItemWeights;
    List<Double> meatItemSets;
    List<Double> meatItemPieces;

    List<OrderDetails> pendingOrders;     //  Pending Orders

    List<AddressBookItemDetails> addressBook;

    String paymentURL;
    String outstandingAmountPaymentURL;
    String paymentCheckURL;

    List<String> installSources;

//    String paytmMerchantID;
//    String paytmChecksumURL;
//    String paytmChecksumVerificationURL;

    DeliveringHours deliveringHours;

    class DeliveringHours{
        String fromHour;
        String fromMinutes;
        String toHour;
        String toMinutes;

        public String getStartTime() {
            return fromHour+":"+fromMinutes;
        }

        public String getEndTime() {
            return toHour+":"+toMinutes;
        }
    }


    public String getAccessToken() {
        return token;
    }

    public boolean isUpgradeAvailable() {
        return upgrade;
    }

    public boolean isCompulsoryUpgrade() {
        return compulsoryUpgrade;
    }

    public int getLatestVersion() {
        return latestVersion;
    }

    public boolean isOpen() {
        return open;
    }

    public String getClosedMessage() {
        return closedMsg;
    }

    public String getServingAt() {
        return servingAt;
    }



    public String getServerTime() {
        return serverTime;
    }

    public String getAlertMessage() {
        return msg;
    }

    public String getSupportContactNumber() {
        return supportContactNumber;
    }

    //--- USER ----//

    public BuyerDetails getBuyerDetails() {
        return user;
    }

    public int getCartItemsCount() {
        return cartItems;
    }

    public AddressBookItemDetails getCartDeliveryAddress() {
        return cartAddress;
    }

    public List<Double> getMeatItemWeights() {
        return meatItemWeights;
    }

    public List<Double> getMeatItemPieces() {
        return meatItemPieces;
    }

    public List<Double> getMeatItemSets() {
        return meatItemSets;
    }

    public List<OrderDetails> getPendingOrders() {
        return pendingOrders;
    }

    public List<AddressBookItemDetails> getAddressBookList() {
        return addressBook;
    }

    public String getPaymentGatewayURL() {
        return paymentURL;
    }

    public String getOutstandingPaymentGatewayURL(){
        return outstandingAmountPaymentURL;
    }

    public String getPaymentCheckURL() {
        return paymentCheckURL;
    }

    public List<String> getInstallSources() {
        if ( installSources == null ){  installSources = new ArrayList<>(); }
        return installSources;
    }

    public String getDeliveringHoursStartTime(){
        if ( deliveringHours != null && deliveringHours.getStartTime() != null ){    return deliveringHours.getStartTime();   }
        return "";
    }

    public String getDeliveringHoursEndTime(){
        if ( deliveringHours != null && deliveringHours.getEndTime() != null ){    return deliveringHours.getEndTime(); }
        return "";
    }

//    public String getPaytmMerchantID() {
//        return paytmMerchantID;
//    }
//
//    public String getPaytmChecksumURL() {
//        return paytmChecksumURL;
//    }
//
//    public String getPaytmChecksumVerificationURL() {
//        return paytmChecksumVerificationURL;
//    }
}
