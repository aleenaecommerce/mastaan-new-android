package com.mastaan.buyer.models;

import com.mastaan.buyer.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 14/10/15.
 */

public class OrderItemDetails {

    String _id;

    String h;               //  HashValue

    MeatItemDetails i;      //  Meat Item Details
    double ta;              //  Total Amount including Attributes price
    double ita;             //  Initial Total Amount
    float n;               //  Quantity

    String f;               // FeedbackID;

    int status;
    String sr;          //  Status Reason

    boolean csbyr;

    List<SelectedAttributeDetails> a;

    public String si;              // Special Instructions


    public String getID() {
        return _id;
    }

    public String getHash() {
        return h;
    }

    public void setHash(String hash) {
        this.h = hash;
    }

    public MeatItemDetails getMeatItemDetails() {
        return i;
    }

    public double getTotal() {
        return ta;
    }
    public void setTotal(double total) {
        this.ta = total;
    }

    public double getInitialTotalAmount() {
        return ita;
    }

    public float getQuantity() {
        return n;
    }

    public void setWeight(float weight) {
        this.n = weight;
    }

    public List<SelectedAttributeDetails> getSelectedAttributes() {
        if ( a == null ){
            a = new ArrayList<>();
        }
        return a;
    }

    public int getStatus() {
        return status;
    }

    public String getStatusString() {
        if ( status == 0 ){
            return Constants.ORDERED;
        }else if(status == 7) {
            return Constants.ACKNOWLEDGED;
        }else if(status == 1) {
            return Constants.PROCESSING;
        }else if(status == 2){
            return Constants.PROCESSED;
        }else if(status == 8) {
            return Constants.ASSIGNED;
        }else if(status == 9) {
            return Constants.PICKED_UP;
        }else if(status == 3) {
            return Constants.DELIVERING;
        }else if(status == 4) {
            return Constants.DELIVERED;
        }else if(status == 5) {
            return Constants.REJECTED;
        }else if(status == 6) {
            return Constants.FAILED;
        }
        return "";
    }

    public String getStatusReason() {
        if ( sr == null ){  sr = "";    }
        return sr;
    }

    public boolean isAssignedToCustomerSupport() {
        return csbyr;
    }

    public void setSelectedAttributes(List<SelectedAttributeDetails> selectedAttributes) {
        this.a = selectedAttributes;
    }

    public String getFeedbackID() {
        return f;
    }

    public boolean isSubmittedFeedback(){
        if ( f != null && f.length() > 0 ){
            return true;
        }
        return false;
    }

    public String getSpecialInstructions() {
        return si;
    }

}
