package com.mastaan.buyer.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 12/10/15.
 */

public class MeatItemDetails {

    String _id;     //  ID
    List<String> n; //  MultiLingual Names
    List<String> enn; //  English Names
    String p;       //  Image URL
    String t;       //  Thumbnail URL
    String d;       // Details
    String nut;     // Nutritions
    String rec;     // Recipes
    double pr;      //  Buying Price
    double opr;     //  Selling Price
    double mw;      //   Minimum weight to add to cart

    double pt;      //  Preparation Time
    String qt;      //  Quantity Type
    boolean adn;     // Is AddOn

    String size;

    boolean avl;    //  Availability

    boolean po;     // Pre order
    String pot;     //  Pre order cut off time

    List<CategoryDetails> c;

    List<AttributeDetails> ma;      //     Available Attributes


    public String getID() {
        return _id;
    }

    public boolean getAvailability() {
        return avl;
    }

    public boolean isPreOrderable() {
        return po;
    }

    public String getPreOrderCutoffTime() {
        return pot;
    }

    public double getPreparationHours() {
        return pt;
    }

    public String getQuantityType() {
        return qt;
    }
    public String getQuantityUnit() {
        if ( qt != null ){
            if ( qt.equalsIgnoreCase("w") ){
                return "kg";
            }else if ( qt.equalsIgnoreCase("n") ){
                return "piece";
            }else if ( qt.equalsIgnoreCase("s") ){
                return "set";
            }else{
                return qt;
            }
        }
        return "";
    }

    public boolean isAddOn() {
        return adn;
    }

    public String getName() {
        if ( n != null && n.size() > 0 ){
            return n.get(0);
        }else{
            return "";
        }
    }

    public String getNameWithCategory() {
        CategoryDetails categoryDetails = getCategoryDetails();
        if ( categoryDetails != null
                && (categoryDetails.getName().toLowerCase().contains("goat") || categoryDetails.getName().toLowerCase().contains("sheep")) ){
            return categoryDetails.getName()+" " + getName().replaceAll(categoryDetails.getName(), "");
        }
        return getName();
    }
    public String getNameWithCategoryAndSize(){
        return getNameWithCategory() + (size!=null && size.trim().length()>0?" ("+size+")":"");
    }
    public String getNameWithSize(){
        return getName() + (size!=null && size.trim().length()>0?" ("+size+")":"");
    }

    public List<String> getMultiLingualNamesList() {
        if ( n == null ){ n = new ArrayList<>();    }
        return n;
    }

    public List<String> getEnglishNamesList() {
        if ( enn == null ){ enn = new ArrayList<>();    }
        return enn;
    }

    public String getCombinedEnglishNamesString() {
        if ( enn != null && enn.size() > 0 ){
            String concatedEnglishNamesString = "";
            for (int x=0;x<enn.size();x++){
                if ( concatedEnglishNamesString.length() == 0 ) {
                    concatedEnglishNamesString += enn.get(x);
                }else{
                    concatedEnglishNamesString += ", "+enn.get(x);
                }
            }
            return concatedEnglishNamesString;
        }
        return getName();
    }

    public String getImageURL() {
        return "http:"+p;
    }

    public String getThumbnailImageURL() {
        return "http:"+t;
    }

    public String getDetails() {
        return d;
    }

    public String getNutritionsImage() {
        if ( nut != null && nut.trim().length() > 0 ){
            return "http:"+nut;
        }
        return null;
    }

    public String getRecipes() {
        return rec;
    }

    public String getSize() {
        return size;
    }

    public double getBuyingPrice() {
        return pr;
    }

    public double getSellingPrice() {
        return opr;
    }

    public double getMinimumWeightToAddToCart() {
        return mw;
    }

    public void setAttributes(List<AttributeDetails> attributesList) {
        this.ma = attributesList;
    }

    public List<AttributeDetails> getAttributes() {
        if ( ma != null ){  return ma;  }
        return new ArrayList<>();
    }

    public List<CategoryDetails> getCategoriesList() {
        if ( c == null ){ c = new ArrayList<>();    }
        return c;
    }

    public CategoryDetails getCategoryDetails(){
        if ( getCategoriesList().size() > 0 ){
            return getCategoriesList().get(0);
        }
        return null;
    }

    public String getCategoryID(){
        if ( getCategoriesList().size() > 0 ){
            return getCategoriesList().get(0).getID();
        }
        return "";
    }

    public String getCategoryValue(){
        if ( getCategoriesList().size() > 0 ){
            return getCategoriesList().get(0).getValue();
        }
        return "";
    }

    public void setAttributeOptionsAvailabilities(List<OptionAvailabilities> optionsList){
        for (int i=0;i<ma.size();i++){
            ma.get(i).setAttributeOptionsAvailabilities(optionsList);
        }
    }

}
