package com.mastaan.buyer.models;

/**
 * Created by Venkatesh Uppu on 19/02/18.
 */

public class MessageDetails {

    String type;

    boolean status;

    String title;
    String details;

    /*String color;*/

    String image;

    String url;
    String app_url;

    String action;

    String start_date;
    String end_date;

    boolean show_once;


    public String getType() {
        if ( type == null ){  type = "";    }
        return type;
    }

    public boolean getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public String getDetails() {
        return details;
    }

    /*public String getColor() {
        return color!=null?color:"#ffffff";
    }*/

    public String getImage() {
        return image;
    }

    public String getURL() {
        return url;
    }
    public String getFormattedURL() {
        if ( url != null && url.trim().length() > 0 ){
            if ( url.contains("http://") == false && url.contains("https://") == false ){
                url = "https://"+url.trim();
            }
        }
        return url;
    }

    public String getAppURL() {
        return app_url;
    }

    public String getAction() {
        return action;
    }

    public String getStartDate() {
        return start_date;
    }

    public String getEndDate() {
        return end_date;
    }

    public boolean showOnce() {
        return show_once;
    }

    //----

    public boolean isValidMessage(){
        if ( (title != null && title.trim().length() > 0) || (details != null && details.trim().length() > 0) ){
            return true;
        }else if ( image != null && image.trim().length() > 0 ){
            return true;
        }
        return false;
    }

}
