package com.mastaan.buyer.models;

/**
 * Created by Venkatesh Uppu on 07/09/18.
 */

public class NotificationDetails {

    String id;
    String type;

    String title;
    String details;
    String image;
    String icon;
    String action;
    String url;

    public NotificationDetails(){}

    public NotificationDetails(String id, String type, String title, String details, String image, String icon, String action, String url){
        this.id = id;
        this.type = type;
        this.title = title;
        this.details = details;
        this.image = image;
        this.icon = icon;
        this.action = action;
        this.url = url;
    }

    public String getID() {
        return id!=null?id:"";
    }
    public NotificationDetails setID(String id) {
        this.id = id;
        return this;
    }

    public String getType() {
        return type;
    }
    public NotificationDetails setType(String type) {
        this.type = type;
        return this;
    }

    public String getTitle() {
        return title;
    }
    public NotificationDetails setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDetails() {
        return details;
    }
    public NotificationDetails setDetails(String details) {
        this.details = details;
        return this;
    }


    public String getImage() {
        return image;
    }
    public NotificationDetails setImage(String image) {
        this.image = image;
        return this;
    }

    public String getIcon() {
        return icon;
    }
    public NotificationDetails setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    public String getAction() {
        return action;
    }
    public NotificationDetails setAction(String action) {
        this.action = action;
        return this;
    }

    public String getURL() {
        return url;
    }
    public String getFormattedURL() {
        if ( url != null && url.length() > 0 ){
            String formattedURL = url.trim();
            if ( formattedURL.toLowerCase().contains("http://") == false && formattedURL.toLowerCase().contains("https://") == false ){
                formattedURL = "http://"+formattedURL;
            }
            return formattedURL;
        }
        return null;
    }
    public NotificationDetails setURL(String url) {
        this.url = url;
        return this;
    }

}
