package com.mastaan.buyer.models;

/**
 * Created by venkatesh on 5/10/15.
 */

public class OrderItemFeedback {
    String o;   //  OrderItemID
    String m;   //  meatID
    float r;   //  Rating
    String c;   //  Comments

    public OrderItemFeedback(String orderItemID, String meatItemID, float rating, String comments){
        this.o = orderItemID;
        this.m = meatItemID;
        this.r = rating;
        this.c = comments;
    }

    public String getOrderItemID() {
        return o!=null?o:"";
    }

    public float getRating() {
        return r;
    }

    public void setComments(String comments) {
        this.c = comments;
    }
    public String getComments() {
        return c;
    }

}
