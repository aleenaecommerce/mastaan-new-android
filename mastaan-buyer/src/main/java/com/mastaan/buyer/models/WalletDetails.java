package com.mastaan.buyer.models;

/**
 * Created by venkatesh on 10/03/19.
 */

public class WalletDetails {

    String _id;         //  ID
    double ba;          //  Available Balance


    public String getID() {
        return _id;
    }

    public double getBalance() {
        return ba;
    }

}
