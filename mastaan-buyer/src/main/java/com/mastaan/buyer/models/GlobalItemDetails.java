package com.mastaan.buyer.models;

/**
 * Created by Venkatesh Uppu on 25/8/16.
 */
public class GlobalItemDetails {
    DayDetails dayDetails;
    WarehouseMeatItemDetails warehouseMeatItemDetails;
    CartItemDetails cartItemDetails;

    MessageDetails messageDetails;


    public GlobalItemDetails setDayDetails(DayDetails dayDetails) {
        this.dayDetails = dayDetails;
        return this;
    }
    public DayDetails getDayDetails() {
        return dayDetails;
    }

    public GlobalItemDetails setWarehouseMeatItemDetails(WarehouseMeatItemDetails warehouseMeatItemDetails) {
        this.warehouseMeatItemDetails = warehouseMeatItemDetails;
        return this;
    }
    public WarehouseMeatItemDetails getWarehouseMeatItemDetails() {
        return warehouseMeatItemDetails;
    }

    public GlobalItemDetails setCartItemDetails(CartItemDetails cartItemDetails) {
        this.cartItemDetails = cartItemDetails;
        return this;
    }
    public CartItemDetails getCartItemDetails() {
        return cartItemDetails;
    }


    public GlobalItemDetails setMessageDetails(MessageDetails messageDetails) {
        this.messageDetails = messageDetails;
        return this;
    }
    public MessageDetails getMessageDetails() {
        return messageDetails;
    }
}
