package com.mastaan.buyer.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by venkatesh on 15/10/15.
 */
public class AttributeOptionDetails {
    String _id;
    String v;       //  Option ID
    String n;       //  Option Name
    String ic;      //  ImageURL
    double pd;      //  Price Difference
    double apd;     //  Actual Price Difference
    double wd;      // Weight Difference (In Percentage)

    List<String> da;    //  Disabled Attributes
    List<String> dao;   // Disabled AttributeOptions

    boolean en;     // Enabled Status
    String ar;      //  Availability Reason

    //RUNTIME

    List<AttributeOptionAvailabilityDetails> avl;

    String date;            // Date
    boolean status = true;      // Dummy To Handle Enabling/Disabling
    String disabledTag;

    public AttributeOptionDetails(AttributeOptionDetails attributeOptionDetails){
        this._id = attributeOptionDetails._id;
        this.v = attributeOptionDetails.v;
        this.n = attributeOptionDetails.n;
        this.ic = attributeOptionDetails.ic;
        this.pd = attributeOptionDetails.pd;
        this.apd = attributeOptionDetails.apd;
        this.wd = attributeOptionDetails.wd;

        this.da = attributeOptionDetails.da;
        this.dao = attributeOptionDetails.dao;

        this.en = attributeOptionDetails.en;
        this.ar = attributeOptionDetails.ar;
    }

    public AttributeOptionDetails(String ID, String value, String name){
        this._id = ID;
        this.v = value;
        this.n = name;
        this.en = true;
        this.status = true;
    }

    public AttributeOptionDetails(String ID, String name, String image, double price, boolean status){
        this._id = ID;
        this.n = name;
        this.ic = image;
        this.pd = price;
        this.apd = price;
        this.en = status;
        this.status = status;
    }

    public AttributeOptionDetails(AvailabilityDetails availabilityDetails, String serverTime){
        this._id = availabilityDetails.getID();

        String todayDate = DateMethods.getOnlyDate(serverTime);
        //String tomorrowDate = DateMethods.addToDateInDays(todayDate, 1);
        if ( DateMethods.compareDates(todayDate, availabilityDetails.getDate()) == 0 ){
            this.n = "Today ("+DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD)+")";
        }
//        else if ( DateMethods.compareDates(tomorrowDate, availabilityDetails.getDate()) == 0 ){
//            this.n = "Tomorrow ("+DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD)+")";
//        }
        else{
            this.n = DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD);
        }

        this.pd = availabilityDetails.getSellingPrice();
        this.apd = availabilityDetails.getActualSellingPrice();
        this.en = availabilityDetails.isAvailable();
        this.ar = availabilityDetails.getAvailabilityReason();

        this.date = DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD_YYYY);
        this.status = availabilityDetails.isAvailable();
        this.disabledTag = "Tap for info";
    }

    public String getID() {
        return _id;
    }

    public String getValue() {
        return v;
    }

    public String getName() {
        return n;
    }

    public void setPriceDifference(double priceDifference) {
        this.pd = priceDifference;
    }

    public double getPriceDifference() {
        return pd;
    }

    public double getActualPriceDifference() {
        return apd;
    }

    public double getWeightDifferencePercentage() {
        return wd;
    }

    public String getImageURL() {
        return "http:"+ic;
    }

    public List<String> getDisabledAttributes() {
        if ( da == null ){  da = new ArrayList<>(); }
        return da;
    }

    public List<String> getDisabledAttributeOptions() {
        if ( dao == null ){  dao = new ArrayList<>(); }
        return dao;
    }

    public void setEnablity(boolean en) {
        this.en = en;
    }

    public boolean isEnabled() {
        return en;
    }

    public void setAvailabilityReason(String ar) {
        this.ar = ar;
    }

    public String getAvailabilityReason() {
        if ( ar == null ){  return "";  }
        return ar;
    }


    public void setAvailabilitiesList(List<AttributeOptionAvailabilityDetails> availabilitiesList) {
        if ( availabilitiesList != null && availabilitiesList.size() > 0 ){
            Collections.sort(availabilitiesList, new Comparator<AttributeOptionAvailabilityDetails>() {
                public int compare(AttributeOptionAvailabilityDetails item1, AttributeOptionAvailabilityDetails item2) {
                    return DateMethods.compareDates(item1.getDate(), item2.getDate());
                }
            });
        }
        this.avl = availabilitiesList;
    }

    public AttributeOptionDetails getOptionDetailsForDate(String date){
        AttributeOptionDetails optionDetailsForDate = new AttributeOptionDetails(this);
        optionDetailsForDate.setStatus(true);

        if ( avl != null && avl.size() > 0 ){
            for (int i = avl.size()-1; i>=0; i--){
                int dateComparisionValue = DateMethods.compareDates(date, avl.get(i).getDate());
                if ( dateComparisionValue >= 0 ){
                    if ( optionDetailsForDate.isEnabled() ){
                        optionDetailsForDate.setPriceDifference(avl.get(i).getPriceDifference());
                        //Setting Availability if dates match
                        if ( dateComparisionValue == 0 ){
                            optionDetailsForDate.setEnablity(avl.get(i).isAvailable());
                            optionDetailsForDate.setAvailabilityReason(avl.get(i).getAvailabilityReason());
                        }
                    }
                    break;
                }
            }
        }
        return optionDetailsForDate;
    }

    public String getDate() {
        if ( date != null ){    return date;    }
        return "";
    }

    public boolean isAvailable(){
        if ( status && en ){
            return true;
        }
        return false;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDisabledTag() {
        return disabledTag;
    }
}
