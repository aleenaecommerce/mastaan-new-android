package com.mastaan.buyer.models;

import com.aleena.common.methods.DateMethods;

/**
 * Created by Venkatesh Uppu on 27/02/19.
 */

public class FeedbackDetails {
    String _id;
    String cd;

    //OrderItemDetails o;

    float r;
    String c;

    //UserDetailsMini prb;
    //String pmc;

    //boolean p;


    public String getID() {
        return _id;
    }

    public String getCreatedDate() {
        return DateMethods.getReadableDateFromUTC(cd);
    }

    public float getRating() {
        return r;
    }

    public String getComments() {
        if ( c == null ){   return  "";  }
        return c.trim();
    }

}
