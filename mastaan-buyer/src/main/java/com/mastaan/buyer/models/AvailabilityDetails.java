package com.mastaan.buyer.models;

/**
 * Created by Venkatesh Uppu on 24/8/16.
 */
public class AvailabilityDetails {
    String _id;
    String dstr;
    boolean a;
    double pr;      // Vendor Price
    double opr;     // Selling Price After Discount
    double apr;     // Actual Selling Price Before Discount
    String ar;

    public AvailabilityDetails(String id, String date, boolean availability, double sellingPrice){
        this(id, date, availability, sellingPrice, sellingPrice);
    }
    public AvailabilityDetails(String id, String date, boolean availability, double sellingPrice, double actualSellingPrice){
        this._id = id;
        this.dstr = date;
        this.a = availability;
        this.opr = sellingPrice;
        this.apr = actualSellingPrice;
    }

    public String getID() {
        return _id;
    }

    public String getDate() {
        return dstr;
    }

    public void setAvailability(boolean availability) {
        this.a = availability;
    }

    public boolean isAvailable() {
        return a;
    }

    public double getBuyingPrice() {
        return pr;
    }

    public double getSellingPrice() {
        return opr;
    }

    public double getActualSellingPrice() {
        if ( apr <= 0 ){    return  opr;    }
        return apr;
    }

    public void setAvailabilityReason(String availabilityReason) {
        this.ar = availabilityReason;
    }

    public String getAvailabilityReason() {
        if ( ar != null ){  return ar;  }
        return "";
    }

}
