package com.mastaan.buyer.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 14/10/15.
 */

public class CartItemDetails {

    String _id;
    String b;

    String h;                       //  HashValue

    SlotDetails wcs;                     //  Slot
    String dsy;                     //  Delivery start by
    String dby;                     //  Delivery complete by

    WarehouseMeatItemDetails i;      //  Meat Item Details
    double ta;                       //  Total Amount including Attributes price

    double n;                        //  Weight

    List<SelectedAttributeDetails> a;

    String si;                      //  Special Instructions


    public String getID() {
        return _id;
    }

    public String getHash() {
        return h;
    }

    public void setHash(String hash) {
        this.h = hash;
    }

    public String getSlotID() {
        return wcs!=null?wcs.getID():"";
    }

    public void setDeliveryStartByDate(String dsy) {
        this.dsy = dsy;
    }
    public String getDeliveryStartByDate() {
        if ( dsy == null ){ return "";  }
        return dsy;
    }

    public void setDeliveryByDate(String dby) {
        this.dby = dby;
    }
    public String getDeliveryByDate() {
        if ( dby == null ){ return "";  }
        return dby;
    }

    public String getFormattedDeliveryDate(){
        if ( dsy != null && dsy.trim().length() > 0 ){
            return DateMethods.getReadableDateFromUTC(dby, DateConstants.DD_MMM)
                    +", "+DateMethods.getReadableDateFromUTC(dsy, DateConstants.HH_MM_A)+" to "+DateMethods.getReadableDateFromUTC(dby, DateConstants.HH_MM_A);
        }
        return DateMethods.getReadableDateFromUTC(dby, DateConstants.DD_MMM)+", "+DateMethods.getReadableDateFromUTC(dby, DateConstants.HH_MM_A);
    }

    public WarehouseMeatItemDetails getWarehouseMeatItemDetails() {
        return i;
    }

    public double getTotal() {
        return ta;
    }

    public void setTotal(double total) {
        this.ta = total;
    }

    public double getWeight() {
        return n;
    }

    public String getWeightString() {
        try {
            String weightString = CommonMethods.getInDecimalFormat(getWeight()) + " " + getWarehouseMeatItemDetails().getMeatItemDetais().getQuantityUnit();
            if ( getWeight() != 1 ){    weightString += "s";    }
            return weightString;
        }catch (Exception e){}
        return n+"";
    }

    public void setWeight(double weight) {
        this.n = weight;
    }

    public List<SelectedAttributeDetails> getSelectedAttributes() {
        if ( a == null ){
            a = new ArrayList<>();
        }
        return a;
    }

    public void setSelectedAttributes(List<SelectedAttributeDetails> selectedAttributes) {
        this.a = selectedAttributes;
    }

    public String getSpecialInstructions() {
        if ( si != null && si.trim().length() > 0 ){
            return si.trim();
        }
        return null;
    }
    public void setSpecialInstructions(String specialInstructions) {
        this.si = specialInstructions;
    }

}
