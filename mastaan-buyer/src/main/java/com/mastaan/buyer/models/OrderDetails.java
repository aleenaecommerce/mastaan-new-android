package com.mastaan.buyer.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.buyer.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 14/10/15.
 */
public class OrderDetails {

    String _id;      // OrderAccessID
    String oid;      //  Order ID
    String gid;      //  Group Order ID if Present
    String cd;       // Created Date

    double ita;      // Initial Total Amount
    double ta;       // Total Amount
    double caa;      // Total Cash Amount
    double wa;       // Wallet Amount
    double iwa;      // Initial Wallet Amount
    double opa;      // Total OnlinePayment Amount
    double delfee;   // Delivery Charges
    double svchg;    // Service Charges
    double suchg;    // Surcharges
    double govtax;   // Govt Taxes
    double ref;      // Refunded Amount
    double di;       // Discount
    double mdi;      // Membership Total Discount
    double mdcdi;    // Membership Delivery Charges Discount
    double modi;     // Membership Order Discount
    double cba;      // Cashback
    CouponDetails cc;//  CouponCode

    int pt;          // Payment Type

    String dsy;       // Delivery start by
    String dby;       // Delivery complete by
    double mpt;         // Temp Value

    String os;       // Order Status

    String n;        // User Name
    String m;        // User Mobile
    String am;        // User Alternate Mobile
    String e;        // User Email
    String si;       //  Special Instructions

    String ad;       // AddressBookItemID
    String da;       // Full Address
    String pos;      // LatLng
    String lm;       // Landmark
    String p;        //  Premise
    String s1;       //  SubLocality-1
    String s2;       //  SubLocality-2
    String r;        //  Road No
    String l;        //  Locality
    String s;        //  State
    String c;        //  Country
     String pin;     // PinCode

    String mfd;      // MovedFeedbackDate

    List<OrderItemDetails> i;        // Ordered Meat Items.

    boolean pf;      // Given Feedback


    public String getID() {
        return _id;
    }

    public String getOrderID() {
        return oid;
    }

    public String getGroupOrderID() {
        return gid;
    }

    public void setOrderStaus(String orderStaus) {
        this.os = orderStaus;
    }

    public String getStatus() {
        return os;
    }

    public String getCreatedDate() {
        if ( cd != null ){
            return DateMethods.getReadableDateFromUTC(cd);
        }
        return cd;
    }

    public String getDeliveryDateInISTFormat() {
        if ( dby != null ){
            return DateMethods.getReadableDateFromUTC(dby);
        }
        return dby;
    }

    public String getDeliveryDate() {
        if ( dby != null ){     return dby; }
        return "";
    }

    public String getFormattedDeliveryDate(){
        if ( dsy != null && dsy.trim().length() > 0 ){
            return DateMethods.getReadableDateFromUTC(dby, DateConstants.MMM_DD_YYYY)
                    +", "+DateMethods.getReadableDateFromUTC(dsy, DateConstants.HH_MM_A)+" to "+DateMethods.getReadableDateFromUTC(dby, DateConstants.HH_MM_A);
        }
        return DateMethods.getReadableDateFromUTC(dby, DateConstants.MMM_DD_YYYY_HH_MM_A);
    }

    //----

    public double getMaxCartPreparationTime() {
        return mpt;
    }

    public List<OrderItemDetails> getOrderItems() {
        if ( i == null ){ return new ArrayList<>(); }
        return i;
    }

    public String getOrderItemsNames(){
        String itemsNames = "";
        if ( i != null && i.size() > 0 ){
            for (int j = 0; j < i.size(); j++) {
                if ( i.get(j) != null ) {
                    itemsNames += (itemsNames.length() > 0 ? ", " : "") + CommonMethods.capitalizeFirstLetter(i.get(j).getMeatItemDetails().getNameWithCategoryAndSize());
                }
            }
        }
        return itemsNames;
    }


    //-----

//    public List<OrderItemDetails> getOrderItemsExcludingFailed() {
//        List<OrderItemDetails> orderItems = new ArrayList<>();
//        if ( i != null && i.size() > 0 ){
//            for (int a=0;a<i.size();a++){
//                if ( i != null && i.get(a).getStatus() != 5 && i.get(a).getStatus() != 6 ){
//                    orderItems.add(i.get(a));
//                }
//            }
//        }
//        return orderItems;
//    }

    public List<OrderItemDetails> getOrderItemsExcludingSubmittedFeedbackItemsAndFailedItems() {
        List<OrderItemDetails> orderItems = new ArrayList<>();
        if ( i != null && i.size() > 0 ){
            for (int a=0;a<i.size();a++){
                if ( i != null && i.get(a).isSubmittedFeedback() == false && i.get(a).getStatus() != 5 && i.get(a).getStatus() != 6 ){
                    orderItems.add(i.get(a));
                }
            }
        }
        return orderItems;
    }

    public String getMovedFeedbackDate() {
        return mfd;
    }

    public boolean isAlreadyRemindedFeedbackForLater(){
        if ( mfd != null && mfd.length() > 0 ){
            return true;
        }
        return false;
    }

    //------

    public double getInitialTotalAmount() {
        return ita;
    }

    public double getTotalAmount() {
        return ta;
    }
    public double getSubTotal() {
        return (ta+di+mdi-delfee-svchg-suchg-govtax);
    }

    public double getDeliveryCharges() {
        return delfee;
    }

    public double getSurcharges() {
        return suchg;
    }

    public double getServiceCharges() {
        return svchg;
    }

    public double getGovernmentTaxes() {
        return govtax;
    }

    public double getCODAmount() {
        return caa;
    }

    public double getWalletAmount() {
        return wa;
    }
    public double getInitialWalletAmount() {
        return iwa;
    }

    public double getOnlinePaymentAmount() {
        return opa;
    }

    public double getRefundedAmount() {
        return ref;
    }

    public double getCouponDiscount(){  return di;  }

    public double getCouponCashback(){  return cba;  }

    public double getMembershipDiscount() {
        return mdi;
    }

    public double getMembershipDeliveryDiscount() {
        return mdcdi;
    }

    public double getMembershipOrderDiscount() {
        return modi;
    }

    public String getPaymentString(){
        String paymentModeString = "";
        if (wa > 0||iwa>0) {
            paymentModeString += "Wallet"+(opa>0||caa>0?" (" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(wa) + ")":"");
            if ( iwa > 0 && iwa != wa ){
                paymentModeString += " [Initial: "+SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(iwa)+"]";
            }
        }
        if (opa > 0) {
            if ( paymentModeString.length() > 0 ){   paymentModeString += "\n";  }
            paymentModeString += "Online payment"+(wa>0||caa>0?" (" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(opa) + ")":"");

        }
        if (caa > 0) {
            if ( paymentModeString.length() > 0 ){   paymentModeString += "\n";  }
            paymentModeString += "Cash on delivery"+(wa>0||opa>0?" (" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(caa) + ")":"");
        }

        return paymentModeString;
    }

    public String getPaymentType() {
        if ( pt == 1 ){
            return Constants.CASH_ON_DELIVERY;
        }else if ( pt == 2 ){
            return Constants.CREDIT_CARD;
        }else if ( pt == 3 ){
            return Constants.DEBIT_CARD;
        }else if ( pt == 4 ){
            return Constants.NET_BANKING;
        }else if ( pt == 6 || pt == 7  || pt == 8 ){
            return Constants.ONLINE_PAYMENT;
        }
        return "";
    }

    public String getCouponcode() {
        if ( cc != null ){
            return cc.getCouponCode();
        }
        return "";
    }

    //---

    public String getCustomerName() {
        return n;
    }

    public String getCustomerMobile() {
        return m;
    }

    public String getCustomerAlternateMobile() {
        return am;
    }

    public String getCustomerEmail() {
        return e;
    }

    public String getFormattedContactDetails(){
        String contactDetails = m;
        if ( am != null && am.trim().length() > 0 ){
            contactDetails += " (Primary)\n"+am+" (Alternate)";
        }
        if ( e != null && e.trim().length() > 0 ){
            contactDetails += "\n"+e.trim();
        }
        return contactDetails;
    }

    public String getSpecialInstructions() {
        return si;
    }

    //----

    public String getDeliveryAddressID() {
        if ( ad == null ){ ad = "DUMMY_ID"; }
        return ad;
    }

    public String getDeliveryAddressLatLngString() {
        LatLng latLng = getDeliveryAddressLatLng();
        return latLng.latitude + "," + latLng.longitude;
    }

    public LatLng getDeliveryAddressLatLng() {
        return LatLngMethods.getLatLngFromString(pos);
    }

    public String getDeliveryAddressPremise() {
        return p;
    }

    public String getDeliveryAddress() {
        return da;
    }

    public String getDeliveryAddressSubLocalityLevel1() {
        return s1;
    }

    public String getDeliveryAddressSubLocalityLevel2() {
        return s2;
    }

    public String getDeliveryAddressRoadNo() {
        return r;
    }

    public String getDeliveryAddressLandmark() {
        return lm;
    }

    public String getDeliveryAddressLocality() {
        return l;
    }

    public String getDeliveryAddressState() {
        return s;
    }

    public String getDeliveryAddressCountry() {
        return c;
    }

    public String getDeliveryAddressPinCode() {
        return pin;
    }

    public boolean isGivenFeedback() {
        return pf;
    }
}
