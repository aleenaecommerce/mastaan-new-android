package com.mastaan.buyer.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.Response;
import com.mastaan.buyer.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 19/07/18.
 */

public class WarehouseMeatItemStockDetails {

    String _id;         // ID

    String ty;          // Stock Type ('ul' - Unlimited, 'l' - Limited, 'd' - daily )
    double qu;          // In Kgs or Units or Pieces
    List<String> ad;    // Available Days ('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat')


    public String getID() {
        return _id;
    }

    public String getType() {
        if ( ty == null ){  ty = "";    }
        return ty;
    }

    public double getQuantity() {
        return qu;
    }

    public List<String> getAvailableDays() {
        if ( ad == null ){  ad = new ArrayList<>(); }
        return ad;
    }

    //--------

    public Response<String> isAvailableForDate(String date, double minWeightToAddToCart){
        try{
            if ( getType().equalsIgnoreCase("l") ){
                // If Limited stock - No Stock
                if ( getQuantity() <= 0 || getQuantity() < minWeightToAddToCart ) {
                    return new Response(false, -1, "We are very sorry. Unfortunately we just ran out of stock.");
                    //availabilityDetails.setAvailability(false);
                    //availabilityDetails.setAvailabilityReason("Out of stock");
                }
            }
            // If Daywise stock
            else if ( getType().equalsIgnoreCase("d") ){
                String weekday = DateMethods.getWeekDayFromDate(date).toLowerCase().substring(0, 3);
                // If Available week day
                if ( getAvailableDays().contains(weekday) ){
                    // If no stock
                    if ( getQuantity() <= 0 || getQuantity() < minWeightToAddToCart ) {
                        return new Response(false, -1, "We are very sorry. Unfortunately we just ran out of stock.");
                        //availabilityDetails.setAvailability(false);
                        //availabilityDetails.setAvailabilityReason("Out of stock");
                    }
                }
                // If Unavailable week day
                else{
                    return new Response(false, -1, "Item is not available on selected date (only available on "+ CommonMethods.capitalizeStringWords(CommonMethods.getStringFromStringList(getAvailableDays()))+")");
                    //availabilityDetails.setAvailability(false);
                    //availabilityDetails.setAvailabilityReason("Not available");
                }
            }
        }catch (Exception e){e.printStackTrace();}
        return new Response(true, 200, "Success");
    }

}
