package com.mastaan.buyer.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 15/10/15.
 */
public class AttributeDetails {

    String _id;     //  Attribute ID
    String n;       //  Attribute Name
    boolean ila;        // Item Level Attribute
    List<AttributeOptionDetails> op;        //  Available Options

    String opt;

    public AttributeDetails(String id, String name, boolean isItemLevelAttribue, List<AttributeOptionDetails> options, String optionsDisplayType){
        this._id = id;
        this.n = name;
        this.ila = isItemLevelAttribue;
        this.op = options;
        this.opt = optionsDisplayType;
    }

    public String getID() {
        return _id;
    }

    public String getName() {
        return n;
    }

    public boolean isItemLevelAttribute() {
        return ila;
    }

    public List<AttributeOptionDetails> getAvailableOptions() {
        if ( op != null ){  return op;  }
        return op;
    }

    public void addOption(AttributeOptionDetails attributeOptionDetails){
        if ( op == null ){  op = new ArrayList<>(); }
        op.add(attributeOptionDetails);
    }

    public void setOptionsDisplayType(String opt) {
        this.opt = opt;
    }

    public String getOptionsDisplayType() {
        return opt;
    }

    public void setAttributeOptionsAvailabilities(List<OptionAvailabilities> availabilitiesList){

        if ( op != null && op.size() > 0 ){
            for (int i=0;i<op.size();i++){
                for (int j=0;j<availabilitiesList.size();j++){
                    if ( op.get(i).getID().equals(availabilitiesList.get(j).getAttributeOptionID()) ){
                        op.get(i).setAvailabilitiesList(availabilitiesList.get(j).getAvailabilitiesList());
                    }
                }
            }
        }

    }
}
