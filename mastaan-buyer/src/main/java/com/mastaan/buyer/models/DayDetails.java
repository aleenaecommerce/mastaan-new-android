package com.mastaan.buyer.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 8/6/16.
 */
public class DayDetails {
    String date;
    String msg;
    long count;
    double amount;
    List<CategoryDetails> c;

    //Runtime
    String formatted_date;

    public DayDetails(String date, String message, long count, double amount){
        this.date = date;
        this.msg = message;
        this.count = count;
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public DayDetails setFormattedDate(String formatted_date) {
        this.formatted_date = formatted_date;
        return this;
    }
    public String getFormattedDate() {
        return formatted_date!=null&&formatted_date.trim().length()>0?formatted_date:date;
    }

    public String getMessage() {
        return msg;
    }

    public long getCount() {
        return count;
    }

    public double getAmount() {
        return amount;
    }

    public List<CategoryDetails> getCategories() {
        if ( c != null ){   return c;   }
        return new ArrayList<>();
    }
}
