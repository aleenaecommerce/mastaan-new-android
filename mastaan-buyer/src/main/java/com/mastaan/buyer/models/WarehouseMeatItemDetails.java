package com.mastaan.buyer.models;

import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by venkatesh on 12/10/15.
 */

public class WarehouseMeatItemDetails {

    String _id;     // Warehouse Meat Item ID
    String wh;      //
    double pr;      //  Buying Price
    double opr;     //  Selling Price
    MeatItemDetails i;     //  Meat Items Details
    List<AvailabilityDetails> avl;      //  Availability Details
    boolean a;      //  Available or not
    String avst;    // Availbility start
    String hlt;
    String so;


    public String getID() {
        return _id;
    }

    public String getMeatItemID() {
        return i.getID();
    }

    public String getCategoryID(){
        return i.getCategoryID();
    }

    public double getBuyingPrice() {
        return pr;
    }

    public double getSellingPrice() {
        return opr;
    }

    public double getSellingPriceForDate(String date) {
        AvailabilityDetails availabilityDetails = getAvailabilityDetailsForDate(date);
        if ( availabilityDetails != null ){
            availabilityDetails.getSellingPrice();
        }
        return opr;
    }

    public AvailabilityDetails getAvailabilityDetailsForDate(String date){
        if ( avl == null ){ avl = new ArrayList<>();    }
        // SORTING GROUPS BY DATE
        Collections.sort(avl, new Comparator<AvailabilityDetails>() {
            public int compare(AvailabilityDetails item1, AvailabilityDetails item2) {
                return DateMethods.compareDates(item1.getDate(), item2.getDate());
            }
        });

        for (int i=avl.size()-1;i>=0;i--){
            //Log.d(Constants.LOG_TAG, i+" Date = "+availabilities.get(i).getDate());
            if ( DateMethods.compareDates(date, avl.get(i).getDate()) >= 0 ){
                //Log.d(Constants.LOG_TAG, "Prev of "+date+" is "+availabilities.get(i).getDate());
                return avl.get(i);
            }
        }
        return new AvailabilityDetails("", date, true, opr, opr);
    }

    public MeatItemDetails getMeatItemDetais() {
        if ( i!= null ){    return i;}
        return new MeatItemDetails();
    }

    public boolean isAvailable() {
        return a;
    }

    public String getHighlightText() {
        return hlt;
    }

    public String getSource() {
        return so;
    }

    public List<AvailabilityDetails> getAvailabilitiesList() {
        if ( avl == null ){     avl = new ArrayList<>();    }
        return avl;
    }

    public List<AvailabilityDetails> getFullAvailabilitiesList(String startDate, int daysToCount, List<DayDetails> disabledDates, WarehouseMeatItemStockDetails stockDetails, String serverTime, String deliveringHoursEndTime) {
        startDate = DateMethods.getOnlyDate(startDate);
//        if ( getMeatItemDetais() != null && getMeatItemDetais().isPreOrderable() ){
//            startDate = DateMethods.addToDateInDays(startDate, 1);
//        }
        double minWeightToAddToCart = getMeatItemDetais().getMinimumWeightToAddToCart();

        if ( startDate != null && startDate.length() > 0 ) {
            if (avl != null) {
                List<AvailabilityDetails> availabilitiesList = new ArrayList<>();
                for (int x = 0; x < daysToCount; x++) {
                    String date = DateMethods.addToDateInDays(startDate, x);
                    AvailabilityDetails availabilityDetails = getAvaialbilityDetailsOfDate(date);
                    if ( availabilityDetails == null ){
                        if ( availabilitiesList.size() > 0 ){
                            availabilityDetails = new AvailabilityDetails("", date, true, availabilitiesList.get(availabilitiesList.size()-1).getSellingPrice(), availabilitiesList.get(availabilitiesList.size()-1).getActualSellingPrice());
                        }else{
                            if ( x == 0 ) {
                                AvailabilityDetails tempAvailDetails = getAvaialbilityDetailsOfLatestPreviousDate(date);
                                if ( tempAvailDetails != null ) {
                                    availabilityDetails = new AvailabilityDetails("", date, true, tempAvailDetails.getSellingPrice(), tempAvailDetails.getActualSellingPrice());
                                }else{
                                    availabilityDetails = new AvailabilityDetails("", date, true, opr, opr);
                                }
                            }else{
                                availabilityDetails = new AvailabilityDetails("", date, true, opr, opr);
                            }
                        }
                    }

                    // Checking for disabled days
                    if ( disabledDates != null && disabledDates.size() > 0 ){
                        for (int i=0;i<disabledDates.size();i++){
                            DayDetails dayDetails = disabledDates.get(i);
                            if ( DateMethods.compareDates(availabilityDetails.getDate(), dayDetails.getDate()) == 0 ){
                                if ( dayDetails.getCategories().size() == 0 ) {
                                    availabilityDetails.setAvailability(false);
                                    availabilityDetails.setAvailabilityReason(disabledDates.get(i).getMessage());
                                }else{
                                    for (int s=0;s<dayDetails.getCategories().size();s++){
                                        if ( getMeatItemDetais().getCategoryID().equals(dayDetails.getCategories().get(s).getID()) ){
                                            availabilityDetails.setAvailability(false);
                                            availabilityDetails.setAvailabilityReason(disabledDates.get(i).getMessage());
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Checking for pre order
                    if ( x == 0 && getMeatItemDetais() != null && getMeatItemDetais().isPreOrderable() ){
                        availabilityDetails.setAvailability(false);
                        availabilityDetails.setAvailabilityReason("Unavailable because the item is available only on pre-order basis");
                    }

                    // Checking for stock if available on selected date
                    if ( availabilityDetails.isAvailable() && stockDetails != null ){
                        if ( stockDetails.getType().equalsIgnoreCase("l") ){
                            // If Limited stock - No Stock
                            if ( stockDetails.getQuantity() <= 0 || stockDetails.getQuantity() < minWeightToAddToCart ) {
                                availabilityDetails.setAvailability(false);
                                availabilityDetails.setAvailabilityReason("Out of stock");
                            }
                        }
                        // If Daywise stock
                        else if ( stockDetails.getType().equalsIgnoreCase("d") ){
                            String weekday = DateMethods.getWeekDayFromDate(availabilityDetails.getDate()).toLowerCase().substring(0, 3);
                            // If Available week day
                            if ( stockDetails.getAvailableDays().contains(weekday) ){
                                // If no stock
                                if ( stockDetails.getQuantity() <= 0 || stockDetails.getQuantity() < minWeightToAddToCart ) {
                                    availabilityDetails.setAvailability(false);
                                    availabilityDetails.setAvailabilityReason("Out of stock");
                                }
                            }
                            // If Unavailable week day
                            else{
                                availabilityDetails.setAvailability(false);
                                availabilityDetails.setAvailabilityReason("Not available");
                            }
                        }
                    }

                    //-----------

                    if ( availabilitiesList != null ){
                        if ( x == 0 ) {
                            // ADDING TODAYS DATE ONLY IF SLOTS AVAILABLE
                            if (DateMethods.compareDates(deliveringHoursEndTime, DateMethods.getTimeIn24HrFormat(serverTime)) >= 0) {
                                availabilitiesList.add(availabilityDetails);
                            }
                        }else{
                            // ADDING FUTURE DATES DIRECTLY
                            availabilitiesList.add(availabilityDetails);
                        }
                    }
                }
                return availabilitiesList;
            }
        }
        return new ArrayList<>();
    }

    private AvailabilityDetails getAvaialbilityDetailsOfDate(String date){
        List<AvailabilityDetails> availabilities = getAvailabilitiesList();
        for (int i=0;i<availabilities.size();i++){
            if ( availabilities.get(i) != null &&
                    DateMethods.compareDates(availabilities.get(i).getDate(), date) == 0 ){
                return availabilities.get(i);
            }
        }
        return null;
    }

    private AvailabilityDetails getAvaialbilityDetailsOfLatestPreviousDate(String date){
        List<AvailabilityDetails> availabilities = getAvailabilitiesList();
        // SORTING GROUPS BY DATE
        Collections.sort(availabilities, new Comparator<AvailabilityDetails>() {
            public int compare(AvailabilityDetails item1, AvailabilityDetails item2) {
                return DateMethods.compareDates(item1.getDate(), item2.getDate());
            }
        });

        for (int i=availabilities.size()-1;i>=0;i--){
            //Log.d(Constants.LOG_TAG, i+" Date = "+availabilities.get(i).getDate());
            if ( DateMethods.compareDates(date, availabilities.get(i).getDate()) >= 0 ){
                //Log.d(Constants.LOG_TAG, "Prev of "+date+" is "+availabilities.get(i).getDate());
                return availabilities.get(i);
            }
        }
        return null;
    }

    public boolean isAvailableOnDate(String date, List<DayDetails> disabledDates){

        boolean isDisabledDate = false;
        if ( disabledDates != null && disabledDates.size() > 0 ){
            for (int i=0;i<disabledDates.size();i++){
                if ( DateMethods.compareDates(disabledDates.get(i).getDate(), date) == 0 ){
                    isDisabledDate = true;
                    break;
                }
            }
        }

        if ( isDisabledDate ){
            return false;
        }else {
            AvailabilityDetails availabilityDetails = getAvaialbilityDetailsOfDate(date);
            if (availabilityDetails == null) {
                return true;
            } else {
                return availabilityDetails.isAvailable();
            }
        }
    }

}
