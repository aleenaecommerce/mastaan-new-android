package com.mastaan.buyer.models;

/**
 * Created by venkatesh on 16/3/16.
 */
public class CartTotalDetails {

    String msg;                     // Message
    float ta;                       // Total Amount
    String cc;                      // CouponCode
    float ccd;                      // CouponCode Discount
    float cccb;                      // CouponCode Cashback
    float mdi;                      // Membership Total Discount
    float mdcdi;                    // Membership Delivery Charges Discount
    float modi;                     // Membership Order Discount
    float deliveryCharges;          //
    float serviceCharges;           //
    float surCharges;
    float govtTaxes;                //

    String deliveryChargesMessage;
    String totalMessage;


//    public CartTotalDetails(String message, float cartTotal, float cartGovtTaxes, float cartProcessingFees, float cartCouponDiscount, String couponCode, List<String> couponEnabledDays) {
//        this.msg = message;
//        this.ta = cartTotal;
//        this.govtTaxes = cartGovtTaxes;
//        this.serviceCharges = cartProcessingFees;
//        this.ccd = cartCouponDiscount;
//        this.cc = couponCode;
////        this.ed = couponEnabledDays;
//
//    }

    public String getMessage() {
        return msg;
    }

    public float getTotal() {
        return ta;
    }

    public float getSubTotal() {
        return (ta + ccd + modi - deliveryCharges - serviceCharges - surCharges - govtTaxes);
    }

    public String getCouponCode() {
        return cc;
    }

    public float getCouponDiscount() {
        return ccd;
    }

    public float getCouponCashback() {
        return cccb;
    }

    public float getMembershipDiscount() {
        return mdi;
    }

    public float getDeliveryCharges() {
        return deliveryCharges;
    }

    public float getServiceCharges() {
        return serviceCharges;
    }

    public float getSurCharges() {
        return surCharges;
    }

    public float getGovtTaxes() {
        return govtTaxes;
    }

    public String getDeliveryChargesMessage() {
        return deliveryChargesMessage;
    }

    public String getTotalMessage() {
        return totalMessage;
    }

}
