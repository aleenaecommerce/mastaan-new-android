package com.mastaan.buyer.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 25/8/16.
 */
public class GroupedCartItems {
    String date;
    List<CartItemDetails> cartItems;

    //Runtime
    String formatted_date;


    public GroupedCartItems(String date, List<CartItemDetails> cartItems){
        this.date = date;
        this.cartItems = cartItems;
    }

    public String getDate() {
        if ( date == null ){    date = "";  }
        return date;
    }

    public GroupedCartItems setFormattedDate(String formatted_date) {
        this.formatted_date = formatted_date;
        return this;
    }
    public String getFormattedDate() {
        return formatted_date!=null&&formatted_date.trim().length()>0?formatted_date:date;
    }

    public void setCartItems(List<CartItemDetails> cartItems) {
        this.cartItems = cartItems;
    }

    public void addCartItem(CartItemDetails cartItemDetails) {
        if ( cartItems == null ){    cartItems = new ArrayList<>();  }
        this.cartItems.add(cartItemDetails);
    }

    public List<CartItemDetails> getCartItems() {
        if ( cartItems == null ){   cartItems = new ArrayList<>();  }
        return cartItems;
    }

    public int getItemsCount(){
        return getCartItems().size();
    }

    public double getTotalAmount(){
        double totalAmount = 0;
        List<CartItemDetails> cartItems = getCartItems();
        for (int i=0;i<cartItems.size();i++){
            if ( cartItems.get(i) != null ){
                totalAmount += cartItems.get(i).getTotal();
            }
        }
        return totalAmount;
    }

}
