package com.mastaan.buyer.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.services.vService;
import com.mastaan.buyer.MastaanApplication;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.dialogs.LaunchMessageDialog;
import com.mastaan.buyer.flows.AskInstallSourceFlow;
import com.mastaan.buyer.flows.RateOnPlaystoreFlow;
import com.mastaan.buyer.interfaces.MessagesCallback;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.MessageDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 19/02/18.
 */

public class MastaanService extends vService {

    String TAG = "MASTAAN_SERVICE : ";

    Context context;

    LocalStorageData localStorageData;
    MastaanApplication mastaanApplication;


    public MastaanService() {}

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(Constants.LOG_TAG, TAG+"Created");

        context = getApplicationContext();

        localStorageData = new LocalStorageData(getApplicationContext());

        //registerServiceReceiver(Constants.MASTAAN_SERVICE_RECEIVER);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.LOG_TAG, TAG+"Started");

        try{
            // IF APP IS LAUNCHED
            if ( getMastaanApplication().getCurrentActivity() != null ){
                // ASKING RATE ON PLAYSTORE
                StatusCallback askRateOnStoreCallback = new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        // If not clicked rate on play store then continue asking install source and showing alert message
                        if ( status == false ){
                            // ASKING ABOUT INSTALL SOURCE
                            new AskInstallSourceFlow(getMastaanApplication().getCurrentActivity()).check(new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                    // CHECKING ALERT MESSAGE
                                    getMastaanApplication().getAlertMessages("launchMessage", new MessagesCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message, List<MessageDetails> messagesList) {
                                            try{
                                                if ( status && messagesList.get(0) != null ){
                                                    new LaunchMessageDialog(getMastaanApplication().getCurrentActivity()).show(messagesList.get(0));
                                                }
                                            }catch (Exception e){}
                                        }
                                    });
                                }
                            });
                        }
                    }
                };

                if ( getMastaanApplication().isSubmittedHighRatedFeedback() ){
                    new RateOnPlaystoreFlow(getMastaanApplication().getCurrentActivity()).check(askRateOnStoreCallback);
                }else{
                    askRateOnStoreCallback.onComplete(false, -1, "");
                }

            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(Constants.LOG_TAG, TAG+"Destroyed");
    }

    //--------

    public MastaanApplication getMastaanApplication() {
        if ( mastaanApplication == null){
            mastaanApplication = (MastaanApplication) getApplication();       // Accessing Application Class;
        }
        return mastaanApplication;
    }

}


/*@Override
    public void onServiceReceiverMessage(String serviceReceiverName, Intent receivedData) {
        super.onServiceReceiverMessage(serviceReceiverName, receivedData);

        try{
            String actionType = receivedData.getStringExtra(Constants.ACTION_TYPE);

            if ( actionType.equalsIgnoreCase(Constants.ASK_ABOUT_INSTALL_SOURCE) ){
                askAboutInstallSource();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }*/

    /*public void askAboutInstallSource(final StatusCallback callback){

        new AsyncTask<Void, Void, Void>() {
            boolean ask = false;
            List<String> installSources = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    BuyerDetails buyerDetails = localStorageData.getBuyerDetails();
                    if ( localStorageData.getSessionFlag() && (buyerDetails != null && buyerDetails.getID() != null && buyerDetails.getID().length() > 0 )
                            && ( buyerDetails.getInstallSource() == null || buyerDetails.getInstallSource().trim().length() == 0 ) ){
                        ask = true;
                        installSources = localStorageData.getInstallSources();
                    }
                }catch (Exception e){}
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                try{
                    installSources.add("Others");
                    if ( ask ){
                        getMastaanApplication().getCurrentActivity().showListChooserDialog(getString(R.string.app_type).equalsIgnoreCase("internal")?true:false, "How did you hear about Mastaan?", installSources, -1, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                try{
                                    if ( installSources.get(position).equalsIgnoreCase("Others") ){
                                        View dialogView = LayoutInflater.from(getMastaanApplication().getCurrentActivity()).inflate(R.layout.dialog_install_source, null);
                                        final vTextInputLayout comments = (vTextInputLayout) dialogView.findViewById(R.id.comments);
                                        dialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                String eComments = comments.getText().trim();
                                                comments.checkError("* Enter comments");

                                                if ( eComments.length() == 0 ) {
                                                    return;
                                                }
                                                if ( eComments.length() < 4 ){
                                                    comments.showError("* Input must be at least 4 characters");
                                                    return;
                                                }
                                                if ( eComments.matches(".*[a-zA-Z]+.*") == false ){
                                                    comments.showError("* Invalid input");
                                                    return;
                                                }

                                                callback.onComplete(true, 200, "Success");
                                                continueWithAskInstallSource("Others", eComments);
                                                getMastaanApplication().getCurrentActivity().closeCustomDialog();
                                            }
                                        });
                                        getMastaanApplication().getCurrentActivity().showCustomDialog(dialogView, false);
                                    }
                                    else{
                                        callback.onComplete(true, 200, "Success");
                                        continueWithAskInstallSource(installSources.get(position), "");
                                    }
                                }catch (Exception e){}
                            }
                        });
                    }else{
                        callback.onComplete(true, 200, "Success");
                    }
                }catch (Exception e){
                    callback.onComplete(true, 200, "Success");
                }
            }
            private void continueWithAskInstallSource(final String installSource, String installSourceDetails){
                try{
                    getMastaanApplication().getCurrentActivity().getBackendAPIs().getProfileAPI().updateInstallSource(new RequestUpdateInstallSource(installSource, installSourceDetails), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            try{
                                if ( status ){
                                    Toast.makeText(getApplicationContext(), "Thanks for your input", Toast.LENGTH_LONG).show();
                                    getMastaanApplication().getCurrentActivity().getGoogleAnalyticsMethods().sendEvent("Install Source", installSource, localStorageData.getBuyerMobileOrEmail());
                                }
                            }catch (Exception e){}
                        }
                    });
                }catch (Exception e){}
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }*/