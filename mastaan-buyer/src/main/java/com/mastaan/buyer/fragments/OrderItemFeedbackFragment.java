package com.mastaan.buyer.fragments;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vLinearLayout;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.AttributeDetails;
import com.mastaan.buyer.models.CategoryDetails;
import com.mastaan.buyer.models.OrderItemDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class OrderItemFeedbackFragment extends MastaanFragment {

    float viewScale;
    OrderItemDetails orderItemDetails;

    Context context;
    vLinearLayout carouselHolder;
    ImageView item_image;
    TextView item_name;
    TextView item_quantity;

    List<CategoryDetails> categoriesList;

    public static Fragment newInstance(Context context, float viewScale, OrderItemDetails orderItemDetails) {
        Bundle bundle = new Bundle();
        bundle.putFloat("viewScale", viewScale);
        bundle.putString("cartItemDetails", new Gson().toJson(orderItemDetails));
        return Fragment.instantiate(context, OrderItemFeedbackFragment.class.getName(), bundle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_item_feedback, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle data) {
        super.onViewCreated(view, data);
        this.context = getActivity();
        categoriesList = new LocalStorageData(context).getCategories();

        viewScale = getArguments().getFloat("viewScale");
        orderItemDetails = new Gson().fromJson(getArguments().getString("cartItemDetails"), OrderItemDetails.class);
        carouselHolder = (vLinearLayout) view.findViewById(R.id.carouselHolder);
        carouselHolder.setScaleBoth(viewScale);

        item_image = (ImageView) view.findViewById(R.id.item_image);
        item_name = (TextView) view.findViewById(R.id.item_name);
        item_quantity = (TextView) view.findViewById(R.id.item_details);

        item_name.setText(CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getName()));
        //item_quantity.setText(getSelectedOptions());

        Picasso.get()
                .load(orderItemDetails.getMeatItemDetails().getImageURL())
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .fit()
                .centerCrop()
                .tag(context)
                .into(item_image);
    }

    public String getSelectedOptions(){

        String selectedOptionsString = "";

        List<String> selectedOptions = new ArrayList<>();
        for (int i=0;i< orderItemDetails.getSelectedAttributes().size(); i++) {
            selectedOptions.add(getOptionName(orderItemDetails.getSelectedAttributes().get(i).getAttributeID(), orderItemDetails.getSelectedAttributes().get(i).getOptinID()));//+"("+cartItemDetails.getSelectedAttributes().get(i).getName()+")");
        }

        String weightQuantityUnit = "";
        if ( orderItemDetails.getMeatItemDetails().getQuantityType().equalsIgnoreCase("n") ){
            weightQuantityUnit = "piece";
        }
        else if ( orderItemDetails.getMeatItemDetails().getQuantityType().equalsIgnoreCase("w") ){
            weightQuantityUnit = "kg";
        }
        else if ( orderItemDetails.getMeatItemDetails().getQuantityType().equalsIgnoreCase("s") ){
            weightQuantityUnit = "set";
        }

        selectedOptionsString = getCategoryName(orderItemDetails.getMeatItemDetails().getCategoriesList().get(0).getValue());
        if ( orderItemDetails.getQuantity() == 1 ){
            selectedOptionsString += ", "+CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity())+" "+weightQuantityUnit;
        }else{
            selectedOptionsString += ", "+CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity())+" "+weightQuantityUnit+"s";
        }

        if ( CommonMethods.getStringFromStringList(selectedOptions).length() > 0 ) {
            selectedOptionsString += "\n" + CommonMethods.getStringFromStringList(selectedOptions);
        }
        return selectedOptionsString;
    }

    public String getOptionName(String attributeID, String attributeOptionID){

        String attributeOptionName = "";

        for (int i=0;i<orderItemDetails.getMeatItemDetails().getAttributes().size();i++){
            if ( attributeID.equals(orderItemDetails.getMeatItemDetails().getAttributes().get(i).getID())) {
                AttributeDetails attributeDetails = orderItemDetails.getMeatItemDetails().getAttributes().get(i);
                for (int j=0;j<attributeDetails.getAvailableOptions().size();j++){
                    if ( attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getValue()) || attributeOptionID.equals(attributeDetails.getAvailableOptions().get(j).getID()) ){
                        attributeOptionName = attributeDetails.getAvailableOptions().get(j).getName();
                        break;
                    }
                }
            }
        }
        return attributeOptionName;
    }

    public String getCategoryName(String categoryValue){


        String categoryName = "";

        if ( categoriesList != null ){

            for (int i=0;i<categoriesList.size();i++){
                CategoryDetails categoryDetails = categoriesList.get(i);
                if ( categoryDetails.getValue().equalsIgnoreCase(categoryValue) ){
                    categoryName = categoryDetails.getName();
                    break;
                }
                if ( categoryDetails.getSubCategoriesCount() > 0 ){
                    for (int j=0;j<categoryDetails.getSubCategories().size();j++){
                        CategoryDetails subCategoryDetails = categoryDetails.getSubCategories().get(j);
                        if ( subCategoryDetails.getValue().equalsIgnoreCase(categoryValue) ){
                            categoryName = subCategoryDetails.getName() + " "+categoryDetails.getName();
                            if ( subCategoryDetails.getValue().equals("seawatersf") || subCategoryDetails.getValue().equals("freshwatersf") ){
                                categoryName = subCategoryDetails.getName();
                            }
                        }
                    }
                }
            }
        }
        return categoryName;
    }
}