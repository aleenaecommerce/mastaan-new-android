package com.mastaan.buyer.fragments;


import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.AddressBookActivity;
import com.mastaan.buyer.activities.CrispActivity;
import com.mastaan.buyer.activities.LegalActivity;
import com.mastaan.buyer.activities.LocationSelectionActivity;
import com.mastaan.buyer.activities.LoyaltyProgramActivity;
import com.mastaan.buyer.activities.MapLocationSelectionActivity;
import com.mastaan.buyer.activities.OrderHistoryActivity;
import com.mastaan.buyer.activities.ProfileActivity;
import com.mastaan.buyer.activities.ReferFriendActivity;
import com.mastaan.buyer.activities.SettingsActivity;
import com.mastaan.buyer.activities.WalletActivity;
import com.mastaan.buyer.backend.GeneralAPI;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.interfaces.RegistrationCallback;
import com.mastaan.buyer.models.BuyerDetails;
import com.mastaan.buyer.support.CrispSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.freshdesk.mobihelp.Mobihelp;
//import com.freshdesk.mobihelp.MobihelpConfig;


public class SideBarFragment extends MastaanFragment implements View.OnClickListener{

    TextView membership;
    LinearLayout header_location;
    ImageView header_logo;
    LinearLayout itemsHolder;
    View []itemViewHolders;
    AddressBookItemDetails deliveryLocation;
    AlertDialog supportDialog;
    Button deliver_location;
    Button helpdesk, send_message, call_us;

    List<Map<String, Object>> sideBarItems = new ArrayList<>();

    SideBarCallback callback;
    public void setCallback(SideBarCallback callback){
        this.callback = callback;
    }

    public interface SideBarCallback{
        void onItemSelected(int position);
        void onDeliverLocationChange(PlaceDetails deliverLocationDetails, ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation);
    }

    public SideBarFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sidebar, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Setting & Updating Freshchat session user details
        new CrispSupport(context).setup();

        registerFragmentReceiver(Constants.SIDEBAR_RECEIVER);

        //---------------------------

        membership = view.findViewById(R.id.membership);
        membership.setOnClickListener(this);
        header_location = view.findViewById(R.id.header_location);
        header_logo = view.findViewById(R.id.header_logo);

        supportDialog = new AlertDialog.Builder(context).create();
        View supportView = inflater.inflate(R.layout.dialog_support, null);
        supportDialog.setView(supportView);

        deliver_location = view.findViewById(R.id.deliver_location);
        deliver_location.setOnClickListener(this);
        helpdesk = supportView.findViewById(R.id.helpdesk);
        helpdesk.setOnClickListener(this);
        send_message = supportView.findViewById(R.id.send_message);
        send_message.setOnClickListener(this);
        call_us = supportView.findViewById(R.id.call_us);
        call_us.setOnClickListener(this);

        //------------------------

        displayMembershipDetails();

        displayDeliveryLocation();

        itemsHolder = view.findViewById(R.id.itemsHolder);
        displaySideBarItems();     // Display Items List...

    }


    @Override
    public void onFragmentReceiverMessage(String receiverName, Intent receivedData) {
        super.onFragmentReceiverMessage(receiverName, receivedData);

        try{
            String type = receivedData.getStringExtra(Constants.ACTION_TYPE);
            if ( type.equalsIgnoreCase(Constants.DISPLAY_USER_SESSION_INFO) ){
                updateUIBasedOnSession();
            }
            else if ( type.equalsIgnoreCase(Constants.DISPLAY_MEMBERSHIP_DETAILS) ){
                displayMembershipDetails();
            }
            else if ( type.equalsIgnoreCase(Constants.DISPLAY_DELIVERY_LOCATION) ){
                displayDeliveryLocation();
            }
        }catch (Exception e){}

    }

    @Override
    public void onClick(View view) {

        if ( view == membership ){
            if ( localStorageData.isDeliveryLocationExists() ) {
                Intent membershipsAct = new Intent(context, LoyaltyProgramActivity.class);
                startActivity(membershipsAct);
            }
        }
        else if ( view == deliver_location ){
            if ( callback != null ){
                callback.onItemSelected(-1);
            }

            if ( localStorageData.isHasLocationHistory() ){
                Intent locationSelectionAct = new Intent(context, LocationSelectionActivity.class);
                locationSelectionAct.putExtra("isForActivityResult", true);
                startActivityForResult(locationSelectionAct, Constants.LOCATION_SELECTOR_ACTIVITY_CODE);
            }else{
                Intent mapLocationSelectionAct = new Intent(context, MapLocationSelectionActivity.class);
                mapLocationSelectionAct.putExtra("isForActivityResult", true);
                startActivityForResult(mapLocationSelectionAct, Constants.LOCATION_SELECTOR_ACTIVITY_CODE);
            }
        }
        else if ( view == helpdesk ){
            supportDialog.dismiss();

            getGoogleAnalyticsMethods().sendEvent("Support", "Support channel", "Helpdesk");         // Send Event to Analytics

            /*FaqOptions faqOptions = new FaqOptions()
                    .showFaqCategoriesAsGrid(true)
                    .showContactUsOnAppBar(true)
                    .showContactUsOnFaqScreens(false)
                    .showContactUsOnFaqNotHelpful(false);
                    //.filterByTags(CommonMethods.getStringListFromStringArray(new String[]{"payment"}), "Payment");
            Freshchat.showFAQs(context, faqOptions);*/
        }
        else if ( view == send_message ){
            supportDialog.dismiss();

            getGoogleAnalyticsMethods().sendEvent("Support", "Support channel", "Chat");         // Send Event to Analytics
            /*Freshchat.showConversations(context);*/

            Intent cirspAct = new Intent(context, CrispActivity.class);
            startActivity(cirspAct);
        }
        else if ( view == call_us ){
            supportDialog.dismiss();

            getGoogleAnalyticsMethods().sendEvent("Support", "Support channel", "Call");         // Send Event to Analytics
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + localStorageData.getSupportContactNumber()));
            startActivity(intent);
        }
    }


    public void displayDeliveryLocation(){
         deliveryLocation = localStorageData.getDeliveryLocation();
        if( deliveryLocation != null && deliveryLocation.getFullAddress() != null ) {
            header_location.setVisibility(View.VISIBLE);
            header_logo.setVisibility(View.GONE);
            deliver_location.setText(localStorageData.getDeliveryLocation().getFullAddress());
        }
        else{
            header_location.setVisibility(View.GONE);
            header_logo.setVisibility(View.VISIBLE);
        }
    }

    public void changeDeliveryLocation(String deliveryLocationName){
        deliver_location.setText(deliveryLocationName);
    }

    public void updateUIBasedOnSession(){

        for (int i=0;i<sideBarItems.size();i++){
            String itemName = sideBarItems.get(i).get("itemName").toString();

            if ( itemName.contains("You") || itemName.equalsIgnoreCase("Order history") || itemName.equalsIgnoreCase("Wallet") ){
                itemViewHolders[i].setVisibility(localStorageData.getSessionFlag()?View.VISIBLE:View.GONE);
            }
            else if ( itemName.equalsIgnoreCase("Loyalty program") ){
                itemViewHolders[i].setVisibility(localStorageData.isDeliveryLocationExists()?View.VISIBLE:View.GONE);
            }
            else if ( itemName.equalsIgnoreCase("Login/Register") ){
                itemViewHolders[i].setVisibility(localStorageData.getSessionFlag()?View.GONE:View.VISIBLE);
            }
        }

        displayMembershipDetails();

        displayDeliveryLocation();
    }

    public void displayMembershipDetails(){
        try{
            if ( localStorageData.getSessionFlag() ){
                String buyerMembershipType = localStorageData.getBuyerMembershipType();
                if ( buyerMembershipType != null && buyerMembershipType.trim().length() > 0 && buyerMembershipType.equalsIgnoreCase(Constants.BRONZE_MEMBER) == false ){
                    membership.setVisibility(View.VISIBLE);
                    membership.setText(CommonMethods.addSpacesBetweenCharacters(buyerMembershipType, 2));
                }else{
                    membership.setVisibility(View.GONE);
                }
            }else{
                membership.setVisibility(View.GONE);
            }
        }catch (Exception e){}
    }

    public void displaySideBarItems(){

        String []listItems =  new String[]{"You", "Order history", "Wallet", "Loyalty program", "Refer a friend", "Legal", "Support", "Rate our app", "Settings", "Login/Register"};//, "Address book"
        int []listImages = new int[]{R.drawable.ic_profile, R.drawable.ic_previous, R.drawable.ic_wallet_grey, R.drawable.ic_loyality_center, R.drawable.ic_refer_friend, R.drawable.ic_gavel_grey, R.drawable.ic_support/*, R.drawable.ic_flower_grey*/, R.drawable.ic_rate, R.drawable.ic_settings, R.drawable.ic_login_grey};//, R.drawable.ic_map_marker_grey
        for (int i = 0; i < listItems.length; i++) {
            Map<String, Object> item = new HashMap<>();
            item.put("itemName", listItems[i]);
            item.put("itemImage", listImages[i]);
            sideBarItems.add(item);
        }

        itemViewHolders = new View[sideBarItems.size()];

        for(int i=0;i<sideBarItems.size();i++){
            final int position = i;
            View itemView = inflater.inflate(R.layout.view_sidebar_item, null, false);
            itemViewHolders [i] = itemView;
            itemsHolder.addView(itemView);

            TextView title = itemView.findViewById(R.id.title);
            ImageView image = itemView.findViewById(R.id.image);

            title.setText(sideBarItems.get(position).get("itemName").toString());
            /*if ( sideBarItems.get(position).get("itemName").toString().equalsIgnoreCase("You")){
                if ( localStorageData.getSessionFlag() ){
                    title.setText(Html.fromHtml("You (<b>"+localStorageData.getBuyerDetails().getLoyaltyPoints()+"</b> points)"));
                }
            }*/
            image.setBackgroundResource((int) sideBarItems.get(position).get("itemImage"));


            Button itemSelector = itemView.findViewById(R.id.itemSelector);
            itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sideBarItemSelect(position);
                }
            });
        }

        updateUIBasedOnSession();
    }

    public void sideBarItemSelect(int position){

        if ( callback != null ){
            callback.onItemSelected(position);
        }

        String selectedItem = sideBarItems.get(position).get("itemName").toString();

        if ( selectedItem.contains("You")){
            Intent profileAct = new Intent(context, ProfileActivity.class);
            startActivity(profileAct);
        }
        else if ( selectedItem.equalsIgnoreCase("Login/Register")) {
            goToRegistration(new RegistrationCallback() {
                @Override
                public void onComplete(boolean status, String token, BuyerDetails buyerDetails) {
                    if (status) {
                        //updateSidebarUIBasedOnSessioin();     ITS CALLED BY FROM RECEIVER
                        showSnackbarMessage("Logged in successfully.", null, null);
                    }
                }
            });
        } else if ( selectedItem.equalsIgnoreCase("Order history")) {
            Intent orderHistoryAct = new Intent(context, OrderHistoryActivity.class);
            startActivity(orderHistoryAct);
        }
        else if ( selectedItem.equals("Address book")) {
            Intent addressBookAct = new Intent(context, AddressBookActivity.class);
            startActivity(addressBookAct);
        }
        else if ( selectedItem.equals("Wallet")) {
            Intent walletAct = new Intent(context, WalletActivity.class);
            startActivity(walletAct);
        }
        else if ( selectedItem.equals("Loyalty program")) {
            if ( localStorageData.isDeliveryLocationExists() ) {
                Intent membershipsAct = new Intent(context, LoyaltyProgramActivity.class);
                startActivity(membershipsAct);
            }
        }
        else if ( selectedItem.equalsIgnoreCase("Refer a Friend") ){
            getGoogleAnalyticsMethods().sendEvent("Growth", "Refer friend", "");         // Send Event to Analytics

            Intent referFriendAct = new Intent(context, ReferFriendActivity.class);
            startActivity(referFriendAct);
        }
        else if ( selectedItem.equalsIgnoreCase("Legal") ){
            getGoogleAnalyticsMethods().sendEvent("General", "Legal", "");         // Send Event to Analytics
            Intent legalAct = new Intent(context, LegalActivity.class);
            startActivity(legalAct);
        }
        else if ( selectedItem.equalsIgnoreCase("Support") ) {
            getGoogleAnalyticsMethods().sendEvent("Support", "Support", "");         // Send Event to Analytics
            supportDialog.show();
        }
        else if ( selectedItem.equalsIgnoreCase("Rate our app") ){
            getGoogleAnalyticsMethods().sendEvent("Growth", "Rate app", "");         // Send Event to Analytics
            activity.openAppInPlayStore();
        }
        else if ( selectedItem.equalsIgnoreCase("Settings") ){
            Intent settingsAct = new Intent(context, SettingsActivity.class);
            startActivity(settingsAct);
        }
        else{
            showSnackbarMessage(selectedItem + " is under construction", null, null);
        }
    }

    private void changeDeliveryLocation(final PlaceDetails placeDetails){
        showLoadingDialog("Checking availability...");
        getBackendAPIs().getGeneralAPI().changeLocation(placeDetails.getId(), placeDetails.getLatLng(), new GeneralAPI.CheckAvailabilityAtLocationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation) {
                closeLoadingDialog();
                if ( status ){
                    if ( responseCheckAvailabilityAtLocation.isAvailable() ) {
                        changeDeliveryLocation(placeDetails.getFullAddress());  // Updating name in sidebar

                        if (callback != null) {
                            callback.onDeliverLocationChange(placeDetails, responseCheckAvailabilityAtLocation);
                        }
                    }else{
                        activity.showNoServiceDialog(new AddressBookItemDetails(placeDetails), null);
                    }
                }else{
                    showChoiceSelectionDialog(true, "Error!", "Something went wrong while checking availability at selected location.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                changeDeliveryLocation(placeDetails);
                            }
                        }
                    });
                }
            }
        });
    }

    //=================

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if ( requestCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE && resultCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE ) {      // If It is from PlaceSearch Activity
                final PlaceDetails placeDetails = new Gson().fromJson(data.getStringExtra("deliver_location_json"), PlaceDetails.class);
                //ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation = new Gson().fromJson(data.getStringExtra(Constants.RESPONSE_CHECK_AVAILABILITY_AT_LOCATION), ResponseCheckAvailabilityAtLocation.class);
                if ( localStorageData.getCartCount() > 0 ) {
                    showChoiceSelectionDialog(false, "Alert", "Sorry, changing the delivery location will clear your meat basket.\n\nDo you want to change your delivery location?", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                changeDeliveryLocation(placeDetails);
                            }
                        }
                    });
                }else{
                    changeDeliveryLocation(placeDetails);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
