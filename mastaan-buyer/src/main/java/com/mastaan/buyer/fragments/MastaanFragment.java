package com.mastaan.buyer.fragments;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleena.common.fragments.vFragment;
import com.mastaan.buyer.MastaanApplication;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.LaunchActivity;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.analytics.GoogleAnalyticsMethods;
import com.mastaan.buyer.backend.BackendAPIs;
import com.mastaan.buyer.interfaces.RegistrationCallback;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.payment.razorpay.RazorpayPayment;

public class MastaanFragment extends vFragment {

    Context context;
    MastaanToolBarActivity activity;

    LocalStorageData localStorageData;

    String MASTAAN_BASE_URL;


    public MastaanFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
        activity = (MastaanToolBarActivity) getActivity();

        localStorageData = new LocalStorageData(context);

        MASTAAN_BASE_URL = getString(R.string.mastaan_base_url);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public MastaanToolBarActivity getMastaanActivity() {
        if ( activity == null ){    activity = (MastaanToolBarActivity) getActivity();    }
        return activity;
    }

    public MastaanApplication getMastaanApplication() {
        return getMastaanActivity().getMastaanApplication();
    }

    public GoogleAnalyticsMethods getGoogleAnalyticsMethods() {
        return getMastaanActivity().getGoogleAnalyticsMethods();
    }

    public BackendAPIs getBackendAPIs() {
        return getMastaanActivity().getBackendAPIs();
    }

    public RazorpayPayment getRazorpayPayment(){
        return getMastaanActivity().getRazorpayPayment();
    }

    public void checkSessionValidity(int statusCode){
        getMastaanActivity().checkSessionValidity(statusCode);
    }
    public void checkMaintainance(int statusCode){
        if (statusCode == 503 ){
            clearUserSession("Under maintainance.");
            Intent loadingAct = new Intent(context, LaunchActivity.class);
            ComponentName componentName = loadingAct.getComponent();
            Intent mainIntent = Intent.makeRestartActivityTask(componentName);
            startActivity(mainIntent);
        }
    }

    public void goToRegistration(RegistrationCallback registrationCallback){
        getMastaanActivity().goToRegistration(registrationCallback);
    }

    public void clearUserSession(String messageToDisplay){
        getMastaanActivity().clearUserSession(messageToDisplay);
    }

}
