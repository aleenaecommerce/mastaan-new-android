package com.mastaan.buyer.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.buyer.methods.ShowCaseSequenceMethods;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.FavouriteActivity;
import com.mastaan.buyer.activities.MapLocationSelectionActivity;
import com.mastaan.buyer.adapters.AddressBookAdapter;
import com.mastaan.buyer.backend.AddressBookAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;

import java.util.ArrayList;
import java.util.List;


public class AddressBookFragment extends MastaanFragment {

    View layoutView;

    FrameLayout addFavouriteInfo;
    TextView favourite_location_name;
    View addFavourite;

    vRecyclerView addressBookHolder;
    AddressBookAdapter addressBookAdapter;
    AddressBookAdapter.CallBack addressBookCallback;

    int selectedItemIndex;

    boolean isForResult;
    PlaceDetails faouriteLocationDetails;

    public AddressBookFragment(){}

    public static Fragment newInstance(Context context, boolean isForResult, PlaceDetails faouriteLocationDetails) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isForResult", isForResult);
        bundle.putString("faouriteLocationDetails", new Gson().toJson(faouriteLocationDetails));
        return Fragment.instantiate(context, AddressBookFragment.class.getName(), bundle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_address_book, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutView = view;
        hasLoadingView();

        isForResult = getArguments().getBoolean("isForResult");
        faouriteLocationDetails = new Gson().fromJson(getArguments().getString("faouriteLocationDetails"), PlaceDetails.class);

        //...........................................................

        ShowCaseSequenceMethods ShowCaseSequenceMethods = new ShowCaseSequenceMethods(getActivity(), "ADDRESS_BOOK_SHOWCASE");

        addFavouriteInfo = (FrameLayout) view.findViewById(R.id.addFavouriteInfo);
        favourite_location_name = (TextView) view.findViewById(R.id.favourite_location_name);
        addFavourite = view.findViewById(R.id.addFavourite);
        if ( faouriteLocationDetails != null ){
            ShowCaseSequenceMethods.showSingeView(addFavourite, "Press here to add this location as a favourite in your address book.", "GOT IT", "ADDRESS_BOOK_SHOWCASE");
        }

        addressBookHolder = (vRecyclerView) view.findViewById(R.id.addressBookHolder);
        setupAddressBookHolder();

        toggleAddFavouriteButton();     //  Toggling AddFavButton based on Location Availability

        getAddressBook();      // Load Favourites

    }

    @Override
    public void onFABPressed() {
        super.onFABPressed();

        Intent mapLocationSelectionAct = new Intent(context, MapLocationSelectionActivity.class);
        mapLocationSelectionAct.putExtra("activityTitle", "SELECT FAVOURITE");
        mapLocationSelectionAct.putExtra("isForActivityResult", true);
        mapLocationSelectionAct.putExtra("isForAddFavourite", true);
        startActivityForResult(mapLocationSelectionAct, Constants.LOCATION_SELECTOR_ACTIVITY_CODE);
    }

    public void toggleAddFavouriteButton(){

        if ( faouriteLocationDetails != null ){

            addFavouriteInfo.setVisibility(View.VISIBLE);
            addFavourite.setVisibility(View.VISIBLE);

            favourite_location_name.setText(faouriteLocationDetails.getFullAddress());

            addFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") == false ) {
                        Intent addFavActivity = new Intent(context, FavouriteActivity.class);
                        addFavActivity.putExtra("mode", "New");
                        addFavActivity.putExtra("address_book_item_json", new Gson().toJson(new AddressBookItemDetails(faouriteLocationDetails)));
                        startActivityForResult(addFavActivity, Constants.FAVOURITE_ACTIVITY_CODE);
                    }
                }
            });
        }
        else{
            addFavouriteInfo.setVisibility(View.GONE);
            addFavourite.setVisibility(View.GONE);
        }
    }

    public void setupAddressBookHolder(){

        addressBookHolder.setHasFixedSize(true);
        addressBookHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        addressBookHolder.setItemAnimator(new DefaultItemAnimator());

        addressBookCallback = new AddressBookAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                if ( isForResult == true ) {
                    selectedItemIndex = position;
                    String address_item_json = new Gson().toJson(addressBookAdapter.getItem(position));
                    Intent placeData = new Intent();
                    placeData.putExtra("address_item_json", address_item_json);
                    getActivity().setResult(Constants.ADDRESS_BOOK_ACTIVITY_CODE, placeData);
                    getActivity().finish();
                }
            }

            @Override
            public void onItemMenuClick(final int position, View view) {
                if ( getResources().getString(R.string.app_type).equalsIgnoreCase("internal") == false ) {
                    selectedItemIndex = position;

                    PopupMenu popup = new PopupMenu(context, view);;//new PopupMenu(getActivity().getSupportActionBar().getThemedContext(), view);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.menu_options, popup.getMenu());
                    popup.show();

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {

                            if ( menuItem.getItemId() == R.id.action_edit ) {
                                Intent addFavActivity = new Intent(context, FavouriteActivity.class);
                                addFavActivity.putExtra("mode", "Edit");
                                addFavActivity.putExtra("address_book_item_json", new Gson().toJson(addressBookAdapter.getItem(position)));
                                startActivityForResult(addFavActivity, Constants.FAVOURITE_ACTIVITY_CODE);
                            }
                            else if ( menuItem.getItemId() == R.id.action_delete ) {
                                deleteFavourite(position, addressBookAdapter.getItem(position));
                            }
                            return true;
                        }
                    });
                }
            }
        };

        addressBookAdapter = new AddressBookAdapter(context, R.layout.view_address_book_item1, new ArrayList<AddressBookItemDetails>(), addressBookCallback);
        addressBookHolder.setAdapter(addressBookAdapter);

    }

    public void deleteFavourite(final int position, final AddressBookItemDetails  addressBookItemDetails){

        showLoadingDialog("Deleting favourite, please wait...");
        getBackendAPIs().getAddressBookAPI().deleteFavourie(addressBookItemDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {

                closeLoadingDialog();

                if (status == true) {
                    addressBookAdapter.deleteItem(position);

                    new BroadcastReceiversMethods(context).displayAddressBookListInAddressBookPage();

                    if (addressBookAdapter.getItemsCount() == 0) {
                        showNoDataIndicator("No Favourites added yet");
                        //toggleAddFavouriteButton();
                    } else {
                        switchToContentPage();
                    }
                    showSnackbarMessage("Favourite Deleted!", null, null);
                } else {
                    checkSessionValidity(statusCode);
                    checkMaintainance(statusCode);
                    showSnackbarMessage("Unable to Delete Favourite , try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            deleteFavourite(position, addressBookItemDetails);
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getAddressBook();
    }

    public void getAddressBook(){

        showLoadingIndicator("Loading your favourites...");
        getBackendAPIs().getAddressBookAPI().getFavourites(new AddressBookAPI.FavouritesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<AddressBookItemDetails> addressBookItems) {
                if (status == true) {
                    if (addressBookItems.size() > 0) {
                        addressBookAdapter.addItems(addressBookItems);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No favourites added yet");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    checkMaintainance(statusCode);
                    showReloadIndicator(statusCode, "Unable to load your favourites, try again!");
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if ( requestCode == Constants.FAVOURITE_ACTIVITY_CODE && resultCode == Constants.FAVOURITE_ACTIVITY_CODE ) {

                String mode = data.getStringExtra("mode");
                String address_book_item_json = data.getStringExtra("address_book_item_json");
                AddressBookItemDetails addressBookItemDetails = new Gson().fromJson(address_book_item_json, AddressBookItemDetails.class);

                if ( mode.equalsIgnoreCase("Edit") ){
                    addressBookAdapter.updateItem(selectedItemIndex, addressBookItemDetails);
                    showSnackbarMessage("Favourite Location (" + addressBookItemDetails.getTitle() + ") Updated.", null, null);
                }
                else if ( mode.equalsIgnoreCase("New") ){
                    addressBookAdapter.addItem(addressBookItemDetails);
                    showSnackbarMessage("Favourite Location (" + addressBookItemDetails.getTitle() + ") Saved.", null, null);
                    addFavouriteInfo.setVisibility(View.GONE);
                    addFavourite.setVisibility(View.GONE);
                }
                switchToContentPage();
            }
            else  if ( requestCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE && resultCode == Constants.LOCATION_SELECTOR_ACTIVITY_CODE ){

                String deliver_location_json = data.getStringExtra("deliver_location_json");
                faouriteLocationDetails = new Gson().fromJson(deliver_location_json, PlaceDetails.class);
                Intent addFavActivity = new Intent(context, FavouriteActivity.class);
                addFavActivity.putExtra("mode", "New");
                addFavActivity.putExtra("address_book_item_json", new Gson().toJson(new AddressBookItemDetails(faouriteLocationDetails)));
                startActivityForResult(addFavActivity, Constants.FAVOURITE_ACTIVITY_CODE);
            }
        }catch (Exception e){   e.printStackTrace();    }

    }
}
