package com.mastaan.buyer.fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.activities.LoyaltyProgramActivity;
import com.mastaan.buyer.activities.ProfileActivity;
import com.mastaan.buyer.activities.ProfileEditActivity;
import com.mastaan.buyer.backend.ProfileAPI;
import com.mastaan.buyer.backend.models.RequestAddOnlinePayment;
import com.mastaan.buyer.backend.models.RequestProfileUpdate;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.methods.BroadcastReceiversMethods;
import com.mastaan.buyer.models.BuyerDetails;
import com.mastaan.buyer.models.ProfileSummaryDetails;
import com.mastaan.buyer.payment.models.PaymentDetails;
import com.mastaan.buyer.payment.paytm.PaytmPayment;
import com.mastaan.buyer.payment.razorpay.RazorpayPayment;

public class ProfileSummaryFragment extends MastaanFragment implements View.OnClickListener{

    ProfileActivity profileActivity;

    View ordersCount;
    TextView orders_count;
    View ordersValue;
    TextView orders_value;
    TextView membership;
    TextView user_name;
    TextView join_date;
    TextView user_email;
    TextView user_phone;///*, user_dob, user_anniversary*/;
    View outstandingDetailsView;
    TextView outstanding_amount;
    View payOutstandingOnline;

    BuyerDetails buyerDetails;


    public ProfileSummaryFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_profile_summary, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        profileActivity = (ProfileActivity) getActivity();      // Accessing Activity from Fragment..

        //------------------------------------------------------------

        ordersCount = view.findViewById(R.id.ordersCount);
        ordersCount.setOnClickListener(this);
        orders_count = view.findViewById(R.id.orders_count);
        ordersValue = view.findViewById(R.id.ordersValue);
        ordersValue.setOnClickListener(this);
        orders_value = view.findViewById(R.id.orders_value);
        membership = view.findViewById(R.id.membership);
        membership.setOnClickListener(this);
        user_name = view.findViewById(R.id.user_name);
        join_date= view.findViewById(R.id.join_date);
        user_email = view.findViewById(R.id.user_email);
        user_phone = view.findViewById(R.id.user_phone);
        /*user_dob = view.findViewById(R.id.user_dob);
        user_anniversary = view.findViewById(R.id.user_anniversary);*/

        outstandingDetailsView = view.findViewById(R.id.outstandingDetailsView);
        outstanding_amount = view.findViewById(R.id.outstanding_amount);
        payOutstandingOnline = view.findViewById(R.id.payOutstandingOnline);
        payOutstandingOnline.setOnClickListener(this);


        //--------------

        getProfileDetails();        //  Getting User Profile Details.

    }

    @Override
    public void onClick(View view) {

        if ( view == ordersCount || view == ordersValue ){
            profileActivity.changePage("Order history");
        }
        else if ( view == payOutstandingOnline ){
            View dialogMakeOnlinePaymentView = LayoutInflater.from(context).inflate(R.layout.dialog_make_online_payment, null);
            final vTextInputLayout amount = (vTextInputLayout) dialogMakeOnlinePaymentView.findViewById(R.id.amount);
            amount.setText(CommonMethods.getInDecimalFormat(buyerDetails.getOutstandingAmount())+"");

            dialogMakeOnlinePaymentView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeCustomDialog();
                }
            });
            dialogMakeOnlinePaymentView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String oAmount = amount.getText();
                    amount.checkError("* Enter amount");
                    if ( oAmount.length() > 0 ){
                        inputMethodManager.hideSoftInputFromWindow(amount.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
                        closeCustomDialog();
                        checkProfileAndGoToOnlinePayment(Double.parseDouble(oAmount));
                    }
                }
            });
            showCustomDialog(dialogMakeOnlinePaymentView);
        }
        else if ( view == membership ){
            if ( localStorageData.isDeliveryLocationExists() ) {
                Intent membershipsAct = new Intent(context, LoyaltyProgramActivity.class);
                startActivity(membershipsAct);
            }
        }
    }

    public void toggleEditButton() {
        if ( buyerDetails != null ){
            toggleEditButton(true);
        }else {
            toggleEditButton(false);
        }
    }

    private void toggleEditButton(boolean status){
        if ( profileActivity != null ){
            if ( profileActivity.profileItems[profileActivity.pageNumber].equalsIgnoreCase("Profile") ){
                profileActivity.setFABVisibility(status);
            }
        }
    }

    private void displayProfileDetails(ProfileSummaryDetails profileSummaryDetails){
        switchToContentPage();

        orders_count.setText(CommonMethods.getIndianFormatNumber(profileSummaryDetails.getOrdersCount()));
        orders_value.setText(SpecialCharacters.RS + " "+ CommonMethods.getIndianFormatNumber(Math.round(profileSummaryDetails.getFinalTotal())));

        displayBuyerDetails(profileSummaryDetails.getBuyerDetails());

        if ( buyerDetails.getOutstandingAmount() > 0 ){
            outstandingDetailsView.setVisibility(View.VISIBLE);
            payOutstandingOnline.setVisibility(View.VISIBLE);
            outstanding_amount.setText(Html.fromHtml("Outstanding: <b>"+ SpecialCharacters.RS+" "+ CommonMethods.getIndianFormatNumber(buyerDetails.getOutstandingAmount())+"</b>"));
        }else if ( buyerDetails.getOutstandingAmount() > 0 ){
            outstandingDetailsView.setVisibility(View.VISIBLE);
            payOutstandingOnline.setVisibility(View.GONE);
            outstanding_amount.setText(Html.fromHtml("Outstanding: <b>"+ SpecialCharacters.RS+" "+ CommonMethods.getIndianFormatNumber(buyerDetails.getOutstandingAmount())+"</b> (Pending from us)"));
        }else{
            outstandingDetailsView.setVisibility(View.GONE);
        }
        outstandingDetailsView.setVisibility(View.GONE);
    }

    private void displayBuyerDetails(BuyerDetails buyerDetails){
        this.buyerDetails = buyerDetails;

        switchToContentPage();

        user_name.setText(buyerDetails.getName());
        if ( buyerDetails.getName() == null || buyerDetails.getName().trim().length() == 0 ){
            user_name.setText("User Name");
        }
        join_date.setText("Joined on "+ DateMethods.getDateInFormat(buyerDetails.getJoinedDate(), DateConstants.MMM_DD_YYYY));
        user_email.setText(buyerDetails.getEmail());
        user_phone.setText(buyerDetails.getMobile());

        String buyerMembershipType = buyerDetails.getMembershipType();//buyerMembershipType=Constants.DIAMOND_MEMBER;
        if ( buyerMembershipType != null && buyerMembershipType.trim().length() > 0 && buyerMembershipType.equalsIgnoreCase(Constants.BRONZE_MEMBER) == false ){
            membership.setVisibility(View.VISIBLE);
            membership.setText(CommonMethods.addSpacesBetweenCharacters(buyerMembershipType, 2));
        }else{
            membership.setVisibility(View.GONE);
        }

        /*user_dob.setText(DateMethods.getDateInFormat(buyerDetails.getDOB(), DateConstants.MMM_DD));
        if ( buyerDetails.getMatrialStatus() ){
            user_anniversary.setText(DateMethods.getDateInFormat(buyerDetails.getAnniversary(), DateConstants.MMM_DD));
        }else {
            user_anniversary.setText("");
        }*/
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getProfileDetails();
    }

    private void getProfileDetails(){
        toggleEditButton(false);
        showLoadingIndicator("Loading your profile, wait..");
        getBackendAPIs().getProfileAPI().getProfileDetails(new ProfileAPI.ProfileDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, ProfileSummaryDetails profileSummaryDetails) {
                if (status == true) {
                    displayProfileDetails(profileSummaryDetails);
                    toggleEditButton(true);

                    localStorageData.setBuyerDetails(profileSummaryDetails.getBuyerDetails(), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            new BroadcastReceiversMethods(context).displayMembershipDetailsInSidebar();
                        }
                    });
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load your profile, try again!");
                }
            }
        });
    }

    @Override
    public void onFABPressed() {
        super.onFABPressed();
        Intent profileEditAct = new Intent(getActivity(), ProfileEditActivity.class);
        profileEditAct.putExtra("buyer_details_json", new Gson().toJson(buyerDetails));
        startActivityForResult(profileEditAct, Constants.PROFILE_ACTIVITY_CODE);
    }

    public void checkProfileAndGoToOnlinePayment(final double amount){
        if ( buyerDetails.getName() != null && buyerDetails.getName().length() > 0
                && buyerDetails.getEmail() != null && buyerDetails.getEmail().length() > 0
                && buyerDetails.getMobile() != null && buyerDetails.getMobile().length() > 0 ){
            goToOnlinePayment(amount);
        }else{
            View completeProfileDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_complete_profile, null);
            final vTextInputLayout user_name = (vTextInputLayout) completeProfileDialogView.findViewById(R.id.user_name);
            final vTextInputLayout user_email = (vTextInputLayout) completeProfileDialogView.findViewById(R.id.user_email);
            //vTextInputLayout user_phone = (vTextInputLayout) completeProfileDialogView.findViewById(R.id.user_phone);

            user_name.setText(buyerDetails.getName());
            user_email.setText(buyerDetails.getEmail());

            completeProfileDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String userName = user_name.getText();
                    user_name.checkError("*Enter name");
                    String userEmail = user_email.getText().trim();
                    user_email.checkError("*Enter Email address");

                    if ( userName.length() > 0 && userEmail.length() > 0 ){
                        if ( CommonMethods.isValidEmailAddress(userEmail) ){
                            closeCustomDialog();
                            RequestProfileUpdate requestProfileUpdate = new RequestProfileUpdate(userName, null, userEmail, null, null, false, null);
                            updateProfileAndGoToOnlinePayment(requestProfileUpdate, amount);
                        }else{
                            user_email.showError("* Enter valid email address.");
                        }
                    }
                }
            });

            showCustomDialog(completeProfileDialogView);
        }
    }
    public void updateProfileAndGoToOnlinePayment(final RequestProfileUpdate requestProfileUpdate, final double amount){

        showLoadingDialog("Updating profile...");
        getBackendAPIs().getProfileAPI().updateProfile(requestProfileUpdate, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();

                if (status == true) {
                    showSnackbarMessage("Profile updated successfully.");
                    buyerDetails.setEmail(requestProfileUpdate.getEmail());
                    buyerDetails.setName(requestProfileUpdate.getName());

                    localStorageData.setBuyerDetails(buyerDetails);

                    displayBuyerDetails(buyerDetails);

                    goToOnlinePayment(amount);

                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to update profile.Try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateProfileAndGoToOnlinePayment(requestProfileUpdate, amount);
                        }
                    });
                }
            }
        });
    }


    public void goToOnlinePayment(final double amount){

        getRazorpayPayment().proceedForOutstanding(new PaymentDetails(localStorageData.getBuyerName(), localStorageData.getBuyerMobile(), localStorageData.getBuyerEmail(), amount), new RazorpayPayment.Callback() {
            @Override
            public void onPaymentSuccess(String message, int paymentType, double amount, String transactionID, String paymentID) {
                addOnlinePaymentToOutstanding(new RequestAddOnlinePayment(paymentType, amount, transactionID, paymentID));
            }

            @Override
            public void onPaymentFailure(String message, double amount, String newTransactioinID) {
                showSnackbarMessage("Payment failed");
                showNotificationDialog(false, "Failure!", "Your payment is failed. If the money is debited from your account, it will be refunded.", new ActionCallback() {
                    @Override
                    public void onAction() {
                        getProfileDetails();
                    }
                });
            }

            @Override
            public void onPaymentCancel() {
                Log.d(Constants.LOG_TAG, "Payment Cancelled");
                showSnackbarMessage("Payment Cancelled");
                getProfileDetails();
            }

            @Override
            public void onExternalWalletSelect(String walletName, double amount) {
                if ( walletName.equalsIgnoreCase("paytm") ){
                    new PaytmPayment((MastaanToolBarActivity)getActivity()).proceed(new PaymentDetails(localStorageData.getBuyerName(), localStorageData.getBuyerMobile(), localStorageData.getBuyerEmail(), amount), new PaytmPayment.Callback() {
                        @Override
                        public void onPaymentSuccess(String message, int paymentType, double amount, String transactionID, String paymentID) {
                            addOnlinePaymentToOutstanding(new RequestAddOnlinePayment(paymentType, amount, transactionID, paymentID));
                        }

                        @Override
                        public void onPaymentFailure(String message, double amount, String newTransactioinID) {
                            showSnackbarMessage("Payment failed");
                            showNotificationDialog(false, "Failure!", "Your payment is failed. If the money is debited from your account, it will be refunded.", new ActionCallback() {
                                @Override
                                public void onAction() {
                                    getProfileDetails();
                                }
                            });
                        }

                        @Override
                        public void onPaymentCancel() {
                            Log.d(Constants.LOG_TAG, "Payment Cancelled");
                            showSnackbarMessage("Payment Cancelled");
                            getProfileDetails();
                        }
                    });
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    public void addOnlinePaymentToOutstanding(final RequestAddOnlinePayment requestAddOnlinePayment){

        showLoadingDialog("Processing payment...");
        getBackendAPIs().getPaymentAPI().addOnlinePaymentForOutstanding(requestAddOnlinePayment, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Payment successfull");
                    getProfileDetails();
                }else{
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while processing payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                addOnlinePaymentToOutstanding(requestAddOnlinePayment);
                            }
                        }
                    });
                }
            }
        });
    }

    //======================

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if ( resultCode == Constants.PROFILE_ACTIVITY_CODE && requestCode == Constants.PROFILE_ACTIVITY_CODE ){

                String buyer_details_json = data.getStringExtra("profile_data_json");
                BuyerDetails updatedBuyerDetails = new Gson().fromJson(buyer_details_json, BuyerDetails.class);

                displayBuyerDetails(updatedBuyerDetails);

                if (profileActivity != null) {
                    showSnackbarMessage(profileActivity.activityFAB, "Profile updated successfully.", null, null);
                }else{
                    showSnackbarMessage("Profile updated successfully.", null, null);
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

}
