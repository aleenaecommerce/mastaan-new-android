package com.mastaan.buyer.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mastaan.buyer.R;

public class TermsAndConditionsFragment extends MastaanFragment {

    WebView webview_terms_and_conditions;

    public TermsAndConditionsFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_terms_and_conditions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        //----------

        webview_terms_and_conditions = (WebView) view.findViewById(R.id.webview_terms_and_conditions);
        webview_terms_and_conditions.getSettings().setJavaScriptEnabled(true);
        webview_terms_and_conditions.setWebViewClient(new WebViewClient());
        showLoadingIndicator("Loading...");
        webview_terms_and_conditions.setWebChromeClient(new WebChromeClient(){
            public void onProgressChanged(WebView view, int progress) {
                if( progress == 100) {
                    switchToContentPage();
                }
            }
        });
        webview_terms_and_conditions.loadUrl("http://www.mastaan.com/t-and-c-no-header.html");//"file:///android_asset/html/MastaanTermsAndConditions.html");//"file://html/MastaanTermsAndConditions.html");

    }
}
