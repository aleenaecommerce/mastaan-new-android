package com.mastaan.buyer.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mastaan.buyer.R;

public class PrivacyPolicyFragment extends MastaanFragment {

    WebView webview_privacy_policy;

    public PrivacyPolicyFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_privacy_policy, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        webview_privacy_policy = (WebView) view.findViewById(R.id.webview_privacy_policy);
        webview_privacy_policy.getSettings().setJavaScriptEnabled(true);
        webview_privacy_policy.setWebViewClient(new WebViewClient());
        showLoadingIndicator("Loading...");
        webview_privacy_policy.setWebChromeClient(new WebChromeClient(){
            public void onProgressChanged(WebView view, int progress) {
                if( progress == 100) {
                    switchToContentPage();
                }
            }
        });
        webview_privacy_policy.loadUrl("http://www.mastaan.com/policy-no-header.html");//file:///android_asset/html/MastaanPrivacyPolicy.html");


    }
}
