package com.mastaan.buyer.fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.OrderDetailsActivity;
import com.mastaan.buyer.adapters.OrdersHistoryAdapter;
import com.mastaan.buyer.backend.OrdersAPI;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.OrderDetails;

import java.util.ArrayList;
import java.util.List;


public class OrderHistoryFragment extends MastaanFragment {

    vRecyclerView ordersListHolder;
    OrdersHistoryAdapter ordersListAdapter;

    public OrderHistoryFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();
        hasSwipeRefresh();

        registerFragmentReceiver(Constants.ORDER_DETAILS_RECEIVER);

        //------------

        ordersListHolder = (vRecyclerView) view.findViewById(R.id.ordersListHolder);
        ordersListHolder.setHasFixedSize(true);
        ordersListHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        ordersListHolder.setItemAnimator(new DefaultItemAnimator());
        ordersListAdapter = new OrdersHistoryAdapter(context, new ArrayList<OrderDetails>(), new OrdersHistoryAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                OrderDetails orderDetails = ordersListAdapter.getOrderItem(position);
                if ( orderDetails != null ) {
                    Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
                    orderDetailsAct.putExtra(Constants.ID, orderDetails.getID());
                    startActivity(orderDetailsAct);
                }
            }
        });
        ordersListHolder.setAdapter(ordersListAdapter);

        //------------------------------------

        getPreviousOrders();      // GetOrders

    }

    @Override
    public void onFragmentReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onFragmentReceiverMessage(activityReceiverName, receivedData);
        try {
            if (receivedData.getStringExtra(Constants.ACTION_TYPE).equalsIgnoreCase(Constants.DISPLAY_ORDER_STATUS)) {
                String orderID = receivedData.getStringExtra(Constants.ID);
                String orderStatus = receivedData.getStringExtra(Constants.STATUS);

                Log.d(Constants.LOG_TAG, "Checking to update OrderDetails in OrderHistoryPage, for oID = " + orderID + " ,  with DS = " + orderStatus);

                ordersListAdapter.updateOrderStatus(orderID, orderStatus);
            }
        }catch (Exception e){}
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPreviousOrders();      // GetMyOrders
    }

    public void getPreviousOrders(){

        ordersListAdapter.clearItems();

        showLoadingIndicator("Loading your orders...");
        getBackendAPIs().getOrdersAPI().getOrders(new OrdersAPI.OrdersListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList) {
                if (status) {
                    if (ordersList.size() > 0) {
                        ordersListAdapter.addItems(ordersList);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No orders yet!");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Error in loading your order, try again!");
                }
            }
        });

    }

}
