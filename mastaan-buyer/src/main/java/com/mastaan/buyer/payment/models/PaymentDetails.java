package com.mastaan.buyer.payment.models;

import com.mastaan.buyer.models.OrderDetails;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public class PaymentDetails{
    String name;
    String mobile;
    String email;
    double amount;
    String transactionID;
    String orderID;

    public PaymentDetails(String name, String mobile, String email, double amount){
        this(name, mobile, email, amount, null, null);
    }

    public PaymentDetails(String name, String mobile, String email, double amount, String transactionID, String orderID){
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.amount = amount;
        this.transactionID = transactionID;
        this.orderID = orderID;
    }

    public PaymentDetails(OrderDetails orderDetails){
        this.name = orderDetails.getCustomerName();
        this.mobile = orderDetails.getCustomerMobile();
        this.email = orderDetails.getCustomerEmail();
        this.orderID = orderDetails.getID();
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getMobile() {
        return mobile;
    }

    public PaymentDetails setAmount(double amount) {
        this.amount = amount;
        return this;
    }
    public double getAmount() {
        return amount;
    }

    public PaymentDetails setTransactionID(String transactionID) {
        this.transactionID = transactionID;
        return this;
    }
    public String getTransactionID() {
        return transactionID;
    }

    public PaymentDetails setOrderID(String orderID) {
        this.orderID = orderID;
        return this;
    }
    public String getOrderID() {
        return orderID;
    }

}