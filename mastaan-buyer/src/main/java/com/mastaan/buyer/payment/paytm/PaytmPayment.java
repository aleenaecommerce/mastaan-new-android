package com.mastaan.buyer.payment.paytm;

import android.os.Bundle;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.payment.models.PaymentDetails;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public class PaytmPayment {
    MastaanToolBarActivity activity;
    LocalStorageData localStorageData;
    PaymentDetails paymentDetails;
    Callback callback;

    public interface Callback {
        void onPaymentSuccess(String message, int paymentType, double amount, String transactionID, String paymentID);
        void onPaymentFailure(String message, double amount, String newTransactioinID);
        void onPaymentCancel();
    }


    public PaytmPayment(MastaanToolBarActivity activity){
        this.activity = activity;
        this.localStorageData = new LocalStorageData(activity);
    }

    public void proceed(final PaymentDetails paymentDetails, final Callback callback){
        this.paymentDetails = paymentDetails;
        this.callback = callback;
        checkTransactionIDAndProceed();
    }
    public void proceed(final OrderDetails orderDetails, final double amount, final Callback callback){
        this.paymentDetails = new PaymentDetails(orderDetails).setAmount(amount);
        this.callback = callback;
        checkTransactionIDAndProceed();
    }
    public void proceed(final List<OrderDetails> ordersList, final Callback callback){
        double amount = 0;
        for (int i=0;i<ordersList.size();i++){
            amount += ordersList.get(i).getTotalAmount();
        }
        this.paymentDetails = new PaymentDetails(ordersList.get(0)).setAmount(amount);
        this.callback = callback;
        checkTransactionIDAndProceed();
    }

    private void checkTransactionIDAndProceed(){
        if ( paymentDetails.getTransactionID() != null && paymentDetails.getTransactionID().length() > 0 ){
            goToPaytmPayment();
        }else{
            activity.showLoadingDialog("Initiating payment, wait...");
            activity.getBackendAPIs().getPaymentAPI().getTransactionID(new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String transactionID) {
                    activity.closeLoadingDialog();
                    if ( status ){
                        paymentDetails.setTransactionID(transactionID);
                        goToPaytmPayment();
                    }else{
                        activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while initiating payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("RETRY")){
                                    checkTransactionIDAndProceed();
                                }
                            }
                        });//
                    }
                }
            });
        }
    }

    private void goToPaytmPayment(){

        String mobileNumber = paymentDetails.getMobile();
        if ( localStorageData.getBuyerMobile() != null && localStorageData.getBuyerMobile().length() > 0 ){
            mobileNumber = localStorageData.getBuyerMobile();
        }
        String emailAddress = paymentDetails.getEmail();
        if ( localStorageData.getBuyerEmail() != null && localStorageData.getBuyerEmail().length() > 0 ){
            emailAddress = localStorageData.getBuyerEmail();
        }

        //Getting the Service Instance. PaytmPGService.getStagingService()  will return the Service pointing to Staging Environment and PaytmPGService.getProductionService() will return the Service pointing to Production Environment.
        PaytmPGService Service = null;
        if ( activity.getString(R.string.app_mode).equalsIgnoreCase("development") ){
            Service = PaytmPGService.getStagingService();
        }else if ( activity.getString(R.string.app_mode).equalsIgnoreCase("production") ){
            Service = PaytmPGService.getProductionService();
        }

        //Create new order Object having all order information.
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("REQUEST_TYPE", "DEFAULT");
        paramMap.put("ORDER_ID", paymentDetails.getTransactionID());
        paramMap.put("MID", activity.getString(R.string.paytm_merchant_id));
        paramMap.put("CUST_ID", mobileNumber);//localStorageData.getBuyerID());
        paramMap.put("MOBILE_NO", mobileNumber);
        paramMap.put("EMAIL", emailAddress);

        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("INDUSTRY_TYPE_ID", activity.getString(R.string.paytm_industry_type_id));
        paramMap.put("WEBSITE", activity.getString(R.string.paytm_website));
        paramMap.put("TXN_AMOUNT", paymentDetails.getAmount()+"");
        paramMap.put("THEME", "merchant");

        PaytmOrder Order = new PaytmOrder(paramMap);
        //Create new Merchant Object having all merchant configuration.
        PaytmMerchant Merchant = new PaytmMerchant(activity.getString(R.string.paytm_checksum_url), activity.getString(R.string.paytm_checksum_verification_url));

        //Set PaytmOrder and PaytmMerchant objects. Call this method and set both objects before starting transaction.
        Service.initialize(Order, Merchant, null);

        //Start the Payment Transaction. Before starting the transaction ensure that initialize method is called.
        Service.startPaymentTransaction(activity, true, true, new PaytmPaymentTransactionCallback() {
            @Override
            public void onTransactionSuccess(Bundle bundle) {
                if ( callback != null ){
                    //BUNDLE :  {STATUS=TXN_SUCCESS, BANKNAME=, ORDERID=TC66221216130456688, TXNAMOUNT=67.00, TXNDATE=2016-12-22 13:15:57.0, MID=xbPHdM37714236449257, TXNID=32150548, RESPCODE=01, PAYMENTMODE=PPI, BANKTXNID=419293, CURRENCY=INR, GATEWAYNAME=WALLET, IS_CHECKSUM_VALID=Y, RESPMSG=Txn Successful.}]
                    callback.onPaymentSuccess("Payment success", Constants.PAYTM_WALLET_CODE, paymentDetails.getAmount(), paymentDetails.getTransactionID(), bundle.getString("TXNID"));
                }
            }

            @Override
            public void onTransactionFailure(String s, Bundle bundle) {
                if ( callback != null ){
                    callback.onPaymentFailure("Failure: "+s, paymentDetails.getAmount(), null);
                }
            }

            @Override
            public void networkNotAvailable() {
                if ( callback != null ){
                    callback.onPaymentFailure("Failure: Network not available", paymentDetails.getAmount(), null);
                }
            }

            @Override
            public void clientAuthenticationFailed(String s) {
                if ( callback != null ){
                    callback.onPaymentFailure("Failure: Authentication failed", paymentDetails.getAmount(), null);
                }
            }

            @Override
            public void someUIErrorOccurred(String s) {
                if ( callback != null ){
                    callback.onPaymentFailure("Failure: Some UI error occurred", paymentDetails.getAmount(), null);
                }
            }

            @Override
            public void onErrorLoadingWebPage(int i, String s, String s1) {
                if ( callback != null ){
                    callback.onPaymentFailure("Failure: Error loading web page", paymentDetails.getAmount(), null);
                }
            }

            @Override
            public void onBackPressedCancelTransaction() {
                if ( callback != null ){
                    callback.onPaymentCancel();
                }
            }
        });
    }



}
