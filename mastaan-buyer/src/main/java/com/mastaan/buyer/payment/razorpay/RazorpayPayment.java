package com.mastaan.buyer.payment.razorpay;

import android.util.Log;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.mastaan.buyer.R;
import com.mastaan.buyer.activities.MastaanToolBarActivity;
import com.mastaan.buyer.backend.PaymentAPI;
import com.mastaan.buyer.backend.models.RequestCreateRazorpayOrder;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.payment.models.PaymentDetails;
import com.razorpay.PaymentData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public class RazorpayPayment {
    MastaanToolBarActivity activity;
    String type;
    PaymentDetails paymentDetails;
    Callback callback;

    String ORDER = "order";
    String GROUPED_ORDER = "grouped_order";
    String OUTSTANDING = "outstanding";


    public interface Callback {
        void onPaymentSuccess(String message, int paymentType, double amount, String transactionID, String paymentID);
        void onPaymentFailure(String message, double amount, String newTransactioinID);
        void onPaymentCancel();

        void onExternalWalletSelect(String walletName, double amount);
    }


    public RazorpayPayment(MastaanToolBarActivity activity){
        this.activity = activity;
    }

    public void proceedForOutstanding(final PaymentDetails paymentDetails, final Callback callback){
        this.type = OUTSTANDING;
        this.paymentDetails = paymentDetails;
        this.callback = callback;
        checkTransactionIDAndProceed();
    }
    public void proceedForOrder(final OrderDetails orderDetails, final double amount, final Callback callback){
        this.type = ORDER;
        this.paymentDetails = new PaymentDetails(orderDetails).setAmount(amount);
        this.callback = callback;
        checkTransactionIDAndProceed();
    }
    public void proceedForGroupedOrder(final List<OrderDetails> ordersList, final Callback callback){
        double amount = 0;
        for (int i=0;i<ordersList.size();i++){
            amount += ordersList.get(i).getCODAmount();
        }
        this.type = GROUPED_ORDER;
        this.paymentDetails = new PaymentDetails(ordersList.get(0)).setOrderID(ordersList.get(0).getGroupOrderID()).setAmount(amount);
        this.callback = callback;
        checkTransactionIDAndProceed();
    }

    private void checkTransactionIDAndProceed(){
        if ( paymentDetails.getTransactionID() != null && paymentDetails.getTransactionID().length() > 0 ){
            createRazorpayOrderAndProceed(true);
        }
        else{
            activity.showLoadingDialog("Initiating payment, wait...");
            activity.getBackendAPIs().getPaymentAPI().getTransactionID(new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String transactionID) {
                    if ( status ){
                        paymentDetails.setTransactionID(transactionID);
                        createRazorpayOrderAndProceed();
                    }else{
                        activity.closeLoadingDialog();
                        activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while initiating payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("RETRY")){
                                    checkTransactionIDAndProceed();
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    private void createRazorpayOrderAndProceed(){
        createRazorpayOrderAndProceed(false);
    }
    private void createRazorpayOrderAndProceed(final boolean showLoadingDialog){
        if ( type.equalsIgnoreCase(OUTSTANDING) ){
            goToRazorpayPayment(null);      // TODO Handle Auto Capture for Outstanding payments
        }
        else{
            if ( showLoadingDialog ){   activity.showLoadingDialog("Initiating payment, wait...");  }
            activity.getBackendAPIs().getPaymentAPI().createRazorpayOrder(type, new RequestCreateRazorpayOrder(paymentDetails.getOrderID(), Math.round(paymentDetails.getAmount() * 100), "INR", true), new PaymentAPI.RazorPayOrderCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String razorpayOrderID) {
                    /*if (showLoadingDialog) {    */activity.closeLoadingDialog();/*  }*/
                    if ( status ){
                        goToRazorpayPayment(razorpayOrderID);
                    }
                    else{
                        activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while initiating payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("RETRY")){
                                    createRazorpayOrderAndProceed(true);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    private void goToRazorpayPayment(final String razorpayOrderID){
        try {
            RazorPayCheckout razorPayCheckout = new RazorPayCheckout();
            razorPayCheckout.setCallback(new RazorPayCheckout.RazorPayCallback() {
                @Override
                public void onSuccess(String razorpayPaymentID) {
                    Log.d(Constants.LOG_TAG, "{RAZORPAY} , Payment success, pID: " + razorpayPaymentID);
                    captureRazorpayPayment(razorpayPaymentID);
                }

                @Override
                public void onFailure(int statusCode, String message) {
                    Log.d(Constants.LOG_TAG, "{RAZORPAY} , Payment failed, sC: " + statusCode + "Msg: "+message);
                    if ( callback != null ){
                        callback.onPaymentFailure("Failed", paymentDetails.getAmount(), null);
                    }
                }
            });

            JSONObject prefillDetails = new JSONObject();
            prefillDetails.put("contact", paymentDetails.getMobile());          //  MOBILE
            prefillDetails.put("email", paymentDetails.getEmail());             //  EMAIL
            prefillDetails.put("name", paymentDetails.getName());               //  NAME

            JSONObject additionalDetails = new JSONObject();
            prefillDetails.put("udf1", activity.getPackageName());                  //  PACKAGE ANME
            prefillDetails.put("udf2", activity.localStorageData.getBuyerID());     //  BUYER ID
            prefillDetails.put("udf3", activity.getDeviceID());                     //  DEVICE ID
            prefillDetails.put("txnid", paymentDetails.getTransactionID());         //  TRANSACTION ID

            JSONArray wallets = new JSONArray();
            wallets.put("paytm");
            JSONObject externalWallets = new JSONObject();
            externalWallets.put("wallets", wallets);

            JSONObject requestObject = new JSONObject();
            requestObject.put("name", "MASTAAN");                                       //  MERCHANT NAME
            requestObject.put("description", "For Aleena E-Commerce Pvt. Ltd.");        //  DESCRIPTION
            requestObject.put("image", "http://www.gocibo.in/images/logo-gocibo.png");  //  LOGO IMAGE URL
            requestObject.put("order_id", razorpayOrderID);                                   //  ORDER ID
            requestObject.put("currency", "INR");                                       //  CURRENCY TYPE
            requestObject.put("amount",  ((long)paymentDetails.getAmount()*100));               //  AMOUNT
            requestObject.put("prefill", prefillDetails);                                     //  PREFIL DETAILS
            requestObject.put("notes", additionalDetails);                                    //  ADDITIONAL DATA
            requestObject.put("external", externalWallets);                                   //  EXTERNAL WALLETS

            razorPayCheckout.setImage(R.mipmap.ic_launcher);
            razorPayCheckout.open(activity, requestObject);
        } catch(Exception e){
            activity.showToastMessage("Payment exception");
        }
    }

    private void captureRazorpayPayment(final String paymentID){
        activity.showLoadingDialog("Processing payment...");
        activity.getBackendAPIs().getPaymentAPI().captureRazorpayPayment(paymentID, paymentDetails.getAmount()*100, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    if ( callback != null ){
                        callback.onPaymentSuccess("Success", Constants.ONLINE_PAYMENT_CODE, paymentDetails.getAmount(), paymentDetails.getTransactionID(), paymentID);
                    }
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while processing payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                captureRazorpayPayment(paymentID);
                            }
                        }
                    });
                }
            }
        });
    }

    //=========

    public void onExternalWalletSelected(String walletName, PaymentData razorpayPaymentData){
        if ( callback != null && paymentDetails != null && walletName != null ){
            callback.onExternalWalletSelect(walletName, paymentDetails.getAmount());
        }
    }

}
