package com.mastaan.buyer.payment.razorpay;

import com.razorpay.Checkout;

public class RazorPayCheckout extends Checkout {

  RazorPayCallback razorPayCallback;

  public interface RazorPayCallback{
    void onSuccess(String razorpayPaymentID);
    void onFailure(int statusCode, String message);
  }

  // override onSuccess method to capture razorpay_payment_id
  public void onSuccess(String razorpay_payment_id) {
    if (razorPayCallback != null) {
      razorPayCallback.onSuccess(razorpay_payment_id);
    }
  }

  public void onError(int code, String response){
    if (razorPayCallback != null) {
      razorPayCallback.onFailure(code, response);
    }
  }

  public void setCallback(RazorPayCallback razorPayCallback){
    this.razorPayCallback= razorPayCallback;
  }
};
