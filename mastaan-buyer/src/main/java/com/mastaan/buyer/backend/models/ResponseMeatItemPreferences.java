package com.mastaan.buyer.backend.models;

import com.mastaan.buyer.models.CartItemDetails;
import com.mastaan.buyer.models.AttributeDetails;
import com.mastaan.buyer.models.OptionAvailabilities;
import com.mastaan.buyer.models.SelectedAttributeDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 1/6/16.
 */
public class ResponseMeatItemPreferences {

    Item item;
    MeatItem meatItem;
    List<OptionAvailabilities> attributeOptions;

    class Item{
        List<SelectedAttributeDetails> a;
    }
    class MeatItem{
        List<AttributeDetails> ma;
    }


    public List<OptionAvailabilities> getAttributeOptions() {
        if ( attributeOptions != null ){    return attributeOptions;    }
        return new ArrayList<>();
    }

    public CartItemDetails getPreviouesCartItemDetails(){
        if ( item != null && item.a != null && item.a.size() > 0 ) {
            CartItemDetails cartItemDetails = new CartItemDetails();
            cartItemDetails.setSelectedAttributes(item.a);
            return cartItemDetails;
        }
        return null;
    }

    public List<AttributeDetails> getMeatItemAttributesList() {
        if ( meatItem != null ){    return meatItem.ma; }
        return new ArrayList<>();
    }
}
