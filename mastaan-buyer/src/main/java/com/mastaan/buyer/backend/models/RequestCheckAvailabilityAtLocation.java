package com.mastaan.buyer.backend.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 23/10/15.
 */
public class RequestCheckAvailabilityAtLocation {

    String adid;
    double lat;
    double lng;

    public RequestCheckAvailabilityAtLocation(String addressBookID, LatLng latLng){
        this.adid = addressBookID;
        if ( latLng == null ){  latLng = new LatLng(0,0);   }
        this.lat = latLng.latitude;
        this.lng = latLng.longitude;
    }
}
