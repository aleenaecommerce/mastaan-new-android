package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.buyer.backend.models.RequestCheckAvailabilityAtLocation;
import com.mastaan.buyer.backend.models.RequestRegisterForAvailabilityAtLocationUpdates;
import com.mastaan.buyer.backend.models.RequestRegisterForMeatItemAvailabilityUpdates;
import com.mastaan.buyer.backend.models.RequestSlotsForDate;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.backend.models.ResponseFirebaseToken;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.interfaces.MembershipsCallback;
import com.mastaan.buyer.interfaces.MessagesCallback;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.BootStrapDetails;
import com.mastaan.buyer.models.MembershipDetails;
import com.mastaan.buyer.models.MessageDetails;
import com.mastaan.buyer.models.ReferralOfferDetails;
import com.mastaan.buyer.models.SlotDetails;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by venkatesh on 13/7/15.
 */

public class GeneralAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public GeneralAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getFirebaseToken(final FirebaseTokenCallback callback) {

        retrofitInterface.getFireBaseToken(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<ResponseFirebaseToken>() {
            @Override
            public void success(ResponseFirebaseToken responseFirebase, Response response) {
                if (responseFirebase != null && responseFirebase.getFirebaseToken() != null && responseFirebase.getFirebaseToken().length() > 0) {
                    callback.onComplete(true, 200, responseFirebase.getFirebaseToken());
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API:  [FIREBASE_TOKEN] >> Success , Token = " + responseFirebase.getFirebaseToken());
                } else {
                    callback.onComplete(false, 500, null);
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API:  [FIREBASE_TOKEN] >> Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API:  [FIREBASE_TOKEN] >> Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API:  [FIREBASE_TOKEN] >> Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getBootStrap(final BootStrapCallback callback) {
        getBootStrap(localStorageData.getAccessToken(), callback);
    }

    public void getBootStrap(final String accessToken, final BootStrapCallback callback) {

        retrofitInterface.getBootStrap(deviceID, appVersion, accessToken, new Callback<BootStrapDetails>() {
            @Override
            public void success(BootStrapDetails bootStrapDetails, Response response) {
                if (bootStrapDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ BOOTSTRAP ]  >>  Success");

                    callback.onComplete(true, 200, "Succes", bootStrapDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ BOOTSTRAP ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).ignoreMaintenance().check(error) == false) {
                    try {
                        String message = "Error";
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);
                        try {
                            if (jsonResponse.getBoolean("maintenance")) {
                                message = jsonResponse.getString("message");
                            }
                        } catch (Exception e) {
                        }
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ BOOTSTRAP ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), message, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ BOOTSTRAP ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void checkAvailabilityAtLocation(final String addressBookID, final LatLng latLng, final CheckAvailabilityAtLocationCallback callback) {

        retrofitInterface.checkAvailabilityAtLocation(deviceID, appVersion, localStorageData.getAccessToken(), new RequestCheckAvailabilityAtLocation(addressBookID, latLng), new Callback<ResponseCheckAvailabilityAtLocation>() {
            @Override
            public void success(ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation, Response response) {

                if (responseCheckAvailabilityAtLocation != null) {

                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ CHECK_AVAILABILITY_AT_LOCATION ]  >>  Success , isAvail = " + responseCheckAvailabilityAtLocation.isAvailable() + " at Loc = " + latLng);
                    callback.onComplete(true, 200, "Success", responseCheckAvailabilityAtLocation);
                } else {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ CHECK_AVAILABILITY_AT_LOCATION ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ CHECK_AVAILABILITY_AT_LOCATION ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ CHECK_AVAILABILITY_AT_LOCATION ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void changeLocation(final String addressBookID, final LatLng latLng, final CheckAvailabilityAtLocationCallback callback) {

        retrofitInterface.changeLocation(deviceID, appVersion, localStorageData.getAccessToken(), new RequestCheckAvailabilityAtLocation(addressBookID, latLng), new Callback<ResponseCheckAvailabilityAtLocation>() {
            @Override
            public void success(ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation, Response response) {

                if (responseCheckAvailabilityAtLocation != null) {
                    Log.e("deviceID", "" + deviceID);
                    Log.e("appVersion", "" + appVersion);
                    Log.e("accessToken", "" + localStorageData.getAccessToken());
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ CHANGE_LOCATION ]  >>  Success , isAvail = " + responseCheckAvailabilityAtLocation.isAvailable() + " at Loc = " + latLng);
                    callback.onComplete(true, 200, "Success", responseCheckAvailabilityAtLocation);
                } else {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ CHANGE_LOCATION ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ CHANGE_LOCATION ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ CHANGE_LOCATION ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void registerForAvailabilityAtLocationUpdates(RequestRegisterForAvailabilityAtLocationUpdates requestObject, final StatusCallback callback) {

        retrofitInterface.registerForAvailabilityAtLocationUpdates(deviceID, appVersion, localStorageData.getAccessToken(), requestObject, new Callback<JSONObject>() {
            @Override
            public void success(JSONObject jsonObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ REGISTER_FOR_AVAILABILITY_AT_LOCATION_UPDATES ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ REGISTER_FOR_AVAILABILITY_AT_LOCATION_UPDATES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ REGISTER_FOR_AVAILABILITY_AT_LOCATION_UPDATES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void registerForMeatItemAvailabilityUpdates(String warehouseMeatItemID, final StatusCallback callback) {

        retrofitInterface.registerForMeatAvailabilityUpdates(deviceID, appVersion, localStorageData.getAccessToken(), new RequestRegisterForMeatItemAvailabilityUpdates(warehouseMeatItemID), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ REGISTER_FOR_MEAT_ITEM_AVAILABILITY_UPDATES ]  >>  Success");
                    callback.onComplete(true, 200, "Success");
                } else if (responseStatus.getCode().equalsIgnoreCase("ALREADY_REGISTERED")) {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ REGISTER_FOR_MEAT_ITEM_AVAILABILITY_UPDATES ]  >>  Failure, Msg: Already Registered");
                    callback.onComplete(false, 409, "Already Registered");
                } else {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ REGISTER_FOR_MEAT_ITEM_AVAILABILITY_UPDATES ]  >>  Failure, Msg: " + responseStatus.getMessage());
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ REGISTER_FOR_MEAT_ITEM_AVAILABILITY_UPDATES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : [ REGISTER_FOR_MEAT_ITEM_AVAILABILITY_UPDATES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getSlotsForDate(final String requriedDate, final RequestSlotsForDate requestSlotsForDate, final SlotsForDateCallback callback) {

        Date date = DateMethods.getDateFromString(requriedDate);

        if (date != null) {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            final int day = calendar.get(Calendar.DATE);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);

            retrofitInterface.getSlotsForDate(deviceID, appVersion, localStorageData.getAccessToken(), day, month, year, requestSlotsForDate, new Callback<List<SlotDetails>>() {
                @Override
                public void success(List<SlotDetails> slotsList, Response response) {
                    if (slotsList != null) {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : GET_SLOTS_FOR_DATE : Success");
                        callback.onComplete(true, 200, "Success", requriedDate, slotsList);
                    } else {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : GET_SLOTS_FOR_DATE : Failure");
                        callback.onComplete(false, 500, "Failure", requriedDate, null);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (new CheckError(context).check(error) == false) {
                        try {
                            Log.d(Constants.LOG_TAG, "\nGENERAL_API : GET_SLOTS_FOR_DATE : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                            callback.onComplete(false, error.getResponse().getStatus(), "Error", requriedDate, null);
                        } catch (Exception e) {
                            Log.d(Constants.LOG_TAG, "\nGENERAL_API : GET_SLOTS_FOR_DATE : Error = " + error.getKind());
                            callback.onComplete(false, -1, "Error", requriedDate, null);
                        }
                    }
                }
            });
        } else {
            Log.d(Constants.LOG_TAG, "\nGENERAL_API : BuyerPushNotificationsRegistration : Error = DATE_FORMAT_EXCEPTION");
            callback.onComplete(false, -1, "Error", requriedDate, null);
        }
    }

    public void getAlertMessages(final MessagesCallback callback) {

        retrofitInterface.getAlertMessages(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<List<MessageDetails>>() {
            @Override
            public void success(List<MessageDetails> messagesList, Response response) {
                callback.onComplete(true, 200, "Success", messagesList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getReferralOfferDetails(final ReferralOfferCallback callback) {

        retrofitInterface.getReferralOfferDetails(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<ReferralOfferDetails>() {
            @Override
            public void success(ReferralOfferDetails referralOfferDetails, Response response) {
                callback.onComplete(true, 200, "Success", referralOfferDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getMemberships(final MembershipsCallback callback) {

        retrofitInterface.getMemberships(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<List<MembershipDetails>>() {
            @Override
            public void success(List<MembershipDetails> membershipsList, Response response) {
                callback.onComplete(true, 200, "Success", membershipsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public interface BootStrapCallback {
        void onComplete(boolean status, int statusCode, String message, BootStrapDetails bootStrapDetails);
    }

    public interface ReferralOfferCallback {
        void onComplete(boolean status, int statusCode, String message, ReferralOfferDetails referralOfferDetails);
    }

    public interface CheckAvailabilityAtLocationCallback {
        void onComplete(boolean status, int statusCode, String message, ResponseCheckAvailabilityAtLocation responseCheckAvailabilityAtLocation);
    }

    public interface FirebaseTokenCallback {
        void onComplete(boolean status, int statusCode, String firebaseToken);
    }

    public interface SlotsForDateCallback {
        void onComplete(boolean status, int statusCode, String message, String loadedDate, List<SlotDetails> slotsList);
    }

}


/*

 public void registerForPushNotifications(final StatusCallback callback){

        retrofitInterface.registerForPushNotifications(deviceID, appVersion, localStorageData.getAccessToken(), new RequestRegisterPushNotifications(localStorageData.getGCMRegistrationID()), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK") == true) {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : PushNotificationsRegistration : Success , for (gcmRegID=" + localStorageData.getGCMRegistrationID() + ")");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : PushNotificationsRegistration : Failure ,  for (gcmRegID=" + localStorageData.getGCMRegistrationID() + ")");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : PushNotificationsRegistration : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : PushNotificationsRegistration : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void registerBuyerForPushNotifications(final StatusCallback callback){

        retrofitInterface.registerBuyerForPushNotifications(deviceID, appVersion, localStorageData.getAccessToken(), new RequestRegisterPushNotifications(localStorageData.getGCMRegistrationID()), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK") == true) {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : BuyerPushNotificationsRegistration : Success , for (gcmRegID=" + localStorageData.getGCMRegistrationID() + ")");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nGENERAL_API : BuyerPushNotificationsRegistration : Failure ,  for (gcmRegID=" + localStorageData.getGCMRegistrationID() + ")");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : BuyerPushNotificationsRegistration : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nGENERAL_API : BuyerPushNotificationsRegistration : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

 */