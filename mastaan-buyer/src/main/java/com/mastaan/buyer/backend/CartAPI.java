package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.buyer.backend.models.RequestAddToCart;
import com.mastaan.buyer.backend.models.RequestUpdateCartItem;
import com.mastaan.buyer.backend.models.RequestUpdateCartItemSpecialInstructions;
import com.mastaan.buyer.backend.models.ResponseAddToCart;
import com.mastaan.buyer.backend.models.ResponseCart;
import com.mastaan.buyer.backend.models.ResponseDeleteCartItem;
import com.mastaan.buyer.backend.models.ResponseUpdateCartItem;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.CartItemDetails;
import com.mastaan.buyer.models.CartTotalDetails;
import com.mastaan.buyer.models.WalletDetails;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by venkatesh on 13/7/15.
 */

public class CartAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface CheckoutCartCallback {
        void onComplete(boolean status, int statusCode, String message, boolean refreshCart, String cartMessage);
    }
    public interface CartCallback {
        void onComplete(boolean status, int statusCode, String message, String serverTime, List<CartItemDetails> cartItemsList, CartTotalDetails cartTotalDetails, WalletDetails walletDetails, String transactionID);
    }

    public interface AddToCartCallback{
        void onComplete(boolean status, int statusCode, String message, boolean isAlreadyPresent);
    }

    public interface UpdateCartItemCallback{
        void onComplete(boolean status, int statusCode, String message, String newHash, String newDeliveryDate, double newPrice, CartTotalDetails cartTotalDetails);
    }

    public interface DeleteCartItemCallback {
        void onComplete(boolean status, int statusCode, String message, CartTotalDetails cartTotalDetails);
    }

    public CartAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    //==========

    public void checkoutCart(final CheckoutCartCallback callback){

        retrofitInterface.checkoutCart(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nCART_API : [ CHECKOUT_CART ]  >>  Success");
                callback.onComplete(true, 200, "Success", false, null);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        String code = "";
                        String message = "";
                        try{
                            JSONObject response = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                            message = response.getString("msg");
                            code = response.getString("code");
                        }catch (Exception e){}

                        Log.d(Constants.LOG_TAG, "\nCART_API : [ CHECKOUT_CART ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", (code!=null&&code.toLowerCase().contains("no_stock_item"))?true:false, message);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ CHECKOUT_CART ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", false, null);
                    }
                }
            }
        });
    }

    public void getCart(final CartCallback callback){

        retrofitInterface.getCart(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<ResponseCart>() {
            @Override
            public void success(final ResponseCart responseObject, Response response) {
                if ( responseObject.getCartItems() != null ){
                    Log.d("MastaanLogs", "\nCART_API : [ CART ]  >>  Success");
                    callback.onComplete(true, 200, responseObject.getMessage(), responseObject.getServerTime(), responseObject.getCartItems(), responseObject.getCartTotalDetails(), responseObject.getWalletDetails(), responseObject.getTransactionID());
                }else{
                    Log.d(Constants.LOG_TAG, "\nCART_API : [ CART ]  >>  Failure");
                    callback.onComplete(false, 500, "", null, null, null, null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ CART ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "", null, null, null, null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ CART ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "", null, null, null, null, null);
                    }
                }
            }
        });
    }

    public void clearCart(final StatusCallback callback){

        retrofitInterface.clearCart(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {

                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nCART_API : [ CLEAR_CART ]  >>  Success");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nCART_API : [ CLEAR_CART ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ GET_CART_ITEMS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ GET_CART_ITEMS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    //==========

    public void addToCart(final RequestAddToCart requestAddToCart, final AddToCartCallback callback){

        retrofitInterface.addToCart(deviceID, appVersion, localStorageData.getAccessToken(), requestAddToCart, new Callback<ResponseAddToCart>() {
            @Override
            public void success(ResponseAddToCart responseAddToCart, Response response) {

                if (responseAddToCart != null && responseAddToCart.getCode() != null && responseAddToCart.getCode().equals("OK")) {
                    Log.d(Constants.LOG_TAG, "\nCART_API : [ ADD_TO_CART ]  >>  Success");
                    callback.onComplete(true, 200, responseAddToCart.getMessage(), responseAddToCart.isAlreadyPresent());
                } else {
                    Log.d(Constants.LOG_TAG, "\nCART_API : [ ADD_TO_CART ]  >>  Failure , with Message = " + responseAddToCart.getCode());
                    callback.onComplete(false, 500, "", false);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    String message = "";
                    try{new String(((TypedByteArray) error.getResponse().getBody()).getBytes());}catch (Exception e){e.printStackTrace();}
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ ADD_TO_CART ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), message, false);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ ADD_TO_CART ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, message, false);
                    }
                }
            }
        });
    }

    public void updateCartItem(final String cartItemID, final RequestUpdateCartItem requestUpdateCartItem, final UpdateCartItemCallback callback){

        retrofitInterface.updateCartItem(deviceID, appVersion, localStorageData.getAccessToken(), cartItemID, requestUpdateCartItem, new Callback<ResponseUpdateCartItem>() {
            @Override
            public void success(ResponseUpdateCartItem responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nCART_API : [ UPDATE_CART_ITEM ]  >>  Success");
                callback.onComplete(true, 200, responseObject.getMessage(), responseObject.getUpdatedHash(), responseObject.getUpdatedDeliveryDate(), responseObject.getUpdatedPrice(), responseObject.getCartTotalDetails());
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ UPDATE_CART_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "", "", "", 0, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ UPDATE_CART_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "", "", "", 0, null);
                    }
                }
            }
        });
    }

    public void updateCartItemSpecialInstructions(final String cartItemID, final String specialInstructions, final StatusCallback callback){

        retrofitInterface.updateCartItemSpecialInstructions(deviceID, appVersion, localStorageData.getAccessToken(), cartItemID, new RequestUpdateCartItemSpecialInstructions(specialInstructions), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nCART_API : [ UPDATE_CART_ITEM_SPECIAL_INSTRUCTIONS ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ UPDATE_CART_ITEM_SPECIAL_INSTRUCTIONS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ UPDATE_CART_ITEM_SPECIAL_INSTRUCTIONS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void deleteCartItem(final String cartItemID, final DeleteCartItemCallback callback){

        retrofitInterface.deleteCartItem(deviceID, appVersion, localStorageData.getAccessToken(), cartItemID, new Callback<ResponseDeleteCartItem>() {
            @Override
            public void success(ResponseDeleteCartItem responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nCART_API : [ DELETE_CART_ITEM ]  >>  Success");
                callback.onComplete(true, 200, responseObject.getMessage(), responseObject.getCartTotalDetails());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ DELETE_CART_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCART_API : [ DELETE_CART_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "", null);
                    }
                }
            }
        });
    }


}
