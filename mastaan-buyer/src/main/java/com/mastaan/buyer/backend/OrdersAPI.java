package com.mastaan.buyer.backend;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.buyer.backend.models.RequestCancel;
import com.mastaan.buyer.backend.models.RequestPlaceOrder;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.models.OrderItemDetails;

import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by venkatesh on 13/7/15.
 */

public class OrdersAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface PlaceOrderCallback {
        void onComplete(boolean status, int statusCode, String message, String errorCode, List<OrderDetails> ordersList);
    }

    public interface OrderDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, OrderDetails orderDetails);
    }

    public interface OrderItemDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails);
    }

    public interface GroupedOrderDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList);
    }

    public interface OrdersListCallback {
        void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList);
    }

    public interface PendingOrdersCallback {
        void onComplete(boolean status, int statusCode, String message, List<OrderDetails> pendingOrders);
    }

    public OrdersAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void placeOrder(final RequestPlaceOrder requestPlaceOrder, final PlaceOrderCallback callback){

        retrofitInterface.placeOrder(deviceID, appVersion, localStorageData.getAccessToken(), requestPlaceOrder, new Callback<List<OrderDetails>>() {
            @Override
            public void success(List<OrderDetails> ordersList, Response response) {

                if (ordersList != null && ordersList.size() > 0) {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ PLACE_ORDER ]  >>  Success");
                    callback.onComplete(true, 200, "Success", null, ordersList);
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ PLACE_ORDER ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        String code = "";
                        String message = "Error";
                        try{
                            JSONObject response = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()));
                            message = response.getString("msg");
                            code = response.getString("code");
                        }catch (Exception e){}

                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ PLACE_ORDER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), message, code, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ PLACE_ORDER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }


    public void cancelOrder(String orderID, String cancellationReason, final StatusCallback callback){

        retrofitInterface.cancelOrder(deviceID, appVersion, localStorageData.getAccessToken(), orderID, new RequestCancel(cancellationReason), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nORDER_API : [ CACNEL_ORDER ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        JSONObject jsonResponse = null;
                        try{    jsonResponse = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())); }catch (Exception e){}
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ CACNEL_ORDER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), (jsonResponse!=null&&jsonResponse.getString("msg")!=null)?jsonResponse.getString("msg"):"Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ CACNEL_ORDER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


    public void checkForCancelOrderItem(String orderItemID, final StatusCallback callback){

        retrofitInterface.checkForCancelOrderItem(deviceID, appVersion, localStorageData.getAccessToken(), orderItemID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if ( responseStatus.getCode().equalsIgnoreCase("OK") ){
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ CHECK_FOR_CACNEL_ORDER_ITEM ]  >>  Success");
                    callback.onComplete(true, 200, "Success");
                }else{
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ CHECK_FOR_CACNEL_ORDER_ITEM ]  >>  Failure");
                    callback.onComplete(false, 500, responseStatus.getMessage());
                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ CHECK_FOR_CACNEL_ORDER_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ CHECK_FOR_CACNEL_ORDER_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void cancelOrderItem(String orderItemID, String cancellationReason, final StatusCallback callback){

        retrofitInterface.cancelOrderItem(deviceID, appVersion, localStorageData.getAccessToken(), orderItemID, new RequestCancel(cancellationReason), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nORDER_API : [ CACNEL_ORDER_ITEM ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        JSONObject jsonResponse = null;
                        try{    jsonResponse = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())); }catch (Exception e){}
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ CACNEL_ORDER_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), (jsonResponse!=null&&jsonResponse.getString("msg")!=null)?jsonResponse.getString("msg"):"Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ CACNEL_ORDER_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


    public void getOrderDetails(final String orderID, final OrderDetailsCallback callback){

        retrofitInterface.getOrderDetails(deviceID, appVersion, localStorageData.getAccessToken(), orderID, new Callback<OrderDetails>() {
            @Override
            public void success(OrderDetails orderDetails, Response response) {

                if (orderDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDER_DETAILS ]  >>  Success , For orderID = " + orderID);
                    callback.onComplete(true, 200, "Success", orderDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDER_DETAILS ]  >>  Failure");
                    callback.onComplete(false, -1, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDER_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDER_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getOrderItemDetails(final String orderItemID, final OrderItemDetailsCallback callback){

        retrofitInterface.getOrderItemDetails(deviceID, appVersion, localStorageData.getAccessToken(), orderItemID, new Callback<OrderItemDetails>() {
            @Override
            public void success(OrderItemDetails orderItemDetails, Response response) {
                if (orderItemDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDER_ITEM_DETAILS ]  >>  Success");
                    callback.onComplete(true, 200, "Success", orderItemDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDER_ITEM_DETAILS ]  >>  Failure");
                    callback.onComplete(false, -1, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDER_ITEM_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDER_ITEM_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getOrders(final OrdersListCallback callback){

        retrofitInterface.getOrders(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> ordersList, Response response) {

                if (ordersList != null) {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDERS ]  >>  Success");
                    callback.onComplete(true, 200, "Success", ordersList);
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDERS ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDERS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ ORDERS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getPendingOrders(final PendingOrdersCallback callback){

        retrofitInterface.getPendingOrders(deviceID, appVersion, localStorageData.getAccessToken(), new retrofit.Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> pendingOrders, Response response) {

                if (pendingOrders != null) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            Collections.sort(pendingOrders, new Comparator<OrderDetails>() {
                                public int compare(OrderDetails item1, OrderDetails item2) {
                                    return item1.getDeliveryDate().compareToIgnoreCase(item2.getDeliveryDate());
                                }
                            });
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            Log.d(Constants.LOG_TAG, "\nORDERS_API - PendingOrders : Success , Found = " + pendingOrders.size());
                            callback.onComplete(true, 200, "Success", pendingOrders);
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } else {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API - PendingOrders : Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - PendingOrders : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API - PendingOrders : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }






    //-----

    public void getGroupedOrder(final String orderID, final GroupedOrderDetailsCallback callback){

        retrofitInterface.getGroupedOrderDetails(deviceID, appVersion, localStorageData.getAccessToken(), orderID, new Callback<List<OrderDetails>>() {
            @Override
            public void success(List<OrderDetails> ordersList, Response response) {

                if (ordersList != null && ordersList.size() > 0) {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ GROUPED_ORDER_DETAILS ]  >>  Success");
                    callback.onComplete(true, 200, "Success", ordersList);
                } else {
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ GROUPED_ORDER_DETAILS ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ GROUPED_ORDER_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ GROUPED_ORDER_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }


}
