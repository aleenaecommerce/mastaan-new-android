package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.buyer.backend.models.ResponeValidateMobile;
import com.mastaan.buyer.backend.models.ResponseConnectBuyer;
import com.mastaan.buyer.backend.models.ResponseValidateEmail;
import com.mastaan.buyer.backend.models.ResponseVerifyEmail;
import com.mastaan.buyer.backend.models.ResponseVerifyMobile;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.BuyerDetails;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by venkatesh on 13/7/15.
 */

public class AuthenticationAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface MobileValidationCallback {
        void onComplete(boolean status, int statusCode, ResponeValidateMobile responeValidateMobile);
    }

    public interface MobileVerificationCallback {
        void onComplete(boolean status, int statusCode, String accessToken);
    }

    public interface BuyerConnectCallback {
        void onComplete(boolean status, int statusCode, String message, ResponseConnectBuyer responseConnectBuyer);
    }

    public interface EmailValidationCallback {
        void onComplete(boolean status, int statusCode, String message, ResponseValidateEmail responseValidateEmail);
    }

    public interface EmailVerificationCallback {
        void onComplete(boolean status, int statusCode, String message, ResponseVerifyEmail responseVerifyEmail);
    }

    public interface BuyerDetailsCallback {
        void onComplete(boolean status, int statusCode, BuyerDetails buyerDetails);
    }


    public AuthenticationAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void validatePhoneNumber(final String mobileNumber, final MobileValidationCallback callback){

        retrofitInterface.validatePhoneNumber1(deviceID, appVersion, localStorageData.getAccessToken(), mobileNumber, new Callback<ResponeValidateMobile>() {
            @Override
            public void success(ResponeValidateMobile responeValidateMobile, Response response) {

                if (responeValidateMobile.getError() != null && responeValidateMobile.getError().length() > 0) {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-validateNumber1 : Success , FailureMsg=" + responeValidateMobile.getError());
                    callback.onComplete(false, 200, responeValidateMobile);
                } else {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-validateNumber1 : Success , SMSID=" + responeValidateMobile.getSmsID());
                    callback.onComplete(true, 500, responeValidateMobile);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-validateNumber1 : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), new ResponeValidateMobile());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-validateNumber1 : Error = " + error.getKind());
                        callback.onComplete(false, -1, new ResponeValidateMobile());
                    }
                }
            }
        });
    }

    public void verifyPhoneNumber(final String smsID, final String otp, final MobileVerificationCallback callback){

        retrofitInterface.verifyPhoneNumber1(deviceID, appVersion, localStorageData.getAccessToken(), smsID, otp, new Callback<ResponseVerifyMobile>() {
            @Override
            public void success(ResponseVerifyMobile responseVerifyMobile, Response response) {
                Log.d(Constants.LOG_TAG, "\nAuthentication-verifyNumber1 : Success , Token=" + responseVerifyMobile.getToken());
                callback.onComplete(true, 200, responseVerifyMobile.getToken()!=null?responseVerifyMobile.getToken():localStorageData.getAccessToken());
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-verifyNumber1 : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-verifyNumber1 : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    //================================

    public void validateEmail(final String emailAddress, final EmailValidationCallback callback){

        retrofitInterface.validateEmail(deviceID, appVersion, localStorageData.getAccessToken(), emailAddress, new Callback<ResponseValidateEmail>() {
            @Override
            public void success(ResponseValidateEmail responseValidateEmail, Response response) {

                if (responseValidateEmail != null && responseValidateEmail.getPinID() != null && responseValidateEmail.getPinID().length() > 0) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Success , PINID=" + responseValidateEmail.getPinID());
                    callback.onComplete(true, 200, "OK", responseValidateEmail);
                } else {
                    if (responseValidateEmail != null && responseValidateEmail.getCode() != null && responseValidateEmail.getCode().length() > 0) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Failure, Message = " + responseValidateEmail.getCode());
                        callback.onComplete(false, 500, responseValidateEmail.getCode(), null);
                    } else {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Failure");
                        callback.onComplete(false, 500, "Failure", null);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", new ResponseValidateEmail());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - validateEmail : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", new ResponseValidateEmail());
                    }
                }
            }
        });
    }

    public void verifyEmail(final String pinID, final String pin, final EmailVerificationCallback callback){

        retrofitInterface.verifyEmail(deviceID, appVersion, localStorageData.getAccessToken(), pinID, pin, new Callback<ResponseVerifyEmail>() {
            @Override
            public void success(ResponseVerifyEmail responseVerifyEmail, Response response) {

                if (responseVerifyEmail != null && responseVerifyEmail.getToken() != null && responseVerifyEmail.getToken().length() > 0 && responseVerifyEmail.getBuyerDetails() != null) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Success , Token=" + responseVerifyEmail.getToken());
                    callback.onComplete(true, 200, "OK", responseVerifyEmail);
                } else {
                    if (responseVerifyEmail != null && responseVerifyEmail.getCode() != null && responseVerifyEmail.getCode().length() > 0) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Failure, Message = " + responseVerifyEmail.getCode());
                        callback.onComplete(false, 500, responseVerifyEmail.getCode(), responseVerifyEmail);
                    } else {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Failure");
                        callback.onComplete(false, 500, "Failure", responseVerifyEmail);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", new ResponseVerifyEmail());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API - verifyEmail : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", new ResponseVerifyEmail());
                    }
                }
            }
        });
    }

    public void connectBuyer(final String accessToken, final BuyerConnectCallback callback){

        retrofitInterface.connectBuyer(deviceID, appVersion, accessToken, new Callback<ResponseConnectBuyer>() {
            @Override
            public void success(ResponseConnectBuyer responseConnectBuyer, Response response) {

                if (responseConnectBuyer != null && responseConnectBuyer.getBuyerDetails() != null && responseConnectBuyer.getBuyerDetails().getMobile() != null && responseConnectBuyer.getBuyerDetails().getMobile().length() > 0) {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-buyerDetails : Success");
                    //localStorageData.setPushNotificationsRegistratinStatus(false);      // Clearing For BuyerPush from JustPush
                    callback.onComplete(true, 200, "Success", responseConnectBuyer);
                } else {
                    Log.d(Constants.LOG_TAG, "\nAuthentication-buyerDetails : Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-buyerDetails : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAuthentication-buyerDetails : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void confirmLogout(final String accessToken, final StatusCallback callback){

        retrofitInterface.confirmLogout(deviceID, appVersion, accessToken, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                //if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ LOGOUT ]  >>  Success for (a=" + accessToken + ")");
                    localStorageData.setPushNotificationsRegistratinStatus(false);      // Clearing For JustPush from BuyerPush
                    callback.onComplete(true, 200, "Success");
                /*} else {
                    Log.d(Constants.LOG_TAG, "nCOMMON_API : [ LOGOUT ]  >>  Failure for (a=" + accessToken + ")");
                    callback.onComplete(false, 500, "Failure");
                }*/
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ LOGOUT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ LOGOUT ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}
