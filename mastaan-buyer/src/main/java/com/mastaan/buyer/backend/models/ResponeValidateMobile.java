package com.mastaan.buyer.backend.models;

import java.io.Serializable;

public class ResponeValidateMobile implements Serializable{

    public String id;
    public String type;
    public String code;
    public String error;
    public ValidationInfo validation_info;

    class ValidationInfo{
        public String country_code;
        public String country_iso_code;
        public String carrier;
        public boolean is_mobile;
        public String e164_format;
        public String formatting;
    }

    public ResponeValidateMobile(){
        this.id = "valID";
    }

    public String getSmsID(){  return id;  }
    public String getType() {   return type;    }
    public String getCode(){  return code;  }
    public String getError(){  return error;  }

    public String getFormattedNumber(){  return validation_info.e164_format;  }
    public String getCountryCode(){  return validation_info.country_code;  }
    public String getCarrier(){  return validation_info.carrier;  }
    public boolean getIsMobile(){  return validation_info.is_mobile;  }

}
