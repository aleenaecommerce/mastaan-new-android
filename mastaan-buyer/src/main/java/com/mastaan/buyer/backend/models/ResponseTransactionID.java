package com.mastaan.buyer.backend.models;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public class ResponseTransactionID {
    String tid;

    public String getTransactionID() {
        return tid;
    }
}
