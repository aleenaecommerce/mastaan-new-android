package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.buyer.backend.models.RequestAddOnlinePayment;
import com.mastaan.buyer.backend.models.RequestCaptureRazorpayPayment;
import com.mastaan.buyer.backend.models.RequestCreateRazorpayOrder;
import com.mastaan.buyer.backend.models.ResponseCreateRazorpayOrder;
import com.mastaan.buyer.backend.models.ResponseTransactionID;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by venkatesh on 13/7/15.
 */

public class PaymentAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface CheckPaymentStatusCallback {
        void onComplete(boolean status, int statusCode, String message, boolean paymentStatus, String paymentID, String newTransactionID);
    }

    public interface RazorPayOrderCallback {
        void onComplete(boolean status, int statusCode, String message, String razorpayOrderID);
    }



    public PaymentAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void captureRazorpayPayment(final String paymentID, final double amount, final StatusCallback callback){

        retrofitInterface.captureRazorpayPayment(deviceID, appVersion, localStorageData.getAccessToken(), new RequestCaptureRazorpayPayment(paymentID, amount), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CAPTURE_RAZORPAY ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CAPTURE_RAZORPAY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CAPTURE_RAZORPAY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getTransactionID(final StatusCallback callback){

        retrofitInterface.getTransactionID(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<ResponseTransactionID>() {
            @Override
            public void success(ResponseTransactionID responseTransactionID, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ TRANSACTION_ID ]  >>  Success");
                callback.onComplete(true, 200, responseTransactionID.getTransactionID());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ TRANSACTION_ID ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ TRANSACTION_ID ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void createRazorpayOrder(final String type, final RequestCreateRazorpayOrder requestCreateRazorpayOrder, final RazorPayOrderCallback callback){

        retrofitInterface.createRazorpayOrder(deviceID, appVersion, localStorageData.getAccessToken(), type, requestCreateRazorpayOrder, new Callback<ResponseCreateRazorpayOrder>() {
            @Override
            public void success(ResponseCreateRazorpayOrder responseCreateRazorpayOrder, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CREATE_RAZORPAY_ORDER ]  >>  Success");
                callback.onComplete(true, 200, "Success", responseCreateRazorpayOrder.getID());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CREATE_RAZORPAY_ORDER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CREATE_RAZORPAY_ORDER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addOnlinePaymentForGroupedOrder(String groupedOrderID, RequestAddOnlinePayment requestAddOnlinePayment, final StatusCallback callback){

        retrofitInterface.addOnlinePaymentForGroupedOrder(deviceID, appVersion, localStorageData.getAccessToken(), groupedOrderID, requestAddOnlinePayment, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_FOR_GROUP_ORDER ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_FOR_GROUP_ORDER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_FOR_GROUP_ORDER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void addOnlinePaymentForOrder(String orderID, RequestAddOnlinePayment requestAddOnlinePayment, final StatusCallback callback){

        retrofitInterface.addOnlinePaymentForOrder(deviceID, appVersion, localStorageData.getAccessToken(), orderID, requestAddOnlinePayment, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_FOR_ORDER ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_FOR_ORDER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_FOR_ORDER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void addOnlinePaymentForOutstanding(RequestAddOnlinePayment requestAddOnlinePayment, final StatusCallback callback){

        retrofitInterface.addOnlinePaymentForOutstanding(deviceID, appVersion, localStorageData.getAccessToken(), requestAddOnlinePayment, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_TO_OUTSTANDING ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_TO_OUTSTANDING ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_TO_OUTSTANDING ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


}
