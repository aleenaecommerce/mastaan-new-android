package com.mastaan.buyer.backend.models;


import com.mastaan.buyer.models.BuyerDetails;

import java.io.Serializable;

public class ResponseVerifyEmail implements Serializable{

    public String token;
    public String code;
    public String error;

    boolean signup;
    boolean signin;
    BuyerDetails user;

    public String getToken(){  return token;  }
    public String getCode(){  return code;  }
    public String getError(){  return error;  }

    public boolean isSignIn() {
        return signin;
    }

    public boolean isSignUp() {
        return signup;
    }

    public BuyerDetails getBuyerDetails() {
        return user;
    }


}
