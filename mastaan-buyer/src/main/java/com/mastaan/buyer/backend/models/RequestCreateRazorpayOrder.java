package com.mastaan.buyer.backend.models;

/**
 * Created by Venkatesh Uppu on 04/07/18.
 */

public class RequestCreateRazorpayOrder {

    String receipt;
    long amount;
    String currency;
    boolean payment_capture;


    public RequestCreateRazorpayOrder(String receipt, long amount, String currency){
        this(receipt, amount, currency, false);
    }
    public RequestCreateRazorpayOrder(String receipt, long amount, String currency, boolean payment_capture){
        this.receipt = receipt;
        this.amount = amount;
        this.currency = currency;
        this.payment_capture = payment_capture;
    }

}
