package com.mastaan.buyer.backend.models;

import com.mastaan.buyer.models.CartTotalDetails;

import java.util.List;

/**
 * Created by aleena on 17/10/15.
 */

public class ResponseUpdateCartItem {

    String h;                       //  Item New HashValue
    float itemAmount;               //  Updated Item Price
    String dby;                     //  Delivery Date

    String msg;                     // Message
    CartTotalDetails cartDetails;


    public String getUpdatedHash() {
        return h;
    }

    public String getUpdatedDeliveryDate() {
        return dby;
    }

    public double getUpdatedPrice() {
        return itemAmount;
    }

    public String getMessage() {
        return msg;
    }

    public CartTotalDetails getCartTotalDetails() {
        if ( cartDetails != null ){ return cartDetails; }
        return new CartTotalDetails();
    }
}
