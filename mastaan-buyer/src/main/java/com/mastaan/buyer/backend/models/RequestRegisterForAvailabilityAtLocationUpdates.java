package com.mastaan.buyer.backend.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.AddressBookItemDetails;

/**
 * Created by venkatesh on 12/5/16.
 */
public class RequestRegisterForAvailabilityAtLocationUpdates {

    String e;
    String m;

    public String fa;       // Full Address
    public String pos;      // LatLng
    public double []loc;    //
    public String lm;       // Landmark
    public String p;        //  Premise
    public String s1;       //  SubLocality-1
    public String s2;       //  SubLocality-2
    public String r;        //  Road No
    public String l;        //  Locality
    public String s;        //  State
    public String c;        //  Country
    public  String pin;     // PinCode

    public RequestRegisterForAvailabilityAtLocationUpdates(String mobile, String email, AddressBookItemDetails location){

        this.e = email;
        this.m = mobile;

        if ( location.getLatLng() != null ) {
            this.pos = location.getLatLng().latitude + "," + location.getLatLng().longitude;
            this.loc = new double[2];
            loc[0] = location.getLatLng().latitude;
            loc[1] = location.getLatLng().longitude;
        }
        this.fa = location.getFullAddress();
        this.p = location.getPremise();
        this.s2 = location.getSublocalityLevel_2();
        this.s1 = location.getSublocalityLevel_1();
        this.l = location.getLocality();
        this.lm = location.getLandmark() == null ? location.getRoute(): "";
        this.s = location.getState();
        this.c = location.getCountry();
        this.pin = location.getPostalCode();

    }
}
