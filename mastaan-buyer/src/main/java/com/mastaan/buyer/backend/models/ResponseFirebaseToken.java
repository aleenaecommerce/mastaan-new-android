package com.mastaan.buyer.backend.models;

/**
 * Created by venkatesh on 15/7/15.
 */

public class ResponseFirebaseToken {

    String code;
    String data;

    public String getCode() {
        return code;
    }

    public String getFirebaseToken() {
        return data;
    }
}
