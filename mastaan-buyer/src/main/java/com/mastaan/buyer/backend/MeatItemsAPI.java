package com.mastaan.buyer.backend;


import android.content.Context;
import android.util.Log;

import com.mastaan.buyer.backend.models.RequestGetMeatItems;
import com.mastaan.buyer.backend.models.ResponseMeatItemPreferences;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.AttributeDetails;
import com.mastaan.buyer.models.CartItemDetails;
import com.mastaan.buyer.models.CategoryDetails;
import com.mastaan.buyer.models.WarehouseMeatItemDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MeatItemsAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface CategoriesListCallback {
        void onComplete(boolean status, int statusCode, List<CategoryDetails> foodItems);
    }

    public interface MeatItemsListCallback {
        void onComplete(boolean status, int statusCode, List<WarehouseMeatItemDetails> foodItems);
    }

    public interface MeatItemPreferencesCallback {
        void onComplete(boolean status, int statusCode, String message, List<AttributeDetails> attributesList, CartItemDetails previousCartItemDetails);
    }

    public MeatItemsAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getCategoriesList(final CategoriesListCallback callback){

        retrofitInterface.getCategories(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<List<CategoryDetails>>() {
            @Override
            public void success(List<CategoryDetails> categoriesList, Response response) {
                if (categoriesList != null) {
                    Log.i(Constants.LOG_TAG, "\nITEMS_API : [ CATEGORIES ]  >>  Success , Found = " + categoriesList.size());
                    callback.onComplete(true, 200, categoriesList);
                } else {
                    Log.i(Constants.LOG_TAG, "\nITEMS_API : [ CATEGORIES ]  >>  Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nITEMS_API : [ CATEGORIES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nITEMS_API : [ CATEGORIES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    //===============================================

    public void getMeatItems(final String categoryID, final MeatItemsListCallback callback){
        retrofitInterface.getMeatItems(deviceID, appVersion, localStorageData.getAccessToken(), new RequestGetMeatItems(categoryID, localStorageData.getDeliveryLocation()), new Callback<List<WarehouseMeatItemDetails>>() {
            @Override
            public void success(List<WarehouseMeatItemDetails> meatItems, Response response) {

                if (meatItems != null) {
//                    for (int i=0;i<meatItems.size();i++){
//                        WarehouseMeatItemDetails warehouseMeatItemDetails = meatItems.get(i);
//                        for (int a = 0; a< warehouseMeatItemDetails.getMeatItemDetais().getAttributes().size(); a++){
//                            AttributeDetails attributeDetails = warehouseMeatItemDetails.getMeatItemDetais().getAttributes().get(a);
//                            for (int o=0;o<attributeDetails.getAvailableOptions().size();o++){
//                                attributeDetails.getAvailableOptions().get(o).setStatus(true);
//                            }
//                        }
//                    }
                    Log.i(Constants.LOG_TAG, "\nITEMS_API : [ MEAT_ITEMS ]  >>  Success , Found = " + meatItems.size() + " , for Categroy/SubCategory = " + categoryID);
                    callback.onComplete(true, 200, meatItems);
                } else {
                    Log.i(Constants.LOG_TAG, "\nITEMS_API : [ MEAT_ITEMS ]  >>  Failure ,  for Categroy/SubCategory = " + categoryID);
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nITEMS_API : [ MEAT_ITEMS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " ,  for Categroy/SubCategory = " + categoryID);
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nITEMS_API : [ MEAT_ITEMS ]  >>  Error = " + error.getKind() + " ,  for Categroy/SubCategory = " + categoryID);
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }


    public void getMeatItemPreferences(final String meatItemID, final MeatItemPreferencesCallback callback){

        retrofitInterface.getMeatItemPreferences(deviceID, appVersion, localStorageData.getAccessToken(), meatItemID, new Callback<ResponseMeatItemPreferences>() {
            @Override
            public void success(ResponseMeatItemPreferences responseData, Response response) {
                Log.i(Constants.LOG_TAG, "\nITEMS_API : [ MEAT_ITEM_PREVIOUS_SELECTION_DETAILS ]  >>  Success");
                callback.onComplete(true, 200, "Success", responseData.getMeatItemAttributesList(), responseData.getPreviouesCartItemDetails());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nITEMS_API : [ MEAT_ITEM_PREVIOUS_SELECTION_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() );
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nITEMS_API : [ MEAT_ITEM_PREVIOUS_SELECTION_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }
}