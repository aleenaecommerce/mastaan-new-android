package com.mastaan.buyer.backend.models;

import com.mastaan.buyer.models.OrderItemFeedback;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestSubmitOrderFeedback {

    List<OrderItemFeedback> feedbacks = new ArrayList<>();
    boolean remind;


    public void addOrUpdateFeedback(OrderItemFeedback orderItemFeedback){
        if ( feedbacks == null ){   feedbacks = new ArrayList<>();  }
        int containIndex = -1;
        for(int i=0;i<feedbacks.size();i++){
            if ( feedbacks.get(i) != null && feedbacks.get(i).getOrderItemID().equals(orderItemFeedback.getOrderItemID()) ){
                containIndex = i;
                break;
            }
        }

        if ( containIndex >= 0 ){
            feedbacks.set(containIndex, orderItemFeedback);
        }else {
            feedbacks.add(orderItemFeedback);
        }
    }

    public void setRemindLater(boolean remindLater) {
        this.remind = remindLater;
    }

}
