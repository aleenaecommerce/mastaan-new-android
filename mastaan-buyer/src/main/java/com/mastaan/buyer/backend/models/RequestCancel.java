package com.mastaan.buyer.backend.models;

/**
 * Created by Venkatesh Uppu on 12/5/17.
 */
public class RequestCancel {
    String reason;

    public RequestCancel(String reason){
        this.reason = reason;
    }

}
