package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.buyer.backend.models.RequestAddAddress;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by venkatesh on 13/7/15.
 */

public class AddressBookAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface FavouritesCallback {
        void onComplete(boolean status, int statusCode, List<AddressBookItemDetails> addressBookItems);
    }

    public interface AddAddressCallback{
        void onComplete(boolean status, int statusCode, String addressID);
    }

    public AddressBookAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getLocationHistory(final FavouritesCallback callback){

        retrofitInterface.getLocationHistory(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<List<AddressBookItemDetails>>() {
            @Override
            public void success(List<AddressBookItemDetails> addressBookItems, Response response) {

                if (addressBookItems != null) {
                    Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ LOCATION_HISTORY ]  >>  Success , Found = " + addressBookItems.size());
                    callback.onComplete(true, 200, addressBookItems);
                } else {
                    Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ LOCATION_HISTORY ]  >>  Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ LOCATION_HISTORY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ LOCATION_HISTORY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getFavourites(final FavouritesCallback callback){

        retrofitInterface.getFavourites(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<List<AddressBookItemDetails>>() {
            @Override
            public void success(List<AddressBookItemDetails> addressBookItems, Response response) {

                if (addressBookItems != null) {
                    Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ FAVOURITES ]  >>  Success , Found = " + addressBookItems.size());
                    callback.onComplete(true, 200, addressBookItems);
                } else {
                    Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ FAVOURITES ]  >>  Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ FAVOURITES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ FAVOURITES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void addFavourie(final AddressBookItemDetails addressBookItemDetails, final AddAddressCallback callback){

        retrofitInterface.addFavourite(deviceID, appVersion, localStorageData.getAccessToken(), new RequestAddAddress(addressBookItemDetails), new Callback<RequestAddAddress>() {
            @Override
            public void success(RequestAddAddress responseObject, Response response) {

                if (responseObject != null && responseObject.getID() != null && responseObject.getID().length() > 0) {
                    Log.d(Constants.LOG_TAG, "ADDRESS_BOOK_API : [ ADD_FAVOURITE ]  >>  Success , AddressID = " + responseObject.getID());
                    callback.onComplete(true, 200, responseObject.getID());
                } else {
                    Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ ADD_FAVOURITE ]  >>  Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ ADD_FAVOURITE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ ADD_FAVOURITE ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void updateFavourite(final AddressBookItemDetails addressBookItemDetails, final StatusCallback callback){

        retrofitInterface.updateFavourite(deviceID, appVersion, localStorageData.getAccessToken(), new RequestAddAddress(addressBookItemDetails), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "ADDRESS_BOOK_API : [ UPDATE_FAVOURITE ]  >>  Success , for ID = " + addressBookItemDetails.getID());
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "ADDRESS_BOOK_API : [ UPDATE_FAVOURITE ]  >>  Failure , for ID = " + addressBookItemDetails.getID());
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ UPDATE_FAVOURITE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ UPDATE_FAVOURITE ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void deleteFavourie(final String addressID, final StatusCallback callback){

        retrofitInterface.deleteFavourite(deviceID, appVersion, localStorageData.getAccessToken(), addressID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "ADDRESS_BOOK_API : [ DELETE_FAVOURITE ]  >>  Success , for ID = " + addressID);
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "ADDRESS_BOOK_API : [ DELETE_FAVOURITE ]  >>  Failure , for ID = " + addressID);
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ DELETE_FAVOURITE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nADDRESS_BOOK_API : [ DELETE_FAVOURITE ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}
