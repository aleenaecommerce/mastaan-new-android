package com.mastaan.buyer.backend.models;

import com.mastaan.buyer.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 13/5/17.
 */
public class RequestAssignToCustomerSupport {
    List<String> items;
    String reason;

    public RequestAssignToCustomerSupport(String reason, List<String> items){
        this.items = items;
        this.reason = reason;
    }

    public RequestAssignToCustomerSupport(List<OrderItemDetails> items, String reason){
        if ( items != null ){
            this.items = new ArrayList<>();
            for (int i=0;i<items.size();i++){
                if ( items.get(i) != null && items.get(i).getID() != null ){
                    this.items.add(items.get(i).getID());
                }
            }
        }
        this.reason = reason;
    }

}
