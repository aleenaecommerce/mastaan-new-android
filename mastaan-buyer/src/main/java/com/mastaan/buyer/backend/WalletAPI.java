package com.mastaan.buyer.backend;

import android.content.Context;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.TransactionDetails;
import com.mastaan.buyer.models.WalletDetails;

import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 09/03/19.
 */

public class WalletAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface WalletCallback{
        void onComplete(boolean status, int statusCode, String message, WalletDetails walletDetails);
    }

    public interface WalletStatementCallback{
        void onComplete(boolean status, int statusCode, String message, String month, List<TransactionDetails> transactionsList);
    }

    public interface TransactionCallback{
        void onComplete(boolean status, int statusCode, String message, TransactionDetails transactionDetails);
    }


    public WalletAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getWalletDetails(final WalletCallback callback){

        retrofitInterface.getWalletDetails(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<WalletDetails>() {
            @Override
            public void success(WalletDetails walletDetails, Response response) {
                callback.onComplete(true, 200, "Success", walletDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getWalletStatement(final String month, final WalletStatementCallback callback){

        final Calendar calendar = DateMethods.getCalendarFromDate(month);

        retrofitInterface.getWalletStatement(deviceID, appVersion, localStorageData.getAccessToken(), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR), new Callback<List<TransactionDetails>>() {
            @Override
            public void success(List<TransactionDetails> transactionsList, Response response) {
                callback.onComplete(true, 200, "Success", month, transactionsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", month, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", month, null);
                    }
                }
            }
        });
    }

    public void getTransactionDetails(final String transactionID, final TransactionCallback callback){

        retrofitInterface.getTransactionDetails(deviceID, appVersion, localStorageData.getAccessToken(), transactionID, new Callback<TransactionDetails>() {
            @Override
            public void success(TransactionDetails transactionDetails, Response response) {
                callback.onComplete(true, 200, "Success", transactionDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

}