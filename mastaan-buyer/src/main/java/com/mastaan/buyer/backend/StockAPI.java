package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.buyer.backend.models.RequestWarehouseMeatItemStock;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.WarehouseMeatItemStockDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by venkatesh on 19/07/18.
 */

public class StockAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface WarehouseMeatItemStockCallback {
        void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemStockDetails warehouseMeatItemStockDetails);
    }


    public StockAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getWarehouseMeatItemStock(final String warehouseMeatItemID, List<String> itemLevelAttributeOptions, final WarehouseMeatItemStockCallback callback){
        String warehouseMeatItemIDWithItemLevelAttributeOptions = warehouseMeatItemID + (itemLevelAttributeOptions.size()>0?","+ CommonMethods.getStringFromStringList(itemLevelAttributeOptions, ","):"");

        retrofitInterface.getWarehouseMeatItemStock(deviceID, appVersion, localStorageData.getAccessToken(), warehouseMeatItemIDWithItemLevelAttributeOptions, new Callback<WarehouseMeatItemStockDetails>() {
            @Override
            public void success(WarehouseMeatItemStockDetails warehouseMeatItemStockDetails, Response response) {
                if (warehouseMeatItemStockDetails != null && warehouseMeatItemStockDetails.getID() != null && warehouseMeatItemStockDetails.getID().length() > 0 ) {
                    Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Success");
                    callback.onComplete(true, 200, "Success", warehouseMeatItemStockDetails);
                } else {
                    Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Failure, Msg: Stock Not Found");
                    callback.onComplete(true, 200, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

}