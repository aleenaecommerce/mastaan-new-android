package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.mastaan.buyer.backend.models.ResponseAddorRemoveCoupon;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.CartTotalDetails;
import com.mastaan.buyer.models.CouponDetails;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by venkatesh on 13/7/15.
 */

public class CouponsAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public CouponsAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getCoupons(final CouponsCallback callback) {

        retrofitInterface.getCoupons(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<List<CouponDetails>>() {
            @Override
            public void success(List<CouponDetails> couponsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ COUPONS ]  >>  Success");
                callback.onComplete(true, 200, "Success", couponsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ COUPONS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ COUPONS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void applyCoupon(final String couponCode, final AddOrRemoveCouponCallback callback) {

        retrofitInterface.applyCoupon(deviceID, appVersion, localStorageData.getAccessToken(), couponCode, new Callback<ResponseAddorRemoveCoupon>() {
            @Override
            public void success(ResponseAddorRemoveCoupon responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ APPLY_COUPON ]  >>  Success");
                callback.onComplete(true, 200, responseObject.getMessage(), responseObject.getCartTotalDetails());
            }

            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);

                        String msg = "";
                        String code = "";
                        try {
                            msg = jsonResponse.getString("msg");
                            code = jsonResponse.getString("code");
                            Log.e("msg coupn", jsonResponse.getString("code"));
                        } catch (Exception e) {
                        }

                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ APPLY_COUPON ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), msg, null);

                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ APPLY_COUPON ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "", null);
                    }
                }
            }
        });
    }

    public void removeCoupon(final AddOrRemoveCouponCallback callback) {

        retrofitInterface.removeCoupon(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<ResponseAddorRemoveCoupon>() {
            @Override
            public void success(ResponseAddorRemoveCoupon responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ REMOVE_COUPON ]  >>  Success");
                callback.onComplete(true, 200, "Success", responseObject.getCartTotalDetails());

                Log.e("res fail", "" + responseObject.getCartTotalDetails());

            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ REMOVE_COUPON ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOUPONS_API : [ REMOVE_COUPON ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public interface CouponsCallback {
        void onComplete(boolean status, int statusCode, String message, List<CouponDetails> couponsList);
    }

    public interface AddOrRemoveCouponCallback {
        void onComplete(boolean status, int statusCode, String message, CartTotalDetails cartTotalDetails);
    }

}
