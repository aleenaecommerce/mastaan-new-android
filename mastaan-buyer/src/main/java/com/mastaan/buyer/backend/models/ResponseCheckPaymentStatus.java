package com.mastaan.buyer.backend.models;

/**
 * Created by venkatesh on 8/1/16.
 */
public class ResponseCheckPaymentStatus {

    String code;
    String msg;
    String pid;
    String newTxnID;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return msg;
    }

    public String getPaymentID() {
        return pid;
    }

    public String getNewTransactionID() {
        return newTxnID;
    }
}
