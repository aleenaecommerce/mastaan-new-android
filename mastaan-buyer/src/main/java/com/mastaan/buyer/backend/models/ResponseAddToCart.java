package com.mastaan.buyer.backend.models;

/**
 * Created by aleena on 17/10/15.
 */
public class ResponseAddToCart {

    String code;
    String msg;                     // Message

    boolean alreadyPresent;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return msg;
    }

    public boolean isAlreadyPresent() {
        return alreadyPresent;
    }
}
