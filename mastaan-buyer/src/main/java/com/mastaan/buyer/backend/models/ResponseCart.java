package com.mastaan.buyer.backend.models;

import com.mastaan.buyer.models.CartItemDetails;
import com.mastaan.buyer.models.CartTotalDetails;
import com.mastaan.buyer.models.WalletDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 20/10/15.
 */
public class ResponseCart {

    String serverTime;                  // Server Time
    String msg;                         // Message

    List<CartItemDetails> cartItems;    // Cart Items
    CartTotalDetails cartDetails;       // Total Details

    WalletDetails wallet;               // Wallet
    String tid;                         // Transaction ID


    public String getServerTime() {
        return serverTime;
    }

    public String getMessage() {
        return msg;
    }

    public List<CartItemDetails> getCartItems() {
        if ( cartItems == null ){ cartItems = new ArrayList<>();  }
        return cartItems;
    }

    public CartTotalDetails getCartTotalDetails() {
        if ( cartDetails != null ){ return cartDetails; }
        return new CartTotalDetails();
    }

    public WalletDetails getWalletDetails() {
        return wallet;
    }

    public String getTransactionID() {
        return tid;
    }

}
