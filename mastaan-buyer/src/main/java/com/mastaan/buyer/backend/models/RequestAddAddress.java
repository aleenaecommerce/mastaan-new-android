package com.mastaan.buyer.backend.models;

import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.PlaceDetails;

/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestAddAddress {

    public String _id;
    public String b;

    public String t;    // Name
    public String fa;       // Address
    public String pos;     // LatLng
    public String lm;       // Landmark
    public String p;
    public String s1;
    public String s2;
    public String r;
    public String l;
    public String s;
    public String c;
    public  String pin;   // PinCode

    public RequestAddAddress(PlaceDetails placeDetails){
        this._id = placeDetails.getId();
        this.t = placeDetails.getTitle();
        this.fa = placeDetails.getFullAddress();
        this.pos = placeDetails.getLatLng().latitude+","+placeDetails.getLatLng().longitude;
        this.p = placeDetails.getPremise();
        this.lm = placeDetails.getLandmark();
        this.r = placeDetails.getRoute();
        this.s2 = placeDetails.getSublocalityLevel_2();
        this.s1 = placeDetails.getSublocalityLevel_1();
        this.l = placeDetails.getLocality();
        this.s = placeDetails.getState();
        this.c = placeDetails.getCountry();
        this.pin = placeDetails.getPostal_code();
    }

    public RequestAddAddress(AddressBookItemDetails addressBookItemDetails){
        this._id = addressBookItemDetails.getID();
        this.t = addressBookItemDetails.getTitle();
        this.fa = addressBookItemDetails.getFullAddress();
        this.pos = addressBookItemDetails.getLatLng().latitude+","+addressBookItemDetails.getLatLng().longitude;
        this.p = addressBookItemDetails.getPremise();
        this.lm = addressBookItemDetails.getLandmark();
        this.r = addressBookItemDetails.getRoute();
        this.s2 = addressBookItemDetails.getSublocalityLevel_2();
        this.s1 = addressBookItemDetails.getSublocalityLevel_1();
        this.l = addressBookItemDetails.getLocality();
        this.s = addressBookItemDetails.getState();
        this.c = addressBookItemDetails.getCountry();
        this.pin = addressBookItemDetails.getPostalCode();
    }

    public String getID() {
        return _id;
    }

    public String getB() {
        return b;
    }
}
