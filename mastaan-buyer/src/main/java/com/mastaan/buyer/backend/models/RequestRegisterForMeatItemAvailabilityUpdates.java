package com.mastaan.buyer.backend.models;

/**
 * Created by Venkatesh Uppu on 15/03/18.
 */

public class RequestRegisterForMeatItemAvailabilityUpdates {

    String wmi; // Warehouse Meat Item

    public RequestRegisterForMeatItemAvailabilityUpdates(String warehouseMeatItemID){
        this.wmi = warehouseMeatItemID;
    }

}
