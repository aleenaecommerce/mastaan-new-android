package com.mastaan.buyer.backend.models;

import com.aleena.common.models.AddressBookItemDetails;
import com.mastaan.buyer.models.SelectedAttributeDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 17/10/15.
 */

public class RequestAddToCart {

    String i;       //  ItemID
    String dby;     //  Delivery Date
    double n;       //  NoOfKGs
    List<SelectedAttributeDetails> a;   // Selected Attributes

    //String adid;    //  Delivery Address ID
    //String pos;     //  Delivery Address LatLng
    AddressBookItemDetails ad;      //  Delivery Address
    String wcs;     // category slot ID

    String si;      //  Special instructions

    public RequestAddToCart(String itemID, String slotID, String deliveryDateAndTime, double noOfKGs, List<SelectedAttributeDetails> selectedAttributes, AddressBookItemDetails deliveryLocation, String specialInstructions){
        this.i = itemID;
        this.dby = deliveryDateAndTime;
        this.n = noOfKGs;
        this.a = selectedAttributes;
        this.wcs = slotID;
        this.ad = deliveryLocation;
        this.si = specialInstructions;
        /*if ( deliveryLocation != null ){
            if ( deliveryLocation.getLatLng() != null ){
                this.pos = deliveryLocation.getLatLng().latitude+","+deliveryLocation.getLatLng().longitude;
            }else{
                this.pos = "0,0";
            }
            this.ad = deliveryLocation.getFullAddress();
            this.adid = deliveryLocation.getID();
        }*/
    }
}
