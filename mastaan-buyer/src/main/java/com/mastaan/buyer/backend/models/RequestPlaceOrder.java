package com.mastaan.buyer.backend.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 13/07/15.
 */

public class RequestPlaceOrder {

    int pt;          // Payment Type
    String cc;       // Coupon
    String tid;      // TransactionID
    String pid;      // PaymentID

    String n;        // Name
    String m;        // Mobile
    String am;       // Alternate Mobile
    String e;        // Email

    String si;       // Instructions

    String dby;      // DeliveryBy

    String ad;       // AddressBookItemID
    String da;       // Full Address
    String pos;      // LatLng
    String lm;       // Landmark
    String p;        // Premise
    String s1;       // SubLocality-1
    String s2;       // SubLocality-2
    String r;        // Road No
    String l;        // Locality
    String s;        // State
    String c;        // Country
    String pin;      // PinCode

    boolean de;      // Early Delivery
    boolean bnr;     // Bill Not Required

    boolean uwb;     // Use Wallet Balance

    String cb;       // Internal App Initiated Employee ID

    // TEMP
    double total;    // Total Amount
    double mpt;      // Max Cart Prep Time



    public RequestPlaceOrder(String addresBookItemID, String name, String mobile, String alternateMobile, String email, String instructions, String couponCode, String deliverySlot, double maxCartPrepTime, LatLng latLng, String premise, String sublocality2, String sublocality1, String locality, String landmark, String address, String state, String country, String pincode, boolean deliverEarlyIfPossible, boolean billNotRequired, boolean useWalletBalance){
        if ( addresBookItemID != null && addresBookItemID.equalsIgnoreCase("DUMMY_ID") == false ){
            this.ad = addresBookItemID;
        }
        this.n = name;
        this.m = mobile;
        this.am = alternateMobile;
        this.e = email;
        this.si = instructions;

        this.cc = couponCode;
        if ( cc != null && cc.length() == 0 ){ cc = null;   }// TO PREVENT EMPTY COUPON CODE SENDING TO SERVER

        this.dby = deliverySlot;
        this.mpt = maxCartPrepTime;

        if ( latLng != null ) {
            this.pos = latLng.latitude + "," + latLng.longitude;
        }
        this.p = premise;
        this.s2 = sublocality2;
        this.s1 = sublocality1;
        this.l = locality;
        this.lm = landmark;
        this.da = CommonMethods.getStringFromStringArray(new String[]{premise, s2, lm, s1, l, s, c, pin});//premise+", "+sublocality2+", "+address;
        this.s = state;
        this.c = country;
        this.pin = pincode;

        this.de = deliverEarlyIfPossible;
        this.bnr = billNotRequired;

        this.uwb = useWalletBalance;
    }


    public String getUserName() {
        return n;
    }

    public String getUserEmail() {
        return e;
    }

    public String getUserMobile() {
        return m;
    }

    public String getUserAlternateMobile() {
        return am;
    }

    public String getDeliverBy() {
        return dby;
    }

    public String getDeliveryAddressID() {
        if ( ad == null ){ ad = ""; }
        return ad;
    }

    public LatLng getDeliveryAddressLatLng() {
        return LatLngMethods.getLatLngFromString(pos);
    }

    public String getDeliveryAddressPremise() {
        return p;
    }

    public String getDeliveryAddress() {
        return da;
    }

    public String getDeliveryAddressSubLocalityLevel1() {
        return s1;
    }

    public String getDeliveryAddressSubLocalityLevel2() {
        return s2;
    }


    public String getDeliveryAddressLandmark() {
        return lm;
    }

    public String getDeliveryAddressLocality() {
        return l;
    }

    public String getDeliveryAddressState() {
        return s;
    }

    public String getDeliveryAddressCountry() {
        return c;
    }

    public String getDeliveryAddressPinCode() {
        return pin;
    }

    //------------

    public RequestPlaceOrder setPaymentType(int pt) {
        this.pt = pt;
        return this;
    }

    public int getPaymentType() {
        return pt;
    }

    public RequestPlaceOrder setTotal(double total) {
        this.total = total;
        return this;
    }

    public double getTotal() {
        return total;
    }

    public void setTransactionID(String transactionID) {
        this.tid = transactionID;
    }

    public String getTransactionID() {
        return tid;
    }

    public void setPaymentID(String paymentID) {
        this.pid = paymentID;
    }

    public String getPaymentID() {
        return pid;
    }


    public void setCreatedByEmployeeID(String createdByEmployeeID) {
        this.cb = createdByEmployeeID;
    }
    public String getCreatedByEmployeeID() {
        return cb;
    }

}
