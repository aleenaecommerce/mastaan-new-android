package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.buyer.backend.models.RequestAssignToCustomerSupport;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by venkatesh on 17/05/17.
 */

public class CustomerSupportAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;


    public CustomerSupportAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
        
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }


    public void assignToCustomerSupport(String orderID, RequestAssignToCustomerSupport requestAssignToCustomerSupport, final StatusCallback callback){

        retrofitInterface.assignToCustomerSupport(deviceID, appVersion, localStorageData.getAccessToken(), orderID, requestAssignToCustomerSupport, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nCUSTOMER_SUPPORT_API : [ ASSIGN_TO_CS ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        JSONObject jsonResponse = null;
                        try{    jsonResponse = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())); }catch (Exception e){}
                        Log.d(Constants.LOG_TAG, "\nCUSTOMER_SUPPORT_API : [ ASSIGN_TO_CS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), (jsonResponse!=null&&jsonResponse.getString("msg")!=null)?jsonResponse.getString("msg"):"Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCUSTOMER_SUPPORT_API : [ ASSIGN_TO_CS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


}
