package com.mastaan.buyer.backend.models;

import com.mastaan.buyer.models.CartTotalDetails;

import java.util.List;

/**
 * Created by venkatesh on 15/3/16.
 */

public class ResponseAddorRemoveCoupon {

    String msg;                     // Message
    String code;                     // code set
    CartTotalDetails cartDetails;

    public String getMessage() {
        return msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CartTotalDetails getCartTotalDetails() {
        if ( cartDetails != null ){ return cartDetails; }
        return new CartTotalDetails();
    }
}
