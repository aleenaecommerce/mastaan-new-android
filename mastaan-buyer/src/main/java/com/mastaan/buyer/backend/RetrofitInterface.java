package com.mastaan.buyer.backend;

import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.buyer.backend.models.RequestAddAddress;
import com.mastaan.buyer.backend.models.RequestAddOnlinePayment;
import com.mastaan.buyer.backend.models.RequestAddToCart;
import com.mastaan.buyer.backend.models.RequestAssignToCustomerSupport;
import com.mastaan.buyer.backend.models.RequestCancel;
import com.mastaan.buyer.backend.models.RequestCaptureRazorpayPayment;
import com.mastaan.buyer.backend.models.RequestCheckAvailabilityAtLocation;
import com.mastaan.buyer.backend.models.RequestCreateRazorpayOrder;
import com.mastaan.buyer.backend.models.RequestGetMeatItems;
import com.mastaan.buyer.backend.models.RequestLinkToFCM;
import com.mastaan.buyer.backend.models.RequestPlaceOrder;
import com.mastaan.buyer.backend.models.RequestProfileUpdate;
import com.mastaan.buyer.backend.models.RequestRegisterForAvailabilityAtLocationUpdates;
import com.mastaan.buyer.backend.models.RequestRegisterForMeatItemAvailabilityUpdates;
import com.mastaan.buyer.backend.models.RequestSlotsForDate;
import com.mastaan.buyer.backend.models.RequestSubmitOrderFeedback;
import com.mastaan.buyer.backend.models.RequestUpdateCartItem;
import com.mastaan.buyer.backend.models.RequestUpdateCartItemSpecialInstructions;
import com.mastaan.buyer.backend.models.RequestUpdateInstallSource;
import com.mastaan.buyer.backend.models.ResponeValidateMobile;
import com.mastaan.buyer.backend.models.ResponseAddToCart;
import com.mastaan.buyer.backend.models.ResponseAddorRemoveCoupon;
import com.mastaan.buyer.backend.models.ResponseCart;
import com.mastaan.buyer.backend.models.ResponseCheckAvailabilityAtLocation;
import com.mastaan.buyer.backend.models.ResponseConnectBuyer;
import com.mastaan.buyer.backend.models.ResponseCreateRazorpayOrder;
import com.mastaan.buyer.backend.models.ResponseDeleteCartItem;
import com.mastaan.buyer.backend.models.ResponseFirebaseToken;
import com.mastaan.buyer.backend.models.ResponseMeatItemPreferences;
import com.mastaan.buyer.backend.models.ResponseTransactionID;
import com.mastaan.buyer.backend.models.ResponseUpdateCartItem;
import com.mastaan.buyer.backend.models.ResponseValidateEmail;
import com.mastaan.buyer.backend.models.ResponseVerifyEmail;
import com.mastaan.buyer.backend.models.ResponseVerifyMobile;
import com.mastaan.buyer.models.BootStrapDetails;
import com.mastaan.buyer.models.CategoryDetails;
import com.mastaan.buyer.models.CouponDetails;
import com.mastaan.buyer.models.FeedbackDetails;
import com.mastaan.buyer.models.MembershipDetails;
import com.mastaan.buyer.models.MessageDetails;
import com.mastaan.buyer.models.OrderDetails;
import com.mastaan.buyer.models.OrderItemDetails;
import com.mastaan.buyer.models.ProfileSummaryDetails;
import com.mastaan.buyer.models.ReferralOfferDetails;
import com.mastaan.buyer.models.SlotDetails;
import com.mastaan.buyer.models.TransactionDetails;
import com.mastaan.buyer.models.WalletDetails;
import com.mastaan.buyer.models.WarehouseMeatItemDetails;
import com.mastaan.buyer.models.WarehouseMeatItemStockDetails;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by venkatesh on 29/6/15.
 */

public interface RetrofitInterface {

    //------------------ GENERAL API --------------------//

    @GET("/buyer/getrttoken")
    void getFireBaseToken(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ResponseFirebaseToken> callback);

    @GET("/buyer/bootstrap")
    void getBootStrap(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<BootStrapDetails> callback);

    @POST("/buyer/checkavailability")
    void checkAvailabilityAtLocation(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestCheckAvailabilityAtLocation requestObj, Callback<ResponseCheckAvailabilityAtLocation> callback);

    @POST("/buyer/changelocation")
    void changeLocation(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestCheckAvailabilityAtLocation requestObj, Callback<ResponseCheckAvailabilityAtLocation> callback);

    @POST("/buyer/submitavailabilityrequest")
    void registerForAvailabilityAtLocationUpdates(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestRegisterForAvailabilityAtLocationUpdates requestObj, Callback<JSONObject> callback);

    @POST("/buyer/meatitemavailabilityrequests")
    void registerForMeatAvailabilityUpdates(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestRegisterForMeatItemAvailabilityUpdates requestObj, Callback<ResponseStatus> callback);

    @POST("/buyer/slotsfordate/{date}/{month}/{year}")
    void getSlotsForDate(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("date") int date, @Path("month") int month, @Path("year") int year, @Body RequestSlotsForDate requestSlotsForDate, Callback<List<SlotDetails>> callback);

    @GET("/buyer/alertmessages")
    void getAlertMessages(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<List<MessageDetails>> callback);

    @GET("/buyer/referraloffer")
    void getReferralOfferDetails(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ReferralOfferDetails> callback);

    @GET("/buyer/memberships")
    void getMemberships(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<List<MembershipDetails>> callback);

    @GET("/buyer/promotioanlalerts/{preference}")
    void updatePromotionalAlertsPreference(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("preference") boolean preference, Callback<ResponseStatus> callback);

    //-------------- AUTHENTICATION API --------------//

    @GET("/general/validatenumber1/{phonenumber}")
    void validatePhoneNumber1(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("phonenumber") String phonenumber, Callback<ResponeValidateMobile> callback);

    @GET("/buyer/verifynumber1/buyer/{smsid}/{otp}")
    void verifyPhoneNumber1(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("smsid") String smsid, @Path("otp") String otp, Callback<ResponseVerifyMobile> callback);

    @GET("/buyer/verifyemail/{email}")
    void validateEmail(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("email") String emailAddress, Callback<ResponseValidateEmail> callback);

    @GET("/buyer/validateemail/{pinid}/{pin}")
    void verifyEmail(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("pinid") String pinid, @Path("pin") String pin, Callback<ResponseVerifyEmail> callback);


    @GET("/buyer/connect")
    void connectBuyer(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ResponseConnectBuyer> callback);

    @GET("/buyer/disconnect")
    void confirmLogout(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ResponseStatus> callback);

    //-------------- PROFILE API --------------//

    @GET("/buyer/profile")
    void getProfileDetails(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ProfileSummaryDetails> callback);

    @POST("/buyer/update")
    void updateProfile(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestProfileUpdate profileUpdateRequest, Callback<ResponseStatus> callback);

    @POST("/buyer/updateinstallsource")
    void updateInstallSource(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestUpdateInstallSource requestUpdateInstallSource, Callback<ResponseStatus> callback);

    @POST("/buyer/linktofcm")
    void linkToFCM(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestLinkToFCM requestObj, Callback<ResponseStatus> callback);

    @GET("/buyer/markratedonstore")
    void markRatedOnStore(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ResponseStatus> callback);


    //-------------- ADDRESS BOOK API --------//

    @GET("/buyer/locationhistory")
    void getLocationHistory(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<List<AddressBookItemDetails>> callback);

    @GET("/buyer/addressbook")
    void getFavourites(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<List<AddressBookItemDetails>> callback);

    @POST("/buyer/addaddress")
    void addFavourite(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestAddAddress userObj, Callback<RequestAddAddress> callback);

    @POST("/buyer/updateaddress")
    void updateFavourite(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestAddAddress userObj, Callback<ResponseStatus> callback);

    @GET("/buyer/deleteaddress/{addr_id}")
    void deleteFavourite(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("addr_id") String addr_id, Callback<ResponseStatus> callback);

    //-------------- MEAT ITEMS API --------------//

    @GET("/buyer/categories")
    void getCategories(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<List<CategoryDetails>> callback);

    @POST("/buyer/searchitems")
    void getMeatItems(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestGetMeatItems requestObj, Callback<List<WarehouseMeatItemDetails>> callback);

    @GET("/buyer/meatitemprefs/{meat_item_id}")
    void getMeatItemPreferences(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("meat_item_id") String meat_item_id, Callback<ResponseMeatItemPreferences> callback);

    //-------------- CART API  --------------//

    @POST("/buyer/cart/add")
    void addToCart(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestAddToCart requestAddToCart, Callback<ResponseAddToCart> callback);

    @POST("/buyer/cart/item/{cart_item_id}/update")
    void updateCartItem(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("cart_item_id") String item_id, @Body RequestUpdateCartItem requestUpdateCartItem, Callback<ResponseUpdateCartItem> callback);

    @POST("/buyer/cart/item/{cart_item_id}/specialinstructions/update")
    void updateCartItemSpecialInstructions(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("cart_item_id") String item_id, @Body RequestUpdateCartItemSpecialInstructions requestObject, Callback<ResponseStatus> callback);

    @GET("/buyer/cart/item/{cart_item_id}/remove")
    void deleteCartItem(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("cart_item_id") String item_id, Callback<ResponseDeleteCartItem> callback);

    @GET("/buyer/cart")
    void getCart(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ResponseCart> callback);

    @GET("/buyer/cart/clear")
    void clearCart(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ResponseStatus> callback);

    @GET("/buyer/cart/checkforplaceorder")
    void checkoutCart(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ResponseStatus> callback);

    //-------------- STOCK API --------------//

    @GET("/buyer/stock/whmeatitem/{whmeatitem_id_with_item_level_attrribute_options}")
    void getWarehouseMeatItemStock(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("whmeatitem_id_with_item_level_attrribute_options") String warehouseMeatItemIDWithItemLevelAttributeOptions, Callback<WarehouseMeatItemStockDetails> callback);

    //-------------- COUPONS API -----------//

    @GET("/buyer/coupons")
    void getCoupons(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<List<CouponDetails>> callback);

    @GET("/buyer/applycouponcode/{code}")
    void applyCoupon(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("code") String couponCode, Callback<ResponseAddorRemoveCoupon> callback);

    @GET("/buyer/clearcouponcode")
    void removeCoupon(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ResponseAddorRemoveCoupon> callback);

    //-------------- PAYMENTS API ----------------//

    @POST("/buyer/createrazorpayorder/{type}")
    void createRazorpayOrder(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("type") String type, @Body RequestCreateRazorpayOrder requestCreateRazorpayOrder, Callback<ResponseCreateRazorpayOrder> callback);

    @POST("/buyer/capturerazorpaypayment")
    void captureRazorpayPayment(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestCaptureRazorpayPayment requestCaptureRazorpayPayment, Callback<ResponseStatus> callback);

    @GET("/buyer/gettransactionid")
    void getTransactionID(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<ResponseTransactionID> callback);

    @POST("/buyer/addonlinepaymentforgroupedorder/{group_order_id}")
    void addOnlinePaymentForGroupedOrder(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("group_order_id") String groupOrderID, @Body RequestAddOnlinePayment requestAddOnlinePayment, Callback<ResponseStatus> callback);

    @POST("/buyer/addonlinepaymentfororder/{order_id}")
    void addOnlinePaymentForOrder(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_id") String orderID, @Body RequestAddOnlinePayment requestAddOnlinePayment, Callback<ResponseStatus> callback);

    @POST("/buyer/markonlinepaymentforoutstandingamount")
    void addOnlinePaymentForOutstanding(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestAddOnlinePayment requestAddOnlinePayment, Callback<ResponseStatus> callback);

    //-------------- WALLET API --------------//

    @GET("/buyer/wallet")
    void getWalletDetails(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<WalletDetails> callback);

    @GET("/buyer/wallet/statement/{month}/{year}")
    void getWalletStatement(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("month") int month, @Path("year") int year, Callback<List<TransactionDetails>> callback);

    @GET("/buyer/transaction/{id}")
    void getTransactionDetails(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("id") String transactionID, Callback<TransactionDetails> callback);

    //-------------- ORDER API -------------//

    @POST("/buyer/placeorder")
    void placeOrder(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestPlaceOrder requestPlaceOrder, Callback<List<OrderDetails>> callback);

    @POST("/buyer/order/{order_id}/cancel")
    void cancelOrder(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_id") String orderID, @Body RequestCancel requestCancel, Callback<ResponseStatus> callback);

    @GET("/buyer/orderitem/{order_item_id}/checkforcancel")
    void checkForCancelOrderItem(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_item_id") String orderItemID, Callback<ResponseStatus> callback);

    @POST("/buyer/orderitem/{order_item_id}/cancel")
    void cancelOrderItem(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_item_id") String orderItemID, @Body RequestCancel requestCancel, Callback<ResponseStatus> callback);

    @GET("/buyer/orderdetails/{order_id}")
    void getOrderDetails(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_id") String order_Id, Callback<OrderDetails> callback);

    @GET("/buyer/orderitem/{order_item_id}")
    void getOrderItemDetails(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_item_id") String order_item_id, Callback<OrderItemDetails> callback);

    @GET("/buyer/groupedorderdetails/{order_Id}")
    void getGroupedOrderDetails(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_Id") String order_Id, Callback<List<OrderDetails>> callback);

    @GET("/buyer/orderhistory")
    void getOrders(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<List<OrderDetails>> callback);

    @GET("/buyer/pendingorders")
    void getPendingOrders(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<List<OrderDetails>> callback);

    //------------- CUSTOMER SUPPORT API --------//

    @POST("/buyer/order/{order_id}/assigntocustomersupport")
    void assignToCustomerSupport(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_id") String orderID, @Body RequestAssignToCustomerSupport requestAssignToCustomerSupport, Callback<ResponseStatus> callback);

    //-------------- FEEDBACK API --------------//

    @GET("/buyer/pendingfeedbacks")
    void getPendingFeedbackOrders(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, Callback<List<OrderDetails>> callback);

    @POST("/buyer/postfeedback/{order_id}")
    void submitFeedback(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestSubmitOrderFeedback requestSubmitOrderFeedback, @Path("order_id") String order_id, Callback<ResponseStatus> callback);

    //@GET("/buyer/askfeedacknextday/{order_id}")
    //void remindFeedbackLater(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_id") String order_id, Callback<ResponseStatus> callback);

    @GET("/buyer/skipfeedback/{order_id}")
    void skipOrderFeedback(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_id") String order_id, Callback<ResponseStatus> callback);

    @GET("/buyer/orderitemfeedback/{order_item_id}")
    void getOrderItemFeedback(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_item_id") String orderItemID, Callback<FeedbackDetails> callback);

}
