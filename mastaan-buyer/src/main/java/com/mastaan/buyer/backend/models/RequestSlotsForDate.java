package com.mastaan.buyer.backend.models;

/**
 * Created by valueduser on 26/8/16.
 */
public class RequestSlotsForDate {
    String c;
    String whi;
    String i;

    public RequestSlotsForDate(){}

    public RequestSlotsForDate(String category, String warehouseMeatItemID, String meatItemID){
        this.c = category;
        this.whi = warehouseMeatItemID;
        this.i = meatItemID;
    }
}
