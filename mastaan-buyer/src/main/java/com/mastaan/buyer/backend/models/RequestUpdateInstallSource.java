package com.mastaan.buyer.backend.models;

/**
 * Created by Venkatesh Uppu on 26/05/18.
 */

public class RequestUpdateInstallSource {

    String is;  // Install Source
    String isd; // Install Source Details

    public RequestUpdateInstallSource(String installSource, String installSourceDetails){
        this.is = installSource;
        this.isd = installSourceDetails;
    }

}
