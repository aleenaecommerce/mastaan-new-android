package com.mastaan.buyer.backend.models;

import com.aleena.common.methods.DateMethods;
import com.mastaan.buyer.models.DayDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 23/10/15.
 */
public class ResponseCheckAvailabilityAtLocation {

    boolean available;

    int daysToEnableSlots;
    List<DayDetails> noDeliveryDatesMap;

    //List<CategoryDetails> categories;   // Categories

    public boolean isAvailable() {
        return available;
    }

    public List<DayDetails> getNoDeliveryDates() {
        if ( noDeliveryDatesMap == null ){  noDeliveryDatesMap = new ArrayList<>(); }
        return noDeliveryDatesMap;
    }

    public String getNoDeliveryDateMessage(String date){
        if ( noDeliveryDatesMap != null ){
            for (int i=0;i<noDeliveryDatesMap.size();i++){
                if ( DateMethods.compareDates(DateMethods.getOnlyDate(date), noDeliveryDatesMap.get(i).getDate()) == 0 ){
                    return noDeliveryDatesMap.get(i).getMessage();
                }
            }
        }
        return "";
    }

    public int getDaysToEnableSlots() {
        return daysToEnableSlots;
    }

    //public List<CategoryDetails> getCategories() {
    //    return categories;
    //}
}
