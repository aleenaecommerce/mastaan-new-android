package com.mastaan.buyer.backend.models;

/**
 * Created by Venkatesh Uppu on 27/08/18.
 */

public class RequestLinkToFCM {

    public String rid;

    public RequestLinkToFCM(String fcmRegistrationID){
        rid = fcmRegistrationID;
    }

}
