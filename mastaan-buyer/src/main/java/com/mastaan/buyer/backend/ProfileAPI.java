package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.buyer.backend.models.RequestLinkToFCM;
import com.mastaan.buyer.backend.models.RequestProfileUpdate;
import com.mastaan.buyer.backend.models.RequestUpdateInstallSource;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.ProfileSummaryDetails;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ProfileAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface ProfileDetailsCallback{
        void onComplete(boolean status, int statusCode, ProfileSummaryDetails profileSummaryDetails);
    }


    public ProfileAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }

    public void getProfileDetails(final ProfileDetailsCallback callback){

        retrofitInterface.getProfileDetails(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<ProfileSummaryDetails>() {
            @Override
            public void success(ProfileSummaryDetails profileSummaryDetails, Response response) {

                if (profileSummaryDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - ProfileDetails : Success");
                    callback.onComplete(true, 200, profileSummaryDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - ProfileDetails : Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - ProfileDetails : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - ProfileDetails : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void updateProfile(RequestProfileUpdate requestProfileUpdate, final StatusCallback callback){

        retrofitInterface.updateProfile(deviceID, appVersion, localStorageData.getAccessToken(), requestProfileUpdate, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("ok")) {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateProfile : Success");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateProfile : Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateProfile : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateProfile : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateInstallSource(RequestUpdateInstallSource requestUpdateInstallSource, final StatusCallback callback){

        retrofitInterface.updateInstallSource(deviceID, appVersion, localStorageData.getAccessToken(), requestUpdateInstallSource, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("ok")) {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateInstallSource : Success");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateInstallSource : Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateInstallSource : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - UpdateInstallSource : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void linkToFCM(String fcmRegistrationID, final StatusCallback callback){

        retrofitInterface.linkToFCM(deviceID, appVersion, localStorageData.getAccessToken(), new RequestLinkToFCM(fcmRegistrationID), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nPROFILE_API - LinkToFCM : Success");
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - LinkToFCM : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        if ( callback != null ){
                            callback.onComplete(false, error.getResponse().getStatus(), "Error");
                        }
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - LinkToFCM : Error = " + error.getKind());
                        if ( callback != null ){
                            callback.onComplete(false, -1, "Error");
                        }
                    }
                }
            }
        });
    }


    public void markRatedOnStore(final StatusCallback callback){

        retrofitInterface.markRatedOnStore(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nPROFILE_API - MarkRatedOnStore : Success");
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - MarkRatedOnStore : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        if ( callback != null ){
                            callback.onComplete(false, error.getResponse().getStatus(), "Error");
                        }
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPROFILE_API - MarkRatedOnStore : Error = " + error.getKind());
                        if ( callback != null ){
                            callback.onComplete(false, -1, "Error");
                        }
                    }
                }
            }
        });
    }

    public void updatePromotionalAlertsPreference(final boolean preference, final StatusCallback callback){

        retrofitInterface.updatePromotionalAlertsPreference(deviceID, appVersion, localStorageData.getAccessToken(), preference, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}