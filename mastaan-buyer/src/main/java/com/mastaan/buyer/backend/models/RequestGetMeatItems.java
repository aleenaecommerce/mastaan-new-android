package com.mastaan.buyer.backend.models;

import com.aleena.common.models.AddressBookItemDetails;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 15/10/15.
 */
public class RequestGetMeatItems {
    String category;
    String location;

    public RequestGetMeatItems(String category, String location){
        this.category = category;
        this.location = location;
    }
    public RequestGetMeatItems(String category, AddressBookItemDetails addressBookItemDetails){
        this(category, addressBookItemDetails!=null?addressBookItemDetails.getLatLng():null);
    }
    public RequestGetMeatItems(String category, LatLng location){
        this.category = category;
        if ( location != null ) {
            this.location = location.latitude + "," + location.longitude;
        }
    }
}
