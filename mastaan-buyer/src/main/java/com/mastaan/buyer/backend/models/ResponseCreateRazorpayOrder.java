package com.mastaan.buyer.backend.models;

/**
 * Created by Venkatesh Uppu on 04/07/18.
 */

public class ResponseCreateRazorpayOrder {
    String id;
    String status;
    String receipt;
    long amount;
    String currency;
    boolean payment_capture;


    public String getID() {
        return id;
    }

    public String getStatus() {
        return status;
    }

}
