package com.mastaan.buyer.backend.models;

import com.mastaan.buyer.models.BuyerDetails;

/**
 * Created by Venkatesh Uppu on 28/07/18.
 */

public class ResponseConnectBuyer {

    boolean signup;
    boolean signin;
    BuyerDetails user;

    public boolean isSignIn() {
        return signin;
    }

    public boolean isSignUp() {
        return signup;
    }

    public BuyerDetails getBuyerDetails() {
        return user;
    }

}
