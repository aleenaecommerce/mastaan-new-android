package com.mastaan.buyer.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.buyer.backend.models.RequestSubmitOrderFeedback;
import com.mastaan.buyer.constants.Constants;
import com.mastaan.buyer.localdata.LocalStorageData;
import com.mastaan.buyer.methods.CheckError;
import com.mastaan.buyer.models.FeedbackDetails;
import com.mastaan.buyer.models.OrderDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by venkatesh on 13/7/15.
 */

public class FeedbacksAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface PendingFeedbackOrdersCallback{
        void onComplete(boolean status, int statusCode, String message, List<OrderDetails> pendingFeedbackOrders);
    }
    public interface FeedbackCallback{
        void onComplete(boolean status, int statusCode, String message, FeedbackDetails feedbackDetails);
    }


    public FeedbacksAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        localStorageData = new LocalStorageData(context);
    }


    public void getPendingFeedbackOrders(final PendingFeedbackOrdersCallback callback){

        retrofitInterface.getPendingFeedbackOrders(deviceID, appVersion, localStorageData.getAccessToken(), new Callback<List<OrderDetails>>() {
            @Override
            public void success(List<OrderDetails> pendingFeedbackOrders, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [PendingFeedbackOrders] >> Success");
                callback.onComplete(true, 200, "Success", pendingFeedbackOrders);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [PendingFeedbackOrders] >> Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [PendingFeedbackOrders] >> Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void submitFeedback(final String orderID, RequestSubmitOrderFeedback requestSubmitOrderFeedback, final StatusCallback callback){

        retrofitInterface.submitFeedback(deviceID, appVersion, localStorageData.getAccessToken(), requestSubmitOrderFeedback, orderID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {

                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [SubmitFeedback] >> Success , for order_id = " + orderID);
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [SubmitFeedback] >> Failure , for order_id = " + orderID);
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [SubmitFeedback] >> Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [SubmitFeedback] >> Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void skipFeedback(final String orderID, final StatusCallback callback){

        retrofitInterface.skipOrderFeedback(deviceID, appVersion, localStorageData.getAccessToken(), orderID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {

                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [SkipFeedback] >> Success , for order_id = " + orderID);
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [SkipFeedback] >> Failure , for order_id = " + orderID);
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [SkipFeedback] >> Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [SkipFeedback] >> Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getOrderItemFeedback(final String orderItemID, final FeedbackCallback callback){

        retrofitInterface.getOrderItemFeedback(deviceID, appVersion, localStorageData.getAccessToken(), orderItemID, new Callback<FeedbackDetails>() {
            @Override
            public void success(FeedbackDetails feedbackDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [OrderItemFeedback] >> Success");
                callback.onComplete(true, 200, "Success", feedbackDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [OrderItemFeedback] >> Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [OrderItemFeedback] >> Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

}
