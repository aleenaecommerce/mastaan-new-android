package com.mastaan.buyer.backend.models;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public class RequestAddOnlinePayment {
    int pt;
    double amt;
    String tid;
    String pid;

    public RequestAddOnlinePayment(int paymentType, double amount, String transactionID, String paymentID){
        this.pt = paymentType;
        this.amt = amount;
        this.tid = transactionID;
        this.pid = paymentID;
    }

    public double getAmount() {
        return amt;
    }

}
