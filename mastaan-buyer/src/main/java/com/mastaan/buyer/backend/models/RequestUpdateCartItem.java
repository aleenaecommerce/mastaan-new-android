package com.mastaan.buyer.backend.models;

import com.mastaan.buyer.models.SelectedAttributeDetails;

import java.util.List;

/**
 * Created by aleena on 17/10/15.
 */

public class RequestUpdateCartItem {

    String i;   //  MeatItemID
    String h;   //  HashValue
    String dby;     //  Delivery Date
    double n;      //  NoOfKGs
    List<SelectedAttributeDetails> a;   //  SelectedAttributes
    String wcs;
    String si;

    public RequestUpdateCartItem(String meatItemID, String hashValue, String slotID, String deliveryDateAndTime, double noOfKGs, List<SelectedAttributeDetails> selectedAttributes, String specialInstructions){
        this.i = meatItemID;
        this.h = hashValue;
        this.dby = deliveryDateAndTime;
        this.n = noOfKGs;
        this.a = selectedAttributes;
        this.wcs = slotID;
        this.si = specialInstructions;
    }
}
