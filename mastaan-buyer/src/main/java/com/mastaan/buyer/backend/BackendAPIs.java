package com.mastaan.buyer.backend;

import android.content.Context;

import com.mastaan.buyer.R;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by venkatesh on 15/9/15.
 */

public class BackendAPIs {

    Context context;
    String deviceID;
    int appVersion;

    RestAdapter restAdapter;
    //RetrofitInterface retrofitInterface;

    GeneralAPI generalAPI;
    AuthenticationAPI authenticationAPI;
    ProfileAPI profileAPI;
    AddressBookAPI addressBookAPI;
    MeatItemsAPI meatItemsAPI;
    CartAPI cartAPI;
    StockAPI stockAPI;
    CouponsAPI couponsAPI;
    OrdersAPI ordersAPI;
    PaymentAPI paymentAPI;
    WalletAPI walletAPI;
    CustomerSupportAPI customerSupportAPI;
    FeedbacksAPI feedbacksAPI;


    public BackendAPIs(Context context, String deviceID, int appVersion){
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
    }

    private RestAdapter getRestAdapter(){
        if ( restAdapter == null ){
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setConnectTimeout(5, TimeUnit.MINUTES);
            okHttpClient.setReadTimeout(5, TimeUnit.MINUTES);

            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(context.getString(R.string.mastaan_base_url))
                    .setClient(new OkClient(okHttpClient))
                    /*.setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            request.addHeader("x-access-token", new LocalStorageData(context).getAccessToken());
                        }
                    })*/
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            //retrofitInterface = restAdapter.create(RetrofitInterface.class);
        }
        return restAdapter;
    }

    public GeneralAPI getGeneralAPI() {
        if ( generalAPI == null ){
            generalAPI = new GeneralAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return generalAPI;
    }

    public AuthenticationAPI getAuthenticationAPI() {
        if ( authenticationAPI == null ){
            authenticationAPI = new AuthenticationAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return authenticationAPI;
    }

    public ProfileAPI getProfileAPI() {
        if ( profileAPI == null ){
            profileAPI = new ProfileAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return profileAPI;
    }

    public AddressBookAPI getAddressBookAPI() {
        if ( addressBookAPI == null ){
            addressBookAPI = new AddressBookAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return addressBookAPI;
    }

    public MeatItemsAPI getMeatItemsAPI() {
        if ( meatItemsAPI == null ){
            meatItemsAPI = new MeatItemsAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return meatItemsAPI;
    }

    public StockAPI getStockAPI() {
        if ( stockAPI == null ){
            stockAPI = new StockAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return stockAPI;
    }

    public CartAPI getCartAPI() {
        if ( cartAPI == null ){
            cartAPI = new CartAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return cartAPI;
    }

    public CouponsAPI getCouponsAPI() {
        if ( couponsAPI == null ){
            couponsAPI = new CouponsAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return couponsAPI;
    }

    public PaymentAPI getPaymentAPI() {
        if ( paymentAPI == null ){
            paymentAPI = new PaymentAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return paymentAPI;
    }

    public WalletAPI getWalletAPI() {
        if ( walletAPI == null ){
            walletAPI = new WalletAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return walletAPI;
    }

    public OrdersAPI getOrdersAPI() {
        if ( ordersAPI == null ){
            ordersAPI = new OrdersAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return ordersAPI;
    }

    public CustomerSupportAPI getCustomerSupportAPI() {
        if ( customerSupportAPI == null ){
            customerSupportAPI = new CustomerSupportAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return customerSupportAPI;
    }

    public FeedbacksAPI getFeedbacksAPI() {
        if ( feedbacksAPI == null ){
            feedbacksAPI = new FeedbacksAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return feedbacksAPI;
    }
}
