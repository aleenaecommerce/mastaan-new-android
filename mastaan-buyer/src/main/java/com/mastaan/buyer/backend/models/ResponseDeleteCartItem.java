package com.mastaan.buyer.backend.models;

import com.mastaan.buyer.models.CartTotalDetails;

import java.util.List;

/**
 * Created by venkatesh on 15/3/16.
 */

public class ResponseDeleteCartItem {

    String code;

    String msg;                     // Message
    CartTotalDetails cartDetails;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return msg;
    }

    public CartTotalDetails getCartTotalDetails() {
        if ( cartDetails != null ){ return cartDetails; }
        return new CartTotalDetails();
    }
}
