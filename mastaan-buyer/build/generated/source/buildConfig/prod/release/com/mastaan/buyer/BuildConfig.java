/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mastaan.buyer;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.mastaan.buyer";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "prod";
  public static final int VERSION_CODE = 81;
  public static final String VERSION_NAME = "2.5.49";
  // Field from product flavor: prod
  public static final boolean PRODUCTION = true;
}
