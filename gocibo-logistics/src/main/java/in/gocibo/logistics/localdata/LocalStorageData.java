package in.gocibo.logistics.localdata;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.aleena.common.methods.CommonMethods;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.models.User;
import in.gocibo.logistics.models.WhereToNextItem;

public class LocalStorageData {

    public SharedPreferences prefs;
    public SharedPreferences.Editor editor;
    Context context;
    CommonMethods commonMethods;

    public LocalStorageData(Context context) {
        this.context = context;
        commonMethods = new CommonMethods();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        editor = prefs.edit();
    }

    public void setSession(String accessToken, User user){
        editor.putBoolean("sessionFlag", true);
        editor.putString("token", accessToken);
        editor.putString("user_details_json", new Gson().toJson(user));
        editor.putString("user_id", user.get_id());
        editor.putString("user_name", user.getName());
        editor.commit();
    }

    public void clearSession(){
        editor.putBoolean("sessionFlag", false);
        editor.putString("token", "");
        editor.putString("user_details_json", "");
        editor.putString("user_id", "");
        editor.putString("user_name", "");
        editor.commit();
    }

    public boolean getSessionFlag(){
        return prefs.getBoolean("sessionFlag", false);
    }

    public String getAccessToken() {
        return prefs.getString("token", "");
    }

    public String getUserID() {
        return prefs.getString("user_id", "");
    }

    public String getUserName() {
        return prefs.getString("user_name", "");
    }

    public void setUser(User user) {
        editor.putString("user_details_json", new Gson().toJson(user));
        editor.commit();
    }

    public User getUser() {
        String user_details_json = prefs.getString("user_details_json", "");
        return new Gson().fromJson(user_details_json, User.class);
    }

    //-----

    public void setServerTime(String serverTime){
        editor.putString("server_time", serverTime);
        editor.commit();
    }

    public String getServerTime(){
        return prefs.getString("server_time", "");
    }

    public void setUpgradeURL(String upgradeURL){
        editor.putString("upgrade_url", upgradeURL);
        editor.commit();
    }

    public String getUpgradeURL(){
        return prefs.getString("upgrade_url", "");
    }

    public void storeWhereToNextPlaces(List<WhereToNextItem> whereToNextList) {
        if ( whereToNextList != null ){
            List<String> where_to_next_jsons_list = new ArrayList<String>();
            for (int i = 0; i < whereToNextList.size(); i++) {
                where_to_next_jsons_list.add(new Gson().toJson(whereToNextList.get(i)));
            }
            editor.putString("where_to_next_list", CommonMethods.getJSONArryString(where_to_next_jsons_list));
        }else{
            editor.putString("where_to_next_list", "[]");
        }
        editor.commit();
    }

    public List<WhereToNextItem> getWhereToNextPlaces() {
        List<WhereToNextItem> whereToNextList = new ArrayList<>();
        try {
            JSONArray where_to_next_jsons_list = new JSONArray(prefs.getString("where_to_next_list", "[]"));
            for (int i = 0; i < where_to_next_jsons_list.length(); i++) {
                whereToNextList.add(new Gson().fromJson(where_to_next_jsons_list.get(i).toString(), WhereToNextItem.class));
            }
        } catch (Exception e) {}

        return whereToNextList;
    }

    //------------

    public void saveLastLocation(LatLng loc) {
        String last_loc = loc.latitude + "," + loc.longitude;
        Log.d("Retrofit ", Constants.LOG_TAG + " Saved Location " + last_loc);
        editor.putString("last_loc", last_loc);
        editor.commit();
    }

    public void saveLastLocation(String last_loc) {
        Log.d("Retrofit ", Constants.LOG_TAG + " Saved Location" + last_loc);

        editor.putString("last_loc", last_loc);
        editor.commit();
    }

    public String getLastLocation() {
        String s = prefs.getString("last_loc", null);
        Log.d("Retrofit ", Constants.LOG_TAG + " Fetched " + s);
        return prefs.getString("last_loc", null);
    }

    public void clearLastLocation() {
        Log.d("Retrofit ", Constants.LOG_TAG + " Clearing Last Location");
        editor.remove("last_loc");
        editor.commit();
    }

}
