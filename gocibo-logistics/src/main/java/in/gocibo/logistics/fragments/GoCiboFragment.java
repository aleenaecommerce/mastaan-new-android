package in.gocibo.logistics.fragments;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleena.common.fragments.vFragment;

import in.gocibo.logistics.R;
import in.gocibo.logistics.backend.BackendAPIs;
import in.gocibo.logistics.localdata.LocalStorageData;

public class GoCiboFragment extends vFragment {

    public int INTERNET_ENABLER_ACTIVITY_CODE;
    public int LOCATION_ENABLER_ACTIVITY_CODE;
    public int LOCATION_SELECTOR_ACTIVITY_CODE;
    public int PLACE_SEARCH_ACTIVITY_CODE;
    public int HOME_ACTIVITY_CODE;
    public String HOME_RECEIVER_NAME;
    Context context;
    String GOCIBO_LOGISTICS_BASE_URL;
    BackendAPIs gociboLogisticsBackendAPIs;

    LocalStorageData localStorageData;
    BackendAPIs backendAPIs;


    public GoCiboFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();

        localStorageData = new LocalStorageData(context);
        gociboLogisticsBackendAPIs = new BackendAPIs(context);

        GOCIBO_LOGISTICS_BASE_URL = getString(R.string.base_url);

        // ACITIVITY_CODES

        INTERNET_ENABLER_ACTIVITY_CODE = getResources().getInteger(R.integer.internet_enabler_activity_code);
        LOCATION_ENABLER_ACTIVITY_CODE = getResources().getInteger(R.integer.location_enabler_activity_code);
        LOCATION_SELECTOR_ACTIVITY_CODE = getResources().getInteger(R.integer.location_selector_activity_code);
        PLACE_SEARCH_ACTIVITY_CODE = getResources().getInteger(R.integer.place_search_activity_code);
        HOME_ACTIVITY_CODE = getResources().getInteger(R.integer.home_activity_code);

        // RECEIVER NAMES

        HOME_RECEIVER_NAME = getResources().getString(R.string.home_receiver_name);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public LocalStorageData getLocalStorageData() {
        if ( localStorageData == null ){
            localStorageData = new LocalStorageData(context);
        }
        return localStorageData;
    }

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(context);
        }
        return backendAPIs;
    }

}
