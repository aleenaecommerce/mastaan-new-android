package in.gocibo.logistics.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.models.SidebarItemDetails;


public class SideBarFragment extends GoCiboFragment {

    TextView version_name;
    LinearLayout itemsHolder;

    List<SidebarItemDetails> sidebarItems = new ArrayList<>();

    Callback callback;

    public interface Callback {
        void onItemSelected(int position, String item_name);
    }

    public SideBarFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sidebar, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //-------------

        version_name = (TextView) view.findViewById(R.id.version_name);
//        version_name.setText(String.format("V %s", BuildConfig.VERSION_NAME));

        itemsHolder = (LinearLayout) view.findViewById(R.id.itemsHolder);

        //----------

        displaySideBarItems();     // Display Items List...

    }

    public void displaySideBarItems() {

        sidebarItems = new ArrayList<>();
        sidebarItems.add(new SidebarItemDetails("Profile", R.drawable.ic_profile));
        sidebarItems.add(new SidebarItemDetails("KM Diary", R.drawable.ic_kmlog));
        sidebarItems.add(new SidebarItemDetails(Html.fromHtml("Logout (<b>" + localStorageData.getUserName() + "</b>)"), R.drawable.ic_logout));

        for (int i = 0; i < sidebarItems.size(); i++) {
            final int position = i;
            final SidebarItemDetails sidebarItemDetails = sidebarItems.get(i);

            View itemView = inflater.inflate(R.layout.view_sidebar_item, null, false);
            itemsHolder.addView(itemView);

            ImageView image = (ImageView) itemView.findViewById(R.id.image);
            TextView title = (TextView) itemView.findViewById(R.id.title);

            image.setBackgroundResource(sidebarItemDetails.getIcon());
            title.setText(sidebarItemDetails.getName());

            itemView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSideBarItemSelected(position);
                }
            });
        }
    }

    public void onSideBarItemSelected(int position) {

        String selectedItem = sidebarItems.get(position).getName().toString();

//        if (selectedItem.equalsIgnoreCase("Profile")) {
//            Intent profileAct = new Intent(context, ProfileActivity.class);
//            startActivity(profileAct);
//        }
//        else if (selectedItem.equalsIgnoreCase("KM Diary")) {
//            Intent kmLogAct = new Intent(context, KMLogActivity.class);
//            startActivity(kmLogAct);
//        }
//        else if (selectedItem.contains("Logout")) {
//            logOut();
//        }

        if (callback != null) {
            callback.onItemSelected(position, selectedItem);
        }
    }

    public void setSidebarItemSelectedListener(Callback mCallback) {
        this.callback = mCallback;
    }

//    public void logOut() {
//
//        showLoadingDialog("Logging out, wait...");
//        getBackendAPIs().getAccountsAPI().logout(new StatusCallback() {
//            @Override
//            public void onComplete(boolean status, int statusCode, String message) {
//                closeLoadingDialog();
//
//                localStorageData.clearSession();
//                Intent launchAct = new Intent(context, LaunchActivity.class);
//                ComponentName componentName = launchAct.getComponent();
//                Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
//                startActivity(mainIntent);
//            }
//        });
//
//    }

}