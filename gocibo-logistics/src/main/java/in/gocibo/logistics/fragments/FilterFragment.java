package in.gocibo.logistics.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aleena.common.methods.CommonMethods;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.R;


public class FilterFragment extends GoCiboFragment {

    Callback callback;

    RadioGroup filterGroup;
    List<String> filterList = new ArrayList<>();

    public interface Callback {
        void onItemSelected(int position, String itemName);
    }

    public FilterFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_filter, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //--------

        filterGroup = (RadioGroup) view.findViewById(R.id.filterGroup);
        view.findViewById(R.id.calculateTotal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( callback != null ){
                    callback.onItemSelected(-1, "total");
                }
            }
        });

        //--------

        displayFilterItems();

    }

    public void displayFilterItems() {

        filterList = CommonMethods.getStringListFromStringArray(new String[] {"All", "Ordered", "Acknowledged", "Processing", "Processed", "Pickup pending", "Delivering", "Delivered", "Failed", "Rejected"});

        for (int i=0;i<filterList.size();i++){
            final int index = i;
            final RadioButton radioButton = new RadioButton(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            filterGroup.addView(radioButton, layoutParams);

            radioButton.setText(filterList.get(i));
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if ( isChecked ){
                        if ( callback != null ) {
                            callback.onItemSelected(index, radioButton.getText().toString());
                        }
                    }
                }
            });
        }
    }

    public void setItemSelectionListener(Callback mCallback) {
        this.callback = mCallback;
    }


}
