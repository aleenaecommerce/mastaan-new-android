package in.gocibo.logistics.flows;

import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.aleena.common.interfaces.LocationCallback;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.backend.DabbasAPI;
import in.gocibo.logistics.models.Dabba;

/**
 * Created by venkatesh on 30/3/16.
 */
public class DabbasSelectionFlow {

    GoCiboToolBarActivity activity;

    //List<Dabba> itemsList = new ArrayList<>();
    List<String> selectedDabbas = new ArrayList<>();

    Callback callback;

    public interface Callback{
        void onComplete(List<String> selectedDabbas);
    }

    public DabbasSelectionFlow(GoCiboToolBarActivity activity){
        this.activity = activity;
    }

    public DabbasSelectionFlow setSelectedDabbas(List<String> selectedDabbas) {
        this.selectedDabbas = selectedDabbas;
        return this;
    }

    public void start(Callback callback){
        this.callback = callback;

        activity.getCurrentLocation(new LocationCallback() {
            @Override
            public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                if (status) {
                    activity.showLoadingDialog("Getting dabbas in stock, wait...");
                    activity.getBackendAPIs().getDabbasAPI().getDabbasInStock(location, new DabbasAPI.DabbasListCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<Dabba> dabbasList) {
                            activity.closeLoadingDialog();
                            if (status) {
                                showDabbasWithSelected(dabbasList);
                            } else {
                                activity.showToastMessage("Something went wrong while getting dabbas in stock, try again!");
                            }
                        }
                    });
                } else {
                    activity.showToastMessage(message);
                }
            }
        });
    }

    private void showDabbasWithSelected(List<Dabba> itemsList){

        if ( selectedDabbas != null && selectedDabbas.size() > 0 ){
            for (int i=0;i<selectedDabbas.size();i++){
                itemsList.add(0, new Dabba(selectedDabbas.get(i)));
            }
        }

        View selectDabbasDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_select_dabbas, null);
        final LinearLayout itemsHolder = (LinearLayout) selectDabbasDialogView.findViewById(R.id.itemsHolder);

        for (int i=0;i<itemsList.size();i++){

            final Dabba dabba = itemsList.get(i);

            if ( dabba != null ){

                View dabbaHolderView = LayoutInflater.from(activity).inflate(R.layout.view_check_item, null);
                itemsHolder.addView(dabbaHolderView);

                final CheckBox checkbox = (CheckBox) dabbaHolderView.findViewById(R.id.checkbox);
                checkbox.setText(dabba.get_id());
                if ( selectedDabbas.contains(dabba.get_id())){
                    checkbox.setChecked(true);
                }
                dabbaHolderView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkbox.performClick();
                        if (checkbox.isChecked()) {
                            if (selectedDabbas.contains(dabba.get_id()) == false) {
                                selectedDabbas.add(dabba.get_id());
                            }
                        } else {
                            selectedDabbas.remove(dabba.get_id());
                        }
                    }
                });
            }
        }

        selectDabbasDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();
            }
        });

        selectDabbasDialogView.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( getSelectedDabbas().size() > 0 ){
                    activity.closeCustomDialog();
                    if ( callback != null ){
                        callback.onComplete(getSelectedDabbas());
                    }
                }else{
                    activity.showToastMessage("* Selecte atleast 1 dabba");
                }
            }
        });
        activity.showCustomDialog(selectDabbasDialogView);

    }

    public List<String> getSelectedDabbas() {
        if ( selectedDabbas == null ){ selectedDabbas = new ArrayList<>();  }
        return selectedDabbas;
    }
}
