package in.gocibo.logistics.flows;

import android.location.Location;
import android.text.Html;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.google.android.gms.maps.model.LatLng;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.models.Job;
import in.gocibo.logistics.models.OrderItemDetails;

/**
 * Created by venkatesh on 27/4/16.
 */
public class PickupActionsFlow {

    GoCiboToolBarActivity activity;
    OrderItemDetails orderItemDetailsItemDetails;
    StatusCallback callback;

    public PickupActionsFlow(GoCiboToolBarActivity activity) {
        this.activity = activity;
    }

    //===== CLAIM PICKUP FLOW

    public void claimPickup(final OrderItemDetails orderItemDetailsItemDetails, final StatusCallback callback) {
        this.orderItemDetailsItemDetails = orderItemDetailsItemDetails;
        this.callback = callback;

        activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Can you pickup the <b>" + orderItemDetailsItemDetails.getDish().getDishDetails().getName() + "</b> from <b>" + orderItemDetailsItemDetails.getChefDetails().getFullName() + "</b> ?"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equalsIgnoreCase("YES")) {

                    activity.getCurrentLocation(new LocationCallback() {
                        @Override
                        public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                            if ( status ){
                                confirmPickup(location);
                            }else{
                                activity.showToastMessage(message);
                            }
                        }
                    });
                }
            }
        });
    }

    private void confirmPickup(final Location currentLocation) {

        activity.showLoadingDialog("Updating your pickup request, wait...");
        activity.getBackendAPIs().getDeliveriesAPI().createJob(currentLocation.getLatitude(), currentLocation.getLongitude(), new Job(orderItemDetailsItemDetails), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int status_code, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Your pickup request updated successfully");
                    callback.onComplete(true, 200, "Success");
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating your pickup request.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                confirmPickup(currentLocation);
                            }
                        }
                    });
                }
            }
        });
    }

    //======




}