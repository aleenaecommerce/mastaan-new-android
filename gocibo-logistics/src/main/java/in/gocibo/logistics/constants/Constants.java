package in.gocibo.logistics.constants;

/**
 * Created by venkatesh on 8/9/15.
 */
public final class Constants {

    public static final String CHEF = "CHEF";
    public static final String CUSTOMER = "CUSTOMER";

    public static final String LOG_TAG = "GociboLogisticsLogs";
    public static final String DEFAULT_PREFERENCES_NAME = "LogisticsPreferences";
    public static final String TOKEN = "x-access-token";
    static final String BASE_URL = "http://devapi.gocibo.in:3000/1/logistics";
    public static final String SHOW_WHERE_TO_NEXT_FLOW = "SHOW_WHERE_TO_NEXT_FLOW";

    public static class code {

        public static class response {
            public static final int EMAIL_INVALID = 101;
            public static final int WRONG_PASSWORD = 102;
            public static final int STATUS_OK = 200;

            public static final int UN_KNOWN = 1000;
            public static final int NO_DATA = 1001;
            public static final int NOT_AVAILABLE = 500;
        }
    }

    public static class param {
        public static final String Location = "loc";
        public static final String TOKEN = "x-access-token";
    }
}
