package in.gocibo.logistics.models;

import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Naresh-Crypsis on 17-12-2015.
 */
public class Job {
    String cu_or_ch_id;
    String item_id;
    String chef_upload_id;
    String cardType;// pickup or delivery or retrieval
    String area;
    String chef_or_customer_name;
    String address_line_1;
    String address_line_2;
    String land_mark;
    String item_name;// for pickup or delivery only
    Integer item_type;// for pickup or delivery only // veg or non-veg
    int number_of_portions;
    List<String> dabbas; //dabba_ids
    String phone_number;
    String alternative_number;
    String slot; // time slot for pickup/retrieval/delivery
    int local_status;
    String loc;
    double total_amount_of_order;
    int payment_type;

    float buyer_outstanding;

    public Job(String cu_or_ch_id) {
        this.cu_or_ch_id = cu_or_ch_id;
    }

    public Job(OrderItemDetails orderItemDetails) {
        item_id = orderItemDetails.getId();
        chef_upload_id = orderItemDetails.getDish()._id;
        item_name = orderItemDetails.getDish().getDishDetails().getName();
        item_type = orderItemDetails.getDish().getDishDetails().getType();
        number_of_portions = orderItemDetails.getQuantity();
        total_amount_of_order = orderItemDetails.getDeliveryDetails().getBillAmount();
        payment_type = orderItemDetails.getDeliveryDetails().getPaymentType();
                buyer_outstanding = orderItemDetails.getDeliveryDetails().getBuyerDetails().getOutstandingAmount();
        dabbas = orderItemDetails.getDabbas();
        slot = orderItemDetails.getDeliverBy();
        if (orderItemDetails.getStatus() == 0 || orderItemDetails.getStatus() == 1 || orderItemDetails.getStatus() == 7) {
            cu_or_ch_id = orderItemDetails.getChefDetails().get_id();
            cardType = "Pickup";
            local_status = 0;
            area = orderItemDetails.getChefDetails().getArea();
            chef_or_customer_name = orderItemDetails.getChefName();
            address_line_1 = orderItemDetails.getChefDetails().getAddressLine1();
            address_line_2 = orderItemDetails.getChefDetails().getAddressLine2();
            land_mark = orderItemDetails.getChefDetails().getLandMark();
            phone_number = orderItemDetails.getChefDetails().getMobileNumber();
            alternative_number = orderItemDetails.getChefDetails().getAlternativeMobileNumber();
            loc = orderItemDetails.getChefDetails().getLocation();
        } else {
            cu_or_ch_id = orderItemDetails.getCustomerDetails().get_id();
            cardType = "Delivery";
            local_status = 1;
    int payment_type;
            area = orderItemDetails.getCustomerDetails().getArea();
            chef_or_customer_name = orderItemDetails.getCustomerDetails().getCustomerName();
            address_line_1 = orderItemDetails.getCustomerDetails().getAddressLine1();
            address_line_2 = orderItemDetails.getCustomerDetails().getAddressLine2();
            land_mark = orderItemDetails.getCustomerDetails().getLandMark();
            phone_number = orderItemDetails.getCustomerDetails().getMobileNumber();
            loc = orderItemDetails.getCustomerDetails().getLocation();
        }
    }

    public Job(RetrievalItemDetails details) {
        local_status = 2;
        cardType = "Retrieval";
        cu_or_ch_id = details.getCustomer_id();
        item_id = details.getId();
        this.loc = details.getLatLng().latitude + "," + details.getLatLng().longitude;
        area = details.getArea();
        chef_or_customer_name = details.getCustomerName();
        address_line_2 = details.getAddress_line_2();
        address_line_1 = details.getAddress_line_1();
        land_mark = details.getLand_mark();
        number_of_portions = details.getNumberOfPortions();
        dabbas = details.getDabbass();
        phone_number = details.getPhoneNumber();
        alternative_number = details.getAltPhoneNumber();
        slot = details.getRetrievalTime();
    }

    public LatLng getLocation() {
        return LatLngMethods.getLatLngFromString(loc);
    }

    public String getCu_or_ch_id() {
        return cu_or_ch_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public String getChef_upload_id() {
        return chef_upload_id;
    }

    public Integer getItem_type() {
        return item_type;
    }

    public int getLocal_status() {
        return local_status;
    }

    public int getNumber_of_portions() {
        return number_of_portions;
    }

    public List<String> getDabbas() {
        return dabbas;
    }

    public void setDabbas(ArrayList<String> dabbas) {
        this.dabbas = dabbas;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public String getAddress_line_2() {
        return address_line_2;
    }

    public String getAlternative_number() {
        return alternative_number;
    }

    public String getArea() {
        return area;
    }

    public String getCardType() {
        return cardType;
    }

    public String getChef_or_customer_name() {
        return chef_or_customer_name;
    }

    public String getItem_name() {
        return item_name;
    }

    public String getLand_mark() {
        return land_mark;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getLoc() {
        return loc;
    }

    public String getSlot() {
        return slot;
    }

    public String getFormatedDabbas() {
        if (dabbas == null)
            return null;
        StringBuilder builder = new StringBuilder();
        int number_of_portions = dabbas.size();
        if (number_of_portions > 0) {
            for (int i = 0; i < number_of_portions - 1; i++) {
                builder.append(dabbas.get(i)).append(", ");
            }
            builder.append(dabbas.get(number_of_portions - 1));
        }
        return new String(builder);
    }

    public double getAmount() {
        return total_amount_of_order;
    }

    public int getPaymentType() {
        return payment_type;
    }

    public String getPaymentTypeString() {
        if ( payment_type == 1 ){
            return "COD";
        }
        else if ( payment_type == 6 ){
            return "OP";
        }
        return "";
    }

    public float getBuyerOutstanding() {
        return buyer_outstanding;
    }
}
