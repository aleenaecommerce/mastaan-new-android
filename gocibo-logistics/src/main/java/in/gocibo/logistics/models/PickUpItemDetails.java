package in.gocibo.logistics.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import com.aleena.common.models.AddressBookItemDetails;

/**
 * Created by Kishore on 11/10/2015.
 */
public class PickUpItemDetails {
    private String chef_name;
    private LatLng lat_lng;
    private String phone_number;
    private AddressBookItemDetails address;
    private String item_name;
    private float distance = 0.0f;
    private String item_type;
    private int number_of_portions;
    private String alt_phone_number;
    private List<String> dabbas;
    private String pickup_time;
    private int status = 0;

    public PickUpItemDetails(String chef_name, String phone_number) {
        this.chef_name = chef_name;
        this.phone_number = phone_number;
    }

    public PickUpItemDetails item_name(String item_name) {
        this.item_name = item_name;
        return this;
    }

    public PickUpItemDetails address(AddressBookItemDetails address) {
        this.address = address;
        return this;
    }

    public PickUpItemDetails lat_lng(LatLng lat_lng) {
        this.lat_lng = lat_lng;
        return this;
    }

    public PickUpItemDetails item_type(String item_type) {
        this.item_type = item_type;
        return this;
    }

    public PickUpItemDetails number_of_portions(int number_of_portions) {
        this.number_of_portions = number_of_portions;
        return this;
    }

    public PickUpItemDetails alt_phone_number(String alt_phone_number) {
        this.alt_phone_number = alt_phone_number;
        return this;
    }

    public PickUpItemDetails dabbas(List<String> dabbas) {
        this.dabbas = dabbas;
        return this;
    }

    public PickUpItemDetails pickup_time(String pickup_time) {
        this.pickup_time = pickup_time;
        return this;
    }

    public String getChefName() {
        return chef_name;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public AddressBookItemDetails getAddress() {
        return address;
    }

    public String getItemName() {
        return item_name;
    }

    public LatLng getLatLng() {
        return lat_lng != null ? lat_lng : address.getLatLng();
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {

        this.distance = (Math.round(distance * 100) / 100) / 1000.0f;
    }

    public String getItemType() {
        return item_type;
    }

    public void setItemType(String item_type) {
        this.item_type = item_type;
    }

    public String getAltPhoneNumber() {
        return alt_phone_number;
    }

    public void setAltPhoneNumber(String alt_phone_number) {
        this.alt_phone_number = alt_phone_number;
    }

    public int getNumberOfPortions() {
        return number_of_portions;
    }

    public void setNumberOfPortions(int number_of_portions) {
        this.number_of_portions = number_of_portions;
    }

    public List<String> getDabbass() {
        return dabbas;
    }

    public String getFormatedDabbas() {
        StringBuilder builder = new StringBuilder();
        if (dabbas.size() > 0) {
            for (int i = 0; i < number_of_portions - 1; i++) {
                builder.append(dabbas.get(i)).append(", ");
            }
            builder.append(dabbas.get(number_of_portions - 1));
        }
        return new String(builder);
    }

    public void setDabbas(List<String> dabba_numbers) {
        this.dabbas = dabba_numbers;
    }

    public String getPickupTime() {
        return pickup_time;
    }

    public void setPickupTime(String pickup_time) {
        this.pickup_time = pickup_time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
