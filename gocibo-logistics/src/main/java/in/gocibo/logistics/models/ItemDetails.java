package in.gocibo.logistics.models;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nares Katta on 25/10/2015.
 */

public class ItemDetails implements Serializable {
    String landmark;
    String area;
    LatLng latLng;
    //  ---> common details <---
    private String cu_or_ch_name;
    private String phone_number;
    //    private AddressBookItemDetails address;
    private String item_id;
    private String item_name;
    private int item_type;
    private String phone_number_2;
    private String time;
    private List<String> dabbas;
    private float distance = 0.0f;
    private int number_of_portions;
    private int status = 0;
    private String address_line_1;
    private String address_line_2;
    private String cu_or_ch_id;
    private DeliveryDetails deliveryDetails;
    private String total_amount_of_order;
    //  ---> specific for delivery <---
    private String payment_mode;
    private boolean is_final_delivery = false;
    private String name2;
    private String phone_number_3;
    private String address_line_3;
    private String address_line_4;
    private String land_mark_2;

    public ItemDetails() {
    }

    public ItemDetails(OrderItemDetails item) {
        this.item_id = item.getId();
        String locationString = null;
        // for Chef
        if (item.getMy_local_status() == 0 || item.getMy_local_status() == 1) {
            this.cu_or_ch_name = item.getDish().getDishDetails().getChefDetails().getFirstName() + " " + item.getDish().getDishDetails().getChefDetails().getLastName();
            this.landmark = item.getDish().getDishDetails().getChefDetails().getLandMark();
            this.area = item.getDish().getDishDetails().getChefDetails().getArea();
            this.address_line_1 = item.getDish().getDishDetails().getChefDetails().getAddressLine1();
            this.address_line_2 = item.getDish().getDishDetails().getChefDetails().getAddressLine2();
            this.phone_number_2 = item.getDish().getDishDetails().getChefDetails().getAlternativeMobileNumber();
            this.phone_number = item.getDish().getDishDetails().getChefDetails().getMobileNumber();
            locationString = item.getDish().getDishDetails().getChefDetails().getLocation();
            this.cu_or_ch_id = item.getDish().getDishDetails().getChefDetails().get_id();
            this.name2 = item.getCustomerDetails().getCustomerName();
            this.phone_number_3 = item.getCustomerDetails().getMobileNumber();
            this.address_line_3 = item.getCustomerDetails().getP() + " " + item.getCustomerDetails().getR() + item.getCustomerDetails().getS2();
            this.address_line_4 = item.getCustomerDetails().getS1() + item.getCustomerDetails().getL();
            this.land_mark_2 = item.getCustomerDetails().getLandMark();
        } else if (item.getMy_local_status() == 2 || item.getMy_local_status() == 3) {
            this.cu_or_ch_id = item.getCustomerDetails().get_id();
            this.landmark = item.getCustomerDetails().getLandMark();
            this.land_mark_2 = item.getDish().getDishDetails().getChefDetails().getLandMark();
//            this.area = item.getDeliveryDetails().getEmail()
            this.area = item.getCustomerDetails().getArea();
            this.name2 = item.getDish().getDishDetails().getChefDetails().getFirstName() + " " + item.getDish().getDishDetails().getChefDetails().getLastName();
            this.address_line_1 = item.getCustomerDetails().getP() + " " + item.getCustomerDetails().getR() + item.getCustomerDetails().getS2();
            this.address_line_2 = item.getCustomerDetails().getS1() + item.getCustomerDetails().getL();
            this.phone_number = item.getCustomerDetails().getMobileNumber();
            this.phone_number_3 = item.getDish().getDishDetails().getChefDetails().getMobileNumber();
            this.phone_number_2 = item.getDish().getDishDetails().getChefDetails().getAlternativeMobileNumber();
            locationString = item.getCustomerDetails().getLocation();
            this.cu_or_ch_name = item.getCustomerDetails().getCustomerName();
            this.payment_mode = item.getCustomerDetails().getPaymentMode();
            this.total_amount_of_order = item.getCustomerDetails().getBillAmount() + "";
            this.address_line_3 = item.getDish().getDishDetails().getChefDetails().getAddressLine1();
            this.address_line_4 = item.getDish().getDishDetails().getChefDetails().getAddressLine2();
        }
        this.number_of_portions = item.getDabbas().size();
        this.dabbas = item.getDabbas();
        if (locationString != null) {
            String latString = locationString.substring(0, locationString.indexOf(","));
            String lngString = locationString.substring(locationString.indexOf(",") + 1, locationString.length());
            Float lat = Float.parseFloat(latString);
            Float lng = Float.parseFloat(lngString);
            this.latLng = new LatLng(lat, lng);
        }
        this.deliveryDetails = item.getCustomerDetails();
        this.item_name = item.getDish().getDishDetails().getName();
        this.status = item.getMy_local_status();
        this.item_type = item.getDish().getDishDetails().getType();
        this.time = item.getDeliverBy();
        this.number_of_portions = item.getQuantity();
    }

    public ItemDetails(String cu_or_ch_name, String phone_number) {
        this.cu_or_ch_name = cu_or_ch_name;
        this.phone_number = phone_number;
    }

    public String getName2() {
        return name2;
    }

    public String getPhone_number_3() {
        return phone_number_3;
    }

    public String getLand_mark_2() {
        return land_mark_2;
    }

    public String getAddress_line_3() {
        return address_line_3;
    }

    public String getAddress_line_4() {
        return address_line_4;
    }

    public String getLandmark() {
        return landmark;
    }

    public String getArea() {
        return area;
    }

    public void setAddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }

    public void setAddress_line_2(String address_line_2) {
        this.address_line_2 = address_line_2;
    }

    public String getItemId() {
        return item_id;
    }

    public String getCustomerOrChefId() {
        return cu_or_ch_id;
    }

    public ItemDetails item_name(String item_name) {
        this.item_name = item_name;
        return this;
    }
/*
    public ItemDetails address(AddressBookItemDetails address) {
        this.address = address;
        return this;
    }*/

    public ItemDetails item_type(int item_type) {
        this.item_type = item_type;
        return this;
    }

    public ItemDetails alt_phone_number(String alt_phone_number) {
        this.phone_number_2 = alt_phone_number;
        return this;
    }

    public ItemDetails time(String time) {
        this.time = time;
        return this;
    }

    public ItemDetails dabbas(List<String> dabbas) {
        this.dabbas = dabbas;
        return this;
    }

    public ItemDetails number_of_portions(int number_of_portions) {
        this.number_of_portions = number_of_portions;
        return this;
    }

    public String getAddressLine1() {
        return address_line_1;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public List<String> getDabbas() {
        return dabbas;
    }

    public void setDabbas(List<String> dabbas) {
        this.dabbas = dabbas;
    }

    public String getAltPhoneNumber() {
        return phone_number_2;
    }

    public void setAltPhoneNumber(String alt_phone_number) {
        this.phone_number_2 = alt_phone_number;
    }

    public int getNumberOfPortions() {
        return number_of_portions;
    }

    public void setNumberOfPortions(int number_of_portions) {
        this.number_of_portions = number_of_portions;
    }

    public int getItemType() {
        return item_type;
    }

    public void setItemType(int item_type) {
        this.item_type = item_type;
    }

    /*public AddressBookItemDetails getAddress() {
        return address;
    }*/

 /*   public void setAddress(AddressBookItemDetails address) {
        this.address = address;
    }*/

    public String getItemName() {
        return item_name;
    }

    public void setItemName(String item_name) {
        this.item_name = item_name;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public void setPhoneNumber(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getName() {
        return cu_or_ch_name;
    }

    public void setName(String name) {
        this.cu_or_ch_name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTotalAmountOfOrder() {
        return total_amount_of_order;
    }

    public void setTotalAmountOfOrder(String total_amount_of_order) {
        this.total_amount_of_order = total_amount_of_order;
    }

    public String getPaymentMode() {
        return payment_mode;
    }

    public void setPaymentMode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public boolean isFinalDelivery() {
        return is_final_delivery;
    }

    public void setIsFinalDelivery(boolean is_final_delivery) {
        this.is_final_delivery = is_final_delivery;
    }

    public LatLng getLatLng() {
        return latLng;
    }
/*
    public void makeItRetrieval(Context context, String slot) {
        SharedPreferences preferences = context.getSharedPreferences("LogisticsPreferences", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        RetrievalItemDetails retrievalItemDetails = new RetrievalItemDetails(cu_or_ch_name, phone_number);
        retrievalItemDetails*//*.address(address)*//*.phone_number_2(phone_number_2).dabbas(dabbas).retrieval_time(slot).number_of_portions(number_of_portions)
                .item_name(item_name).item_type(item_type).lat_lng(getLatLng());
        retrievalItemDetails.setId(phone_number + getFormattedDabbas());
        ArrayList<RetrievalItemDetails> oldRetrievals;
        String responseString = preferences.getString("ResponseRetrievalItems", null);
        if (responseString != null) {
            ResponseRetrievalItems response = new Gson().fromJson(responseString, ResponseRetrievalItems.class);
            oldRetrievals = response.getRetrievalItemsDetails();
        } else
            oldRetrievals = new ArrayList<>();
        oldRetrievals.add(retrievalItemDetails);
        ResponseRetrievalItems newRetrievals = new ResponseRetrievalItems();
        newRetrievals.setRetrievalItemsDetails(oldRetrievals);
        String responseDetails = new Gson().toJson(newRetrievals);
        editor.putString("ResponseRetrievalItems", responseDetails);
        editor.commit();
    }*/

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getFormattedDabbas() {
        StringBuilder builder = new StringBuilder();
        if (dabbas.size() > 0) {
            for (int i = 0; i < dabbas.size() - 1 && i < dabbas.size(); i++) {
                builder.append(dabbas.get(i)).append(", ");
            }
            builder.append(dabbas.get(dabbas.size() - 1));
        }
        return new String(builder);
    }

    public String getAddressLine2() {
        return address_line_2;
    }

    public DeliveryDetails getDeliveryDetails() {
        return deliveryDetails;
    }
}
