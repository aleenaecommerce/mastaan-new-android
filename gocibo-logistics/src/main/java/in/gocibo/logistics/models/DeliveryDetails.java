package in.gocibo.logistics.models;

import com.aleena.common.methods.CommonMethods;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Naresh-Crypsis on 30-11-2015.
 */
public class DeliveryDetails {
    private String _id;
    private String c;
    private String da; // full address
    private String dby;// delivery slot
    private String e;  // email
    private String l;  // city
    private String lm; // land mark
    private String m; //  mobile number
    private String n; // name
    private String p; // premise
    private String pin;// pincode
    private String pos;//location
    private String s; // state
    private String s1;//street
    private String s2;
    private int pt; //payment_type
    private BuyerDetails b;
    private String oid;
    private double ta;// bill amount
    private Integer prf;
    private Integer gt;
    private Integer __v;
    private String cd;
    private String ud;
    private Boolean pf;
    private Integer dc;
    private String ar;
    private String os;
    public List<String> i = new ArrayList<>();
    private String r;

    String si;

    public String get_id() {
        return _id;
    }

    public String getFullAddress() {
        return da;
    }

    public String getDeliverBy() {
        return dby;
    }

    public String getEmail() {
        return e;
    }

    public String getMobileNumber() {
        return m;
    }

    public String getCustomerName() {
        return n;
    }


    public String getLocation() {
        return pos;
    }

    public String getStreet() {
        return s1;
    }


    public double getBillAmount() {
        return ta;
    }

    public String getPaymentMode() {
        if ( pt == 1 ){
            return "COD";
        }
        else if ( pt == 6 ){
            return "OP";
        }
        return "";
    }

    public LatLng getLatLng() {
        String locationString = pos;
        if (locationString != null) {
            String latString = locationString.substring(0, locationString.indexOf(","));
            String lngString = locationString.substring(locationString.indexOf(",") + 1, locationString.length());
            Float lat = Float.parseFloat(latString);
            Float lng = Float.parseFloat(lngString);
            return new LatLng(lat, lng);
        }
        return null;
    }

    public String getArea() {
        return ar == null || ar.length() <= 0 ? s1 : ar;
    }

    public String getS2() {
        return s2;
    }


    public String getAddressLine1() {
        return getP() + " " + getR() + getS2();
    }

    public String getAddressLine2() {
        return getS1() + getL();
    }

    public String getDa() {
        return da;
    }

    public String getP() {
        return p;
    }

    public String getR() {
        return r != null ? r + " " : "";
    }

    public String getS1() {
        return s1 == null ? "" : s1 + " ";
    }

    public String getL() {
        return l != null ? l + " " : "";
    }


    public String getSpecialInstructions() {
        if ( si == null ){  si = "";    }
        return si;
    }

    public int getPaymentType() {
        return pt;
    }

    public String getPaymentTypeString() {
        if ( pt == 1 ){
            return "COD";
        }
        else if ( pt == 6 ){
            return "OP";
        }
        return "";
    }

    public double getAmountOfOrder() {
        return ta;
    }

    public void setAmountOfOrder(double ta) {
        this.ta = ta;
    }

    public String getPremise() {
        return p;
    }

    public String getLandMark() {
        return lm;
    }

    public String getDeliveryAddress() {
        return CommonMethods.getStringFromStringArray(new String[]{p, s2, s1, l, s, pin});
    }

    public String getSublocalityLevel1() {
        return s1;
    }

    public String getSublocalityLevel2() {
        return s2;
    }

    public String getLocality() {
        return l;
    }

    public String getState() {
        return s;
    }

    public String getPincode() {
        return pin;
    }

    public BuyerDetails getBuyerDetails() {
        return b;
    }


}
