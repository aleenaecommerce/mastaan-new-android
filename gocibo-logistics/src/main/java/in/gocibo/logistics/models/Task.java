package in.gocibo.logistics.models;

import com.aleena.common.models.AddressBookItemDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Naresh Katta on 7/11/2015.
 */
public class Task {
    String date;
    //    AddressBookItemDetails address;
    List<String> dabbas;
    String name;
    LatLng location;
    String address_line_1;
    String land_mark;
    ItemDetails details;

    public Task(String titleOrDate, AddressBookItemDetails address, List<String> dabbas) {
        this.dabbas = dabbas;
//        this.address = address;
        this.date = titleOrDate;
    }

    public Task(String type, String name, /*AddressBookItemDetails address,*/ List<String> dabbas, LatLng location, String address_line_1, String land_mark, ItemDetails details) {
        this.dabbas = dabbas;
//        this.address = address;
        this.date = type;
        this.name = name;
        this.location = location;
        this.address_line_1 = address_line_1;
        this.land_mark = land_mark;
        this.details = details;
    }

    public Task(String title, List<String> dabbas) {
        this.dabbas = dabbas;
        this.date = title;
    }

    public ItemDetails getDetails() {
        return details;
    }

    //    public AddressBookItemDetails getAddress() {
//        return address;
//    }

    public List<String> getDabbas() {
        return dabbas;
    }

    public String getFormattedDabbas() {
        StringBuffer formatted_dabbas = new StringBuffer();
        for (String dabba : dabbas) {
            formatted_dabbas.append(dabba).append(" ");
        }
        return new String(formatted_dabbas);
    }

    public String getTitle() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public LatLng getLocation() {
        return location;
    }

    public String getLand_mark() {
        return land_mark;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }
}
