package in.gocibo.logistics.models;

/**
 * Created by Naresh-Crypsis on 17-11-2015.
 */
public class Stop {
    Long start_meter_reading;
    Long end_meter_reading;
    String start_time;
    String stop_time;

    Stop() {
    }

    public Stop(Long start_meter_reading, Long end_meter_reading, String start_time, String stop_time) {
        this.start_meter_reading = start_meter_reading;
        this.end_meter_reading = end_meter_reading;
        this.start_time = start_time;
        this.stop_time = stop_time;
    }

    public Long getStart_meter_reading() {
        return start_meter_reading;
    }

    public Long getEnd_meter_reading() {
        return end_meter_reading;
    }

    public String getStart_time() {
        return start_time;
    }

    public String getStop_time() {
        return stop_time;
    }
}
