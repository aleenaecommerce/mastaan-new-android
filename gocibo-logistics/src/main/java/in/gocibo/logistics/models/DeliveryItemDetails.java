package in.gocibo.logistics.models;

import android.content.Context;

import com.aleena.common.models.AddressBookItemDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Naresh-Gocibo on 09-10-2015.
 */
public class DeliveryItemDetails {
    private String customer_name;
    private LatLng lat_lng;
    private String phone_number;
    private AddressBookItemDetails address;
    private String orderId;
    private String item_name;
    private float distance = 0.0f;
    private Integer item_type;
    private int number_of_portions;
    private String alt_phone_number;
    private List<String> dabbas;
    private String delivery_time;
    private String total_amount_of_order;
    private String payment_mode;
    private int status = 0;
    private boolean is_final_delivery = false;

    public DeliveryItemDetails(String customer_name, String phoneNumber) {
        this.customer_name = customer_name;
        this.phone_number = phoneNumber;
    }

    public String getOrderId() {
        return orderId;
    }


    public DeliveryItemDetails item_name(String item_name) {
        this.item_name = item_name;
        return this;
    }

    public DeliveryItemDetails lat_lng(LatLng lat_lng) {
        this.lat_lng = lat_lng;
        return this;
    }

    public DeliveryItemDetails address(AddressBookItemDetails address) {
        this.address = address;
        return this;
    }

    public DeliveryItemDetails item_type(int item_type) {
        this.item_type = item_type;
        return this;
    }

    public DeliveryItemDetails number_of_portions(int number_of_portions) {
        this.number_of_portions = number_of_portions;
        return this;
    }

    public DeliveryItemDetails alt_phone_number(String alt_phone_number) {
        this.alt_phone_number = alt_phone_number;
        return this;
    }

    public DeliveryItemDetails dabbas(List<String> dabbas) {
        this.dabbas = dabbas;
        return this;
    }

    public DeliveryItemDetails delivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
        return this;
    }

    public DeliveryItemDetails total_amount_of_order(String total_amount_of_order) {
        this.total_amount_of_order = total_amount_of_order;
        return this;
    }

    public DeliveryItemDetails payment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
        return this;
    }


    public DeliveryItemDetails is_final_delivery(boolean is_final_delivery) {
        this.is_final_delivery = is_final_delivery;
        return this;
    }

    public String getCustomerName() {
        return customer_name;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public AddressBookItemDetails getAddress() {
        return address;
    }

    public String getItemName() {
        return item_name;
    }

    public LatLng getLatLng() {
        return lat_lng != null ? lat_lng : address.getLatLng();
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {

        this.distance = (Math.round(distance * 100) / 100) / 1000.0f;
    }

    public int getItemType() {
        return item_type;
    }

    public void setItemType(int item_type) {
        this.item_type = item_type;
    }

    public String getAltPhoneNumber() {
        return alt_phone_number;
    }

    public void setAltPhoneNumber(String alt_phone_number) {
        this.alt_phone_number = alt_phone_number;
    }

    public int getNumberOfPortions() {
        return number_of_portions;
    }

    public void setNumberOfPortions(int number_of_portions) {
        this.number_of_portions = number_of_portions;
    }

    public List<String> getDabbass() {
        return dabbas;
    }

    public String getFormatedDabbas() {
        StringBuilder builder = new StringBuilder();
        if (dabbas.size() > 0) {
            for (int i = 0; i < number_of_portions - 1 && i < number_of_portions; i++) {
                builder.append(dabbas.get(i)).append(", ");
            }
            builder.append(dabbas.get(dabbas.size() - 1));
        }
        return new String(builder);
    }

    public void setDabbas(List<String> dabba_numbers) {
        this.dabbas = dabba_numbers;
    }

    public String getDeliveryTime() {
        return delivery_time;
    }

    public void setPickupTime(String pickup_time) {
        this.delivery_time = pickup_time;
    }

    public String getTotalAmountOfOrder() {
        return total_amount_of_order;
    }

    public void setTotalAmountOfOrder(String total_amount_of_order) {
        this.total_amount_of_order = total_amount_of_order;
    }

    public String getPaymentMode() {
        return payment_mode;
    }

    public void setPaymentMode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public void makeItRetrieval(Context context, String retrievalTime) {
        /*SharedPreferences preferences = context.getSharedPreferences("LogisticsPreferences", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        new DeliveryItemsAPI(context*//*, null*//*).getDeliveryItems(new DeliveryItemsAPI.GetDeliveryItemsCallback() {
            @Override
            public void onComplete(List<DeliveryItemDetails> deliveryItemsDetails) {
                List<DeliveryItemDetails> temp = new ArrayList<>(deliveryItemsDetails);
                for (int i = 0; i < deliveryItemsDetails.size(); i++) {
                    DeliveryItemDetails details = deliveryItemsDetails.get(i);
                    if (getCustomerName().equalsIgnoreCase(details.getCustomerName()) && getPhoneNumber().equalsIgnoreCase(details.getPhoneNumber())) {
                        temp.remove(i);
                        break;
                    }
                }
                ResponseDeliveryItems deliveryItemsResponse = new ResponseDeliveryItems();
                deliveryItemsResponse.setDeliveryItems(temp);
                String responseGson = new Gson().toJson(deliveryItemsResponse);
                editor.putString("ResponseDeliveryItems", responseGson);
                editor.commit();
            }
        });
        RetrievalItemDetails retrievalItemDetails = new RetrievalItemDetails(customer_name, phone_number);
        retrievalItemDetails.address(address).alt_phone_number(alt_phone_number).dabbas(dabbas).retrieval_time(retrievalTime).number_of_portions(number_of_portions)
                .item_name(item_name).item_type(item_type).lat_lng(lat_lng);
        retrievalItemDetails.setId(phone_number + getFormatedDabbas());
        ArrayList<RetrievalItemDetails> oldRetrievals;
        String responseString = preferences.getString("ResponseRetrievalItems", null);
        if (responseString != null) {
            ResponseRetrievalItems response = new Gson().fromJson(responseString, ResponseRetrievalItems.class);
            oldRetrievals = response.getRetrievalItemsDetails();
        } else
            oldRetrievals = new ArrayList<>();
        oldRetrievals.add(retrievalItemDetails);
        ResponseRetrievalItems newRetrievals = new ResponseRetrievalItems();
        newRetrievals.setRetrievalItemsDetails(oldRetrievals);
        String responseDetails = new Gson().toJson(newRetrievals);
        editor.putString("ResponseRetrievalItems", responseDetails);
        editor.commit();*/
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public boolean isFinalDelivery() {
        return is_final_delivery;
    }

    public void setIsFinalDelivery(boolean is_final_delivery) {
        this.is_final_delivery = is_final_delivery;
    }
}
