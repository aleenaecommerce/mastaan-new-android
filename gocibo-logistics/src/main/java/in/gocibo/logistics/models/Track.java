package in.gocibo.logistics.models;

/**
 * Created by Naresh-Crypsis on 24-11-2015.
 */
public class Track {
    Float track_distance;
    String track_id;

    Track() {
    }

    public Track(Float track_distance, String track_id) {
        this.track_distance = track_distance;
        this.track_id = track_id;
    }

    public Float getTrack_distance() {
        return track_distance;
    }

    public String getFormattedTrackDistance() {
        StringBuilder distance = new StringBuilder(String.valueOf(track_distance));
        while (distance.length() < 4) {
            distance.append("0");
        }
        return new String(distance);
    }

    public String getTrack_id() {
        return track_id;
    }
}
