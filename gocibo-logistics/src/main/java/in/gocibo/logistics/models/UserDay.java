package in.gocibo.logistics.models;

import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Naresh-Crypsis on 28-10-2015.
 */
public class UserDay {
    String km;
    Date date;
    //    Long start_meter_reading;
//    Long end_meter_reading;
    double distance_in_km;
    //    Task task;
//    List<Task> tasks;
//    List<Stop> stops;
    List<KmLogItem> logItems = new ArrayList<>();

    public UserDay(Date date, String km, double distance_in_km) {
        this.date = date;
        this.km = km;
        this.distance_in_km = distance_in_km;
        ArrayList<String> dabbas = new ArrayList<>();
        dabbas.add(new Dabba("SS111").toString());
        dabbas.add(new Dabba("SS112").toString());
        dabbas.add(new Dabba("SS113").toString());
        PlaceDetails placeDetails;
        placeDetails = new PlaceDetails();
        placeDetails.setLatLng(new LatLng(17.443330, 78.390035));
        placeDetails.setPremise("1-98/87/124");
        placeDetails.setLandmark("Near Tumpa Bengali food");
        placeDetails.setSublocalityLevel_2("Sri Sai Nagar");
        placeDetails.setSublocalityLevel_1("Madhapur");
        placeDetails.setLocality("Hyderabad");
        placeDetails.setState("Telangana");
        placeDetails.setCountry("India");
        placeDetails.setPostalCode("500081");
//        task = new Task("8:30", new AddressBookItemDetails(placeDetails), dabbas);
//        tasks = new ArrayList<>();
        /*for (int i = 0; i < 10; i++) {
            tasks.add(task);
        }*/
    }


    public Date getDate() {
        return date;
    }

    public String getKm() {
        return km;
    }

    /*public void setKm(String km) {
        this.km = km;
    }*/

    /*public List<Task> getTasks() {
        return tasks;
    }*/

    /*public List<Stop> getStops() {
        return stops;
    }*/

    public double getDistanceInKm() {
        return distance_in_km;
    }

    public void setLogItems(List<KmLogItem> logItems) {
        this.logItems = logItems;
    }

    public List<KmLogItem> getLogItems() {
        return logItems;
    }

    /*public String getFormattedDistanceInKm() {

    }*/
}
