package in.gocibo.logistics.models;

import java.io.Serializable;

public class BuyerDetails implements Serializable{

    String _id;     //  ID
    String m;       // Mobile Number
    String e;       //  Email Address
    String f;       //  First Name
    String l;       //  Last Name
    String d;       //  DateOfBirth
    boolean mr;     //  Matrial Status
    String a;       //  Anniversary Date if Married

    long lp;        //  Loyalty Points
    String r;       // Referral ID

    String cd;
    String ud;

    float amt;      // Outstanding Amount

    public BuyerDetails(){}

    public BuyerDetails(String ID, String joinedDate, String mobileNumber, String name, String email, String dateOfBirth, boolean matrialStatus, String anniversaryDate, String referralID){
        this._id = ID;
        this.cd = joinedDate;
        this.m = mobileNumber;
        this.f = name;
        this.e = email;
        this.d = dateOfBirth;
        this.mr = matrialStatus;
        this.a = anniversaryDate;
        this.r = referralID;
    }

    public String getID() {    return _id;     }
    public String getMobile() {      return m;   }

    public String getEmail() {
        return e;
    }
    public void setEmail(String email){  this.e = email;   }

    public String getName() {
        return f;
    }
    public void setName(String name){  this.f = name;   }

    public String getDOB() {
        return d;
    }
    public void setDOB(String dob){  this.d = dob;   }

    public boolean getMatrialStatus() {
        return mr;
    }
    public void setMatrialStatus(boolean mr) {
        this.mr = mr;
    }

    public String getAnniversary() {
        return a;
    }
    public void setAnniversary(String anniversary){  this.a = anniversary;   }

    public long getLoyaltyPoints() {
        return lp;
    }

    public String getReferralID() {
        return r;
    }

    public String getJoinedDate() {     return cd;      }

    public float getOutstandingAmount() {
        return amt;
    }
}
