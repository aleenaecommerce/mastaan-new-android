package in.gocibo.logistics.models;

/**
 * Created by aleena on 28/11/15.
 */
public class Dabba {
    private String _id;
    private String c;//chef id
    private String s;//dabba status

    public Dabba(String _id) {
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }

    @Override
    public String toString() {
        return _id;
    }
}
