package in.gocibo.logistics.models;

import android.util.Log;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.gocibo.logistics.backend.models.RetrievalItem;
import in.gocibo.logistics.constants.Constants;

/**
 * Created by Naresh-Gocibo on 09-10-2015.
 */
public class RetrievalItemDetails implements Serializable {
    private String customer_name;
    private String customer_id;
    private LatLng lat_lng;
    private String phone_number;
    //    private AddressBookItemDetails address;
    private String address_line_1;
    private String address_line_2;
    private String dabba_id;
    private String item_name;
    private float distance = 0.0f;
    private int item_type;
    private int number_of_portions = 0;
    private String alt_phone_number;
    private List<String> dabbas;
    private List<String> dabbas_comments;
    private String retrieval_time;
    private String retrievalDate;
    private String id;
    private int status;
    private String land_mark;
    private String area;
    private int toH = -1;
    private int toM = -1;

    public RetrievalItemDetails(String customer_name, String phoneNumber) {
        this.customer_name = customer_name;
        this.phone_number = phoneNumber;
    }

    public RetrievalItemDetails(RetrievalItem retrievalItem) {
        CommonMethods commonMethods = new CommonMethods();
        this.customer_id = retrievalItem.getItem().getCustomerDetails().get_id();
        this.customer_name = retrievalItem.getItem().getCustomerDetails().getCustomerName();
        // latlng string
        this.phone_number = retrievalItem.getItem().getCustomerDetails().getMobileNumber();
        this.address_line_1 = retrievalItem.getItem().getCustomerDetails().getAddressLine1();
        this.address_line_2 = retrievalItem.getItem().getCustomerDetails().getAddressLine2();
        this.dabba_id = retrievalItem.get_id();
        this.dabbas = new ArrayList<>();
        this.dabbas.add(dabba_id);
        this.dabbas_comments = new ArrayList<>();
        this.dabbas_comments.add(retrievalItem.getComments());
        this.land_mark = retrievalItem.getItem().getCustomerDetails().getLandMark();
        this.area = retrievalItem.getItem().getCustomerDetails().getArea();
        this.number_of_portions = dabbas.size();
        this.lat_lng = retrievalItem.getItem().getCustomerDetails().getLatLng();
        if (retrievalItem.getRt() != null) {
            this.toH = commonMethods.getOnlyHourInLocal(retrievalItem.getRt());
            this.toM = commonMethods.getOnlyMinutesInLocal(retrievalItem.getRt());
            Date date = commonMethods.getDateFromStringInLocal(retrievalItem.getRt());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            Log.i(Constants.LOG_TAG, " " + (toH * 100 + toM) + "");
            StringBuffer timeSlotString = new StringBuffer();
            calendar.add(Calendar.HOUR, -1);
            timeSlotString.append("");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
            timeSlotString.append(simpleDateFormat.format(calendar.getTime()));
            timeSlotString.append(" to ");
            timeSlotString.append(commonMethods.getDateInFormatInLocal(retrievalItem.getRt(), "hh:mm a"));
            this.retrieval_time = new String(timeSlotString);
            this.retrievalDate = commonMethods.getDateInFormatInLocal(retrievalItem.getRt(), "dd MMM");
        }
        this.status = retrievalItem.getLocalStatus();
    }

    public String getRetrievalDate() {
        return retrievalDate;
    }

    public int getToH() {
        return toH;
    }

    public int getToM() {
        return toM;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public String getArea() {
        return area;
    }

    public String getLand_mark() {
        return land_mark;
    }

    public String getAddress_line_2() {
        return address_line_2;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public String getDabbaId() {
        return dabba_id;
    }


    public RetrievalItemDetails item_name(String item_name) {
        this.item_name = item_name;
        return this;
    }

    /*public RetrievalItemDetails address(AddressBookItemDetails address) {
        this.address = address;
        return this;
    }*/

    public RetrievalItemDetails lat_lng(LatLng lat_lng) {
        this.lat_lng = lat_lng;
        return this;
    }

    public RetrievalItemDetails item_type(int item_type) {
        this.item_type = item_type;
        return this;
    }

    public RetrievalItemDetails number_of_portions(int number_of_portions) {
        this.number_of_portions = number_of_portions;
        return this;
    }

    public RetrievalItemDetails alt_phone_number(String alt_phone_number) {
        this.alt_phone_number = alt_phone_number;
        return this;
    }

    public RetrievalItemDetails dabbas(List<String> dabbas) {
        this.dabbas = dabbas;
        return this;
    }

    public RetrievalItemDetails retrieval_time(String retrieval_time) {
        this.retrieval_time = retrieval_time;
        return this;
    }

    public String getCustomerName() {
        return customer_name;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public AddressBookItemDetails getAddress() {
        return /*address*/ null;
    }


    public String getItemName() {
        return item_name;
    }

    public LatLng getLatLng() {
        return lat_lng;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {

        this.distance = (Math.round(distance * 100) / 100) / 1000.0f;
    }

    public int getItemType() {
        return item_type;
    }

    public void setItemType(int item_type) {
        this.item_type = item_type;
    }

    public String getAltPhoneNumber() {
        return alt_phone_number;
    }

    public void setAltPhoneNumber(String alt_phone_number) {
        this.alt_phone_number = alt_phone_number;
    }

    public int getNumberOfPortions() {
        return dabbas == null ? number_of_portions : dabbas.size();
    }

    public void setNumberOfPortions(int number_of_portions) {
        this.number_of_portions = number_of_portions;
    }

    public List<String> getDabbass() {
        return dabbas;
    }

    public List<String> getDabbasComments() {
        return dabbas_comments;
    }

    public String getFormatedDabbas() {
        StringBuilder builder = new StringBuilder();
        int number_of_portions = dabbas.size();
        if (number_of_portions > 0) {
            for (int i = 0; i < number_of_portions - 1; i++) {
                builder.append(dabbas.get(i)).append(", ");
            }
            builder.append(dabbas.get(number_of_portions - 1));
        }
        return new String(builder);
    }

    public void setDabbas(List<String> dabba_numbers) {
        this.dabbas = dabba_numbers;
    }

    public String getRetrievalTime() {
        return retrieval_time != null ? retrieval_time : "Undefined";
    }

    public void setRetrievalTime(String retrieval_time) {
        this.retrieval_time = retrieval_time;
    }
/*
    public void setRetrievalIsTaken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("LogisticsPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        String responseString = preferences.getString("ResponseRetrievalItems", null);
        if (responseString != null) {
            ResponseRetrievalItems response = new Gson().fromJson(responseString, ResponseRetrievalItems.class);
            ArrayList<RetrievalItemDetails> oldRetrievals = response.getRetrievalItemsDetails();
            if (oldRetrievals != null) {
                ArrayList<RetrievalItemDetails> newRetrievals = new ArrayList<>(oldRetrievals);
                for (int i = 0; i < oldRetrievals.size(); i++) {
                    if (this.getId().equalsIgnoreCase(oldRetrievals.get(i).getId())) {
                        newRetrievals.remove(i);
                        break;
                    }
                }
                ResponseRetrievalItems newResponse = new ResponseRetrievalItems();
                newResponse.setRetrievalItemsDetails(oldRetrievals);
                String responseDetails = new Gson().toJson(newRetrievals);
                editor.putString("ResponseRetrievalItems", responseDetails);
            }
        }
    }*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void addDabba(String dabba_id, String dabba_comment) {
        if (dabbas == null)
            dabbas = new ArrayList<>();
        if (dabbas_comments == null)
            dabbas_comments = new ArrayList<>();
        dabbas.add(dabba_id);
        dabbas_comments.add(dabba_comment);
        number_of_portions++;
    }



}
