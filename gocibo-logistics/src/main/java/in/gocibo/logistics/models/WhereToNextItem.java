package in.gocibo.logistics.models;

/**
 * Created by aleena on 15/12/15.
 */
public class WhereToNextItem {
    String id;
    String name;
    // /1/logistics/chefs

    public WhereToNextItem(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
