package in.gocibo.logistics.models;

/**
 * Created by Naresh Katta on 23-01-2016.
 */
public class Hub extends CustomPlace {
    public String _id;
    public String n;
    public String pos;
    public Integer __v;
    public String cd;
    public String ud;

    public String getLoc() {
        return pos;
    }

    public String getName() {
        return n;
    }

    public String getId() {
        return _id;
    }

    public String toString() {
        return "hub_" + _id;
    }
}
