
package in.gocibo.logistics.models;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.models.Cat;
import in.gocibo.logistics.models.ChefDetails;
import in.gocibo.logistics.models.Cu;

public class DishDetails2 {

    public String _id;
    public String n;//name
    public ChefDetails c;
    public Cu cu;
    public int t;//dish type 0-veg 1- non-veg
    public String d;
    public String p;
    public String th;
    public Integer __v;
    public Cat cat;
    public String cd;
    public String ud;
    public String st;
    public String qt;
    public Boolean dec;
    public Boolean apr;
    public List<String> i = new ArrayList<>();

    public ChefDetails getChefDetails() {
        return c;
    }

    public String getName() {
        return n;
    }

    public int getType() {
        return t;
    }
}
