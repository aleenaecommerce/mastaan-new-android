package in.gocibo.logistics.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Naresh-Crypsis on 25-11-2015.
 */
public class User {
    String _id;
    String e;
    String __v;
    String cd;
    String f;//firstName;
    String l; //last name
    String eid;//email
    String m;// mobileNumber
    String em; //emergencyNumber
    String bg;//blood group
    String esi;
    String dl;
    String a1;
    String a2;
    String lmk;
    String ar;
    String ci;
    String pin;
    String s;
    String t;
    VehicleDetails vd;

    List<String> r;     // Roles List

    public String get_id() {
        return _id;
    }

    public String getE() {
        return e;
    }

    public String getCd() {
        return cd;
    }

    public String getFormattedJoinDate() {
        if (cd == null)
            return null;
        String sub = cd.substring(0, cd.indexOf("T"));
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = originalFormat.parse(sub);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            SimpleDateFormat targetFormat = new SimpleDateFormat("MMM dd");
            return targetFormat.format(date);
        } catch (ParseException e1) {
            e1.printStackTrace();
            return cd;
        }
    }

    public String getName() {
        return getF() + " " + getL();
    }

    public String getF() {
        return f == null ? "" : f;
    }

    public String getA1() {
        return a1;
    }

    public String getA2() {
        return a2;
    }

    public String getAr() {
        return ar;
    }

    public String getBg() {
        return bg;
    }

    public String getCi() {
        return ci;
    }

    public String getDl() {
        return dl;
    }

    public String getEid() {
        return eid;
    }

    public String getEm() {
        return em;
    }

    public String getEsi() {
        return esi;
    }

    public String getL() {
        return l == null ? "" : l;
    }

    public String getLmk() {
        return lmk;
    }

    public String getM() {
        return m;
    }

    public String getPin() {
        return pin;
    }

    public String getS() {
        return s;
    }

    public VehicleDetails getVd() {
        return vd;
    }

    public String getProfilePic() {
        return t;
    }

    public List<String> getRoles() {
        if ( r == null ){ return new ArrayList<>(); }
        return r;
    }

    public boolean isReadOnly(){
        if ( getRoles().contains("r") ){    return  true;   }
        return false;
    }

    public boolean isDeliveryBoy(){
        if ( getRoles().contains("db") ){    return  true;   }
        return false;
    }

    public boolean isProcessingManager(){
        if ( getRoles().contains("pm") ){    return  true;   }
        return false;
    }

    public boolean isSuperUser(){
        if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }
}
