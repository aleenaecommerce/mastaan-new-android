package in.gocibo.logistics.models;

/**
 * Created by venkatesh on 28/11/15.
 */
public class DishDetails {

    String _id;                 //  dishID

    public String n;            // dishName
    public String d;            // dishDescription
    public int t;               // dishType   { veg / nonveg }
    public String p;            // dishImageURL
    public String th;           // dishImageThubnailURL
    public String qt;           //  QuantityTYpe

    Chef c;                     //  chefDetails

    public Chef getChefDetails() {
        return c;
    }

    public String getID(){  return _id;  }

    public String getName(){
        if ( n != null ) {
            return n;
        }
        return "";
    }

    public String getDescription(){     return  d;    }

    public String getImageURL(){
        String str = "http:";
        if (p != null) {
            str = str + p;
            return str;
        }
        return null;
    }

    public String getThumbnailImageURL() {
        String str = "http:";
        if (th != null) {
            str = str + th;
            return str;
        }
        return null;
    }

    public String getType(){
        if ( t == 1 ){
            return "non-veg";
        }
        else if ( t== 2 ){
            return "jain";
        }
        return "veg";
    }

    public String getQuantityType(){  return qt;    }

    public String getChefID(){  return  c.getId(); }

    public String getChefFullName(){    return c.getName();    }

    public String getChefFirstName(){    return c.getF();    }
}
