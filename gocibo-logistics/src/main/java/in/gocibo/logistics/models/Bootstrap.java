package in.gocibo.logistics.models;

import java.util.List;

/**
 * Created by aleena on 15/12/15.
 */
public class Bootstrap {
    String serverTime;
    List<WhereToNextItem> nextPlaces;
    List<User> deliveryBoys;

    User user;
    Job job;

    String upgradeURL;

    public String getServerTime() {
        return serverTime;
    }

    public String getUpgradeURL() {
        return upgradeURL;
    }

    public User getUser() {
        return user;
    }

    public Job getJob() {
        return job;
    }

    public List<WhereToNextItem> getWhereToNextPlaces() {
        return nextPlaces;
    }

    public List<User> getDeliveryBoys() {
        return deliveryBoys;
    }
}
