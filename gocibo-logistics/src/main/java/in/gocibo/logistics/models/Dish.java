package in.gocibo.logistics.models;

import java.util.ArrayList;
import java.util.List;

public class Dish {

    public String _id;//dist id
    public DishDetails2 d;
    public String c;
    public Integer pr;
    public Integer opr;
    public Integer pro;
    public String p;
    public String t;
    public Double cnum;
    public Integer __v;
    public String cd;
    public String ud;
    public String st;
    public Boolean apr;
    public List<String> o = new ArrayList<>();
    public List<String> i = new ArrayList<>();

    public DishDetails2 getDishDetails() {
        return d;
    }
}
