package in.gocibo.logistics.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class ChefDetails {

    private String _id;
    private String r;
    private String f;//first name
    private String l;//lastName
    private String m;// mobile number
    private Integer __v;
    private String ci;// city
    private String e;
    private String am;// alternative number
    private String d;
    private String t;
    private String p;
    private String b;
    private String g;
    private String loc;//position
    private String n;
    private String a1;//address line 1
    private String a;//address line 1
    private String a2;// address line 2
    private String ar;//area
    private String lmk;//landmark
    private String cd;
    private String ud;
    private List<String> cc = new ArrayList<>();
    private Boolean vb;
    private Boolean st;

    public String getAddressLine1() {
        return (getA().length() > 0 ? getA() + " \n" : "") + (a1 == null ? "" : a1);
    }

    public String get_id() {
        return _id;
    }

    public String getAddressLine2() {
        return a2 == null ? "" : a2;
    }

    public String getA() {
        return a == null ? "" : a;
    }

    public String getFirstName() {
        return f == null ? "" : f;
    }

    public String getLastName() {
        return l == null ? "" : l;
    }

    public String getMobileNumber() {
        return m;
    }

    public String getCity() {
        return ci;
    }

    public String getAlternativeMobileNumber() {
        return am;
    }

    public String getLocation() {
        return loc;
    }

    public LatLng getLatLng() {
        String locationString = loc;
        if (locationString != null) {
            String latString = locationString.substring(0, locationString.indexOf(","));
            String lngString = locationString.substring(locationString.indexOf(",") + 1, locationString.length());
            Float lat = Float.parseFloat(latString);
            Float lng = Float.parseFloat(lngString);
            return new LatLng(lat, lng);
        }
        return null;
    }

    public String getLandMark() {
        return lmk;
    }

    public String getArea() {
        return ar;
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public String getFullAddress() {
        return a1 + " " + a + " " + ar + " " + lmk;
    }
}
