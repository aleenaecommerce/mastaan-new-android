package in.gocibo.logistics.models;

/**
 * Created by Naresh-Crypsis on 24-11-2015.
 */
public class TrackDetails {
    String point;
    String point_location;
    String time;
    Float distance_to_next_point;

    TrackDetails() {
    }

    public TrackDetails(String point, String point_location, String time, Float distance_to_next_point) {
        this.point = point;
        this.point_location = point_location;
        this.time = time;
        this.distance_to_next_point = distance_to_next_point;
    }

    public String getPoint() {
        return point;
    }

    public Float getDistance_to_next_point() {
        return distance_to_next_point;
    }

    public String getFormattedDistance() {
        StringBuilder distance = new StringBuilder(String.valueOf(distance_to_next_point));
        while (distance.length() < 4) {
            distance.append("0");
        }

        return new String(distance);
    }

    public String getPoint_location() {
        return point_location;
    }

    public String getTime() {
        return time;
    }
}
