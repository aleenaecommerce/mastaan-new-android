package in.gocibo.logistics.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 23/3/16.
 */
public class UploadDetails {

    String _id;
    public double pr;           // dishPrice
    public double opr;          // dishProfitPrice
    public double cnum;            // dish Current Availability
    public String p;            // dishImageURL
    public String t;           // dishImageThubnailURL

    DishDetails d;
    AvailDetails a;

    List<String> dab;

    class AvailDetails{
        public String slot;         // Available time
        public String slote;        //  Item End Time if Any
        public double num;         // dishTotal Availability
    }

    public String getID(){  return _id;  }

    public String getName(){
        return d.getName();
    }
    public String getDescription(){     return  d.getDescription();    }

    public double getPrice(){   return  opr;      }

    public double getProfitPrice(){   return  opr;      }

    public double getQuantity(){ return a.num; }

    public String getType(){
        return d.getType();
    }

    public String getQuantityType(){    return d.getQuantityType();}

    public String getAvailabilityStartTime(){
        return a.slot;
    }

    public String getAvailabilityEndTime() {
        if ( a.slote != null ){
            return a.slote;
        }
        return "";
        //return "2016-01-19T07:29:50.083Z";//a.slote;
    }

    public List<String> getLinkedDabbas() {
        if ( dab == null ){ dab = new ArrayList<>();    }
        return dab;
    }

    public void setLinkedDabbas(List<String> dabbas) {
        this.dab = dabbas;
    }

    public String getChefID(){  return  d.getChefID(); }
    public String getChefFullName(){    return d.getChefFullName() ;   }
    public String getChefFirstName(){    return d.getChefFirstName();    }
    public String getChefMobile(){  return d.c.getMobile();}
    public LatLng getChefLatLng(){  return d.c.getLatLng();}
    public String getChefAddress(){ return d.c.getAddress();}

    public Chef getChefDetails(){ return d.c;}

}
