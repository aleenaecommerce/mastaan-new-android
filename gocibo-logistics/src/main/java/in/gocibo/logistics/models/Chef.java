package in.gocibo.logistics.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Naresh-Crypsis on 05-01-2016.
 */
public class Chef extends CustomPlace {
    private String _id;
    private String f;
    private String m;
    private String t;
    private String g;
    private Boolean st;


    private String loc;

    private String a1;
    private String a2;
    private String lmk;
    private String ar;

    String pos;
    String da;
    String p;
    String lm;
    String s1;
    String s2;
    String l;
    String s;
    String pin;


    public String getLoc() {
        return loc;
    }

    @Override
    public String getName() {
        return getF() + " " + getL();
    }

    public String getF() {
        return f == null ? "" : f;
    }

    public String getL() {
        return l == null ? "" : l;
    }

    public String getId() {
        return _id;
    }

    public String getMobile() {
        return m;
    }

    public LatLng getLatLng(){
        return LatLngMethods.getLatLngFromString(loc);
    }

    public String getAddress(){
        if ( lmk != null ){
            return CommonMethods.getStringFromStringArray(new String[]{a1,a2,"near "+lmk,ar,s,pin});
        }
        return CommonMethods.getStringFromStringArray(new String[]{a1,a2,ar,s,pin});
    }

    @Override
    public String toString() {
        return "chef_" + _id;
    }

    public String getPremise() {
        return p;
    }

    public String getLandMark() {
        return lm;
    }

    public String getDeliveryAddress() {
        return CommonMethods.getStringFromStringArray(new String[]{p, s2, s1, l, s, pin});
    }

    public String getSublocalityLevel1() {
        return a2;
    }

    public String getSublocalityLevel2() {
        return a1;
    }

    public String getArea() {
        return ar;
    }

    public String getLocality() {
        return l;
    }

    public String getState() {
        return s;
    }

    public String getPincode() {
        return pin;
    }

    public String getAddressLine2() {
        return s1;
    }
}
