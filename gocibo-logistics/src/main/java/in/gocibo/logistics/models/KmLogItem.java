package in.gocibo.logistics.models;

/**
 * Created by Naresh-Crypsis on 11-12-2015.
 */
public class KmLogItem {
    private String _id;
    private String d;// delivery boy id
    private String sloc; //start location
    private String eloc; //end location
    private String tt; //trip type
    private String o; //order id
    private String cd; //trip date
    private Integer dis; //distance travelled

    public String get_id() {
        return _id;
    }

    public String getDeliverBoyId() {
        return d;
    }

    public String getStartLocation() {
        return sloc;
    }

    public String getEndLocation() {
        return eloc;
    }

    public String getTripType() {
        String tripType = "Pickup";
        if (tt.equalsIgnoreCase("p"))
            return "Pickup";
        if (tt.equalsIgnoreCase("d"))
            return "Delivery";
        if (tt.equalsIgnoreCase("r"))
            return "Retrieval";
        if (tt.equalsIgnoreCase("o"))
            return "Office";
        if (tt.equalsIgnoreCase("hu"))
            return "Hub";
        if (tt.equalsIgnoreCase("ho"))
            return "Home";
        return tt;
    }

    public String getOrderId() {
        return o;
    }

    public String getTripDate() {
        return cd;
    }

    public Integer getDistanceTravelledInMeters() {
        return dis;
    }

    public String getFormattedTrackDistance() {
        StringBuilder distance = new StringBuilder(String.valueOf(dis / 1000.0));
        while (distance.length() < 4) {
            distance.append("0");
        }
        return new String(distance);
    }
}
