package in.gocibo.logistics.models;

import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class OrderItemDetails {
    String _id;// item id
    Dish dish;
    Double num;
    Integer status;
    String dby;
    List<String> d = new ArrayList<>();//dabba list
    String pdb = null;
    String ddb;
    int my_local_status = 0;
    float distance;
    double ta;
    DeliveryDetails o;
    private boolean finalDelivery = false;

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getId() {
        return _id;
    }

    public Dish getDish() {
        return dish;
    }

    public List<String> getDabbas() {
        return d;
    }

    public void setDabbas(List<String> d) {
        this.d = d;
    }

    public String getFormattedDabbas() {
        StringBuilder builder = new StringBuilder();
        List<String> dabbas = getDabbas();
        if (dabbas.size() > 0) {
            for (int i = 0; i < dabbas.size() - 1 && i < dabbas.size(); i++) {
                builder.append(dabbas.get(i)).append(", ");
            }
            builder.append(dabbas.get(dabbas.size() - 1));
        }
        return new String(builder);
    }

    public String getDeliverBy() {
        return dby;
    }

    public LatLng getLatLng() {
        return LatLngMethods.getLatLngFromString(getChefDetails().getLocation());
    }

    public String getDdb() {
        return ddb;
    }

    public int getQuantity() {
        return (int) Math.ceil(num);
    }

    public String getPickedUpByBb() {
        return pdb;
    }

    public String getDeliveringBy() {
        return ddb;
    }

    public void setMyLocalStatus(int status_id) {
        my_local_status = status_id;
    }

    public int getMy_local_status() {
        return my_local_status;
    }

    public Integer getStatus() {
        return status;
    }

    public String getStatusString() {
        String status = "";
        switch ( getStatus() ) {
            case 0:
                status = "ORDERED";
                break;
            case 1:
                status = "PROCESSING";
                break;
            case 2:
                status = "PICKUP PENDING";
                break;
            case 3:
                status = "DELIVERING";
                break;
            case 4:
                status = "DELIVERED";
                break;
            case 5:
                status = "REJECTED";
                break;
            case 6:
                status = "FAILED";
                break;
            case 7:
                status = "ACKNOWLEDGED";
                break;
        }
        return status;
    }

    public void setStatus(Integer status) {
        this.my_local_status = status;
    }

    public DeliveryDetails getCustomerDetails() {
        return o;
    }

    public ChefDetails getChefDetails() {
        return dish.getDishDetails().getChefDetails();
    }

    public String getChefName() {
        return getChefDetails().getFirstName() + " " + getChefDetails().getLastName();
    }

    public String getOrderAmountAndPaymentType() {
        return "Rs." + getCustomerDetails().getBillAmount() + " (" + getCustomerDetails().getPaymentMode() + ")";
    }

    public boolean isFinalDelivery() {
        return finalDelivery;
    }

    public DeliveryDetails getDeliveryDetails() {
        return o;
    }

    public String getItemLocation() {
        if (o.i.size() <= 0)
            return null;
        String il = "";
        int size = o.i.size();
        int location = 0;
        for (int j = 0; j < size; j++)
            if (o.i.get(j).equals(_id)) {
                location = j + 1;
            }
        return location + "/" + size;
    }

    public long getOrderItemsCount(){
        if ( o != null && o.i != null ){
            return o.i.size();
        }
        return 0;
    }

    public double getAmountOfItem() {
        if ( dish != null ) {
            return (num * dish.opr);
        }
        return ta;
    }

    public void setAmountOfItem(double ta) {
        this.ta = ta;
    }

    public int getPaymentType(){
        return o.getPaymentType();
    }
    public String getPaymentTypeString() {
        return o.getPaymentTypeString();
    }

    public double getAmountOfOrder() {
        return o.getAmountOfOrder();
    }

    public void setAmountOfOrder(double oa) {
        o.setAmountOfOrder(oa);
    }



}
