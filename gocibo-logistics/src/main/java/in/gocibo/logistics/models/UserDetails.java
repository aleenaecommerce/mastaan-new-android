package in.gocibo.logistics.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Naresh-Gocibo on 08-10-2015.
 */
public class UserDetails {
    private String mobile;
    private String user_name;
    private String join_date;
    private String address;
    private String number_of_pickups;
    private String number_of_deliveries;
    private String number_of_retrievals;
    private String employee_id;
    private String email;
    private String user_phone;
    private String alternative_phone;
    private String licence_number;
    private String blood_group;
    private String vehicle_reg_num;
    private String ownership;
    private String insurance_provider;
    private String insurance_number;
    private String image_url;
    private String esi;
    private LatLng location;

    public LatLng getLocation() {
        return location;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserName() {
        return user_name;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getJoinDate() {
        return join_date;
    }

    public void setJoinDate(String join_date) {
        this.join_date = join_date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumberOfPickups() {
        return number_of_pickups;
    }

    public void setNumberOfPickups(String number_of_pickups) {
        this.number_of_pickups = number_of_pickups;
    }

    public String getNumberOfDeliveries() {
        return number_of_deliveries;
    }

    public void setNumberOfDeliveries(String number_of_deliveries) {
        this.number_of_deliveries = number_of_deliveries;
    }

    public String getNumberOfRetrievals() {
        return number_of_retrievals;
    }

    public void setNumberOfRetrievals(String number_of_retrievals) {
        this.number_of_retrievals = number_of_retrievals;
    }

    public String getEmployeeId() {
        return employee_id;
    }

    public void setEmployeeId(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserPhone() {
        return user_phone;
    }

    public void setUserPhone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getAlternativePhone() {
        return alternative_phone;
    }

    public void setAlternative_phone(String alternative_phone) {
        this.alternative_phone = alternative_phone;
    }

    public String getLicence_number() {
        return licence_number;
    }

    public void setLicenceNumber(String licence_number) {
        this.licence_number = licence_number;
    }

    public String getBloodGroup() {
        return blood_group;
    }

    public void setBloodGroup(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getVehicleRegNum() {
        return vehicle_reg_num;
    }

    public void setVehicleRegNum(String vehicle_reg_num) {
        this.vehicle_reg_num = vehicle_reg_num;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getInsurance_provider() {
        return insurance_provider;
    }

    public void setInsurance_provider(String insurance_provider) {
        this.insurance_provider = insurance_provider;
    }

    public String getInsurance_number() {
        return insurance_number;
    }

    public void setInsurance_number(String insurance_number) {
        this.insurance_number = insurance_number;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getEsi() {
        return esi;
    }

    public void setEsi(String esi) {
        this.esi = esi;
    }
}
