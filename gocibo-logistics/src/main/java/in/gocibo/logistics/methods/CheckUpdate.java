package in.gocibo.logistics.methods;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.ClipboardManager;
import android.text.Html;
import android.widget.Toast;

import com.aleena.common.interfaces.ChoiceSelectionCallback;

import org.json.JSONObject;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.localdata.LocalStorageData;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 15/05/16.
 */

public  class CheckUpdate {

    GoCiboToolBarActivity activity;

    public CheckUpdate(Context context){
        this.activity = (GoCiboToolBarActivity) context;
    }

    public boolean check(RetrofitError error){

        if ( error != null ) {
            try {
                String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                JSONObject jsonResponse = new JSONObject(response_json_string);
                boolean isUpdateAvailable = jsonResponse.getBoolean("upgrade");
                //final String updateURL = jsonResponse.getString("url");

//                if (isUpdateAvailable) {
//
//                    final String upgradeURL = new LocalStorageData(activity).getUpgradeURL();
//
//                    ClipboardManager clipboardManager = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
//                    clipboardManager.setText(upgradeURL);
//                    Toast.makeText(activity, "Download URL ( " + upgradeURL + " ) is copied", Toast.LENGTH_SHORT).show();

//                    activity.showChoiceSelectionDialog(false, "Update Available!", Html.fromHtml("This version is no longer compatible. Please update to new version to continue.<br><br>URL = <b>" + upgradeURL + "</b>"), "EXIT", "UPDATE", new ChoiceSelectionCallback() {
//                        @Override
//                        public void onSelect(int choiceNo, String choiceName) {
//                            if (choiceName.equalsIgnoreCase("UPDATE")) {
//                                Intent browserIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(upgradeURL));
//                                activity.startActivity(browserIntent);
//                                activity.finishAffinity();
//                                System.exit(0);
//                            } else {
//                                activity.finishAffinity();
//                                System.exit(0);
//                            }
//                        }
//                    });

                    return true;
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
