package in.gocibo.logistics.methods;

import android.app.Activity;

import in.gocibo.logistics.activities.MyPickupsAndDeliveriesActivity2;
import in.gocibo.logistics.activities.MyRetrievalsActivity;
import in.gocibo.logistics.activities.PendingRetrievalsActivity;
import in.gocibo.logistics.activities.TodayOrdersActivity;

/**
 * Created by venkatesh on 27/4/16.
 */
public class ActivityMethods {

    //-----------

    public static final boolean isMyPickupsAndDeliveriesActivity(Activity activity){
        try{
            MyPickupsAndDeliveriesActivity2 myPickupsAndDeliveriesActivity = (MyPickupsAndDeliveriesActivity2) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isTodaysOrdersActivity(Activity activity){
        try{
            TodayOrdersActivity todayOrdersActivity = (TodayOrdersActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isMyRetrievalsActivity(Activity activity){
        try{
            MyRetrievalsActivity myRetrievalsActivity = (MyRetrievalsActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isPendingRetrievalsActivity(Activity activity){
        try{
            PendingRetrievalsActivity pendingRetrievalsActivity = (PendingRetrievalsActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

}
