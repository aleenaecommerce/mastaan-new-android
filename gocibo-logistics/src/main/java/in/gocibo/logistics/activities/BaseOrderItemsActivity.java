package in.gocibo.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;

import androidx.appcompat.widget.SearchView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.adapters.OrderItemsAdapter;
import in.gocibo.logistics.flows.DeliveryActionsFlow;
import in.gocibo.logistics.flows.PickupActionsFlow;
import in.gocibo.logistics.fragments.FilterFragment;
import in.gocibo.logistics.methods.ActivityMethods;
import in.gocibo.logistics.models.ChefDetails;
import in.gocibo.logistics.models.DeliveryDetails;
import in.gocibo.logistics.models.Job;
import in.gocibo.logistics.models.OrderItemDetails;

/**
 * Created by venkatesh on 29/4/16.
 */

public class BaseOrderItemsActivity extends GoCiboToolBarActivity implements View.OnClickListener{

    MenuItem searchViewMenuITem;
    MenuItem filterMenuITem;
    SearchView searchView;

    public DrawerLayout drawerLayout;
    FilterFragment filterFragment;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    vRecyclerView orderItemsHolder;
    OrderItemsAdapter orderItemsAdapter;

    List<OrderItemDetails> originalList = new ArrayList<>();
    String searchQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void initializeUI(){
        setContentView(R.layout.activity_base_order_items);
        hasLoadingView();
        hasSwipeRefresh();

        filterFragment = new FilterFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterFragment.Callback() {
            @Override
            public void onItemSelected(int position, String item_name) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (item_name.equalsIgnoreCase("All")) {
                    onClearSearch();
                } else if (item_name.equalsIgnoreCase("total")) {
                    displayTotalInfo();
                } else {
                    onPerformSearch("(" + item_name + ")");
                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        //------

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        orderItemsHolder = (vRecyclerView) findViewById(R.id.orderItemsHolder);
        setupHolder();
    }

    @Override
    public void onClick(View view) {
        if ( view == clearFilter ){
            collapseSearchView();
            onClearSearch();
        }
    }

    public void setupHolder() {

        orderItemsHolder.setHasFixedSize(true);
        orderItemsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        orderItemsHolder.setItemAnimator(new DefaultItemAnimator());

        orderItemsAdapter = new OrderItemsAdapter(context, new ArrayList<OrderItemDetails>(), getDeliveryBoys(), new OrderItemsAdapter.Callback() {

            @Override
            public void onShowChefDirections(int position) {
                OrderItemDetails orderItemDetailsItemDetails = orderItemsAdapter.getItem(position);
                showDirections(orderItemDetailsItemDetails.getChefDetails().getLatLng(), orderItemDetailsItemDetails.getChefDetails().getFullName() + " " + orderItemDetailsItemDetails.getChefDetails().getArea());
            }

            @Override
            public void onCallChef(int position) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + orderItemsAdapter.getItem(position).getChefDetails().getMobileNumber()));
                startActivity(intent);
            }

            @Override
            public void onShowCustomerDirections(int position) {
                OrderItemDetails orderItemDetailsItemDetails = orderItemsAdapter.getItem(position);
                showDirections(orderItemDetailsItemDetails.getCustomerDetails().getLatLng(), orderItemDetailsItemDetails.getCustomerDetails().getCustomerName() + " " + orderItemDetailsItemDetails.getCustomerDetails().getArea());
            }

            @Override
            public void onCallCustomer(int position) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + orderItemsAdapter.getItem(position).getCustomerDetails().getMobileNumber()));
                startActivity(intent);
            }

            public void showDirections(final LatLng desnitation, final String details){
                getCurrentLocation(new LocationCallback() {
                    @Override
                    public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                        if (status) {
                            Intent showDirectionsIntent = new Intent(context, DirectionsViewerDialogActivity.class);
                            //showDirectionsIntent.setAction(Constants.POPUP_ACTION);
                            showDirectionsIntent.putExtra("origin", latLng);
                            showDirectionsIntent.putExtra("destination", desnitation);
                            showDirectionsIntent.putExtra("location", details);
                            startActivity(showDirectionsIntent);
                        } else {
                            showToastMessage(message);
                        }
                    }
                });
            }


            //==========


            @Override
            public void onReadyToPickup(final int position) {
                //showToastMessage("onReadyToPickup");

                if (ActivityMethods.isMyPickupsAndDeliveriesActivity(activity) ){
                    new PickupActionsFlow(activity).claimPickup(orderItemsAdapter.getItem(position), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if ( status ){
                                Intent intent = new Intent(context, JobDetailsActivity.class);
                                String itemDetailsString = new Gson().toJson(new Job(orderItemsAdapter.getItem(position)));
                                intent.putExtra("job_details", itemDetailsString);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }
            }

            @Override
            public void onPickupTaken(int position) {
                //showToastMessage("onPickupTaken");
            }

            @Override
            public void onReadyToDeliver(final int position) {
                //showToastMessage("onReadyToDeliver");
            }

            @Override
            public void onDelivered(final int position) {
                //showToastMessage("onDelivered");

                if (ActivityMethods.isMyPickupsAndDeliveriesActivity(activity) ){
                    new DeliveryActionsFlow(activity).claimDelivery(orderItemsAdapter.getItem(position), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if (status) {
                                Intent intent = new Intent(context, JobDetailsActivity.class);
                                String itemDetailsString = new Gson().toJson(new Job(orderItemsAdapter.getItem(position)));
                                intent.putExtra("job_details", itemDetailsString);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }
            }

            @Override
            public void onShowOrderDetails(int position) {

            }
        });

        orderItemsHolder.setAdapter(orderItemsAdapter);

    }

    public void displayItems(List<OrderItemDetails> itemsList){
        if ( itemsList != null && itemsList.size() > 0 ){
            originalList = itemsList;
            orderItemsAdapter.setItems(itemsList);
            switchToContentPage();
            showMenuItems();
        }else{
            originalList = new ArrayList<>();
            showNoDataIndicator("No items");
        }
    }

    public OrderItemsAdapter getOrderItemsAdapter() {
        return orderItemsAdapter;
    }

    //----------------

    public void onPerformSearch(String search_query){
        this.searchQuery = search_query;

        if ( searchQuery.length() > 0 ){
            searchQuery = searchQuery.toLowerCase();
            showLoadingIndicator("Searching for (" + search_query + ")");
            getSearchList(searchQuery, new SearchResultsCallback() {
                @Override
                public void onComplete(List<OrderItemDetails> searchList) {
                    if (searchList.size() > 0) {
                        searchResultsCountIndicator.setVisibility(View.VISIBLE);
                        results_count.setText(searchList.size() + " result(s) found.\n(" + searchQuery + ")");
                        orderItemsAdapter.setItems(searchList);
                        switchToContentPage();
                    } else {
                        searchResultsCountIndicator.setVisibility(View.VISIBLE);
                        results_count.setText("No Results");
                        showNoDataIndicator("No Results found for (" + searchQuery + ")");
                        orderItemsAdapter.clearItems();
                    }
                }
            });
        }
        else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( originalList != null && originalList.size() > 0 ) {
            orderItemsAdapter.setItems(originalList);
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }
    }

    public interface SearchResultsCallback{
        void onComplete(List<OrderItemDetails> searchList);
    }

    private void getSearchList(final String searchQuery, final SearchResultsCallback callback){

        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> filteredList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {

                if ( originalList != null && originalList.size() > 0 ){
                    for (int i=0;i<originalList.size();i++){
                        boolean isMatched = false;
                        OrderItemDetails orderItemDetailsItemDetails = originalList.get(i);
                        if ( orderItemDetailsItemDetails != null && orderItemDetailsItemDetails.getDeliveryDetails() != null ){
                            try {
                                DeliveryDetails deliveryDetails = orderItemDetailsItemDetails.getDeliveryDetails();
                                ChefDetails chefDetails = orderItemDetailsItemDetails.getChefDetails();
                                if (searchQuery.equalsIgnoreCase("(" + orderItemDetailsItemDetails.getPaymentTypeString() + ")") || searchQuery.equalsIgnoreCase("(" + orderItemDetailsItemDetails.getStatusString() + ")") || /*searchQuery.equalsIgnoreCase("("+orderItemDetailsItemDetails.getPrevCompletedStatusString()+")") ||*/ (chefDetails != null && chefDetails.getFullName().toLowerCase().contains(searchQuery)) || (deliveryDetails.getCustomerName() != null && deliveryDetails.getCustomerName().toLowerCase().contains(searchQuery)) || (deliveryDetails.getPremise() != null && deliveryDetails.getPremise().toLowerCase().contains(searchQuery)) || (deliveryDetails.getSublocalityLevel2() != null && deliveryDetails.getSublocalityLevel2().toLowerCase().contains(searchQuery)) || (deliveryDetails.getSublocalityLevel1() != null && deliveryDetails.getSublocalityLevel1().toLowerCase().contains(searchQuery)) || (deliveryDetails.getLocality() != null && deliveryDetails.getLocality().toLowerCase().contains(searchQuery)) || (deliveryDetails.getLandMark() != null && deliveryDetails.getLandMark().toLowerCase().contains(searchQuery)) || (deliveryDetails.getState() != null && deliveryDetails.getState().toLowerCase().contains(searchQuery)) || (deliveryDetails.getPincode() != null && deliveryDetails.getPincode().contains(searchQuery))) {
                                    isMatched = true;
                                }
                            }catch (Exception e){}
                        }
                        if ( isMatched ){
                            filteredList.add(orderItemDetailsItemDetails);
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(filteredList);
                }
            }
        }.execute();
    }

    public  void displayTotalInfo(){
        double totalSuccessAmount = 0;
        double totalFailedAmount = 0;

        for (int j=0;j<originalList.size();j++) {
            // IF FAILED
            if (originalList.get(j).getStatusString().equalsIgnoreCase("FAILED") || originalList.get(j).getStatusString().equalsIgnoreCase("REJECTED")) {
                totalFailedAmount += originalList.get(j).getAmountOfItem();
            } else {
                totalSuccessAmount += originalList.get(j).getAmountOfItem();
            }
        }

        String totalDisplayString = "Success items : <b>"+ SpecialCharacters.RS+" "+ CommonMethods.getIndianFormatNumber(totalSuccessAmount)+"</b><br>";
        if ( totalFailedAmount > 0 ){
            totalDisplayString += "Failed items : <b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(totalFailedAmount)+"</b><br>";
        }

        showNotificationDialog("Calculated Total", Html.fromHtml(totalDisplayString));
    }

    //----------

    public void hideMenuItems(){
        hideSearchView();
        hideFilterMenuItem();
    }

    public void showMenuItems(){
        showSearchView();
        showFilterMenuItem();
    }


    public void collapseSearchView(){
        try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
    }

    public void showSearchView(){
        try{    searchViewMenuITem.setVisible(true);    }catch (Exception e){}
    }

    public void hideSearchView(){
        try{
            searchViewMenuITem.setVisible(false);
            collapseSearchView();
        }catch (Exception e){}
    }

    public void showFilterMenuItem(){
        try{    filterMenuITem.setVisible(true);    }catch (Exception e){}
    }

    public void hideFilterMenuItem(){
        try{    filterMenuITem.setVisible(false);    }catch (Exception e){}
    }

    public String getSearchViewText(){
        String searchViewText = "";
        try{ searchViewText = searchView.getQuery().toString();      }catch (Exception e){}
        return searchViewText;
    }

    //=================

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_base_order_items, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filterMenuITem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){
        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}