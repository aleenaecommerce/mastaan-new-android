package in.gocibo.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import in.gocibo.logistics.R;
import in.gocibo.logistics.backend.WhereToNextAPI;
import in.gocibo.logistics.methods.Broadcaster;

public class WhereToNextActivity extends GoCiboToolBarActivity {
    boolean needResume = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_where_to_next);
    }

    @Override
    protected void setupActionBar() {
        try {
            toolbar = (Toolbar) findViewById(com.aleena.common.R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
//            actionBar.setHomeButtonEnabled(true);
//            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
        }
    }

    public void resume(View view) {
        needResume = true;
        context = this;
        showLoadingDialog("Loading...");
        getBackendAPIs().getWhereToNextAPI().resume(new WhereToNextAPI.SimpleStatusCallback() {
            @Override
            public void onComplete(boolean status, int status_code) {
                closeLoadingDialog();
                if (status) {
                    goToHome();
                    finish();
                } else
                    showMessageLong("Unable to resume");
            }
        });
    }

    private void goToHome() {
        new Broadcaster(context).broadcastReload();
        if (getIntent().getBooleanExtra("fromBootStrap", false)) {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (needResume)
            super.onBackPressed();
    }
}
