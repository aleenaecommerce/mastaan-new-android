package in.gocibo.logistics.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.adapters.UploadsAdapter;
import in.gocibo.logistics.backend.DabbasAPI;
import in.gocibo.logistics.backend.FoodItemsAPI;
import in.gocibo.logistics.flows.DabbasSelectionFlow;
import in.gocibo.logistics.models.UploadDetails;


public class TodaysUploadsActivity extends GoCiboToolBarActivity {

    vRecyclerView uploadsHolder;
    UploadsAdapter uploadsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todays_uploads);
        hasLoadingView();
        hasSwipeRefresh();

        //---------------

        uploadsHolder = (vRecyclerView) findViewById(R.id.uploadsHolder);
        uploadsHolder.setHasFixedSize(true);
        uploadsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        uploadsHolder.setItemAnimator(new DefaultItemAnimator());

        uploadsAdapter = new UploadsAdapter(context, new ArrayList<UploadDetails>(), new UploadsAdapter.Callback() {
            @Override
            public void onItemClick(final int position) {
                //if ( uploadsAdapter.getItem(position).getLinkedDabbas().size() == 0 ){
                    //getDabbasInStockAndShow(position);
                new DabbasSelectionFlow(activity).setSelectedDabbas(uploadsAdapter.getItem(position).getLinkedDabbas()).start(new DabbasSelectionFlow.Callback() {
                    @Override
                    public void onComplete(List<String> selectedDabbas) {
                        linkDabbasToChefUpload(position, selectedDabbas);
                    }
                });
                //}
            }

            @Override
            public void onCallChef(int position) {
                callToPhoneNumber(uploadsAdapter.getItem(position).getChefMobile());
            }

            @Override
            public void onShowChefDirections(final int position) {
                checkLocationAccess(new CheckLocatoinAccessCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if (status) {
                            showLoadingDialog("Getting your location, wait...");
                            new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                                @Override
                                public void onComplete(Location location) {
                                    closeLoadingDialog();
                                    if (location != null) {
                                        UploadDetails uploadDetails = uploadsAdapter.getItem(position);
                                        LatLng chefLocation = uploadDetails.getChefLatLng();
                                        String chefName = uploadDetails.getChefFullName();
                                        String chefAddress = uploadDetails.getChefAddress();

                                        Intent showDirectionsIntent = new Intent(context, DirectionsViewerDialogActivity.class);
                                        showDirectionsIntent.setAction("in.gocibo.logistics.Popup");
                                        showDirectionsIntent.putExtra("origin", new LatLng(location.getLatitude(), location.getLongitude()));
                                        showDirectionsIntent.putExtra("destination", chefLocation);
                                        showDirectionsIntent.putExtra("location", chefName + " " + chefAddress);
                                        startActivity(showDirectionsIntent);
                                    } else {
                                        showToastMessage("* Something went wrong while getting your location, please try again!");
                                    }
                                }
                            });
                        } else {
                            showToastMessage("* Location access required to show directions/route");
                        }
                    }
                });
            }
        });
        uploadsHolder.setAdapter(uploadsAdapter);

        //--------------

        getTodaysUploads();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getTodaysUploads();
    }

    public void getTodaysUploads(){

        showLoadingIndicator("Loading Today's uploads, wait..");
        getBackendAPIs().getFoodItemsAPI().getTodaysFoodItems(new FoodItemsAPI.FoodItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, List<UploadDetails> uploadsList) {
                if (status) {
                    if (uploadsList.size() > 0) {
                        uploadsAdapter.setItems(uploadsList);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No Uploads.");
                    }
                } else {
                    showReloadIndicator(statusCode, "Unable to load Today's uploads, try again!");
                }
            }
        });

    }

    public void linkDabbasToChefUpload(final int position, final List<String> selectedDabbas){

        showLoadingDialog("Linking dabbas, wait...");
        getBackendAPIs().getDabbasAPI().linkDabbasToChefUpload(uploadsAdapter.getItem(position).getID(), selectedDabbas, new DabbasAPI.LinkDabbasCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<String> linkedDabbas) {
                closeLoadingDialog();
                if (status) {
                    showToastMessage("Successfully linked " + linkedDabbas.size() + " dabba(s).");
                    UploadDetails uploadDetails = uploadsAdapter.getItem(position);
                    uploadDetails.setLinkedDabbas(linkedDabbas);
                    uploadsAdapter.updateItem(position, uploadDetails);
                } else {
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while linking dabbas.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                linkDabbasToChefUpload(position, selectedDabbas);
                            }
                        }
                    });
                }
            }
        });

    }

    //===================

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_reload, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_reload:
                onReloadPressed();
                return true;
            default:
                onBackPressed();
                return true;
        }
    }

}
