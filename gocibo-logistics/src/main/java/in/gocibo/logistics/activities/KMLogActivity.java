package in.gocibo.logistics.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aleena.common.methods.LatLngMethods;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.gocibo.logistics.R;
import in.gocibo.logistics.adapters.CalendarAdapter;
import in.gocibo.logistics.adapters.TrackAdapter;
import in.gocibo.logistics.backend.AccountsAPI;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.models.KmLogItem;
import in.gocibo.logistics.models.Track;
import in.gocibo.logistics.models.TrackDetails;
import in.gocibo.logistics.models.UserDay;

public class KMLogActivity extends GoCiboToolBarActivity {

    private static final int MAX_DAYS_COUNT = 42;
    // default date format
    private static final String DATE_FORMAT = "MMM yyyy";
    // seasons' rainbow
    int[] rainbow = new int[]{
            R.color.summer,
            R.color.fall,
            R.color.winter,
            R.color.spring
    };
    // month-season association (northern hemisphere, sorry australia :)
    int[] monthSeason = new int[]{2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2};
    CalendarAdapter adapter;
    // date format
    private String dateFormat;
    // current displayed month
    private Calendar currentDate = Calendar.getInstance();
    // internal components
    private LinearLayout header;
    private ImageView btnPrev;
    private ImageView btnNext;
    private TextView txtDate;
    private GridView grid;
    ArrayList<UserDay> userDays;

    AlertDialog dialog;
    AlertDialog.Builder builder;

    AlertDialog inner_dialog;
    AlertDialog.Builder inner_builder;
    View dialog_km_log_track_list;
    ListView dialog_list_view;
    TextView dialog_total_distance;
    TextView dialog_title;

    View inner_dialog_km_log_track_details;
    TextView inner_dialog_track_distance;
    TextView inner_dialog_track_id;
    LinearLayout inner_dialog_track_details_holder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kmlog);
        hasLoadingView();
        switchToContentPage();

        initControl();
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        inner_builder = new AlertDialog.Builder(this);
        inner_builder.setCancelable(true);

        dialog_km_log_track_list = LayoutInflater.from(this).inflate(R.layout.dialog_kmlog_track_list, null);
        dialog_list_view = (ListView) dialog_km_log_track_list.findViewById(R.id.list_view);
        dialog_total_distance = (TextView) dialog_km_log_track_list.findViewById(R.id.total_distance);
        dialog_title = (TextView) dialog_km_log_track_list.findViewById(R.id.title);


        inner_dialog_km_log_track_details = LayoutInflater.from(this).inflate(R.layout.dialog_kmlog_track_details, null);
        inner_dialog_track_distance = (TextView) inner_dialog_km_log_track_details.findViewById(R.id.track_distance);
        inner_dialog_track_id = (TextView) inner_dialog_km_log_track_details.findViewById(R.id.track_id);
        inner_dialog_track_details_holder = (LinearLayout) inner_dialog_km_log_track_details.findViewById(R.id.track_details_holder);

    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_reload:
                updateCalendar();
                return true;
            default:
                onBackPressed();
                return true;
        }
    }

    private void initControl() {

        loadDateFormat();
        assignUiElements();
        assignClickHandlers();
        updateCalendar();
    }

    private void loadDateFormat() {
        dateFormat = DATE_FORMAT;
    }

    private void assignUiElements() {
        // layout is inflated, assign local variables to components
        header = (LinearLayout) findViewById(R.id.calendar_header);
        btnPrev = (ImageView) findViewById(R.id.calendar_prev_button);
        btnNext = (ImageView) findViewById(R.id.calendar_next_button);
        txtDate = (TextView) findViewById(R.id.calendar_date_display);
        grid = (GridView) findViewById(R.id.calendar_grid);
        btnNext.setVisibility(View.INVISIBLE);
    }

    private void assignClickHandlers() {
        // add one month and refresh UI
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate.add(Calendar.MONTH, 1);
                updateCalendar();
            }
        });

        // subtract one month and refresh UI
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate.add(Calendar.MONTH, -1);
                updateCalendar();
            }
        });
    }

    boolean requestSent = false;

    public void updateCalendar() {
        if (!requestSent) {
            requestSent = true;
            showLoadingDialog("Loading...");
            initOrUpdateTitle();

            getBackendAPIs().getAccountsAPI().getKmLogs(currentDate.get(Calendar.MONTH) + 1, currentDate.get(Calendar.YEAR), new AccountsAPI.KmLogsCallback() {
                @Override
                public void onComplete(boolean status, Map<Integer, List<KmLogItem>> kmLogMap, int status_code) {
                    requestSent = false;
                    closeLoadingDialog();
                    if (status)
                        updateCalendar(kmLogMap);
                    else if (status_code == Constants.code.response.NO_DATA)
                        updateCalendar(new HashMap<Integer, List<KmLogItem>>());
                }
            });
        }
    }

    public void updateCalendar(Map<Integer, List<KmLogItem>> kmLogMap) {
        Calendar toDay = Calendar.getInstance();
        Date toDayDate = new Date();
        toDay.setTime(toDayDate);
        if (currentDate.get(Calendar.YEAR) == toDay.get(Calendar.YEAR) && currentDate.get(Calendar.MONTH) == toDay.get(Calendar.MONTH))
            btnNext.setVisibility(View.INVISIBLE);
        else
            btnNext.setVisibility(View.VISIBLE);
        ArrayList<Date> cells = new ArrayList<>();
        Calendar calendar = (Calendar) currentDate.clone();

        int daysOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int currentVisibleMonth = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);
        int days_count = (daysOfMonth + monthBeginningCell) > 35 ? MAX_DAYS_COUNT : ((daysOfMonth + monthBeginningCell) > 28 ? 35 : 28);
        // fill cells
        while (cells.size() < days_count) {
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        userDays = new ArrayList<>();
        for (Date cell : cells) {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(cell);
            int year = calendar1.get(Calendar.YEAR);
            int month = calendar1.get(Calendar.MONTH) + 1;
            int day = calendar1.get(Calendar.DAY_OF_MONTH);
            Integer key = year * 10000 + month * 100 + day;
            List<KmLogItem> logItems = kmLogMap.get(key);
            if (logItems != null && logItems.size() > 0) {
                int distance = 0;
                for (KmLogItem logItem : logItems)
                    distance += logItem.getDistanceTravelledInMeters();
                double distance_in_km = distance / 1000.0;
                DecimalFormat df = new DecimalFormat("#.#");
                df.setRoundingMode(RoundingMode.CEILING);
                String rounded_distance_in_km = df.format(distance_in_km);
                UserDay userDay = new UserDay(cell, "" + rounded_distance_in_km, distance_in_km);
                userDay.setLogItems(logItems);
                userDays.add(userDay);
            } else {
                UserDay userDay = new UserDay(cell, "0", 0);
                userDays.add(userDay);
            }

            /*UserDay userDay = new UserDay(cell, "40");
            userDay.setStartMeterReading(1000L);
            userDay.setEndMeterReading(2000L);
            List<Stop> stops = new ArrayList<>();
            stops.add(new Stop(123445L, 123450L, "08:00 AM", "09:00 AM"));
            stops.add(new Stop(123450L, 123459L, "10:00 AM", "11:00 AM"));
            userDay.setStops(stops);
            userDays.add(userDay);*/
        }
        // update grid
        adapter = new CalendarAdapter(this, userDays, null, currentVisibleMonth);
        adapter.setOnClickListener(new CalendarAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                showTrackList(adapter.getItem(position));
            }
        });
        grid.setAdapter(adapter);
    }

    private void initOrUpdateTitle() {
        // update title
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        txtDate.setText(sdf.format(currentDate.getTime()));

        // set header color according to current season
        int month = currentDate.get(Calendar.MONTH);
        int season = monthSeason[month];
        int color = rainbow[season];

        header.setBackgroundColor(getResources().getColor(color));
    }

    private void showTrackList(UserDay item) {
//        List<Track> tracks = getDummyTrackList();
        List<KmLogItem> logItems = item.getLogItems();

        dialog_total_distance.setText(item.getDistanceInKm() + " km");
        Date date = item.getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String title = calendar.get(Calendar.DAY_OF_MONTH) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.YEAR);
        dialog_title.setText(title);
        final TrackAdapter track_adapter = new TrackAdapter(this, R.layout.view_km_track_item, logItems);
        dialog_list_view.setAdapter(track_adapter);
        dialog_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                KmLogItem logItem = track_adapter.getItem(position);
                openDirectionsPopUp(logItem);
//                showTrackDetails(logItem.getTrack_id(), track.getTrack_distance());
            }
        });
        ViewGroup viewGroup = (ViewGroup) dialog_km_log_track_list.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(dialog_km_log_track_list);
        }
        builder.setView(dialog_km_log_track_list);
        dialog = builder.create();
        dialog.show();
    }

    private void openDirectionsPopUp(KmLogItem logItem) {
        Intent showDirectionsIntent = new Intent(this, DirectionsViewerDialogActivity.class);
        showDirectionsIntent.setAction("in.gocibo.logistics.Popup");
        showDirectionsIntent.putExtra("origin", LatLngMethods.getLatLngFromString(logItem.getStartLocation()));
        showDirectionsIntent.putExtra("destination", LatLngMethods.getLatLngFromString(logItem.getEndLocation()));
        showDirectionsIntent.putExtra("location", "Destination"/*.getAddress().getLocality()*/);
        showDirectionsIntent.putExtra("needLegDetails", true);
        startActivity(showDirectionsIntent);
    }

    private List<Track> getDummyTrackList() {
        List<Track> tracks = new ArrayList<>();
        tracks.add(new Track(5.00f, "HYDDDDSAS1"));
        tracks.add(new Track(9.10f, "HYDDDDSAS2"));
        tracks.add(new Track(2.25f, "HYDDDDSAS4"));
        return tracks;
    }

    private void showTrackDetails(String track_id, Float track_distance) {
        List<TrackDetails> track_details_list = getDummyTrackDetails();
        inner_dialog_track_distance.setText(track_distance + " km");
        inner_dialog_track_id.setText(track_id);
        ViewGroup viewGroup = (ViewGroup) inner_dialog_km_log_track_details.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(inner_dialog_km_log_track_details);
        }
        inner_dialog_track_details_holder.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(this);
        for (TrackDetails trackDetails : track_details_list) {
            View holder = inflater.inflate(R.layout.view_track_details, null);
            TextView point = (TextView) holder.findViewById(R.id.point);
            TextView location_name = (TextView) holder.findViewById(R.id.location_name);
            TextView time = (TextView) holder.findViewById(R.id.time);
            TextView distance_to_next_point = (TextView) holder.findViewById(R.id.distance_to_next_point);
            if (trackDetails.getDistance_to_next_point() != null && trackDetails.getDistance_to_next_point().equals(0.0F)) {
                distance_to_next_point.setVisibility(View.GONE);
            } else
                distance_to_next_point.setText(trackDetails.getFormattedDistance() + " km");
            point.setText("" + trackDetails.getPoint());
            location_name.setText("" + trackDetails.getPoint_location());
            time.setText("" + trackDetails.getTime());
            inner_dialog_track_details_holder.addView(holder);
        }
        inner_builder.setView(inner_dialog_km_log_track_details);
        inner_dialog = inner_builder.create();
        inner_dialog.show();
    }

    private List<TrackDetails> getDummyTrackDetails() {
        List<TrackDetails> trackDetailsList = new ArrayList<>();
        trackDetailsList.add(new TrackDetails("Point A", "Lorven Arcade, Miyapur", "12:020 PM", 2.5F));
        trackDetailsList.add(new TrackDetails("Point B", "Hi-tech City, Madhapur", "12:50 PM", 2.5F));
        trackDetailsList.add(new TrackDetails("Point C", "Jubli Hills, Madhapur", "1:20 PM", 1F));
        trackDetailsList.add(new TrackDetails("Point DishDetails2", "Lorven Arcade, Miyapur", "3:20 PM", 0F));
        return trackDetailsList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_reload, menu);
        return true;
    }
}
