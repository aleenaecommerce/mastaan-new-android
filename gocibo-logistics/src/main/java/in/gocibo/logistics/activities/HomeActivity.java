package in.gocibo.logistics.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.aleena.common.location.GetCurrentLocation;

import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.backend.CommonAPI;
import in.gocibo.logistics.backend.WhereToNextAPI;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.fragments.SideBarFragment;
import in.gocibo.logistics.models.Chef;
import in.gocibo.logistics.models.CustomPlace;
import in.gocibo.logistics.models.Hub;

public class HomeActivity extends GoCiboNavigationDrawerActivity implements View.OnClickListener {

    SideBarFragment sideBarFragment;

    Button myPickupsAndDeliveries;
    Button myRetrievals;
    Button pendingRetrievals;
    Button todaysOrders;
    Button todaysUploads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_home);
        hasLoadingView();

        sideBarFragment = new SideBarFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();
        sideBarFragment.setSidebarItemSelectedListener(new SideBarFragment.Callback() {
            @Override
            public void onItemSelected(int position, String item_name) {
                closeDrawer();
                if (item_name.equalsIgnoreCase("go to"))
                    showWhereToNextSelectionFlow();
            }
        });
        registerReloadReceiver("HOME_RECEIVER");

        //-----------

        myPickupsAndDeliveries = (Button) findViewById(R.id.myPickupsAndDeliveries);
        myPickupsAndDeliveries.setOnClickListener(this);
        myRetrievals = (Button) findViewById(R.id.myRetrievals);
        myRetrievals.setOnClickListener(this);
        pendingRetrievals = (Button) findViewById(R.id.pendingRetrievals);
        pendingRetrievals.setOnClickListener(this);
        todaysOrders = (Button) findViewById(R.id.todaysOrders);
        todaysOrders.setOnClickListener(this);
        todaysUploads = (Button) findViewById(R.id.todaysUploads);
        todaysUploads.setOnClickListener(this);

        //---------

        if ( getIntent().getBooleanExtra(Constants.SHOW_WHERE_TO_NEXT_FLOW, false) ){
            showWhereToNextSelectionFlow();
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ( intent.getBooleanExtra(Constants.SHOW_WHERE_TO_NEXT_FLOW, false) ){
            showWhereToNextSelectionFlow();//showWhereToNextSelectionFlow();
        }
    }

    @Override
    public void onClick(View view) {

        if ( view == myPickupsAndDeliveries ){
            Intent myPickupsAndDeliveriesAct = new Intent(this, MyPickupsAndDeliveriesActivity2.class);
            startActivity(myPickupsAndDeliveriesAct);
        }
        else if ( view == myRetrievals ){
            Intent myRetrievalsAct = new Intent(this, MyRetrievalsActivity.class);
            startActivity(myRetrievalsAct);
        }
        else if ( view == pendingRetrievals ){
            Intent pendingRetrievalsAct = new Intent(this, PendingRetrievalsActivity.class);
            startActivity(pendingRetrievalsAct);
        }
        else if ( view == todaysOrders ){
            Intent todaysOrdersAct = new Intent(this, TodayOrdersActivity.class);
            startActivity(todaysOrdersAct);
        }
        else if ( view == todaysUploads ){
            Intent todaysUploadsAct = new Intent(this, TodaysUploadsActivity.class);
            startActivity(todaysUploadsAct);
        }

    }

    private void showWhereToNextSelectionFlow() {
        showWhereToNextDialog(new WhereToCallback() {
            @Override
            public void onSubmit(final String whereTo) {
                showLoadingDialog("Loading...");
                new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                    @Override
                    public void onComplete(final Location location) {
                        closeLoadingDialog();
                        if (location != null)
                            onLocation(location);
                        else onNullLocation();
                    }

                    private void onLocation(final Location location) {
                        if (!(whereTo.equalsIgnoreCase("chef_place") || whereTo.equalsIgnoreCase("to_hub")))
                            sendWhereToRequest(location, whereTo);
                        else if (whereTo.equalsIgnoreCase("chef_place")) {
                            showLoadingDialog("Getting chefs...");
                            getBackendAPIs().getCommonAPI().getChefs(location.getLatitude(), location.getLongitude(), new CommonAPI.ChefsCallback() {
                                @Override
                                public void onComplete(boolean status, List<Chef> chefs, int status_code) {
                                    closeLoadingDialog();
                                    if (status) {
                                        showPlaceChooserDialog(chefs, new PlaceChooserCallback() {
                                            @Override
                                            public void onSelect(CustomPlace place) {
                                                sendWhereToRequest(location, place.toString());
                                            }
                                        });
                                    } else if (status_code == Constants.code.response.NO_DATA)
                                        showErrorDialog("No chefs with location details", null);
                                    else
                                        showErrorDialog("Unable to get chefs details", null);
                                }
                            });
                        } else {
                            showLoadingDialog("Getting hubs...");
                            getBackendAPIs().getCommonAPI().getHubs(location.getLatitude(), location.getLongitude(), new CommonAPI.HubsCallback() {
                                @Override
                                public void onComplete(boolean status, List<Hub> hubs, int status_code) {
                                    closeLoadingDialog();
                                    if (status) {
                                        showPlaceChooserDialog(hubs, new PlaceChooserCallback() {
                                            @Override
                                            public void onSelect(CustomPlace place) {
                                                sendWhereToRequest(location, place.toString());
                                            }
                                        });
                                    } else if (status_code == Constants.code.response.NO_DATA)
                                        showErrorDialog("No hubs available currently", null);
                                    else
                                        showErrorDialog("Unable to get chefs details", null);
                                }
                            });
                        }
                    }

                    public void onNullLocation() {
                        checkLocationAccessAndProceed(new LocationEnablerCallback() {
                            @Override
                            public void onComplete(Location location) {
                                onLocation(location);
                            }
                        });
                    }
                });
            }
        });
    }

    private void sendWhereToRequest(Location location, String whereTo) {
        showLoadingDialog("Loading...");
        getBackendAPIs().getWhereToNextAPI().whereToNext(whereTo, location.getLatitude(), location.getLongitude(), localStorageData.getUser().get_id(), new WhereToNextAPI.SimpleStatusCallback() {
            @Override
            public void onComplete(boolean status, int status_code) {
                closeLoadingDialog();
                if (status) {
                    Intent intent = new Intent(context, WhereToNextActivity.class);
                    startActivity(intent);
                } else showErrorDialog("Unable to update your status in server", null);
            }
        });
    }

    //============

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerListener.onOptionsItemSelected(item)) {
            return true;
        }
        else if ( item.getItemId() == R.id.action_goto ) {
            showWhereToNextSelectionFlow();
        }
        return super.onOptionsItemSelected(item);
    }

}
