package in.gocibo.logistics.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.widgets.vFlowLayout;
import com.aleena.common.widgets.vGridView;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.adapters.RetrievalItemsListAdapter;
import in.gocibo.logistics.backend.RetrievalsAPI;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.models.RetrievalItemDetails;

public class PendingRetrievalsActivity extends GoCiboToolBarActivity {
    Location current_location;
    RetrievalItemsListAdapter.Callback callback;
    boolean isRequestSent = false;
    private vGridView listHolder;
    private RetrievalItemsListAdapter itemsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrievals);
        hasLoadingView();
        hasSwipeRefresh();

        context = this;
        initRetrievalsListHolder();
        currentLocationCallback = new GetCurrentLocation.Callback() {
            @Override
            public void onComplete(Location location) {
                if (location != null)
                    onLocation(location);
                else onNullLocation();
            }

            private void onLocation(Location location) {
                current_location = location;
                updateAdapterData(location, itemsListAdapter);
            }

            public void onNullLocation() {
                checkLocationAccessAndProceed(new LocationEnablerCallback() {
                    @Override
                    public void onComplete(Location location) {
                        onLocation(location);
                    }
                });
            }
        };
        registerReloadReceiver("HOME_RECEIVER");
        //-----------------------------------------
        getCurrentLocation = new GetCurrentLocation(this, currentLocationCallback);
    }

    private void initRetrievalsListHolder() {

        listHolder = (vGridView) findViewById(R.id.listHolder);
        callback = new RetrievalItemsListAdapter.Callback() {
            @Override
            public void onDirectionsClick(int position) {
                Intent showDirectionsIntent = new Intent(context, DirectionsViewerDialogActivity.class);
                showDirectionsIntent.setAction("in.gocibo.logistics.Popup");
                showDirectionsIntent.putExtra("origin", new LatLng(current_location.getLatitude(), current_location.getLongitude()));
                showDirectionsIntent.putExtra("destination", itemsListAdapter.getItem(position).getLatLng());
                showDirectionsIntent.putExtra("location", "Retrieve\n" + itemsListAdapter.getItem(position).getCustomerName()/* + "\n" + itemsListAdapter.getItem(position).getAddress()*/);
                startActivity(showDirectionsIntent);
            }

            @Override
            public void onPhoneNumberClicked(int position) {
                callToPhoneNumber(itemsListAdapter.getItem(position).getPhoneNumber());
            }

            @Override
            public void onAltPhoneNumberClicked(int position) {
                callToPhoneNumber(itemsListAdapter.getItem(position).getAltPhoneNumber());
            }

            @Override
            public void onReadyToRetrievalClicked(final int position) {
                showConfirmDialog("Confirm",
                        "Can you retrieve " + itemsListAdapter.getItem(position).getDabbass().size() + " dabbas" +
                                " from " + itemsListAdapter.getItem(position).getCustomerName() + " ?", new ConfirmDialogCallback() {
                            @Override
                            public void isConfirmed(boolean isConfirmed) {
                                if (isConfirmed)
                                    onReadyToRetrieve(position);
                            }
                        });
            }

            @Override
            public void onRetrievedClicked(final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                final View view_confirm_retrieval_taken = LayoutInflater.from(context).inflate(R.layout.dialog_retrieval_taken, null);
                view_confirm_retrieval_taken.findViewById(R.id.tag_dabbas).setVisibility(View.GONE);
                final ArrayList<String> selected_boxes = new ArrayList<>();
                vFlowLayout boxes = (vFlowLayout) view_confirm_retrieval_taken.findViewById(R.id.boxes);
                builder.setView(view_confirm_retrieval_taken);
                final AlertDialog dialog = builder.create();
                for (int i = 0; i < itemsListAdapter.getItem(position).getDabbass().size(); i++) {
                    final int i1 = i;
                    View boxesView = inflater.inflate(R.layout.view_check_item, null, false);
                    final CheckBox checkbox = (CheckBox) boxesView.findViewById(R.id.checkbox);
                    checkbox.setText(itemsListAdapter.getItem(position).getDabbass().get(i));

                    boxesView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (checkbox.isChecked() == false) {
                                checkbox.setChecked(true);
                            } else {
                                checkbox.setChecked(false);
                            }
                        }
                    });

                    checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                            if (isChecked) {
                                if (!selected_boxes.contains(itemsListAdapter.getItem(position).getDabbass().get(i1))) {
                                    selected_boxes.add(itemsListAdapter.getItem(position).getDabbass().get(i1));
                                }
                            } else {
                                selected_boxes.remove(itemsListAdapter.getItem(position).getDabbass().get(i1));
                            }
                        }
                    });
                    boxes.addView(boxesView);
                }

                view_confirm_retrieval_taken.findViewById(R.id.taken).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        /*if (selected_boxes.size() != itemsListAdapter.getItem(position).getQuantity()) {
                            TextView error_message = (TextView) view_confirm_retrieval_taken.findViewById(R.id.no_box_selected);
                            error_message.setText("Please select " + itemsListAdapter.getItem(position).getQuantity() + " dabbas");
                        } else {*/
                            markAsRetrieved(position);
//                            itemsListAdapter.setRetrieved(position, selected_boxes, null);

                            for (String d : selected_boxes) {
                                Log.d(Constants.LOG_TAG, "BOX SELECTED " + d);
                            }
                            if (dialog.isShowing())
                                dialog.dismiss();
                        //}
                    }
                });
                view_confirm_retrieval_taken.findViewById(R.id.unable_to_pick).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        itemsListAdapter.setUnableToRetrieve(position);
                        if (dialog.isShowing())
                            dialog.dismiss();
                    }
                });
                view_confirm_retrieval_taken.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (dialog.isShowing())
                            dialog.dismiss();
                    }
                });
                dialog.show();
            }
        };
        itemsListAdapter = new RetrievalItemsListAdapter(this, R.layout.view_retrieval_card, new ArrayList<RetrievalItemDetails>(), callback);
        listHolder.setAdapter(itemsListAdapter);
        getRetrievalsDetails();
        updateAdapterData(current_location, itemsListAdapter);
    }

    private void markAsRetrieved(final int position) {
        showLoadingDialog("Updating...");

        context = this;
        getCurrentLocation = new GetCurrentLocation(this, new GetCurrentLocation.Callback() {
            @Override
            public void onComplete(Location location) {
                if (location != null)
                    onLocation(location);
                else onNullLocation();
            }

            private void onLocation(Location location) {
                current_location = location;
                getBackendAPIs().getRetrievalsAPI().retrievalDone(itemsListAdapter.getItem(position).getDabbass(), current_location.getLatitude(), current_location.getLongitude(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int status_code, String message) {
                        closeLoadingDialog();
                        if (status) {
                            itemsListAdapter.setRetrieved(position, itemsListAdapter.getItem(position).getDabbass(), null);
                        }
                        /*showWhereToNextDialog(new WhereToCallback() {
                            @Override
                            public void onSubmit(String whereTo) {
                                // TODO: 21-11-2015 Handle where he is going next
                            }
                        });*/
                    }
                });
            }

            public void onNullLocation() {
                checkLocationAccessAndProceed(new LocationEnablerCallback() {
                    @Override
                    public void onComplete(Location location) {
                        onLocation(location);
                    }
                });
            }
        });

    }

    private void onReadyToRetrieve(final int position) {
        showLoadingDialog("Requesting...");
        LatLng bLoc = itemsListAdapter.getItem(position).getLatLng();
        String buyer_id = itemsListAdapter.getItem(position).getCustomer_id();
        String last_loc = localStorageData.getLastLocation();
        if (last_loc == null)
            last_loc = current_location.getLatitude() + "," + current_location.getLongitude();
        getBackendAPIs().getRetrievalsAPI().readyToRetrieval(itemsListAdapter.getItem(position).getDabbass(), current_location.getLatitude(), current_location.getLongitude(), bLoc.latitude, bLoc.longitude, buyer_id, last_loc, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int status_code, String message) {
                closeLoadingDialog();
                if (status) {

                    RetrievalItemDetails itemDetails = itemsListAdapter.getItem(position);
                    localStorageData.saveLastLocation(itemDetails.getLatLng());
                    itemsListAdapter.setReadyToRetrieve(position);
//                    openJobDetailsActivity(itemsListAdapter.getItem(position));
                    broadcaster.broadcastReload();
                } else if (status_code == Constants.code.response.UN_KNOWN) {
                    showMessageLong("Please check your internet connection");
                }
            }
        });

    }

    private void getRetrievalsDetails() {
        if (!isRequestSent) {
            showLoadingIndicator("Loading retrieval items, please wait..");
            isRequestSent = true;
            if (current_location == null) {
                getCurrentLocation = new GetCurrentLocation(this, new GetCurrentLocation.Callback() {
                    @Override
                    public void onComplete(Location location) {
                        if (location != null)
                            onLocation(location);
                        else onNullLocation();
                    }

                    private void onLocation(Location location) {
                        current_location = location;
                        getRetrievalsDetails(location);
                    }

                    public void onNullLocation() {
                        checkLocationAccessAndProceed(new LocationEnablerCallback() {
                            @Override
                            public void onComplete(Location location) {
                                onLocation(location);
                            }
                        });
                    }
                });
            } else
                getRetrievalsDetails(current_location);
        }
    }

    private void getRetrievalsDetails(Location current_location) {

        getBackendAPIs().getRetrievalsAPI().getRetrievalItems(current_location.getLatitude(), current_location.getLongitude(), localStorageData.getUser().get_id(), new RetrievalsAPI.GetRetrievalItemsCallback() {
            @Override
            public void onComplete(List<RetrievalItemDetails> retrievalItemsDetails, int status_code) {
                switchToContentPage();
                isRequestSent = false;
                if (retrievalItemsDetails != null) {
                    if (retrievalItemsDetails.size() > 0)
                        itemsListAdapter.addItems(retrievalItemsDetails);
                    else
                        showNoDataIndicator("No Retrievals to show\n please deliver at least one item and come here");
                } else
                    showNoDataIndicator("No Retrievals to show\n please deliver at least one item \nand come here");

            }
        });
    }

    private void updateAdapterData(Location location, RetrievalItemsListAdapter deliveryItemsListAdapter) {
        if (location != null && deliveryItemsListAdapter != null)
            deliveryItemsListAdapter.setCurrentLocation(new LatLng(current_location.getLatitude(), current_location.getLongitude()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_reload, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_reload:
                onReloadPressed();
                return true;
            default:
                onBackPressed();
                return true;
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        try {
            itemsListAdapter.clear();
        } catch (Exception e) {
        }
        getRetrievalsDetails();
    }
}
