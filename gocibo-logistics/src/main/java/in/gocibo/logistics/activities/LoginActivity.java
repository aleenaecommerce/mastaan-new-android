package in.gocibo.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;

import java.util.ArrayList;

import in.gocibo.logistics.R;
import in.gocibo.logistics.backend.AccountsAPI;
import in.gocibo.logistics.backend.CommonAPI;
import in.gocibo.logistics.models.Bootstrap;
import in.gocibo.logistics.models.Cookies;
import in.gocibo.logistics.models.User;

public class LoginActivity extends GoCiboToolBarActivity implements View.OnClickListener{
    vTextInputLayout phone;
    vTextInputLayout password;
    Button confirmLogin;
    ProgressBar loading_indicator;
    TextView login_status;

    Bootstrap bootstrap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        actionBar.setDisplayHomeAsUpEnabled(false);

        //----------

        phone = (vTextInputLayout) findViewById(R.id.phone);
        password = (vTextInputLayout) findViewById(R.id.password);
        confirmLogin = (Button) findViewById(R.id.confirmLogin);
        confirmLogin.setOnClickListener(this);
        loading_indicator = (ProgressBar) findViewById(R.id.loading_indicator);
        login_status = (TextView) findViewById(R.id.login_status);


    }

    @Override
    public void onClick(View view) {

        if ( view == confirmLogin){
            String ePhone = phone.getText();
            phone.checkError("* Enter Phone number");
            String ePassword = password.getText();
            password.checkError("* Enter Password");

            if ( ePhone.length() > 0 && ePassword.length() > 0 ){

                if (CommonMethods.isValidPhoneNumber(ePhone) ){

                    inputMethodManager.hideSoftInputFromWindow(phone.getWindowToken(), 0);    // Hides Key Board After Item Select..
                    inputMethodManager.hideSoftInputFromWindow(password.getWindowToken(), 0);    // Hides Key Board After Item Select..

                    login(ePhone, ePassword);
                }
                else{
                    phone.showError("* Enter valid Phone number");
                }
            }
        }
    }

    private void login(String phone, String password) {
        confirmLogin.setEnabled(false);
        loading_indicator.setVisibility(View.VISIBLE);
        login_status.setText("Logging in, wait...");

        getBackendAPIs().getAccountsAPI().login(phone, password, new AccountsAPI.LoginCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String token, User user, ArrayList<Cookies> cookies) {
                if (status) {
                    localStorageData.setSession(token, user);
                    getBootStrap();
                } else {
                    confirmLogin.setEnabled(true);
                    loading_indicator.setVisibility(View.GONE);

                    if (isInternetAvailable() == false) {
                        login_status.setText("No Internet connection.");
                    } else {
                        login_status.setText(CommonMethods.capitalizeFirstLetter(message));
                    }
                }
            }
        });
    }

    public void getBootStrap() {

        confirmLogin.setEnabled(false);
        loading_indicator.setVisibility(View.VISIBLE);
        login_status.setText("Loading, wait...");

        getBackendAPIs().getCommonAPI().getBootStrap(new CommonAPI.BootstrapCallback() {
            @Override
            public void onComplete(boolean status, Bootstrap boot_strap, int status_code) {
                if (status) {
                    bootstrap = boot_strap;
                    saveBootstrap(bootstrap);
                    localStorageData.storeWhereToNextPlaces(bootstrap.getWhereToNextPlaces());

                    checkLocationAccessAndProceedToHome();
                } else {
                    confirmLogin.setEnabled(true);
                    loading_indicator.setVisibility(View.GONE);
                    login_status.setText("Something went wrong, try again!");
                }
            }
        });
    }

    public void checkLocationAccessAndProceedToHome(){

        checkLocationAccess(new CheckLocatoinAccessCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status) {
                    if (bootstrap.getJob() != null && bootstrap.getJob().getLoc() != null ){// && bootstrap.getJob().getItem_id() != null) {
                        if ( bootstrap.getJob().getCu_or_ch_id() != null && bootstrap.getJob().getCu_or_ch_id().equalsIgnoreCase(localStorageData.getUser().get_id())) {
                            Intent intent = new Intent(context, WhereToNextActivity.class);
                            intent.putExtra("fromBootStrap", true);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(context, JobDetailsActivity.class);
                            intent.putExtra("fromBootStrap", true);
                            intent.putExtra("job_details", new Gson().toJson(bootstrap.getJob()));
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        Intent homeAct = new Intent(context, HomeActivity.class);
                        startActivity(homeAct);
                        finish();
                    }
                } else {
                    checkLocationAccess(this);
                }
            }
        });
    }
}
