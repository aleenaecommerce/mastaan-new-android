package in.gocibo.logistics.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DayAndTimePickerCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.google.gson.Gson;

import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.adapters.PlaceChooserAdapter;
import in.gocibo.logistics.backend.BackendAPIs;
import in.gocibo.logistics.backend.CommonAPI;
import in.gocibo.logistics.methods.Broadcaster;
import in.gocibo.logistics.localdata.LocalStorageData;
import in.gocibo.logistics.models.Bootstrap;
import in.gocibo.logistics.models.CustomPlace;
import in.gocibo.logistics.models.Job;
import in.gocibo.logistics.models.OrderItemDetails;
import in.gocibo.logistics.models.RetrievalItemDetails;
import in.gocibo.logistics.models.User;
import in.gocibo.logistics.models.WhereToNextItem;


public class GoCiboToolBarActivity extends vToolBarActivity {

    public Context context;
    public GoCiboToolBarActivity activity;

    public Typeface kaushanFont;

    public int INTERNET_ENABLER_ACTIVITY_CODE;
    public int LOCATION_ENABLER_ACTIVITY_CODE;
    public int LOCATION_SELECTOR_ACTIVITY_CODE;
    public int PLACE_SEARCH_ACTIVITY_CODE;
    public int HOME_ACTIVITY_CODE;
    public String HOME_RECEIVER_NAME;
    AlertDialog locationSettingsDialog;
    View locationSettingsDialogView;
    AlertDialog retryFetchLocationDialog;
    View retryFetchLocationDialogView;
    String GOCIBO_LOGISTICS_BASE_URL;
    GetCurrentLocation getCurrentLocation;
    GetCurrentLocation.Callback currentLocationCallback;

    AlertDialog dayAndTimePickerDialog;
    View dayAndTimePickerDialogView;

    AlertDialog whereToNextDialog;
    View whereToNextView;
    BroadcastReceiver activityReceiver;
    Broadcaster broadcaster;

    int nullLocationHitCount = 0;
    LocationEnablerCallback locationEnablerCallback;


    LocalStorageData localStorageData;
    BackendAPIs backendAPIs;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        context = this;
        activity = this;
        localStorageData = new LocalStorageData(context);
        GOCIBO_LOGISTICS_BASE_URL = getString(R.string.base_url);
        broadcaster = new Broadcaster(this);


        //-------

        kaushanFont = Typeface.createFromAsset(getAssets(), "fonts/KaushanScript-Regular.ttf");

        // ACITIVITY_CODES

        INTERNET_ENABLER_ACTIVITY_CODE = getResources().getInteger(R.integer.internet_enabler_activity_code);
        LOCATION_ENABLER_ACTIVITY_CODE = getResources().getInteger(R.integer.location_enabler_activity_code);
        LOCATION_SELECTOR_ACTIVITY_CODE = getResources().getInteger(R.integer.location_selector_activity_code);
        PLACE_SEARCH_ACTIVITY_CODE = getResources().getInteger(R.integer.place_search_activity_code);
        HOME_ACTIVITY_CODE = getResources().getInteger(R.integer.home_activity_code);

        // RECEIVER NAMES

        HOME_RECEIVER_NAME = getResources().getString(R.string.home_receiver_name);

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        context = this;
        localStorageData = new LocalStorageData(context);
        backendAPIs = new BackendAPIs(context);
        GOCIBO_LOGISTICS_BASE_URL = getString(R.string.base_url);
    }


    public LocalStorageData getLocalStorageData() {
        if ( localStorageData == null ){
            localStorageData = new LocalStorageData(context);
        }
        return localStorageData;
    }

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(context);
        }
        return backendAPIs;
    }

        // Location Settings Dialog..
    public void showLocationAccessDialog(final ChoiceSelectionCallback callback) {

        if (locationSettingsDialog == null) {
            locationSettingsDialog = new AlertDialog.Builder(this).create();
            locationSettingsDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_location_access, null);
            locationSettingsDialog.setView(locationSettingsDialogView);
            locationSettingsDialog.setCancelable(false);
        }
        locationSettingsDialog.show();
        locationSettingsDialogView.findViewById(R.id.settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //locationSettingsDialog.dismiss();
                callback.onSelect(0, "YES");
            }
        });
    }

    //==============  CUSTOM DIALOGS METHODS  ==============//

    public void closeLocationAccessDialog() {
        if (locationSettingsDialog != null) {
            locationSettingsDialog.dismiss();
        }
    }

    // Retry Fetch Location
    public void showRetryFetchLocationDialog(final ChoiceSelectionCallback callback) {

        if (retryFetchLocationDialog == null) {
            retryFetchLocationDialog = new AlertDialog.Builder(this).create();
            retryFetchLocationDialogView = LayoutInflater.from(this).inflate(com.aleena.common.R.layout.dialog_retry_fetch_location, null);
            retryFetchLocationDialog.setView(retryFetchLocationDialogView);
        }
        retryFetchLocationDialog.show();

        retryFetchLocationDialogView.findViewById(com.aleena.common.R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retryFetchLocationDialog.dismiss();
            }
        });
        retryFetchLocationDialogView.findViewById(com.aleena.common.R.id.retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retryFetchLocationDialog.dismiss();
                callback.onSelect(1, "YES");
            }
        });
    }

    public void closeRetryFetchLocationDialog() {
        if (retryFetchLocationDialog != null) {
            retryFetchLocationDialog.dismiss();
        }
    }

    public int getAppCurrentVersion() {
        int currentVersion = 0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            currentVersion = pInfo.versionCode;
        } catch (Exception e) {
        }
        return currentVersion;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            locationSettingsDialog.dismiss();
        } catch (Exception e) {
        }
        try {
            retryFetchLocationDialog.dismiss();
        } catch (Exception e) {
        }
    }

    public void showMessageLong(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void showConfirmDialog(@Nullable String title, String message, final ConfirmDialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View confirmView = LayoutInflater.from(context).inflate(R.layout.dialog_confirm, null);
        TextView title_view = (TextView) confirmView.findViewById(R.id.title);
        TextView message_view = (TextView) confirmView.findViewById(R.id.message);
        Button positiveButton = (Button) confirmView.findViewById(R.id.confirm);
        Button negativeButton = (Button) confirmView.findViewById(R.id.cancel);
        builder.setCancelable(false);
        if (title != null)
            title_view.setText(title);
        else
            title_view.setText("Confirm");
        message_view.setText(message);
        positiveButton.setText("Yes");
        builder.setView(confirmView);
        final AlertDialog dialog = builder.create();
        dialog.show();
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.isConfirmed(true);
            }
        });

        negativeButton.setText("Cancel");
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.isConfirmed(false);
            }
        });
    }

    public void showConfirmDialog(@Nullable String title, String message, String pbText, String nbText, final ConfirmDialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View confirmView = LayoutInflater.from(context).inflate(R.layout.dialog_confirm, null);
        TextView title_view = (TextView) confirmView.findViewById(R.id.title);
        TextView message_view = (TextView) confirmView.findViewById(R.id.message);
        Button positiveButton = (Button) confirmView.findViewById(R.id.confirm);
        Button negativeButton = (Button) confirmView.findViewById(R.id.cancel);
        builder.setCancelable(false);
        if (title != null)
            title_view.setText(title);
        else
            title_view.setText("Confirm");
        message_view.setText(message);
        positiveButton.setText(pbText);
        builder.setView(confirmView);
        final AlertDialog dialog = builder.create();
        dialog.show();
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.isConfirmed(true);
            }
        });
        ;
        if (nbText != null) {
            negativeButton.setText(nbText);
            negativeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    callback.isConfirmed(false);
                }
            });
        } else
            negativeButton.setVisibility(View.GONE);
    }

    public void showErrorDialog(String message, final ConfirmDialogCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View confirmView = LayoutInflater.from(context).inflate(R.layout.dialog_confirm, null);
        TextView title_view = (TextView) confirmView.findViewById(R.id.title);
        TextView message_view = (TextView) confirmView.findViewById(R.id.message);
        Button positiveButton = (Button) confirmView.findViewById(R.id.confirm);
        Button negativeButton = (Button) confirmView.findViewById(R.id.cancel);
        builder.setCancelable(false);
        title_view.setText("Error");
        message_view.setText(message);
        positiveButton.setText("OK");
        builder.setView(confirmView);
        final AlertDialog dialog = builder.create();
        dialog.show();
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (callback != null)
                    callback.isConfirmed(true);
            }
        });
        negativeButton.setVisibility(View.GONE);
    }

    protected void showPlaceChooserDialog(List<? extends CustomPlace> places, final PlaceChooserCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View convertView = LayoutInflater.from(context).inflate(R.layout.dialog_chef_chooser, null);
        ListView list_view = (ListView) convertView.findViewById(R.id.list_view);
        builder.setView(convertView);
        builder.setCancelable(true);
        final AlertDialog dialog = builder.create();
        dialog.show();
        list_view.setAdapter(new PlaceChooserAdapter(context, places, new PlaceChooserAdapter.ItemClickListener() {
            @Override
            public void onSelect(int position, CustomPlace place) {
                dialog.dismiss();
                callback.onSelect(place);
            }
        }));
    }

    public void getBootStrap(final CommonAPI.BootstrapCallback callback) {
        /*new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
            @Override
            public void onComplete(Location location) {

            }
        });*/
        getBackendAPIs().getCommonAPI().getBootStrap(/*location.getLatitude(), location.getLongitude(),*/ callback);
    }

    public void showDayAndTimePickerDialog(final String displayTitle, final String[] displayValues, final int startValue, final int minValue, final int maxValue, final DayAndTimePickerCallback callback) {
        final String fullDaySlots[] = {"10:00 AM to 11:00 AM", "11:00 AM to 12:00 PM", "12:00 PM to 01:00 PM", "01:00 PM to 02:00 PM", "02:00 PM to 03:00 PM", "03:00 PM to 04:00 PM",
                "04:00 PM to 05:00 PM", "05:00 PM to 06:00 PM", "06:00 PM to 07:00 PM", "07:00 PM to 08:00 PM", "08:00 PM to 09:00 PM", "09:00 PM to 10:00 PM"};
        dayAndTimePickerDialog = new AlertDialog.Builder(this).create();
        dayAndTimePickerDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_day_and_time_picker, null);
        dayAndTimePickerDialog.setView(dayAndTimePickerDialogView);
        dayAndTimePickerDialog.setCancelable(true);
        final TextView title = (TextView) dayAndTimePickerDialogView.findViewById(R.id.title);
        final NumberPicker numberPicker = (NumberPicker) dayAndTimePickerDialogView.findViewById(R.id.numberPicker);
        final RadioGroup dayPicker = (RadioGroup) dayAndTimePickerDialogView.findViewById(R.id.dayPicker);
//        ArrayList<String> days = new ArrayList<>();
//        final Calendar calendar = Calendar.getInstance();
//        boolean isNextDaytemp = true;
//        if (calendar.get(Calendar.HOUR_OF_DAY) <= 22) {
//            days.add("Today");
//            isNextDaytemp = false;
//        }
//        final boolean isNextDay = isNextDaytemp
        if (displayTitle != null && displayTitle.length() > 0) {
            title.setText(displayTitle);
        }

        if (displayValues != null && displayValues.length > 0) {
            dayPicker.check(R.id.toDay);
            numberPicker.setDisplayedValues(displayValues);
            numberPicker.setMinValue(minValue);
            numberPicker.setMaxValue(maxValue);
            numberPicker.setWrapSelectorWheel(false);
            numberPicker.setValue(startValue);
        } else {
            dayAndTimePickerDialogView.findViewById(R.id.toDay).setVisibility(View.GONE);
            dayPicker.check(R.id.tomorrow);
            numberPicker.setDisplayedValues(fullDaySlots);
            numberPicker.setMinValue(0);
            numberPicker.setMaxValue(fullDaySlots.length - 1);
            numberPicker.setValue(0);
        }

        final int[] selected_day = {0};
        dayPicker.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.toDay) {
                    selected_day[0] = 0;
                    numberPicker.setMinValue(0);
                    numberPicker.setMaxValue(maxValue);
                    numberPicker.setDisplayedValues(displayValues);
                    numberPicker.setValue(0);
                    numberPicker.setWrapSelectorWheel(false);
                } else {
                    selected_day[0] = 1;
                    numberPicker.setDisplayedValues(fullDaySlots);
                    numberPicker.setMinValue(0);
                    numberPicker.setMaxValue(fullDaySlots.length - 1);
                    numberPicker.setValue(0);
                    numberPicker.setWrapSelectorWheel(false);
                }
            }
        });

        dayAndTimePickerDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (isNextDay)
//                    callback.onSelect( + 1, numberPicker.getValue());
//                else
                callback.onSelect(selected_day[0], numberPicker.getValue());
                dayAndTimePickerDialog.dismiss();
            }
        });
        dayAndTimePickerDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dayAndTimePickerDialog.dismiss();
            }
        });

        dayAndTimePickerDialog.show();
    }

    protected void callToPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(intent);
    }

    public void showWhereToNextDialog(final WhereToCallback callback) {
        whereToNextDialog = new AlertDialog.Builder(this).create();
        whereToNextView = LayoutInflater.from(this).inflate(R.layout.dialog_where_to_next, null);
        whereToNextDialog.setView(whereToNextView);
        whereToNextDialog.setCancelable(false);
        final RadioGroup whereToNextPicker = (RadioGroup) whereToNextView.findViewById(R.id.whereToNextPicker);
        final String[] selected_location = {"no_selection"};
        Bootstrap bootstrap = getSavedBootstrap();
        if (bootstrap != null) {
            LayoutInflater inflater = LayoutInflater.from(this);
            List<WhereToNextItem> nextItems = bootstrap.getWhereToNextPlaces();
            nextItems.add(new WhereToNextItem("to_hub", "Hub"));
            nextItems.add(new WhereToNextItem("chef_place", "Chef House"));
            for (final WhereToNextItem nextItem : nextItems) {
                final RadioButton item = (RadioButton) inflater.inflate(R.layout.radio_button, null);
                whereToNextPicker.addView(item);
                item.setText(nextItem.getName());
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.setSelected(!item.isSelected());
                        selected_location[0] = nextItem.getId();
                    }
                });
            }
        }

        /*whereToNextPicker.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.office:
                        selected_location[0] = 1;
                        break;
                    case R.id.hub:
                        selected_location[0] = 2;
                        break;
                    case R.id.home:
                        selected_location[0] = 3;
                        break;
                    case R.id.stay:
                        selected_location[0] = 4;
                        break;

                }
            }

        });*/
//        whereToNextPicker.check(R.id.office);
        whereToNextView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whereToNextDialog.dismiss();
            }
        });
        whereToNextView.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selected_location[0].equalsIgnoreCase("no_selection")) {
                    callback.onSubmit(selected_location[0]);
                    whereToNextView.findViewById(R.id.validationStatus).setVisibility(View.INVISIBLE);
                    whereToNextDialog.dismiss();
                } else {
                    whereToNextView.findViewById(R.id.validationStatus).setVisibility(View.VISIBLE);
                }
            }
        });
        whereToNextDialog.show();
    }

    public void openJobDetailsActivity(OrderItemDetails orderItemDetails) {
        Intent intent = new Intent(context, JobDetailsActivity.class);
        String itemDetailsString = new Gson().toJson(new Job(orderItemDetails));
        intent.putExtra("job_details", itemDetailsString);
        startActivity(intent);
        finish();
    }

    public void openJobDetailsActivity(RetrievalItemDetails details) {
        Intent intent = new Intent(context, JobDetailsActivity.class);
        String itemDetailsString = new Gson().toJson(new Job(details));
        intent.putExtra("job_details", itemDetailsString);
        startActivity(intent);
//        finish();
    }

    public void saveBootstrap(Bootstrap bootstrap) {
        String bootstrap_string = new Gson().toJson(bootstrap);
        localStorageData.editor.putString("bootstrap", bootstrap_string);
        localStorageData.editor.commit();
    }

    public Bootstrap getSavedBootstrap() {
        String json = localStorageData.prefs.getString("bootstrap", null);
        if (json != null)
            return new Gson().fromJson(json, Bootstrap.class);
        else return null;
    }

    public List<User> getDeliveryBoys() {
        if (getSavedBootstrap() != null)
            return getSavedBootstrap().getDeliveryBoys();
        else return null;
    }

    protected void showRetrievalOptionsDialog(final RetrievalOptionCallback callback) {
        View view = LayoutInflater.from(this).inflate(R.layout.view_retrieval_options, null);
        view.findViewById(R.id.picked_up_empty_dabbas).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onSelect(0);
                closeCustomDialog();
            }
        });
        view.findViewById(R.id.schedule_retrieval).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onSelect(1);
                closeCustomDialog();
            }
        });
        showCustomDialog(view, false);
    }

    public void registerReloadReceiver(final String activityReceiverName) {
        IntentFilter intentFilter = new IntentFilter(activityReceiverName);
        activityReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    if (intent.getStringExtra("type").equalsIgnoreCase("reload"))
                        onReloadPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        this.registerReceiver(activityReceiver, intentFilter);  //registering receiver
    }

    public void checkLocationAccessAndProceed(final LocationEnablerCallback callback) {
        try {
            this.locationEnablerCallback = callback;
            localStorageData = new LocalStorageData(this);
            boolean isLocationAccessEnabled = false;
            LocationManager locationManager = (LocationManager) getSystemService(context.LOCATION_SERVICE);
            if (locationManager != null) {
                isLocationAccessEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                if (isLocationAccessEnabled == false) {
                    isLocationAccessEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                }
            }

            if (isLocationAccessEnabled == true) {
                new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                    @Override
                    public void onComplete(Location location) {
                        if (location == null)
                            onNullLocation();
                        else {
                            closeLocationAccessDialog();
                            locationEnablerCallback.onComplete(location);
                        }
                    }

                    public void onNullLocation() {
                        nullLocationHitCount++;
                        if (nullLocationHitCount <= 5)
                            checkLocationAccessAndProceed(callback);
                        else
                            showErrorDialog("Unable to get current location", null);
                    }
                });
            } else {
                showLocationAccessDialog(new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, 2);
                    }
                });
            }
        } catch (Exception e) {
            context = getApplicationContext();
        }
    }

    public void checkLocationAccessAndProceedNonCallback() {
        try {
            localStorageData = new LocalStorageData(this);
            boolean isLocationAccessEnabled = false;
            LocationManager locationManager = (LocationManager) getSystemService(context.LOCATION_SERVICE);
            if (locationManager != null) {
                isLocationAccessEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                if (isLocationAccessEnabled == false) {
                    isLocationAccessEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                }
            }

            if (isLocationAccessEnabled == true) {
                new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                    @Override
                    public void onComplete(Location location) {
                        if (location == null)
                            onNullLocation();
                        else {
                            closeLocationAccessDialog();
                            locationEnablerCallback.onComplete(location);
                        }
                    }

                    public void onNullLocation() {
                        nullLocationHitCount++;
                        if (nullLocationHitCount <= 5)
                            checkLocationAccessAndProceedNonCallback();
                        else
                            showErrorDialog("Unable to get current location", null);
                    }
                });
            } else {
                showLocationAccessDialog(new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, 2);
                    }
                });
            }
        } catch (Exception e) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == 2) {     // From Location Settings..
                if (locationEnablerCallback != null) {
                    closeLocationAccessDialog();
                    checkLocationAccessAndProceedNonCallback();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void showAssignToDialog(final AssignOptionCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View convertView = LayoutInflater.from(context).inflate(R.layout.dialog_assign_order, null);
        builder.setView(convertView);
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                callback.onSelectOption(10, null);
            }
        });
        convertView.findViewById(R.id.btnMoveToQueue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onSelectOption(0, "queue");
                dialog.dismiss();
            }
        });
        convertView.findViewById(R.id.btnAssignToMe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onSelectOption(1, "toMe");
                dialog.dismiss();
            }
        });
        convertView.findViewById(R.id.btnAssignToOther).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onSelectOption(2, "toOther");
                dialog.dismiss();
            }
        });
    }

    public void showAlertDialog(String title, String message) {

    }

    public interface AssignOptionCallback {
        void onSelectOption(int option, String option_name);
    }

    public interface LocationEnablerCallback {
        void onComplete(Location location);
    }

    public interface ConfirmDialogCallback {
        void isConfirmed(boolean isConfirmed);
    }

    protected interface PlaceChooserCallback {
        void onSelect(CustomPlace place);
    }


    //==============   RECEIVER METHODS    ========//

    public interface WhereToCallback {
        void onSubmit(String whereTo);
    }

    public interface RetrievalOptionCallback {
        void onSelect(int option);
    }
}
