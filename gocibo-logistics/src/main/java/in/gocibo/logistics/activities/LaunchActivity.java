package in.gocibo.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.google.gson.Gson;
import com.melnykov.fab.FloatingActionButton;

import in.gocibo.logistics.R;
import in.gocibo.logistics.backend.CommonAPI;
import in.gocibo.logistics.models.Bootstrap;

public class LaunchActivity extends GoCiboToolBarActivity implements View.OnClickListener{

    ProgressBar loading_indicator;
    FloatingActionButton reload_indicator;
    TextView loading_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        //--------

        loading_indicator = (ProgressBar) findViewById(R.id.loading_indicator);
        loading_message = (TextView) findViewById(R.id.loading_message);
        reload_indicator = (FloatingActionButton) findViewById(R.id.reload_indicator);
        reload_indicator.setOnClickListener(this);

        checkInternetConnection(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status) {
                    // IF LOGGED IN
                    if (localStorageData.prefs.getString("token", null) != null) {//localStorageData.getSessionFlag()) {
                        checkLocationAccessAndProceedToBootstrap();
                    }
                    // IF NOT LOGGED IN
                    else {
                        new CountDownTimer(800, 400) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                Intent intent = new Intent(context, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }.start();
                    }
                }
            }
        });
    }

    public void checkLocationAccessAndProceedToBootstrap(){

        checkLocationAccess(new CheckLocatoinAccessCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status) {
                    getBootStrap();
                } else {
                    checkLocationAccess(this);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {

        if ( view == reload_indicator ){
            checkLocationAccessAndProceedToBootstrap();
        }
    }

    private void getBootStrap() {

        reload_indicator.setVisibility(View.GONE);
        loading_indicator.setVisibility(View.VISIBLE);
        loading_message.setText("TALKING TO SERVER");

        getBackendAPIs().getCommonAPI().getBootStrap(new CommonAPI.BootstrapCallback() {
            @Override
            public void onComplete(boolean status, Bootstrap bootstrap, int status_code) {
                if (status) {
                    saveBootstrap(bootstrap);
                    localStorageData.setServerTime(DateMethods.getReadableDateFromUTC(bootstrap.getServerTime()));
                    localStorageData.setUpgradeURL(bootstrap.getUpgradeURL());
                    localStorageData.setUser(bootstrap.getUser());
                    localStorageData.storeWhereToNextPlaces(bootstrap.getWhereToNextPlaces());

                    // IF VALID SESSION
                    if (bootstrap.getUser() != null && bootstrap.getUser().get_id() != null && bootstrap.getUser().get_id().length() > 0) {
                        if (bootstrap.getJob() != null && bootstrap.getJob().getLoc() != null) {// && bootstrap.getJob().getItem_id() != null) {
                            if (bootstrap.getJob().getCu_or_ch_id() != null && bootstrap.getJob().getCu_or_ch_id().equalsIgnoreCase(localStorageData.getUser().get_id())) {
                                Intent intent = new Intent(context, WhereToNextActivity.class);
                                intent.putExtra("fromBootStrap", true);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(context, JobDetailsActivity.class);
                                intent.putExtra("fromBootStrap", true);
                                intent.putExtra("job_details", new Gson().toJson(bootstrap.getJob()));
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Intent homeAct = new Intent(context, HomeActivity.class);
                            startActivity(homeAct);
                            finish();
                        }
                    }
                    // IF INVALID SESSION
                    else {
                        localStorageData.clearSession();
                        Intent loginAct = new Intent(context, LoginActivity.class);
                        startActivity(loginAct);
                        finish();
                    }
                } else {
                    if (status_code == 403) {
                        localStorageData.clearSession();
                        Intent intent = new Intent(LaunchActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        loading_indicator.setVisibility(View.GONE);
                        reload_indicator.setVisibility(View.VISIBLE);
                        loading_message.setText("Error in connecting to server, try again!");
                    }
                }
            }
        });
    }
}
