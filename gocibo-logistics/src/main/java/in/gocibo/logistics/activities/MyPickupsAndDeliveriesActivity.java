package in.gocibo.logistics.activities;

import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.widgets.vGridView;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.adapters.PickOrDeliverItemsListAdapter;
import in.gocibo.logistics.backend.DeliveriesAPI;
import in.gocibo.logistics.backend.PickOrDeliverItemsAPI;
import in.gocibo.logistics.models.Job;
import in.gocibo.logistics.models.OrderItemDetails;

/**
 * Created by Naresh Katta on 25/10/2015.
 */

public class MyPickupsAndDeliveriesActivity extends BaseOrderItemsActivity implements View.OnClickListener{

    BaseOrderItemsActivity activity;

    vGridView listHolder;
    PickOrDeliverItemsListAdapter listAdapter;
    PickOrDeliverItemsListAdapter.Callback listClickCallback;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    List<OrderItemDetails> originalItemList;
    String searchQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickups_and_deliveries);
        hasLoadingView();
        hasSwipeRefresh();

        activity = this;

        //-------

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        listHolder = (vGridView) findViewById(R.id.listHolder);
        setupHolder();

        //-----------

        getLocationAndProceedToGetItems();

    }

    @Override
    public void onClick(View view) {
        if ( view == clearFilter ){
            activity.collapseSearchView();
            onClearSearch();
        }
    }

    public void setupHolder() {

        listClickCallback = new PickOrDeliverItemsListAdapter.Callback() {

            @Override
            public void onDirectionsClick(int position, String name, String area, LatLng location) {
                /*Intent showDirectionsIntent = new Intent(context, DirectionsViewerDialogActivity.class);
                showDirectionsIntent.setAction("in.gocibo.logistics.Popup");
                showDirectionsIntent.putExtra("origin", new LatLng(current_location.getLatitude(), current_location.getLongitude()));
                showDirectionsIntent.putExtra("destination", location);
                showDirectionsIntent.putExtra("location", name + " " + area);
                startActivity(showDirectionsIntent);*/
            }

            @Override
            public void onReadyToPickup(final int position) {
                showConfirmDialog("Confirm", "Can you pickup the " + listAdapter.getItem(position).getDish().getDishDetails().getName() +
                        " from " + listAdapter.getItem(position).getChefDetails().getFullName() + " ?", new ConfirmDialogCallback() {
                    @Override
                    public void isConfirmed(boolean isConfirmed) {
                        if (isConfirmed) {
                            pickupConfirmed(position);
                        }
                    }
                });
            }

            @Override
            public void onPickupTaken(final int position) {
                showConfirmDialog("Confirm", "Can you pickup the " + listAdapter.getItem(position).getDish().getDishDetails().getName() +
                        " from " + listAdapter.getItem(position).getChefDetails().getFullName() + " ?", new ConfirmDialogCallback() {
                    @Override
                    public void isConfirmed(boolean isConfirmed) {
                        if (isConfirmed) {
//                            pickupConfirmed(position);
                            confirmPickupTaken(position);
                        }
                    }
                });
            }

            @Override
            public void onReadyToDeliver(final int position) {
                showConfirmDialog("Confirm", "Can you deliver the " + listAdapter.getItem(position).getDish().getDishDetails().getName() +
                        " to " + listAdapter.getItem(position).getCustomerDetails().getCustomerName() + " ?", new ConfirmDialogCallback() {
                    @Override
                    public void isConfirmed(boolean confirmed) {
                        if (confirmed) {
                            readyToDeliverConfirmed(position);
                        }
                    }
                });
            }

            @Override
            public void onDelivered(final int position) {
                showConfirmDialog("Confirm", "Can you deliver the " + listAdapter.getItem(position).getDish().getDishDetails().getName() +
                        " to " + listAdapter.getItem(position).getCustomerDetails().getCustomerName() + " ?", new ConfirmDialogCallback() {
                    @Override
                    public void isConfirmed(boolean confirmed) {
                        if (confirmed) {
                            readyToDeliverConfirmed(position);
                        }
                    }
                });
            }

            @Override
            public void onPhoneNumberClicked(int position, String number) {
                callToPhoneNumber(number);
            }
        };

        listAdapter = new PickOrDeliverItemsListAdapter(this, R.layout.view_item_card_full, new ArrayList<OrderItemDetails>(), getDeliveryBoys(), listClickCallback);
        listHolder.setAdapter(listAdapter);
    }

    private void updateAdapterData(Location location, PickOrDeliverItemsListAdapter listAdapter) {
        if (location != null && listAdapter != null) {
            listAdapter.setCurrentLocation(new LatLng(location.getLatitude(), location.getLongitude()));
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getLocationAndProceedToGetItems();
    }

    public void getLocationAndProceedToGetItems(){

        showLoadingIndicator("Getting your location, wait...");
        getCurrentLocation(false, new LocationCallback() {
            @Override
            public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                if (status) {
                    getMyPickupsAndDeliveries(location);
                } else {
                    showReloadIndicator("Unable to get your location, try again!");
                }
            }
        });
    }

    private void getMyPickupsAndDeliveries(final Location location) {

        hideMenuItems();
        showLoadingIndicator("Loading your pickups and deliveries, wait...");
        getBackendAPIs().getPickOrDeliverItemsAPI().getMyPickOrDeliveryOrders(location, new PickOrDeliverItemsAPI.OrderCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> itemsList) {
                if (status) {
                    if (itemsList.size() > 0) {
                        listAdapter.clear();
                        listAdapter.addItems(itemsList);
                        updateAdapterData(location, listAdapter);
                        switchToContentPage();
                        showMenuItems();
                    } else {
                        showNoDataIndicator("No items to show");
                    }
                } else {
                    showReloadIndicator("Unable to load you pickups and deliveries, try again!");
                }
            }
        });
    }

    //-------------

    @Override
    public void onPerformSearch(String searchQuery) {
        super.onPerformSearch(searchQuery);

        if (searchQuery == null) {
            searchQuery = "";
        }
        if (originalItemList == null) {
            originalItemList = new ArrayList<>(listAdapter.getItemList());
        }

        List<OrderItemDetails> tempItems = new ArrayList<>();

        if (searchQuery.length() > 0){
            for (OrderItemDetails details : originalItemList) {
                if (details.getChefDetails().getFullName().toLowerCase().contains(searchQuery.toLowerCase()) || details.getCustomerDetails().getCustomerName().toLowerCase().contains(searchQuery.toLowerCase()) || details.getCustomerDetails().getDa().toLowerCase().contains(searchQuery.toLowerCase()) || details.getChefDetails().getFullAddress().toLowerCase().contains(searchQuery.toLowerCase())) {
                    tempItems.add(details);
                }
            }
        }

        if (tempItems.size() == 0)
            showMessageLong("No results found");
        else {
            listAdapter.clear();
            listAdapter.addItems(tempItems);
        }
    }

    @Override
    public void onClearSearch() {
        super.onClearSearch();

        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( originalItemList.size() > 0 ) {
            listAdapter.clear();
            listAdapter.addItems(originalItemList);
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }
    }

    //--------------

    private void getDeliveryDetails(final int position) {

        showLoadingDialog("Loading...");
        context = this;
        getBackendAPIs().getDeliveriesAPI().markAsPickedUp(listAdapter.getItem(position).getId(), new DeliveriesAPI.PickCallback() {
            @Override
            public void onComplete(boolean status, int status_code) {
                closeLoadingDialog();
                if (status) {
                    listAdapter.setPickupTaken(position/*, details*/);
                    broadcaster.broadcastReload();
                } else
                    showErrorDialog("Unable to mark as pickup, try again", null);
            }
        });
    }

    private void readyToDeliverConfirmed(final int position) {
        context = this;
        showLoadingDialog("Loading...");
        new GetCurrentLocation(this, new GetCurrentLocation.Callback() {
            @Override
            public void onComplete(Location location) {
                if (location != null)
                    onLocation(location);
                else
                    onNullLocation();
            }

            void onLocation(Location location) {
                getBackendAPIs().getDeliveriesAPI().createJob(location.getLatitude(), location.getLongitude(), new Job(listAdapter.getItem(position)), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int status_code, String message) {
                        if (status) {
                            openJobDetailsActivity(listAdapter.getItem(position));
                        } else showErrorDialog("Unable to update status.\nTry again later", null);
                    }
                });
            }

            void onNullLocation() {
                checkLocationAccessAndProceed(new LocationEnablerCallback() {
                    @Override
                    public void onComplete(Location location) {
                        onLocation(location);
                    }
                });
            }
        });

    }

    private void confirmPickupTaken(final int position) {
        context = this;
        showLoadingDialog("Loading...");
        new GetCurrentLocation(this, new GetCurrentLocation.Callback() {
            @Override
            public void onComplete(Location location) {
                if (location != null)
                    onLocation(location);
                else
                    onNullLocation();
            }

            void onLocation(Location location) {
                getBackendAPIs().getDeliveriesAPI().createJob(location.getLatitude(), location.getLongitude(), new Job(listAdapter.getItem(position)), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int status_code, String message) {
                        closeLoadingDialog();
                        if (status) {
                            openJobDetailsActivity(listAdapter.getItem(position));
                        } else showErrorDialog("Unable to update status.\nTry again later", null);
                    }
                });
            }

            void onNullLocation() {
                checkLocationAccessAndProceed(new LocationEnablerCallback() {
                    @Override
                    public void onComplete(Location location) {
                        onLocation(location);
                    }
                });
            }
        });
    }

    private void pickupConfirmed(final int position) {
        /*showLoadingDialog("Loading...");
        String last_loc = localStorageData.getLastLocation();
        if (last_loc == null)
            last_loc = current_location.getLatitude() + "," + current_location.getLongitude();
        getBackendAPIs().getDeliveriesAPI().readyToPickup(listAdapter.getItem(position).getId(), current_location.getLatitude(), current_location.getLongitude(), last_loc, new DeliveriesAPI.PickCallback() {
            @Override
            public void onComplete(boolean status, int status_code) {
                closeLoadingDialog();
                if (status) {
                    OrderItemDetails order = listAdapter.getItem(position);
                    localStorageData.saveLastLocation(order.getLatLng());
                    listAdapter.setReadyToPickup(position);
                    openJobDetailsActivity(order);
//                            broadcaster.broadcastReload();
                } else if (status_code == Constants.code.response.NOT_AVAILABLE) {
                    listAdapter.remove(position);
                    showMessageLong("Item not available");
                } else showErrorDialog("Unable to claim pickup, try again", null);
            }
        });*/
    }

}
