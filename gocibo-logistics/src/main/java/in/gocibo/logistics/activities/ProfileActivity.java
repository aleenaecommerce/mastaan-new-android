package in.gocibo.logistics.activities;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aleena.common.widgets.vCircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.gocibo.logistics.R;
import in.gocibo.logistics.adapters.DayTasksAdapter;
import in.gocibo.logistics.backend.AccountsAPI;
import in.gocibo.logistics.models.DayTask;
import in.gocibo.logistics.models.User;

public class ProfileActivity extends GoCiboToolBarActivity implements View.OnClickListener {
    TextView user_name, join_date, address;
    TextView number_of_pickups, number_of_deliveries, number_of_retrievals;
    TextView employee_id, email, user_phone, alternative_phone, licence_number, blood_group, esi;
    TextView vehicle_reg_num, /*ownership,*/
            insurance_provider, insurance_number;
    vCircularImageView image;
    ProgressBar image_loading;
    AlertDialog dialog;
    AlertDialog.Builder builder;
    View dialog_show_details;
    View vehicle_details;
    ListView taskList;
    TextView dialog_title;

    ArrayList<DayTask> pickups;
    ArrayList<DayTask> deliveries;
    ArrayList<DayTask> retrievals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        hasLoadingView();

        initUI();
        showLoadingIndicator("Loading...");
        getBackendAPIs().getAccountsAPI().getUserDetails(new AccountsAPI.UserDetailsCallback() {
            @Override
            public void onComplete(boolean status, User user, int status_code) {
                switchToContentPage();
                if (status) {
                    updateUI(user);
                }
            }
        });
    }

    private void updateUI(User user) {
        String name = user.getF();
        if (name != null) {
            if (user.getL() != null) {
                name = name + " " + user.getL();
            }
            user_name.setText(name);
        }
        join_date.setText("Joined on " + user.getFormattedJoinDate());
        address.setText(user.getA1() + "\n" + user.getA2() + ", " + user.getAr() + ",\n" + user.getCi() + ", " + user.getS() + ", " + user.getPin());
        employee_id.setText(user.getEid());
        email.setText(user.getE());
        user_phone.setText(user.getM());
        alternative_phone.setText(user.getEm());
        blood_group.setText(user.getBg());
        esi.setText(user.getEsi());
        licence_number.setText(user.getDl());
        if (user.getVd() != null) {
            if (user.getVd().getOActual().equals("so")) {
                vehicle_details.setVisibility(View.VISIBLE);

                vehicle_reg_num.setText(user.getVd().getRn());
//        ownership.setText(user.getVd().getO());
                insurance_provider.setText(user.getVd().getInsuranceProvider());
                insurance_number.setText(user.getVd().getInsuranceNumber());
            } else {
                vehicle_details.setVisibility(View.INVISIBLE);
            }
        } else {
            vehicle_details.setVisibility(View.INVISIBLE);
        }
        pickups = new ArrayList<>();
        deliveries = new ArrayList<>();
        retrievals = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DayTask task = new DayTask("7/11/2015", 10);
            deliveries.add(task);
            pickups.add(task);
            retrievals.add(task);
        }
        Picasso.get().load(user.getProfilePic()).placeholder(R.drawable.ic_account_circle_grey).error(R.drawable.ic_account_circle_grey).into(image);
    }

    private void initUI() {
        user_name = (TextView) findViewById(R.id.user_name);
        join_date = (TextView) findViewById(R.id.join_date);
        address = (TextView) findViewById(R.id.address);

        number_of_pickups = (TextView) findViewById(R.id.number_of_pickups);
        number_of_deliveries = (TextView) findViewById(R.id.number_of_deliveries);
        number_of_retrievals = (TextView) findViewById(R.id.number_of_retrievals);

        employee_id = (TextView) findViewById(R.id.employee_id);
        email = (TextView) findViewById(R.id.email);
        user_phone = (TextView) findViewById(R.id.user_phone);
        alternative_phone = (TextView) findViewById(R.id.alternative_phone);
        licence_number = (TextView) findViewById(R.id.licence_number);
        blood_group = (TextView) findViewById(R.id.blood_group);
        esi = (TextView) findViewById(R.id.esi);

        vehicle_details = findViewById(R.id.vehicle_details);
        vehicle_reg_num = (TextView) findViewById(R.id.vehicle_reg_num);
//        ownership = (TextView) findViewById(R.id.ownership);
        insurance_provider = (TextView) findViewById(R.id.insurance_provider);
        insurance_number = (TextView) findViewById(R.id.insurance_number);

        image = (vCircularImageView) findViewById(R.id.image);
        image_loading = (ProgressBar) findViewById(R.id.image_loading);
        image_loading.setVisibility(View.GONE);

        findViewById(R.id.pickups).setOnClickListener(this);
        findViewById(R.id.deliveries).setOnClickListener(this);
        findViewById(R.id.retrievals).setOnClickListener(this);
        builder = new AlertDialog.Builder(context);
        dialog_show_details = LayoutInflater.from(this).inflate(R.layout.dialog_tasks, null);
        taskList = (ListView) dialog_show_details.findViewById(R.id.list_view);
        dialog_title = (TextView) dialog_show_details.findViewById(R.id.title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                onBackPressed();
                return true;
        }
    }

    @Override
    public void onClick(View view) {
        ViewGroup viewGroup;
        switch (view.getId()) {
            case R.id.pickups:
                viewGroup = (ViewGroup) dialog_show_details.getParent();
                if (viewGroup != null) viewGroup.removeView(dialog_show_details);
                showPickups();
                break;
            case R.id.deliveries:
                viewGroup = (ViewGroup) dialog_show_details.getParent();
                if (viewGroup != null) viewGroup.removeView(dialog_show_details);
                showDeliveries();
                break;
            case R.id.retrievals:
                viewGroup = (ViewGroup) dialog_show_details.getParent();
                if (viewGroup != null) viewGroup.removeView(dialog_show_details);
                showRetrievals();
                break;
        }
    }

    private void showRetrievals() {
        dialog_title.setText("Retrievals");
        taskList.setAdapter(new DayTasksAdapter(this, R.layout.view_day_task, retrievals));
        builder.setView(dialog_show_details);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    private void showDeliveries() {
        dialog_title.setText("Deliveries");
        taskList.setAdapter(new DayTasksAdapter(this, R.layout.view_day_task, deliveries));
        builder.setView(dialog_show_details);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    private void showPickups() {
        dialog_title.setText("Pickups");
        taskList.setAdapter(new DayTasksAdapter(this, R.layout.view_day_task, pickups));
        builder.setView(dialog_show_details);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }
}
