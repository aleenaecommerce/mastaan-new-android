package in.gocibo.logistics.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DayAndTimePickerCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vFlowLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.backend.DabbasAPI;
import in.gocibo.logistics.backend.DeliveriesAPI;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.methods.Broadcaster;
import in.gocibo.logistics.models.Dabba;
import in.gocibo.logistics.models.Job;

public class JobDetailsActivity extends GoCiboToolBarActivity {
    TextView cardHeader;
    TextView area;
    TextView name;
    TextView location;
    TextView land_mark;
    TextView item_name;
    ImageView item_type;
    LinearLayout dish_holder;
    TextView number_of_portions;
    TextView dabbas;
    TextView total_amount_of_order;
    TextView time;
    FloatingActionButton phone_number;
    FloatingActionButton alt_phone_number;
    FloatingActionButton directions;
    View confirm;
    Job job;
    boolean fromBootStrap;

    TextView buyer_outstanding_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);
        initUI();
        String claim_details_string = getIntent().getStringExtra("job_details");
        if (claim_details_string != null) {
            job = new Gson().fromJson(claim_details_string, Job.class);
            updateUI(job);
        }
    }

    private void initUI() {
        cardHeader = (TextView) findViewById(R.id.cardHeader);
        area = (TextView) findViewById(R.id.area);
        name = (TextView) findViewById(R.id.name);
        location = (TextView) findViewById(R.id.location);
        land_mark = (TextView) findViewById(R.id.land_mark);
        item_name = (TextView) findViewById(R.id.item_name);
        item_type = (ImageView) findViewById(R.id.item_type);
        dish_holder = (LinearLayout) findViewById(R.id.dish_holder);
        number_of_portions = (TextView) findViewById(R.id.number_of_portions);
        dabbas = (TextView) findViewById(R.id.dabbas);
        total_amount_of_order = (TextView) findViewById(R.id.total_amount_of_order);
        phone_number = (FloatingActionButton) findViewById(R.id.phone_number);
        alt_phone_number = (FloatingActionButton) findViewById(R.id.phone_number_2);
        directions = (FloatingActionButton) findViewById(R.id.directions);
        confirm = findViewById(R.id.confirm);
        time = (TextView) findViewById(R.id.time);

        buyer_outstanding_amount = (TextView) findViewById(R.id.buyer_outstanding_amount);
    }

    public void setActionbarTitle(CharSequence title) {
        try {
            actionBar.setTitle(title);
        } catch (Exception e) {
        }
    }

    @Override
    protected void setupActionBar() {
        try {
            toolbar = (Toolbar) findViewById(com.aleena.common.R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
    }

    private void updateUI(final Job claim_details) {
        phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToPhoneNumber(claim_details.getPhone_number());
            }
        });
        directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directions.setEnabled(false);
                new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                    @Override
                    public void onComplete(Location location) {
                        if (location != null)
                            onLocation(location);
                        else onNullLocation();
                    }

                    private void onLocation(Location location) {
                        directions.setEnabled(true);
                        Intent showDirectionsIntent = new Intent(context, DirectionsViewerDialogActivity.class);
                        showDirectionsIntent.setAction("in.gocibo.logistics.Popup");
                        showDirectionsIntent.putExtra("origin", new LatLng(location.getLatitude(), location.getLongitude()));
                        showDirectionsIntent.putExtra("destination", claim_details.getLocation());
                        showDirectionsIntent.putExtra("location", claim_details.getChef_or_customer_name() + "\n" + claim_details.getArea());
                        startActivity(showDirectionsIntent);
                    }

                    public void onNullLocation() {
                        checkLocationAccessAndProceed(new LocationEnablerCallback() {
                            @Override
                            public void onComplete(Location location) {
                                onLocation(location);
                            }
                        });
                    }
                });

            }
        });
        item_name.setText(claim_details.getItem_name());
        cardHeader.setText(claim_details.getCardType());
        setActionbarTitle(claim_details.getCardType());
        area.setText(claim_details.getArea());
        land_mark.setText(claim_details.getLand_mark());
        if (claim_details.getAlternative_number() == null)
            alt_phone_number.setVisibility(View.GONE);
        alt_phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callToPhoneNumber(claim_details.getAlternative_number());
            }
        });
        dabbas.setText(claim_details.getFormatedDabbas());
        number_of_portions.setText(claim_details.getNumber_of_portions() + "");
        if (claim_details.getLocal_status() == 0)
            name.setText("Chef " + claim_details.getChef_or_customer_name());
        else
            name.setText("Customer " + claim_details.getChef_or_customer_name());
        location.setText(claim_details.getAddress_line_1() + "\n" + claim_details.getAddress_line_2());
        time.setText("Time : " + claim_details.getSlot());

        if ( job.getBuyerOutstanding() > 0 ){
            findViewById(R.id.buyerOutstandingDetails).setVisibility(View.VISIBLE);
            buyer_outstanding_amount.setText(SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(job.getBuyerOutstanding()));
        }

        switch (claim_details.getLocal_status()) {
            case 0:
                setActionbarTitle("Pickup");
                cardHeader.setText("Pickup");
                updateItemTypeHolder(claim_details);
                total_amount_of_order.setText(CommonMethods.getInDecimalFormat(claim_details.getAmount())+"");
                break;
            case 1:
                setActionbarTitle("Delivery");
                cardHeader.setText("Delivery");
                updateItemTypeHolder(claim_details);
                total_amount_of_order.setText(CommonMethods.getInDecimalFormat(claim_details.getAmount())+"");
                break;
            case 2:
                setActionbarTitle("Retrieval");
                cardHeader.setText("Retrieval");
                findViewById(R.id.amount_label).setVisibility(View.GONE);
                findViewById(R.id.total_amount_of_order).setVisibility(View.GONE);
                findViewById(R.id.dish_holder).setVisibility(View.GONE);
                break;
        }
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (claim_details.getLocal_status() == 2) {
                    openConfirmRetrievalTakenDialog(claim_details);
                }
                if (claim_details.getLocal_status() == 1) {
                    onCompleteDelivery(claim_details);
                }
                if (claim_details.getLocal_status() == 0) {
                    onPickupTaken(claim_details);
                }
            }
        });
    }

    private void updateItemTypeHolder(Job claim_details) {
        if (claim_details.getItem_type() != null)
            switch (claim_details.getItem_type()) {
                case 0:
                    item_type.setImageResource(R.drawable.ic_veg);
                    break;
                case 1:
                    item_type.setImageResource(R.drawable.ic_non_veg);
                    break;
                case 2:
                    item_type.setImageResource(R.drawable.ic_jain2);
                    break;
                default:
                    item_type.setImageResource(R.drawable.ic_veg);
                    break;
            }
        else
            item_type.setVisibility(View.GONE);
    }

    private void openConfirmRetrievalTakenDialog(final Job claim_details) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View view_confirm_retrieval_taken = LayoutInflater.from(context).inflate(R.layout.dialog_retrieval_taken, null);
        view_confirm_retrieval_taken.findViewById(R.id.tag_dabbas).setVisibility(View.GONE);
        final ArrayList<String> selected_boxes = new ArrayList<>();
        vFlowLayout boxes = (vFlowLayout) view_confirm_retrieval_taken.findViewById(R.id.boxes);
        builder.setView(view_confirm_retrieval_taken);
        final AlertDialog dialog = builder.create();
        for (int i = 0; i < claim_details.getDabbas().size(); i++) {
            final int i1 = i;
            View boxesView = inflater.inflate(R.layout.view_check_item, null, false);
            final CheckBox checkbox = (CheckBox) boxesView.findViewById(R.id.checkbox);
            checkbox.setText(claim_details.getDabbas().get(i));

            boxesView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkbox.isChecked() == false) {
                        checkbox.setChecked(true);
                    } else {
                        checkbox.setChecked(false);
                    }
                }
            });

            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        if (!selected_boxes.contains(claim_details.getDabbas().get(i1))) {
                            selected_boxes.add(claim_details.getDabbas().get(i1));
                        }
                    } else {
                        selected_boxes.remove(claim_details.getDabbas().get(i1));
                    }
                }
            });
            boxes.addView(boxesView);
        }

        view_confirm_retrieval_taken.findViewById(R.id.taken).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (selected_boxes.size() != job.getNumber_of_portions()) {
                    TextView error_message = (TextView) view_confirm_retrieval_taken.findViewById(R.id.no_box_selected);
                    error_message.setText("Please select " + job.getNumber_of_portions() + " dabbas");
                } else {*/
                markAsRetrieved(claim_details);
//                            itemsListAdapter.setRetrieved(position, selected_boxes, null);

                for (String d : selected_boxes) {
                    Log.d(Constants.LOG_TAG, "BOX SELECTED " + d);
                }
                if (dialog.isShowing())
                    dialog.dismiss();
                //}
            }
        });
        view_confirm_retrieval_taken.findViewById(R.id.unable_to_pick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                        itemsListAdapter.setUnableToRetrieve(position);
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
        view_confirm_retrieval_taken.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
        dialog.show();
    }

    //-----------------

    private void onCompleteDelivery(final Job job) {

        showChoiceSelectionDialog("Confirm!", Html.fromHtml("Did you deliver the dabbas?"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equals("YES")) {

                    getCurrentLocation(new LocationCallback() {
                        @Override
                        public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, final Location location, LatLng latLng) {

                            if (status) {
                                confirmDeliveryDabbas(job, location);     // MARKING AS DELVIERED

                                /*boolean isAnyAmountToBeCollected = false;

                                if ( job.getBuyerOutstanding() > 0 ){
                                    isAnyAmountToBeCollected = true;
                                }else{
                                    // FOR ONLINE PAYMENT
                                    if ( job.getPaymentTypeString().equalsIgnoreCase("OP") ){
                                        isAnyAmountToBeCollected = false;
                                    }
                                    // FOR COD
                                    else{
                                        isAnyAmountToBeCollected = true;
                                    }
                                }

                                if ( isAnyAmountToBeCollected ){
                                    View inputCashDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_input_cash, null);
                                    TextView choice_dialog_title = (TextView) inputCashDialogView.findViewById(R.id.choice_dialog_title);
                                    choice_dialog_title.setText("Amount collected?");
                                    final EditText input = (EditText) inputCashDialogView.findViewById(R.id.input);
                                    TextView dialog_message = (TextView) inputCashDialogView.findViewById(R.id.message);
                                    inputCashDialogView.findViewById(R.id.input_comments).setVisibility(View.GONE);

                                    dialog_message.setText(Html.fromHtml("Item cost: <b>"+ SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(job.getAmount())+"</b>"));
                                    if ( job.getBuyerOutstanding() > 0 ){
                                        dialog_message.append(Html.fromHtml("<br>Buyer outstanding: <b>"+SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(job.getBuyerOutstanding())+"</b>"));
                                    }

                                    inputCashDialogView.findViewById(R.id.choice_dialog_option1).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String amountCollected = input.getText().toString();
                                            if (amountCollected.length() > 0) {
                                                closeCustomDialog();
                                                confirmDeliveryDabbas(job, amountCollected, location);     // MARKING AS DELVIERED
                                            } else {
                                                showToastMessage("* Please enter amount collected.");
                                            }
                                        }
                                    });
                                    showCustomDialog(inputCashDialogView);
                                }
                                else{
                                    confirmDeliveryDabbas(job, "0", location);
                                }*/
                            } else {
                                showToastMessage(message);
                            }
                        }
                    });
                }
            }
        });
    }

    private void confirmDeliveryDabbas(final Job job/*, final String amountCollected*/, final Location location) {

        showLoadingDialog("Updating status, wait...");
        getBackendAPIs().getDeliveriesAPI().deliveryDone(job.getItem_id(), location, new DeliveriesAPI.MarkAsDeliveredCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, boolean collectAmount, double amountToBeCollected) {
                closeLoadingDialog();
                if (status) {
                    if (collectAmount == false) {
                        showRetrievalFlow();
                    } else {
                        startCollectAmountFlow(amountToBeCollected);
                    }
                } else {
                    showErrorDialog("Unable to update delivery status of item", null);
                }
            }
        });

    }

    public void showRetrievalFlow(){
        final Calendar calendar = Calendar.getInstance();
        final String[] slots = CommonMethods.getTimeSlotsFromNow(true, 4);
        showRetrievalOptionsDialog(new RetrievalOptionCallback() {
            @Override
            public void onSelect(int option) {
                if (option == 1) {
                    showDayAndTimePickerDialog("Select retrieval slot", slots, 0, 0, slots.length - 1, new DayAndTimePickerCallback() {
                        @Override
                        public void onSelect(int selectedDayPosition, int selectedSlotPosition) {
                            convertToRetrieval(job, selectedDayPosition, selectedSlotPosition, calendar/*, isConfirmed*/);
                        }
                    });
                } else {
                    markAsRetrieved(job);
                }
            }
        });
    }

    public void startCollectAmountFlow(double amountToBeCollected){

        View amountCollectedDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_amount_collected, null);
        final EditText amount_collected = (EditText) amountCollectedDialogView.findViewById(R.id.amount_collected);
        TextView dialog_message = (TextView) amountCollectedDialogView.findViewById(R.id.message);

        //dialog_message.setText(Html.fromHtml("OrderItemDetails cost: <b>"+SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(job.getOrderAmount())+"</b><br>Item cost: <b>"+SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(job.getAmountOfItem())+"</b>"));
        dialog_message.setText(Html.fromHtml("Amount to be collected: <b>" + SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(amountToBeCollected) + "</b>"));
        if ( job.getBuyerOutstanding() < 0 ){
            dialog_message.append(Html.fromHtml("<br>Outstanding: <b>"+SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(job.getBuyerOutstanding())+" (Return to customer)</b>"));
        }else if  ( job.getBuyerOutstanding() > 0 ){
            dialog_message.append(Html.fromHtml("<br>Outstanding: <b>"+SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(job.getBuyerOutstanding())+" (Collect from customer)</b>"));
        }

        amountCollectedDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String amountCollected = amount_collected.getText().toString();
                if (amountCollected.length() > 0) {
                    closeCustomDialog();
                    updateAmountCollected(amountCollected);     // UPDATE AMOUNT COLLECTED
                } else {
                    showToastMessage("* Please enter amount collected.");
                }
            }
        });
        showCustomDialog(amountCollectedDialogView, false);
    }

    public void updateAmountCollected(final String amountCollected){

        showLoadingDialog("Updating amount collected, wait...");
        getBackendAPIs().getDeliveriesAPI().updateOrderAmountCollected(job.getItem_id(), amountCollected, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    showRetrievalFlow();
                } else {
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating amount collected.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equals("YES")) {
                                updateAmountCollected(amountCollected);
                            }
                        }
                    });
                }
            }
        });

    }

    //----------------------

    private void markAsRetrieved(final Job retrievalItemDetails) {
        showLoadingDialog("Updating...");
        context = this;
        getCurrentLocation = new GetCurrentLocation(this, new GetCurrentLocation.Callback() {
            @Override
            public void onComplete(Location location) {
                if (location != null)
                    onLocation(location);
                else onNullLocation();
            }

            private void onLocation(Location location) {
                getBackendAPIs().getRetrievalsAPI().retrievalDone(retrievalItemDetails.getDabbas(), location.getLatitude(), location.getLongitude(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int status_code, String message) {
                        closeLoadingDialog();
                        if (status) {
                            localStorageData.editor.remove("wasInClaim");
                            localStorageData.editor.remove("retrieval_details");
                            localStorageData.editor.commit();
                            goToHome();
                            finish();
//                            itemsListAdapter.setRetrieved(position, itemsListAdapter.getItem(position).getDabbass(), null);
                        } else
                            showErrorDialog("Unable to update retrieval status of item", null);
                    }
                });
            }

            public void onNullLocation() {
                checkLocationAccessAndProceed(new LocationEnablerCallback() {
                    @Override
                    public void onComplete(Location location) {
                        onLocation(location);
                    }
                });
            }
        });
    }

    private void convertToRetrieval(final Job pickOrDeliveryItemDetails, int dayFromToday, final int slot, Calendar calendar) {
        showLoadingDialog("Loading...");
        String day = "today";
        if (dayFromToday != 0)
            day = "tomorrow";
        int hour = 0;
        if (dayFromToday == 0) {
            calendar.add(Calendar.MINUTE, 60 - calendar.get(Calendar.MINUTE));
            hour = calendar.get(Calendar.HOUR_OF_DAY) + slot + 1; //+1 for end hour
        } else {
            hour = slot + 10 + 1;//+1 for end hour
        }
        final int finalHour = hour;
        final String finalDay = day;
        getCurrentLocation = new GetCurrentLocation(this, new GetCurrentLocation.Callback() {
            @Override
            public void onComplete(Location location) {
                if (location != null)
                    onLocation(location);
                else
                    onNullLocation();
            }

            private void onLocation(Location location) {

                getBackendAPIs().getDabbasAPI().markDabbasForRetrieval(location.getLatitude(), location.getLongitude(), pickOrDeliveryItemDetails.getDabbas(), finalHour, finalDay, new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int status_code, String message) {
                        closeLoadingDialog();
                        if (status) {
                            localStorageData.editor.remove("wasInClaim");
                            localStorageData.editor.remove("pickup_or_delivery_details");
                            localStorageData.editor.commit();
                            finish();
                        } else
                            showErrorDialog("Unable to mark dabbas for retrieval", null);
                    }
                });
            }

            public void onNullLocation() {
                checkLocationAccessAndProceed(new LocationEnablerCallback() {
                    @Override
                    public void onComplete(Location location) {
                        onLocation(location);
                    }
                });
            }
        });
    }

    private void onPickupTaken(final Job itemDetails) {
        if (itemDetails.getDabbas() == null || itemDetails.getDabbas().size() == 0) {
            showLoadingDialog("Loading...");
            new GetCurrentLocation(this, new GetCurrentLocation.Callback() {
                @Override
                public void onComplete(Location location) {
                    if (location != null)
                        onLocation(location);
                    else onNullLocation();
                }

                private void onLocation(Location location) {
                    getBackendAPIs().getDabbasAPI().getDabbasForChefUpload(itemDetails.getChef_upload_id(), location.getLatitude(), location.getLongitude(), new DabbasAPI.DabbasCallback() {
                        @Override
                        public void onComplete(boolean status, List<Dabba> dabbas, int status_code) {
                            closeLoadingDialog();
                            if (status)
                                getPickedUpDabbas(dabbas, itemDetails, true);
                            else if (status_code == Constants.code.response.NO_DATA) {
                                confirmRefills(itemDetails);
                            } else showErrorDialog("Unable to get data", null);
                        }
                    });
                }

                public void onNullLocation() {
                    checkLocationAccessAndProceed(new LocationEnablerCallback() {
                        @Override
                        public void onComplete(Location location) {
                            onLocation(location);
                        }
                    });
                }
            });
        } else {
            List<String> dabba_ids = itemDetails.getDabbas();
            List<Dabba> dabbas = new ArrayList<>();
            for (String dabba : dabba_ids)
                dabbas.add(new Dabba(dabba));
            getPickedUpDabbas(dabbas, itemDetails, false);
        }
    }

    private void getPickedUpDabbas(final List<Dabba> dabbas, final Job itemDetails, final boolean newBind) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View viewConfirmPickupTaken = LayoutInflater.from(context).inflate(R.layout.dialog_pickup_taken, null);
        viewConfirmPickupTaken.findViewById(R.id.tag_dabbas).setVisibility(View.GONE);
        final ArrayList<Dabba> selected_boxes = new ArrayList<>();
        vFlowLayout boxes = (vFlowLayout) viewConfirmPickupTaken.findViewById(R.id.boxes);
        builder.setView(viewConfirmPickupTaken);
        final AlertDialog dialog = builder.create();
        for (int i = 0; i < dabbas.size(); i++) {
            final int i1 = i;
            View boxesView = inflater.inflate(R.layout.view_check_item, null, false);
            final CheckBox checkbox = (CheckBox) boxesView.findViewById(R.id.checkbox);
            checkbox.setText(dabbas.get(i).toString());

            boxesView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkbox.isChecked() == false)
                        checkbox.setChecked(true);
                    else
                        checkbox.setChecked(false);
                }
            });

            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        if (!selected_boxes.contains(dabbas.get(i1))) {
                            selected_boxes.add(dabbas.get(i1));
                        }
                    } else {
                        selected_boxes.remove(dabbas.get(i1));
                    }
                }
            });
            boxes.addView(boxesView);
        }

        viewConfirmPickupTaken.findViewById(R.id.taken).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validatePickupForm(viewConfirmPickupTaken, selected_boxes, itemDetails.getNumber_of_portions())) {
                    if (newBind) {
                        showLoadingDialog("Loading...");
                        new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                            @Override
                            public void onComplete(Location location) {
                                if (location != null)
                                    onLocation(location);
                                else onNullLocation();
                            }

                            private void onLocation(Location location) {
                                getBackendAPIs().getDabbasAPI().linkDabbasToOrder(itemDetails.getCu_or_ch_id(), itemDetails.getItem_id(), location.getLatitude(), location.getLatitude(), selected_boxes, new StatusCallback() {
                                    @Override
                                    public void onComplete(boolean status, int status_code, String message) {
                                        closeLoadingDialog();
                                        if (status) {
                                            ArrayList<String> dabbasIDList = new ArrayList<>();
                                            for (Dabba dabba : selected_boxes)
                                                dabbasIDList.add(dabba.get_id());
                                            itemDetails.setDabbas(dabbasIDList);
                                            confirmRefills(itemDetails);
                                        } else
                                            showErrorDialog("Unable to get dabbas for chef", null);
                                    }
                                });
                            }

                            public void onNullLocation() {
                                checkLocationAccessAndProceed(new LocationEnablerCallback() {
                                    @Override
                                    public void onComplete(Location location) {
                                        onLocation(location);
                                    }
                                });
                            }
                        });

                    } else
                        confirmRefills(itemDetails);
                    if (dialog.isShowing())
                        dialog.dismiss();
                }
            }
        });
        viewConfirmPickupTaken.findViewById(R.id.unable_to_pick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
        viewConfirmPickupTaken.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
        try {
            closeLoadingDialog();
        } catch (Exception e) {
        }
        dialog.show();
    }

    private void confirmRefills(final Job itemDetails) {
        context = this;
        getDeliveryDetails(itemDetails);
    }

    private void getDeliveryDetails(final Job itemDetails) {
        showLoadingDialog("Loading...");
        context = this;
        getBackendAPIs().getDeliveriesAPI().markAsPickedUp(itemDetails.getItem_id(), new DeliveriesAPI.PickCallback() {
            @Override
            public void onComplete(boolean status, int status_code) {
                closeLoadingDialog();
                if (status) {
                    localStorageData.editor.remove("wasInClaim");
                    localStorageData.editor.remove("pickup_or_delivery_details");
                    localStorageData.editor.commit();
                    goToHome();
                    finish();
                } else
                    showErrorDialog("Unable to update pick up status of item", null);
            }
        });
    }

    private void goToHome() {
        new Broadcaster(this).broadcastReload();
//        Intent intent = new Intent(context, HomeActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
        Intent homeAct = new Intent(context, HomeActivity.class);
        homeAct.putExtra(Constants.SHOW_WHERE_TO_NEXT_FLOW, true);
        homeAct.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        homeAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeAct);
    }

    private boolean validatePickupForm(View viewConfirmPickupTaken, ArrayList<Dabba> selected_boxes, int numberOfPortions) {
        boolean valid = true;
        /*if (selected_boxes.size() != numberOfPortions) {
            valid = false;
            TextView no_boxes = (TextView) viewConfirmPickupTaken.findViewById(R.id.no_box_selected);
            no_boxes.setText("Please select " + numberOfPortions + " dabba(s)");
            no_boxes.setVisibility(View.VISIBLE);
        }*/
        return valid;
    }
}
