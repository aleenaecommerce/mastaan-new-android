package in.gocibo.logistics.activities;

import android.location.Location;
import android.os.Bundle;

import com.aleena.common.interfaces.LocationCallback;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import in.gocibo.logistics.backend.PickOrDeliverItemsAPI;
import in.gocibo.logistics.models.OrderItemDetails;

/**
 * Created by Naresh Katta on 25/10/2015.
 */

public class MyPickupsAndDeliveriesActivity2 extends BaseOrderItemsActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeUI();

        //---------

        getLocationAndProceedToGetItems();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getLocationAndProceedToGetItems();
    }

    public void getLocationAndProceedToGetItems(){

        showLoadingIndicator("Getting your location, wait...");
        getCurrentLocation(false, new LocationCallback() {
            @Override
            public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                if (status) {
                    getMyPickupsAndDeliveries(location);
                } else {
                    showReloadIndicator("Unable to get your location, try again!");
                }
            }
        });
    }

    public void getMyPickupsAndDeliveries(final Location location) {

        hideMenuItems();
        showLoadingIndicator("Loading your pickups and deliveries, wait...");
        getBackendAPIs().getPickOrDeliverItemsAPI().getMyPickOrDeliveryOrders(location, new PickOrDeliverItemsAPI.OrderCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> itemsList) {
                if (status) {
                    displayItems(itemsList);

                } else {
                    showReloadIndicator("Unable to load you pickups and deliveries, try again!");
                }
            }
        });
    }


}
