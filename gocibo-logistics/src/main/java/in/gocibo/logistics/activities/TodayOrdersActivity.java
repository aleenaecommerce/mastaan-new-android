package in.gocibo.logistics.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.widgets.vGridView;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.adapters.PickOrDeliverItemsListAdapter;
import in.gocibo.logistics.backend.PickOrDeliverItemsAPI;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.models.OrderItemDetails;

/**
 * Created by Venkatesh Uppu on 12/05/2016.
 */
public class TodayOrdersActivity extends BaseOrderItemsActivity {
    PickOrDeliverItemsListAdapter listAdapter;
    vGridView listHolder;
    PickOrDeliverItemsListAdapter.Callback listClickCallback;
    Location current_location;
    boolean isRequestSent = false;


    List<OrderItemDetails> originalItemList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickups_and_deliveries);
        hasLoadingView();
        hasSwipeRefresh();
        currentLocationCallback = new GetCurrentLocation.Callback() {
            @Override
            public void onComplete(Location location) {
                if (location != null)
                    onLocation(location);
                else onNullLocation();
            }

            private void onLocation(Location location) {
                current_location = location;
                updateAdapterData(location, listAdapter);
            }

            public void onNullLocation() {
                checkLocationAccessAndProceed(new LocationEnablerCallback() {
                    @Override
                    public void onComplete(Location location) {
                        onLocation(location);
                    }
                });
            }
        };
        registerReloadReceiver("HOME_RECEIVER");
        setupAdapter();
        getCurrentLocation =  new GetCurrentLocation(this, currentLocationCallback);
    }


    private void updateAdapterData(Location location, PickOrDeliverItemsListAdapter listAdapter) {
        if (location != null && listAdapter != null) {
            listAdapter.setCurrentLocation(new LatLng(location.getLatitude(), location.getLongitude()));
        }
    }

    private void setupAdapter() {
        listHolder = (vGridView) findViewById(R.id.listHolder);
        listClickCallback = new PickOrDeliverItemsListAdapter.Callback() {

            @Override
            public void onDirectionsClick(int position, String name, String area, LatLng location) {
                Intent showDirectionsIntent = new Intent(context, DirectionsViewerDialogActivity.class);
                showDirectionsIntent.setAction("in.gocibo.logistics.Popup");
                showDirectionsIntent.putExtra("origin", new LatLng(current_location.getLatitude(), current_location.getLongitude()));
                showDirectionsIntent.putExtra("destination", location);
                showDirectionsIntent.putExtra("location", name + " " + area);
                startActivity(showDirectionsIntent);
            }

            @Override
            public void onReadyToPickup(final int position) {
            }

            @Override
            public void onPickupTaken(int position) {
            }

            @Override
            public void onReadyToDeliver(final int position) {
            }

            @Override
            public void onDelivered(final int position) {
            }

            @Override
            public void onPhoneNumberClicked(int position, String number) {
                callToPhoneNumber(number);
            }
        };
        listAdapter = new PickOrDeliverItemsListAdapter(this, R.layout.view_item_card_full, new ArrayList<OrderItemDetails>(), getDeliveryBoys(), true, listClickCallback);
        listHolder.setAdapter(listAdapter);
        getItemsDetails();

    }

    /*private void performSearch() {
        if (originalItemList == null)
            originalItemList = new ArrayList<>(listAdapter.getItemList());
        List<OrderItemDetails> tempItems = new ArrayList<>();
        String query = input_search.getText().toString();
        if (query.length() == 0)
            return;
        for (OrderItemDetails details : originalItemList) {
            if (details.getChefDetails().getFullName().toLowerCase().contains(query.toLowerCase()) || details.getDeliveryDetails().getCustomerName().toLowerCase().contains(query.toLowerCase()) || details.getDeliveryDetails().getDa().toLowerCase().contains(query.toLowerCase()) || details.getChefDetails().getFullAddress().toLowerCase().contains(query.toLowerCase())) {
                tempItems.add(details);
            }
        }
        if (tempItems.size() == 0)
            showMessageLong("No results found");
        else {
            listAdapter.clear();
            listAdapter.addItems(tempItems);
        }
    }

    private void toggleSearchBarVisibility() {
        if (search_details_holder.getVisibility() != View.VISIBLE) {
            search_details_holder.setVisibility(View.VISIBLE);
            search_toggler.setImageResource(R.drawable.abc_ic_clear_mtrl_alpha);
            listAdapter.clear();
            listAdapter.addItems(originalItemList);
        } else {
            search_details_holder.setVisibility(View.GONE);
            search_toggler.setImageResource(R.drawable.ic_search_white);
        }
    }*/

    private void getItemsDetails() {
        if (!isRequestSent) {
            isRequestSent = true;
            showLoadingIndicator("Loading...");
            getCurrentLocation = new GetCurrentLocation(this, new GetCurrentLocation.Callback() {
                @Override
                public void onComplete(Location location) {
                    if (location != null)
                        onLocation(location);
                    else onNullLocation();
                }

                private void onLocation(Location location) {
                    current_location = location;
                    getItems(current_location);
                }

                public void onNullLocation() {
                    isRequestSent = false;
                    checkLocationAccessAndProceed(new LocationEnablerCallback() {
                        @Override
                        public void onComplete(Location location) {
                            onLocation(location);
                        }
                    });
                }
            });
        }
    }

    private void getItems(final Location current_location) {
        getBackendAPIs().getPickOrDeliverItemsAPI().getOrderItemsForToday(current_location.getLatitude(), current_location.getLongitude(), localStorageData.getUser().get_id(), new PickOrDeliverItemsAPI.OrderCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> itemsList) {
                switchToContentPage();
                isRequestSent = false;
                if (status) {
                    listAdapter.clear();
                    listAdapter.addItems(itemsList);
                    originalItemList = new ArrayList<>(listAdapter.getItemList());
                    if (current_location != null)
                        updateAdapterData(current_location, listAdapter);
                } else if (statusCode == Constants.code.response.NO_DATA) {
                    showNoDataIndicator("No items to show");
                } else
                    showReloadIndicator("An error occurred\ntry again later");
            }
        });
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getItemsDetails();
    }

    //==============


}
