package in.gocibo.logistics.backend.models;

import java.util.ArrayList;

import in.gocibo.logistics.models.Cookies;
import in.gocibo.logistics.models.User;

/**
 * Created by Naresh-Gocibo on 16-10-2015.
 */
public class ResponseObjectLogin {
    String token;
    User user;
    ArrayList<Cookies> cookies;

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public ArrayList<Cookies> getCookies() {
        return cookies;
    }
}
