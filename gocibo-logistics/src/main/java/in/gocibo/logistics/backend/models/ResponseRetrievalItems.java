package in.gocibo.logistics.backend.models;

import java.util.ArrayList;

import in.gocibo.logistics.models.RetrievalItemDetails;

/**
 * Created by Naresh-Gocibo on 15-10-2015.
 */
public class ResponseRetrievalItems {
    private ArrayList<RetrievalItemDetails> retrievalItemsDetails;

    public ArrayList<RetrievalItemDetails> getRetrievalItemsDetails() {
        return retrievalItemsDetails;
    }

    public void setRetrievalItemsDetails(ArrayList<RetrievalItemDetails> retrievalItemsDetails) {
        this.retrievalItemsDetails = retrievalItemsDetails;
    }
}
