package in.gocibo.logistics.backend;

import android.content.Context;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.ResponseStatus;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.backend.models.RequestObjectClaimRetrieval;
import in.gocibo.logistics.backend.models.RequestObjectDoneRetrieval;
import in.gocibo.logistics.backend.models.RetrievalItem;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.methods.CheckUpdate;
import in.gocibo.logistics.models.RetrievalItemDetails;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public class RetrievalsAPI {

    GoCiboToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface GetRetrievalItemsCallback {
        void onComplete(List<RetrievalItemDetails> retrievalItemsDetails, int status_code);
    }

    public interface RetrievalStatusCallback {
        void onComplete(RetrievalItemDetails details, int status_code);
    }

    public RetrievalsAPI(Context context, RestAdapter restAdapter) {
        this.activity = (GoCiboToolBarActivity) context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getRetrievalItems(final Double latitude, final Double longitude, final String myId, final GetRetrievalItemsCallback callback) {

        Map<String, String> loc = new HashMap<>();
        loc.put("loc", "" + latitude + "," + longitude);

        retrofitInterface.retrievals(activity.getDeviceID(), activity.getAppVersionCode(), loc, new Callback<List<RetrievalItem>>() {
            @Override
            public void success(List<RetrievalItem> responseRetrievalItems, Response response) {
                if (responseRetrievalItems != null && responseRetrievalItems.size() > 0) {
                    List<RetrievalItemDetails> list = new ArrayList<>();
                    for (int i = 0; i < responseRetrievalItems.size(); i++) {
                        RetrievalItem item = responseRetrievalItems.get(i);
                        item.setLocalStatus(0);
                        boolean item_found = false;
                        for (int j = 0; j < i; j++) {
                            RetrievalItem item2 = responseRetrievalItems.get(j);
                            if (item.equals(item2)) {
                                for (RetrievalItemDetails retrievalItemDetails : list) {
                                    if (retrievalItemDetails.getCustomer_id().equals(item.getItem().getCustomerDetails().get_id())) {
                                        retrievalItemDetails.addDabba(item.get_id(), item.getComments());
                                        item_found = true;
                                        break;
                                    }
                                }
                                if (item_found)
                                    break;
                            }
                        }
                        if (!item_found) {
                            list.add(new RetrievalItemDetails(item));
                        }
                    }
                    callback.onComplete(sortItems(list, new LatLng(latitude, longitude)), response.getStatus());
                } else
                    callback.onComplete(null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    callback.onComplete(null, -1);
                }
            }
        });
    }

    public void getMyRetrievals(final Double latitude, final Double longitude, final String myId, final GetRetrievalItemsCallback callback) {

        Map<String, String> loc = new HashMap<>();
        loc.put("loc", "" + latitude + "," + longitude);

        retrofitInterface.getDeliveryBoyRetrievals(activity.getDeviceID(), activity.getAppVersionCode(), loc, new Callback<List<RetrievalItem>>() {
            @Override
            public void success(List<RetrievalItem> responseRetrievalItems, Response response) {
                if (responseRetrievalItems != null) {
                    List<RetrievalItemDetails> list = new ArrayList<>();
                    for (int i = 0; i < responseRetrievalItems.size(); i++) {
                        RetrievalItem item = responseRetrievalItems.get(i);
                        item.setLocalStatus(1);
                        boolean item_found = false;
                        for (int j = 0; j < i; j++) {
                            RetrievalItem item2 = responseRetrievalItems.get(j);
                            if (item.equals(item2)) {
                                for (RetrievalItemDetails retrievalItemDetails : list) {
                                    if (retrievalItemDetails.getCustomer_id().equals(item.getItem().getCustomerDetails().get_id())) {
                                        retrievalItemDetails.addDabba(item.get_id(), item.getComments());
                                        item_found = true;
                                        break;
                                    }
                                }
                                if (item_found)
                                    break;
                            }
                        }
                        if (!item_found) {
                            list.add(new RetrievalItemDetails(item));
                        }
                    }
                    callback.onComplete(sortItems(list, new LatLng(latitude, longitude)), response.getStatus());
                } else
                    callback.onComplete(null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    callback.onComplete(null, -1);
                }
            }
        });
    }

    public void readyToRetrieval(List<String> dabba_ids, double latitude, double longitude, double bLatitude, double bLongitude, String buyer_id, String last_loc, final StatusCallback callback) {

        RequestObjectClaimRetrieval requestObject = new RequestObjectClaimRetrieval(dabba_ids, latitude, longitude, bLatitude, bLongitude, buyer_id, last_loc);

        retrofitInterface.readyToRetrieve(activity.getDeviceID(), activity.getAppVersionCode(), requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK"))
                    callback.onComplete(true, response.getStatus(), "");
                else if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("retrieval_already_claimed"))
                    callback.onComplete(false, Constants.code.response.NOT_AVAILABLE, "");
                else
                    callback.onComplete(false, response.getStatus(), "");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        ResponseStatus responseStatus = new Gson().fromJson(json, ResponseStatus.class);
                        if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("retrieval_already_claimed"))
                            callback.onComplete(false, Constants.code.response.NOT_AVAILABLE, "");
                        else
                            callback.onComplete(false, error.getResponse().getStatus(), "");
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN, "");
                    }
                }
            }
        });
    }

    public void retrievalDone(List<String> dabbas, double latitude, double longitude, final StatusCallback callback) {

        retrofitInterface.retrievalDone(activity.getDeviceID(), activity.getAppVersionCode(), new RequestObjectDoneRetrieval(dabbas, latitude, longitude), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK"))
                    callback.onComplete(true, response.getStatus(), "");
                else
                    callback.onComplete(false, response.getStatus(), "");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "");
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN, "");
                    }
                }
            }
        });
    }

    public void unableToRetrieve(final RetrievalItemDetails details, final RetrievalStatusCallback callback) {

        retrofitInterface.unableToRetrieve(activity.getDeviceID(), activity.getAppVersionCode(), new HashMap(), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null)
                    callback.onComplete(details, response.getStatus());
                else
                    callback.onComplete(null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    callback.onComplete(null, -1);
                }
            }
        });
    }

    //------

    private List<RetrievalItemDetails> sortItems(List<RetrievalItemDetails> list, LatLng currentLatLng) {
        Map<Integer, List<RetrievalItemDetails>> map = new HashMap<>();
        for (RetrievalItemDetails details : list) {
            details.setDistance(LatLngMethods.getDistanceBetweenLatLng(details.getLatLng(), currentLatLng));
            Integer key = (details.getToH() * 100) + (details.getToM());
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<RetrievalItemDetails>());
                map.get(key).add(details);
            } else map.get(key).add(details);
        }
        Set<Integer> keySet = map.keySet();
        List<Integer> dummyList = new ArrayList<>(keySet);
        Collections.sort(dummyList, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                if (lhs > rhs)
                    return 1;
                else if (rhs > lhs)
                    return -1;
                return 0;
            }
        });
        list.clear();
        for (Integer integer : dummyList) {
            List<RetrievalItemDetails> subList = map.get(integer);
            if (subList != null) {
                if (subList.size() > 1) {
                    Collections.sort(subList, new Comparator<RetrievalItemDetails>() {
                        @Override
                        public int compare(RetrievalItemDetails lhs, RetrievalItemDetails rhs) {
                            return lhs.getDistance() == rhs.getDistance() ? 0 : (lhs.getDistance() > rhs.getDistance() ? 1 : -1);
                        }
                    });
                }
                for (RetrievalItemDetails details : subList) {
                    list.add(details);
                }

            }
        }
        return list;
    }


}
