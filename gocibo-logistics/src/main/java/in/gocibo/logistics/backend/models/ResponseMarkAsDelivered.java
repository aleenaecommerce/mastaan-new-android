package in.gocibo.logistics.backend.models;

/**
 * Created by venkatesh on 7/4/16.
 */
public class ResponseMarkAsDelivered {

    String code;
    boolean collectPayment;
    double collectAmount;

    public String getCode() {
        return code;
    }

    public boolean getCollecteAmountStatus() {
        return collectPayment;
    }

    public double getAmountToBeCollected() {
        return collectAmount;
    }
}
