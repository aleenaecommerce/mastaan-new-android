package in.gocibo.logistics.backend.models;

import in.gocibo.logistics.models.Job;

/**
 * Created by Naresh-Crypsis on 17-12-2015.
 */
public class RequestObjectClaimPickup {
    String loc;
    Job job;
    String last_loc;

    public RequestObjectClaimPickup(double latitude, double longitude, Job job) {
//        this.job = job;
        loc = latitude + "," + longitude;
    }

    public RequestObjectClaimPickup(double latitude, double longitude, String last_loc) {
        loc = latitude + "," + longitude;
        this.last_loc = last_loc;
    }
}
