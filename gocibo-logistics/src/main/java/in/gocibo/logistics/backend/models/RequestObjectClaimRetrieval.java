package in.gocibo.logistics.backend.models;

import java.util.List;

import in.gocibo.logistics.models.Job;

/**
 * Created by Naresh-Crypsis on 10-12-2015.
 */
public class RequestObjectClaimRetrieval {
    List<String> dabbas;
    String loc;
    String location;
    String buyer;
    Job job;
    String last_loc;

    public RequestObjectClaimRetrieval(List<String> dabbas, double latitude, double longitude, double bLatitude, double bLongitude, String buyer, String last_loc) {
        this.dabbas = dabbas;
        this.loc = latitude + "," + longitude;
        this.location = bLatitude + "," + bLongitude;
        this.buyer = buyer;
        this.last_loc = last_loc;
    }

    public RequestObjectClaimRetrieval(List<String> dabbas, double latitude, double longitude, double bLatitude, double bLongitude, String buyer, Job job) {
        this.dabbas = dabbas;
        this.loc = latitude + "," + longitude;
        this.location = bLatitude + "," + bLongitude;
        this.buyer = buyer;
        this.job = job;
    }
}
