package in.gocibo.logistics.backend.models;

import java.util.List;

/**
 * Created by Naresh-Crypsis on 29-11-2015.
 */
public class RequestObjectLinkDabbas {
    String chefid;
    String oid;
    List<String> dabbas;
    String loc;

    public RequestObjectLinkDabbas(String chefid, String oid, List<String> dabbas, Double latitude, Double longitude) {
        this.chefid = chefid;
        this.oid = oid;
        this.dabbas = dabbas;
        this.loc = latitude + "," + longitude;
    }
}
