package in.gocibo.logistics.backend.models;

import java.util.ArrayList;
import java.util.List;

import in.gocibo.logistics.models.DeliveryItemDetails;

/**
 * Created by Naresh-Gocibo on 15-10-2015.
 */
public class ResponseDeliveryItems {
    private List<DeliveryItemDetails> deliveryItems;

    public List<DeliveryItemDetails> getDeliveryItems() {
        return deliveryItems;
    }

    public void setDeliveryItems(List<DeliveryItemDetails> deliveryItems) {
        this.deliveryItems = deliveryItems;
    }
}
