package in.gocibo.logistics.backend.models;

import android.location.Location;

/**
 * Created by venkatesh on 1/4/16.
 */
public class RequestMarkAsDelivered {
    String loc;
    //String amt;

    public RequestMarkAsDelivered(/*String amt, */Location location){
        //this.amt = amt;
        if ( location != null ){
            this.loc = location.getLatitude()+","+location.getLongitude();
        }
    }
}
