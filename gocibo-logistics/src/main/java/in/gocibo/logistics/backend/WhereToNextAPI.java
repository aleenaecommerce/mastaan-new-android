package in.gocibo.logistics.backend;

import android.content.Context;

import com.aleena.common.models.ResponseStatus;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.backend.models.RequestObjectLocation;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.methods.CheckUpdate;
import in.gocibo.logistics.models.Job;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public class WhereToNextAPI {

    GoCiboToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface SimpleStatusCallback {
        void onComplete(boolean status, int status_code);
    }

    public WhereToNextAPI(Context context, RestAdapter restAdapter) {
        this.activity = (GoCiboToolBarActivity) context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void whereToNext(String id, double latitude, double longitude, String my_id, final SimpleStatusCallback callback) {

        RequestObjectLocation requestObject = new RequestObjectLocation(latitude, longitude, new Job(my_id));

        retrofitInterface.whereToNext(activity.getDeviceID(), activity.getAppVersionCode(), id, requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null) {
                    if (responseStatus.getCode().equalsIgnoreCase("ok"))
                        callback.onComplete(true, response.getStatus());
                    else callback.onComplete(false, response.getStatus());
                } else callback.onComplete(false, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void resume(final SimpleStatusCallback callback) {

        retrofitInterface.resume(activity.getDeviceID(), activity.getAppVersionCode(), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null) {
                    if (responseStatus.getCode().equalsIgnoreCase("ok"))
                        callback.onComplete(true, response.getStatus());
                    else callback.onComplete(false, response.getStatus());
                } else callback.onComplete(false, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }


}
