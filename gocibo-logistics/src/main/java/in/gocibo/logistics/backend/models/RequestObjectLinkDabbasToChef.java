package in.gocibo.logistics.backend.models;

import java.util.List;

/**
 * Created by Naresh-Crypsis on 29-11-2015.
 */
public class RequestObjectLinkDabbasToChef {
    String chefid;
    List<String> dabbas;
    String loc;

    public RequestObjectLinkDabbasToChef(String chefid, List<String> dabbas, Double latitude, double longitude) {
        this.chefid = chefid;
        this.dabbas = dabbas;
        this.loc = latitude + "," + longitude;
    }
}
