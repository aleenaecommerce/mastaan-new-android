package in.gocibo.logistics.backend.models;

import java.util.List;

/**
 * Created by Naresh-Crypsis on 03-12-2015.
 */
public class RequestObjectMarkDabbaForRetrieval {
    String day;
    int rby;
    List<String> dabbas;
    String loc;

    public RequestObjectMarkDabbaForRetrieval(String day, int rby, List<String> dabbas, Double latitude, Double longitude) {
        this.day = day;
        this.rby = rby;
        this.dabbas = dabbas;
        this.loc = latitude + "," + longitude;
    }
}
