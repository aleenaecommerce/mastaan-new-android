package in.gocibo.logistics.backend;

import android.content.Context;
import android.location.Location;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.google.gson.Gson;

import java.util.HashMap;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.backend.models.RequestAmountCollected;
import in.gocibo.logistics.backend.models.RequestMarkAsDelivered;
import in.gocibo.logistics.backend.models.RequestObjectClaimPickup;
import in.gocibo.logistics.backend.models.RequestObjectLocation;
import in.gocibo.logistics.backend.models.ResponseMarkAsDelivered;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.methods.CheckUpdate;
import in.gocibo.logistics.models.Job;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public class DeliveriesAPI {

    GoCiboToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface MarkAsDeliveredCallback{
        void onComplete(boolean status, int statusCode, String message, boolean collectAmount, double amountToBeCollected);
    }

    public interface PickCallback {
        void onComplete(boolean status, int status_code);
    }

    public DeliveriesAPI(Context context, RestAdapter restAdapter) {
        this.activity = (GoCiboToolBarActivity) context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void readyToPickup(String item_id, Double latitude, Double longitude, String last_loc, final PickCallback callback) {

        RequestObjectClaimPickup requestObject = new RequestObjectClaimPickup(latitude, longitude, last_loc);

        retrofitInterface.readyToPickup(activity.getDeviceID(), activity.getAppVersionCode(), item_id, requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK"))
                    callback.onComplete(true, response.getStatus());
                else if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("pickup_already_claimed"))
                    callback.onComplete(false, Constants.code.response.NOT_AVAILABLE);
                else
                    callback.onComplete(false, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        ResponseStatus responseStatus = new Gson().fromJson(json, ResponseStatus.class);
                        if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("pickup_already_claimed"))
                            callback.onComplete(false, Constants.code.response.NOT_AVAILABLE);
                        else
                            callback.onComplete(false, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void markAsPickedUp(String item_id/*final PickUpItemDetails details*/, final PickCallback callback) {

        retrofitInterface.markAsPickedUp(activity.getDeviceID(), activity.getAppVersionCode(), item_id, new HashMap(), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus());
                } else
                    callback.onComplete(false, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void readyToDeliver(String item_id, double latitude, double longitude, String last_loc,/* final DeliveryItemDetails details,*/ final StatusCallback callback) {

        retrofitInterface.readyToDeliver(activity.getDeviceID(), activity.getAppVersionCode(), item_id, new RequestObjectLocation(latitude, longitude, last_loc), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK"))
                    callback.onComplete(true, response.getStatus(), "");
                else if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("delivery_already_claimed"))
                    callback.onComplete(false, Constants.code.response.NOT_AVAILABLE, "");
                else
                    callback.onComplete(false, response.getStatus(), "");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        ResponseStatus responseStatus = new Gson().fromJson(json, ResponseStatus.class);
                        if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("delivery_already_claimed"))
                            callback.onComplete(false, Constants.code.response.NOT_AVAILABLE, "");
                        else
                            callback.onComplete(false, error.getResponse().getStatus(), "");
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN, "");
                    }
                }
            }
        });
    }

    public void deliveryDone(String item_id/*, String amountCollected, */,final Location location, final MarkAsDeliveredCallback callback) {

        retrofitInterface.deliveryDone(activity.getDeviceID(), activity.getAppVersionCode(), item_id, new RequestMarkAsDelivered(/*amountCollected, */location), new Callback<ResponseMarkAsDelivered>() {
            @Override
            public void success(ResponseMarkAsDelivered responseObject, Response response) {
                callback.onComplete(true, response.getStatus(), "Success", responseObject.getCollecteAmountStatus(), responseObject.getAmountToBeCollected());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", false, 0);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", false, 0);
                    }
                }
            }
        });
    }

    public void updateOrderAmountCollected(String orderitem_id, String amountCollected, final StatusCallback callback) {

        retrofitInterface.updateOrderAmountCollected(activity.getDeviceID(), activity.getAppVersionCode(), orderitem_id, new RequestAmountCollected(amountCollected), new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void createJob(final double latitude, final Double longitude, Job job, final StatusCallback callback) {

        retrofitInterface.createJob(activity.getDeviceID(), activity.getAppVersionCode(), new RequestObjectLocation(latitude, longitude, job), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null) {
                    if (responseStatus.getCode().equalsIgnoreCase("ok"))
                        callback.onComplete(true, response.getStatus(), "");
                    else callback.onComplete(false, response.getStatus(), "");
                } else callback.onComplete(false, response.getStatus(), "");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "");
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN, "");
                    }
                }
            }
        });
    }

}
