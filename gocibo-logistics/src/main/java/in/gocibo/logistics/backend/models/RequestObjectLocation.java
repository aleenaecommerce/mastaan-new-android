package in.gocibo.logistics.backend.models;

import in.gocibo.logistics.models.Job;

/**
 * Created by aleena on 12/12/15.
 */
public class RequestObjectLocation {
    String loc;
    Job job;
    String last_loc;

    public RequestObjectLocation(double latitude, double longitude, String last_loc) {
        loc = latitude + "," + longitude;
        this.last_loc = last_loc;
    }

    public RequestObjectLocation(double latitude, double longitude, Job job) {
        loc = latitude + "," + longitude;
        this.job = job;
    }
}
