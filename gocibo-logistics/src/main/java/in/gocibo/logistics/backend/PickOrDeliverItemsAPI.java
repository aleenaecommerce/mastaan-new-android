package in.gocibo.logistics.backend;

import android.content.Context;
import android.location.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.localdata.LocalStorageData;
import in.gocibo.logistics.methods.CheckUpdate;
import in.gocibo.logistics.models.ItemDetails;
import in.gocibo.logistics.models.OrderItemDetails;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public class PickOrDeliverItemsAPI {

    GoCiboToolBarActivity activity;
    RetrofitInterface retrofitInterface;


    public interface Callback {
        void onComplete(boolean status, List<ItemDetails> itemsDetails, int status_code);
    }

    public interface OrderCallback {
        void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> itemsList);
    }

    public PickOrDeliverItemsAPI(Context context, RestAdapter restAdapter) {
        this.activity = (GoCiboToolBarActivity) context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getItems(Double latitude, double longitude, final String myId, final Callback callback) {

        Map<String, Object> map = new HashMap<>();
        map.put("loc", latitude + "," + longitude);

        retrofitInterface.getPickUpsAndDeliveries(activity.getDeviceID(), activity.getAppVersionCode(), map, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(List<OrderItemDetails> items, Response response) {
                if (items != null && items.size() > 0) {
                    List<ItemDetails> itemsDetails = new ArrayList<>();
                    for (OrderItemDetails item : items) {
                        if (item.getStatus() == 3) {
                            if (item.getDdb() == null)
                                item.setMyLocalStatus(2);
                            else
                                item.setMyLocalStatus(3);
                            itemsDetails.add(new ItemDetails(item));
                        } else {
                            if (item.getPickedUpByBb() == null)
                                item.setMyLocalStatus(0);
                            else
                                item.setMyLocalStatus(1);
                            itemsDetails.add(new ItemDetails(item));
                        }
                    }
                    callback.onComplete(true, itemsDetails, response.getStatus());
                } else if (items != null && items.size() == 0) {
                    callback.onComplete(false, null, Constants.code.response.NO_DATA);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    /*public void getOrders(Double latitude, double longitude, final String myId, final OrderCallback callback) {

        Map<String, Object> map = new HashMap<>();
        map.put("loc", latitude + "," + longitude);

        retrofitInterface.getPickUpsAndDeliveries(map, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(List<OrderItemDetails> items, Response response) {
                if (items != null && items.size() > 0) {
                    List<OrderItemDetails> itemsDetails = new ArrayList<>();
                    for (OrderItemDetails item : items) {
                        if (item.getStatus() == 3) {
                            if (item.getDdb() == null)
                                item.setMyLocalStatus(2);
                            else
                                item.setMyLocalStatus(3);
                            itemsDetails.add(item);
                        } else {
                            if (item.getPickedUpByBb() == null)
                                item.setMyLocalStatus(0);
                            else
                                item.setMyLocalStatus(1);
                            itemsDetails.add(item);
                        }
                    }
                    callback.onComplete(true, itemsDetails, response.getStatus());
                } else if (items != null && items.size() == 0) {
                    callback.onComplete(false, null, Constants.code.response.NO_DATA);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }*/

    public void getOrderItemsForToday(Double latitude, double longitude, final String myId, final OrderCallback callback) {

        Map<String, Object> map = new HashMap<>();
        map.put("loc", latitude + "," + longitude);

        retrofitInterface.getOrderItemsForToday(activity.getDeviceID(), activity.getAppVersionCode(), map, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(List<OrderItemDetails> items, Response response) {

                if (items != null) {
                    List<OrderItemDetails> itemsDetails = new ArrayList<>();
                    for (OrderItemDetails item : items) {
                        if (item.getStatus() == 3) {
                            if (item.getDdb() == null)
                                item.setMyLocalStatus(2);
                            else
                                item.setMyLocalStatus(3);
                        } else if (item.getStatus() != 4 && item.getStatus() != 5 && item.getStatus() != 6) {
                            if (item.getPickedUpByBb() == null)
                                item.setMyLocalStatus(0);
                            else
                                item.setMyLocalStatus(1);
                        } else
                            item.setMyLocalStatus(3);
                        itemsDetails.add(item);
                    }
                    callback.onComplete(true, response.getStatus(), "Success", itemsDetails);
                }
                else  {
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getMyPickOrDeliveryItem(Double latitude, double longitude, final String myId, final Callback callback) {

        Map<String, Object> map = new HashMap<>();
        map.put("loc", latitude + "," + longitude);

        retrofitInterface.getMyPickUpsAndDeliveries(activity.getDeviceID(), activity.getAppVersionCode(), map, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(List<OrderItemDetails> items, Response response) {
                if (items != null && items.size() > 0) {
                    List<ItemDetails> itemsDetails = new ArrayList<>();
                    for (OrderItemDetails item : items) {
                        if (item.getStatus() == 3) {
                            if (item.getDdb() == null)
                                item.setMyLocalStatus(2);
                            else
                                item.setMyLocalStatus(3);
                            itemsDetails.add(new ItemDetails(item));
                        } else {
                            if (item.getPickedUpByBb() == null)
                                item.setMyLocalStatus(0);
                            else
                                item.setMyLocalStatus(1);
                            itemsDetails.add(new ItemDetails(item));
                        }
                    }
                    callback.onComplete(true, itemsDetails, response.getStatus());
                } else if (items != null && items.size() == 0) {
                    callback.onComplete(false, null, Constants.code.response.NO_DATA);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void getMyPickOrDeliveryOrders(final Location location, final OrderCallback callback) {

        Map<String, Object> map = new HashMap<>();
        map.put("loc", location.getLatitude() + "," + location.getLongitude());

        retrofitInterface.getMyPickUpsAndDeliveries(activity.getDeviceID(), activity.getAppVersionCode(), map, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(List<OrderItemDetails> items, Response response) {

                if ( items != null ) {
                    List<OrderItemDetails> itemsList = new ArrayList<>();
                    String userID = new LocalStorageData(activity).getUserID();

                    for (OrderItemDetails item : items) {
                        if (item.getStatus() == 3 && (item.getDdb() != null && item.getDdb().equals(userID))) {
                            item.setMyLocalStatus(3);
                            itemsList.add(item);
                        } else if (item.getPickedUpByBb() != null && item.getPickedUpByBb().equals(userID)) {
                            item.setMyLocalStatus(1);
                            itemsList.add(item);
                        }
                    }
                    callback.onComplete(true, response.getStatus(), "Success", itemsList);
                }
                else {
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }


}
