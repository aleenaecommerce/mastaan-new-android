package in.gocibo.logistics.backend;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.backend.models.ReponseLinkDabbasToChefUpload;
import in.gocibo.logistics.backend.models.RequestLinkDabbasToChefUpload;
import in.gocibo.logistics.backend.models.RequestObjectLinkDabbas;
import in.gocibo.logistics.backend.models.RequestObjectLinkDabbasToChef;
import in.gocibo.logistics.backend.models.RequestObjectMarkDabbaForRetrieval;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.methods.CheckUpdate;
import in.gocibo.logistics.models.Dabba;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public class DabbasAPI {

    GoCiboToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface DabbasListCallback {
        void onComplete(boolean status, int statusCode, String message, List<Dabba> dabbasList);
    }

    public interface LinkDabbasCallback {
        void onComplete(boolean status, int statusCode, String message, List<String> linkedDabbas);
    }

    public interface DabbasCallback {
        void onComplete(boolean status, List<Dabba> dabbas, int status_code);
    }

    public DabbasAPI(Context context, RestAdapter restAdapter) {
        this.activity = (GoCiboToolBarActivity) context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getDabbasInStock(final Location location, final DabbasListCallback callback) {

        retrofitInterface.getDabbasInStock(activity.getDeviceID(), activity.getAppVersionCode(), location.getLatitude() + "," + location.getLongitude(), new Callback<List<Dabba>>() {
            @Override
            public void success(List<Dabba> dabbas, Response response) {
                if (dabbas != null) {
                    Log.d(Constants.LOG_TAG, "\nDABBAS_API : DABBAS_API : Success");
                    callback.onComplete(true, 200, "Success", dabbas);
                } else {
                    Log.d(Constants.LOG_TAG, "\nDABBAS_API : DABBAS_API : Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nDABBAS_API : DABBAS_API : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nDABBAS_API : DABBAS_API : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void linkDabbasToChefUpload(final String uploadID, final List<String> selectedDabbas, final LinkDabbasCallback callback) {

        retrofitInterface.linkDabbasToChefUpload(activity.getDeviceID(), activity.getAppVersionCode(), new RequestLinkDabbasToChefUpload(selectedDabbas), uploadID, new Callback<ReponseLinkDabbasToChefUpload>() {
            @Override
            public void success(ReponseLinkDabbasToChefUpload reposneObject, Response response) {
                if (reposneObject.getDabbas() != null) {
                    Log.d(Constants.LOG_TAG, "\nDABBAS_API : LINK_DABBAS_TO_CHEF_UPLOAD : Success");
                    callback.onComplete(true, 200, "Success", reposneObject.getDabbas());
                } else {
                    Log.d(Constants.LOG_TAG, "\nDABBAS_API : LINK_DABBAS_TO_CHEF_UPLOAD : Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nDABBAS_API : LINK_DABBAS_TO_CHEF_UPLOAD : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nDABBAS_API : LINK_DABBAS_TO_CHEF_UPLOAD : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getDabbasForAChef(String id, final DabbasCallback callback) {

        retrofitInterface.getDabbasForAChef(activity.getDeviceID(), activity.getAppVersionCode(), id, new Callback<List<Dabba>>() {
            @Override
            public void success(List<Dabba> dabbas, Response response) {
                if (dabbas != null && dabbas.size() > 0)
                    callback.onComplete(true, dabbas, response.getStatus());
                else if (dabbas != null && dabbas.size() == 0)
                    callback.onComplete(false, null, Constants.code.response.NO_DATA);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void linkDabbasToOrder(String chef_id, String order_id, Double latitude, Double longitude, List<Dabba> dabbas, final StatusCallback callback) {
        List<String> dabbas_ids = new ArrayList<>();
        for (Dabba dabba : dabbas)
            dabbas_ids.add(dabba.get_id());
        RequestObjectLinkDabbas requestObject = new RequestObjectLinkDabbas(chef_id, order_id, dabbas_ids, latitude, longitude);

        retrofitInterface.linkDabbas(activity.getDeviceID(), activity.getAppVersionCode(), requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null) {
                    if (responseStatus.getCode().equalsIgnoreCase("ok"))
                        callback.onComplete(true, response.getStatus(), "");
                    else callback.onComplete(false, response.getStatus(), "");
                } else callback.onComplete(false, response.getStatus(), "");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "");
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN, "");
                    }
                }
            }
        });
    }

    public void linkDabbasToChef(String chef_id, double latitude, double longitude, List<Dabba> dabbas, final StatusCallback callback) {
        List<String> dabbas_ids = new ArrayList<>();
        for (Dabba dabba : dabbas)
            dabbas_ids.add(dabba.get_id());
        RequestObjectLinkDabbasToChef requestObject = new RequestObjectLinkDabbasToChef(chef_id, dabbas_ids, latitude, longitude);

        retrofitInterface.linkDabbasToChef(activity.getDeviceID(), activity.getAppVersionCode(), requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null) {
                    if (responseStatus.getCode().equalsIgnoreCase("ok"))
                        callback.onComplete(true, response.getStatus(), "");
                    else callback.onComplete(false, response.getStatus(), "");
                } else callback.onComplete(false, response.getStatus(), "");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "");
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN, "");
                    }
                }
            }
        });
    }

    public void getMyDabbas(Double latitude, Double longitude, final DabbasCallback callback) {

        Map<String, Object> map = new HashMap<>();
        map.put("loc", latitude + "," + longitude);

        retrofitInterface.getMyDabbas(activity.getDeviceID(), activity.getAppVersionCode(), map, new Callback<List<Dabba>>() {
            @Override
            public void success(List<Dabba> dabbas, Response response) {
                if (dabbas != null && dabbas.size() > 0)
                    callback.onComplete(true, dabbas, response.getStatus());
                else if (dabbas != null && dabbas.size() == 0)
                    callback.onComplete(false, null, Constants.code.response.NO_DATA);
                else callback.onComplete(false, null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void getDabbasForChefUpload(String uplodID, Double latitude, Double longitude, final DabbasCallback callback) {

        retrofitInterface.getDabbasForChefUpload(activity.getDeviceID(), activity.getAppVersionCode(), uplodID, latitude + "," + longitude, new Callback<List<Dabba>>() {
            @Override
            public void success(List<Dabba> dabbas, Response response) {
                if (dabbas != null && dabbas.size() > 0)
                    callback.onComplete(true, dabbas, response.getStatus());
                else if (dabbas != null && dabbas.size() == 0)
                    callback.onComplete(false, null, Constants.code.response.NO_DATA);
                else callback.onComplete(false, null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void markDabbasForRetrieval(double latitude, double longitude, List<String> dabba_ids, int hour, String day, final StatusCallback callback) {

        RequestObjectMarkDabbaForRetrieval requestObject = new RequestObjectMarkDabbaForRetrieval(day, hour, dabba_ids, latitude, longitude);

        retrofitInterface.markDabbasForRetrieval(activity.getDeviceID(), activity.getAppVersionCode(), requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null) {
                    if (responseStatus.getCode().equalsIgnoreCase("ok"))
                        callback.onComplete(true, response.getStatus(), "");
                    else callback.onComplete(false, response.getStatus(), "");
                } else callback.onComplete(false, response.getStatus(), "");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    callback.onComplete(false, -1, "");
                }
            }
        });
    }


}
