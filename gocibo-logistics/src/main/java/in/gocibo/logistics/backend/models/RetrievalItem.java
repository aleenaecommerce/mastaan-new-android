package in.gocibo.logistics.backend.models;

import in.gocibo.logistics.models.OrderItemDetails;

/**
 * Created by Naresh-Crypsis on 03-12-2015.
 */
public class RetrievalItem {
    private String _id;// dabba id
    String cm;          // Comment
    private Integer __v;
    private String c;
    private OrderItemDetails o;//order item
    private String b;
    private Integer idx;
    private String cd;
    private String ud;
    private Boolean a;
    private String rt;
    private String s;
    private String t;
    private int status;
    int localStatus = 0;
    private String rdb;

    public void setLocalStatus(int localStatus) {
        this.localStatus = localStatus;
    }

    public int getLocalStatus() {
        return localStatus;
    }

    public OrderItemDetails getItem() {
        return o;
    }

    public String get_id() {
        return _id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getRt() {
        return rt;
    }

    public boolean equals(RetrievalItem item) {
        return this.getRt() != null && item.getRt() != null && this.getItem().getCustomerDetails().getDa().equalsIgnoreCase(item.getItem().getCustomerDetails().getDa()) && this.getRt().equals(item.getRt()) && this.localStatus == item.localStatus;
    }

    public String getRdb() {
        return rdb;
    }

    public String getComments() {
        return cm;
    }
}
