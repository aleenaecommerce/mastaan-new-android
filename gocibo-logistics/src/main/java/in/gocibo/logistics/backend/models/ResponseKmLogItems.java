package in.gocibo.logistics.backend.models;

import java.util.List;

import in.gocibo.logistics.models.KmLogItemDetails;

/**
 * Created by Naresh-Gocibo on 18-10-2015.
 */
public class ResponseKmLogItems {
    private List<KmLogItemDetails> km_log_items;

    public void setKmLogItems(List<KmLogItemDetails> km_log_items) {
        this.km_log_items = km_log_items;
    }

    public List<KmLogItemDetails> getKm_log_items() {
        return km_log_items;
    }
}
