package in.gocibo.logistics.backend.models;

import java.util.List;

/**
 * Created by Naresh-Crypsis on 11-12-2015.
 */
public class RequestObjectDoneRetrieval {
    String loc;
    List<String> dabbas;

    public RequestObjectDoneRetrieval(List<String> dabbas, double latitude, double longitude) {
        this.dabbas = dabbas;
        loc = latitude + "," + longitude;
    }
}
