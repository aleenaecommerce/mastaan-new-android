package in.gocibo.logistics.backend;

import android.content.Context;
import android.util.Log;

import java.util.List;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.methods.CheckUpdate;
import in.gocibo.logistics.models.UploadDetails;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public class FoodItemsAPI {

    GoCiboToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface FoodItemsListCallback {
        void onComplete(boolean status, int statusCode, List<UploadDetails> uploadsList);
    }

    public FoodItemsAPI(Context context, RestAdapter restAdapter) {
        this.activity = (GoCiboToolBarActivity) context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getTodaysFoodItems(final FoodItemsListCallback callback) {

        retrofitInterface.getTodaysUploads(activity.getDeviceID(), activity.getAppVersionCode(), new retrofit.Callback<List<UploadDetails>>() {
            @Override
            public void success(List<UploadDetails> uploadsList, Response response) {
                if (uploadsList != null) {
                    Log.d(Constants.LOG_TAG, "\nUPLOADS_API : [ TODAYS_UPLOADS ]  >>  Success");
                    callback.onComplete(true, 200, uploadsList);
                } else {
                    Log.d(Constants.LOG_TAG, "\nUPLOADS_API : [ TODAYS_UPLOADS ]  >>  Failure");
                    callback.onComplete(false, 500, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nUPLOADS_API : [ TODAYS_UPLOADS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nUPLOADS_API : [ TODAYS_UPLOADS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }
}
