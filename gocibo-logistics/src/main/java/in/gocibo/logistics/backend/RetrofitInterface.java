package in.gocibo.logistics.backend;

import com.aleena.common.models.ResponseStatus;

import java.util.List;
import java.util.Map;

import in.gocibo.logistics.backend.models.ReponseLinkDabbasToChefUpload;
import in.gocibo.logistics.backend.models.RequestAmountCollected;
import in.gocibo.logistics.backend.models.RequestLinkDabbasToChefUpload;
import in.gocibo.logistics.backend.models.RequestMarkAsDelivered;
import in.gocibo.logistics.backend.models.RequestObjectClaimPickup;
import in.gocibo.logistics.backend.models.RequestObjectClaimRetrieval;
import in.gocibo.logistics.backend.models.RequestObjectDoneRetrieval;
import in.gocibo.logistics.backend.models.RequestObjectLinkDabbas;
import in.gocibo.logistics.backend.models.RequestObjectLinkDabbasToChef;
import in.gocibo.logistics.backend.models.RequestObjectLocation;
import in.gocibo.logistics.backend.models.RequestObjectLogin;
import in.gocibo.logistics.backend.models.RequestObjectMarkDabbaForRetrieval;
import in.gocibo.logistics.backend.models.ResponseMarkAsDelivered;
import in.gocibo.logistics.backend.models.ResponseObjectLogin;
import in.gocibo.logistics.backend.models.RetrievalItem;
import in.gocibo.logistics.models.Bootstrap;
import in.gocibo.logistics.models.Chef;
import in.gocibo.logistics.models.Dabba;
import in.gocibo.logistics.models.Hub;
import in.gocibo.logistics.models.KmLogItem;
import in.gocibo.logistics.models.OrderItemDetails;
import in.gocibo.logistics.models.UploadDetails;
import in.gocibo.logistics.models.User;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public interface RetrofitInterface {
    @POST("/logout")
    void confirmLogout(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<ResponseStatus> callback);

    @POST("/connect")
    void login(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestObjectLogin user, Callback<ResponseObjectLogin> callback);

    @GET("/retrievals")
    void retrievals(@Query("i") String deviceID, @Query("appVersion") int appVersion, @QueryMap Map options, Callback<List<RetrievalItem>> callback);

    @GET("/deliveryboyretrievals")
    void getDeliveryBoyRetrievals(@Query("i") String deviceID, @Query("appVersion") int appVersion, @QueryMap Map options, Callback<List<RetrievalItem>> callback);

    @POST("/pickups/claim/{item_id}")
    void readyToPickup(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestObjectClaimPickup options, Callback<ResponseStatus> callback);

    @POST("/deliveries/claim/{item_id}")
    void readyToDeliver(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestObjectLocation requestObject, Callback<ResponseStatus> callback);

    @POST("/retrievals/claim")
    void readyToRetrieve(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestObjectClaimRetrieval requestObject, Callback<ResponseStatus> callback);

    @POST("/pickups/markaspickedup/{item_id}")
    void markAsPickedUp(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body Map options, Callback<ResponseStatus> callback);

    @POST("/pickups/markasdelivered/{item_id}")
    void deliveryDone(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestMarkAsDelivered requestMarkAsDelivered, Callback<ResponseMarkAsDelivered> callback);

    @POST("/orderitem/markmoneycollection/{item_id}")
    void updateOrderAmountCollected(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestAmountCollected requestObject, Callback<ResponseStatus> callback);

    @POST("/retrievals/markaspickedup")
    void retrievalDone(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestObjectDoneRetrieval requestObject, Callback<ResponseStatus> callback);

    @POST("/unable-to-pickup")
    void unableToPickup(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body Map options, Callback<ResponseStatus> callback);

    @POST("/unable-to-deliver")
    void unableToDeliver(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body Map options, Callback<ResponseStatus> callback);

    @POST("/unable-to-pickup")
    void unableToRetrieve(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body Map options, Callback<ResponseStatus> callback);

    @GET("/profile")
    void getProfile(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<User> userCallback);

    @GET("/pendingorderitems")
    void getPickUpsAndDeliveries(@Query("i") String deviceID, @Query("appVersion") int appVersion, @QueryMap Map<String, Object> map, Callback<List<OrderItemDetails>> items);

    @GET("/orderitemsfortoday")
    void getOrderItemsForToday(@Query("i") String deviceID, @Query("appVersion") int appVersion,@QueryMap Map<String, Object> map, Callback<List<OrderItemDetails>> items);

    @GET("/deliveryboypendingorderitems")
    void getMyPickUpsAndDeliveries(@Query("i") String deviceID, @Query("appVersion") int appVersion,@QueryMap Map<String, Object> map, Callback<List<OrderItemDetails>> items);

    @GET("/getdabbasforchef/{chef_id}")
    void getDabbasForAChef(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("chef_id") String chef_id, Callback<List<Dabba>> callback);

    @POST("/pickups/linkdabbas")
    void linkDabbas(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestObjectLinkDabbas objectLinkDabbas, Callback<ResponseStatus> callback);

    @POST("/linkdabbastochef")
    void linkDabbasToChef(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestObjectLinkDabbasToChef objectLinkDabbas, Callback<ResponseStatus> callback);

    @GET("/getdabbas")
    void getMyDabbas(@Query("i") String deviceID, @Query("appVersion") int appVersion, @QueryMap Map<String, Object> query, Callback<List<Dabba>> callback);

    @GET("/getdabbasforchefupload/{upload_id}")
    void getDabbasForChefUpload(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("upload_id") String upload_id, @Query("loc") String location, Callback<List<Dabba>> callback);

    @GET("/getdabbasinstock")
    void getDabbasInStock(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<Dabba>> callback);

    @POST("/retrievals/markdabbaforretrieval")
    void markDabbasForRetrieval(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestObjectMarkDabbaForRetrieval requestObject, Callback<ResponseStatus> callback);

    @POST("/linkdabbastochefupload/{upload_id}")
    void linkDabbasToChefUpload(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestLinkDabbasToChefUpload requestObject, @Path("upload_id") String upload_id, Callback<ReponseLinkDabbasToChefUpload> callback);

    @GET("/trips/{month}/{year}")
    void getKmLogTripList(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("month") int month, @Path("year") int year, @QueryMap Map<String, Object> options, Callback<List<KmLogItem>> callback);

    @GET("/bootstrap")
    void getBootstrap(@Query("i") String deviceID, @Query("appVersion") int appVersion, @QueryMap Map<String, Object> query, Callback<Bootstrap> callback);

    @POST("/next/{next_id}")
    void whereToNext(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("next_id") String id, @Body RequestObjectLocation requestObject, Callback<ResponseStatus> callback);

    @GET("/resume")
    void resume(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<ResponseStatus> callback);

    @GET("/chefs")
    void getChefs(@Query("i") String deviceID, @Query("appVersion") int appVersion, @QueryMap Map<String, Object> query, Callback<List<Chef>> callback);

    @GET("/hubs")
    void getHubs(@Query("i") String deviceID, @Query("appVersion") int appVersion, @QueryMap Map<String, Object> query, Callback<List<Hub>> callback);

    @POST("/createjob")
    void createJob(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestObjectLocation requestObject, Callback<ResponseStatus> callback);

    //------

    @GET("/uploads/today")
    void getTodaysUploads(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<UploadDetails>> callback);
}