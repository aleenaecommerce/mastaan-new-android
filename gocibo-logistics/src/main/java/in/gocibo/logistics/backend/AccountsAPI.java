package in.gocibo.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.backend.models.RequestObjectLogin;
import in.gocibo.logistics.backend.models.ResponseObjectLogin;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.methods.CheckUpdate;
import in.gocibo.logistics.models.Cookies;
import in.gocibo.logistics.models.KmLogItem;
import in.gocibo.logistics.models.User;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public class AccountsAPI {

    GoCiboToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface LoginCallback {
        void onComplete(boolean status, int statusCode, String message, String token, User user, ArrayList<Cookies> cookies);
    }

    public interface UserDetailsCallback {
        void onComplete(boolean status, User user, int status_code);
    }

    public interface KmLogsCallback {
        void onComplete(boolean status, Map<Integer, List<KmLogItem>> kmLogMap, int status_code);
    }

    public AccountsAPI(Context context, RestAdapter restAdapter) {
        this.activity = (GoCiboToolBarActivity) context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    /*public AccountsAPI(Context context, RetrofitInterface retrofitInterface) {
        this.context = context;
        adapter = GetRetrofitRestAdapter.getRetrofitRestAdapter(context.getString(R.string.base_url));
        String header_value = PreferenceManager.getDefaultSharedPreferences(context).getString("token", "");
        adapterWithHeader = GetRetrofitRestAdapter.getRetrofitRestAdapterWithHeader(context.getString(R.string.base_url), Constants.TOKEN, header_value);
    }*/

    public void login(final String email, final String password, final LoginCallback callback) {

        retrofitInterface.login(activity.getDeviceID(), activity.getAppVersionCode(), new RequestObjectLogin(email, password), new Callback<ResponseObjectLogin>() {
            @Override
            public void success(ResponseObjectLogin responseObjectLogin, Response response) {
                if (responseObjectLogin != null && responseObjectLogin.getToken() != null) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  Success");
                    callback.onComplete(true, 200, "Success", responseObjectLogin.getToken(), responseObjectLogin.getUser(), responseObjectLogin.getCookies());
                } else {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  Success");
                    callback.onComplete(false, 500, "Something went wrong, try again!", null, null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        String response_message = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        if (response_message == null || response_message.length() == 0) {
                            response_message = "Something went wrong, try again!";
                        }
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), response_message, null, null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  ERROR");
                        callback.onComplete(false, -1, "Something went wrong, try again!", null, null, null);
                    }
                }
            }
        });
    }

    public void logout(final StatusCallback callback) {

        retrofitInterface.confirmLogout(activity.getDeviceID(), activity.getAppVersionCode(), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nGoCibo-Logout : Success for (a="  + ")");
                    //localStorageData.setPushNotificationsRegistratinStatus(false);      // Clearing For JustPush from BuyerPush
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nGoCibo-Logout : Failure for (a="  + ")");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nGoCibo-Logout : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nGoCibo-Logout : Error = " + error.getKind());
                    }
                    callback.onComplete(false, -1, "Error");
                }
            }
        });
    }

    public void getUserDetails(final UserDetailsCallback callback) {

        retrofitInterface.getProfile(activity.getDeviceID(), activity.getAppVersionCode(), new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                if (user != null) {
                    callback.onComplete(true, user, response.getStatus());
                } else callback.onComplete(false, null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void getKmLogs(int month, int year, final KmLogsCallback callback) {

        retrofitInterface.getKmLogTripList(activity.getDeviceID(), activity.getAppVersionCode(), month, year, new HashMap<String, Object>(), new retrofit.Callback<List<KmLogItem>>() {
            @Override
            public void success(List<KmLogItem> kmLogItems, Response response) {
                if (kmLogItems != null && kmLogItems.size() > 0) {
                    Map<Integer, List<KmLogItem>> kmLogMap = new HashMap<>();
                    for (KmLogItem item : kmLogItems) {
                        int year = DateMethods.getYearFromDate(item.getTripDate());
                        int month = DateMethods.getMonthFromDate(item.getTripDate());
                        int day = DateMethods.getDayFromDate(item.getTripDate());
                        Integer key = year * 10000 + month * 100 + day;
                        if (!kmLogMap.containsKey(key)) {
                            kmLogMap.put(key, new ArrayList<KmLogItem>());
                            kmLogMap.get(key).add(item);
                        } else
                            kmLogMap.get(key).add(item);
                    }
                    callback.onComplete(true, kmLogMap, response.getStatus());
                } else if (kmLogItems != null && kmLogItems.size() <= 0)
                    callback.onComplete(false, null, Constants.code.response.NO_DATA);
                else callback.onComplete(false, null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

}
