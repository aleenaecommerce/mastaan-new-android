package in.gocibo.logistics.backend;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.gocibo.logistics.activities.GoCiboToolBarActivity;
import in.gocibo.logistics.constants.Constants;
import in.gocibo.logistics.methods.CheckUpdate;
import in.gocibo.logistics.models.Bootstrap;
import in.gocibo.logistics.models.Chef;
import in.gocibo.logistics.models.Hub;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public class CommonAPI {

    GoCiboToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface BootstrapCallback {
        void onComplete(boolean status, Bootstrap bootstrap, int status_code);
    }

    public interface ChefsCallback {
        void onComplete(boolean status, List<Chef> chefs, int status_code);
    }

    public interface HubsCallback {
        void onComplete(boolean status, List<Hub> chefs, int status_code);
    }

    public CommonAPI(Context context, RestAdapter restAdapter) {
        this.activity = (GoCiboToolBarActivity) context;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getBootStrap(/*Double latitude, Double longitude,*/ final BootstrapCallback callback) {

        Map<String, Object> map = new HashMap<>();
        //map.put("loc", latitude + "," + longitude);
        retrofitInterface.getBootstrap(activity.getDeviceID(), activity.getAppVersionCode(), map, new Callback<Bootstrap>() {
            @Override
            public void success(Bootstrap bootstrap, Response response) {
                if (bootstrap != null)
                    callback.onComplete(true, bootstrap, response.getStatus());
                else
                    callback.onComplete(false, null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void getChefs(double latitude, double longitude, final ChefsCallback callback) {

        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("loc", latitude + "," + longitude);

        retrofitInterface.getChefs(activity.getDeviceID(), activity.getAppVersionCode(), queryMap, new Callback<List<Chef>>() {
            @Override
            public void success(List<Chef> all_chefs, Response response) {
                if (all_chefs != null && all_chefs.size() > 0) {
                    List<Chef> chefs = new ArrayList<>();
                    for (Chef chef : all_chefs)
                        if (chef.getLoc() != null)
                            chefs.add(chef);
                    if (chefs.size() > 0)
                        callback.onComplete(true, chefs, response.getStatus());
                    else
                        callback.onComplete(false, chefs, Constants.code.response.NO_DATA);
                } else
                    callback.onComplete(false, null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

    public void getHubs(double latitude, double longitude, final HubsCallback callback) {

        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("loc", latitude + "," + longitude);

        retrofitInterface.getHubs(activity.getDeviceID(), activity.getAppVersionCode(), queryMap, new Callback<List<Hub>>() {
            @Override
            public void success(List<Hub> hubs, Response response) {
                if (hubs != null && hubs.size() > 0) {
                    callback.onComplete(true, hubs, response.getStatus());
                } else if (hubs != null && hubs.size() == 0)
                    callback.onComplete(false, hubs, Constants.code.response.NO_DATA);
                else
                    callback.onComplete(false, null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckUpdate(activity).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }

}
