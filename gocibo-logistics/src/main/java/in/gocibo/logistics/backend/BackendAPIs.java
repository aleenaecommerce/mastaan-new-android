package in.gocibo.logistics.backend;

import android.content.Context;

import in.gocibo.logistics.R;
import in.gocibo.logistics.localdata.LocalStorageData;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by Venkatesh Uppu on 06/05/2016.
 */

public class BackendAPIs {

    Context context;

    RestAdapter restAdapter;
    //RetrofitInterface retrofitInterface;

    CommonAPI commonAPI;
    AccountsAPI accountsAPI;
    DeliveriesAPI deliveriesAPI;
    DabbasAPI dabbasAPI;
    PickOrDeliverItemsAPI pickOrDeliverItemsAPI;
    WhereToNextAPI whereToNextAPI;
    FoodItemsAPI foodItemsAPI;
    RetrievalsAPI retrievalsAPI;

    public BackendAPIs(Context context){
        this.context = context;
    }

    private RestAdapter getRestAdapter(){
        if ( restAdapter == null ){
            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(context.getString(R.string.base_url))
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            request.addHeader("x-access-token", new LocalStorageData(context).getAccessToken());
                        }
                    })
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            //retrofitInterface = restAdapter.create(RetrofitInterface.class);
        }
        return restAdapter;
    }

    public CommonAPI getCommonAPI() {
        if ( commonAPI == null ){
            commonAPI = new CommonAPI(context, getRestAdapter());
        }
        return commonAPI;
    }

    public AccountsAPI getAccountsAPI() {
        if ( accountsAPI == null ){
            accountsAPI = new AccountsAPI(context, getRestAdapter());
        }
        return accountsAPI;
    }

    public DeliveriesAPI getDeliveriesAPI() {
        if ( deliveriesAPI == null ){
            deliveriesAPI = new DeliveriesAPI(context, getRestAdapter());
        }
        return deliveriesAPI;
    }

    public DabbasAPI getDabbasAPI() {
        if ( dabbasAPI == null ){
            dabbasAPI = new DabbasAPI(context, getRestAdapter());
        }
        return dabbasAPI;
    }

    public RetrievalsAPI getRetrievalsAPI() {
        if ( retrievalsAPI == null ){
            retrievalsAPI = new RetrievalsAPI(context, getRestAdapter());
        }
        return retrievalsAPI;
    }

    public PickOrDeliverItemsAPI getPickOrDeliverItemsAPI() {
        if ( pickOrDeliverItemsAPI == null ){
            pickOrDeliverItemsAPI = new PickOrDeliverItemsAPI(context, getRestAdapter());
        }
        return pickOrDeliverItemsAPI;
    }

    public WhereToNextAPI getWhereToNextAPI() {
        if ( whereToNextAPI == null ){
            whereToNextAPI = new WhereToNextAPI(context, getRestAdapter());
        }
        return whereToNextAPI;
    }

    public FoodItemsAPI getFoodItemsAPI() {
        if ( foodItemsAPI == null ){
            foodItemsAPI = new FoodItemsAPI(context, getRestAdapter());
        }
        return foodItemsAPI;
    }
}
