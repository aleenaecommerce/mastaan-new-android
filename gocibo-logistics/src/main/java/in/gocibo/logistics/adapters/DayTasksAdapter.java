package in.gocibo.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.models.DayTask;

/**
 * Created by Naresh-Crypsis on 08-11-2015.
 */
public class DayTasksAdapter extends ArrayAdapter<DayTask> {
    Context context;
    int resource;
    List<DayTask> itemList;

    public DayTasksAdapter(Context context, int resource, List<DayTask> itemList) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        TextView date = (TextView) convertView.findViewById(R.id.date);
        TextView count = (TextView) convertView.findViewById(R.id.count);
        date.setText(itemList.get(position).getDate());
        count.setText(itemList.get(position).getTaskCount() + "");
        return convertView;
    }
}
