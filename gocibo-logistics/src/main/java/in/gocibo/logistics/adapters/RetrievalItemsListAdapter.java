package in.gocibo.logistics.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;
import com.melnykov.fab.FloatingActionButton;

import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.models.RetrievalItemDetails;

/**
 * Created by Naresh-Gocibo on 09-10-2015.
 */
public class RetrievalItemsListAdapter extends ArrayAdapter<RetrievalItemDetails> {
    List<RetrievalItemDetails> itemList;
    Callback callback;
    LayoutInflater inflater;
    Context context;
    int resourceId;
    LatLng current_location = null;

    public RetrievalItemsListAdapter(Context context, int resourceId, List<RetrievalItemDetails> itemList, Callback callback) {
        super(context, resourceId);
        this.itemList = itemList;
        this.callback = callback;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.resourceId = resourceId;

    }

    public void addItem(RetrievalItemDetails details) {
        itemList.add(details);
        notifyDataSetChanged();
    }

    public void addItems(List<RetrievalItemDetails> deliveryItemDetailsList) {
        for (RetrievalItemDetails details : deliveryItemDetailsList) {
            if (current_location != null) {
                details.setDistance(LatLngMethods.getDistanceBetweenLatLng(current_location, details.getLatLng()));
            }
            itemList.add(details);
        }
        /*Collections.sort(itemList, new Comparator<RetrievalItemDetails>() {
            @Override
            public int compare(RetrievalItemDetails lhs, RetrievalItemDetails rhs) {
                return lhs.getDistance() == rhs.getDistance() ? 0 : (lhs.getDistance() > rhs.getDistance() ? 1 : -1);
            }
        });*/
        notifyDataSetChanged();
        notifyDataSetChanged();
    }

    public void clear() {
        itemList.clear();
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(resourceId, null);

        CardView item_card = (CardView) convertView.findViewById(R.id.item_card);
        TextView cardHeader = (TextView) convertView.findViewById(R.id.cardHeader);
        TextView name = (TextView) convertView.findViewById(R.id.name);
//        ImageView item_type = (ImageView) convertView.findViewById(R.id.item_type);
        View dish_holder = convertView.findViewById(R.id.dish_holder);
        dish_holder.setVisibility(View.GONE);
//        TextView item_name = (TextView) convertView.findViewById(R.id.item_name);
        TextView location = (TextView) convertView.findViewById(R.id.location);
        TextView number_of_portions = (TextView) convertView.findViewById(R.id.number_of_portions);
        TextView dabbas = (TextView) convertView.findViewById(R.id.dabbas);
        TextView time = (TextView) convertView.findViewById(R.id.time);
        FloatingActionButton phone_number = (FloatingActionButton) convertView.findViewById(R.id.phone_number);
        FloatingActionButton alt_phone_number = (FloatingActionButton) convertView.findViewById(R.id.phone_number_2);
        FloatingActionButton directions = (FloatingActionButton) convertView.findViewById(R.id.directions);
        TextView area = (TextView) convertView.findViewById(R.id.area);
        TextView land_mark = (TextView) convertView.findViewById(R.id.land_mark);
        //Updations
        convertView.findViewById(R.id.total_amount_of_order).setVisibility(View.GONE);
        convertView.findViewById(R.id.amount_label).setVisibility(View.GONE);

        cardHeader.setText(" R ");
        area.setText(itemList.get(position).getArea());
        land_mark.setText(itemList.get(position).getLand_mark());
        if (itemList.get(position).getAltPhoneNumber() != null)
            alt_phone_number.setVisibility(View.VISIBLE);
        else
            alt_phone_number.setVisibility(View.GONE);
        if (itemList.get(position).getStatus() == 1) {
            item_card.setCardBackgroundColor(Color.parseColor("#696558"));
            phone_number.setColorNormal(Color.parseColor("#FF4081"));
            directions.setColorNormal(Color.parseColor("#FF4081"));
        } else {
            item_card.setCardBackgroundColor(Color.parseColor("#9c27b0"));
            phone_number.setColorNormal(Color.parseColor("#4CAF50"));
            directions.setColorNormal(Color.parseColor("#4CAF50"));
        }
        alt_phone_number.setVisibility(View.GONE);
/*
        if (itemList.get(position).getItemType() == 0) {
            item_type.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_veg));
        } else {
            item_type.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_non_veg));
        }*/

        RetrievalItemDetails retrievalItemDetails = itemList.get(position);

        String dabbasString = "";
        for (int i=0;i<retrievalItemDetails.getDabbass().size();i++){
            if ( dabbasString.length() > 0 ) {
                dabbasString += ", " + retrievalItemDetails.getDabbass().get(i);
            }else{
                dabbasString += retrievalItemDetails.getDabbass().get(i);
            }
            try{
                if ( retrievalItemDetails.getDabbasComments().get(i) != null && retrievalItemDetails.getDabbasComments().get(i).length() > 0 ){
                    dabbasString += " ("+retrievalItemDetails.getDabbasComments().get(i)+")";
                }
            }catch (Exception e){}
        }
        dabbas.setText(dabbasString);//itemList.get(position).getFormatedDabbas());
        number_of_portions.setText("" + itemList.get(position).getNumberOfPortions());
        name.setText("Customer " + itemList.get(position).getCustomerName());
        location.setText(itemList.get(position).getAddress_line_1() + "\n" + itemList.get(position).getAddress_line_2());
//        item_name.setText(itemList.get(position).getItemName());
        time.setText((itemList.get(position).getRetrievalDate() == null ? "" : itemList.get(position).getRetrievalDate() + "\n")
                + "Time : " + itemList.get(position).getRetrievalTime());
        directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onDirectionsClick(position);
            }
        });
        phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onPhoneNumberClicked(position);
            }
        });
        alt_phone_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onAltPhoneNumberClicked(position);
            }
        });
        convertView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemList.get(position).getStatus() == 1)
                    callback.onRetrievedClicked(position);
                else
                    callback.onReadyToRetrievalClicked(position);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public RetrievalItemDetails getItem(int position) {
        return itemList.get(position);
    }

    public void setCurrentLocation(LatLng current_location) {
        this.current_location = current_location;
        for (RetrievalItemDetails details : itemList) {
            if (current_location != null)
                details.setDistance(LatLngMethods.getDistanceBetweenLatLng(current_location, details.getLatLng()));
        }
        /*Collections.sort(itemList, new Comparator<RetrievalItemDetails>() {
            @Override
            public int compare(RetrievalItemDetails lhs, RetrievalItemDetails rhs) {
                return lhs.getDistance() == rhs.getDistance() ? 0 : (lhs.getDistance() > rhs.getDistance() ? 1 : -1);
            }
        });*/
        notifyDataSetChanged();
    }

    public void setReadyToRetrieve(int position) {
        itemList.get(position).setStatus(1);
        notifyDataSetChanged();
    }

    public void setUnableToRetrieve(int position) {
        itemList.get(position).setStatus(0);
        notifyDataSetChanged();
    }

    public void setRetrieved(int position, List<String> dabbas, String nextTimeSlot) {
//        if (itemList.get(position).getDabbass().size() == dabbas.size())
        itemList.remove(position);
//        else {
//            List<String> tempDabbas = new ArrayList<>(itemList.get(position).getDabbass());
//            for (String dabba : dabbas) {
//                tempDabbas.remove(dabba);
//            }
//            itemList.get(position).setDabbas(tempDabbas);
//            itemList.get(position).setNumberOfPortions(tempDabbas.size());
//            itemList.get(position).setRetrievalTime(nextTimeSlot);
//            itemList.get(position).setStatus(0);
//        }
        notifyDataSetChanged();
    }

    public interface Callback {
        void onDirectionsClick(int position);

        void onPhoneNumberClicked(int position);

        void onAltPhoneNumberClicked(int position);

        void onReadyToRetrievalClicked(int position);

        void onRetrievedClicked(int position);
    }
}
