package in.gocibo.logistics.adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.melnykov.fab.FloatingActionButton;

import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.localdata.LocalStorageData;
import in.gocibo.logistics.models.OrderItemDetails;
import in.gocibo.logistics.models.User;

/**
 * Created by aleena on 29/1/16.
 */
public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder> {

    Context context;
    LocalStorageData localStorageData;
    List<OrderItemDetails> itemsList;
    List<User> deliveryBoys;

    Callback callback;

    public interface Callback{

        void onShowChefDirections(int position);
        void onCallChef(int position);
        void onShowCustomerDirections(int position);
        void onCallCustomer(int position);

        void onReadyToPickup(int position);
        void onPickupTaken(int position);

        void onReadyToDeliver(int position);
        void onDelivered(int position);

        void onShowOrderDetails(int position);
    }

    public OrderItemsAdapter(Context context, List<OrderItemDetails> itemsList, List<User> deliveryBoys, Callback callback) {
        this.context = context;
        this.localStorageData = new LocalStorageData(context);
        this.itemsList = itemsList;
        this.deliveryBoys = deliveryBoys;
        this.callback = callback;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        CardView container;

        TextView card_title;
        TextView action_type;
        TextView item_index;

        TextView name_1;
        TextView address_1;
        TextView land_mark_1;
        FloatingActionButton phone_1;
        FloatingActionButton directions_1;

        TextView name_2;
        TextView address_2;
        TextView land_mark_2;
        FloatingActionButton phone_2;
        FloatingActionButton directions_2;

        TextView item_name;
        ImageView item_type_indicator;
        TextView item_quantity;
        TextView item_cost;

        TextView order_cost;
        View buyerOutstandingDetails;
        TextView buyer_outstanding;

        TextView dabbas;

        View specialInstructionsHolder;
        TextView special_instructions;
        TextView delivery_time;
        TextView order_status;

        public ViewHolder(View itemView) {
            super(itemView);

            itemSelector = itemView.findViewById(R.id.itemSelector);
            container = (CardView) itemView.findViewById(R.id.container);

            card_title = (TextView) itemView.findViewById(R.id.card_title);
            action_type = (TextView) itemView.findViewById(R.id.action_type);
            item_index = (TextView) itemView.findViewById(R.id.item_index);

            name_1 = (TextView) itemView.findViewById(R.id.name_1);
            address_1 = (TextView) itemView.findViewById(R.id.address_1);
            land_mark_1 = (TextView) itemView.findViewById(R.id.land_mark_1);
            phone_1 = (FloatingActionButton) itemView.findViewById(R.id.phone_1);
            directions_1 = (FloatingActionButton) itemView.findViewById(R.id.directions_1);

            name_2 = (TextView) itemView.findViewById(R.id.name_2);
            address_2 = (TextView) itemView.findViewById(R.id.address_2);
            land_mark_2 = (TextView) itemView.findViewById(R.id.land_mark_2);
            phone_2 = (FloatingActionButton) itemView.findViewById(R.id.phone_2);
            directions_2 = (FloatingActionButton) itemView.findViewById(R.id.directions_2);

            item_name = (TextView) itemView.findViewById(R.id.item_name);
            item_type_indicator = (ImageView) itemView.findViewById(R.id.item_type_indicator);
            item_quantity = (TextView) itemView.findViewById(R.id.item_quantity);
            item_cost = (TextView) itemView.findViewById(R.id.item_cost);

            order_cost = (TextView) itemView.findViewById(R.id.order_cost);
            buyerOutstandingDetails = itemView.findViewById(R.id.buyerOutstandingDetails);
            buyer_outstanding = (TextView) itemView.findViewById(R.id.buyer_outstanding);

            dabbas = (TextView) itemView.findViewById(R.id.dabbas);

            specialInstructionsHolder = itemView.findViewById(R.id.specialInstructionsHolder);
            special_instructions = (TextView) itemView.findViewById(R.id.special_instructions);

            order_status = (TextView) itemView.findViewById(R.id.order_status);
            delivery_time = (TextView) itemView.findViewById(R.id.delivery_time);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.view_order_item, parent, false));
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final OrderItemDetails orderItemDetailsItemDetails = itemsList.get(position);

        if ( orderItemDetailsItemDetails != null ) {

            if ( orderItemDetailsItemDetails.getStatus() == 1 || orderItemDetailsItemDetails.getStatus() == 2 ) {
                setColors(viewHolder, R.color.background_material_dark, R.color.color_processing);
            }
            else if ( orderItemDetailsItemDetails.getStatus() == 4 ){
                setColors(viewHolder, R.color.order_delivered_color_fab, R.color.order_delivered_color);
            }
            else if ( orderItemDetailsItemDetails.getStatus() == 6 ){
                setColors(viewHolder, R.color.background_material_dark, R.color.color_processing);
            }
            else if ( orderItemDetailsItemDetails.getStatus() == 3 ){
                setColors(viewHolder, R.color.color_mutton, R.color.color_mutton_fab);
            }
            else if ( orderItemDetailsItemDetails.getStatus() == 7 ){
                setColors(viewHolder, R.color.color_seafood, R.color.color_seafood_fab);
            }
            else{
                setColors(viewHolder, R.color.default_card_color, R.color.default_card_fab_color);
            }

            //--------

            viewHolder.item_index.setText(orderItemDetailsItemDetails.getItemLocation());
            if ( orderItemDetailsItemDetails.getMy_local_status() == 0 || orderItemDetailsItemDetails.getMy_local_status() == 1 ) {
                viewHolder.action_type.setText("P");
            }else{
                viewHolder.action_type.setText("D");
            }
            viewHolder.card_title.setText(orderItemDetailsItemDetails.getDeliveryDetails().getSublocalityLevel1());
            if ( viewHolder.card_title.getText().toString().length() == 0 ){
                viewHolder.card_title.setText(orderItemDetailsItemDetails.getDeliveryDetails().getSublocalityLevel2());
            }

            //---------


            if ( orderItemDetailsItemDetails.getMy_local_status() == 0 || orderItemDetailsItemDetails.getMy_local_status() == 1 ) {
                viewHolder.name_1.setText(orderItemDetailsItemDetails.getChefDetails().getFullName());
                viewHolder.address_1.setText(CommonMethods.getStringFromStringArray(new String[]{orderItemDetailsItemDetails.getChefDetails().getFullAddress()}));//orderItemDetailsItemDetails.getDeliveryDetails().getPremise() + ",\n" + orderItemDetailsItemDetails.getDeliveryDetails().getAddressLine2());
                if ( orderItemDetailsItemDetails.getChefDetails().getLandMark() != null && orderItemDetailsItemDetails.getChefDetails().getLandMark().length() > 0 ){
                    viewHolder.land_mark_1.setText("Landmark : " + orderItemDetailsItemDetails.getChefDetails().getLandMark().toUpperCase());
                }else{  viewHolder.land_mark_1.setVisibility(View.GONE); }

                viewHolder.name_2.setText(orderItemDetailsItemDetails.getCustomerDetails().getCustomerName());
                viewHolder.address_2.setText(CommonMethods.getStringFromStringArray(new String[]{orderItemDetailsItemDetails.getCustomerDetails().getFullAddress()}));//orderItemDetailsItemDetails.getDeliveryDetails().getPremise() + ",\n" + orderItemDetailsItemDetails.getDeliveryDetails().getAddressLine2());
                if ( orderItemDetailsItemDetails.getCustomerDetails().getLandMark() != null && orderItemDetailsItemDetails.getCustomerDetails().getLandMark().length() > 0 ){
                    viewHolder.land_mark_2.setText("Landmark : " + orderItemDetailsItemDetails.getCustomerDetails().getLandMark().toUpperCase());
                }else{  viewHolder.land_mark_2.setVisibility(View.GONE); }

            }else{
                viewHolder.name_1.setText(orderItemDetailsItemDetails.getCustomerDetails().getCustomerName());
                viewHolder.address_1.setText(CommonMethods.getStringFromStringArray(new String[]{orderItemDetailsItemDetails.getCustomerDetails().getFullAddress()}));//orderItemDetailsItemDetails.getDeliveryDetails().getPremise() + ",\n" + orderItemDetailsItemDetails.getDeliveryDetails().getAddressLine2());
                if ( orderItemDetailsItemDetails.getCustomerDetails().getLandMark() != null && orderItemDetailsItemDetails.getCustomerDetails().getLandMark().length() > 0 ){
                    viewHolder.land_mark_1.setText("Landmark : " + orderItemDetailsItemDetails.getCustomerDetails().getLandMark().toUpperCase());
                }else{  viewHolder.land_mark_1.setVisibility(View.GONE); }

                viewHolder.name_2.setText(orderItemDetailsItemDetails.getChefDetails().getFullName());
                viewHolder.address_2.setText(CommonMethods.getStringFromStringArray(new String[]{orderItemDetailsItemDetails.getChefDetails().getFullAddress()}));//orderItemDetailsItemDetails.getDeliveryDetails().getPremise() + ",\n" + orderItemDetailsItemDetails.getDeliveryDetails().getAddressLine2());
                if ( orderItemDetailsItemDetails.getChefDetails().getLandMark() != null && orderItemDetailsItemDetails.getChefDetails().getLandMark().length() > 0 ){
                    viewHolder.land_mark_2.setText("Landmark : " + orderItemDetailsItemDetails.getChefDetails().getLandMark().toUpperCase());
                }else{  viewHolder.land_mark_2.setVisibility(View.GONE); }
            }

            viewHolder.item_cost.setText(SpecialCharacters.RS + " "+CommonMethods.getInDecimalFormat(orderItemDetailsItemDetails.getAmountOfItem()));
            viewHolder.order_cost.setText(SpecialCharacters.RS + " "+CommonMethods.getInDecimalFormat(orderItemDetailsItemDetails.getAmountOfOrder()) + " (" + orderItemDetailsItemDetails.getPaymentTypeString() + ")");

            float buyerOutstandingAmount = orderItemDetailsItemDetails.getDeliveryDetails().getBuyerDetails().getOutstandingAmount();
            if ( buyerOutstandingAmount < 0 ){
                viewHolder.buyer_outstanding.setText(SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(buyerOutstandingAmount) + " (Return to customer)");
            }else if  ( buyerOutstandingAmount > 0 ) {
                viewHolder.buyer_outstanding.setText(SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(buyerOutstandingAmount) + " (Collect from customer)");
            }else{
                viewHolder.buyerOutstandingDetails.setVisibility(View.GONE);
            }

            //---------

            viewHolder.item_name.setText(orderItemDetailsItemDetails.getDish().getDishDetails().getName());
            viewHolder.dabbas.setText(orderItemDetailsItemDetails.getFormattedDabbas());

            if ( orderItemDetailsItemDetails.getDish().getDishDetails().getType() == 1 ) {
                viewHolder.item_type_indicator.setImageResource(R.drawable.ic_non_veg);
            }else if ( orderItemDetailsItemDetails.getDish().getDishDetails().getType() == 2 ) {
                viewHolder.item_type_indicator.setImageResource(R.drawable.ic_jain2);
            }else {
                viewHolder.item_type_indicator.setImageResource(R.drawable.ic_veg);
            }

            viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(orderItemDetailsItemDetails.getQuantity()) );//+ " " + orderItemDetailsItemDetails.getQuantityUnit() + "s");
            if ( orderItemDetailsItemDetails.getQuantity() == 1 ){
                viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(orderItemDetailsItemDetails.getQuantity()) );//+ " " + orderItemDetailsItemDetails.getQuantityUnit());
            }

            if ( orderItemDetailsItemDetails.getDeliveryDetails().getSpecialInstructions().length() > 0 ) {
                viewHolder.special_instructions.setText(orderItemDetailsItemDetails.getDeliveryDetails().getSpecialInstructions());
            }else{
                viewHolder.special_instructions.setText("");
            }

            //--------

            // FOR FUTURE ORDERS
            viewHolder.delivery_time.setText("Time: " + orderItemDetailsItemDetails.getDeliverBy());

            //----------

            String orderStatusString = "";
            if ( orderItemDetailsItemDetails.getPickedUpByBb() != null ){
                String string = "PICK UP";
                if ( orderItemDetailsItemDetails.getStatusString().equalsIgnoreCase("DELIVERED") || orderItemDetailsItemDetails.getStatusString().equalsIgnoreCase("DELIVERING") || orderItemDetailsItemDetails.getStatusString().equalsIgnoreCase("PICKUP PENDING") ){
                    string = "PICKED UP";
                }
                if ( orderItemDetailsItemDetails.getPickedUpByBb().equals(localStorageData.getUserID()) ){
                    orderStatusString += string+" by : Me";
                }else{
                    orderStatusString += string+" by : "+ getBoyNameById(orderItemDetailsItemDetails.getPickedUpByBb()).toUpperCase();
                }
            }
            if ( orderItemDetailsItemDetails.getDeliveringBy() != null ){
                if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
                String currentStatus = "DELIVERING";
                if ( orderItemDetailsItemDetails.getStatusString().equalsIgnoreCase("DELIVERED")){
                    currentStatus = "DELIVERED";
                }
                if ( orderItemDetailsItemDetails.getDeliveringBy().equals(localStorageData.getUserID()) ){
                    orderStatusString += currentStatus+" by : Me";
                }else{
                    orderStatusString += currentStatus+" by : " + getBoyNameById(orderItemDetailsItemDetails.getDeliveringBy());
                }
            }/*else{
                if ( orderStatusString.length() > 0 && orderStatusString.toLowerCase().contains(orderItemDetailsItemDetails.getStatusString().toLowerCase()) == false ){
                    orderStatusString += "\n"+ orderItemDetailsItemDetails.getStatusString();
                }
            }*/
            if ( orderStatusString.length() > 0 ){
                viewHolder.order_status.setText(orderStatusString);
            }else{
                viewHolder.order_status.setText(orderItemDetailsItemDetails.getStatusString());
            }

            if ( orderStatusString.length() > 0 ){
                viewHolder.order_status.setText(orderStatusString);
            }else{
                viewHolder.order_status.setText(orderItemDetailsItemDetails.getStatus());
            }
            
            //---------

            if ( callback != null ){

                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(context, "Status: " + orderItemDetailsItemDetails.getStatus(), Toast.LENGTH_LONG).show();
                        if ( orderItemDetailsItemDetails.getStatus() == 0 || orderItemDetailsItemDetails.getStatus() == 7 ) {
                            callback.onReadyToPickup(position);
                        }
                        else if ( orderItemDetailsItemDetails.getStatus() == 1 ){
                            callback.onPickupTaken(position);
                        }
                        else if ( orderItemDetailsItemDetails.getStatus() == 2 ) {
                            callback.onReadyToDeliver(position);
                        }
                        else if ( orderItemDetailsItemDetails.getStatus() == 3 ) {
                            callback.onDelivered(position);
                        }
                    }
                });

                viewHolder.directions_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( orderItemDetailsItemDetails.getMy_local_status() == 0 || orderItemDetailsItemDetails.getMy_local_status() == 1 ) {
                            callback.onShowChefDirections(position);
                        }else{
                            callback.onShowCustomerDirections(position);
                        }
                    }
                });

                viewHolder.phone_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( orderItemDetailsItemDetails.getMy_local_status() == 0 || orderItemDetailsItemDetails.getMy_local_status() == 1 ) {
                            callback.onCallChef(position);
                        }else{
                            callback.onCallCustomer(position);
                        }
                    }
                });

                viewHolder.directions_2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( orderItemDetailsItemDetails.getMy_local_status() == 0 || orderItemDetailsItemDetails.getMy_local_status() == 1 ) {
                            callback.onShowCustomerDirections(position);
                        }else{
                            callback.onShowChefDirections(position);
                        }
                    }
                });

                viewHolder.phone_2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( orderItemDetailsItemDetails.getMy_local_status() == 0 || orderItemDetailsItemDetails.getMy_local_status() == 1 ) {
                            callback.onCallCustomer(position);
                        }else{
                            callback.onCallChef(position);
                        }
                    }
                });

                viewHolder.item_index.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onShowOrderDetails(position);
                    }
                });
            }

            //----------
        }

    }

    private void setColors(ViewHolder viewHolder,int normalColor, int fabColor){
        fabColor = R.color.default_card_fab_color;
        viewHolder.container.setCardBackgroundColor(context.getResources().getColor(normalColor));
        viewHolder.phone_1.setColorNormalResId(fabColor);
        viewHolder.phone_1.setColorPressedResId(fabColor);
        viewHolder.directions_1.setColorNormalResId(fabColor);
        viewHolder.directions_1.setColorPressedResId(fabColor);
        viewHolder.phone_2.setColorNormalResId(fabColor);
        viewHolder.phone_2.setColorPressedResId(fabColor);
        viewHolder.directions_2.setColorNormalResId(fabColor);
        viewHolder.directions_2.setColorPressedResId(fabColor);
    }

    //--------------

    private String getBoyNameById(String pickedUpByBb) {
        for (User boy : deliveryBoys)
            if (boy.get_id().equals(pickedUpByBb))
                return boy.getName();
        return "Unknown";
    }

    /*public void updateQuantity(int position, double newQuantity) {
        OrderItemDetails orderItemDetails = itemsList.get(position);
        double newItemPrice = (orderItemDetails.getAmountOfItem()*newQuantity)/ orderItemDetails.getQuantity();
        double newOrderPrice = (orderItemDetails.getAmountOfOrder()- orderItemDetails.getAmountOfItem())+newItemPrice;

        orderItemDetails.setQuantity(newQuantity);
        orderItemDetails.setAmountOfItem(newItemPrice);
        orderItemDetails.setAmountOfOrder(newOrderPrice);
        itemsList.set(position, orderItemDetails);

        notifyDataSetChanged();
    }

    public void updateDeliveringBy(int position, User deliveryBoy){
        itemsList.get(position).setDeliveringBy(deliveryBoy);
        notifyDataSetChanged();
    }
    public void markAsAcknowledged(int position) {
        itemsList.get(position).setStatus(7);
        notifyDataSetChanged();
    }

    public void markAsProcessing(int position) {
        itemsList.get(position).setStatus(1);
        notifyDataSetChanged();
    }

    public void markAsPickupPending(int position) {
        itemsList.get(position).setStatus(2);
        notifyDataSetChanged();
    }*/

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<OrderItemDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<OrderItemDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public OrderItemDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    /*public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).get_id().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }*/

    public List<OrderItemDetails> getAllItems(){
        return itemsList;
    }


}
