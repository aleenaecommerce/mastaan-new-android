package in.gocibo.logistics.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import in.gocibo.logistics.R;
import in.gocibo.logistics.models.OrderItemDetails;
import in.gocibo.logistics.models.User;
import in.gocibo.logistics.constants.Constants;

/**
 * Created by Naresh Katta on 11/10/2015.
 */
public class PickOrDeliverItemsListAdapter extends ArrayAdapter<OrderItemDetails> {
    List<OrderItemDetails> itemList;
    Callback callback;
    LayoutInflater inflater;
    Context context;
    int resourceId;
    LatLng current_location = null;
    List<User> deliveryBoys;
    boolean isColored;

    public PickOrDeliverItemsListAdapter(Context context, int resourceId, List<OrderItemDetails> itemList, Callback callback) {
        this(context, resourceId, itemList, null, callback);
    }

    public PickOrDeliverItemsListAdapter(Context context, int resourceId, List<OrderItemDetails> itemList, List<User> deliveryBoys, Callback callback) {
        this(context, resourceId, itemList, deliveryBoys, false, callback);
    }

    public PickOrDeliverItemsListAdapter(Context context, int resourceId, List<OrderItemDetails> itemList, List<User> deliveryBoys, boolean isColored, Callback callback) {
        super(context, resourceId);
        this.itemList = itemList;
        this.callback = callback;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.resourceId = R.layout.view_item_card_2;
        this.deliveryBoys = deliveryBoys;
        this.isColored = isColored;
    }

    public List<OrderItemDetails> getItemList() {
        return itemList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(resourceId, null);
        //0,1 pickup
        //2,3 deliver

        CardView item_card = (CardView) findViewById(convertView, R.id.item_card);
        TextView cardHeader = (TextView) findViewById(convertView, R.id.cardHeader);
        TextView area = (TextView) findViewById(convertView, R.id.area);


        TextView name_1 = (TextView) findViewById(convertView, R.id.name_1);
        TextView location_1 = (TextView) findViewById(convertView, R.id.location_1);
        TextView land_mark_1 = (TextView) findViewById(convertView, R.id.land_mark_1);

        TextView name_2 = (TextView) findViewById(convertView, R.id.name_2);
        TextView location_2 = (TextView) findViewById(convertView, R.id.location_2);
        TextView land_mark_2 = (TextView) findViewById(convertView, R.id.land_mark_2);

        //------ Food Details -----------

        TextView item_name = (TextView) findViewById(convertView, R.id.item_name);
        TextView number_of_portions = (TextView) findViewById(convertView, R.id.number_of_portions);
        TextView dabbas = (TextView) findViewById(convertView, R.id.dabbas);
        TextView total_amount_of_order = (TextView) findViewById(convertView, R.id.total_amount_of_order);
        ImageView item_type = (ImageView) findViewById(convertView, R.id.item_type);

        TextView time = (TextView) findViewById(convertView, R.id.time);

        //fabs

        FloatingActionButton phone_number_1 = (FloatingActionButton) findViewById(convertView, R.id.phone_number_1);
        FloatingActionButton phone_number_2 = (FloatingActionButton) findViewById(convertView, R.id.phone_number_2);
        FloatingActionButton directions_1 = (FloatingActionButton) findViewById(convertView, R.id.directions_1);
        FloatingActionButton phone_number_3 = (FloatingActionButton) findViewById(convertView, R.id.phone_number_3);
        FloatingActionButton phone_number_4 = (FloatingActionButton) findViewById(convertView, R.id.phone_number_4);
        FloatingActionButton directions_2 = (FloatingActionButton) findViewById(convertView, R.id.directions_2);

        View itemSelector = findViewById(convertView, R.id.itemSelector);

        final OrderItemDetails orderItemDetails = itemList.get(position);

        item_name.setText(orderItemDetails.getDish().getDishDetails().getName());
        number_of_portions.setText(orderItemDetails.getQuantity() + "");
        dabbas.setText(orderItemDetails.getFormattedDabbas());
        total_amount_of_order.setText(orderItemDetails.getOrderAmountAndPaymentType());
        String statusText = "";
        if (deliveryBoys != null) {
            if (orderItemDetails.getPickedUpByBb() != null)
                statusText = statusText + "Pickup: " + getBoyNameById(orderItemDetails.getPickedUpByBb());
            if (orderItemDetails.getDeliveringBy() != null)
                statusText = (statusText.length() > 0 ? statusText + "\n" : "") + "Delivery: " + getBoyNameById(orderItemDetails.getDeliveringBy());
        }
        if (statusText.length() > 0) {
            findViewById(convertView, R.id.status_holder).setVisibility(View.VISIBLE);
            TextView status = (TextView) findViewById(convertView, R.id.status);
            status.setText(statusText);
        } else
            findViewById(convertView, R.id.status_holder).setVisibility(View.GONE);
        time.setText(orderItemDetails.getDeliverBy());
        if (orderItemDetails.getDish().getDishDetails().getType() == 0)
            item_type.setImageResource(R.drawable.ic_veg);
        else if (orderItemDetails.getDish().getDishDetails().getType() == 1)
            item_type.setImageResource(R.drawable.ic_non_veg);
        else
            item_type.setImageResource(R.drawable.ic_jain2);

        if (orderItemDetails.getMy_local_status() == 0 || orderItemDetails.getMy_local_status() == 1) {
            cardHeader.setText(" P ");
            item_card.setCardBackgroundColor(Color.parseColor("#795548"));
            if (orderItemDetails.getMy_local_status() == 1)
                item_card.setCardBackgroundColor(Color.parseColor("#696558"));
            phone_number_1.setColorNormal(Color.parseColor("#FF4081"));
            phone_number_2.setColorNormal(Color.parseColor("#FF4081"));
            directions_1.setColorNormal(Color.parseColor("#FF4081"));
            phone_number_3.setColorNormal(Color.parseColor("#FF4081"));
            phone_number_4.setColorNormal(Color.parseColor("#FF4081"));
            directions_2.setColorNormal(Color.parseColor("#FF4081"));
            area.setText(orderItemDetails.getChefDetails().getArea());
            name_1.setText("Chef " + orderItemDetails.getChefDetails().getFullName());
            location_1.setText(orderItemDetails.getChefDetails().getAddressLine1() + "\n" + orderItemDetails.getChefDetails().getAddressLine2());
            land_mark_1.setText(orderItemDetails.getChefDetails().getLandMark());

            name_2.setText("Customer " + orderItemDetails.getCustomerDetails().getCustomerName());
            location_2.setText(orderItemDetails.getCustomerDetails().getAddressLine1() + "\n" + orderItemDetails.getCustomerDetails().getAddressLine2());
            land_mark_2.setText(orderItemDetails.getCustomerDetails().getLandMark());

            if (orderItemDetails.getChefDetails().getAlternativeMobileNumber() == null) {
                phone_number_2.setVisibility(View.GONE);
            }
            phone_number_4.setVisibility(View.GONE);
            phone_number_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onPhoneNumberClicked(position, orderItemDetails.getChefDetails().getMobileNumber());
                }
            });
            phone_number_2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onPhoneNumberClicked(position, orderItemDetails.getChefDetails().getAlternativeMobileNumber());
                }
            });
            phone_number_3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onPhoneNumberClicked(position, orderItemDetails.getCustomerDetails().getMobileNumber());
                }
            });
            directions_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onDirectionsClick(position, orderItemDetails.getChefName(), orderItemDetails.getChefDetails().getArea(), orderItemDetails.getChefDetails().getLatLng());
                }
            });
            directions_2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onDirectionsClick(position, orderItemDetails.getCustomerDetails().getCustomerName(), orderItemDetails.getCustomerDetails().getArea(), orderItemDetails.getCustomerDetails().getLatLng());
                }
            });
        }
        if (orderItemDetails.getMy_local_status() == 2 || orderItemDetails.getMy_local_status() == 3) {
            cardHeader.setText(" D ");
            item_card.setCardBackgroundColor(Color.parseColor("#009688"));
            if (orderItemDetails.getMy_local_status() == 3)
                item_card.setCardBackgroundColor(Color.parseColor("#000000"));

            phone_number_1.setColorNormal(Color.parseColor("#FFC107"));
            phone_number_2.setColorNormal(Color.parseColor("#FFC107"));
            directions_1.setColorNormal(Color.parseColor("#FFC107"));
            phone_number_3.setColorNormal(Color.parseColor("#FFC107"));
            phone_number_4.setColorNormal(Color.parseColor("#FFC107"));
            directions_2.setColorNormal(Color.parseColor("#FFC107"));

            area.setText(orderItemDetails.getCustomerDetails().getArea());
            name_1.setText("Customer " + orderItemDetails.getCustomerDetails().getCustomerName());
            location_1.setText(orderItemDetails.getCustomerDetails().getAddressLine1() + "\n" + orderItemDetails.getCustomerDetails().getAddressLine2());
            land_mark_1.setText(orderItemDetails.getCustomerDetails().getLandMark());

            name_2.setText("Chef " + orderItemDetails.getChefDetails().getFullName());
            location_2.setText(orderItemDetails.getChefDetails().getAddressLine1() + "\n" + orderItemDetails.getChefDetails().getAddressLine2());
            land_mark_2.setText(orderItemDetails.getChefDetails().getLandMark());
            if (orderItemDetails.getChefDetails().getAlternativeMobileNumber() == null) {
                phone_number_4.setVisibility(View.GONE);
            }
            phone_number_2.setVisibility(View.GONE);
            phone_number_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onPhoneNumberClicked(position, orderItemDetails.getCustomerDetails().getMobileNumber());
                }
            });
            phone_number_3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onPhoneNumberClicked(position, orderItemDetails.getChefDetails().getMobileNumber());
                }
            });
            phone_number_4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onPhoneNumberClicked(position, orderItemDetails.getChefDetails().getAlternativeMobileNumber());
                }
            });
            directions_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onDirectionsClick(position, orderItemDetails.getCustomerDetails().getCustomerName(), orderItemDetails.getCustomerDetails().getArea(), orderItemDetails.getCustomerDetails().getLatLng());
                }
            });
            directions_2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null)
                        callback.onDirectionsClick(position, orderItemDetails.getChefName(), orderItemDetails.getChefDetails().getArea(), orderItemDetails.getChefDetails().getLatLng());
                }
            });
        }
        if (isColored) {
            int orderItemStatus = orderItemDetails.getStatus();
            switch (orderItemStatus) {
                case 0:
                case 7:
                    setCardColors(convertView,
                            context.getResources().getColor(R.color.color_seafood),
                            R.color.color_seafood_fab,
                            R.color.color_seafood);
                    break;
                case 1:
                case 2:
                    setCardColors(convertView,
                            context.getResources().getColor(R.color.color_processing),
                            R.color.background_material_dark,
                            R.color.color_processing);
                    break;
                case 5:
                case 6:
                    setCardColors(convertView,
                            context.getResources().getColor(R.color.background_material_dark),
                            R.color.color_processing,
                            R.color.background_material_dark);
                    break;
                case 4:
                    setCardColors(convertView,
                            context.getResources().getColor(R.color.order_delivered_color),
                            R.color.order_delivered_color_fab,
                            R.color.order_delivered_color);
                    break;
                case 3:
                    setCardColors(convertView,
                            context.getResources().getColor(R.color.color_mutton),
                            R.color.color_mutton_fab,
                            R.color.color_mutton);
                    break;
            }
        }
        itemSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null)
                    switch (orderItemDetails.getMy_local_status()) {
                        case 0:
                            callback.onReadyToPickup(position);
                            break;
                        case 1:
                            callback.onPickupTaken(position);
                            break;
                        case 2:
                            callback.onReadyToDeliver(position);
                            break;
                        case 3:
                            callback.onDelivered(position);
                            break;
                    }
            }
        });
        return convertView;
    }

    private void setCardColors(View convertView,
                               int color, int fabColorNormal, int fabColorPressed) {
        ((CardView) convertView.findViewById(R.id.item_card)).setCardBackgroundColor(color);
        ((FloatingActionButton) convertView.findViewById(R.id.phone_number_1)).setColorNormalResId(fabColorNormal);
        ((FloatingActionButton) convertView.findViewById(R.id.phone_number_2)).setColorNormalResId(fabColorNormal);
        ((FloatingActionButton) convertView.findViewById(R.id.directions_1)).setColorNormalResId(fabColorNormal);
        ((FloatingActionButton) convertView.findViewById(R.id.phone_number_3)).setColorNormalResId(fabColorNormal);
        ((FloatingActionButton) convertView.findViewById(R.id.phone_number_4)).setColorNormalResId(fabColorNormal);
        ((FloatingActionButton) convertView.findViewById(R.id.directions_2)).setColorNormalResId(fabColorNormal);
        ((FloatingActionButton) convertView.findViewById(R.id.phone_number_1)).setColorPressedResId(fabColorPressed);
        ((FloatingActionButton) convertView.findViewById(R.id.phone_number_2)).setColorPressedResId(fabColorPressed);
        ((FloatingActionButton) convertView.findViewById(R.id.directions_1)).setColorPressedResId(fabColorPressed);
        ((FloatingActionButton) convertView.findViewById(R.id.phone_number_3)).setColorPressedResId(fabColorPressed);
        ((FloatingActionButton) convertView.findViewById(R.id.phone_number_4)).setColorPressedResId(fabColorPressed);
        ((FloatingActionButton) convertView.findViewById(R.id.directions_2)).setColorPressedResId(fabColorPressed);
    }

    private String getBoyNameById(String pickedUpByBb) {
        for (User boy : deliveryBoys)
            if (boy.get_id().equals(pickedUpByBb))
                return boy.getName();
        return "Unknown";
    }

    final View findViewById(View view, int id) {
        return view.findViewById(id);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public OrderItemDetails getItem(int position) {
        return itemList.get(position);
    }

    public void addItem(OrderItemDetails details) {
        itemList.add(details);
        notifyDataSetChanged();
    }

    @Override
    public void clear() {
        super.clear();
        itemList.clear();
    }

    public void addItems(List<OrderItemDetails> addressDetailsList) {
        if (addressDetailsList == null || addressDetailsList.size() <= 0)
            return;
        for (OrderItemDetails details : addressDetailsList) {
            if (current_location != null)
                details.setDistance(LatLngMethods.getDistanceBetweenLatLng(current_location, details.getLatLng()));
            itemList.add(details);
        }
        sortItems();
        notifyDataSetChanged();
    }

    private void sortItems() {
        HashMap<String, List<OrderItemDetails>> map = new HashMap<>();// hash map for grouping items with same slot
        for (OrderItemDetails details : itemList) { // itemList is list containing all items from server
            if (!map.containsKey(details.getDeliverBy().toUpperCase())) {    //checks weather time is already existing or not #toUppercase to remove case sensitivity
                map.put(details.getDeliverBy().toUpperCase(), new ArrayList<OrderItemDetails>()); // creates new key value pair for new time slot
                map.get(details.getDeliverBy().toUpperCase()).add(details); //adds this item to list of items with same time slot
            } else {
                map.get(details.getDeliverBy().toUpperCase()).add(details);
            }
        }
        Set<String> keySet = map.keySet();// extracts all time slots in hash map
        List<String> dummyList = new ArrayList<>(keySet);
        Collections.sort(dummyList, new Comparator<String>() { // java's defaults sorting technique to sort collection of data
            @Override
            public int compare(String lhs, String rhs) {
                Log.d(Constants.LOG_TAG, lhs + " " + rhs);
                String rhTo = rhs.substring(rhs.lastIndexOf("TO") + 2).trim();
                int rhH = Integer.parseInt(rhTo.substring(0, rhTo.indexOf(":")).trim());
                int rhM = Integer.parseInt(rhTo.substring(rhTo.indexOf(":") + 1, rhTo.length() - 2).trim());
                String lhTo = lhs.substring(lhs.indexOf("TO") + 2).trim();
                int lhH = Integer.parseInt(lhTo.substring(0, lhTo.indexOf(":")).trim());
                if (lhH == 12)
                    lhH = 0; // converts 12 pm to 0 pm (for comparison)
                int lhM = Integer.parseInt(lhTo.substring(lhTo.indexOf(":") + 1, lhTo.length() - 2).trim());
                if (lhH == rhH) {
                    if (lhM == rhM) return 0;
                    else if (lhM > rhM) return 1;
                    else return -1;
                } else if (lhH > rhH) return 1;
                else return -1;
            }
        });
        this.itemList.clear();// clears items before adding new sorted items
        for (String s : dummyList) {
            List<OrderItemDetails> itemList = map.get(s);
            if (itemList != null) {
                Collections.sort(itemList, new Comparator<OrderItemDetails>() { // distance comparison for items with same time slot s
                    @Override
                    public int compare(OrderItemDetails lhs, OrderItemDetails rhs) {
                        return lhs.getDistance() == rhs.getDistance() ? 0 : (lhs.getDistance() > rhs.getDistance() ? 1 : -1);
                    }
                });
                for (OrderItemDetails details : itemList)
                    this.itemList.add(details);
            }
        }
    }

    public void setCurrentLocation(LatLng current_location) {
        this.current_location = current_location;
        for (OrderItemDetails details : itemList) {
            if (current_location != null)
                details.setDistance(LatLngMethods.getDistanceBetweenLatLng(current_location, details.getLatLng()));
        }
        sortItems();
        notifyDataSetChanged();
    }

    public void remove(int position) {
        itemList.remove(position);
        notifyDataSetChanged();
    }

    public void setReadyToPickup(int position) {
        itemList.get(position).setStatus(1);
        notifyDataSetChanged();
    }

    public void setPickupTaken(int position) {
    }

    public void setPickupCancelled(int position) {
        itemList.get(position).setStatus(0);
        notifyDataSetChanged();
    }

    public void convertToRetrieval(int position) {
        itemList.remove(position);
        notifyDataSetChanged();
    }

    public void setReadyToDeliver(int position) {
        itemList.get(position).setStatus(3);
        notifyDataSetChanged();
    }

    public void setRetrieved(int position, List<String> dabbas) {
        itemList.remove(position);
        notifyDataSetChanged();
    }


    public interface Callback {
        void onDirectionsClick(int position, String name, String area, LatLng location);

        void onReadyToPickup(int position);

        void onPickupTaken(int position);

        void onReadyToDeliver(int position);

        void onDelivered(int position);

        void onPhoneNumberClicked(int position, String number);
    }
}

