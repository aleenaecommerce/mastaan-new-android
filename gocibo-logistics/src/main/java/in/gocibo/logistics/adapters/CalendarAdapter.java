package in.gocibo.logistics.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import in.gocibo.logistics.R;
import in.gocibo.logistics.models.UserDay;

/**
 * Created by Naresh Katta on 31/10/2015.
 */
public class CalendarAdapter extends ArrayAdapter<UserDay> {
    int currentViewingMonth;
    Context context;
    // days with events
    private HashSet<Date> eventDays;
    // for view inflation
    private LayoutInflater inflater;

    public interface OnItemClickListener {
        void onClick(int position);
    }

    OnItemClickListener onItemClickListener;

    public void setOnClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public CalendarAdapter(Context context, ArrayList<UserDay> days, HashSet<Date> eventDays, int currentViewingMonth) {
        super(context, R.layout.view_calender_day, days);
        this.context = context;
        this.eventDays = eventDays;
        inflater = LayoutInflater.from(context);
        this.currentViewingMonth = currentViewingMonth;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        // day in question
        Date date = getItem(position).getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        // today
        Date today = new Date();
        //Today calender

        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.setTime(today);
        // inflate item if it does not exist yet
        if (view == null)
            view = inflater.inflate(R.layout.view_calender_day, parent, false);

        // if this day has an event, specify event image
        view.setBackgroundResource(0);
        if (eventDays != null) {
            for (Date eventDate : eventDays) {
                Calendar eventCalendar = Calendar.getInstance();
                eventCalendar.setTime(eventDate);
                if (eventCalendar.get(Calendar.DAY_OF_MONTH) == day &&
                        eventCalendar.get(Calendar.MONTH) == month &&
                        eventCalendar.get(Calendar.YEAR) == year) {
                    // mark this day for event
                    view.setBackgroundResource(R.drawable.reminder);
                    break;
                }
            }
        }

        // clear styling
        ((TextView) view.findViewById(R.id.date)).setTypeface(null, Typeface.NORMAL);
        ((TextView) view.findViewById(R.id.date)).setTextColor(Color.WHITE);
        view.findViewById(R.id.number_of_km).setVisibility(View.VISIBLE);
        if (month != currentViewingMonth) {
            // if this day is outside current month, grey it out
            ((TextView) view.findViewById(R.id.date)).setTextColor(context.getResources().getColor(R.color.greyed_out));
            view.findViewById(R.id.number_of_km).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.button).setEnabled(false);
        } else if (day == todayCalendar.get(Calendar.DAY_OF_MONTH) && month == todayCalendar.get(Calendar.MONTH) && year == todayCalendar.get(Calendar.YEAR)) {
            // if it is today, set it to blue/bold
            ((TextView) view.findViewById(R.id.date)).setTypeface(null, Typeface.BOLD);
            ((TextView) view.findViewById(R.id.date)).setTextColor(context.getResources().getColor(R.color.today));
//            getItem(position).setKm("99.9");
        }
        if (day > todayCalendar.get(Calendar.DAY_OF_MONTH) && month >= todayCalendar.get(Calendar.MONTH) && year >= todayCalendar.get(Calendar.YEAR)) {
            // if it is future day, set it to blue/bold
            view.findViewById(R.id.number_of_km).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.button).setEnabled(false);
        }
        if (month > todayCalendar.get(Calendar.MONTH) && year >= todayCalendar.get(Calendar.YEAR)) {
            // if it is future day, set it to blue/bold
            view.findViewById(R.id.number_of_km).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.button).setEnabled(false);
        }
        if (year > todayCalendar.get(Calendar.YEAR)) {
            // if it is future day, set it to blue/bold
            view.findViewById(R.id.number_of_km).setVisibility(View.INVISIBLE);
            view.findViewById(R.id.button).setEnabled(false);
        }
        if (getItem(position).getKm().equalsIgnoreCase("0"))
            view.findViewById(R.id.button).setEnabled(false);

        // set text
        ((TextView) view.findViewById(R.id.date)).setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        ((TextView) view.findViewById(R.id.number_of_km)).setText(getItem(position).getKm());
        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onClick(position);
            }
        });
        return view;
    }
}
