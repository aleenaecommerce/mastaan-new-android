package in.gocibo.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.models.CustomPlace;

/**
 * Created by Naresh-Crypsis on 06-01-2016.
 */
public class PlaceChooserAdapter extends ArrayAdapter {
    Context context;
    List<? extends CustomPlace> itemList;
    ItemClickListener mItemClickListener;

    public PlaceChooserAdapter(Context context, List<? extends CustomPlace> itemList, ItemClickListener mItemClickListener) {
        super(context, R.layout.view_chef_item, itemList);
        this.context = context;
        this.itemList = itemList;
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_chef_item, parent, false);
        }
        TextView name = (TextView) convertView.findViewById(R.id.name);
//        View selector = convertView.findViewById(R.id.item_selector);
        name.setText(itemList.get(position).getName());
        /*if (position % 2 == 0)
            name.setBackgroundColor(Color.parseColor("#99999999"));
        else
            name.setBackgroundColor(Color.parseColor("#ffffffff"));*/
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null)
                    mItemClickListener.onSelect(position, itemList.get(position));
            }
        });
        if (position == itemList.size() - 1)
            convertView.findViewById(R.id.divider).setVisibility(View.GONE);
        else
            convertView.findViewById(R.id.divider).setVisibility(View.VISIBLE);
        return convertView;
    }


    public interface ItemClickListener {
        void onSelect(int position, CustomPlace place);
    }
}
