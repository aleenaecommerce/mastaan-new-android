package in.gocibo.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.melnykov.fab.FloatingActionButton;

import java.util.List;

import in.gocibo.logistics.R;
import in.gocibo.logistics.models.UploadDetails;

public class UploadsAdapter extends RecyclerView.Adapter<UploadsAdapter.ViewHolder> {

    Context context;
    List<UploadDetails> itemsList;
    Callback callback;

    ViewGroup parent;

    public UploadsAdapter(Context context, List<UploadDetails> chefsList, Callback callback) {
        this.context = context;
        this.itemsList = chefsList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public UploadsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_upload, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final UploadDetails uploadDetails = itemsList.get(position);

        if (uploadDetails != null) {

            viewHolder.location_header.setText(uploadDetails.getChefDetails().getArea());
            if (viewHolder.location_header.getText().toString().length() == 0) {
                viewHolder.location_header.setText(uploadDetails.getChefDetails().getSublocalityLevel1());
            }

            viewHolder.item_name.setText(uploadDetails.getName());
            viewHolder.item_price.setText(SpecialCharacters.RS + " " + uploadDetails.getProfitPrice());

            if (uploadDetails.getType().equalsIgnoreCase("veg")) {
                viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_veg);
                viewHolder.item_card.setCardBackgroundColor(context.getResources().getColor(R.color.veg_color));
            } else if (uploadDetails.getType().equalsIgnoreCase("non-veg")) {
                viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_non_veg);
                viewHolder.item_card.setCardBackgroundColor(context.getResources().getColor(R.color.nonveg_color));
            } else if (uploadDetails.getType().equalsIgnoreCase("jain")) {
                viewHolder.item_type_indicator.setBackgroundResource(R.drawable.ic_jain);
                viewHolder.item_card.setCardBackgroundColor(context.getResources().getColor(R.color.jain_color));
            }

            viewHolder.item_quantity.setText("" + uploadDetails.getQuantity());
            if (uploadDetails.getQuantityType().equalsIgnoreCase("n")) {
                viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(uploadDetails.getQuantity()) + " Servings");
                if (uploadDetails.getQuantity() == 1) {
                    viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(uploadDetails.getQuantity()) + " Serving");
                }
            } else if (uploadDetails.getQuantityType().equalsIgnoreCase("w")) {
                viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(uploadDetails.getQuantity()) + " Kgs");
                if (uploadDetails.getQuantity() == 1) {
                    viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(uploadDetails.getQuantity()) + " Kg");
                }
            } else if (uploadDetails.getQuantityType().equalsIgnoreCase("v")) {
                viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(uploadDetails.getQuantity()) + " Litres");
                if (uploadDetails.getQuantity() == 1) {
                    viewHolder.item_quantity.setText(CommonMethods.getInDecimalFormat(uploadDetails.getQuantity()) + " Litre");
                }
            }

            viewHolder.item_availability.setText(DateMethods.getTimeIn12HrFormat(DateMethods.getReadableDateFromUTC(uploadDetails.getAvailabilityStartTime())) + " to " + DateMethods.getTimeIn12HrFormat(DateMethods.getReadableDateFromUTC(uploadDetails.getAvailabilityEndTime())));

            viewHolder.chef_name.setText("Chef: " + uploadDetails.getChefFullName());
            viewHolder.chef_address.setText(uploadDetails.getChefAddress());

            if (uploadDetails.getLinkedDabbas() != null && uploadDetails.getLinkedDabbas().size() > 0) {
                viewHolder.linkedDabbasDetails.setVisibility(View.VISIBLE);
                viewHolder.linked_dabbas.setText(CommonMethods.getStringFromStringList(uploadDetails.getLinkedDabbas()));
            } else {
                viewHolder.linkedDabbasDetails.setVisibility(View.GONE);
            }

            if (callback != null) {

                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onItemClick(position);
                    }
                });
                viewHolder.callChef.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onCallChef(position);
                    }
                });

                viewHolder.chefDirections.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onShowChefDirections(position);
                    }
                });
            }

        }
    }

    public void setItems(List<UploadDetails> items) {
        itemsList.clear();
        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<UploadDetails> items) {
        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    //---------

    public List<UploadDetails> getAllItems() {
        return itemsList;
    }

    public int getItemsCount() {
        return itemsList.size();
    }

    public UploadDetails getItem(int position) {
        if (position >= 0 && position < itemsList.size()) {
            return itemsList.get(position);
        }
        return null;
    }

    public void updateItem(int position, UploadDetails uploadDetails) {
        if (position >= 0 && position < itemsList.size()) {
            itemsList.set(position, uploadDetails);
            notifyDataSetChanged();
        }
    }

    public interface Callback {
        void onItemClick(int position);

        void onCallChef(int position);

        void onShowChefDirections(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView item_card;
        LinearLayout itemSelector;

        TextView location_header;
        TextView item_name;
        ImageView item_type_indicator;
        TextView item_quantity;
        TextView item_price;
        TextView item_availability;

        TextView chef_name;
        TextView chef_address;

        LinearLayout linkedDabbasDetails;
        TextView linked_dabbas;

        FloatingActionButton callChef;
        FloatingActionButton chefDirections;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            item_card = itemLayoutView.findViewById(R.id.item_card);
            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);

            location_header = itemLayoutView.findViewById(R.id.location_header);
            item_name = itemLayoutView.findViewById(R.id.item_name);
            item_type_indicator = itemLayoutView.findViewById(R.id.item_type_indicator);
            item_quantity = itemLayoutView.findViewById(R.id.item_quantity);
            item_price = itemLayoutView.findViewById(R.id.item_price);
            item_availability = itemLayoutView.findViewById(R.id.item_availability);

            chef_name = itemLayoutView.findViewById(R.id.chef_name);
            chef_address = itemLayoutView.findViewById(R.id.chef_address);

            linkedDabbasDetails = itemLayoutView.findViewById(R.id.linkedDabbasDetails);
            linked_dabbas = itemLayoutView.findViewById(R.id.linked_dabbas);

            callChef = itemLayoutView.findViewById(R.id.callChef);
            chefDirections = itemLayoutView.findViewById(R.id.chefDirections);
        }
    }

}