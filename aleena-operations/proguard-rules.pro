# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and orderItemDetails by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# =========================

-keepattributes SourceFile,LineNumberTable
-keep class * extends android.app.Application
-keep class * implements android.os.Parcelable { public static final android.os.Parcelable$Creator *; }

# ======= COMMONLIB =======

-keepclassmembers class com.aleena.common.models.** { *; }
-keepclassmembers class com.aleena.common.location.model.** { *; }
#-keep class com.aleena.common.** { *; }

# === ALEENA OPERATIONS ===

-keepclassmembers class com.aleenaecommerce.operations.models.** { *; }
-keepclassmembers class com.aleenaecommerce.operations.backend.models.** { *; }
#-keep class com.aleenaecommerce.operations.** { *; }

# ======= APPCOMPAT =======

-keep class android.support.v7.widget.SearchView { *; }

# ======== RAZORPAY =======

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

-keepattributes JavascriptInterface
-keepattributes *Annotation*

-dontwarn com.razorpay.**
-keep class com.razorpay.** {*;}

-optimizations !method/inlining/*

-keepclasseswithmembers class * {
  public void onPayment*(...);
}

# ===== RETROFIT & PICASSO ======

-keep class retrofit.** { *; }
-dontwarn retrofit.**
-dontwarn okio.**
-dontwarn com.squareup.okhttp.**
-dontwarn com.squareup.okhttp.**

# ========= REY WIDGETS =======

-keep class com.rey.material.** { *; }
-dontwarn com.rey.material.**

# ========= JACKSON LIB =======

-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}
-keepnames class com.fasterxml.jackson.** { *; }
-dontwarn com.fasterxml.jackson.databind.**