package com.aleenaecommerce.operations.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.core.content.IntentCompat;

import com.aleenaecommerce.operations.localdata.LocalStorageData;
import com.aleenaecommerce.operations.services.OperationsService;

/**
 * Created by venkatesh on 21/6/16.
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        new LocalStorageData(context).setServerTime("");    // To Load Bootstrap in service
        Intent operationsService = new Intent(context, OperationsService.class);
        context.startService(operationsService);

//        Intent launchAct = new Intent(context, LaunchActivity.class);
//        ComponentName componentName = launchAct.getComponent();
//        Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
//        context.startActivity(mainIntent);

    }
}