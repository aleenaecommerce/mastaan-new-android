package com.aleenaecommerce.operations.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.models.WifiDetails;
import com.aleenaecommerce.operations.activities.AlertActivity;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.localdata.LocalStorageData;
import com.aleenaecommerce.operations.methods.AttendanceMethods;
import com.aleenaecommerce.operations.methods.ComparisionMethods;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.services.OperationsService;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 21/6/16.
 */
public class WifiReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent operationsService = new Intent(context, OperationsService.class);
        context.startService(operationsService);

        try{
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if ( networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI ) {
                WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);

                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                WifiDetails wifiDetails = new WifiDetails(wifiInfo.getBSSID(), wifiInfo.getSSID().replace("\"", ""));

                Intent wifiReceiver = new Intent(Constants.WIFI_RECEIVER);
                wifiReceiver.putExtra("status", true);
                wifiReceiver.putExtra("msg", "connected");
                wifiReceiver.putExtra("wifi_details", new Gson().toJson(wifiDetails));
                context.sendBroadcast(wifiReceiver);

                // CHECKING ATTENDANCE
                checkAttendance(context, wifiDetails);
                //BroadcastReceiversMethods.updatePinnedNotification(context, true);

                //Toast.makeText(context, "WIFI Connected: BSSID: "+wifiDetails.getBSSID()+", SSID: "+wifiDetails.getSSID(), Toast.LENGTH_LONG).show();
                Log.d(Constants.LOG_TAG, "WIFI Connected: BSSID: " + wifiDetails.getBSSID() + ", SSID: " + wifiDetails.getSSID());
            }
            else {
                Intent wifiReceiver = new Intent(Constants.WIFI_RECEIVER);
                wifiReceiver.putExtra("status", false);
                wifiReceiver.putExtra("msg", "disconnected");
                context.sendBroadcast(wifiReceiver);

                //Toast.makeText(context, "WIFI DisConnected", Toast.LENGTH_LONG).show();
                Log.d(Constants.LOG_TAG, "WIFI DisConnected");
            }

        }catch (Exception e){e.printStackTrace();}

    }


    private void checkAttendance(final Context context, final WifiDetails connectedWifiDetails){
        final LocalStorageData localStorageData = new LocalStorageData(context);

        new AsyncTask<Void, Void, Void>() {
            List<WifiDetails> wifisList = new ArrayList<WifiDetails>();
            String todaysDateAndTime;
            EmployeeDetails employeeDetails;
            @Override
            protected Void doInBackground(Void... params) {
                wifisList = localStorageData.getWifiNetworks();
                todaysDateAndTime = localStorageData.getServerTime();
                employeeDetails = localStorageData.getUserDetails();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                try{
                    if ( localStorageData.getSessionFlag() && employeeDetails.isDeliveryBoy() ){

                        // ALLOWED WIFI NETWORK
                        if (ComparisionMethods.isAllowedWifiNetwork(connectedWifiDetails, wifisList, null) ){
                            Log.d(Constants.LOG_TAG, "{WIFI_CONNECTION_CHECK}  Connected Wifi: "+connectedWifiDetails.getSSID().toUpperCase()+" is Allowed network");

                            if ( AttendanceMethods.isWorkingDayToday(employeeDetails, todaysDateAndTime) ){
                                Log.d(Constants.LOG_TAG, "{WIFI_CONNECTION_CHECK} Has Schedule today");

                                if ( AttendanceMethods.isScheduleStartedToday(employeeDetails, todaysDateAndTime) ){
                                    Log.d(Constants.LOG_TAG, "{WIFI_CONNECTION_CHECK} Schedule started today already");

                                    if ( AttendanceMethods.isStartedWorkingToday(employeeDetails.getAttendanceDetails(), todaysDateAndTime) ){
                                        Log.d(Constants.LOG_TAG, "{WIFI_CONNECTION_CHECK} Already started working today");
                                    }
                                    // SHOWING ALERT NOTIFICATION
                                    else{
                                        Log.d(Constants.LOG_TAG, "{WIFI_CONNECTION_CHECK} Not yet started working today");

                                        String message = "You are not yet checked in today.\nPlease checkin to start your day!";
                                        Intent alertAct = new Intent(context, AlertActivity.class);
                                        alertAct.putExtra(Constants.MESSAGE, message);
                                        alertAct.putExtra(Constants.ACTION_NAME, Constants.CHECK_IN);
                                        alertAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(alertAct);
                                    }
                                }else{
                                    Log.d(Constants.LOG_TAG, "{WIFI_CONNECTION_CHECK} Schedule not yet started today");
                                }

                            }else{
                                Log.d(Constants.LOG_TAG, "{WIFI_CONNECTION_CHECK} No Schedule today");
                            }
                        }
                        // NOT ALLOWED WIFI NETWORK
                        else{
                            Log.d(Constants.LOG_TAG, "Connected Wifi is NOT allowed network: "+connectedWifiDetails.getSSID().toUpperCase());
                        }
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        }.execute();
    }
}