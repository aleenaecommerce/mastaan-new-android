package com.aleenaecommerce.operations.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.rey.material.widget.TextView;

/**
 * Created by venkatesh on 29/7/15.
 */

public class KaushanTextView extends TextView {

    Typeface kaushanFont;

    public KaushanTextView(Context context) {
        super(context);
        kaushanFont = Typeface.createFromAsset(context.getAssets(), "fonts/KaushanScript-Regular.ttf");
        this.setTypeface(kaushanFont);
    }

    public KaushanTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        kaushanFont = Typeface.createFromAsset(context.getAssets(), "fonts/KaushanScript-Regular.ttf");
        this.setTypeface(kaushanFont);
    }

    public KaushanTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        kaushanFont = Typeface.createFromAsset(context.getAssets(), "fonts/KaushanScript-Regular.ttf");
        this.setTypeface(kaushanFont);
    }
}
