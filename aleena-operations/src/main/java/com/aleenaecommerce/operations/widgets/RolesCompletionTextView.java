package com.aleenaecommerce.operations.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.tokenautocomplete.TokenCompleteTextView;
import com.aleenaecommerce.operations.R;

import java.util.List;

/**
 * Created in vCommonLib on 09/01/17.
 * @author Venkatesh Uppu (c)
 */

public class RolesCompletionTextView extends TokenCompleteTextView<String> {

    Context context;
    RolesCompletionTextView rolesCompletionTextView;
    Typeface hintFont;
    Typeface normalFont;

    public RolesCompletionTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.rolesCompletionTextView = this;
        setupStyles();
        //tagsCompletionTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf"));
    }

    @Override
    protected View getViewForObject(final String tag) {
        View tagView = LayoutInflater.from(getContext()).inflate(R.layout.view_role, null);
        TextView name = (TextView) tagView.findViewById(R.id.name);
        name.setText(tag);

        return tagView;
    }

    @Override
    protected String defaultObject(String completionText) {
//        //Stupid simple example of guessing if we have an email or not
//        int index = completionText.indexOf('@');
//        if (index == -1) {
//            return new Person(completionText, completionText.replace(" ", "") + "@example.com");
//        } else {
//            return new Person(completionText.substring(0, index), completionText);
//        }
        return completionText;
    }

    private void setupStyles(){

        hintFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        normalFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");

        rolesCompletionTextView.setTypeface(hintFont);
        rolesCompletionTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,14);
        rolesCompletionTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if ( charSequence.length() == 0 ){
                    rolesCompletionTextView.setTypeface(hintFont);
                    rolesCompletionTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,14);
                }else{
                    rolesCompletionTextView.setTypeface(normalFont);
                    rolesCompletionTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,16);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void setRoles(String text){
        String []tags = CommonMethods.getStringArrayFromString(text, ",");
        setRoles(tags);
    }
    public void setRoles(List<String> roles){
        setRoles(CommonMethods.getStringArrayFromStringList(roles));
    }
    public void setRoles(String []roles){
        if ( roles != null ){
            for(int i=0;i<roles.length;i++){
                if ( roles[i] != null ){
                    //rolesCompletionTextView.addObject(roles[i]);
                }
            }
        }
    }
    public void clearRoles(){
        List<String> tags = getObjects();
        for(int i=0;i<tags.size();i++) {
           // removeObject(tags.get(i));
        }
    }
}