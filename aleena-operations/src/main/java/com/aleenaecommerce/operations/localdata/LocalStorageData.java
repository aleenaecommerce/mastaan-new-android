package com.aleenaecommerce.operations.localdata;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.WifiDetails;
import com.aleenaecommerce.operations.methods.AttendanceMethods;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class LocalStorageData {

    public SharedPreferences prefs;
    public SharedPreferences.Editor editor;
    Context context;

    public LocalStorageData(Context context) {
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);//context.getSharedPreferences("operationsPrefs", Context.MODE_WORLD_READABLE);//
        editor = prefs.edit();
    }

    public void storeSession(String accessToken, EmployeeDetails employeeDetails){
        editor.putBoolean("sessionFlag", true);
        editor.putString("token", accessToken);
        storeUserDetails(employeeDetails);
        editor.commit();
    }



    public void clearSession(){
        editor.putBoolean("sessionFlag", false);
        editor.putString("token", "");
        editor.putString("user_details_json", "");
        editor.putBoolean("check_in_status", false);
        editor.putString("user_id", "");
        editor.putString("user_name", "");

        editor.putString("attendance_request_id", "");

        editor.commit();
    }

    public boolean getSessionFlag(){
        return prefs.getBoolean("sessionFlag", false);
    }

    public void setAccessToken(String accessToken) {
        editor.putString("token", accessToken);
        editor.commit();
    }

    public String getAccessToken() {
        return prefs.getString("token", "");
    }

    public String getUserID() {
        return prefs.getString("user_id", "");
    }

    public String getUserName() {
        return prefs.getString("user_name", "");
    }

    public void storeUserDetails(EmployeeDetails employeeDetails) {
        if ( employeeDetails != null ) {
            editor.putString("user_details_json", new Gson().toJson(employeeDetails));
            editor.putString("user_id", employeeDetails.getID());
            editor.putString("user_name", employeeDetails.getFullName());
            editor.putBoolean("check_in_status", AttendanceMethods.isCheckedInNow(employeeDetails.getAttendanceDetails(), getServerTime()));
            editor.commit();
        }
    }

    public EmployeeDetails getUserDetails() {
        String user_details_json = prefs.getString("user_details_json", "");
        return new Gson().fromJson(user_details_json, EmployeeDetails.class);
    }

    public String getUserDetailsString() {
        return prefs.getString("user_details_json", "");
    }

    public boolean getCheckInStatus(){
        return prefs.getBoolean("check_in_status", false);
    }

    //-----

    public void setServerTime(String serverTime){
        editor.putString("server_time", serverTime);
        editor.commit();
    }

    public String getServerTime(){
        return prefs.getString("server_time", "");
    }

    public void setUpgradeURL(String upgradeURL){
        editor.putString("upgrade_url", upgradeURL);
        editor.commit();
    }

    public String getUpgradeURL(){
        return prefs.getString("upgrade_url", "");
    }

    public void storeHubLocation(LatLng hubLocation){
        editor.putString("hub_location_lat", hubLocation.latitude+"");
        editor.putString("hub_location_lng", hubLocation.longitude+"");
        editor.commit();
    }

    public LatLng getHubLocation(){
        try{
            return new LatLng(Double.parseDouble(prefs.getString("hub_location_lat", "0")), Double.parseDouble(prefs.getString("hub_location_lng", "0")));
        }catch (Exception e){}
        return new LatLng(0,0);
    }

    public void storeWifiNetworks(List<WifiDetails> wifiNetworksList) {
        if ( wifiNetworksList != null ){
            List<String> wifi_networks_list = new ArrayList<String>();
            for (int i = 0; i < wifiNetworksList.size(); i++) {
                wifi_networks_list.add(new Gson().toJson(wifiNetworksList.get(i)));
            }
            editor.putString("wifi_networks_list", CommonMethods.getJSONArryString(wifi_networks_list));
        }else{
            editor.putString("wifi_networks_list", "[]");
        }
        editor.commit();
    }

    public List<WifiDetails> getWifiNetworks() {
        List<WifiDetails> wifiNetworksList = new ArrayList<>();
        try {
            JSONArray wifi_networks_list = new JSONArray(prefs.getString("wifi_networks_list", "[]"));
            for (int i = 0; i < wifi_networks_list.length(); i++) {
                wifiNetworksList.add(new Gson().fromJson(wifi_networks_list.get(i).toString(), WifiDetails.class));
            }
        } catch (Exception e) {}

        return wifiNetworksList;
    }

    //--------------- UX IMPROVE DATA --------------//

    public void setHomePageLastLoadedTime(String homePageLastLoadedTime){
        editor.putString("home_page_last_loaded_time", homePageLastLoadedTime);
        editor.commit();
    }

    public String getHomePageLastLoadedTime(){
        return  prefs.getString("home_page_last_loaded_time", "NA");
    }

    //--------------

    public void setAttendanceRequestID(String attendanceRequestID){
        editor.putString("attendance_request_id", attendanceRequestID);
        editor.commit();
    }

    public String getAttendanceRequestID(){
        return  prefs.getString("attendance_request_id", null);
    }

}
