package com.aleenaecommerce.operations.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.services.vService;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.activities.AlertActivity;
import com.aleenaecommerce.operations.activities.AttendanceActivity;
import com.aleenaecommerce.operations.activities.LaunchActivity;
import com.aleenaecommerce.operations.activities.LoginActivity;
import com.aleenaecommerce.operations.backend.BackendAPIs;
import com.aleenaecommerce.operations.backend.CommonAPI;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.localdata.LocalStorageData;
import com.aleenaecommerce.operations.methods.AttendanceMethods;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.BootstrapDetails;
import com.aleenaecommerce.operations.models.BreakDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.models.ScheduleDetails;

import java.util.ArrayList;
import java.util.List;

public class OperationsService extends vService {

    Context context;
    LocalStorageData localStorageData;
    BackendAPIs backendAPIs;

    BroadcastReceiver broadcastReceiver;
    boolean isActive = true;

    int autoRefreshTimeOutInMinutes = 1;
    int pinnedNotificationIndex = 117;

    String serverTime;
    EmployeeDetails employeeDetails;

    //FusedLocationService fusedLocationService;
    //List<LocationDetails> locationsQueue = new ArrayList<>();

    public OperationsService() {}

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(Constants.LOG_TAG, "OPERATIONS Service is CREATED");

        context = getApplicationContext();
        localStorageData = new LocalStorageData(context);
        backendAPIs = new BackendAPIs(context, getDeviceID(), getAppVersionCode());

        //fusedLocationService = new FusedLocationService(context, true, null);

        registerServiceReceiver(Constants.OPERATIONS_RECEIVER);
        startAutoRefreshTimer(autoRefreshTimeOutInMinutes * 60);

        //-------------------

        if ( context.getResources().getString(R.string.app_name).equalsIgnoreCase("dev")){
            pinnedNotificationIndex = 118;
        }

        //notificationIconID = R.drawable.ic_launcher;
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationIconID = R.drawable.ic_app_notification;
        }*/

        //---------

        if ( localStorageData.getServerTime().length() == 0 ){
            Log.d(Constants.LOG_TAG, "Loading Bootstrap and showing Pinned notification");
            getAttendanceDetailsAndShowNotification(0);
        }else {
            Log.d(Constants.LOG_TAG, "Directly showing Pinned notification");
            showPinnedNotification(false, false);
        }
        //showPinnedNotification(true, false);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.LOG_TAG, "OPERATIONS Service is STARTED");
        return Service.START_STICKY;
    }

    public void getAttendanceDetailsAndShowNotification(final int retryCount){

        if ( localStorageData.getSessionFlag() ) {
            Log.d(Constants.LOG_TAG, "Loading Bootstrap in Service with rC: "+retryCount);
            backendAPIs.getCommonAPI().getBootStrap(new CommonAPI.BootstrapCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, BootstrapDetails bootstrapDetails) {
                    if (status) {
                        localStorageData.setServerTime(bootstrapDetails.getServerTime());
                        localStorageData.storeUserDetails(bootstrapDetails.getUserDetails());
                        //---
                        showPinnedNotification(false, false);
                    } else {
                        if (retryCount < 4) {
                            new CountDownTimer(2000, 500) {
                                @Override
                                public void onTick(long millisUntilFinished) {
                                }

                                @Override
                                public void onFinish() {
                                    getAttendanceDetailsAndShowNotification(retryCount + 1);
                                }
                            }.start();

                        }
                    }
                }
            });
        }

    }

    @Override
    public void onAutoRefreshTimeElapsed() {
        super.onAutoRefreshTimeElapsed();

        localStorageData.setServerTime(DateMethods.addToDateInMinutes(localStorageData.getServerTime(), autoRefreshTimeOutInMinutes));

        //----

        /*try {
            if (localStorageData.getSessionFlag() && localStorageData.getCheckInStatus()) {
                if (fusedLocationService == null) {
                    fusedLocationService = new FusedLocationService(context, true, null);
                }
                Location currentLocation = fusedLocationService.getCurrentLocation();
                if (currentLocation.getLatitude() != 0 && currentLocation.getLongitude() != 0) {
                    locationsQueue.add(new LocationDetails(currentLocation, localStorageData.getServerTime()));
                    updateLocation();
                } else {
                    Log.d(Constants.LOG_TAG, "Current location is null");
                }
            } else {
                if (fusedLocationService != null) {
                    Log.d(Constants.LOG_TAG, "Stopping fused location as user is not checked in");
                    try{fusedLocationService.stop();}catch (Exception e){}
                    fusedLocationService = null;
                }
            }
        }catch (Exception e){e.printStackTrace();}*/

        //-----

        showPinnedNotification(true, false);
    }

    @Override
    public void onServiceReceiverMessage(String serviceReceiverName, Intent receivedData) {
        super.onServiceReceiverMessage(serviceReceiverName, receivedData);

        if ( isActive == true ) {
            try {
                if (receivedData.getStringExtra("type").equalsIgnoreCase("update_pinned_notification")) {
                    showPinnedNotification(false, receivedData.getBooleanExtra(Constants.PLAY_SOUND, false));
                }
            } catch (Exception e) {}
        }
    }

    //----

    /*public void updateLocation(){

        backendAPIs.getLocationAPI().updateLocation(locationsQueue, new LocationAPI.LocationUpdateCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, int updatedLocationCount) {
                if (status) {
                    if ( locationsQueue.size() >= updatedLocationCount ) {
                        for (int i = 0; i < updatedLocationCount; i++) {
                            locationsQueue.remove(0);
                        }
                    }
                }else{

                }
            }
        });
    }*/

    //-----

    public void showPinnedNotification(boolean isFromTimer, boolean playSound) {

        serverTime = localStorageData.getServerTime();

        ArrayList<String> completeNotificationMessage = new ArrayList<String>();
        String simpleNotificationMessage = "";
        String actionButtonTitle = "";

        // IF LOGGED IN
        if (localStorageData.getSessionFlag()) {

            if ( serverTime != null && serverTime.length() > 0 ){
                employeeDetails = localStorageData.getUserDetails();
                AttendanceDetails attendanceDetails = employeeDetails.getAttendanceDetails();

                // IF DELIVERY BOY ONLY
                if (employeeDetails.isDeliveryBoy() == true) {

                    if ( DateMethods.compareDates(DateMethods.getOnlyDate(serverTime), DateMethods.getOnlyDate(attendanceDetails.getDate())) == 0
                            && attendanceDetails.isLeave() ){
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancel(pinnedNotificationIndex);
                    }
                    else {
                        // IF WORKING DAY ONLY
                        if (AttendanceMethods.isWorkingDayToday(employeeDetails, serverTime)) {
                            // IF STARTED WORKING TODAY
                            if (AttendanceMethods.isStartedWorkingToday(attendanceDetails, serverTime)) {
                                completeNotificationMessage = AttendanceMethods.getTodaysAttendanceDetailsList(attendanceDetails, serverTime);
                                if (attendanceDetails.getCheckInStatus()) {
                                    simpleNotificationMessage = "Checkin : " + DateMethods.getDateInFormat(attendanceDetails.getLastCheckInTime(), DateConstants.HH_MM_A);
                                    actionButtonTitle = "CHECK OUT";
                                } else {
                                    simpleNotificationMessage = "Checkout : " + DateMethods.getDateInFormat(attendanceDetails.getLastCheckOutTime(), DateConstants.HH_MM_A);
                                    actionButtonTitle = "CHECK IN";
                                }
                                // TO PREVENT CALL FROM ATTENDANCE PAGE
                                if (isFromTimer) {
                                    checkForBreaks();
                                }
                            }
                            // IF NOT YET CHECKED IN TODAY
                            else {
                                // IF SCHEDULE NOT STARTED TODAY
                                if (AttendanceMethods.isScheduleStartedToday(employeeDetails, serverTime) == false) {
                                    simpleNotificationMessage = "Your schedule not yet started today";
                                    completeNotificationMessage.add("Your schedule not yet started today");
                                    actionButtonTitle = "CHECK IN";
                                } else {
                                    simpleNotificationMessage = "Please check in to start your day";
                                    completeNotificationMessage.add("Please check in to start your day");
                                    actionButtonTitle = "CHECK IN";
                                }
                            }

                            showNotification(playSound, simpleNotificationMessage, completeNotificationMessage, actionButtonTitle);
                        }
                        // IF NOT WORKING DAY
                        else {
                            // IF STARTED WORKING TODAY EVEN THOUGH HOLIDAY
                            if (AttendanceMethods.isStartedWorkingToday(attendanceDetails, serverTime)) {
                                completeNotificationMessage = AttendanceMethods.getTodaysAttendanceDetailsList(attendanceDetails, serverTime);
                                if (attendanceDetails.getCheckInStatus()) {
                                    simpleNotificationMessage = "Checkin : " + DateMethods.getDateInFormat(attendanceDetails.getLastCheckInTime(), DateConstants.HH_MM_A);
                                    actionButtonTitle = "CHECK OUT";
                                } else {
                                    simpleNotificationMessage = "Checkout : " + DateMethods.getDateInFormat(attendanceDetails.getLastCheckOutTime(), DateConstants.HH_MM_A);
                                    actionButtonTitle = "CHECK IN";
                                }
                                showNotification(playSound, simpleNotificationMessage, completeNotificationMessage, actionButtonTitle);
                            } else {
                                //Log.d(Constants.LOG_TAG, "IS NOT working day");
                                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.cancel(pinnedNotificationIndex);
                            }
                        }
                    }
                }
                else{
                    Log.d(Constants.LOG_TAG, "User " + employeeDetails.getFullName() + " is not Delivery boy");
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel(pinnedNotificationIndex);
                }
            }
            // IF BOOTSTRSP IS NOT LOADED
            else{
                simpleNotificationMessage = "Unable to load details, tap here to reload";
                completeNotificationMessage.add("Unable to load details, tap here to reload");
                showNotification(playSound, simpleNotificationMessage, completeNotificationMessage, actionButtonTitle);
            }
        }
        // IF NOT LOGGED IN
        else{
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(pinnedNotificationIndex);
//            actionButtonTitle = "LOGIN";
//            simpleNotificationMessage = "Please Login";
//            completeNotificationMessage.add("Please Login");
//
//            showNotification(playSound, simpleNotificationMessage, completeNotificationMessage, actionButtonTitle);
        }

    }

    public void showNotification(boolean playSound, String simpleNotificationMessage, ArrayList<String> completeNotificationMessage, String actionButtonTitle){

        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
//                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notification))
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setContentText(simpleNotificationMessage)
                .setPriority(Notification.PRIORITY_MIN);
        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        inboxStyle.setBigContentTitle(context.getResources().getString(R.string.app_name));

        for (int i = 0; i < completeNotificationMessage.size(); i++) {
            inboxStyle.addLine(completeNotificationMessage.get(i));
        }

        notificationBuilder.setStyle(inboxStyle);
        notificationBuilder.setOngoing(true);

        if ( actionButtonTitle != null && actionButtonTitle.length() > 0 ){
            if ( actionButtonTitle.equalsIgnoreCase("Check in") ){
                notificationBuilder.setSmallIcon(R.drawable.ic_notification);

                Intent notificationClickAct = new Intent(context, AttendanceActivity.class);
                notificationClickAct.putExtra(Constants.AUTO_CHECK_IN, true);
                //if ( isAllowedWifiNetwork(getConnectedWifiDetails()) ){
                notificationBuilder.addAction(R.drawable.ic_action_done, actionButtonTitle, PendingIntent.getActivity(this, 0, notificationClickAct, PendingIntent.FLAG_CANCEL_CURRENT));
                if ( playSound ){
                    notificationBuilder.setPriority(Notification.PRIORITY_HIGH);
                    notificationBuilder.setSmallIcon(R.drawable.ic_notification);
                    notificationBuilder.setDefaults(Notification.DEFAULT_ALL);
                }
                //}
            }
            else if ( actionButtonTitle.equalsIgnoreCase("Check out") ){
                notificationBuilder.setSmallIcon(R.drawable.ic_check_in);
                notificationBuilder.setPriority(Notification.PRIORITY_HIGH);

                Intent notificationClickAct = new Intent(context, AttendanceActivity.class);
                notificationClickAct.putExtra(Constants.AUTO_CHECK_OUT, true);
                notificationBuilder.addAction(R.drawable.ic_action_done, actionButtonTitle, PendingIntent.getActivity(this, 0, notificationClickAct, PendingIntent.FLAG_CANCEL_CURRENT));
            }
            else if ( actionButtonTitle.equalsIgnoreCase("Login") ){
                notificationBuilder.setSmallIcon(R.drawable.ic_notification);

                Intent notificationClickAct = new Intent(context, LoginActivity.class);
                notificationBuilder.addAction(R.drawable.ic_action_done, actionButtonTitle, PendingIntent.getActivity(this, 0, notificationClickAct, PendingIntent.FLAG_CANCEL_CURRENT));
            }
        }

        ComponentName componentName = new Intent(context, LaunchActivity.class).getComponent();
        Intent launchAct = Intent.makeRestartActivityTask(componentName);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, launchAct, 0);
        notificationBuilder.setContentIntent(contentIntent);

        //NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //notificationManager.notify(pinnedNotificationIndex, notificationBuilder.build());
        try{    startForeground(pinnedNotificationIndex, notificationBuilder.build());  }catch (Exception e){}
    }

    public void checkForBreaks(){

        String serverTime = DateMethods.getDateInFormat(localStorageData.getServerTime(), DateConstants.HH_MM_SS);
        AttendanceDetails attendanceDetails = employeeDetails.getAttendanceDetails();
        ScheduleDetails scheduleDetails = AttendanceMethods.getTodaysScheduleDetails(employeeDetails, localStorageData.getServerTime());
        List<BreakDetails> breaksList = scheduleDetails.getBreaksList();

        for (int i=0;i<breaksList.size();i++){
            BreakDetails breakDetails = breaksList.get(i);

            long diffToStartTimeInSeconds = DateMethods.getDifferenceBetweenDatesInMilliSecondsWithSign(breakDetails.getStartTime(), serverTime) / 1000;
            long diffToEndTimeInSeconds = DateMethods.getDifferenceBetweenDatesInMilliSecondsWithSign(breakDetails.getEndTime(), serverTime) / 1000;

            //Log.d(Constants.LOG_TAG, breakDetails.getStartTime()+" sD: "+diffToStartTimeInSeconds+"s, eD: "+diffToEndTimeInSeconds+"s "+breakDetails.getEndTime()+" with "+serverTime);
            if ( attendanceDetails.getCheckInStatus() && diffToStartTimeInSeconds >= -60 && diffToStartTimeInSeconds <= 0 ){
                String message = "BREAK:\n"
                        + DateMethods.getTimeIn12HrFormat(breakDetails.getStartTime()).toUpperCase()+" to "+DateMethods.getTimeIn12HrFormat(breakDetails.getEndTime())
                        + "\nStarts now, take a break for "+DateMethods.getTimeStringFromMinutes(breakDetails.getDurationInMinutes());

                Intent alertAct = new Intent(context, AlertActivity.class);
                alertAct.putExtra(Constants.MESSAGE, message);
                alertAct.putExtra(Constants.ACTION_NAME, Constants.CHECK_OUT);
                alertAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(alertAct);

                break;
            }
            else if ( attendanceDetails.getCheckInStatus() == false && diffToEndTimeInSeconds >= 0 && diffToEndTimeInSeconds <= 60 ){
                String message = "BREAK:\n"
                        + DateMethods.getTimeIn12HrFormat(breakDetails.getStartTime()).toUpperCase()+" to "+DateMethods.getTimeIn12HrFormat(breakDetails.getEndTime())
                        + "\nEnds now, please check in";

                Intent alertAct = new Intent(context, AlertActivity.class);
                alertAct.putExtra(Constants.MESSAGE, message);
                alertAct.putExtra(Constants.ACTION_NAME, Constants.CHECK_IN);
                alertAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(alertAct);

                break;
            }
        }

//        Intent alertAct = new Intent(context, AlertActivity.class);
//        alertAct.putExtra(Constants.MESSAGE, "Demo Notification message");
//        alertAct.putExtra(Constants.ACTION_NAME, Constants.CHECK_IN);
//        alertAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(alertAct);

    }

    //=============

    @Override
    public void onDestroy() {
        super.onDestroy();
        isActive = false;

        //if ( fusedLocationService != null ) {
        //    try{fusedLocationService.stop();}catch (Exception e){}
        //}

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(pinnedNotificationIndex);

        try{
            context.unregisterReceiver(broadcastReceiver);
        }catch (Exception e){}
        Log.d(Constants.LOG_TAG, "OPERATIONS Service is FINISHED");
    }
}
