package com.aleenaecommerce.operations.backend.models;

import com.aleenaecommerce.operations.models.AccountDetails;

import java.util.List;

/**
 * Created by venkatesh on 28/7/16.
 */
public class ResponseTransactionAccounts {

    List<AccountDetails> creditAccounts;
    List<AccountDetails> debitAccounts;

    public List<AccountDetails> getCreditAccounts() {
        if ( creditAccounts == null ){ return creditAccounts;   }
        return creditAccounts;
    }

    public List<AccountDetails> getDebitAccounts() {
        if ( debitAccounts == null ){ return creditAccounts;   }
        return debitAccounts;
    }
}
