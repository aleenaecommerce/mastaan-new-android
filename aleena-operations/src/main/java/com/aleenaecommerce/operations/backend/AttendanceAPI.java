package com.aleenaecommerce.operations.backend;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.aleenaecommerce.operations.backend.models.RequestCheckIn;
import com.aleenaecommerce.operations.backend.models.RequestCheckOut;
import com.aleenaecommerce.operations.backend.models.RequestMarkLeave;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.localdata.LocalStorageData;
import com.aleenaecommerce.operations.methods.CheckError;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.KmLogItem;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class AttendanceAPI {

    Context context;
    String deviceID;
    int appVersion;
    //OperationsToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface AttendanceCallback{
        void onComplete(boolean status, int statusCode, String message, AttendanceDetails attendanceDetails);
    }

    public interface KmLogItemListCallback {
        void onComplete(boolean status, Map<Integer, List<KmLogItem>> kmLogMap, int status_code);
    }

    public interface AttendanceReportsListCallback {
        void onComplete(boolean status, List<AttendanceDetails> attendanceReportsList, int status_code);
    }

    public AttendanceAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        //this.activity = (OperationsToolBarActivity) context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void checkIN(RequestCheckIn requestCheckIn, final AttendanceCallback callback){

        final LocalStorageData localStorageData = new LocalStorageData(context);
        String requestID = localStorageData.getAttendanceRequestID();
        if ( requestID == null || requestID.length() == 0 ) {
            requestID = Base64.encodeToString((localStorageData.getUserID() + ":CI" + System.currentTimeMillis()).getBytes(), Base64.NO_WRAP);
            localStorageData.setAttendanceRequestID(requestID);     // Storing RequestID in LocalStorage
        }
        requestCheckIn.setRequestID(requestID);

        retrofitInterface.checkIN(deviceID, appVersion, requestCheckIn, new Callback<AttendanceDetails>() {
            @Override
            public void success(AttendanceDetails attendanceDetails, Response response) {
                if (attendanceDetails != null) {
                    localStorageData.setAttendanceRequestID("");        // Clearing RequestID from LocalStorage
                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_IN ]  >>  Success");
                    callback.onComplete(true, 200, "Success", attendanceDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_IN ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    int statusCode = -1;
                    String message = "Unable to check in try again";
                    try{
                        statusCode = error.getResponse().getStatus();
                    }catch (Exception e){}
                    try{
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);
                        message = jsonResponse.getString("msg");
                    }catch (Exception e){}

                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_IN ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, statusCode, message, null);
                }
            }
        });
    }

    public void checkOUT(final AttendanceCallback callback){
//req_id":"NTczMzE4YjUyYjNkMzA2YTRmODI1ZjEyOkNJMTQ3MDgxNzkxMTI3Mg\u003d\u003d"
        final LocalStorageData localStorageData = new LocalStorageData(context);
        String requestID = localStorageData.getAttendanceRequestID();
        if ( requestID == null || requestID.length() == 0 ) {
            requestID = Base64.encodeToString((localStorageData.getUserID() + ":CO" + System.currentTimeMillis()).getBytes(), Base64.NO_WRAP);
            localStorageData.setAttendanceRequestID(requestID);     // Storing RequestID in LocalStorage
        }

        retrofitInterface.checkOUT(deviceID, appVersion, new RequestCheckOut(requestID), new Callback<AttendanceDetails>() {
            @Override
            public void success(AttendanceDetails attendanceDetails, Response response) {
                if (attendanceDetails != null) {
                    localStorageData.setAttendanceRequestID("");        // Clearing RequestID from LocalStorage
                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_OUT ]  >>  Success");
                    callback.onComplete(true, 200, "Success", attendanceDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_OUT ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    int statusCode = -1;
                    String message = "Unable to check out try again";
                    try {
                        statusCode = error.getResponse().getStatus();
                    } catch (Exception e) {
                    }
                    try {
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);
                        message = jsonResponse.getString("msg");
                    } catch (Exception e) {
                    }

                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_OUT ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, statusCode, message, null);
                }
            }
        });
    }

    public void checkINUser(final String userID, RequestCheckIn requestCheckIn, final AttendanceCallback callback){

        retrofitInterface.checkINUser(deviceID, appVersion, userID, requestCheckIn, new Callback<AttendanceDetails>() {
            @Override
            public void success(AttendanceDetails attendanceDetails, Response response) {
                if (attendanceDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_IN_USER ]  >>  Success");
                    callback.onComplete(true, 200, "Success", attendanceDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_IN_USER ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_IN_USER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_IN_USER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void checkOUTUser(final String userID, final AttendanceCallback callback){

        retrofitInterface.checkOUTUser(deviceID, appVersion, userID, new JSONObject(), new Callback<AttendanceDetails>() {
            @Override
            public void success(AttendanceDetails attendanceDetails, Response response) {
                if (attendanceDetails != null) {
                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_OUT_USER ]  >>  Success");
                    callback.onComplete(true, 200, "Success", attendanceDetails);
                } else {
                    Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_OUT_USER ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_OUT_USER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ CHECK_OUT_USER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void markLeave(final String userID, final RequestMarkLeave requestMarkLeave, final StatusCallback callback){

        retrofitInterface.markLeave(deviceID, appVersion, userID, requestMarkLeave, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ MARK_LEAVE ]  >>  Success for uID: " + userID);
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ MARK_LEAVE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for uID: " + userID);
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ MARK_LEAVE ]  >>  Error = " + error.getKind() + " for uID: " + userID);
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void unmarkLeave(final String attendanceID, final StatusCallback callback){

        retrofitInterface.unmarkLeave(deviceID, appVersion, attendanceID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ UNMARK_LEAVE ]  >>  Success for aID: " + attendanceID);
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ UNMARK_LEAVE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for aID: " + attendanceID);
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nATTENDENCE_API : [ UNMARK_LEAVE ]  >>  Error = " + error.getKind() + " for aID: " + attendanceID);
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getEmployeeAttendanceReport(String userID, int month, int year, final AttendanceReportsListCallback callback) {

        retrofitInterface.getEmployeeAttendanceReport(deviceID, appVersion, userID, month, year, new HashMap<String, Object>(), new retrofit.Callback<List<AttendanceDetails>>() {
            @Override
            public void success(List<AttendanceDetails> attendanceReportsList, Response response) {
                if (attendanceReportsList != null) {
                    callback.onComplete(true, attendanceReportsList, response.getStatus());
                } else {
                    callback.onComplete(false, null, 500);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, -1);
                    }
                }
            }
        });
    }


}
