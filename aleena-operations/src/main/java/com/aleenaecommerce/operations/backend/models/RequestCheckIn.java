package com.aleenaecommerce.operations.backend.models;

import android.location.Location;

/**
 * Created by venkatesh on 10/8/16.
 */
public class RequestCheckIn {
    String req_id;
    String lcil;        // Last Checked in Location
    String lcib;        // Last Checked in By

    public RequestCheckIn(Location location, String checkedInBy){
        this(location!=null?location.getLatitude()+","+location.getLongitude():null, checkedInBy);
    }
    public RequestCheckIn(String location, String checkedInBy){
        this.lcil = location;
        this.lcib = checkedInBy;
    }

    public void setRequestID(String requestID) {
        this.req_id = requestID;
    }

    public String getLastCheckInByEmployeeID() {
        return lcib;
    }

    public String getLastCheckInPlaceLatLng() {
        return lcil;
    }
}
