package com.aleenaecommerce.operations.backend.models;

/**
 * Created by Venkatesh Uppu on 23-06-2016.
 */
public class RequestLogin {
    String m;
    String did;
    String n;;

    public RequestLogin(String mobileNumber, String deviceID, String deviceName) {
        this.m = mobileNumber;
        this.did = deviceID;
        this.n = deviceName;
    }
}
