package com.aleenaecommerce.operations.backend.models;

import com.aleena.common.methods.DateMethods;

/**
 * Created by venkatesh on 23/7/16.
 */
public class RequestCreateOrUpdateTransaction {
    String _id;
    String tid;
    String cra;
    String dra;
    double amt;
    String ed;
    String com;
    String ath;

    public RequestCreateOrUpdateTransaction(String transactionID, String debitAccountID, String creditAccountID, double amount, String entryDate, String comments){
        this.tid = transactionID;
        this.dra = debitAccountID;
        this.cra = creditAccountID;
        this.amt = amount;
        this.ed = entryDate;
        this.com = comments;
    }

    public String getID() {
        return _id;
    }

    public String getTransactionTypeID() {
        return tid;
    }

    public String getCreditAccountID() {
        return cra;
    }

    public String getDebitAccountID() {
        return dra;
    }

    public double getAmount() {
        return amt;
    }

    public String getEntryDate() {
        return DateMethods.getReadableDateFromUTC(ed);
    }

    public String getComments() {
        return com;
    }

    public String getAttachment() {
        return ath;
    }

}
