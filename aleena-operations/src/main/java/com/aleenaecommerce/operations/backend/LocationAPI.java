package com.aleenaecommerce.operations.backend;

import android.content.Context;
import android.util.Log;

import com.aleenaecommerce.operations.backend.models.RequestUpdateLocation;
import com.aleenaecommerce.operations.backend.models.ResponseUpdateLocation;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.methods.CheckError;
import com.aleenaecommerce.operations.models.LocationDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class LocationAPI {

    Context context;
    String deviceID;
    int appVersion;
    //OperationsToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface LocationUpdateCallback {
        void onComplete(boolean status, int statusCode, String message, int updatedLocationCount);
    }

    public LocationAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        //this.activity = (OperationsToolBarActivity) context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void updateLocation(final List<LocationDetails> locationsQueue, final LocationUpdateCallback callback) {

        retrofitInterface.updateLocation(deviceID, appVersion, new RequestUpdateLocation(locationsQueue), new Callback<ResponseUpdateLocation>() {
            @Override
            public void success(ResponseUpdateLocation responseUpdateLocation, Response response) {
                Log.d(Constants.LOG_TAG, "\nLOCATION_API : [ UPDATE_LOCATION ]  >>  Success, uC: " + responseUpdateLocation.getCount());
                callback.onComplete(true, 200, "Success", responseUpdateLocation.getCount());
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nLOCATION_API : [ UPDATE_LOCATION ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", 0);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nLOCATION_API : [ UPDATE_LOCATION ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", 0);
                    }
                }
            }
        });
    }

}
