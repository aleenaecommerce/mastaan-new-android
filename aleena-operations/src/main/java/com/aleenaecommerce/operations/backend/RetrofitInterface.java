package com.aleenaecommerce.operations.backend;

import com.aleena.common.models.ResponseStatus;
import com.aleena.common.models.WifiDetails;
import com.aleenaecommerce.operations.backend.models.RequestAddOrEditEmployee;
import com.aleenaecommerce.operations.backend.models.RequestCheckIn;
import com.aleenaecommerce.operations.backend.models.RequestCheckOut;
import com.aleenaecommerce.operations.backend.models.RequestCreateOrUpdateTransaction;
import com.aleenaecommerce.operations.backend.models.RequestLogin;
import com.aleenaecommerce.operations.backend.models.RequestMarkLeave;
import com.aleenaecommerce.operations.backend.models.RequestUpdateLocation;
import com.aleenaecommerce.operations.backend.models.ResponseCreateAuthPassword;
import com.aleenaecommerce.operations.backend.models.ResponseLogin;
import com.aleenaecommerce.operations.backend.models.ResponseTransactionAccounts;
import com.aleenaecommerce.operations.backend.models.ResponseUpdateLocation;
import com.aleenaecommerce.operations.models.AccountDetails;
import com.aleenaecommerce.operations.models.AppDetails;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.BootstrapDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.models.JournalEntryCompleteDetails;
import com.aleenaecommerce.operations.models.TransactionDetails;
import com.aleenaecommerce.operations.models.TransactionTypeDetails;
import com.aleenaecommerce.operations.models.WarehouseDetails;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public interface RetrofitInterface {

    // BASE URL:   //

    // AUTHENTICATION API

    @POST("/employee/deviceconnect")
    void login(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestLogin user, Callback<ResponseLogin> callback);

    @GET("/employee/disconnect")
    void logout(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<ResponseStatus> callback);

    @GET("/employee/authid")
    void createAuthPassword(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<ResponseCreateAuthPassword> callback);


    // COMMON API

    @GET("/employee/bootstrap")
    void getBootstrap(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<BootstrapDetails> callback);

    @GET("/employee/wifis")
    void getWifis(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<WifiDetails>> callback);

    @POST("/employee/wifinetwork/add")
    void addWifi(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body WifiDetails wifiDetails, Callback<WifiDetails> callback);

    @GET("/employee/wifinetwork/delete/{network_id}")
    void deleteWifi(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("network_id") String network_id, Callback<ResponseStatus> callback);

    @GET("/employee/apps")
    void getApps(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<AppDetails>> callback);

    @GET("/employee/warehouses")
    void getWarehouses(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<WarehouseDetails>> callback);


    // ATTENDENCE API

    @POST("/employee/checkin")
    void checkIN(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestCheckIn requestCheckIn, Callback<AttendanceDetails> callback);

    @POST("/employee/checkout")
    void checkOUT(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestCheckOut requestCheckOut, Callback<AttendanceDetails> callback);

    @POST("/employee/checkinemployee/{user_id}")
    void checkINUser(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("user_id") String user_id, @Body RequestCheckIn requestCheckIn, Callback<AttendanceDetails> callback);

    @POST("/employee/checkoutemployee/{user_id}")
    void checkOUTUser(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("user_id") String user_id, @Body JSONObject requestObject, Callback<AttendanceDetails> callback);

    @POST("/employee/markleave/{user_id}")
    void markLeave(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("user_id") String user_id, @Body RequestMarkLeave requestObject, Callback<ResponseStatus> callback);

    @GET("/employee/unmarkleave/{attendance_id}")
    void unmarkLeave(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("attendance_id") String attendance_id, Callback<ResponseStatus> callback);

    @GET("/employee/deliveryboyattendance/{employee_id}/{month}/{year}")
    void getEmployeeAttendanceReport(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("employee_id") String employee_id, @Path("month") int month, @Path("year") int year, @QueryMap Map<String, Object> options, Callback<List<AttendanceDetails>> callback);


    // LOCATION API

    @POST("/employee/tracklocation")
    void updateLocation(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestUpdateLocation requestUpdateLocation, Callback<ResponseUpdateLocation> callback);


    // EMPLOYEES API

    @POST("/employee/create")
    void addEmployee(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestAddOrEditEmployee requestObject, Callback<EmployeeDetails> callback);

    @POST("/employee/update/{employee_id}")
    void updateEmployee(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("employee_id") String employee_id, @Body RequestAddOrEditEmployee requestObject, Callback<EmployeeDetails> callback);

    @GET("/employee/employees")
    void getEmployees(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<EmployeeDetails>> callback);

    // FINANCE API

    @GET("/finance/summary")
    void getAccountsSummary(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<AccountDetails>> callback);

    @GET("/finance/transactions")
    void getTransactionsTypes(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<TransactionTypeDetails>> callback);

    @GET("/finance/accountsfortransaction/{transaction_type_id}")
    void getTransactionTypeAccounts(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("transaction_type_id") String transactionTypeID, Callback<ResponseTransactionAccounts> callback);

    @GET("/finance/childaccounts/{account_id}")
    void getChildAccounts(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("account_id") String accountID, Callback<List<AccountDetails>> callback);

    @GET("/finance/accountstatement/{account_id}")
    void getAccountStatementForCurrentMonth(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("account_id") String accountID, Callback<AccountDetails> callback);

    @GET("/finance/accountstatement/{account_id}/{day}/{month}/{year}")
    void getAccountStatementForDay(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("account_id") String accountID, @Path("day") int day, @Path("month") int month, @Path("year") int year, Callback<AccountDetails> callback);

    @GET("/finance/accountstatement/{account_id}/{month}/{year}")
    void getAccountStatementForMonth(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("account_id") String accountID, @Path("month") int month, @Path("year") int year, Callback<AccountDetails> callback);

    @GET("/finance/entry/{entry_id}")
    void getJournalEntryDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("entry_id") String entry_id, Callback<JournalEntryCompleteDetails> callback);

    @Multipart
    @POST("/finance/creatependingtransaction")
    void createPendingTransaction(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Part("tid") String transactionTypeID, @Part("dra") String debitAccountID, @Part("cra") String creditAccountID, @Part("amt") double amount, @Part("ed") String entryDate, @Part("com") String comments, @Part("ath") TypedFile typedFile, Callback<RequestCreateOrUpdateTransaction> callback);

    @Multipart
    @POST("/finance/updatependingtransaction/{transaction_id}")
    void updatePendingTransaction(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("transaction_id") String transactionID, @Part("tid") String transactionTypeID, @Part("dra") String debitAccountID, @Part("cra") String creditAccountID, @Part("amt") double amount, @Part("ed") String entryDate, @Part("com") String comments, @Part("delAth") boolean removeAttachment, @Part("ath") TypedFile typedFile, Callback<RequestCreateOrUpdateTransaction> callback);

    @GET("/finance/deletependingtransaction/{transaction_id}")
    void deletePendingTransaction(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("transaction_id") String transactionID, Callback<ResponseStatus> callback);

    @GET("/finance/pendingtransactions")
    void getPendingTransactions(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<TransactionDetails>> callback);

    @GET("/finance/pendingtransaction/{transaction_id}")
    void getPendingTransactionDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("transaction_id") String transactionID, Callback<TransactionDetails> callback);

    @GET("/finance/approvependingtransaction/{transaction_id}")
    void approvePendingTransaction(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("transaction_id") String transactionID, Callback<ResponseStatus> callback);


}