package com.aleenaecommerce.operations.backend.models;

import com.aleena.common.methods.DateMethods;

import java.util.Calendar;

/**
 * Created by venkatesh on 4/8/16.
 */
public class RequestMarkLeave {
    int day;
    int month;
    int year;
    boolean pl;

    public RequestMarkLeave(String date, boolean isPaidLeave){
        Calendar calendar = DateMethods.getCalendarFromDate(date);
        this.day = calendar.get(Calendar.DAY_OF_MONTH);
        this.month = calendar.get(Calendar.MONTH)+1;
        this.year = calendar.get(Calendar.YEAR);
        this.pl = isPaidLeave;
    }

    public RequestMarkLeave(int day, int month, int year, boolean isPaidLeave){
        this.day = day;
        this.month = month;
        this.year = year;
        this.pl = isPaidLeave;
    }

    public String getDate(){
        return day+"/"+month+"/"+year;
    }
}
