package com.aleenaecommerce.operations.backend;

import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;
import com.aleenaecommerce.operations.backend.models.RequestCreateOrUpdateTransaction;
import com.aleenaecommerce.operations.backend.models.ResponseTransactionAccounts;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.localdata.LocalStorageData;
import com.aleenaecommerce.operations.models.AccountDetails;
import com.aleenaecommerce.operations.models.JournalEntryCompleteDetails;
import com.aleenaecommerce.operations.models.JournalEntryDetails;
import com.aleenaecommerce.operations.models.PrefillDetails;
import com.aleenaecommerce.operations.models.PurposeDetails;
import com.aleenaecommerce.operations.models.TransactionDetails;
import com.aleenaecommerce.operations.models.TransactionTypeDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class FinanceAPI {

    Context context;
    LocalStorageData localStorageData;
    String deviceID;
    int appVersion;
    //OperationsToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface AccountsListCallback{
        void onComplete(boolean status, int statusCode, String message, List<AccountDetails> accountsList);
    }

    public interface TransactionsTypesListCallback {
        void onComplete(boolean status, int statusCode, String message, List<TransactionTypeDetails> transactionTypesList);
    }

    public interface TransactionsAccountsCallback{
        void onComplete(boolean status, int statusCode, String message, List<AccountDetails> creditAccounts, List<AccountDetails>debitAccounts);
    }

    public interface AddTransactionCallback{
        void onComplete(boolean status, int statusCode, String message, RequestCreateOrUpdateTransaction responseObject);
    }

    public interface AccountSummaryCallback {
        void onComplete(boolean status, int statusCode, String message, String date, AccountDetails accountDetails);
    }

    public interface TransactionsListCallback {
        void onComplete(boolean status, int statusCode, String message, List<TransactionDetails> transactionsList);
    }

    public interface TransactionDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, TransactionDetails transactionDetails);
    }

    public interface JournalEntryDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, JournalEntryCompleteDetails journalEntryDetails);
    }

    public interface FinanceDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, PrefillDetails prefillDetails);
    }

    public FinanceAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.localStorageData = new LocalStorageData(context);
        //this.activity = (OperationsToolBarActivity) context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }



    public void getTransactionsTypes(final TransactionsTypesListCallback callback){

        retrofitInterface.getTransactionsTypes(deviceID, appVersion, new Callback<List<TransactionTypeDetails>>() {
            @Override
            public void success(List<TransactionTypeDetails> transactionsList, Response response) {
                if (transactionsList != null) {
                    // SORTING GROUPS BY NAME
                    Collections.sort(transactionsList, new Comparator<TransactionTypeDetails>() {
                        public int compare(TransactionTypeDetails item1, TransactionTypeDetails item2) {
                            return item1.getName().compareToIgnoreCase(item2.getName());
                        }
                    });

                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ TRANSACTIONS_LIST ]  >>  Success");
                    callback.onComplete(true, 200, "Success", transactionsList);
                } else {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ TRANSACTIONS_LIST ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ TRANSACTIONS_LIST ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ TRANSACTIONS_LIST ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", null);
                }
            }
        });
    }

    public void getTransactionTypeAccounts(final String transactionTypeID, final TransactionsAccountsCallback callback){

        retrofitInterface.getTransactionTypeAccounts(deviceID, appVersion, transactionTypeID, new Callback<ResponseTransactionAccounts>() {
            @Override
            public void success(ResponseTransactionAccounts responeObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ TRANSACTION_ACCOUNTS ]  >>  Success");
                callback.onComplete(true, 200, "Success", responeObject.getCreditAccounts(), responeObject.getDebitAccounts());
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ TRANSACTION_ACCOUNTS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ TRANSACTION_ACCOUNTS ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", null, null);
                }
            }
        });
    }

    public void getChildAccounts(final String accountID, final AccountsListCallback callback){

        retrofitInterface.getChildAccounts(deviceID, appVersion, accountID, new Callback<List<AccountDetails>>() {
            @Override
            public void success(List<AccountDetails> accounsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ CHILD_ACCOUNTS ]  >>  Success for aID: " + accountID);
                callback.onComplete(true, 200, "Success", accounsList);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ CHILD_ACCOUNTS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for aID: " + accountID);
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ CHILD_ACCOUNTS ]  >>  Error = " + error.getKind() + " for aID: " + accountID);
                    callback.onComplete(false, -1, "Error", null);
                }
            }
        });
    }

    public void getAccountsSummary(final AccountsListCallback callback){

        retrofitInterface.getAccountsSummary(deviceID, appVersion, new Callback<List<AccountDetails>>() {
            @Override
            public void success(List<AccountDetails> accountList, Response response) {
                if (accountList != null) {
                    // SORTING GROUPS BY NAME
                    Collections.sort(accountList, new Comparator<AccountDetails>() {
                        public int compare(AccountDetails item1, AccountDetails item2) {
                            return item1.getName().compareToIgnoreCase(item2.getName());
                        }
                    });

                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNTS_SUMMARY ]  >>  Success");
                    callback.onComplete(true, 200, "Success", accountList);
                } else {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNTS_SUMMARY ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNTS_SUMMARY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNTS_SUMMARY ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", null);
                }
            }
        });
    }

    public void getAccountStatementForCurrentMonth(final String accountID, final AccountSummaryCallback callback){

        final String date = DateMethods.getOnlyDate(localStorageData.getServerTime());
        retrofitInterface.getAccountStatementForCurrentMonth(deviceID, appVersion, accountID, new Callback<AccountDetails>() {
            @Override
            public void success(AccountDetails accountDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNT_STATEMENT_CURRENT_MONTH ]  >>  Success");
                callback.onComplete(true, 200, "Success", date, accountDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNT_STATEMENT_CURRENT_MONTH ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", date, null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNT_STATEMENT_CURRENT_MONTH ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", date, null);
                }
            }
        });
    }

    public void getAccountStatementForDay(final String accountID, final String date, final AccountSummaryCallback callback){
        Calendar calendar = DateMethods.getCalendarFromDate(date);
        retrofitInterface.getAccountStatementForDay(deviceID, appVersion, accountID, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR), new Callback<AccountDetails>() {
            @Override
            public void success(final AccountDetails accountDetails, Response response) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        // SORTING GROUPS BY DATE
                        Collections.sort(accountDetails.getAccountEntries(), new Comparator<JournalEntryDetails>() {
                            public int compare(JournalEntryDetails item2, JournalEntryDetails item1) {
                                return item1.getEntryDate().compareToIgnoreCase(item2.getEntryDate());
                            }
                        });
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNT_STATEMENT_FOR_MONTH ]  >>  Success");
                        callback.onComplete(true, 200, "Success", date, accountDetails);
                    }
                }.execute();
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNT_STATEMENT_FOR_MONTH ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", date, null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNT_STATEMENT_FOR_MONTH ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", date, null);
                }
            }
        });
    }

    public void getAccountStatementForMonth(final String accountID, final String date, final AccountSummaryCallback callback){
        Calendar calendar = DateMethods.getCalendarFromDate(date);
        retrofitInterface.getAccountStatementForMonth(deviceID, appVersion, accountID, calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR), new Callback<AccountDetails>() {
            @Override
            public void success(final AccountDetails accountDetails, Response response) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        // SORTING GROUPS BY DATE
                        Collections.sort(accountDetails.getAccountEntries(), new Comparator<JournalEntryDetails>() {
                            public int compare(JournalEntryDetails item2, JournalEntryDetails item1) {
                                return item1.getEntryDate().compareToIgnoreCase(item2.getEntryDate());
                            }
                        });
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNT_STATEMENT_FOR_MONTH ]  >>  Success");
                        callback.onComplete(true, 200, "Success", date, accountDetails);
                    }
                }.execute();
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNT_STATEMENT_FOR_MONTH ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", date, null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ ACCOUNT_STATEMENT_FOR_MONTH ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", date, null);
                }
            }
        });
    }


    public void getJournalEntryDetails(final String journalEntryID, final JournalEntryDetailsCallback callback){

        retrofitInterface.getJournalEntryDetails(deviceID, appVersion, journalEntryID, new Callback<JournalEntryCompleteDetails>() {
            @Override
            public void success(JournalEntryCompleteDetails journalEntryCompleteDetails, Response response) {
                if ( journalEntryCompleteDetails != null ){
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ JOURNAL_ENTRY_DETAILS ]  >>  Success");
                    callback.onComplete(true, 200, "Success", journalEntryCompleteDetails);
                }else{
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ JOURNAL_ENTRY_DETAILS ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ JOURNAL_ENTRY_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ JOURNAL_ENTRY_DETAILS ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", null);
                }
            }
        });
    }

    public void createPendingTransaction(final RequestCreateOrUpdateTransaction requestObject, final File attachment, final AddTransactionCallback callback){

        //RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), new Gson().toJson(requestCreateOrUpdateTransaction));
        retrofitInterface.createPendingTransaction(deviceID, appVersion, requestObject.getTransactionTypeID(), requestObject.getDebitAccountID(), requestObject.getCreditAccountID(), requestObject.getAmount(), requestObject.getEntryDate(), requestObject.getComments(), attachment!=null?new TypedFile("multipart/form-data", attachment):null, new Callback<RequestCreateOrUpdateTransaction>() {
            @Override
            public void success(RequestCreateOrUpdateTransaction responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ CREATE_PENDING_TRANSACTION ]  >>  Success");
                callback.onComplete(true, 200, "Success", responseObject);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ CREATE_PENDING_TRANSACTION ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ CREATE_PENDING_TRANSACTION ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", null);
                }
            }
        });
    }

    public void updatePendingTransaction(final String transactionID, final RequestCreateOrUpdateTransaction requestObject, final boolean removeAttachment, final File attachment, final AddTransactionCallback callback){

        retrofitInterface.updatePendingTransaction(deviceID, appVersion, transactionID, requestObject.getTransactionTypeID(), requestObject.getDebitAccountID(), requestObject.getCreditAccountID(), requestObject.getAmount(), requestObject.getEntryDate(), requestObject.getComments(), attachment!=null?false:removeAttachment, attachment!=null?new TypedFile("multipart/form-data", attachment):null, new Callback<RequestCreateOrUpdateTransaction>() {
            @Override
            public void success(RequestCreateOrUpdateTransaction responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ UPDATE_PENDING_TRANSACTION ]  >>  Success");
                callback.onComplete(true, 200, "Success", responseObject);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ UPDATE_PENDING_TRANSACTION ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ UPDATE_PENDING_TRANSACTION ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", null);
                }
            }
        });
    }

    public void deletePendingTransaction(final String transactionID, final StatusCallback callback){

        retrofitInterface.deletePendingTransaction(deviceID, appVersion, transactionID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ DELETE_PENDING_TRANSACTION ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ DELETE_PENDING_TRANSACTION ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error");
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ DELETE_PENDING_TRANSACTION ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error");
                }
            }
        });
    }

    public void getPendingTransactions(final TransactionsListCallback callback){

        retrofitInterface.getPendingTransactions(deviceID, appVersion, new Callback<List<TransactionDetails>>() {
            @Override
            public void success(List<TransactionDetails> transactionsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ PENDING_TRANSACTIONS ]  >>  Succcess");
                callback.onComplete(true, 200, "Success", transactionsList);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ PENDING_TRANSACTIONS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ PENDING_TRANSACTIONS ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", null);
                }
            }
        });

    }

    public void getPendingTransactionDetails(final String transactionID, final TransactionDetailsCallback callback){

        retrofitInterface.getPendingTransactionDetails(deviceID, appVersion, transactionID, new Callback<TransactionDetails>() {
            @Override
            public void success(TransactionDetails transactionDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ PENDING_TRANSACTION_DETAILS ]  >>  Succcess");
                callback.onComplete(true, 200, "Success", transactionDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ PENDING_TRANSACTION_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ PENDING_TRANSACTION_DETAILS ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error", null);
                }
            }
        });

    }

    public void approvePendingTransaction(final String transactionID, final StatusCallback callback){

        retrofitInterface.approvePendingTransaction(deviceID, appVersion, transactionID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ APPROVE_PENDING_TRANSACTION ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ APPROVE_PENDING_TRANSACTION ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    callback.onComplete(false, error.getResponse().getStatus(), "Error");
                } catch (Exception e) {
                    Log.d(Constants.LOG_TAG, "\nFINANCE_API : [ APPROVE_PENDING_TRANSACTION ]  >>  Error = " + error.getKind());
                    callback.onComplete(false, -1, "Error");
                }
            }
        });
    }

    public void getFinanceDetails(final FinanceDetailsCallback callback) {

        new CountDownTimer(1400, 100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                List<EmployeeDetails> employeesList = new ArrayList<EmployeeDetails>();
                List<PurposeDetails> purposeList = new ArrayList<PurposeDetails>();
                List<PurposeDetails> detailsList = new ArrayList<PurposeDetails>();
                for (int i=1;i<6;i++){
                    employeesList.add(new EmployeeDetails("employee_"+i+"_id", "Employee - "+i));
                    purposeList.add(new PurposeDetails("purpose_"+i+"_id", "Purpose - "+i));
                    detailsList.add(new PurposeDetails("detail_"+i+"_id", "Detail - "+i));
                }

                PrefillDetails prefillDetails = new PrefillDetails();
                prefillDetails.setEmployeesList(employeesList).setPurposeList(purposeList).setDetailsList(detailsList);

                callback.onComplete(true, 200, "Success", prefillDetails);

            }
        }.start();
    }

}
