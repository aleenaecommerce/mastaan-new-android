package com.aleenaecommerce.operations.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.aleenaecommerce.operations.backend.models.RequestLogin;
import com.aleenaecommerce.operations.backend.models.ResponseCreateAuthPassword;
import com.aleenaecommerce.operations.backend.models.ResponseLogin;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.methods.CheckError;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class AuthenticationAPI {

    Context context;
    String deviceID;
    int appVersion;
    //OperationsToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface LoginCallback {
        void onComplete(boolean status, int statusCode, String message, String token);//, EmployeeDetails userDetails);
    }

    public AuthenticationAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        //this.activity = (OperationsToolBarActivity) context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void login(final String mobileNumber, final String deviceID, final String deviceName, final LoginCallback callback) {

        retrofitInterface.login(deviceID, appVersion, new RequestLogin(mobileNumber, deviceID, deviceName), new Callback<ResponseLogin>() {
            @Override
            public void success(ResponseLogin responseObjectLogin, Response response) {
                if (responseObjectLogin != null && responseObjectLogin.getToken() != null) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  Success");
                    callback.onComplete(true, 200, "Success", responseObjectLogin.getToken());//, responseObjectLogin.getUserDetails());
                }else {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  Success");
                    callback.onComplete(false, 500, "Something went wrong, try again!", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).ignoreSessionValidity().check(error) == false ) {
                    int statusCode = -1;
                    String errorMessage = "Something went wrong, try again!";
                    try {
                        statusCode = error.getResponse().getStatus();
                        String response_object = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_object);
                        errorMessage = jsonResponse.getString("msg");
                    } catch (Exception e) {e.printStackTrace();}

                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  Error = " + error.getKind() + " , StatusCode = " + statusCode);
                    callback.onComplete(false, statusCode, errorMessage, null);
                }
            }
        });
    }


    public void logout(final StatusCallback callback) {

        retrofitInterface.logout(deviceID, appVersion, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGOUT ]  >>  Success");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGOUT ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGOUT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGOUT ]  >>  ERROR");
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void createAuthPassword(final StatusCallback callback){

        retrofitInterface.createAuthPassword(deviceID, appVersion, new Callback<ResponseCreateAuthPassword>() {
            @Override
            public void success(ResponseCreateAuthPassword responseCreateAuthPassword, Response response) {
                if ( responseCreateAuthPassword.getAuthCode() != null && responseCreateAuthPassword.getAuthCode().length() > 0 ) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ CREATE_AUTH_PASSWORD ]  >>  SUCCESS");
                    callback.onComplete(true, 200, responseCreateAuthPassword.getAuthCode());
                }else{
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ CREATE_AUTH_PASSWORD ]  >>  FAILURE");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ CREATE_AUTH_PASSWORD ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ CREATE_AUTH_PASSWORD ]  >>  ERROR");
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }
}
