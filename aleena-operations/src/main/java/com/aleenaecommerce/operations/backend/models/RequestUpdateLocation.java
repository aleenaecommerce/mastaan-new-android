package com.aleenaecommerce.operations.backend.models;

import com.aleenaecommerce.operations.models.LocationDetails;

import java.util.List;

/**
 * Created by venkatesh on 9/8/16.
 */
public class RequestUpdateLocation {
    List<LocationDetails> locationQueues;

    public RequestUpdateLocation(List<LocationDetails> locationQueues){
        this.locationQueues = locationQueues;
    }
}
