package com.aleenaecommerce.operations.backend.models;

import com.aleenaecommerce.operations.models.VehicleDetails;

import java.util.List;


public class RequestAddOrEditEmployee { //extends CustomPlace {
    String f;       // First Name;
    String l;       // Last Name
    String m;       // Primary Mobile
    String em;      // Emergency Mobile / Alternate Mobile
    String e;       // Email Address

    String ps;      // Password

    List<String> r; // Roles

    String p;       // Image
    String t;       // Thumbnail Image
    String bg;      // Blood Group
    String g;       // Gender
    String d;       // DOB

    String et;      // Employment Type
    String pt;      // Payment Type
    double sal;     // Salary
    double hsal;    // Hourly Salary
    //String ot;      // Overtime
    double ots;     // Overtime Salary

    String esi;     // ESI Number
    String dl;      // Driving Liscense Number
    VehicleDetails vd;

    //String a;       // Address
    //String loc;     // Address LatLng
    String a1;      // Address Line1
    String a2;      // Address Line2
    String lmk;     // Landmark
    String ar;      // Area
    String ci;      // City
    String s;       // State
    String pin;     // Pincode




    public RequestAddOrEditEmployee(){}

    public RequestAddOrEditEmployee(String firstName, String lastName, String mobileNumber, String alternateMobileNumber, String emailAddress
                , List<String> roles
                , String password
                , String eSINumber, String drivingLicenseNumber
                , String vehicleRegistrationNumber, String vehicleOwnType, String vehicleInsuranceProvider, String vehicleIsuranceNumber
                , String addressLine1, String addressLine2, String area, String landmark, String city, String state, String pincode
    ){

        this.f = firstName;
        this.l = lastName;
        this.m = mobileNumber;
        this.em = alternateMobileNumber;
        this.e = emailAddress;

        this.r = roles;

        this.p = password;

        this.esi = eSINumber;
        this.dl = drivingLicenseNumber;

        this.vd = new VehicleDetails(vehicleRegistrationNumber, vehicleOwnType, vehicleInsuranceProvider, vehicleIsuranceNumber);

        this.a1 = addressLine1;
        this.a2 = addressLine2;
        this.ar = area;
        this.lmk = landmark;
        this.ci = city;
        this.s = state;
        this.pin = pincode;
    }


    public String getFirstName() {
        return f == null ? "" : f;
    }

    public String getLastName() {
        return l == null ? "" : l;
    }

    public String getMobile() {
        return m;
    }

    public String getAlternateMobile() {
        return em;
    }

    public String getEmail() {
        return e;
    }

    public String getImage() {
        return p;
    }

    public String getImageThumbnail() {
        return t;
    }

    public String getPassword() {
        return p;
    }

    public List<String> getRoles() {
        return r;
    }

    public String getBloodGroup() {
        return bg;
    }

    public String getAddressLine1() {
        return a1;
    }

    public String getAddressLine2() {
        return a2;
    }

    public String getArea() {
        return ar;
    }

    public String getLandMark() {
        return lmk;
    }

    public String getCity() {
        return ci;
    }

    public String getState() {
        return s;
    }

    public String getPincode() {
        return pin;
    }

    public String getESINumber() {
        return esi;
    }

    public String getDrivingLicenseNumber() {
        return dl;
    }

    public VehicleDetails getVehicleDetails() {
        return vd;
    }


}
