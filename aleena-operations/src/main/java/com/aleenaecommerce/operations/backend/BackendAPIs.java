package com.aleenaecommerce.operations.backend;

import android.content.Context;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.localdata.LocalStorageData;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class BackendAPIs {

    Context context;
    String deviceID;
    int appVersion;
    //OperationsToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    RestAdapter restAdapter;
    //RetrofitInterface retrofitInterface;

    CommonAPI commonAPI;
    AuthenticationAPI authenticationAPI;
    AttendanceAPI attendanceAPI;
    EmployeesAPI employeesAPI;
    LocationAPI locationAPI;
    FinanceAPI financeAPI;

    public BackendAPIs(Context context, String deviceID, int appVersion){
        this.context = context;
        //this.activity = (OperationsToolBarActivity) context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
    }

    private RestAdapter getRestAdapter(){
        if ( restAdapter == null ){
            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(context.getString(R.string.base_url))
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            request.addHeader("x-access-token", new LocalStorageData(context).getAccessToken());
                        }
                    })
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            //retrofitInterface = restAdapter.create(RetrofitInterface.class);
        }
        return restAdapter;
    }

    public CommonAPI getCommonAPI() {
        if ( commonAPI == null ){
            commonAPI = new CommonAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return commonAPI;
    }

    public AuthenticationAPI getAuthenticationAPI() {
        if ( authenticationAPI == null ){
            authenticationAPI = new AuthenticationAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return authenticationAPI;
    }

    public AttendanceAPI getAttendanceAPI() {
        if ( attendanceAPI == null ){
            attendanceAPI = new AttendanceAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return attendanceAPI;
    }

    public EmployeesAPI getEmployeesAPI() {
        if ( employeesAPI == null ){
            employeesAPI = new EmployeesAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return employeesAPI;
    }

    public LocationAPI getLocationAPI() {
        if ( locationAPI == null ){
            locationAPI = new LocationAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return locationAPI;
    }

    public FinanceAPI getFinanceAPI() {
        if ( financeAPI == null ){
            financeAPI = new FinanceAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return financeAPI;
    }
}
