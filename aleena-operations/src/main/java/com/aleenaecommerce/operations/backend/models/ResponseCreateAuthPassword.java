package com.aleenaecommerce.operations.backend.models;

/**
 * Created by venkatesh on 30/7/16.
 */
public class ResponseCreateAuthPassword {
    String authCode;

    public String getAuthCode() {
        if ( authCode != null && authCode.length() > 0 ){   return authCode;    }
        return "";
    }
}
