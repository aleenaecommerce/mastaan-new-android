package com.aleenaecommerce.operations.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.aleena.common.models.WifiDetails;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.methods.CheckError;
import com.aleenaecommerce.operations.models.AppDetails;
import com.aleenaecommerce.operations.models.BootstrapDetails;
import com.aleenaecommerce.operations.models.WarehouseDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class CommonAPI {

    Context context;
    String deviceID;
    int appVersion;
    //OperationsToolBarActivity activity;
    RetrofitInterface retrofitInterface;

    public interface BootstrapCallback {
        void onComplete(boolean status, int statusCode, BootstrapDetails bootstrapDetails);
    }

    public interface AppsListCallback {
        void onComplete(boolean status, int statusCode, String message, List<AppDetails> appsList);
    }

    public interface WarehousesListCallback {
        void onComplete(boolean status, int statusCode, String message, List<WarehouseDetails> deliveryBoysList);
    }


    public interface AddWifiCallback{
        void onComplete(boolean status, int statusCode, String message, WifiDetails addedwifiDetails);
    }

    public CommonAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        //this.activity = (OperationsToolBarActivity) context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getBootStrap(final BootstrapCallback callback) {

        retrofitInterface.getBootstrap(deviceID, appVersion, new Callback<BootstrapDetails>() {
            @Override
            public void success(BootstrapDetails bootstrap, Response response) {
                Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ GET_BOOTSTRAP ]  >>  Success");
                callback.onComplete(true, 200, bootstrap);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).ignoreMaintenance().ignoreSessionValidity().check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ GET_BOOTSTRAP ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ GET_BOOTSTRAP ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getAppsList(final AppsListCallback callback) {

        retrofitInterface.getApps(deviceID, appVersion, new Callback<List<AppDetails>>() {
            @Override
            public void success(List<AppDetails> appsList, Response response) {
                if (appsList != null) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ APPS_LIST ]  >>  Success");
                    callback.onComplete(true, 200, "Success", appsList);
                } else {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ APPS_LIST ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ APPS_LIST ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ APPS_LIST ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addWifi(final WifiDetails wifiDetails, final AddWifiCallback callback){

        retrofitInterface.addWifi(deviceID, appVersion, wifiDetails, new Callback<WifiDetails>() {
            @Override
            public void success(WifiDetails addedwifiDetails, Response response) {
                if ( addedwifiDetails != null ){
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ ADD_WIFI ]  >>  Success");
                    callback.onComplete(true, 200, "Success", addedwifiDetails);
                }else{
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ ADD_WIFI ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ ADD_WIFI ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ ADD_WIFI ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void deleteWifi(final String networkID, final StatusCallback callback){

        retrofitInterface.deleteWifi(deviceID, appVersion, networkID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ DELETE_WIFI ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ DELETE_WIFI ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ DELETE_WIFI ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getWarehouses(final WarehousesListCallback callback) {

        retrofitInterface.getWarehouses(deviceID, appVersion, new Callback<List<WarehouseDetails>>() {
            @Override
            public void success(final List<WarehouseDetails> warehousesList, Response response) {
                callback.onComplete(true, 200, "Success", warehousesList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }


}
