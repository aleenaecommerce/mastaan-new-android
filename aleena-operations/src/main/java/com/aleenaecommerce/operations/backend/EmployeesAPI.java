package com.aleenaecommerce.operations.backend;

import android.content.Context;
import android.os.AsyncTask;

import com.aleenaecommerce.operations.activities.OperationsToolBarActivity;
import com.aleenaecommerce.operations.backend.models.RequestAddOrEditEmployee;
import com.aleenaecommerce.operations.methods.CheckError;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class EmployeesAPI {

    Context context;
    OperationsToolBarActivity activity;
    String deviceID;
    int appVersion;
    RetrofitInterface retrofitInterface;

    public interface EmployeeDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, EmployeeDetails employeeDetails);
    }

    public interface EmployeesListCallback {
        void onComplete(boolean status, int statusCode, String message, List<EmployeeDetails> deliveryBoysList);
    }


    public EmployeesAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        try{ this.activity = (OperationsToolBarActivity) context; }catch (Exception e){}
        this.deviceID = deviceID;
        this.appVersion = appVersion;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }


    public void addEmployee(final RequestAddOrEditEmployee requestObject, final EmployeeDetailsCallback callback){

        retrofitInterface.addEmployee(deviceID, appVersion, requestObject, new Callback<EmployeeDetails>() {
            @Override
            public void success(EmployeeDetails employeeDetails, Response response) {
                callback.onComplete(true, 200, "Success", employeeDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateEmployee(final String employeeID, final RequestAddOrEditEmployee requestObject, final EmployeeDetailsCallback callback){

        retrofitInterface.updateEmployee(deviceID, appVersion, employeeID, requestObject, new Callback<EmployeeDetails>() {
            @Override
            public void success(EmployeeDetails employeeDetails, Response response) {
                callback.onComplete(true, 200, "Success", employeeDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getEmployees(final EmployeesListCallback callback) {

        retrofitInterface.getEmployees(deviceID, appVersion, new Callback<List<EmployeeDetails>>() {
            @Override
            public void success(final List<EmployeeDetails> deliveryBoys, Response response) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        Collections.sort(deliveryBoys, new Comparator<EmployeeDetails>() {
                            public int compare(EmployeeDetails item1, EmployeeDetails item2) {
                                return item1.getFullName().compareToIgnoreCase(item2.getFullName());
                            }
                        });

                        //-----

                        if ( activity != null ) {
                            for (int i = 0; i < deliveryBoys.size(); i++) {
                                if (deliveryBoys.get(i).getAttendanceDetails() != null) {
                                    LatLng latLng = deliveryBoys.get(i).getAttendanceDetails().getLastCheckInPlaceLatLng();
                                    if (latLng != null && latLng.latitude != 0 && latLng.longitude != 0) {
                                        deliveryBoys.get(i).getAttendanceDetails().setLastCheckInPlaceDetails(activity.getGooglePlacesAPI().getPlaceDetailsByLatLng(latLng).getItem());
                                    }
                                }
                            }
                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        callback.onComplete(true, 200, "Success", deliveryBoys);
                    }
                }.execute();
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

}
