package com.aleenaecommerce.operations.models;

import java.util.Date;

/**
 * Created by Venkatesh Uppu on 19-07-2016.
 */

public class EmployeeDay {

    Date date;
    AttendanceDetails attendanceDetails;

    public EmployeeDay(Date date, AttendanceDetails attendanceDetails) {
        this.date = date;
        this.attendanceDetails = attendanceDetails;
    }

    public Date getDate() {
        return date;
    }

    public AttendanceDetails getAttendanceDetails() {
        return attendanceDetails;
    }
}
