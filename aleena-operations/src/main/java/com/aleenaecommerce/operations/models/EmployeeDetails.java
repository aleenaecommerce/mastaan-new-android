package com.aleenaecommerce.operations.models;

import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;


public class EmployeeDetails { //extends CustomPlace {
    String _id;

    String f;       // First Name;
    String l;       // Last Name
    String m;       // Primary Mobile
    String em;      // Emergency Mobile / Alternate Mobile
    String e;       // Email Address

    String p;       // Image
    String t;       // Thumbnail Image
    String bg;      // Blood Group
    String g;       // Gender
    String d;       // DOB

    String et;      // Employment Type
    String pt;      // Payment Type
    double sal;     // Salary
    double hsal;    // Hourly Salary
    //String ot;      // Overtime
    double ots;     // Overtime Salary

    String ps;      // Password

    String esi;     // ESI Number
    String dl;      // Driving Liscense Number
    VehicleDetails vd;

    //String a;       // Address
    //String loc;     // Address LatLng
    String a1;      // Address Line1
    String a2;      // Address Line2
    String lmk;     // Landmark
    String ar;      // Area
    String ci;      // City
    String s;       // State
    String pin;     // Pincode

    List<String> r;     // Roles List

    AttendanceDetails at;
    Schedule sch;       // Schedules List
    List<Break> brs;    //  Breaks List

    class Schedule{
        List<String> sun;
        List<String> mon;
        List<String> tue;
        List<String> wed;
        List<String> thu;
        List<String> fri;
        List<String> sat;
    }
    class Break{
        String t;
        int p;
    }


    public EmployeeDetails(){}

    public EmployeeDetails(String id, String name){
        this._id = id;
        this.f = name;
    }


    public String getID() {
        if ( _id != null ){ return _id; }
        return "";
    }

    public String getFirstName() {
        return f == null ? "" : f;
    }
    public String getLastName() {
        return l == null ? "" : l;
    }
    public String getFullName() {
        if ( l != null && l.length() > 0 ){
            return f+" "+l;
        }
        return getFirstName();
    }

    public String getMobile() {
        return m;
    }

    public String getLast4NumbersOfMobileNumber(){
        if ( m != null && m.length() > 4 ){
            return m.substring(m.length() - 4);
        }
        return "";
    }
    public String getAlternateMobile() {
        return em;
    }

    public String getEmail() {
        return e;
    }

    public String getImage() {
        return p;
    }
    public String getImageThumbnail() {
        return t;
    }

    public String getPassword() {
        return ps;
    }

    public String getBloodGroup() {
        return bg;
    }

    public String getAddressLine1() {
        return a1;
    }

    public String getAddressLine2() {
        return a2;
    }

    public String getArea() {
        return ar;
    }

    public String getLandMark() {
        return lmk;
    }

    public String getCity() {
        return ci;
    }

    public String getState() {
        return s;
    }

    public String getPincode() {
        return pin;
    }


    public String getESINumber() {
        return esi;
    }

    public String getDrivingLicenseNumber() {
        return dl;
    }

    public VehicleDetails getVehicleDetails() {
        if ( vd == null ){  vd = new VehicleDetails();  }
        return vd;
    }

    public List<ScheduleDetails> getSchedulesList(){
        List<ScheduleDetails> scheduleList = new ArrayList<>();
        if ( sch != null ){
            if ( sch.sun != null ){
                if ( sch.sun.size() == 2 ){
                    scheduleList.add(new ScheduleDetails("Sunday", sch.sun.get(0), sch.sun.get(1), getBreaksList()));
                }else if ( sch.sun.size() == 0 ){
                    scheduleList.add(new ScheduleDetails("Sunday", true));
                }
            }
            if ( sch.mon != null ){
                if ( sch.mon.size() == 2 ){
                    scheduleList.add(new ScheduleDetails("Monday", sch.mon.get(0), sch.mon.get(1), getBreaksList()));
                }else if ( sch.mon.size() == 0 ){
                    scheduleList.add(new ScheduleDetails("Monday", true));
                }
            }
            if ( sch.tue != null ){
                if ( sch.tue.size() == 2 ){
                    scheduleList.add(new ScheduleDetails("Tuesday", sch.tue.get(0), sch.tue.get(1), getBreaksList()));
                }else if ( sch.tue.size() == 0 ){
                    scheduleList.add(new ScheduleDetails("Tuesday", true));
                }
            }
            if ( sch.wed != null ){
                if ( sch.wed.size() == 2 ){
                    scheduleList.add(new ScheduleDetails("Wednesday", sch.wed.get(0), sch.wed.get(1), getBreaksList()));
                }else if ( sch.wed.size() == 0 ){
                    scheduleList.add(new ScheduleDetails("Wednesday", true));
                }
            }
            if ( sch.thu != null ){
                if ( sch.thu.size() == 2 ){
                    scheduleList.add(new ScheduleDetails("Thursday", sch.thu.get(0), sch.thu.get(1), getBreaksList()));
                }else if ( sch.thu.size() == 0 ){
                    scheduleList.add(new ScheduleDetails("Thursday", true));
                }
            }
            if ( sch.fri != null ){
                if ( sch.fri.size() == 2 ){
                    scheduleList.add(new ScheduleDetails("Friday", sch.fri.get(0), sch.fri.get(1), getBreaksList()));
                }else if ( sch.fri.size() == 0 ){
                    scheduleList.add(new ScheduleDetails("Friday", true));
                }
            }
            if ( sch.sat != null ){
                if ( sch.sat.size() == 2 ){
                    scheduleList.add(new ScheduleDetails("Saturday", sch.sat.get(0), sch.sat.get(1), getBreaksList()));
                }else if ( sch.sat.size() == 0 ){
                    scheduleList.add(new ScheduleDetails("Saturday", true));
                }
            }
        }
        return scheduleList;
    }

    public List<BreakDetails> getBreaksList() {
        List<BreakDetails> breaksList = new ArrayList<>();

        if ( brs != null && brs.size() > 0 ){
            for (int i=0;i<brs.size();i++){
                if ( brs.get(i) != null && (brs.get(i).t != null && brs.get(i).t.length() > 0 && brs.get(i).p > 0 )  ){
                    breaksList.add(new BreakDetails(brs.get(i).t, brs.get(i).p));
                }
            }
        }
        return breaksList;
    }

    public void setAttendanceDetails(AttendanceDetails attendanceDetails) {
        this.at = attendanceDetails;
    }
    public AttendanceDetails getAttendanceDetails() {
        if ( at == null ){  return new AttendanceDetails(); }
        return at;
    }

    public String getLastCheckedInTime() {
        if ( at != null ){ return at.getLastCheckInTime();  }
        return "";
    }

    public String getLastCheckedOutTime() {
        if ( at != null ){ return at.getLastCheckOutTime();  }
        return "";
    }

    public boolean getCheckInStatus() {
        if ( at != null ){
            return at.getCheckInStatus();
        }
        return false;
    }


    public String getPaymentType() {
        return pt;
    }

    public double getSalary() {
        return sal;
    }

    public double getHourlySalary() {
        return hsal;
    }

    public double getOverTimeSalary() {
        return ots;
    }

    public String getEmploymentType() {
        if ( et == null ){  et = "";    }
        return et;
    }
    public boolean isFullTimeEmployee() {
        if ( et == null || et.equalsIgnoreCase("ft")) {
            return true;
        }
        return false;
    }
    public boolean isFreeLancer() {
        if ( et != null || et.equalsIgnoreCase("fl")) {
            return true;
        }
        return false;
    }


    public List<String> getRoles() {
        if ( r == null ){ return new ArrayList<>(); }
        return r;
    }
    public boolean isReadOnly(){
        if ( getRoles().contains("r") ){    return  true;   }
        return false;
    }
    public boolean isDeliveryBoy(){
        if ( getRoles().contains("db") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }
    public boolean isProcessingManager(){
        if ( getRoles().contains("pm") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }
    public boolean isDeliveryBoyManager(){
        if ( getRoles().contains("dbm") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }
    public boolean isWarehouseManager(){
        if ( getRoles().contains("wm") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }
    public boolean isSuperUser(){
        if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }
    public boolean isAccountingManager(){
        if ( getRoles().contains("ac") ){    return  true;   }
        else if ( getRoles().contains("ac.su") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }
    public boolean isAccountsApprovalManager(){
        if ( getRoles().contains("ac.ap") ){    return  true;   }
        else if ( getRoles().contains("ac.su") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }
    public boolean isAccountsSuperUser(){
        if ( getRoles().contains("ac.su") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }



}
