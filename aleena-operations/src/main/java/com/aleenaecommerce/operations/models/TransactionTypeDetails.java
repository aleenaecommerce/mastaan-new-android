package com.aleenaecommerce.operations.models;

/**
 * Created by venkatesh on 23/7/16.
 */
public class TransactionTypeDetails {
    String _id;
    String n;
    String des;

    public TransactionTypeDetails(){}

    public TransactionTypeDetails(String id, String name){
        this._id = id;
        this.n = name;
    }

    public String getID() {
        return _id;
    }

    public String getName() {
        if ( n != null ){   return  n;  }
        return "";
    }

    public String getDescription() {
        if ( des != null ){   return  des;  }
        return "";
    }
}
