package com.aleenaecommerce.operations.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 3/8/16.
 */
public class ListObject {

    List<AccountDetails> accountsList;

    public ListObject(List<AccountDetails> accountsList){
        this.accountsList = accountsList;
    }

    public List<AccountDetails> getAccountsList() {
        if ( accountsList != null ){    return accountsList;    }
        return new ArrayList<>();
    }
}
