package com.aleenaecommerce.operations.models;

import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.WifiDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 15/12/15.
 */
public class BootstrapDetails {
    String serverTime;

    EmployeeDetails user;

    String upgradeURL;

    List<WifiDetails> wifiNetworks;

    public String getServerTime() {
        return DateMethods.getReadableDateFromUTC(serverTime);
    }

    public String getUpgradeURL() {
        return upgradeURL;
    }

    public EmployeeDetails getUserDetails() {
        return user;
    }

    public List<WifiDetails> getWifiNetworks() {
        if ( wifiNetworks == null ){    return new ArrayList<>();   }
        return wifiNetworks;
    }
}
