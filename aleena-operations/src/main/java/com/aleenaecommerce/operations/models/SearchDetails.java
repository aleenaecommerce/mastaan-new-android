package com.aleenaecommerce.operations.models;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;

/**
 * Created by venkatesh on 12/7/16.
 */
public class SearchDetails {

    String action;

    String status;

    String userName;

    String transactionDate;
    String transactionType;
    String transactionName;
    String creatorName;
    String approverName;

    String fromAccount;
    String toAccount;

    double minAmount = -1;
    double maxAmount = -1;

    String comments;

    String status_type;
    String delay_type;
    String schedule_type;

    boolean considerAtleastOneMatch;

    public SearchDetails(){}

    public boolean isSearchable(){
        if ( (status != null && status.trim().length() > 0)
                || (userName != null && userName.length() > 0)
                || (transactionDate != null && transactionDate.length() > 0)
                || (transactionType != null && transactionType.length() > 0)
                || (transactionName != null && transactionName.length() > 0)
                || (creatorName != null && creatorName.length() > 0)
                || (approverName != null && approverName.length() > 0)
                || (fromAccount != null && fromAccount.length() > 0)
                || (toAccount != null && toAccount.length() > 0)
                || minAmount != -1
                || maxAmount != -1
                || (comments != null && comments.length() > 0)
                || (status_type != null && status_type.trim().length() > 0)
                || (delay_type != null && delay_type.trim().length() > 0)
                || (schedule_type != null && schedule_type.trim().length() > 0)
                ){
            return true;
        }
        return false;
    }

    public boolean isConsiderAtleastOneMatch() {
        return considerAtleastOneMatch;
    }

    public SearchDetails setConsiderAtleastOneMatch(String searchName) {
        this.considerAtleastOneMatch = true;
        this.userName = searchName;
        this.creatorName = searchName;
        this.approverName = searchName;
        this.transactionName = searchName;
        this.fromAccount = searchName;
        this.toAccount = searchName;
        this.comments = searchName;
        return this;
    }

    public String getSearchString(){
        String searchString = "";
        // FOR ATLEAST ONE MATCH
        if ( considerAtleastOneMatch ){
            searchString = "<b>"+CommonMethods.capitalizeFirstLetter(userName)+"</b>";
        }
        // FOR COMBINED MATCH
        else{
            String separatorText = ", ";

            if ( (userName != null && userName.trim().length() > 0) ){
                searchString += "User: <b>"+CommonMethods.capitalizeFirstLetter(userName)+"</b>";
            }
            if ( (transactionDate != null && transactionDate.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Date: <b>"+CommonMethods.capitalizeFirstLetter(transactionDate)+"</b>";
            }
            if ( (transactionType != null && transactionType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Transaction type: <b>"+CommonMethods.capitalizeFirstLetter(transactionType)+"</b>";
            }
            if ( (transactionName != null && transactionName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Transaction name: <b>"+CommonMethods.capitalizeFirstLetter(transactionName)+"</b>";
            }
            if ( (fromAccount != null && fromAccount.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "From Account: <b>"+CommonMethods.capitalizeFirstLetter(fromAccount)+"</b>";
            }
            if ( (toAccount != null && toAccount.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "To Account: <b>"+CommonMethods.capitalizeFirstLetter(toAccount)+"</b>";
            }
            if ( (creatorName != null && creatorName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Created by: <b>"+CommonMethods.capitalizeFirstLetter(creatorName)+"</b>";
            }
            if ( (approverName != null && approverName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Approved by: <b>"+CommonMethods.capitalizeFirstLetter(approverName)+"</b>";
            }
            if ( minAmount != -1 || maxAmount != -1 ){
                if ( minAmount != -1 && maxAmount == -1 ){
                    if ( searchString.length() > 0 ){   searchString += separatorText;    }
                    searchString += "Min. Amt: <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(minAmount)+"</b>";;
                }
                else if ( maxAmount != -1 && minAmount == -1 ){
                    if ( searchString.length() > 0 ){   searchString += separatorText;    }
                    searchString += "Max. Amt: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(maxAmount)+"</b>";;
                }
                else {
                    if ( searchString.length() > 0 ){   searchString += separatorText;    }
                    searchString += "Amount: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(minAmount)+" to "+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(maxAmount)+"</b>";;
                }
            }

            if ( (comments != null && comments.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Comments: <b>"+CommonMethods.capitalizeFirstLetter(comments)+"</b>";
            }
            if ( (status_type != null && status_type.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += ",";    }
                searchString += "Status: <b>"+CommonMethods.capitalizeFirstLetter(status_type)+"</b>";
            }
            if ( (delay_type != null && delay_type.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += ",";    }
                searchString += "Delay: <b>"+CommonMethods.capitalizeFirstLetter(delay_type)+"</b>";
            }
            if ( (schedule_type != null && schedule_type.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += ",";    }
                searchString += "Schedule: <b>"+CommonMethods.capitalizeFirstLetter(schedule_type)+"</b>";
            }
        }

        return searchString;
    }

    public SearchDetails setAction(String action) {
        this.action = action;
        return this;
    }

    public String getAction() {
        if ( action == null ){  return "";  }
        return action;
    }

    public SearchDetails setUserName(String name) {
        this.userName = name;
        return this;
    }

    public String getUserName() {
        return userName;
    }


    public SearchDetails setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public SearchDetails setTransactionType(String transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public SearchDetails setTransactionName(String transactionName) {
        this.transactionName = transactionName;
        return this;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public SearchDetails setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
        return this;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public SearchDetails setToAccount(String toAccount) {
        this.toAccount = toAccount;
        return this;
    }

    public String getToAccount() {
        return toAccount;
    }

    public SearchDetails setCreatorName(String creatorName) {
        this.creatorName = creatorName;
        return this;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public SearchDetails setApproverName(String approverName) {
        this.approverName = approverName;
        return this;
    }

    public String getApproverName() {
        return approverName;
    }


    public void setMinAmount(double minAmount) {
        this.minAmount = minAmount;
    }

    public double getMinAmount() {
        return minAmount;
    }

    public void setMaxAmount(double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public double getMaxAmount() {
        return maxAmount;
    }

    public SearchDetails setComments(String comments) {
        this.comments = comments;
        return this;
    }

    public String getComments() {
        return comments;
    }

    public SearchDetails setStatusType(String status_type) {
        this.status_type = status_type;
        return this;
    }

    public String getStatusType() {
        return status_type;
    }

    public SearchDetails setDelayType(String delay_type) {
        this.delay_type = delay_type;
        return this;
    }

    public String getDelayType() {
        return delay_type;
    }

    public SearchDetails setScheduleType(String schedule_type) {
        this.schedule_type = schedule_type;
        return this;
    }

    public String getScheduleType() {
        return schedule_type;
    }
}
