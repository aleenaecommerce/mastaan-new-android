package com.aleenaecommerce.operations.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 23/7/16.
 */
public class PrefillDetails {

    List<EmployeeDetails> employeesList;
    List<PurposeDetails> purposeList;
    List<PurposeDetails> detailsList;

    List<TransactionTypeDetails> transactionTypesList;
    List<AccountDetails> creditAccountsList;
    List<AccountDetails> debitAccountsList;


    public PrefillDetails setEmployeesList(List<EmployeeDetails> employeesList) {
        if ( employeesList != null && employeesList.size() > 0 ) {
            employeesList.add(0, new EmployeeDetails("select", "Select"));
        }else{
            employeesList = new ArrayList<>();
        }
        this.employeesList = employeesList;
        return this;
    }

    public List<EmployeeDetails> getEmployeesList() {
        if ( employeesList == null ){ return new ArrayList<>(); }
        return employeesList;
    }

    public List<String> getEmployeesStringsList() {
        List<String> employeesStringsList = new ArrayList<>();
        List<EmployeeDetails> employessList = getEmployeesList();
        for (int i=0;i<employessList.size();i++){
            employeesStringsList.add(employessList.get(i).getFullName());
        }
        return employeesStringsList;
    }

    public PrefillDetails setPurposeList(List<PurposeDetails> purposeList) {
        if ( purposeList != null && purposeList.size() > 0 ) {
            purposeList.add(0, new PurposeDetails("select", "Select"));
        }else{
            purposeList = new ArrayList<>();
        }
        this.purposeList = purposeList;
        return this;
    }

    public List<PurposeDetails> getPurposeList() {
        if ( purposeList == null ){ return new ArrayList<>(); }
        return purposeList;
    }

    public List<String> getPurposeStringsList() {
        List<String> purposeStringsList = new ArrayList<>();
        List<PurposeDetails> purposeList = getPurposeList();
        for (int i=0;i<purposeList.size();i++){
            purposeStringsList.add(purposeList.get(i).getName());
        }
        return purposeStringsList;
    }

    public PrefillDetails setDetailsList(List<PurposeDetails> detailsList) {
        if ( detailsList != null && detailsList.size() > 0 ) {
            detailsList.add(0, new PurposeDetails("select", "Select"));
        }else{
            detailsList = new ArrayList<>();
        }
        this.detailsList = detailsList;
        return this;
    }

    public List<PurposeDetails> getDetailsList() {
        if ( detailsList == null ){ return new ArrayList<>(); }
        return detailsList;
    }

    public List<String> getDetailsStringsList() {
        List<String> detailsStringsList = new ArrayList<>();
        List<PurposeDetails> detailsList = getDetailsList();
        for (int i=0;i<detailsList.size();i++){
            detailsStringsList.add(detailsList.get(i).getName());
        }
        return detailsStringsList;
    }

    //---

    public PrefillDetails setTransactionTypesList(List<TransactionTypeDetails> transactionTypesList) {
        if ( transactionTypesList == null ){ transactionTypesList = new ArrayList<>();  }
        if ( transactionTypesList.size() > 1 ) {
            transactionTypesList.add(0, new TransactionTypeDetails("select", "Select"));
        }
        this.transactionTypesList = transactionTypesList;
        return this;
    }

    public List<TransactionTypeDetails> getTransactionTypesList() {
        if ( transactionTypesList == null ){ return new ArrayList<>(); }
        return transactionTypesList;
    }

    public List<String> getTransactionTypeStringsList() {
        List<String> transactionStringsList = new ArrayList<>();
        List<TransactionTypeDetails> transactionList = getTransactionTypesList();
        for (int i=0;i<transactionList.size();i++){
            transactionStringsList.add(transactionList.get(i).getName());
        }
        return transactionStringsList;
    }

    //
    public PrefillDetails setCreditAccounstList(List<AccountDetails> creditAccountsList) {
        if ( creditAccountsList == null ){ creditAccountsList = new ArrayList<>();  }
        if ( creditAccountsList.size() > 1 ) {
            creditAccountsList.add(0, new AccountDetails("select", "Select"));
        }
        this.creditAccountsList = creditAccountsList;
        return this;
    }

    public List<AccountDetails> getCreditAccounstList() {
        if ( creditAccountsList == null ){ return new ArrayList<>(); }
        return creditAccountsList;
    }

    public List<String> getCreditAccountStringsList() {
        List<String> creditAccountStringsList = new ArrayList<>();
        List<AccountDetails> accountsList = getCreditAccounstList();
        for (int i=0;i<accountsList.size();i++){
            creditAccountStringsList.add(accountsList.get(i).getName());
        }
        return creditAccountStringsList;
    }

    //
    public PrefillDetails setDebitAccounstList(List<AccountDetails> debitAccountsList) {
        if ( debitAccountsList == null ){ debitAccountsList = new ArrayList<>();  }
        if ( debitAccountsList.size() > 1 ) {
            debitAccountsList.add(0, new AccountDetails("select", "Select"));
        }
        this.debitAccountsList = debitAccountsList;
        return this;
    }

    public List<AccountDetails> getDebitAccounstList() {
        if ( debitAccountsList == null ){ return new ArrayList<>(); }
        return debitAccountsList;
    }

    public List<String> getDebitAccountStringsList() {
        List<String> debitAccountStringsList = new ArrayList<>();
        List<AccountDetails> accountsList = getDebitAccounstList();
        for (int i=0;i<accountsList.size();i++){
            debitAccountStringsList.add(accountsList.get(i).getName());
        }
        return debitAccountStringsList;
    }


}
