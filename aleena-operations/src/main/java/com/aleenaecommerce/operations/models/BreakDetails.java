package com.aleenaecommerce.operations.models;

import com.aleena.common.methods.DateMethods;

/**
 * Created by venkatesh on 23/6/16.
 */
public class BreakDetails {
    String start_time;
    String end_time;
    int period;

    public BreakDetails(String start_time, int durationInMinutes){
        this.start_time = start_time;
        this.end_time = DateMethods.addToDateInMinutes(start_time, durationInMinutes);
        this.period = durationInMinutes;
    }

    public String getStartTime() {
        return start_time;
    }

    public String getEndTime() {
        return end_time;
    }

    public int getDurationInMinutes() {
        return period;
    }
}
