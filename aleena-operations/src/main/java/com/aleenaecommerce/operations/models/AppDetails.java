package com.aleenaecommerce.operations.models;

/**
 * Created by venkatesh on 22/6/16.
 */
public class AppDetails {
    String name;
    String image;
    String url;

    public AppDetails(String appName, String imageURL, String downloadURL){
        this.name = appName;
        this.image = imageURL;
        this.url = downloadURL;
    }

    public String getName() {
        return name;
    }

    public String getImageURL() {
        if ( image == null || image.length() == 0 ){    return "http://";   }
        return image;
    }

    public String getDownloadURL() {
        if ( url == null || url.length() == 0 ){    return "http://";   }
        return url;
    }
}
