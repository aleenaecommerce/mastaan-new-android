package com.aleenaecommerce.operations.models;

/**
 * Created by venkatesh on 28/7/16.
 */
public class JournalEntryTransactionDetails {
    String _id;
    AccountDetails acc;
    double amt;

    public JournalEntryTransactionDetails(String id, String accountID, double amount){
        this._id = id;
        this.acc = new AccountDetails(accountID, accountID);
        this.amt = amount;
    }

    public String getID() {
        return _id;
    }

    public String getAccountID() {
        if ( acc != null ){  return acc.getID();}
        return "";
    }

    public AccountDetails getAccountDetails() {
        if ( acc != null ){ return acc; }
        return new AccountDetails();
    }

    public double getAmount() {
        return amt;
    }
}
