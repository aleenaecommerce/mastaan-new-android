package com.aleenaecommerce.operations.models;

import android.location.Location;

import com.aleena.common.methods.DateMethods;

/**
 * Created by venkatesh on 9/8/16.
 */
public class LocationDetails {
    double lat;
    double lng;
    double acc;
    long ts;

    public LocationDetails(Location location, String timeStamp){
        if ( location != null ){
            this.lat = location.getLatitude();
            this.lng = location.getLongitude();
            this.acc = location.getAccuracy();
            this.ts = DateMethods.getDateInMilliSeconds(timeStamp);//System.currentTimeMillis();
        }
    }

    public LocationDetails(double latitude, double longitude, double accuracy, String timeStamp){
        this.lat = latitude;
        this.lng = longitude;
        this.acc = accuracy;
        this.ts = DateMethods.getDateInMilliSeconds(timeStamp);//System.currentTimeMillis();
    }
}
