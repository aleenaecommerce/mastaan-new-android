package com.aleenaecommerce.operations.models;

import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 28/7/16.
 */
public class JournalEntryCompleteDetails {
    String _id;     //  ID
    String des;     //  Description
    String ed;      //  Entry Date

    User cb; // Created By
    User ab; // Accepted By
    TransactionTypeDetails tid;
    List<JournalEntryTransactionDetails> trs;       // Transactions

    String com;
    String ath;

    class User{
        String _id;
        String f;
        String l;

        public String getID() {
            return _id;
        }

        public String getName(){
            if ( f != null && l != null ){
                return f.trim()+" "+l.trim();
            }
            return f;
        }
    }

    public String getID() {
        return _id;
    }

    public String getDescription() {
        return des;
    }

    public String getEntryDate() {
        return DateMethods.getReadableDateFromUTC(ed);
    }

    public List<JournalEntryTransactionDetails> getTransactions() {
        if ( trs != null ){ return  trs;  }
        return new ArrayList<>();
    }

    public TransactionTypeDetails getTransactionTypeDetails() {
        if ( tid != null ){ return tid; }
        return new TransactionTypeDetails();
    }

    public EmployeeDetails getCreatorDetails() {
        if ( cb != null ){  return new EmployeeDetails(cb.getID(), cb.getName());  }
        return new EmployeeDetails();
    }

    public EmployeeDetails getAcceptorDetails() {
        if ( ab != null ){  return new EmployeeDetails(ab.getID(), ab.getName());  }
        return new EmployeeDetails();
    }

    public String getComments() {
        return com;
    }

    public String getAttachment() {
        return ath;
    }

}
