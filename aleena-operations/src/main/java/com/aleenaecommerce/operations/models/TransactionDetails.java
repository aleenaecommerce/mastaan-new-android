package com.aleenaecommerce.operations.models;

import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.backend.models.RequestCreateOrUpdateTransaction;

import java.util.List;

/**
 * Created by venkatesh on 29/7/16.
 */
public class TransactionDetails {
    String _id;
    String cd;
    TransactionTypeDetails tid;
    AccountDetails cra;
    AccountDetails dra;
    double amt;
    String ed;
    String com;
    String ath;
    User cb;

    class User{
        String _id;
        String f;
        String l;

        public User(String id, String name){
            this._id = id;
            this.f = name;
        }

        public String getID() {
            if ( _id != null ){ return _id; }
            return "";
        }

        public String getName(){
            if ( f != null && l != null ){
                return f.trim()+" "+l.trim();
            }
            return f;
        }
    }

    public TransactionDetails(){}

    public TransactionDetails(RequestCreateOrUpdateTransaction requestCreateOrUpdateTransaction, List<TransactionTypeDetails> transactionTypes, List<AccountDetails> creditAccoutns, List<AccountDetails> debitAccoutns){
        this._id = requestCreateOrUpdateTransaction.getID();
        this.amt = requestCreateOrUpdateTransaction.getAmount();
        this.ed = requestCreateOrUpdateTransaction.getEntryDate();
        this.com = requestCreateOrUpdateTransaction.getComments();
        this.ath = requestCreateOrUpdateTransaction.getAttachment();
        for (int i=0;i<transactionTypes.size();i++){
            if ( transactionTypes.get(i).getID().equals(requestCreateOrUpdateTransaction.getTransactionTypeID())){
                this.tid = transactionTypes.get(i);
                break;
            }
        }
        for (int i=0;i<debitAccoutns.size();i++){
            if ( debitAccoutns.get(i).getID().equals(requestCreateOrUpdateTransaction.getDebitAccountID())){
                this.dra = debitAccoutns.get(i);
                break;
            }
        }
        for (int i=0;i<creditAccoutns.size();i++){
            if ( creditAccoutns.get(i).getID().equals(requestCreateOrUpdateTransaction.getCreditAccountID())){
                this.cra = creditAccoutns.get(i);
                break;
            }
        }
    }

    public String getID() {
        return _id;
    }

    public TransactionDetails setCreatedDate(String createdDate) {
        this.cd = createdDate;
        return this;
    }
    public String getCreatedDate() {
        return DateMethods.getReadableDateFromUTC(cd);
    }

    public void setID(String id) {
        this._id = id;
    }

    public TransactionTypeDetails getTransactionTypeDetails() {
        if ( tid != null ){ return tid; }
        return new TransactionTypeDetails();
    }

    public AccountDetails getCreditAccountDetails() {
        if ( cra != null ){     return cra;     }
        return new AccountDetails();
    }

    public AccountDetails getDebitAccountDetails() {
        if ( dra != null ){     return dra;     }
        return new AccountDetails();
    }

    public double getAmount() {
        return amt;
    }

    public String getEntryDate() {
        return DateMethods.getReadableDateFromUTC(ed);
    }

    public String getComments() {
        if ( com != null ){ return com; }
        return "";
    }

    public String getAttachment() {
        return ath;
    }

    public TransactionDetails setCreatorDetails(EmployeeDetails employeeDetails) {
        this.cb = new User(employeeDetails !=null? employeeDetails.getID():"", employeeDetails !=null? employeeDetails.getFullName():"");
        return this;
    }
    public EmployeeDetails getCreatorDetails() {
        if ( cb != null ){  return new EmployeeDetails(cb.getID(), cb.getName());  }
        return new EmployeeDetails();
    }
}
