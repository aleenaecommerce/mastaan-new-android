package com.aleenaecommerce.operations.models;

/**
 * Created by venkatesh on 23/7/16.
 */
public class PurposeDetails {
    String id;
    String name;

    public PurposeDetails(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String getID() {
        return id;
    }

    public String getName() {
        return name;
    }


}
