package com.aleenaecommerce.operations.models;

import com.aleenaecommerce.operations.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 28/7/16.
 */
public class AccountDetails extends CustomPlace{
    String _id;         //  ID
    String n;           //  Name
    String pa;          //  ParentAccount
    String ty;          //  Type ( Detbit / Credit )

    boolean ha;         //  Is HubAccount

    double bal;         //  Balance
    double obal;        //  Opening Balance

    String emp;

    List<JournalEntryDetails> entries;

    List<String> children;      // Children accounts ids

    public AccountDetails(){}

    public AccountDetails(String id, String name){
        this._id = id;
        this.n = name;
    }

    public String getID() {
        return _id;
    }

    public String getName() {
        if ( n != null ){   return  n;  }
        return "";
    }

    public String getParentAccount() {
        return pa;
    }

    public String getType() {
        if ( ty != null && ty.equalsIgnoreCase("dr") ){
            return Constants.DEBIT;
        }else if ( ty != null && ty.equalsIgnoreCase("cr") ){
            return Constants.CREDIT;
        }else if ( ty != null && ty.equalsIgnoreCase("bo") ){
            return Constants.CREDIT_AND_DEBIT;
        }
        return "";
    }

    public boolean isHubAccount() {
        return ha;
    }

    public double getBalance() {
        return bal;
    }

    public double getOpeningBalance() {
        return obal;
    }

    public void setEntries(List<JournalEntryDetails> accountEntries) {
        this.entries = accountEntries;
    }

    public List<JournalEntryDetails> getAccountEntries() {
        if ( entries == null ){ return  new ArrayList<>();  }
        return entries;
    }

    public String getEmployeeID() {
        if ( emp != null && emp.length() > 0 ){ return emp; }
        return "";
    }

    public List<String> getChildrenAccounts() {
        if ( children != null ){
            List<String> childrenAccounts = new ArrayList<>();
            for (int i=0;i<children.size();i++){
                if ( children.get(i) != null && children.get(i).length() > 0 ){
                    childrenAccounts.add(children.get(i));
                }
            }
            return childrenAccounts;
        }
        return new ArrayList<>();
    }
}
