package com.aleenaecommerce.operations.models;

import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 22/6/16.
 */
public class ScheduleDetails {
    String date;
    boolean isLeave;
    String checkIn;
    String checkOut;

    List<BreakDetails> breaksList;

    public ScheduleDetails(String date, boolean isLeave){
        this.date = date;
        this.isLeave = isLeave;
    }

    public ScheduleDetails(String date, String checkIn, String checkOut, List<BreakDetails> breaksList){
        this.date = date;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.breaksList = breaksList;
        this.isLeave = false;
    }

    public ScheduleDetails(String date, String checkIn, String checkOut){
        this.date = date;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.isLeave = false;
    }

    public String getDate() {
        return date;
    }

    public boolean isLeave() {
        return isLeave;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setBreaksList(List<BreakDetails> breaksList) {
        this.breaksList = breaksList;
    }

    public List<BreakDetails> getBreaksList() {
        if ( breaksList == null ){ return  new ArrayList<>();   }
        return breaksList;
    }
}
