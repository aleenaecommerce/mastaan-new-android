package com.aleenaecommerce.operations.models;

import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 28/6/16.
 */
public class AttendanceDetails {

    String _id;

    String date;
    double tm;              // Total Working Minutes
    boolean olv;            // Leave status
    boolean pl;             // PAID LEAVE

    boolean cis;            // Check In Status
    double cid;             //  Check In Delay
    List<String> ci;        // Check In Times
    List<String> co;        // Check Out Times

    double m;                 // Total Minutes On Work
    double otm;               // Total OverTime Minutes
    boolean mot;               // Marked Overtime

    boolean mfd;            // MarkAsFullDay

    String lcil;
    PlaceDetails lastCheckInPlaceDetails;
    EmployeeDetails lcib;

    public String getID() {
        return _id;
    }

    public String getDate() {
        return DateMethods.getReadableDateFromUTC(date);
    }

    public long getTotalMinutesToWork() {
        return (long) tm;
    }

    public boolean isLeave() {
        return olv;
    }

    public void setLeave(boolean isLeave){ this.olv = isLeave;  }

    public boolean isPaidLeave() {
        return pl;
    }

    public boolean getCheckInStatus() {
        return cis;
    }

    public long getCheckInDelay() {
        return (long) cid;
    }

    public List<String> getCheckInTimes() {
        if ( ci ==  null ){ return new ArrayList<>();   }
        return ci;
    }

    public String getFirstCheckInTime(){
        if ( ci != null && ci.size() > 0 ){
            return DateMethods.getReadableDateFromUTC(ci.get(0));
        }
        return "";
    }

    public String getLastCheckInTime(){
        if ( ci != null && ci.size() > 0 ){
            return DateMethods.getReadableDateFromUTC(ci.get(ci.size() - 1));
        }
        return "";
    }

    public List<String> getCheckOutTimes() {
        if ( co ==  null ){ return new ArrayList<>();   }
        return co;
    }

    public String getFirstCheckOutTime(){
        if ( co != null && co.size() > 0 ){
            return DateMethods.getReadableDateFromUTC(co.get(0));
        }
        return "";
    }

    public String getLastCheckOutTime(){
        if ( co != null && co.size() > 0 ){
            return DateMethods.getReadableDateFromUTC(co.get(co.size() - 1));
        }
        return "";
    }

    public long getWorkedMinutes() {
        return(long) m;
    }

    public long getOvertimeMinutes() {
        return (long)otm;
    }

    public boolean isMarkedOvertime() {
        return mot;
    }

    public boolean isMarkedFullDay() {
        return mfd;
    }

    public String getLastCheckedInByEmployeeID() {
        if ( lcib != null ){  return lcib.getID(); }
        return null;
    }

    public void setLastCheckedInByEmployeeDetails(EmployeeDetails employeeDetails) {
        this.lcib = employeeDetails;
    }
    public EmployeeDetails getLastCheckedInByEmployeeDetails() {
        if ( lcib == null ){    lcib = new EmployeeDetails();   }
        return lcib;
    }

    public void setLastCheckInPlaceLatLng(LatLng latlng) {
        if ( latlng != null ){
            this.lcil = latlng.latitude+","+latlng.longitude;
        }else{  this.lcil = null;    }
    }
    public void setLastCheckInPlaceLatLng(String latlng) {
        this.lcil = latlng;
    }
    public LatLng getLastCheckInPlaceLatLng() {
        return LatLngMethods.getLatLngFromString(lcil);
    }
    public void setLastCheckInPlaceDetails(PlaceDetails placeDetails) {
        this.lastCheckInPlaceDetails = placeDetails;
    }
    public PlaceDetails getLastCheckInPlaceDetails() {
        if ( lcil != null && lastCheckInPlaceDetails == null ){ lastCheckInPlaceDetails = new PlaceDetails(LatLngMethods.getLatLngFromString(lcil));    }
        return lastCheckInPlaceDetails;
    }
}
