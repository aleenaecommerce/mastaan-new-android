package com.aleenaecommerce.operations.models;

/**
 * Created by Naresh-Gocibo on 18-10-2015.
 */
public class KmLogItemDetails {
    private String date;
    private String distance;

    public KmLogItemDetails(String date, String distance) {
        this.distance = distance;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
