package com.aleenaecommerce.operations.models;

import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 28/7/16.
 */
public class JournalEntryDetails {
    String _id;     //  ID
    String des;     //  Description

    String cb;      //  Created By
    String ed;      //  Entry Date

    String com;     //  Comments

    List<TransactionDetails> trs;       // Transactions

    boolean pending;

    class TransactionDetails {
        String _id;
        String acc;
        double amt;
    }

    public String getID() {
        return _id;
    }

    public String getDescription() {
        if ( des == null ){ des = "";   }
        return des;
    }

    public String getCreatedBy() {
        return cb;
    }

    public String getEntryDate() {
        return DateMethods.getReadableDateFromUTC(ed);
    }

    public List<JournalEntryTransactionDetails> getTransactions() {
        if ( trs != null ){
            List<JournalEntryTransactionDetails> transactions = new ArrayList<>();
            for (int i=0;i<trs.size();i++){
                transactions.add(new JournalEntryTransactionDetails(trs.get(i)._id, trs.get(i).acc, trs.get(i).amt));
            }
            return  transactions;
        }
        return new ArrayList<>();
    }

    public String getComments() {
        return com;
    }

    public boolean isPending() {
        return pending;
    }
}
