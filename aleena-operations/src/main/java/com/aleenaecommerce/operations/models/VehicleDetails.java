package com.aleenaecommerce.operations.models;

import com.aleenaecommerce.operations.constants.Constants;

public class VehicleDetails {
    String rn;
    String o;
    String ip;//insurance provider
    String in;//insurance Number


    public VehicleDetails(){}

    public VehicleDetails(String registrationNumber, String ownType, String insuranceProvider, String insuranceNumber){
        this.rn = registrationNumber;
        this.o = ownType;
        this.ip =insuranceProvider;
        this.in = insuranceNumber;
    }

    public String getRegistrationNumber() {
        return rn;
    }

    public String getOwnType() {
        if ( o == null ){   o = ""; }
        return o;
    }
    public String getOwnTypeString() {
        if (getOwnType().equalsIgnoreCase("so"))
            return Constants.SELF_OWNED;
        else if (getOwnType().equalsIgnoreCase("co"))
            return Constants.COMPANY_OWNED;
        return getOwnType();
    }

    public String getInsuranceNumber() {
        return in;
    }

    public String getInsuranceProvider() {
        return ip;
    }
}
