package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.models.KmLogItem;

import java.util.List;


/**
 * Created by Naresh-Crypsis on 23-11-2015.
 */
public class TrackAdapter extends ArrayAdapter<KmLogItem> {
    Context context;
    int resource;
    List<KmLogItem> itemList;

    /*public TrackAdapter(Context context, int resource, List<Track> itemList) {
        super(context, resource, itemList);
        this.context = context;
        this.resource = resource;
        this.itemList = itemList;
    }*/

    public TrackAdapter(Context context, int resource, List<KmLogItem> itemList) {
        super(context, resource, itemList);
        this.context = context;
        this.resource = resource;
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public KmLogItem getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        TextView track_distance = (TextView) convertView.findViewById(R.id.track_distance);
        TextView track_id = (TextView) convertView.findViewById(R.id.track_id);
        track_distance.setText(itemList.get(position).getFormattedTrackDistance() + " km");
        track_id.setText(itemList.get(position).getTripType());
        return convertView;
    }

    public void addItems(List<KmLogItem> logItems) {
        for (KmLogItem logItem : logItems) {
            itemList.add(logItem);
        }
        notifyDataSetChanged();
    }
}
