package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.models.AccountDetails;

import java.util.List;


public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.ViewHolder> {

    Context context;
    List<AccountDetails> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout itemSelector;
        TextView name;
        TextView information;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = (LinearLayout) itemLayoutView.findViewById(R.id.itemSelector);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            information = (TextView) itemLayoutView.findViewById(R.id.information);
        }
    }

    public AccountsAdapter(Context context, List<AccountDetails> accountsList, CallBack callBack) {
        this.context = context;
        this.itemsList = accountsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AccountsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_account, null);
        return (new ViewHolder(itemView));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final AccountDetails accountDetails = itemsList.get(position);

        if ( accountDetails != null ) {
            viewHolder.name.setText(accountDetails.getName().toUpperCase());// + " (" + accountDetails.getType() + ")");
            String displayString = "";
            if ( accountDetails.getType().equalsIgnoreCase(Constants.CREDIT) || accountDetails.getType().equalsIgnoreCase(Constants.DEBIT)) {
                displayString += SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(Math.abs(accountDetails.getBalance()));
            }else{
                displayString += SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(accountDetails.getBalance());
            }

            viewHolder.information.setText(Html.fromHtml(displayString));

            if (callBack != null) {
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
            }
        }

    }

    //---------------

    public void setItems(List<AccountDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<AccountDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public AccountDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<AccountDetails> getAllItems(){
        return itemsList;
    }
}