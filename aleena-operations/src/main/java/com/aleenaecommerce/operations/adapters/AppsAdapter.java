package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.models.AppDetails;
import com.squareup.picasso.Picasso;

import java.util.List;


public class AppsAdapter extends RecyclerView.Adapter<AppsAdapter.ViewHolder> {

    Context context;
    List<AppDetails> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public interface CallBack {
        void onItemClick(int position);
        void onShareClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout appSelector;
        ImageView appImage;
        TextView appName;
        View shareApp;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            appSelector = (LinearLayout) itemLayoutView.findViewById(R.id.appSelector);
            appImage = (ImageView) itemLayoutView.findViewById(R.id.appIcon);
            appName = (TextView) itemLayoutView.findViewById(R.id.appName);
            shareApp = itemLayoutView.findViewById(R.id.shareApp);
        }
    }

    public AppsAdapter(Context context, List<AppDetails> appsList, CallBack callBack) {
        this.context = context;
        this.itemsList = appsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AppsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View appView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_app, null);
        return (new ViewHolder(appView));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final AppDetails appDetails = itemsList.get(position);

        viewHolder.appName.setText(appDetails.getName());
        Picasso.get()
                .load(appDetails.getImageURL())
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .tag(context)
                .into(viewHolder.appImage);

        if ( callBack != null && viewHolder.appSelector != null ){
            viewHolder.appSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemClick(position);
                }
            });
            viewHolder.shareApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBack.onShareClick(position);
                }
            });
        }

    }

    //---------------

    public void setItems(List<AppDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<AppDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public AppDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<AppDetails> getAllItems(){
        return itemsList;
    }
}