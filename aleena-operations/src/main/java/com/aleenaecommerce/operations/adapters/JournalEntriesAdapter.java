package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.models.AccountDetails;
import com.aleenaecommerce.operations.models.JournalEntryDetails;
import com.aleenaecommerce.operations.models.JournalEntryTransactionDetails;

import java.util.List;


public class JournalEntriesAdapter extends RecyclerView.Adapter<JournalEntriesAdapter.ViewHolder> {

    ViewGroup parent;
    Context context;
    AccountDetails parentAccountDetails;
    List<JournalEntryDetails> itemsList;
    CallBack callBack;

    public void setParentAccountDetails(AccountDetails parentAccountDetails) {
        this.parentAccountDetails = parentAccountDetails;
    }

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        TextView date;
        View transactionTypeIndicator;
        View pendingIndicator;
        TextView amount;
        TextView name;
        TextView information;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            date = (TextView) itemLayoutView.findViewById(R.id.date);
            transactionTypeIndicator = itemLayoutView.findViewById(R.id.transactionTypeIndicator);
            pendingIndicator = itemLayoutView.findViewById(R.id.pendingIndicator);
            amount = (TextView) itemLayoutView.findViewById(R.id.amount);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            information = (TextView) itemLayoutView.findViewById(R.id.information);
        }
    }

    public JournalEntriesAdapter(Context context, AccountDetails parentAccountDetails, List<JournalEntryDetails> accountEntriesList, CallBack callBack) {
        this.context = context;
        this.parentAccountDetails = parentAccountDetails;
        this.itemsList = accountEntriesList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public JournalEntriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_journal_entry, null);
        return (new ViewHolder(itemView));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final JournalEntryDetails journalEntryDetails = itemsList.get(position);

        if ( journalEntryDetails != null ) {
            viewHolder.date.setText(DateMethods.getDateInFormat(journalEntryDetails.getEntryDate(), DateConstants.MMM_DD_YYYY));

            if ( journalEntryDetails.getDescription() != null && journalEntryDetails.getDescription().length() > 0 ) {
                viewHolder.name.setVisibility(View.VISIBLE);
                viewHolder.name.setText(journalEntryDetails.getDescription().toUpperCase());// + " (" + accountDetails.getType() + ")");
            }else{  viewHolder.name.setVisibility(View.GONE);   }

            viewHolder.pendingIndicator.setVisibility(journalEntryDetails.isPending()?View.VISIBLE:View.GONE);

            double debitAmount = 0;
            double creditAmount = 0;
            for (int i=0;i< journalEntryDetails.getTransactions().size();i++){
                JournalEntryTransactionDetails transactionDetails = journalEntryDetails.getTransactions().get(i);
                if ( ( parentAccountDetails.isHubAccount() && parentAccountDetails.getChildrenAccounts().contains(transactionDetails.getAccountID()) ) || transactionDetails.getAccountID().equals(parentAccountDetails.getID()) ){
                    if ( transactionDetails.getAmount() > 0 ){
                        debitAmount += transactionDetails.getAmount();
                    }else{
                        creditAmount += transactionDetails.getAmount();
                    }
                }
            }

            if ( debitAmount != 0 ) {
                viewHolder.amount.setText(SpecialCharacters.RS+"  "+CommonMethods.getIndianFormatNumber(Math.abs(debitAmount)));
                viewHolder.transactionTypeIndicator.setBackgroundColor(Color.parseColor("#e8f5e9"));
            }
            else if ( creditAmount !=0 ) {
                viewHolder.amount.setText(SpecialCharacters.RS+"  "+CommonMethods.getIndianFormatNumber(Math.abs(creditAmount)));
                viewHolder.transactionTypeIndicator.setBackgroundColor(Color.parseColor("#ffebee"));
            }else{
                viewHolder.amount.setText(SpecialCharacters.RS+"  "+CommonMethods.getIndianFormatNumber(Math.abs(journalEntryDetails.getTransactions().get(0).getAmount())));
                viewHolder.transactionTypeIndicator.setBackgroundColor(Color.parseColor("#fafafa"));
            }

            //-----

            String displayString = "";
            if ( journalEntryDetails.getComments() != null && journalEntryDetails.getComments().trim().length() > 0 ){
                displayString += "Comments: "+journalEntryDetails.getComments().trim()+"";
            }

            if ( displayString.trim().length() > 0 ) {
                viewHolder.information.setVisibility(View.VISIBLE);
                viewHolder.information.setText(Html.fromHtml(displayString));
            }else{
                viewHolder.information.setVisibility(View.GONE);
            }

            if (callBack != null) {
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
            }
        }

    }

    //---------------

    public void setItems(List<JournalEntryDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<JournalEntryDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public JournalEntryDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<JournalEntryDetails> getAllItems(){
        return itemsList;
    }
}