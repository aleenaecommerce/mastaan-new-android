package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.os.Bundle;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aleena.common.fragments.NotYetFragment;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.fragments.AttendanceDetailsFragment;
import com.aleenaecommerce.operations.fragments.AttendanceReportFragment;
import com.aleenaecommerce.operations.fragments.WeekSchdeuleFragment;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.google.gson.Gson;

public class TabsFragmentAdapter extends FragmentPagerAdapter {

    Context context;
    String[] itemsList;
    EmployeeDetails employeeDetails;

    public TabsFragmentAdapter(Context context, FragmentManager fragmentManager, String[] itemsList) {
        super(fragmentManager);
        this.context = context;
        this.itemsList =  itemsList;
    }

    public TabsFragmentAdapter setEmployeeDetails(EmployeeDetails employeeDetails) {
        this.employeeDetails = employeeDetails;
        return this;
    }

    @Override
    public Fragment getItem(int position) {

        if ( itemsList[position].equalsIgnoreCase(Constants.ATTENDANCE_DETAILS_FRAGMENT) ){
            AttendanceDetailsFragment fragment = new AttendanceDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putString("employeeDetails", new Gson().toJson(employeeDetails));
            fragment.setArguments(bundle);
            return fragment;
        }
        if ( itemsList[position].equalsIgnoreCase(Constants.WEEK_SCHEDULE_FRAGMENT) ){
            WeekSchdeuleFragment fragment = new WeekSchdeuleFragment();
            Bundle bundle = new Bundle();
            bundle.putString("employeeDetails", new Gson().toJson(employeeDetails));
            fragment.setArguments(bundle);
            return fragment;
        }
        else if ( itemsList[position].equalsIgnoreCase(Constants.ATTENDANCE_REPORT_FRAGMENT) ){
            AttendanceReportFragment fragment = new AttendanceReportFragment();
            Bundle bundle = new Bundle();
            bundle.putString("userID", employeeDetails.getID());
            fragment.setArguments(bundle);
            return fragment;
        }

        return new NotYetFragment();
    }

    @Override
    public int getCount() {
        return itemsList.length;
    }

}
