package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.EmployeeDay;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;


/**
 * Created by Naresh Katta on 31/10/2015.
 */
public class CalendarAdapter extends ArrayAdapter<EmployeeDay> {

    Context context;
    List<EmployeeDay> itemsList;
    int currentViewingMonth;
    private HashSet<Date> eventDays;

    Callback callback;

    public interface Callback {
        void onClick(int position);
    }

    public CalendarAdapter(Context context, List<EmployeeDay> daysList, HashSet<Date> eventDays, int currentViewingMonth, Callback callback) {
        super(context, R.layout.view_calender_day, daysList);
        this.context = context;
        this.itemsList = daysList;
        this.eventDays = eventDays;
        this.currentViewingMonth = currentViewingMonth;
        this.callback = callback;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.view_calender_day, parent, false);
        }
        CardView item_card = (CardView) view.findViewById(R.id.item_card);
        TextView date_details = (TextView) view.findViewById(R.id.date);
        TextView information = (TextView) view.findViewById(R.id.number_of_km);
        View itemSelector = view.findViewById(R.id.button);

        //-------

        final EmployeeDay dayDetails = getItem(position);

        if ( dayDetails != null ){
            AttendanceDetails attendanceDetails = dayDetails.getAttendanceDetails();

            // day in question
            Date date = dayDetails.getDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);

            // today
            Date today = new Date();
            //Today calender

            Calendar todayCalendar = Calendar.getInstance();
            todayCalendar.setTime(today);
            // inflate item if it does not exist yet

            // if this day has an event, specify event image
            view.setBackgroundResource(0);
            if (eventDays != null) {
                for (Date eventDate : eventDays) {
                    Calendar eventCalendar = Calendar.getInstance();
                    eventCalendar.setTime(eventDate);
                    if (eventCalendar.get(Calendar.DAY_OF_MONTH) == day &&
                            eventCalendar.get(Calendar.MONTH) == month &&
                            eventCalendar.get(Calendar.YEAR) == year) {
                        // mark this day for event
                        view.setBackgroundResource(R.drawable.reminder);
                        break;
                    }
                }
            }

            //----------

            date_details.setTypeface(null, Typeface.NORMAL);
            date_details.setTextColor(Color.WHITE);
            information.setVisibility(View.VISIBLE);

            if (month != currentViewingMonth) {
                // if this day is outside current month, grey it out
                date_details.setTextColor(context.getResources().getColor(R.color.greyed_out));
                information.setVisibility(View.INVISIBLE);
                itemSelector.setEnabled(false);
            } else if (day == todayCalendar.get(Calendar.DAY_OF_MONTH) && month == todayCalendar.get(Calendar.MONTH) && year == todayCalendar.get(Calendar.YEAR)) {
                // if it is today, set it to blue/bold
                date_details.setTypeface(null, Typeface.BOLD);
                date_details.setTextColor(context.getResources().getColor(R.color.today));
            }
            if (day > todayCalendar.get(Calendar.DAY_OF_MONTH) && month >= todayCalendar.get(Calendar.MONTH) && year >= todayCalendar.get(Calendar.YEAR)) {
                // if it is future day, set it to blue/bold
                information.setVisibility(View.INVISIBLE);
                itemSelector.setEnabled(false);
            }
            if (month > todayCalendar.get(Calendar.MONTH) && year >= todayCalendar.get(Calendar.YEAR)) {
                // if it is future day, set it to blue/bold
                information.setVisibility(View.INVISIBLE);
                itemSelector.setEnabled(false);
            }
            if (year > todayCalendar.get(Calendar.YEAR)) {
                // if it is future day, set it to blue/bold
                information.setVisibility(View.INVISIBLE);
                itemSelector.setEnabled(false);
            }


            // set text
            date_details.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));


            if ( attendanceDetails != null){
                long workedMin = (long)attendanceDetails.getWorkedMinutes();
                String workedInHours = "";
                if ( workedMin >= 60 ) {
                    if (workedMin % 60 == 0) {
                        workedInHours = (workedMin / 60)+":0";
                    } else {
                        workedInHours = (workedMin / 60) + ":" + (workedMin % 60)+"";
                    }
                }else{
                    workedInHours = "0:"+workedMin + "";
                }


                if ( attendanceDetails.isMarkedOvertime() ){
                    item_card.setCardBackgroundColor(context.getResources().getColor(R.color.overtimeDayColor));
                }
                else if ( attendanceDetails.isLeave() ){
                    item_card.setCardBackgroundColor(context.getResources().getColor(R.color.red));
                    if ( attendanceDetails.isPaidLeave() ){
                        workedInHours = "PL";
                    }else{
                        workedInHours = "UPL";
                    }
                }
                else{
                    item_card.setCardBackgroundColor(context.getResources().getColor(R.color.normalDayColor));
                }
                information.setText(workedInHours);//DateMethods.getTimeStringFromMinutes((long)getItem(position).getAttendanceDetails().getWorkedMinutes(), "h", "m").replaceAll("\\s+", "").trim());
            }else{
               information.setText("0");
                item_card.setCardBackgroundColor(context.getResources().getColor(R.color.normalDayColor));
            }

            if ( callback != null ) {
                itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dayDetails.getAttendanceDetails() != null) {
                            callback.onClick(position);
                        }
                    }
                });
            }

        }

        return view;
    }

    public void updateLeave(int position, boolean isLeave){
        itemsList.get(position).getAttendanceDetails().setLeave(isLeave);
        notifyDataSetChanged();
    }

    //--------

    public void setItems(List<EmployeeDay> daysList) {
        if ( daysList != null ) {
            this.itemsList = daysList;
            notifyDataSetChanged();
        }
    }

    public void addItems(List<EmployeeDay> daysList) {
        if ( daysList != null && daysList.size() > 0 ){
            for (int i=0;i<daysList.size();i++){
                itemsList.add(daysList.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void deleteItem(int position){
        itemsList.remove(position);
        notifyDataSetChanged();
    }
}
