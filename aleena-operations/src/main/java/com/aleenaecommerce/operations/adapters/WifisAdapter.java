package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.WifiDetails;
import com.aleenaecommerce.operations.R;

import java.util.ArrayList;
import java.util.List;


public class WifisAdapter extends RecyclerView.Adapter<WifisAdapter.ViewHolder> {

    Context context;
    List<WifiDetails> itemsList;
    Callback callback;

    ViewGroup parent;

    public interface Callback {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;

        TextView wifi_name;
        TextView wif_details;
        View deleteWifi;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);

            wifi_name = (TextView) itemLayoutView.findViewById(R.id.wifi_name);
            wif_details = (TextView) itemLayoutView.findViewById(R.id.wif_details);
            deleteWifi =  itemLayoutView.findViewById(R.id.deleteWifi);
        }
    }

    public WifisAdapter(Context context, List<WifiDetails> wifisList, Callback callback) {
        this.context = context;
        this.itemsList = wifisList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public WifisAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View appView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_wifi, null);
        return (new ViewHolder(appView));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final WifiDetails wifiDetails = itemsList.get(position);

        if ( wifiDetails != null ){
            viewHolder.wifi_name.setText(CommonMethods.capitalizeFirstLetter(wifiDetails.getSSID()));

            if ( callback != null ) {
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onItemClick(position);
                    }
                });

                viewHolder.deleteWifi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onDeleteClick(position);
                    }
                });
            }
        }
    }

    //--------------

    public boolean isContains(String wifiBSSID){
        if ( wifiBSSID != null ) {
            for (int i = 0; i < itemsList.size(); i++) {
                if (itemsList.get(i) != null && itemsList.get(i).getBSSID() != null && itemsList.get(i).getBSSID().equals(wifiBSSID)) {
                    return true;
                }
            }
        }
        return false;
    }
    //---------------

    public void setItems(List<WifiDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItem(WifiDetails wifiDetails){
        if ( itemsList == null ){ itemsList = new ArrayList<>();    }
        itemsList.add(wifiDetails);
        notifyDataSetChanged();
    }

    public void addItems(List<WifiDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public WifiDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<WifiDetails> getAllItems(){
        return itemsList;
    }
}