package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.text.ClipboardManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vCircularImageView;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.localdata.LocalStorageData;
import com.aleenaecommerce.operations.methods.AttendanceMethods;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;

import java.util.ArrayList;
import java.util.List;

public class EmployeesAdapter extends RecyclerView.Adapter<EmployeesAdapter.ViewHolder> {

    Context context;
    LocalStorageData localStorageData;
    List<EmployeeDetails> itemsList;
    CallBack callBack;

    EmployeeDetails employeeDetails;

    public interface CallBack {
        void onItemClick(int position);
        void onMenuClick(int position, View view);
        void onCheckIn(int position);
        void onCheckOut(int position);
        void onMarkLeave(int position);
        void onUnmarkLeaveToday(int position);
        void onCallDeliveryBoy(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        vCircularImageView image;
        TextView name;
        TextView mobile;
        TextView information;

        View menuOptions;
        View checkIn;
        View checkOut;
        View markLeave;
        View unmarkLeaveToday;
        View callDeliveryBoy;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            image = (vCircularImageView) itemLayoutView.findViewById(R.id.image);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            mobile = (TextView) itemLayoutView.findViewById(R.id.mobile);
            information = (TextView) itemLayoutView.findViewById(R.id.information);
            menuOptions = itemLayoutView.findViewById(R.id.menuOptions);
            checkIn = itemLayoutView.findViewById(R.id.checkIn);
            checkOut = itemLayoutView.findViewById(R.id.checkOut);
            markLeave = itemLayoutView.findViewById(R.id.markLeave);
            unmarkLeaveToday = itemLayoutView.findViewById(R.id.unmarkLeaveToday);
            callDeliveryBoy = itemLayoutView.findViewById(R.id.callDeliveryBoy);
        }
    }

    public EmployeesAdapter(Context context, List<EmployeeDetails> chefsList, CallBack callBack) {
        this.context = context;
        this.localStorageData = new LocalStorageData(context);
        this.itemsList = chefsList;
        this.callBack = callBack;

        employeeDetails = new LocalStorageData(context).getUserDetails();
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public EmployeesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_employee, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final EmployeeDetails employeeDetails = itemsList.get(position);    // Getting Food Items Details using Position...

        if ( employeeDetails != null ){

            final AttendanceDetails attendanceDetails = employeeDetails.getAttendanceDetails();

            if ( this.employeeDetails.isWarehouseManager() ){
                if ( AttendanceMethods.isCheckedInNow(attendanceDetails, localStorageData.getServerTime()) ) {
                    viewHolder.checkOut.setVisibility(View.VISIBLE);
                    viewHolder.checkIn.setVisibility(View.GONE);
                }else {
                    viewHolder.checkIn.setVisibility(View.VISIBLE);
                    viewHolder.checkOut.setVisibility(View.GONE);
                }

                if ( employeeDetails.getID().equals(this.employeeDetails.getID()) ){
                    viewHolder.markLeave.setVisibility(View.GONE);
                }else{
                    viewHolder.markLeave.setVisibility(View.VISIBLE);
                }
            }else{
                viewHolder.checkIn.setVisibility(View.GONE);
                viewHolder.checkOut.setVisibility(View.GONE);
                viewHolder.markLeave.setVisibility(View.GONE);
            }

            if ( this.employeeDetails.isSuperUser() ){
                viewHolder.menuOptions.setVisibility(View.VISIBLE);
            }else{  viewHolder.menuOptions.setVisibility(View.GONE);    }

            //--------

            viewHolder.name.setText(new CommonMethods().capitalizeStringWords(employeeDetails.getFullName()));
            viewHolder.mobile.setText(CommonMethods.removeCountryCodeFromNumber(employeeDetails.getMobile()));

            if ( employeeDetails.isDeliveryBoy() ) {
                String informationString = "";
                if (DateMethods.compareDates(DateMethods.getOnlyDate(localStorageData.getServerTime()), DateMethods.getOnlyDate(attendanceDetails.getDate())) == 0
                        && attendanceDetails.isLeave()) {
                    informationString += "<b>" + (attendanceDetails.isPaidLeave() ? "PAID" : "UNPAID") + " LEAVE</b>";
                    viewHolder.checkIn.setVisibility(View.GONE);
                    viewHolder.checkOut.setVisibility(View.GONE);
                    if (this.employeeDetails.isDeliveryBoyManager()) {
                        viewHolder.unmarkLeaveToday.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.unmarkLeaveToday.setVisibility(View.GONE);
                    }
                } else {
                    viewHolder.unmarkLeaveToday.setVisibility(View.GONE);

                    // HAS SCHEDULE TODAY
                    if (AttendanceMethods.isWorkingDayToday(employeeDetails, localStorageData.getServerTime())) {
                        if (AttendanceMethods.isScheduleStartedToday(employeeDetails, localStorageData.getServerTime())) {
                            if (AttendanceMethods.isStartedWorkingToday(attendanceDetails, localStorageData.getServerTime())) {
                                if (attendanceDetails.getCheckInStatus()) {
                                    informationString += "<b>(" + SpecialCharacters.CHECK_MARK + ") IN @ </b>Today " + DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(employeeDetails.getLastCheckedInTime()), DateConstants.HH_MM_A);
                                    if ( attendanceDetails.getLastCheckedInByEmployeeID() != null && attendanceDetails.getLastCheckedInByEmployeeID().equalsIgnoreCase(employeeDetails.getID()) == false ){
                                        informationString += "<br><b>By: </b>"+CommonMethods.capitalizeStringWords(attendanceDetails.getLastCheckedInByEmployeeDetails().getFullName());
                                    }
                                    if ( attendanceDetails.getLastCheckInPlaceDetails() != null){
                                        informationString += "<br><b>At: </b>"+CommonMethods.capitalizeStringWords(attendanceDetails.getLastCheckInPlaceDetails().getFullAddress());
                                    }
                                } else {
                                    informationString += "<b>(" + SpecialCharacters.X_MARK + ") OUT @ </b>Today " + DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(employeeDetails.getLastCheckedOutTime()), DateConstants.HH_MM_A);
                                }
                                double delayInMinutes = AttendanceMethods.getCheckInDelayToday(attendanceDetails, localStorageData.getServerTime());
                                if (delayInMinutes > 0) {
                                    informationString += "<br><b>Delay:  </b>" + DateMethods.getTimeStringFromMinutes((long) delayInMinutes, "Hr", "Min");
                                }
                            } else {
                                informationString += "<b>NOTYET CHECKED IN</b>";
                            }
                        } else {
                            informationString += "<b>SCHEDULE NOTYET STARTED</b>";
                        }
                    }
                    // WEEK OFF TODAY
                    else {
                        if (AttendanceMethods.isStartedWorkingToday(attendanceDetails, localStorageData.getServerTime())) {
                            if (attendanceDetails.getCheckInStatus()) {
                                informationString += "<b>(" + SpecialCharacters.CHECK_MARK + ") IN @ </b>Today " + DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(employeeDetails.getLastCheckedInTime()), DateConstants.HH_MM_A);
                            } else {
                                informationString += "<b>(" + SpecialCharacters.X_MARK + ") OUT @ </b>Today " + DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(employeeDetails.getLastCheckedOutTime()), DateConstants.HH_MM_A);
                            }
                        } else {
                            informationString += "<b>WEEK OFF TODAY</b>";
                        }
                    }
                }
                viewHolder.information.setVisibility(View.VISIBLE);
                viewHolder.information.setText(Html.fromHtml(informationString));
            }else{
                viewHolder.information.setVisibility(View.GONE);
            }


            if ( callBack != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
                viewHolder.itemSelector.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboardManager.setText(CommonMethods.removeCountryCodeFromNumber(employeeDetails.getMobile()));
                        Toast.makeText(context, employeeDetails.getFullName() + "'s mobile number ( " + employeeDetails.getMobile() + " ) is copied", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                });
                viewHolder.menuOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callBack.onMenuClick(position, viewHolder.menuOptions);
                    }
                });
                viewHolder.checkIn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onCheckIn(position);
                    }
                });
                viewHolder.checkOut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onCheckOut(position);
                    }
                });
                viewHolder.markLeave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onMarkLeave(position);
                    }
                });
                viewHolder.unmarkLeaveToday.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onUnmarkLeaveToday(position);
                    }
                });
                viewHolder.callDeliveryBoy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onCallDeliveryBoy(position);
                    }
                });
            }

        }
    }

    //-------

    public void updateLeaveDetails(int position, boolean isLeave){
        if ( itemsList.size() > position && itemsList.get(position) != null ){
            itemsList.get(position).getAttendanceDetails().setLeave(isLeave);
            notifyDataSetChanged();
        }
    }

    public void updateAttendanceDetails(int position, AttendanceDetails attendanceDetails){
        if ( itemsList.size() > position && itemsList.get(position) != null ){
            itemsList.get(position).setAttendanceDetails(attendanceDetails);
            notifyDataSetChanged();
        }
    }

    //---------------

    public void setItems(List<EmployeeDetails> employeesList){

        itemsList.clear();;
        if ( employeesList != null && employeesList.size() > 0 ) {
            for (int i = 0; i < employeesList.size(); i++) {
                itemsList.add(employeesList.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<EmployeeDetails> employeesList){

        if ( employeesList != null && employeesList.size() > 0 ) {
            for (int i = 0; i < employeesList.size(); i++) {
                itemsList.add(employeesList.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void addItem(EmployeeDetails employeeDetails){
        if ( itemsList == null ){   itemsList = new ArrayList<>();  }
        if ( employeeDetails != null ) {
            itemsList.add(employeeDetails);
            notifyDataSetChanged();
        }
    }
    public void setItem(EmployeeDetails employeeDetails){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(employeeDetails.getID()) ){
                itemsList.set(i, employeeDetails);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void setItem(int position, EmployeeDetails employeeDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, employeeDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public EmployeeDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<EmployeeDetails> getAllItems(){
        return itemsList;
    }

}