package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.localdata.LocalStorageData;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.models.TransactionDetails;

import java.util.List;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder> {

    Context context;
    List<TransactionDetails> itemsList;
    CallBack callBack;

    EmployeeDetails employeeDetails;

    public interface CallBack {
        void onItemClick(int position);
        void onMenuClick(int position, View view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        TextView transaction_type;
        TextView information;
        View menuOptions;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            transaction_type = (TextView) itemLayoutView.findViewById(R.id.transaction_type);
            information = (TextView) itemLayoutView.findViewById(R.id.information);
            menuOptions = itemLayoutView.findViewById(R.id.menuOptions);
        }
    }

    public TransactionsAdapter(Context context, List<TransactionDetails> transactionsList, CallBack callBack) {
        this.context = context;
        this.itemsList = transactionsList;
        this.callBack = callBack;

        employeeDetails = new LocalStorageData(context).getUserDetails();
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public TransactionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_transaction, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final TransactionDetails transactionDetails = itemsList.get(position);    // Getting Food Items Details using Position...

        if ( transactionDetails != null ){
            viewHolder.transaction_type.setText(transactionDetails.getTransactionTypeDetails().getName().toUpperCase());

            String informationString = "Date: <b>"+DateMethods.getDateInFormat(transactionDetails.getEntryDate(), DateConstants.MMM_DD_YYYY)+"</b>";
            informationString += "<br>From: <b>"+transactionDetails.getCreditAccountDetails().getName()+"</b>";
            informationString += "<br>To: <b>"+transactionDetails.getDebitAccountDetails().getName()+"</b>";
            informationString += "<br>Amount: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(transactionDetails.getAmount())+"</b>";
            informationString += "<br>Created by: <b>"+CommonMethods.capitalizeStringWords(transactionDetails.getCreatorDetails().getFullName())+"</b> on <b>"+DateMethods.getDateInFormat(transactionDetails.getCreatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b>";
            if ( transactionDetails.getComments() != null && transactionDetails.getComments().trim().length() > 0 ){
                informationString += "<br>Comments: <b>"+transactionDetails.getComments().toLowerCase()+"</b>";
            }

            viewHolder.information.setText(Html.fromHtml(informationString));

            if ( employeeDetails.isAccountsApprovalManager() || transactionDetails.getCreatorDetails().getID().equals(employeeDetails.getID())){
                viewHolder.menuOptions.setVisibility(View.VISIBLE);
            }else{
                viewHolder.menuOptions.setVisibility(View.GONE);
            }

            if ( callBack != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
                viewHolder.menuOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onMenuClick(position, viewHolder.menuOptions);
                    }
                });
            }
        }

    }

    //---------------

    public void setItems(List<TransactionDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<TransactionDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void updateItem(TransactionDetails transactionDetails){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(transactionDetails.getID()) ){
                itemsList.set(i, transactionDetails);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void updateItem(int position, TransactionDetails transactionDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, transactionDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public TransactionDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<TransactionDetails> getAllItems(){
        return itemsList;
    }

}