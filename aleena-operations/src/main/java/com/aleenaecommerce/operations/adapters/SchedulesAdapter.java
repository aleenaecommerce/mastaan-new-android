package com.aleenaecommerce.operations.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.models.BreakDetails;
import com.aleenaecommerce.operations.models.ScheduleDetails;

import java.util.List;


public class SchedulesAdapter extends RecyclerView.Adapter<SchedulesAdapter.ViewHolder> {

    Context context;
    List<ScheduleDetails> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout itemSelector;
        TextView date;
        TextView information;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = (LinearLayout) itemLayoutView.findViewById(R.id.itemSelector);
            date = (TextView) itemLayoutView.findViewById(R.id.date);
            information = (TextView) itemLayoutView.findViewById(R.id.information);
        }
    }

    public SchedulesAdapter(Context context, List<ScheduleDetails> schedulesList, CallBack callBack) {
        this.context = context;
        this.itemsList = schedulesList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public SchedulesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View appView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_schedule, null);
        return (new ViewHolder(appView));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final ScheduleDetails scheduleDetails = itemsList.get(position);

        viewHolder.date.setText(scheduleDetails.getDate().toUpperCase());

        if ( scheduleDetails.isLeave() == false ){
            String detailsString = "Check in: "+DateMethods.getTimeIn12HrFormat(scheduleDetails.getCheckIn())+", Check out: "+DateMethods.getTimeIn12HrFormat(scheduleDetails.getCheckOut());
            if ( scheduleDetails.getBreaksList() != null && scheduleDetails.getBreaksList().size() > 0 ){
                detailsString += "\n\nBreaks:\n";
                List<BreakDetails> breaksList = scheduleDetails.getBreaksList();
                for (int i=0;i<breaksList.size();i++){
                    if ( i != 0 ){  detailsString += "\n";  }
                    detailsString += DateMethods.getTimeIn12HrFormat(breaksList.get(i).getStartTime())+" to "+ DateMethods.getTimeIn12HrFormat(DateMethods.addToDateInMinutes(breaksList.get(i).getStartTime(), breaksList.get(i).getDurationInMinutes()));
                }
            }
            viewHolder.information.setText(detailsString);
        }
        else{
            viewHolder.information.setText(Html.fromHtml("<b>WEEK OFF</b>"));
        }


    }

    //---------------

    public void setItems(List<ScheduleDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<ScheduleDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public ScheduleDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<ScheduleDetails> getAllItems(){
        return itemsList;
    }
}