package com.aleenaecommerce.operations.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.activities.OperationsToolBarActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by Venkatesh Uppu on 23/6/17.
 */

public class ImageDialog {
    OperationsToolBarActivity activity;

    public ImageDialog(OperationsToolBarActivity activity){
        this.activity = activity;
    }

    public void show(String imageURL){
        if ( imageURL == null ){    imageURL = "";  }
        View imageHolderView = LayoutInflater.from(activity).inflate(R.layout.dialog_image, null);
        ImageView image = (ImageView) imageHolderView.findViewById(R.id.image);
        Picasso.get()
                .load(imageURL)
                .placeholder(R.drawable.image_default)
                .error(R.drawable.image_default)
                .into(image);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });
        imageHolderView.findViewById(R.id.container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });

        activity.showCustomDialog(imageHolderView, true, true);

    }

}
