package com.aleenaecommerce.operations.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.text.ClipboardManager;
import android.view.MenuItem;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.adapters.AppsAdapter;
import com.aleenaecommerce.operations.backend.CommonAPI;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.models.AppDetails;

import java.util.ArrayList;
import java.util.List;

public class AppsActivity extends OperationsToolBarActivity {

    vRecyclerView appsHolder;
    AppsAdapter appsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.next_activity_enter_anim, R.anim.next_activity_exit_anim);     // Enter Transition Animation..
        setContentView(R.layout.activity_apps);
        hasLoadingView();

        //-----------------

        appsHolder = (vRecyclerView) findViewById(R.id.appsHolder);
        setupHolder();

        //------------------

        getAppsList();        //

    }

    public void setupHolder(){

        appsHolder.setHasFixedSize(true);
        appsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        appsHolder.setItemAnimator(new DefaultItemAnimator());

        appsAdapter = new AppsAdapter(context, new ArrayList<AppDetails>(), new AppsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                String downloadURL = appsAdapter.getItem(position).getDownloadURL();

                ClipboardManager clipboardManager = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboardManager.setText(downloadURL);
                showToastMessage("Download URL ( " + downloadURL + " ) is copied");

                Intent browserIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(downloadURL));
                startActivity(browserIntent);
            }

            @Override
            public void onShareClick(int position) {
                AppDetails appDetails = appsAdapter.getItem(position);

                Intent shareAct = new Intent(context, ShareActivity.class);
                shareAct.putExtra(Constants.ACTIVITY_TITLE, "Share "+appDetails.getName());
                shareAct.putExtra(Constants.SHARE_TITLE, appDetails.getName().toUpperCase());
                shareAct.putExtra(Constants.SHARE_MESSAGE, "Download "+CommonMethods.capitalizeFirstLetter(appDetails.getName())+" using this link - "+appDetails.getDownloadURL());
                startActivity(shareAct);
            }
        });

        appsHolder.setAdapter(appsAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getAppsList();
    }

    public void getAppsList(){

        showLoadingIndicator("Loading apps, wait..");
        getBackendAPIs().getCommonAPI().getAppsList(new CommonAPI.AppsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<AppDetails> appsList) {
                if (status == true) {
                    if (appsList.size() > 0) {
                        appsAdapter.addItems(appsList);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No apps");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load apps, try again!");
                }
            }
        });
    }

    //=================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
