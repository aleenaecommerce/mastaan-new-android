package com.aleenaecommerce.operations.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;

import com.aleena.common.widgets.vWebViewProgressIndicator;
import com.aleenaecommerce.operations.R;

public class WeekOffScheduleActivity extends OperationsToolBarActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week_off_schedule);
        hasLoadingView();

        //--------

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new vWebViewProgressIndicator(new vWebViewProgressIndicator.CallBack() {
            @Override
            public void onLoading(int progress) {
                if (progress == 100) {
                    switchToContentPage();
                }
            }
        }));

        webView.loadUrl("http://api.mastaan.com:7000/mastaan_week_off");
        showLoadingIndicator("Loading week off schedule, wait...");

    }

    //==========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
