package com.aleenaecommerce.operations.activities;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RadioButton;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.backend.EmployeesAPI;
import com.aleenaecommerce.operations.backend.models.RequestAddOrEditEmployee;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.widgets.RolesCompletionTextView;
import com.google.gson.Gson;

import java.util.List;


public class AddOrEditEmployeeActivity extends OperationsToolBarActivity implements View.OnClickListener{

    String actionType;

    vTextInputLayout first_name;
    vTextInputLayout last_name;
    vTextInputLayout mobile;
    vTextInputLayout alternate_mobile;
    vTextInputLayout email;
    vTextInputLayout rolesView;
    RolesCompletionTextView roles;
    vTextInputLayout esi_number;
    vTextInputLayout driving_license_number;
    RadioButton selfOwnedVehicle;
    RadioButton companyOwnedVehicle;
    vTextInputLayout vehicle_registration_number;
    vTextInputLayout vehicle_insurance_provider;
    vTextInputLayout vehicle_insurance_number;
    vTextInputLayout password;
    vTextInputLayout address_line1;
    vTextInputLayout address_line2;
    vTextInputLayout area;
    vTextInputLayout landmark;
    vTextInputLayout city;
    vTextInputLayout state;
    vTextInputLayout pincode;

    View done;

    EmployeeDetails employeeDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_employee);

        actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        if ( actionType == null || actionType.length() == 0 ){  actionType = Constants.ADD;   }

        //------------

        first_name = (vTextInputLayout) findViewById(R.id.first_name);
        last_name = (vTextInputLayout) findViewById(R.id.last_name);
        mobile = (vTextInputLayout) findViewById(R.id.mobile);
        alternate_mobile = (vTextInputLayout) findViewById(R.id.alternate_mobile);
        email = (vTextInputLayout) findViewById(R.id.email);
        rolesView = (vTextInputLayout) findViewById(R.id.rolesView);
        roles = (RolesCompletionTextView) findViewById(R.id.roles);
        email = (vTextInputLayout) findViewById(R.id.email);
        esi_number = (vTextInputLayout) findViewById(R.id.esi_number);
        driving_license_number = (vTextInputLayout) findViewById(R.id.driving_license);
        selfOwnedVehicle = (RadioButton) findViewById(R.id.selfOwnedVehicle);
        companyOwnedVehicle = (RadioButton) findViewById(R.id.companyOwnedVehicle);
        vehicle_registration_number = (vTextInputLayout) findViewById(R.id.vehicle_registration_number);
        vehicle_insurance_provider = (vTextInputLayout) findViewById(R.id.vehicle_insurance_provider);
        vehicle_insurance_number = (vTextInputLayout) findViewById(R.id.vehicle_insurance_number);
        password = (vTextInputLayout) findViewById(R.id.password);
        address_line1 = (vTextInputLayout) findViewById(R.id.address_line1);
        address_line2 = (vTextInputLayout) findViewById(R.id.address_line2);
        area = (vTextInputLayout) findViewById(R.id.area);
        landmark = (vTextInputLayout) findViewById(R.id.landmark);
        city = (vTextInputLayout) findViewById(R.id.city);
        state = (vTextInputLayout) findViewById(R.id.state);
        pincode = (vTextInputLayout) findViewById(R.id.pincode);

        done = findViewById(R.id.done);
        done.setOnClickListener(this);

        roles.allowCollapse(false);
        roles.setThreshold(1);
        roles.performBestGuess(false);
        //roles.allowDuplicates(false);
        roles.setAdapter(new ListArrayAdapter(context, Constants.ROLES));

        final View rootView = getWindow().getDecorView().getRootView();
        if ( rootView != null ){
            rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Rect r = new Rect();
                    rootView.getWindowVisibleDisplayFrame(r);
                    int screenHeight = rootView.getRootView().getHeight();
                    int keypadHeight = screenHeight - r.bottom;
                    // keyboard is opened
                    if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                        done.setVisibility(View.GONE);
                    }
                    // keyboard is closed
                    else {
                        done.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        //-------------

        if ( actionType.equalsIgnoreCase(Constants.EDIT) ){
            getAppCompactActionBar().setTitle("Edit Employee");
            employeeDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.USER_DETAILS), EmployeeDetails.class);

            password.setVisibility(View.GONE);

            first_name.setText(employeeDetails.getFirstName());
            last_name.setText(employeeDetails.getLastName());
            mobile.setText(employeeDetails.getMobile());
            alternate_mobile.setText(employeeDetails.getAlternateMobile());
            email.setText(employeeDetails.getEmail());
            password.setText(employeeDetails.getPassword());

            roles.setRoles(Constants.getReadableRoles(employeeDetails.getRoles()));
            esi_number.setText(employeeDetails.getESINumber());
            driving_license_number.setText(employeeDetails.getDrivingLicenseNumber());

            if ( employeeDetails.getVehicleDetails().getOwnTypeString().equalsIgnoreCase(Constants.COMPANY_OWNED) ){
                companyOwnedVehicle.setChecked(true);
            }else if ( employeeDetails.getVehicleDetails().getOwnTypeString().equalsIgnoreCase(Constants.SELF_OWNED)){
                selfOwnedVehicle.setChecked(true);
            }
            vehicle_registration_number.setText(employeeDetails.getVehicleDetails().getRegistrationNumber());
            vehicle_insurance_provider.setText(employeeDetails.getVehicleDetails().getInsuranceProvider());
            vehicle_insurance_number.setText(employeeDetails.getVehicleDetails().getInsuranceNumber());

            address_line1.setText(employeeDetails.getAddressLine1());
            address_line2.setText(employeeDetails.getAddressLine2());
            area.setText(employeeDetails.getArea());
            landmark.setText(employeeDetails.getLandMark());
            city.setText(employeeDetails.getCity());
            state.setText(employeeDetails.getState());
            pincode.setText(employeeDetails.getPincode());
        }else{
            getAppCompactActionBar().setTitle("Add Employee");
        }

    }

    @Override
    public void onClick(View view) {

        if ( view == done ){
            String eFirstName = first_name.getText();
            first_name.checkError("*Enter First Name");
            String eLastName = last_name.getText();
            String eMobile = mobile.getText();
            mobile.checkError("* Enter Mobile Number");
            String eAlternateMobile = alternate_mobile.getText();
            String eEmail = email.getText();
            List<String> eRoles = Constants.getFormattedRoles(roles.getObjects());
            //rolesView.checkError("* Enter Roles");//TODO Unable to fix issue with it
            String eESINumber = esi_number.getText();
            String eDrivingLicenseNumber = driving_license_number.getText();
            driving_license_number.checkError("* Enter Driving License Number");
            String eVehicleOwnType = "";
            if ( companyOwnedVehicle.isChecked() ){ eVehicleOwnType = "co"; }
            else if ( selfOwnedVehicle.isChecked() ){   eVehicleOwnType = "so"; }
            String eVehicleRegistrationNumber = vehicle_registration_number.getText();
            //vehicle_registration_number.checkError("* Enter Vehicle Registration Number");
            String eVehicleInsuranceProvider = vehicle_insurance_provider.getText();
            //vehicle_insurance_provider.checkError("* Enter Vehicle Inusurance Provider");
            String eVehicleInsuranceNumber = vehicle_insurance_number.getText();
            //vehicle_insurance_number.checkError("* Enter Vehicle Insurance Number");
            String ePassword = password.getText();
            password.checkError("* Enter Password");
            String eAddressLine1 = address_line1.getText();
            address_line1.checkError("* Enter Address Line1");
            String eAddressLine2 = address_line2.getText();
            String eArea = area.getText();
            area.checkError("* Enter Area");
            String eLandmark = landmark.getText();
            String eCity = city.getText();
            city.checkError("* Enter City");
            String eState = state.getText();
            state.checkError("* Enter State");
            String ePincode = pincode.getText();
            pincode.checkError("* Enter Pincode");


            if ( eRoles != null && eRoles.size() > 0 ){       // TODO Fis TextInputLayou ShowError issue with it
                if ( eFirstName.length() > 0 && eMobile.length() > 0 && eDrivingLicenseNumber.length() > 0 //&& eRoles.size() > 0
                        && ( ePassword.length() > 0 || actionType.equalsIgnoreCase(Constants.EDIT) )
                        && eAddressLine1.length() > 0 && eArea.length() > 0 && eCity.length() > 0 && eState.length() > 0 && ePincode.length() > 0 ){
                    if (CommonMethods.isValidPhoneNumber(eMobile) ){
                        if ( eEmail == null || eEmail.length() == 0 || CommonMethods.isValidEmailAddress(eEmail) ){
                            RequestAddOrEditEmployee requestObject = new RequestAddOrEditEmployee(eFirstName, eLastName, eMobile, (eAlternateMobile!=null&&eAlternateMobile.length()>0)?eAlternateMobile:null, (eEmail!=null&&eEmail.length()>0)?eEmail:null, eRoles, actionType.equalsIgnoreCase(Constants.EDIT)?null:ePassword, eESINumber, eDrivingLicenseNumber, eVehicleRegistrationNumber, eVehicleOwnType.length()==0?null:eVehicleOwnType, eVehicleInsuranceProvider, eVehicleInsuranceNumber, eAddressLine1, eAddressLine2, eArea, eLandmark, eCity, eState, ePincode);
                            if ( actionType.equalsIgnoreCase(Constants.ADD) ){
                                addEmployee(requestObject);
                            }else if ( actionType.equalsIgnoreCase(Constants.EDIT) ){
                                updateEmployee(requestObject);
                            }
                        }
                        else{
                            showToastMessage("* Enter valid email address");
                        }
                    }
                    else{
                        showToastMessage("* Enter Valid Mobile Number");
                    }
                }else{
                    showToastMessage("* Enter all required fields");
                }
            }else{
                showToastMessage("* Enter roles");
            }
        }
    }

    public void addEmployee(final RequestAddOrEditEmployee requestObject){

        showLoadingDialog("Creating employee, wait...");
        getBackendAPIs().getEmployeesAPI().addEmployee(requestObject, new EmployeesAPI.EmployeeDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, EmployeeDetails employeeDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Employee created successfully");
                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                    resultData.putExtra(Constants.USER_DETAILS, new Gson().toJson(employeeDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }
                else{
                    showChoiceSelectionDialog("Failure", "Something went wrong while creating employee, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                addEmployee(requestObject);
                            }
                        }
                    });
                }
            }
        });
    }

    public void updateEmployee(final RequestAddOrEditEmployee requestObject){

        showLoadingDialog("Updating employee details, wait...");
        getBackendAPIs().getEmployeesAPI().updateEmployee(employeeDetails.getID(), requestObject, new EmployeesAPI.EmployeeDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, EmployeeDetails employeeDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Employee details updated successfully");
                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.EDIT);
                    resultData.putExtra(Constants.USER_DETAILS, new Gson().toJson(employeeDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }
                else{
                    showChoiceSelectionDialog("Failure", "Something went wrong while updating employee details, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                addEmployee(requestObject);
                            }
                        }
                    });
                }
            }
        });
    }

    //============

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
