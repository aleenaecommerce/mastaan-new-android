package com.aleenaecommerce.operations.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.os.Bundle;

import android.view.MenuItem;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.aleena.common.adapters.ShareAppsAdapter;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.constants.Constants;

import java.util.ArrayList;
import java.util.List;

public class ShareActivity extends OperationsToolBarActivity {

    String shareTitle, shareMessage;

    vRecyclerView shareAppsHolder;
    ShareAppsAdapter shareAppsAdapter;
    ShareAppsAdapter.CallBack shareAppsCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        hasLoadingView();

        shareTitle = getIntent().getStringExtra(Constants.SHARE_TITLE);
        shareMessage = getIntent().getStringExtra(Constants.SHARE_MESSAGE);

        if ( getIntent().getStringExtra(Constants.ACTIVITY_TITLE) != null ) {
            actionBar.setTitle(getIntent().getStringExtra(Constants.ACTIVITY_TITLE));
        }

        //................................................

        shareAppsHolder = (vRecyclerView) findViewById(R.id.shareAppsHolder);
        shareAppsHolder.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false);
        shareAppsHolder.setLayoutManager(layoutManager);
        shareAppsHolder.setItemAnimator(new DefaultItemAnimator());

        shareAppsCallback = new ShareAppsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                ResolveInfo appInfo = shareAppsAdapter.getAppInfo(position);

                ActivityInfo activity = appInfo.activityInfo;
                ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, (shareMessage));
                shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                shareIntent.setComponent(name);
                startActivity(shareIntent);             //  Opening Sharing App
            }
        };

        shareAppsAdapter = new ShareAppsAdapter(context, new ArrayList<ResolveInfo>(), shareAppsCallback);
        shareAppsHolder.setAdapter(shareAppsAdapter);

        getShareAppsList();         //      Get Share Apps

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getShareAppsList();
    }

    public void getShareAppsList(){

        showLoadingIndicator("Loading availabe sharing apps, wait..");
        CommonMethods.getShareAppsList(context, new CommonMethods.ShareAppsListCallback(){
            @Override
            public void onComplete(List<ResolveInfo> shareAppsList) {
                if ( shareAppsList != null ) {
                    if (shareAppsList.size() > 0) {
                        shareAppsAdapter.addItems(shareAppsList);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No sharing apps found ");
                    }
                }else{
                    showReloadIndicator("Unable to load available sharing apps, try again!");
                }

            }
        });
    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
