package com.aleenaecommerce.operations.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vEditText;
import com.aleena.common.widgets.vRecyclerView;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.adapters.EmployeesAdapter;
import com.aleenaecommerce.operations.backend.AttendanceAPI;
import com.aleenaecommerce.operations.backend.EmployeesAPI;
import com.aleenaecommerce.operations.backend.models.RequestCheckIn;
import com.aleenaecommerce.operations.backend.models.RequestMarkLeave;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.fragments.FilterEmployeesFragment;
import com.aleenaecommerce.operations.methods.BroadcastReceiversMethods;
import com.aleenaecommerce.operations.methods.ComparisionMethods;
import com.aleenaecommerce.operations.methods.SearchMethods;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.models.SearchDetails;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class EmployeesActivity extends OperationsToolBarActivity implements View.OnClickListener{

    DrawerLayout drawerLayout;
    FilterEmployeesFragment filterFragment;

    MenuItem searchViewMenuITem;
    SearchView searchView;
    MenuItem filterMenuITem;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    View addEmployee;

    vRecyclerView employeesHolder;
    EmployeesAdapter employeesAdapter;

    List<EmployeeDetails> originalEmployeesList = new ArrayList<>();

    SearchDetails searchDetails;

    EmployeeDetails employeeDetails;

    int selectedItemPosition;

    public interface SearchResultsCallback{
        void onComplete(List<EmployeeDetails> searchList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employees);
        hasLoadingView();
        hasSwipeRefresh();

        filterFragment = new FilterEmployeesFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterEmployeesFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    onClearSearch();
                } else if (searchDetails.getAction().equalsIgnoreCase(Constants.ANALYSE)) {
                    showToastMessage("Under construction / Not yet done..");
                    //new TotalStatisticsDialog(activity, items).show();
                } else {
                    onPerformSearch(searchDetails);
                }
            }
        });

        employeeDetails = getLocalStorageData().getUserDetails();

        //-----------------

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        addEmployee = findViewById(R.id.addEmployee);
        addEmployee.setVisibility(employeeDetails.isSuperUser()?View.VISIBLE:View.GONE);
        addEmployee.setOnClickListener(this);

        employeesHolder = (vRecyclerView) findViewById(R.id.employeesHolder);
        setupHolder();

        //------------------

        getEmployees();        //  LOADING DATA

    }

    @Override
    public void onClick(View view) {
        if ( view == clearFilter ){
            onClearSearch();
        }
        else if ( view == addEmployee ){
            Intent addEmployeeAct = new Intent(context, AddOrEditEmployeeActivity.class);
            addEmployeeAct.putExtra(Constants.ACTION_TYPE, Constants.ADD);
            startActivityForResult(addEmployeeAct, Constants.ADD_OR_EDIT_EMPLOYEE_ACTIVITY_CODE);
        }
    }

    public void setupHolder(){

        employeesHolder.setHasFixedSize(true);
        employeesHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        employeesHolder.setItemAnimator(new DefaultItemAnimator());

        employeesAdapter = new EmployeesAdapter(context, new ArrayList<EmployeeDetails>(), new EmployeesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Intent employeeDetailsAct = new Intent(context, EmployeeDetailsActivity.class);
                employeeDetailsAct.putExtra("employeeDetails", new Gson().toJson(employeesAdapter.getItem(position)));
                startActivity(employeeDetailsAct);
            }

            @Override
            public void onMenuClick(final int position, View view) {
                selectedItemPosition = position;

                PopupMenu popup = new PopupMenu(context, view);;//new PopupMenu(getActivity().getSupportActionBar().getThemedContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_employee, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if ( menuItem.getItemId() == R.id.action_edit ) {
                            Intent editEmployeeAct = new Intent(context, AddOrEditEmployeeActivity.class);
                            editEmployeeAct.putExtra(Constants.ACTION_TYPE, Constants.EDIT);
                            editEmployeeAct.putExtra(Constants.USER_DETAILS, new Gson().toJson(employeesAdapter.getItem(position)));
                            startActivityForResult(editEmployeeAct, Constants.ADD_OR_EDIT_EMPLOYEE_ACTIVITY_CODE);
                        }
                        return true;
                    }
                });
            }

            @Override
            public void onCheckIn(final int position) {
                isUserAllowedToCheckInEmployees(new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String location) {
                        if ( status ){
                            checkINUser(position, new RequestCheckIn(location, getLocalStorageData().getUserID()));
                        }else{
                            showNotificationDialog("Failure!", "You are only allowed to check in other employees at warehouse");
                        }
                    }
                });
            }

            @Override
            public void onCheckOut(int position) {
                checkOUTUser(position);
            }

            private void isUserAllowedToCheckInEmployees(final StatusCallback callback){
                // IF USER IS SUPER USER NO NEED TO BE AT WAREHOUSE
                if ( employeeDetails.isSuperUser() ){
                    getCurrentLocation(new LocationCallback() {
                        @Override
                        public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                            callback.onComplete(true, 200, location!=null?location.getLatitude()+","+location.getLongitude():null);
                        }
                    });
                }
                // IF USER IS WAREHOUSE MANAGER HE/SHE NEED TO BE AT WAREHOUSE
                else {
                    getCurrentLocation(new LocationCallback() {
                        @Override
                        public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                            if (status) {
                                if (ComparisionMethods.isAllowedWifiNetwork(getConnectedWifiDetails(), getLocalStorageData().getWifiNetworks(), latLng)) {
                                    callback.onComplete(true, 200, location!=null?location.getLatitude()+","+location.getLongitude():null);
                                } else {
                                    callback.onComplete(false, 500, null);
                                }
                            } else {
                                showToastMessage(message);
                            }
                        }
                    });
                }
            }

            @Override
            public void onMarkLeave(final int position) {
                View markLeaveDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_mark_leave, null);
                final vEditText date = (vEditText) markLeaveDialogView.findViewById(R.id.date);
                final RadioButton paidLeave = (RadioButton) markLeaveDialogView.findViewById(R.id.paidLeave);
                final RadioButton unpaidLeave = (RadioButton) markLeaveDialogView.findViewById(R.id.unpaidLeave);
                date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String eDate = date.getText().toString();
                        showDatePickerDialog("Select date", eDate.length()>0?eDate:DateMethods.getCurrentDate(), new DatePickerCallback() {
                            @Override
                            public void onSelect(String fullDate, int day, int month, int year) {
                                date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                            }
                        });
                    }
                });

                markLeaveDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String eDate = date.getText().toString();
                        if ( eDate.length() > 0 && (paidLeave.isChecked() || unpaidLeave.isChecked()) ){
                            closeCustomDialog();
                            RequestMarkLeave requestMarkLeave = new RequestMarkLeave(eDate, paidLeave.isChecked()? true:false);
                            markLeave(position, requestMarkLeave);
                        }else{
                            showToastMessage("* Enter all details");
                        }
                    }
                });
                markLeaveDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        closeCustomDialog();
                    }
                });
                showCustomDialog(markLeaveDialogView);
            }

            @Override
            public void onUnmarkLeaveToday(final int position) {
                showChoiceSelectionDialog("Confirm!", "Do you want to unmark leave today?", "CANCEL", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if (choiceName.equalsIgnoreCase("YES")){
                            unmarkLeave(position);
                        }
                    }
                });
            }

            @Override
            public void onCallDeliveryBoy(int position) {
                callPhoneNumber(employeesAdapter.getItem(position).getMobile());
            }
        });
        employeesHolder.setAdapter(employeesAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getEmployees();
    }

    public void getEmployees(){

        hideMenuItems();
        searchResultsCountIndicator.setVisibility(View.GONE);
        addEmployee.setVisibility(View.GONE);
        try{filterFragment.clearSelection();}catch (Exception e){}
        showLoadingIndicator("Loading employees, wait..");
        getBackendAPIs().getEmployeesAPI().getEmployees(new EmployeesAPI.EmployeesListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<EmployeeDetails> deliveryBoysList) {
                if (status == true) {
                    addEmployee.setVisibility(View.VISIBLE);

                    originalEmployeesList = deliveryBoysList;
                    if (deliveryBoysList.size() > 0) {
                        employeesAdapter.setItems(deliveryBoysList);
                        showMenuItems();
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No employees");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load employees, try again!");
                }
            }
        });
    }

    public void checkINUser(final int position, final RequestCheckIn requestCheckIn){
        showLoadingDialog("Checking in, wait...");
        getBackendAPIs().getAttendanceAPI().checkINUser(employeesAdapter.getItem(position).getID(), requestCheckIn, new AttendanceAPI.AttendanceCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final AttendanceDetails attendanceDetails) {
                if (status) {
                    attendanceDetails.setLastCheckedInByEmployeeDetails(employeeDetails);
                    attendanceDetails.setLastCheckInPlaceLatLng(requestCheckIn.getLastCheckInPlaceLatLng());
                    getGooglePlacesAPI().getPlaceDetailsByLatLng(attendanceDetails.getLastCheckInPlaceLatLng(), new GooglePlacesAPI.PlaceDetailsCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                            closeLoadingDialog();

                            attendanceDetails.setLastCheckInPlaceDetails(placeDetails);
                            /*employeesAdapter.getItem(position).getAttendanceDetails().setLastCheckedInByEmployeeDetails(getLocalStorageData().getUserDetails());
                            employeesAdapter.getItem(position).getAttendanceDetails().setLastCheckInPlaceLatLng(requestCheckIn.getLastCheckInPlaceLatLng());
                            employeesAdapter.getItem(position).getAttendanceDetails().setLastCheckInPlaceDetails(getGooglePlacesAPI().getPlaceDetailsByLatLng(LatLngMethods.getLatLngFromString(requestCheckIn.getLastCheckInPlaceLatLng())).getItem());*/

                            showSnackbarMessage("Checked in sucessfully.");
                            updateAttendanceDetails(position, attendanceDetails);

                            if (employeesAdapter.getItem(position).getID().equals(getLocalStorageData().getUserID())) {
                                EmployeeDetails employeeDetails = getLocalStorageData().getUserDetails();
                                employeeDetails.setAttendanceDetails(attendanceDetails);
                                getLocalStorageData().storeUserDetails(employeeDetails);

                                BroadcastReceiversMethods.updatePinnedNotification(context);
                            }
                        }
                    });
                } else {
                    closeLoadingDialog();
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to check in, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            checkINUser(position, requestCheckIn);
                        }
                    });
                }
            }
        });
    }

    public void checkOUTUser(final int position){

        showLoadingDialog("Checking out, wait...");
        getBackendAPIs().getAttendanceAPI().checkOUTUser(employeesAdapter.getItem(position).getID(), new AttendanceAPI.AttendanceCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, AttendanceDetails attendanceDetails) {
                closeLoadingDialog();
                if (status) {
                    showSnackbarMessage("Checked out sucessfully.");
                    updateAttendanceDetails(position, attendanceDetails);

                    if (employeesAdapter.getItem(position).getID().equals(getLocalStorageData().getUserID())) {
                        EmployeeDetails employeeDetails = getLocalStorageData().getUserDetails();
                        employeeDetails.setAttendanceDetails(attendanceDetails);
                        getLocalStorageData().storeUserDetails(employeeDetails);

                        BroadcastReceiversMethods.updatePinnedNotification(context);
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to check out, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            checkOUTUser(position);
                        }
                    });
                }
            }
        });
    }

    public void markLeave(final int position, final RequestMarkLeave requestMarkLeave){

        showLoadingDialog("Marking leave, wait...");
        getBackendAPIs().getAttendanceAPI().markLeave(employeesAdapter.getItem(position).getID(), requestMarkLeave, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    if ( DateMethods.compareDates(requestMarkLeave.getDate(), DateMethods.getOnlyDate(getLocalStorageData().getServerTime())) == 0 ){
                        updateLeaveDetails(position, true);
                    }
                    showToastMessage("Successfully marked leave");
                } else {
                    showSnackbarMessage("Unable to mark leave, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            markLeave(position, requestMarkLeave);
                        }
                    });
                }
            }
        });
    }

    public void unmarkLeave(final int position){

        showLoadingDialog("Unmarking leave, wait...");
        getBackendAPIs().getAttendanceAPI().unmarkLeave(employeesAdapter.getItem(position).getAttendanceDetails().getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    updateLeaveDetails(position, false);
                    showToastMessage("Successfully unmarked leave");
                } else {
                    showSnackbarMessage("Unable to unmark leave, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            unmarkLeave(position);
                        }
                    });
                }
            }
        });
    }

    //------------

    public void updateAttendanceDetails(int position, AttendanceDetails attendanceDetails){
        employeesAdapter.updateAttendanceDetails(position, attendanceDetails);

        String userID = employeesAdapter.getItem(position).getID();
        for (int i=0;i< originalEmployeesList.size();i++){
            if ( originalEmployeesList.get(i).getID().equals(userID) ){
                originalEmployeesList.get(i).setAttendanceDetails(attendanceDetails);
                break;
            }
        }
    }

    public void updateLeaveDetails(int position, boolean isLeave){
        employeesAdapter.updateLeaveDetails(position, isLeave);

        String userID = employeesAdapter.getItem(position).getID();
        for (int i=0;i< originalEmployeesList.size();i++){
            if ( originalEmployeesList.get(i).getID().equals(userID) ){
                originalEmployeesList.get(i).getAttendanceDetails().setLeave(isLeave);
                break;
            }
        }
    }

    //--------------

    public void onPerformSearch(final SearchDetails searchDetails){
        if ( searchDetails != null ){
            this.searchDetails = searchDetails;

            searchResultsCountIndicator.setVisibility(View.GONE);
            showLoadingIndicator(Html.fromHtml("Searching\n(" + searchDetails.getSearchString() + ")"));
            SearchMethods.searchTransactions(originalEmployeesList, searchDetails, getLocalStorageData().getServerTime(), new SearchMethods.SearchUsersCallback() {
                @Override
                public void onComplete(boolean status, List<EmployeeDetails> filteredList) {
                    if ( status ){
                        if (filteredList.size() > 0) {
                            searchResultsCountIndicator.setVisibility(View.VISIBLE);
                            results_count.setText(Html.fromHtml("<b>"+filteredList.size() + "</b> result(s) found.\n(" + searchDetails.getSearchString() + ")"));

                            employeesAdapter.setItems(filteredList);
                            switchToContentPage();
                        } else {
                            searchResultsCountIndicator.setVisibility(View.VISIBLE);
                            results_count.setText("No Results");
                            showNoDataIndicator(Html.fromHtml("No results found\n(" + searchDetails.getSearchString() + ")"));

                            employeesAdapter.clearItems();
                        }
                    }else{
                        showToastMessage("Something went wrong, try again!");
                    }
                }
            });
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        searchResultsCountIndicator.setVisibility(View.GONE);
        filterFragment.clearSelection();
        employeesAdapter.setItems(originalEmployeesList);
        if ( employeesAdapter.getItemCount() > 0 ){
            switchToContentPage();
        }else{
            showNoDataIndicator("No delivery boys");
        }
    }

    //--------------

    public void showMenuItems(){
        try{    searchViewMenuITem.setVisible(true);    }catch (Exception e){}
        try{    filterMenuITem.setVisible(true);    }catch (Exception e){}
    }

    public void hideMenuItems(){
        try{    searchViewMenuITem.setVisible(false);    }catch (Exception e){}
        try{    filterMenuITem.setVisible(false);    }catch (Exception e){}
    }

    public String getSearchViewText(){
        String searchViewText = "";
        try{ searchViewText = searchView.getQuery().toString();      }catch (Exception e){}
        return searchViewText;
    }

    //====================


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.ADD_OR_EDIT_EMPLOYEE_ACTIVITY_CODE && resultCode == RESULT_OK ){
                EmployeeDetails employeeDetails = new Gson().fromJson(data.getStringExtra(Constants.USER_DETAILS), EmployeeDetails.class);
                if ( data.getStringExtra(Constants.ACTION_TYPE).equalsIgnoreCase(Constants.EDIT) ){
                    employeesAdapter.setItem(selectedItemPosition, employeeDetails);
                }else{
                    employeesAdapter.addItem(employeeDetails);
                    switchToContentPage();
                    employeesHolder.scrollToPosition(employeesAdapter.getItemCount()-1);
                }
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_employees, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filterMenuITem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchDetails searchDetails = new SearchDetails();
                searchDetails.setUserName(query);
                onPerformSearch(searchDetails);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){
        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
