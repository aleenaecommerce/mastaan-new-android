package com.aleenaecommerce.operations.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.fragments.SideBarFragment;
import com.aleenaecommerce.operations.models.EmployeeDetails;

public class HomeActivity extends OperationsNavigationDrawerActivity {

    SideBarFragment sideBarFragment;

    EmployeeDetails employeeDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sideBarFragment = new SideBarFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();

        employeeDetails = getLocalStorageData().getUserDetails();

        showToastMessage("Logged in as " + employeeDetails.getFullName().toUpperCase()+" from "+getDeviceName());

        //-------------

        if ( employeeDetails.isDeliveryBoy() ){
            Intent attendanceAct = new Intent(context, AttendanceActivity.class);
            attendanceAct.putExtra("displayDirectly", true);
            startActivity(attendanceAct);
        }

        //if ( employeeDetails.isDeliveryBoy() == true && employeeDetails.isProcessingManager() == false ){
             //showPinnedNotification();
        //}

    }

    /*public void showPinnedNotification(){
        //        vNotification pinnedNotification = new vNotification(context);
//        pinnedNotification.makePinnedNotification();
//        View notificationView = LayoutInflater.from(context).inflate(R.layout.pinned_notification, null);
//        notificationView.findViewById(R.id.image).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showToastMessage("CLICKED IMAGE");
//            }
//        });
//        pinnedNotification.setCustomView(notificationView);
//        pinnedNotification.setLaunchActivity(new Intent(context, HomeActivity.class));
//        pinnedNotification.show();


        Notification notification = new Notification(R.drawable.ic_launcher, "Pinned Notification", System.currentTimeMillis());
        NotificationManager mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.pinned_notification);
//        contentView.setImageViewResource(R.id.image, R.drawable.ic_launcher);
//        contentView.setTextViewText(R.id.title, "Custom notification");
//        contentView.setTextViewText(R.id.text, "This is a custom layout");


        String displayMessage = "";
        String serverTime = DateMethods.getOnlyDate(getLocalStorageData().getServerTime());
        String lastCheckInTime = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(getLocalStorageData().getCheckInTime()));

        if ( getLocalStorageData().getCheckInStatus()
                && DateMethods.compareDates(serverTime, lastCheckInTime) == 0 ){
            displayMessage += "Check In: <b>"+DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(getLocalStorageData().getCheckInTime()), DateConstants.MMM_DD_HH_MM_A)+"</b>";
            if ( employeeDetails.getAttendanceDetails().getCheckInDelay() > 0 ){
                displayMessage += "<br>Check In Delay: <b>"+((int) employeeDetails.getAttendanceDetails().getCheckInDelay())+"min</b>";
            }

        }else{
            displayMessage += "Not checked in yet.";
        }

        contentView.setTextViewText(R.id.message, Html.fromHtml(displayMessage));

        notification.contentView = contentView;

//        Intent notificationIntent = new Intent(this, AttendanceActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
//        notification.contentIntent = contentIntent;

        notification.flags |= Notification.FLAG_NO_CLEAR; //Do not clear the notification
        notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
        notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
        notification.defaults |= Notification.DEFAULT_SOUND; // Sound

        mNotificationManager.notify(1, notification);
    }*/

    //============

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerListener.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
