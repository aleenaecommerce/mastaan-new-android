package com.aleenaecommerce.operations.activities;

import android.os.Bundle;
import android.view.MenuItem;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.fragments.AttendanceReportFragment;

public class AttendanceReportActivity extends OperationsToolBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_report);

        AttendanceReportFragment attendanceReportFragment = new AttendanceReportFragment();
        Bundle bundle = new Bundle();
        bundle.putString("userID", getLocalStorageData().getUserID());
        attendanceReportFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.attendanceDetailsView, attendanceReportFragment).commit();


    }

    //==========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
