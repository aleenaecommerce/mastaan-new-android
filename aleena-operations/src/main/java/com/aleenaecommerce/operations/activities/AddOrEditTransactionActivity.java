package com.aleenaecommerce.operations.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.methods.CameraMethods;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.FileChooserMethods;
import com.aleena.common.widgets.vEditText;
import com.aleena.common.widgets.vSpinner;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.backend.FinanceAPI;
import com.aleenaecommerce.operations.backend.models.RequestCreateOrUpdateTransaction;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.models.AccountDetails;
import com.aleenaecommerce.operations.models.PrefillDetails;
import com.aleenaecommerce.operations.models.TransactionDetails;
import com.aleenaecommerce.operations.models.TransactionTypeDetails;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AddOrEditTransactionActivity extends OperationsToolBarActivity implements View.OnClickListener{

    String actionType = Constants.ADD;

    vSpinner transactions_dropdown;
    vSpinner credit_accounts_dropdown;
    vSpinner debit_accounts_dropdown;
    vEditText amount;
    vEditText entry_date;
    vEditText comments;
    ImageView attachment;
    View deleteAttachment;
    Button submit;

    String selectedTransactionID;
    File selectedAttachment;
    boolean removeAttachment;

    PrefillDetails prefillDetails;

    FileChooserMethods fileChooserMethods;
    CameraMethods cameraMethods;

    TransactionDetails transactionDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_transaction);
        hasLoadingView();

        actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        if ( actionType == null || actionType.length() == 0 ){  actionType = Constants.ADD;   }

        //-----------------

        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(this);

        transactions_dropdown = (vSpinner) findViewById(R.id.transactions_dropdown);
        credit_accounts_dropdown = (vSpinner) findViewById(R.id.credit_accounts_dropdown);
        debit_accounts_dropdown = (vSpinner) findViewById(R.id.debit_accounts_dropdown);
        amount = (vEditText) findViewById(R.id.amount);
        entry_date = (vEditText) findViewById(R.id.entry_date);
        entry_date.setOnClickListener(this);
        comments = (vEditText) findViewById(R.id.comments);

        attachment = (ImageView) findViewById(R.id.attachment);
        attachment.setOnClickListener(this);
        deleteAttachment = findViewById(R.id.deleteAttachment);
        deleteAttachment.setOnClickListener(this);

        //--------

        transactions_dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String transactionID = "";
                try {
                    transactionID = prefillDetails.getTransactionTypesList().get(transactions_dropdown.getSelectedItemPosition()).getID();
                } catch (Exception e) {
                }

                if (transactionID.length() > 0 && transactionID.equalsIgnoreCase("select") == false) {
                    getTransactionAccounts(transactionID);
                }else{
                    findViewById(R.id.fillingView).setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //-------

        if ( actionType.equalsIgnoreCase(Constants.EDIT) ) {
            actionBar.setTitle("Edit transaction");
            submit.setText("UPDATE");
            transactionDetails = new Gson().fromJson(getIntent().getStringExtra("transaction_details_json"), TransactionDetails.class);
        }

        //-------

        getPrefillData();

    }

    @Override
    public void onClick(View view) {

        if ( view == entry_date ){
            showDatePickerDialog("Entry date", null, getLocalStorageData().getServerTime(), entry_date.getText().toString(), new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    entry_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }

        else if ( view == attachment ){
            showListChooserDialog(new String[]{"Upload Image / File", "Take Image"}, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( position == 0 ){
                        fileChooserMethods = new FileChooserMethods(context).showFileChooser(/*"image/*",*/ new FileChooserMethods.FileChooserCallback() {
                            @Override
                            public void onComplete(boolean status, Uri uri, File file, String filePath) {
                                selectedAttachment = file;
                                removeAttachment = false;

                                if ( filePath.toLowerCase().contains(".jpg") || filePath.toLowerCase().contains(".jpeg")
                                        || filePath.toLowerCase().contains(".png") ){
                                    Picasso.get()
                                            .load(file)
                                            .placeholder(R.drawable.image_default)
                                            .error(R.drawable.image_default)
                                            .into(attachment);
                                }else{
                                    Picasso.get()
                                            .load(R.drawable.ic_file)
                                            .placeholder(R.drawable.ic_file)
                                            .error(R.drawable.ic_file)
                                            .into(attachment);
                                }
                            }
                        });
                    }
                    else if ( position == 1 ){
                        cameraMethods = new CameraMethods(context).captureImage(new CameraMethods.CaptureImageCallback() {
                            @Override
                            public void onComplete(boolean status, File capturedFile, Bitmap capturedBitmap) {
                                selectedAttachment = capturedFile;
                                removeAttachment = false;

                                Picasso.get()
                                        .load(capturedFile)
                                        .placeholder(R.drawable.image_default)
                                        .error(R.drawable.image_default)
                                        .into(attachment);
                            }
                        });
                    }
                }
            });
        }

        else if ( view == deleteAttachment ){
            selectedAttachment = null;
            removeAttachment = true;
            attachment.setImageResource(0);
            deleteAttachment.setVisibility(View.GONE);
        }

        else if ( view == submit ){
            String amountEntered = amount.getText().toString().trim();
            String entryDate = DateMethods.getDateInFormat(entry_date.getText().toString().trim(), "dd/MM/yyyy");
            String commentss = comments.getText().toString().trim();
            String transactionID = "";
            try{    transactionID = prefillDetails.getTransactionTypesList().get(transactions_dropdown.getSelectedItemPosition()).getID();}catch (Exception e){}
            String debitAccount = "";
            try{    debitAccount = prefillDetails.getDebitAccounstList().get(debit_accounts_dropdown.getSelectedItemPosition()).getID();}catch (Exception e){}
            String creditAccount = "";
            try{    creditAccount = prefillDetails.getCreditAccounstList().get(credit_accounts_dropdown.getSelectedItemPosition()).getID();}catch (Exception e){}

            if ( amountEntered.length() > 0 && transactionID.length() > 0 && creditAccount.length() > 0 && debitAccount.length() > 0 && entryDate.length() > 0 ){
                if ( Double.parseDouble(amountEntered) > 0 ){
                    if ( transactionID.length() > 0 && transactionID.equalsIgnoreCase("select") == false ){
                        if ( creditAccount.length() > 0 && creditAccount.equalsIgnoreCase("select") == false ){
                            if ( debitAccount.length() > 0 && debitAccount.equalsIgnoreCase("select") == false ){
                                if ( entryDate.length() > 0 ){

                                    RequestCreateOrUpdateTransaction requestCreateOrUpdateTransaction = new RequestCreateOrUpdateTransaction(transactionID, debitAccount, creditAccount, Double.parseDouble(amountEntered), entryDate, commentss);

                                    if ( submit.getText().toString().equalsIgnoreCase("Add") || actionType.equalsIgnoreCase(Constants.ADD) ) {
                                        addTransaction(requestCreateOrUpdateTransaction);
                                    }else{
                                        updateTransaction(requestCreateOrUpdateTransaction);
                                    }
                                }else{
                                    entry_date.showError("* Fill entry date");
                                }
                            }else{
                                activity.showToastMessage("* Select To account");
                            }
                        }else{
                            activity.showToastMessage("* Select From account");
                        }
                    }else{
                        activity.showToastMessage("* Select transaction");
                    }
                }else{
                    amount.showError("* Enter valid amount");
                }
            }else{
                activity.showToastMessage("* Enter all details");
            }
        }
    }

    //-----

    private void display(boolean displayAll){
        if ( displayAll == false ) {
            ArrayAdapter<String> transactionsAdapter = new ListArrayAdapter(activity, prefillDetails.getTransactionTypeStringsList());
            transactionsAdapter.setDropDownViewResource(R.layout.view_list_item);
            transactions_dropdown.setAdapter(transactionsAdapter);
            transactions_dropdown.setSelection(0);

            // EDIT > Prefilling
            if ( actionType.equalsIgnoreCase(Constants.EDIT) && transactionDetails != null ){
                transactions_dropdown.setSelection(getSelectedTransactionTypeIndex(prefillDetails.getTransactionTypesList(), transactionDetails.getTransactionTypeDetails().getID()));
            }

        }else{
            findViewById(R.id.fillingView).setVisibility(View.VISIBLE);

            entry_date.setText(DateMethods.getDateInFormat(getLocalStorageData().getServerTime(), DateConstants.MMM_DD_YYYY));

            ArrayAdapter<String> creditAccountsAdapter = new ListArrayAdapter(activity, prefillDetails.getCreditAccountStringsList());
            creditAccountsAdapter.setDropDownViewResource(R.layout.view_list_item);
            credit_accounts_dropdown.setAdapter(creditAccountsAdapter);
            credit_accounts_dropdown.setSelection(0);

            ArrayAdapter<String> debitAccountsAdapter = new ListArrayAdapter(activity, prefillDetails.getDebitAccountStringsList());
            debitAccountsAdapter.setDropDownViewResource(R.layout.view_list_item);
            debit_accounts_dropdown.setAdapter(debitAccountsAdapter);
            debit_accounts_dropdown.setSelection(0);

            // EDIT > Prefilling
            if ( actionType.equalsIgnoreCase(Constants.EDIT) && transactionDetails != null ){
                credit_accounts_dropdown.setSelection(getSelectedAccountIndex(prefillDetails.getCreditAccounstList(), transactionDetails.getCreditAccountDetails().getID()));
                debit_accounts_dropdown.setSelection(getSelectedAccountIndex(prefillDetails.getDebitAccounstList(), transactionDetails.getDebitAccountDetails().getID()));

                amount.setText(CommonMethods.getInDecimalFormat(transactionDetails.getAmount())+"");
                entry_date.setText(DateMethods.getDateInFormat(transactionDetails.getEntryDate(), DateConstants.MMM_DD_YYYY));
                comments.setText(transactionDetails.getComments());

                if ( transactionDetails.getAttachment() != null ) {
                    Picasso.get()
                            .load(transactionDetails.getAttachment())
                            .placeholder(R.drawable.image_default)
                            .error(R.drawable.image_default)
                            .into(attachment);
                }
            }
        }

        //-----

        switchToContentPage();
    }

    public int getSelectedTransactionTypeIndex(List<TransactionTypeDetails> transactionTypes, String selectedTransactionID){
        if ( transactionTypes != null && transactionTypes.size() > 0 && selectedTransactionID != null && selectedTransactionID.length() > 0 ){
            for (int i=0;i<transactionTypes.size();i++){
                if ( transactionTypes.get(i).getID().equals(selectedTransactionID) ){
                    return i;
                }
            }
        }
        return 0;
    }

    public int getSelectedAccountIndex(List<AccountDetails> accountsList, String selectedAccountID){
        if ( accountsList != null && accountsList.size() > 0 && selectedAccountID != null && selectedAccountID.length() > 0 ){
            for (int i=0;i<accountsList.size();i++){
                if ( accountsList.get(i).getID().equals(selectedAccountID) ){
                    return i;
                }
            }
        }
        return 0;
    }


    //------

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPrefillData();
    }

    public void getPrefillData() {

        showLoadingIndicator("Loading, wait..");
        getBackendAPIs().getFinanceAPI().getTransactionsTypes(new FinanceAPI.TransactionsTypesListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<TransactionTypeDetails> transactionTypesList) {
                if (status) {
                    prefillDetails = new PrefillDetails();
                    prefillDetails.setTransactionTypesList(transactionTypesList);
                    display(false);
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Something went wrong, try again");
                }
            }
        });
    }

    List<TransactionAccounts> offlineTransactionAccounts = new ArrayList<>();
    public void getTransactionAccounts(final String transactionID){
        this.selectedTransactionID = transactionID;

        findViewById(R.id.fillingView).setVisibility(View.GONE);

        final TransactionAccounts offlineAccountsDetails = getTransacAccounts(transactionID);
        if ( offlineAccountsDetails != null ){
            prefillDetails.setCreditAccounstList(offlineAccountsDetails.getCreditAccounts());
            prefillDetails.setDebitAccounstList(offlineAccountsDetails.getDebitAccounts());
            display(true);
        }
        else{
            showLoadingDialog("Loading, wait..");
            getBackendAPIs().getFinanceAPI().getTransactionTypeAccounts(transactionID, new FinanceAPI.TransactionsAccountsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<AccountDetails> creditAccounts, List<AccountDetails> debitAccounts) {
                    closeLoadingDialog();
                    if (status) {
                        offlineTransactionAccounts.add(new TransactionAccounts(transactionID, creditAccounts, debitAccounts));
                        prefillDetails.setCreditAccounstList(creditAccounts);
                        prefillDetails.setDebitAccounstList(debitAccounts);
                        display(true);
                    } else {
                        checkSessionValidity(statusCode);
                        showChoiceSelectionDialog("Failure!", "Something went wrong.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                    getTransactionAccounts(transactionID);
                                } else {
                                    transactions_dropdown.setSelection(0);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void addTransaction(final RequestCreateOrUpdateTransaction requestCreateOrUpdateTransaction){
        showLoadingDialog("Adding transaction, wait...");
        getBackendAPIs().getFinanceAPI().createPendingTransaction(requestCreateOrUpdateTransaction, selectedAttachment, new FinanceAPI.AddTransactionCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, RequestCreateOrUpdateTransaction responseObject) {
                if (status) {
                    showToastMessage("Added transaction successfully");

                    TransactionDetails transactionDetails = new TransactionDetails(responseObject, prefillDetails.getTransactionTypesList(), prefillDetails.getCreditAccounstList(), prefillDetails.getDebitAccounstList())
                            .setCreatorDetails(getLocalStorageData().getUserDetails())
                            .setCreatedDate(DateMethods.getDateInUTCFormat(getLocalStorageData().getServerTime()));

                    Intent transactionDetailsAct = new Intent(context, TransactionDetailsActivity.class);
                    transactionDetailsAct.putExtra(Constants.ID, transactionDetails.getID());
                    startActivity(transactionDetailsAct);
                    finish();
                } else {
                    checkSessionValidity(statusCode);
                    closeLoadingDialog();
                    showSnackbarMessage("Unable to add transaction, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            addTransaction(requestCreateOrUpdateTransaction);
                        }
                    });
                }
            }
        });
    }

    public void updateTransaction(final RequestCreateOrUpdateTransaction requestCreateOrUpdateTransaction){
        showLoadingDialog("Updating transaction, wait...");
        getBackendAPIs().getFinanceAPI().updatePendingTransaction(transactionDetails.getID(), requestCreateOrUpdateTransaction, removeAttachment, selectedAttachment, new FinanceAPI.AddTransactionCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, RequestCreateOrUpdateTransaction responseObject) {
                if (status) {
                    showToastMessage("Transaction updated successfully");

                    TransactionDetails updatedTransactionDetails = new TransactionDetails(responseObject, prefillDetails.getTransactionTypesList(), prefillDetails.getCreditAccounstList(), prefillDetails.getDebitAccounstList())
                            .setCreatorDetails(transactionDetails.getCreatorDetails())
                            .setCreatedDate(transactionDetails.getCreatedDate());
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("transaction_details_json", new Gson().toJson(updatedTransactionDetails));
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                } else {
                    checkSessionValidity(statusCode);
                    closeLoadingDialog();
                    showSnackbarMessage("Unable to update transaction, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateTransaction(requestCreateOrUpdateTransaction);
                        }
                    });
                }
            }
        });
    }

    //--------

    private TransactionAccounts getTransacAccounts(String transactionID){
        if ( offlineTransactionAccounts != null ) {
            for (int i = 0; i < offlineTransactionAccounts.size(); i++) {
                if (offlineTransactionAccounts.get(i).getTransactionID().equals(transactionID)) {
                    return offlineTransactionAccounts.get(i);
                }
            }
        }
        return  null;
    }

    class TransactionAccounts{
        String transactionID;
        List<AccountDetails> creditAccounts;
        List<AccountDetails> debitAccounts;
        TransactionAccounts(String transactionID, List<AccountDetails> creditAccounts, List<AccountDetails> debitAccounts){
            this.transactionID = transactionID;
            this.creditAccounts = creditAccounts;
            this.debitAccounts = debitAccounts;
        }

        public String getTransactionID() {
            return transactionID;
        }

        public List<AccountDetails> getCreditAccounts() {
            return creditAccounts;
        }

        public List<AccountDetails> getDebitAccounts() {
            return debitAccounts;
        }
    }

    //=================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( fileChooserMethods != null ){  fileChooserMethods.onActivityResult(requestCode, resultCode, data); }
        if ( cameraMethods != null ){   cameraMethods.onActivityResult(requestCode, resultCode, data);  }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
