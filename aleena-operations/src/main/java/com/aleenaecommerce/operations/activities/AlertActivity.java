package com.aleenaecommerce.operations.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.constants.Constants;

public class AlertActivity extends Activity implements View.OnClickListener{

    Activity activity;
    MediaPlayer notificationSound;

    TextView message;
    Button actionButton;
    Button actionCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
        setFinishOnTouchOutside(false);
        activity = this;

        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);

        notificationSound = MediaPlayer.create(getApplicationContext(), R.raw.appsound);
        //notificationSound.setLooping(true);

        //-------

        message = (TextView) findViewById(R.id.message);
        actionButton = (Button) findViewById(R.id.actionButton);
        actionButton.setOnClickListener(this);
        actionCancel = (Button) findViewById(R.id.actionCancel);
        actionCancel.setOnClickListener(this);

        //-----------------

        String msg = getIntent().getStringExtra(Constants.MESSAGE);
        String action = getIntent().getStringExtra(Constants.ACTION_NAME);
        if ( msg != null && msg.length() > 0 ){
            message.setText(msg);
        }
        if ( action != null && action.length() > 0 ){
            actionCancel.setVisibility(View.VISIBLE);
            actionButton.setText(action);
        }else{
            actionCancel.setVisibility(View.GONE);
        }
        notificationSound.start();

        //------

        new CountDownTimer(50000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }
            @Override
            public void onFinish() {
                try{activity.finish();}catch (Exception e){}
            }
        }.start();

    }

    @Override
    public void onClick(View view) {
        if ( view == actionButton ){
            if ( actionButton.getText().toString().equalsIgnoreCase(Constants.CHECK_IN) ){
                Intent attendanceAct = new Intent(getApplicationContext(), AttendanceActivity.class);
                attendanceAct.putExtra(Constants.AUTO_CHECK_IN, true);
                startActivity(attendanceAct);
                finish();
            }
            else if ( actionButton.getText().toString().equalsIgnoreCase(Constants.CHECK_OUT) ){
                Intent attendanceAct = new Intent(getApplicationContext(), AttendanceActivity.class);
                attendanceAct.putExtra(Constants.AUTO_CHECK_OUT, true);
                startActivity(attendanceAct);
                finish();
            }
            else{
                finish();
            }
        }
        else if ( view == actionCancel ){
            finish();
        }
    }

    //==============

    @Override
    protected void onDestroy() {
        super.onDestroy();
        notificationSound.stop();
    }

}
