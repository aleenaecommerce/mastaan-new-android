package com.aleenaecommerce.operations.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.backend.FinanceAPI;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.dialogs.ImageDialog;
import com.aleenaecommerce.operations.models.JournalEntryCompleteDetails;
import com.aleenaecommerce.operations.models.JournalEntryTransactionDetails;

public class JournalEntryDetailsActivity extends OperationsToolBarActivity implements View.OnClickListener {

    String journalEntryID;

    TextView transaction_id;
    TextView transaction_name;
    TextView debit_account;
    TextView credit_account;
    TextView amount;
    TextView creator_name;
    TextView acceptor_name;
    TextView entry_date;
    TextView comments;
    TextView attachment;

    JournalEntryCompleteDetails journalEntryDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal_entry_details);
        hasLoadingView();

        journalEntryID = getIntent().getStringExtra(Constants.ID);

        //-----------------

        transaction_id = (TextView) findViewById(R.id.transaction_id);
        transaction_name = (TextView) findViewById(R.id.transaction_name);
        debit_account = (TextView) findViewById(R.id.debit_account);
        credit_account = (TextView) findViewById(R.id.credit_account);
        amount = (TextView) findViewById(R.id.amount);
        creator_name = (TextView) findViewById(R.id.creator_name);
        acceptor_name = (TextView) findViewById(R.id.acceptor_name);
        entry_date = (TextView) findViewById(R.id.entry_date);
        comments = (TextView) findViewById(R.id.comments);
        attachment = (TextView) findViewById(R.id.attachment);
        attachment.setOnClickListener(this);
        attachment.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboardManager.setText(journalEntryDetails.getAttachment());
                Toast.makeText(context, "Attachment URL "+journalEntryDetails.getAttachment()+" is copied", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        //------------------

        getJournalEntryDetails(journalEntryID);

    }

    @Override
    public void onClick(View view) {
        if ( view == attachment ){
            if ( journalEntryDetails.getAttachment().toLowerCase().contains(".jpg") || journalEntryDetails.getAttachment().toLowerCase().contains(".jpeg")
                    || journalEntryDetails.getAttachment().toLowerCase().contains(".png") ){
                new ImageDialog(activity).show(journalEntryDetails.getAttachment());
            }else{
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(journalEntryDetails.getAttachment()));
                activity.startActivity(browserIntent);
            }
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getJournalEntryDetails(journalEntryID);
    }

    public void displayDetails(JournalEntryCompleteDetails journalEntryDetails){
        if ( journalEntryDetails != null ){
            this.journalEntryDetails = journalEntryDetails;

            switchToContentPage();

            transaction_id.setText(journalEntryDetails.getID());
            transaction_name.setText(journalEntryDetails.getTransactionTypeDetails().getName());
            for (int i=0;i<journalEntryDetails.getTransactions().size();i++){
                JournalEntryTransactionDetails transactionDetails = journalEntryDetails.getTransactions().get(i);
                if ( transactionDetails != null ){
                    if ( transactionDetails.getAmount() > 0 ){
                        debit_account.setText(transactionDetails.getAccountDetails().getName());
                    }else{
                        credit_account.setText(transactionDetails.getAccountDetails().getName());
                    }
                }
            }
            if ( journalEntryDetails.getTransactions().size() > 0 && journalEntryDetails.getTransactions().get(0) != null ) {
                amount.setText(SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(Math.abs(journalEntryDetails.getTransactions().get(0).getAmount())));
            }
            creator_name.setText(CommonMethods.capitalizeFirstLetter(journalEntryDetails.getCreatorDetails().getFullName()));
            acceptor_name.setText(CommonMethods.capitalizeFirstLetter(journalEntryDetails.getAcceptorDetails().getFullName()));
            entry_date.setText(DateMethods.getDateInFormat(journalEntryDetails.getEntryDate(), DateConstants.MMM_DD_YYYY));
            comments.setText(journalEntryDetails.getComments());
            if ( journalEntryDetails.getAttachment() != null ) {
                attachment.setClickable(true);
                attachment.setText(Html.fromHtml("<u>"+journalEntryDetails.getAttachment()+"</u>"));
            }else{  attachment.setClickable(false); }

        }

    }

    //-------

    public void getJournalEntryDetails(String journalEntryID){
        showLoadingIndicator("Loading journal entry details, wait...");
        getBackendAPIs().getFinanceAPI().getJournalEntryDetails(journalEntryID, new FinanceAPI.JournalEntryDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, JournalEntryCompleteDetails journalEntryDetails) {
                if ( status ){
                    displayDetails(journalEntryDetails);
                }else{
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load journal entry details, try again!");
                }
            }
        });
    }

    //=================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
