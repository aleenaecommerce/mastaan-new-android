package com.aleenaecommerce.operations.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import androidx.drawerlayout.widget.DrawerLayout;

import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.adapters.JournalEntriesAdapter;
import com.aleenaecommerce.operations.backend.FinanceAPI;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.fragments.FilterItemsFragment;
import com.aleenaecommerce.operations.methods.SearchMethods;
import com.aleenaecommerce.operations.models.AccountDetails;
import com.aleenaecommerce.operations.models.JournalEntryDetails;
import com.aleenaecommerce.operations.models.ListObject;
import com.aleenaecommerce.operations.models.SearchDetails;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class AccountStatementActivity extends OperationsToolBarActivity implements View.OnClickListener{

    MenuItem sibilingAccountsMenuITem;
    MenuItem searchViewMenuITem;
    MenuItem filterMenuITem;
    SearchView searchView;

    public DrawerLayout drawerLayout;
    public FilterItemsFragment filterItemsFragment;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    AccountDetails accountDetails;
    List<AccountDetails> siblingAccounts;
    String showDate;

    TextView showing_date;
    View changeDate;
    View statementTypeView;
    RadioButton dailyStatement;
    RadioButton monthlyStatement;
    TextView current_balance;
    TextView opening_balance;

    vRecyclerView journalEntriesHolder;
    JournalEntriesAdapter journalEntriesAdapter;

    List<JournalEntryDetails> originalJournalEntriesList = new ArrayList<>();
    SearchDetails searchDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_statement);
        hasLoadingView();
        hasSwipeRefresh();

        accountDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.ACCOUNT_DETAILS), AccountDetails.class);
        try{
            siblingAccounts = new Gson().fromJson(getIntent().getStringExtra(Constants.SIBLING_ACCOUNTS), ListObject.class).getAccountsList();
        }catch (Exception e){}

        actionBar.setTitle(accountDetails.getName());

        filterItemsFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterItemsFragment).commit();
        filterItemsFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    clearSearch();
                }
                else if ( searchDetails.getAction().equalsIgnoreCase(Constants.BACKGROUND_RELOAD) ){
//                    onBackgroundReloadPressed();
                }
                else if (searchDetails.getAction().equalsIgnoreCase(Constants.ANALYSE)) {
//                    new AnalyseOrderItemsDialog(activity, groupedOrdersItemsLists).show();
                    showToastMessage("Under construction / Not yet done.");
                }
                else {
                    performSearch(searchDetails);
                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        //-----------------

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        showing_date = (TextView) findViewById(R.id.showing_date);
        changeDate = findViewById(R.id.changeDate);
        changeDate.setOnClickListener(this);

        statementTypeView = findViewById(R.id.statementTypeView);
        monthlyStatement = (RadioButton) findViewById(R.id.monthlyStatement);
        dailyStatement = (RadioButton) findViewById(R.id.dailyStatement);
        if ( getIntent().getStringExtra(Constants.STATEMENT_TYPE) != null && getIntent().getStringExtra(Constants.STATEMENT_TYPE).equalsIgnoreCase(Constants.DAILY_STATEMENT) ){
            dailyStatement.setChecked(true);
        }else {
            monthlyStatement.setChecked(true);
        }
        dailyStatement.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                changeDate.performClick();
            }
        });

        current_balance = (TextView) findViewById(R.id.current_balance);
        opening_balance = (TextView) findViewById(R.id.opening_balance);

        journalEntriesHolder = (vRecyclerView) findViewById(R.id.journalEntriesHolder);
        setupHolder();

        //------------------

        getAccountStatement(getIntent().getStringExtra("showDate"));

    }

    @Override
    public void onClick(View view) {
        if ( view == changeDate ){
            if ( dailyStatement.isChecked() ){
                showDatePickerDialog("Select date", null, DateMethods.getOnlyDate(getLocalStorageData().getServerTime()), showDate/*, true, false*/, new DatePickerCallback() {
                    @Override
                    public void onSelect(String fullDate, int day, int month, int year) {
                        getAccountStatement(fullDate);
                    }
                });
            }else{
                showDatePickerDialog("Select month", null, DateMethods.getOnlyDate(getLocalStorageData().getServerTime()), showDate, true, false, new DatePickerCallback() {
                    @Override
                    public void onSelect(String fullDate, int day, int month, int year) {
                        getAccountStatement(fullDate);
                    }
                });
            }
        }
        else if ( view == clearFilter ){
            collapseSearchView();
            clearSearch();
        }
    }

    public void setupHolder(){

        journalEntriesHolder.setupVerticalOrientation();

        journalEntriesAdapter = new JournalEntriesAdapter(context, accountDetails, new ArrayList<JournalEntryDetails>(), new JournalEntriesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                JournalEntryDetails journalEntryDetails = journalEntriesAdapter.getItem(position);

                if ( journalEntryDetails.isPending() ) {
                    Intent transactionDetailsAct = new Intent(context, TransactionDetailsActivity.class);
                    transactionDetailsAct.putExtra(Constants.ID, journalEntryDetails.getID());
                    startActivity(transactionDetailsAct);
                }else{
                    Intent journalEntryDetailsAct = new Intent(context, JournalEntryDetailsActivity.class);
                    journalEntryDetailsAct.putExtra(Constants.ID, journalEntryDetails.getID());
                    startActivity(journalEntryDetailsAct);
                }
            }
        });

        journalEntriesHolder.setAdapter(journalEntriesAdapter);
    }

    public void display(AccountDetails accountDetails){
        this.accountDetails = accountDetails;

        if ( accountDetails.getID() != null && accountDetails.getID().length() > 0 ){
            actionBar.setTitle(accountDetails.getName().toUpperCase());

            current_balance.setVisibility(View.VISIBLE);
            current_balance.setText(Html.fromHtml("<b>"+SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(accountDetails.getBalance())+"</b> balance"));
            opening_balance.setVisibility(View.VISIBLE);
            opening_balance.setText(Html.fromHtml("<b>"+SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(accountDetails.getOpeningBalance())+"</b> opening balance"));

            originalJournalEntriesList = accountDetails.getAccountEntries();

            journalEntriesAdapter.setParentAccountDetails(accountDetails);
            if ( accountDetails.getAccountEntries().size() > 0 ){
                showMenuItems();
                journalEntriesAdapter.setItems(accountDetails.getAccountEntries());
                journalEntriesHolder.scrollToPosition(0);
                switchToContentPage();
            }else{
                hideMenuItems();
                if ( siblingAccounts.size() > 0 ){  try { sibilingAccountsMenuITem.setVisible(true); } catch (Exception e) {}   }
                showNoDataIndicator("No transactions to show");
            }
        }else{
            hideMenuItems();
            if ( siblingAccounts.size() > 0 ){  try { sibilingAccountsMenuITem.setVisible(true); } catch (Exception e) {}   }
            showNoDataIndicator("No information to show)");
        }
    }

    //----

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getAccountStatement(showDate);
    }

    public void getAccountStatement(String date){
        if ( date == null || date.length() == 0 ){
            date = getLocalStorageData().getServerTime();
        }
        this.showDate = date;
        showing_date.setText(DateMethods.getDateInFormat(showDate, dailyStatement.isChecked()?DateConstants.MMM_DD_YYYY:DateConstants.MMM_YYYY));

        hideMenuItems();
        changeDate.setVisibility(View.GONE);
        statementTypeView.setVisibility(View.GONE);
        current_balance.setVisibility(View.GONE);
        opening_balance.setVisibility(View.GONE);
        showLoadingIndicator("Loading account statement, wait..");
        FinanceAPI.AccountSummaryCallback callback = new FinanceAPI.AccountSummaryCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String date, AccountDetails accountDetails) {
                changeDate.setVisibility(View.VISIBLE);
                statementTypeView.setVisibility(View.VISIBLE);
                if (status) {
                    display(accountDetails);
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load account statement, try again!");
                }
            }
        };
        if ( dailyStatement.isChecked() ){
            getBackendAPIs().getFinanceAPI().getAccountStatementForDay(accountDetails.getID(), date, callback);
        }else{
            getBackendAPIs().getFinanceAPI().getAccountStatementForMonth(accountDetails.getID(), date, callback);
        }
    }

    //---------------

    public void performSearch(final SearchDetails search_details){
        this.searchDetails = search_details;

        if ( isShowingLoadingPage() == false && originalJournalEntriesList != null && originalJournalEntriesList.size() > 0 ) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showLoadingIndicator(Html.fromHtml("Searching\n(" + searchDetails.getSearchString() + "), wait..."));
                SearchMethods.searchJournalEntries(originalJournalEntriesList, accountDetails, searchDetails, new SearchMethods.SearchJournalEntriesCallback() {
                    @Override
                    public void onComplete(boolean status, List<JournalEntryDetails> filteredList) {
                        if (status && searchDetails == search_details ) {
                            searchResultsCountIndicator.setVisibility(View.VISIBLE);
                            if (filteredList.size() > 0) {
                                if ( filteredList.size() == 1 ){
                                    results_count.setText(Html.fromHtml("<b>1</b> result found.<br>(" + searchDetails.getSearchString() + ")"));
                                }else {
                                    results_count.setText(Html.fromHtml("<b>"+filteredList.size() + "</b> results found.<br>(" + searchDetails.getSearchString() + ")"));
                                }

                                journalEntriesAdapter.setItems(filteredList);
                                journalEntriesHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else {
                                results_count.setText("No Results");
                                showNoDataIndicator(Html.fromHtml("No results found\n(" + searchDetails.getSearchString() + ")"));

                                journalEntriesAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                journalEntriesAdapter.setItems(originalJournalEntriesList);
                journalEntriesHolder.scrollToPosition(0);
            }
        }
    }

    public void clearSearch(){
        this.searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( originalJournalEntriesList.size() > 0 ) {
            journalEntriesAdapter.setItems(originalJournalEntriesList);
            journalEntriesHolder.scrollToPosition(0);
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }
        filterItemsFragment.clearSelection();
    }

    //=================

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_account_statement, menu);

        sibilingAccountsMenuITem = menu.findItem(R.id.action_sibling_accounts);
        searchViewMenuITem = menu.findItem(R.id.action_search);
        filterMenuITem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                clearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchDetails searchDetails = new SearchDetails().setConsiderAtleastOneMatch(query);
                performSearch(searchDetails);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_sibling_accounts){
            final List<AccountDetails> accountsList = new ArrayList<>();
            for (int i=0;i<siblingAccounts.size();i++){
                if ( siblingAccounts.get(i).getID().equals(accountDetails.getID()) == false ){
                    accountsList.add(siblingAccounts.get(i));
                }
            }
            List<String> accountsNames = new ArrayList<>();
            for (int i=0;i<accountsList.size();i++){
                accountsNames.add(accountsList.get(i).getName());
            }

            showListChooserDialog("Select account", accountsNames, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    accountDetails = accountsList.get(position);
                    journalEntriesAdapter.setParentAccountDetails(accountsList.get(position));
                    actionBar.setTitle(accountDetails.getName());
                    getAccountStatement(showDate);
                }
            });
        }
        else if ( item.getItemId() == R.id.action_search){

        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    //--------

    public void showMenuItems(){
        if ( siblingAccounts != null && siblingAccounts.size() > 0 ) {
            try {    sibilingAccountsMenuITem.setVisible(true); } catch (Exception e) {}
        }else{
            try {    sibilingAccountsMenuITem.setVisible(false); } catch (Exception e) {}
        }
        try{ searchViewMenuITem.setVisible(true); }catch (Exception e){}
        try{ filterMenuITem.setVisible(true); }catch (Exception e){}
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(R.id.right_drawer));
    }

    public void hideMenuItems(){
        try{ sibilingAccountsMenuITem.setVisible(false); }catch (Exception e){}
        try{ searchViewMenuITem.setVisible(false); }catch (Exception e){}
        try{ filterMenuITem.setVisible(false); }catch (Exception e){}
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
    }

    public void collapseSearchView(){
        try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
    }


}
