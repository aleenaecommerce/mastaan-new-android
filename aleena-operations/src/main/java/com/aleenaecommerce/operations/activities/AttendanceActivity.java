package com.aleenaecommerce.operations.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.WifiMethods;
import com.aleena.common.models.WifiDetails;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.backend.AttendanceAPI;
import com.aleenaecommerce.operations.backend.CommonAPI;
import com.aleenaecommerce.operations.backend.models.RequestCheckIn;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.methods.AttendanceMethods;
import com.aleenaecommerce.operations.methods.BroadcastReceiversMethods;
import com.aleenaecommerce.operations.methods.ComparisionMethods;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.BootstrapDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class AttendanceActivity extends OperationsToolBarActivity implements View.OnClickListener{

    EmployeeDetails employeeDetails;

    TextView current_status;
    Button checkIN;
    Button checkOUT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        hasLoadingView();

        getLocalStorageData().getUserDetails();

        //-----------------

        current_status = (TextView) findViewById(R.id.current_status);

        checkIN = (Button) findViewById(R.id.checkIN);
        checkIN.setOnClickListener(this);
        checkOUT = (Button) findViewById(R.id.checkOUT);
        checkOUT.setOnClickListener(this);

        //-----------

        display();

    }

    public void display(){
        if (getIntent().getBooleanExtra(Constants.AUTO_CHECK_IN, false)) {
            displayAttendanceDetails();
            checkIN.performClick();
        }
        else if (getIntent().getBooleanExtra(Constants.AUTO_CHECK_OUT, false)) {
            displayAttendanceDetails();
            checkOUT.performClick();
        } else{
            if ( getIntent().getBooleanExtra("displayDirectly", false)){
                displayAttendanceDetails();
            }else {
                getAttendanceDetails();
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        display();
    }

    public void displayAttendanceDetails(){

        employeeDetails = getLocalStorageData().getUserDetails();

        if ( employeeDetails != null ){
            BroadcastReceiversMethods.updatePinnedNotification(context);
            AttendanceDetails attendanceDetails = employeeDetails.getAttendanceDetails();
            String serverTime = getLocalStorageData().getServerTime();

            if ( DateMethods.compareDates(DateMethods.getOnlyDate(serverTime), DateMethods.getOnlyDate(attendanceDetails.getDate())) == 0
                    && attendanceDetails.isLeave() ){
                current_status.setText("Marked as "+(attendanceDetails.isPaidLeave()?"paid":"unpaid")+" leave");
                checkIN.setVisibility(View.GONE);
                checkOUT.setVisibility(View.GONE);
            }else {
                // IF WORKING DAY ONLY
                if (AttendanceMethods.isWorkingDayToday(employeeDetails, serverTime)) {
                    // IF STARTED WORKING TODAY
                    if ( AttendanceMethods.isStartedWorkingToday(attendanceDetails, serverTime)) {
                        current_status.setText(CommonMethods.getStringFromStringList(AttendanceMethods.getTodaysAttendanceDetailsList(attendanceDetails, serverTime), "\n"));
                        if (attendanceDetails.getCheckInStatus()) {
                            checkOUT.setVisibility(View.VISIBLE);
                            checkIN.setVisibility(View.GONE);
                        } else {
                            checkIN.setVisibility(View.VISIBLE);
                            checkOUT.setVisibility(View.GONE);
                        }
                    }
                    // IF NOT YET CHECKED IN TODAY
                    else {
                        String serverTimeWithCutoff15minMinus = DateMethods.addToDateInMinutes(serverTime, 15);
                        // IF SCHEDULE STARTED TODAY WITH/WITHOUT CUT OF 15min
                        if (AttendanceMethods.isScheduleStartedToday(employeeDetails, serverTimeWithCutoff15minMinus) == false ) {
                            current_status.setText("Your schedule not yet started today");
                            checkIN.setVisibility(View.GONE);
                            checkOUT.setVisibility(View.GONE);
                        } else {
                            current_status.setText("Please check in to start your day");
                            checkIN.setVisibility(View.VISIBLE);
                            checkOUT.setVisibility(View.GONE);
                        }
                    }
                }
                // IF NOT WORKING DAY
                else {
                    // IF STARTED WORKING EVEN THOUGH ITS HOLIDAY
                    if (AttendanceMethods.isStartedWorkingToday(attendanceDetails, serverTime)) {
                        current_status.setText(CommonMethods.getStringFromStringList(AttendanceMethods.getTodaysAttendanceDetailsList(attendanceDetails, serverTime), "\n"));
                        if (attendanceDetails.getCheckInStatus()) {
                            checkOUT.setVisibility(View.VISIBLE);
                            checkIN.setVisibility(View.GONE);
                        } else {
                            checkIN.setVisibility(View.VISIBLE);
                            checkOUT.setVisibility(View.GONE);
                        }
                    } else {
                        current_status.setText("No schedule for you today.");
                        checkIN.setVisibility(View.GONE);
                        checkOUT.setVisibility(View.GONE);
                    }
                }
            }
            switchToContentPage();
        }

    }

    @Override
    public void onClick(final View view) {

        showInputNumberDialog("Confirm!", "Mobile last 4 numbers", "", 4, new TextInputCallback() {
            @Override
            public void onComplete(String inputText) {
                if ( employeeDetails.getLast4NumbersOfMobileNumber().equals(inputText) ){
                    if ( view == checkIN ){
                        getCurrentLocation(new LocationCallback() {
                            @Override
                            public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, final Location location, final LatLng latLng) {
                                WifiMethods.getAvailableWifis(context, new WifiMethods.WifisCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message, List<WifiDetails> availableWifisList) {
                                        if ( status ){
                                            ComparisionMethods.isAllowedWifiNetwork(availableWifisList, getLocalStorageData().getWifiNetworks(), latLng, new StatusCallback() {
                                                @Override
                                                public void onComplete(boolean status, int statusCode, String message) {
                                                    if ( status ){
                                                        checkIN(new RequestCheckIn(location, getLocalStorageData().getUserID()));
                                                    }else{
                                                        showNotificationDialog("Alert", "You are not allowed to check in here, please try to check in at warehouse");
                                                    }
                                                }
                                            });
                                        }else{
                                            showToastMessage(message);
                                        }
                                    }
                                });
                                /*if ( status ){
                                    if (ComparisionMethods.isAllowedWifiNetwork(getConnectedWifiDetails(), getLocalStorageData().getWifiNetworks(), latLng) ){
                                        checkIN();
                                    }else{
                                        showToastMessage("You are not allowed to check in here, please try to check in at warehouse");
                                    }
                                }else{
                                    showToastMessage(message);
                                }*/
                            }
                        });
                    }
                    else if ( view == checkOUT ){
                        checkOUT();
                    }
                }
                else{
                    showToastMessage("Mobile verification failed");
                }
            }
        });


    }


    //-----

    public void getAttendanceDetails(){

        showLoadingIndicator("Loading, wait...");
        getBackendAPIs().getCommonAPI().getBootStrap(new CommonAPI.BootstrapCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, BootstrapDetails bootstrapDetails) {
                if (status) {
                    getLocalStorageData().setServerTime(bootstrapDetails.getServerTime());
                    getLocalStorageData().storeUserDetails(bootstrapDetails.getUserDetails());
                    displayAttendanceDetails();
                }else{
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load, try again!");
                }
            }
        });
    }

    public void checkIN(final RequestCheckIn requestCheckIn){

        showLoadingDialog("Checking in, wait...");
        getBackendAPIs().getAttendanceAPI().checkIN(requestCheckIn, new AttendanceAPI.AttendanceCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, AttendanceDetails attendanceDetails) {
                closeLoadingDialog();
                if ( status ){
                    showSnackbarMessage("You have checked in sucessfully.");

                    EmployeeDetails employeeDetails = getLocalStorageData().getUserDetails();
                    employeeDetails.setAttendanceDetails(attendanceDetails);
                    getLocalStorageData().storeUserDetails(employeeDetails);

                    displayAttendanceDetails();
                }else{
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to check in, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            checkIN(requestCheckIn);
                        }
                    });
                }
            }
        });
    }

    public void checkOUT(){

        showLoadingDialog("Checking out, wait...");
        getBackendAPIs().getAttendanceAPI().checkOUT(new AttendanceAPI.AttendanceCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, AttendanceDetails attendanceDetails) {
                closeLoadingDialog();
                if (status) {
                    showSnackbarMessage("You have checked out sucessfully.");

                    EmployeeDetails employeeDetails = getLocalStorageData().getUserDetails();
                    employeeDetails.setAttendanceDetails(attendanceDetails);
                    getLocalStorageData().storeUserDetails(employeeDetails);

                    displayAttendanceDetails();
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to check out, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            checkOUT();
                        }
                    });
                }
            }
        });
    }

    //=================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
