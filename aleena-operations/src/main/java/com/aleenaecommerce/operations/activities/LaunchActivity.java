package com.aleenaecommerce.operations.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.BuildConfig;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.backend.CommonAPI;
import com.aleenaecommerce.operations.models.BootstrapDetails;
import com.aleenaecommerce.operations.services.OperationsService;
import com.rey.material.widget.ProgressView;
import com.testfairy.TestFairy;

public class LaunchActivity extends OperationsToolBarActivity implements View.OnClickListener{

    TextView version_name;

    ProgressView loading_indicator;
    View reload_indicator;
    TextView loading_message;

    Intent operationsService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        TestFairy.begin(this, getString(R.string.testfailry_app_token));    // Initializing Testfairy

        operationsService = new Intent(context, OperationsService.class);
        context.stopService(operationsService);

        //--------

        version_name = (TextView) findViewById(R.id.version_name);

        loading_indicator = (ProgressView) findViewById(R.id.loading_indicator);
        loading_message = (TextView) findViewById(R.id.loading_message);
        reload_indicator = findViewById(R.id.reload_indicator);
        reload_indicator.setOnClickListener(this);

        //--------

        version_name.setText(/*"v" + BuildConfig.VERSION_NAME*/"");

        //--------

        getBootStrap(); // Get Bootstrap

    }

    @Override
    public void onClick(View view) {

        if ( view == reload_indicator ){
            getBootStrap();
        }
    }

    private void getBootStrap() {

        reload_indicator.setVisibility(View.GONE);
        loading_indicator.setVisibility(View.VISIBLE);
        loading_message.setText("");

        getBackendAPIs().getCommonAPI().getBootStrap(new CommonAPI.BootstrapCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, final BootstrapDetails bootstrapDetails) {
                if (status) {
                    getLocalStorageData().setServerTime(bootstrapDetails.getServerTime());
                    getLocalStorageData().setUpgradeURL(bootstrapDetails.getUpgradeURL());

                    // IF VALID SESSION
                    if (bootstrapDetails.getUserDetails() != null && bootstrapDetails.getUserDetails().getID() != null && bootstrapDetails.getUserDetails().getID().length() > 0) {
                        new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... params) {
                                getLocalStorageData().storeWifiNetworks(bootstrapDetails.getWifiNetworks());
                                getLocalStorageData().storeSession(getLocalStorageData().getAccessToken(), bootstrapDetails.getUserDetails());
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);

                                context.startService(operationsService);

                                Intent homeAct = new Intent(context, HomeActivity.class);
                                startActivity(homeAct);
                                finish();
                            }
                        }.execute();
                    }
                    // IF INVALID SESSION
                    else {
                        getLocalStorageData().clearSession();
                        Intent loginAct = new Intent(context, LoginActivity.class);
                        startActivity(loginAct);
                        finish();
                    }
                } else {
                    if (statusCode == 403) {
                        getLocalStorageData().clearSession();
                        Intent intent = new Intent(context, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        loading_indicator.setVisibility(View.GONE);
                        reload_indicator.setVisibility(View.VISIBLE);
                        loading_message.setText("Error in connecting to server, try again!");
                    }
                }
            }
        });
    }
}
