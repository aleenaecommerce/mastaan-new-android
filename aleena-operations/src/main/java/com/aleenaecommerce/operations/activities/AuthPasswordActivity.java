package com.aleenaecommerce.operations.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.StatusCallback;
import com.aleenaecommerce.operations.R;

public class AuthPasswordActivity extends OperationsToolBarActivity implements View.OnClickListener{

    TextView auth_password;
    View reGenerate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_password);
        hasLoadingView();

        //-----------------

        auth_password = (TextView) findViewById(R.id.auth_password);
        auth_password.setOnClickListener(this);
        reGenerate = findViewById(R.id.reGenerate);
        reGenerate.setOnClickListener(this);

        //------------------

        createAuthPassword();        //

    }

    @Override
    public void onClick(View view) {

        if ( view == auth_password ){
            copyToClipBoard(auth_password.getText().toString());
            showToastMessage("Code '"+auth_password.getText().toString()+"' is copied");
        }
        else if ( view == reGenerate ){
            createAuthPassword();
        }
    }

    //---------

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        createAuthPassword();
    }

    public void createAuthPassword(){

        showLoadingIndicator("Creating authentication code, wait..");
        getBackendAPIs().getAuthenticationAPI().createAuthPassword(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status) {
                    auth_password.setText(message);
                    copyToClipBoard(message);
                    showToastMessage("Code '" + message + "' is copied");
                    switchToContentPage();
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator("Something went wrong while creating authentication code, try again");
                }
            }
        });
    }


    //=================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
