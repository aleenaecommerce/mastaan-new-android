package com.aleenaecommerce.operations.activities;

import android.os.Bundle;
import android.os.CountDownTimer;

import android.view.MenuItem;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.adapters.TabsFragmentAdapter;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

public class EmployeeDetailsActivity extends OperationsToolBarActivity {

    EmployeeDetails employeeDetails;

    TabLayout tabLayout;
    ViewPager viewPager;
    String []tabItems = new String[]{Constants.ATTENDANCE_DETAILS_FRAGMENT, Constants.WEEK_SCHEDULE_FRAGMENT, Constants.ATTENDANCE_REPORT_FRAGMENT};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_details);
        hasLoadingView();

        employeeDetails = new Gson().fromJson(getIntent().getStringExtra("employeeDetails"), EmployeeDetails.class);

        actionBar.setTitle(employeeDetails.getFullName());

        if ( employeeDetails.isDeliveryBoy() == false ){
            tabItems = new String[]{Constants.WEEK_SCHEDULE_FRAGMENT};
        }

        //-----------------

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabTextColors(getResources().getColor(R.color.white), getResources().getColor(R.color.white));
        if ( tabItems == null || tabItems.length == 1 ){
            tabLayout.setVisibility(View.GONE);
        }

        for(int i=0;i<tabItems.length;i++) {
            TabLayout.Tab tab = tabLayout.newTab();
            tab.setText(tabItems[i]);
            tabLayout.addTab(tab);
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });

        // View Pager...

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(new TabsFragmentAdapter(context, getSupportFragmentManager(), tabItems).setEmployeeDetails(employeeDetails));

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));  // To Sync ViewPager with Tabs.

        //------

        new CountDownTimer(500, 100) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                switchToContentPage();
            }
        }.start();

    }


    //=================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
