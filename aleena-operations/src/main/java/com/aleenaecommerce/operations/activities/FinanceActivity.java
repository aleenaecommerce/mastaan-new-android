package com.aleenaecommerce.operations.activities;

import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.adapters.AccountsAdapter;
import com.aleenaecommerce.operations.backend.FinanceAPI;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.models.AccountDetails;
import com.aleenaecommerce.operations.models.ListObject;
import com.aleenaecommerce.operations.models.PrefillDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class FinanceActivity extends OperationsToolBarActivity implements View.OnClickListener{

    EmployeeDetails employeeDetails;

    PrefillDetails prefillDetails;

    vRecyclerView accountsHolder;
    AccountsAdapter accountsAdapter;

    FloatingActionMenu floatingMenu;
    View showPendingTransactions;
    View addTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finance);
        hasLoadingView();
        hasSwipeRefresh();

        employeeDetails = getLocalStorageData().getUserDetails();

        //-----------

        floatingMenu = (FloatingActionMenu) findViewById(R.id.floatingMenu);
        floatingMenu.setClosedOnTouchOutside(true);

        showPendingTransactions = findViewById(R.id.showPendingTransactions);
        showPendingTransactions.setOnClickListener(this);
        addTransaction = findViewById(R.id.addTransaction);
        addTransaction.setOnClickListener(this);

        accountsHolder = (vRecyclerView) findViewById(R.id.accountsHolder);
        setupHolder();

        //------

        if ( employeeDetails.isAccountingManager() == false ){
            floatingMenu.setVisibility(View.GONE);
        }

        getAccountsSummary();

    }

    public void setupHolder(){

        accountsHolder.setHasFixedSize(true);
        accountsHolder.setLayoutManager(new StaggeredGridLayoutManager(2, 1));
        accountsHolder.setItemAnimator(new DefaultItemAnimator());

        accountsAdapter = new AccountsAdapter(context, new ArrayList<AccountDetails>(), new AccountsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                //if ( employeeDetails.isAccountsSuperUser() || employeeDetails.isAccountsApprovalManager() || accountsAdapter.getItem(position).getEmployeeID().equals(employeeDetails.getID())){
                    if( accountsAdapter.getItem(position).isHubAccount() ){
                        getChildAccounts(accountsAdapter.getItem(position));
                    }else {
                        goToAccountSummaryPage(accountsAdapter.getItem(position), new ArrayList<AccountDetails>());
                    }
                //}
            }
        });

        accountsHolder.setAdapter(accountsAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getAccountsSummary();
    }

    @Override
    public void onClick(View view) {

        if ( view == showPendingTransactions ){
            floatingMenu.close(true);

            Intent pendingTransactionsAct = new Intent(context, PendingTransactionsActivity.class);
            startActivity(pendingTransactionsAct);
        }
        else if ( view == addTransaction ){
            floatingMenu.close(true);

            Intent addTransactionAct = new Intent(context, AddOrEditTransactionActivity.class);
            startActivity(addTransactionAct);
        }
    }

    //--------

    public void getAccountsSummary(){

        showLoadingIndicator("Loading accounts summary, wait...");
        getBackendAPIs().getFinanceAPI().getAccountsSummary(new FinanceAPI.AccountsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<AccountDetails> accountsList) {
                if (status) {
                    if (accountsList.size() > 0) {
                        accountsAdapter.setItems(accountsList);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("Nothing to show");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load accounts summary, try again!");
                }
            }
        });
        getBackendAPIs().getFinanceAPI().getFinanceDetails(new FinanceAPI.FinanceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, PrefillDetails prefill_details) {
                if (status) {
                    prefillDetails = prefill_details;
                }
            }
        });
    }

    public void getChildAccounts(final AccountDetails accountDetails){

        showLoadingDialog("Loading " + accountDetails.getName().toLowerCase() + "'s child accounts, wait...");
        getBackendAPIs().getFinanceAPI().getChildAccounts(accountDetails.getID(), new FinanceAPI.AccountsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<AccountDetails> accountsList) {
                closeLoadingDialog();
                if (status) {
                    displayChildAccounts(accountsList);
                } else {
                    checkSessionValidity(statusCode);
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong getting child accounts of " + accountDetails.getName().toLowerCase() + ".\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                getChildAccounts(accountDetails);
                            }
                        }
                    });
                }
            }
        });
    }

    public void displayChildAccounts(final List<AccountDetails> childAccounts){

        if ( childAccounts != null && childAccounts.size() > 0 ){
            // IF ONLY 1 ACCOUNT DIRECTLY GOING TO ACCOUNT SUMMARY PAGE
            if ( childAccounts.size() == 1 ){
                goToAccountSummaryPage(childAccounts.get(0), childAccounts);
            }
            // SHOWING ACCOUNTS CHOOSER
            else{
                List<String> accountsNames = new ArrayList<>();
                for (int i=0;i<childAccounts.size();i++){
                    accountsNames.add(childAccounts.get(i).getName());
                }

                showListChooserDialog("Select account", accountsNames, new ListChooserCallback() {
                    @Override
                    public void onSelect(int position) {
                        if ( position != 0 && childAccounts.get(position).isHubAccount()) {
                            getChildAccounts(childAccounts.get(position));
                        } else {
                            goToAccountSummaryPage(childAccounts.get(position), childAccounts);
                        }
                    }
                });
            }
        }else{
            showToastMessage("No accounts to display.");
        }
    }

    public void goToAccountSummaryPage(AccountDetails accountDetails, List<AccountDetails> siblingAccounts){
        Intent accountStatementAct = new Intent(context, AccountStatementActivity.class);
//        accountStatementAct.putExtra("accountID", accountDetails.getID());
//        accountStatementAct.putExtra("accountName", accountDetails.getFullName());
        accountStatementAct.putExtra(Constants.ACCOUNT_DETAILS, new Gson().toJson(accountDetails));
        accountStatementAct.putExtra(Constants.SIBLING_ACCOUNTS, new Gson().toJson(new ListObject(siblingAccounts)));
        if ( accountDetails.getName().toLowerCase().contains("cash")){
            accountStatementAct.putExtra(Constants.STATEMENT_TYPE, Constants.DAILY_STATEMENT);
        }
        startActivity(accountStatementAct);
    }

    //=========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        /*if ( isShowingContentPage() && addTransaction.getVisibility() == View.VISIBLE ){
            floatingMenu.close(true);
        }else{*/
            super.onBackPressed();
        //}
    }

}
