package com.aleenaecommerce.operations.activities;

import android.os.Bundle;
import android.view.MenuItem;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.fragments.WeekSchdeuleFragment;

public class WeekScheduleActivity extends OperationsToolBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week_schedule);

        WeekSchdeuleFragment weekSchdeuleFragment = new WeekSchdeuleFragment();
        Bundle bundle = new Bundle();
        bundle.putString("employeeDetails", getLocalStorageData().getUserDetailsString());
        weekSchdeuleFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.scheduleDetailsView, weekSchdeuleFragment).commit();

    }

    //=================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
