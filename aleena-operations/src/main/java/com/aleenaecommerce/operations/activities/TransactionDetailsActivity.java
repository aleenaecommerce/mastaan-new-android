package com.aleenaecommerce.operations.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.backend.FinanceAPI;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.dialogs.ImageDialog;
import com.aleenaecommerce.operations.models.TransactionDetails;

public class TransactionDetailsActivity extends OperationsToolBarActivity implements View.OnClickListener{

    String transactionID;

    TextView transaction_id;
    TextView transaction_name;
    TextView debit_account;
    TextView credit_account;
    TextView amount;
    TextView creator_name;
    TextView entry_date;
    TextView comments;
    TextView attachment;

    TransactionDetails transactionDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_details);
        hasLoadingView();

        transactionID = getIntent().getStringExtra(Constants.ID);

        //-----------------

        transaction_id = (TextView) findViewById(R.id.transaction_id);
        transaction_name = (TextView) findViewById(R.id.transaction_name);
        debit_account = (TextView) findViewById(R.id.debit_account);
        credit_account = (TextView) findViewById(R.id.credit_account);
        amount = (TextView) findViewById(R.id.amount);
        creator_name = (TextView) findViewById(R.id.creator_name);
        entry_date = (TextView) findViewById(R.id.entry_date);
        comments = (TextView) findViewById(R.id.comments);
        attachment = (TextView) findViewById(R.id.attachment);
        attachment.setOnClickListener(this);
        attachment.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                clipboardManager.setText(transactionDetails.getAttachment());
                Toast.makeText(context, "Attachment URL "+transactionDetails.getAttachment()+" is copied", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        //------------------

        getTransactionDetails();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getTransactionDetails();
    }

    @Override
    public void onClick(View view) {
        if ( view == attachment ){
            if ( transactionDetails.getAttachment().toLowerCase().contains(".jpg") || transactionDetails.getAttachment().toLowerCase().contains(".jpeg")
                || transactionDetails.getAttachment().toLowerCase().contains(".png") ){
                new ImageDialog(activity).show(transactionDetails.getAttachment());
            }else{
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(transactionDetails.getAttachment()));
                activity.startActivity(browserIntent);
            }
        }
    }

    public void display(TransactionDetails transactionDetails){
        this.transactionDetails = transactionDetails;

        if ( transactionDetails != null ){
            transaction_id.setText(transactionDetails.getID());
            transaction_name.setText(transactionDetails.getTransactionTypeDetails().getName());
            debit_account.setText(transactionDetails.getDebitAccountDetails().getName());
            credit_account.setText(transactionDetails.getCreditAccountDetails().getName());
            amount.setText(SpecialCharacters.RS+" "+ CommonMethods.getIndianFormatNumber(transactionDetails.getAmount()));
            creator_name.setText(CommonMethods.capitalizeFirstLetter(transactionDetails.getCreatorDetails().getFullName())+"\nOn "+DateMethods.getDateInFormat(transactionDetails.getCreatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A));
            entry_date.setText(DateMethods.getDateInFormat(transactionDetails.getEntryDate(), DateConstants.MMM_DD_YYYY));
            comments.setText(transactionDetails.getComments());
            if ( transactionDetails.getAttachment() != null ) {
                attachment.setClickable(true);
                attachment.setText(Html.fromHtml("<u>"+transactionDetails.getAttachment()+"</u>"));
            }else{  attachment.setClickable(false); }
        }else{
            showToastMessage("Something went wrong, try again");
        }

    }

    public void getTransactionDetails(){

        showLoadingIndicator("Loading transaction details, wait...");
        getBackendAPIs().getFinanceAPI().getPendingTransactionDetails(transactionID, new FinanceAPI.TransactionDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, TransactionDetails transactionDetails) {
                if ( status ){
                    switchToContentPage();
                    display(transactionDetails);
                }else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    //=================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
