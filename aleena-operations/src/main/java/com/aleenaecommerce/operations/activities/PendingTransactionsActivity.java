package com.aleenaecommerce.operations.activities;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;

import androidx.appcompat.widget.SearchView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.adapters.TransactionsAdapter;
import com.aleenaecommerce.operations.backend.FinanceAPI;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.fragments.FilterItemsFragment;
import com.aleenaecommerce.operations.methods.SearchMethods;
import com.aleenaecommerce.operations.models.SearchDetails;
import com.aleenaecommerce.operations.models.TransactionDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class PendingTransactionsActivity extends OperationsToolBarActivity implements View.OnClickListener{

    MenuItem searchViewMenuITem;
    MenuItem filterMenuITem;
    SearchView searchView;

    public DrawerLayout drawerLayout;
    public FilterItemsFragment filterItemsFragment;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    EmployeeDetails employeeDetails;

    vRecyclerView transactionsHolder;
    TransactionsAdapter transactionsAdapter;

    List<TransactionDetails> originalTransactionsList = new ArrayList<>();

    SearchDetails searchDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_transactions);
        hasLoadingView();
        hasSwipeRefresh();

        employeeDetails = getLocalStorageData().getUserDetails();

        filterItemsFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterItemsFragment).commit();
        filterItemsFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    clearSearch();
                }
                else if ( searchDetails.getAction().equalsIgnoreCase(Constants.BACKGROUND_RELOAD) ){
//                    onBackgroundReloadPressed();
                }
                else if (searchDetails.getAction().equalsIgnoreCase(Constants.ANALYSE)) {
//                    new AnalyseOrderItemsDialog(activity, groupedOrdersItemsLists).show();
                    showToastMessage("Under construction / Not yet done.");
                }
                else {
                    performSearch(searchDetails);
                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        //-----------------

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        transactionsHolder = (vRecyclerView) findViewById(R.id.transactionsHolder);
        setupHolder();

        //------------------

        getPendingTransactions();        //

    }

    @Override
    public void onClick(View view) {
        if ( view == clearFilter ){
            collapseSearchView();
            clearSearch();
        }
    }

    public void setupHolder(){

        transactionsHolder.setHasFixedSize(true);
        transactionsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        transactionsHolder.setItemAnimator(new DefaultItemAnimator());

        transactionsAdapter = new TransactionsAdapter(context, new ArrayList<TransactionDetails>(), new TransactionsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Intent transactionDetailsAct = new Intent(context, TransactionDetailsActivity.class);
                transactionDetailsAct.putExtra(Constants.ID, transactionsAdapter.getItem(position).getID());
                //transactionDetailsAct.putExtra("transaction_details_json", new Gson().toJson(transactionsAdapter.getItem(position)));
                startActivity(transactionDetailsAct);
            }

            @Override
            public void onMenuClick(final int position, View view) {
                PopupMenu popup = new PopupMenu(context, view);;//new PopupMenu(getActivity().getSupportActionBar().getThemedContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_pending_transaction, popup.getMenu());
                popup.show();

                // DISABLING APPROVAL IF USER IS NOT ACCOUNTS APPROVER
                if ( employeeDetails.isAccountsApprovalManager() == false ){
                    popup.getMenu().findItem(R.id.action_approve).setVisible(false);
                }

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        if ( menuItem.getItemId() == R.id.action_approve ) {
                            approvePendingTransaction(position);
                        }
                        else if ( menuItem.getItemId() == R.id.action_edit ) {
                            Intent addTransactionAct = new Intent(context, AddOrEditTransactionActivity.class);
                            addTransactionAct.putExtra(Constants.ACTION_TYPE, Constants.EDIT);
                            addTransactionAct.putExtra("transaction_details_json", new Gson().toJson(transactionsAdapter.getItem(position)));
                            startActivityForResult(addTransactionAct, Constants.ADD_OR_UPDATE_TRANSACTION_ACTIVITY_CODE);
                        }
                        else if ( menuItem.getItemId() == R.id.action_delete ) {
                            TransactionDetails transactionDetails = transactionsAdapter.getItem(position);

                            showChoiceSelectionDialog("Confirm!", Html.fromHtml("Are you to delete the transaction <b>"+ CommonMethods.capitalizeFirstLetter(transactionDetails.getTransactionTypeDetails().getName())+"</b> of <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(transactionDetails.getAmount())+"</b> created by <b>"+CommonMethods.capitalizeFirstLetter(transactionDetails.getCreatorDetails().getFullName())+"</b> on <b> "+ DateMethods.getDateInFormat(transactionDetails.getEntryDate(), DateConstants.MMM_DD_YYYY)+"</b>?"), "CANCEL", "PROCEED", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if ( choiceName.equalsIgnoreCase("PROCEED") ){
                                        deletePendingTransaction(position);
                                    }
                                }
                            });
                        }
                        return true;
                    }
                });
            }
        });

        transactionsHolder.setAdapter(transactionsAdapter);
    }

    //-----------

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPendingTransactions();
    }

    public void getPendingTransactions(){

        toggleMenuItemsVisibility(false);
        searchResultsCountIndicator.setVisibility(View.GONE);
        showLoadingIndicator("Loading pending transactions, wait..");
        getBackendAPIs().getFinanceAPI().getPendingTransactions(new FinanceAPI.TransactionsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<TransactionDetails> transactionsList) {
                if (status == true) {
                    if (transactionsList.size() > 0) {
                        toggleMenuItemsVisibility(true);

                        originalTransactionsList = transactionsList;
                        transactionsAdapter.setItems(transactionsList);
                        transactionsHolder.smoothScrollToPosition(0);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No pending transactions");
                    }
                } else {
                    checkSessionValidity(statusCode);
                    showReloadIndicator(statusCode, "Unable to load pending transactions, try again!");
                }
            }
        });
    }

    public void approvePendingTransaction(final int position){

        showLoadingDialog("Approving transaction, wait...");
        getBackendAPIs().getFinanceAPI().approvePendingTransaction(transactionsAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    showSnackbarMessage("Transaction approved successfully.");
                    deleteFromItemsList(position);
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Something went wrong while approving transaction, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            approvePendingTransaction(position);
                        }
                    });
                }
            }
        });
    }

    public void deletePendingTransaction(final int position){

        showLoadingDialog("Deleting transaction, wait...");
        getBackendAPIs().getFinanceAPI().deletePendingTransaction(transactionsAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    showSnackbarMessage("Transaction deleted successfully.");
                    deleteFromItemsList(position);
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Something went wrong while deleting transaction, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            deletePendingTransaction(position);
                        }
                    });
                }
            }
        });
    }

    //---------

    public void deleteFromItemsList(int position){
        String transactionID = transactionsAdapter.getItem(position).getID();

        transactionsAdapter.deleteItem(position);

        for (int i=0;i<originalTransactionsList.size();i++){
            if ( originalTransactionsList.get(i).getID().equalsIgnoreCase(transactionID)){
                originalTransactionsList.remove(i);
                break;
            }
        }
    }

    public void updateInItemsList(TransactionDetails transactionDetails){
        String transactionID = transactionDetails.getID();

        transactionsAdapter.updateItem(transactionDetails);

        for (int i=0;i<originalTransactionsList.size();i++){
            if ( originalTransactionsList.get(i).getID().equalsIgnoreCase(transactionID)){
                originalTransactionsList.set(i, transactionDetails);
                break;
            }
        }
    }

    //-----------

    public void performSearch(final SearchDetails search_details){
        this.searchDetails = search_details;

        if ( isShowingLoadingPage() == false && originalTransactionsList != null && originalTransactionsList.size() > 0 ) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showLoadingIndicator(Html.fromHtml("Searching\n(" + searchDetails.getSearchString() + "), wait..."));
                SearchMethods.searchTransactions(originalTransactionsList, searchDetails, new SearchMethods.SearchTransactionsCallback() {
                    @Override
                    public void onComplete(boolean status, List<TransactionDetails> filteredList) {
                        if (status && searchDetails == search_details ) {
                            searchResultsCountIndicator.setVisibility(View.VISIBLE);
                            if (filteredList.size() > 0) {
                                if ( filteredList.size() == 1 ){
                                    results_count.setText(Html.fromHtml("<b>1</b> result found.<br>(" + searchDetails.getSearchString() + ")"));
                                }else {
                                    results_count.setText(Html.fromHtml("<b>"+filteredList.size() + "</b> results found.<br>(" + searchDetails.getSearchString() + ")"));
                                }

                                transactionsAdapter.setItems(filteredList);
                                transactionsHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else {
                                results_count.setText("No Results");
                                showNoDataIndicator(Html.fromHtml("No results found\n(" + searchDetails.getSearchString() + ")"));

                                transactionsAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                transactionsAdapter.setItems(originalTransactionsList);
                transactionsHolder.scrollToPosition(0);
            }
        }

    }

    public void clearSearch(){
        this.searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( originalTransactionsList.size() > 0 ) {
            transactionsAdapter.setItems(originalTransactionsList);
            transactionsHolder.scrollToPosition(0);
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }
        filterItemsFragment.clearSelection();
    }

    //=================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.ADD_OR_UPDATE_TRANSACTION_ACTIVITY_CODE && resultCode == Activity.RESULT_OK ){
                TransactionDetails transactionDetails = new Gson().fromJson(data.getStringExtra("transaction_details_json"), TransactionDetails.class);
                updateInItemsList(transactionDetails);
            }
        }catch (Exception e){}
    }

    public void toggleMenuItemsVisibility(boolean visibility){
        try{
            searchViewMenuITem.setVisible(visibility);
            filterMenuITem.setVisible(visibility);
            if ( visibility ){
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(R.id.right_drawer));
            }else{
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
            }
        }catch (Exception e){}
    }

    public void collapseSearchView(){
        try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pending_transactions, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filterMenuITem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                clearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchDetails searchDetails = new SearchDetails().setConsiderAtleastOneMatch(query);
                performSearch(searchDetails);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){
        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
