package com.aleenaecommerce.operations.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.backend.AuthenticationAPI;
import com.aleenaecommerce.operations.backend.CommonAPI;
import com.aleenaecommerce.operations.models.BootstrapDetails;
import com.aleenaecommerce.operations.services.OperationsService;


public class LoginActivity extends OperationsToolBarActivity implements View.OnClickListener {

    vTextInputLayout mobile_or_email;
    Button confirmLogin;
    ProgressBar loading_indicator;

    BootstrapDetails bootstrapDetails;

    Intent operationsService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        operationsService = new Intent(context, OperationsService.class);
        context.stopService(operationsService);

        actionBar.setDisplayHomeAsUpEnabled(false);

        //----------

        mobile_or_email = findViewById(R.id.mobile_or_email);
        confirmLogin = findViewById(R.id.confirmLogin);
        confirmLogin.setOnClickListener(this);
        loading_indicator = findViewById(R.id.loading_indicator);


    }

    @Override
    public void onClick(View view) {

        if (view == confirmLogin) {
            String eMobielOrEmail = mobile_or_email.getText();
            mobile_or_email.checkError("* Enter mobile/email");

            if (eMobielOrEmail.length() > 0) {
                inputMethodManager.hideSoftInputFromWindow(mobile_or_email.getWindowToken(), 0);    // Hides Key Board After Item Select..t..
                login(eMobielOrEmail);
            }
        }
    }

    private void login(String mobileOrEmail) {
        confirmLogin.setEnabled(false);
        loading_indicator.setVisibility(View.VISIBLE);

        Log.e("getDeviceName", "" + getDeviceName());
        getBackendAPIs().getAuthenticationAPI().login(mobileOrEmail, getDeviceID(), getDeviceName(), new AuthenticationAPI.LoginCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String token) {
                if (status) {
                    getLocalStorageData().setAccessToken(token);
                    getBootStrap();
                } else {
                    confirmLogin.setEnabled(true);
                    loading_indicator.setVisibility(View.GONE);

                    if (isInternetAvailable() == false) {
                        showNotificationDialog("Faillure!", "No Internet connection.");
                    } else {
                        showNotificationDialog("Faillure!", CommonMethods.capitalizeFirstLetter(message));
                    }
                }
            }
        });
    }

    public void getBootStrap() {

        confirmLogin.setEnabled(false);
        loading_indicator.setVisibility(View.VISIBLE);

        getBackendAPIs().getCommonAPI().getBootStrap(new CommonAPI.BootstrapCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, BootstrapDetails boot_strap_details) {
                if (status) {
                    bootstrapDetails = boot_strap_details;
                    proceedToHome();
                } else {
                    checkSessionValidity(statusCode);
                    confirmLogin.setEnabled(true);
                    loading_indicator.setVisibility(View.GONE);
                    showNotificationDialog("Faillure!", "Something went wrong, try again!");
                }
            }
        });
    }

    public void proceedToHome() {
        getLocalStorageData().setServerTime(bootstrapDetails.getServerTime());
        getLocalStorageData().setUpgradeURL(bootstrapDetails.getUpgradeURL());

        // IF VALID SESSION
        if (bootstrapDetails.getUserDetails() != null && bootstrapDetails.getUserDetails().getID() != null && bootstrapDetails.getUserDetails().getID().length() > 0) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    getLocalStorageData().storeWifiNetworks(bootstrapDetails.getWifiNetworks());
                    getLocalStorageData().storeSession(getLocalStorageData().getAccessToken(), bootstrapDetails.getUserDetails());
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    context.startService(operationsService);

                    Intent homeAct = new Intent(context, HomeActivity.class);
                    startActivity(homeAct);
                    finish();
                }
            }.execute();
        }
        // IF INVALID SESSION
        else {
            showToastMessage("Invalid session, please login to continue");
            getLocalStorageData().clearSession();
            Intent loginAct = new Intent(context, LoginActivity.class);
            startActivity(loginAct);
            finish();
        }
        /*checkLocationAccess(new CheckLocatoinAccessCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status) {

                } else {
                    checkLocationAccess(this);
                }
            }
        });*/
    }
}
