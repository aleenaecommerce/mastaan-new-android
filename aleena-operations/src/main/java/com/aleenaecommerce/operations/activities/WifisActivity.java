package com.aleenaecommerce.operations.activities;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;

import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.WifiDetails;
import com.aleena.common.widgets.vRecyclerView;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.adapters.WifisAdapter;
import com.aleenaecommerce.operations.backend.CommonAPI;
import com.aleenaecommerce.operations.constants.Constants;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;


public class WifisActivity extends OperationsToolBarActivity implements View.OnClickListener{

    vRecyclerView wifisHolder;
    WifisAdapter wifisAdapter;

    Button addWifi;

    WifiDetails connectedWifiDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifis);
        hasLoadingView();
        hasSwipeRefresh();

        registerActivityReceiver(Constants.WIFI_RECEIVER);

        //----------

        addWifi = (Button) findViewById(R.id.addWifi);
        addWifi.setOnClickListener(this);

        wifisHolder = (vRecyclerView) findViewById(R.id.wifisHolder);
        setupHolder();

        //----------

        getWifis();

    }

    public void setupHolder(){

        wifisHolder.setHasFixedSize(true);
        wifisHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        wifisHolder.setItemAnimator(new DefaultItemAnimator());

        wifisAdapter = new WifisAdapter(context, new ArrayList<WifiDetails>(), new WifisAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }
            @Override
            public void onDeleteClick(final int position) {
                showChoiceSelectionDialog("Confirm!", Html.fromHtml("Do you want to delete wifi <b>" + wifisAdapter.getItem(position).getSSID().toUpperCase() + "</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES")){
                            deleteWifi(position);
                        }
                    }
                });
            }
        });

        wifisHolder.setAdapter(wifisAdapter);
    }

    public void toggleWifiButton(boolean isWifiConnected, WifiDetails wifiDetails){
        if ( isWifiConnected ){
            if ( wifisAdapter.isContains(wifiDetails.getBSSID()) == false ) {
                connectedWifiDetails = wifiDetails;
                addWifi.setVisibility(View.VISIBLE);
                addWifi.setText("ADD WIFI \n(" + wifiDetails.getSSID() + ")");
                addWifi.setEnabled(true);
            }else{
                connectedWifiDetails = null;
                addWifi.setVisibility(View.GONE);
            }
        }else{
            connectedWifiDetails = null;
            addWifi.setVisibility(View.VISIBLE);
            addWifi.setText("NO WIFI CONNECTED");
            addWifi.setEnabled(false);
        }
    }

    @Override
    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
        super.onActivityReceiverMessage(activityReceiverName, receivedData);
        try {
            boolean status = receivedData.getBooleanExtra("status", false);
            if ( status ){
                WifiDetails wifiDetails = new Gson().fromJson(receivedData.getStringExtra("wifi_details"), WifiDetails.class);
                toggleWifiButton(true, wifiDetails);
            }else{
                toggleWifiButton(false, null);
            }

        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onClick(View view) {

        if ( view == addWifi ){
            if ( connectedWifiDetails != null ){
                getCurrentLocation(new LocationCallback() {
                    @Override
                    public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                        if (status) {
                            connectedWifiDetails.setLocation(latLng);
                            addWifi(connectedWifiDetails);
                        } else {
                            showToastMessage(message);
                        }
                    }
                });
            }
        }
    }

    public void getWifis(){

        showLoadingIndicator("Loading wifis, wait...");
        new AsyncTask<Void, Void, Void>() {
            List<WifiDetails> wifisList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {
                wifisList = getLocalStorageData().getWifiNetworks();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if ( wifisList != null && wifisList.size() > 0 ){
                    wifisAdapter.setItems(wifisList);
                    switchToContentPage();
                }else{
                    showNoDataIndicator("No wifis added yet.");
                }

                //----

                WifiDetails wifiDetails = getConnectedWifiDetails();
                toggleWifiButton(wifiDetails != null ? true : false, wifiDetails);
            }
        }.execute();
    }

    public void addWifi(final WifiDetails wifiDetails){

        showLoadingDialog("Adding wifi, wait...");
        getBackendAPIs().getCommonAPI().addWifi(wifiDetails, new CommonAPI.AddWifiCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, WifiDetails addedwifiDetails) {
                closeLoadingDialog();
                if (status) {
                    showSnackbarMessage("Wifi added succesfully");
                    wifisAdapter.addItem(addedwifiDetails);
                    switchToContentPage();

                    // Updating wifis in database
                    getLocalStorageData().storeWifiNetworks(wifisAdapter.getAllItems());

                    toggleWifiButton(true, addedwifiDetails);
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to add wifi, try again", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            addWifi(wifiDetails);
                        }
                    });
                }
            }
        });
    }

    public void deleteWifi(final int position){

        showLoadingDialog("Deleting wifi, wait...");
        getBackendAPIs().getCommonAPI().deleteWifi(wifisAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    showSnackbarMessage("Wifi deleted succesfully");
                    wifisAdapter.deleteItem(position);

                    // Updating wifis in database
                    getLocalStorageData().storeWifiNetworks(wifisAdapter.getAllItems());

                    if ( wifisAdapter.getItemCount() == 0 ){
                        showNoDataIndicator("No wifis");
                    }

                    WifiDetails wifiDetails = getConnectedWifiDetails();
                    toggleWifiButton(wifiDetails != null ? true : false, wifiDetails);
                } else {
                    checkSessionValidity(statusCode);
                    showSnackbarMessage("Unable to delete wifi, try again", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            deleteWifi(position);
                        }
                    });
                }
            }
        });
    }


    //================

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
