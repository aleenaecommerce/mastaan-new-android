package com.aleenaecommerce.operations.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.telephony.TelephonyManager;
import android.widget.TextView;
import android.widget.Toast;

import com.aleena.common.activities.vToolBarActivity;
import com.aleenaecommerce.operations.backend.BackendAPIs;
import com.aleenaecommerce.operations.localdata.LocalStorageData;
import com.aleenaecommerce.operations.methods.BroadcastReceiversMethods;

import java.util.UUID;


public class OperationsToolBarActivity extends vToolBarActivity {

    OperationsToolBarActivity activity;

    TextView customActionBarTitle;

    private LocalStorageData localStorageData;

    public Typeface kaushanFont;
    public Typeface bangersFont;

    private BackendAPIs backendAPIs;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        this.activity = this;

        //----------------------------------

//        kaushanFont = Typeface.createFromAsset(getAssets(), "fonts/KaushanScript-Regular.ttf");
//        bangersFont = Typeface.createFromAsset(getAssets(), "fonts/Bangers.ttf");
//
//        try {
//            View customActionbarView = inflater.inflate(R.layout.custom_actionbar_layout, null);
//            customActionBarTitle = (TextView) customActionbarView.findViewById(R.id.toolbar_title);
//            customActionBarTitle.setText(actionBar.getTitle() + " ");
//            customActionBarTitle.setTypeface(bangersFont);
//            actionBar.setTitle("");
//            toolbar.addView(customActionbarView);
//        }catch (Exception e){}

        //-----------------------------------

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        //mastaanApplication = (MastaanApplication) getApplication();       // Accessing Application Class;
    }

    public LocalStorageData getLocalStorageData() {
        if ( localStorageData == null ){
            localStorageData = new LocalStorageData(context);
        }
        return localStorageData;
    }

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(this, getDeviceID(), getAppVersionCode());
        }
        return backendAPIs;
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        // Storing Current Activity Context in Application class
//        getMastaanApplication().setCurrentActivityContext(context);
//        // Sending current Screen Name to Google Analytics
//        //getGoogleAnalyticsMethods().sendScreenName(this.getClass().getSimpleName());
//
//        long differenceTime = DateMethods.getDifferenceToCurrentTime(new LocalStorageData(this).getHomePageLastLoadedTime());
//        if ( differenceTime > 600000 ) {     // If greater than 10 mins (600,000 millisec) InActiveTime
//            Log.d(Constants.LOG_TAG, "Reloading App as InActive time > 10min");
//
//            Intent launchActivity = new Intent(this, LaunchActivity.class);
//            ComponentName componentName = launchActivity.getComponent();
//            Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
//            startActivity(mainIntent);
//        }
//    }

//    @Override
//    protected void onPause() {
//        try {
//            localStorageData.setHomePageLastLoadedTime(DateMethods.getCurrentDateAndTime());
//        }catch (Exception e){}
//
//        super.onPause();
//    }

    public void checkSessionValidity(int statusCode){
        if ( getLocalStorageData().getSessionFlag() == true && statusCode == 403 ){
            clearUserSession("Your session has expired.");
        }
    }

    public void clearUserSession(String messageToDisplay){

        getLocalStorageData().clearSession();

        BroadcastReceiversMethods.updatePinnedNotification(context);

        if ( messageToDisplay != null && messageToDisplay.length() > 0 ) {
            Toast.makeText(context, messageToDisplay, Toast.LENGTH_LONG).show();
        }

        Intent launchAct = new Intent(context, LaunchActivity.class);
        ComponentName componentName = launchAct.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
        startActivity(mainIntent);
    }

    public String getDeviceID() {
        String deviceID = "";
        try{
            TelephonyManager telephonyManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
            String tmDevice = "" + telephonyManager.getDeviceId();
            String tmSerial = "";// + telephonyManager.getSimSerialNumber();
            String androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

            deviceID = deviceUuid.toString();
        }catch (Exception e){}
        return deviceID;
    }


    //============================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
