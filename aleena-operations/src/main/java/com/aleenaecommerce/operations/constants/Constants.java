package com.aleenaecommerce.operations.constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 8/9/15.
 */

public final class Constants {

    public static final String LOG_TAG =  "OperationsLogs";

    public static final String ID = "ID";

    public static final String WIFI_RECEIVER = "WIFI_RECEIVER";

    public static final String OPERATIONS_RECEIVER = "OPERATIONS_RECEIVER";

    public static final String AUTO_CHECK_IN = "AUTO_CHECK_IN";
    public static final String AUTO_CHECK_OUT = "AUTO_CHECK_OUT";
    public static final String PLAY_SOUND = "PLAY_SOUND";

    public static final String ACTIVITY_TITLE = "ACTIVITY_TITLE";
    public static final String SHARE_TITLE = "SHARE_TITLE";
    public static final String SHARE_MESSAGE = "SHARE_MESSAGE";

    public static final String ANALYSE = "ANALYSE";
    public static final String CLEAR = "CLEAR";
    public static final String BACKGROUND_RELOAD = "BACKGROUND_RELOAD";
    public static final String FILTER = "FILTER";
    public static final String CHECKED_IN = "CHECKED IN";
    public static final String CHECKED_OUT = "CHECKED OUT";
    public static final String CHECKED_IN_NOW = "CHECKED IN NOW";
    public static final String CHECKED_OUT_TODAY = "CHECKED OUT TODAY";
    public static final String NOTYET_CHECKEDIN_TODAY = "NOTYET CHECKED IN TODAY";
    public static final String WITH_DELAY_TODAY = "WITH DELAY TODAY";
    public static final String WITHOUT_DELAY_TODAY = "WITHOUT DELAY TODAY";
    public static final String WORKING_TODAY = "WORKING DAY";
    public static final String WEEK_OFF_TODAY = "WEEKOFF DAY";
    public static final String LEAVE_TODAY = "LEAVE";


    public static final String MESSAGE = "MESSAGE";
    public static final String ACTION_NAME = "ACTION_NAME";
    public static final String CHECK_IN = "CHECK IN";
    public static final String CHECK_OUT = "CHECK OUT";


    public static final int ADD_OR_UPDATE_TRANSACTION_ACTIVITY_CODE = 221;

    public static final String ACTION_TYPE = "ACTION_TYPE";
    public static final String ADD = "ADDD";
    public static final String EDIT = "EDIT";

    public static final String ATTENDANCE_DETAILS_FRAGMENT = "ATTENDANCE";
    public static final String WEEK_SCHEDULE_FRAGMENT = "WEEK SCHEDULE";
    public static final String ATTENDANCE_REPORT_FRAGMENT = "ATTENDANCE REPORT";

    public static final String DEBIT = "debit";
    public static final String CREDIT = "credit";
    public static final String CREDIT_AND_DEBIT = "credit and debit";


    public static final String ACCOUNT_DETAILS = "ACCOUNT_DETAILS";
    public static final String SIBLING_ACCOUNTS = "SIBLING_ACCOUNTS";

    public static final String STATEMENT_TYPE = "STATEMENT_TYPE";
    public static final String MONTHLY_STATEMENT = "MONTHLY_STATEMENT";
    public static final String DAILY_STATEMENT = "DAILY_STATEMENT";

    public static final String USER_DETAILS = "USER_DETAILS";


    public static final int ADD_OR_EDIT_EMPLOYEE_ACTIVITY_CODE = 124;


    //--------

    public static final String READ_ONLY = "read only";
    public static final String SUPER_USER = "super user";
    public static final String ADMIN = "admin";
    public static final String DELIVERY_BOY = "delivery boy";
    public static final String CHICKEN_BUTCHER = "chicken butcher";
    public static final String MUTTON_BUTCHER = "mutton butcher";
    public static final String FISH_BUTCHER = "fish butcher";
    public static final String DELIVERY_BOYS_MANAGER = "delivery boys manager";
    public static final String WAREHOUSE_MANAGER = "warehouse manager";
    public static final String STOCK_MANAGER = "stock manager";
    public static final String PROCUREMENT_MANAGER = "procurement manager";
    public static final String ORDERS_MANAGER = "orders manager";
    public static final String CUSTOMER_RELATIONSHIP_MANAGER = "customer relationship manager";
    public static final String HUMAN_RESOURCE = "human resource";
    public static final String ACCOUNTS = "accounts";
    public static final String ACCOUNTS_SUPER_USER = "accounts - super user";
    public static final String ACCOUNTS_PURCHASER = "accounts - purchaser";
    public static final String ACCOUNTS_APPROVER = "accounts - approver";
    public static final String ACCOUNTS_HR = "accounts - hr - salaries";
    public static final String ACCOUNTS_GENERAL = "accounts - general";
    public static final String ACCOUNTS_EQUITY_MANAGER = "accounts - equity manager";
    public static final String ACCOUNTS_EXPENSES_MANAGER = "accounts - expenses manager";
    public static final String ACCOUNTS_SALES_MANAGER = "accounts - sales manager";
    public static final String ACCOUNTS_BANK_TRANSACTIONS = "accounts - bank transactions";

    public static final String[] ROLES = {READ_ONLY, SUPER_USER, ADMIN, DELIVERY_BOY, CHICKEN_BUTCHER, MUTTON_BUTCHER, FISH_BUTCHER, DELIVERY_BOYS_MANAGER, WAREHOUSE_MANAGER,
            STOCK_MANAGER, PROCUREMENT_MANAGER, ORDERS_MANAGER, CUSTOMER_RELATIONSHIP_MANAGER, HUMAN_RESOURCE,
            ACCOUNTS, ACCOUNTS_SUPER_USER, ACCOUNTS_PURCHASER, ACCOUNTS_APPROVER, ACCOUNTS_HR, ACCOUNTS_GENERAL, ACCOUNTS_EQUITY_MANAGER, ACCOUNTS_EXPENSES_MANAGER, ACCOUNTS_SALES_MANAGER, ACCOUNTS_BANK_TRANSACTIONS};

    public static final List<String> getReadableRoles(List<String> roles){
        if ( roles != null && roles.size() > 0 ){
            List<String> readableRoles = new ArrayList<>(roles);
            for (int i=0;i<readableRoles.size();i++){
                if ( readableRoles.get(i).equalsIgnoreCase("r") ){  readableRoles.set(i, READ_ONLY);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("su") ){  readableRoles.set(i, SUPER_USER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ad") ){  readableRoles.set(i, ADMIN);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("db") ){  readableRoles.set(i, DELIVERY_BOY);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("cbut") ){  readableRoles.set(i, CHICKEN_BUTCHER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("mbut") ){  readableRoles.set(i, MUTTON_BUTCHER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("fbut") ){  readableRoles.set(i, FISH_BUTCHER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("dbm") ){  readableRoles.set(i, DELIVERY_BOYS_MANAGER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("wm") ){  readableRoles.set(i, WAREHOUSE_MANAGER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("sm") ){  readableRoles.set(i, STOCK_MANAGER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("om") ){  readableRoles.set(i, ORDERS_MANAGER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("pm") ){  readableRoles.set(i, PROCUREMENT_MANAGER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("crm") ){  readableRoles.set(i, CUSTOMER_RELATIONSHIP_MANAGER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("hr") ){  readableRoles.set(i, HUMAN_RESOURCE);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac") ){  readableRoles.set(i, ACCOUNTS);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac.su") ){  readableRoles.set(i, ACCOUNTS_SUPER_USER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac.pu") ){  readableRoles.set(i, ACCOUNTS_PURCHASER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac.ap") ){  readableRoles.set(i, ACCOUNTS_APPROVER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac.hr") ){  readableRoles.set(i, ACCOUNTS_HR);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac.ge") ){  readableRoles.set(i, ACCOUNTS_GENERAL);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac.ba") ){  readableRoles.set(i, ACCOUNTS_BANK_TRANSACTIONS);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac.eq") ){  readableRoles.set(i, ACCOUNTS_EQUITY_MANAGER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac.ex") ){  readableRoles.set(i, ACCOUNTS_EXPENSES_MANAGER);    }
                else if ( readableRoles.get(i).equalsIgnoreCase("ac.sa") ){  readableRoles.set(i, ACCOUNTS_SALES_MANAGER);    }
            }
            return readableRoles;
        }
        return new ArrayList<>();
    }
    public static final List<String> getFormattedRoles(List<String> roles){
        if ( roles != null && roles.size() > 0 ){
            List<String> formattedRoles = new ArrayList<>(roles);
            for (int i=0;i<formattedRoles.size();i++){
                if ( formattedRoles.get(i).equalsIgnoreCase(READ_ONLY) ){  formattedRoles.set(i, "r");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(SUPER_USER) ){  formattedRoles.set(i, "su");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ADMIN) ){  formattedRoles.set(i, "ad");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(DELIVERY_BOY) ){  formattedRoles.set(i, "db");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(CHICKEN_BUTCHER) ){  formattedRoles.set(i, "cbut");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(MUTTON_BUTCHER) ){  formattedRoles.set(i, "mbut");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(FISH_BUTCHER) ){  formattedRoles.set(i, "fbut");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(DELIVERY_BOYS_MANAGER) ){  formattedRoles.set(i, "dbm");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(WAREHOUSE_MANAGER) ){  formattedRoles.set(i, "wm");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(STOCK_MANAGER) ){  formattedRoles.set(i, "sm");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ORDERS_MANAGER) ){  formattedRoles.set(i, "om");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(PROCUREMENT_MANAGER) ){  formattedRoles.set(i, "pm");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(CUSTOMER_RELATIONSHIP_MANAGER) ){  formattedRoles.set(i, "crm");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(HUMAN_RESOURCE) ){  formattedRoles.set(i, "hr");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS) ){  formattedRoles.set(i, "ac");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS_SUPER_USER) ){  formattedRoles.set(i, "ac.su");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS_PURCHASER) ){  formattedRoles.set(i, "ac.pu");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS_APPROVER) ){  formattedRoles.set(i, "ac.ap");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS_HR) ){  formattedRoles.set(i, "ac.hr");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS_GENERAL) ){  formattedRoles.set(i, "ac.ge");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS_BANK_TRANSACTIONS) ){  formattedRoles.set(i, "ac.ba");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS_EQUITY_MANAGER) ){  formattedRoles.set(i, "ac.eq");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS_EXPENSES_MANAGER) ){  formattedRoles.set(i, "ac.ex");    }
                else if ( formattedRoles.get(i).equalsIgnoreCase(ACCOUNTS_SALES_MANAGER) ){  formattedRoles.set(i, "ac.sa");    }
            }
            return formattedRoles;
        }
        return new ArrayList<>();
    }


    public static final String SELF_OWNED = "Self Owned";
    public static final String COMPANY_OWNED = "Company Owned";

}
