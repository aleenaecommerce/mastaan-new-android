package com.aleenaecommerce.operations.methods;

import android.os.AsyncTask;

import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.models.AccountDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.models.JournalEntryDetails;
import com.aleenaecommerce.operations.models.JournalEntryTransactionDetails;
import com.aleenaecommerce.operations.models.SearchDetails;
import com.aleenaecommerce.operations.models.TransactionDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 13/7/16.
 */
public final class SearchMethods {

    public interface SearchUsersCallback{
        void onComplete(boolean status, List<EmployeeDetails> filteredList);
    }

    public interface SearchTransactionsCallback{
        void onComplete(boolean status, List<TransactionDetails> filteredList);
    }

    public interface SearchJournalEntriesCallback{
        void onComplete(boolean status, List<JournalEntryDetails> filteredList);
    }


    // SEARCH USERS

    public static final void searchTransactions(final List<EmployeeDetails> originalList, final SearchDetails searchDetails, final String todaysDateAndTime, final SearchUsersCallback callback){
        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            new AsyncTask<Void, Void, Void>() {
                List<EmployeeDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i=0;i<originalList.size();i++){
                        EmployeeDetails employeeDetails = originalList.get(i);
                        if ( employeeDetails != null ){
                            // WITH NAME
                            if ( searchDetails.getUserName() != null && searchDetails.getUserName().length() > 0 ){
                                if ( employeeDetails.getFullName().toLowerCase().contains(searchDetails.getUserName().toLowerCase()) ){
                                    filteredList.add(employeeDetails);
                                }
                            }
                            // WITH STATUS
                            else if ( searchDetails.getStatusType() != null && searchDetails.getStatusType().length() > 0 ){
                                if ( searchDetails.getStatusType().equalsIgnoreCase(Constants.CHECKED_IN_NOW) ){
                                    if ( AttendanceMethods.isWorkingDayToday(employeeDetails, todaysDateAndTime) && AttendanceMethods.isCheckedInNow(employeeDetails.getAttendanceDetails(), todaysDateAndTime)){
                                        filteredList.add(employeeDetails);
                                    }
                                }
                                else if ( searchDetails.getStatusType().equalsIgnoreCase(Constants.CHECKED_OUT_TODAY) ){
                                    if ( AttendanceMethods.isWorkingDayToday(employeeDetails, todaysDateAndTime) && AttendanceMethods.isCheckedOutNow(employeeDetails.getAttendanceDetails(), todaysDateAndTime)){
                                        filteredList.add(employeeDetails);
                                    }
                                }
                                else if ( searchDetails.getStatusType().equalsIgnoreCase(Constants.NOTYET_CHECKEDIN_TODAY) ){
                                    if ( AttendanceMethods.isWorkingDayToday(employeeDetails, todaysDateAndTime) && AttendanceMethods.isCheckedInNow(employeeDetails.getAttendanceDetails(), todaysDateAndTime) == false && AttendanceMethods.isCheckedOutNow(employeeDetails.getAttendanceDetails(), todaysDateAndTime) == false ){
                                        filteredList.add(employeeDetails);
                                    }
                                }
                            }
                            // WITH DELAY
                            else if ( searchDetails.getDelayType() != null && searchDetails.getDelayType().length() > 0 ){
                                if ( searchDetails.getDelayType().equalsIgnoreCase(Constants.WITH_DELAY_TODAY) ){
                                    if ( AttendanceMethods.isWorkingDayToday(employeeDetails, todaysDateAndTime) && AttendanceMethods.getCheckInDelayToday(employeeDetails.getAttendanceDetails(), todaysDateAndTime) > 0 ){
                                        filteredList.add(employeeDetails);
                                    }
                                }
                                else if ( searchDetails.getDelayType().equalsIgnoreCase(Constants.WITHOUT_DELAY_TODAY) ){
                                    if ( AttendanceMethods.isWorkingDayToday(employeeDetails, todaysDateAndTime) && AttendanceMethods.getCheckInDelayToday(employeeDetails.getAttendanceDetails(), todaysDateAndTime) == 0 ){
                                        filteredList.add(employeeDetails);
                                    }
                                }
                            }
                            // WITH SCHEDULE
                            else if ( searchDetails.getScheduleType() != null && searchDetails.getScheduleType().length() > 0 ){
                                if ( searchDetails.getScheduleType().equalsIgnoreCase(Constants.WORKING_TODAY) ){
                                    if ( AttendanceMethods.isWorkingDayToday(employeeDetails, todaysDateAndTime) ){
                                        filteredList.add(employeeDetails);
                                    }
                                }
                                else if ( searchDetails.getScheduleType().equalsIgnoreCase(Constants.WEEK_OFF_TODAY) ){
                                    if ( AttendanceMethods.isWorkingDayToday(employeeDetails, todaysDateAndTime) == false ){
                                        filteredList.add(employeeDetails);
                                    }
                                }
                                else if ( searchDetails.getScheduleType().equalsIgnoreCase(Constants.LEAVE_TODAY) ){
                                    if ( DateMethods.compareDates(DateMethods.getOnlyDate(todaysDateAndTime), DateMethods.getOnlyDate(employeeDetails.getAttendanceDetails().getDate())) == 0
                                            && employeeDetails.getAttendanceDetails().isLeave() ){
                                        filteredList.add(employeeDetails);
                                    }
                                }
                            }

                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, filteredList);
                    }
                }

            }.execute();
        }
    }


    // SEARCH TRANSACTIONS

    public static final void searchTransactions(final List<TransactionDetails> originalList, final SearchDetails searchDetails, final SearchTransactionsCallback callback){

        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            new AsyncTask<Void, Void, Void>() {
                List<TransactionDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i=0;i<originalList.size();i++){
                        TransactionDetails transactionDetails = originalList.get(i);

                        if ( transactionDetails != null ){

                            boolean skipSearch = false;
                            boolean isMatched = false;


                            // WITH TRANSACTION DATE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getTransactionDate() != null && searchDetails.getTransactionDate().trim().length() > 0 ){
                                String transactionDate = DateMethods.getOnlyDate(transactionDetails.getEntryDate());
                                if ( DateMethods.compareDates(transactionDate, searchDetails.getTransactionDate()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH  TRANSACTION TYPE
//                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getTransactionType() != null && searchDetails.getTransactionType().trim().length() > 0 ){
//                                if ( searchDetails.getTransactionType().equalsIgnoreCase(Constants.DEBIT) ){
//
//                                }
//                                else if ( searchDetails.getTransactionType().equalsIgnoreCase(Constants.CREDIT) ){
//
//                                }
//                            }

                            // WITH TRANSACTION NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getTransactionName() != null && searchDetails.getTransactionName().trim().length() > 0 ){
                                String transactionName = transactionDetails.getTransactionTypeDetails().getName();
                                if ( transactionName.toLowerCase().contains(searchDetails.getTransactionName().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH CREATOR NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCreatorName() != null && searchDetails.getCreatorName().trim().length() > 0 ){
                                if ( transactionDetails.getCreatorDetails().getFullName().toLowerCase().contains(searchDetails.getCreatorName().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH FROM ACCOUNT
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFromAccount() != null && searchDetails.getFromAccount().trim().length() > 0 ){
                                String fromAccountName = transactionDetails.getCreditAccountDetails().getName();
                                if ( fromAccountName.toLowerCase().contains(searchDetails.getFromAccount().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH TO ACCOUNT
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getToAccount() != null && searchDetails.getToAccount().trim().length() > 0 ){
                                String toAccountName = transactionDetails.getDebitAccountDetails().getName();
                                if ( toAccountName.toLowerCase().contains(searchDetails.getToAccount().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH MINIMUM & MAXIMUM AMOUNT
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && ( searchDetails.getMinAmount() != -1 || searchDetails.getMaxAmount() != -1 )){
                                if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() == -1 ){
                                    if ( transactionDetails.getAmount() >= searchDetails.getMinAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMaxAmount() > 0 && searchDetails.getMinAmount() == -1 ){
                                    if ( transactionDetails.getAmount() <= searchDetails.getMaxAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() > 0 ){
                                    if ( transactionDetails.getAmount() >= searchDetails.getMinAmount()
                                            && transactionDetails.getAmount() <= searchDetails.getMaxAmount()  ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH COMMENTS
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getComments() != null && searchDetails.getComments().trim().length() > 0 ){
                                if ( transactionDetails.getComments().toLowerCase().contains(searchDetails.getComments().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            //=============

                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                                filteredList.add(transactionDetails);
                            }

                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, filteredList);
                    }
                }

            }.execute();
        }
    }

    // SEARCH TRANSACTIONS

    public static final void searchJournalEntries(final List<JournalEntryDetails> originalList, final AccountDetails parentAccountDetails, final SearchDetails searchDetails, final SearchJournalEntriesCallback callback){

        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            new AsyncTask<Void, Void, Void>() {
                List<JournalEntryDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i=0;i<originalList.size();i++){
                        JournalEntryDetails journalEntryDetails = originalList.get(i);

                        if ( journalEntryDetails != null ){

                            boolean skipSearch = false;
                            boolean isMatched = false;


                            // WITH TRANSACTION DATE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getTransactionDate() != null && searchDetails.getTransactionDate().trim().length() > 0 ){
                                String transactionDate = DateMethods.getOnlyDate(journalEntryDetails.getEntryDate());
                                if ( DateMethods.compareDates(transactionDate, searchDetails.getTransactionDate()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH  TRANSACTION TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getTransactionType() != null && searchDetails.getTransactionType().trim().length() > 0 ){
                                if ( parentAccountDetails != null ){
                                    double debitAmount = 0;
                                    double creditAmount = 0;
                                    for (int a=0;a< journalEntryDetails.getTransactions().size();a++){
                                        JournalEntryTransactionDetails transactionDetails = journalEntryDetails.getTransactions().get(a);
                                        if ( ( parentAccountDetails.isHubAccount() && parentAccountDetails.getChildrenAccounts().contains(transactionDetails.getAccountID()) ) || transactionDetails.getAccountID().equals(parentAccountDetails.getID()) ){
                                            if ( transactionDetails.getAmount() > 0 ){
                                                debitAmount += transactionDetails.getAmount();
                                            }else{
                                                creditAmount += transactionDetails.getAmount();
                                            }
                                        }
                                    }

                                    if ( searchDetails.getTransactionType().equalsIgnoreCase(Constants.DEBIT) ){
                                        if ( creditAmount != 0 ){
                                            isMatched = true;
                                        }else{
                                            skipSearch = true;
                                        }
                                    }
                                    else if ( searchDetails.getTransactionType().equalsIgnoreCase(Constants.CREDIT) ){
                                        if ( debitAmount != 0 ){
                                            isMatched = true;
                                        }else{
                                            skipSearch = true;
                                        }
                                    }
                                }
                            }

                            // WITH TRANSACTION NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getTransactionName() != null && searchDetails.getTransactionName().trim().length() > 0 ){
                                String transactionName = journalEntryDetails.getDescription();
                                if ( transactionName.toLowerCase().contains(searchDetails.getTransactionName().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH MINIMUM & MAXIMUM AMOUNT
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && ( searchDetails.getMinAmount() != -1 || searchDetails.getMaxAmount() != -1 )){
                                double amount = 0;
                                for (int a=0;a< journalEntryDetails.getTransactions().size();a++){
                                    JournalEntryTransactionDetails transactionDetails = journalEntryDetails.getTransactions().get(a);
                                    if ( ( parentAccountDetails.isHubAccount() && parentAccountDetails.getChildrenAccounts().contains(transactionDetails.getAccountID()) ) || transactionDetails.getAccountID().equals(parentAccountDetails.getID()) ){
                                        amount += transactionDetails.getAmount();
                                    }
                                }

                                if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() == -1 ){
                                    if ( amount >= searchDetails.getMinAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMaxAmount() > 0 && searchDetails.getMinAmount() == -1 ){
                                    if ( amount <= searchDetails.getMaxAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() > 0 ){
                                    if ( amount >= searchDetails.getMinAmount()
                                            && amount <= searchDetails.getMaxAmount()  ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH COMMENTS
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getComments() != null && searchDetails.getComments().trim().length() > 0 ){
                                if ( journalEntryDetails.getComments().toLowerCase().contains(searchDetails.getComments().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            //=============

                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                                filteredList.add(journalEntryDetails);
                            }

                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, filteredList);
                    }
                }

            }.execute();
        }
    }

}
