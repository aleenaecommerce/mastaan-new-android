package com.aleenaecommerce.operations.methods;

import android.os.AsyncTask;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.WifiDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 15/7/16.
 */
public final class ComparisionMethods {

    public static final void isAllowedWifiNetwork(final List<WifiDetails> availableWifisList, final List<WifiDetails> wifisList, final LatLng currentLocation, final StatusCallback callback){
        new AsyncTask<Void, Void, Void>() {
            boolean isAllowedWifiNetwork;
            @Override
            protected Void doInBackground(Void... voids) {
                for (int i=0;i<availableWifisList.size();i++){
                    if ( isAllowedWifiNetwork(availableWifisList.get(i), wifisList!=null?wifisList:new ArrayList<WifiDetails>(), currentLocation) ){
                        isAllowedWifiNetwork =  true;
                        break;
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(isAllowedWifiNetwork, 200, "");
                }
            }
        }.execute();
    }

    public static final boolean isAllowedWifiNetwork(final WifiDetails connectedWifiDetails, List<WifiDetails> wifisList, final LatLng currentLocation){
        if ( wifisList == null ){   wifisList = new ArrayList<>();  }

        boolean isAllowedWifiNetwork = false;

        String wifiBSSID = "NA";
        if ( connectedWifiDetails != null && connectedWifiDetails.getBSSID() != null && connectedWifiDetails.getBSSID().length() > 0 ){
            wifiBSSID = connectedWifiDetails.getBSSID();
        }

        for (int i = 0; i < wifisList.size(); i++) {
            WifiDetails wifiDetails = wifisList.get(i);
            if ( wifiDetails != null ){
                // IF ALLOWED WIFI NETWORK
                //Log.d(Constants.LOG_TAG, "cL: "+currentLocation+", with wL: "+wifiDetails.getLocation()+", dis: "+LatLngMethods.getDistanceBetweenLatLng(currentLocation, wifiDetails.getLocation()));
                if ( (wifiDetails.getBSSID().equals(wifiBSSID))
                        || ( currentLocation != null && LatLngMethods.getDistanceBetweenLatLng(currentLocation, wifiDetails.getLocation()) <= 100 )
                        ){
                    return true;
                }

            }
        }

        return isAllowedWifiNetwork;
    }
}
