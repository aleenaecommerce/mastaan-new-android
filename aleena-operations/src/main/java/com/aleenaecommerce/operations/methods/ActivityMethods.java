package com.aleenaecommerce.operations.methods;

import android.app.Activity;

import com.aleenaecommerce.operations.activities.AccountStatementActivity;
import com.aleenaecommerce.operations.activities.PendingTransactionsActivity;

/**
 * Created by Venkatesh Uppu on 1/11/16.
 */
public class ActivityMethods {

    //-----------

    public static final boolean isPendingTransactionsActivity(Activity activity){
        try{
            PendingTransactionsActivity pendingTransactionsActivity = (PendingTransactionsActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isAccountStatementActivity(Activity activity){
        try{
            AccountStatementActivity accountStatementActivity = (AccountStatementActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

}
