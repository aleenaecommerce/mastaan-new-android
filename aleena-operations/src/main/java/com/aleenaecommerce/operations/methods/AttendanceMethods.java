package com.aleenaecommerce.operations.methods;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.models.ScheduleDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 14/7/16.
 */
public final class AttendanceMethods {

    public static final boolean isWorkingDayToday(EmployeeDetails employeeDetails, String todaysDateAndTime) {

        List<ScheduleDetails> schedulesList = employeeDetails.getSchedulesList();
        if (schedulesList != null && schedulesList.size() > 0) {
            String today = DateMethods.getDayOfWeekFromDate(todaysDateAndTime);
            for (int i = 0; i < schedulesList.size(); i++) {
                if ( schedulesList.get(i).getDate().toLowerCase().contains(today.toLowerCase()) ){
                    return !schedulesList.get(i).isLeave();
                }
            }
        }
        return false;
    }

    public static final ScheduleDetails getTodaysScheduleDetails(EmployeeDetails employeeDetails, String todaysDateAndTime){
        if ( employeeDetails != null && todaysDateAndTime != null ) {
            List<ScheduleDetails> schedulesList = employeeDetails.getSchedulesList();
            String today = DateMethods.getDayOfWeekFromDate(todaysDateAndTime);
            for (int i = 0; i < schedulesList.size(); i++) {
                if (schedulesList.get(i).getDate().toLowerCase().contains(today.toLowerCase())) {
                    return schedulesList.get(i);
                }
            }
        }
        return null;
    }

    public static final boolean isScheduleStartedToday(EmployeeDetails employeeDetails, String todaysDateAndTime) {
        if ( isWorkingDayToday(employeeDetails, todaysDateAndTime) ){
            ScheduleDetails scheduleDetails = getTodaysScheduleDetails(employeeDetails, todaysDateAndTime);
            if ( scheduleDetails != null ){
                if ( DateMethods.compareDates(DateMethods.getTimeIn24HrFormat(todaysDateAndTime), DateMethods.getTimeIn24HrFormat(scheduleDetails.getCheckIn())) >=0 ){
                    return true;
                }
            }
        }
        return false;
    }

    public static final boolean isCheckedInNow(AttendanceDetails attendanceDetails, String todaysDateAndTime) {

        if ( attendanceDetails != null ){
            if ( attendanceDetails.getCheckInStatus()
                    && DateMethods.compareDates(DateMethods.getOnlyDate(todaysDateAndTime), DateMethods.getOnlyDate(attendanceDetails.getLastCheckInTime())) == 0
                    ){
                return true;
            }
        }
        return false;
    }

    public static final boolean isCheckedOutNow(AttendanceDetails attendanceDetails, String todaysDateAndTime) {

        if ( attendanceDetails != null ){
            if ( attendanceDetails.getCheckInStatus() == false
                    && DateMethods.compareDates(DateMethods.getOnlyDate(todaysDateAndTime), DateMethods.getOnlyDate(attendanceDetails.getLastCheckOutTime())) == 0
                    ){
                return true;
            }
        }
        return false;
    }

    public static final boolean isStartedWorkingToday(AttendanceDetails attendanceDetails, String todaysDateAndTime) {

        if ( attendanceDetails != null ){
            if ( DateMethods.compareDates(DateMethods.getOnlyDate(todaysDateAndTime), DateMethods.getOnlyDate(attendanceDetails.getLastCheckInTime())) == 0
                    || DateMethods.compareDates(DateMethods.getOnlyDate(todaysDateAndTime), DateMethods.getOnlyDate(attendanceDetails.getLastCheckOutTime())) == 0
                    ){
                return true;
            }
        }
        return false;
    }

    public static final double getCheckInDelayToday(AttendanceDetails attendanceDetails, String todaysDateAndTime) {

        if (attendanceDetails != null ){
            if ( DateMethods.compareDates(DateMethods.getOnlyDate(todaysDateAndTime), DateMethods.getOnlyDate(attendanceDetails.getDate())) == 0
                    ){
                return attendanceDetails.getCheckInDelay();
            }
        }
        return -1;
    }

    //===================

    public static final ArrayList<String> getTodaysAttendanceDetailsList(AttendanceDetails attendanceDetails, String todaysDateAndTime) {
        ArrayList<String> attendanceDetailsList = new ArrayList<>();
        if (attendanceDetails != null ){
            if ( DateMethods.compareDates(DateMethods.getOnlyDate(todaysDateAndTime), DateMethods.getOnlyDate(attendanceDetails.getDate())) == 0 ){
                // IF MARKED LEAVE
                if ( attendanceDetails.isLeave() ){
                    attendanceDetailsList.add("Marked as "+(attendanceDetails.isPaidLeave()?"paid":"unpaid")+" leave");
                }
                // IF NOT LEAVE
                else{
                    attendanceDetailsList.add("First check in: " + DateMethods.getDateInFormat(attendanceDetails.getFirstCheckInTime(), DateConstants.HH_MM_A));
                    if (attendanceDetails.getCheckInDelay() > 0) {
                        attendanceDetailsList.add("First check in delay: " + DateMethods.getTimeStringFromMinutes((long) attendanceDetails.getCheckInDelay(), "Hr", "Min"));
                    }
                    if (attendanceDetails.getCheckInTimes().size() > 1) {
                        attendanceDetailsList.add("Last check in: " + DateMethods.getDateInFormat(attendanceDetails.getLastCheckInTime(), DateConstants.HH_MM_A));
                        if ( attendanceDetails.getLastCheckedInByEmployeeID() != null ){//&& attendanceDetails.getLastCheckedInByEmployeeDetails().getID().equalsIgnoreCase(employeeDetails.getID()) == false ){
                            attendanceDetailsList.add("Last check in by: "+ CommonMethods.capitalizeStringWords(attendanceDetails.getLastCheckedInByEmployeeDetails().getFullName()));
                        }
                        if ( attendanceDetails.getLastCheckInPlaceDetails() != null){
                            attendanceDetailsList.add("Last check in at: <b>"+CommonMethods.capitalizeStringWords(attendanceDetails.getLastCheckInPlaceDetails().getFullAddress()));
                        }
                    }

                    //---
                    if ( attendanceDetails.getCheckInStatus() ){
                    }else{
                        attendanceDetailsList.add("Last check out: " + DateMethods.getDateInFormat(attendanceDetails.getLastCheckOutTime(), DateConstants.HH_MM_A));
                    }

                    //----
                    long totalWorkedMinutes = attendanceDetails.getWorkedMinutes()+attendanceDetails.getOvertimeMinutes();
                    if ( attendanceDetails.getCheckInStatus() ){
                        totalWorkedMinutes += (DateMethods.getDifferenceBetweenDatesInMilliSeconds(attendanceDetails.getLastCheckInTime(), todaysDateAndTime) / (1000 * 60));
                    };

                    attendanceDetailsList.add("Worked: " + DateMethods.getTimeStringFromMinutes(totalWorkedMinutes, "Hr", "Min"));
                    if ( attendanceDetails.getTotalMinutesToWork() > totalWorkedMinutes) {
                        attendanceDetailsList.add("Remaining: " + DateMethods.getTimeStringFromMinutes((long) (attendanceDetails.getTotalMinutesToWork() - totalWorkedMinutes), "Hr", "Min"));
                    }else{
                        if ( attendanceDetails.getCheckInStatus() ){
                            attendanceDetailsList.add("Overtime: " + DateMethods.getTimeStringFromMinutes((totalWorkedMinutes-attendanceDetails.getTotalMinutesToWork()), "Hr", "Min"));
                        }else{
                            attendanceDetailsList.add("Overtime: " + DateMethods.getTimeStringFromMinutes((long) (attendanceDetails.getOvertimeMinutes()), "Hr", "Min"));
                        }
                    }
                }
            }
        }
        return attendanceDetailsList;
    }

    public static final ArrayList<String> getAttendanceDetailsList(AttendanceDetails attendanceDetails, String todaysDateAndTime) {
        ArrayList<String> attendanceDetailsList = new ArrayList<>();
        if (attendanceDetails != null ){
            // IF MARKED LEAVE
            if ( attendanceDetails.isLeave() ){
                attendanceDetailsList.add("Marked as "+(attendanceDetails.isPaidLeave()?"paid":"unpaid")+" leave");
            }
            // IF NOT LEAVE
            else {
                attendanceDetailsList.add("First check in: " + DateMethods.getDateInFormat(attendanceDetails.getFirstCheckInTime(), DateConstants.HH_MM_A));
                if (attendanceDetails.getCheckInDelay() > 0) {
                    attendanceDetailsList.add("First check in delay: " + DateMethods.getTimeStringFromMinutes((long) attendanceDetails.getCheckInDelay(), "Hr", "Min"));
                }
                if (attendanceDetails.getCheckInTimes().size() > 1) {
                    attendanceDetailsList.add("Last check in: " + DateMethods.getDateInFormat(attendanceDetails.getLastCheckInTime(), DateConstants.HH_MM_A));
                }

                //---
                if (attendanceDetails.getCheckInStatus()) {
                } else {
                    attendanceDetailsList.add("Last check out: " + DateMethods.getDateInFormat(attendanceDetails.getLastCheckOutTime(), DateConstants.HH_MM_A));
                }

                //----

                long totalWorkedMinutes = attendanceDetails.getWorkedMinutes() + attendanceDetails.getOvertimeMinutes();
                if (DateMethods.compareDates(DateMethods.getOnlyDate(todaysDateAndTime), DateMethods.getOnlyDate(attendanceDetails.getDate())) == 0) {
                    if (attendanceDetails.getCheckInStatus()) {
                        totalWorkedMinutes += (DateMethods.getDifferenceBetweenDatesInMilliSeconds(attendanceDetails.getLastCheckInTime(), todaysDateAndTime) / (1000 * 60));
                    }
                }

                attendanceDetailsList.add("Worked: " + DateMethods.getTimeStringFromMinutes(totalWorkedMinutes, "Hr", "Min"));
                if (attendanceDetails.getTotalMinutesToWork() > totalWorkedMinutes) {
                    attendanceDetailsList.add("Remaining: " + DateMethods.getTimeStringFromMinutes((long) (attendanceDetails.getTotalMinutesToWork() - totalWorkedMinutes), "Hr", "Min"));
                } else {
                    if (attendanceDetails.getCheckInStatus()) {
                        attendanceDetailsList.add("Overtime: " + DateMethods.getTimeStringFromMinutes((totalWorkedMinutes - attendanceDetails.getTotalMinutesToWork()), "Hr", "Min"));
                    } else {
                        attendanceDetailsList.add("Overtime: " + DateMethods.getTimeStringFromMinutes((long) (attendanceDetails.getOvertimeMinutes()), "Hr", "Min"));
                    }
                }
            }

        }
        return attendanceDetailsList;
    }


}
