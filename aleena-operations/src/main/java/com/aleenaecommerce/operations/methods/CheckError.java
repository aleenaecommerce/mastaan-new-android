package com.aleenaecommerce.operations.methods;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.ClipboardManager;
import android.text.Html;
import android.widget.Toast;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleenaecommerce.operations.activities.LaunchActivity;
import com.aleenaecommerce.operations.activities.LoginActivity;
import com.aleenaecommerce.operations.activities.OperationsToolBarActivity;
import com.aleenaecommerce.operations.localdata.LocalStorageData;

import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 15/05/16.
 */

public  class CheckError {

    Context context;
    OperationsToolBarActivity activity;

    boolean ignoreSessionValidity;
    boolean ignoreMaintenance;
    boolean ignoreUpgrade;

    public CheckError(Context context){
        this.context = context;
        try{    activity = (OperationsToolBarActivity) context;  }catch (Exception e){}
    }


    public CheckError ignoreMaintenance(){
        this.ignoreMaintenance = true;
        return this;
    }

    public CheckError ignoreUpgrade(){
        this.ignoreUpgrade = true;
        return this;
    }

    public CheckError ignoreSessionValidity(){
        this.ignoreSessionValidity = true;
        return this;
    }

    public boolean check(RetrofitError error){

        if ( error != null ) {
            try {
                String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                JSONObject jsonResponse = null;
                try{    jsonResponse = new JSONObject(response_json_string);    }catch (Exception e){}

                boolean maintainance = false;
                try{    maintainance = jsonResponse.getBoolean("maintenance");  }catch (Exception e){}
                boolean upgrade = false;
                try{    upgrade = jsonResponse.getBoolean("upgrade");  }catch (Exception e){}
                //final String updateURL = jsonResponse.getString("url");

                if ( !ignoreSessionValidity && error.getResponse().getStatus() == 403 ){
                    if ( activity != null ){
                        Toast.makeText(activity, "Your session has expired, please login to continue.", Toast.LENGTH_LONG).show();
                        new LocalStorageData(activity).clearSession();

                        Intent loginAct = new Intent(context, LoginActivity.class);
                        ComponentName componentName = loginAct.getComponent();
                        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                        activity.startActivity(mainIntent);
                    }
                    return true;
                }
                else if ( !ignoreMaintenance && maintainance ) {
                    if ( activity != null ) {
                        Intent launchAct = new Intent(context, LaunchActivity.class);
                        ComponentName componentName = launchAct.getComponent();
                        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                        activity.startActivity(mainIntent);
                    }
                    return true;
                }
                else if ( !ignoreUpgrade && upgrade) {
                    if ( activity != null ) {
                        final String upgradeURL = new LocalStorageData(activity).getUpgradeURL();

                        ClipboardManager clipboardManager = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboardManager.setText(upgradeURL);
                        Toast.makeText(activity, "Download URL ( " + upgradeURL + " ) is copied", Toast.LENGTH_SHORT).show();

                        activity.showChoiceSelectionDialog(false, "Update Available!", Html.fromHtml("This version is no longer compatible. Please update to new version to continue.<br><br>URL = <b>" + upgradeURL + "</b>"), "EXIT", "UPDATE", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("UPDATE")) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(upgradeURL));
                                    activity.startActivity(browserIntent);
                                    activity.finishAffinity();
                                    System.exit(0);
                                } else {
                                    activity.finishAffinity();
                                    System.exit(0);
                                }
                            }
                        });
                    }
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
