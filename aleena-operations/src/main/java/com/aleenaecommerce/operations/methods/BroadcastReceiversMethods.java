package com.aleenaecommerce.operations.methods;

import android.content.Context;
import android.content.Intent;

import com.aleenaecommerce.operations.constants.Constants;


/**
 * Created by venkatesh on 4/8/15.
 */

public final class BroadcastReceiversMethods {

    public static final void updatePinnedNotification(Context context) {
        Intent locationSelectionActivityReceiver = new Intent(Constants.OPERATIONS_RECEIVER)
                .putExtra("type", "update_pinned_notification");
        context.sendBroadcast(locationSelectionActivityReceiver);
    }
    public static final void updatePinnedNotification(Context context, boolean playSound) {
        Intent locationSelectionActivityReceiver = new Intent(Constants.OPERATIONS_RECEIVER)
                .putExtra("type", "update_pinned_notification")
                .putExtra(Constants.PLAY_SOUND, playSound);
        context.sendBroadcast(locationSelectionActivityReceiver);
    }
}
