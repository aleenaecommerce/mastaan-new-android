package com.aleenaecommerce.operations.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.adapters.CalendarAdapter;
import com.aleenaecommerce.operations.backend.AttendanceAPI;
import com.aleenaecommerce.operations.methods.AttendanceMethods;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.EmployeeDay;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class AttendanceReportFragment extends OperationsFragment implements View.OnClickListener{

    private static final int MAX_DAYS_COUNT = 42;
    // seasons' rainbow
    int[] rainbow = new int[]{
            R.color.summer,
            R.color.fall,
            R.color.winter,
            R.color.spring
    };
    // month-season association (northern hemisphere, sorry australia :)
    int[] monthSeason = new int[]{2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2};
    CalendarAdapter adapter;
    // date format
    private String dateFormat = "MMM yyyy";
    // current displayed month
    private Calendar currentDate = Calendar.getInstance();
    // internal components
    private LinearLayout header;
    private ImageView btnPrev;
    private ImageView btnNext;
    private TextView txtDate;
    private GridView grid;

    String userID;

    public AttendanceReportFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_attendance_report, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userID = getArguments().getString("userID");

        //---------

        header = (LinearLayout) view.findViewById(R.id.calendar_header);
        btnPrev = (ImageView) view.findViewById(R.id.calendar_prev_button);
        btnPrev.setOnClickListener(this);
        btnNext = (ImageView) view.findViewById(R.id.calendar_next_button);
        btnNext.setOnClickListener(this);
        txtDate = (TextView) view.findViewById(R.id.calendar_date_display);
        grid = (GridView) view.findViewById(R.id.calendar_grid);

        btnNext.setVisibility(View.INVISIBLE);

        //--------

        currentDate = DateMethods.getCalendarFromDate(getLocalStorageData().getServerTime());
        updateCalendar(currentDate);


    }

    @Override
    public void onClick(View view) {
        if ( view == btnPrev ){
            updateCalendar(DateMethods.addToCalendarInMonths(currentDate, -1));
        }
        else if ( view == btnNext ){
            updateCalendar(DateMethods.addToCalendarInMonths(currentDate, 1));
        }
    }

    public void updateCalendar(final Calendar selectedDateCalendar) {
        if ( selectedDateCalendar != null ) {
            showLoadingDialog("Loading...");
            getBackendAPIs().getAttendanceAPI().getEmployeeAttendanceReport(userID, selectedDateCalendar.get(Calendar.MONTH) + 1, selectedDateCalendar.get(Calendar.YEAR), new AttendanceAPI.AttendanceReportsListCallback() {
                @Override
                public void onComplete(boolean status, List<AttendanceDetails> attendanceReportsList, int status_code) {
                    closeLoadingDialog();
                    if (status) {
                        currentDate = selectedDateCalendar;
                        initOrUpdateTitle();
                        updateCalendar(attendanceReportsList);
                    } else {
                        checkSessionValidity(status_code);
                        showToastMessage("Something went wrong , please try again!");
                    }
                }
            });
        }
    }

    public void updateCalendar(List<AttendanceDetails> attendanceReportsList) {

        showLoadingDialog("Displaying, wait...");
        getFormattedAttendaceReportsList((Calendar) currentDate.clone(), attendanceReportsList, new FormattedReportsListCallback() {
            @Override
            public void onComplete(boolean status, List<EmployeeDay> formattedAttendanceReportsList) {
                closeLoadingDialog();

                int currentVisibleMonth = ((Calendar) currentDate.clone()).get(Calendar.MONTH);
                // update grid
                adapter = new CalendarAdapter(context, formattedAttendanceReportsList, null, currentVisibleMonth, new CalendarAdapter.Callback() {
                    @Override
                    public void onClick(final int position) {
                        AttendanceDetails attendanceDetails = adapter.getItem(position).getAttendanceDetails();
                        if ( attendanceDetails != null ){
                            if ( attendanceDetails.isLeave()
                                    && getLocalStorageData().getUserDetails().isDeliveryBoyManager() ){
                                showChoiceSelectionDialog("Confirm!", "Do you want to unmark leave?", "CANCEL", "YES", new ChoiceSelectionCallback() {
                                    @Override
                                    public void onSelect(int choiceNo, String choiceName) {
                                        if (choiceName.equalsIgnoreCase("YES")) {
                                            unmarkLeave(position);
                                        }
                                    }
                                });
                            }else {
                                String attendanceInfo = CommonMethods.getStringFromStringList(AttendanceMethods.getAttendanceDetailsList(attendanceDetails, getLocalStorageData().getServerTime()), "\n");
                                showNotificationDialog(DateMethods.getDateInFormat(attendanceDetails.getDate(), "MMM yyyy"), attendanceInfo);
                            }
                        }
                    }
                });
                grid.setAdapter(adapter);
            }
        });

    }

    private void initOrUpdateTitle() {

        Calendar serverTime = DateMethods.getCalendarFromDate(getLocalStorageData().getServerTime());
        if ( currentDate.get(Calendar.MONTH) == serverTime.get(Calendar.MONTH)
                && currentDate.get(Calendar.YEAR) == serverTime.get(Calendar.YEAR) ){
            btnNext.setVisibility(View.INVISIBLE);
        }else{
            btnNext.setVisibility(View.VISIBLE);
        }
        // update title
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        txtDate.setText(sdf.format(currentDate.getTime()));

        // set header color according to current season
        int month = currentDate.get(Calendar.MONTH);
        int season = monthSeason[month];
        int color = rainbow[season];

        header.setBackgroundColor(getResources().getColor(color));
    }



    //-----

    private interface FormattedReportsListCallback{
        void onComplete(boolean status, List<EmployeeDay> attendanceReportsList);
    }
    private void getFormattedAttendaceReportsList(final Calendar dateCalendar, final List<AttendanceDetails> attendanceReportsList, final FormattedReportsListCallback callback){

        new AsyncTask<Void, Void, Void>() {
            List<EmployeeDay> formattedAttendanceReportsList = new ArrayList<EmployeeDay>();
            @Override
            protected Void doInBackground(Void... params) {
                //Calendar calendar = (Calendar) currentDate.clone();

                int daysOfMonth = dateCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                int currentVisibleMonth = dateCalendar.get(Calendar.MONTH);
                dateCalendar.set(Calendar.DAY_OF_MONTH, 1);
                int monthBeginningCell = dateCalendar.get(Calendar.DAY_OF_WEEK) - 1;

                // move calendar backwards to the beginning of the week
                dateCalendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);
                int days_count = (daysOfMonth + monthBeginningCell) > 35 ? MAX_DAYS_COUNT : ((daysOfMonth + monthBeginningCell) > 28 ? 35 : 28);
                // fill cells
                ArrayList<Date> cells = new ArrayList<>();
                while (cells.size() < days_count) {
                    cells.add(dateCalendar.getTime());
                    dateCalendar.add(Calendar.DAY_OF_MONTH, 1);
                }
                for (Date cell : cells) {
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(cell);

                    AttendanceDetails attendanceDetails = null;
                    for (int i=0;i<attendanceReportsList.size();i++){
                        if (DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getDateFromCalendar(calendar1)), DateMethods.getOnlyDate(attendanceReportsList.get(i).getDate())) == 0 ) {
                            attendanceDetails = attendanceReportsList.get(i);
                            break;
                        }
                    }

                    EmployeeDay employeeDay = new EmployeeDay(cell, attendanceDetails);
                    formattedAttendanceReportsList.add(employeeDay);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, formattedAttendanceReportsList);
                }
            }
        }.execute();

    }

    //--------

    public void unmarkLeave(final int position){
        showLoadingDialog("Unmarking leave, wait...");
        getBackendAPIs().getAttendanceAPI().unmarkLeave(adapter.getItem(position).getAttendanceDetails().getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Successfully unmarked leave");
                    adapter.updateLeave(position, false);
                }else{
                    showSnackbarMessage("Unable to unmark leave, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            unmarkLeave(position);
                        }
                    });
                }
            }
        });
    }

}
