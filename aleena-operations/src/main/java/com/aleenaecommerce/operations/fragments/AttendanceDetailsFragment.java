package com.aleenaecommerce.operations.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.methods.AttendanceMethods;
import com.aleenaecommerce.operations.models.AttendanceDetails;
import com.aleenaecommerce.operations.models.BreakDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.models.ScheduleDetails;
import com.google.gson.Gson;

import java.util.List;


public class AttendanceDetailsFragment extends OperationsFragment {

    EmployeeDetails employeeDetails;

    TextView schedule_details;
    TextView current_status;

    public AttendanceDetailsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_attendance_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        employeeDetails = new Gson().fromJson(getArguments().getString("employeeDetails"), EmployeeDetails.class);

        //-----------------

        schedule_details = (TextView) view.findViewById(R.id.schedule_details);
        current_status = (TextView) view.findViewById(R.id.current_status);

        //-----------

        displayDetails();


    }

    public void displayDetails(){

        String serverTime = getLocalStorageData().getServerTime();
        ScheduleDetails scheduleDetails = AttendanceMethods.getTodaysScheduleDetails(employeeDetails, serverTime);
        AttendanceDetails attendanceDetails = employeeDetails.getAttendanceDetails();


        // ATTENDANCE DETAILS

        if ( attendanceDetails != null ){
            if ( DateMethods.compareDates(DateMethods.getOnlyDate(serverTime), DateMethods.getOnlyDate(attendanceDetails.getDate())) == 0
                    && attendanceDetails.isLeave() ){
                current_status.setText("Marked as " + (attendanceDetails.isPaidLeave() ? "paid" : "unpaid") + " leave");
            }else {
                // IF WORKING DAY ONLY
                if (AttendanceMethods.isWorkingDayToday(employeeDetails, serverTime)) {
                    // IF STARTED WORKING TODAY
                    if (AttendanceMethods.isStartedWorkingToday(attendanceDetails, serverTime)) {
                        current_status.setText(Html.fromHtml("<u><b>ATTENDANCE:</b></u><br><br>" + CommonMethods.getStringFromStringList(AttendanceMethods.getTodaysAttendanceDetailsList(attendanceDetails, serverTime), "<br>")));
                    }
                    // IF NOT YET CHECKED IN TODAY
                    else {
                        if ( AttendanceMethods.isScheduleStartedToday(employeeDetails, serverTime) == false ) {
                            current_status.setText(Html.fromHtml("<u><b>ATTENDANCE:</b></u><br><br>"+"Schedule not yet started today"));
                        }else {
                            current_status.setText(Html.fromHtml("<u><b>ATTENDANCE:</b></u><br><br>" + "Not yet checked in today"));
                        }
                    }
                }
                // IF NOT WORKING DAY
                else {
                    // IF STARTED WORKING EVEN THOUGH ITS HOLIDAY
                    if (AttendanceMethods.isStartedWorkingToday(attendanceDetails, serverTime)) {
                        current_status.setText(Html.fromHtml("<u><b>ATTENDANCE:</b></u><br><br>" + CommonMethods.getStringFromStringList(AttendanceMethods.getTodaysAttendanceDetailsList(attendanceDetails, serverTime), "<br>")));
                    } else {
                        current_status.setText(Html.fromHtml("<u><b>ATTENDANCE:</b></u><br><br>" + "No schedule today"));
                    }
                }
            }
        }else{
            current_status.setText(Html.fromHtml("Unable to load attendance."));
        }




        // SCHEDULE DETAILS

        if ( scheduleDetails != null ){
            if ( scheduleDetails.isLeave() == false ) {
                String detailsString = "<u><b>SCHEDULE TODAY:</b></u> (" + DateMethods.getDateInFormat(getLocalStorageData().getServerTime(), DateConstants.MMM_DD_YYYY) + ")<br><br>";
                detailsString += "Check in: " + DateMethods.getTimeIn12HrFormat(scheduleDetails.getCheckIn()) + "<br>Check out: " + DateMethods.getTimeIn12HrFormat(scheduleDetails.getCheckOut());
                if (scheduleDetails.getBreaksList() != null && scheduleDetails.getBreaksList().size() > 0) {
                    detailsString += "<br><br>Breaks:<br>";
                    List<BreakDetails> breaksList = scheduleDetails.getBreaksList();
                    for (int i = 0; i < breaksList.size(); i++) {
                        if (i != 0) {
                            detailsString += "<br>";
                        }
                        detailsString += DateMethods.getTimeIn12HrFormat(breaksList.get(i).getStartTime()) + " to " + DateMethods.getTimeIn12HrFormat(DateMethods.addToDateInMinutes(breaksList.get(i).getStartTime(), breaksList.get(i).getDurationInMinutes()));
                    }
                }
                schedule_details.setText(Html.fromHtml(detailsString));
            }else{
                schedule_details.setText(Html.fromHtml("<u><b>SCHEDULE TODAY:</b></u> ("+DateMethods.getDateInFormat(getLocalStorageData().getServerTime(), DateConstants.MMM_DD_YYYY)+")<br><br>"+"NO SCHEDULE TODAY"));
            }
        }else{
            schedule_details.setText(Html.fromHtml("Unable to load schedule."));
        }
    }

}
