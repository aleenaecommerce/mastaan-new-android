package com.aleenaecommerce.operations.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vEditText;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.methods.ActivityMethods;
import com.aleenaecommerce.operations.models.SearchDetails;


public class FilterItemsFragment extends OperationsFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{

    Callback callback;

    View layoutView;

    vEditText transaction_date;
    View clearTransactionDate;

    CheckBox creditTransactions;
    CheckBox debitTransactions;

    vEditText transaction_name;
    View clearTransactionName;
    vEditText from_account;
    View clearFromAccount;
    vEditText to_account;
    View clearToAccount;
    vEditText creator_name;
    View clearCreatorName;
    vEditText approver_name;
    View clearApproverName;
    vEditText min_amount;
    View clearMinAmount;
    vEditText max_amount;
    View clearMaxAmount;
    vEditText comments;
    View clearComments;

    View clearFilter;
//    View backgroundReload;
    View filterData;
    View analyseData;


    public interface Callback {
        void onItemSelected(int position, SearchDetails searchDetails);
    }

    public FilterItemsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_filter_items, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutView = view;

        //--------

        transaction_date = (vEditText) view.findViewById(R.id.transaction_date);
        transaction_date.setOnClickListener(this);
        clearTransactionDate = view.findViewById(R.id.clearTransactionDate);
        clearTransactionDate.setOnClickListener(this);

        creditTransactions = (CheckBox) view.findViewById(R.id.creditTransactions);
        creditTransactions.setOnCheckedChangeListener(this);
        debitTransactions = (CheckBox) view.findViewById(R.id.debitTransactions);
        debitTransactions.setOnCheckedChangeListener(this);

        transaction_name = (vEditText) view.findViewById(R.id.transaction_name);
        clearTransactionName = view.findViewById(R.id.clearTransactionName);
        clearTransactionName.setOnClickListener(this);
        from_account = (vEditText) view.findViewById(R.id.from_account);
        clearFromAccount = view.findViewById(R.id.clearFromAccount);
        clearFromAccount.setOnClickListener(this);
        to_account = (vEditText) view.findViewById(R.id.to_account);
        clearToAccount = view.findViewById(R.id.clearToAccount);
        clearToAccount.setOnClickListener(this);
        creator_name = (vEditText) view.findViewById(R.id.creator_name);
        clearCreatorName = view.findViewById(R.id.clearCreatorName);
        clearCreatorName.setOnClickListener(this);
        approver_name = (vEditText) view.findViewById(R.id.approver_name);
        clearApproverName = view.findViewById(R.id.clearApproverName);
        clearApproverName.setOnClickListener(this);
        min_amount = (vEditText) view.findViewById(R.id.min_amount);
        clearMinAmount = view.findViewById(R.id.clearMinAmount);
        clearMinAmount.setOnClickListener(this);
        max_amount = (vEditText) view.findViewById(R.id.max_amount);
        clearMaxAmount = view.findViewById(R.id.clearMaxAmount);
        clearMaxAmount.setOnClickListener(this);
        comments = (vEditText) view.findViewById(R.id.comments);
        clearComments = view.findViewById(R.id.clearComments);
        clearComments.setOnClickListener(this);

        clearFilter = view.findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);
//        backgroundReload = view.findViewById(R.id.backgroundReload);
//        backgroundReload.setOnClickListener(this);
        filterData = view.findViewById(R.id.filterData);
        filterData.setOnClickListener(this);
        analyseData = view.findViewById(R.id.analyseData);
        analyseData.setOnClickListener(this);


        //----------

        if (ActivityMethods.isPendingTransactionsActivity(getActivity()) ){
            toggleVisibility(new View[]{view.findViewById(R.id.transactionTypeView), view.findViewById(R.id.approverView)}, false);
        }
        else if (ActivityMethods.isAccountStatementActivity(getActivity()) ){
            toggleVisibility(new View[]{view.findViewById(R.id.fromAccountView), view.findViewById(R.id.toAccountView),
                    view.findViewById(R.id.approverView), view.findViewById(R.id.creatorNameView)}, false);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton view, boolean isChecked) {

        if ( isChecked ){

            if ( view == creditTransactions ){
                debitTransactions.setChecked(false);
            }else if ( view == debitTransactions ){
                creditTransactions.setChecked(false);
            }
        }

    }


    @Override
    public void onClick(View view) {

        if ( view == clearTransactionDate ){
            transaction_date.setText("");
        }
        else if ( view == clearTransactionName ){
            transaction_name.setText("");
        }
        else if ( view == clearFromAccount ){
            from_account.setText("");
        }
        else if ( view == clearToAccount ){
            to_account.setText("");
        }
        else if ( view == clearCreatorName ){
            creator_name.setText("");
        }
        else if ( view == clearApproverName ){
            approver_name.setText("");
        }
        else if ( view == clearMinAmount ){
            min_amount.setText("");
        }
        else if ( view == clearMaxAmount ){
            max_amount.setText("");
        }
        else if ( view == clearComments ){
            comments.setText("");
        }

        else if ( view == transaction_date ){
            String initialDate = transaction_date.getText().toString();
            if ( initialDate == null || initialDate.length() == 0 ) {
                initialDate = getLocalStorageData().getServerTime();
            }
            showDatePickerDialog("Transaction date!", null, DateMethods.getCurrentDate(), initialDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    transaction_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }

        else if ( view == clearFilter ){
            clearFilter.setVisibility(View.GONE);
            clearSelection();
            if (callback != null) {
                callback.onItemSelected(-1, new SearchDetails().setAction(Constants.CLEAR));
            }
        }
        else if ( view == analyseData){
            if ( callback != null ){
                callback.onItemSelected(-1, new SearchDetails().setAction(Constants.ANALYSE));
            }
        }
//        else if ( view == backgroundReload ){
//            if (callback != null) {
//                callback.onItemSelected(-1, new SearchDetails().setAction(Constants.BACKGROUND_RELOAD));
//            }
//        }

        else if ( view == filterData){
            inputMethodManager.hideSoftInputFromWindow(transaction_name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(from_account.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(to_account.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(creator_name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(approver_name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(min_amount.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(max_amount.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(comments.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..

            if ( callback != null ){
                SearchDetails searchDetails = new SearchDetails();
                searchDetails.setAction(Constants.FILTER);

                if ( transaction_date.getText() != null && transaction_date.getText().toString().trim().length() > 0 ){
                    searchDetails.setTransactionDate(transaction_date.getText().toString());
                }

                if ( creditTransactions.isChecked() ){
                    searchDetails.setTransactionType(Constants.CREDIT);
                }else if ( debitTransactions.isChecked() ){
                    searchDetails.setTransactionType(Constants.DEBIT);
                }

                if ( transaction_name.getText() != null && transaction_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setTransactionName(transaction_name.getText().toString());
                }
                if ( from_account.getText() != null && from_account.getText().toString().trim().length() > 0 ){
                    searchDetails.setFromAccount(from_account.getText().toString());
                }
                if ( to_account.getText() != null && to_account.getText().toString().trim().length() > 0 ){
                    searchDetails.setToAccount(to_account.getText().toString());
                }
                if ( creator_name.getText() != null && creator_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setCreatorName(creator_name.getText().toString());
                }
                if ( approver_name.getText() != null && approver_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setApproverName(approver_name.getText().toString());
                }
                if ( min_amount.getText() != null && min_amount.getText().toString().trim().length() > 0 ){
                    searchDetails.setMinAmount(Double.parseDouble(min_amount.getText().toString()));
                }
                if ( max_amount.getText() != null && max_amount.getText().toString().trim().length() > 0 ){
                    searchDetails.setMaxAmount(Double.parseDouble(max_amount.getText().toString()));
                }
                if ( comments.getText() != null && comments.getText().toString().trim().length() > 0 ){
                    searchDetails.setComments(comments.getText().toString());
                }

                if ( searchDetails.isSearchable() ) {
                    clearFilter.setVisibility(View.VISIBLE);
                    callback.onItemSelected(-1, searchDetails);
                }else{
                    clearFilter.setVisibility(View.GONE);
                    showToastMessage("Please select your choices.");
                }
            }
        }
    }

    public void setItemSelectionListener(Callback mCallback) {
        this.callback = mCallback;
    }


    public void clearSelection(){

        try {
            clearFilter.setVisibility(View.GONE);

            transaction_date.setText("");

            creditTransactions.setChecked(false);
            debitTransactions.setChecked(false);

            transaction_name.setText("");
            from_account.setText("");
            to_account.setText("");
            creator_name.setText("");
            approver_name.setText("");
            min_amount.setText("");
            max_amount.setText("");
            comments.setText("");
        }catch (Exception e){}
    }

    //----------

    private void toggleVisibility(View []viewsList, boolean visibility){
        if ( viewsList != null && viewsList.length > 0 ){
            for (int i=0;i<viewsList.length;i++){
                if ( visibility ){
                    viewsList[i].setVisibility(visibility?View.VISIBLE:View.VISIBLE);
                }else {
                    viewsList[i].setVisibility(visibility ? View.VISIBLE : View.GONE);
                }
            }
        }
    }


}
