package com.aleenaecommerce.operations.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.constants.Constants;
import com.aleenaecommerce.operations.models.SearchDetails;


public class FilterEmployeesFragment extends OperationsFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{

    Callback callback;

    RadioGroup statusFilterGroup;
    RadioButton checkedInNow;
    RadioButton checkedOutToday;
    RadioButton absentToday;

    RadioGroup delayFilterGroup;
    RadioButton withDelay;
    RadioButton withoutDelay;

    RadioGroup scheduleFilterGroup;
    RadioButton withSchedule;
    RadioButton withoutSchedule;
    RadioButton leaveToday;

    View clearFilter;
    View analyseData;


    public interface Callback {
        void onItemSelected(int position, SearchDetails searchDetails);
    }

    public FilterEmployeesFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_filter_employees, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //--------

        statusFilterGroup = (RadioGroup) view.findViewById(R.id.statusFilterGroup);
        checkedInNow = (RadioButton) view.findViewById(R.id.checkedInNow);
        checkedInNow.setOnCheckedChangeListener(this);
        checkedOutToday = (RadioButton) view.findViewById(R.id.checkedOutToday);
        checkedOutToday.setOnCheckedChangeListener(this);
        absentToday = (RadioButton) view.findViewById(R.id.absentToday);
        absentToday.setOnCheckedChangeListener(this);

        delayFilterGroup = (RadioGroup) view.findViewById(R.id.delayFilterGroup);
        withDelay = (RadioButton) view.findViewById(R.id.withDelayToday);
        withDelay.setOnCheckedChangeListener(this);
        withoutDelay = (RadioButton) view.findViewById(R.id.withoutDelayToday);
        withoutDelay.setOnCheckedChangeListener(this);

        scheduleFilterGroup = (RadioGroup) view.findViewById(R.id.scheduleFilterGroup);
        withSchedule = (RadioButton) view.findViewById(R.id.workingToday);
        withSchedule.setOnCheckedChangeListener(this);
        withoutSchedule = (RadioButton) view.findViewById(R.id.weekoffToday);
        withoutSchedule.setOnCheckedChangeListener(this);
        leaveToday = (RadioButton) view.findViewById(R.id.leaveToday);
        leaveToday.setOnCheckedChangeListener(this);

        clearFilter = view.findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);
        analyseData = view.findViewById(R.id.analyseData);
        analyseData.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if ( view == clearFilter ){
            clearFilter.setVisibility(View.GONE);
            statusFilterGroup.clearCheck();
            delayFilterGroup.clearCheck();
            scheduleFilterGroup.clearCheck();
            if (callback != null) {
                callback.onItemSelected(-1, new SearchDetails().setAction(Constants.CLEAR));
            }
        }

        else if ( view == analyseData){
            if ( callback != null ){
                callback.onItemSelected(-1, new SearchDetails().setAction(Constants.ANALYSE));
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton view, boolean isChecked) {
        if ( view == checkedInNow){
            if (isChecked) {
                clearFilter.setVisibility(View.VISIBLE);
                delayFilterGroup.clearCheck();
                scheduleFilterGroup.clearCheck();
                if (callback != null) {
                    callback.onItemSelected(-1, new SearchDetails().setStatusType(Constants.CHECKED_IN_NOW));
                }
            }
        }
        else if ( view == checkedOutToday){
            if (isChecked) {
                clearFilter.setVisibility(View.VISIBLE);
                delayFilterGroup.clearCheck();
                scheduleFilterGroup.clearCheck();
                if (callback != null) {
                    callback.onItemSelected(-1, new SearchDetails().setStatusType(Constants.CHECKED_OUT_TODAY));
                }
            }
        }
        else if ( view == absentToday){
            if (isChecked) {
                clearFilter.setVisibility(View.VISIBLE);
                delayFilterGroup.clearCheck();
                scheduleFilterGroup.clearCheck();
                if (callback != null) {
                    callback.onItemSelected(-1, new SearchDetails().setStatusType(Constants.NOTYET_CHECKEDIN_TODAY));
                }
            }
        }
        else if ( view == withDelay){
            if (isChecked) {
                clearFilter.setVisibility(View.VISIBLE);
                statusFilterGroup.clearCheck();
                scheduleFilterGroup.clearCheck();
                if (callback != null) {
                    callback.onItemSelected(-1, new SearchDetails().setDelayType(Constants.WITH_DELAY_TODAY));
                }
            }
        }
        else if ( view == withoutDelay){
            if (isChecked) {
                clearFilter.setVisibility(View.VISIBLE);
                statusFilterGroup.clearCheck();
                scheduleFilterGroup.clearCheck();
                if (callback != null) {
                    callback.onItemSelected(-1, new SearchDetails().setDelayType(Constants.WITHOUT_DELAY_TODAY));
                }
            }
        }
        else if ( view == withSchedule){
            if (isChecked) {
                clearFilter.setVisibility(View.VISIBLE);
                statusFilterGroup.clearCheck();
                delayFilterGroup.clearCheck();
                if (callback != null) {
                    callback.onItemSelected(-1, new SearchDetails().setScheduleType(Constants.WORKING_TODAY));
                }
            }
        }
        else if ( view == withoutSchedule){
            if (isChecked) {
                clearFilter.setVisibility(View.VISIBLE);
                statusFilterGroup.clearCheck();
                delayFilterGroup.clearCheck();
                if (callback != null) {
                    callback.onItemSelected(-1, new SearchDetails().setScheduleType(Constants.WEEK_OFF_TODAY));
                }
            }
        }
        else if ( view == leaveToday){
            if (isChecked) {
                clearFilter.setVisibility(View.VISIBLE);
                statusFilterGroup.clearCheck();
                delayFilterGroup.clearCheck();
                if (callback != null) {
                    callback.onItemSelected(-1, new SearchDetails().setScheduleType(Constants.LEAVE_TODAY));
                }
            }
        }
    }

    public void setItemSelectionListener(Callback mCallback) {
        this.callback = mCallback;
    }

    public void clearSelection(){
        clearFilter.setVisibility(View.GONE);
        statusFilterGroup.clearCheck();
        scheduleFilterGroup.clearCheck();
        delayFilterGroup.clearCheck();
    }


}
