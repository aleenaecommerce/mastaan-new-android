package com.aleenaecommerce.operations.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aleena.common.fragments.vFragment;
import com.aleenaecommerce.operations.activities.LaunchActivity;
import com.aleenaecommerce.operations.backend.BackendAPIs;
import com.aleenaecommerce.operations.localdata.LocalStorageData;
import com.aleenaecommerce.operations.methods.BroadcastReceiversMethods;

/**
 * Created by Venkatesh Uppu on 08-01-2016.
 */
public class OperationsFragment extends vFragment {
    Context context;
    private LocalStorageData localStorageData;

    private BackendAPIs backendAPIs;


    public OperationsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
    }

    public LocalStorageData getLocalStorageData() {
        if ( localStorageData == null ){
            localStorageData = new LocalStorageData(context);
        }
        return localStorageData;
    }

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(getActivity(), getDeviceID(), getAppVersionCode());
        }
        return backendAPIs;
    }

    protected void callToPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void checkSessionValidity(int statusCode){
        if ( getLocalStorageData().getSessionFlag() == true && statusCode == 403 ){
            clearUserSession("Your session has expired.");
        }
    }

    public void clearUserSession(String messageToDisplay){

        getLocalStorageData().clearSession();

        BroadcastReceiversMethods.updatePinnedNotification(context);

        if ( messageToDisplay != null && messageToDisplay.length() > 0 ) {
            Toast.makeText(context, messageToDisplay, Toast.LENGTH_LONG).show();
        }

        Intent launchAct = new Intent(context, LaunchActivity.class);
        ComponentName componentName = launchAct.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
        startActivity(mainIntent);
    }


}
