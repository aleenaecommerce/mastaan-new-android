package com.aleenaecommerce.operations.fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.interfaces.StatusCallback;
import com.aleenaecommerce.operations.BuildConfig;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.activities.AppsActivity;
import com.aleenaecommerce.operations.activities.AttendanceActivity;
import com.aleenaecommerce.operations.activities.AttendanceReportActivity;
import com.aleenaecommerce.operations.activities.AuthPasswordActivity;
import com.aleenaecommerce.operations.activities.EmployeesActivity;
import com.aleenaecommerce.operations.activities.FinanceActivity;
import com.aleenaecommerce.operations.activities.WeekOffScheduleActivity;
import com.aleenaecommerce.operations.activities.WeekScheduleActivity;
import com.aleenaecommerce.operations.activities.WifisActivity;
import com.aleenaecommerce.operations.models.SidebarItemDetails;
import com.aleenaecommerce.operations.models.EmployeeDetails;

import java.util.ArrayList;
import java.util.List;


public class SideBarFragment extends OperationsFragment {

    LinearLayout itemsHolder;
    TextView version_name;
    Callback mCallback;

    List<SidebarItemDetails> sidebarItems = new ArrayList<>();

    EmployeeDetails employeeDetails;

    public interface Callback {
        void onItemSelected(int position, String item_name);
    }

    public SideBarFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sidebar, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        employeeDetails = getLocalStorageData().getUserDetails();

        //--------

        itemsHolder = (LinearLayout) view.findViewById(R.id.itemsHolder);
        version_name = (TextView) view.findViewById(R.id.version_name);

        //--------

        version_name.setText("v" + BuildConfig.VERSION_NAME);

        displaySideBarItems();

    }

    public void displaySideBarItems() {

        sidebarItems = new ArrayList<>();
        if ( employeeDetails.isWarehouseManager() ){
            sidebarItems.add(new SidebarItemDetails("Wifi manager", R.drawable.ic_wifi_grey));
        }
        sidebarItems.add(new SidebarItemDetails("Finance", R.drawable.ic_profile));
        //sidebarItems.add(new SidebarItemDetails("Profile", R.drawable.ic_profile));
        if ( employeeDetails.isDeliveryBoy()) {
            sidebarItems.add(new SidebarItemDetails("Attendance", R.drawable.ic_profile));
            sidebarItems.add(new SidebarItemDetails("Attendance report", R.drawable.ic_profile));
        }
        if ( employeeDetails.isFullTimeEmployee() ) {
            sidebarItems.add(new SidebarItemDetails("Week schedule", R.drawable.ic_profile));
        }
        sidebarItems.add(new SidebarItemDetails("Week off schedule", R.drawable.ic_profile));
        sidebarItems.add(new SidebarItemDetails("Employees", R.drawable.ic_people_grey));
        sidebarItems.add(new SidebarItemDetails("Apps", R.drawable.ic_profile));
        sidebarItems.add(new SidebarItemDetails("Auth", R.drawable.ic_profile));
        //sidebarItems.add(new SidebarItemDetails(Html.fromHtml("Logout (<b>" + getLocalStorageData().getUserName() + "</b>)"), R.drawable.ic_logout));

        for (int i = 0; i < sidebarItems.size(); i++) {
            final int position = i;
            View itemView = inflater.inflate(R.layout.view_sidebar_item, null, false);
            itemsHolder.addView(itemView);

            ImageView image = (ImageView) itemView.findViewById(R.id.image);
            image.setBackgroundResource((int) sidebarItems.get(position).getIcon());
            TextView title = (TextView) itemView.findViewById(R.id.title);
            title.setText(sidebarItems.get(position).getName());

            itemView.findViewById(R.id.itemSelector).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSideBarItemSelected(position);
                }
            });
        }
    }

    public void onSideBarItemSelected(int position) {

        String selectedItem = sidebarItems.get(position).getName().toString();
        if (mCallback != null){
            mCallback.onItemSelected(position, selectedItem);
        }

        if (selectedItem.equalsIgnoreCase("Wifi manager")) {
            Intent wifisAct = new Intent(context, WifisActivity.class);
            startActivity(wifisAct);
        }
        else if (selectedItem.equalsIgnoreCase("Finance")) {
            Intent finacneAct = new Intent(context, FinanceActivity.class);
            startActivity(finacneAct);
        }
        else if (selectedItem.equalsIgnoreCase("Attendance")) {
            Intent attendanceAct = new Intent(context, AttendanceActivity.class);
            startActivity(attendanceAct);
        }
        else if (selectedItem.equalsIgnoreCase("Attendance report")) {
            Intent attendanceReportAct = new Intent(context, AttendanceReportActivity.class);
            startActivity(attendanceReportAct);
        }
        else if ( selectedItem.equalsIgnoreCase("Employees") ){
            Intent deliveryBoysAct = new Intent(context, EmployeesActivity.class);
            startActivity(deliveryBoysAct);
        }
        else if ( selectedItem.equalsIgnoreCase("Apps") ){
            Intent appsAct = new Intent(context, AppsActivity.class);
            startActivity(appsAct);
        }
        else if ( selectedItem.equalsIgnoreCase("Week schedule") ){
            Intent weekScheduleAct = new Intent(context, WeekScheduleActivity.class);
            startActivity(weekScheduleAct);
        }
        else if ( selectedItem.equalsIgnoreCase("Week off schedule")){
            Intent weekOffScheduleAct = new Intent(context, WeekOffScheduleActivity.class);
            startActivity(weekOffScheduleAct);
        }
        else if (selectedItem.contains("Auth")) {
            Intent authPasswordAct = new Intent(context, AuthPasswordActivity.class);
            startActivity(authPasswordAct);
        }
        else if (selectedItem.contains("Logout")) {
            logout();
        }

    }

    public void logout(){

        showLoadingDialog("Logging out, wait...");
        getBackendAPIs().getAuthenticationAPI().logout(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if ( status ){
                    showToastMessage("Logged out successfully.");
                    clearUserSession("Logged out successfully");
                }else{
                    clearUserSession("");
                }
            }
        });
    }

    public void setSidebarItemSelectedListener(Callback mCallback) {
        this.mCallback = mCallback;
    }

}
