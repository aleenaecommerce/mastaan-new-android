package com.aleenaecommerce.operations.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aleena.common.widgets.vRecyclerView;
import com.aleenaecommerce.operations.R;
import com.aleenaecommerce.operations.adapters.SchedulesAdapter;
import com.aleenaecommerce.operations.models.EmployeeDetails;
import com.aleenaecommerce.operations.models.ScheduleDetails;
import com.google.gson.Gson;

import java.util.ArrayList;


public class WeekSchdeuleFragment extends OperationsFragment {

    EmployeeDetails employeeDetails;

    vRecyclerView schedulesHolder;
    SchedulesAdapter schedulesAdapter;

    public WeekSchdeuleFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_week_schedule, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        employeeDetails = new Gson().fromJson(getArguments().getString("employeeDetails"), EmployeeDetails.class);

        schedulesHolder = (vRecyclerView) view.findViewById(R.id.schedulesHolder);
        setupHolder();

        //------------------

        schedulesAdapter.setItems(employeeDetails.getSchedulesList());
        switchToContentPage();

    }

    public void setupHolder(){

        schedulesHolder.setHasFixedSize(true);
        schedulesHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        schedulesHolder.setItemAnimator(new DefaultItemAnimator());

        schedulesAdapter = new SchedulesAdapter(context, new ArrayList<ScheduleDetails>(), new SchedulesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {

            }
        });

        schedulesHolder.setAdapter(schedulesAdapter);
    }

}
