package com.mastaan.logistics.methods;

import android.app.Activity;

import com.mastaan.logistics.activities.AssignDeliveriesActivity;
import com.mastaan.logistics.activities.BuyerFeedbacksActivity;
import com.mastaan.logistics.activities.BuyerOrdersActivity;
import com.mastaan.logistics.activities.BuyersActivity;
import com.mastaan.logistics.activities.CashNCarryActivity;
import com.mastaan.logistics.activities.CustomerSupportActivity;
import com.mastaan.logistics.activities.DeliveryBoyDeliveriesActivity;
import com.mastaan.logistics.activities.FeedbacksActivity;
import com.mastaan.logistics.activities.FeedbacksByDateActivity;
import com.mastaan.logistics.activities.FollowupsActivity;
import com.mastaan.logistics.activities.FutureOrdersActivity;
import com.mastaan.logistics.activities.MoneyCollectionActivity;
import com.mastaan.logistics.activities.MyDeliveriesActivity;
import com.mastaan.logistics.activities.NewOrdersActivity;
import com.mastaan.logistics.activities.OrderDetailsActivity;
import com.mastaan.logistics.activities.OrdersByDateActivity;
import com.mastaan.logistics.activities.PastPendingOrdersActivity;
import com.mastaan.logistics.activities.PendingCashNCarryActivity;
import com.mastaan.logistics.activities.PendingDeliveriesActivity;
import com.mastaan.logistics.activities.PendingProcessingActivity;
import com.mastaan.logistics.activities.PendingQCActivity;
import com.mastaan.logistics.activities.StockStatementActivity;
import com.mastaan.logistics.activities.TodaysOrdersActivity;
import com.mastaan.logistics.activities.UnProcessedFeedbacksActivity;
import com.mastaan.logistics.activities.UnprocessedCustomerSupportActivity;

/**
 * Created by venkatesh on 27/4/16.
 */
public class ActivityMethods {

    //-----------

    public static final boolean isMyDeliveriesActivity(Activity activity){
        try{
            MyDeliveriesActivity myDeliveriesActivity = (MyDeliveriesActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isPendingDeliveriesActivity(Activity activity){
        try{
            PendingDeliveriesActivity pendingDeliveriesActivity = (PendingDeliveriesActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isAssignDeliveriesActivity(Activity activity){
        try{
            AssignDeliveriesActivity assignDeliveriesActivity = (AssignDeliveriesActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isPendingQCActivity(Activity activity){
        try{
            PendingQCActivity pendingQCActivity = (PendingQCActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isPendingProcessingActivity(Activity activity){
        try{
            PendingProcessingActivity pendingProcessingActivity = (PendingProcessingActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isNewOrdersActivity(Activity activity){
        try{
            NewOrdersActivity newOrdersActivity = (NewOrdersActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isTodaysOrdersActivity(Activity activity){
        try{
            TodaysOrdersActivity todaysOrdersActivity = (TodaysOrdersActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isFutureOrdersActivity(Activity activity){
        try{
            FutureOrdersActivity futureOrdersActivity = (FutureOrdersActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isOrderDetailsActivity(Activity activity){
        try{
            OrderDetailsActivity orderDetailsActivity = (OrderDetailsActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isDeliveryBoyDeliveriesActivity(Activity activity){
        try{
            DeliveryBoyDeliveriesActivity deliveryBoyDeliveriesActivity = (DeliveryBoyDeliveriesActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isPendingCashNCarryItemsActivity(Activity activity){
        try{
            PendingCashNCarryActivity pendingCashNCarryActivity = (PendingCashNCarryActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isCashNCarryItemsActivity(Activity activity){
        try{
            CashNCarryActivity cashNCarryActivity = (CashNCarryActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isPastPendingOrdersActivity(Activity activity){
        try{
            PastPendingOrdersActivity pastPendingOrdersActivity = (PastPendingOrdersActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isCustomerSupportActivity(Activity activity){
        try{
            CustomerSupportActivity customerSupportActivity = (CustomerSupportActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isUnprocessedCustomerSupportActivity(Activity activity){
        try{
            UnprocessedCustomerSupportActivity unprocessedCustomerSupportActivity = (UnprocessedCustomerSupportActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isOrdersByDateActivity(Activity activity){
        try{
            OrdersByDateActivity ordersByDateActivity = (OrdersByDateActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isFeedbacksByDateActivity(Activity activity){
        try{
            FeedbacksByDateActivity feedbacksByDateActivity = (FeedbacksByDateActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isFeedbacksActivity(Activity activity){
        try{
            FeedbacksActivity feedbacksActivity = (FeedbacksActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isFollowupsActivity(Activity activity){
        try{
            FollowupsActivity followupsActivity = (FollowupsActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isUnProcessedFeedbacksActivity(Activity activity){
        try{
            UnProcessedFeedbacksActivity unProcessedFeedbacksActivity = (UnProcessedFeedbacksActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isMoneyCollectionActivity(Activity activity){
        try{
            MoneyCollectionActivity moneyCollectionActivity = (MoneyCollectionActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isBuyerOrdersActivity(Activity activity){
        try{
            BuyerOrdersActivity buyerOrdersActivity = (BuyerOrdersActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isBuyersActivity(Activity activity){
        try{
            BuyersActivity buyersActivity = (BuyersActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isBuyerFeedbacksActivity(Activity activity){
        try{
            BuyerFeedbacksActivity buyerFeedbacksActivity = (BuyerFeedbacksActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

    public static final boolean isStockStatementActivity(Activity activity){
        try{
            StockStatementActivity stockStatementActivity = (StockStatementActivity) activity;
            return true;
        }catch (Exception e){}
        return false;
    }

}
