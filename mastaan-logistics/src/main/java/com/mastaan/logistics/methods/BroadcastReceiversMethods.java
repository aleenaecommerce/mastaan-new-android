package com.mastaan.logistics.methods;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Naresh-Crypsis on 21-12-2015.
 */
public class BroadcastReceiversMethods {
    Context context;

    public BroadcastReceiversMethods(Context context) {
        this.context = context;
    }

    public void broadcastReload() {
        String broadcast_name = "HOME_RECEIVER";
        Intent intent = new Intent(broadcast_name);
        intent.putExtra("type", "Reload");
        context.sendBroadcast(intent);
    }
}
