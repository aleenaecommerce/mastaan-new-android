package com.mastaan.logistics.methods;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by venkatesh on 18/6/16.
 */
public class AnalyseOrderitemsMethods {

    List<String> sectionsList = new ArrayList<>();
    List<GroupedOrdersItemsList> items;

    List<String> categoriesList = new ArrayList<>();

    List<String> successOrdersList = new ArrayList<>();
    List<String> failedOrdersList = new ArrayList<>();
    long totalSuccessAndroidOrdersCount = 0;
    long totalSuccessIosOrdersCount = 0;
    long totalSuccessWebOrdersCount = 0;
    long totalSuccessInternalOrdersCount = 0;

    double totalSuccessAmount = 0;
    double totalSuccessCODCollectAmount = 0;
    double totalSuccessCODReturnAmount = 0;
    double totalSuccessOPAmount = 0;
    double totalSuccessWalletAmount = 0;
    double totalSuccessDiscountAmount = 0;
    double totalSuccessCouponDiscountAmount = 0;
    double totalSuccessMembershipDiscountAmount = 0;
    double totalSuccessMembershipOrderDiscountAmount = 0;
    double totalSuccessMembershipDeliveryDiscountAmount = 0;
    double totalSuccessCouponCashbackAmount = 0;
    double totalSuccessDeliveryCharges = 0;
    double totalSuccessServiceCharges = 0;
    double totalSuccessSurCharges = 0;
    double totalSuccessGovtTaxes = 0;
    long totalFailedAmount = 0;
    List<Double> categoryAmountTotal = new ArrayList<>();
    List<Double> categoryWeightsTotal = new ArrayList<>();
    List<Double> categorySetsTotal = new ArrayList<>();
    List<Double> categoryPiecesTotal = new ArrayList<>();

    List<Map<String, String>> weightSlotsTotalInfoStrings = new ArrayList<>();
    List<String> timesTotalInfoStrings = new ArrayList<>();
    List<String> delaysTotalInfoStrings = new ArrayList<>();
    List<String> vendorsInfoStrings = new ArrayList<>();
    List<InfoDetails> groupedAreaInfos = new ArrayList<>();
    List<InfoDetails> groupedDeliveryBoyInfos = new ArrayList<>();
    List<InfoDetails> groupedStatusInfos = new ArrayList<>();



    public AnalyseOrderitemsMethods(List<String> sectionsList, List<GroupedOrdersItemsList> items){
        this.sectionsList = sectionsList;
        this.items = items;
    }


    public void analyse(){

        Map<String, InfoDetails> areaKeyMapSet = new HashMap<>();
        Map<String, InfoDetails> deliveryBoyKeyMapSet = new HashMap<>();
        Map<String, InfoDetails> statusKeyMapSet = new HashMap<>();

        // SORTING GROPED LIST BY CATEGORY NAME
        Collections.sort(items, new Comparator<GroupedOrdersItemsList>() {
            public int compare(GroupedOrdersItemsList item1, GroupedOrdersItemsList item2) {
                return item1.getCategoryName().compareToIgnoreCase(item2.getCategoryName());
            }
        });

        for (int i=0;i<items.size();i++){
            categoriesList.add(items.get(i).getCategoryName());

            List<OrderItemDetails> itemsList = items.get(i).getItems();
            double catAmountTotal = 0, catWeightsTotal = 0, catSetsTotal = 0, catPiecesTotal = 0;

            Map<String, InfoDetails> categoryTimesKeyMapSet = new HashMap<>();
            Map<String, InfoDetails> categoryDelayKeyMapSet = new HashMap<>();
            Map<String, InfoDetails> categoryVendorsKeyMapSet = new HashMap<>();

            InfoDetails categoryWeightSlotInfoDetails = new InfoDetails().setCategoryName(items.get(i).getCategoryName());
            InfoDetails categoryWeightAllSlotsInfoDetails = new InfoDetails().setDeliveryBy(" all ").setTimeSlot(" All slots ");

            for (int j=0;j<itemsList.size();j++){

                OrderItemDetails orderItemDetails = itemsList.get(j);

                //----- STATUS -----

                String status = orderItemDetails.getStatusString();
                if ( status == null ){ status = " ";    }

                // ADDING TO MAPSET IF NOT PRESENT
                if ( statusKeyMapSet.containsKey(status) == false ) {
                    InfoDetails statusInfoDetails = new InfoDetails().setStatus(status);
                    statusKeyMapSet.put(status, statusInfoDetails);
                }

                statusKeyMapSet.get(status).increaseCount();

                //====================================

                // IF ITEM IS SUCCESS ONLY
                if ( orderItemDetails.isSuccess() ){

                    String itemName = orderItemDetails.getFormattedMeatItemNameUsingAttributeOptions();
                    String timeSlot = orderItemDetails.getDeliveryTimeSlot();
                    String deliveryBy = DateMethods.getTimeIn24HrFormat(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()));

                    String areaName = orderItemDetails.getOrderDetails().getDeliveryAddressSubLocalityLevel1();
                    if ( areaName == null || areaName.length() == 0 ){
                        areaName = orderItemDetails.getOrderDetails().getDeliveryAddressSubLocalityLevel2();
                    }
                    if ( areaName == null ){ areaName = " ";    }

                    String deliveryBoyName = "";


                    //----- AMOUNT -----

                    if ( successOrdersList.contains(orderItemDetails.getOrderID()) == false ){
                        successOrdersList.add(orderItemDetails.getOrderID());
                        if ( orderItemDetails.getOrderDetails().getSource().equalsIgnoreCase("a") ){
                            totalSuccessAndroidOrdersCount += 1;
                        }else if ( orderItemDetails.getOrderDetails().getSource().equalsIgnoreCase("i") ){
                            totalSuccessIosOrdersCount += 1;
                        }else if ( orderItemDetails.getOrderDetails().getSource().equalsIgnoreCase("w") ){
                            totalSuccessWebOrdersCount += 1;
                        }else if ( orderItemDetails.getOrderDetails().getSource().equalsIgnoreCase("p") ){
                            totalSuccessInternalOrdersCount += 1;
                        }

                        totalSuccessAmount += orderItemDetails.getTotalOrderAmount();
                        if ( orderItemDetails.getTotalOrderCashAmount() >= 0 ){
                            totalSuccessCODCollectAmount += orderItemDetails.getTotalOrderCashAmount();
                        }else{
                            totalSuccessCODReturnAmount += Math.abs(orderItemDetails.getTotalOrderCashAmount());
                        }
//                      totalSuccessCODAmount += orderItemDetails.getTotalOrderCashAmount();
                        totalSuccessOPAmount += orderItemDetails.getTotalOrderOnlinePaymentAmount();
                        totalSuccessWalletAmount += orderItemDetails.getTotalOrderWalletAmount();
                        totalSuccessDiscountAmount += orderItemDetails.getTotalOrderDiscount();
                        totalSuccessCouponDiscountAmount += orderItemDetails.getOrderCouponDiscount();
                        totalSuccessMembershipDiscountAmount += orderItemDetails.getOrderMembershipDiscount();
                        totalSuccessMembershipOrderDiscountAmount += orderItemDetails.getOrderMembershipOrderDiscount();
                        totalSuccessMembershipDeliveryDiscountAmount += orderItemDetails.getOrderMembershipDeliveryChargesDiscount();
                        totalSuccessCouponCashbackAmount += orderItemDetails.getOrderCouponCashback();
                        totalSuccessDeliveryCharges += orderItemDetails.getTotalOrderDeliveryCharges();
                        totalSuccessServiceCharges += orderItemDetails.getTotalOrderServiceCharges();
                        totalSuccessSurCharges += orderItemDetails.getTotalOrderSurcharges();
                        totalSuccessGovtTaxes += orderItemDetails.getTotalOrderGovernmentTaxes();
                    }
                    //totalSuccessAmount += orderItems.get(j).getAmountOfItem();
                    catAmountTotal += orderItemDetails.getAmountOfItem();

                    if ( orderItemDetails.getQuantityUnit().equalsIgnoreCase("kg")){
                        catWeightsTotal += orderItemDetails.getQuantity();
                    }
                    else if ( orderItemDetails.getQuantityUnit().equalsIgnoreCase("set")){
                        catSetsTotal += orderItemDetails.getQuantity();
                    }
                    else if ( orderItemDetails.getQuantityUnit().equalsIgnoreCase("piece")){
                        catPiecesTotal += orderItemDetails.getQuantity();
                    }

                    //----- WEIGHT -----

                    if ( sectionsList.contains(Constants.WEIGHT) ){
                        categoryWeightSlotInfoDetails.addToWeightSlotsList(orderItemDetails);
                        categoryWeightAllSlotsInfoDetails.addToWeightsList(orderItemDetails);
                    }

                    //----------- TIME -----------

                    if ( sectionsList.contains(Constants.TIME) ){
                        if ( categoryTimesKeyMapSet.containsKey(timeSlot) == false ) {      // ADDING TO MAPSET IF NOT PRESENT
                            InfoDetails timeInfoDetails = new InfoDetails().setTimeSlot(timeSlot).setDeliveryBy(deliveryBy);
                            categoryTimesKeyMapSet.put(timeSlot, timeInfoDetails);
                        }

                        // FOR DELIVERED / UN DELIVERED
                        if ( orderItemDetails.isDelivered() ){   // DELIVERED
                            categoryTimesKeyMapSet.get(timeSlot).increaseDeliveredCount();
                        }else {
                            categoryTimesKeyMapSet.get(timeSlot).increaseUnDeliveredCount();
                        }

                        // FOR PROCESSED / UNPROCESSED
                        if ( orderItemDetails.isProcessed() ){
                            categoryTimesKeyMapSet.get(timeSlot).addProcessedWeight(orderItemDetails.getQuantity());
                            categoryTimesKeyMapSet.get(timeSlot).increaseProcessedCount();
                        }else{
                            categoryTimesKeyMapSet.get(timeSlot).addUnProcessedWeight(orderItemDetails.getQuantity());
                            categoryTimesKeyMapSet.get(timeSlot).increaseUnProcessedCount();
                        }

                        categoryTimesKeyMapSet.get(timeSlot).increaseCount();

                    }

                    //----------- DELAY -----------

                    if ( sectionsList.contains(Constants.DELAY) ){
                        if ( categoryDelayKeyMapSet.containsKey(timeSlot) == false ) {      // ADDING TO MAPSET IF NOT PRESENT
                            InfoDetails delayInfoDetails = new InfoDetails().setTimeSlot(timeSlot).setDeliveryBy(deliveryBy);
                            categoryDelayKeyMapSet.put(timeSlot, delayInfoDetails);
                        }

                        String deliveryDate = DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate());
                        // DELIVERED
                        if ( orderItemDetails.isDelivered() ){
                            long deliveryDelay = DateMethods.getDifferenceBetweenDatesInMilliSecondsWithSign(orderItemDetails.getDeliveredDate(), deliveryDate);
                            if ( deliveryDelay <= 5*60*1000 ) {
                            }else{
                                categoryDelayKeyMapSet.get(timeSlot).increaseDelayedDeliveryCount();
                            }
                        }
                        // PROCESSED
                        else if ( orderItemDetails.isProcessed() ){
                            long deliveryDelay = DateMethods.getDifferenceBetweenDatesInMilliSecondsWithSign(orderItemDetails.getProcessingCompletedDate(), deliveryDate);
                            if ( deliveryDelay <= 5*60*1000 ) {
                            }else{
                                categoryDelayKeyMapSet.get(timeSlot).increaseDelayedProcessingCount();
                            }
                        }

                    }

                    //------- AREA -----

                    if ( sectionsList.contains(Constants.AREA) ){
                        if ( areaKeyMapSet.containsKey(areaName) == false ) {   // ADDING TO MAPSET IF NOT PRESENT
                            InfoDetails areaInfoDetails = new InfoDetails().setAreaName(areaName);
                            areaKeyMapSet.put(areaName, areaInfoDetails);
                        }

                        // FOR DELIVERED / UNDELIVERED
                        if ( orderItemDetails.isDelivered() ){
                            areaKeyMapSet.get(areaName).increaseDeliveredCount();
                        }else {
                            areaKeyMapSet.get(areaName).increaseUnDeliveredCount();
                        }

                        // FOR PROCESSED / UNPROCESSED
                        if ( orderItemDetails.isProcessed() ){
                            areaKeyMapSet.get(areaName).addProcessedWeight(orderItemDetails.getQuantity());
                            areaKeyMapSet.get(areaName).increaseProcessedCount();
                        }else{
                            areaKeyMapSet.get(areaName).addUnProcessedWeight(orderItemDetails.getQuantity());
                            areaKeyMapSet.get(areaName).increaseUnProcessedCount();
                        }

                        areaKeyMapSet.get(areaName).increaseCount();
                    }

                    //------- VENDOR -----

                    if ( sectionsList.contains(Constants.VENDOR) && orderItemDetails.getVendorDetails() != null ) {
                        if (categoryVendorsKeyMapSet.containsKey(orderItemDetails.getVendorID()) == false) {   // ADDING TO MAPSET IF NOT PRESENT
                            InfoDetails areaInfoDetails = new InfoDetails().setVendorName(orderItemDetails.getVendorDetails().getName());
                            categoryVendorsKeyMapSet.put(orderItemDetails.getVendorID(), areaInfoDetails);
                        }

                        categoryVendorsKeyMapSet.get(orderItemDetails.getVendorID()).increaseCount();
                        categoryVendorsKeyMapSet.get(orderItemDetails.getVendorID()).increaseAmount(orderItemDetails.getVendorPaidAmount());
                        categoryVendorsKeyMapSet.get(orderItemDetails.getVendorID()).increaseQuantity(orderItemDetails.getQuantity(), orderItemDetails.getQuantityUnit());
                    }

                    //------- DELIVERY BOY -----

                    if ( sectionsList.contains(Constants.DELIVERY_BOY) ) {
                        if ( orderItemDetails.isDeliveryAssigned() ){
                            try {
                                deliveryBoyName = orderItemDetails.getDeliveryBoyDetails().getName();
                            }catch (Exception e){}
                            if ( deliveryBoyName == null ){ deliveryBoyName = " ";    }

                            if ( deliveryBoyKeyMapSet.containsKey(deliveryBoyName) == false ) {    // ADDING TO MAPSET IF NOT PRESENT
                                InfoDetails deliveryBoyInfoDetails = new InfoDetails().setDeliveryBoyName(deliveryBoyName);
                                deliveryBoyInfoDetails.setDeliveryBoyName(deliveryBoyName);
                                deliveryBoyKeyMapSet.put(deliveryBoyName, deliveryBoyInfoDetails);
                            }

                            // DELIVERED / UNDELIVERED
                            if ( orderItemDetails.isDelivered() ){
                                deliveryBoyKeyMapSet.get(deliveryBoyName).increaseDeliveredCount();
                            }else {
                                deliveryBoyKeyMapSet.get(deliveryBoyName).increaseUnDeliveredCount();
                            }
                        }
                    }
                }
            }

            //---- ADDING TO CAT WEIGHTS

            categoryAmountTotal.add(catAmountTotal);
            categoryWeightsTotal.add(catWeightsTotal);
            categorySetsTotal.add(catSetsTotal);
            categoryPiecesTotal.add(catPiecesTotal);

            //---- ADDING TO INFO STRINGS

            Map<String, String> categoryWeightSlotInfos = new HashMap<>();
            List<InfoDetails> weightSlotsList = categoryWeightSlotInfoDetails.getWeightSlotsList();
            weightSlotsList.add(categoryWeightAllSlotsInfoDetails);
            for (int w=0;w<weightSlotsList.size();w++){
                categoryWeightSlotInfos.put(weightSlotsList.get(w).getTimeSlot()
                        , getWeightSlotInfoString(weightSlotsList.get(w).getWeightsList()));
            }
            weightSlotsTotalInfoStrings.add(categoryWeightSlotInfos);
            timesTotalInfoStrings.add(getTimeInfoString(categoryTimesKeyMapSet));
            delaysTotalInfoStrings.add(getDelayInfoString(categoryDelayKeyMapSet));
            vendorsInfoStrings.add(getVendorsInfoString(categoryVendorsKeyMapSet));


        }

        //----- AREA ----

        if ( sectionsList.contains(Constants.AREA) ){
            for (String key : areaKeyMapSet.keySet()) {
                groupedAreaInfos.add(areaKeyMapSet.get(key));
            }

            Collections.sort(groupedAreaInfos, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    return item1.getAreaName().compareToIgnoreCase(item2.getAreaName());
                }
            });
        }

        //----- DELIVERY BOY -----

        if ( sectionsList.contains(Constants.DELIVERY_BOY) ){
            for (String key : deliveryBoyKeyMapSet.keySet()) {
                groupedDeliveryBoyInfos.add(deliveryBoyKeyMapSet.get(key));
            }

            Collections.sort(groupedDeliveryBoyInfos, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    return item1.getDeliveryBoyName().compareToIgnoreCase(item2.getDeliveryBoyName());
                }
            });
        }

        //-------- STATUS ------

        if ( sectionsList.contains(Constants.STATUS) ){
            for (String key : statusKeyMapSet.keySet()) {
                groupedStatusInfos.add(statusKeyMapSet.get(key));
            }

            Collections.sort(groupedStatusInfos, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    return item1.getStatus().compareToIgnoreCase(item2.getStatus());
                }
            });
        }

    }

    //===================

    private String getWeightSlotInfoString(List<InfoDetails> groupedWeightInfosList){
        String fullCategoryWeightInfoString = "";

        Collections.sort(groupedWeightInfosList, new Comparator<InfoDetails>() {
            public int compare(InfoDetails item1, InfoDetails item2) {
                return item1.getItemName().compareToIgnoreCase(item2.getItemName());
            }
        });

        Map<String, List<InfoDetails>> groupedSubCategoryWeightInfosKeyMapSet = new HashMap<>();
        for (int i=0;i<groupedWeightInfosList.size();i++){
            if ( groupedSubCategoryWeightInfosKeyMapSet.containsKey(groupedWeightInfosList.get(i).getCategoryName()) ==  false ){
                groupedSubCategoryWeightInfosKeyMapSet.put(groupedWeightInfosList.get(i).getCategoryName(), new ArrayList<InfoDetails>());
            }
            groupedSubCategoryWeightInfosKeyMapSet.get(groupedWeightInfosList.get(i).getCategoryName()).add(groupedWeightInfosList.get(i));
        }

        //-----

        InfoDetails catAnalyticsDetails = new InfoDetails();

        for (String key : groupedSubCategoryWeightInfosKeyMapSet.keySet()) {
            String subCategoryWeightsTotalInfoString = "";

            InfoDetails subCatAnalyticsDetails = new InfoDetails();

            List<InfoDetails> groupedSubCategoryWeightInfos = groupedSubCategoryWeightInfosKeyMapSet.get(key);
            for (int k=0;k<groupedSubCategoryWeightInfos.size();k++) {
                InfoDetails infoDetails = groupedSubCategoryWeightInfos.get(k);

                subCatAnalyticsDetails.processedCount += infoDetails.getProcessedCount();
                subCatAnalyticsDetails.unProcessedCount += infoDetails.getUnProcessedCount();

                subCatAnalyticsDetails.addToSkinQuantities(infoDetails);

                if ( infoDetails.getQuantityUnit().equalsIgnoreCase("kg")) {
                    subCatAnalyticsDetails.totalProcessedKgs += infoDetails.getProcessedQuantity();
                    subCatAnalyticsDetails.totalUnProcessedKgs += infoDetails.getUnProcessedQuantity();
                }else if ( infoDetails.getQuantityUnit().equalsIgnoreCase("piece")) {
                    subCatAnalyticsDetails.totalProcessedPieces += infoDetails.getProcessedQuantity();
                    subCatAnalyticsDetails.totalUnProcessedPieces += infoDetails.getUnProcessedQuantity();
                }
                else if ( infoDetails.getQuantityUnit().equalsIgnoreCase("set")) {
                    subCatAnalyticsDetails.totalProcessedSets += infoDetails.getProcessedQuantity();
                    subCatAnalyticsDetails.totalUnProcessedSets += infoDetails.getUnProcessedQuantity();
                }

                //----

                if (subCategoryWeightsTotalInfoString.length() != 0 ){
                    subCategoryWeightsTotalInfoString += "<br><br>";
                }

                if ( infoDetails.getProcessedQuantity() > 0 && infoDetails.getUnProcessedQuantity() == 0 ){
                    subCategoryWeightsTotalInfoString += "<b>"+SpecialCharacters.CHECK_MARK+"</b> ";
                }
                subCategoryWeightsTotalInfoString += "<b>"+CommonMethods.capitalizeFirstLetter(infoDetails.getFormattedItemName())+"</b>("+infoDetails.getCount()+"): ("+"<b>"+getQuantityWithUnit(infoDetails.getTotalWeight(), infoDetails.getQuantityUnit())+"</b>)";

                String skinSkinlessString = "";
                if ( infoDetails.withSkinQuantity.withSkinQuantity.isHasValues() ){
                    skinSkinlessString += infoDetails.withSkinQuantity.getSkinInfoString();
                }
                if ( infoDetails.withSkinBurntQuantity.withSkinBurntQuantity.isHasValues() ){
                    if ( skinSkinlessString.length() > 0 ){    skinSkinlessString += "<br>";  }
                    skinSkinlessString += infoDetails.withSkinBurntQuantity.getSkinInfoString();
                }
                if ( infoDetails.skinlessQuantity.skinlessQuantity.isHasValues() ){
                    if ( skinSkinlessString.length() > 0 ){    skinSkinlessString += "<br>";  }
                    skinSkinlessString += infoDetails.skinlessQuantity.getSkinInfoString();
                }
                subCategoryWeightsTotalInfoString += (skinSkinlessString.length()>0?"<br>":"")+skinSkinlessString+(skinSkinlessString.length()>0?"<br>":"");

                subCategoryWeightsTotalInfoString += "<br>";
                if ( infoDetails.getUnProcessedQuantity() > 0 ){
                    subCategoryWeightsTotalInfoString += "UnProcessed: "+getQuantityWithUnit(infoDetails.getUnProcessedQuantity(), infoDetails.getQuantityUnit());
                }
                if ( infoDetails.getProcessedQuantity() > 0 ){
                    if ( infoDetails.getUnProcessedQuantity() > 0 ){
                        subCategoryWeightsTotalInfoString += ", ";
                    }
                    subCategoryWeightsTotalInfoString += "Processed: "+getQuantityWithUnit(infoDetails.getProcessedQuantity(), infoDetails.getQuantityUnit());
                }

                //subCategoryWeightsTotalInfoString += "UnProcessed: "+CommonMethods.getIndianFormatNumber(infoDetails.getUnProcessedQuantity())+" kg, Processed: "+CommonMethods.getIndianFormatNumber(infoDetails.getProcessedQuantity())+" kg";
            }

            // AFTER LOOP ENDS

            catAnalyticsDetails.processedCount += subCatAnalyticsDetails.processedCount;
            catAnalyticsDetails.unProcessedCount += subCatAnalyticsDetails.unProcessedCount;

            //catAnalyticsDetails.addToSkinQuantities(subCatAnalyticsDetails);/* NO NEED*/

            catAnalyticsDetails.totalProcessedKgs += subCatAnalyticsDetails.totalProcessedKgs;
            catAnalyticsDetails.totalUnProcessedKgs += subCatAnalyticsDetails.totalUnProcessedKgs;
            catAnalyticsDetails.totalProcessedPieces += subCatAnalyticsDetails.totalProcessedPieces;
            catAnalyticsDetails.totalUnProcessedPieces += subCatAnalyticsDetails.totalUnProcessedPieces;
            catAnalyticsDetails.totalProcessedSets += subCatAnalyticsDetails.totalProcessedSets;
            catAnalyticsDetails.totalUnProcessedSets += subCatAnalyticsDetails.totalUnProcessedSets;

            String totalWeightString = getTotalWeightString((groupedSubCategoryWeightInfos.get(0).getCategoryName()+" "+groupedSubCategoryWeightInfos.get(0).getParentCategoryName()).toUpperCase(), subCatAnalyticsDetails);

            subCategoryWeightsTotalInfoString = totalWeightString+subCategoryWeightsTotalInfoString;

            //==========

            fullCategoryWeightInfoString += subCategoryWeightsTotalInfoString+"<br><br>";
        }

        //-----

        fullCategoryWeightInfoString = getTotalWeightString("TOTAL WEIGHT:", catAnalyticsDetails)+fullCategoryWeightInfoString;


        return fullCategoryWeightInfoString;
    }

    private String getTotalWeightString(String title, InfoDetails infoDetails){

        String totalWeightString = "";
        if ( infoDetails.processedCount > 0 && infoDetails.unProcessedCount == 0 ){
            totalWeightString += "<b>("+SpecialCharacters.CHECK_MARK+") </b>";
        }
        totalWeightString += "<b><u>"+title+"</b></u><br>";



        String skinSkinlessString = "";
        if ( infoDetails.withSkinQuantity.withSkinQuantity.isHasValues() ){
            skinSkinlessString += infoDetails.withSkinQuantity.getSkinInfoString();
        }
        if ( infoDetails.withSkinBurntQuantity.withSkinBurntQuantity.isHasValues() ){
            if ( skinSkinlessString.length() > 0 ){    skinSkinlessString += "<br>";  }
            skinSkinlessString += infoDetails.withSkinBurntQuantity.getSkinInfoString();
        }
        if ( infoDetails.skinlessQuantity.skinlessQuantity.isHasValues() ){
            if ( skinSkinlessString.length() > 0 ){    skinSkinlessString += "<br>";  }
            skinSkinlessString += infoDetails.skinlessQuantity.getSkinInfoString();
        }

        String processedUnProcessedString ="";
        if ( infoDetails.totalProcessedKgs > 0 || infoDetails.totalProcessedSets > 0 || infoDetails.totalProcessedPieces > 0 ){
            processedUnProcessedString += "Processed ("+infoDetails.processedCount+") : ";
            if ( infoDetails.totalProcessedKgs > 0 ){
                processedUnProcessedString += getQuantityWithUnit(infoDetails.totalProcessedKgs, "kg");
            }
            if ( infoDetails.totalProcessedPieces > 0 ){
                if ( infoDetails.totalProcessedKgs > 0 ){ processedUnProcessedString += ", "; }
                processedUnProcessedString += getQuantityWithUnit(infoDetails.totalProcessedPieces, "piece");
            }
            if ( infoDetails.totalProcessedSets > 0 ){
                if ( infoDetails.totalProcessedKgs > 0 || infoDetails.totalProcessedPieces > 0 ){ processedUnProcessedString += ", "; }
                processedUnProcessedString += getQuantityWithUnit(infoDetails.totalProcessedSets, "set");
            }
        }
        if ( infoDetails.totalUnProcessedKgs > 0 || infoDetails.totalUnProcessedSets > 0 || infoDetails.totalUnProcessedPieces > 0 ){
            if ( processedUnProcessedString.length() > 0){ processedUnProcessedString += "<br>";  }
            processedUnProcessedString += "UnProcessed ("+infoDetails.unProcessedCount+") : ";
            if ( infoDetails.totalUnProcessedKgs > 0 ){
                processedUnProcessedString += getQuantityWithUnit(infoDetails.totalUnProcessedKgs, "kg");
            }
            if ( infoDetails.totalUnProcessedPieces > 0 ){
                if ( infoDetails.totalUnProcessedKgs > 0 ){ processedUnProcessedString += ", "; }
                processedUnProcessedString += getQuantityWithUnit(infoDetails.totalUnProcessedPieces, "piece");
            }
            if ( infoDetails.totalUnProcessedSets > 0 ){
                if ( infoDetails.totalUnProcessedKgs > 0 || infoDetails.totalUnProcessedPieces > 0 ){ processedUnProcessedString += ", "; }
                processedUnProcessedString += getQuantityWithUnit(infoDetails.totalUnProcessedSets, "set");
            }
        }

        //---

        totalWeightString += /*cutString+(cutString.length()>0?"<br>":"")+*/skinSkinlessString+(skinSkinlessString.length()>0?"<br><br>":"")
                +processedUnProcessedString+"</b><br><br>";

        return totalWeightString;
    }

    private String getTimeInfoString(Map<String, InfoDetails> timesKeyMapSet) {
        String timeInfo = "Success order items wise:";

        List<InfoDetails> groupedTimeInfos = new ArrayList<>();
        if ( sectionsList.contains(Constants.TIME) ){
            for (String key : timesKeyMapSet.keySet()) {
                groupedTimeInfos.add(timesKeyMapSet.get(key));
            }

            Collections.sort(groupedTimeInfos, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    return DateMethods.compareDates(item1.getDeliveryBy(), item2.getDeliveryBy());//item1.getDeliveryTimeSlot().compareToIgnoreCase(item2.getDeliveryTimeSlot());
                }
            });
        }

        //--------

        long totalDeliveredCount = 0;
        long totalUnDeliveredCount = 0;
        for (int k=0;k<groupedTimeInfos.size();k++) {
            InfoDetails timeInfoDetails = groupedTimeInfos.get(k);
            totalDeliveredCount += timeInfoDetails.getDeliveredCount();
            totalUnDeliveredCount += timeInfoDetails.getUnDeliveredCount();

            if ( timeInfo.length() > 0 ){
                timeInfo += "<br>";
            }
            timeInfo += "<br>";
            if ( timeInfoDetails.getDeliveredCount() > 0 && timeInfoDetails.getUnDeliveredCount() == 0 ){
                timeInfo += "<b>"+SpecialCharacters.CHECK_MARK+" </b>";
            }
            timeInfo += getStatusDetailsString(timeInfoDetails.getTimeSlot(), timeInfoDetails.getCount(), timeInfoDetails.getUnProcessedCount(), timeInfoDetails.getProcessedCount(), timeInfoDetails.getUnDeliveredCount(), timeInfoDetails.getDeliveredCount(), 0, 0);
        }

        if ( totalDeliveredCount > 0 && totalUnDeliveredCount == 0 ){
            timeInfo = "<b>("+SpecialCharacters.CHECK_MARK+")</b> "+timeInfo;
        }

        return timeInfo;
    }

    private String getDelayInfoString(Map<String, InfoDetails> delayKeyMapSet) {
        String delayInfo = "Success order items wise:<br>";

        List<InfoDetails> groupedDelayInfos = new ArrayList<>();
        if ( sectionsList.contains(Constants.DELAY) ){
            for (String key : delayKeyMapSet.keySet()) {
                groupedDelayInfos.add(delayKeyMapSet.get(key));
            }

            Collections.sort(groupedDelayInfos, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    return DateMethods.compareDates(item1.getDeliveryBy(), item2.getDeliveryBy());//item1.getDeliveryTimeSlot().compareToIgnoreCase(item2.getDeliveryTimeSlot());
                }
            });
        }

        //--------

        long totalDelayedProcessingCount = 0;
        long totalDelayedDeliveryCount = 0;
        for (int k=0;k<groupedDelayInfos.size();k++) {
            InfoDetails delayInfoDetails = groupedDelayInfos.get(k);
            totalDelayedProcessingCount += delayInfoDetails.getDelayedProcessingCount();
            totalDelayedDeliveryCount += delayInfoDetails.getDelayedDeliveryCount();

            if ( delayInfoDetails.getDelayedProcessingCount() > 0 || delayInfoDetails.getDelayedDeliveryCount() > 0 ) {
                if (delayInfo.length() > 0) {   delayInfo += "<br>";  }
                delayInfo += getStatusDetailsString(delayInfoDetails.getTimeSlot(), 0, delayInfoDetails.getUnProcessedCount(), delayInfoDetails.getProcessedCount(), delayInfoDetails.getUnDeliveredCount(), delayInfoDetails.getDeliveredCount(), delayInfoDetails.getDelayedProcessingCount(), delayInfoDetails.getDelayedDeliveryCount())+"<br>";
            }
        }

        if ( totalDelayedProcessingCount == 0 && totalDelayedDeliveryCount == 0 ){
            delayInfo = "No processing/delivery delay so far";
        }

        return delayInfo;
    }

    private String getVendorsInfoString(Map<String, InfoDetails> vendorsKeyMapSet) {
        String vendrosInfo = "Success order items wise:<br>";

        List<InfoDetails> groupedVendorsInfos = new ArrayList<>();
        for (String key : vendorsKeyMapSet.keySet()) {
            groupedVendorsInfos.add(vendorsKeyMapSet.get(key));
        }

        Collections.sort(groupedVendorsInfos, new Comparator<InfoDetails>() {
            public int compare(InfoDetails item1, InfoDetails item2) {
                return item1.getVendorName().compareToIgnoreCase(item2.getVendorName());
            }
        });

        //--------

        if ( groupedVendorsInfos.size() > 0 ){
            for (int k=0;k<groupedVendorsInfos.size();k++) {
                InfoDetails vendorInfoDetails = groupedVendorsInfos.get(k);

                if ( vendorInfoDetails.getTotalWeigt() > 0 || vendorInfoDetails.getTotalSets() > 0 || vendorInfoDetails.getTotalPieces() > 0) {
                    if (vendrosInfo.length() > 0) {   vendrosInfo += "<br>";  }
                    vendrosInfo += "<b>"+CommonMethods.capitalizeFirstLetter(vendorInfoDetails.getVendorName())+"</b>("+vendorInfoDetails.getCount()+"): (<b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(vendorInfoDetails.getAmount())+"</b>)<br>";
                    String weightsString = "";
                    if ( vendorInfoDetails.getTotalWeigt() > 0 ){
                        weightsString += CommonMethods.getInDecimalFormat(vendorInfoDetails.getTotalWeigt())+" kg";
                        if ( vendorInfoDetails.getTotalWeigt() != 1){ weightsString += "s";}
                    }
                    if ( vendorInfoDetails.getTotalSets() > 0 ){
                        if (weightsString.length() > 0) {   weightsString += ", ";  }
                        weightsString += CommonMethods.getInDecimalFormat(vendorInfoDetails.getTotalSets())+" set";
                        if ( vendorInfoDetails.getTotalSets() != 1){ weightsString += "s";}
                    }
                    if ( vendorInfoDetails.getTotalPieces() > 0 ){
                        if (weightsString.length() > 0) {   weightsString += ", ";  }
                        weightsString += CommonMethods.getInDecimalFormat(vendorInfoDetails.getTotalPieces())+" piece";
                        if ( vendorInfoDetails.getTotalPieces() != 1){ weightsString += "s";}
                    }
                    vendrosInfo += weightsString+"<br>";
                }
            }
        }else{
            vendrosInfo = "No vendors info so far";
        }

        return vendrosInfo;
    }

    private String getStatusDetailsString(String titleString, long totalCount, long unProcessedCount, long processedCount, long unDeliveredCount, long deliveredCount, long delayedProcessingCount, long delayedDeliveryCount){
        String statusString = "<b>"+titleString+"</b>";
        if ( totalCount > 0 ){  statusString += "("+totalCount+")"; }// : (<b>"+timeInfoDetails.getOrdersCount()+" orders</b>)"+"<br>";;

        if ( unProcessedCount > 0 ){
            statusString += "<br>UnProcessed: <b>"+unProcessedCount+"</b>";
            if ( processedCount > 0 ){
                statusString += ", Processed: <b>"+processedCount+"</b>";
            }
        }

        if ( processedCount > 0 ) {
            statusString += "<br>";
            if ( unDeliveredCount > 0 ){
                statusString += "UnDelivered: <b>"+unDeliveredCount+"</b>";
            }
            if ( deliveredCount > 0 ){
                if ( unDeliveredCount > 0 ){   statusString += ", ";   }
                statusString += "Delivered: <b>"+deliveredCount+"</b>";
            }
        }

        if ( delayedProcessingCount > 0 ){
            statusString += "<br>DelayedProcessing: <b>"+delayedProcessingCount+"</b>";
        }

        if ( delayedDeliveryCount > 0 ){
            statusString += "<br>DelayedDelivery: <b>"+delayedDeliveryCount+"</b>";
        }

        return statusString;
    }

    //===================

    public List<String> getCategoriesList(){
        return categoriesList;
    }

    public String getFullAmountInfo() {
        return getAmountInfo(true);
    }
    public String getShortAmountInfo() {
        return getAmountInfo(false);
    }
    private String getAmountInfo(boolean isFullInfo) {
        String totalAmountString = "";

        // ITERATING FOR FAILED ORDERS AFTER FILTERING SUCCESS ORDERS
        for (int i=0;i<items.size();i++){
            List<OrderItemDetails> orderItems = items.get(i).getItems();
            for (int j=0;j<orderItems.size();j++) {
                final OrderItemDetails orderItemDetails = items.get(i).getItems().get(j);
                // IF FAILURE
                if (orderItems.get(j).getStatusString().equalsIgnoreCase("FAILED") || orderItems.get(j).getStatusString().equalsIgnoreCase("REJECTED") ) {
                    if ( failedOrdersList.contains(orderItems.get(j).getOrderID()) == false  && successOrdersList.contains(orderItems.get(j).getOrderID()) == false ){
                        failedOrdersList.add(orderItems.get(j).getOrderID());
                    }
                    totalFailedAmount += orderItems.get(j).getAmountOfItem();
                }
            }
        }

        totalAmountString = "";
        if ( /*((totalSuccessCODCollectAmount-totalSuccessCODReturnAmount)+totalSuccessOPAmount+totalSuccessDiscountAmount)*/successOrdersList.size() > 0 ) {
            totalAmountString += "<b><u>Success Orders:</b></u> <br>Count: <b>" + successOrdersList.size() + "</b>, Amount: <b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(Math.round(totalSuccessAmount+totalSuccessDiscountAmount))+"</b>";//CommonMethods.getIndianFormatNumber(((totalSuccessCODCollectAmount-totalSuccessCODReturnAmount)+totalSuccessOPAmount+totalSuccessDiscountAmount))+"</b>";
            if ( totalSuccessDeliveryCharges > 0 || totalSuccessServiceCharges > 0 || totalSuccessSurCharges > 0 || totalSuccessGovtTaxes > 0 ){
                String amountSplitString = "O: <b>"+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessAmount+totalSuccessCouponDiscountAmount+totalSuccessMembershipDiscountAmount-totalSuccessDeliveryCharges-totalSuccessServiceCharges-totalSuccessSurCharges-totalSuccessGovtTaxes))+"</b>";
                if ( totalSuccessDeliveryCharges > 0 ) {
                    amountSplitString += ", DEL: <b>" + CommonMethods.getIndianFormatNumber(Math.round(totalSuccessDeliveryCharges)) + "</b>";
                }
                if ( totalSuccessServiceCharges > 0 ) {
                    amountSplitString += ", SER: <b>" + CommonMethods.getIndianFormatNumber(Math.round(totalSuccessServiceCharges)) + "</b>";
                }
                if ( totalSuccessSurCharges > 0 ) {
                    amountSplitString += ", SUR: <b>" + CommonMethods.getIndianFormatNumber(Math.round(totalSuccessSurCharges)) + "</b>";
                }
                if ( totalSuccessGovtTaxes > 0 ) {
                    amountSplitString += ", TAX: <b>" + CommonMethods.getIndianFormatNumber(Math.round(totalSuccessGovtTaxes)) + "</b>";
                }
                totalAmountString += " ("+amountSplitString+")";
            }
            totalAmountString += ", Final Amount: <b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(Math.round(totalSuccessAmount))+"</b>";

            if ( totalSuccessAndroidOrdersCount > 0 || totalSuccessIosOrdersCount > 0 || totalSuccessWebOrdersCount > 0 || totalSuccessInternalOrdersCount > 0 ){
                totalAmountString += "<br>("+(totalSuccessAndroidOrdersCount >0?"Android: <b>"+ totalSuccessAndroidOrdersCount +"</b>":"")
                        +(totalSuccessIosOrdersCount>0?((totalSuccessAndroidOrdersCount >0?", ":"")+"Ios: <b>"+totalSuccessIosOrdersCount+"</b>"):"")
                        +(totalSuccessWebOrdersCount>0?((totalSuccessAndroidOrdersCount >0||totalSuccessIosOrdersCount>0?", ":"")+"Web: <b>"+totalSuccessWebOrdersCount+"</b>"):"")
                        +(totalSuccessInternalOrdersCount>0?((totalSuccessAndroidOrdersCount >0||totalSuccessIosOrdersCount>0||totalSuccessWebOrdersCount>0?", ":"")+"Phone: <b>"+totalSuccessInternalOrdersCount+"</b>"):"")+")";
            }
            // TODO REQUIRED BUT HIDING TEMPORARILY
            /*if ( (totalSuccessCODCollectAmount-totalSuccessCODReturnAmount) != 0 ){
                if ( totalSuccessCODCollectAmount >= 0 && totalSuccessCODReturnAmount == 0 ){
                    totalAmountString += "COD("+totalSuccessCODOrdersCount+"): <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(totalSuccessCODCollectAmount)+"</b>";
                }
                else if ( totalSuccessCODReturnAmount >= 0 && totalSuccessCODCollectAmount == 0 ){
                    totalAmountString += "COD("+totalSuccessCODOrdersCount+"): <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(totalSuccessCODReturnAmount)+"</b>";
                }
                else{
                    totalAmountString += "COD("+totalSuccessCODOrdersCount+"): <br>";
                    totalAmountString += "<b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(totalSuccessCODCollectAmount)+"</b>(c)";
                    totalAmountString += " - <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(totalSuccessCODReturnAmount)+"</b>(r)";
                    totalAmountString += " = <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber((totalSuccessCODCollectAmount-totalSuccessCODReturnAmount))+"</b>";
                }
            }*/
            if ( totalSuccessCODCollectAmount > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                totalAmountString += "COD: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessCODCollectAmount))+"</b>";
            }
            if ( totalSuccessCODReturnAmount > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                totalAmountString += "REF: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessCODReturnAmount))+"</b>";
            }

            if ( totalSuccessOPAmount > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                totalAmountString += "OP: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessOPAmount))+"</b>";
            }

            if ( totalSuccessWalletAmount > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                totalAmountString += "WAL: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessWalletAmount))+"</b>";
            }

            if ( totalSuccessDiscountAmount > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                String discountString = "DIS: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessDiscountAmount))+"</b>";
                if ( totalSuccessCouponDiscountAmount > 0 ){
                    discountString += " (Cou: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessCouponDiscountAmount))+"</b>)";
                }
                if ( totalSuccessMembershipDiscountAmount > 0 ){
                    discountString += " (Mem: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessMembershipDiscountAmount))+"</b>"
                            +(" ["
                                +(totalSuccessMembershipOrderDiscountAmount>0?"O: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessMembershipOrderDiscountAmount))+"</b>":"")
                                +(totalSuccessMembershipDeliveryDiscountAmount>0?(totalSuccessMembershipOrderDiscountAmount>0?", ":"")+"Del: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessMembershipDeliveryDiscountAmount))+"</b>":"")
                            +"]")
                            +")";
                }
                totalAmountString += discountString;
            }
            if ( totalSuccessCouponCashbackAmount > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                totalAmountString += "CB: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(totalSuccessCouponCashbackAmount))+"</b>";
            }
            /*if ( totalSuccessDeliveryCharges > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                totalAmountString += "DEL: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(totalSuccessDeliveryCharges)+"</b>";
            }
            if ( totalSuccessServiceCharges > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                totalAmountString += "SER: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(totalSuccessServiceCharges)+"</b>";
            }
            if ( totalSuccessSurCharges > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                totalAmountString += "SUR: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(totalSuccessSurCharges)+"</b>";
            }
            if ( totalSuccessGovtTaxes > 0 ){
                if ( totalAmountString.length() > 0 ){  totalAmountString += "<br>";    }
                totalAmountString += "TAX: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(totalSuccessGovtTaxes)+"</b>";
            }*/
            totalAmountString += "</b><br>";
        }

        if ( isFullInfo ) {

            if ( totalFailedAmount > 0 ){
                totalAmountString += "<br><b><u>Failed Orders:</b></u> <br>Count: <b>" + failedOrdersList.size()+"</b>, Items Amount: <b>"+SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(Math.round(totalFailedAmount))+"</b><br>";
            }

            totalAmountString += "<br><b><u>Success items category wise:"+"</u></b>";
            for (int i=0;i<items.size();i++){
                totalAmountString += "<br><b>"+items.get(i).getCategoryName()+"</b> : ";
                totalAmountString += "    (<b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(categoryAmountTotal.get(i)))+"</b>)<br>";

                String quantityString = "";
                if ( categoryWeightsTotal.get(i) > 0 ){
                    if ( quantityString.length() > 0 ){ quantityString += "<b>, </b>";  }
                    if ( categoryWeightsTotal.get(i) == 1 ){
                        quantityString += CommonMethods.getIndianFormatNumber(categoryWeightsTotal.get(i))+" Kg";
                    }else{
                        quantityString += CommonMethods.getIndianFormatNumber(categoryWeightsTotal.get(i))+" Kgs";
                    }
                }
                if ( categorySetsTotal.get(i) > 0 ){
                    if ( quantityString.length() > 0 ){ quantityString += "<b>, </b>";  }
                    if ( categorySetsTotal.get(i) == 1 ) {
                        quantityString += CommonMethods.getIndianFormatNumber(categorySetsTotal.get(i)) + " Set";
                    }else{
                        quantityString += CommonMethods.getIndianFormatNumber(categorySetsTotal.get(i))+" Sets";
                    }
                }
                if ( categoryPiecesTotal.get(i) > 0 ){
                    if ( quantityString.length() > 0 ){ quantityString += "<b>, </b>";  }
                    if ( categoryPiecesTotal.get(i) == 1 ) {
                        quantityString += CommonMethods.getIndianFormatNumber(categoryPiecesTotal.get(i)) + " Piece";
                    }else{
                        quantityString += CommonMethods.getIndianFormatNumber(categoryPiecesTotal.get(i))+" Pieces";
                    }
                }
                totalAmountString += quantityString;
            }
        }

        return totalAmountString;
    }

    public List<Map<String, String>> getWeightSlotInfo(){
        return  weightSlotsTotalInfoStrings;
    }

    public List<String> getTimeInfo(){
        return  timesTotalInfoStrings;
    }

    public List<String> getDelayInfo(){
        return delaysTotalInfoStrings;
    }

    public List<String> getVendorsInfo() {
        return vendorsInfoStrings;
    }

    public String getAreaInfo() {
        String areaInfo = "Success order items wise:";

        long totalDeliveredCount = 0;
        long totalUnDeliveredCount = 0;
        for (int k=0;k< groupedAreaInfos.size();k++) {
            InfoDetails areaInfoDetails = groupedAreaInfos.get(k);
            totalDeliveredCount += areaInfoDetails.getDeliveredCount();
            totalUnDeliveredCount += areaInfoDetails.getUnDeliveredCount();

            if ( areaInfo.length() > 0 ){
                areaInfo += "<br>";
            }
            areaInfo += "<br>";
            if ( areaInfoDetails.getDeliveredCount() > 0 && areaInfoDetails.getUnDeliveredCount() == 0 ){
                areaInfo += "<b>" + SpecialCharacters.CHECK_MARK+" </b>";
            }
            areaInfo += getStatusDetailsString(areaInfoDetails.getAreaName(), areaInfoDetails.getCount(), areaInfoDetails.getUnProcessedCount(), areaInfoDetails.getProcessedCount(), areaInfoDetails.getUnDeliveredCount(), areaInfoDetails.getDeliveredCount(), 0, 0);
        }

        if ( totalDeliveredCount > 0 && totalUnDeliveredCount == 0 ){
            areaInfo = "<b>("+SpecialCharacters.CHECK_MARK+")</b> "+areaInfo;
        }

        //------

        return areaInfo;
    }

    public String getDeliveryBoyInfo(){

        String deliveryBoyInfo = "Success order items wise:";

        long totalDeliveredCount = 0;
        long totalUnDeliveredCount = 0;
        for (int k=0;k< groupedDeliveryBoyInfos.size();k++) {
            InfoDetails deliveryBoyInfoDetails = groupedDeliveryBoyInfos.get(k);
            totalDeliveredCount += deliveryBoyInfoDetails.getDeliveredCount();
            totalUnDeliveredCount += deliveryBoyInfoDetails.getUnDeliveredCount();

            if ( deliveryBoyInfo.length() > 0 ){
                deliveryBoyInfo += "<br>";
            }
            deliveryBoyInfo += "<br>";
            if ( deliveryBoyInfoDetails.getDeliveredCount() > 0 && deliveryBoyInfoDetails.getUnDeliveredCount() == 0 ){
                deliveryBoyInfo += "<b>"+SpecialCharacters.CHECK_MARK+" </b>";
            }
            deliveryBoyInfo += "<b>"+CommonMethods.capitalizeFirstLetter(deliveryBoyInfoDetails.getDeliveryBoyName())+"</b><br>";// : (<b>" + deliveryBoyInfoDetails.getOrdersCount()+" orders</b>)"+"<br>";;

            if ( deliveryBoyInfoDetails.getUnDeliveredCount() > 0 ){
                deliveryBoyInfo += "UnDelivered: <b>"+deliveryBoyInfoDetails.getUnDeliveredCount()+"</b>";
            }
            if ( deliveryBoyInfoDetails.getDeliveredCount() > 0 ){
                if ( deliveryBoyInfoDetails.getUnDeliveredCount() > 0 ){   deliveryBoyInfo += ", ";   }
                deliveryBoyInfo += "Delivered: <b>"+deliveryBoyInfoDetails.getDeliveredCount()+"</b>";
            }
        }

        if ( totalDeliveredCount > 0 && totalUnDeliveredCount == 0 ){
            deliveryBoyInfo = "<b>("+SpecialCharacters.CHECK_MARK+")</b> "+deliveryBoyInfo;
        }

        //------

        return deliveryBoyInfo;
    }

    public String getStatusInfo(){
        String statusInfo = "Order items wise:";

        for (int k=0;k< groupedStatusInfos.size();k++) {
            InfoDetails statusInfoDetails = groupedStatusInfos.get(k);

            if ( statusInfo.length() > 0 ){
                statusInfo += "<br>";
            }
            statusInfo += "<br><b>"+CommonMethods.capitalizeFirstLetter(statusInfoDetails.getStatus().toLowerCase())+": "+statusInfoDetails.getCount()+"</b>";
        }

        //------

        return statusInfo;
    }












    //==================

    private String getQuantityWithUnit(double quantity, String quantityUnit){
        String outputString = "";
        if ( quantity == 1 ){
            outputString += "<b>1 "+quantityUnit+"</b>";
        }else{
            outputString += "<b>"+CommonMethods.getIndianFormatNumber(quantity)+" "+quantityUnit+"s</b>";
        }
        return outputString;
    }


    class QuantityInfoDetails{
        QuantitySplitDetails withSkinQuantity = new QuantitySplitDetails();
        QuantitySplitDetails withSkinBurntQuantity = new QuantitySplitDetails();
        QuantitySplitDetails skinlessQuantity = new QuantitySplitDetails();

        QuantitySplitDetails smallCutQuantity = new QuantitySplitDetails();
        QuantitySplitDetails mediumCutQuantity = new QuantitySplitDetails();
        QuantitySplitDetails largeCutQuantity = new QuantitySplitDetails();
        QuantitySplitDetails wholeGuttedCutQuantity = new QuantitySplitDetails();
        QuantitySplitDetails curryCutQuantity = new QuantitySplitDetails();
        QuantitySplitDetails bengaliCutQuantity = new QuantitySplitDetails();
        QuantitySplitDetails noCutQuantity = new QuantitySplitDetails();
        
        class QuantitySplitDetails{
            double totalWeight;
            double totalSets;
            double totalPieces;

            List<QuantitySummary> weightsSummaries = new ArrayList<>();
            List<QuantitySummary> setsSummaries = new ArrayList<>();
            List<QuantitySummary> piecesSummaries = new ArrayList<>();

            class QuantitySummary{
                double quantity;
                long count;
                QuantitySummary(double quantity, long count){
                    this.quantity = quantity;
                    this.count = count;
                }
            }

            public boolean isHasValues(){
                if ( totalWeight > 0 || totalSets > 0 || totalPieces > 0 ){
                    return true;
                }
                return false;
            }
            public void addQuantity(QuantitySplitDetails quantitySplitDetails){
                this.totalWeight += quantitySplitDetails.totalWeight;
                for (int i=0;i<quantitySplitDetails.weightsSummaries.size();i++){
                    addToGroupedSummary(this.weightsSummaries, quantitySplitDetails.weightsSummaries.get(i).quantity, quantitySplitDetails.weightsSummaries.get(i).count);
                }
                this.totalSets += quantitySplitDetails.totalSets;
                for (int i=0;i<quantitySplitDetails.setsSummaries.size();i++){
                    addToGroupedSummary(this.setsSummaries, quantitySplitDetails.setsSummaries.get(i).quantity, quantitySplitDetails.setsSummaries.get(i).count);
                }
                this.totalPieces += quantitySplitDetails.totalPieces;
                for (int i=0;i<quantitySplitDetails.piecesSummaries.size();i++){
                    addToGroupedSummary(this.piecesSummaries, quantitySplitDetails.piecesSummaries.get(i).quantity, quantitySplitDetails.piecesSummaries.get(i).count);
                }
            }
            public void addQuantity(double quantity, String quantityUnit){
                addQuantity(quantity, quantityUnit, true);
            }
            public void addQuantity(double quantity, String quantityUnit, boolean groupQuantities){
                if ( quantityUnit.equalsIgnoreCase("kg")){
                    this.totalWeight += quantity;
                    if ( groupQuantities ){ addToGroupedSummary(this.weightsSummaries, quantity, 1);    }
                }else if ( quantityUnit.equalsIgnoreCase("set")){
                    this.totalSets += quantity;
                    if ( groupQuantities ){ addToGroupedSummary(this.setsSummaries, quantity, 1);    }
                }else if ( quantityUnit.equalsIgnoreCase("piece")){
                    this.totalPieces += quantity;
                        if ( groupQuantities ){ addToGroupedSummary(this.piecesSummaries, quantity, 1);    }
                }
            }
            private void addToGroupedSummary(List<QuantitySummary> quantitySummaries, double quantity, long count){
                if ( quantitySummaries != null && quantity > 0 && count > 0 ) {
                    int containIndex = -1;
                    for (int j = 0; j < quantitySummaries.size(); j++) {
                        if (quantity == quantitySummaries.get(j).quantity) {
                            containIndex = j;
                            break;
                        }
                    }

                    if (containIndex >= 0) {
                        quantitySummaries.get(containIndex).count += count;
                    } else {
                        quantitySummaries.add(new QuantitySummary(quantity, count));
                    }
                }
            }

            public String getQuantityString(){
                String quantityString = "";
                if ( totalWeight > 0 ){
                    String groupedQuantitiesSummary = getGroupedQuantitiesSummary(weightsSummaries, "kg");
                    quantityString += getQuantityWithUnit(totalWeight, "kg")+(groupedQuantitiesSummary.length()>0?" ("+groupedQuantitiesSummary+")":"");
                }
                if ( totalSets > 0 ){
                    String groupedQuantitiesSummary = getGroupedQuantitiesSummary(setsSummaries, "set");
                    quantityString += (quantityString.length()>0?", ":"")+getQuantityWithUnit(totalSets, "set")+(groupedQuantitiesSummary.length()>0?" ("+groupedQuantitiesSummary+")":"");
                }
                if ( totalPieces > 0 ){
                    String groupedQuantitiesSummary = getGroupedQuantitiesSummary(piecesSummaries, "piece");
                    quantityString += (quantityString.length()>0?", ":"")+getQuantityWithUnit(totalPieces, "piece")+(groupedQuantitiesSummary.length()>0?" ("+groupedQuantitiesSummary+")":"");
                }
                return quantityString;
            }
            private String getGroupedQuantitiesSummary(List<QuantitySummary> groupedQuantities, String quantityUnit){
                String summaryString = "";
                if ( groupedQuantities != null && groupedQuantities.size() > 0 ){
                    Collections.sort(groupedQuantities, new Comparator<QuantitySummary>() {
                        public int compare(QuantitySummary item1, QuantitySummary item2) {
                            if ( item1.quantity > item2.quantity ){ return 1;  }
                            if ( item1.quantity < item2.quantity ){ return -1;  }
                            return 0;
                        }
                    });

                    for (int i=0;i<groupedQuantities.size();i++){
                        QuantitySummary quantitySummary = groupedQuantities.get(i);
                        if ( quantitySummary != null && quantitySummary.quantity > 0 && quantitySummary.count > 0 ) {
                            summaryString += (summaryString.length() > 0 ? ", " : "") + CommonMethods.getIndianFormatNumber(quantitySummary.quantity) + " " + quantityUnit + (quantitySummary.quantity > 1 ? "s" : "") + ": <b>" + CommonMethods.getIndianFormatNumber(quantitySummary.count) + "</b>";
                        }
                    }
                }
                return summaryString;
            }
        }

        public void addQuantity(QuantityInfoDetails quantityInfoDetails){
            this.withSkinQuantity.addQuantity(quantityInfoDetails.withSkinQuantity);
            this.withSkinBurntQuantity.addQuantity(quantityInfoDetails.withSkinBurntQuantity);
            this.skinlessQuantity.addQuantity(quantityInfoDetails.skinlessQuantity);

            this.smallCutQuantity.addQuantity(quantityInfoDetails.smallCutQuantity);
            this.mediumCutQuantity.addQuantity(quantityInfoDetails.mediumCutQuantity);
            this.largeCutQuantity.addQuantity(quantityInfoDetails.largeCutQuantity);
            this.wholeGuttedCutQuantity.addQuantity(quantityInfoDetails.wholeGuttedCutQuantity);
            this.curryCutQuantity.addQuantity(quantityInfoDetails.curryCutQuantity);
            this.bengaliCutQuantity.addQuantity(quantityInfoDetails.bengaliCutQuantity);
            this.noCutQuantity.addQuantity(quantityInfoDetails.noCutQuantity);
        }

        public String getSkinInfoString(){
            String skinString = "";
            if ( withSkinQuantity.isHasValues() ){
                String cutInfo = getCutInfoString();
                skinString += "<u>With Skin: "+withSkinQuantity.getQuantityString()+"</u>"+(cutInfo.length()>0?"<br>"+cutInfo:"");
            }
            if ( withSkinBurntQuantity.isHasValues() ){
                String cutInfo = getCutInfoString();
                skinString += (skinString.length()>0?", ":"")+"<u>With Skin (Burnt): "+withSkinBurntQuantity.getQuantityString()+"</u>"+(cutInfo.length()>0?"<br>"+cutInfo:"");
            }
            if ( skinlessQuantity.isHasValues() ){
                String cutInfo = getCutInfoString();
                skinString += (skinString.length()>0?", ":"")+"<u>Skinless: "+skinlessQuantity.getQuantityString()+"</u>"+(cutInfo.length()>0?"<br>"+cutInfo:"");
            }
            return skinString;
        }

        public String getCutInfoString(){
            String cutString = "";
            if ( smallCutQuantity.isHasValues() ){
                cutString += "<b>-></b> Small Cut: "+smallCutQuantity.getQuantityString();
            }
            if ( mediumCutQuantity.isHasValues() ){
                cutString += (cutString.length()>0?"<br>":"")+"<b>-></b> Medium Cut: "+mediumCutQuantity.getQuantityString();
            }
            if ( largeCutQuantity.isHasValues() ){
                cutString += (cutString.length()>0?"<br>":"")+"<b>-></b> Large Cut: "+largeCutQuantity.getQuantityString();
            }
            if ( wholeGuttedCutQuantity.isHasValues() ){
                cutString += (cutString.length()>0?"<br>":"")+"<b>-></b> Whole gutted: "+wholeGuttedCutQuantity.getQuantityString();
            }
            if ( curryCutQuantity.isHasValues() ){
                cutString += (cutString.length()>0?"<br>":"")+"<b>-></b> Curry Cut: "+curryCutQuantity.getQuantityString();
            }
            if ( bengaliCutQuantity.isHasValues() ){
                cutString += (cutString.length()>0?"<br>":"")+"<b>-></b> Bengali Cut: "+bengaliCutQuantity.getQuantityString();
            }
            if ( noCutQuantity.isHasValues() ){
                cutString += (cutString.length()>0?"<br>":"")+"<b>-></b> No Cut: "+noCutQuantity.getQuantityString();
            }
            return cutString;
        }
    }

    class InfoDetails{

        String parent_category;
        String category_name;
        String item_name;
        String quantityUnit;

        long codPaymentCount;
        long onlinePaymentCount;
        long onlineAndCODPaymentCount;
        long delayedProcessingCount;
        long delayedDeliveryCount;

        String areaName;

        String vendorName;

        double totalWeigt;
        double totalSets;
        double totalPieces;

        QuantityInfoDetails withSkinQuantity = new QuantityInfoDetails();
        QuantityInfoDetails withSkinBurntQuantity = new QuantityInfoDetails();
        QuantityInfoDetails skinlessQuantity = new QuantityInfoDetails();

        long processedCount;
        double processedQuantity;

        long unProcessedCount;
        double unProcessedQuantity;

        double totalProcessedKgs;
        double totalUnProcessedKgs;
        double totalProcessedPieces;
        double totalUnProcessedPieces;
        double totalProcessedSets;
        double totalUnProcessedSets;

        String timeSlot;
        String deliveryBy;

        String deliveryBoyName;

        String status;

        long count;
        double amount;

        long ordersCount;
        long deliveredCount;
        long unDeliveredCount;

        List<InfoDetails> slotsList;
        List<InfoDetails> weightsList;
        List<InfoDetails> weightSlotsList;

        String details;

        public void setDetails(String details) {
            this.details = details;
        }
        public String getDetails() {
            return details;
        }

        public InfoDetails setParentCategoryName(String parent_category) {
            this.parent_category = parent_category;
            return this;
        }

        public String getParentCategoryName() {
            return parent_category;
        }

        public InfoDetails setCategoryName(String category_name) {
            this.category_name = category_name;
            return this;
        }

        public String getCategoryName() {
            return category_name;
        }

        public InfoDetails setItemName(String item_name) {
            this.item_name = item_name;
            return this;
        }

        public String getItemName() {
            return item_name;
        }

        public String getFormattedItemName() {
            String itemName = item_name;//.replaceAll(QuantityInfoDetails.getCategoryName()+" ", "");
            if ( itemName.toLowerCase().contains("whole") == false
                    && itemName.toLowerCase().contains("mutton") == false ){
                itemName = itemName.replaceAll(parent_category+" ", "");
            }
            itemName = itemName.replaceAll("[()]","");

            return itemName;
        }

        public InfoDetails setQuantityUnit(String quantityUnit) {
            this.quantityUnit = quantityUnit;
            return this;
        }

        public String getQuantityUnit() {
            return quantityUnit;
        }

        public long getProcessedCount() {
            return processedCount;
        }

        public void increaseProcessedCount() {
            this.processedCount += 1;
        }

        public double getProcessedQuantity() {
            return processedQuantity;
        }

        public void addProcessedWeight(double processedWeight) {
            this.processedQuantity += processedWeight;
        }

        public long getUnProcessedCount() {
            return unProcessedCount;
        }

        public void increaseUnProcessedCount() {
            this.unProcessedCount +=1;
        }

        public double getUnProcessedQuantity() {
            return unProcessedQuantity;
        }

        public void addUnProcessedWeight(double unProcessedWeight) {
            this.unProcessedQuantity += unProcessedWeight;
        }

        public void increaseCODPaymentCount() {
            this.codPaymentCount += 1;
        }

        public long getCODPaymentCount() {
            return codPaymentCount;
        }

        public void increaseOnlinePaymentCount() {
            this.onlinePaymentCount += 1;
        }

        public long getOnlinePaymentCount() {
            return onlinePaymentCount;
        }

        public void increaseOnlineAndCODPaymentCount() {
            this.onlineAndCODPaymentCount += 1;
        }

        public long getOnlineAndCODPaymentCount() {
            return onlineAndCODPaymentCount;
        }

        public void increaseDelayedProcessingCount() {
            this.delayedProcessingCount +=1;
        }

        public long getDelayedProcessingCount() {
            return delayedProcessingCount;
        }

        public void increaseDelayedDeliveryCount() {
            this.delayedDeliveryCount +=1;
        }

        public long getDelayedDeliveryCount() {
            return delayedDeliveryCount;
        }

        public double getTotalWeight(){
            return processedQuantity + unProcessedQuantity;
        }

        public InfoDetails setDeliveryBoyName(String deliveryBoyName) {
            this.deliveryBoyName = deliveryBoyName;
            return this;
        }

        public String getDeliveryBoyName() {
            return deliveryBoyName;
        }

        public InfoDetails setAreaName(String areaName) {
            this.areaName = areaName;
            return this;
        }

        public String getAreaName() {
            return areaName;
        }

        public InfoDetails setVendorName(String vendorName) {
            this.vendorName = vendorName;
            return this;
        }

        public String getVendorName() {
            return vendorName;
        }

        public void increaseQuantity(double increaseQuantity, String quanityUnity){
            if ( quanityUnity.equalsIgnoreCase("kg")){
                increaseWeightsTotal(increaseQuantity);
            }
            else if ( quanityUnity.equalsIgnoreCase("set")){
                increaseSetsTotal(increaseQuantity);
            }
            else if ( quanityUnity.equalsIgnoreCase("piece")){
                increasePiecesTotal(increaseQuantity);
            }
        }

        public double getTotalWeigt() {
            return totalWeigt;
        }

        public void increaseWeightsTotal(double increaseWeight){
            this.totalWeigt += increaseWeight;
        }

        public double getTotalSets() {
            return totalSets;
        }

        public void increaseSetsTotal(double increaseSets){
            this.totalSets += increaseSets;
        }

        public double getTotalPieces() {
            return totalPieces;
        }

        public void increasePiecesTotal(double increasePieces){
            this.totalPieces += increasePieces;
        }

        public void addToSkinQuantities(InfoDetails infoDetails){
            withSkinQuantity.addQuantity(infoDetails.withSkinQuantity);
            withSkinBurntQuantity.addQuantity(infoDetails.withSkinBurntQuantity);
            skinlessQuantity.addQuantity(infoDetails.skinlessQuantity);
        }
        public void addToSkinQuantities(OrderItemDetails orderItemDetails){
            String formattedExtras = orderItemDetails.getFormattedExtras().toLowerCase();
            double quantity = orderItemDetails.getQuantity();
            String quantityUnit = orderItemDetails.getQuantityUnit();

            // SKIN
            QuantityInfoDetails skinInfoDetails = null;
            if ( formattedExtras.contains("with skin") ){
                withSkinQuantity.withSkinQuantity.addQuantity(quantity, quantityUnit, false);
                skinInfoDetails = withSkinQuantity;
            }else if ( formattedExtras.contains("burnt") ){
                withSkinBurntQuantity.withSkinBurntQuantity.addQuantity(quantity, quantityUnit, false);
                skinInfoDetails = withSkinBurntQuantity;
            }else {
                skinlessQuantity.skinlessQuantity.addQuantity(quantity, quantityUnit, false);
                skinInfoDetails = skinlessQuantity;
            }

            // CUT
            if ( formattedExtras.contains("small cut") ){
                skinInfoDetails.smallCutQuantity.addQuantity(quantity, quantityUnit);
            }else if ( formattedExtras.contains("medium cut") ){
                skinInfoDetails.mediumCutQuantity.addQuantity(quantity, quantityUnit);
            }else if ( formattedExtras.contains("large cut") ){
                skinInfoDetails.largeCutQuantity.addQuantity(quantity, quantityUnit);
            }else if ( formattedExtras.contains("whole gutted") ){
                skinInfoDetails.wholeGuttedCutQuantity.addQuantity(quantity, quantityUnit);
            }else if ( formattedExtras.contains("curry cut") ){
                skinInfoDetails.curryCutQuantity.addQuantity(quantity, quantityUnit);
            }else if ( formattedExtras.contains("bengali cut") ){
                skinInfoDetails.bengaliCutQuantity.addQuantity(quantity, quantityUnit);
            }else{// if ( formattedExtras.contains("no cut") ){
                skinInfoDetails.noCutQuantity.addQuantity(quantity, quantityUnit);
            }
        }


        public InfoDetails setTimeSlot(String timeSlot) {
            this.timeSlot = timeSlot;
            return this;
        }

        public String getTimeSlot() {
            return timeSlot;
        }

        public InfoDetails setDeliveryBy(String deliveryBy) {
            this.deliveryBy = deliveryBy;
            return this;
        }

        public String getDeliveryBy() {
            return deliveryBy;
        }

        public void increaseOrdersCount() {
            this.ordersCount += 1;
        }

        public long getOrdersCount() {
            return ordersCount;
        }

        public void increaseDeliveredCount() {
            this.deliveredCount += 1;
        }

        public long getDeliveredCount() {
            return deliveredCount;
        }

        public void increaseUnDeliveredCount() {
            this.unDeliveredCount += 1;
        }

        public long getUnDeliveredCount() {
            return unDeliveredCount;
        }

        public InfoDetails setStatus(String status) {
            this.status = status;
            return this;
        }

        public String getStatus() {
            return status;
        }

        public void increaseCount() {
            this.count += 1;
        }

        public long getCount() {
            return count;
        }

        public void increaseAmount(double increaseAmount){   this.amount += increaseAmount; }

        public double getAmount() {
            return amount;
        }

        public InfoDetails addToWeightSlotsList(OrderItemDetails orderItemDetails){

            // ADDING TO WEIGHT SLOTS LIST
            if ( weightSlotsList == null ){  weightSlotsList = new ArrayList<>(); }
            int containIndex = -1;
            for (int i=0;i<weightSlotsList.size();i++){
                if ( orderItemDetails.getOrderDetails().getDeliveryDate().equals(weightSlotsList.get(i).getDeliveryBy())){
                    containIndex = i;
                    break;
                }
            }
            if ( containIndex == -1 ){
                weightSlotsList.add(new InfoDetails().setDeliveryBy(orderItemDetails.getOrderDetails().getDeliveryDate()).setTimeSlot(orderItemDetails.getDeliveryTimeSlot()));
                containIndex = weightSlotsList.size()-1;
            }

            weightSlotsList.get(containIndex).addToWeightsList(orderItemDetails);
            weightSlotsList.get(containIndex).increaseCount();

//            // ADDING TO WEIGHTS OF SLOTS
//            InfoDetails weightSlotInfoDetails = weightSlotsList.get(containIndex);
//            for (int i=0;i<weightSlotInfoDetails.getWeightsList().size();i++){
//                if ( orderItemDetails.getFormattedMeatItemNameUsingAttributeOptions().equalsIgnoreCase(weightSlotInfoDetails.getWeightsList().get(i).getItemName())){
//                    containIndex = i;
//                    break;
//                }
//            }
//            if ( containIndex == -1 ){
//                weightSlotInfoDetails.getWeightsList().add(new InfoDetails().setItemName(orderItemDetails.getFormattedMeatItemNameUsingAttributeOptions()));
//                containIndex = weightSlotInfoDetails.getWeightsList().size()-1;
//            }
//            weightSlotInfoDetails.getWeightsList().get(containIndex).increaseCount();

            //------

            return this;
        }

        public List<InfoDetails> getWeightSlotsList() {
            if ( weightSlotsList == null ){   weightSlotsList = new ArrayList<>();   }
//            Collections.sort(weightSlotsList, new Comparator<InfoDetails>() {
//                public int compare(InfoDetails item1, InfoDetails item2) {
//                    return item1.getTimeSlot().compareToIgnoreCase(item2.getTimeSlot());
//                }
//            });
            return weightSlotsList;
        }

        public InfoDetails addToWeightsList(OrderItemDetails orderItemDetails){
            if ( weightsList == null ){  weightsList = new ArrayList<>(); }
            int containIndex = -1;
            for (int i=0;i<weightsList.size();i++){
                if ( orderItemDetails.getFormattedMeatItemNameUsingAttributeOptions().equalsIgnoreCase(weightsList.get(i).getItemName())){
                    containIndex = i;
                    break;
                }
            }
            if ( containIndex == -1 ){
                weightsList.add(new InfoDetails().setItemName(orderItemDetails.getFormattedMeatItemNameUsingAttributeOptions()).setQuantityUnit(orderItemDetails.getMeatItemDetails().getQuantityUnit()).setParentCategoryName(orderItemDetails.getMeatItemDetails().getParentCategory()).setCategoryName(orderItemDetails.getMeatItemDetails().getCategoryDetails().getName()));
                containIndex = weightsList.size()-1;
            }


            weightsList.get(containIndex).increaseCount();

            if ( orderItemDetails.getQuantityUnit().equalsIgnoreCase("kg")){
                weightsList.get(containIndex).increaseWeightsTotal(orderItemDetails.getQuantity());
            }
            else if ( orderItemDetails.getQuantityUnit().equalsIgnoreCase("set")){
                weightsList.get(containIndex).increaseSetsTotal(orderItemDetails.getQuantity());
            }
            else if ( orderItemDetails.getQuantityUnit().equalsIgnoreCase("piece")){
                weightsList.get(containIndex).increasePiecesTotal(orderItemDetails.getQuantity());
            }

            // PROCESSED / UNPROCESSED
            if ( orderItemDetails.isProcessed() ){
                weightsList.get(containIndex).addProcessedWeight(orderItemDetails.getQuantity());
                weightsList.get(containIndex).increaseProcessedCount();
            }else{
                weightsList.get(containIndex).addUnProcessedWeight(orderItemDetails.getQuantity());
                weightsList.get(containIndex).increaseUnProcessedCount();
            }

            //
            weightsList.get(containIndex).addToSkinQuantities(orderItemDetails);

            return this;
        }

        public List<InfoDetails> getWeightsList() {
            if ( weightsList == null ){   weightsList = new ArrayList<>();   }
//            Collections.sort(weightsList, new Comparator<InfoDetails>() {
//                public int compare(InfoDetails item1, InfoDetails item2) {
//                    return item1.getItemName().compareToIgnoreCase(item2.getItemName());
//                }
//            });
            return weightsList;
        }
    }

}
