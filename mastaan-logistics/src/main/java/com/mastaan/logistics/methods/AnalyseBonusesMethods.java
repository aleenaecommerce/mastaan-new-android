package com.mastaan.logistics.methods;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.EmployeeBonusDetails;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.UserDetailsMini;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by venkatesh on 26/11/18.
 */

public class AnalyseBonusesMethods {

    List<EmployeeBonusDetails> itemsList = new ArrayList<>();

    InfoDetails summaryDetails = new InfoDetails();
    List<InfoDetails> employeesSummariesList = new ArrayList<>();
    List<InfoDetails> buyersSummariesList = new ArrayList<>();

    List<String> sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW, Constants.EMPLOYEES, Constants.BUYERS});

    class InfoDetails {
        String id;
        String name;

        long totalBonusesCount;
        double totalBonusesAmount;
        long totalEmployeesCount;
        long totalBuyersCount;
        long totalOrdersCount;
        double totalOrdersAmount;

        List<String> employeesList = new ArrayList<>();
        List<String> buyersList = new ArrayList<>();

        void addToSummary(EmployeeBonusDetails bonusDetails){
            if ( bonusDetails != null && bonusDetails.getEmployeeDetails() != null && bonusDetails.getBuyerDetails() != null && bonusDetails.getOrderDetails() != null ){
                UserDetailsMini employeeDetails = bonusDetails.getEmployeeDetails();
                BuyerDetails buyerDetails = bonusDetails.getBuyerDetails();
                OrderDetails orderDetails = bonusDetails.getOrderDetails();

                // Bonuses Count & Amount
                totalBonusesCount += 1;
                totalBonusesAmount += bonusDetails.getAmount();

                // Employees Count
                if ( employeesList.contains(employeeDetails.getID()) == false ){
                    totalEmployeesCount += 1;
                    employeesList.add(employeeDetails.getID());
                }

                // Buyers Count
                if ( buyersList.contains(buyerDetails.getID()) == false ){
                    totalBuyersCount += 1;
                    buyersList.add(buyerDetails.getID());
                }

                // Orders Count & Amount
                totalOrdersCount += 1;
                totalOrdersAmount += (orderDetails.getTotalAmount()+orderDetails.getTotalDiscount());

            }
        }
    }


    public AnalyseBonusesMethods(List<EmployeeBonusDetails> itemsList){
        if ( itemsList == null ){   itemsList = new ArrayList<>();  }
        this.itemsList = itemsList;
    }


    public AnalyseBonusesMethods setSectionsList(List<String> sectionsList){
        if ( sectionsList != null && sectionsList.size() > 0 ){
            this.sectionsList = sectionsList;
        }
        return this;
    }

    public AnalyseBonusesMethods analyse(){
        for (int i=0;i<itemsList.size();i++){
            try{
                EmployeeBonusDetails bonusDetails = itemsList.get(i);
                if ( bonusDetails != null && bonusDetails.getEmployeeDetails() != null && bonusDetails.getBuyerDetails() != null && bonusDetails.getOrderDetails() != null ){
                    // SUMMARY
                    summaryDetails.addToSummary(bonusDetails);

                    // EMPLOYEES SUMMARY
                    if ( sectionsList.contains(Constants.EMPLOYEES) ) {
                        InfoDetails existingEmployeeSummary = null;
                        for (int j = 0; j < employeesSummariesList.size(); j++) {
                            if (employeesSummariesList.get(j).id.equals(bonusDetails.getEmployeeDetails().getID())) {
                                existingEmployeeSummary = employeesSummariesList.get(j);
                                break;
                            }
                        }
                        if (existingEmployeeSummary == null) {
                            existingEmployeeSummary = new InfoDetails();
                            existingEmployeeSummary.id = bonusDetails.getEmployeeDetails().getID();
                            existingEmployeeSummary.name = bonusDetails.getEmployeeDetails().getAvailableName();
                            employeesSummariesList.add(existingEmployeeSummary);
                        }
                        existingEmployeeSummary.addToSummary(bonusDetails);
                    }

                    // BUYERS SUMMARY
                    if ( sectionsList.contains(Constants.BUYERS) ) {
                        InfoDetails existingBuyerSummary = null;
                        for (int j = 0; j < buyersSummariesList.size(); j++) {
                            if (buyersSummariesList.get(j).id.equals(bonusDetails.getBuyerDetails().getID())) {
                                existingBuyerSummary = buyersSummariesList.get(j);
                                break;
                            }
                        }
                        if (existingBuyerSummary == null) {
                            existingBuyerSummary = new InfoDetails();
                            existingBuyerSummary.id = bonusDetails.getBuyerDetails().getID();
                            existingBuyerSummary.name = bonusDetails.getBuyerDetails().getAvailableName();
                            buyersSummariesList.add(existingBuyerSummary);
                        }
                        existingBuyerSummary.addToSummary(bonusDetails);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if ( sectionsList.contains(Constants.EMPLOYEES) ) {
            Collections.sort(employeesSummariesList, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    if (item1.totalBonusesCount > item2.totalBonusesCount) {
                        return -1;
                    } else if (item1.totalBonusesCount < item2.totalBonusesCount) {
                        return 1;
                    }
                    return 0;
                }
            });
        }

        if ( sectionsList.contains(Constants.BUYERS) ) {
            Collections.sort(buyersSummariesList, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    if (item1.totalBonusesCount > item2.totalBonusesCount) {
                        return -1;
                    } else if (item1.totalBonusesCount < item2.totalBonusesCount) {
                        return 1;
                    }
                    return 0;
                }
            });
        }
        return this;
    }

    public String getSummaryInfo(){
        return getSummaryString(summaryDetails);
    }

    public String getEmployeesSummaryInfo(){
        String employeesSummaryInfo = "";
        for (int i=0;i<employeesSummariesList.size();i++){
            employeesSummaryInfo += (employeesSummaryInfo.length()>0?"<br><br>":"") + getSummaryString(employeesSummariesList.get(i), false, true);
        }
        return employeesSummaryInfo;
    }

    public String getBuyersSummaryInfo(){
        String buyersSummaryInfo = "";
        for (int i=0;i<buyersSummariesList.size();i++){
            buyersSummaryInfo += (buyersSummaryInfo.length()>0?"<br><br>":"") + getSummaryString(buyersSummariesList.get(i), false, false);
        }
        return buyersSummaryInfo;
    }

    //---------

    private String getSummaryString(InfoDetails infoDetails){
        return getSummaryString(infoDetails, true, true);
    }
    private String getSummaryString(InfoDetails infoDetails, boolean showEmployeesSummary, boolean showBuyersSummary){
        String summaryInfo = "";
        if ( infoDetails != null ) {
            if (infoDetails.name != null && infoDetails.name.trim().length() > 0) {
                summaryInfo += "<b><u>" + CommonMethods.capitalizeStringWords(infoDetails.name) + "</u></b>";
            }

            summaryInfo += (summaryInfo.length() > 0 ? "<br>" : "") + "Bonuses amount (<b>"+CommonMethods.getIndianFormatNumber(infoDetails.totalBonusesCount)+"</b>):   <b>" + CommonMethods.getIndianFormatNumber(infoDetails.totalBonusesAmount) + "</b>";
            if (showEmployeesSummary) {
                summaryInfo += (summaryInfo.length() > 0 ? "<br>" : "") + "Employees:   <b>" + CommonMethods.getIndianFormatNumber(infoDetails.totalEmployeesCount) + "</b>";
            }
            if (showBuyersSummary) {
                summaryInfo += (summaryInfo.length() > 0 ? "<br>" : "") + "Buyers:   <b>" + CommonMethods.getIndianFormatNumber(infoDetails.totalBuyersCount) + "</b>";
            }
            summaryInfo += (summaryInfo.length() > 0 ? "<br>" : "") + "Orders amount "/*"(<b>"+CommonMethods.getIndianFormatNumber(infoDetails.totalOrdersCount)+")</b>"*/+":   <b>" + CommonMethods.getIndianFormatNumber(infoDetails.totalOrdersAmount) + "</b>";
        }
        return summaryInfo;
    }

}