package com.mastaan.logistics.methods;

import android.os.AsyncTask;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.EmployeeBonusDetails;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.MoneyCollectionDetails;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.ProcessedStockDetails;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.StockDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 12/7/16.
 */
public final class SearchMethods {

    public interface SearchOrderItemsCallback{
        void onComplete(boolean status, SearchDetails searchDetails, List<OrderItemDetails> filteredList);
    }

    public interface SearchOrdersCallback{
        void onComplete(boolean status, SearchDetails searchDetails, List<OrderDetails> filteredList);
    }

    public interface SearchFeedbacksCallback{
        void onComplete(boolean status, SearchDetails searchDetails, List<FeedbackDetails> filteredList);
    }

    public interface SearchFollowupsCallback{
        void onComplete(boolean status, SearchDetails searchDetails, List<FollowupDetails> filteredList);
    }

    public interface SearchMoneyCollectionsCallback{
        void onComplete(boolean status, SearchDetails searchDetails, List<MoneyCollectionDetails> filteredList);
    }

    public interface SearchStocksCallback{
        void onComplete(boolean status, SearchDetails searchDetails, List<StockDetails> stocksList);
    }

    public interface SearchProcessedStocksCallback{
        void onComplete(boolean status, SearchDetails searchDetails, List<ProcessedStockDetails> stocksList);
    }

    public interface SearchEmployeeBonusesCallback{
        void onComplete(boolean status, SearchDetails searchDetails, List<EmployeeBonusDetails> bonusesList);
    }


    //======  ORDER ITEMS SEARCH   =======//

    public static final void searchOrderItems(final List<OrderItemDetails> originalList, final SearchDetails searchDetails, final String serverTime, final SearchOrderItemsCallback callback){
        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> filteredList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {
                filteredList = searchOrderItems(originalList, searchDetails, serverTime);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ) {
                    callback.onComplete(true, searchDetails, filteredList);
                }
            }

        }.execute();
    }
    public static final List<OrderItemDetails> searchOrderItems(final List<OrderItemDetails> originalList, final SearchDetails searchDetails, final String serverTime){
        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            List<OrderItemDetails> filteredList = new ArrayList<>();
            for (int i=0;i<originalList.size();i++){
                OrderItemDetails orderItemDetails = originalList.get(i);
                if ( orderItemDetails != null && orderItemDetails.getOrderDetails() != null ){
                    OrderDetails2 orderDetails = orderItemDetails.getOrderDetails();

                    boolean skipSearch = false;
                    boolean isMatched = false;

                    // WITH ORDER ID
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getID() != null && searchDetails.getID().trim().length() > 0 ){
                        if ( orderDetails.getFormattedOrderID().toLowerCase().contains(searchDetails.getID().trim().toLowerCase())
                               || orderDetails.getReadableOrderID().toLowerCase().contains(searchDetails.getID().trim().toLowerCase()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH STATUS
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getStatus() != null && searchDetails.getStatus().trim().length() > 0 ){
                        if ( orderItemDetails.getStatusString().equalsIgnoreCase(searchDetails.getStatus()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH SOURCE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getSource() != null && searchDetails.getSource().trim().length() > 0 ){
                        String source = searchDetails.getSource().replaceAll(Constants.ANDROID, "a").replaceAll(Constants.IOS, "i").replaceAll(Constants.WEB, "w").replaceAll(Constants.PHONE, "p");
                        if ( orderDetails.getSource().equalsIgnoreCase(source) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH CUSTOMER NAME
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCustomerName() != null && searchDetails.getCustomerName().trim().length() > 0 ){
                        if ( orderDetails.getCustomerName().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())
                                || orderDetails.getCustomerMobile().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())
                                || orderDetails.getCustomerAlternateMobile().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH ITEM NAME
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getItemName() != null && searchDetails.getItemName().trim().length() > 0 ){
                        if ( orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize().toLowerCase().contains(searchDetails.getItemName().trim().toLowerCase()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH ITEM PREFERENCES
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getItemPreferences() != null && searchDetails.getItemPreferences().trim().length() > 0 ){
                        if ( orderItemDetails.getFormattedExtras().toLowerCase().contains(searchDetails.getItemPreferences().trim().toLowerCase()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH PROCESSOR NAME
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getProcessorName() != null && searchDetails.getProcessorName().trim().length() > 0 ){
                        String processorName = "";
                        try{ processorName = orderItemDetails.getProcessorDetails().getName(); }catch (Exception e){}
                        if ( processorName.trim().toLowerCase().contains(searchDetails.getProcessorName().trim().toLowerCase()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH DELIVERYBOY NAME
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryBoyName() != null && searchDetails.getDeliveryBoyName().trim().length() > 0 ){
                        String deliveryBoyName = "";
                        try{ deliveryBoyName = orderItemDetails.getDeliveryBoyDetails().getName(); }catch (Exception e){}
                        if ( deliveryBoyName.trim().toLowerCase().contains(searchDetails.getDeliveryBoyName().trim().toLowerCase()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH VENDOR NAME
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getVendorName() != null && searchDetails.getVendorName().trim().length() > 0 ){
                        String vendorName = "";
                        if ( orderItemDetails.getVendorDetails() != null ){ vendorName = orderItemDetails.getVendorDetails().getName(); }
                        if ( vendorName.trim().toLowerCase().contains(searchDetails.getVendorName().trim().toLowerCase()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH BUTCHER NAME
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getButcherName() != null && searchDetails.getButcherName().trim().length() > 0 ){
                        String butcherName = "";
                        if ( orderItemDetails.getButcherDetails() != null ){ butcherName = orderItemDetails.getButcherDetails().getAvailableName(); }
                        if ( butcherName.trim().toLowerCase().contains(searchDetails.getButcherName().trim().toLowerCase()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH DELIVERY ZONE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryZone() != null && searchDetails.getDeliveryZone().trim().length() > 0 ){
                        if ( orderDetails.getDeliveryArea() != null && orderDetails.getDeliveryArea().getDeliveryZoneID() != null
                                && orderDetails.getDeliveryArea().getDeliveryZoneID().toLowerCase().contains(searchDetails.getDeliveryZone().toLowerCase())
                                ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH DELIVERY AREA
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryArea() != null && searchDetails.getDeliveryArea().trim().length() > 0 ){
                        String deliveryArea = CommonMethods.getStringFromStringArray(new String[]{orderDetails.getDeliveryAddress(), orderDetails.getDeliveryAddressLandmark()});
                        if ( (orderDetails.getDeliveryArea() != null
                                && (orderDetails.getDeliveryArea().getCode().toLowerCase().contains(searchDetails.getDeliveryArea().toLowerCase())
                                || orderDetails.getDeliveryArea().getName().toLowerCase().contains(searchDetails.getDeliveryArea().toLowerCase())
                        ))
                                || deliveryArea.toLowerCase().contains(searchDetails.getDeliveryArea().toLowerCase()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH HOUSING SOCIETY
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getHousingSociety() != null && searchDetails.getHousingSociety().trim().length() > 0 ){
                        if ( orderDetails.getHousingSociety() != null && orderDetails.getHousingSociety().toLowerCase().contains(searchDetails.getHousingSociety().toLowerCase()) ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH DELIVERY DATE
                    if ( skipSearch == false && searchDetails.getDeliveryDate() != null && searchDetails.getDeliveryDate().trim().length() > 0 ){
                        String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()));
                        if ( DateMethods.compareDates(deliveryDate, searchDetails.getDeliveryDate()) == 0 ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH DELIVERY TIME
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryTime() != null && searchDetails.getDeliveryTime().trim().length() > 0 ){
                        String deliveryTime = DateMethods.getTimeIn24HrFormat(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()));
                        if ( DateMethods.compareDates(deliveryTime, searchDetails.getDeliveryTime()) == 0 ){
                            isMatched = true;
                        }else{
                            skipSearch = true;
                        }
                    }

                    // WITH MINIMUM & MAXIMUM AMOUNT
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && ( searchDetails.getMinAmount() != -1 || searchDetails.getMaxAmount() != -1 )){
                        if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() == -1 ){
                            if ( orderItemDetails.getOrderDetails().getTotalAmount() >= searchDetails.getMinAmount() ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getMaxAmount() > 0 && searchDetails.getMinAmount() == -1 ){
                            if ( orderItemDetails.getOrderDetails().getTotalAmount() <= searchDetails.getMaxAmount() ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() > 0 ){
                            if ( orderItemDetails.getOrderDetails().getTotalAmount() >= searchDetails.getMinAmount()
                                    && orderItemDetails.getOrderDetails().getTotalAmount() <= searchDetails.getMaxAmount()  ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH DISCOUNT TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDiscountType() != null && searchDetails.getDiscountType().trim().length() > 0 ){
                        if ( searchDetails.getDiscountType().equalsIgnoreCase(Constants.WITH_DISCOUNT) ){
                            if ( orderItemDetails.getOrderDetails().getTotalDiscount() > 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getDiscountType().equalsIgnoreCase(Constants.WITHOUT_DISCOUNT) ){
                            if ( orderItemDetails.getOrderDetails().getTotalDiscount() == 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH OUTSTANDING TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getOutstandingType() != null && searchDetails.getOutstandingType().trim().length() > 0 ){
                        if ( searchDetails.getOutstandingType().equalsIgnoreCase(Constants.WITH_OUTSTANDING) ){
                            if ( orderItemDetails.getOrderDetails().getBuyerDetails().getOutstandingAmount() != 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getOutstandingType().equalsIgnoreCase(Constants.WITHOUT_OUTSTANDING) ){
                            if (  orderItemDetails.getOrderDetails().getBuyerDetails().getOutstandingAmount() == 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH PAYMENT TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getPaymentType() != null && searchDetails.getPaymentType().trim().length() > 0 ){
                        if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.CASH_ON_DELIVERY) ){
                            if ( orderItemDetails.getOrderDetails().getTotalCashAmount() != 0
                                    && orderItemDetails.getOrderDetails().getTotalOnlinePaymentAmount() == 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.ONLINE_PAYMENT) ){
                            if ( orderItemDetails.getOrderDetails().getTotalOnlinePaymentAmount() != 0
                                    && orderItemDetails.getOrderDetails().getTotalCashAmount() == 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.COD_AND_OP) ){
                            if ( orderItemDetails.getOrderDetails().getTotalOnlinePaymentAmount() != 0
                                    && orderItemDetails.getOrderDetails().getTotalCashAmount() != 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.NEGATIVE_COD) ){
                            if ( orderItemDetails.getOrderDetails().getTotalCashAmount() < 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH ORDRE DAY TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getOrderDayType() != null && searchDetails.getOrderDayType().trim().length() > 0 ){
                        String orderCreatedDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getCreatedDate()));
                        String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getDeliveryDate()));

                        if ( searchDetails.getOrderDayType().equalsIgnoreCase(Constants.PRE_ORDER) ){
                            if ( DateMethods.compareDates(deliveryDate, orderCreatedDate) > 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getOrderDayType().equalsIgnoreCase(Constants.SAME_DAY_ORDER) ){
                            if ( DateMethods.compareDates(deliveryDate, orderCreatedDate) == 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH REFERRAL ORDER TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getReferralOrderType() != null && searchDetails.getReferralOrderType().trim().length() > 0 ){
                        if ( searchDetails.getReferralOrderType().equalsIgnoreCase(Constants.REFERRAL_ORDERS) ) {
                            if (orderDetails.isReferralOrder()) {
                                isMatched = true;
                            } else {
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH FOLLOWUP RESULTED ORDER TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFollowupResultedOrderType() != null && searchDetails.getFollowupResultedOrderType().trim().length() > 0 ){
                        if ( searchDetails.getFollowupResultedOrderType().equalsIgnoreCase(Constants.FOLLOWUP_RESULTED_ORDERS) ) {
                            if (orderDetails.isFollowupResultedOrder()) {
                                isMatched = true;
                            } else {
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH FIRST TIME ORDER TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFirstTimeOrderType() != null && searchDetails.getFirstTimeOrderType().trim().length() > 0 ){
                        if ( searchDetails.getFirstTimeOrderType().equalsIgnoreCase(Constants.FIRST_TIME_ORDERS) ) {
                            if (orderItemDetails.getOrderDetails().isFirstOrder()) {
                                isMatched = true;
                            } else {
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH BILL PRINT TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getBillPrintType() != null && searchDetails.getBillPrintType().trim().length() > 0
                            && orderItemDetails.getStatusString().equalsIgnoreCase(Constants.FAILED) == false ){
                        if ( searchDetails.getBillPrintType().equalsIgnoreCase(Constants.BILL_PRINTED) ){
                            if ( orderItemDetails.getOrderDetails().isBillPrinted() ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getBillPrintType().equalsIgnoreCase(Constants.BILL_NOT_PRINTED) ){
                            if ( orderItemDetails.getOrderDetails().isBillPrinted() == false){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH DELIVERY AREA ASSIGN TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryAreaAssignType() != null && searchDetails.getDeliveryAreaAssignType().trim().length() > 0 ){
                        if ( searchDetails.getDeliveryAreaAssignType().equalsIgnoreCase(Constants.UNASSIGNED_DELIVERY_AREA) ){
                            if ( orderItemDetails.getOrderDetails().getDeliveryArea() == null ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH CUSTOMER SUPPORT TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCustomerSupportType() != null && searchDetails.getCustomerSupportType().trim().length() > 0 ){
                        if ( searchDetails.getCustomerSupportType().equalsIgnoreCase(Constants.IN_CUSTOMER_SUPPORT) ){
                            if ( orderItemDetails.isInCustomerSupportState() ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getCustomerSupportType().equalsIgnoreCase(Constants.NOT_IN_CUSTOMER_SUPPORT) ){
                            if ( orderItemDetails.isInCustomerSupportState() == false){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH CASH-N-CARRY TYPE
                    if ((skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCashNCarryType() != null && searchDetails.getCashNCarryType().trim().length() > 0 ){
                        if ( searchDetails.getCashNCarryType().equalsIgnoreCase(Constants.CASH_N_CARRY) ){
                            if ( orderItemDetails.isCashNCarryItem() ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getCashNCarryType().equalsIgnoreCase(Constants.NOT_CASH_N_CARRY) ){
                            if ( orderItemDetails.isCashNCarryItem() == false){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH DELAY TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDelayType() != null && searchDetails.getDelayType().trim().length() > 0 ){
                        if ( orderItemDetails.getStatusString().equalsIgnoreCase(Constants.DELIVERED) ){
                            String delayComments = orderItemDetails.getDelayedDeliveryComment();
                            if ( searchDetails.getDelayType().equalsIgnoreCase(Constants.WITH_DELAY) ){
                                if ( delayComments != null && delayComments.trim().length() > 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }
                            else if ( searchDetails.getDelayType().equalsIgnoreCase(Constants.WITHOUT_DELAY) ){
                                if ( delayComments == null || delayComments.trim().length() == 0){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }
                        }
                    }

                    // WITH INSTRUCTIONS TYPE
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getInstructionsType() != null && searchDetails.getInstructionsType().trim().length() > 0 ){
                        String specialInstructions = orderItemDetails.getCombinedSpecialInstructions();
                        if ( searchDetails.getInstructionsType().equalsIgnoreCase(Constants.WITH_INSTRUCTIONS) ){
                            if ( specialInstructions != null && specialInstructions.trim().length() > 0 ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                        else if ( searchDetails.getInstructionsType().equalsIgnoreCase(Constants.WITHOUT_INSTRUCTIONS) ){
                            if ( specialInstructions == null || specialInstructions.trim().length() == 0){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    // WITH CREATED BY
                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCreatedBy() != null && searchDetails.getCreatedBy().trim().length() > 0 ){
                        if ( searchDetails.getCreatedBy().equalsIgnoreCase(Constants.SUPER_USER) ){
                            if ( orderDetails.getOrderCreatorName() != null ){
                                isMatched = true;
                            }else{
                                skipSearch = true;
                            }
                        }
                    }

                    //=============

                    if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                        filteredList.add(orderItemDetails);
                    }

                }
            }
            return filteredList;
        }
        else{
            return originalList;
        }
    }


    //======  ORDERS SEARCH   =======//

    public static final void searchOrders(final List<OrderDetails> originalList, final SearchDetails searchDetails, final String serverTime, final SearchOrdersCallback callback) {

        if (originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable()) {

            new AsyncTask<Void, Void, Void>() {

                List<OrderDetails> filteredList = new ArrayList<>();

                @Override
                protected Void doInBackground(Void... voids) {

                    for (int i=0;i<originalList.size();i++){

                        boolean skipSearch = false;
                        boolean isMatched = false;

                        OrderDetails orderDetails = originalList.get(i);

                        if ( orderDetails != null && orderDetails.getOrderedItems() != null && orderDetails.getOrderedItems().size() > 0 ){

                            // WITH ORDER ID
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getID() != null && searchDetails.getID().trim().length() > 0 ){
                                if ( orderDetails.getFormattedOrderID().toLowerCase().contains(searchDetails.getID().trim().toLowerCase())
                                        || orderDetails.getReadableOrderID().toLowerCase().contains(searchDetails.getID().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH STATUS
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getStatus() != null && searchDetails.getStatus().trim().length() > 0 ){
                                if ( orderDetails.getStatus().equalsIgnoreCase(searchDetails.getStatus()) ){
                                    isMatched = true;
                                }else{
                                    boolean isMatchedAnyOrderItemStatus = false;
                                    if ( orderDetails.getOrderedItems() != null && orderDetails.getOrderedItems().size() > 0 ) {
                                        for (int j = 0; j < orderDetails.getOrderedItems().size(); j++) {
                                            if ( orderDetails.getOrderedItems().get(j) != null && orderDetails.getOrderedItems().get(j).getStatusString().equalsIgnoreCase(searchDetails.getStatus()) ){
                                                isMatchedAnyOrderItemStatus = true;
                                                break;
                                            }
                                        }
                                    }

                                    if ( isMatchedAnyOrderItemStatus ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH SOURCE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getSource() != null && searchDetails.getSource().trim().length() > 0 ){
                                String source = searchDetails.getSource().replaceAll(Constants.ANDROID, "a").replaceAll(Constants.IOS, "i").replaceAll(Constants.WEB, "w").replaceAll(Constants.PHONE, "p");
                                if ( orderDetails.getSource().equalsIgnoreCase(source) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH CUSTOMER NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCustomerName() != null && searchDetails.getCustomerName().trim().length() > 0 ){
                                if ( orderDetails.getCustomerName().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())
                                        || orderDetails.getCustomerMobile().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())
                                        || orderDetails.getCustomerAlternateMobile().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH DELIVERY ZONE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryZone() != null && searchDetails.getDeliveryZone().trim().length() > 0 ){
                                if ( orderDetails.getDeliveryArea() != null && orderDetails.getDeliveryArea().getDeliveryZoneID() != null
                                        && orderDetails.getDeliveryArea().getDeliveryZoneID().toLowerCase().contains(searchDetails.getDeliveryZone().toLowerCase())
                                        ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH DELIVERY AREA
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryArea() != null && searchDetails.getDeliveryArea().trim().length() > 0 ){
                                String deliveryArea = CommonMethods.getStringFromStringArray(new String[]{orderDetails.getDeliveryAddress(), orderDetails.getDeliveryAddressLandmark()});
                                if ( (orderDetails.getDeliveryArea() != null
                                        && (orderDetails.getDeliveryArea().getCode().toLowerCase().contains(searchDetails.getDeliveryArea().toLowerCase())
                                        || orderDetails.getDeliveryArea().getName().toLowerCase().contains(searchDetails.getDeliveryArea().toLowerCase())
                                ))
                                        || deliveryArea.toLowerCase().contains(searchDetails.getDeliveryArea().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH HOUSING SOCIETY
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getHousingSociety() != null && searchDetails.getHousingSociety().trim().length() > 0 ){
                                if ( orderDetails.getHousingSociety() != null && orderDetails.getHousingSociety().toLowerCase().contains(searchDetails.getHousingSociety().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH DELIVERY DATE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryDate() != null && searchDetails.getDeliveryDate().trim().length() > 0 ){
                                String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate()));
                                if ( DateMethods.compareDates(deliveryDate, searchDetails.getDeliveryDate()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH DELIVERY TIME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryTime() != null && searchDetails.getDeliveryTime().trim().length() > 0 ){
                                String deliveryTime = DateMethods.getTimeIn24HrFormat(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate()));
                                if ( DateMethods.compareDates(deliveryTime, searchDetails.getDeliveryTime()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH MINIMUM & MAXIMUM AMOUNT
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && ( searchDetails.getMinAmount() != -1 || searchDetails.getMaxAmount() != -1 )){
                                if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() == -1 ){
                                    if ( orderDetails.getTotalAmount() >= searchDetails.getMinAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMaxAmount() > 0 && searchDetails.getMinAmount() == -1 ){
                                    if ( orderDetails.getTotalAmount() <= searchDetails.getMaxAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() > 0 ){
                                    if ( orderDetails.getTotalAmount() >= searchDetails.getMinAmount()
                                            && orderDetails.getTotalAmount() <= searchDetails.getMaxAmount()  ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH DISCOUNT TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDiscountType() != null && searchDetails.getDiscountType().trim().length() > 0 ){
                                if ( searchDetails.getDiscountType().equalsIgnoreCase(Constants.WITH_DISCOUNT) ){
                                    if ( orderDetails.getTotalDiscount() > 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getDiscountType().equalsIgnoreCase(Constants.WITHOUT_DISCOUNT) ){
                                    if ( orderDetails.getTotalDiscount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH OUTSTANDING TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getOutstandingType() != null && searchDetails.getOutstandingType().trim().length() > 0 ){
                                if ( searchDetails.getOutstandingType().equalsIgnoreCase(Constants.WITH_OUTSTANDING) ){
                                    if ( orderDetails.getBuyerDetails().getOutstandingAmount() != 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getOutstandingType().equalsIgnoreCase(Constants.WITHOUT_OUTSTANDING) ){
                                    if (  orderDetails.getBuyerDetails().getOutstandingAmount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH PAYMENT TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getPaymentType() != null && searchDetails.getPaymentType().trim().length() > 0 ){
                                if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.CASH_ON_DELIVERY) ){
                                    if ( orderDetails.getTotalCashAmount() != 0
                                            && orderDetails.getTotalOnlinePaymentAmount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.ONLINE_PAYMENT) ){
                                    if ( orderDetails.getTotalOnlinePaymentAmount() != 0
                                            && orderDetails.getTotalCashAmount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.COD_AND_OP) ){
                                    if ( orderDetails.getTotalOnlinePaymentAmount() != 0
                                            && orderDetails.getTotalCashAmount() != 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.NEGATIVE_COD) ){
                                    if ( orderDetails.getTotalCashAmount() < 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH ORDRE DAY TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getOrderDayType() != null && searchDetails.getOrderDayType().trim().length() > 0 ){
                                String orderCreatedDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()));
                                String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate()));

                                if ( searchDetails.getOrderDayType().equalsIgnoreCase(Constants.PRE_ORDER) ){
                                    if ( DateMethods.compareDates(deliveryDate, orderCreatedDate) > 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getOrderDayType().equalsIgnoreCase(Constants.SAME_DAY_ORDER) ){
                                    if ( DateMethods.compareDates(deliveryDate, orderCreatedDate) == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH REFERRAL ORDER TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getReferralOrderType() != null && searchDetails.getReferralOrderType().trim().length() > 0 ){
                                if ( searchDetails.getReferralOrderType().equalsIgnoreCase(Constants.REFERRAL_ORDERS) ) {
                                    if (orderDetails.isReferralOrder()) {
                                        isMatched = true;
                                    } else {
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH FOLLOWUP RESULTED ORDER TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFollowupResultedOrderType() != null && searchDetails.getFollowupResultedOrderType().trim().length() > 0 ){
                                if ( searchDetails.getFollowupResultedOrderType().equalsIgnoreCase(Constants.FOLLOWUP_RESULTED_ORDERS) ) {
                                    if (orderDetails.isFollowupResultedOrder()) {
                                        isMatched = true;
                                    } else {
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH FIRST TIME ORDER TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFirstTimeOrderType() != null && searchDetails.getFirstTimeOrderType().trim().length() > 0 ){
                                if ( searchDetails.getFirstTimeOrderType().equalsIgnoreCase(Constants.FIRST_TIME_ORDERS) ) {
                                    if (orderDetails.isFirstOrder()) {
                                        isMatched = true;
                                    } else {
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH BILL PRINT TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getBillPrintType() != null && searchDetails.getBillPrintType().trim().length() > 0 ){
                                if ( searchDetails.getBillPrintType().equalsIgnoreCase(Constants.BILL_PRINTED) ){
                                    if ( orderDetails.isBillPrinted() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getBillPrintType().equalsIgnoreCase(Constants.BILL_NOT_PRINTED) ){
                                    if ( orderDetails.isBillPrinted() == false){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH DELIVERY AREA ASSIGN TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryAreaAssignType() != null && searchDetails.getDeliveryAreaAssignType().trim().length() > 0 ){
                                if ( searchDetails.getDeliveryAreaAssignType().equalsIgnoreCase(Constants.UNASSIGNED_DELIVERY_AREA) ){
                                    if ( orderDetails.getDeliveryArea() == null ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH DELAY TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDelayType() != null && searchDetails.getDelayType().trim().length() > 0 ){
                                String delayComments = null;
                                for (int s=0;s<orderDetails.getOrderedItems().size();s++){
                                    if ( orderDetails.getOrderedItems().get(s).getDelayedDeliveryComment() != null && orderDetails.getOrderedItems().get(s).getDelayedDeliveryComment().trim().length() > 0 ){
                                        delayComments += orderDetails.getOrderedItems().get(s).getDelayedDeliveryComment();
                                    }
                                }
                                if ( searchDetails.getDelayType().equalsIgnoreCase(Constants.WITH_DELAY) ){
                                    if ( delayComments != null && delayComments.trim().length() > 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getDelayType().equalsIgnoreCase(Constants.WITHOUT_DELAY) ){
                                    if ( delayComments == null || delayComments.trim().length() == 0){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH INSTRUCTIONS TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getInstructionsType() != null && searchDetails.getInstructionsType().trim().length() > 0 ){
                                String specialInstructions = null;
                                for (int s=0;s<orderDetails.getOrderedItems().size();s++){
                                    if ( orderDetails.getOrderedItems().get(s).getCombinedSpecialInstructions() != null && orderDetails.getOrderedItems().get(s).getCombinedSpecialInstructions().trim().length() > 0 ){
                                        specialInstructions += orderDetails.getOrderedItems().get(s).getCombinedSpecialInstructions();
                                    }
                                }
                                if ( searchDetails.getInstructionsType().equalsIgnoreCase(Constants.WITH_INSTRUCTIONS) ){
                                    if ( specialInstructions != null && specialInstructions.trim().length() > 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getInstructionsType().equalsIgnoreCase(Constants.WITHOUT_INSTRUCTIONS) ){
                                    if ( specialInstructions == null || specialInstructions.trim().length() == 0){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH CREATED BY
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCreatedBy() != null && searchDetails.getCreatedBy().trim().length() > 0 ){
                                if ( searchDetails.getCreatedBy().equalsIgnoreCase(Constants.SUPER_USER) ){
                                    if ( orderDetails.getOrderCreatorName() != null ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            /// WITH PROCESSOR NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getProcessorName() != null && searchDetails.getProcessorName().trim().length() > 0 ){
                                boolean isAnyItemMatched = false;
                                for (int j=0;j<orderDetails.getOrderedItems().size();j++) {
                                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(j);
                                    if (orderItemDetails != null) {
                                        String processorName = "";
                                        try{ processorName = orderItemDetails.getProcessorDetails().getName(); }catch (Exception e){}
                                        if ( processorName.trim().toLowerCase().contains(searchDetails.getProcessorName().trim().toLowerCase()) ){
                                            isAnyItemMatched = true;
                                            break;
                                        }
                                    }
                                }
                                if ( isAnyItemMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }

                            }

                            // WITH DELIVERYBOY NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryBoyName() != null && searchDetails.getDeliveryBoyName().trim().length() > 0 ){
                                boolean isAnyItemMatched = false;
                                for (int j=0;j<orderDetails.getOrderedItems().size();j++) {
                                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(j);
                                    if (orderItemDetails != null) {
                                        String deliveryBoyName = "";
                                        if ( orderItemDetails.getDeliveryBoyDetails() != null ){ deliveryBoyName = orderItemDetails.getDeliveryBoyDetails().getName(); }
                                        if ( deliveryBoyName.trim().toLowerCase().contains(searchDetails.getDeliveryBoyName().trim().toLowerCase()) ){
                                            isAnyItemMatched = true;
                                            break;
                                        }
                                    }
                                }
                                if ( isAnyItemMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH VENDOR NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getVendorName() != null && searchDetails.getVendorName().trim().length() > 0 ){
                                boolean isAnyItemMatched = false;
                                for (int j=0;j<orderDetails.getOrderedItems().size();j++) {
                                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(j);
                                    if (orderItemDetails != null) {
                                        String vendorName = "";
                                        if ( orderItemDetails.getVendorDetails() != null ){ vendorName = orderItemDetails.getVendorDetails().getName(); }
                                        if ( vendorName.trim().toLowerCase().contains(searchDetails.getVendorName().trim().toLowerCase()) ){
                                            isAnyItemMatched = true;
                                            break;
                                        }
                                    }
                                }
                                if ( isAnyItemMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH BUTCHER NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getButcherName() != null && searchDetails.getButcherName().trim().length() > 0 ){
                                boolean isAnyItemMatched = false;
                                for (int j=0;j<orderDetails.getOrderedItems().size();j++) {
                                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(j);
                                    if (orderItemDetails != null) {
                                        String butcherName = "";
                                        if ( orderItemDetails.getButcherDetails() != null ){ butcherName = orderItemDetails.getButcherDetails().getAvailableName(); }
                                        if ( butcherName.trim().toLowerCase().contains(searchDetails.getButcherName().trim().toLowerCase()) ){
                                            isAnyItemMatched = true;
                                            break;
                                        }
                                    }
                                }
                                if ( isAnyItemMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }


                            // WITH CUSTOMER SUPPORT TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCustomerSupportType() != null && searchDetails.getCustomerSupportType().trim().length() > 0 ){
                                boolean isAnyItemMatched = false;
                                for (int j=0;j<orderDetails.getOrderedItems().size();j++){
                                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(j);
                                    if ( orderItemDetails != null ){

                                        if ( searchDetails.getCustomerSupportType().equalsIgnoreCase(Constants.IN_CUSTOMER_SUPPORT) ){
                                            if ( orderItemDetails.isInCustomerSupportState() ){
                                                isAnyItemMatched = true;
                                                break;
                                            }
                                        }
                                        else if ( searchDetails.getCustomerSupportType().equalsIgnoreCase(Constants.NOT_IN_CUSTOMER_SUPPORT) ){
                                            if ( orderItemDetails.isInCustomerSupportState() == false){
                                                isAnyItemMatched = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if ( isAnyItemMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH CASH-N-CARRY TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCashNCarryType() != null && searchDetails.getCashNCarryType().trim().length() > 0 ) {
                                boolean isAnyItemMatched = false;
                                for (int j=0;j<orderDetails.getOrderedItems().size();j++) {
                                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(j);
                                    if (orderItemDetails != null) {
                                        if ( searchDetails.getCashNCarryType().equalsIgnoreCase(Constants.CASH_N_CARRY) ){
                                            if ( orderItemDetails.isCashNCarryItem() ){
                                                isAnyItemMatched = true;
                                                break;
                                            }
                                        }
                                        else if ( searchDetails.getCashNCarryType().equalsIgnoreCase(Constants.NOT_CASH_N_CARRY) ){
                                            if ( orderItemDetails.isCashNCarryItem() == false){
                                                isAnyItemMatched = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if ( isAnyItemMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH ITEM NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getItemName() != null && searchDetails.getItemName().trim().length() > 0 ){
                                boolean isAnyItemMatched = false;
                                for (int j=0;j<orderDetails.getOrderedItems().size();j++) {
                                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(j);
                                    if (orderItemDetails != null) {
                                        if ( orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize().toLowerCase().contains(searchDetails.getItemName().trim().toLowerCase()) ){
                                            isAnyItemMatched = true;
                                            break;
                                        }
                                    }
                                }
                                if ( isAnyItemMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH ITEM PREFERENCES
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getItemPreferences() != null && searchDetails.getItemPreferences().trim().length() > 0 ){
                                boolean isAnyItemMatched = false;
                                for (int j=0;j<orderDetails.getOrderedItems().size();j++) {
                                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(j);
                                    if (orderItemDetails != null) {
                                        if ( orderItemDetails.getFormattedExtras().toLowerCase().contains(searchDetails.getItemPreferences().trim().toLowerCase()) ){
                                            isAnyItemMatched = true;
                                            break;
                                        }
                                    }
                                }
                                if ( isAnyItemMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }


                            //=============

                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                                filteredList.add(orderDetails);
                            }

                        }
                    }

                    return null;
                }


                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, searchDetails, filteredList);
                    }
                }

            }.execute();
        }
    }


    //======  FEEDBACKS SEARCH   =======//

    public static final void searchFeedbacks(final List<FeedbackDetails> originalList, final SearchDetails searchDetails, final SearchFeedbacksCallback callback){
        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            new AsyncTask<Void, Void, Void>() {
                List<FeedbackDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i=0;i<originalList.size();i++){
                        FeedbackDetails feedbackDetails = originalList.get(i);
                        if ( feedbackDetails != null && feedbackDetails.getOrderItemDetails() != null && feedbackDetails.getOrderItemDetails().getOrderDetails() != null ){
                            OrderItemDetails orderItemDetails = feedbackDetails.getOrderItemDetails();
                            OrderDetails2 orderDetails = orderItemDetails.getOrderDetails();

                            boolean skipSearch = false;
                            boolean isMatched = false;


                            // WITH DELIVERY DATE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryDate() != null && searchDetails.getDeliveryDate().trim().length() > 0 ){
                                String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()));
                                if ( DateMethods.compareDates(deliveryDate, searchDetails.getDeliveryDate()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH FEEDBACK DATE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFeedbackDate() != null && searchDetails.getFeedbackDate().trim().length() > 0 ){
                                String feedbackDate = DateMethods.getOnlyDate(feedbackDetails.getCreatedDate());
                                if ( DateMethods.compareDates(feedbackDate, searchDetails.getFeedbackDate()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH CATEOGRY
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && orderItemDetails.getMeatItemDetails() != null && searchDetails.getCategoryType() != null && searchDetails.getCategoryType().trim().length() > 0 ){
                                if ( orderItemDetails.getMeatItemDetails().getParentCategory().replaceAll("\\s+", "").equalsIgnoreCase(searchDetails.getCategoryType()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }
                            // WITH RATING
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getRatingType() != -1 ){
                                if ( feedbackDetails.getCustomerRating() == searchDetails.getRatingType() ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }
                            // WITH COMMENTS
                            if ( searchDetails.getCommentsType() != null && searchDetails.getCommentsType().trim().length() > 0 ){
                                if ( searchDetails.getCommentsType().equalsIgnoreCase(Constants.WITH_COMMENTS) ){
                                    if ( feedbackDetails.getCustomerComments() != null && feedbackDetails.getCustomerComments().trim().length() > 0  ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getCommentsType().equalsIgnoreCase(Constants.WITHOUT_COMMENTS) ){
                                    if ( feedbackDetails.getCustomerComments() == null || feedbackDetails.getCustomerComments().trim().length() == 0  ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH CUSTOMER NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCustomerName() != null && searchDetails.getCustomerName().trim().length() > 0 ){
                                if ( orderDetails.getCustomerName().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())
                                        || orderDetails.getCustomerMobile().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())
                                        || orderDetails.getCustomerAlternateMobile().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH ITEM NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getItemName() != null && searchDetails.getItemName().trim().length() > 0 ){
                                if ( orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize().toLowerCase().contains(searchDetails.getItemName().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH ITEM PREFERENCES
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getItemPreferences() != null && searchDetails.getItemPreferences().trim().length() > 0 ){
                                if ( orderItemDetails.getFormattedExtras().toLowerCase().contains(searchDetails.getItemPreferences().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH PROCESSOR NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getProcessorName() != null && searchDetails.getProcessorName().trim().length() > 0 ){
                                String processorName = "";
                                try{ processorName = orderItemDetails.getProcessorDetails().getName(); }catch (Exception e){}
                                if ( processorName.trim().toLowerCase().contains(searchDetails.getProcessorName().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH DELIVERY BOY NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryBoyName() != null && searchDetails.getDeliveryBoyName().trim().length() > 0 ){
                                String deliveryBoyName = "";
                                try{ deliveryBoyName = orderItemDetails.getDeliveryBoyDetails().getName(); }catch (Exception e){}
                                if ( deliveryBoyName.trim().toLowerCase().contains(searchDetails.getDeliveryBoyName().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH VENDOR NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getVendorName() != null && searchDetails.getVendorName().trim().length() > 0 ){
                                String vendorName = "";
                                if ( orderItemDetails.getVendorDetails() != null ){ vendorName = orderItemDetails.getVendorDetails().getName(); }
                                if ( vendorName.trim().toLowerCase().contains(searchDetails.getVendorName().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH BUTCHER NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getButcherName() != null && searchDetails.getButcherName().trim().length() > 0 ){
                                String butcherName = "";
                                if ( orderItemDetails.getButcherDetails() != null ){ butcherName = orderItemDetails.getButcherDetails().getAvailableName(); }
                                if ( butcherName.trim().toLowerCase().contains(searchDetails.getButcherName().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH MINIMUM & MAXIMUM AMOUNT
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && ( searchDetails.getMinAmount() != -1 || searchDetails.getMaxAmount() != -1 )){
                                if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() == -1 ){
                                    if ( orderItemDetails.getOrderDetails().getTotalAmount() >= searchDetails.getMinAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMaxAmount() > 0 && searchDetails.getMinAmount() == -1 ){
                                    if ( orderItemDetails.getOrderDetails().getTotalAmount() <= searchDetails.getMaxAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() > 0 ){
                                    if ( orderItemDetails.getOrderDetails().getTotalAmount() >= searchDetails.getMinAmount()
                                            && orderItemDetails.getOrderDetails().getTotalAmount() <= searchDetails.getMaxAmount()  ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH DISCOUNT TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDiscountType() != null && searchDetails.getDiscountType().trim().length() > 0 ){
                                if ( searchDetails.getDiscountType().equalsIgnoreCase(Constants.WITH_DISCOUNT) ){
                                    if ( orderItemDetails.getOrderDetails().getTotalDiscount() > 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getDiscountType().equalsIgnoreCase(Constants.WITHOUT_DISCOUNT) ){
                                    if ( orderItemDetails.getOrderDetails().getTotalDiscount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH PAYMENT TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getPaymentType() != null && searchDetails.getPaymentType().trim().length() > 0 ){
                                if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.CASH_ON_DELIVERY) ){
                                    if ( orderItemDetails.getOrderDetails().getTotalCashAmount() != 0
                                            && orderItemDetails.getOrderDetails().getTotalOnlinePaymentAmount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.ONLINE_PAYMENT) ){
                                    if ( orderItemDetails.getOrderDetails().getTotalOnlinePaymentAmount() != 0
                                            && orderItemDetails.getOrderDetails().getTotalCashAmount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.COD_AND_OP) ){
                                    if ( orderItemDetails.getOrderDetails().getTotalOnlinePaymentAmount() != 0
                                            && orderItemDetails.getOrderDetails().getTotalCashAmount() != 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.NEGATIVE_COD) ){
                                    if ( orderItemDetails.getOrderDetails().getTotalCashAmount() < 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH ORDRE DAY TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getOrderDayType() != null && searchDetails.getOrderDayType().trim().length() > 0 ){
                                String orderCreatedDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getCreatedDate()));
                                String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getDeliveryDate()));

                                if ( searchDetails.getOrderDayType().equalsIgnoreCase(Constants.PRE_ORDER) ){
                                    if ( DateMethods.compareDates(deliveryDate, orderCreatedDate) > 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getOrderDayType().equalsIgnoreCase(Constants.SAME_DAY_ORDER) ){
                                    if ( DateMethods.compareDates(deliveryDate, orderCreatedDate) == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH REFERRAL ORDER TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getReferralOrderType() != null && searchDetails.getReferralOrderType().trim().length() > 0 ){
                                if ( searchDetails.getReferralOrderType().equalsIgnoreCase(Constants.REFERRAL_ORDERS) ) {
                                    if (orderDetails.isReferralOrder()) {
                                        isMatched = true;
                                    } else {
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH FOLLOWUP RESULTED ORDER TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFollowupResultedOrderType() != null && searchDetails.getFollowupResultedOrderType().trim().length() > 0 ){
                                if ( searchDetails.getFollowupResultedOrderType().equalsIgnoreCase(Constants.FOLLOWUP_RESULTED_ORDERS) ) {
                                    if (orderDetails.isFollowupResultedOrder()) {
                                        isMatched = true;
                                    } else {
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH FIRST TIME ORDER TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFirstTimeOrderType() != null && searchDetails.getFirstTimeOrderType().trim().length() > 0 ){
                                if ( searchDetails.getFirstTimeOrderType().equalsIgnoreCase(Constants.FIRST_TIME_ORDERS) ) {
                                    if (orderDetails.isFirstOrder()) {
                                        isMatched = true;
                                    } else {
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH CASH-N-CARRY TYPE
                            if ((skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCashNCarryType() != null && searchDetails.getCashNCarryType().trim().length() > 0 ){
                                if ( searchDetails.getCashNCarryType().equalsIgnoreCase(Constants.CASH_N_CARRY) ){
                                    if ( orderItemDetails.isCashNCarryItem() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getCashNCarryType().equalsIgnoreCase(Constants.NOT_CASH_N_CARRY) ){
                                    if ( orderItemDetails.isCashNCarryItem() == false){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH DELAY TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDelayType() != null && searchDetails.getDelayType().trim().length() > 0 ){
                                String delayComments = orderItemDetails.getDelayedDeliveryComment();
                                if ( searchDetails.getDelayType().equalsIgnoreCase(Constants.WITH_DELAY) ){
                                    if ( delayComments != null && delayComments.trim().length() > 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getInstructionsType().equalsIgnoreCase(Constants.WITHOUT_DELAY) ){
                                    if ( delayComments == null || delayComments.trim().length() == 0){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH INSTRUCTIONS TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getInstructionsType() != null && searchDetails.getInstructionsType().trim().length() > 0 ){
                                String specialInstructions = orderItemDetails.getCombinedSpecialInstructions();
                                if ( searchDetails.getInstructionsType().equalsIgnoreCase(Constants.WITH_INSTRUCTIONS) ){
                                    if ( specialInstructions != null && specialInstructions.trim().length() > 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getInstructionsType().equalsIgnoreCase(Constants.WITHOUT_INSTRUCTIONS) ){
                                    if ( specialInstructions == null || specialInstructions.trim().length() == 0){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH CREATED BY
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCreatedBy() != null && searchDetails.getCreatedBy().trim().length() > 0 ){
                                if ( searchDetails.getCreatedBy().equalsIgnoreCase(Constants.SUPER_USER) ){
                                    if ( orderDetails.getOrderCreatorName() != null ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH COMMENTS
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getComments() != null && searchDetails.getComments().trim().length() > 0 ){
                                if ( feedbackDetails.getCustomerComments().trim().toLowerCase().contains(searchDetails.getComments().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            //==========

                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                                filteredList.add(feedbackDetails);
                            }

                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, searchDetails, filteredList);
                    }
                }

            }.execute();
        }
    }


    //======  FOLLOWUPS SEARCH   =======//

    public static final void searchFollowups(final List<FollowupDetails> originalList, final SearchDetails searchDetails, final SearchFollowupsCallback callback){
        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            new AsyncTask<Void, Void, Void>() {
                List<FollowupDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i=0;i<originalList.size();i++){
                        FollowupDetails followupDetails = originalList.get(i);
                        if ( followupDetails != null ){
                            boolean skipSearch = false;
                            boolean isMatched = false;

                            // WITH DATE
                            /*if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFeedbackDate() != null && searchDetails.getFeedbackDate().trim().length() > 0 ){
                                String feedbackDate = DateMethods.getOnlyDate(followupDetails.getCreatedDate());
                                if ( DateMethods.compareDates(feedbackDate, searchDetails.getFeedbackDate()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }*/

                            // WITH CUSTOMER NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCustomerName() != null && searchDetails.getCustomerName().trim().length() > 0
                                    && followupDetails.getBuyerDetails() != null ){
                                if ( followupDetails.getBuyerName().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())
                                        || (followupDetails.getBuyerDetails().getMobileNumber() != null && followupDetails.getBuyerDetails().getMobileNumber().toLowerCase().contains(searchDetails.getCustomerName().toLowerCase()))){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH EXECUTIVE NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getProcessorName() != null && searchDetails.getProcessorName().trim().length() > 0
                                    && followupDetails.getEmployeeDetails() != null ){
                                if ( followupDetails.getEmployeeName().trim().toLowerCase().contains(searchDetails.getProcessorName().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH COMMENTS
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getComments() != null && searchDetails.getComments().trim().length() > 0 ){
                                if ( followupDetails.getComments().trim().toLowerCase().contains(searchDetails.getComments().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH FOLLOWUP TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFollowupType() != null && searchDetails.getFollowupType().trim().length() > 0 ){
                                if ( searchDetails.getFollowupType().equalsIgnoreCase(Constants.FIRST_ORDER) ) {
                                    if (followupDetails.getType().equalsIgnoreCase(Constants.FIRST_ORDER_FOLLOWUP)) {
                                        isMatched = true;
                                    } else {
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH FIRST ORDER
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getFollowupResultedOrderType() != null && searchDetails.getFollowupResultedOrderType().trim().length() > 0 ){
                                if ( searchDetails.getFollowupResultedOrderType().equalsIgnoreCase(Constants.FOLLOWUP_RESULTED_ORDERS) ) {
                                    if (followupDetails.isResultedInOrder()) {
                                        isMatched = true;
                                    } else {
                                        skipSearch = true;
                                    }
                                }
                            }

                            //==========

                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                                filteredList.add(followupDetails);
                            }

                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, searchDetails, filteredList);
                    }
                }

            }.execute();
        }
    }


    //======  MONEY COLLECTION SEARCH   =======//

    public static final void searchMoneyCollections(final List<MoneyCollectionDetails> originalList, final SearchDetails searchDetails, final SearchMoneyCollectionsCallback callback){

        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            new AsyncTask<Void, Void, Void>() {
                List<MoneyCollectionDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i=0;i<originalList.size();i++){
                        MoneyCollectionDetails moneyCollectionDetails = originalList.get(i);
                        if ( moneyCollectionDetails != null && moneyCollectionDetails.getOrderDetails() != null ){
                            OrderDetails orderDetails = moneyCollectionDetails.getOrderDetails();

                            boolean skipSearch = false;
                            boolean isMatched = false;

                            // WITH TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getMoneyCollectionType() != null && searchDetails.getMoneyCollectionType().trim().length() > 0 ){
                                if ( searchDetails.getMoneyCollectionType().equalsIgnoreCase(Constants.ORDER) ){
                                    if ( moneyCollectionDetails.getType().equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_ORDER) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMoneyCollectionType().equalsIgnoreCase(Constants.OUTSTANDING) ){
                                    if ( moneyCollectionDetails.getType().equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_OUTSTANDING) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH DELIVERY DATE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryDate() != null && searchDetails.getDeliveryDate().trim().length() > 0 ){
                                String deliveryDate = DateMethods.getOnlyDate(orderDetails.getDeliveryDate());
                                if ( DateMethods.compareDates(deliveryDate, searchDetails.getDeliveryDate()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH COLLECTION DATE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCollectionDate() != null && searchDetails.getCollectionDate().trim().length() > 0 ){
                                boolean isAnyTransactionMatched = false;
                                for (int t=0;t<moneyCollectionDetails.getTransactions().size();t++){
                                    if ( DateMethods.compareDates(DateMethods.getReadableDateFromUTC(moneyCollectionDetails.getTransactions().get(t).getCollectionDate(), DateConstants.DD_MM_YYYY), searchDetails.getCollectionDate()) == 0 ){
                                        isAnyTransactionMatched = true;
                                        break;
                                    }
                                }
                                if ( isAnyTransactionMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH CONSOLIDATION DATE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getConsolidationDate() != null && searchDetails.getConsolidationDate().trim().length() > 0 ){
                                boolean isAnyTransactionMatched = false;
                                for (int t=0;t<moneyCollectionDetails.getTransactions().size();t++){
                                    if ( DateMethods.compareDates(DateMethods.getReadableDateFromUTC(moneyCollectionDetails.getTransactions().get(t).getConsolidationDate(), DateConstants.DD_MM_YYYY), searchDetails.getConsolidationDate()) == 0 ){
                                        isAnyTransactionMatched = true;
                                        break;
                                    }
                                }
                                if ( isAnyTransactionMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH STATUS
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && (searchDetails.getStatus() != null && searchDetails.getStatus().trim().length() > 0) ){
                                if ( searchDetails.getStatus().equalsIgnoreCase(Constants.PENDING) ){
                                    if ( moneyCollectionDetails.isConsolidated() == false ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }else if ( searchDetails.getStatus().equalsIgnoreCase(Constants.CONSOLIDATED) ){
                                    if ( moneyCollectionDetails.isConsolidated() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }

                            }

                            // WITH CUSTOMER NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCustomerName() != null && searchDetails.getCustomerName().trim().length() > 0 ){
                                if ( orderDetails.getCustomerName().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())
                                        || orderDetails.getCustomerMobile().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())
                                        || orderDetails.getCustomerAlternateMobile().trim().toLowerCase().contains(searchDetails.getCustomerName().trim().toLowerCase())){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH CONSOLIDATOR NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getConsolidatorName() != null && searchDetails.getConsolidatorName().trim().length() > 0 ){
                                boolean isAnyTransactionMatched = false;
                                for (int t=0;t<moneyCollectionDetails.getTransactions().size();t++){
                                    String consolidatorName = "";
                                    try{ consolidatorName = moneyCollectionDetails.getTransactions().get(t).getConsolidatorDetails().getAvailableName(); }catch (Exception e){}
                                    if ( consolidatorName.trim().toLowerCase().contains(searchDetails.getConsolidatorName().trim().toLowerCase()) ){
                                        isAnyTransactionMatched = true;
                                        break;
                                    }
                                }
                                if ( isAnyTransactionMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH DELIVERYBOY NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDeliveryBoyName() != null && searchDetails.getDeliveryBoyName().trim().length() > 0 ){
                                boolean isAnyTransactionMatched = false;
                                for (int t=0;t<moneyCollectionDetails.getTransactions().size();t++){
                                    String deliveryBoyName = "";
                                    try{ deliveryBoyName = moneyCollectionDetails.getTransactions().get(t).getDeliveryBoyDetails().getAvailableName(); }catch (Exception e){}
                                    if ( deliveryBoyName.trim().toLowerCase().contains(searchDetails.getDeliveryBoyName().trim().toLowerCase()) ){
                                        isAnyTransactionMatched = true;
                                        break;
                                    }
                                }
                                if ( isAnyTransactionMatched ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH COLLECTION TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCollectionType() != null && searchDetails.getCollectionType().trim().length() > 0 ){
                                if ( searchDetails.getCollectionType().equalsIgnoreCase(Constants.COLLECT) ){
                                    if ( orderDetails.getTotalCashAmount() >= 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getCollectionType().equalsIgnoreCase(Constants.RETURN) ){
                                    if ( orderDetails.getTotalCashAmount() < 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH MATCH TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getMatchType() != null && searchDetails.getMatchType().trim().length() > 0 ){
                                if ( searchDetails.getMatchType().equalsIgnoreCase(Constants.MATCHED) ){
                                    if ( Math.abs(orderDetails.getTotalAmount() - moneyCollectionDetails.getCollectedAmount()) < 1 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMatchType().equalsIgnoreCase(Constants.MIS_MATCHED) ){
                                    if ( Math.abs(orderDetails.getTotalAmount() - moneyCollectionDetails.getCollectedAmount()) >= 1 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH MINIMUM & MAXIMUM AMOUNT
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && ( searchDetails.getMinAmount() != -1 || searchDetails.getMaxAmount() != -1 )){
                                if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() == -1 ){
                                    if ( orderDetails.getTotalAmount() >= searchDetails.getMinAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMaxAmount() > 0 && searchDetails.getMinAmount() == -1 ){
                                    if ( orderDetails.getTotalAmount() <= searchDetails.getMaxAmount() ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getMinAmount() > 0 && searchDetails.getMaxAmount() > 0 ){
                                    if ( orderDetails.getTotalAmount() >= searchDetails.getMinAmount()
                                            && orderDetails.getTotalAmount() <= searchDetails.getMaxAmount()  ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH DISCOUNT TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getDiscountType() != null && searchDetails.getDiscountType().trim().length() > 0 ){
                                if ( searchDetails.getDiscountType().equalsIgnoreCase(Constants.WITH_DISCOUNT) ){
                                    if ( orderDetails.getTotalDiscount() > 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getDiscountType().equalsIgnoreCase(Constants.WITHOUT_DISCOUNT) ){
                                    if ( orderDetails.getTotalDiscount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH PAYMENT TYPE
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getPaymentType() != null && searchDetails.getPaymentType().trim().length() > 0 ){
                                if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.CASH_ON_DELIVERY) ){
                                    if ( orderDetails.getTotalCashAmount() != 0
                                            && orderDetails.getTotalOnlinePaymentAmount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.ONLINE_PAYMENT) ){
                                    if ( orderDetails.getTotalOnlinePaymentAmount() != 0
                                            && orderDetails.getTotalCashAmount() == 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.COD_AND_OP) ){
                                    if ( orderDetails.getTotalOnlinePaymentAmount() != 0
                                            && orderDetails.getTotalCashAmount() != 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                                else if ( searchDetails.getPaymentType().equalsIgnoreCase(Constants.NEGATIVE_COD) ){
                                    if ( orderDetails.getTotalCashAmount() < 0 ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH COMMENTS
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getComments() != null && searchDetails.getComments().trim().length() > 0 && moneyCollectionDetails.getComments() != null ){
                                if ( moneyCollectionDetails.getComments().trim().toLowerCase().contains(searchDetails.getComments().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            //==========

                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                                filteredList.add(moneyCollectionDetails);
                            }

                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, searchDetails, filteredList);
                    }
                }

            }.execute();
        }
    }


    //======  STOCKS SEARCH   =======//

    public static final void searchStocks(final List<StockDetails> originalList, final SearchDetails searchDetails, final SearchStocksCallback callback){

        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            new AsyncTask<Void, Void, Void>() {
                List<StockDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i=0;i<originalList.size();i++){
                        StockDetails stockDetails = originalList.get(i);
                        if ( stockDetails != null ){
                            boolean skipSearch = false;
                            boolean isMatched = false;


                            // WITH DATE
                            if ( skipSearch == false && searchDetails.getDeliveryDate() != null && searchDetails.getDeliveryDate().trim().length() > 0 ){
                                String date = DateMethods.getReadableDateFromUTC(stockDetails.getDate(), DateConstants.DD_MM_YYYY);
                                if ( DateMethods.compareDates(date, searchDetails.getDeliveryDate()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH CATEOGRY
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCategoryType() != null && searchDetails.getCategoryType().trim().length() > 0 ){
                                CategoryDetails categoryDetails = null;
                                if ( stockDetails.getCategoryDetails() != null ){
                                    categoryDetails = stockDetails.getCategoryDetails();
                                }else if ( stockDetails.getWarehouseMeatItemDetails() != null && stockDetails.getWarehouseMeatItemDetails().getMeatItemDetais() != null && stockDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getCategoryDetails() != null ){
                                    categoryDetails = stockDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getCategoryDetails();
                                }
                                if ( categoryDetails != null ){
                                    if ( categoryDetails.getParentCategoryName().replaceAll("\\s+", "").equalsIgnoreCase(searchDetails.getCategoryType()) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH STOCK TYPE
                            if ( skipSearch == false && searchDetails.getStockType() != null && searchDetails.getStockType().trim().length() > 0 ){
                                if ( searchDetails.getStockType().equalsIgnoreCase(Constants.INPUT) ){
                                    if ( stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_INPUT) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }else if ( searchDetails.getStockType().equalsIgnoreCase(Constants.PURCHASE) ) {
                                    if (stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_PURCHASE) || stockDetails.getType().equalsIgnoreCase(Constants.CATEGORY_PURCHASE)) {
                                        isMatched = true;
                                    } else {
                                        skipSearch = true;
                                    }
                                }else if ( searchDetails.getStockType().equalsIgnoreCase(Constants.CONSOLIDATION) ){
                                    if ( stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_CONSOLIDATION) || stockDetails.getType().equalsIgnoreCase(Constants.CATEGORY_CONSOLIDATION) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }else if ( searchDetails.getStockType().equalsIgnoreCase(Constants.ADJUSTMENT) ){
                                    if ( stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT) || stockDetails.getType().equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }else if ( searchDetails.getStockType().equalsIgnoreCase(Constants.WASTAGE) ){
                                    if ( stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_WASTAGE) || stockDetails.getType().equalsIgnoreCase(Constants.HUB_MEAT_ITEM_WASTAGE) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }else if ( searchDetails.getStockType().equalsIgnoreCase(Constants.ORDER) ){
                                    if ( stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_ORDER) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }else if ( searchDetails.getStockType().equalsIgnoreCase(Constants.NOT_ORDER) ){
                                    if ( stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_ORDER) == false ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH ITEM NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getItemName() != null && searchDetails.getItemName().trim().length() > 0 ){
                                String itemName = "";
                                if ( stockDetails.getCategoryDetails() != null ){
                                    itemName = stockDetails.getCategoryDetails().getName();
                                }else if ( stockDetails.getWarehouseMeatItemDetails() != null && stockDetails.getWarehouseMeatItemDetails().getMeatItemDetais() != null ){
                                    itemName = stockDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getMainName();
                                }
                                if ( itemName.toLowerCase().contains(searchDetails.getItemName().trim().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH VENDOR NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getVendorName() != null && searchDetails.getVendorName().trim().length() > 0 ){
                                if ( stockDetails.getVendorDetails() != null ){
                                    if ( stockDetails.getVendorDetails().getName().trim().toLowerCase().contains(searchDetails.getVendorName().trim().toLowerCase()) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            // WITH LINKED STOCK
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getStockID() != null && searchDetails.getStockID().trim().length() > 0 ){
                                if ( stockDetails.getLinkedStock() != null || stockDetails.getLinkedProcessedStock() != null ){
                                    if ( (stockDetails.getLinkedStock() != null && stockDetails.getLinkedStock().toLowerCase().contains(searchDetails.getStockID().toLowerCase()))
                                            || (stockDetails.getLinkedProcessedStock() != null && stockDetails.getLinkedProcessedStock().toLowerCase().contains(searchDetails.getStockID().toLowerCase())) ){
                                        isMatched = true;
                                    }else{
                                        skipSearch = true;
                                    }
                                }
                            }

                            //==========

                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                                filteredList.add(stockDetails);
                            }

                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, searchDetails, filteredList);
                    }
                }

            }.execute();
        }
    }


    public static final void searchProcessedStocks(final List<ProcessedStockDetails> originalList, final SearchDetails searchDetails, final SearchProcessedStocksCallback callback){

        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            new AsyncTask<Void, Void, Void>() {
                List<ProcessedStockDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i=0;i<originalList.size();i++){
                        ProcessedStockDetails stockDetails = originalList.get(i);
                        if ( stockDetails != null ){
                            boolean skipSearch = false;
                            boolean isMatched = false;


                            // WITH DATE
                            if ( skipSearch == false && searchDetails.getDeliveryDate() != null && searchDetails.getDeliveryDate().trim().length() > 0 ){
                                String date = DateMethods.getOnlyDate(stockDetails.getDate());
                                if ( DateMethods.compareDates(date, searchDetails.getDeliveryDate()) == 0 ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH STOCK TYPE
                            if ( skipSearch == false && searchDetails.getStockType() != null && searchDetails.getStockType().trim().length() > 0 ){
                                if ( stockDetails.getType().toLowerCase().contains(searchDetails.getStockType().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            //==========

                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                                filteredList.add(stockDetails);
                            }

                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, searchDetails, filteredList);
                    }
                }
            }.execute();
        }
    }


    public static final void searchBonuses(final List<EmployeeBonusDetails> originalList, final SearchDetails searchDetails, final SearchEmployeeBonusesCallback callback){

        if ( originalList != null && originalList.size() > 0 && searchDetails != null && searchDetails.isSearchable() ){
            new AsyncTask<Void, Void, Void>() {
                List<EmployeeBonusDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i=0;i<originalList.size();i++){
                        EmployeeBonusDetails bonusDetails = originalList.get(i);
                        if ( bonusDetails != null ){
                            boolean skipSearch = false;
                            boolean isMatched = false;

                            // WITH EMPLOYEE NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getEmployeeName() != null && searchDetails.getEmployeeName().trim().length() > 0 ){
                                if ( bonusDetails.getEmployeeDetails().getAvailableName().toLowerCase().contains(searchDetails.getEmployeeName().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            // WITH CUSTOMER NAME
                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && searchDetails.getCustomerName() != null && searchDetails.getCustomerName().trim().length() > 0 ){
                                if ( bonusDetails.getBuyerDetails().getAvailableName().toLowerCase().contains(searchDetails.getCustomerName().toLowerCase())
                                        || bonusDetails.getOrderDetails().getCustomerName().toLowerCase().contains(searchDetails.getCustomerName().toLowerCase()) ){
                                    isMatched = true;
                                }else{
                                    skipSearch = true;
                                }
                            }

                            //==========

                            if ( (skipSearch == false || searchDetails.isConsiderAtleastOneMatch()) && isMatched == true ){
                                filteredList.add(bonusDetails);
                            }
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ) {
                        callback.onComplete(true, searchDetails, filteredList);
                    }
                }
            }.execute();
        }
    }

}
