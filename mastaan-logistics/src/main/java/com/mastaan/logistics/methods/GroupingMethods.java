package com.mastaan.logistics.methods;

import android.os.AsyncTask;

import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.interfaces.OrdersListCallback;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.GroupedFeedbacksList;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.GroupedOrdersList;
import com.mastaan.logistics.models.MoneyCollectionDetails;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by venkatesh on 16/5/16.
 */
public final class GroupingMethods {

    public interface GroupedOrdersListCallback{
        void onComplete(List<GroupedOrdersList> groupedOrdersLists);
    }

    public interface GroupedOrderItemsListCallback{
        void onComplete(List<GroupedOrdersItemsList> groupedOrdersItemsLists);
    }

    public interface GroupAndSortOrderItemsByCategoryCallback{
        void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsLists);
    }

    public interface GroupAndSortOrdersByStatusCallback{
        void onComplete(boolean status, List<GroupedOrdersList> groupedOrdersLists);
    }

    //========== ORDERS ============//

    public static final void sortOrders(final String serverTime, final List<OrderDetails> itemsList, final LatLng latLng, final OrdersListCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            List<OrderDetails> ordersList = new ArrayList<OrderDetails>();
            @Override
            protected Void doInBackground(Void... params) {
                ordersList = sortOrders(serverTime, itemsList, latLng);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ) {
                    callback.onComplete(true, 200, "", ordersList);
                }
            }
        }.execute();
    }

    public static final List<OrderDetails> sortOrders(String serverTime, List<OrderDetails> itemsList, final LatLng latLng) {

        List<OrderDetails> sortedList = new ArrayList<>();

        Map<String, GroupedOrdersList> groupByDatekeyMapSet = new HashMap<>();
        for (int i=0;i<itemsList.size();i++){
            final OrderDetails orderDetails = itemsList.get(i);
            if ( orderDetails != null && orderDetails.getDeliveryDate() != null ){
                if ( groupByDatekeyMapSet.containsKey(orderDetails.getDeliveryDate())) {
                    groupByDatekeyMapSet.get(orderDetails.getDeliveryDate()).addItem(orderDetails);
                }
                else {
                    GroupedOrdersList groupedOrdersList = new GroupedOrdersList();
                    groupedOrdersList.setCategoryName(orderDetails.getDeliveryDate());
                    groupedOrdersList.addItem(orderDetails);
                    groupByDatekeyMapSet.put(orderDetails.getDeliveryDate(), groupedOrdersList);
                }
            }
        }

        List<GroupedOrdersList> groupedOrdersLists = new ArrayList<>();
        for (String key : groupByDatekeyMapSet.keySet()) {
            groupedOrdersLists.add(groupByDatekeyMapSet.get(key));
        }

        // SORTING GROUPS BY DATE
        Collections.sort(groupedOrdersLists, new Comparator<GroupedOrdersList>() {
            public int compare(GroupedOrdersList item1, GroupedOrdersList item2) {
                return item1.getCategoryName().compareToIgnoreCase(item2.getCategoryName());
            }
        });

        for (int i=0;i< groupedOrdersLists.size();i++) {
            //Toast.makeText(context, DateMethods.getTimeIn12HrFormat(DateMethods.getReadableDateFromUTC(groupedOrdersLists.get(i).getCategoryName())), Toast.LENGTH_LONG).show();
            List<OrderDetails> list = groupedOrdersLists.get(i).getItems();

            // SORTING ITEMS IN GROUP BY DISTANCE
            if ( latLng != null && latLng.latitude != 0 && latLng.longitude != 0 ) {
                Collections.sort(list, new Comparator<OrderDetails>() {
                    public int compare(OrderDetails item1, OrderDetails item2) {
                        float distanceTo2 = LatLngMethods.getDistanceBetweenLatLng(item1.getDeliveryAddressLatLng(), latLng);
                        float distanceTo1 = LatLngMethods.getDistanceBetweenLatLng(item2.getDeliveryAddressLatLng(), latLng);

                        if (distanceTo1 > distanceTo2) {
                            return 1;
                        } else if (distanceTo2 > distanceTo1) {
                            return -1;
                        }
                        return 0;
                    }
                });
            }

            // SORTING ITEMS IN GROUP BY PRE ORDERS
            /*String serverDate = DateMethods.getOnlyDate(serverTime);
            if ( serverDate != null && serverDate.length() > 0 ){
                List<OrderDetails> noPreOrderItems = new ArrayList<>();
                List<OrderDetails> preOrderItems = new ArrayList<>();
                //List<OrderDetails> failedItems = new ArrayList<>();
                for (int z=0;z<list.size();z++){
                    OrderDetails orderDetails = list.get(z);
                    String createdDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()));
                    if ( DateMethods.compareDates(serverDate, createdDate) > 0 ){
                        preOrderItems.add(orderDetails);
                    }else{
                        noPreOrderItems.add(orderDetails);
                    }
                }

                list = new ArrayList<>();
                for (int z=0;z<preOrderItems.size();z++){
                    list.add(preOrderItems.get(z));
                }
                for (int z=0;z<noPreOrderItems.size();z++){
                    list.add(noPreOrderItems.get(z));
                }
                //for (int z=0;z<failedItems.size();z++){
                //    list.add(failedItems.get(z));
                //}
            }*/

            // ADDING TO OUTPUT LIST
            for (int k=0;k<list.size();k++){
                sortedList.add(list.get(k));
            }
        }

        //-----

        return sortedList;
    }


    public static final void groupAndSortOrdersByStatus(final String serverTime, final List<OrderDetails> items, final LatLng location, final GroupAndSortOrdersByStatusCallback callback) {
        groupAndSortOrdersByStatus(serverTime, items, null, location, callback);
    }
    public static final void groupAndSortOrdersByStatus(final String serverTime, final List<OrderDetails> items, final String deliveryBoyID, final LatLng location, final GroupAndSortOrdersByStatusCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            List<GroupedOrdersList> groupedOrdersLists;
            @Override
            protected Void doInBackground(Void... params) {
                groupedOrdersLists = groupAndSortOrdersByStatus(serverTime, items, deliveryBoyID, location);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, groupedOrdersLists);
                }
            }
        }.execute();
    }
    public static final List<GroupedOrdersList> groupAndSortOrdersByStatus(final String serverTime, List<OrderDetails> items, LatLng location) {
        return groupAndSortOrdersByStatus(serverTime, items, null, location);
    }
    public static final List<GroupedOrdersList> groupAndSortOrdersByStatus(final String serverTime, List<OrderDetails> items, String deliveryBoyID, LatLng location) {

        List<GroupedOrdersList> groupedOrdersLists = new ArrayList<>();

        Map<String, GroupedOrdersList> keyMapSet = new HashMap<>();
        for (int i=0;i<items.size();i++){
            final OrderDetails orderDetails = items.get(i);
            if ( orderDetails != null ){
                String orderStatus = orderDetails.getStatus();
                if ( deliveryBoyID != null && deliveryBoyID.length() > 0 && orderDetails.getStatus().equalsIgnoreCase(Constants.PENDING)){
                    orderStatus = Constants.DELIVERED;
                    for (int x=0;x<orderDetails.getOrderedItems().size();x++){
                        OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(x);
                        if ( orderItemDetails.getDeliveryBoyID().equals(deliveryBoyID)
                                && (orderItemDetails.getStatusString().equalsIgnoreCase(Constants.ASSIGNED)||orderItemDetails.getStatusString().equalsIgnoreCase(Constants.PICKED_UP)||orderItemDetails.getStatusString().equalsIgnoreCase(Constants.DELIVERING)) ){
                            orderStatus = Constants.PENDING;
                            break;
                        }
                    }
                }
                orderStatus = orderStatus.toLowerCase();

                if ( keyMapSet.containsKey(orderStatus) ) {
                    keyMapSet.get(orderStatus).addItem(orderDetails);
                }
                else {
                    GroupedOrdersList groupedOrdersList = new GroupedOrdersList(orderStatus, new ArrayList<OrderDetails>());
                    groupedOrdersList.addItem(orderDetails);
                    keyMapSet.put(orderStatus, groupedOrdersList);
                }
            }
        }

        for (String key : keyMapSet.keySet()) {
            groupedOrdersLists.add(keyMapSet.get(key));
        }//
        List<GroupedOrdersList> sGropuedList = new ArrayList<>();
        for (int i=0;i<30;i++){
            sGropuedList.add(null);
        }
        for (int i=0;i<groupedOrdersLists.size();i++){
            if ( groupedOrdersLists.get(i).getCategoryName().equalsIgnoreCase(Constants.PENDING) ){
                sGropuedList.add(0, groupedOrdersLists.get(i));
            }
            else if ( groupedOrdersLists.get(i).getCategoryName().equalsIgnoreCase(Constants.DELIVERED) ){
                sGropuedList.add(1, groupedOrdersLists.get(i));
            }
            else if ( groupedOrdersLists.get(i).getCategoryName().equalsIgnoreCase(Constants.FAILED) ){
                sGropuedList.add(2, groupedOrdersLists.get(i));
            }else{
                sGropuedList.add(groupedOrdersLists.get(i));
            }
        }

        List<GroupedOrdersList> sortedGropuedList = new ArrayList<>();
        for (int i=0;i<sGropuedList.size();i++){
            if ( sGropuedList.get(i) != null ){
                sortedGropuedList.add(sGropuedList.get(i));
            }
        }

        for (int i=0;i<sortedGropuedList.size();i++){
            sortedGropuedList.get(i).setItems(sortOrders(serverTime, sortedGropuedList.get(i).getItems(), location));
        }

        return sortedGropuedList;
    }


    //========== ORDER ITEMS ============//

    public static final void sortOrderItems(final String serverTime, final List<OrderItemDetails> itemsList, final LatLng latLng, final OrderItemsListCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> orderItemsList = new ArrayList<OrderItemDetails>();
            @Override
            protected Void doInBackground(Void... params) {
                orderItemsList = sortOrderItems(serverTime, itemsList, latLng);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ) {
                    callback.onComplete(true, 200, "", orderItemsList);
                }
            }
        }.execute();
    }

    public static final List<OrderItemDetails> sortOrderItems(String serverTime, List<OrderItemDetails> itemsList, final LatLng latLng) {

        List<OrderItemDetails> sortedList = new ArrayList<>();

        Map<String, GroupedOrdersItemsList> groupByDatekeyMapSet = new HashMap<>();
        for (int i=0;i<itemsList.size();i++){
            final OrderItemDetails orderItemDetails = itemsList.get(i);
            if ( orderItemDetails != null && orderItemDetails.getPrepareByDate() != null ){
                if ( groupByDatekeyMapSet.containsKey(orderItemDetails.getPrepareByDate())) {
                    groupByDatekeyMapSet.get(orderItemDetails.getPrepareByDate()).addItem(orderItemDetails);
                }
                else {
                    GroupedOrdersItemsList groupedOrdersItemsList = new GroupedOrdersItemsList();
                    groupedOrdersItemsList.setCategoryName(orderItemDetails.getPrepareByDate());
                    groupedOrdersItemsList.addItem(orderItemDetails);
                    groupByDatekeyMapSet.put(orderItemDetails.getPrepareByDate(), groupedOrdersItemsList);
                }
            }
        }

        List<GroupedOrdersItemsList> groupedOrdersItemsLists = new ArrayList<>();
        for (String key : groupByDatekeyMapSet.keySet()) {
            groupedOrdersItemsLists.add(groupByDatekeyMapSet.get(key));
        }

        // SORTING GROUPS BY PREPARE BY DATE
        Collections.sort(groupedOrdersItemsLists, new Comparator<GroupedOrdersItemsList>() {
            public int compare(GroupedOrdersItemsList item1, GroupedOrdersItemsList item2) {
                //return DateMethods.compareDates(item1.getCategoryName(), item2.getCategoryName());/* Use if Date in 12 hr format*/
                return item1.getCategoryName().compareToIgnoreCase(item2.getCategoryName());/*As Dates in 24 hr format*/
            }
        });

        for (int i=0;i< groupedOrdersItemsLists.size();i++) {
            //Toast.makeText(context, DateMethods.getTimeIn12HrFormat(DateMethods.getReadableDateFromUTC(groupedOrdersItemsLists.get(i).getCategoryName())), Toast.LENGTH_LONG).show();
            List<OrderItemDetails> list = groupedOrdersItemsLists.get(i).getItems();

            // SORTING ITEMS IN GROUP BY DISTANCE
            if ( latLng != null && latLng.latitude != 0 && latLng.longitude != 0 ) {
                Collections.sort(list, new Comparator<OrderItemDetails>() {
                    public int compare(OrderItemDetails item1, OrderItemDetails item2) {
                        float distanceTo2 = LatLngMethods.getDistanceBetweenLatLng(item1.getOrderDetails().getDeliveryAddressLatLng(), latLng);
                        float distanceTo1 = LatLngMethods.getDistanceBetweenLatLng(item2.getOrderDetails().getDeliveryAddressLatLng(), latLng);

                        if (distanceTo1 > distanceTo2) {
                            return 1;
                        } else if (distanceTo2 > distanceTo1) {
                            return -1;
                        }
                        return 0;
                    }
                });
            }

            // SORTING ITEMS IN GROUP BY PRE ORDERS
            /*String serverDate = DateMethods.getOnlyDate(serverTime);
            if ( serverDate != null && serverDate.length() > 0 ){
                List<OrderItemDetails> noPreOrderItems = new ArrayList<>();
                List<OrderItemDetails> preOrderItems = new ArrayList<>();
                List<OrderItemDetails> failedItems = new ArrayList<>();
                for (int z=0;z<list.size();z++){
                    OrderItemDetails orderItemDetails = list.get(z);
                    if ( orderItemDetails.getStatus() != 5 && orderItemDetails.getStatus() != 6 ){
                        String createdDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getCreatedDate()));
                        if ( DateMethods.compareDates(serverDate, createdDate) > 0 ){
                            preOrderItems.add(orderItemDetails);
                        }else{
                            noPreOrderItems.add(orderItemDetails);
                        }
                    }else{
                        failedItems.add(orderItemDetails);
                    }
                }

                list = new ArrayList<>();
                for (int z=0;z<preOrderItems.size();z++){
                    list.add(preOrderItems.get(z));
                }
                for (int z=0;z<noPreOrderItems.size();z++){
                    list.add(noPreOrderItems.get(z));
                }
                for (int z=0;z<failedItems.size();z++){
                    list.add(failedItems.get(z));
                }
            }*/


            // ADDING TO OUTPUT LIST
            for (int k=0;k<list.size();k++){
                sortedList.add(list.get(k));
            }
        }

        //-----

        return sortedList;
    }

    public static final void groupAndSortOrderItemsByCategory(final String serverTime, final List<OrderItemDetails> items, final LatLng location, final GroupedOrderItemsListCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            List<GroupedOrdersItemsList> groupedList = new ArrayList<GroupedOrdersItemsList>();
            @Override
            protected Void doInBackground(Void... params) {
                groupedList = groupAndSortOrderItemsByCategory(serverTime, items, location);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(groupedList);
                }
            }
        }.execute();
    }



    public static final void groupAndSortOrderItemsByCategory(final String serverTime, final List<OrderItemDetails> items, final LatLng location, final GroupAndSortOrderItemsByCategoryCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            List<GroupedOrdersItemsList> groupedOrdersItemsLists;
            @Override
            protected Void doInBackground(Void... params) {
                groupedOrdersItemsLists = groupAndSortOrderItemsByCategory(serverTime, items, location);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, groupedOrdersItemsLists);
                }
            }
        }.execute();
    }

    public static final List<GroupedOrdersItemsList> groupAndSortOrderItemsByCategory(final List<OrderItemDetails> items) {
        return groupAndSortOrderItemsByCategory(null, items, null);
    }

    public static final List<GroupedOrdersItemsList> groupAndSortOrderItemsByCategory(final String serverTime, List<OrderItemDetails> items, LatLng location) {

        List<GroupedOrdersItemsList> groupedOrdersItemsLists = new ArrayList<>();

        Map<String, GroupedOrdersItemsList> keyMapSet = new HashMap<>();
        for (int i=0;i<items.size();i++){
            final OrderItemDetails orderItemDetails = items.get(i);
            if ( orderItemDetails != null ){
                if ( keyMapSet.containsKey(orderItemDetails.getMeatItemDetails().getCategoryDetails().getParentCategoryName()) ) {
                    keyMapSet.get(orderItemDetails.getMeatItemDetails().getCategoryDetails().getParentCategoryName()).addItem(orderItemDetails);
                }
                else {
                    GroupedOrdersItemsList groupedOrdersItemsList = new GroupedOrdersItemsList(orderItemDetails.getMeatItemDetails().getCategoryDetails().getParentCategoryName(), new ArrayList<OrderItemDetails>());
                    keyMapSet.put(orderItemDetails.getMeatItemDetails().getCategoryDetails().getParentCategoryName(), groupedOrdersItemsList);
                    keyMapSet.get(orderItemDetails.getMeatItemDetails().getCategoryDetails().getParentCategoryName()).addItem(orderItemDetails);
                }
            }
        }

        for (String key : keyMapSet.keySet()) {
            groupedOrdersItemsLists.add(keyMapSet.get(key));
        }

        Collections.sort(groupedOrdersItemsLists, new Comparator<GroupedOrdersItemsList>() {
            public int compare(GroupedOrdersItemsList item1, GroupedOrdersItemsList item2) {
                return item1.getCategoryName().compareToIgnoreCase(item2.getCategoryName());
            }
        });

        for (String key : keyMapSet.keySet()) {
            keyMapSet.get(key).setItems(sortOrderItems(serverTime, keyMapSet.get(key).getItems(), location));
        }

        return groupedOrdersItemsLists;
    }


    //================================

    public static final List<OrderItemDetails> getOrderItemsListFromGroups(List<GroupedOrdersItemsList> groupedList){
        List<OrderItemDetails> orderItemsList = new ArrayList<>();
        if ( groupedList != null ) {
            for (int i = 0; i < groupedList.size(); i++) {
                for (int j = 0; j < groupedList.get(i).getItems().size(); j++) {
                    orderItemsList.add(groupedList.get(i).getItems().get(j));
                }
            }
        }

        return orderItemsList;
    }

    //----------

    public static final void getOrderItemsFromOrders(final List<OrderDetails> ordersList, final OrderItemsListCallback callback){

        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> orderItemsList = new ArrayList<OrderItemDetails>();
            @Override
            protected Void doInBackground(Void... params) {
                if ( ordersList != null && ordersList.size() > 0 ){
                    for (int i=0;i<ordersList.size();i++){
                        OrderDetails orderDetails = ordersList.get(i);
                        List<OrderItemDetails> orderItems = orderDetails.getOrderedItems();
                        if ( orderDetails != null ){
                            for (int j=0;j<orderItems.size();j++){
                                orderItemsList.add(orderItems.get(j).setOrderDetails(new OrderDetails2(orderDetails)));
                            }
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success", orderItemsList);
                }

            }
        }.execute();
    }

    public static final List<OrderItemDetails> getOrderItemsFromOrders(final List<OrderDetails> ordersList){
        List<OrderItemDetails> orderItemsList = new ArrayList<OrderItemDetails>();
        if ( ordersList != null && ordersList.size() > 0 ){
            for (int i=0;i<ordersList.size();i++){
                OrderDetails orderDetails = ordersList.get(i);
                List<OrderItemDetails> orderItems = orderDetails.getOrderedItems();
                if ( orderDetails != null ){
                    for (int j=0;j<orderItems.size();j++){
                        orderItemsList.add(orderItems.get(j).setOrderDetails(new OrderDetails2(orderDetails)));
                    }
                }
            }
        }
        return orderItemsList;
    }

    public static final void getOrdersFromOrderItems(final List<OrderItemDetails> orderItemsList, final OrdersListCallback callback){

        new AsyncTask<Void, Void, Void>() {
            List<OrderDetails> ordersList = new ArrayList<OrderDetails>();
            @Override
            protected Void doInBackground(Void... params) {
                if ( orderItemsList != null && orderItemsList.size() > 0 ){
                    Map<String, OrderDetails> ordersKeyMapSet = new HashMap<>();

                    for (int i=0;i<orderItemsList.size();i++){
                        OrderItemDetails orderItemDetails = orderItemsList.get(i);

                        if ( ordersKeyMapSet.containsKey(orderItemDetails.getOrderID()) == false ) {
                            ordersKeyMapSet.put(orderItemDetails.getOrderID(), new OrderDetails(orderItemDetails.getOrderDetails()));
                        }
                        ordersKeyMapSet.get(orderItemDetails.getOrderID()).addOrderItem(orderItemDetails);//.setOrderDetails(new OrderDetails2()));
                    }

                    for (String key : ordersKeyMapSet.keySet()) {
                        ordersList.add(ordersKeyMapSet.get(key));
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success", ordersList);
                }

            }
        }.execute();
    }

    public interface MoneyCollectionStatusStringsCallback{
        void onComplete(boolean status, List<String> statusStrings);
    }
    public static final void getMoneyCollectionsStatusStrings(final List<MoneyCollectionDetails> itemsList, final MoneyCollectionStatusStringsCallback callback){
        new AsyncTask<Void, Void, Void>() {
            List<String> statusStrings = new ArrayList<String>();
            @Override
            protected Void doInBackground(Void... voids) {
                if ( itemsList != null && itemsList.size() > 0 ) {
                    for (int i = 0; i < itemsList.size(); i++) {
                        String statusString = Constants.PENDING;
                        if ( itemsList.get(i).isConsolidated() ){
                            statusString = Constants.CONSOLIDATED;
                        }
                        if ( statusStrings.contains(statusString) == false ){
                            statusStrings.add(statusString);
                        }
                    }

                    Collections.sort(statusStrings, new Comparator<String>() {
                        public int compare(String item1, String item2) {
                            return item2.compareToIgnoreCase(item1);
                        }
                    });
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                for (int i=0;i<statusStrings.size();i++){
//                    if ( statusStrings.get(i).equalsIgnoreCase(Constants.WITH_DELIVERBOY) ){
//                        statusStrings.set(i, Constants.PENDING);
//                    }else if ( statusStrings.get(i).equalsIgnoreCase(Constants.WITH_ACCOUNTS) ){
//                        statusStrings.set(i, Constants.CONSOLIDATED);
//                    }else if ( statusStrings.get(i).equalsIgnoreCase(Constants.WITH_BANK) ){
//                        statusStrings.set(i, Constants.CONSOLIDATED);
//                    }
//                }
                if ( callback != null ){
                    callback.onComplete(true, statusStrings);
                }
            }
        }.execute();
    }

    //==================================

    public static final List<GroupedFeedbacksList> groupFeedbacksByOrders(List<FeedbackDetails> items) {

        List<GroupedFeedbacksList> groupedFeedbacksLists = new ArrayList<>();

        Map<String, GroupedFeedbacksList> keyMapSet = new HashMap<>();
        for (int i=0;i<items.size();i++){
            final FeedbackDetails feedbackDetails = items.get(i);
            if ( feedbackDetails != null ){
                if ( keyMapSet.containsKey(feedbackDetails.getOrderItemDetails().getOrderID()) ) {
                    keyMapSet.get(feedbackDetails.getOrderItemDetails().getOrderID()).addItem(feedbackDetails);
                }
                else {
                    GroupedFeedbacksList groupedFeedbacksList = new GroupedFeedbacksList(feedbackDetails.getOrderItemDetails().getReadableOrderID(), feedbackDetails.getOrderItemDetails().getDeliveryDate(), new ArrayList<FeedbackDetails>());
                    keyMapSet.put(feedbackDetails.getOrderItemDetails().getOrderID(), groupedFeedbacksList);
                    keyMapSet.get(feedbackDetails.getOrderItemDetails().getOrderID()).addItem(feedbackDetails);
                }
            }
        }

        for (String key : keyMapSet.keySet()) {
            groupedFeedbacksLists.add(keyMapSet.get(key));
        }

        Collections.sort(groupedFeedbacksLists, new Comparator<GroupedFeedbacksList>() {
            public int compare(GroupedFeedbacksList item1, GroupedFeedbacksList item2) {
                return item1.getDeliveryDate().compareToIgnoreCase(item2.getDeliveryDate());
            }
        });

        return groupedFeedbacksLists;
    }

}
