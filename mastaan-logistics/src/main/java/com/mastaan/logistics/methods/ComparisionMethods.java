package com.mastaan.logistics.methods;

import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.models.ChangeDetails;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 13/7/16.
 */
public final class ComparisionMethods {

    public static final List<OrderItemDetails> getNonExistedOrderItemsList(List<OrderItemDetails> pastList, List<OrderItemDetails> presentList){

        if ( pastList != null && presentList != null ){
            List<OrderItemDetails> nonExistedList = new ArrayList<>();

            for (int i=0;i<pastList.size();i++){
                boolean isExistsInPresentList = false;
                for (int j=0;j<presentList.size();j++){
                    if ( presentList.get(j).getID().equals(pastList.get(i).getID()) ){
                        isExistsInPresentList = true;
                        break;
                    }
                }

                if ( isExistsInPresentList == false ){
                    nonExistedList.add(pastList.get(i));
                }
            }
            return nonExistedList;
        }
        else{
            if ( pastList == null ){
                return  presentList;
            }else{
                return new ArrayList<>();
            }
        }
    }

    public static ChangeDetails isAnyChanges(final List<GroupedOrdersItemsList> pastItems, final List<GroupedOrdersItemsList> presentItems){
        //boolean isAnyChanges = false;
        ChangeDetails changeDetails = new ChangeDetails();
        // IF CHANGES IN NO. OF CATEGORIES
        if ( pastItems == null || pastItems.size() != presentItems.size() ){
            //isAnyChanges = true;
            changeDetails.setVisibleChanges(true);
        }
        // IF NO CHANGES IN NO. OF CATEGORIES
        else{
            for (int i=0;i<pastItems.size();i++){
                // IF CHANGES IN NO. OF ITEMS IN CATEGORY
                if ( pastItems.get(i).getItems().size() != presentItems.get(i).getItems().size() ){
                    //isAnyChanges = true;
                    changeDetails.setVisibleChanges(true);
                    break;
                }
                // IF NO CHANGES IN NO. OF ITEMS IN CATEGORY
                else{
                    for (int j=0;j<presentItems.get(i).getItems().size();j++){
                        OrderItemDetails presentItemDetails = presentItems.get(i).getItems().get(j);
                        OrderItemDetails pastItemDetails = getItemFromList(pastItems.get(i).getItems(), presentItemDetails.getID());

                        if ( pastItemDetails.getID() == null || pastItemDetails.getID().length() == 0
                                || presentItemDetails.getStatusString().equalsIgnoreCase(pastItemDetails.getStatusString()) == false
                                || presentItemDetails.getOrderDetails().getStatus().equalsIgnoreCase(pastItemDetails.getOrderDetails().getStatus()) == false
                                || presentItemDetails.getStatusReasons().equalsIgnoreCase(pastItemDetails.getStatusReasons()) == false
                                || presentItemDetails.getOrderDetails().getFormattedOrderID().equalsIgnoreCase(pastItemDetails.getOrderDetails().getFormattedOrderID()) == false

                                || presentItemDetails.getTotalOrderAmount() != pastItemDetails.getTotalOrderAmount()
                                || presentItemDetails.getTotalOrderCashAmount() != pastItemDetails.getTotalOrderCashAmount()
                                || presentItemDetails.getTotalOrderOnlinePaymentAmount() != pastItemDetails.getTotalOrderOnlinePaymentAmount()
                                || presentItemDetails.getTotalOrderDiscount() != pastItemDetails.getTotalOrderDiscount()
                                || presentItemDetails.getOrderDetails().getRefundAmount() != pastItemDetails.getOrderDetails().getRefundAmount()

                                || presentItemDetails.getQuantity() != pastItemDetails.getQuantity()
                                || presentItemDetails.getFinalNetWeight() != pastItemDetails.getFinalNetWeight()
                                || presentItemDetails.getAmountOfItem() != pastItemDetails.getAmountOfItem()

                                || presentItemDetails.getOrderItemsCount() != pastItemDetails.getOrderItemsCount()
                                || presentItemDetails.getDeliveryDate().equalsIgnoreCase(pastItemDetails.getDeliveryDate()) == false
                                || presentItemDetails.getOrderDetails().getSpecialInstructions().equalsIgnoreCase(pastItemDetails.getOrderDetails().getSpecialInstructions()) == false

                                || presentItemDetails.getOrderDetails().isBillPrinted() != pastItemDetails.getOrderDetails().isBillPrinted()

                                || presentItemDetails.isCashNCarryItem() != pastItemDetails.isCashNCarryItem()

                                || presentItemDetails.getVendorID().equals(pastItemDetails.getVendorID()) == false
                                || presentItemDetails.getVendorPaidAmount() != pastItemDetails.getVendorPaidAmount()
                                || presentItemDetails.getVendorPaymentType().equalsIgnoreCase(pastItemDetails.getVendorPaymentType()) == false
                                || presentItemDetails.getVendorComments().equalsIgnoreCase(pastItemDetails.getVendorComments()) == false

                                || presentItemDetails.isInCustomerSupportState() != pastItemDetails.isInCustomerSupportState()
                                || presentItemDetails.getCustomerSupporAssignerID().equalsIgnoreCase(pastItemDetails.getCustomerSupporAssignerID()) == false
                                || presentItemDetails.getCustomerSupportExecutiveID().equalsIgnoreCase(pastItemDetails.getCustomerSupportExecutiveID()) == false
                                || presentItemDetails.getCustomerSupportComment().equalsIgnoreCase(pastItemDetails.getCustomerSupportComment()) == false

                                ) {
                            //isAnyChanges = true;
                            changeDetails.setVisibleChanges(true);
                            break;
                        }
                        else if ( pastItemDetails.getID() == null || pastItemDetails.getID().length() == 0
                                /*Temp Code*/
                                || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getProcessingClaimDate(), pastItemDetails.getProcessingClaimDate()) > (300*1000) || (presentItemDetails.getProcessingClaimDate() != null && pastItemDetails.getProcessingClaimDate() == null) )
                                || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getProcessingCompletedDate(), pastItemDetails.getProcessingCompletedDate()) > (300*1000) || (presentItemDetails.getProcessingCompletedDate() != null && pastItemDetails.getProcessingCompletedDate() == null) )
                                || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getDeliveryBoyAssignedDate(), pastItemDetails.getDeliveryBoyAssignedDate()) > (300*1000) || (presentItemDetails.getDeliveryBoyAssignedDate() != null && pastItemDetails.getDeliveryBoyAssignedDate() == null) )
                                || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getDeliveryClaimedDate(), pastItemDetails.getDeliveryClaimedDate()) > (300*1000) || (presentItemDetails.getDeliveryClaimedDate() != null && pastItemDetails.getDeliveryClaimedDate() == null) )
                                || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getDeliveredDate(), pastItemDetails.getDeliveredDate()) > (300*1000) || (presentItemDetails.getDeliveredDate() != null && pastItemDetails.getDeliveredDate() == null) )
//
//                                || presentItemDetails.getProcessingClaimDate().equalsIgnoreCase(pastItemDetails.getProcessingClaimDate()) == false
//                                || presentItemDetails.getProcessingCompletedDate().equalsIgnoreCase(pastItemDetails.getProcessingCompletedDate()) == false
//                                || presentItemDetails.getDeliveryBoyAssignedDate().equalsIgnoreCase(pastItemDetails.getDeliveryBoyAssignedDate()) == false
//                                || presentItemDetails.getDeliveryClaimedDate().equalsIgnoreCase(pastItemDetails.getDeliveryClaimedDate()) == false
//                                || presentItemDetails.getDeliveredDate().equalsIgnoreCase(pastItemDetails.getDeliveredDate()) == false
                                ) {
                            //isAnyChanges = true;
                            changeDetails.setNonVisibleChanges(true);
                        }
                    }
                }
            }
        }
        return changeDetails;
    }

    public static List<OrderItemDetails> getModifiedItems(final List<OrderItemDetails> pastItems, final List<OrderItemDetails> presentItems){
        List<OrderItemDetails> modifiedItems = new ArrayList<>();
        if ( pastItems == null || pastItems.size() == 0 ){
            modifiedItems = presentItems;
        }
        else {
            for (int j = 0; j < presentItems.size(); j++) {
                try{
                    OrderItemDetails presentItemDetails = presentItems.get(j);
                    OrderItemDetails pastItemDetails = getItemFromList(pastItems, presentItemDetails.getID());

                    if ( pastItemDetails.getID() == null || pastItemDetails.getID().length() == 0
                            || presentItemDetails.getStatusString().equalsIgnoreCase(pastItemDetails.getStatusString()) == false
                            || presentItemDetails.getOrderDetails().getStatus().equalsIgnoreCase(pastItemDetails.getOrderDetails().getStatus()) == false
                            || presentItemDetails.getStatusReasons().equalsIgnoreCase(pastItemDetails.getStatusReasons()) == false
                            || presentItemDetails.getOrderDetails().getFormattedOrderID().equalsIgnoreCase(pastItemDetails.getOrderDetails().getFormattedOrderID()) == false

                            || presentItemDetails.getTotalOrderAmount() != pastItemDetails.getTotalOrderAmount()
                            || presentItemDetails.getTotalOrderCashAmount() != pastItemDetails.getTotalOrderCashAmount()
                            || presentItemDetails.getTotalOrderOnlinePaymentAmount() != pastItemDetails.getTotalOrderOnlinePaymentAmount()
                            || presentItemDetails.getTotalOrderDiscount() != pastItemDetails.getTotalOrderDiscount()

                            || presentItemDetails.getQuantity() != pastItemDetails.getQuantity()
                            || presentItemDetails.getAmountOfItem() != pastItemDetails.getAmountOfItem()

                            || presentItemDetails.getOrderItemsCount() != pastItemDetails.getOrderItemsCount()
                            || presentItemDetails.getDeliveryDate().equalsIgnoreCase(pastItemDetails.getDeliveryDate()) == false
                            || presentItemDetails.getOrderDetails().getSpecialInstructions().equalsIgnoreCase(pastItemDetails.getOrderDetails().getSpecialInstructions()) == false

                        /*Temp Code*/
                            || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getProcessingClaimDate(), pastItemDetails.getProcessingClaimDate()) > (300*1000) || (presentItemDetails.getProcessingClaimDate() != null && pastItemDetails.getProcessingClaimDate() == null) )
                            || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getProcessingCompletedDate(), pastItemDetails.getProcessingCompletedDate()) > (300*1000) || (presentItemDetails.getProcessingCompletedDate() != null && pastItemDetails.getProcessingCompletedDate() == null) )
                            || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getDeliveryBoyAssignedDate(), pastItemDetails.getDeliveryBoyAssignedDate()) > (300*1000) || (presentItemDetails.getDeliveryBoyAssignedDate() != null && pastItemDetails.getDeliveryBoyAssignedDate() == null) )
                            || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getDeliveryClaimedDate(), pastItemDetails.getDeliveryClaimedDate()) > (300*1000) || (presentItemDetails.getDeliveryClaimedDate() != null && pastItemDetails.getDeliveryClaimedDate() == null) )
                            || ( DateMethods.getDifferenceBetweenDatesInMilliSeconds(presentItemDetails.getDeliveredDate(), pastItemDetails.getDeliveredDate()) > (300*1000) || (presentItemDetails.getDeliveredDate() != null && pastItemDetails.getDeliveredDate() == null) )

                            || presentItemDetails.getOrderDetails().isBillPrinted() != pastItemDetails.getOrderDetails().isBillPrinted()

                            || presentItemDetails.isCashNCarryItem() != pastItemDetails.isCashNCarryItem()

                            || presentItemDetails.getVendorID().equals(pastItemDetails.getVendorID()) == false
                            || presentItemDetails.getVendorPaidAmount() != pastItemDetails.getVendorPaidAmount()
                            || presentItemDetails.getVendorPaymentType().equalsIgnoreCase(pastItemDetails.getVendorPaymentType()) == false
                            || presentItemDetails.getVendorComments().equalsIgnoreCase(pastItemDetails.getVendorComments()) == false

                            || presentItemDetails.isInCustomerSupportState() != pastItemDetails.isInCustomerSupportState()
                            || presentItemDetails.getCustomerSupporAssignerID().equalsIgnoreCase(pastItemDetails.getCustomerSupporAssignerID()) == false
                            || presentItemDetails.getCustomerSupportExecutiveID().equalsIgnoreCase(pastItemDetails.getCustomerSupportExecutiveID()) == false
                            || presentItemDetails.getCustomerSupportComment().equalsIgnoreCase(pastItemDetails.getCustomerSupportComment()) == false

//                        || presentItemDetails.getProcessingClaimDate().equalsIgnoreCase(pastItemDetails.getProcessingClaimDate()) == false
//                        || presentItemDetails.getProcessingCompletedDate().equalsIgnoreCase(pastItemDetails.getProcessingCompletedDate()) == false
//                        || presentItemDetails.getDeliveryBoyAssignedDate().equalsIgnoreCase(pastItemDetails.getDeliveryBoyAssignedDate()) == false
//                        || presentItemDetails.getDeliveryClaimedDate().equalsIgnoreCase(pastItemDetails.getDeliveryClaimedDate()) == false
//                        || presentItemDetails.getDeliveredDate().equalsIgnoreCase(pastItemDetails.getDeliveredDate()) == false
                            ) {
                        //showToastMessage(presentItemDetails.getID()+"("+presentItemDetails.getStatusString()+") with "+getStatusofItem(items.get(i).getItems(), presentItemDetails.getID()));
                        modifiedItems.add(presentItemDetails);
                    }
                }catch (Exception e){}
            }
        }
        return modifiedItems;
    }

    public static final OrderItemDetails getItemFromList(List<OrderItemDetails> itemsList, String itemID){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(itemID) ){
                return itemsList.get(i);
            }
        }
        return new OrderItemDetails();
    }

    /*public static final String getStatusofItem(List<OrderItemDetails> itemsList, String itemID){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(itemID) ){
                return itemsList.get(i).getStatusString();
            }
        }
        return "";
    }

    public static final double getWeightofItem(List<OrderItemDetails> itemsList, String itemID){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(itemID) ){
                return itemsList.get(i).getQuantity();
            }
        }
        return 0;
    }

    public static final String getDeliveryDate(List<OrderItemDetails> itemsList, String itemID){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(itemID) ){
                return itemsList.get(i).getDeliveryDate();
            }
        }
        return "";
    }

    public static final long getOrderItemsCount(List<OrderItemDetails> itemsList, String itemID){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(itemID) ){
                return itemsList.get(i).getOrderItemsCount();
            }
        }
        return 0;
    }

    public static final String getCombinedSpecialInstructions(List<OrderItemDetails> itemsList, String itemID){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(itemID) ){
                return itemsList.get(i).getOrderDetails().getCombinedSpecialInstructions();
            }
        }
        return "";
    }*/

}
