package com.mastaan.logistics.methods;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.UserDetailsMini;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 09/02/19.
 */

public class AnalyseFollowupsMethods {

    List<FollowupDetails> itemsList = new ArrayList<>();

    InfoDetails summaryDetails = new InfoDetails();
    List<InfoDetails> employeesSummariesList = new ArrayList<>();
    List<InfoDetails> buyersSummariesList = new ArrayList<>();
    List<InfoDetails> commentsSummariesList = new ArrayList<>();

    List<String> sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW, Constants.EMPLOYEES, Constants.BUYERS});


    public AnalyseFollowupsMethods(List<FollowupDetails> itemsList){
        if ( itemsList == null ){   itemsList = new ArrayList<>();  }
        this.itemsList = itemsList;
    }


    public AnalyseFollowupsMethods setSectionsList(String []sectionsList){
        return setSectionsList(CommonMethods.getStringListFromStringArray(sectionsList));
    }
    public AnalyseFollowupsMethods setSectionsList(List<String> sectionsList){
        if ( sectionsList != null && sectionsList.size() > 0 ){
            this.sectionsList = sectionsList;
        }
        return this;
    }

    public AnalyseFollowupsMethods analyse(){
        for (int i=0;i<itemsList.size();i++){
            try{
                FollowupDetails followupDetails = itemsList.get(i);
                if ( followupDetails != null && followupDetails.getEmployeeDetails() != null && followupDetails.getBuyerDetails() != null ){
                    // SUMMARY
                    summaryDetails.addToSummary(followupDetails);

                    // EMPLOYEES SUMMARY
                    if ( sectionsList.contains(Constants.EMPLOYEES) ) {
                        InfoDetails existingEmployeeSummary = null;
                        for (int j = 0; j < employeesSummariesList.size(); j++) {
                            if (employeesSummariesList.get(j).id.equals(followupDetails.getEmployeeDetails().getID())) {
                                existingEmployeeSummary = employeesSummariesList.get(j);
                                break;
                            }
                        }
                        if (existingEmployeeSummary == null) {
                            existingEmployeeSummary = new InfoDetails(followupDetails.getEmployeeDetails().getID(), followupDetails.getEmployeeDetails().getAvailableName());
                            employeesSummariesList.add(existingEmployeeSummary);
                        }
                        existingEmployeeSummary.addToSummary(followupDetails);
                    }

                    // BUYERS SUMMARY
                    if ( sectionsList.contains(Constants.BUYERS) ) {
                        InfoDetails existingBuyerSummary = null;
                        for (int j = 0; j < buyersSummariesList.size(); j++) {
                            if (buyersSummariesList.get(j).id.equals(followupDetails.getBuyerDetails().getID())) {
                                existingBuyerSummary = buyersSummariesList.get(j);
                                break;
                            }
                        }
                        if (existingBuyerSummary == null) {
                            existingBuyerSummary = new InfoDetails(followupDetails.getBuyerDetails().getID(), followupDetails.getBuyerDetails().getAvailableName());
                            buyersSummariesList.add(existingBuyerSummary);
                        }
                        existingBuyerSummary.addToSummary(followupDetails);
                    }

                    // COMMENTS SUMMARY
                    if ( sectionsList.contains(Constants.COMMENTS) ) {
                        InfoDetails existingCommentSummary = null;
                        for (int j = 0; j < commentsSummariesList.size(); j++) {
                            if (commentsSummariesList.get(j).id.equalsIgnoreCase(followupDetails.getComments().trim())) {
                                existingCommentSummary = commentsSummariesList.get(j);
                                break;
                            }
                        }
                        if (existingCommentSummary == null) {
                            existingCommentSummary = new InfoDetails(followupDetails.getComments().trim().toLowerCase(), followupDetails.getComments().trim().toLowerCase());
                            commentsSummariesList.add(existingCommentSummary);
                        }
                        existingCommentSummary.increaseCount();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if ( sectionsList.contains(Constants.EMPLOYEES) ) {
            Collections.sort(employeesSummariesList, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    if (item1.count > item2.count) {
                        return -1;
                    } else if (item1.count < item2.count) {
                        return 1;
                    }
                    return 0;
                }
            });
        }

        if ( sectionsList.contains(Constants.BUYERS) ) {
            Collections.sort(buyersSummariesList, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    if (item1.count > item2.count) {
                        return -1;
                    } else if (item1.count < item2.count) {
                        return 1;
                    }
                    return 0;
                }
            });
        }

        if ( sectionsList.contains(Constants.COMMENTS) ) {
            Collections.sort(commentsSummariesList, new Comparator<InfoDetails>() {
                public int compare(InfoDetails item1, InfoDetails item2) {
                    if (item1.count > item2.count) {
                        return -1;
                    } else if (item1.count < item2.count) {
                        return 1;
                    }
                    return 0;
                }
            });
        }

        return this;
    }

    public String getSummaryInfo(){
        return getSummaryString(summaryDetails);
    }

    public String getEmployeesSummaryInfo(){
        String employeesSummaryInfo = "";
        for (int i=0;i<employeesSummariesList.size();i++){
            employeesSummaryInfo += (employeesSummaryInfo.length()>0?"<br><br>":"") + getSummaryString(employeesSummariesList.get(i), false, true);
        }
        return employeesSummaryInfo;
    }

    public String getBuyersSummaryInfo(){
        String buyersSummaryInfo = "";
        for (int i=0;i<buyersSummariesList.size();i++){
            buyersSummaryInfo += (buyersSummaryInfo.length()>0?"<br><br>":"") + getSummaryString(buyersSummariesList.get(i), true, false);
        }
        return buyersSummaryInfo;
    }

    public String getCommentsSummaryInfo(){
        String commentsSummaryInfo = "";
        for (int i=0;i<commentsSummariesList.size();i++){
            if ( commentsSummaryInfo.length() > 0 ){    commentsSummaryInfo += "<br>";  }
            commentsSummaryInfo += "<b><u>" + CommonMethods.capitalizeFirstLetter(commentsSummariesList.get(i).name) + "</u></b>";
            commentsSummaryInfo += "<br>Count: <b>"+CommonMethods.getIndianFormatNumber(commentsSummariesList.get(i).count)+"</b>";
        }
        return commentsSummaryInfo;
    }

    //---------

    private String getSummaryString(InfoDetails infoDetails){
        return getSummaryString(infoDetails, true, true);
    }
    private String getSummaryString(InfoDetails infoDetails, boolean showEmployeesSummary, boolean showBuyersSummary){
        String summaryInfo = "";
        if ( infoDetails != null ) {
            if (infoDetails.name != null && infoDetails.name.trim().length() > 0) {
                summaryInfo += "<b><u>" + CommonMethods.capitalizeStringWords(infoDetails.name) + "</u></b>";
            }

            summaryInfo += (summaryInfo.length() > 0 ? "<br>" : "") + "Count:   <b>"+CommonMethods.getIndianFormatNumber(infoDetails.count)+"</b>";
            if ( infoDetails.followup_resulted_orders > 0 ){
                summaryInfo += (summaryInfo.length() > 0 ? "<br>" : "") + "Followup resulted orders:   <b>" + CommonMethods.getIndianFormatNumber(infoDetails.followup_resulted_orders) + "</b>";
            }
            if (showEmployeesSummary) {
                summaryInfo += (summaryInfo.length() > 0 ? "<br>" : "") + "Employees:   <b>" + CommonMethods.getIndianFormatNumber(infoDetails.employeesCount) + "</b>";
            }
            if (showBuyersSummary) {
                summaryInfo += (summaryInfo.length() > 0 ? "<br>" : "") + "Buyers:   <b>" + CommonMethods.getIndianFormatNumber(infoDetails.buyersCount) + "</b>";
            }

            if ( infoDetails.typesList.size() > 0 ){
                Collections.sort(infoDetails.typesList, new Comparator<InfoDetails>() {
                    public int compare(InfoDetails item1, InfoDetails item2) {
                        if (item1.count > item2.count) {
                            return -1;
                        } else if (item1.count < item2.count) {
                            return 1;
                        }
                        return 0;
                    }
                });

                String typesSummary = "";
                for (int i=0;i<infoDetails.typesList.size();i++){
                    typesSummary += (typesSummary.length()>0?"<br>":"") + infoDetails.typesList.get(i).name + " followups:   <b>" +infoDetails.typesList.get(i).count+"</b>";
                }
                summaryInfo += "<br>"+typesSummary;
            }
        }
        return summaryInfo;
    }

    //==========

    class InfoDetails {
        String id;
        String name;

        long count;

        long followup_resulted_orders;

        long employeesCount;
        List<String> employeesList = new ArrayList<>();

        long buyersCount;
        List<String> buyersList = new ArrayList<>();

        List<InfoDetails> typesList = new ArrayList<>();


        InfoDetails(){}
        InfoDetails(String id, String name){
            this.id = id;
            this.name = name;
        }

        void addToSummary(FollowupDetails followupDetails){
            if ( followupDetails != null && followupDetails.isExcluded() == false
                    && followupDetails.getEmployeeDetails() != null && followupDetails.getBuyerDetails() != null ){
                UserDetailsMini employeeDetails = followupDetails.getEmployeeDetails();
                BuyerDetails buyerDetails = followupDetails.getBuyerDetails();

                count += 1;

                // Order Resulted Count
                if ( followupDetails.isResultedInOrder() ){
                    followup_resulted_orders += 1;
                }

                // Employees Count
                if ( employeesList.contains(employeeDetails.getID()) == false ){
                    employeesCount += 1;
                    employeesList.add(employeeDetails.getID());
                }

                // Buyers Count
                if ( buyersList.contains(buyerDetails.getID()) == false ){
                    buyersCount += 1;
                    buyersList.add(buyerDetails.getID());
                }

                // Types
                InfoDetails existingTypeInfoDetails = null;
                for (int i=0;i<typesList.size();i++){
                    if ( typesList.get(i).id.equalsIgnoreCase(followupDetails.getType()) ){
                        existingTypeInfoDetails = typesList.get(i);
                        break;
                    }
                }
                if ( existingTypeInfoDetails == null ){
                    existingTypeInfoDetails = new InfoDetails();
                    existingTypeInfoDetails.id = followupDetails.getType();
                    existingTypeInfoDetails.name = followupDetails.getTypeName();
                    typesList.add(existingTypeInfoDetails);
                }
                existingTypeInfoDetails.count += 1;
            }
        }

        void increaseCount(){
            this.count += 1;
        }
    }

}