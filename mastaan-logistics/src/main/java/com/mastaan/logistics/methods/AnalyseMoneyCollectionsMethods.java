package com.mastaan.logistics.methods;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.MoneyCollectionDetails;
import com.mastaan.logistics.models.MoneyCollectionTransactionDetails;
import com.mastaan.logistics.models.OrderDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Venkatesh Uppu on 2/11/16.
 */

public class AnalyseMoneyCollectionsMethods {

    List<String> actionsList = new ArrayList<>();

    String forCollectionDate;
    String forConsolidationDate;
    String forDeliveryBoy;

    List<MoneyCollectionDetails> itemsList = new ArrayList<>();

    InfoDetails summaryDetails = new InfoDetails();
    List<InfoDetails> deliveryBoyCollectionsInfos = new ArrayList<>();
    List<InfoDetails> consolidatorsInfos = new ArrayList<>();

    public AnalyseMoneyCollectionsMethods(List<MoneyCollectionDetails> itemsList){
        this(null, itemsList);
    }
    public AnalyseMoneyCollectionsMethods(String []actionsList, List<MoneyCollectionDetails> itemsList){
        if ( actionsList != null && actionsList.length > 0 ){   this.actionsList = CommonMethods.getStringListFromStringArray(actionsList);}
        else {  this.actionsList = new ArrayList<>(); this.actionsList.add(Constants.SUMMARY);this.actionsList.add(Constants.DELIVERY_BOY);this.actionsList.add(Constants.CONSOLIDATOR); }

        if ( itemsList == null ){   itemsList = new ArrayList<>();  }
        this.itemsList = itemsList;
    }

    public AnalyseMoneyCollectionsMethods setForCollectionDate(String forCollectionDate) {
        this.forCollectionDate = forCollectionDate;
        return this;
    }

    public AnalyseMoneyCollectionsMethods setForConsolidationDate(String forConsolidationDate) {
        this.forConsolidationDate = forConsolidationDate;
        return this;
    }

    public AnalyseMoneyCollectionsMethods setForDeliveryBoy(String forDeliveryBoy) {
        this.forDeliveryBoy = forDeliveryBoy;
        return this;
    }

    public void analyse(){
        Map<String, InfoDetails> deliveryBoyCollectionsKeyMapSet = new HashMap<>();
        Map<String, InfoDetails> consolidatorsKeyMapSet = new HashMap<>();

        for (int i=0;i<itemsList.size();i++){
            MoneyCollectionDetails moneyCollectionDetails = itemsList.get(i);
            OrderDetails orderDetails = moneyCollectionDetails.getOrderDetails();

            if ( moneyCollectionDetails != null && orderDetails != null && moneyCollectionDetails.getTransactions() != null ){
                for (int j=0;j<moneyCollectionDetails.getTransactions().size();j++){
                    MoneyCollectionTransactionDetails transactionDetails = moneyCollectionDetails.getTransactions().get(j);
                    String transactionPaymentType = transactionDetails.getPaymentTypeString();

                    if ( forCollectionDate == null || forCollectionDate.trim().length() == 0 || forCollectionDate.trim().equalsIgnoreCase("date")
                            || DateMethods.compareDates(forCollectionDate.trim(), DateMethods.getReadableDateFromUTC(transactionDetails.getCollectionDate().trim(), DateConstants.DD_MM_YYYY)) == 0 ){

                        if ( forConsolidationDate == null || forConsolidationDate.trim().length() == 0 || forConsolidationDate.trim().equalsIgnoreCase("date")
                                || DateMethods.compareDates(forConsolidationDate.trim(), DateMethods.getReadableDateFromUTC(transactionDetails.getConsolidationDate().trim(), DateConstants.DD_MM_YYYY)) == 0 ){

                            // SUMMARY
                            summaryDetails.addMoneyCollectionAmount(transactionDetails);

                            // DELIVERY BOY
                            if ( actionsList.contains(Constants.DELIVERY_BOY) ){
                                if ( transactionPaymentType.equalsIgnoreCase(Constants.CASH_ON_DELIVERY) || transactionPaymentType.equalsIgnoreCase(Constants.PAYTM_QR) ){//transactionDetails.getDeliveryBoyID() != null && transactionDetails.getDeliveryBoyID().length() > 0 ){
                                    if ( forDeliveryBoy == null || forDeliveryBoy.length() == 0 || transactionDetails.getDeliveryBoyID().equals(forDeliveryBoy) ){
                                        if ( deliveryBoyCollectionsKeyMapSet.containsKey(transactionDetails.getDeliveryBoyID()) == false ) {   // ADDING TO MAPSET IF NOT PRESENT
                                            deliveryBoyCollectionsKeyMapSet.put(transactionDetails.getDeliveryBoyID(), new InfoDetails(transactionDetails.getDeliveryBoyDetails().getID(), transactionDetails.getDeliveryBoyDetails().getAvailableName()));
                                        }
                                        InfoDetails deliveryBoyInfoDetails = deliveryBoyCollectionsKeyMapSet.get(transactionDetails.getDeliveryBoyID());
                                        deliveryBoyInfoDetails.addMoneyCollectionAmount(transactionDetails).addToConsolidatorsList(transactionDetails);
                                    }
                                }
                            }

                            // CONSOLIDATOR
                            if (actionsList.contains(Constants.CONSOLIDATOR) ){
                                if ( transactionDetails.isConsolidated() && (transactionPaymentType.equalsIgnoreCase(Constants.CASH_ON_DELIVERY) || transactionPaymentType.equalsIgnoreCase(Constants.PAYTM_QR)) ){//transactionDetails.getConsolidatorID() != null && transactionDetails.getConsolidatorID().length() > 0 ){
                                    if ( forDeliveryBoy == null || forDeliveryBoy.length() == 0 || transactionDetails.getDeliveryBoyID().equals(forDeliveryBoy) ) {
                                        if (consolidatorsKeyMapSet.containsKey(transactionDetails.getConsolidatorID()) == false) {   // ADDING TO MAPSET IF NOT PRESENT
                                            consolidatorsKeyMapSet.put(transactionDetails.getConsolidatorID(), new InfoDetails(transactionDetails.getConsolidatorDetails().getID(), transactionDetails.getConsolidatorDetails().getAvailableName()));
                                        }
                                        InfoDetails consolidatorInfoDetails = consolidatorsKeyMapSet.get(transactionDetails.getConsolidatorID());
                                        consolidatorInfoDetails.addMoneyCollectionAmount(transactionDetails).addToDeliveryBoysList(transactionDetails);
                                    }
                                }
                            }
                        }else{
                            //Log.d(Constants.LOG_TAG, "========> IGNORING: "+transactionDetails.getAmount()+" of "+orderDetails.getCustomerName());
                        }
                    }
                }
            }
        }

        //-------

        for (String key : deliveryBoyCollectionsKeyMapSet.keySet()) {
            deliveryBoyCollectionsInfos.add(deliveryBoyCollectionsKeyMapSet.get(key));
        }

        Collections.sort(deliveryBoyCollectionsInfos, new Comparator<InfoDetails>() {
            public int compare(InfoDetails item1, InfoDetails item2) {
                return item1.name.compareToIgnoreCase(item2.name);
            }
        });

        //-------

        for (String key : consolidatorsKeyMapSet.keySet()) {
            consolidatorsInfos.add(consolidatorsKeyMapSet.get(key));
        }

        Collections.sort(consolidatorsInfos, new Comparator<InfoDetails>() {
            public int compare(InfoDetails item1, InfoDetails item2) {
                return item1.name.compareToIgnoreCase(item2.name);
            }
        });

    }


    //----------

    public String getFullSummaryInfo(){
        return getSummaryString(summaryDetails);
    }

    public String getDeliveryBoyInfo(){
        String deliveryBoysInfo = "";

        if ( deliveryBoyCollectionsInfos != null && deliveryBoyCollectionsInfos.size() > 0 ) {
            for (int i = 0; i < deliveryBoyCollectionsInfos.size(); i++) {
                InfoDetails deliveryBoyDetails = deliveryBoyCollectionsInfos.get(i);

                String summary = getSummaryString(deliveryBoyDetails, true, true, false);
                if ( summary.length() > 0 ) {
                    if ( deliveryBoysInfo.length() > 0 ){    deliveryBoysInfo += "<br><br>";  }
                    deliveryBoysInfo += "<b>\u2023 <u>"+deliveryBoyDetails.name.toUpperCase()+"</u></b><br>" + summary;

                    if (deliveryBoyDetails.consolidatorsList.size() > 0) {
                        Collections.sort(deliveryBoyDetails.consolidatorsList, new Comparator<InfoDetails>() {
                            public int compare(InfoDetails item1, InfoDetails item2) {
                                return item1.name.compareToIgnoreCase(item2.name);
                            }
                        });

                        deliveryBoysInfo += "<br>";//<br><b>Consolidator wise:</b>";
                        for (int j = 0; j < deliveryBoyDetails.consolidatorsList.size(); j++) {
                            InfoDetails consolidatorDetails = deliveryBoyDetails.consolidatorsList.get(j);

                            String info = getSplitInfo(consolidatorDetails.consolidatedCollectedOPAmount, consolidatorDetails.consolidatedCollectedWalletAmount, consolidatorDetails.consolidatedCollectedCODAmount, consolidatorDetails.consolidatedCollectedPaytmQRAmount, consolidatorDetails.consolidatedReturnedOPAmount, consolidatorDetails.consolidatedReturnedWalletAmount, consolidatorDetails.consolidatedReturnedCODAmount, consolidatorDetails.consolidatedReturnedPaytmQRAmount);
                            if (info.length() > 0) {
                                deliveryBoysInfo += "<br>\u2022 " + CommonMethods.capitalizeStringWords(consolidatorDetails.name) + "<br>" + info;
                            }
                        }
                    }

                    String pendingInfo = getSummaryString(deliveryBoyDetails, false, false, true);
                    if (pendingInfo.length() > 0) {
                        deliveryBoysInfo += "<br><br>" + pendingInfo;
                    }
                }
            }
        }else{
            deliveryBoysInfo = "No info to show";
        }

        return deliveryBoysInfo;
    }

    public String getConsolidatorsInfo(){
        String consolidatorsInfo = "";
        if ( consolidatorsInfos != null && consolidatorsInfos.size() > 0 ) {
            for (int i = 0; i < consolidatorsInfos.size(); i++) {
                InfoDetails consolidatorDetails = consolidatorsInfos.get(i);

                String summary = getSummaryString(consolidatorDetails, false, true, false);
                if ( summary.length() > 0 ){
                    if ( consolidatorsInfo.length() > 0 ){    consolidatorsInfo += "<br><br>";  }
                    consolidatorsInfo += "<b>\u2023 <u>"+consolidatorDetails.name.toUpperCase()+"</u></b><br>"+summary;

                    if ( consolidatorDetails.deliveryBoysList.size() > 0 ) {
                        Collections.sort(consolidatorDetails.deliveryBoysList, new Comparator<InfoDetails>() {
                            public int compare(InfoDetails item1, InfoDetails item2) {
                                return item1.name.compareToIgnoreCase(item2.name);
                            }
                        });

                        consolidatorsInfo += "<br>";//<br><b>Delivery boy wise:</b>";
                        for (int j = 0; j < consolidatorDetails.deliveryBoysList.size(); j++) {
                            InfoDetails deliveryBoyDetails = consolidatorDetails.deliveryBoysList.get(j);

                            String info = getSplitInfo(deliveryBoyDetails.consolidatedCollectedOPAmount, deliveryBoyDetails.consolidatedCollectedWalletAmount, deliveryBoyDetails.consolidatedCollectedCODAmount, deliveryBoyDetails.consolidatedCollectedPaytmQRAmount, deliveryBoyDetails.consolidatedReturnedOPAmount, deliveryBoyDetails.consolidatedReturnedWalletAmount, deliveryBoyDetails.consolidatedReturnedCODAmount, deliveryBoyDetails.consolidatedReturnedPaytmQRAmount);
                            if ( info.length() > 0 ) {
                                consolidatorsInfo += "<br>\u2022 " + CommonMethods.capitalizeStringWords(deliveryBoyDetails.name) + "<br>" + info;
                            }
                        }
                    }
                }
            }
        }else{
            consolidatorsInfo = "No info to show";
        }

        return consolidatorsInfo;
    }

    //----------

    private String getSummaryString(InfoDetails infoDetails){
        return getSummaryString(infoDetails, true, true, true);
    }
    private String getSummaryString(InfoDetails infoDetails, boolean showCollectedInfo, boolean showConsolidatedInfo, boolean showPendingInfo){
        String summary = "";

        if ( showCollectedInfo ) {
            if (infoDetails.collectedOPAmount > 0 || infoDetails.returnedOPAmount > 0
                    || infoDetails.collectedWalletAmount > 0 || infoDetails.returnedWalletAmount > 0
                    || infoDetails.collectedCODAmount > 0 || infoDetails.returnedCODAmount > 0
                    || infoDetails.collectedPaytmQRAmount > 0 || infoDetails.returnedPaytmQRAmount > 0) {
                summary += (summary.length() > 0 ? "<br><br>" : "") + "<u>Collected</u>: <b>" + CommonMethods.getIndianFormatNumber(infoDetails.collectedOPAmount + infoDetails.collectedWalletAmount + infoDetails.collectedCODAmount + infoDetails.collectedPaytmQRAmount - infoDetails.returnedOPAmount - infoDetails.returnedWalletAmount - infoDetails.returnedCODAmount - infoDetails.returnedPaytmQRAmount) + "</b>"
                        + "<br>" + getSplitInfo(infoDetails.collectedOPAmount, infoDetails.collectedWalletAmount, infoDetails.collectedCODAmount, infoDetails.collectedPaytmQRAmount, infoDetails.returnedOPAmount, infoDetails.returnedWalletAmount, infoDetails.returnedCODAmount, infoDetails.returnedPaytmQRAmount);
            }
        }

        if ( showConsolidatedInfo ) {
            if (infoDetails.consolidatedCollectedOPAmount > 0 || infoDetails.consolidatedReturnedOPAmount > 0
                    || infoDetails.consolidatedCollectedWalletAmount > 0 || infoDetails.consolidatedReturnedWalletAmount > 0
                    || infoDetails.consolidatedCollectedCODAmount > 0 || infoDetails.consolidatedReturnedCODAmount > 0
                    || infoDetails.consolidatedCollectedPaytmQRAmount > 0 || infoDetails.consolidatedReturnedPaytmQRAmount > 0) {
                summary += (summary.length() > 0 ? "<br><br>" : "") + "<u>Consolidated</u>: <b>" + CommonMethods.getIndianFormatNumber(infoDetails.consolidatedCollectedOPAmount + infoDetails.consolidatedCollectedWalletAmount + infoDetails.consolidatedCollectedCODAmount + infoDetails.consolidatedCollectedPaytmQRAmount - infoDetails.consolidatedReturnedOPAmount - infoDetails.consolidatedReturnedWalletAmount - infoDetails.consolidatedReturnedCODAmount - infoDetails.consolidatedReturnedPaytmQRAmount) + "</b>"
                        + "<br>" + getSplitInfo(infoDetails.consolidatedCollectedOPAmount, infoDetails.consolidatedCollectedWalletAmount, infoDetails.consolidatedCollectedCODAmount, infoDetails.consolidatedCollectedPaytmQRAmount, infoDetails.consolidatedReturnedOPAmount, infoDetails.consolidatedReturnedWalletAmount, infoDetails.consolidatedReturnedCODAmount, infoDetails.consolidatedReturnedPaytmQRAmount);
            }
        }

        if ( showPendingInfo ) {
            if (infoDetails.pendingCollectedOPAmount > 0 || infoDetails.pendingReturnedOPAmount > 0
                    || infoDetails.pendingCollectedWalletAmount > 0 || infoDetails.pendingReturnedWalletAmount > 0
                    || infoDetails.pendingCollectedCODAmount > 0 || infoDetails.pendingReturnedCODAmount > 0
                    || infoDetails.pendingCollectedPaytmQRAmount > 0 || infoDetails.pendingReturnedPaytmQRAmount > 0) {
                summary += (summary.length() > 0 ? "<br><br>" : "") + "<u>Pending</u>: <b>" + CommonMethods.getIndianFormatNumber(infoDetails.pendingCollectedOPAmount + infoDetails.pendingCollectedWalletAmount + infoDetails.pendingCollectedCODAmount + infoDetails.pendingCollectedPaytmQRAmount - infoDetails.pendingReturnedOPAmount - infoDetails.pendingReturnedWalletAmount - infoDetails.pendingReturnedCODAmount - infoDetails.pendingReturnedPaytmQRAmount) + "</b>"
                        + "<br>" + getSplitInfo(infoDetails.pendingCollectedOPAmount, infoDetails.pendingCollectedWalletAmount, infoDetails.pendingCollectedCODAmount, infoDetails.pendingCollectedPaytmQRAmount, infoDetails.pendingReturnedOPAmount, infoDetails.pendingReturnedWalletAmount, infoDetails.pendingReturnedCODAmount, infoDetails.pendingReturnedPaytmQRAmount);
            }
        }

        return summary;
    }

    private String getSplitInfo(double collectedOPAmount, double collectedWalletAmount, double collectedCODAmount, double collectedPaytmQRAmount, double returnedOPAmount, double returnedWalletAmount, double returnedCODAmount, double returnedPaytmQRAmount){
        String collectionInfo = "";
        if ( collectedOPAmount > 0 ){
            collectionInfo += (collectionInfo.length()>0?" + ":"") + "<b>"+CommonMethods.getIndianFormatNumber(collectedOPAmount)+"</b>(OP)";
        }
        if ( collectedWalletAmount > 0 ){
            collectionInfo += (collectionInfo.length()>0?" + ":"") + "<b>"+CommonMethods.getIndianFormatNumber(collectedWalletAmount)+"</b>(WAL)";
        }
        if ( collectedCODAmount > 0 ){
            collectionInfo += (collectionInfo.length()>0?" + ":"") + "<b>"+CommonMethods.getIndianFormatNumber(collectedCODAmount)+"</b>(COD)";
        }
        if ( collectedPaytmQRAmount > 0 ){
            collectionInfo += (collectionInfo.length()>0?" + ":"") + "<b>"+CommonMethods.getIndianFormatNumber(collectedPaytmQRAmount)+"</b>(QR)";
        }

        String returnInfo = "";
        if ( returnedOPAmount > 0 ){
            returnInfo += (returnInfo.length()>0?" + ":"") + "<b>"+CommonMethods.getIndianFormatNumber(returnedOPAmount)+"</b>(OP)";
        }
        if ( returnedWalletAmount > 0 ){
            returnInfo += (returnInfo.length()>0?" + ":"") + "<b>"+CommonMethods.getIndianFormatNumber(returnedWalletAmount)+"</b>(WAL)";
        }
        if ( returnedCODAmount > 0 ){
            returnInfo += (returnInfo.length()>0?" + ":"") + "<b>"+CommonMethods.getIndianFormatNumber(returnedCODAmount)+"</b>(COD)";
        }
        if ( returnedPaytmQRAmount > 0 ){
            returnInfo += (returnInfo.length()>0?" + ":"") + "<b>"+CommonMethods.getIndianFormatNumber(returnedPaytmQRAmount)+"</b>(QR)";
        }

        //--------------------

        String splitInfo = "";

        if ( collectionInfo.length() > 0 ){
            splitInfo += (splitInfo.length()>0?"<br>":"") + "Col: <b>"+CommonMethods.getIndianFormatNumber(collectedOPAmount+collectedWalletAmount+collectedCODAmount+collectedPaytmQRAmount)+"</b> ["+collectionInfo+"]";
        }
        if ( returnInfo.length() > 0 ){
            splitInfo += (splitInfo.length()>0?"<br>":"") + "Ret: <b>"+CommonMethods.getIndianFormatNumber(returnedOPAmount+returnedWalletAmount+returnedCODAmount+returnedPaytmQRAmount)+"</b> ["+returnInfo+"]";
        }
        return splitInfo;
    }


    //===============

    class InfoDetails{

        String id;
        String name;

        double collectedCODAmount = 0;
        double collectedOPAmount = 0;
        double collectedWalletAmount = 0;
        double collectedPaytmQRAmount = 0;
        double returnedCODAmount = 0;
        double returnedOPAmount = 0;
        double returnedWalletAmount = 0;
        double returnedPaytmQRAmount = 0;

        double consolidatedCollectedCODAmount = 0;
        double consolidatedCollectedOPAmount = 0;
        double consolidatedCollectedWalletAmount = 0;
        double consolidatedCollectedPaytmQRAmount = 0;
        double consolidatedReturnedCODAmount = 0;
        double consolidatedReturnedOPAmount = 0;
        double consolidatedReturnedWalletAmount = 0;
        double consolidatedReturnedPaytmQRAmount = 0;

        double pendingCollectedCODAmount = 0;
        double pendingCollectedOPAmount = 0;
        double pendingCollectedWalletAmount = 0;
        double pendingCollectedPaytmQRAmount = 0;
        double pendingReturnedCODAmount = 0;
        double pendingReturnedOPAmount = 0;
        double pendingReturnedWalletAmount = 0;
        double pendingReturnedPaytmQRAmount = 0;

        List<InfoDetails> deliveryBoysList = new ArrayList<>();
        List<InfoDetails> consolidatorsList = new ArrayList<>();


        InfoDetails(){}
        InfoDetails (String id, String name){
            this.id = id;
            this.name = name;
        }


        InfoDetails addMoneyCollectionAmount(MoneyCollectionTransactionDetails transactionDetails){
            double amount = transactionDetails.getAmount();
            // OP
            if (transactionDetails.getPaymentTypeString().equalsIgnoreCase(Constants.ONLINE_PAYMENT) || transactionDetails.getPaymentTypeString().equalsIgnoreCase(Constants.PAYTM_WALLET)) {
                if ( amount >= 0 ) {
                    collectedOPAmount += amount;
                    if ( transactionDetails.isConsolidated() ){
                        consolidatedCollectedOPAmount += amount;
                    }else{
                        pendingCollectedOPAmount += amount;
                    }
                }else{
                    returnedOPAmount += Math.abs(amount);
                    if ( transactionDetails.isConsolidated() ){
                        consolidatedReturnedOPAmount += Math.abs(amount);
                    }else{
                        pendingReturnedOPAmount += Math.abs(amount);
                    }
                }
            }
            // WALLET
            else if (transactionDetails.getPaymentTypeString().equalsIgnoreCase(Constants.WALLET)) {
                if ( amount >= 0 ) {
                    collectedWalletAmount += amount;
                    if ( transactionDetails.isConsolidated() ){
                        consolidatedCollectedWalletAmount += amount;
                    }else{
                        pendingCollectedWalletAmount += amount;
                    }
                }else{
                    returnedWalletAmount += Math.abs(amount);
                    if ( transactionDetails.isConsolidated() ){
                        consolidatedReturnedWalletAmount += Math.abs(amount);
                    }else{
                        pendingReturnedWalletAmount += Math.abs(amount);
                    }
                }
            }
            // PAYTM QR
            else if ( transactionDetails.getPaymentTypeString().equalsIgnoreCase(Constants.PAYTM_QR) ){
                if ( amount >= 0 ) {
                    collectedPaytmQRAmount += amount;
                    if ( transactionDetails.isConsolidated() ){
                        consolidatedCollectedPaytmQRAmount += amount;
                    }else{
                        pendingCollectedPaytmQRAmount += amount;
                    }
                }else{
                    returnedPaytmQRAmount += Math.abs(amount);
                    if ( transactionDetails.isConsolidated() ){
                        consolidatedReturnedPaytmQRAmount += Math.abs(amount);
                    }else{
                        pendingReturnedPaytmQRAmount += Math.abs(amount);
                    }
                }
            }
            // COD
            else{
                if ( amount >= 0 ) {
                    collectedCODAmount += amount;
                    if ( transactionDetails.isConsolidated() ){
                        consolidatedCollectedCODAmount += amount;
                    }else{
                        pendingCollectedCODAmount += amount;
                    }
                }else{
                    returnedCODAmount += Math.abs(amount);
                    if ( transactionDetails.isConsolidated() ){
                        consolidatedReturnedCODAmount += Math.abs(amount);
                    }else{
                        pendingReturnedCODAmount += Math.abs(amount);
                    }
                }
            }
            return this;
        }

        InfoDetails addToDeliveryBoysList(MoneyCollectionTransactionDetails transactionDetails){
            //if ( transactionDetails.getDeliveryBoyID() != null && transactionDetails.getDeliveryBoyID().length() > 0 ) {
                InfoDetails deliveryBoyDetails = null;
                for (int i=0;i<deliveryBoysList.size();i++){
                    if ( deliveryBoysList.get(i).id.equals(transactionDetails.getDeliveryBoyID()) ){
                        deliveryBoyDetails = deliveryBoysList.get(i);
                        break;
                    }
                }
                if ( deliveryBoyDetails == null ){
                    deliveryBoyDetails = new InfoDetails(transactionDetails.getDeliveryBoyDetails().getID(), transactionDetails.getDeliveryBoyDetails().getAvailableName());
                    deliveryBoysList.add(deliveryBoyDetails);
                }

                deliveryBoyDetails.addMoneyCollectionAmount(transactionDetails);
            //}
            return this;
        }

        InfoDetails addToConsolidatorsList(MoneyCollectionTransactionDetails transactionDetails){
            //if ( transactionDetails.isConsolidated() && transactionDetails.getConsolidatorID() != null && transactionDetails.getConsolidatorID().length() > 0 ) {
                InfoDetails consolidatorDetails = null;
                for (int i=0;i<consolidatorsList.size();i++){
                    if ( consolidatorsList.get(i).id.equals(transactionDetails.getConsolidatorID()) ){
                        consolidatorDetails = consolidatorsList.get(i);
                        break;
                    }
                }
                if ( consolidatorDetails == null ){
                    consolidatorDetails  = new InfoDetails(transactionDetails.getConsolidatorDetails().getID(), transactionDetails.getConsolidatorDetails().getAvailableName());
                    consolidatorsList.add(consolidatorDetails);
                }

                consolidatorDetails.addMoneyCollectionAmount(transactionDetails);
            //}
            return this;
        }

    }
}