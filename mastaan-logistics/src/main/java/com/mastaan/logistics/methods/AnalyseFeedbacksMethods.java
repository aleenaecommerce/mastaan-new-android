package com.mastaan.logistics.methods;

import android.util.Log;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.MeatItemDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by venkatesh on 22/7/16.
 */
public class AnalyseFeedbacksMethods {

    List<FeedbackDetails> itemsList = new ArrayList<>();

    long totalCount = 0;
    String startDate = "";
    String endDate = "";

    List<String> categoriesList = new ArrayList<>();
    List<InfoDetails> groupedTotalInfosList = new ArrayList<>();
    List<InfoDetails> groupedCategoryInfosList = new ArrayList<>();

    public AnalyseFeedbacksMethods(List<FeedbackDetails> itemsList){
        if ( itemsList == null ){   itemsList = new ArrayList<>();  }
        this.itemsList = itemsList;
    }


    public AnalyseFeedbacksMethods analyse(){

        Map<Float, InfoDetails> totalInfosKeyMapSet = new HashMap<>();
        Map<String, InfoDetails> categoryInfosKeyMapSet = new HashMap<>();

        for (int i=0;i<itemsList.size();i++){
            FeedbackDetails feedbackDetails = itemsList.get(i);

            if ( feedbackDetails != null && feedbackDetails.getOrderItemDetails() != null ){
                OrderItemDetails orderItemDetails = feedbackDetails.getOrderItemDetails();
                OrderDetails2 orderDetails2 = orderItemDetails.getOrderDetails();
                MeatItemDetails meatItemDetails = orderItemDetails.getMeatItemDetails();

                try {
                    if ( categoriesList.contains(meatItemDetails.getParentCategory()) == false ){
                        categoriesList.add(meatItemDetails.getParentCategory());
                    }

                    String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()));

                    totalCount += 1;
                    if (startDate == null || startDate.length() == 0   || DateMethods.compareDates(startDate, deliveryDate) > 0 ){
                        startDate = deliveryDate;
                    }
                    if (endDate == null || endDate.length() == 0   || DateMethods.compareDates(deliveryDate, endDate) > 0 ){
                        endDate = deliveryDate;
                    }

                    //----- TOTAL INFO -----

                    if ( totalInfosKeyMapSet.containsKey(feedbackDetails.getCustomerRating()) == false ) {   // ADDING TO MAPSET IF NOT PRESENT
                        InfoDetails totalInfoDetails = new InfoDetails(feedbackDetails.getCustomerRating());
                        totalInfosKeyMapSet.put(feedbackDetails.getCustomerRating(), totalInfoDetails);
                    }

                    // WITH COMMENTS
                    if ( feedbackDetails.getCustomerComments() != null && feedbackDetails.getCustomerComments().trim().length() > 0 ){
                        totalInfosKeyMapSet.get(feedbackDetails.getCustomerRating()).increaseWithCommentsCount();
                    }
                    // WITHOUT COMMENTS
                    else {
                        totalInfosKeyMapSet.get(feedbackDetails.getCustomerRating()).increaseWithoutCommentsCount();
                    }


                    //------ CATEGORY ----

                    if ( categoryInfosKeyMapSet.containsKey(meatItemDetails.getParentCategory()) == false ) {   // ADDING TO MAPSET IF NOT PRESENT
                        InfoDetails categoryInfoDetails = new InfoDetails(meatItemDetails.getParentCategory());
                        categoryInfosKeyMapSet.put(meatItemDetails.getParentCategory(), categoryInfoDetails);
                    }
                    int containIndex = -1;
                    InfoDetails infoDetails = categoryInfosKeyMapSet.get(meatItemDetails.getParentCategory());
                    for (int j=0;j<infoDetails.getRatingsList().size();j++){
                        if ( containIndex == -1 ) {
                            InfoDetails ratingDetails = infoDetails.getRatingsList().get(j);
                            if (ratingDetails.getRating() == feedbackDetails.getCustomerRating()) {
                                containIndex = j;
                            }
                        }
                    }

                    if ( containIndex != -1 ){
                        if ( feedbackDetails.getCustomerComments() != null && feedbackDetails.getCustomerComments().trim().length() > 0 ) {
                            categoryInfosKeyMapSet.get(meatItemDetails.getParentCategory()).getRatingsList().get(containIndex).increaseWithCommentsCount();
                        }else{
                            categoryInfosKeyMapSet.get(meatItemDetails.getParentCategory()).getRatingsList().get(containIndex).increaseWithoutCommentsCount();
                        }
                    }else{
                        InfoDetails ratingDetails = new InfoDetails(feedbackDetails.getCustomerRating());
                        if ( feedbackDetails.getCustomerComments() != null && feedbackDetails.getCustomerComments().trim().length() > 0 ) {
                            ratingDetails.increaseWithCommentsCount();
                        }else{
                            ratingDetails.increaseWithoutCommentsCount();
                        }
                        categoryInfosKeyMapSet.get(meatItemDetails.getParentCategory()).addToRatingsList(ratingDetails);
                    }

                    //-----------


                }catch (Exception e){
                    e.printStackTrace();
                    Log.d(Constants.LOG_TAG, "ANALYSE_FEEDBACKS: Exception: "+e);
                }

            }

        }

        //-------

        Collections.sort(categoriesList, new Comparator<String>() {
            public int compare(String item1, String item2) {
                return item1.compareToIgnoreCase(item2);
            }
        });

        //-------

        for (Float key : totalInfosKeyMapSet.keySet()) {
            groupedTotalInfosList.add(totalInfosKeyMapSet.get(key));
        }

        //------

        for (String key : categoryInfosKeyMapSet.keySet()) {
            groupedCategoryInfosList.add(categoryInfosKeyMapSet.get(key));
        }

        Collections.sort(groupedCategoryInfosList, new Comparator<InfoDetails>() {
            public int compare(InfoDetails item1, InfoDetails item2) {
                return item1.getCategory().compareToIgnoreCase(item2.getCategory());
            }
        });

        return this;
    }

    //----------


    public List<String> getCategoriesList() {
        if (categoriesList == null ){ return new ArrayList<>(); }
        return categoriesList;
    }

    public String getTotalInfo(){

        String totalInfoDetails = "";

        totalInfoDetails += "Total count: <b>"+totalCount+"</b>";
        totalInfoDetails += "<br>From date: <b>"+DateMethods.getDateInFormat(startDate, DateConstants.MMM_DD_YYYY)+"</b>";
        totalInfoDetails += "<br>End date: <b>"+DateMethods.getDateInFormat(endDate, DateConstants.MMM_DD_YYYY)+"</b>";
        //totalInfoDetails += "<br>";

        for (int i=0;i<groupedTotalInfosList.size();i++){
            totalInfoDetails += "<br><br>"+getInfoDetailsString(groupedTotalInfosList.get(i));
        }

        return totalInfoDetails;
    }

    public List<String> getCategoryInfoDetailsList(){
        List<String> categoryInfoDetailsList = new ArrayList<>();

        for (int i=0;i<groupedCategoryInfosList.size();i++){
            String categoryInfoDetailsString = "";
            long totalCount = 0;
            for (int j=0;j<groupedCategoryInfosList.get(i).getRatingsList().size();j++){
                totalCount += groupedCategoryInfosList.get(i).getRatingsList().get(j).getWithCommentsCount() + groupedCategoryInfosList.get(i).getRatingsList().get(j).getWithoutCommentsCount();
                categoryInfoDetailsString += "<br><br>"+getInfoDetailsString(groupedCategoryInfosList.get(i).getRatingsList().get(j));
            }
            categoryInfoDetailsList.add("Total count: <b>"+totalCount+"</b>"+categoryInfoDetailsString);
        }

        return categoryInfoDetailsList;
    }

    public String getRatingsSummaryInfo(){
        String ratingsSummary = "";

        double totalRatingsSum = 0;
        long totalRatingsCount = 0;
        long lowRatingCount = 0;
        long averageRatingsCount = 0;
        long highRatingsCount = 0;

        for (int i=0;i<itemsList.size();i++){
            FeedbackDetails feedbackDetails = itemsList.get(i);
            if ( feedbackDetails != null ){
                totalRatingsSum += feedbackDetails.getCustomerRating();
                totalRatingsCount ++;

                if ( feedbackDetails.getCustomerRating() == 1 ){
                    lowRatingCount ++;
                }else if ( feedbackDetails.getCustomerRating() == 3 ){
                    averageRatingsCount ++;
                }else if ( feedbackDetails.getCustomerRating() == 5 ){
                    highRatingsCount ++;
                }
            }
        }

        ratingsSummary += "Overall Rating: <b>"+CommonMethods.getInDecimalFormat(totalRatingsSum/totalRatingsCount)+"</b><br>";

        if ( highRatingsCount > 0 ){
            ratingsSummary += "<br>High rated: <b>"+CommonMethods.getIndianFormatNumber(highRatingsCount)+"</b>";
        }
        if ( averageRatingsCount > 0 ){
            ratingsSummary += "<br>Average rated: <b>"+CommonMethods.getIndianFormatNumber(averageRatingsCount)+"</b>";
        }
        if ( lowRatingCount > 0 ){
            ratingsSummary += "<br>Low rated: <b>"+CommonMethods.getIndianFormatNumber(lowRatingCount)+"</b>";
        }

        return ratingsSummary;
    }

    //-----

    public String getInfoDetailsString(InfoDetails ratingInfoDetails){

        String totalInfoDetails = "";

        if ( ratingInfoDetails.getRating() == Constants.LOW_RATED ){
            totalInfoDetails += "<u><b>Low Rated:</u></b> (<b>"+(ratingInfoDetails.getWithCommentsCount()+ratingInfoDetails.getWithoutCommentsCount())+"</b>)";
        }else if ( ratingInfoDetails.getRating() == Constants.AVERAGE_RATED ){
            totalInfoDetails += "<u><b>Average Rated:</u></b> (<b>"+(ratingInfoDetails.getWithCommentsCount()+ratingInfoDetails.getWithoutCommentsCount())+"</b>)";
        }else if ( ratingInfoDetails.getRating() == Constants.HIGH_RATED ){
            totalInfoDetails += "<u><b>High Rated:</u></b> (<b>"+(ratingInfoDetails.getWithCommentsCount()+ratingInfoDetails.getWithoutCommentsCount())+"</b>)";
        }else{
            totalInfoDetails += "<u><b>Rating "+ CommonMethods.getInDecimalFormat(ratingInfoDetails.getRating())+":</u></b> (<b>"+(ratingInfoDetails.getWithCommentsCount()+ratingInfoDetails.getWithoutCommentsCount())+"</b>)";
        }

        totalInfoDetails += "<br>";
        if ( ratingInfoDetails.getWithCommentsCount() > 0 ){
            totalInfoDetails += "WithComments: <b>"+ratingInfoDetails.getWithCommentsCount()+"</b>";
        }
        if ( ratingInfoDetails.getWithoutCommentsCount() > 0 ){
            if ( ratingInfoDetails.getWithCommentsCount() > 0 ){    totalInfoDetails += ", ";   }
            totalInfoDetails += "WithoutComments: <b>"+ratingInfoDetails.getWithoutCommentsCount()+"</b>";
        }

        return totalInfoDetails;
    }


    //===============

    class InfoDetails{

        String category;
        List<InfoDetails> ratingsList;

        double rating;
        //long ratingCount;
        long withCommentsCount;
        long withoutCommentsCount;

        InfoDetails(String category){
            this.category = category;
            this.ratingsList = new ArrayList<>();
        }

        InfoDetails(double rating){
            this.rating = rating;
        }

        public String getCategory() {
            return category;
        }

        public void addToRatingsList(InfoDetails ratingDetails){
            if ( ratingsList == null ){ ratingsList = new ArrayList<>();    }
            ratingsList.add(ratingDetails);
        }
        public List<InfoDetails> getRatingsList() {
            return ratingsList;
        }

        public double getRating() {
            return rating;
        }

        public long getWithCommentsCount() {
            return withCommentsCount;
        }

        public void increaseWithCommentsCount(){
            withCommentsCount += 1;
        }

        public long getWithoutCommentsCount() {
            return withoutCommentsCount;
        }

        public void increaseWithoutCommentsCount(){
            withoutCommentsCount += 1;
        }
    }
}