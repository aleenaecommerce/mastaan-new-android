package com.mastaan.logistics.interfaces;

import com.mastaan.logistics.models.StockDetails;

/**
 * Created by Venkatesh Uppu on 3/12/16.
 */
public interface StockDetailsCallback {
    void onComplete(boolean status, int statusCode, String message, StockDetails stockDetails);
}
