package com.mastaan.logistics.interfaces;

import com.mastaan.logistics.models.AttributeOptionDetails;

/**
 * Created by venkatesh on 12/1/16.
 */
public interface AttributeOptionCallbak {
    void onSelect(int previousPosition, int currentPosition, AttributeOptionDetails attributeOptionDetails);
}
