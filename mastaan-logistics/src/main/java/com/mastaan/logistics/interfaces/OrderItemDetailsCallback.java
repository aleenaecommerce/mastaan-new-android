package com.mastaan.logistics.interfaces;

import com.mastaan.logistics.models.OrderItemDetails;

import java.util.List;

/**
 * Created by venkatesh on 5/8/16.
 */
public interface OrderItemDetailsCallback {
    void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails);
}
