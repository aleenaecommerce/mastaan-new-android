package com.mastaan.logistics.interfaces;

import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.List;

/**
 * Created by venkatesh on 5/8/16.
 */
public interface OrdersListCallback {
    void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList);
}
