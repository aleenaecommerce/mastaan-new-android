package com.mastaan.logistics.interfaces;

import com.mastaan.logistics.models.UserDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 8/12/16.
 */
public interface UsersCallback {
    void onComplete(boolean status, int statusCode, String message, List<UserDetails> usersList);
}
