package com.mastaan.logistics.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.util.Log;

import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.services.vService;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.mastaan.logistics.BuildConfig;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.CustomerSupportActivity;
import com.mastaan.logistics.backend.BackendAPIs;
import com.mastaan.logistics.backend.CommonAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class    LogisticsService extends vService {
    boolean isServiceActive = true;

    int autoRefreshTimeOutInSeconds = 60;

    String firebaseToken;
    Firebase firebaseCustomerSupport;
    Firebase firebaseLocationTracking;
    Firebase firebaseCheckinStatusTracking;

    Context context;
    LocalStorageData localStorageData;
    BackendAPIs backendAPIs;
    UserDetails userDetails;

    //FusedLocationService fusedLocationService;

    int notificationIconID;
    int notificationIndex = 0;

    public LogisticsService() {}

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(Constants.LOG_TAG, "FIRE_BASE Service is CREATED");

        Firebase.setAndroidContext(this);
        //Firebase.getDefaultConfig().setPersistenceEnabled(true);

        context = getApplicationContext();
        localStorageData = new LocalStorageData(context);
        backendAPIs = new BackendAPIs(context, getDeviceID(), getAppVersionCode());
        userDetails = new LocalStorageData(context).getUserDetails();

        registerServiceReceiver(Constants.FIREBASE_RECEIVER);

        if ( BuildConfig.PRODUCTION == false ){
            autoRefreshTimeOutInSeconds = 5;
        }
        startAutoRefreshTimer(autoRefreshTimeOutInSeconds);

        //-------------------

        firebaseToken = localStorageData.getFireBaseToken();
        if (firebaseToken.length() == 0) {
            backendAPIs.getCommonAPI().getFirebaseToken(new CommonAPI.FirebaseTokenCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String firebase_token) {
                    if (status) {
                        firebaseToken = firebase_token;
                        localStorageData.setFireBaseToken(firebaseToken);
                        setupFireBase();
                    }
                }
            });
        } else {
            setupFireBase();
        }

        notificationIconID = R.drawable.ic_app_notification;
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationIconID = R.drawable.ic_app_notification;
        }*/

        //---------

        if ( localStorageData.getSessionFlag() && localStorageData.isUserCheckedIn() ){
            showPinnedNotification();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(Constants.LOG_TAG, "FIRE_BASE Service is STARTED");
        return Service.START_STICKY;
    }

    @Override
    public void onServiceReceiverMessage(String serviceReceiverName, Intent receivedData) {
        super.onServiceReceiverMessage(serviceReceiverName, receivedData);

        /*if ( isServiceActive == true ) {
            try {
                if ( userDetails.isCustomerRelationshipManager() ) {
                    if (receivedData.getStringExtra("type").equalsIgnoreCase(Constants.ADD_CS_LISTENER_FOR_ORDER_ITEM)) {
                        OrderItemDetails orderItemDetails = new Gson().fromJson(receivedData.getStringExtra(Constants.ORDER_ITEM_DETAILS), OrderItemDetails.class);
                        setupCustomerSupportFirebaseUpdatesForOrderItem(orderItemDetails);
                    } else if (receivedData.getStringExtra("type").equalsIgnoreCase(Constants.CLEAR_CS_NOTIFICATIONS_CACHE)) {
                        if (customerSupportNotificationsMessages != null) {
                            customerSupportNotificationsMessages.clear();
                        }
                        try {
                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.cancel(customerSupportNotificationIndex);
                        } catch (Exception e) {
                        }
                    }
                }
            } catch (Exception e) {}
        }*/
    }

    @Override
    public void onAutoRefreshTimeElapsed() {
        super.onAutoRefreshTimeElapsed();
        localStorageData.setServerTime(DateMethods.addToDateInSeconds(localStorageData.getServerTime(), autoRefreshTimeOutInSeconds));

        try {
            if (localStorageData.getSessionFlag() && localStorageData.isUserCheckedIn()) {
//                if (fusedLocationService == null) {
//                    fusedLocationService = new FusedLocationService(context, true, null);
//                }
//                Location currentLocation = fusedLocationService.getCurrentLocation();
                new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                    @Override
                    public void onComplete(Location currentLocation) {
                        if ( currentLocation != null ){
                            Map<String, Object> location_map = new HashMap<>();
                            location_map.put("lat", currentLocation.getLatitude());
                            location_map.put("lng", currentLocation.getLongitude());
                            location_map.put("time", System.currentTimeMillis());

                            if ( firebaseLocationTracking != null ){
                                firebaseLocationTracking.setValue(location_map);
                                //Log.d(Constants.LOG_TAG, "Location_Tracking: Sending current location "+currentLocation.getLatitude()+","+currentLocation.getLongitude()+ " @ "+System.currentTimeMillis());
                            }
                        }else{
                            Log.d(Constants.LOG_TAG, "Location_Tracking: Current location is null");
                        }
                    }
                });
            } else {
                closePinnedNotification();
//                if (fusedLocationService != null) {
//                    Log.d(Constants.LOG_TAG, "Stopping fused location as user is not checked in");
//                    try{fusedLocationService.stop();}catch (Exception e){}
//                    fusedLocationService = null;
//                }
            }
        }catch (Exception e){e.printStackTrace();}

        //-----

        //showPinnedNotification(true, false);
    }

    public void setupFireBase() {

        if ( userDetails.isCustomerRelationshipManager() ) {
            firebaseCustomerSupport = new Firebase(getString(R.string.firebase_customer_suppor_url) + "/" + localStorageData.getWarehouseID());
            firebaseCustomerSupport.authWithCustomToken(firebaseToken, new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    Log.d(Constants.LOG_TAG, "FIRE_BASE : {Customer_Support} Authentication is SUCCESS with Token = " + firebaseToken);
                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    Log.d(Constants.LOG_TAG, "FIRE_BASE : {Customer_Support} Authentication is FAILED with Token = " + firebaseToken);
                }
            });
        }

        firebaseCheckinStatusTracking = new Firebase(getString(R.string.firebase_checkin_status_tracking_url)+"/"+localStorageData.getUserID());
        firebaseCheckinStatusTracking.authWithCustomToken(firebaseToken, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                Log.d(Constants.LOG_TAG, "FIRE_BASE : {Checkin_Status_Tracking} Authentication is SUCCESS with Token = " + firebaseToken);
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                Log.d(Constants.LOG_TAG, "FIRE_BASE : {Checkin_Status_Tracking} Authentication is FAILED with Token = " + firebaseToken);
            }
        });
        firebaseCheckinStatusTracking.addValueEventListener(new ValueEventListener() {
            boolean listenForUpdates;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if ( listenForUpdates ) {
                    boolean isCheckedIn = dataSnapshot.child("checkedin").getValue(Boolean.class);
                    localStorageData.setUser(userDetails.setCheckIn(isCheckedIn, localStorageData.getServerTime()));
                    if ( isCheckedIn ) {
                        onAutoRefreshTimeElapsed();
                        showPinnedNotification();
                    }else{
                        closePinnedNotification();
                    }
                }
                listenForUpdates = true;
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        firebaseLocationTracking = new Firebase(getString(R.string.firebase_location_tracking_url)+"/"+localStorageData.getUserID());
        firebaseLocationTracking.authWithCustomToken(firebaseToken, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                Log.d(Constants.LOG_TAG, "FIRE_BASE : {Location_Tracking} Authentication is SUCCESS with Token = " + firebaseToken);
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                Log.d(Constants.LOG_TAG, "FIRE_BASE : {Location_Tracking} Authentication is FAILED with Token = " + firebaseToken);
            }
        });

        /*if ( userDetails.isCustomerRelationshipManager() ){
            Log.d(Constants.LOG_TAG, "FIRE_BASE : Setting up customer support child listener as user is CRM");
            firebaseCustomerSupport.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final long intialItemsCount = dataSnapshot.getChildrenCount();

                    firebaseCustomerSupport.addChildEventListener(new ChildEventListener() {
                        int count = 0;

                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            try{
                                count++;
                                String status = dataSnapshot.child("status").getValue(String.class);
                                if ( status == null ){  status = "";    }
                                boolean isRequestedByCustomer = false;
                                try{ isRequestedByCustomer = dataSnapshot.child("csbyr").getValue(Boolean.class);   }catch (Exception e){}

                                if ( count > intialItemsCount ){
                                    if (isServiceActive && status.equalsIgnoreCase(Constants.PENDING) ) {
                                        Log.d(Constants.LOG_TAG, "FIRE_BASE :  New item is added in customer support, S: "+status);// "+count+"/"+intialItemsCount);
                                        showCustomerSupportNotification("Customer support", isRequestedByCustomer?"Customer requested support.":"New item is added.");
                                    }
                                }

                                if ( count == intialItemsCount ){
                                    Log.d(Constants.LOG_TAG, "FIRE_BASE :  Done loading initial data");
                                }
                            }catch (Exception e){e.printStackTrace();}
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                            try{
                                count++;
                                String status = dataSnapshot.child("status").getValue(String.class);
                                if ( status == null ){  status = "";    }
                                boolean isRequestedByCustomer = false;
                                try{ isRequestedByCustomer = dataSnapshot.child("csbyr").getValue(Boolean.class);   }catch (Exception e){}

                                if ( count > intialItemsCount ){
                                    if (isServiceActive && status.equalsIgnoreCase(Constants.PENDING) ) {
                                        Log.d(Constants.LOG_TAG, "FIRE_BASE :  Item is removed from customer support, S: "+status);
                                        showCustomerSupportNotification("Customer support", isRequestedByCustomer?"Customer item is removed":"Item is removed.");
                                    }
                                }

                                if ( count == intialItemsCount ){
                                    Log.d(Constants.LOG_TAG, "FIRE_BASE :  Done loading initial data");
                                }
                            }catch (Exception e){e.printStackTrace();}
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {}
                    });
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

        }else{
            Log.d(Constants.LOG_TAG, "FIRE_BASE : Not setting up customer support child listener as user is not CRM");
        }*/
    }


    /*public void setupCustomerSupportFirebaseUpdatesForOrderItem(final OrderItemDetails orderItemDetails){

        if ( orderItemDetails != null && orderItemDetails.getID() != null && orderItemDetails.getID().length() > 0 ) {
            ++notificationIndex;

            Log.d(Constants.LOG_TAG, "FIRE_BASE :  Listening Updates for OrderItem = " + orderItemDetails.getID() + " with NtfIndex = " + notificationIndex);
            firebaseCustomerSupport.child(orderItemDetails.getID()).addValueEventListener(new ValueEventListener() {
                boolean listenForUpdates;
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (isServiceActive == true && dataSnapshot.getValue() != null && listenForUpdates == true) {
                        String currentStatus = dataSnapshot.getValue().toString();
                        Log.d(Constants.LOG_TAG, "FIRE_BASE : Update For Order Item("+orderItemDetails.getID()+") >> "+currentStatus);

                        if ( currentStatus.equalsIgnoreCase(Constants.PROCESSING) ) {

                        }
                        else if ( currentStatus.equalsIgnoreCase(Constants.PROCESSED) ) {
                            String infoString = "Processed "+CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+" of "+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName());
                            showCustomerSupportNotification( "Customer support", infoString);//"Items you assigned to customer support are processed."

                            Log.d(Constants.LOG_TAG, "FIRE_BASE :  Removed Listening Updates for OrderItem = " + orderItemDetails.getID());
                            firebaseCustomerSupport.child(orderItemDetails.getID()).removeEventListener(this); // Removing
                        }
                    }
                    listenForUpdates = true;
                }

                @Override
                public void onCancelled(FirebaseError error) {
                }
            });
        }
    }*/

    //------

    List<String> customerSupportNotificationsMessages = new ArrayList<>();
    int customerSupportNotificationIndex = 128;

    public void showCustomerSupportNotification(String title, String message){
        if ( customerSupportNotificationsMessages == null ){    customerSupportNotificationsMessages = new ArrayList<>();   }
        if ( customerSupportNotificationsMessages.size() == 0 ){
            customerSupportNotificationsMessages.add(message);
        }else{
            customerSupportNotificationsMessages.add(0, message);
        }

        Notification.Builder customerSupportNotificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_app_notification)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL);
        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        inboxStyle.setBigContentTitle(title);
        int showMessagesUpto = customerSupportNotificationsMessages.size();
        if ( showMessagesUpto > 6 ){
            showMessagesUpto = 6;
            inboxStyle.setSummaryText("+"+(customerSupportNotificationsMessages.size()-6)+" more");
        }
        for (int i=0;i<showMessagesUpto;i++) {
            inboxStyle.addLine(customerSupportNotificationsMessages.get(i));
        }
        customerSupportNotificationBuilder.setStyle(inboxStyle);

        customerSupportNotificationBuilder.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(getApplicationContext(), CustomerSupportActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(customerSupportNotificationIndex, customerSupportNotificationBuilder.build());
    }

    int pinnedNotificationIndex = 177;
    public void showPinnedNotification(){
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_app_notification)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setContentText(localStorageData.getUserName())
                .setPriority(Notification.PRIORITY_MIN)
                .setOngoing(true);
        try{    this.startForeground(pinnedNotificationIndex, notificationBuilder.build());  }catch (Exception e){}
    }
    public void closePinnedNotification(){
        this.stopForeground(true);
    }

    //================

    @Override
    public void onDestroy() {
        super.onDestroy();
        isServiceActive = false;
        try{this.stopForeground(true);}catch (Exception e){}
        Log.d(Constants.LOG_TAG, "FIRE_BASE Service is FINISHED");
    }

}







/*

    final static String GROUP_CUSOMER_SUPPORT_NOTIFICATIONS = "GROUP_CUSOMER_SUPPORT_NOTIFICATIONS";
    int customerSupportNotificationID = 1;
    NotificationManagerCompat notificationManager;

    public void showCustomerSupportNotification(String title, String message){
        Notification notification = new NotificationCompat.Builder(getApplicationContext())
                .setContentTitle(message)
                .setContentText("Text")
                .setSmallIcon(R.drawable.ic_app_notification)
                .setGroup(GROUP_CUSOMER_SUPPORT_NOTIFICATIONS)
                .setGroupSummary(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .build();

        //NotificationManager customerSupportNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //customerSupportNotificationManager.notify(customerSupportNotificationID, notification);
        if ( notificationManager  == null ){
            notificationManager = NotificationManagerCompat.from(getApplicationContext());
        }
        notificationManager.cancelAll();
        notificationManager.notify(customerSupportNotificationID, notification);
        customerSupportNotificationID += 1;

    }*/

