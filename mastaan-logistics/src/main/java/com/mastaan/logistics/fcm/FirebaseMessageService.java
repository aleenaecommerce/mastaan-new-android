package com.mastaan.logistics.fcm;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.aleena.common.methods.NotificationMethods;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.CustomerSupportActivity;
import com.mastaan.logistics.activities.FeedbackDetailsActivity;
import com.mastaan.logistics.activities.FeedbacksActivity;
import com.mastaan.logistics.activities.MeatItemDetailsActivity;
import com.mastaan.logistics.activities.OrderDetailsActivity;
import com.mastaan.logistics.activities.PendingCashNCarryActivity;
import com.mastaan.logistics.activities.StockAlertsActivity;
import com.mastaan.logistics.constants.Constants;

import java.util.Random;

/**
 * Created in Mastaan Buyer on 13/12/17.
 * @author Venkatesh Uppu
 */

public class FirebaseMessageService extends FirebaseMessagingService {

    String TAG = "FIREBASE_MESSAGING_SERVICE : ";
    Context context;

    final String MEAT_ITEM = "meatitem";
    final String ORDER = "order";
    final String CASHNCARRY = "cashncarry";
    final String STOCK = "stock";
    final String SUPPORT = "support";
    final String FEEDBACK = "feedback";


    final String NOTIFICATION_TITLE = "gcm.notification.title";
    final String NOTIFICATION_DETAILS = "gcm.notification.body";
    final String ID = "id";
    final String TITLE = "title";
    final String DETAILS = "details";
    final String USER = "user";
    final String URL = "url";


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(Constants.LOG_TAG, TAG+"Created");
        context = getApplicationContext();
    }

    public void handleIntent(Intent intent) {
        try{
            Log.d(Constants.LOG_TAG, TAG+ "Message Received with handleIntent, D: "+intent.getExtras());

            int icon = R.drawable.ic_app_notification;
            int iconBackground = ContextCompat.getColor(context, R.color.color_seafood);

            String type = intent.getStringExtra("type");//channel_id
            if ( type == null ){    type = "";  }
            String title = intent.getStringExtra(NOTIFICATION_TITLE);
            String details = intent.getStringExtra(NOTIFICATION_DETAILS);

            if ( title != null || details != null ){
                if ( title == null ){   title = ""; }
                if ( details == null ){   details = ""; }

                // ITEM
                if ( type.trim().equalsIgnoreCase(MEAT_ITEM) ){
                    Log.d(Constants.LOG_TAG, TAG+ "Meat item message");

                    String warehouseMeatItem = intent.getStringExtra(MEAT_ITEM);

                    Intent clickActivity = null;
                    if ( warehouseMeatItem != null && warehouseMeatItem.trim().length() > 0 ){
                        clickActivity = new Intent(context, MeatItemDetailsActivity.class);
                        clickActivity.putExtra(Constants.ID, warehouseMeatItem);
                    }

                    // Showing notification
                    new NotificationMethods(context).showNotification(icon, iconBackground, new Random().nextInt(), title, details, null, clickActivity);
                }

                // ORDER
                else if ( type.trim().equalsIgnoreCase(ORDER) ){
                    Log.d(Constants.LOG_TAG, TAG+ "Order message");

                    String order = intent.getStringExtra(ORDER);

                    Intent clickActivity = null;
                    if ( order != null && order.trim().length() > 0 ){
                        clickActivity = new Intent(context, OrderDetailsActivity.class);
                        clickActivity.putExtra(Constants.ORDER_ID, order);
                    }

                    // Showing notification
                    new NotificationMethods(context).showNotification(icon, iconBackground, new Random().nextInt(), title, details, null, clickActivity);
                }

                // CASH N CARRY
                else if ( type.trim().equalsIgnoreCase(CASHNCARRY) ) {
                    Log.d(Constants.LOG_TAG, TAG + "Cash & Carry message");

                    // Showing notification
                    new NotificationMethods(context).showNotification(icon, iconBackground, new Random().nextInt(), title, details, null, new Intent(context, PendingCashNCarryActivity.class));
                }

                // STOCK
                else if ( type.trim().equalsIgnoreCase(STOCK) ) {
                    Log.d(Constants.LOG_TAG, TAG + "Stock message");

                    Intent clickActivity = new Intent(context, StockAlertsActivity.class);

                    // Showing notification
                    new NotificationMethods(context).showNotification(icon, iconBackground, new Random().nextInt(), title, details, null, clickActivity);
                }

                // SUPPORT
                else if ( type.trim().equalsIgnoreCase(SUPPORT) ) {
                    Log.d(Constants.LOG_TAG, TAG + "Support message");

                    // Showing notification
                    new NotificationMethods(context).showNotification(icon, iconBackground, new Random().nextInt(), title, details, null, new Intent(context, CustomerSupportActivity.class));
                }

                // FEEDBACK
                else if ( type.trim().equalsIgnoreCase(FEEDBACK) ) {
                    Log.d(Constants.LOG_TAG, TAG + "Feedback message");

                    String feedback = intent.getStringExtra(FEEDBACK);

                    Intent clickActivity = null;
                    if ( feedback != null && feedback.trim().length() > 0 ){
                        clickActivity = new Intent(context, FeedbackDetailsActivity.class);
                        clickActivity.putExtra(Constants.ID, feedback);
                    }else{
                        clickActivity = new Intent(context, FeedbacksActivity.class);
                    }

                    // Showing notification
                    new NotificationMethods(context).showNotification(icon, iconBackground, new Random().nextInt(), title, details, null, clickActivity);
                }

                // OTHER
                else{
                    Log.d(Constants.LOG_TAG, TAG+ "Other message");

                    // Showing notification
                    new NotificationMethods(context).showNotification(icon, iconBackground, new Random().nextInt(), title, details, null, null);
                }
            }
        }
        catch (Exception e){
            Log.d(Constants.LOG_TAG, TAG+"Message Received with handleIntent, Parse Exception, Msg: "+e.getMessage());
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(Constants.LOG_TAG, TAG+"Destroyed");
    }

}