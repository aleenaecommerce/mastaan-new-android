package com.mastaan.logistics.fcm;

import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.mastaan.logistics.backend.BackendAPIs;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;

/**
 * Created in Mastaan Logistics on 30/08/18.
 * @author Venkatesh Uppu
 */

public class FirebaseTokenService extends FirebaseInstanceIdService {

    String TAG = "FIREBASE_TOKEN_SERVICE : ";
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(Constants.LOG_TAG, TAG+"Created");
        context = getApplicationContext();
    }

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        try {
            Log.d(Constants.LOG_TAG, TAG + "Token refresh , T: " + FirebaseInstanceId.getInstance().getToken());

            // Linking buyer to FCM
            if ( new LocalStorageData(context).getSessionFlag() ) {
                new BackendAPIs(context, "", getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionCode).getEmployeesAPI().linkToFCM(FirebaseInstanceId.getInstance().getToken(), null);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(Constants.LOG_TAG, TAG+"Destroyed");
    }

}
