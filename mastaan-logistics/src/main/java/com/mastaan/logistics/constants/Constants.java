package com.mastaan.logistics.constants;

import com.aleena.common.methods.CommonMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 8/9/15.
 */
public final class Constants {

    public static final String LOG_TAG = "MastaanLogisticsLogs";

    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";

    public static final String DATE = "DATE";

    public static final String QUERY = "QUERY";

    public static final String GENERAL = "General";

    public static final String DEFAULT_PREFERENCES_NAME = "LogisticsPreferences";
    public static final String POPUP_ACTION = "LogisticsPreferences";

    public static final String SHOW_WHERE_TO_NEXT_FLOW = "SHOW_WHERE_TO_NEXT_FLOW";

    public static final String HIDE_SUMMARY = "HIDE_SUMMARY";

    public static final int BUYERS_ACTIVITY_CODE = 146;
    public static final int COFIRM_DELIVERY_ACTIVITY_CODE = 147;
    public static final int ORDER_DETAILS_ACTIVITY_CODE = 148;
    public static final int MEAT_ITEM_DETAILS_ACTIVITY_CODE = 149;
    public static final int MEAT_ITEMS_ACTIVITY_CODE = 153;
    public static final int CATEGORIES_ACTIVITY_CODE = 154;
    public static final int EDIT_ORDER_ITEM_ACTIVITY_CODE = 155;
    public static final int ADD_OR_EDIT_WAREHOUSE_ACTIVITY_CODE = 156;
    public static final int ADD_OR_EDIT_HUB_ACTIVITY_CODE = 157;
    public static final int ADD_OR_EDIT_DELIVERY_AREA_ACTIVITY_CODE = 158;
    public static final int ADD_OR_EDIT_HOUSING_SOCIETY_ACTIVITY_CODE = 159;
    public static final int ADD_OR_EDIT_DELIVERY_SLOT_ACTIVITY_CODE = 160;
    public static final int LOCATION_SELECTION_ACTIVITY_CODE = 161;
    public static final int ADD_OR_EDIT_ALERT_MESSAGE_ACTIVITY_CODE = 162;
    public static final int ADD_OR_EDIT_MEMBERSHIP_ACTIVITY_CODE = 163;
    public static final int STOCK_STATEMENT_ACTIVITY_CODE = 164;
    public static final int STOCK_DETAILS_ACTIVITY_CODE = 165;

    public static final String PROFILE = "Profile";
    public static final String KM_DIARY = "KM Diary";
    public static final String DELIVERY_BOYS = "Delivery boys";
    public static final String STOCK = "Stock";
    public static final String CATEGORIES = "Categories";
    public static final String MEAT_ITEMS = "Meat items";
    public static final String DELIVERY_DATES = "Delivery dates";
    public static final String DELIVERY_SLOTS = "Delivery slots";
    public static final String MONEY_COLLECTION = "Money collection";
    public static final String MONEY_CONSOLIDATION = "Money consolidation";
    public static final String PENDING_CONSOLIDATION = "Pending consolidation";
    public static final String PAST_PENDING_ORDES = "Past pending orders";
    public static final String EMPLOYEE_BONUSES = "Employee bonuses";
    public static final String FEEDBACK = "Feedback";
    public static final String FEEDBACKS = "Feedbacks";
    public static final String FOLLOWUPS = "Followups";
    public static final String HUBS = "Hubs";
    public static final String DELIVERY_AREAS = "Delivery areas";
    public static final String HOUSING_SOCIETIES = "Housing societies";
    public static final String CASH_N_CARRY_BY_DATE = "Cash n carry by date";
    public static final String ORDERS_BY_DATE = "Orders by date";
    public static final String FEEDBACKS_BY_DATE = "Feedbacks by date";
    public static final String REPORTS = "Reports";
    public static final String BUYERS = "Buyers";
    public static final String PENDING_FOLLOWUP_BUYERS = "Pending Followup Buyers";
    public static final String PENDING_FIRST_ORDER_FOLLOWUPS = "Pending First Order Followups";
    public static final String QR_CODES = "QR Codes";
    public static final String CONFIGURATIONS = "Configurations";
    public static final String SETTINGS = "Settings";
    //public static final String LOGOUT = "Logout";
    public static final String COMMENTS = "Comments";

    public static final String CHECKIN_REQUIRED_ALERT_MESSAGE = "Please check in from aleena app to continue...";

    public static final String USER_ID = "USER_ID";
    public static final String USER_NAME = "USER_NAME";
    public static final String DESTINATION = "DESTINATION";
    public static final String JOB_DETAILS = "JOB_DETAILS";
    public static final String EMPLOYEE_ID = "EMPLOYEE_ID";
    public static final String ITEM_ID = "ITEM_ID";
    public static final String WAREHOUSE_DETAILS = "WAREHOUSE_DETAILS";
    public static final String HUB_DETAILS = "HUB_DETAILS";
    public static final String SLOT_DETAILS = "SLOT_DETAILS";
    public static final String BUYER_DETAILS = "BUYER_DETAILS";

    public static final String ID = "ID";
    public static final String TYPE = "TYPE";
    public static final String TYPE_NAME = "TYPE_NAME";
    public static final String NAME = "NAME";
    public static final String MESSAGE = "MESSAGE";
    public static final String TAB = "TAB";

    public static final String BASIC = "BASIC";
    public static final String AVAILABILITY = "AVAILABILITY";
    public static final String ATTRIBUTES = "ATTRIBUTES";
    public static final String ATTRIBUTES_OPTIONS = "ATTRIBUTES_OPTIONS";
    public static final String LOGS = "LOGS";

    public static final String OUTSTANDING_BUYERS = "Outstanding buyers";
    public static final String MEATITEM_AVAILABILITY_REQUESTED_BUYERS = "Meat item availability requested buyers";
    public static final String COUPON_BUYERS = "Coupon buyers";
    public static final String REFERRAL_BUYERS = "Referral buyers";

    public static final String BRONZE = "Bronze";
    public static final String SILVER = "Silver";
    public static final String GOLD = "Gold";
    public static final String DIAMOND = "Diamond";
    public static final String PLATINUM = "Platinum";

    public static final String SUMMARY = "Summary";
    public static final String MISMATCH = "Mismatch";

    public static final String CHICKEN = "chicken";
    public static final String FARM_CHICKEN = "farmchicken";
    public static final String COUNTRY_CHICKEN = "countrychicken";

    public static final String MUTTON = "mutton";
    public static final String SHEEP_MUTTON = "sheepmutton";
    public static final String GOAT_MUTTON = "goatmutton";

    public static final String SEAFOOD = "seafood";
    public static final String FRESH_WATER_SF = "freshwatersf";
    public static final String SEA_WATER_SF = "seawatersf";

    public static final String OTHERS = "others";
    public static final String PICKLES = "pickles";
    public static final String MARINADES = "marinades";
    public static final String EGGS = "eggs";
    public static final String READY_TO_EAT = "readytoeat";

    public static final String LOW_RATED_FEEDBACKS = "LOW RATED";
    public static final String AVERAGE_RATED_FEEDBACKS = "AVERAGE RATED";
    public static final String HIGH_RATED_FEEDBACKS = "HIGH RATED";
    public static final String PROCESSING_FEEDBACKS = "PROCESSING";
    public static final String PROCESSED_FEEDBACKS = "PROCESSED";
    public static final String GROUPED_FEEDBACKS = "GROUPED_FEEDBACKS";

    public static final String AMOUNT = "AMOUNT";
    public static final String WEIGHT = "WEIGHT";
    public static final String STATUS = "STATUS";
    public static final String TIME = "TIME";
    public static final String DELAY = "DELAY";
    public static final String AREA = "AREA";
    public static final String VENDOR = "VENDOR";
    public static final String DELIVERY_BOY = "DELIVERY BOY";
    public static final String CONSOLIDATOR = "CONSOLIDATOR";

    public static final String OVERVIEW = "OVERVIEW";
    public static final String CATEGORY = "CATEGORY";
    public static final String WAREHOUSE_MEAT_ITEM = "WAREHOUSE_MEAT_ITEM";
    public static final String EMPLOYEES = "EMPLOYEES";

    public static final String SHOW_ITEM_LEVEL_ATTRIBUTES_OPTIONS_SELECTION = "SHOW_ITEM_LEVEL_ATTRIBUTES_OPTIONS_SELECTION";


    public static final String UNLIMITED = "Unlimited";
    public static final String LIMITED = "Limited";
    public static final String DAYWISE = "Daywise";

    public static final String FILTER = "FILTER";
    public static final String CLEAR = "CLEAR";
    public static final String RELOAD = "RELOAD";
    public static final String BACKGROUND_RELOAD = "BACKGROUND_RELOAD";
    public static final String ANALYSE = "ANALYSE";
    public static final double LOW_RATED = 1;
    public static final double AVERAGE_RATED = 3;
    public static final double HIGH_RATED = 5;
    public static final String WITH_COMMENTS = "With comments";
    public static final String WITHOUT_COMMENTS = "Without comments";
    public static final String CASH_ON_DELIVERY = "COD";
    public static final String ONLINE_PAYMENT = "OP";
    public static final String PAYTM_WALLET = "Paytm";
    public static final String PAYTM_QR = "Paytm QR";
    public static final String BANK_TRANSFER = "Bank Transfer";
    public static final String UPI = "UPI";
    public static final String WRITEOFF = "Writeoff";
    public static final String WALLET = "Wallet";
    public static final String COD_AND_OP = "COD+OP";
    public static final String DEBIT_CARD = "Debit card";
    public static final String CREDIT_CARD = "Credit card";
    public static final String NET_BANKING = "Net banking";
    public static final String WITH_DISCOUNT = "With discount";
    public static final String WITHOUT_DISCOUNT = "Without discount";
    public static final String MATCHED = "Matched";
    public static final String MIS_MATCHED = "Mismatched";
    public static final String COLLECT = "Collect";
    public static final String RETURN = "Return";
    public static final String WITH_OUTSTANDING = "With outstanding";
    public static final String WITHOUT_OUTSTANDING = "Without outstanding";
    public static final String PRE_ORDER = "Pre-order";
    public static final String SAME_DAY_ORDER = "Same day order";
    public static final String BILL_PRINTED = "Bill printed";
    public static final String FIRST_TIME_ORDERS = "First time orders";
    public static final String NEGATIVE_COD = "Negative cod";
    public static final String REFERRAL_ORDERS = "Referral orders";
    public static final String FOLLOWUP_RESULTED_ORDERS = "Followup resulted orders";
    public static final String BILL_NOT_PRINTED = "Bill not printed";
    public static final String UNASSIGNED_DELIVERY_AREA = "Unassigned delivery area";
    public static final String IN_CUSTOMER_SUPPORT = "In customer support";
    public static final String NOT_IN_CUSTOMER_SUPPORT = "Not in customer support";
    public static final String CASH_N_CARRY = "Cash N Carry";
    public static final String NOT_CASH_N_CARRY = "Not Cash N Carry";
    public static final String WITH_INSTRUCTIONS = "With instructions";
    public static final String WITHOUT_INSTRUCTIONS = "Without instructions";
    public static final String WITH_DELAY = "With delay";
    public static final String WITHOUT_DELAY = "Without delay";

    public static final String ACTION_TYPE = "ACTION_TYPE";
    public static final String BUYER_NAME = "BUYER_NAME";
    public static final String BUYER_ID = "BUYER_ID";
    public static final String DELIVERY_BOY_ID = "DELIVERY_BOY_ID";
    public static final String DELIVERY_BOY_NAME = "DELIVERY_BOY_NAME";
    public static final String MONEY_COLLECTION_FOR_TODAY = "MONEY_COLLECTION_FOR_TODAY";
    public static final String MONEY_COLLECTION_FOR_DATE = "MONEY_COLLECTION_FOR_DATE";
//    public static final String MONEY_COMSOLIDATION_FOR_DATE = "MONEY_COMSOLIDATION_FOR_DATE";
    public static final String MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_TODAY = "MONEY_COLLECTION_OF_DELIVERY_BOY_TODAY";
    public static final String MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_DATE = "MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_DATE";
    public static final String PENDING_CONSOLIDATION_OF_DELIVERY_BOY = "PENDING_CONSOLIDATION_OF_DELIVERY_BOY";
    public static final String BUYER_MONEY_COLLECTION = "BUYER_MONEY_COLLECTION";

    public static final String PENDING_DELIVERIES = "PENDING_DELIVERIES";
    public static final String DELIVERY_GROUP_DELIVERIES = "DELIVERY_GROUP_DELIVERIES";

    public static final String INPUT = "Input";
    public static final String PURCHASE = "Purchase";
    public static final String CONSOLIDATION = "Consolidation";
    public static final String ADJUSTMENT = "Adjustment";
    public static final String WASTAGE = "Wastage";
    public static final String CART = "Cart";
    public static final String ORDER = "Order";
    public static final String OUTSTANDING = "Outstanding";
    public static final String FIRST_ORDER = "First Order";
    public static final String NOT_ORDER = "Not Order";
    public static final String CATEGORIES_STOCK_STATEMENT = "Categories stock statement";
    public static final String MEAT_ITEMS_STOCK_STATEMENT = "Meat items stock statement";
    public static final String CATEGORY_PURCHASE = "Category purchase";//"cp";
    public static final String CATEGORY_CONSOLIDATION = "Category consolidation";//"cc";
    public static final String CATEGORY_ADJUSTMENT = "Category adjustment";//"ca";
    public static final String MEAT_ITEM_INPUT = "Meat item input";//"mi";
    public static final String MEAT_ITEM_PURCHASE = "Meat item purchase";//"mp";
    public static final String MEAT_ITEM_CONSOLIDATION = "Meat item consolidation";//"mc";
    public static final String MEAT_ITEM_ADJUSTMENT = "Meat item adjustment";//"ma";
    public static final String MEAT_ITEM_ORDER = "Meat item order";//"mo";
    public static final String MEAT_ITEM_WASTAGE = "Meat item wastage";//"mw";
    public static final String HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE = "Hub - Meat item transfer in from warehouse";//"hmti";
    public static final String HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB = "Hub - Meat item transfer out to hub";//"hmto";
    public static final String HUB_MEAT_ITEM_ORDER = "Hub - Meat item order";//"hmo";
    public static final String HUB_MEAT_ITEM_WASTAGE = "Hub - Meat item wastage";//"hmw";

    public static final String FOR_STOCK = "For Stock";
    public static final String FOR_CUSTOMER = "For Customer";
    public static final String FOR_EMPLOYEE = "For Employee";
    public static final String FOR_PENDING_FOLLOWUPS = "For Pending Followups";

    public static final String GENERAL_FOLLOWUP = "gf";
    public static final String FEEDBACK_FOLLOWUP = "ff";
    public static final String ORDER_FOLLOWUP = "of";
    public static final String FIRST_ORDER_FOLLOWUP = "fof";
    public static final String CART_FOLLOWUP = "cf";

    public static final String ORDER_RESULTED = "Order Resulted";

    public static final String ADD_ALL = "Add all visible items to bucket";
    public static final String REMOVE_ALL = "Remove all visible items from bucket";

    public static class code {
        public static class response {
            public static final int EMAIL_INVALID = 101;
            public static final int WRONG_PASSWORD = 102;
            public static final int STATUS_OK = 200;

            public static final int UN_KNOWN = 1000;
            public static final int NO_DATA = 1001;
            public static final int NOT_AVAILABLE = 500;
        }
    }

    public static class param {
        public static final String Location = "loc";
        public static final String TOKEN = "x-access-token";
    }

    //--------

    public static final String ALL = "ALL";
    public static final String PENDING = "PENDING";
    public static final String CONSOLIDATED = "CONSOLIDATED";

    public static final String ORDERED = "ORDERED";
    public static final String ACKNOWLEDGED = "ACKNOWLEDGED";
    public static final String PROCESSING = "PROCESSING";
    public static final String PROCESSED = "PROCESSED";
    public static final String ASSIGNED = "ASSIGNED";
    public static final String PICKED_UP = "PICKED UP";
    public static final String DELIVERING = "DELIVERING";
    public static final String DELIVERED = "DELIVERED";
    public static final String REJECTED = "REJECTED";
    public static final String FAILED = "FAILED";

    public static final String ANDROID = "ANDROID";
    public static final String IOS = "IOS";
    public static final String WEB = "WEB";
    public static final String PHONE = "PHONE";


    public static final int QC_PENDING = 0;
    public static final int QC_DONE = 1;
    public static final int QC_FAILED = 2;

    //--------

    public static final String START_DELIVERY_REQUEST = "START_DELIVERY_REQUEST";
    public static final String COMPLETE_DELIVERY_REQUEST = "COMPLETE_DELIVERY_REQUEST";
    public static final String WHERE_TO_NEXT_REQUEST = "WHERE_TO_NEXT_REQUEST";

    public static final String TITLE = "TITLE";
    public static final String SUB_TITLE = "SUB_TITLE";
    public static final String ADD = "ADD";
    public static final String UPDATE = "UPDATE";
    public static final String SELECT = "Select";
    public static final String OTHER = "Other";

    public static final String ASSIGN_HUB = "Assign hub";
    public static final String ACKNOWLEDGE = "Acknowledge";
    public static final String UPDATE_SPECIAL_INSTRUCTIONS = "Update special instructions";
    public static final String MARK_AS_PROCESSING = "Mark as processing";
    //public static final String UPDATE_VENDOR_DETAILS = "Update vendor details";
    //public static final String UPDATE_BUTCHER_DETAILS = "Update butcher details";
    public static final String UPDATE_STOCK_DETAILS = "Update stock details";
    public static final String RESET_TO_NEW = "Reset to new";
    public static final String REMOVE_PROCESSING_INFO = "Remove processing info";
    public static final String REMOVE_DELIVERY_BOY_INFO = "Remove delivery boy info";
    public static final String START_PROCESING = "Start processing";
    public static final String COMPLETE_PROCESING = "Complete processing";
    public static final String COMPLETE_COUNTER_SALE_ORDER = "Complete counter sale";
    public static final String EDIT_COUNTER_SALE_ORDER = "Edit counter sale";
    public static final String FIX_QUALITY_CHECK = "Fix quality check";
    public static final String COMPLETE_QUALITY_CHECK = "Complete quality check";
    public static final String FAIL_QUALITY_CHECK = "Fail quality check";
    public static final String ASSIGN_DELIVERY_BOY = "Assign delivery boy";
    public static final String START_DELIVERY = "Start delivery";
    public static final String REPROCESS_ITEM = "Reprocess item";
    public static final String REASSIGN_DELIVERY_BOY = "Reassign delivery boy";
    //public static final String MARK_AS_CASH_N_CARRY = "Mark as cash n carry";
    //public static final String UNMARK_AS_CASH_N_CARRY = "Unmark as cash n carry";
    public static final String UPDATE_QUANTITY = "Update quantity";
    public static final String CHANGE_DELIVERY_DATE = "Change delivery date";
    public static final String REPLACE_ITEM = "Replace item";
    public static final String EDIT_ORDER_ITEM = "Edit order item";
    public static final String MARK_AS_DELIVERED = "Mark as delivered";
    public static final String MARK_AS_FAILED = "Mark as failed";
    public static final String ASSIGN_TO_CUSTOMER_SUPPORT = "Assign to customer support";
    public static final String REMOVE_FROM_CUSTOMER_SUPPORT = "Remove from customer support";
    public static final String CLAIM_CUSTOMER_SUPPORT = "Claim for processing";
    public static final String ADD_DELAYED_DELIVERY_COMMENT = "Add delayed delivery comment";
    public static final String ADD_CUSTOMER_SUPPORT_COMMENT = "Add comment";
    public static final String ASSIGN_TO_DELIVERY_AREA = "Assign to delivery area";
    public static final String CHANGE_DELIVERY_AREA = "Change delivery area";
    public static final String CHANGE_DELIVERY_ADDRESS = "Change delivery address";
    public static final String UPDATE_HOUSING_SOCIETY = "Update housing society";
    public static final String REFUND_AMOUNT = "Refund amount";
    public static final String CLAIM_FEEDBACK = "Claim feedback";
    public static final String ADD_COMMENT = "Add comment";
    public static final String MARK_AS_PROCESSED = "Mark as processed";
    public static final String ADD_TO_TESTIMONIALS = "Add to testimonials";
    public static final String REMOVE_FROM_TESTIMONIALS = "Remove from testimonials";

    public static final String UPDATE_NET_WEIGHT = "Update net weight";
    public static final String UPDATE_ALTERNATE_NUMBER = "Update alternate number";

    public static final String PRINT_BILL = "Print bill";
    public static final String PRINT_LABEL = "Print label";

    //public static final String MARK_AS_WITH_ACCOUNTS = "Mark as with accounts";
    //public static final String MARK_AS_WITH_BANK = "Mark as with bank";
    public static final String MARK_AS_CONSOLIDATED = "Mark as consolidated";
    public static final String UPDATE_MONEY_COLLECTION_TRANSACTION_DETAILS = "Update transaction details";


    public static final String UNPROCESSED_FEEDBACKS = "UNPROCESSED_FEEDBACKS";
    public static final String UNPROCESSED_CUSTOMER_SUPPORT = "UNPROCESSED_CUSTOMER_SUPPORT";

    public static final String FIREBASE_RECEIVER = "FIREBASE_SERVICE_RECEIVER";
    public static final String ADD_CS_LISTENER_FOR_ORDER_ITEM = "ADD_CS_LISTENER_FOR_ORDER_ITEM";
    public static final String REMOVE_CS_LISTENER_FOR_ORDER_ITEM = "REMOVE_CS_LISTENER_FOR_ORDER_ITEM";
    public static final String CLEAR_CS_NOTIFICATIONS_CACHE = "CLEAR_CS_NOTIFICATIONS_CACHE";
    public static final String ORDER_ID = "ORDER_ID";
    public static final String ORDER_ITEM_ID = "ORDER_ITEM_ID";
    public static final String DELIVERY_AREA_ID = "DELIVERY_AREA_ID";
    public static final String ORDER_DETAILS = "ORDER_DETAILS";
    public static final String ORDER_ITEM_DETAILS = "ORDER_ITEM_DETAILS";
    public static final String DELIVERY_AREA_DETAILS = "DELIVERY_AREA_DETAILS";
    public static final String HOUSING_SOCIETY_DETAILS = "HOUSING_SOCIETY_DETAILS";
    public static final String WAREHOUSE_MEAT_ITEM_DETAILS = "WAREHOUSE_MEAT_ITEM_DETAILS";
    public static final String MEAT_ITEM_DETAILS = "MEAT_ITEM_DETAILS";
    public static final String CATEGORY_DETAILS = "CATEGORY_DETAILS";
    public static final String MEMBERSHIP_DETAILS = "MEMBERSHIP_DETAILS";
    public static final String MESSAGE_DETAILS = "MESSAGE_DETAILS";
    public static final String SEARCH_DETAILS = "SEARCH_DETAILS";

    public static final String SUPER_USER = "Super user";

    public static final String CREDIT = "CREDIT";
    public static final String CASH = "CASH";

    public static final String RESTART_APP = "RESTART_APP";

    public static final String WITH_ACCOUNTS = "With accounts";
    public static final String WITH_DELIVERBOY = "With delivery boy";
    public static final String WITH_BANK = "With bank";

    public static final String BUYER_FEEDBACK_HISTORY = "Buyer Feedback history";

    public static final String ECOD_PAYMENT = "ecod";
    public static final String LINK_PAYMENT = "link";

    public static final String MONEY_COLLECTION_FOR_ORDER = "o";
    public static final String MONEY_COLLECTION_FOR_OUTSTANDING = "amt";

    public static final String MONEY_COLLECTION_TRANSACTION_FOR_COLLECTION = "col";
    public static final String MONEY_COLLECTION_TRANSACTION_FOR_REFUND = "ref";
    public static final String MONEY_COLLECTION_TRANSACTION_FOR_RETURN = "ret";

    public static final String UNDER_CONSTRUCTION_OR_NOT_YET_DONE = "Under construction / Not yet done...";

    public static final String []MESSAGE_TYPES = new String[]{"launchmessage", "all", "home"/*, "chicken"*/, "farmchicken", "countrychicken"/*, "mutton"*/, "goatmutton", "sheepmutton"/*, "seafood"*/, "freshwatersf", "seawatersf"/*, "specialities"*/, "pickles", "marinades"/*, "others"*/, "eggs", "payments"/*, "referrals"*/, "webslider"};
    public static final List<String> getFormattedMessagesTyeps(List<String> messageTypes){
        List<String> supportedTypes = CommonMethods.getStringListFromStringArray(MESSAGE_TYPES);
        List<String> finalTypes = new ArrayList<>();
        for (int i=0;i<messageTypes.size();i++){
            if ( supportedTypes.contains(messageTypes.get(i).trim()) ){
                finalTypes.add(messageTypes.get(i).trim());
            }
        }
        return finalTypes;
    }

}
