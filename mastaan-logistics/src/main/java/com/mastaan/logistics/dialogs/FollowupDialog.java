package com.mastaan.logistics.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.BuyersActivity;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.models.RequestAddFollowup;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.BuyerDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 10/02/18.
 */

public class FollowupDialog {

    MastaanToolbarActivity activity;

    String followupType = Constants.GENERAL_FOLLOWUP;

    View dialogView;
    TextView title;
    vTextInputLayout buyer;
    vSpinner commentsSpinner;
    vTextInputLayout comments;

    BuyerDetails selectedBuyer;

    boolean disableChangeBuyer;


    Callback callback;
    public interface Callback{
        void onComplete(RequestAddFollowup requestObject);
    }

    public FollowupDialog(final MastaanToolbarActivity activity) {
        this(activity, null);
    }
    public FollowupDialog(final MastaanToolbarActivity activity, final String dialogTitle) {
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_followup, null);
        title = dialogView.findViewById(R.id.title);
        buyer = dialogView.findViewById(R.id.buyer);
        commentsSpinner = dialogView.findViewById(R.id.commentsSpinner);
        comments = dialogView.findViewById(R.id.comments);

        if ( dialogTitle != null && dialogTitle.trim().length() > 0 ){
            title.setText(dialogTitle);
        }

        ListArrayAdapter followupsAdapter = new ListArrayAdapter(activity, R.layout.view_list_item, getFollowupReasons());
        followupsAdapter.setPopupLayoutView(R.layout.view_list_item_dropdown_item);
        commentsSpinner.setAdapter(followupsAdapter);
        commentsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedComment = commentsSpinner.getSelectedItem().toString();
                if ( selectedComment.equalsIgnoreCase("other") ){
                    comments.setVisibility(View.VISIBLE);
                }else{
                    comments.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        buyer.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( disableChangeBuyer == false ) {
                    Intent buyersAct = new Intent(activity, BuyersActivity.class);
                    activity.startActivityForResult(buyersAct, Constants.BUYERS_ACTIVITY_CODE);
                }
            }
        });

        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });
        dialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buyer.checkError("* Select buyer");

                String eComments = commentsSpinner.getSelectedItem().toString();
                if ( commentsSpinner.getSelectedItem().toString().equalsIgnoreCase("other") ){
                    eComments = comments.getText();
                    comments.checkError("*Enter comments");
                }

                if ( followupType == null || followupType.trim().length() == 0 ){
                    activity.showToastMessage("* Invalid followup type");
                    return;
                }

                if ( followupType.length() > 0 && selectedBuyer != null && eComments.length() > 0  ){
                    activity.closeCustomDialog();

                    if ( callback != null ){
                        callback.onComplete(new RequestAddFollowup(followupType, selectedBuyer.getID(), eComments));
                    }
                }
            }
        });
    }

    public FollowupDialog setFollowupType(String followupType) {
        this.followupType = followupType;
        return this;
    }

    public FollowupDialog show(Callback callback){
        return show(null, callback);
    }

    public FollowupDialog show(BuyerDetails buyerDetails, Callback callback){
        this.callback = callback;

        if ( buyerDetails != null ){
            disableChangeBuyer = true;
            selectedBuyer = buyerDetails;
            buyer.setText(buyerDetails.getAvailableName());
        }
        activity.showCustomDialog(dialogView);

        return this;
    }

    public FollowupDialog initiateForBuyer(String buyerMobileOrEmail){
        Intent buyersAct = new Intent(activity, BuyersActivity.class);
        buyersAct.putExtra(Constants.QUERY, buyerMobileOrEmail);
        activity.startActivityForResult(buyersAct, Constants.BUYERS_ACTIVITY_CODE);
        activity.showToastMessage("* Select the buyer to continue");
        return this;
    }


    //-------

    private List<String> getFollowupReasons(){
        List<String> followupReasons = new LocalStorageData(activity).getFollowupReasons();
        if ( followupReasons != null && followupReasons.size() > 0 ){
            followupReasons.add(0, "Other");
        }else{
            followupReasons = new ArrayList<>();followupReasons.add("Other");
        }
        return followupReasons;
    }


    //======

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if ( requestCode == Constants.BUYERS_ACTIVITY_CODE && resultCode == Activity.RESULT_OK ){
                selectedBuyer = new Gson().fromJson(data.getStringExtra(Constants.BUYER_DETAILS), BuyerDetails.class);
                buyer.setText(selectedBuyer.getAvailableName());
            }
        }catch (Exception e){}
    }

}
