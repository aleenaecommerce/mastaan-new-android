package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.widgets.vSpinner;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.AttributeOptionsAdapter;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.models.AttributeDetails;
import com.mastaan.logistics.models.AttributeOptionDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 23/10/18.
 */

public class AttributeOptionsSelectionDialog implements View.OnClickListener{

    MastaanToolbarActivity activity;

    View dialogView;
    LinearLayout attributesHolder;
    View done;
    View cancel;

    List<View> attributesViews;

    Callback callback;

    public interface Callback{
        void onComplete(List<AttributeOptionDetails> attributeOptions);
    }


    public AttributeOptionsSelectionDialog(final vToolBarActivity activity) {
        this.activity = (MastaanToolbarActivity) activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_attributes_options_selection, null);
        attributesHolder = dialogView.findViewById(R.id.attributesHolder);

        done = dialogView.findViewById(R.id.done);
        done.setOnClickListener(this);

        cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if ( view == done ){
            if ( callback != null ){
                List<AttributeOptionDetails> attributeOptions = new ArrayList<>();
                if ( attributesViews != null && attributesViews.size() > 0 ){
                    for (int i=0;i<attributesViews.size();i++){
                        attributeOptions.add((AttributeOptionDetails) ((vSpinner)attributesViews.get(i).findViewById(R.id.attribute_options)).getSelectedItem());
                    }
                }

                callback.onComplete(attributeOptions);
            }
        }
        else if ( view == cancel ){
            activity.closeCustomDialog();
        }
    }


    public void show(final String meatItemID, final Callback callback){
        show(meatItemID, false, callback);
    }
    public void show(final String meatItemID, final boolean onlyItemLevelAttributes, final Callback callback){

        activity.showLoadingDialog("Loading attributes, wait...");
        activity.getBackendAPIs().getMeatItemsAPI().getMeatItemAttributes(meatItemID, new MeatItemsAPI.MeatItemAttributesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<AttributeDetails> attributesList) {
                activity.closeLoadingDialog();
                if ( status ){
                    List<AttributeDetails> finalAttributesList = new ArrayList<>();
                    if ( onlyItemLevelAttributes ){
                        for (int i=0;i<attributesList.size();i++){
                            if ( attributesList.get(i) != null && attributesList.get(i).isItemLevelAttribute() ){
                                finalAttributesList.add(attributesList.get(i));
                            }
                        }
                    }else{
                        finalAttributesList = attributesList;
                    }
                    show(finalAttributesList, callback);
                }
                else{
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while loading attributes.\n\nDo you want to try again?", "Cancel", "Retry", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("Retry") ){
                                show(meatItemID, callback);
                            }
                        }
                    });
                }
            }
        });
    }
    public void show(List<AttributeDetails> attributesList, Callback callback){
        this.callback = callback;

        if ( attributesList != null && attributesList.size() > 0 ){
            attributesHolder.removeAllViews();
            attributesViews = new ArrayList<>();
            for (int i=0;i<attributesList.size();i++){
                AttributeDetails attributeDetails = attributesList.get(i);
                if ( attributeDetails != null && attributeDetails.isItemLevelAttribute() ){
                    final View attributeView = LayoutInflater.from(activity).inflate(R.layout.view_attribute_item, null);
                    attributesViews.add(attributeView);
                    attributesHolder.addView(attributeView);

                    TextView attribute_name = attributeView.findViewById(R.id.attribute_name);
                    final vSpinner attribute_options = attributeView.findViewById(R.id.attribute_options);

                    attribute_name.setText(attributeDetails.getName());
                    attribute_options.setAdapter(new AttributeOptionsAdapter(activity, attributeDetails.getAvailableOptions(), null, null));

                    attributeView.findViewById(R.id.attributeSelector).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            attribute_options.performClick();
                        }
                    });
                }
            }

            activity.showCustomDialog(dialogView);
        }
        else{
            if ( callback != null ){
                callback.onComplete(new ArrayList<AttributeOptionDetails>());
            }
        }
    }

}
