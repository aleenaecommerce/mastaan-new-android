package com.mastaan.logistics.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.UserDetailsMini;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 09/08/18.
 */

public class EmployeeMultiSelectionDialog {

    MastaanToolbarActivity activity;

    AlertDialog alertDialog;
    View dialogView;
    vRecyclerView itemsHolder;
    UsersAdapter usersAdapter;

    Callback callback;

    public interface Callback{
        void onComplete(List<UserDetailsMini> selectedEmployees);
    }


    public EmployeeMultiSelectionDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        alertDialog = new AlertDialog.Builder(activity).create();
        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_employee_multi_selection, null);
        itemsHolder = (vRecyclerView) dialogView.findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        usersAdapter = new UsersAdapter(activity, new ArrayList<UserDetailsMini>());
        itemsHolder.setAdapter(usersAdapter);

        dialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if ( callback != null ){
                    callback.onComplete(usersAdapter.getSelectedList());
                }
            }
        });
        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

    }

    public void show(List<UserDetailsMini> employeesList, List<UserDetailsMini> selectedList, Callback callback){
        this.callback = callback;

        usersAdapter.setSelectedList(selectedList).setItems(employeesList);

        alertDialog.setView(dialogView);
        alertDialog.show();
    }



    //--------

    public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {
        Context context;
        List<UserDetailsMini> itemsList;

        List<UserDetailsMini> selectedList = new ArrayList<>();

        public class ViewHolder extends RecyclerView.ViewHolder {
            View itemSelector;
            CheckBox checkbox;
            public ViewHolder(View itemLayoutView) {
                super(itemLayoutView);
                itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
                checkbox = (CheckBox) itemLayoutView.findViewById(R.id.checkbox);
            }
        }

        public UsersAdapter(Context context, List<UserDetailsMini> itemsList) {
            this.context = activity;
            this.itemsList = itemsList;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemViewHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_check_item, null);
            return (new UsersAdapter.ViewHolder(itemViewHolder));
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, int position) {
            final UserDetailsMini userDetails = itemsList.get(position);

            if ( userDetails != null ){
                viewHolder.checkbox.setText(userDetails.getAvailableName());
                viewHolder.checkbox.setChecked(isSelected(userDetails));

                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( viewHolder.checkbox.isChecked() ){
                            viewHolder.checkbox.setChecked(false);
                            removeFromSelectedList(userDetails);
                        }else{
                            viewHolder.checkbox.setChecked(true);
                            addToSelectedList(userDetails);
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return itemsList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public void addToSelectedList(UserDetailsMini userDetails){
            if ( selectedList == null ){    selectedList = new ArrayList<>();   }

            if ( isSelected(userDetails) == false ){
                selectedList.add(userDetails);
            }
        }
        public void removeFromSelectedList(UserDetailsMini userDetails){
            if ( selectedList == null ){    selectedList = new ArrayList<>();   }

            for (int i=0;i<selectedList.size();i++){
                if ( selectedList.get(i) != null && selectedList.get(i).getID().equals(userDetails.getID()) ){
                    selectedList.remove(i);
                    break;
                }
            }
        }
        public boolean isSelected(UserDetailsMini userDetails){
            boolean contains = false;
            for (int i=0;i<selectedList.size();i++){
                if ( selectedList.get(i) != null && selectedList.get(i).getID().equals(userDetails.getID()) ){
                    contains = true;
                    break;
                }
            }
            return contains;
        }

        public UsersAdapter setSelectedList(List<UserDetailsMini> selectedList) {
            this.selectedList = selectedList!=null?selectedList:new ArrayList<UserDetailsMini>();
            return this;
        }
        public List<UserDetailsMini> getSelectedList() {
            return selectedList!=null?selectedList:new ArrayList<UserDetailsMini>();
        }

        //----------

        public void setItems(List<UserDetailsMini> items){
            itemsList.clear();;
            if ( items != null && items.size() > 0 ) {
                for (int i = 0; i < items.size(); i++) {
                    itemsList.add(items.get(i));
                }
            }
            notifyDataSetChanged();
        }
    }

}
