package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.FeedbacksAdapter;
import com.mastaan.logistics.backend.FeedbacksAPI;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.OrderDetails2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 29/09/18.
 */

public class FeedbackDetailsDialog {

    MastaanToolbarActivity activity;

    View dialogView;
    TextView title;
    vRecyclerView itemHolder;

    public FeedbackDetailsDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_details, null);
        title = dialogView.findViewById(R.id.title);
        itemHolder = dialogView.findViewById(R.id.itemHolder);
        itemHolder.setupVerticalOrientation();

    }

    public void showStock(String feedbackID){
        activity.showLoadingDialog("Loading feedback details, wait...");
        activity.getBackendAPIs().getFeedbacksAPI().getFeedbackDetails(feedbackID, new FeedbacksAPI.FeedbackCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, FeedbackDetails feedbackDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    show(feedbackDetails);
                }else{
                    activity.showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }
    public void show(final FeedbackDetails feedbackDetails){
        List<FeedbackDetails> itemsList = new ArrayList<>();
        itemsList.add(feedbackDetails);

        title.setText(feedbackDetails.getID());
        itemHolder.setAdapter(new FeedbacksAdapter(activity, itemsList, false, new FeedbacksAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails2 orderDetails = feedbackDetails.getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    activity.showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            activity.callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    activity.callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails2 orderDetails = feedbackDetails.getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    activity.showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowOrderDetails(int position) {
                new OrderDetailsDialog(activity).show(feedbackDetails.getOrderItemDetails().getOrderID());
            }

            @Override
            public void onShowCustomerOrderHistory(int position) {
                new BuyerOptionsDialog(activity).show(feedbackDetails);
            }

            @Override
            public void onShowStockDetails(int position) {
                new StockDetailsDialog(activity).show(feedbackDetails.getOrderItemDetails());
            }
        }));

        activity.showCustomDialog(dialogView);
    }

}
