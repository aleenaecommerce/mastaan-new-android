package com.mastaan.logistics.dialogs;

import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vSpinner;
import com.google.android.material.tabs.TabLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.methods.ActivityMethods;
import com.mastaan.logistics.methods.AnalyseOrderitemsMethods;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Created by venkatesh on 2/7/16.
 */
public class AnalyseOrderItemsDialog {

    vToolBarActivity activity;
    List<GroupedOrdersItemsList> items;
    List<OrderItemDetails> orderItemsLsit;

    int selectedCategoryIndex;

    View totalStatisticsDialogView;
    TabLayout sectionsTabLayout;
    View dateSelectionView;
    vSpinner dateSpinner;
    RadioGroup categoryGroup;
    View slotSelectionView;
    vSpinner slotSpinner;
    ScrollView scrollView;
    TextView total_details;

    List<String> sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.WEIGHT, Constants.STATUS, Constants.TIME, Constants.AREA, Constants.VENDOR, Constants.DELIVERY_BOY});
    List<String> categoriesList = new ArrayList<String>();
    List<String> datesList = new ArrayList<String>();
    String amountTotalInfo;
    List<Map<String, String>> weightSlotTotalInfos = new ArrayList<>();
    String statusInfo;
    List<String> timeInfos = new ArrayList<String>();
    List<String> delayInfos = new ArrayList<String>();
    List<String> vendorInfos = new ArrayList<String>();
    String areaInfo;
    String deliveryBoyInfo;

    public AnalyseOrderItemsDialog(final vToolBarActivity activity, List<GroupedOrdersItemsList> items){
        this.activity = activity;
        this.items = items;

        totalStatisticsDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_analyse_order_items, null);
        sectionsTabLayout = (TabLayout) totalStatisticsDialogView.findViewById(R.id.sectionsLayout);
        sectionsTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        dateSelectionView = totalStatisticsDialogView.findViewById(R.id.dateSelectionView);
        dateSpinner = (vSpinner) totalStatisticsDialogView.findViewById(R.id.dateSpinner);
        categoryGroup = (RadioGroup) totalStatisticsDialogView.findViewById(R.id.categoryGroup);
        slotSelectionView = totalStatisticsDialogView.findViewById(R.id.slotSelectionView);
        slotSpinner = (vSpinner) totalStatisticsDialogView.findViewById(R.id.slotSpinner);
        scrollView = (ScrollView) totalStatisticsDialogView.findViewById(R.id.scrollView);
        total_details = (TextView) totalStatisticsDialogView.findViewById(R.id.total_details);

        //-----

        if ( ActivityMethods.isNewOrdersActivity(activity) ){
            sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.WEIGHT, Constants.STATUS, Constants.TIME, Constants.AREA});
        }
        else if ( ActivityMethods.isPendingProcessingActivity(activity) ){
            sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.WEIGHT, Constants.TIME, Constants.AREA});
        }
        else if ( ActivityMethods.isPendingDeliveriesActivity(activity) ){
            sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.TIME, Constants.AREA});
        }
        else if ( ActivityMethods.isMyDeliveriesActivity(activity) ){
            sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.TIME, Constants.AREA});
        }
        else if ( ActivityMethods.isTodaysOrdersActivity(activity) ){
            sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.WEIGHT, Constants.STATUS, Constants.TIME, Constants.DELAY, Constants.AREA, Constants.VENDOR, Constants.DELIVERY_BOY});
        }
        else if ( ActivityMethods.isFutureOrdersActivity(activity) ){
            sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.WEIGHT, Constants.STATUS, Constants.TIME, Constants.AREA});
        }
        else if ( ActivityMethods.isOrderDetailsActivity(activity) ){
            sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.WEIGHT, Constants.STATUS, Constants.DELIVERY_BOY});
        }
        else if ( ActivityMethods.isOrdersByDateActivity(activity) ){
            sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.WEIGHT, Constants.STATUS, Constants.TIME, Constants.DELAY, Constants.AREA, Constants.VENDOR, Constants.DELIVERY_BOY});
        }
        else if ( ActivityMethods.isBuyerOrdersActivity(activity) ){
            sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT, Constants.STATUS});
        }

        //----------

        totalStatisticsDialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();
            }
        });

        sectionsTabLayout.removeAllTabs();
        for (int i = 0; i < sectionsList.size(); i++) {
            TabLayout.Tab tab = sectionsTabLayout.newTab();
            View rowView = LayoutInflater.from(activity).inflate(R.layout.view_tab, null, false);
            TextView tabTitle = (TextView) rowView.findViewById(R.id.tabTitle);
            tabTitle.setText(sectionsList.get(i));
            tab.setCustomView(rowView);
            sectionsTabLayout.addTab(tab);
        }

        sectionsTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                showSelectedTabDetails();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });
    }

    public void show(){
        show(true);
    }
    public void show(boolean showLoadingDialog){

        if ( showLoadingDialog ){
            activity.showLoadingDialog(true, "Analysing, wait...");
        }
        new AsyncTask<Void, Void, Void>() {
            List<String> datesList = new ArrayList<String>();
            @Override
            protected Void doInBackground(Void... voids) {
                datesList = getDatesList();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( datesList.size() > 1 ){
                    datesList.add(0, "All dates");
                    dateSelectionView.setVisibility(View.VISIBLE);
                    setupDateSpinner(datesList);
                }else{
                    dateSelectionView.setVisibility(View.GONE);
                    showStatsForDate(null, false);
                }
            }
        }.execute();
    }

    private void setupDateSpinner(List<String> dates){
        this.datesList = dates;

        ArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_date_spinner_item, datesList);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setDropDownViewResource(R.layout.view_date_spinner_item);
        dateSpinner.setAdapter(adapter);
        dateSpinner.setSelection(0);
        showStatsForDate(datesList.get(0), false);

        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previousPosition = -1;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( previousPosition != -1 ) {
                    //activity.showToastMessage(datesList.get(position));
                    showStatsForDate(datesList.get(position), true);
                }
                previousPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setupSlotsSpinner(List<String> slotsList){
        Collections.sort(slotsList, new Comparator<String>() {
            public int compare(String item1, String item2) {
                return item1.compareToIgnoreCase(item2);
            }
        });

        ArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_date_spinner_item, slotsList);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setDropDownViewResource(R.layout.view_date_spinner_item);
        slotSpinner.setAdapter(adapter);
        slotSpinner.setSelection(0);

        slotSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previousPosition = -1;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( previousPosition != -1 ) {
                    if ( sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.WEIGHT)){
                        total_details.setText(Html.fromHtml(weightSlotTotalInfos.get(selectedCategoryIndex).get(slotSpinner.getSelectedItem())));
                    }
                }
                previousPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void showStatsForDate(final String selectedDate, boolean showLoadingDialog){

        if ( showLoadingDialog ) {
            activity.showLoadingDialog(true, "Analysing, wait...");
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                AnalyseOrderitemsMethods analyseOrderitemsMethods = null;
                if ( selectedDate == null || selectedDate.toLowerCase().contains("all") ){
                    analyseOrderitemsMethods = new AnalyseOrderitemsMethods(sectionsList, items);
                }else{
                    analyseOrderitemsMethods = new AnalyseOrderitemsMethods(sectionsList, getItemsListForDate(selectedDate));
                }
                analyseOrderitemsMethods.analyse();

                categoriesList = analyseOrderitemsMethods.getCategoriesList();
                amountTotalInfo = analyseOrderitemsMethods.getFullAmountInfo();
                weightSlotTotalInfos = analyseOrderitemsMethods.getWeightSlotInfo();
                statusInfo = analyseOrderitemsMethods.getStatusInfo();
                timeInfos = analyseOrderitemsMethods.getTimeInfo();
                delayInfos = analyseOrderitemsMethods.getDelayInfo();
                vendorInfos = analyseOrderitemsMethods.getVendorsInfo();
                areaInfo = analyseOrderitemsMethods.getAreaInfo();
                deliveryBoyInfo = analyseOrderitemsMethods.getDeliveryBoyInfo();

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                activity.closeLoadingDialog();

                categoryGroup.removeAllViews();
                for (int i = 0; i < categoriesList.size(); i++) {
                    final int index = i;

                    final RadioButton radioButton = new RadioButton(activity);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    categoryGroup.addView(radioButton, layoutParams);
                    if (i == 0) {
                        radioButton.setChecked(true);
                    }

                    radioButton.setText(categoriesList.get(i));
                    radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                selectedCategoryIndex = index;
                                if (sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.WEIGHT)) {
                                    //total_details.setText(Html.fromHtml(weightsTotalInfos.get(index)));
                                    List<String> slotsList = new ArrayList<>();
                                    for (String key:weightSlotTotalInfos.get(selectedCategoryIndex).keySet()){
                                        slotsList.add(key);
                                    }
                                    setupSlotsSpinner(slotsList);
                                    total_details.setText(Html.fromHtml(weightSlotTotalInfos.get(selectedCategoryIndex).get(slotsList.get(0))));
                                }
                                else if (sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.TIME)) {
                                    total_details.setText(Html.fromHtml(timeInfos.get(index)));
                                }
                                else if (sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.DELAY)) {
                                    total_details.setText(Html.fromHtml(delayInfos.get(index)));
                                }
                                else if (sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.VENDOR)) {
                                    total_details.setText(Html.fromHtml(vendorInfos.get(index)));
                                }
                                scrollView.fullScroll(ScrollView.FOCUS_UP);
                            }
                        }
                    });
                }

                //=============

                showDialog();

            }
        }.execute();

    }

    private void showDialog(){
        showSelectedTabDetails();
        activity.showCustomDialog(totalStatisticsDialogView);
    }

    private void showSelectedTabDetails(){
        selectedCategoryIndex = 0;
        if ( sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.AMOUNT)){
            categoryGroup.setVisibility(View.GONE);
            slotSelectionView.setVisibility(View.GONE);
            total_details.setText(Html.fromHtml(amountTotalInfo));
        }
        else if ( sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.WEIGHT)){
            categoryGroup.setVisibility(View.VISIBLE);
            slotSelectionView.setVisibility(View.VISIBLE);
            ((RadioButton)categoryGroup.getChildAt(0)).setChecked(true);

            List<String> slotsList = new ArrayList<>();
            for (String key:weightSlotTotalInfos.get(selectedCategoryIndex).keySet()){
                slotsList.add(key);
            }
            setupSlotsSpinner(slotsList);
            total_details.setText(Html.fromHtml(weightSlotTotalInfos.get(0).get(slotsList.get(0))));
        }
        else if ( sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.STATUS)){
            categoryGroup.setVisibility(View.GONE);
            slotSelectionView.setVisibility(View.GONE);
            total_details.setText(Html.fromHtml(statusInfo));
        }
        else if ( sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.TIME)){
            categoryGroup.setVisibility(View.VISIBLE);
            slotSelectionView.setVisibility(View.GONE);
            ((RadioButton)categoryGroup.getChildAt(0)).setChecked(true);
            total_details.setText(Html.fromHtml(timeInfos.get(0)));
        }
        else if ( sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.DELAY)){
            categoryGroup.setVisibility(View.VISIBLE);
            slotSelectionView.setVisibility(View.GONE);
            ((RadioButton)categoryGroup.getChildAt(0)).setChecked(true);
            total_details.setText(Html.fromHtml(delayInfos.get(0)));
        }
        else if ( sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.AREA)){
            categoryGroup.setVisibility(View.GONE);
            slotSelectionView.setVisibility(View.GONE);
            total_details.setText(Html.fromHtml(areaInfo));
        }
        else if ( sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.VENDOR)){
            categoryGroup.setVisibility(View.VISIBLE);
            slotSelectionView.setVisibility(View.GONE);
            ((RadioButton)categoryGroup.getChildAt(0)).setChecked(true);
            total_details.setText(Html.fromHtml(vendorInfos.get(0)));
        }
        else if ( sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.DELIVERY_BOY)){
            categoryGroup.setVisibility(View.GONE);
            slotSelectionView.setVisibility(View.GONE);
            total_details.setText(Html.fromHtml(deliveryBoyInfo));
        }
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }


    //------------

    private List<String> getDatesList(){

        List<String> datesList = new ArrayList<>();
        for (int i=0;i<items.size();i++) {
            for (int j=0;j<items.get(i).getItems().size();j++){
                String deliveryDate = DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(items.get(i).getItems().get(j).getDeliveryDate()), DateConstants.MMM_DD_YYYY);
                if ( datesList.contains(deliveryDate) == false ){
                    datesList.add(deliveryDate);
                }
            }
        }

        if ( datesList.size() > 0 ){
            Collections.sort(datesList, new Comparator<String>() {
                public int compare(String item1, String item2) {
                    return DateMethods.compareDates(item1, item2);//item1.compareToIgnoreCase(item2);
                }
            });
        }

        return datesList;
    }
    private List<GroupedOrdersItemsList> getItemsListForDate(String date){
        List<GroupedOrdersItemsList> itemsListForDate = new ArrayList<>();
        for (int i=0;i<items.size();i++){
            GroupedOrdersItemsList groupedOrdersItemsList = new GroupedOrdersItemsList(items.get(i).getCategoryName(), new ArrayList<OrderItemDetails>());

            List<OrderItemDetails> itemsList = items.get(i).getItems();
            for (int j=0;j<itemsList.size();j++){
                if ( itemsList.get(j) != null && itemsList.get(j).getOrderDetails() != null ){
                    if (DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(itemsList.get(j).getOrderDetails().getDeliveryDate())), date) == 0 ){
                        groupedOrdersItemsList.addItem(itemsList.get(j));
                    }
                }
            }
            itemsListForDate.add(groupedOrdersItemsList);
        }

        List<GroupedOrdersItemsList> finalItemsListForDate = new ArrayList<>();
        for (int i=0;i<itemsListForDate.size();i++){
            if ( itemsListForDate.get(i).getItems().size() > 0 ){
                finalItemsListForDate.add(itemsListForDate.get(i));
            }
        }
        return finalItemsListForDate;
    }

}
