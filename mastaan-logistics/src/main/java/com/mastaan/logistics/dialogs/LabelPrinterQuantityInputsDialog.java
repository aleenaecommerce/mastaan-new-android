package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;
import com.mastaan.logistics.printers.LabelPrinter;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 26/11/16.
 */
public class LabelPrinterQuantityInputsDialog implements View.OnClickListener{

    MastaanToolbarActivity activity;

    View dialogView;
    TextView title;
    vTextInputLayout quantity1;
    vTextInputLayout quantity2;
    vTextInputLayout quantity3;
    View done;
    View cancel;

    WarehouseMeatItemDetails selectedWarehouseMeatItemDetails;
    List<AttributeOptionDetails> selectedItemLevelAttributesOptions;
    StockDetails selectedStockDetails;

    double quantity1Value = 0.25, quantity2Value = 0.5, quantity3Value = 1;

    StatusCallback callback;


    public LabelPrinterQuantityInputsDialog(final vToolBarActivity activity) {
        this.activity = (MastaanToolbarActivity) activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_label_printer_quantity_inputs, null);
        title = dialogView.findViewById(R.id.title);
        quantity1 = dialogView.findViewById(R.id.quantity1);
        quantity2 = dialogView.findViewById(R.id.quantity2);
        quantity3 = dialogView.findViewById(R.id.quantity3);
        done = dialogView.findViewById(R.id.done);
        done.setOnClickListener(this);
        cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if ( view == done ){
            final String eQuantity1 = quantity1.getText();
            final String eQuantity2 = quantity2.getText();
            final String eQuantity3 = quantity3.getText();

            if ( eQuantity1.length() > 0 || eQuantity2.length() > 0 || eQuantity3.length() > 0 ){
                double totalQuantity = 0;
                List<String> quantity1LabelTexts = null, quantity2LabelTexts = null, quantity3LabelTexts = null;
                if (eQuantity1.length() > 0) {
                    quantity1LabelTexts = new LabelPrinterDialog(activity).getLabelTexts(selectedWarehouseMeatItemDetails, selectedItemLevelAttributesOptions, selectedStockDetails, quantity1Value);
                    totalQuantity += quantity1Value * CommonMethods.parseInteger(eQuantity1);
                }
                if (eQuantity2.length() > 0) {
                    quantity2LabelTexts = new LabelPrinterDialog(activity).getLabelTexts(selectedWarehouseMeatItemDetails, selectedItemLevelAttributesOptions, selectedStockDetails, quantity2Value);
                    totalQuantity += quantity2Value * CommonMethods.parseInteger(eQuantity2);
                }
                if (eQuantity3.length() > 0) {
                    quantity3LabelTexts = new LabelPrinterDialog(activity).getLabelTexts(selectedWarehouseMeatItemDetails, selectedItemLevelAttributesOptions, selectedStockDetails, quantity3Value);
                    totalQuantity += quantity3Value * CommonMethods.parseInteger(eQuantity3);
                }

                if ( totalQuantity > selectedStockDetails.getQuantity() ){
                    activity.showToastMessage("* Max allowed total quantity is "+CommonMethods.getIndianFormatNumber(selectedStockDetails.getQuantity()));
                    return;
                }

                activity.showLoadingDialog(true, "Printing labels, wait...");
                final List<String> finalQuantity1LabelTexts = quantity1LabelTexts, finalQuantity2LabelTexts = quantity2LabelTexts, finalQuantity3LabelTexts = quantity3LabelTexts;
                new LabelPrinter(activity).printLabel(finalQuantity1LabelTexts, CommonMethods.parseInteger(eQuantity1), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if ( status == false && finalQuantity1LabelTexts != null && finalQuantity1LabelTexts.size() > 0 ){
                            activity.closeLoadingDialog();
                            activity.showToastMessage("Failed to print labels, Msg: "+message);
                            if ( callback != null ){
                                callback.onComplete(status, statusCode, message);
                            }
                            return;
                        }

                        new LabelPrinter(activity).printLabel(finalQuantity2LabelTexts, CommonMethods.parseInteger(eQuantity2), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if ( status == false && finalQuantity2LabelTexts != null && finalQuantity2LabelTexts.size() > 0 ){
                                    activity.closeLoadingDialog();
                                    activity.showToastMessage("Failed to print labels, Msg: "+message);
                                    if ( callback != null ){
                                        callback.onComplete(status, statusCode, message);
                                    }
                                    return;
                                }

                                new LabelPrinter(activity).printLabel(finalQuantity3LabelTexts, CommonMethods.parseInteger(eQuantity3), new StatusCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message) {
                                        activity.closeLoadingDialog();

                                        if ( status == false && finalQuantity3LabelTexts != null && finalQuantity3LabelTexts.size() > 0 ){
                                            activity.showToastMessage("Failed to print labels, Msg: "+message);
                                            if ( callback != null ){
                                                callback.onComplete(status, statusCode, message);
                                            }
                                            return;
                                        }

                                        activity.showToastMessage("Labels printed successfully");
                                        if ( callback != null ){
                                            callback.onComplete(status, statusCode, message);
                                        }
                                        activity.closeCustomDialog();
                                    }
                                });

                            }
                        });
                    }
                });
            }else{
                activity.showToastMessage("* Enter at least one quantity copies");
            }
        }
        else if ( view == cancel ){
            activity.closeCustomDialog();
            if ( callback != null ){
                callback.onComplete(false, -1, "Cancelled");
            }
        }

    }

    public LabelPrinterQuantityInputsDialog show(WarehouseMeatItemDetails selectedWarehouseMeatItemDetails, List<AttributeOptionDetails> selectedItemLevelAttributesOptions, StockDetails selectedStockDetails, StatusCallback callback){
        this.selectedWarehouseMeatItemDetails = selectedWarehouseMeatItemDetails;
        this.selectedItemLevelAttributesOptions = selectedItemLevelAttributesOptions;
        this.selectedStockDetails = selectedStockDetails;
        this.callback = callback;

        if ( selectedWarehouseMeatItemDetails != null && selectedStockDetails != null ){
            String quantityUnit = selectedWarehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit();

            title.setText("Label Printer Quantities (Max: "+CommonMethods.getIndianFormatNumber(selectedStockDetails.getQuantity())+" "+quantityUnit+"s)");

            if ( quantityUnit.equalsIgnoreCase("kg") ){
                quantity1Value = 0.25; quantity2Value = 0.5; quantity3Value = 1;
            }else{
                quantity1Value = 1; quantity2Value = 2; quantity3Value = 3;
            }
            quantity1.setHint("No. of "+quantity1Value+" "+quantityUnit+(quantity1Value==1?"":"s")+" copies");
            quantity2.setHint("No. of "+quantity2Value+" "+quantityUnit+(quantity2Value==1?"":"s")+" copies");
            quantity3.setHint("No. of "+quantity3Value+" "+quantityUnit+(quantity3Value==1?"":"s")+" copies");

            activity.showCustomDialog(dialogView, false);
        }else{
            activity.showToastMessage("Can't proceed without meat item & stock selection");
            if ( callback != null ){
                callback.onComplete(false, -1, "Can't proceed without meat item & stock selection");
            }
        }
        return this;
    }

}