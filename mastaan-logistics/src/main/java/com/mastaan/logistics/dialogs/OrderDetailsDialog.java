package com.mastaan.logistics.dialogs;

import android.content.Intent;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.BuyerOrdersActivity;
import com.mastaan.logistics.activities.DirectionsViewerDialogActivity;
import com.mastaan.logistics.activities.DirectionsViewerHereMapsDialogActivity;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.OrdersAdapter;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 17/11/16.
 */
public class OrderDetailsDialog{

    MastaanToolbarActivity activity;

    View dialogView;
    TextView title;
    vRecyclerView itemHolder;
    OrdersAdapter ordersAdapter;

    public OrderDetailsDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_details, null);
        title = dialogView.findViewById(R.id.title);
        itemHolder = dialogView.findViewById(R.id.itemHolder);
        itemHolder.setupVerticalOrientation();
        ordersAdapter = new OrdersAdapter(activity, new ArrayList<OrderDetails>(), new OrdersAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    activity.showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            activity.callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    activity.callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    activity.showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowDirections(final int position) {
                activity.getCurrentLocation(new LocationCallback() {
                    @Override
                    public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                        if (status) {
                            Intent showDirectionsIntent = new Intent(activity, DirectionsViewerHereMapsDialogActivity.class);
                            showDirectionsIntent.setAction(Constants.POPUP_ACTION);
                            showDirectionsIntent.putExtra("origin", latLng);
                            showDirectionsIntent.putExtra("destination", ordersAdapter.getItem(position).getDeliveryAddressLatLng());
                            showDirectionsIntent.putExtra("location", ordersAdapter.getItem(position).getCustomerName() + "\n" + ordersAdapter.getItem(position).getCustomerName()/*.getAddress().getDeliveryAddressLocality()*/);
                            activity.startActivity(showDirectionsIntent);
                        } else {
                            activity.showToastMessage(message);
                        }
                    }
                });
            }

            @Override
            public void onShowBuyerHistory(int position) {
                BuyerDetails buyerDetails = ordersAdapter.getItem(position).getBuyerDetails();
                Intent buyerOrderHistoryAct = new Intent(activity, BuyerOrdersActivity.class);
                buyerOrderHistoryAct.putExtra(Constants.BUYER_ID, buyerDetails.getID());
                buyerOrderHistoryAct.putExtra(Constants.BUYER_NAME, buyerDetails.getAvailableName());
                activity.startActivity(buyerOrderHistoryAct);
            }

            @Override
            public void onShowReferralBuyerDetails(int position) {

            }

            @Override
            public void onShowMoneyCollectionDetails(int position) {
                new MoneyCollectionDetailsDialog(activity).showForOrder(ordersAdapter.getItem(position).getID());
            }

            @Override
            public void onCheckPaymentStatus(final int position) {
                activity.showLoadingDialog("Checking payment status, wait...");
                activity.getBackendAPIs().getPaymentAPI().checkOrderPaymentStatus(ordersAdapter.getItem(position).getID(), new PaymentAPI.CheckOrderPaymentStatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, OrderDetails orderDetails) {
                        activity.closeLoadingDialog();
                        if (status){
                            getOrderDetails(ordersAdapter.getItem(position).getID());
                        }else{
                            activity.showToastMessage("Something went wrong while checking payment status.\nPlease try again!");
                        }
                    }
                });
            }

            @Override
            public void onRequestOnlinePayment(int position) {

            }

            @Override
            public void onShowFollowupResultedInOrder(int position) {
                new FollowupDetailsDialog(activity).show(ordersAdapter.getItem(position).getFollowupResultedInOrder());
            }

            @Override
            public void onPrintBill(int position) {

            }

            @Override
            public void onOrderItemClick(int position, int item_position) {

            }

            @Override
            public void onShowOrderItemStockDetails(int position, int item_position) {
                new StockDetailsDialog(activity).show(ordersAdapter.getItem(position).getOrderedItems().get(item_position));
            }
        }).showCheckPaymentStatus();
        itemHolder.setAdapter(ordersAdapter);

    }

    public void show(String orderID){
        getOrderDetails(orderID);
    }

    public void show(OrderDetails orderDetails){
        List<OrderDetails> orderDetailsList = new ArrayList<>();
        orderDetailsList.add(orderDetails);
        ordersAdapter.setItems(orderDetailsList);
        title.setText(orderDetails.getReadableOrderID());
        activity.showCustomDialog(dialogView);
    }

    private void getOrderDetails(final String orderID){

        activity.showLoadingDialog("Loading order details, wait...");
        activity.getBackendAPIs().getOrdersAPI().getOrderItems(orderID, new OrdersAPI.OrderDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<OrderItemDetails> orderItemsList) {
                activity.closeLoadingDialog();
                if ( status ){
                    title.setText(ordersList.get(0).getReadableOrderID());
                    ordersAdapter.setItems(ordersList);
                    activity.showCustomDialog(dialogView);
                }else{
                    activity.showSnackbarMessage("Unable to load order details, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            getOrderDetails(orderID);
                        }
                    });
                }
            }
        });
    }


}
