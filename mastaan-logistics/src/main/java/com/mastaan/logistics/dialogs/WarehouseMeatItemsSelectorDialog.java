package com.mastaan.logistics.dialogs;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.WarehouseMeatItemsCheckBoxAdapter;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 15/04/18.
 */

public class WarehouseMeatItemsSelectorDialog {

    MastaanToolbarActivity activity;

    View dialogView;
    TextView title;
    vRecyclerView itemsHolder;
    WarehouseMeatItemsCheckBoxAdapter warehouseMeatItemsCheckBoxAdapter;

    Callback callback;

    public interface Callback{
        void onComplete(List<String> selectedWarehouseMeatItems);
    }

    public WarehouseMeatItemsSelectorDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_warehouse_meat_items_selector, null);
        title = (TextView) dialogView.findViewById(R.id.title);
        itemsHolder = (vRecyclerView) dialogView.findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        warehouseMeatItemsCheckBoxAdapter = new WarehouseMeatItemsCheckBoxAdapter(activity, new ArrayList<WarehouseMeatItemDetails>(), null);
        itemsHolder.setAdapter(warehouseMeatItemsCheckBoxAdapter);

        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });
        dialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( callback != null ){    callback.onComplete(warehouseMeatItemsCheckBoxAdapter.getSelectedItems());  }
                activity.closeCustomDialog();
            }
        });
    }

    public WarehouseMeatItemsSelectorDialog setDialogTitle(String dialogTitle) {
        title.setText(dialogTitle);
        return this;
    }

    public void show(final Callback callback){
        show(null, null, callback);
    }
    public void show(final List<String> selectedWarehouseMeatItems, final Callback callback){
        show(null, selectedWarehouseMeatItems, callback);
    }
    public void show(List<WarehouseMeatItemDetails> warehouseMeatItems, final List<String> selectedWarehouseMeatItems, final Callback callback){

        if ( warehouseMeatItems == null || warehouseMeatItems.size() == 0 ){
            activity.showLoadingDialog("Loading meat items, wait...");
            activity.getBackendAPIs().getMeatItemsAPI().getMeatItems(new MeatItemsAPI.WarehouseMeatItemsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<WarehouseMeatItemDetails> warehouseMeatItems) {
                    activity.closeLoadingDialog();
                    if ( status ){
                        activity.getMastaanApplication().setCachedWarehouseMeatItems(warehouseMeatItems);
                        showDialog(warehouseMeatItems, selectedWarehouseMeatItems, callback);
                    }else{
                        activity.showToastMessage("Something went wrong, try again!");
                    }
                }
            });
        }else{
            showDialog(warehouseMeatItems, selectedWarehouseMeatItems, callback);
        }
    }

    private void showDialog(final List<WarehouseMeatItemDetails> warehouseMeatItems, final List<String> selectedWarehouseMeatItems, Callback callback){
        this.callback = callback;

        activity.showLoadingDialog("Loading meat items, wait...");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Collections.sort(warehouseMeatItems, new Comparator<WarehouseMeatItemDetails>() {
                    public int compare(WarehouseMeatItemDetails item1, WarehouseMeatItemDetails item2) {
                        try{
                            return item1.getMeatItemDetais().getNameWithCategoryAndSize().compareToIgnoreCase(item2.getMeatItemDetais().getNameWithCategoryAndSize());
                        }catch (Exception e){}
                        return 0;
                    }
                });
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                activity.closeLoadingDialog();

                warehouseMeatItemsCheckBoxAdapter.setSelectedItems(selectedWarehouseMeatItems);
                warehouseMeatItemsCheckBoxAdapter.setItems(warehouseMeatItems);

                activity.showCustomDialog(dialogView);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

}
