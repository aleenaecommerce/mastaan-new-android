package com.mastaan.logistics.dialogs;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.methods.CommonMethods;
import com.google.android.material.tabs.TabLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.methods.AnalyseBonusesMethods;
import com.mastaan.logistics.models.EmployeeBonusDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 26/11/18.
 */

public class AnalyseBonusesDialog {

    vToolBarActivity activity;
    List<EmployeeBonusDetails> itemsList = new ArrayList<>();

    View dialogView;
    TextView dialog_title;
    TabLayout sectionsTabLayout;
    ScrollView scrollView;
    TextView summary_info;

    List<String> sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW, Constants.EMPLOYEES, Constants.BUYERS});

    String summaryInfoDetails;
    String employeesInfoDetails;
    String buyersInfoDetails;


    public AnalyseBonusesDialog(final vToolBarActivity activity, String dialogTitle, List<EmployeeBonusDetails> itemsList) {
        this(activity, dialogTitle, CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW, Constants.EMPLOYEES, Constants.BUYERS}), itemsList);
    }
    public AnalyseBonusesDialog(final vToolBarActivity activity, final String dialogTitle, final List<String> sectionsList, final List<EmployeeBonusDetails> itemsList) {
        this.activity = activity;
        this.itemsList = itemsList;
        this.sectionsList = sectionsList;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_analyse_bonuses, null);
        dialog_title = dialogView.findViewById(R.id.dialog_title);
        sectionsTabLayout = dialogView.findViewById(R.id.sectionsTabLayout);
        sectionsTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        scrollView = dialogView.findViewById(R.id.scrollView);
        summary_info = dialogView.findViewById(R.id.summary_info);

        dialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();
            }
        });

        //--------

        dialog_title.setText(dialogTitle);

        sectionsTabLayout.removeAllTabs();
        for (int i = 0; i < sectionsList.size(); i++) {
            TabLayout.Tab tab = sectionsTabLayout.newTab();
            View rowView = LayoutInflater.from(activity).inflate(R.layout.view_tab, null, false);
            TextView tabTitle = rowView.findViewById(R.id.tabTitle);
            tabTitle.setText(sectionsList.get(i));
            tab.setCustomView(rowView);
            sectionsTabLayout.addTab(tab);
        }

        sectionsTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (sectionsList.get(tab.getPosition()).equalsIgnoreCase(Constants.OVERVIEW)) {
                    summary_info.setText(CommonMethods.fromHtml(summaryInfoDetails));
                }
                else if (sectionsList.get(tab.getPosition()).equalsIgnoreCase(Constants.EMPLOYEES)) {
                    summary_info.setText(CommonMethods.fromHtml(employeesInfoDetails));
                }
                else if (sectionsList.get(tab.getPosition()).equalsIgnoreCase(Constants.BUYERS)) {
                    summary_info.setText(CommonMethods.fromHtml(buyersInfoDetails));
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}

        });
    }

    public void show(){
        activity.showLoadingDialog(true, "Analysing, wait...");
        new AsyncTask<Void, Void, Void>() {
            List<String> dates = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {
                AnalyseBonusesMethods analyseBonusesMethods = new AnalyseBonusesMethods(itemsList)
                        .setSectionsList(sectionsList).analyse();

                summaryInfoDetails = analyseBonusesMethods.getSummaryInfo();
                employeesInfoDetails = analyseBonusesMethods.getEmployeesSummaryInfo();
                buyersInfoDetails = analyseBonusesMethods.getBuyersSummaryInfo();

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                activity.closeLoadingDialog();
                showDialog();
            }
        }.execute();

    }

    private void showDialog(){
        summary_info.setText(CommonMethods.fromHtml(summaryInfoDetails));
        activity.showCustomDialog(dialogView);
    }

}
