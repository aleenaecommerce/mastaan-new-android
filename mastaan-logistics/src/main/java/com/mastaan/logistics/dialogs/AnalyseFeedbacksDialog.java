package com.mastaan.logistics.dialogs;

import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.methods.CommonMethods;
import com.google.android.material.tabs.TabLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.methods.AnalyseFeedbacksMethods;
import com.mastaan.logistics.models.FeedbackDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 22/7/16.
 */
public class AnalyseFeedbacksDialog {

    vToolBarActivity activity;
    List<FeedbackDetails> itemsList;

    TextView dialog_title;
    View totalStatisticsDialogView;
    TabLayout sectionsTabLayout;
    RadioGroup categoryGroup;
    ScrollView scrollView;
    TextView total_details;

    List<String> sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW, Constants.CATEGORY});

    List<String> categoriesList = new ArrayList<String>();
    String totalInfoDetails;
    List<String> categoryInfosDetailsList = new ArrayList<String>();

    public AnalyseFeedbacksDialog(final vToolBarActivity activity, String dialogTitle, List<FeedbackDetails> itemsList) {
        this.activity = activity;
        this.itemsList = itemsList;

        totalStatisticsDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_analyse_feedbacks, null);
        dialog_title = (TextView) totalStatisticsDialogView.findViewById(R.id.dialog_title);
        sectionsTabLayout = (TabLayout) totalStatisticsDialogView.findViewById(R.id.sectionsLayout);
        sectionsTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        categoryGroup = (RadioGroup) totalStatisticsDialogView.findViewById(R.id.categoryGroup);
        scrollView = (ScrollView) totalStatisticsDialogView.findViewById(R.id.scrollView);
        total_details = (TextView) totalStatisticsDialogView.findViewById(R.id.total_details);

        //--------

        dialog_title.setText(dialogTitle);

        totalStatisticsDialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activity.showToastMessage("Called");
                activity.closeCustomDialog();
            }
        });

        //--------

        sectionsTabLayout.removeAllTabs();
        for (int i = 0; i < sectionsList.size(); i++) {
            TabLayout.Tab tab = sectionsTabLayout.newTab();
            View rowView = LayoutInflater.from(activity).inflate(R.layout.view_tab, null, false);
            TextView tabTitle = (TextView) rowView.findViewById(R.id.tabTitle);
            tabTitle.setText(sectionsList.get(i));
            tab.setCustomView(rowView);
            sectionsTabLayout.addTab(tab);
        }

        sectionsTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //amountTotalView.setTextColor(Color.parseColor("#EF6C00"));
                if (sectionsList.get(tab.getPosition()).equalsIgnoreCase(Constants.OVERVIEW)) {
                    categoryGroup.setVisibility(View.GONE);
                    total_details.setText(Html.fromHtml(totalInfoDetails));
                } else if (sectionsList.get(tab.getPosition()).equalsIgnoreCase(Constants.CATEGORY)) {
                    categoryGroup.setVisibility(View.VISIBLE);
                    ((RadioButton) categoryGroup.getChildAt(0)).setChecked(true);
                    total_details.setText(Html.fromHtml(categoryInfosDetailsList.get(0)));
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });

    }

    public void show(){

        activity.showLoadingDialog(true, "Analysing, wait...");
        new AsyncTask<Void, Void, Void>() {
            List<String> dates = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {
                AnalyseFeedbacksMethods analyseFeedbacksMethods = new AnalyseFeedbacksMethods(itemsList);
                analyseFeedbacksMethods.analyse();

                categoriesList = analyseFeedbacksMethods.getCategoriesList();//CommonMethods.getStringListFromStringArray(new String[]{"Chicken", "Mutton", "Seafood"});
                totalInfoDetails = analyseFeedbacksMethods.getTotalInfo();
                categoryInfosDetailsList = analyseFeedbacksMethods.getCategoryInfoDetailsList();//CommonMethods.getStringListFromStringArray(new String[]{"NA", "NA", "NA"});

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                activity.closeLoadingDialog();
                showDialog();
            }
        }.execute();

    }

    private void showDialog(){

        categoryGroup.removeAllViews();
        for (int i = 0; i < categoriesList.size(); i++) {
            final int index = i;

            final RadioButton radioButton = new RadioButton(activity);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            categoryGroup.addView(radioButton, layoutParams);
            if (i == 0) {
                radioButton.setChecked(true);
            }

            radioButton.setText(categoriesList.get(i));
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        if (sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.CATEGORY)) {
                            total_details.setText(Html.fromHtml(categoryInfosDetailsList.get(index)));
                            scrollView.fullScroll(ScrollView.FOCUS_UP);
                        }
                    }
                }
            });
        }

        //-----

        total_details.setText(Html.fromHtml(totalInfoDetails));

        activity.showCustomDialog(totalStatisticsDialogView);
    }
}
