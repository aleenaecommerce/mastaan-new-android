package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.FollowupsAdapter;
import com.mastaan.logistics.backend.FollowupsAPI;
import com.mastaan.logistics.models.FollowupDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 10/02/19.
 */

public class FollowupDetailsDialog {

    MastaanToolbarActivity activity;

    View dialogView;
    TextView title;
    vRecyclerView itemHolder;
    FollowupsAdapter followupsAdapter;

    public FollowupDetailsDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_details, null);
        title = dialogView.findViewById(R.id.title);
        itemHolder = dialogView.findViewById(R.id.itemHolder);
        itemHolder.setupVerticalOrientation();
        followupsAdapter = new FollowupsAdapter(activity, new ArrayList<FollowupDetails>(), new FollowupsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onShowOrder(int position) {
                new OrderDetailsDialog(activity).show(followupsAdapter.getItem(position).getOrderID());
            }

            @Override
            public void callBuyer(int position) {
                activity.callToPhoneNumber(followupsAdapter.getItem(position).getBuyerDetails().getMobileNumber());
            }

            @Override
            public void onShowBuyerHistory(int position) {
                new BuyerOptionsDialog(activity).show(followupsAdapter.getItem(position).getBuyerDetails());
            }
        });
        itemHolder.setAdapter(followupsAdapter);

    }

    public void show(String followupID){
        getFollowupDetails(followupID);
    }

    public void show(FollowupDetails followupDetails){
        List<FollowupDetails> followupDetailsList = new ArrayList<>();
        followupDetailsList.add(followupDetails);
        followupsAdapter.setItems(followupDetailsList);
        title.setText(followupDetails.getID());
        activity.showCustomDialog(dialogView);
    }

    private void getFollowupDetails(final String followupID){

        activity.showLoadingDialog("Loading followup details, wait...");
        activity.getBackendAPIs().getFollowupsAPI().getFollowupDetails(followupID, new FollowupsAPI.FollowupDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, FollowupDetails followupDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    show(followupDetails);
                }else{
                    activity.showSnackbarMessage("Unable to load followup details, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            getFollowupDetails(followupID);
                        }
                    });
                }
            }
        });
    }


}
