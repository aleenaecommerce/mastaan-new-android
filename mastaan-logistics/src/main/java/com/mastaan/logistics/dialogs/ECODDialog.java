package com.mastaan.logistics.dialogs;

import android.text.Html;
import android.util.Log;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.models.RequestRazorpayInvoiceID;
import com.mastaan.logistics.backend.models.RequetPaytmPayment;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.payment.RazorPayCheckout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */
public class ECODDialog{

    MastaanToolbarActivity activity;
    String ECOD_PAYMENT = "eCOD Payment";
    String LINK_PAYMENT = "Link Payment";
    String LINK_PAYMENT_WITH_PAYMENT_SCREEN = "Link Payment (With payment screen)";
    String PAYTM_WALLET = "Paytm Wallet";

    StatusCallback callback;

    String orderID;
    String buyerID;

    String primaryNumber;
    String alternateNumber;

    public ECODDialog(final vToolBarActivity activity) {
        this.activity = (MastaanToolbarActivity) activity;
    }

    boolean onlyShowLinkPaymentWithPaymentScreen;
    public ECODDialog onlyShowLinkPaymentWithPaymentScreen(){
        this.onlyShowLinkPaymentWithPaymentScreen = true;
        return this;
    }

    public void showForOrder(final String orderID, final StatusCallback callback){
        showForOrder(orderID, 0, callback);
    }
    public void showForOrder(final String orderID, final double prefillAmount, final StatusCallback callback){
        showForOrder(new OrderDetails(orderID), prefillAmount, callback);
    }
    public void showForOrder(final OrderDetails orderDetails, final StatusCallback callback){
        showForOrder(orderDetails, 0, callback);;
    }
    public void showForOrder(final OrderDetails orderDetails, final double prefillAmount, final StatusCallback callback){
        this.orderID = orderDetails.getID();
        this.primaryNumber = orderDetails.getCustomerMobile();
        this.alternateNumber = orderDetails.getCustomerAlternateMobile();

        this.callback = callback;
        showECODTypeDialogAndProceed(prefillAmount);
    }


    public void showForBuyer(final String buyerID, final double prefillAmount, final StatusCallback callback) {
        showForBuyer(new BuyerDetails(buyerID), prefillAmount, callback);
    }
    public void showForBuyer(final BuyerDetails buyerDetails, final double prefillAmount, final StatusCallback callback) {
        this.buyerID = buyerDetails.getID();
        this.primaryNumber = buyerDetails.getMobileNumber();
        this.alternateNumber = buyerDetails.getAlternateMobile();

        this.callback = callback;

        showECODTypeDialogAndProceed(prefillAmount);
    }

    private void showECODTypeDialogAndProceed(double prefillAmount){
        activity.showInputNumberDecimalDialog("Enter Amount", "Amount", prefillAmount>0?prefillAmount:0, new TextInputCallback() {
            @Override
            public void onComplete(final String inputText) {
                final List<String> listItems = new ArrayList<String>();
                listItems.add(ECOD_PAYMENT);
                if ( onlyShowLinkPaymentWithPaymentScreen == false ) {
                    listItems.add(LINK_PAYMENT);
                }
                listItems.add((LINK_PAYMENT_WITH_PAYMENT_SCREEN));
                if ( buyerID != null && buyerID.length() > 0 ){
                    listItems.add(PAYTM_WALLET);
                }
                activity.showListChooserDialog(listItems, new ListChooserCallback() {
                    @Override
                    public void onSelect(final int position) {
                        if ( (primaryNumber != null && primaryNumber.trim().length() > 0)
                                || (alternateNumber != null && alternateNumber.trim().length() > 0 ) ){
                            final List<String> numbers = new ArrayList<>();
                            if ( primaryNumber != null && primaryNumber.trim().length() > 0 ) {
                                numbers.add(primaryNumber+" (Primary)");
                            }
                            if ( alternateNumber != null && alternateNumber.trim().length() > 0 ) {
                                numbers.add(alternateNumber+" (Alternate)");
                            }
                            numbers.add("Other number");

                            activity.showListChooserDialog("Select mobile", numbers, new ListChooserCallback() {
                                @Override
                                public void onSelect(int n_position) {
                                    if ( numbers.get(n_position).toLowerCase().contains("primary") ){
                                        continueWithPayment(primaryNumber, listItems.get(position), CommonMethods.parseDouble(inputText));
                                    }
                                    else if ( numbers.get(n_position).toLowerCase().contains("alternate") ){
                                        continueWithPayment(alternateNumber, listItems.get(position), CommonMethods.parseDouble(inputText));
                                    }
                                    else{
                                        activity.showInputTextDialog("Enter mobile", "Mobile", new TextInputCallback() {
                                            @Override
                                            public void onComplete(final String mobile) {
                                                if ( CommonMethods.isValidPhoneNumber(mobile.trim()) ){
                                                    continueWithPayment(mobile.trim(), listItems.get(position), CommonMethods.parseDouble(inputText));
                                                }else{
                                                    activity.showToastMessage("* Enter valid mobile number");
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        } else{
                            continueWithPayment(null, listItems.get(position), CommonMethods.parseDouble(inputText));
                        }
                    }
                    private void continueWithPayment(String mobile, String paymentType, double amount){
                        if ( paymentType.equalsIgnoreCase(ECOD_PAYMENT) ){
                            getRazorPayInvoiceIDAndProceed(mobile, Constants.ECOD_PAYMENT, amount);
                        }else if ( paymentType.equalsIgnoreCase(LINK_PAYMENT) ){
                            getRazorPayInvoiceIDAndProceed(mobile, Constants.LINK_PAYMENT, amount);
                        }else if ( paymentType.equalsIgnoreCase(LINK_PAYMENT_WITH_PAYMENT_SCREEN) ){
                            getRazorPayInvoiceIDAndProceed(mobile, Constants.LINK_PAYMENT, amount);
                        }else if ( paymentType.equalsIgnoreCase(PAYTM_WALLET) ){
                            initiatePaytmPayment(mobile, amount);
                        }
                    }
                });
            }
        });
    }

    private void initiatePaytmPayment(final String mobile, final double amount){
        activity.showLoadingDialog("Initiating Paytm payment, wait...");
        activity.getBackendAPIs().getPaymentAPI().initiatePaytmePayment(buyerID, new RequetPaytmPayment(mobile, amount), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showNotificationDialog("Success!", Html.fromHtml("Paytm payment request of amount <b>"+ SpecialCharacters.RS+ CommonMethods.getIndianFormatNumber(amount)+"</b> sent to customer."));
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while initiating paytm payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                initiatePaytmPayment(mobile, amount);
                            }
                        }
                    });
                }
            }
        });
    }

    private void getRazorPayInvoiceIDAndProceed(final String mobile, final String paymentType, final double amount){
        getRazorPayInvoiceIDAndProceed(mobile, paymentType, amount, true);
    }
    private void getRazorPayInvoiceIDAndProceed(final String mobile, final String paymentType, final double amount, final boolean showPaymentScreen){

        activity.showLoadingDialog("Initiating payment, wait...");

        if ( orderID != null && orderID.length() > 0 ){
            activity.getBackendAPIs().getPaymentAPI().getRazorpayInvoiceIDForOrder(orderID, new RequestRazorpayInvoiceID(mobile, paymentType, amount), new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String invoiceID) {
                    activity.closeLoadingDialog();
                    if ( status ){
                        if ( showPaymentScreen ) {
                            goToRazorpayPayment(invoiceID, amount);
                        }else{
                            activity.showToastMessage("Payment link generated successfully");
                        }
                    }else{
                        activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while initiating payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("RETRY")){
                                    getRazorPayInvoiceIDAndProceed(mobile, paymentType, amount, showPaymentScreen);
                                }
                            }
                        });
                    }
                }
            });
        }

        else if ( buyerID != null && buyerID.length() > 0 ){
            activity.getBackendAPIs().getPaymentAPI().getRazorpayInvoiceIDForBuyer(buyerID, new RequestRazorpayInvoiceID(mobile, paymentType, amount), new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String invoiceID) {
                    activity.closeLoadingDialog();
                    if ( status ){
                        if ( showPaymentScreen ) {
                            goToRazorpayPayment(invoiceID, amount);
                        }else{
                            activity.showToastMessage("Payment link generated successfully");
                        }
                    }else{
                        activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while initiating payment.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("RETRY")){
                                    getRazorPayInvoiceIDAndProceed(mobile, paymentType, amount, showPaymentScreen);
                                }
                            }
                        });
                    }
                }
            });
        }

        else {
            activity.showToastMessage("NA");
        }

    }

    private void goToRazorpayPayment(final String invoiceID, final double amount){
        RazorPayCheckout razorPayCheckout = new RazorPayCheckout();
        razorPayCheckout.setCallback(new RazorPayCheckout.RazorPayCallback() {
            @Override
            public void onSuccess(String razorpayPaymentID) {
                Log.d(Constants.LOG_TAG, "{RAZORPAY} , Payment success, pID: " + razorpayPaymentID);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }

            @Override
            public void onFailure(int statusCode, String message) {
                Log.d(Constants.LOG_TAG, "{RAZORPAY} , Payment failed, sC: " + statusCode + "Msg: "+message);
                if ( callback != null ){
                    callback.onComplete(false, 500, "Failure");
                }
            }
        });

        try {
            JSONObject requestObject = new JSONObject();
            requestObject.put("name", "MASTAAN");                                      //  MERCHANT NAME
            requestObject.put("description", "For Aleena ecommerce pvt.ltd");          //  DESCRIPTION
            requestObject.put("image", "http://www.mastaan.com/images/avatar.png");    //  LOGO IMAGE URL
            requestObject.put("invoice_id", invoiceID);         //  INVOICE ID
            requestObject.put("ecod", true);                                       //  ECOD FLAG
            razorPayCheckout.open(activity, requestObject);
        } catch(Exception e){
            activity.showToastMessage("Payment Exception");
        }
    }


}










/*

 private void addOnlinePaymentToOrder(final String orderID, final double amount, final String invoiceID, final String paymentID){
        activity.showLoadingDialog("Linking payment to order, wait...");
        activity.getBackendAPIs().getPaymentAPI().addOnlinePaymentForOrder(orderID, amount, invoiceID, paymentID, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    if ( callback != null ){
                        callback.onComplete(true, 200, "Success");
                    }
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while linking payment to order.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                addOnlinePaymentToOrder(orderID, amount, invoiceID, paymentID);
                            }
                        }
                    });
                }
            }
        });
    }

 */