package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.models.RequestUpdateBuyerDetails;
import com.mastaan.logistics.models.BuyerDetails;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */
public class UpdateBuyerDetailsDialog {

    MastaanToolbarActivity activity;

    public interface Callback{
        void onComplete(RequestUpdateBuyerDetails requestUpdateBuyerDetails);
    }

    public UpdateBuyerDetailsDialog(final vToolBarActivity activity) {
        this.activity = (MastaanToolbarActivity) activity;
    }

    public void show(BuyerDetails buyerDetails, final Callback callback){
        if ( buyerDetails != null ){
            View dialogBuyerProfileCompleteView = LayoutInflater.from(activity).inflate(R.layout.dialog_update_buyer_details, null);
            TextView title = dialogBuyerProfileCompleteView.findViewById(R.id.title);
            final vTextInputLayout name = dialogBuyerProfileCompleteView.findViewById(R.id.name);
            final vTextInputLayout email = dialogBuyerProfileCompleteView.findViewById(R.id.email);

            title.setText("Update "+CommonMethods.removeCountryCodeFromNumber(buyerDetails.getAvailableName())+"'s profile");
            name.setText(buyerDetails.getName());
            email.setText(buyerDetails.getEmail());

            dialogBuyerProfileCompleteView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.closeCustomDialog();
                }
            });
            dialogBuyerProfileCompleteView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String eBuyerName = name.getText();
                    name.checkError("* Enter buyer name");
                    final String eBuyerEmail = email.getText();

                    if ( eBuyerName.length() > 0 ){
                        activity.closeCustomDialog();
                        RequestUpdateBuyerDetails requestUpdateBuyerDetails = new RequestUpdateBuyerDetails(eBuyerName, eBuyerEmail);

                        if ( callback != null ){
                            callback.onComplete(requestUpdateBuyerDetails);
                        }
                    }
                }
            });
            activity.showCustomDialog(dialogBuyerProfileCompleteView);
        }else{
            activity.showToastMessage("* Buyer details required");
        }
    }

}