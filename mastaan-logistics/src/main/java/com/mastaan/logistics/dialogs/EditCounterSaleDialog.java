package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.models.RequestUpdateCounterSaleOrderItem;
import com.mastaan.logistics.models.OrderItemDetails;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */
public class EditCounterSaleDialog implements View.OnClickListener{

    MastaanToolbarActivity activity;

    View dialogView;
    vTextInputLayout item_quantity;
    vTextInputLayout item_price;
    vTextInputLayout comments;

    View update;

    OrderItemDetails orderItemDetails;
    Callback callback;

    public interface Callback{
        public void onComplete(boolean status, double newQuantity, double newPrice, String newSpecialInstrutions);
    }

    public EditCounterSaleDialog(final vToolBarActivity activity) {
        this.activity = (MastaanToolbarActivity) activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_edit_counter_sale, null);
        item_quantity = (vTextInputLayout) dialogView.findViewById(R.id.item_quantity);
        item_price = (vTextInputLayout) dialogView.findViewById(R.id.item_price);
        comments = (vTextInputLayout) dialogView.findViewById(R.id.comments);

        update = dialogView.findViewById(R.id.update);
        update.setOnClickListener(this);

    }

    public void show(OrderItemDetails orderItemDetails, Callback callback){
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        item_quantity.setHint("Item quantity (in "+orderItemDetails.getMeatItemDetails().getQuantityUnit()+"s)");
        item_quantity.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity()));
        item_price.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getAmountOfItem()));
        comments.setText(orderItemDetails.getOrderDetails().getSpecialInstructions());

        activity.showCustomDialog(dialogView);
    }

    @Override
    public void onClick(View view) {
        if ( view == update){
            String itemQuantity = item_quantity.getText();
            item_quantity.checkError("* Enter quantity");
            String itemPrice = item_price.getText();
            item_price.checkError("* Enter price");
            String commenteEntered = comments.getText();

            if ( itemQuantity.length() > 0 && itemPrice.length() > 0 ) {
                if ( Double.parseDouble(itemQuantity) > 0 ){
                    if ( Double.parseDouble(itemPrice) > 0 ){
                        activity.closeCustomDialog();

                        RequestUpdateCounterSaleOrderItem requestUpdateCounterSaleOrderItem = new RequestUpdateCounterSaleOrderItem(Double.parseDouble(itemQuantity), Double.parseDouble(itemPrice), commenteEntered);
                        updateCounterSaleOrderItem(requestUpdateCounterSaleOrderItem);
                    }else{
                        item_price.showError("* Enter valid amount");
                    }
                }else{
                    item_quantity.showError("* Enter valid quanity");
                }
            }
        }
    }

    private void updateCounterSaleOrderItem(final RequestUpdateCounterSaleOrderItem requestObject){

        activity.showLoadingDialog("Updating counter sale order, wait...");
        activity.getBackendAPIs().getOrdersAPI().updateCounterSaleOrderItem(orderItemDetails.getID(), requestObject, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Counter sale order updated successfully");
                    if ( callback != null ){
                        callback.onComplete(true, requestObject.getQuantity(), requestObject.getAmount(), requestObject.getSpecialInstructions());
                    }
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating counter sale order.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                updateCounterSaleOrderItem(requestObject);
                            }
                        }
                    });
                }
            }
        });
    }

}
