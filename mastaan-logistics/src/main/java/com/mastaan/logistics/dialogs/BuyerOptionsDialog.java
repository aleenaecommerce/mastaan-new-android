package com.mastaan.logistics.dialogs;

import android.content.Intent;
import android.net.Uri;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.activities.BuyerFeedbacksActivity;
import com.mastaan.logistics.activities.BuyerOrdersActivity;
import com.mastaan.logistics.activities.FollowupsActivity;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.activities.MoneyCollectionActivity;
import com.mastaan.logistics.backend.BuyersAPI;
import com.mastaan.logistics.backend.FollowupsAPI;
import com.mastaan.logistics.backend.models.RequestAddFollowup;
import com.mastaan.logistics.backend.models.RequestUpdateBuyerDetails;
import com.mastaan.logistics.backend.models.RequestUpdateInstallSource;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.flows.PlaceOrderFlow;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.methods.ActivityMethods;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.MoneyCollectionDetails;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 17/11/16.
 */
public class BuyerOptionsDialog {

    MastaanToolbarActivity activity;
    UserDetails userDetails;

    BuyerDetails buyerDetails;
    Callback callback;

    String CLAIM_FOLLOWUP = "Claim Followup";
    String RELEASE_FOLLOWUP = "Release Followup";
    String CALL_BUYER = "Call buyer";
    String EMAIL_BUYER = "Email buyer";
    String ORDER_HISTORY = "Order history";
    String MONEY_COLLECTION_HISTORY = "Money collection history";
    String FEEDBACK_HISTORY = "Feedback history";
    String FOLLOWUPS_HISTORY = "Followups history";
    String ENABLE_BUYER = "Enable buyer";
    String DISABLE_BUYER = "Disable buyer";
    String UPDATE_DETAILS = "Update name/email";
    String UPDATE_INSTALL_SOURCE = "Update install source";
    String ADD_FOLLOWUP = "Add followup";
    String PLACE_ORDER = "Place order";
    String SEND_MESSAGE = "Send message";
    String DISABLE_PROMOTIONAL_ALERTS = "Disable promotional alerts";
    String ENABLE_PROMOTIONAL_ALERTS = "Enable promotional alerts";

    public interface Callback{
        void onDetailsUpdated(BuyerDetails buyerDetails);
        void onEnabledBuyer();
        void onDisabledBuyer(String disabledReason);
        void onFollowupClaimed(UserDetails userDetails);
        void onFollowupReleased();
        void onFollowupAdded(FollowupDetails followupDetails);
        void onInstallSourceChange(String installSource, String installSourceDetails);
        void onUpdatePromotionalAlertsPreference(boolean preference);
    }
    public BuyerOptionsDialog setCallback(Callback callback) {
        this.callback = callback;
        return this;
    }

    public BuyerOptionsDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        userDetails = activity.localStorageData.getUserDetails();
        if ( userDetails == null ){ userDetails = new UserDetails();    }
    }

    boolean showCallEmailOptions;
    public BuyerOptionsDialog showCallEmailOptions() {
        this.showCallEmailOptions = true;
        return this;
    }

    boolean showPromotionalAlertsPreference;
    public BuyerOptionsDialog showPromotionalAlertsPreference() {
        this.showPromotionalAlertsPreference = true;
        return this;
    }

    boolean forPendingFollowup;
    public BuyerOptionsDialog forPendingFollowup() {
        this.forPendingFollowup = true;
        return this;
    }

    public BuyerOptionsDialog show(String buyerID){

        activity.showLoadingDialog("Loading buyer details, wait...");
        activity.getBackendAPIs().getBuyersAPI().getBuyerDetails(buyerID, new BuyersAPI.BuyerDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, BuyerDetails buyerDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    show(buyerDetails);
                }else{
                    activity.showToastMessage("Something went wrong, try again!");
                }
            }
        });
        return this;
    }
    public BuyerOptionsDialog show(BuyerDetails buyerDetails){
        this.buyerDetails = buyerDetails;
        show();
        return this;
    }

    public BuyerOptionsDialog show(OrderDetails orderDetails){
        this.buyerDetails = orderDetails.getBuyerDetails();
        show();
        return this;
    }

    public BuyerOptionsDialog show(OrderItemDetails orderItemDetails){
        this.buyerDetails = orderItemDetails.getOrderDetails().getBuyerDetails();
        show();
        return this;
    }

    public BuyerOptionsDialog show(FeedbackDetails feedbackDetails){
        this.buyerDetails = feedbackDetails.getOrderItemDetails().getOrderDetails().getBuyerDetails();
        show();
        return this;
    }

    public BuyerOptionsDialog show(MoneyCollectionDetails moneyCollectionDetails){
        this.buyerDetails = moneyCollectionDetails.getOrderDetails().getBuyerDetails();
        show();
        return this;
    }

    private void show(){
        final List<String> optionsList = CommonMethods.getStringListFromStringArray(new String[]{ORDER_HISTORY, MONEY_COLLECTION_HISTORY, FEEDBACK_HISTORY, FOLLOWUPS_HISTORY});

        if ( showCallEmailOptions ){
            if ( buyerDetails.getEmail() != null && buyerDetails.getEmail().length() > 0 ){
                optionsList.add(0, EMAIL_BUYER);
            }
            if ( buyerDetails.getMobileNumber() != null && buyerDetails.getMobileNumber().trim().length() > 0 ){
                if ( userDetails.isCustomerRelationshipManager() || userDetails.isDeliveryBoyManager() || userDetails.isProcessingManager() || userDetails.isOrdersManager() ) {
                    optionsList.add(0, SEND_MESSAGE);
                }
                optionsList.add(0, CALL_BUYER);
            }
        }

        if (forPendingFollowup){
            if ( buyerDetails.getFollowupEmployee() != null ){
                if ( userDetails.isSuperUser() || buyerDetails.getFollowupEmployee().getID().equals(userDetails.getID()) ){
                    optionsList.add(0, RELEASE_FOLLOWUP);
                }
            }else{
                optionsList.add(0, CLAIM_FOLLOWUP);
            }
        }

        if (ActivityMethods.isBuyerFeedbacksActivity(activity) ){
            optionsList.remove(FEEDBACK_HISTORY);
        }

        if ( ActivityMethods.isBuyersActivity(activity) ){
            if ( userDetails.isCustomerRelationshipManager()) {
                if ( buyerDetails.isEnabled()){
                    optionsList.add(DISABLE_BUYER);
                }else{
                    optionsList.add(ENABLE_BUYER);
                }
            }
            if (userDetails.isProcessingManager() || userDetails.isCustomerRelationshipManager()) {
                optionsList.add(PLACE_ORDER);
            }
        }

        if ( userDetails.isCustomerRelationshipManager()) {
            if ( buyerDetails.getName() == null || buyerDetails.getName().trim().length() == 0
                    || buyerDetails.getEmail() == null || buyerDetails.getEmail().trim().length() == 0 ){
                optionsList.add(UPDATE_DETAILS);
            }
            if ( buyerDetails.getInstallSource() == null || buyerDetails.getInstallSource().trim().length() == 0 ){
                optionsList.add(UPDATE_INSTALL_SOURCE);
            }
            optionsList.add(ADD_FOLLOWUP);
        }

        if ( showPromotionalAlertsPreference ){
            optionsList.add(buyerDetails.getPromotionalAlertsPreference()?DISABLE_PROMOTIONAL_ALERTS:ENABLE_PROMOTIONAL_ALERTS);
        }

        String title = "";
        title += "Name: "+(buyerDetails.getName()!=null&&buyerDetails.getName().trim().length()>0?buyerDetails.getName().trim():" - ");
        title += ", Mobile: "+(buyerDetails.getMobileNumber()!=null&&buyerDetails.getMobileNumber().trim().length()>0?buyerDetails.getMobileNumber().trim():" - ");
        title += ", Email: "+(buyerDetails.getEmail()!=null&&buyerDetails.getEmail().trim().length()>0?buyerDetails.getEmail().trim():" - ");
        title += ", I.Source: "+(buyerDetails.getInstallSource()!=null&&buyerDetails.getInstallSource().trim().length()>0?buyerDetails.getInstallSource().trim():" - ");

        activity.showListChooserDialog(title, optionsList, new ListChooserCallback() {
            @Override
            public void onSelect(final int position) {
                if ( optionsList.get(position).equalsIgnoreCase(ORDER_HISTORY) ){
                    Intent buyerOrderHistoryAct = new Intent(activity, BuyerOrdersActivity.class);
                    buyerOrderHistoryAct.putExtra(Constants.BUYER_ID, buyerDetails.getID());
                    buyerOrderHistoryAct.putExtra(Constants.BUYER_NAME, buyerDetails.getAvailableName());
                    activity.startActivity(buyerOrderHistoryAct);
                }
                else if ( optionsList.get(position).equalsIgnoreCase(MONEY_COLLECTION_HISTORY) ){
                    Intent moneyCollectionAct = new Intent(activity, MoneyCollectionActivity.class);
                    moneyCollectionAct.putExtra(Constants.ACTION_TYPE, Constants.BUYER_MONEY_COLLECTION);
                    moneyCollectionAct.putExtra(Constants.BUYER_ID, buyerDetails.getID());
                    moneyCollectionAct.putExtra(Constants.BUYER_NAME, buyerDetails.getAvailableName());
                    activity.startActivity(moneyCollectionAct);
                }
                else if ( optionsList.get(position).equalsIgnoreCase(FEEDBACK_HISTORY) ){
                    Intent buyerFeedbacksAct = new Intent(activity, BuyerFeedbacksActivity.class);
                    buyerFeedbacksAct.putExtra(Constants.ACTION_TYPE, Constants.BUYER_FEEDBACK_HISTORY);
                    buyerFeedbacksAct.putExtra(Constants.BUYER_ID, buyerDetails.getID());
                    buyerFeedbacksAct.putExtra(Constants.BUYER_NAME, buyerDetails.getAvailableName());
                    activity.startActivity(buyerFeedbacksAct);
                }
                else if ( optionsList.get(position).equalsIgnoreCase(FOLLOWUPS_HISTORY) ){
                    Intent followupsAct = new Intent(activity, FollowupsActivity.class);
                    followupsAct.putExtra(Constants.BUYER_ID, buyerDetails.getID());
                    followupsAct.putExtra(Constants.BUYER_NAME, buyerDetails.getAvailableName());
                    activity.startActivity(followupsAct);
                }
                else if ( optionsList.get(position).equalsIgnoreCase(CALL_BUYER) ){
                    if ( buyerDetails.getAlternateMobile() != null && buyerDetails.getAlternateMobile().trim().length() > 0 ){
                        activity.showListChooserDialog("Select Number", new String[]{buyerDetails.getMobileNumber()+" (Primary)", buyerDetails.getAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                            @Override
                            public void onSelect(int nPosition) {
                                activity.callToPhoneNumber(nPosition==1?buyerDetails.getMobileNumber():buyerDetails.getAlternateMobile());
                            }
                        });
                    }else{
                        activity.callToPhoneNumber(buyerDetails.getMobileNumber());
                    }
                }
                else if ( optionsList.get(position).equalsIgnoreCase(SEND_MESSAGE) ){
                    if ( buyerDetails.getAlternateMobile() != null && buyerDetails.getAlternateMobile().trim().length() > 0 ){
                        activity.showListChooserDialog("Select Number", new String[]{buyerDetails.getMobileNumber()+" (Primary)", buyerDetails.getAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                            @Override
                            public void onSelect(int nPosition) {
                                new MessageHandler(activity).sendMessage(nPosition==1?buyerDetails.getMobileNumber():buyerDetails.getAlternateMobile());
                            }
                        });
                    }else{
                        new MessageHandler(activity).sendMessage(buyerDetails.getMobileNumber());
                    }
                }
                else if ( optionsList.get(position).equalsIgnoreCase(EMAIL_BUYER) ){
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", buyerDetails.getEmail(), null));
                    //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                    //emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
                    activity.startActivity(Intent.createChooser(emailIntent, "Send email..."));
                }
                else if ( optionsList.get(position).equalsIgnoreCase(ENABLE_BUYER) ){
                    enableBuyer();
                }
                else if ( optionsList.get(position).equalsIgnoreCase(DISABLE_BUYER) ){
                    activity.showInputTextDialog("Disable reason!", "Enter disable reason", new TextInputCallback() {
                        @Override
                        public void onComplete(String inputText) {
                            disableBuyer(inputText);
                        }
                    });
                }
                else if ( optionsList.get(position).equalsIgnoreCase(PLACE_ORDER) ){
                    new PlaceOrderFlow(activity).start(buyerDetails.getID());
                }
                else if ( optionsList.get(position).equalsIgnoreCase(ADD_FOLLOWUP) ){
                    new FollowupDialog(activity).show(buyerDetails, new FollowupDialog.Callback() {
                        @Override
                        public void onComplete(RequestAddFollowup requestObject) {
                            if ( forPendingFollowup ){
                                requestObject.setForPendingFollowup(true);
                            }
                            addFollowup(requestObject);
                        }
                    });
                }
                else if ( optionsList.get(position).equalsIgnoreCase(UPDATE_DETAILS) ){
                    new UpdateBuyerDetailsDialog(activity).show(buyerDetails, new UpdateBuyerDetailsDialog.Callback() {
                        @Override
                        public void onComplete(RequestUpdateBuyerDetails requestUpdateBuyerDetails) {
                            updateDetails(requestUpdateBuyerDetails);
                        }
                        private void updateDetails(final RequestUpdateBuyerDetails requestUpdateBuyerDetails){
                            activity.showLoadingDialog("Updating buyer details, wait...");
                            activity.getBackendAPIs().getBuyersAPI().updateBuyerDetails(buyerDetails.getID(), requestUpdateBuyerDetails, new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCosde, String message) {
                                    activity.closeLoadingDialog();
                                    if ( status ) {
                                        if ( callback != null ){
                                            callback.onDetailsUpdated(buyerDetails.setName(requestUpdateBuyerDetails.getName()).setEmail(requestUpdateBuyerDetails.getEmail()));
                                        }
                                    }else{
                                        activity.showChoiceSelectionDialog("Confirm!", "Something went wrong while updating buyer details, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                                            @Override
                                            public void onSelect(int choiceNo, String choiceName) {
                                                if ( choiceName.equalsIgnoreCase("RETRY") ){
                                                    updateDetails(requestUpdateBuyerDetails);
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
                else if ( optionsList.get(position).equalsIgnoreCase(UPDATE_INSTALL_SOURCE) ){
                    final List<String> installSources = activity.localStorageData.getInstallSources();
                    installSources.add("Others");

                    activity.showListChooserDialog("Select install source", installSources, new ListChooserCallback() {
                        @Override
                        public void onSelect(final int position) {
                            if ( installSources.get(position).equalsIgnoreCase("Others") ){
                                activity.showInputTextDialog("Install source", "Install source", new TextInputCallback() {
                                    @Override
                                    public void onComplete(String inputText) {
                                        updateInstallSource(installSources.get(position), inputText.trim());
                                    }
                                });
                            }else{
                                updateInstallSource(installSources.get(position), "");
                            }
                        }

                        private void updateInstallSource(final String installSource, final String installSourceDetails){
                            activity.showLoadingDialog("Updating install source, wait...");
                            activity.getBackendAPIs().getBuyersAPI().updateBuyerInstallSource(buyerDetails.getID(), new RequestUpdateInstallSource(installSource, installSourceDetails), new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                    activity.closeLoadingDialog();
                                    if ( status ){
                                        activity.showToastMessage("Install source updated successfully");
                                        if ( callback != null ){
                                            callback.onInstallSourceChange(installSource, installSourceDetails);
                                        }
                                    }else{
                                        activity.showChoiceSelectionDialog("Confirm!", "Something went wrong while updating buyer install source, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                                            @Override
                                            public void onSelect(int choiceNo, String choiceName) {
                                                if ( choiceName.equalsIgnoreCase("RETRY") ){
                                                    updateInstallSource(installSource, installSourceDetails);
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
                else if ( optionsList.get(position).equalsIgnoreCase(ENABLE_PROMOTIONAL_ALERTS) || optionsList.get(position).equalsIgnoreCase(DISABLE_PROMOTIONAL_ALERTS) ){
                    final boolean enable = optionsList.get(position).equalsIgnoreCase(ENABLE_PROMOTIONAL_ALERTS)?true:false;
                    activity.showChoiceSelectionDialog("Confirm", CommonMethods.fromHtml("Are you sure to <b>"+(enable?"enable":"disable")+"</b> promotional alerts for <b>"+buyerDetails.getAvailableName()+"</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES") ){
                                activity.showLoadingDialog((enable?"Enabling":"Disabling")+" promotional alerts, wait...");
                                activity.getBackendAPIs().getBuyersAPI().updateBuyerPromotionalAlertsPreference(buyerDetails.getID(), enable, new StatusCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message) {
                                        activity.closeLoadingDialog();
                                        if ( status ){
                                            activity.showToastMessage("Promotional alerts preference updated successfully");
                                            if ( callback != null ){
                                                callback.onUpdatePromotionalAlertsPreference(enable);
                                            }
                                        }else{
                                            activity.showToastMessage("Something went wrong, try again!");
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
                else if ( optionsList.get(position).equalsIgnoreCase(CLAIM_FOLLOWUP) ){
                    activity.showLoadingDialog("Claiming followup, wait...");
                    activity.getBackendAPIs().getFollowupsAPI().claimBuyerFollowup(buyerDetails.getID(), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            activity.closeLoadingDialog();
                            if ( status ){
                                callback.onFollowupClaimed(userDetails);
                            }else{
                                activity.showToastMessage(message.equalsIgnoreCase("Error")?"Something went wrong, try again!":message);
                            }
                        }
                    });
                }
                else if ( optionsList.get(position).equalsIgnoreCase(RELEASE_FOLLOWUP) ){
                    activity.showLoadingDialog("Releasing followup, wait...");
                    activity.getBackendAPIs().getFollowupsAPI().releaseBuyerFollowup(buyerDetails.getID(), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            activity.closeLoadingDialog();
                            if ( status ){
                                callback.onFollowupReleased();
                            }else{
                                activity.showToastMessage("Something went wrong, try again!");
                            }
                        }
                    });
                }
            }
        });
    }


    private void enableBuyer(){
        activity.showLoadingDialog("Enabling buyer, wait...");
        activity.getBackendAPIs().getBuyersAPI().enableBuyer(buyerDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Buyer enabled successfully");
                    if ( callback != null ){
                        callback.onEnabledBuyer();
                    }
                }else{
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while enabling buyer.\nPlease try again.", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                enableBuyer();
                            }
                        }
                    });
                }
            }
        });
    }

    private void disableBuyer(final String disableReason){
        activity.showLoadingDialog("Disabling buyer, wait...");
        activity.getBackendAPIs().getBuyersAPI().disableBuyer(buyerDetails.getID(), disableReason, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Buyer disabled successfully");
                    if ( callback != null ){
                        callback.onDisabledBuyer(disableReason);
                    }
                }else{
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while disabling buyer.\nPlease try again.", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                disableBuyer(disableReason);
                            }
                        }
                    });
                }
            }
        });
    }

    private void addFollowup(final RequestAddFollowup requestAddFollowup){

        activity.showLoadingDialog("Adding followup, wait...");
        activity.getBackendAPIs().getFollowupsAPI().addFollowup(requestAddFollowup, new FollowupsAPI.FollowupDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, FollowupDetails followupDetails) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Followup added successfully");
                    /*Intent followupsAct = new Intent(activity, FollowupsActivity.class);
                    followupsAct.putExtra(Constants.BUYER_ID, buyerDetails.getID());
                    followupsAct.putExtra(Constants.BUYER_NAME, buyerDetails.getAvailableName());
                    activity.startActivity(followupsAct);*/
                    if ( callback != null ){
                        callback.onFollowupAdded(followupDetails);
                    }
                }
                else {
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while adding followup.\nPlease try again.", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                addFollowup(requestAddFollowup);
                            }
                        }
                    });
                }
            }
        });
    }

}
