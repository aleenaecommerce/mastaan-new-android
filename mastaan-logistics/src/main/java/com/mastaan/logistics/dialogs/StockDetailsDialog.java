package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.ProcessedStocksAdapter;
import com.mastaan.logistics.adapters.StocksAdapter;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.ProcessedStockDetails;
import com.mastaan.logistics.models.StockDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 08/08/18.
 */

public class StockDetailsDialog {

    MastaanToolbarActivity activity;

    View dialogView;
    TextView title;
    vRecyclerView itemHolder;

    public StockDetailsDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_details, null);
        title = dialogView.findViewById(R.id.title);
        itemHolder = dialogView.findViewById(R.id.itemHolder);
        itemHolder.setupVerticalOrientation();

    }

    public void show(OrderItemDetails orderItemDetails){
        if ( orderItemDetails != null ){
            if ( orderItemDetails.getStock() != null && orderItemDetails.getStock().length() > 0 ){
                showStock(orderItemDetails.getStock());
            }
            else if ( orderItemDetails.getProcessedStock() != null && orderItemDetails.getProcessedStock().length() > 0 ){
                showProcessedStock(orderItemDetails.getProcessedStock());
            }
        }
    }

    public void showStock(String stockID){
        activity.showLoadingDialog("Loading stock details, wait...");
        activity.getBackendAPIs().getStockAPI().getStockDetails(stockID, new StockAPI.StockDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, StockDetails stockDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    showStock(stockDetails);
                }else{
                    activity.showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }
    public void showStock(StockDetails stockDetails){
        final List<StockDetails> itemsList = new ArrayList<>();
        itemsList.add(stockDetails);

        title.setText(stockDetails.getID());
        itemHolder.setAdapter(new StocksAdapter(activity, itemsList, new StocksAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onOrderItemClick(int position) {

            }

            @Override
            public void onStockClick(int position) {
                if ( itemsList.get(position).getLinkedStock() != null ){
                    activity.closeCustomDialog();
                    new StockDetailsDialog(activity).showStock(itemsList.get(position).getLinkedStock());
                }else if ( itemsList.get(position).getLinkedProcessedStock() != null ){
                    activity.closeCustomDialog();
                    new StockDetailsDialog(activity).showProcessedStock(itemsList.get(position).getLinkedProcessedStock());
                }
            }
        }));

        activity.showCustomDialog(dialogView);
    }

    public void showProcessedStock(String stockID){
        activity.showLoadingDialog("Loading stock details, wait...");
        activity.getBackendAPIs().getStockAPI().getProcessedStockDetails(stockID, new StockAPI.ProcessedStockDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ProcessedStockDetails stockDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    showProcessedStock(stockDetails);
                }else{
                    activity.showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }
    public void showProcessedStock(ProcessedStockDetails processedStockDetails){
        List<ProcessedStockDetails> itemsList = new ArrayList<>();
        itemsList.add(processedStockDetails);

        title.setText(processedStockDetails.getID());
        itemHolder.setAdapter(new ProcessedStocksAdapter(activity, itemsList, null));

        activity.showCustomDialog(dialogView);
    }


}
