package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.DeliveryFeeDetails;

/**
 * Created by Venkatesh Uppu on 28/01/18.
 */

public class DeliveryFeeDialog {

    MastaanToolbarActivity activity;

    View dialogView;
    vTextInputLayout minimum_distance;
    vTextInputLayout maximum_distance;
    vTextInputLayout amount;

    Callback callback;
    public interface Callback{
        void onComplete(DeliveryFeeDetails deliveryFeeDetails);
    }

    public DeliveryFeeDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_delivery_fee, null);
        minimum_distance = (vTextInputLayout) dialogView.findViewById(R.id.minimum_distance);
        maximum_distance = (vTextInputLayout) dialogView.findViewById(R.id.maximum_distance);
        amount = (vTextInputLayout) dialogView.findViewById(R.id.amount);

        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });
        dialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String eMinDistance = minimum_distance.getText();
                minimum_distance.checkError("* Enter minimum distance");
                String eMaxDistance = maximum_distance.getText();
                maximum_distance.checkError("* Enter maximum distance");
                String eAmount = amount.getText();
                amount.checkError("* Enter amount");

                if ( eMinDistance.length() > 0 && eMaxDistance.length() > 0 && eAmount.length() > 0 ){
                    if ( CommonMethods.parseDouble(eMaxDistance) > CommonMethods.parseDouble(eMinDistance) ){
                        activity.closeCustomDialog();
                        if ( callback != null ){
                            callback.onComplete(new DeliveryFeeDetails(CommonMethods.parseDouble(eMinDistance), CommonMethods.parseDouble(eMaxDistance), CommonMethods.parseDouble(eAmount)));
                        }
                    }else{
                        activity.showToastMessage("* Maximum distance must be greater than Minimum distance");
                    }
                }
            }
        });
    }

    public void show(Callback callback){
        show(null, callback);
    }

    public void show(DeliveryFeeDetails deliveryFeeDetails, Callback callback){
        this.callback = callback;

        if ( deliveryFeeDetails != null ){
            minimum_distance.setText(CommonMethods.getInDecimalFormat(deliveryFeeDetails.getMinDistance()));
            maximum_distance.setText(CommonMethods.getInDecimalFormat(deliveryFeeDetails.getMaxDistance()));
            amount.setText(CommonMethods.getInDecimalFormat(deliveryFeeDetails.getFee()));
        }

        activity.showCustomDialog(dialogView);
    }

}
