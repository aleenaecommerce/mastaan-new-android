package com.mastaan.logistics.dialogs;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.OrderItemsAdapter;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.interfaces.OrderItemDetailsCallback;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 16/08/18.
 */

public class OrderItemDetailsDialog {

    MastaanToolbarActivity activity;

    View dialogView;
    TextView title;
    vRecyclerView itemHolder;
    OrderItemsAdapter orderItemsAdapter;

    public OrderItemDetailsDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_details, null);
        title = dialogView.findViewById(R.id.title);
        itemHolder = (vRecyclerView) dialogView.findViewById(R.id.itemHolder);
        itemHolder.setupVerticalOrientation();
        orderItemsAdapter = new OrderItemsAdapter(activity, new ArrayList<OrderItemDetails>(), new OrderItemsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onItemLongClick(int position) {

            }

            @Override
            public void onShowDirections(int position) {

            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails2 orderDetails = orderItemsAdapter.getItem(position).getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    activity.showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            activity.callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    activity.callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails2 orderDetails = orderItemsAdapter.getItem(position).getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    activity.showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowBuyerHistory(int position) {
                new BuyerOptionsDialog(activity).show(orderItemsAdapter.getItem(position));
            }

            @Override
            public void onShowReferralBuyerDetails(int position) {

            }

            @Override
            public void onPrintBill(int position) {

            }

            @Override
            public void onTrackOrder(int position) {

            }

            @Override
            public void onShowOrderDetails(int position) {

            }

            @Override
            public void onShowMoneyCollectionDetails(int position) {
                new MoneyCollectionDetailsDialog(activity).showForOrder(orderItemsAdapter.getItem(position).getOrderID());
            }

            @Override
            public void onCheckPaymentStatus(final int position) {
                activity.showLoadingDialog("Checking payment status, wait...");
                activity.getBackendAPIs().getPaymentAPI().checkOrderPaymentStatus(orderItemsAdapter.getItem(position).getOrderID(), new PaymentAPI.CheckOrderPaymentStatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, OrderDetails orderDetails) {
                        activity.closeLoadingDialog();
                        if (status){
                            getOrderItemDetails(orderItemsAdapter.getItem(position).getID());
                        }else{
                            activity.showToastMessage("Something went wrong while checking payment status.\nPlease try again!");
                        }
                    }
                });
            }

            @Override
            public void onRequestOnlinePayment(int position) {

            }

            @Override
            public void onShowItemImage(int position) {

            }

            @Override
            public void onShowStockDetails(int position) {

            }

            @Override
            public void onShowFollowupResultedInOrder(int position) {
                new FollowupDetailsDialog(activity).show(orderItemsAdapter.getItem(position).getOrderDetails().getFollowupResultedInOrder());
            }

            @Override
            public void onAcknowledge(int position) {

            }

            @Override
            public void onStartProcessing(int position) {

            }

            @Override
            public void onCompleteProcessing(int position) {

            }

            @Override
            public void onAssignDeliveryBoy(int position) {

            }

            @Override
            public void onCompleteDelivery(int position) {

            }

            @Override
            public void onReProcess(int position) {

            }

            @Override
            public void onRejectedItemsHandling(int position) {

            }

            @Override
            public void onCompleteCounterSaleOrder(int position) {

            }

            @Override
            public void onClaimForCustomerSupport(int position) {

            }

            @Override
            public void onCompleteCustomerSupport(int position) {

            }
        }).showCheckPaymentStatus();
        itemHolder.setAdapter(orderItemsAdapter);

    }

    public void show(String orderItemID){
        getOrderItemDetails(orderItemID);
    }

    public void show(OrderItemDetails orderItemDetails){
        List<OrderItemDetails> list = new ArrayList<>();
        list.add(orderItemDetails);
        orderItemsAdapter.setItems(list);
        title.setText(orderItemDetails.getID());
        activity.showCustomDialog(dialogView);
    }

    private void getOrderItemDetails(final String orderItemID){

        activity.showLoadingDialog("Loading order item details, wait...");
        activity.getBackendAPIs().getOrdersAPI().getOrderItemDetails(orderItemID, new OrderItemDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    show(orderItemDetails);
                }else{
                    activity.showSnackbarMessage("Unable to load order item details, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            getOrderItemDetails(orderItemID);
                        }
                    });
                }
            }
        });
    }

}
