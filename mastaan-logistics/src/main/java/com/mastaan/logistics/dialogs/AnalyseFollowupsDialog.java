package com.mastaan.logistics.dialogs;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.methods.CommonMethods;
import com.google.android.material.tabs.TabLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.methods.AnalyseFollowupsMethods;
import com.mastaan.logistics.models.FollowupDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 09/02/19.
 */

public class AnalyseFollowupsDialog {

    vToolBarActivity activity;
    List<FollowupDetails> itemsList = new ArrayList<>();

    View dialogView;
    TextView dialog_title;
    TabLayout sectionsTabLayout;
    ScrollView scrollView;
    TextView summary_info;

    List<String> sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW, Constants.EMPLOYEES, Constants.BUYERS});

    String summaryInfoDetails;
    String employeesInfoDetails;
    String buyersInfoDetails;
    String commentsInfoDetails;


    public AnalyseFollowupsDialog(final vToolBarActivity activity, String dialogTitle, List<FollowupDetails> itemsList) {
        this(activity, dialogTitle, CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW, Constants.EMPLOYEES, Constants.BUYERS}), itemsList);
    }
    public AnalyseFollowupsDialog(final vToolBarActivity activity, final String dialogTitle, final List<String> sectionsList, final List<FollowupDetails> itemsList) {
        this.activity = activity;
        this.itemsList = itemsList;
        this.sectionsList = sectionsList;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_analyse_followups, null);
        dialog_title = dialogView.findViewById(R.id.dialog_title);
        sectionsTabLayout = dialogView.findViewById(R.id.sectionsTabLayout);
        sectionsTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        scrollView = dialogView.findViewById(R.id.scrollView);
        summary_info = dialogView.findViewById(R.id.summary_info);

        dialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();
            }
        });

        //--------

        dialog_title.setText(dialogTitle);

        sectionsTabLayout.removeAllTabs();
        for (int i = 0; i < sectionsList.size(); i++) {
            TabLayout.Tab tab = sectionsTabLayout.newTab();
            View rowView = LayoutInflater.from(activity).inflate(R.layout.view_tab, null, false);
            TextView tabTitle = rowView.findViewById(R.id.tabTitle);
            tabTitle.setText(sectionsList.get(i));
            tab.setCustomView(rowView);
            sectionsTabLayout.addTab(tab);
        }

        sectionsTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (sectionsList.get(tab.getPosition()).equalsIgnoreCase(Constants.OVERVIEW)) {
                    summary_info.setText(CommonMethods.fromHtml(summaryInfoDetails));
                }
                else if (sectionsList.get(tab.getPosition()).equalsIgnoreCase(Constants.EMPLOYEES)) {
                    summary_info.setText(CommonMethods.fromHtml(employeesInfoDetails));
                }
                else if (sectionsList.get(tab.getPosition()).equalsIgnoreCase(Constants.BUYERS)) {
                    summary_info.setText(CommonMethods.fromHtml(buyersInfoDetails));
                }
                else if (sectionsList.get(tab.getPosition()).equalsIgnoreCase(Constants.COMMENTS)) {
                    summary_info.setText(CommonMethods.fromHtml(commentsInfoDetails));
                }
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}

        });
    }

    public void show(){
        activity.showLoadingDialog(true, "Analysing, wait...");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                AnalyseFollowupsMethods analyseFollowupsMethods = new AnalyseFollowupsMethods(itemsList)
                        .setSectionsList(sectionsList).analyse();

                summaryInfoDetails = analyseFollowupsMethods.getSummaryInfo();
                employeesInfoDetails = analyseFollowupsMethods.getEmployeesSummaryInfo();
                buyersInfoDetails = analyseFollowupsMethods.getBuyersSummaryInfo();
                commentsInfoDetails = analyseFollowupsMethods.getCommentsSummaryInfo();

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                activity.closeLoadingDialog();
                showDialog();
            }
        }.execute();

    }

    private void showDialog(){
        summary_info.setText(CommonMethods.fromHtml(summaryInfoDetails));
        activity.showCustomDialog(dialogView);
    }

}
