package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.MoneyCollectionsAdapter;
import com.mastaan.logistics.backend.MoneyCollectionAPI;
import com.mastaan.logistics.models.MoneyCollectionDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 22/09/18.
 */

public class MoneyCollectionDetailsDialog {

    MastaanToolbarActivity activity;

    View dialogView;
    TextView title;
    vRecyclerView itemHolder;
    MoneyCollectionsAdapter moneyCollectionsAdapter;

    public MoneyCollectionDetailsDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_details, null);
        title = dialogView.findViewById(R.id.title);
        itemHolder = (vRecyclerView) dialogView.findViewById(R.id.itemHolder);
        itemHolder.setupVerticalOrientation();
        moneyCollectionsAdapter = new MoneyCollectionsAdapter(activity, new ArrayList<MoneyCollectionDetails>(), new MoneyCollectionsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onTransactionClick(int position, int transactionPosition) {

            }

            @Override
            public void onCallCustomer(int position) {

            }

            @Override
            public void onShowOrderDetails(int position) {

            }

            @Override
            public void onShowBuyerHistory(int position) {

            }
        });
        itemHolder.setAdapter(moneyCollectionsAdapter);

    }


    public void show(MoneyCollectionDetails moneyCollectionDetails){
        List<MoneyCollectionDetails> list = new ArrayList<>();
        list.add(moneyCollectionDetails);
        moneyCollectionsAdapter.setItems(list);
        title.setText(moneyCollectionDetails.getID());
        activity.showCustomDialog(dialogView);
    }

    public void showForOrder(final String orderID){

        activity.showLoadingDialog("Loading money collection for order, wait...");
        activity.getBackendAPIs().getMoneyCollectionAPI().getMoneyCollectionForOrder(orderID, new MoneyCollectionAPI.MoneyCollectionCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, MoneyCollectionDetails moneyCollectionDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    if ( moneyCollectionDetails.getID() != null && moneyCollectionDetails.getID().length() > 0 ){
                        show(moneyCollectionDetails);
                    }else{
                        activity.showToastMessage("Money collection details not found for order "+orderID);
                    }
                }else{
                    activity.showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

}
