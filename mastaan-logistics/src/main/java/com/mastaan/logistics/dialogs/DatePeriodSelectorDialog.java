package com.mastaan.logistics.dialogs;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.Calendar;

/**
 * Created by Venkatesh Uppu on 05/06/18.
 */

public class DatePeriodSelectorDialog implements View.OnClickListener{

    MastaanToolbarActivity activity;

    View dialogView;
    View fromDateView;
    TextView from_date;
    View clearFromDate;
    View fromDateTabIndicator;
    View toDateView;
    TextView to_date;
    View clearToDate;
    View toDateTabIndicator;

    ViewSwitcher viewSwitcher;
    MaterialCalendarView fromDateCalendarView;
    MaterialCalendarView toDateCalendarView;

    View cancel;
    View done;

    Callback callback;

    public interface Callback{
        void onComplete(String fromDate, String toDate);
    }

    public DatePeriodSelectorDialog(final MastaanToolbarActivity activity){
        this.activity = activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_date_period_selector, null);

        fromDateView = dialogView.findViewById(R.id.fromDateView);
        fromDateView.setOnClickListener(this);
        from_date = dialogView.findViewById(R.id.from_date);
        clearFromDate = dialogView.findViewById(R.id.clearFromDate);
        clearFromDate.setOnClickListener(this);
        fromDateTabIndicator = dialogView.findViewById(R.id.fromDateTabIndicator);
        toDateView = dialogView.findViewById(R.id.toDateView);
        toDateView.setOnClickListener(this);
        to_date = dialogView.findViewById(R.id.to_date);
        to_date.setOnClickListener(this);
        clearToDate = dialogView.findViewById(R.id.clearToDate);
        clearToDate.setOnClickListener(this);
        toDateTabIndicator = dialogView.findViewById(R.id.toDateTabIndicator);

        viewSwitcher =  dialogView.findViewById(R.id.viewSwitcher);
        fromDateCalendarView = dialogView.findViewById(R.id.fromDateCalendarView);
        toDateCalendarView = dialogView.findViewById(R.id.toDateCalendarView);

        cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        done = dialogView.findViewById(R.id.done);
        done.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if ( view == fromDateView){
            viewSwitcher.setDisplayedChild(0);
            fromDateTabIndicator.setVisibility(View.VISIBLE);
            toDateTabIndicator.setVisibility(View.GONE);

            if ( toDateCalendarView.getSelectedDate() != null ) {
                fromDateCalendarView.state().edit()
                        .setMaximumDate(toDateCalendarView.getSelectedDate())
                        .commit();
            }else{
                fromDateCalendarView.state().edit()
                        .setMaximumDate(Calendar.getInstance())
                        .commit();
            }
        }
        else if ( view == clearFromDate ){
            from_date.setText("START");
            fromDateCalendarView.clearSelection();
            clearFromDate.setVisibility(View.GONE);

            toDateCalendarView.state().edit()
                    .setMinimumDate(fromDateCalendarView.getSelectedDate())
                    .commit();
        }
        else if ( view == toDateView){
            viewSwitcher.setDisplayedChild(1);
            toDateTabIndicator.setVisibility(View.VISIBLE);
            fromDateTabIndicator.setVisibility(View.GONE);

            toDateCalendarView.state().edit()
                    .setMinimumDate(fromDateCalendarView.getSelectedDate())
                    .commit();
        }
        else if ( view == clearToDate ){
            to_date.setText("END");
            toDateCalendarView.clearSelection();
            clearToDate.setVisibility(View.GONE);

            fromDateCalendarView.state().edit()
                    .setMaximumDate(Calendar.getInstance())
                    .commit();
        }
        else if ( view == cancel ){
            activity.closeCustomDialog();;
        }
        else if ( view == done ){
            activity.closeCustomDialog();
            if ( callback != null ){
                String selectedFromDate = "";
                if ( fromDateCalendarView.getSelectedDate() != null ){
                    selectedFromDate = DateMethods.getDateString(fromDateCalendarView.getSelectedDate().getDate(), DateConstants.MMM_DD_YYYY);
                }
                String selectedToDate = "";
                if ( toDateCalendarView.getSelectedDate() != null ){
                    selectedToDate = DateMethods.getDateString(toDateCalendarView.getSelectedDate().getDate(), DateConstants.MMM_DD_YYYY);
                }
                callback.onComplete(selectedFromDate, selectedToDate);
            }
        }
    }

    public void show(final String fromDateString, final String toDateString, @NonNull Callback callback){
        this.callback = callback;

        Calendar fromDateCalendar = DateMethods.getCalendarFromDate(fromDateString);
        Calendar toDateCalendar = DateMethods.getCalendarFromDate(toDateString);

        if ( fromDateString != null ){
            from_date.setText(DateMethods.getDateInFormat(fromDateString, DateConstants.MMM_DD_YYYY));
            clearFromDate.setVisibility(View.VISIBLE);
            fromDateCalendarView.setSelectedDate(fromDateCalendar);
        }else{  from_date.setText("START");  }

        if ( toDateString != null ){
            to_date.setText(DateMethods.getDateInFormat(toDateString, DateConstants.MMM_DD_YYYY));
            clearToDate.setVisibility(View.VISIBLE);
            toDateCalendarView.setSelectedDate(toDateCalendar);
        }else{  to_date.setText("END");  }

        if ( toDateString != null && toDateString.length() > 0 ){
            fromDateCalendarView.state().edit()
                    .setMaximumDate(toDateCalendar)
                    .commit();
        }else{
            fromDateCalendarView.state().edit()
                    .setMaximumDate(Calendar.getInstance())
                    .commit();
        }
        toDateCalendarView.state().edit()
                .setMinimumDate(fromDateCalendar)
                //.setMaximumDate(Calendar.getInstance())
                .commit();

        fromDateCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay calendarDay, boolean selected) {
                //String selectedDate = String.format("%02d", calendarDay.getDay()) + "/" + String.format("%02d", (calendarDay.getMonth() + 1)) + "/" + calendarDay.getYear();
                from_date.setText(DateMethods.getDateString(calendarDay.getDate(), DateConstants.MMM_DD_YYYY));
                clearFromDate.setVisibility(View.VISIBLE);
            }
        });

        toDateCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay calendarDay, boolean selected) {
                to_date.setText(DateMethods.getDateString(calendarDay.getDate(), DateConstants.MMM_DD_YYYY));
                clearToDate.setVisibility(View.VISIBLE);
            }
        });

        activity.showCustomDialog(dialogView);

    }

}
