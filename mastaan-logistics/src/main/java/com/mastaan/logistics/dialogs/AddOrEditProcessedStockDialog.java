package com.mastaan.logistics.dialogs;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.TimePickerCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.backend.VendorsAndButchersAPI;
import com.mastaan.logistics.backend.models.RequestAddOrEditProcessedStock;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.ProcessedStockDetails;
import com.mastaan.logistics.models.UserDetailsMini;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 28/07/18.
 */

public class AddOrEditProcessedStockDialog implements View.OnClickListener{

    MastaanToolbarActivity activity;
    String serverTime;

    View dialogView;
    TextView title;
    vSpinner typeSpinner;
    View detailsHolder;
    vTextInputLayout date;
    vTextInputLayout time;
    vTextInputLayout before_processing_units;
    vTextInputLayout before_processing_weight;
    vTextInputLayout after_processing_weight;
    vTextInputLayout after_processing_dressed_weight;
    vTextInputLayout after_processing_skinless_weight;
    vTextInputLayout after_processing_liver_weight;
    vTextInputLayout after_processing_gizzard_weight;
    vTextInputLayout after_processing_botee_weight;
    vTextInputLayout after_processing_head_pieces;
    vTextInputLayout after_processing_brain_pieces;
    vTextInputLayout after_processing_trotters_sets;
    vTextInputLayout after_processing_tilli_pieces;
    vTextInputLayout spoiled_units;
    vTextInputLayout spoiled_weight;
    vTextInputLayout comments;
    vTextInputLayout processed_by;

    Button actionButton;

    String[] stockTypes = new String[]{"Select stock type", Constants.FARM_CHICKEN, Constants.COUNTRY_CHICKEN, Constants.GOAT_MUTTON, Constants.SHEEP_MUTTON};

    List<UserDetailsMini> selectedProcessedBy;

    ProcessedStockDetails stockDetails;

    Callback callback;

    public interface Callback{
        void onComplete(boolean status, ProcessedStockDetails processedStockDetails);
    }


    public AddOrEditProcessedStockDialog(final vToolBarActivity activity, Callback callback) {
        this.activity = (MastaanToolbarActivity) activity;
        this.callback = callback;
        setupUI();
        title.setText("Add Processed Stock");
        actionButton.setText("CREATE");
    }
    public AddOrEditProcessedStockDialog(final vToolBarActivity activity, final ProcessedStockDetails stockDetails, Callback callback) {
        this.activity = (MastaanToolbarActivity) activity;
        this.callback = callback;
        this.stockDetails = stockDetails;
        stockTypes = new String[]{stockDetails.getType()};

        setupUI();
        title.setText("Edit Processed Stock");
        actionButton.setText("UPDATE");

        detailsHolder.setVisibility(View.VISIBLE);
        actionButton.setEnabled(true);

        selectedProcessedBy = stockDetails.getProcessedBy();
        updateUIForStockType(stockDetails.getType());

        date.setText(DateMethods.getReadableDateFromUTC(stockDetails.getDate(), DateConstants.MMM_DD_YYYY));
        time.setText(DateMethods.getReadableDateFromUTC(stockDetails.getDate(), DateConstants.HH_MM_A));

        before_processing_units.setText(CommonMethods.getInDecimalFormat(stockDetails.getBeforeProcessingUnits()));
        before_processing_weight.setText(CommonMethods.getInDecimalFormat(stockDetails.getBeforeProcessingWeight()));

        after_processing_weight.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingWeight()));
        after_processing_dressed_weight.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingDressedWeight()));
        after_processing_skinless_weight.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingSkinlessWeight()));
        after_processing_liver_weight.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingLiverWeight()));
        after_processing_gizzard_weight.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingGizzardWeight()));
        after_processing_botee_weight.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingBoteeWeight()));
        after_processing_head_pieces.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingHeadPieces()));
        after_processing_brain_pieces.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingBrainPieces()));
        after_processing_trotters_sets.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingTrotterSets()));
        after_processing_tilli_pieces.setText(CommonMethods.getInDecimalFormat(stockDetails.getAfterProcessingTilliPieces()));

        spoiled_units.setText(CommonMethods.getInDecimalFormat(stockDetails.getSpoiledUnits()));
        spoiled_weight.setText(CommonMethods.getInDecimalFormat(stockDetails.getSpoiledWeight()));

        comments.setText(stockDetails.getComments());

        processed_by.setText(getUsersNames(stockDetails.getProcessedBy()));
    }

    private void setupUI() {
        this.serverTime = new LocalStorageData(activity).getServerTime();

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_add_or_edit_processed_stock, null);
        title = dialogView.findViewById(R.id.title);
        typeSpinner = dialogView.findViewById(R.id.typeSpinner);
        detailsHolder = dialogView.findViewById(R.id.detailsHolder);
        date = dialogView.findViewById(R.id.date);
        date.getEditText().setOnClickListener(this);
        time = dialogView.findViewById(R.id.time);
        time.getEditText().setOnClickListener(this);
        before_processing_units = dialogView.findViewById(R.id.before_processing_units);
        before_processing_weight = dialogView.findViewById(R.id.before_processing_weight);
        after_processing_weight = dialogView.findViewById(R.id.after_processing_weight);
        after_processing_dressed_weight = dialogView.findViewById(R.id.after_processing_dressed_weight);
        after_processing_skinless_weight = dialogView.findViewById(R.id.after_processing_skinless_weight);
        after_processing_liver_weight = dialogView.findViewById(R.id.after_processing_liver_weight);
        after_processing_gizzard_weight = dialogView.findViewById(R.id.after_processing_gizzard_weight);
        after_processing_botee_weight = dialogView.findViewById(R.id.after_processing_botee_weight);
        after_processing_head_pieces = dialogView.findViewById(R.id.after_processing_head_pieces);
        after_processing_brain_pieces = dialogView.findViewById(R.id.after_processing_brain_pieces);
        after_processing_trotters_sets = dialogView.findViewById(R.id.after_processing_trotters_sets);
        after_processing_tilli_pieces = dialogView.findViewById(R.id.after_processing_tilli_pieces);
        spoiled_units = dialogView.findViewById(R.id.spoiled_units);
        spoiled_weight = dialogView.findViewById(R.id.spoiled_weight);
        comments = dialogView.findViewById(R.id.comments);
        processed_by = dialogView.findViewById(R.id.processed_by);
        processed_by.getEditText().setOnClickListener(this);

        actionButton = (Button) dialogView.findViewById(R.id.actionButton);
        actionButton.setOnClickListener(this);

        //--------

        ListArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_spinner, CommonMethods.getStringListFromStringArray(stockTypes));//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setPopupLayoutView(R.layout.view_spinner_popup);
        typeSpinner.setAdapter(adapter);
        if (stockTypes.length > 1) {
            typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    if (stockTypes[position].toLowerCase().contains("select")) {
                        date.setVisibility(View.GONE);
                        time.setVisibility(View.GONE);
                        detailsHolder.setVisibility(View.GONE);
                        actionButton.setEnabled(false);
                    } else {
                        date.setVisibility(View.VISIBLE);
                        time.setVisibility(View.VISIBLE);
                        detailsHolder.setVisibility(View.VISIBLE);
                        actionButton.setEnabled(false);

                        updateUIForStockType(stockTypes[position]);
                        selectedProcessedBy = new ArrayList<>();
                        processed_by.setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    private String getUsersNames(List<UserDetailsMini> usersList){
        String names = "";
        if ( usersList != null && usersList.size() > 0 ){
            for (int i=0;i<usersList.size();i++){
                if ( usersList.get(i) != null ){
                    if ( names.length() > 0 ){  names += ", ";   }
                    names += usersList.get(i).getAvailableName();
                }
            }
        }
        return names;
    }

    private void updateUIForStockType(String type){
        detailsHolder.setVisibility(View.VISIBLE);
        actionButton.setEnabled(true);
        if ( type.equalsIgnoreCase(Constants.FARM_CHICKEN) || type.equalsIgnoreCase(Constants.COUNTRY_CHICKEN) ){
            activity.toggleViewsVisibility(new View[]{after_processing_dressed_weight, after_processing_skinless_weight, after_processing_liver_weight, after_processing_gizzard_weight}, View.VISIBLE);
            activity.toggleViewsVisibility(new View[]{after_processing_weight, after_processing_botee_weight, after_processing_head_pieces, after_processing_brain_pieces, after_processing_trotters_sets, after_processing_tilli_pieces}, View.GONE);
        }
        else if ( type.equalsIgnoreCase(Constants.GOAT_MUTTON) || type.equalsIgnoreCase(Constants.SHEEP_MUTTON) ){
            activity.toggleViewsVisibility(new View[]{after_processing_weight, after_processing_botee_weight, after_processing_liver_weight, after_processing_head_pieces, after_processing_brain_pieces, after_processing_trotters_sets, after_processing_tilli_pieces}, View.VISIBLE);
            activity.toggleViewsVisibility(new View[]{after_processing_dressed_weight, after_processing_skinless_weight, after_processing_gizzard_weight}, View.GONE);
        }
    }

    public AddOrEditProcessedStockDialog show(){
        activity.showCustomDialog(dialogView);
        return this;
    }


    @Override
    public void onClick(View view) {
        if ( view == date.getEditText() ){
            String prefillDate = date.getText();
            if ( prefillDate == null || prefillDate.length() == 0 ){
                prefillDate = serverTime;
            }
            activity.showDatePickerDialog("Select date", null, serverTime, prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }

        else if ( view == time.getEditText() ){
            String prefillTime = time.getText();
            if ( prefillTime == null || prefillTime.length() == 0 ){
                prefillTime = serverTime;
            }
            activity.showTimePickerDialog("Select time", prefillTime, new TimePickerCallback() {
                @Override
                public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
                    time.setText(DateMethods.getDateInFormat(fullTimein12HrFormat, DateConstants.HH_MM_A));
                }
            });
        }

        else if ( view == processed_by.getEditText() ){
            String eStockType = "";
            try{ eStockType = stockTypes[typeSpinner.getSelectedItemPosition()]; }catch (Exception e){}

            if ( eStockType != null && eStockType.toLowerCase().contains("select") == false ){
                activity.showLoadingDialog("Loading butchers, wait...");
                activity.getBackendAPIs().getVendorsAndButchersAPI().getButchers(eStockType, new VendorsAndButchersAPI.ButchersCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, List<UserDetailsMini> butchersList) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            new EmployeeMultiSelectionDialog(activity).show(butchersList, selectedProcessedBy, new EmployeeMultiSelectionDialog.Callback() {
                                @Override
                                public void onComplete(List<UserDetailsMini> selectedEmployees) {
                                    selectedProcessedBy = selectedEmployees;
                                    processed_by.setText(getUsersNames(selectedEmployees));
                                }
                            });
                        }else{
                            activity.showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            }
            else{
                activity.showToastMessage("* Select stock type first");
            }
        }

        else if ( view == actionButton){
            String eStockType = "";
            try{ eStockType = stockTypes[typeSpinner.getSelectedItemPosition()]; }catch (Exception e){}
            String eDate = DateMethods.getDateInFormat(date.getText(), DateConstants.DD_MM_YYYY);
            date.checkError("* Enter date");
            String eTime = DateMethods.getDateInFormat(time.getText(), DateConstants.HH_MM);
            time.checkError("* Enter time");
            String eBPWeight = before_processing_weight.getText();
            before_processing_weight.checkError("* Enter before processing weight");
            String eComments = comments.getText();

            if ( eStockType.toLowerCase().contains("select") ){
                activity.showToastMessage("* Please select stock type");
                return;
            }

            if ( eStockType.length() == 0 || eDate.length() == 0 || eTime.length() == 0 || eBPWeight.length() == 0 ){
                return;
            }

            if ( selectedProcessedBy == null || selectedProcessedBy.size() == 0 ){
                activity.showToastMessage("* Select processed by");
                return;
            }


            RequestAddOrEditProcessedStock requestObject = new RequestAddOrEditProcessedStock(eStockType.toLowerCase(), eDate+" "+eTime);
            requestObject.setBeforeProcessingUnits(CommonMethods.parseDouble(before_processing_units.getText()));
            requestObject.setBeforeProcessingWeight(CommonMethods.parseDouble(eBPWeight));
            requestObject.setComments(eComments);
            requestObject.setProcessedBy(selectedProcessedBy);


            if ( eStockType.equalsIgnoreCase(Constants.FARM_CHICKEN) || eStockType.equalsIgnoreCase(Constants.COUNTRY_CHICKEN) ){
                String eAPDressedWeight = after_processing_dressed_weight.getText();
                String eAPSkinlessWeight = after_processing_skinless_weight.getText();

                if ( eAPDressedWeight.length() == 0 && eAPSkinlessWeight.length() == 0 ){
                    activity.showToastMessage("* Enter after processing dressed or skinless weight");
                    return;
                }
                requestObject.setAfterProcessingDressedWeight(CommonMethods.parseDouble(eAPDressedWeight));
                requestObject.setAfterProcessingSkinlessWeight(CommonMethods.parseDouble(eAPSkinlessWeight));
                requestObject.setAfterProcessingLiverWeight(CommonMethods.parseDouble(after_processing_liver_weight.getText()));
                requestObject.setAfterProcessingGizzardWeight(CommonMethods.parseDouble(after_processing_gizzard_weight.getText()));

                if ( requestObject.getBeforeProcessingWeight() < (requestObject.getAfterProcessingDressedWeight()+requestObject.getAfterProcessingSkinlessWeight()+requestObject.getAfterProcessingLiverWeight()+requestObject.getAfterProcessingGizzardWeight()) ){
                    activity.showToastMessage("* Total after processing weight can't be more than before processing weight");
                    return;
                }
            }
            else if ( eStockType.equalsIgnoreCase(Constants.GOAT_MUTTON) || eStockType.equalsIgnoreCase(Constants.SHEEP_MUTTON) ){
                String eAPWeight = after_processing_weight.getText();
                after_processing_weight.checkError("* Enter after processing weight");

                if ( eAPWeight.length() == 0 ){
                    return;
                }
                requestObject.setAfterProcessingWeight(CommonMethods.parseDouble(eAPWeight));
                requestObject.setAfterProcessingLiverWeight(CommonMethods.parseDouble(after_processing_liver_weight.getText()));
                requestObject.setAfterProcessingBoteeWeight(CommonMethods.parseDouble(after_processing_botee_weight.getText()));
                requestObject.setAfterProcessingHeadPieces(CommonMethods.parseLong(after_processing_head_pieces.getText()));
                requestObject.setAfterProcessingBrainPieces(CommonMethods.parseLong(after_processing_brain_pieces.getText()));
                requestObject.setAfterProcessingTrotterSets(CommonMethods.parseLong(after_processing_trotters_sets.getText()));
                requestObject.setAfterProcessingTilliPieces(CommonMethods.parseLong(after_processing_tilli_pieces.getText()));

                if ( requestObject.getBeforeProcessingWeight() < (requestObject.getAfterProcessingWeight()+requestObject.getAfterProcessingLiverWeight()+requestObject.getAfterProcessingBoteeWeight()) ){
                    activity.showToastMessage("* Total after processing weight can't be more than before processing weight");
                    return;
                }
            }

            requestObject.setSpoiledUnits(CommonMethods.parseDouble(spoiled_units.getText()));
            requestObject.setSpoiledWeight(CommonMethods.parseDouble(spoiled_weight.getText()));

            //-------

            activity.closeCustomDialog();

            if ( stockDetails != null ){
                updateStock(requestObject);
            }
            else{
                addStock(requestObject);
            }

        }
    }

    public void addStock(final RequestAddOrEditProcessedStock requestObject){

        activity.showLoadingDialog("Adding processed stock, wait...");
        activity.getBackendAPIs().getStockAPI().addProcessedStock(requestObject, new StockAPI.ProcessedStockCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ProcessedStockDetails processedStockDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Processed stock added successfully");
                    if ( callback != null ){
                        callback.onComplete(true, processedStockDetails);
                    }
                }
                else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while adding processed stock.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                addStock(requestObject);
                            }
                        }
                    });
                }
            }
        });
    }

    public void updateStock(final RequestAddOrEditProcessedStock requestObject){

        activity.showLoadingDialog("Updating processed stock, wait...");
        activity.getBackendAPIs().getStockAPI().updateProcessedStock(stockDetails.getID(), requestObject, new StockAPI.ProcessedStockCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ProcessedStockDetails processedStockDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Processed stock updated successfully");
                    if ( callback != null ){
                        callback.onComplete(true, processedStockDetails);
                    }
                }
                else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating processed stock.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                updateStock(requestObject);
                            }
                        }
                    });
                }
            }
        });
    }

}
