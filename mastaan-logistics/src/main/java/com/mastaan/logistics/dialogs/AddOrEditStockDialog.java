package com.mastaan.logistics.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TimePickerCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.activities.MeatItemDetailsActivity;
import com.mastaan.logistics.activities.MeatItemsActivity;
import com.mastaan.logistics.activities.StockCategoriesActivity;
import com.mastaan.logistics.backend.CommonAPI;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.backend.VendorsAndButchersAPI;
import com.mastaan.logistics.backend.models.RequestAddOrEditStock;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.StockDetailsCallback;
import com.mastaan.logistics.interfaces.UsersCallback;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.ProcessedStockDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.UserDetailsMini;
import com.mastaan.logistics.models.Vendor;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */
public class AddOrEditStockDialog implements View.OnClickListener{

    MastaanToolbarActivity activity;
    String serverTime;

    HubDetails hubDetails;

    View dialogView;
    TextView title;
    vSpinner typeSpinner;
    View detailsHolder;
    View amountsView;
    vTextInputLayout date;
    vTextInputLayout time;
    vTextInputLayout item_name;
    vTextInputLayout stock;
    vTextInputLayout to_hub;
    vTextInputLayout collected_employee;
    View vendorView;
    vSpinner vendorsSpinner;
    TextView vendorsLoadingIndicator;
    vTextInputLayout quantity;
    vTextInputLayout wastage_quantity;
    vTextInputLayout no_of_units;
    vTextInputLayout vendor_rate;
    vTextInputLayout packaging_cost;
    vTextInputLayout shipping_cost;
    vTextInputLayout other_costs;
    vTextInputLayout farm_rate;
    vTextInputLayout margin;
    View sourceView;
    vTextInputLayout source_name;
    vTextInputLayout source_area;
    vTextInputLayout processed_by;
    vTextInputLayout quality_check_by;
    vTextInputLayout comments;

    CheckBox printStockLabels;

    Button actionButton;

    String[] stockTypes = new String[]{"Select stock type", Constants.CATEGORY_PURCHASE, Constants.CATEGORY_CONSOLIDATION, Constants.CATEGORY_ADJUSTMENT, Constants.MEAT_ITEM_INPUT, Constants.MEAT_ITEM_PURCHASE, Constants.MEAT_ITEM_CONSOLIDATION, Constants.MEAT_ITEM_ADJUSTMENT, Constants.MEAT_ITEM_WASTAGE};
    List<Vendor> vendorsList = new ArrayList<>();

    StockDetails stockDetails;


    CategoryDetails selectedCategoryDetails;
    WarehouseMeatItemDetails selectedWareHouseMeatItemDetails;
    List<AttributeOptionDetails> selectedAttributeOptions;
    ProcessedStockDetails selectedProcessedStockDetails;
    StockDetails selectedStockDetails;
    HubDetails selectedToHubDetails;
    UserDetailsMini selectedCollectedEmployee;
    List<UserDetailsMini> selectedProcessedBy;
    UserDetailsMini selectedQualityCheckBy;

    StockDetailsCallback callback;
    StatusCallback sourceUpdateCallback;
    MarginDetailsCallback marginDetailsCallback;


    public interface MarginDetailsCallback{
        void onComplete(String marginDetails);
    }


    public AddOrEditStockDialog(final vToolBarActivity activity) {
        this.activity = (MastaanToolbarActivity) activity;
    }

    public AddOrEditStockDialog setupForMeatItem(){
        stockTypes = new String[]{"Select stock type", Constants.MEAT_ITEM_INPUT, Constants.MEAT_ITEM_PURCHASE, Constants.MEAT_ITEM_CONSOLIDATION, Constants.MEAT_ITEM_ADJUSTMENT, Constants.MEAT_ITEM_WASTAGE};
        return this;
    }
    public AddOrEditStockDialog setupForMeatItemWastage(){
        stockTypes = new String[]{Constants.MEAT_ITEM_WASTAGE};
        return this;
    }

    public AddOrEditStockDialog setupForHubTransferIn(HubDetails hubDetails){
        return setupForHub(hubDetails, new String[]{Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE});
    }
    public AddOrEditStockDialog setupForHubTransferOut(HubDetails hubDetails){
        return setupForHub(hubDetails, new String[]{Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB});
    }
    public AddOrEditStockDialog setupForHubWastage(HubDetails hubDetails){
        return setupForHub(hubDetails, new String[]{Constants.HUB_MEAT_ITEM_WASTAGE});
    }
    public AddOrEditStockDialog setupForHub(HubDetails hubDetails){
        return setupForHub(hubDetails, new String[]{"Select stock type", Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE, Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB, Constants.HUB_MEAT_ITEM_WASTAGE});
    }
    public AddOrEditStockDialog setupForHub(HubDetails hubDetails, String []types){
        if ( hubDetails != null ){
            this.hubDetails = hubDetails;
            stockTypes = types;
        }
        return this;
    }

    private void setupUI() {
        this.serverTime = new LocalStorageData(activity).getServerTime();

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_add_or_edit_stock, null);
        title = dialogView.findViewById(R.id.title);
        typeSpinner = dialogView.findViewById(R.id.typeSpinner);
        detailsHolder = dialogView.findViewById(R.id.detailsHolder);
        amountsView = dialogView.findViewById(R.id.amountsView);
        date = dialogView.findViewById(R.id.date);
        date.getEditText().setOnClickListener(this);
        time = dialogView.findViewById(R.id.time);
        time.getEditText().setOnClickListener(this);
        item_name = dialogView.findViewById(R.id.item_name);
        item_name.getEditText().setOnClickListener(this);
        stock = dialogView.findViewById(R.id.stock);
        stock.getEditText().setOnClickListener(this);
        to_hub = dialogView.findViewById(R.id.to_hub);
        to_hub.getEditText().setOnClickListener(this);
        collected_employee = dialogView.findViewById(R.id.collected_employee);
        collected_employee.getEditText().setOnClickListener(this);
        vendorView = dialogView.findViewById(R.id.vendorView);
        vendorsSpinner = dialogView.findViewById(R.id.vendorSpinner);
        vendorsLoadingIndicator = dialogView.findViewById(R.id.vendorsLoadingIndicator);
        vendorsLoadingIndicator.setOnClickListener(this);
        quantity = dialogView.findViewById(R.id.quantity);
        wastage_quantity = dialogView.findViewById(R.id.wastage_quantity);
        no_of_units = dialogView.findViewById(R.id.no_of_units);
        vendor_rate = dialogView.findViewById(R.id.vendor_rate);
        packaging_cost = dialogView.findViewById(R.id.packaging_cost);
        shipping_cost = dialogView.findViewById(R.id.shipping_cost);
        other_costs = dialogView.findViewById(R.id.other_costs);
        farm_rate = dialogView.findViewById(R.id.farm_rate);
        margin = dialogView.findViewById(R.id.margin);
        sourceView = dialogView.findViewById(R.id.sourceView);
        source_name = dialogView.findViewById(R.id.source_name);
        source_area = dialogView.findViewById(R.id.source_area);
        processed_by = dialogView.findViewById(R.id.processed_by);
        processed_by.getEditText().setOnClickListener(this);
        quality_check_by = dialogView.findViewById(R.id.quality_check_by);
        quality_check_by.getEditText().setOnClickListener(this);
        comments = dialogView.findViewById(R.id.comments);

        printStockLabels = dialogView.findViewById(R.id.printStockLabels);

        actionButton = dialogView.findViewById(R.id.actionButton);
        actionButton.setOnClickListener(this);

        //--------

        ListArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_spinner, CommonMethods.getStringListFromStringArray(stockTypes));//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setPopupLayoutView(R.layout.view_spinner_popup);
        typeSpinner.setAdapter(adapter);
        if (stockTypes.length > 1) {
            typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    if (stockTypes[position].toLowerCase().contains("select")) {
                        date.setVisibility(View.GONE);
                        time.setVisibility(View.GONE);
                        item_name.setVisibility(View.GONE);
                        detailsHolder.setVisibility(View.GONE);
                        actionButton.setEnabled(false);
                    }
                    else {
                        date.setVisibility(View.VISIBLE);
                        time.setVisibility(View.VISIBLE);
                        item_name.setVisibility(View.VISIBLE);
                        detailsHolder.setVisibility(View.GONE);
                        to_hub.setVisibility(stockTypes[position].equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB)?View.VISIBLE:View.GONE);
                        collected_employee.setVisibility(stockTypes[position].equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB)?View.VISIBLE:View.GONE);
                        processed_by.setVisibility(stockTypes[position].equalsIgnoreCase(Constants.MEAT_ITEM_INPUT)?View.VISIBLE:View.GONE);
                        quality_check_by.setVisibility(stockTypes[position].equalsIgnoreCase(Constants.MEAT_ITEM_INPUT)?View.VISIBLE:View.GONE);
                        printStockLabels.setVisibility(stockDetails==null&&stockTypes[position].equalsIgnoreCase(Constants.MEAT_ITEM_INPUT)?View.VISIBLE:View.GONE);

                        actionButton.setEnabled(false);

                        item_name.setText("");
                        to_hub.setText("");
                        processed_by.setText("");
                        quality_check_by.setText("");

                        collected_employee.setText("");
                        selectedCategoryDetails = null;
                        selectedWareHouseMeatItemDetails = null;
                        selectedStockDetails = null;
                        selectedToHubDetails = null;
                        selectedCollectedEmployee = null;
                        selectedProcessedBy = new ArrayList<>();

                        if ( prefillWarehouseMeatItemDetails != null && stockTypes[position].toLowerCase().contains("meat") ){
                            selectedWareHouseMeatItemDetails = prefillWarehouseMeatItemDetails;
                            selectedAttributeOptions = prefillAttrubutesOptions;
                            updateUIonItemSelect();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
        else if ( stockTypes.length == 1 ){
            if ( stockTypes[0].equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB) ){
                to_hub.setVisibility(View.VISIBLE);
                collected_employee.setVisibility(View.VISIBLE);
            }
        }
    }

    public AddOrEditStockDialog show(StockDetailsCallback callback){
        return show(null, callback);
    }
    public AddOrEditStockDialog show(StockDetails stockDetails, StockDetailsCallback callback){
        this.callback = callback;
        setupUI();

        if ( stockDetails != null ){
            this.stockDetails = stockDetails;
            stockTypes = new String[]{stockDetails.getType()};

            String dialogTitle = "Edit stock - "+stockDetails.getType().toLowerCase();
            if ( stockDetails.getCategoryDetails() != null ){
                dialogTitle += (" - "+stockDetails.getCategoryDetails().getName()+" "+stockDetails.getCategoryDetails().getParentCategoryName()).toLowerCase();
            }else if ( stockDetails.getWarehouseMeatItemDetails() != null ){
                dialogTitle += " - "+stockDetails.getFormattedMeatItemNameWithAttributeOptions().toLowerCase();//stockDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getNameWithCategoryAndSize().toLowerCase();
            }

            title.setText(dialogTitle);
            typeSpinner.setVisibility(View.GONE);
            item_name.setVisibility(View.GONE);
            actionButton.setText("Update");

            if ( stockDetails.getCategoryDetails() != null ){
                this.selectedCategoryDetails = stockDetails.getCategoryDetails();
            }else{
                this.selectedWareHouseMeatItemDetails = stockDetails.getWarehouseMeatItemDetails();
                this.selectedAttributeOptions = stockDetails.getAttributeOptions();
            }
            updateUIonItemSelect();

            if ( stockDetails.getLinkedStock() != null ) {
                this.selectedStockDetails = new StockDetails(stockDetails.getLinkedStock());
                stock.setText(selectedStockDetails.getID()+"\n(Loading details)");
                activity.getBackendAPIs().getStockAPI().getStockDetails(selectedStockDetails.getID(), new StockAPI.StockDetailsCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, StockDetails stockDetails) {
                        if ( status && selectedStockDetails.getID().equals(stockDetails.getID()) ){
                            selectedStockDetails = stockDetails;
                            stock.setText(selectedStockDetails.getName());
                        }else{ stock.setText(selectedStockDetails.getID()); }
                    }
                });
            } else if ( stockDetails.getLinkedProcessedStock() != null ) {
                this.selectedProcessedStockDetails = new ProcessedStockDetails(stockDetails.getLinkedProcessedStock());
                stock.setText(selectedProcessedStockDetails.getID()+"\n(Loading details)");
                activity.getBackendAPIs().getStockAPI().getProcessedStockDetails(selectedProcessedStockDetails.getID(), new StockAPI.ProcessedStockDetailsCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, ProcessedStockDetails processedStockDetails) {
                        if ( status && selectedProcessedStockDetails.getID().equals(processedStockDetails.getID()) ){
                            selectedProcessedStockDetails = processedStockDetails;
                            stock.setText(selectedProcessedStockDetails.getName());
                        }else{ stock.setText(selectedProcessedStockDetails.getID()); }
                    }
                });
            }

            if ( stockDetails.getToHubDetails() != null ){
                this.selectedToHubDetails = stockDetails.getToHubDetails();
                to_hub.setText(selectedToHubDetails.getName());
            }
            if ( stockDetails.getCollectedBy() != null ){
                this.selectedCollectedEmployee = stockDetails.getCollectedBy();
                collected_employee.setText(selectedCollectedEmployee.getAvailableName());
            }

            if ( stockDetails.getProcessedBy() != null && stockDetails.getProcessedBy().size() > 0 ){
                this.selectedProcessedBy = stockDetails.getProcessedBy();
                processed_by.setText(stockDetails.getProcessedByNames());
            }
            if ( stockDetails.getQualityCheckBy() != null ){
                this.selectedQualityCheckBy = stockDetails.getQualityCheckBy();
                quality_check_by.setText(stockDetails.getQualityCheckBy().getAvailableName());
            }

            date.setText(DateMethods.getReadableDateFromUTC(stockDetails.getDate(), DateConstants.MMM_DD_YYYY));
            time.setText(DateMethods.getReadableDateFromUTC(stockDetails.getDate(), DateConstants.HH_MM_A));

            if ( stockDetails.getQuantity() != 0 ){
                if ( stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_WASTAGE) || stockDetails.getType().equalsIgnoreCase(Constants.HUB_MEAT_ITEM_WASTAGE) ){
                    quantity.setText(CommonMethods.getInDecimalFormat(Math.abs(stockDetails.getQuantity())));
                }else{
                    quantity.setText(CommonMethods.getInDecimalFormat(stockDetails.getQuantity()));
                }
            }
            if ( stockDetails.getNoOfUnits() != 0 ){ no_of_units.setText(CommonMethods.getInDecimalFormat(stockDetails.getNoOfUnits()));   }
            if ( stockDetails.getWastageQuantity() > 0 ){   wastage_quantity.setText(CommonMethods.getInDecimalFormat(stockDetails.getWastageQuantity()));    }
            if ( stockDetails.getVendorRate() > 0 ){    vendor_rate.setText(CommonMethods.getInDecimalFormat(stockDetails.getVendorRate()));    }
            if ( stockDetails.getPackagingCost() > 0 ){ packaging_cost.setText(CommonMethods.getInDecimalFormat(stockDetails.getPackagingCost()));    }
            if ( stockDetails.getShippingCost() > 0 ){ shipping_cost.setText(CommonMethods.getInDecimalFormat(stockDetails.getShippingCost()));    }
            if ( stockDetails.getOtherCosts() > 0 ){ other_costs.setText(CommonMethods.getInDecimalFormat(stockDetails.getOtherCosts()));    }
            if ( stockDetails.getFarmRate() > 0 ){  farm_rate.setText(CommonMethods.getInDecimalFormat(stockDetails.getFarmRate()));    }
            if ( stockDetails.getMargin() > 0 ){    margin.setText(CommonMethods.getInDecimalFormat(stockDetails.getMargin())); }
            if ( stockDetails.getSource() != null && stockDetails.getSource().trim().length() > 0 ){
                String []sourceSplit = CommonMethods.getStringArrayFromString(stockDetails.getSource(), ",");
                if ( sourceSplit.length >= 1 ){
                    source_name.setText(sourceSplit[0].trim());
                }
                if ( sourceSplit.length >= 2 ){
                    source_area.setText(sourceSplit[1].trim());
                }
            }
            comments.setText(stockDetails.getComments());
        }

        if ( hubDetails != null ){
            title.append("\nHub - "+ hubDetails.getName());
        }

        activity.showCustomDialog(dialogView);

        return this;
    }

    WarehouseMeatItemDetails prefillWarehouseMeatItemDetails;
    List<AttributeOptionDetails> prefillAttrubutesOptions;
    public AddOrEditStockDialog setWarehouseMeatItemAndAttributeOptions(WarehouseMeatItemDetails warehouseMeatItemDetails, List<AttributeOptionDetails> attrubutesOptions) {
        prefillWarehouseMeatItemDetails = warehouseMeatItemDetails;
        prefillAttrubutesOptions = attrubutesOptions;
        if ( stockTypes.length == 1 ){
            selectedWareHouseMeatItemDetails = warehouseMeatItemDetails;
            selectedAttributeOptions = attrubutesOptions;
        }
        updateUIonItemSelect();
        return this;
    }

    @Override
    public void onClick(View view) {
        if ( view == date.getEditText() ){
            String prefillDate = date.getText();
            if ( prefillDate == null || prefillDate.length() == 0 ){
                prefillDate = serverTime;
            }
            activity.showDatePickerDialog("Select date", null, serverTime, prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }

        else if ( view == time.getEditText() ){
            String prefillTime = time.getText();
            if ( prefillTime == null || prefillTime.length() == 0 ){
                prefillTime = serverTime;
            }
            activity.showTimePickerDialog("Select time", prefillTime, new TimePickerCallback() {
                @Override
                public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
                    time.setText(DateMethods.getDateInFormat(fullTimein12HrFormat, DateConstants.HH_MM_A));
                }
            });
        }

        else if ( view == item_name.getEditText() ){
            if ( stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.CATEGORY_PURCHASE)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.CATEGORY_CONSOLIDATION)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT) ){
                Intent stockCategoriesAct = new Intent(activity, StockCategoriesActivity.class);
                activity.startActivityForResult(stockCategoriesAct, Constants.CATEGORIES_ACTIVITY_CODE);
            }
            else if ( stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_INPUT)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_PURCHASE)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_CONSOLIDATION)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_WASTAGE)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.HUB_MEAT_ITEM_WASTAGE)
                ){
                if ( prefillWarehouseMeatItemDetails == null ) {
                    Intent meatItemsAct = new Intent(activity, MeatItemsActivity.class);
                    meatItemsAct.putExtra(Constants.SHOW_ITEM_LEVEL_ATTRIBUTES_OPTIONS_SELECTION, true);
                    activity.startActivityForResult(meatItemsAct, Constants.MEAT_ITEMS_ACTIVITY_CODE);
                }
            }
        }
        else if ( view == vendorsLoadingIndicator ){
            getVendors();
        }
        else if ( view == stock.getEditText() ){
            if ( stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE) ){
                activity.showLoadingDialog("Loading stocks, wait...");
                activity.getBackendAPIs().getStockAPI().getStocksForWarehouseMeatItem(selectedAttributeOptions, selectedWareHouseMeatItemDetails.getID(), new StockAPI.StocksListCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, final List<StockDetails> stocksList) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            if ( stocksList.size() > 0 ) {
                                final List<String> stocksStrings = new ArrayList<>();
                                for (int i = 0; i < stocksList.size(); i++) {
                                    stocksStrings.add(stocksList.get(i).getFormattedName());
                                }
                                activity.showListChooserDialog("Select stock", stocksStrings, new ListChooserCallback() {
                                    @Override
                                    public void onSelect(int position) {
                                        selectedStockDetails = stocksList.get(position);
                                        selectedProcessedStockDetails = null;
                                        stock.setText(stocksList.get(position).getName());
                                    }
                                });
                            }else{
                                activity.showToastMessage("No stocks to show");
                            }
                        }
                        else{
                            activity.showToastMessage("Unable to load stocks, try again!");
                        }
                    }
                });
            }
            else if ( stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_INPUT) ){
                activity.showLoadingDialog("Loading processed stocks, wait...");
                activity.getBackendAPIs().getStockAPI().getProcessedStocksForCategory(selectedWareHouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getCategoryValue(), new StockAPI.ProcessedStocksListCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, final List<ProcessedStockDetails> stocksList) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            if ( stocksList.size() > 0 ) {
                                final List<String> stocksStrings = new ArrayList<>();
                                for (int i = 0; i < stocksList.size(); i++) {
                                    stocksStrings.add(stocksList.get(i).getFormattedName());
                                }

                                activity.showListChooserDialog("Select processed stock", stocksStrings, new ListChooserCallback() {
                                    @Override
                                    public void onSelect(int position) {
                                        selectedProcessedStockDetails = stocksList.get(position);
                                        selectedStockDetails = null;
                                        stock.setText(stocksList.get(position).getName());
                                    }
                                });
                            }else{
                                activity.showToastMessage("No stocks to show");
                            }
                        }else{
                            activity.showToastMessage("Unable to load processed stocks, try again!");
                        }
                    }
                });
            }
        }

        else if ( view == to_hub.getEditText() ){
            activity.showLoadingDialog("Loading hubs, wait...");
            activity.getBackendAPIs().getRoutesAPI().getHubs(new RoutesAPI.HubsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, final List<HubDetails> hubsList) {
                    activity.closeLoadingDialog();
                    if ( status ){
                        for (int i=0;i<hubsList.size();i++){
                            if ( hubsList.get(i).getID().equals(hubDetails.getID()) ){
                                hubsList.remove(i);
                                break;
                            }
                        }

                        List<String> hubsNames = new ArrayList<>();
                        for (int i=0;i<hubsList.size();i++){
                            hubsNames.add(hubsList.get(i).getName());
                        }

                        activity.showListChooserDialog("Select to hub", hubsNames, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                selectedToHubDetails = hubsList.get(position);
                                to_hub.setText(selectedToHubDetails.getName());
                            }
                        });
                    }else{
                        activity.showToastMessage("Unable to load hubs, try again!");
                    }
                }
            });
        }

        else if ( view == collected_employee.getEditText() ){
            activity.showLoadingDialog("Loading delivery boys, wait...");
            activity.getBackendAPIs().getEmployeesAPI().getDeliveryBoys(new UsersCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, final List<UserDetails> deliveryBoysList) {
                    activity.closeLoadingDialog();
                    if ( status ){
                        List<String> deliveryBoysNames = new ArrayList<>();
                        for (int i=0;i<deliveryBoysList.size();i++){
                            deliveryBoysNames.add(deliveryBoysList.get(i).getAvailableName());
                        }

                        activity.showListChooserDialog("Select delivery boy", deliveryBoysNames, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                selectedCollectedEmployee = new UserDetailsMini(deliveryBoysList.get(position).getID(), deliveryBoysList.get(position).getAvailableName());
                                collected_employee.setText(selectedCollectedEmployee.getAvailableName());
                            }
                        });
                    }else{
                        activity.showToastMessage("Unable to load employees, try again!");
                    }
                }
            });
        }

        else if ( view == processed_by.getEditText() ){
            activity.showLoadingDialog("Loading butchers, wait...");
            activity.getBackendAPIs().getVendorsAndButchersAPI().getButchers(selectedWareHouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getValue(), new VendorsAndButchersAPI.ButchersCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<UserDetailsMini> butchersList) {
                    activity.closeLoadingDialog();
                    if ( status ){
                        new EmployeeMultiSelectionDialog(activity).show(butchersList, selectedProcessedBy, new EmployeeMultiSelectionDialog.Callback() {
                            @Override
                            public void onComplete(List<UserDetailsMini> selectedEmployees) {
                                selectedProcessedBy = selectedEmployees;
                                processed_by.setText(getUsersNames(selectedEmployees));
                            }
                        });
                    }else{
                        activity.showToastMessage("Something went wrong, try again!");
                    }
                }
            });
        }

        else if ( view == quality_check_by.getEditText() ){
            activity.showLoadingDialog("Loading employees, wait...");
            activity.getBackendAPIs().getEmployeesAPI().getProcessingManagers(new UsersCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, final List<UserDetails> usersList) {
                    activity.closeLoadingDialog();
                    if ( status ){
                        List<String> userNames = new ArrayList<>();
                        for (int i=0;i<usersList.size();i++){
                            userNames.add(usersList.get(i).getAvailableName());
                        }
                        activity.showListChooserDialog("Select quality check by", userNames, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                selectedQualityCheckBy = new UserDetailsMini(usersList.get(position).getID(), usersList.get(position).getAvailableName());
                                quality_check_by.setText(usersList.get(position).getAvailableName());
                            }
                        });
                    }else{
                        activity.showToastMessage("Something went wrong, try again!");
                    }
                }
            });
        }

        else if ( view == actionButton){
            String eStockType = "";
            try{ eStockType = stockTypes[typeSpinner.getSelectedItemPosition()]; }catch (Exception e){}
            String eDate = DateMethods.getDateInFormat(date.getText(), DateConstants.DD_MM_YYYY);
            date.checkError("* Enter date");
            String eTime = DateMethods.getDateInFormat(time.getText(), DateConstants.HH_MM);
            time.checkError("* Enter time");
            item_name.checkError("* Select item");
            String eQuantity = quantity.getText();
            if ( quantity.getVisibility() == View.VISIBLE ) {
                quantity.checkError("* Enter quantity");
            }
            String eWastageQuantity = wastage_quantity.getText();
            String eNoOfUnits = no_of_units.getText();
            if ( eStockType.equalsIgnoreCase(Constants.CATEGORY_CONSOLIDATION) ) {
                no_of_units.checkError("* Enter units");
            }
            String eVendorRate = vendor_rate.getText();
            if ( vendor_rate.getVisibility() == View.VISIBLE ) {
                vendor_rate.checkError("* Enter vendor rate");
            }
            String ePackagingCost = packaging_cost.getText();
            String eShippingCost = shipping_cost.getText();
            String eOtherCost = other_costs.getText();
            String eFarmRate = farm_rate.getText();
            String eMargin = margin.getText();
            Vendor eVendor = null;
            if ( vendorView.getVisibility() ==  View.VISIBLE ) {
                try { eVendor = vendorsList.get(vendorsSpinner.getSelectedItemPosition()); } catch (Exception e) {}
            }
            String eComments = comments.getText();

            source_name.setText(source_name.getText().replaceAll(",", " ").replaceAll("\\s+", " ").trim());
            source_area.setText(source_area.getText().replaceAll(",", " ").replaceAll("\\s+", " ").trim());
            String eSource = source_name.getText().trim();
            if ( source_area.getText().trim().length() > 0 ){
                if ( eSource.length() == 0 ) {
                    activity.showToastMessage("* Please provide source name first");
                    return;
                }
                eSource += ", "+source_area.getText().trim().replaceAll(",", " ");
            }

            if ( eStockType == null && eStockType.length() == 0 && eStockType.toLowerCase().contains("select") ){
                activity.showToastMessage("* Select stock type");
                return;
            }

            if ( eStockType.equalsIgnoreCase(Constants.MEAT_ITEM_INPUT) || eStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE) ) {
                stock.checkError("* Select "+(eStockType.equalsIgnoreCase(Constants.MEAT_ITEM_INPUT)?"stock":"processed stock"));
            }
            if ( eStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB) ){
                to_hub.checkError("* Select to hub");
                collected_employee.checkError("* Select collected by employee");
            }
            if ( eStockType.equalsIgnoreCase(Constants.MEAT_ITEM_INPUT) ) {
                processed_by.checkError("* Select processed by");
                quality_check_by.checkError("* Select quality check by");
            }

            if ( selectedWareHouseMeatItemDetails == null && selectedCategoryDetails == null ){
                item_name.showError("* Select item");
                return;
            }

            if ( eDate.length() == 0 || eTime.length() == 0 ){
                return;
            }

            // Hub stocks
            if ( eStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE) || eStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB) || eStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_WASTAGE) ){
                if ( eStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE) && selectedStockDetails == null ){
                    return;
                }
                if ( eStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB) && selectedToHubDetails == null ){
                    return;
                }
                if ( eStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB) && selectedCollectedEmployee == null ){
                    return;
                }

                if ( eQuantity.length() > 0 || (eStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_WASTAGE) && Double.parseDouble(eQuantity) != 0)/*&& (selectedProcessedStockDetails != null || selectedStockDetails != null)*/ ){
                    //activity.closeCustomDialog();

                    RequestAddOrEditStock requestAddOrEditStock = null;
                    if ( selectedCategoryDetails != null ){
                        requestAddOrEditStock = new RequestAddOrEditStock(eStockType, eDate+" "+eTime, selectedCategoryDetails, null, eQuantity, "", "", "", "", "", "", "", "", "", eComments);
                    }else if ( selectedWareHouseMeatItemDetails != null ){
                        requestAddOrEditStock = new RequestAddOrEditStock(eStockType, eDate+" "+eTime, selectedWareHouseMeatItemDetails, selectedAttributeOptions, null, eQuantity, "", "", "", "", "", "", "", "", "", eComments);
                    }
                    addOrUpdateStock(requestAddOrEditStock.setHub(hubDetails.getID())
                            .setStock(selectedStockDetails!=null?selectedStockDetails.getID():null)
                            .setProcessedStock(selectedProcessedStockDetails!=null?selectedProcessedStockDetails.getID():null)
                            .setToHub(selectedToHubDetails!=null?selectedToHubDetails.getID():null)
                            .setCollectedBy(selectedCollectedEmployee!=null?selectedCollectedEmployee.getID():null)
                    );
                }
            }
            // Warehouse stocks
            else{
                if ( vendorView.getVisibility() == View.VISIBLE
                        && ( eVendor == null || eVendor.getID().toLowerCase().equalsIgnoreCase("select") ) ){
                    activity.showToastMessage("* Select vendor");
                    return;
                }

                if ( eStockType.equalsIgnoreCase(Constants.MEAT_ITEM_INPUT) && selectedProcessedStockDetails == null ){
                    return;
                }
                if ( eStockType.equalsIgnoreCase(Constants.MEAT_ITEM_INPUT) && (selectedProcessedBy == null || selectedProcessedBy.size() == 0) ){
                    return;
                }
                if ( eStockType.equalsIgnoreCase(Constants.MEAT_ITEM_INPUT) && (selectedQualityCheckBy == null) ){
                    return;
                }

                if ( (quantity.getVisibility() != View.VISIBLE || eQuantity.length() > 0)
                        && (eStockType.equalsIgnoreCase(Constants.CATEGORY_CONSOLIDATION) == false || eNoOfUnits.length() > 0)
                        && (amountsView.getVisibility() !=  View.VISIBLE || eVendorRate.length() > 0) ){
                    if ( eQuantity.length() == 0 || (Double.parseDouble(eQuantity) > 0 || ((eStockType.equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT)||eStockType.equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT)) && Double.parseDouble(eQuantity) != 0)) ){
                        if ( eNoOfUnits.length() == 0 || Double.parseDouble(eNoOfUnits) > 0 || (eStockType.equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT) && Double.parseDouble(eNoOfUnits) != 0) ){
                            if ( amountsView.getVisibility() !=  View.VISIBLE || Double.parseDouble(eVendorRate) > 0 ){
                                //activity.closeCustomDialog();

                                RequestAddOrEditStock requestAddOrEditStock = null;
                                if ( selectedCategoryDetails != null ){
                                    requestAddOrEditStock = new RequestAddOrEditStock(eStockType, eDate+" "+eTime, selectedCategoryDetails, eVendor, eQuantity, eWastageQuantity, eNoOfUnits, eVendorRate, ePackagingCost, eShippingCost, eOtherCost, eFarmRate, eMargin, eSource, eComments);
                                }else if ( selectedWareHouseMeatItemDetails != null ){
                                    requestAddOrEditStock = new RequestAddOrEditStock(eStockType, eDate+" "+eTime, selectedWareHouseMeatItemDetails, selectedAttributeOptions, eVendor, eQuantity, eWastageQuantity, eNoOfUnits, eVendorRate, ePackagingCost, eShippingCost, eOtherCost, eFarmRate, eMargin, eSource, eComments);
                                }
                                requestAddOrEditStock.setProcessedStock(selectedProcessedStockDetails!=null?selectedProcessedStockDetails.getID():null)
                                        .setProcessedBy(selectedProcessedBy)
                                        .setQualityCheckBy(selectedQualityCheckBy);

                                addOrUpdateStock(requestAddOrEditStock);
                            }else{
                                activity.showToastMessage("* Enter valid vendor rate");
                            }
                        }else{
                            activity.showToastMessage("* Enter valid units");
                        }
                    }else{
                        activity.showToastMessage("* Enter valid quantity");
                    }
                }else{
                    activity.showToastMessage("* Provide all required inputs");
                }
            }


            /*if ( eStockType != null && eStockType.length() > 0 && eStockType.toLowerCase().contains("select") == false ){
                if ( selectedWareHouseMeatItemDetails != null || selectedCategoryDetails != null ){
                    if ( eDate.length() > 0 && eTime.length() > 0 ){
                        if ( vendorView.getVisibility() !=  View.VISIBLE
                                || ( eVendor != null && eVendor.getID().toLowerCase().equalsIgnoreCase("select") == false) ){
                            if ( (quantity.getVisibility() != View.VISIBLE || eQuantity.length() > 0)
                                    && (eStockType.equalsIgnoreCase(Constants.CATEGORY_CONSOLIDATION) == false || eNoOfUnits.length() > 0)
                                    && (amountsView.getVisibility() !=  View.VISIBLE || eVendorRate.length() > 0) ){
                                if ( eQuantity.length() == 0 || (Double.parseDouble(eQuantity) > 0 || ((eStockType.equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT)||eStockType.equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT)) && Double.parseDouble(eQuantity) != 0)) ){
                                    if ( eNoOfUnits.length() == 0 || Double.parseDouble(eNoOfUnits) > 0 || (eStockType.equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT) && Double.parseDouble(eNoOfUnits) != 0) ){
                                        if ( amountsView.getVisibility() !=  View.VISIBLE || Double.parseDouble(eVendorRate) > 0 ){
                                            activity.closeCustomDialog();
                                            if ( selectedCategoryDetails != null ){
                                                addOrUpdateStock(new RequestAddOrEditStock(eStockType, eDate+" "+eTime, selectedCategoryDetails, eVendor, eQuantity, eWastageQuantity, eNoOfUnits, eVendorRate, ePackagingCost, eShippingCost, eOtherCost, eFarmRate, eMargin, eSource, eComments));
                                            }else if ( selectedWareHouseMeatItemDetails != null ){
                                                addOrUpdateStock(new RequestAddOrEditStock(eStockType, eDate+" "+eTime, selectedWareHouseMeatItemDetails, selectedAttributeOptions, eVendor, eQuantity, eWastageQuantity, eNoOfUnits, eVendorRate, ePackagingCost, eShippingCost, eOtherCost, eFarmRate, eMargin, eSource, eComments));
                                            }
                                        }else{
                                            activity.showToastMessage("* Enter valid vendor rate");
                                        }
                                    }else{
                                        activity.showToastMessage("* Enter valid units");
                                    }
                                }else{
                                    activity.showToastMessage("* Enter valid quantity");
                                }
                            }else{
                                activity.showToastMessage("* Provide all required inputs");
                            }
                        }else{
                            activity.showToastMessage("* Select vendor");
                        }
                    }
                }else{
                    item_name.showError("* Select item");
                }
            }else{
                activity.showToastMessage("* Select stock type");
            }*/
        }
    }

    public void displayVendors(List<Vendor> vendorsList){
        if ( vendorsList.size() == 0 ){ vendorsList.add(new Vendor("Select", "Select vendor"));   }
        else{   vendorsList.add(0, new Vendor("Select", "Select vendor"));  }

        this.vendorsList = vendorsList;

        vendorsSpinner.setVisibility(View.VISIBLE);
        vendorsLoadingIndicator.setVisibility(View.GONE);

        List<String> vendorsStrings = new ArrayList<>();
        for (int i=0;i<vendorsList.size();i++){
            vendorsStrings.add(CommonMethods.capitalizeFirstLetter(vendorsList.get(i).getName()));
        }
        ListArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_spinner, vendorsStrings);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setPopupLayoutView(R.layout.view_spinner_popup);
        vendorsSpinner.setAdapter(adapter);

        if ( stockDetails != null && stockDetails.getVendorDetails() != null ){
            int selectedVendorIndex = 0;
            for (int i=0;i<vendorsList.size();i++){
                if ( vendorsList.get(i).getID().equals(stockDetails.getVendorDetails().getID()) ){
                    selectedVendorIndex = i;
                    break;
                }
            }
            vendorsSpinner.setSelection(selectedVendorIndex);
        }
    }

    private void getVendors(){
        String categoryID = "";
        if ( selectedCategoryDetails != null ){ categoryID = selectedCategoryDetails.getCategoryValue();   }
        else if ( selectedWareHouseMeatItemDetails != null ){   categoryID = selectedWareHouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getCategoryValue(); }

        vendorsSpinner.setVisibility(View.INVISIBLE);
        vendorsLoadingIndicator.setText("Loading vendors, wait...");
        vendorsLoadingIndicator.setVisibility(View.VISIBLE);
        vendorsLoadingIndicator.setClickable(false);
        if ( stockTypes[typeSpinner.getSelectedItemPosition()].toLowerCase().contains("category") ){
            activity.getBackendAPIs().getCommonAPI().getLivestockVendorsForCategory(categoryID, new CommonAPI.VendorsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<Vendor> vendorsList) {
                    if ( status ){
                        displayVendors(vendorsList);
                    }else{
                        vendorsLoadingIndicator.setText("Tap here to load vendors again");
                        vendorsLoadingIndicator.setClickable(true);
                    }
                }
            });
        }else{
            activity.getBackendAPIs().getCommonAPI().getVendorsForCategory(categoryID, new CommonAPI.VendorsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<Vendor> vendorsList) {
                    if ( status ){
                        displayVendors(vendorsList);
                    }else{
                        vendorsLoadingIndicator.setText("Tap here to load vendors again");
                        vendorsLoadingIndicator.setClickable(true);
                    }
                }
            });
        }
    }

    private void addOrUpdateStock(final RequestAddOrEditStock requestAddOrEditStock){
        if ( stockDetails != null ) {
            activity.showLoadingDialog("Updating stock, wait...");
            activity.getBackendAPIs().getStockAPI().updateStock(stockDetails.getID(), requestAddOrEditStock, new StockAPI.UpdateStockCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, double cost, double totalCost) {
                    activity.closeLoadingDialog();
                    if (status) {
                        activity.closeCustomDialog();
                        if ( callback != null ){
                            String stockType = requestAddOrEditStock.getTypeName();

                            stockDetails.setCategoryDetails(selectedCategoryDetails);
                            stockDetails.setWarehouseMeatItemDetails(selectedWareHouseMeatItemDetails);
                            if ( vendorsList != null && vendorsList.size() > 0 ) {
                                stockDetails.setVendorDetails(vendorsList.get(vendorsSpinner.getSelectedItemPosition()));
                            }
                            stockDetails.setCost(cost);
                            stockDetails.setTotalCost(totalCost);

                            stockDetails.setDate(DateMethods.getDateInUTCFormat(requestAddOrEditStock.getDate()));

                            stockDetails.setLinkedStock(selectedStockDetails!=null?selectedStockDetails.getID():null);
                            stockDetails.setLinkedProcessedStock(selectedProcessedStockDetails!=null?selectedProcessedStockDetails.getID():null);
                            stockDetails.setToHubDetails(selectedToHubDetails);
                            stockDetails.setCollectedBy(selectedCollectedEmployee);
                            stockDetails.setProcessedBy(selectedProcessedBy);
                            stockDetails.setQualityCheckBy(selectedQualityCheckBy);

                            stockDetails.setVendorRate(requestAddOrEditStock.getVendorRate());
                            stockDetails.setPackagingCost(requestAddOrEditStock.getPackagingCost());
                            stockDetails.setShippingCost(requestAddOrEditStock.getShippingCost());
                            stockDetails.setOtherCost(requestAddOrEditStock.getOtherCost());
                            stockDetails.setFarmRate(requestAddOrEditStock.getFarmRate());
                            stockDetails.setQuantity((stockType.equalsIgnoreCase(Constants.MEAT_ITEM_WASTAGE)||stockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_WASTAGE))
                                    ?-1*Math.abs(requestAddOrEditStock.getQuantity())
                                        :
                                    requestAddOrEditStock.getQuantity());
                            stockDetails.setNoOfUnits(requestAddOrEditStock.getNoOfUnits());
                            stockDetails.setWastageQuantity(requestAddOrEditStock.getWastageQuantity());
                            stockDetails.setSource(requestAddOrEditStock.getSource());
                            stockDetails.setComments(requestAddOrEditStock.getComments());
                            stockDetails.setMargin(requestAddOrEditStock.getMargin());

                            callback.onComplete(true, 200, "Success", stockDetails);
                        }
                    } else {
                        activity.showToastMessage("Something went wrong, try again!");
                        /*activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating stock.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                    addOrUpdateStock(requestAddOrEditStock);
                                }
                            }
                        });*/
                    }
                }
            });
        }else{
            activity.showLoadingDialog("Adding stock, wait...");
            activity.getBackendAPIs().getStockAPI().addStock(requestAddOrEditStock, new StockAPI.StockDetailsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, StockDetails stockDetails) {
                    activity.closeLoadingDialog();
                    if (status) {
                        activity.closeCustomDialog();
                        activity.showToastMessage("Stock added successfully");
                        if ( callback != null ){
                            callback.onComplete(true, 200, "Success", stockDetails);
                        }

                        if ( printStockLabels.getVisibility() == View.VISIBLE && printStockLabels.isChecked() ){
                            new LabelPrinterQuantityInputsDialog(activity).show(selectedWareHouseMeatItemDetails, selectedAttributeOptions, stockDetails, null);
                        }
                        else{
                            if ( requestAddOrEditStock.getTypeName().equalsIgnoreCase(Constants.MEAT_ITEM_PURCHASE) && requestAddOrEditStock.getWarehouseMeatItemID() != null
                                    && requestAddOrEditStock.getSource() != null && requestAddOrEditStock.getSource().trim().length() > 0 ){
                                checkAndAskUpdateMeatItemSourceAndRedirectToItemDetailsPage(requestAddOrEditStock.getWarehouseMeatItemID(), requestAddOrEditStock.getSource(), requestAddOrEditStock.getMarginDetails());
                            }else{
                                checkAndAskRedirectToItemDetailsPage(requestAddOrEditStock.getMarginDetails());
                            }
                        }
                    }
                    else {
                        activity.showToastMessage("Something went wrong, try again!");
                        /*activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while adding stock.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                    addOrUpdateStock(requestAddOrEditStock);
                                }
                            }
                        });*/
                    }
                }
            });
        }
    }

    public AddOrEditStockDialog setSourceUpdateCallback(StatusCallback sourceUpdateCallback) {
        this.sourceUpdateCallback = sourceUpdateCallback;
        return this;
    }
    public AddOrEditStockDialog setMarginDetailsCallback(MarginDetailsCallback marginDetailsCallback) {
        this.marginDetailsCallback = marginDetailsCallback;
        return this;
    }

    private void checkAndAskUpdateMeatItemSourceAndRedirectToItemDetailsPage(final String warehouseMeatItemID, final String source, final String margin_details){
        activity.showChoiceSelectionDialog(false, "Confirm", CommonMethods.fromHtml("Do you want to update meat item source to <b>" + source + "</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if ( choiceName.equalsIgnoreCase("YES") ){
                    updateMeatItemSource(warehouseMeatItemID, source);
                }else{
                    checkAndAskRedirectToItemDetailsPage(margin_details);
                }
            }
            //-----------
            private void updateMeatItemSource(final String warehouseMeatItemID, final String source){
                activity.showLoadingDialog("Updating meat item source, wait...");
                activity.getBackendAPIs().getStockAPI().updateWarehouseMeatItemSource(warehouseMeatItemID, CommonMethods.capitalizeStringWords(source), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            activity.showToastMessage("Meat item source updated successfully");
                            if ( sourceUpdateCallback != null ){
                                sourceUpdateCallback.onComplete(true, 200, source);
                            }
                            checkAndAskRedirectToItemDetailsPage(margin_details);
                        }
                        else{
                            activity.showChoiceSelectionDialog("Failure", "Something went wrong while updating meat item source.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if ( choiceName.equalsIgnoreCase("YES") ){
                                        updateMeatItemSource(warehouseMeatItemID, source);
                                    }else{
                                        checkAndAskRedirectToItemDetailsPage(margin_details);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    boolean redirectToDetailsPage;
    public AddOrEditStockDialog redirectToDetailsPageIfApplicable() {
        this.redirectToDetailsPage = true;
        return this;
    }

    private void checkAndAskRedirectToItemDetailsPage(final String margin_details){
        try{
            String eStockType = "";
            try{ eStockType = stockTypes[typeSpinner.getSelectedItemPosition()]; }catch (Exception e){}
            String category = selectedWareHouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getValue();

            if ( eStockType.equalsIgnoreCase(Constants.MEAT_ITEM_PURCHASE)
                    && (category.equalsIgnoreCase("freshwatersf")
                        || category.equalsIgnoreCase("seawatersf")
                        || category.equalsIgnoreCase("pickles")
                        || category.equalsIgnoreCase("marinades")
                        || category.equalsIgnoreCase("eggs")
                        || category.equalsIgnoreCase("readytoeat")
                    )
                ){
                activity.showChoiceSelectionDialog(false, "Confirm", "Do you want to go to meat item details page to update prices?", "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES") ){
                            if ( marginDetailsCallback != null ){
                                marginDetailsCallback.onComplete(margin_details);
                            }

                            if ( redirectToDetailsPage ) {
                                Intent meatItemDetailsAct = new Intent(activity, MeatItemDetailsActivity.class);
                                meatItemDetailsAct.putExtra(Constants.NAME, selectedWareHouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize());
                                meatItemDetailsAct.putExtra(Constants.ID, selectedWareHouseMeatItemDetails.getID());
                                meatItemDetailsAct.putExtra(Constants.TAB, Constants.AVAILABILITY);
                                meatItemDetailsAct.putExtra(Constants.MESSAGE, margin_details);
                                activity.startActivity(meatItemDetailsAct);
                            }
                        }
                    }
                });
            }
        }catch (Exception e){}
    }

    private void updateUIonItemSelect(){

        String selectedStockType = stockTypes[typeSpinner.getSelectedItemPosition()];

        if ( selectedStockType.toLowerCase().contains("select") == false ){
            detailsHolder.setVisibility(View.VISIBLE);
            actionButton.setEnabled(true);

            String itemName = "";
            String quantityUnit = "kg";
            if ( selectedCategoryDetails != null ){
                itemName = selectedCategoryDetails.getName()+" "+selectedCategoryDetails.getParentCategoryName();
            }
            else if ( selectedWareHouseMeatItemDetails != null ){
                itemName = selectedWareHouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize();
                if ( selectedAttributeOptions != null && selectedAttributeOptions.size() > 0 ){
                    String optionsNames = "";
                    for (int i=0;i<selectedAttributeOptions.size();i++){
                        if ( selectedAttributeOptions.get(i)!= null){
                            optionsNames += (optionsNames.length()>0?", ":"") + selectedAttributeOptions.get(i).getName();
                        }
                    }
                    itemName += " ("+ optionsNames + ")";
                }
                quantityUnit = selectedWareHouseMeatItemDetails.getMeatItemDetais().getQuantityUnit();
            }

            quantity.setHint("Total quantity (in "+quantityUnit+"s)");
            wastage_quantity.setHint("Wastage quantity expected (in "+quantityUnit+"s)");
            vendor_rate.setHint("Vendor rate/"+quantityUnit+" *");
            farm_rate.setHint("Farm rate/"+quantityUnit);

            item_name.setText(CommonMethods.capitalizeFirstLetter(itemName));

            // Hub stocks
            if ( selectedStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE) || selectedStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB) || selectedStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_WASTAGE) ){
                //activity.toggleViewsVisibility(new View[]{stock}, View.VISIBLE);
                stock.setVisibility(selectedStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE)?View.VISIBLE:View.GONE);
                to_hub.setVisibility(selectedStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB)?View.VISIBLE:View.GONE);
                collected_employee.setVisibility(selectedStockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB)?View.VISIBLE:View.GONE);
                activity.toggleViewsVisibility(new View[]{amountsView, vendorView, wastage_quantity, no_of_units, sourceView, processed_by, quality_check_by}, View.GONE);
            }
            // Warehouse stocks
            else{
                if ( selectedStockType.equalsIgnoreCase(Constants.CATEGORY_CONSOLIDATION)
                        || selectedStockType.equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT)
                        || selectedStockType.equalsIgnoreCase(Constants.MEAT_ITEM_CONSOLIDATION)
                        || selectedStockType.equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT)
                        || selectedStockType.equalsIgnoreCase(Constants.MEAT_ITEM_WASTAGE) ){
                    activity.toggleViewsVisibility(new View[]{stock, vendorView, amountsView, wastage_quantity, sourceView, processed_by, quality_check_by}, View.GONE);

                    if ( selectedStockType.equalsIgnoreCase(Constants.MEAT_ITEM_CONSOLIDATION)
                            || selectedStockType.equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT)
                            || selectedStockType.equalsIgnoreCase(Constants.MEAT_ITEM_WASTAGE) ) {
                        no_of_units.setVisibility(View.GONE);
                        quantity.setVisibility(View.VISIBLE);
                        //quantity.getEditText().setInputType(stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT)?InputType.TYPE_NUMBER_FLAG_SIGNED:InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    }else{
                        no_of_units.setVisibility(View.VISIBLE);
                        quantity.setVisibility(selectedStockType.equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT)?View.VISIBLE:View.GONE);
                    }
                }
                else if ( selectedStockType.equalsIgnoreCase(Constants.MEAT_ITEM_PURCHASE ) || selectedStockType.equalsIgnoreCase(Constants.CATEGORY_PURCHASE ) ){
                    activity.toggleViewsVisibility(new View[]{vendorView, amountsView, wastage_quantity, no_of_units, sourceView}, View.VISIBLE);
                    activity.toggleViewsVisibility(new View[]{processed_by, quality_check_by}, View.GONE);
                    getVendors();
                }
                else if ( selectedStockType.equalsIgnoreCase(Constants.MEAT_ITEM_INPUT ) ){
                    activity.toggleViewsVisibility(new View[]{stock, processed_by, quality_check_by}, View.VISIBLE);
                    activity.toggleViewsVisibility(new View[]{vendorView, amountsView, wastage_quantity, no_of_units, sourceView}, View.GONE);
                }
            }
        }


        /*quantity.setText("");
        wastage_quantity.setText("");
        no_of_units.setText("");
        vendor_rate.setText("");
        other_costs.setText("");
        farm_rate.setText("");
        source.setText("");

        String itemName = "";
        String quantityUnit = "kg";
        if ( selectedCategoryDetails != null ){
            itemName = selectedCategoryDetails.getName()+" "+selectedCategoryDetails.getParentCategoryName();
        }else if ( selectedWareHouseMeatItemDetails != null ){
            itemName = selectedWareHouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize();
            if ( selectedAttributeOptions != null && selectedAttributeOptions.size() > 0 ){
                String optionsNames = "";
                for (int i=0;i<selectedAttributeOptions.size();i++){
                    if ( selectedAttributeOptions.get(i)!= null){
                        optionsNames += (optionsNames.length()>0?", ":"") + selectedAttributeOptions.get(i).getName();
                    }
                }
                itemName += " ("+ optionsNames + ")";
            }
        }
        if ( selectedWareHouseMeatItemDetails != null ){
            quantityUnit = selectedWareHouseMeatItemDetails.getMeatItemDetais().getQuantityUnit();
        }

        detailsHolder.setVisibility(View.VISIBLE);
        actionButton.setEnabled(true);

        item_name.setText(CommonMethods.capitalizeFirstLetter(itemName));
        quantity.setHint("Total quantity (in "+quantityUnit+"s)");
        wastage_quantity.setHint("Wastage quantity expected (in "+quantityUnit+"s)");
        vendor_rate.setHint("Vendor rate/"+quantityUnit+" *");
        farm_rate.setHint("Farm rate/"+quantityUnit);

        if ( stockTypes[typeSpinner.getSelectedItemPosition()].toLowerCase().contains("select") == false ){
            if ( stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.CATEGORY_CONSOLIDATION)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_CONSOLIDATION)
                    || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT) ){
                amountsView.setVisibility(View.GONE);
                wastage_quantity.setVisibility(View.GONE);
                vendorView.setVisibility(View.GONE);
                source.setVisibility(View.GONE);
                if ( stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_CONSOLIDATION)
                        || stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT) ) {
                    no_of_units.setVisibility(View.GONE);
                    quantity.setVisibility(View.VISIBLE);
                    //quantity.getEditText().setInputType(stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT)?InputType.TYPE_NUMBER_FLAG_SIGNED:InputType.TYPE_NUMBER_FLAG_DECIMAL);
                }else{
                    no_of_units.setVisibility(View.VISIBLE);
                    quantity.setVisibility(stockTypes[typeSpinner.getSelectedItemPosition()].equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT)?View.VISIBLE:View.GONE);
                }
            }else{
                amountsView.setVisibility(View.VISIBLE);
                quantity.setVisibility(View.VISIBLE);
                no_of_units.setVisibility(View.VISIBLE);
                wastage_quantity.setVisibility(View.VISIBLE);
                vendorView.setVisibility(View.VISIBLE);
                source.setVisibility(View.VISIBLE);
                getVendors();
            }
        }*/
    }

    private String getUsersNames(List<UserDetailsMini> usersList){
        String names = "";
        if ( usersList != null && usersList.size() > 0 ){
            for (int i=0;i<usersList.size();i++){
                if ( usersList.get(i) != null ){
                    if ( names.length() > 0 ){  names += ", ";   }
                    names += usersList.get(i).getAvailableName();
                }
            }
        }
        return names;
    }

    //=========

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if ( requestCode == Constants.CATEGORIES_ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {
                CategoryDetails categoryDetails = new Gson().fromJson(data.getStringExtra(Constants.CATEGORY_DETAILS), CategoryDetails.class);
                if ( categoryDetails != null ){
                    selectedCategoryDetails = categoryDetails;
                    updateUIonItemSelect();
                }
            }
            else if ( requestCode == Constants.MEAT_ITEMS_ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {
                WarehouseMeatItemDetails selectedItemDetails = new Gson().fromJson(data.getStringExtra(Constants.WAREHOUSE_MEAT_ITEM_DETAILS), WarehouseMeatItemDetails.class);
                List<AttributeOptionDetails> selectedAttributes = new ArrayList<>();
                if ( data.getStringExtra(Constants.ATTRIBUTES_OPTIONS) != null ){
                    selectedAttributes = new Gson().fromJson(data.getStringExtra(Constants.ATTRIBUTES_OPTIONS), new TypeToken<ArrayList<AttributeOptionDetails>>(){}.getType());
                }
                if ( selectedItemDetails != null ){
                    this.selectedWareHouseMeatItemDetails = selectedItemDetails;
                    this.selectedAttributeOptions = selectedAttributes;
                    updateUIonItemSelect();
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }

}
