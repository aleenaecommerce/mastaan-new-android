package com.mastaan.logistics.dialogs;

import android.os.AsyncTask;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 14/03/18.
 */

public class OrderItemTimelineDialog {

    MastaanToolbarActivity activity;

    public OrderItemTimelineDialog(final MastaanToolbarActivity activity) {
        this.activity = activity;
    }

    public void show(OrderItemDetails orderItemDetails){
        if ( orderItemDetails != null ){
            List<OrderItemDetails> orderItemsList = new ArrayList<>();
            orderItemsList.add(orderItemDetails);
            show(orderItemsList);
        }
    }
    public void show(final List<OrderItemDetails> orderItemsList){
        if ( orderItemsList != null && orderItemsList.size() > 0 ){
            new AsyncTask<Void, Void, Void>() {
                String timeline = "";

                @Override
                protected Void doInBackground(Void... voids) {
                    for (int i=0;i<orderItemsList.size();i++){
                        try{
                            OrderItemDetails orderItemDetails = orderItemsList.get(i);
                            if ( orderItemDetails != null ){
                                timeline += "<b><u>"+orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize()+"</u></b>";
                                timeline += "<br>Ordered: <b>"+ DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getCreatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b>";
                                timeline += "<br>Delivery: <b>"+ DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getDeliveryDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b>";

                                if ( orderItemDetails.getAcknowledgedBy() != null ){
                                    timeline += "<br><br>Acknowledged: <b>"+orderItemDetails.getAcknowledgedBy().getAvailableName()+"</b>";
                                }

                                if ( orderItemDetails.getProcessorDetails() != null ){
                                    timeline += "<br><br>Processing: <b>"+orderItemDetails.getProcessorDetails().getAvailableName()+"</b>";
                                    if ( orderItemDetails.getProcessingClaimDate().length() > 0 ){
                                        timeline += "<br>(claim: <b>"+DateMethods.getDateInFormat(orderItemDetails.getProcessingClaimDate(), DateConstants.HH_MM_A)+"</b>"
                                                +(orderItemDetails.getProcessingCompletedDate().length()>0?", done: <b>"+DateMethods.getDateInFormat(orderItemDetails.getProcessingCompletedDate(), DateConstants.HH_MM_A)+"</b>":"")
                                                +")";
                                    }
                                }

                                if ( orderItemDetails.getButcherDetails() != null ){
                                    timeline += "<br><br>Butcher: <b>"+orderItemDetails.getButcherDetails().getAvailableName()+"</b>";
                                }

                                if ( orderItemDetails.getDeliveryAssigner() != null ){
                                    timeline += "<br><br>Assignment: <b>"+orderItemDetails.getDeliveryAssigner().getAvailableName()+"</b>";
                                    if ( orderItemDetails.getDeliveryBoyAssignedDate().length() > 0 ){
                                        timeline += "<br>(at: <b>"+DateMethods.getDateInFormat(orderItemDetails.getDeliveryBoyAssignedDate(), DateConstants.HH_MM_A)+"</b>)";
                                    }
                                }

                                if ( orderItemDetails.getDeliveryBoyDetails() != null ){
                                    timeline += "<br><br>Delivery: <b>"+orderItemDetails.getDeliveryBoyDetails().getAvailableName()+"</b>";
                                    if ( orderItemDetails.getPickupDoneDate().length() > 0 ){
                                        timeline += "<br>(pickup: <b>"+DateMethods.getDateInFormat(orderItemDetails.getPickupDoneDate(), DateConstants.HH_MM_A)+"</b>"
                                                +(orderItemDetails.getDeliveryClaimedDate().length()>0?", claim: <b>"+DateMethods.getDateInFormat(orderItemDetails.getDeliveryClaimedDate(), DateConstants.HH_MM_A)+"</b>":"")
                                                +(orderItemDetails.getDeliveredDate().length()>0?", done: <b>"+DateMethods.getDateInFormat(orderItemDetails.getDeliveredDate(), DateConstants.HH_MM_A)+"</b>":"")
                                                +")";
                                    }
                                }
                            }
                        }catch (Exception e){}
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( timeline != null && timeline.trim().length() > 0 ){
                        activity.showNotificationDialog("Timeline", CommonMethods.fromHtml(timeline));
                    }
                }
            }.execute();
        }
    }

}
