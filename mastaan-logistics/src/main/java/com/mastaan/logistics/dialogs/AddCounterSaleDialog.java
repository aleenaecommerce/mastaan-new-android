package com.mastaan.logistics.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.activities.MeatItemsActivity;
import com.mastaan.logistics.backend.models.RequestCreateCounterSaleOrder;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemDetailsCallback;
import com.mastaan.logistics.interfaces.UsersCallback;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */
public class AddCounterSaleDialog implements View.OnClickListener{

    MastaanToolbarActivity activity;

    View dialogView;
    vSpinner typeSpinner;
    vTextInputLayout item_name;
    vTextInputLayout item_quantity;
    vTextInputLayout item_price;
    View amountCollectionConfirmationView;
    RadioButton amount_collected;
    RadioButton amount_not_collected;
    View employeesView;
    vSpinner employeeSpinner;
    TextView employeesLoadingIndicator;
    vTextInputLayout customer_name;
    vTextInputLayout comments;

    View create;


    String []typesList = new String[]{Constants.FOR_CUSTOMER, Constants.FOR_EMPLOYEE};
    List<UserDetails> employeesList = new ArrayList<>();

    WarehouseMeatItemDetails selectedWareHouseMeatItemDetails;

    public AddCounterSaleDialog(final vToolBarActivity activity) {
        this.activity = (MastaanToolbarActivity) activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_add_counter_sale, null);
        typeSpinner = (vSpinner) dialogView.findViewById(R.id.typeSpinner);
        item_name = (vTextInputLayout) dialogView.findViewById(R.id.item_name);
        item_name.getEditText().setOnClickListener(this);
        item_quantity = (vTextInputLayout) dialogView.findViewById(R.id.item_quantity);
        item_price = (vTextInputLayout) dialogView.findViewById(R.id.item_price);
        amountCollectionConfirmationView = dialogView.findViewById(R.id.amountCollectionConfirmationView);
        amount_collected = (RadioButton) dialogView.findViewById(R.id.amount_collected);
        amount_not_collected = (RadioButton) dialogView.findViewById(R.id.amount_not_collected);
        customer_name = (vTextInputLayout) dialogView.findViewById(R.id.customer_name);
        employeesView = dialogView.findViewById(R.id.employeesView);
        employeeSpinner = (vSpinner) dialogView.findViewById(R.id.employeeSpinner);
        employeesLoadingIndicator = (TextView) dialogView.findViewById(R.id.employeesLoadingIndicator);
        employeesLoadingIndicator.setOnClickListener(this);
        comments = (vTextInputLayout) dialogView.findViewById(R.id.comments);

        create = dialogView.findViewById(R.id.create);
        create.setOnClickListener(this);

        //--------

        ListArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_spinner, typesList);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setPopupLayoutView(R.layout.view_spinner_popup);
        typeSpinner.setAdapter(adapter);
        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                amount_collected.setChecked(false);
                amount_not_collected.setChecked(false);
                if ( typesList[position].equalsIgnoreCase(Constants.FOR_EMPLOYEE)) {
                    employeesView.setVisibility(View.VISIBLE);
                    customer_name.setVisibility(View.GONE);
                    amountCollectionConfirmationView.setVisibility(View.GONE);
                    if ( employeesList == null || employeesList.size() == 0 ){
                        getEmployees();
                    }
                } else {
                    customer_name.setVisibility(View.VISIBLE);
                    amountCollectionConfirmationView.setVisibility(View.VISIBLE);
                    employeesView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void show(){
        activity.showCustomDialog(dialogView);
    }

    @Override
    public void onClick(View view) {
        if ( view == item_name.getEditText() ){
            Intent meatItemsAct = new Intent(activity, MeatItemsActivity.class);
            activity.startActivityForResult(meatItemsAct, Constants.MEAT_ITEMS_ACTIVITY_CODE);
        }
        else if ( view == employeesLoadingIndicator ){
            getEmployees();
        }
        else if ( view == create ){
            if ( selectedWareHouseMeatItemDetails != null ){

                item_name.checkError("* Select item");
                String itemQuantity = item_quantity.getText();
                item_quantity.checkError("* Enter quantity");
                String itemPrice = item_price.getText();
                item_price.checkError("* Enter price");
                String commenteEntered = comments.getText();
                String selectedEmployeeID = null;
                String customerName = null;
                if ( employeesView.getVisibility() ==  View.VISIBLE ) {
                    try { selectedEmployeeID = employeesList.get(employeeSpinner.getSelectedItemPosition()).getID(); } catch (Exception e) {}
                }else{
                    customerName = customer_name.getText();
                }

                if ( itemQuantity.length() > 0 && itemPrice.length() > 0 ) {
                    if ( Double.parseDouble(itemQuantity) > 0 ){
                        if ( Double.parseDouble(itemPrice) > 0 ){
                            if ( amountCollectionConfirmationView.getVisibility() != View.VISIBLE || (amount_collected.isChecked() || amount_not_collected.isChecked()) ){
                                if ( employeesView.getVisibility() != View.VISIBLE
                                        || (selectedEmployeeID != null && selectedEmployeeID.length() > 0 && selectedEmployeeID.toLowerCase().contains("select") == false) ){
                                    activity.closeCustomDialog();

                                    boolean isAmountCollected = false;
                                    if ( amount_collected.isChecked() ){    isAmountCollected = true;   }

                                    RequestCreateCounterSaleOrder requestCreateCounterSaleOrder = new RequestCreateCounterSaleOrder(selectedWareHouseMeatItemDetails.getID(), Double.parseDouble(itemQuantity), Double.parseDouble(itemPrice), isAmountCollected, selectedEmployeeID, customerName, commenteEntered);
                                    createCounterSaleOrder(requestCreateCounterSaleOrder);
                                }else{
                                    activity.showToastMessage("* Select employee");
                                }
                            }else{
                                activity.showToastMessage("* Provide amount collected or not");
                            }

                        }else{
                            item_price.showError("* Enter valid amount");
                        }
                    }else{
                        item_quantity.showError("* Enter valid quanity");
                    }
                }

            }else{
                item_name.showError("* Select item");
            }
        }
    }

    private void displayEmployees(List<UserDetails> employeesList){
        if ( employeesList.size() == 0 ){ employeesList.add(new UserDetails("Select", "Select employee"));   }
        else{   employeesList.add(0, new UserDetails("Select", "Select employee"));  }

        this.employeesList = employeesList;

        employeeSpinner.setVisibility(View.VISIBLE);
        employeesLoadingIndicator.setVisibility(View.GONE);

        List<String> employeesStrings = new ArrayList<>();
        for (int i=0;i<employeesList.size();i++){
            employeesStrings.add(CommonMethods.capitalizeFirstLetter(employeesList.get(i).getName()));
        }
        ListArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_spinner, employeesStrings);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setPopupLayoutView(R.layout.view_spinner_popup);
        employeeSpinner.setAdapter(adapter);
        employeeSpinner.setSelection(0);
    }

    private void getEmployees(){

        employeeSpinner.setVisibility(View.INVISIBLE);
        employeesLoadingIndicator.setText("Loading employees, wait...");
        employeesLoadingIndicator.setVisibility(View.VISIBLE);
        employeesLoadingIndicator.setClickable(false);
        activity.getBackendAPIs().getEmployeesAPI().getEmployees(new UsersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<UserDetails> employeesList) {
                if (status == true) {
                    displayEmployees(employeesList);
                } else {
                    employeesLoadingIndicator.setText("Tap here to load employees again");
                    employeesLoadingIndicator.setClickable(true);
                }
            }
        });
    }

    private void createCounterSaleOrder(final RequestCreateCounterSaleOrder requesetCreateCounterSaleOrder){

        activity.showLoadingDialog("Creating counter sale order, wait...");
        activity.getBackendAPIs().getOrdersAPI().createCounterSaleOrder(requesetCreateCounterSaleOrder, new OrderItemDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.getOrderItemsDatabase().addOrUpdateOrderItem(orderItemDetails);

                    if ( orderItemDetails.isCounterSaleEmployeeOrder() ) {
                        Intent fireBaseReceiver = new Intent(Constants.FIREBASE_RECEIVER)
                                .putExtra("type", Constants.ADD_CS_LISTENER_FOR_ORDER_ITEM)
                                .putExtra(Constants.ORDER_ITEM_DETAILS, new Gson().toJson(orderItemDetails));
                        activity.sendBroadcast(fireBaseReceiver);
                    }
                    activity.showToastMessage("Counter sale order created successfully");
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while creating counter sale order.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                createCounterSaleOrder(requesetCreateCounterSaleOrder);
                            }
                        }
                    });
                }
            }
        });
    }

    //=========

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if ( requestCode == Constants.MEAT_ITEMS_ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {
                WarehouseMeatItemDetails selectedItemDetails = new Gson().fromJson(data.getStringExtra(Constants.WAREHOUSE_MEAT_ITEM_DETAILS), WarehouseMeatItemDetails.class);
                if ( selectedItemDetails != null ){
                    selectedWareHouseMeatItemDetails = selectedItemDetails;
                    item_name.setText(CommonMethods.capitalizeFirstLetter(selectedWareHouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize()));
                    item_quantity.setHint("Item quantity (in "+selectedWareHouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+"s)");
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }

}
