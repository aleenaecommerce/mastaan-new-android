package com.mastaan.logistics.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.activities.MeatItemsActivity;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;
import com.mastaan.logistics.printers.LabelPrinter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */
public class LabelPrinterDialog implements View.OnClickListener{

    MastaanToolbarActivity activity;

    View dialogView;
    vTextInputLayout item;
    vTextInputLayout stock;
    vTextInputLayout quantity;
    vTextInputLayout copies;
    View done;
    View cancel;

    WarehouseMeatItemDetails selectedWarehouseMeatItemDetails;
    List<AttributeOptionDetails> selectedItemLevelAttributesOptions;
    StockDetails selectedStockDetails;


    public LabelPrinterDialog(final vToolBarActivity activity) {
        this.activity = (MastaanToolbarActivity) activity;

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_label_printer, null);
        item = dialogView.findViewById(R.id.item);
        item.getEditText().setOnClickListener(this);
        stock = dialogView.findViewById(R.id.stock);
        stock.getEditText().setOnClickListener(this);
        quantity = dialogView.findViewById(R.id.quantity);
        copies = dialogView.findViewById(R.id.copies);
        done = dialogView.findViewById(R.id.done);
        done.setOnClickListener(this);
        cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
    }

    boolean disableItemChange;
    public LabelPrinterDialog disableItemChange() {
        this.disableItemChange = true;
        return this;
    }

    @Override
    public void onClick(View view) {
        if ( view == item.getEditText() ){
            if ( disableItemChange == false ) {
                Intent meatItemsAct = new Intent(activity, MeatItemsActivity.class);
                meatItemsAct.putExtra(Constants.SHOW_ITEM_LEVEL_ATTRIBUTES_OPTIONS_SELECTION, true);
                activity.startActivityForResult(meatItemsAct, Constants.MEAT_ITEMS_ACTIVITY_CODE);
            }
        }
        else if ( view == stock.getEditText() ){
            if ( selectedWarehouseMeatItemDetails != null ){
                activity.showLoadingDialog("Loading stocks, wait...");
                activity.getBackendAPIs().getStockAPI().getStocksForWarehouseMeatItem(selectedItemLevelAttributesOptions, selectedWarehouseMeatItemDetails.getID(), new StockAPI.StocksListCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, final List<StockDetails> stocksList) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            if ( stocksList.size() > 0 ) {
                                final List<String> stocksStrings = new ArrayList<>();
                                for (int i = 0; i < stocksList.size(); i++) {
                                    stocksStrings.add(stocksList.get(i).getFormattedName());
                                }
                                activity.showListChooserDialog("Select stock", stocksStrings, new ListChooserCallback() {
                                    @Override
                                    public void onSelect(int position) {
                                        selectedStockDetails = stocksList.get(position);
                                        stock.setText(stocksList.get(position).getName());
                                    }
                                });
                            }else{
                                activity.showToastMessage("No stocks to show");
                            }
                        }
                        else{
                            activity.showToastMessage("Unable to load stocks, try again!");
                        }
                    }
                });
            }else{
                activity.showToastMessage("* Please select meat item first");
            }
        }
        else if ( view == done ){
            item.checkError("* Select meat item");
            stock.checkError("* Select stock");
            String eQuantity = quantity.getText().trim();
            quantity.checkError("* Enter quantity");
            String eCopies = copies.getText();

            if ( selectedWarehouseMeatItemDetails != null && selectedStockDetails != null && eQuantity.length() > 0 ){
                List<String> labelTexts = getLabelTexts(selectedWarehouseMeatItemDetails, selectedItemLevelAttributesOptions, selectedStockDetails, CommonMethods.parseDouble(eQuantity));

                activity.closeCustomDialog();
                activity.showLoadingDialog(true,"Printing labels, wait...");
                new LabelPrinter(activity).printLabel(labelTexts, eCopies.length()>0?CommonMethods.parseInteger(eCopies):1, new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            activity.showToastMessage("Labels printed successfully");
                        }else{
                            activity.showNotificationDialog("Failure", "Failed to print labels.\n\nMsg: "+message);
                        }
                    }
                });
            }
        }
        else if ( view == cancel ){
            activity.closeCustomDialog();
        }

    }

    public LabelPrinterDialog show(){
        activity.showCustomDialog(dialogView);
        return this;
    }

    public LabelPrinterDialog setSelectedItem(WarehouseMeatItemDetails selectedWarehouseMeatItemDetails, List<AttributeOptionDetails> selectedItemLevelAttributesOptions) {
        this.selectedWarehouseMeatItemDetails = selectedWarehouseMeatItemDetails;
        this.selectedItemLevelAttributesOptions = selectedItemLevelAttributesOptions;

        String formattedName = getSelectedItemName(selectedWarehouseMeatItemDetails);
        if ( selectedItemLevelAttributesOptions != null && selectedItemLevelAttributesOptions.size() > 0 ){
            formattedName += " ("+getSelectedItemLevelAttributesOptionsName(selectedItemLevelAttributesOptions)+")";
        }
        item.setText(formattedName);

        return this;
    }

    public LabelPrinterDialog setStock(StockDetails stockDetails){
        if ( stockDetails != null ){
            this.selectedStockDetails = stockDetails;
            stock.setText(stockDetails.getID());
        }
        return this;
    }

    //----------

    public List<String> getLabelTexts(WarehouseMeatItemDetails warehouseMeatItemDetails, List<AttributeOptionDetails> itemLevelAttributesOptions, StockDetails stockDetails, double quantity){
        List<String> labelsTexts = new ArrayList<>();
        labelsTexts.add("<b>"+getSelectedItemName(warehouseMeatItemDetails).toUpperCase()+"</b>");
        if ( itemLevelAttributesOptions != null && itemLevelAttributesOptions.size() > 0 ) {
            labelsTexts.add("<b>"+getSelectedItemLevelAttributesOptionsName(itemLevelAttributesOptions)+"</b>");
        }
        labelsTexts.add("Weight: "+CommonMethods.getInDecimalFormat(quantity)+" "+warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+(quantity==1?"":"s"));

        Calendar calendar = Calendar.getInstance();
        labelsTexts.add(stockDetails.getID() + "-" + DateMethods.getDateFromCalendar(calendar, DateConstants.HH_MM).replaceAll(":", "") + "-" + new String[]{"", "05", "06", "07", "01", "02", "03", "04"}[calendar.get(Calendar.DAY_OF_WEEK)]);
        labelsTexts.add("");//---------------------------------------");
        labelsTexts.add("A   P R O D U C T   O F   M A S T A A N");

        return labelsTexts;
    }

    public String getSelectedItemName(WarehouseMeatItemDetails warehouseMeatItemDetails){
        if ( warehouseMeatItemDetails != null ){
            return warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize();
        }
        return "";
    }
    public String getSelectedItemLevelAttributesOptionsName(List<AttributeOptionDetails> itemLevelAttributesOptions){
        if ( itemLevelAttributesOptions != null && itemLevelAttributesOptions.size() > 0 ){
            String optionsNames = "";
            for (int i=0;i<itemLevelAttributesOptions.size();i++){
                if ( itemLevelAttributesOptions.get(i)!= null){
                    optionsNames += (optionsNames.length()>0?", ":"") + itemLevelAttributesOptions.get(i).getName();
                }
            }
            return optionsNames;
        }
        return "";
    }


    //===========

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if ( requestCode == Constants.MEAT_ITEMS_ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {
                WarehouseMeatItemDetails warehouseMeatItemDetails = new Gson().fromJson(data.getStringExtra(Constants.WAREHOUSE_MEAT_ITEM_DETAILS), WarehouseMeatItemDetails.class);
                List<AttributeOptionDetails> itemLevelAttributesOptions = null;
                if ( data.getStringExtra(Constants.ATTRIBUTES_OPTIONS) != null ){
                    itemLevelAttributesOptions = new Gson().fromJson(data.getStringExtra(Constants.ATTRIBUTES_OPTIONS), new TypeToken<ArrayList<AttributeOptionDetails>>(){}.getType());
                }
                setSelectedItem(warehouseMeatItemDetails, itemLevelAttributesOptions);
            }
        }catch (Exception e){e.printStackTrace();}
    }

}