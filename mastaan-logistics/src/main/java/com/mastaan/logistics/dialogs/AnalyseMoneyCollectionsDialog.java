package com.mastaan.logistics.dialogs;

import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vSpinner;
import com.google.android.material.tabs.TabLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.methods.AnalyseMoneyCollectionsMethods;
import com.mastaan.logistics.models.MoneyCollectionDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 2/11/16.
 */
public class AnalyseMoneyCollectionsDialog {

    vToolBarActivity activity;

    String forCollectionDate;
    String forConsolidationDate;
    String forDeliveryBoy;

    List<MoneyCollectionDetails> itemsList;

    TextView dialog_title;
    View totalStatisticsDialogView;
    View dateSelectionView;
    vSpinner dateSpinner;
    TabLayout sectionsTabLayout;
    ScrollView scrollView;
    TextView total_details;

    List<String> sectionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.SUMMARY, Constants.DELIVERY_BOY, Constants.CONSOLIDATOR});
    List<String> datesList = new ArrayList<String>();

    String summaryInfoDetails;
    String deliveryBoyInfoDetails;
    String consolidatorInfoDetails;

    public AnalyseMoneyCollectionsDialog(final vToolBarActivity activity, String dialogTitle,  String []sections_List, List<MoneyCollectionDetails> itemsList) {
        this.activity = activity;
        this.itemsList = itemsList;
        if ( sections_List != null && sections_List.length > 0 ){
            this.sectionsList = CommonMethods.getStringListFromStringArray(sections_List);
        }

        totalStatisticsDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_analyse_money_collections, null);
        dialog_title = totalStatisticsDialogView.findViewById(R.id.dialog_title);
        sectionsTabLayout = totalStatisticsDialogView.findViewById(R.id.sectionsLayout);
        sectionsTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        dateSelectionView = totalStatisticsDialogView.findViewById(R.id.dateSelectionView);
        dateSpinner = totalStatisticsDialogView.findViewById(R.id.dateSpinner);
        scrollView = totalStatisticsDialogView.findViewById(R.id.scrollView);
        total_details = totalStatisticsDialogView.findViewById(R.id.total_details);

        //--------

        dialog_title.setText(dialogTitle);

        totalStatisticsDialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activity.showToastMessage("Called");
                activity.closeCustomDialog();
            }
        });

        //--------

        sectionsTabLayout.removeAllTabs();
        for (int i = 0; i < sectionsList.size(); i++) {
            TabLayout.Tab tab = sectionsTabLayout.newTab();
            View rowView = LayoutInflater.from(activity).inflate(R.layout.view_tab, null, false);
            TextView tabTitle = rowView.findViewById(R.id.tabTitle);
            tabTitle.setText(sectionsList.get(i));
            tab.setCustomView(rowView);
            sectionsTabLayout.addTab(tab);
        }
//        if ( sectionsList.size() == 1 ){
//            sectionsTabLayout.setVisibility(View.GONE);
//        }

        sectionsTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                showSelectedTabDetails();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });
    }

    public AnalyseMoneyCollectionsDialog setForCollectionDate(String forCollectionDate) {
        this.forCollectionDate = forCollectionDate;
        return this;
    }

    public AnalyseMoneyCollectionsDialog setForConsolidationDate(String forConsolidationDate) {
        this.forConsolidationDate = forConsolidationDate;
        return this;
    }

    public AnalyseMoneyCollectionsDialog setForDeliveryBoy(String forDeliveryBoy) {
        this.forDeliveryBoy = forDeliveryBoy;
        return this;
    }

    public void show(){
        activity.showLoadingDialog(true, "Analysing, wait...");
        new AsyncTask<Void, Void, Void>() {
            List<String> datesList = new ArrayList<String>();
            @Override
            protected Void doInBackground(Void... voids) {
                datesList = getDatesList();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( datesList.size() > 1 ){
                    datesList.add(0, "All dates");
                    dateSelectionView.setVisibility(View.VISIBLE);
                    setupDateSpinner(datesList);
                }else{
                    dateSelectionView.setVisibility(View.GONE);
                    showStatsForDate(null, false);
                }
            }
        }.execute();
    }

    private void setupDateSpinner(List<String> dates){
        this.datesList = dates;

        ArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_date_spinner_item, datesList);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setDropDownViewResource(R.layout.view_date_spinner_item);
        dateSpinner.setAdapter(adapter);
        dateSpinner.setSelection(0);
        showStatsForDate(datesList.get(0), false);

        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previousPosition = -1;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( previousPosition != -1 ) {
                    showStatsForDate(datesList.get(position), true);
                }
                previousPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void showStatsForDate(final String date, boolean showLoadingDialog){
        if ( showLoadingDialog ) {
            activity.showLoadingDialog(true, "Analysing, wait...");
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                AnalyseMoneyCollectionsMethods analyseMoneyCollectionsMethods = null;
                if ( date == null || date.toLowerCase().contains("all") ){
                    analyseMoneyCollectionsMethods = new AnalyseMoneyCollectionsMethods(itemsList).setForCollectionDate(forCollectionDate).setForConsolidationDate(forConsolidationDate).setForDeliveryBoy(forDeliveryBoy);
                }else{
                    analyseMoneyCollectionsMethods = new AnalyseMoneyCollectionsMethods(getItemsListForDate(date)).setForCollectionDate(forCollectionDate).setForConsolidationDate(forConsolidationDate).setForDeliveryBoy(forDeliveryBoy);
                }
                analyseMoneyCollectionsMethods.analyse();

                summaryInfoDetails = analyseMoneyCollectionsMethods.getFullSummaryInfo();
                deliveryBoyInfoDetails = analyseMoneyCollectionsMethods.getDeliveryBoyInfo();
                consolidatorInfoDetails = analyseMoneyCollectionsMethods.getConsolidatorsInfo();

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                activity.closeLoadingDialog();
                showDialog();
            }
        }.execute();
    }

    private void showDialog(){
        showSelectedTabDetails();
        activity.showCustomDialog(totalStatisticsDialogView);
    }

    private void showSelectedTabDetails(){
        if (sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.SUMMARY)) {
            total_details.setText(Html.fromHtml(summaryInfoDetails));
        } else if (sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.DELIVERY_BOY)) {
            total_details.setText(Html.fromHtml(deliveryBoyInfoDetails));
        }else if (sectionsList.get(sectionsTabLayout.getSelectedTabPosition()).equalsIgnoreCase(Constants.CONSOLIDATOR)) {
            total_details.setText(Html.fromHtml(consolidatorInfoDetails));
        }
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    //------------

    private List<String> getDatesList(){
        List<String> datesList = new ArrayList<>();
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i) != null && itemsList.get(i).getOrderDetails() != null ){
                String date = DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(itemsList.get(i).getOrderDetails().getDeliveryDate()), DateConstants.MMM_DD_YYYY);
                if ( datesList.contains(date) == false ){
                    datesList.add(date);
                }
            }
        }
        Collections.sort(datesList, new Comparator<String>() {
            public int compare(String item1, String item2) {
                return DateMethods.compareDates(item1, item2);//item1.compareToIgnoreCase(item2);
            }
        });
        return datesList;
    }
    private List<MoneyCollectionDetails> getItemsListForDate(String date){
        List<MoneyCollectionDetails> itemsListForDate = new ArrayList<>();
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i) != null && itemsList.get(i).getOrderDetails() != null ){
                if (DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(itemsList.get(i).getOrderDetails().getDeliveryDate())), date) == 0 ){
                    itemsListForDate.add(itemsList.get(i));
                }
            }
        }
        return itemsListForDate;
    }
}
