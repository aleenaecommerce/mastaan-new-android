package com.mastaan.logistics.activities;

import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.View;

import com.mastaan.logistics.R;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

public class RejectedItemsActivity extends BaseOrderItemsActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();

        //--------

        getRejectedItems();
    }


    @Override
    public void onReloadPage() {
        super.onReloadPage();
        getRejectedItems();
    }


    private void getRejectedItems() {

        showLoadingIndicator("Loading rejected items, wait...");
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        getBackendAPIs().getOrdersAPI().getRejectedOrderItems(new OrderItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                if (status) {
                    if ( orderItemsList.size() > 0 ){
                        List<GroupedOrdersItemsList> groupedList = new ArrayList<GroupedOrdersItemsList>();
                        groupedList.add(new GroupedOrdersItemsList("ALL", orderItemsList));
                        displayItems(true, groupedList, new ArrayList<OrderDetails>());
                    }else{
                        showNoDataIndicator("No rejected order items to show");
                    }
                } else {
                    showReloadIndicator("Unable to load rejected order items, try again!");
                }
            }
        });
    }

}