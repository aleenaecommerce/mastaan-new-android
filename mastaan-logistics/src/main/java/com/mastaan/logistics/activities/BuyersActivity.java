package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.BuyersAdapter;
import com.mastaan.logistics.backend.BuyersAPI;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.backend.models.RequestUpdateBuyerDetails;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.ECODDialog;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.BuyerSearchDetails;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.UserDetailsMini;

import java.util.ArrayList;
import java.util.List;

public class BuyersActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String actionType;
    long pageNumber = 1;
    boolean isLoading;

    BuyerSearchDetails baseSearchDetails = new BuyerSearchDetails();
    BuyerSearchDetails searchDetails = new BuyerSearchDetails();

    MenuItem searchViewMenuITem;
    SearchView searchView;

    RadioGroup buyerMembershipType;
    RadioButton allBuyers, bronzeBuyers, silverBuyers, goldBuyers, diamondBuyers, platinumBuyers;

    //FrameLayout searchResultsCountIndicator;
    //TextView results_count;
    //Button clearFilter;

    CheckBox showClaimed;

    vRecyclerView buyersHolder;
    BuyersAdapter buyersAdapter;
    Button loadMore;

    List<BuyerDetails> currentBuyersList = new ArrayList<>();

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyers);
        hasLoadingView();
        hasSwipeRefresh();

        actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        if ( actionType == null || actionType.length() == 0 ){
            actionType = Constants.BUYERS;
        }
        setToolbarTitle(actionType);

        userDetails = localStorageData.getUserDetails();

        if ( getIntent().getStringExtra(Constants.TITLE) != null ){
            setToolbarTitle(getIntent().getStringExtra(Constants.TITLE));
        }

        if ( getIntent().getStringExtra(Constants.SEARCH_DETAILS) != null ){
            try{
                baseSearchDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.SEARCH_DETAILS), BuyerSearchDetails.class);
                searchDetails = baseSearchDetails;
            }catch (Exception e){}
        }

        //-----------

        buyerMembershipType = findViewById(R.id.buyerMembershipType);
        allBuyers = findViewById(R.id.allBuyers);
        bronzeBuyers = findViewById(R.id.bronzeBuyers);
        silverBuyers = findViewById(R.id.silverBuyers);
        goldBuyers = findViewById(R.id.goldBuyers);
        diamondBuyers = findViewById(R.id.diamondBuyers);
        platinumBuyers = findViewById(R.id.platinumBuyers);

        buyerMembershipType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                pageNumber = 1;
                getBuyers();
            }
        });

        //searchResultsCountIndicator = findViewById(R.id.searchResultsCountIndicator);
        //results_count = findViewById(R.id.results_count);
        //clearFilter = findViewById(R.id.clearFilter);
        //clearFilter.setOnClickListener(this);

        showClaimed = findViewById(R.id.showClaimed);
        showClaimed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                pageNumber = 1;
                getBuyers();
            }
        });

        buyersHolder = findViewById(R.id.buyersHolder);
        setupHolder();

        loadMore = findViewById(R.id.loadMore);
        loadMore.setOnClickListener(this);

        //-------------

        showClaimed.setVisibility(getIntent().getBooleanExtra(Constants.FOR_PENDING_FOLLOWUPS, false)?View.VISIBLE:View.GONE);

        // LOADING DATA AFTER MENU CREATED

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        if ( getIntent().getStringExtra(Constants.QUERY) != null ){
            searchViewMenuITem.expandActionView();
            searchView.setQuery(getIntent().getStringExtra(Constants.QUERY), true);
            searchView.clearFocus();
            //onPerformSearch(getIntent().getStringExtra(Constants.QUERY));
        }else {
            getBuyers();
        }
    }

    @Override
    public void onClick(View view) {
        if ( view == loadMore ){
            if ( isLoading == false ) {
                loadMore.setVisibility(View.GONE);
                getBuyers();
            }
        }
        //else if ( view == clearFilter ){
        //    try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
        //    onClearSearch();
        //}
    }

    public void setupHolder(){

        buyersHolder.setupVerticalOrientation();

        buyersHolder.setScrollListener(new vRecyclerView.ScrollListener() {
            @Override
            public void onScrolled(boolean isScrollEnd) {
                if ( isScrollEnd
                        && actionType.equalsIgnoreCase(Constants.MEATITEM_AVAILABILITY_REQUESTED_BUYERS) == false
                        && actionType.equalsIgnoreCase(Constants.COUPON_BUYERS) == false
                        && actionType.equalsIgnoreCase(Constants.REFERRAL_BUYERS) == false ){
                    //Log.d(Constants.LOG_TAG, "======> isLoading: " + isLoading+", cPN: "+pageNumber);
                    if (isLoading == false && pageNumber > 0
                            /*&& searchResultsCountIndicator.getVisibility() != View.VISIBLE*/ ) {
                        getBuyers();
                    }
                }
            }
        });

        buyersAdapter = new BuyersAdapter(activity, new ArrayList<BuyerDetails>(), new BuyersAdapter.CallBack() {
            @Override
            public void onItemClick(final int position) {
                if ( getCallingActivity() != null ){
                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.BUYER_DETAILS, new Gson().toJson(buyersAdapter.getItem(position)));
                    setResult(RESULT_OK, resultData);
                    finish();
                }
                else {
                    if ( getIntent().getBooleanExtra(Constants.FOR_PENDING_FOLLOWUPS, false) ){
                        showLoadingDialog("Loading buyer details, wait...");
                        getBackendAPIs().getBuyersAPI().getBuyers(new BuyerSearchDetails(baseSearchDetails).setID(buyersAdapter.getItem(position).getID()), 1, new BuyersAPI.BuyersCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, BuyerSearchDetails searchDetails, long pageNumber, List<BuyerDetails> buyersList) {
                                if ( status ){
                                    if( buyersList.size() == 0 ){
                                        closeLoadingDialog();
                                        showToastMessage("Buyer followup already completed / not required");
                                        buyersAdapter.deleteItem(position);
                                        return;
                                    }

                                    getBackendAPIs().getBuyersAPI().getBuyerDetails(buyersAdapter.getItem(position).getID(), new BuyersAPI.BuyerDetailsCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message, BuyerDetails buyerDetails) {
                                            closeLoadingDialog();
                                            if ( status ){
                                                buyersAdapter.setItem(position, buyerDetails);
                                                if ( userDetails.isSuperUser() || buyerDetails.getFollowupEmployee() == null || buyerDetails.getFollowupEmployee().getID().equals(userDetails.getID()) ) {
                                                    showBuyerOptionsDialog(position);
                                                }else{
                                                    showToastMessage("Followup is already claimed by "+buyerDetails.getFollowupEmployee().getAvailableName());
                                                }
                                            }else{
                                                showToastMessage("Something went wrong, try again!");
                                            }
                                        }
                                    });
                                }else{
                                    closeLoadingDialog();
                                    showToastMessage("Something went wrong, try again!");
                                }
                            }
                        });
                    }
                    else{
                        showBuyerOptionsDialog(position);
                    }
                }
            }
            private void showBuyerOptionsDialog(final int position){
                final BuyerOptionsDialog buyerOptionsDialog = new BuyerOptionsDialog(activity)
                        .showCallEmailOptions()
                        .showPromotionalAlertsPreference();
                if ( getIntent().getBooleanExtra(Constants.FOR_PENDING_FOLLOWUPS, false) ){
                    buyerOptionsDialog.forPendingFollowup();
                }
                buyerOptionsDialog.show(buyersAdapter.getItem(position)).setCallback(new BuyerOptionsDialog.Callback() {
                    @Override
                    public void onDetailsUpdated(BuyerDetails buyerDetails) {
                        buyersAdapter.setItem(position, buyerDetails);
                        updateInCurrentBuyersList(buyerDetails);
                    }

                    @Override
                    public void onEnabledBuyer() {
                        buyersAdapter.enableBuyer(position);
                        updateInCurrentBuyersList(buyersAdapter.getItem(position));
                    }

                    @Override
                    public void onDisabledBuyer(String disabledReason) {
                        buyersAdapter.disableBuyer(position, disabledReason);
                        updateInCurrentBuyersList(buyersAdapter.getItem(position));
                    }

                    @Override
                    public void onFollowupClaimed(UserDetails userDetails) {
                        BuyerDetails buyerDetails = buyersAdapter.getItem(position);
                        buyerDetails.setFollowupEmployee(new UserDetailsMini(userDetails));
                        buyersAdapter.setItem(position, buyerDetails);
                        updateInCurrentBuyersList(buyerDetails);
                    }

                    @Override
                    public void onFollowupReleased() {
                        BuyerDetails buyerDetails = buyersAdapter.getItem(position);
                        buyerDetails.setFollowupEmployee(null);
                        buyersAdapter.setItem(position, buyerDetails);
                        updateInCurrentBuyersList(buyerDetails);
                    }

                    @Override
                    public void onFollowupAdded(FollowupDetails followupDetails) {
                        if ( getIntent().getBooleanExtra(Constants.FOR_PENDING_FOLLOWUPS, false) ){
                            deleteInCurrentBuyersList(buyersAdapter.getItem(position).getID());
                            buyersAdapter.deleteItem(position);
                            if ( buyersAdapter.getItemCount() == 0 ){
                                onReloadPressed();
                            }
                        }
                        else{
                            BuyerDetails buyerDetails = buyersAdapter.getItem(position);
                            if ( followupDetails.getComments().toLowerCase().contains("not reachable") || followupDetails.getComments().toLowerCase().contains("call back") ){
                                buyerDetails.setComments(followupDetails.getComments());
                            }else {
                                buyerDetails.setLastFollowupDate(followupDetails.getCreatedDate());
                                buyerDetails.setLastFollowupComments(followupDetails.getComments());
                                buyerDetails.setComments(null);
                            }
                            buyerDetails.setFollowupEmployee(null);
                            buyersAdapter.setItem(position, buyerDetails);
                            updateInCurrentBuyersList(buyerDetails);
                        }
                    }

                    @Override
                    public void onInstallSourceChange(String installSource, String installSourceDetails) {
                        BuyerDetails buyerDetails = buyersAdapter.getItem(position);
                        buyerDetails.setInstallSource(installSource).setInstallSourceDetails(installSourceDetails);
                        buyersAdapter.setItem(position, buyerDetails);
                        updateInCurrentBuyersList(buyerDetails);
                    }

                    @Override
                    public void onUpdatePromotionalAlertsPreference(boolean preference) {
                        BuyerDetails buyerDetails = buyersAdapter.getItem(position);
                        buyerDetails.setPromotionalAlertsPreference(preference);
                        buyersAdapter.setItem(position, buyerDetails);
                        updateInCurrentBuyersList(buyerDetails);
                    }
                });
            }

            @Override
            public void onReloadClick(int position) {
                checkBuyerPaymentStatus(position);
            }

            @Override
            public void onRequestOnlinePayment(final int position) {
                if ( buyersAdapter.getItem(position).getName() == null || buyersAdapter.getItem(position).getName().trim().length() == 0 ){
                    showInputTextDialog("Buyer name", "", new TextInputCallback() {
                        @Override
                        public void onComplete(final String inputText) {
                            RequestUpdateBuyerDetails requestUpdateBuyerDetails = new RequestUpdateBuyerDetails(inputText, null);
                            showLoadingDialog("Updating buyer details, wait...");
                            getBackendAPIs().getBuyersAPI().updateBuyerDetails(buyersAdapter.getItem(position).getID(), requestUpdateBuyerDetails, new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCosde, String message) {
                                    closeLoadingDialog();
                                    if ( status ) {
                                        buyersAdapter.setItem(position, buyersAdapter.getItem(position).setName(inputText));
                                        proceedToECOD(position);
                                    }else{
                                        showToastMessage("Something went wrong while updating buyer details, try again!");
                                    }
                                }
                            });
                        }
                    });
                }else{
                    proceedToECOD(position);
                }
            }
            public void proceedToECOD(final int position){
                new ECODDialog(activity).showForBuyer(buyersAdapter.getItem(position), buyersAdapter.getItem(position).getOutstandingAmount(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if ( status ){
                            activity.showToastMessage("Payment success");
                            checkBuyerPaymentStatus(position);
                        }else{
                            activity.showToastMessage("Payment failed");
                        }
                    }
                });
            }
        });
        if ( userDetails.isSuperUser() || userDetails.isAccountsApprover() || userDetails.isCustomerRelationshipManager() || userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() ){
            buyersAdapter.showRequestOnlinePayment();
        }
        buyersHolder.setAdapter(buyersAdapter);
    }



    public void displayList(List<BuyerDetails> buyersList){

        if ( buyersList.size() > 0 ){
            if (pageNumber == 1) {
                showMenuItems();
                currentBuyersList = new ArrayList<>();
                buyersAdapter.setItems(buyersList);
                try{    buyersHolder.scrollToPosition(0);   }catch (Exception e){}
                switchToContentPage();
            } else {
                buyersAdapter.addItems(buyersList);
                loadMore.setVisibility(View.GONE);
            }
            pageNumber += 1;

            for (int i=0;i<buyersList.size();i++){
                currentBuyersList.add(buyersList.get(i));
            }
        }
        else{
            if (pageNumber == 1) {
                currentBuyersList = new ArrayList<>();
                showNoDataIndicator("No buyers");
            }
            else {
                showToastMessage("No more to load");
                pageNumber = -1;
                loadMore.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        /*if ( searchResultsCountIndicator.getVisibility() == View.VISIBLE ){
            onPerformSearch(searchQuery);
        }else {
            pageNumber = 1;
            getBuyers();
        }*/
        pageNumber = 1;
        searchDetails = new BuyerSearchDetails(baseSearchDetails);
        getBuyers();
    }


    public void getBuyers(){

        if ( pageNumber >= 1 ){
            if ( pageNumber == 1 ) {
                loadMore.setVisibility(View.GONE);
                //searchResultsCountIndicator.setVisibility(View.GONE);
                hideMenuItems();
                showLoadingIndicator("Loading, wait...");
            }else{
                loadMore.setVisibility(View.VISIBLE);
                loadMore.setText("Loading...");
            }
            isLoading = true;

            if ( searchDetails != null ){
                if ( bronzeBuyers.isChecked() ){   searchDetails.setMembership("bm");  }
                else if ( silverBuyers.isChecked() ){   searchDetails.setMembership("sm");  }
                else if ( goldBuyers.isChecked() ){   searchDetails.setMembership("gm");  }
                else if ( diamondBuyers.isChecked() ){   searchDetails.setMembership("dm");  }
                else if ( platinumBuyers.isChecked() ){   searchDetails.setMembership("pm");  }
                else {   searchDetails.setMembership(null);  }

                if ( getIntent().getBooleanExtra(Constants.FOR_PENDING_FOLLOWUPS, false) ) {
                    searchDetails.setFollowupEmployee(showClaimed.isChecked()? localStorageData.getUserID() : null);
                }
            }

            if ( actionType.equalsIgnoreCase(Constants.BUYERS) ){
                getBackendAPIs().getBuyersAPI().getBuyers(searchDetails, pageNumber, new BuyersAPI.BuyersCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, BuyerSearchDetails searchDetails, long loadedPageNumber, List<BuyerDetails> buyersList) {
                        if ( pageNumber == loadedPageNumber ){
                            isLoading = false;
                            if ( status ){
                                displayList(buyersList);
                            }else{
                                if (pageNumber == 1) {
                                    showReloadIndicator(statusCode, "Unable to load "+actionType.toLowerCase()+", try again!");
                                } else {
                                    loadMore.setText("Load more");
                                    loadMore.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }
                });
            }
            else if ( actionType.equalsIgnoreCase(Constants.COUPON_BUYERS) ){
                getBackendAPIs().getBuyersAPI().getCouponBuyers(searchDetails.getCouponCode(), searchDetails, new BuyersAPI.BuyersCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, BuyerSearchDetails searchDetails, long loadedPageNumber, List<BuyerDetails> buyersList) {
                        isLoading = false;
                        if ( status ){
                            displayList(buyersList);
                        }else{
                            showReloadIndicator(statusCode, "Unable to load "+actionType.toLowerCase()+", try again!");
                        }
                    }
                });
            }
            else if ( actionType.equalsIgnoreCase(Constants.REFERRAL_BUYERS) ){
                getBackendAPIs().getBuyersAPI().getReferralBuyers(searchDetails.getReferralCode(), searchDetails, new BuyersAPI.BuyersCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, BuyerSearchDetails searchDetails, long loadedPageNumber, List<BuyerDetails> buyersList) {
                        isLoading = false;
                        if ( status ){
                            displayList(buyersList);
                        }else{
                            showReloadIndicator(statusCode, "Unable to load "+actionType.toLowerCase()+", try again!");
                        }
                    }
                });
            }
            else if ( actionType.equalsIgnoreCase(Constants.MEATITEM_AVAILABILITY_REQUESTED_BUYERS) ){
                getBackendAPIs().getBuyersAPI().getMeatItemAvailabilityRequestedBuyers(searchDetails.getMeatItem(), searchDetails, new BuyersAPI.BuyersCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, BuyerSearchDetails searchDetails, long loadedPageNumber, List<BuyerDetails> buyersList) {
                        isLoading = false;
                        if ( status ){
                            displayList(buyersList);
                        }else{
                            showReloadIndicator(statusCode, "Unable to load "+actionType.toLowerCase()+", try again!");
                        }
                    }
                });
            }
        }
    }

    public void checkBuyerPaymentStatus(final int position){

        showLoadingDialog("Checking buyer payment status, wait...");
        getBackendAPIs().getPaymentAPI().checkBuyerPaymentStatus(buyersAdapter.getItem(position).getID(), new PaymentAPI.CheckBuyerPaymentStatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, BuyerDetails buyerDetails) {
                closeLoadingDialog();
                if (status){
                    buyersAdapter.setItem(position, buyersAdapter.getItem(position).setOutstandingAmount(buyerDetails.getOutstandingAmount()));
                    updateInCurrentBuyersList(buyersAdapter.getItem(position));
                }else{
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while checking buyer payment status.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                checkBuyerPaymentStatus(position);
                            }
                        }
                    });
                }
            }
        });
    }

    //--------------

    private void updateInCurrentBuyersList(BuyerDetails buyerDetails){
        try{
            for (int i = 0; i< currentBuyersList.size(); i++){
                if ( currentBuyersList.get(i) != null && currentBuyersList.get(i).getID().equals(buyerDetails.getID()) ){
                    currentBuyersList.set(i, buyerDetails);
                    break;
                }
            }
        }catch (Exception e){}
    }
    private void deleteInCurrentBuyersList(String buyerID){
        try{
            for (int i = 0; i< currentBuyersList.size(); i++) {
                if (currentBuyersList.get(i) != null && currentBuyersList.get(i).getID().equals(buyerID)) {
                    currentBuyersList.remove(i);
                    break;
                }
            }
        }catch (Exception e){}
    }

    public void onPerformSearch(final String search_query){
        if ( search_query != null && search_query.trim().length() > 0 ){
            searchDetails = new BuyerSearchDetails(baseSearchDetails).setQuery(search_query);

            if ( actionType.equalsIgnoreCase(Constants.BUYERS) ){
                pageNumber = 1;
                getBuyers();
            }
            else{
                loadMore.setVisibility(View.GONE);
                hideMenuItems();
                showLoadingIndicator("Loading, wait...");
                new AsyncTask<Void, Void, Void>() {
                    List<BuyerDetails> filteredList = new ArrayList<>();
                    @Override
                    protected Void doInBackground(Void... voids) {
                        for (int i = 0; i< currentBuyersList.size(); i++){
                            BuyerDetails buyerDetails = currentBuyersList.get(i);
                            if ( (buyerDetails.getAvailableName() != null && buyerDetails.getAvailableName().toLowerCase().contains(search_query.toLowerCase()))
                                    || (buyerDetails.getMobileNumber() != null && buyerDetails.getMobileNumber().toLowerCase().contains(search_query.toLowerCase()))
                                    || (buyerDetails.getEmail() != null && buyerDetails.getEmail().toLowerCase().contains(search_query.toLowerCase())) ){
                                filteredList.add(buyerDetails);
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        if ( filteredList.size() > 0 ){
                            showMenuItems();
                            buyersAdapter.setItems(filteredList);
                            switchToContentPage();
                        }else{
                            showNoDataIndicator("Nothing to show");
                        }
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
            /*this.searchQuery = search_query.trim();

            loadMore.setVisibility(View.GONE);
            searchResultsCountIndicator.setVisibility(View.GONE);
            hideMenuItems();
            showLoadingIndicator("Searching for '"+searchQuery.toLowerCase()+"'");
            getBackendAPIs().getBuyersAPI().searchBuyers(searchQuery, new BuyersAPI.SearchBuyersCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String query, List<BuyerDetails> buyersList) {
                    if ( searchQuery.equalsIgnoreCase(query) ){
                        searchResultsCountIndicator.setVisibility(View.VISIBLE);
                        showMenuItems();
                        if ( status ){
                            if ( buyersList.size() > 0 ){
                                results_count.setText(buyersList.size() + " result(s) found.\n(" + searchQuery.toLowerCase() + ")");
                                buyersAdapter.setItems(buyersList);
                                switchToContentPage();
                            }else{
                                results_count.setText("No Results");
                                showNoDataIndicator("Nothing found for '"+searchQuery.toLowerCase()+"'");
                            }
                        }else{
                            results_count.setText(searchQuery.toLowerCase());
                            showReloadIndicator("Something went wrong while searching for '"+searchQuery.toLowerCase()+"', try again!");
                        }
                    }
                }
            });*/
        }
        else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        currentBuyersList = new ArrayList<>();
        pageNumber = 1;
        searchDetails = new BuyerSearchDetails(baseSearchDetails);
        getBuyers();

        /*this.searchQuery = null;
        searchResultsCountIndicator.setVisibility(View.GONE);
        buyersAdapter.setItems(currentBuyersList);
        switchToContentPage();*/
    }



    //===============

    public void hideMenuItems(){
        try {
            searchViewMenuITem.setVisible(false);//searchViewMenuItem.collapseActionView();
        }catch (Exception e){}
    }

    public void showMenuItems(){
        try {
            searchViewMenuITem.setVisible(true);
        }catch (Exception e){}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query.trim());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){

        }else {
            onBackPressed();
        }
        return true;
    }

}
