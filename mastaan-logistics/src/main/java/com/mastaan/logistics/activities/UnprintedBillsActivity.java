package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.OrdersAdapter;
import com.mastaan.logistics.dialogs.FollowupDetailsDialog;
import com.mastaan.logistics.dialogs.MoneyCollectionDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.printers.BillPrinter;
import com.mastaan.logistics.interfaces.OrdersListCallback;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.SearchDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 08/07/17.
 */

public class UnprintedBillsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    MenuItem searchViewMenuItem;
    SearchView searchView;

    vRecyclerView ordersHolder;
    OrdersAdapter ordersAdapter;

    View printAllBills;

    List<OrderDetails> originalItemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unprinted_bills);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        printAllBills = findViewById(R.id.printAllBills);
        printAllBills.setOnClickListener(this);

        ordersHolder = findViewById(R.id.ordersHolder);
        ordersHolder.setupVerticalOrientation();
        ordersAdapter = new OrdersAdapter(activity, new ArrayList<OrderDetails>(), new OrdersAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onShowDirections(int position) {

            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowBuyerHistory(int position) {
            }

            @Override
            public void onShowReferralBuyerDetails(int position) {

            }

            @Override
            public void onShowMoneyCollectionDetails(int position) {
                new MoneyCollectionDetailsDialog(activity).showForOrder(ordersAdapter.getItem(position).getID());
            }

            @Override
            public void onCheckPaymentStatus(int position) {

            }

            @Override
            public void onRequestOnlinePayment(int position) {

            }

            @Override
            public void onShowFollowupResultedInOrder(int position) {
                new FollowupDetailsDialog(activity).show(ordersAdapter.getItem(position).getFollowupResultedInOrder());
            }

            @Override
            public void onPrintBill(final int position) {
                new BillPrinter(activity).start(ordersAdapter.getItem(position), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if ( status ) {
                            ordersAdapter.deleteItem(position);
                        }
                    }
                });
            }

            @Override
            public void onOrderItemClick(int position, int item_position) {

            }

            @Override
            public void onShowOrderItemStockDetails(int position, int item_position) {
                new StockDetailsDialog(activity).show(ordersAdapter.getItem(position).getOrderedItems().get(item_position));
            }
        });
        ordersHolder.setAdapter(ordersAdapter);

        //---------

        //LOADING AFTER MENU CREATED
    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        getUnprintedBills();
    }

    @Override
    public void onClick(View view) {
        if ( view == printAllBills ){
            new BillPrinter(activity).start(ordersAdapter.getAllItems(), new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message) {
                    if ( status ){
                        try{
                            searchViewMenuItem.collapseActionView();
                        }catch (Exception e){}
                        onReloadPressed();
                    }
                }
            });
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getUnprintedBills();
    }

    public void getUnprintedBills(){
        printAllBills.setVisibility(View.GONE);
        searchViewMenuItem.setVisible(false);
        try{
            searchViewMenuItem.collapseActionView();
        }catch (Exception e){}
        showLoadingIndicator("Loading unprinted bills, wait...");
        getBackendAPIs().getOrdersAPI().getUnprintedOrders(new OrdersListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList) {
                if ( status ){
                    originalItemsList = ordersList;
                    if ( ordersList.size() > 0 ) {
                        searchViewMenuItem.setVisible(true);
                        ordersAdapter.setItems(ordersList);
                        ordersHolder.scrollToPosition(0);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("No unprinted bills");
                    }
                }
                else{
                    showReloadIndicator("Unable to load unprinted bills, try again!");
                }
            }
        });
    }


    SearchDetails searchDetails;
    public void onPerformSearch(final SearchDetails search_details){
        if ( search_details != null && search_details.isSearchable() ){
            this.searchDetails = search_details;
            printAllBills.setVisibility(View.GONE);
            showLoadingIndicator("Searching for '"+search_details.getSearchString()+"', wait...");
            SearchMethods.searchOrders(originalItemsList, searchDetails, localStorageData.getServerTime(), new SearchMethods.SearchOrdersCallback() {
                @Override
                public void onComplete(boolean status, SearchDetails searchDetails, List<OrderDetails> filteredList) {
                    if ( filteredList.size() > 0 ){
                        ordersAdapter.setItems(filteredList);
                        ordersHolder.scrollToPosition(0);
                        switchToContentPage();
                        printAllBills.setVisibility(View.VISIBLE);
                    }else{
                        showNoDataIndicator("No results for '"+search_details.getSearchString()+"'");
                    }
                }
            });
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        ordersAdapter.setItems(originalItemsList);
        switchToContentPage();
        ordersHolder.scrollToPosition(0);
        printAllBills.setVisibility(View.GONE);
    }

    //===========

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_assign_deliveries, menu);

        searchViewMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchViewMenuItem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(new SearchDetails().setDeliveryBoyName(query).setID(query).setConsiderAtleastOneMatch(true));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){

        }else {
            onBackPressed();
        }
        return true;
    }
}