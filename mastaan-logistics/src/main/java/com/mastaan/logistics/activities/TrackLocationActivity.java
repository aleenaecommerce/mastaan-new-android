package com.mastaan.logistics.activities;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.location.model.DirectionsData;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vMapFragment;
import com.aleena.common.widgets.vMapWrapperLayout;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.Job;

import java.io.Serializable;
import java.util.List;


public class TrackLocationActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String userID;

    GoogleMap googleMap;
    Marker mapMarker;
    boolean draggingMap;
    boolean initiallyZoomed;

    View locationDetailsHolder;
    TextView time;
    TextView place_name;
    TextView place_info;
    TextView job_details;

    Firebase firebaseLocationTracking;

    LocationDetails lastPlaceDetailFetchedLocation;
    PlaceDetails lastFetchedPlaceDetails;

    LocationDetails lastKnowLocation;

    Job job;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_location);

        userID = getIntent().getStringExtra(Constants.USER_ID);
        setToolbarTitle(getIntent().getStringExtra(Constants.USER_NAME));
        job = new Gson().fromJson(getIntent().getStringExtra(Constants.JOB_DETAILS), Job.class);

        //------

        locationDetailsHolder = findViewById(R.id.locationDetailsHolder);
        locationDetailsHolder.setOnClickListener(this);
        time = (TextView) findViewById(R.id.time);
        place_name = (TextView) findViewById(R.id.place_name);
        place_info = (TextView) findViewById(R.id.place_info);
        job_details = (TextView) findViewById(R.id.job_details);

        vMapFragment map_fragment = ((vMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment));
        map_fragment.setOnDragListener(new vMapWrapperLayout.OnDragListener() {
            @Override
            public void onDrag(MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    draggingMap = true;
                } else {
                    draggingMap = false;
                }
            }
        });
        map_fragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap google_Map) {
                //googleMap = map_fragment.getMap();
                googleMap = google_Map;

                firebaseLocationTracking = new Firebase(getString(R.string.firebase_location_tracking_url)+"/"+userID);
                getLastKnownLocation();
                if ( job != null ){
                    displayJobRoute(job);
                }
            }
        });

        //----

        if ( job != null ){
            job_details.setVisibility(View.VISIBLE);
            job_details.setText(Html.fromHtml(job.getFormattedJobDetailsString().replaceAll("<br><br>", "<br>")));
        }else{  job_details.setVisibility(View.GONE);   }

    }

    @Override
    public void onClick(View view) {
        if ( view == locationDetailsHolder ){
            displayLocation(lastKnowLocation, true);
        }
    }

    public void getLastKnownLocation(){

        firebaseLocationTracking.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if ( dataSnapshot != null && dataSnapshot.getValue() != null ) {
                    //Log.d(Constants.LOG_TAG, "LKL =======> "+dataSnapshot.getAmount());
                    LocationDetails locationDetails = new LocationDetails(dataSnapshot.child("time").getValue(Long.class), dataSnapshot.child("lat").getValue(Double.class), dataSnapshot.child("lng").getValue(Double.class));
                    displayLocation(locationDetails);
                }else{
                    showToastMessage("No Location information found");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                showToastMessage("Cancelled");
            }
        });
    }

    public void displayLocation(final LocationDetails locationDetails){
        displayLocation(locationDetails, false);
    }
    public void displayLocation(final LocationDetails locationDetails, final boolean zoomToLocation){
        if ( locationDetails != null && locationDetails.lat !=0 && locationDetails.lng !=0 ){
            if ( mapMarker != null ){    mapMarker.remove();  }
            mapMarker = googleMap.addMarker(new MarkerOptions().position(new LatLng(locationDetails.lat, locationDetails.lng)).title("Last Known Location").snippet("(Lat: " + locationDetails.lat + ", Lng: " + locationDetails.lng + ")").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)).draggable(true));
            lastKnowLocation = locationDetails;

            if ( draggingMap == false ) {
                CameraPosition.Builder cameraBuilder = new CameraPosition.Builder()
                        .target(new LatLng(locationDetails.lat, locationDetails.lng))      // Sets the center of the google_map to Mountain View
                        .zoom(googleMap.getCameraPosition().zoom)
                        .bearing(0)                         // Sets the orientation of the camera to east
                        .tilt(0);                            // Sets the tilt of the camera to 30 degrees
                if ( initiallyZoomed == false || zoomToLocation ){
                    cameraBuilder.zoom(17);                 // Sets the zoom
                    initiallyZoomed = true;
                }
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraBuilder.build()));
            }

            place_info.setText("(Lat: "+locationDetails.lat+", Lng: "+locationDetails.lng+")");
            time.setText("@ "+ DateMethods.getDateInFormat(DateMethods.getDateFromTimeStamp(locationDetails.time), DateConstants.MMM_DD_YYYY_HH_MM_A));

            // If Difference between last place details fetched location and current location > 100m then loading place details
            if ( lastPlaceDetailFetchedLocation == null
                    || Math.abs(LatLngMethods.getDistanceBetweenLatLng(new LatLng(lastPlaceDetailFetchedLocation.lat, lastPlaceDetailFetchedLocation.lng), new LatLng(locationDetails.lat, locationDetails.lng))) > 100 ){
                // To Prevent Loading while request is going on
                if ( place_name.getText().toString().toLowerCase().contains("fetching") == false ){
                    place_name.setText("Fetching place details, wait...");
                    getGooglePlacesAPI().getPlaceDetailsByLatLng(new LatLng(locationDetails.lat, locationDetails.lng), new GooglePlacesAPI.PlaceDetailsCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                            if ( status ){
                                lastPlaceDetailFetchedLocation = locationDetails;
                                lastFetchedPlaceDetails = placeDetails;
                                place_name.setText(placeDetails.getFullName());
                                mapMarker.setSnippet(placeDetails.getFullName());
                            }else{
                                place_name.setText("Tap to fetch place name");
                            }
                        }
                    });
                }
            }else{
                if ( lastFetchedPlaceDetails != null ){
                    place_name.setText(lastFetchedPlaceDetails.getFullName());
                    mapMarker.setTitle(lastFetchedPlaceDetails.getFullName());
                }else{
                    place_name.setText("Tap to fetch place name");
                }
            }
        }
    }

    class LocationDetails implements Serializable{
        long time;
        double lat;
        double lng;
        LocationDetails(long time, double lat, double lng){
            this.time = time;
            this.lat = lat;
            this.lng = lng;
        }
    }


    public void displayJobRoute(Job jobDetails){

        if ( jobDetails != null ){
            LatLng startPointLatLng = jobDetails.getStartPointLatLng();
            LatLng endPointLatLng = jobDetails.getEndPointLatLng();

            googleMap.addMarker(new MarkerOptions().position(startPointLatLng).title("Job Start Point").snippet(job.getStartPoint()+"\n(Lat: " + startPointLatLng.latitude + ", Lng: " + startPointLatLng.longitude + ")").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).draggable(true));
            googleMap.addMarker(new MarkerOptions().position(endPointLatLng).title("Job End Point").snippet(job.getEndPoint()+"\n(Lat: " + endPointLatLng.latitude + ", Lng: " + endPointLatLng.longitude + ")").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).draggable(true));

            getGooglePlacesAPI().getDirections(startPointLatLng, endPointLatLng, new GooglePlacesAPI.DirectionsCallback() {
                @Override
                public void onComplete(@Nullable List<DirectionsData.Route.Leg> legs, int status_code) {
                    if (legs != null && legs.size() > 0) {//showToastMessage(legs.get(0).steps.size()+"=======>"+job.getEndPoint()+" "+job.getEndPointLatLng());
                        PolylineOptions lineOptions = new PolylineOptions();
                        for (DirectionsData.Route.Leg.Step step : legs.get(0).steps) {
                            lineOptions.addAll(step.polyline.getPolyLinePoints());
                        }
                        lineOptions.color(Color.rgb(127, 0, 255));
                        googleMap.addPolyline(lineOptions);
                    }
                }
            });
        }
    }

    //=========

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_reload, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_reload){
            getLastKnownLocation();
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
