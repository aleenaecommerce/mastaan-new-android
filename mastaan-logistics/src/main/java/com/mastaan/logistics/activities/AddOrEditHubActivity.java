package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.activities.LocationSelectorActivity;
import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateHub;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.HubDetails;

/**
 * Created by Venkatesh on 07/11/18.
 */

public class AddOrEditHubActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String actionType = Constants.ADD;

    vTextInputLayout name;
    vTextInputLayout position;
    vTextInputLayout address;

    View done;

    HubDetails hubDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_hub);

        if ( getIntent().getStringExtra(Constants.ACTION_TYPE) != null ){
            actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        }
        setToolbarTitle(actionType+" Hub");

        //------------

        name = findViewById(R.id.name);
        position = findViewById(R.id.position);
        position.getEditText().setOnClickListener(this);
        address = findViewById(R.id.address);

        done = findViewById(R.id.done);
        done.setOnClickListener(this);

        //-----------

        if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
            hubDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.HUB_DETAILS), HubDetails.class);

            name.setText(hubDetails.getName());
            position.setText(hubDetails.getLocationString());
            address.setText(hubDetails.getFullAddress());
        }

    }

    @Override
    public void onClick(View view) {
        if ( view == position.getEditText() ){
            Intent selectLocationAct = new Intent(context, LocationSelectorActivity.class);
            selectLocationAct.putExtra(ConstantsCommonLibrary.LATLNG, position.getText());
            startActivityForResult(selectLocationAct, Constants.LOCATION_SELECTION_ACTIVITY_CODE);
        }

        else if ( view == done ){
            String eName = name.getText();
            name.checkError("* Enter name");
            String ePosition = position.getText();
            position.checkError("* Enter position latlng");
            String eAddress = address.getText();
            address.checkError("* Enter full address");

            if ( eName.length() > 0 && ePosition.length() > 0 && eAddress.length() > 0 ){
                LatLng eLatLng = LatLngMethods.getLatLngFromString(ePosition);

                if ( eLatLng.latitude !=0 && eLatLng.longitude != 0 ){
                    RequestAddOrUpdateHub requestAddOrUpdateHub = new RequestAddOrUpdateHub(eName, eLatLng, eAddress);

                    if ( actionType.equalsIgnoreCase(Constants.ADD) ){
                        addHub(requestAddOrUpdateHub);
                    }
                    else if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                        updateHub(requestAddOrUpdateHub);
                    }
                }
                else{
                    position.showError("* Enter valid position");
                }
            }
        }
    }

    public void addHub(final RequestAddOrUpdateHub requestObject){

        showLoadingDialog("Adding hub, wait...");
        getBackendAPIs().getRoutesAPI().addHub(requestObject, new RoutesAPI.HubDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, HubDetails hubDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Hub added successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                    resultData.putExtra(Constants.HUB_DETAILS, new Gson().toJson(hubDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showSnackbarMessage("Something went wrong while adding hub, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            addHub(requestObject);
                        }
                    });
                }
            }
        });
    }



    public void updateHub(final RequestAddOrUpdateHub requestObject){

        showLoadingDialog("Updating hub, wait...");
        getBackendAPIs().getRoutesAPI().updateHub(hubDetails.getID(), requestObject, new RoutesAPI.HubDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, HubDetails hub_details) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Hub updated successfully");

                    hubDetails.setName(requestObject.getName());
                    hubDetails.setLocation(requestObject.getLocation());
                    hubDetails.setFullAddress(requestObject.getFullAddress());

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                    resultData.putExtra(Constants.HUB_DETAILS, new Gson().toJson(hubDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showSnackbarMessage("Something went wrong while updating hub, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateHub(requestObject);
                        }
                    });
                }
            }
        });
    }

    //=============


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.LOCATION_SELECTION_ACTIVITY_CODE && resultCode == RESULT_OK ){
                PlaceDetails placeDetails = new Gson().fromJson(data.getStringExtra(ConstantsCommonLibrary.PLACE_DETAILS), PlaceDetails.class);
                position.setText(placeDetails.getLatLng().latitude+","+placeDetails.getLatLng().longitude);
                address.setText(placeDetails.getFullAddress());
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
