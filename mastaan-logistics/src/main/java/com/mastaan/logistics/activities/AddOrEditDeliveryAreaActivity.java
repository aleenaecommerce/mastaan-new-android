package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.activities.LocationSelectorActivity;
import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateDeliveryArea;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.flows.RoutesFlow;
import com.mastaan.logistics.models.DeliveryAreaDetails;

/**
 * Created by Venkatesh on 31/03/17.
 */

public class AddOrEditDeliveryAreaActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String actionType = Constants.ADD;

    vTextInputLayout code;
    vTextInputLayout name;
    vTextInputLayout position;
    vTextInputLayout zone;

    View done;

    DeliveryAreaDetails deliveryAreaDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_delivery_area);

        if ( getIntent().getStringExtra(Constants.ACTION_TYPE) != null ){
            actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        }
        setToolbarTitle(actionType+" Delivery Area");

        //------------

        code = (vTextInputLayout) findViewById(R.id.code);
        name = (vTextInputLayout) findViewById(R.id.name);
        position = (vTextInputLayout) findViewById(R.id.position);
        position.getEditText().setOnClickListener(this);
        zone = (vTextInputLayout) findViewById(R.id.zone);
        zone.getEditText().setOnClickListener(this);

        done = findViewById(R.id.done);
        done.setOnClickListener(this);

        //-----------

        if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
            deliveryAreaDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.DELIVERY_AREA_DETAILS), DeliveryAreaDetails.class);

            code.setText(deliveryAreaDetails.getCode());
            name.setText(deliveryAreaDetails.getName());
            position.setText(deliveryAreaDetails.getLatLngString());
            zone.setText(deliveryAreaDetails.getDeliveryZoneID());
        }

    }

    @Override
    public void onClick(View view) {
        if ( view == position.getEditText() ){
            Intent selectLocationAct = new Intent(context, LocationSelectorActivity.class);
            selectLocationAct.putExtra(ConstantsCommonLibrary.LATLNG, position.getText());
            startActivityForResult(selectLocationAct, Constants.LOCATION_SELECTION_ACTIVITY_CODE);
        }
        else if ( view == zone.getEditText() ){
            new RoutesFlow(activity).selectDeliveryZone(zone.getText(), new RoutesFlow.DeliveryZoneSelectionCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String zoneID) {
                    if ( status ){
                        zone.setText(zoneID);
                    }
                }
            });
        }
        else if ( view == done ){
            String eCode = code.getText().toUpperCase();
            code.checkError("* Enter code");
            String eName = name.getText();
            name.checkError("* Enter name");
            String ePosition = position.getText();
            position.checkError("* Enter position latlng");
            String eZone = zone.getText();
            zone.checkError("* Enter zone id");

            if ( eCode.length() > 0 && eName.length() > 0 && ePosition.length() > 0 && eZone.length() > 0 ){
                LatLng eLatLng = LatLngMethods.getLatLngFromString(ePosition);

                if ( eLatLng.latitude !=0 && eLatLng.longitude != 0 ){
                    RequestAddOrUpdateDeliveryArea requestAddOrUpdateDeliveryArea = new RequestAddOrUpdateDeliveryArea(eCode, eName, eLatLng, eZone);

                    if ( actionType.equalsIgnoreCase(Constants.ADD) ){
                        addDeliveryArea(requestAddOrUpdateDeliveryArea);
                    }
                    else if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                        updateDeliveryArea(requestAddOrUpdateDeliveryArea);
                    }
                }
                else{
                    position.showError("* Enter valid position");
                }
            }
        }
    }

    public void addDeliveryArea(final RequestAddOrUpdateDeliveryArea requestAddOrUpdateDeliveryArea){

        showLoadingDialog("Adding delivery area, wait...");
        getBackendAPIs().getRoutesAPI().addDeliveryArea(requestAddOrUpdateDeliveryArea, new RoutesAPI.DeliveryAreaCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, DeliveryAreaDetails deliveryAreaDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Delivery area added successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                    resultData.putExtra(Constants.DELIVERY_AREA_DETAILS, new Gson().toJson(deliveryAreaDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showSnackbarMessage("Something went wrong while adding delivery area, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            addDeliveryArea(requestAddOrUpdateDeliveryArea);
                        }
                    });
                }
            }
        });
    }



    public void updateDeliveryArea(final RequestAddOrUpdateDeliveryArea requestAddOrUpdateDeliveryArea){

        showLoadingDialog("Updating delivery area, wait...");
        getBackendAPIs().getRoutesAPI().updateDeliveryArea(deliveryAreaDetails.getID(), requestAddOrUpdateDeliveryArea, new RoutesAPI.DeliveryAreaCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, DeliveryAreaDetails deliveryAreaDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Delivery area updated successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                    resultData.putExtra(Constants.DELIVERY_AREA_DETAILS, new Gson().toJson(deliveryAreaDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showSnackbarMessage("Something went wrong while updating delivery area, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateDeliveryArea(requestAddOrUpdateDeliveryArea);
                        }
                    });
                }
            }
        });
    }

    //=============


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.LOCATION_SELECTION_ACTIVITY_CODE && resultCode == RESULT_OK ){
                PlaceDetails placeDetails = new Gson().fromJson(data.getStringExtra(ConstantsCommonLibrary.PLACE_DETAILS), PlaceDetails.class);
                position.setText(placeDetails.getLatLng().latitude+","+placeDetails.getLatLng().longitude);
                name.setText(placeDetails.getFullAddress());
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
