package com.mastaan.logistics.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.TimePickerCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.DeliveryFeesAdapter;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.DeliveryFeeDialog;
import com.mastaan.logistics.dialogs.WarehouseMeatItemsSelectorDialog;
import com.mastaan.logistics.models.DeliveryFeeDetails;
import com.mastaan.logistics.models.DeliverySlotDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddOrEditDeliverySlotActivity extends MastaanToolbarActivity implements View.OnClickListener {

    vTextInputLayout start_time;
    vTextInputLayout end_time;
    vTextInputLayout minimum_distance;
    vTextInputLayout maximum_distance;
    vTextInputLayout maximum_orders;
    vTextInputLayout cutoff_time;

    CheckBox sunday, monday, tuesday, wednesday, thursday, friday, saturday;
    CheckBox chicken, farmchicken, countrychicken, mutton, goatmutton, sheepmutton, seafood, freshwatersf, seawatersf, pickles, marinades, eggs, readytoeat;
    vRecyclerView excludedItemsHolder;
    ExcludedItemsAdapter excludedItemsAdapter;
    View updateExcludedItems;

    vRecyclerView deliveryFeesHolder;
    DeliveryFeesAdapter deliveryFeesAdapter;
    View addDeliveryFee;

    Button actionDone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_delivery_slot);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        start_time = findViewById(R.id.start_time);
        start_time.getEditText().setOnClickListener(this);
        end_time = findViewById(R.id.end_time);
        end_time.getEditText().setOnClickListener(this);
        minimum_distance = findViewById(R.id.minimum_distance);
        maximum_distance = findViewById(R.id.maximum_distance);
        maximum_orders = findViewById(R.id.maximum_orders);
        cutoff_time = findViewById(R.id.cutoff_time);

        sunday = findViewById(R.id.sunday);
        monday = findViewById(R.id.monday);
        tuesday = findViewById(R.id.tuesday);
        wednesday = findViewById(R.id.wednesday);
        thursday = findViewById(R.id.thursday);
        friday = findViewById(R.id.friday);
        saturday = findViewById(R.id.saturday);

        chicken = findViewById(R.id.chicken);
        farmchicken = findViewById(R.id.farmchicken);
        countrychicken = findViewById(R.id.countrychicken);
        mutton = findViewById(R.id.mutton);
        goatmutton = findViewById(R.id.goatmutton);
        sheepmutton = findViewById(R.id.sheepmutton);
        seafood = findViewById(R.id.seafood);
        freshwatersf = findViewById(R.id.freshwatersf);
        seawatersf = findViewById(R.id.seawatersf);
        pickles = findViewById(R.id.pickles);
        marinades = findViewById(R.id.marinades);
        eggs = findViewById(R.id.eggs);
        readytoeat = findViewById(R.id.readytoeat);

        updateExcludedItems = findViewById(R.id.updateExcludedItems);
        updateExcludedItems.setOnClickListener(this);
        excludedItemsHolder = findViewById(R.id.excludedItemsHolder);
        excludedItemsHolder.setupVerticalOrientation();
        excludedItemsHolder.setNestedScrollingEnabled(false);
        excludedItemsAdapter = new ExcludedItemsAdapter(context, new ArrayList<String>());
        excludedItemsHolder.setAdapter(excludedItemsAdapter);

        deliveryFeesHolder = findViewById(R.id.deliveryFeesHolder);
        deliveryFeesHolder.setupVerticalOrientation();
        deliveryFeesHolder.setNestedScrollingEnabled(false);
        deliveryFeesAdapter = new DeliveryFeesAdapter(context, new ArrayList<DeliveryFeeDetails>(), new DeliveryFeesAdapter.Callback() {
            @Override
            public void onItemClick(final int position) {
                new DeliveryFeeDialog(activity).show(deliveryFeesAdapter.getItem(position), new DeliveryFeeDialog.Callback() {
                    @Override
                    public void onComplete(DeliveryFeeDetails deliveryFeeDetails) {
                        deliveryFeesAdapter.setItem(position, deliveryFeeDetails);
                    }
                });
            }

            @Override
            public void onDeleteClick(final int position) {
                showChoiceSelectionDialog("Confirm", "Are you sure to delete delivery fee?", "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES") ){
                            deliveryFeesAdapter.deleteItem(position);
                        }
                    }
                });
            }
        });
        deliveryFeesHolder.setAdapter(deliveryFeesAdapter);
        addDeliveryFee = findViewById(R.id.addDeliveryFee);
        addDeliveryFee.setOnClickListener(this);

        actionDone = (Button) findViewById(R.id.actionDone);
        actionDone.setOnClickListener(this);

        //--------

        if ( getIntent().getStringExtra(Constants.ACTION_TYPE) != null && getIntent().getStringExtra(Constants.ACTION_TYPE).equalsIgnoreCase(Constants.UPDATE) ){
            DeliverySlotDetails deliverySlotDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.SLOT_DETAILS), DeliverySlotDetails.class);

            start_time.getEditText().setEnabled(false);
            end_time.getEditText().setEnabled(false);
            minimum_distance.getEditText().setEnabled(false);
            maximum_distance.getEditText().setEnabled(false);

            start_time.setText(DateMethods.getTimeIn12HrFormat(deliverySlotDetails.getStartTime()));
            end_time.setText(DateMethods.getTimeIn12HrFormat(deliverySlotDetails.getEndTime()));
            minimum_distance.setText(CommonMethods.getInDecimalFormat(deliverySlotDetails.getMinimumDistance()));
            maximum_distance.setText(CommonMethods.getInDecimalFormat(deliverySlotDetails.getMaximumDistance()));
            maximum_orders.setText(deliverySlotDetails.getMaximumOrders()>0?""+deliverySlotDetails.getMaximumOrders():"");
            cutoff_time.setText(deliverySlotDetails.getCutoffTime()>=0?""+deliverySlotDetails.getCutoffTime():"");

            if ( deliverySlotDetails.getDisabledWeekDays() != null ){
                sunday.setChecked(deliverySlotDetails.getDisabledWeekDays().contains("sunday"));
                monday.setChecked(deliverySlotDetails.getDisabledWeekDays().contains("monday"));
                tuesday.setChecked(deliverySlotDetails.getDisabledWeekDays().contains("tuesday"));
                wednesday.setChecked(deliverySlotDetails.getDisabledWeekDays().contains("wednesday"));
                thursday.setChecked(deliverySlotDetails.getDisabledWeekDays().contains("thursday"));
                friday.setChecked(deliverySlotDetails.getDisabledWeekDays().contains("friday"));
                saturday.setChecked(deliverySlotDetails.getDisabledWeekDays().contains("saturday"));
            }

            if ( deliverySlotDetails.getDisabledCategories() != null ){
                chicken.setChecked(deliverySlotDetails.getDisabledCategories().contains("chicken"));
                farmchicken.setChecked(deliverySlotDetails.getDisabledCategories().contains("farmchicken"));
                countrychicken.setChecked(deliverySlotDetails.getDisabledCategories().contains("countrychicken"));
                mutton.setChecked(deliverySlotDetails.getDisabledCategories().contains("mutton"));
                goatmutton.setChecked(deliverySlotDetails.getDisabledCategories().contains("goatmutton"));
                sheepmutton.setChecked(deliverySlotDetails.getDisabledCategories().contains("sheepmutton"));
                seafood.setChecked(deliverySlotDetails.getDisabledCategories().contains("seafood"));
                freshwatersf.setChecked(deliverySlotDetails.getDisabledCategories().contains("freshwatersf"));
                seawatersf.setChecked(deliverySlotDetails.getDisabledCategories().contains("seawatersf"));
                pickles.setChecked(deliverySlotDetails.getDisabledCategories().contains("pickles"));
                marinades.setChecked(deliverySlotDetails.getDisabledCategories().contains("marinades"));
                eggs.setChecked(deliverySlotDetails.getDisabledCategories().contains("eggs"));
                readytoeat.setChecked(deliverySlotDetails.getDisabledCategories().contains("readytoeat"));
            }

            excludedItemsAdapter.setItems(deliverySlotDetails.getDisabledWarehouseMetatItems());
            deliveryFeesAdapter.setItems(deliverySlotDetails.getDeliveryFees());

            setToolbarTitle("Edit Delivery Slot");
            actionDone.setText("Update");
        }

    }

    @Override
    public void onClick(View view) {
        if ( view == start_time.getEditText() ){
            showTimePickerDialog("Start Time"/*, null, end_time.getText()*/, start_time.getText(), new TimePickerCallback() {
                @Override
                public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
                    start_time.setText(fullTimein12HrFormat);
                }
            });
        }

        else if ( view == end_time.getEditText() ){
            showTimePickerDialog("End Time"/*, start_time.getText(), null*/, end_time.getText(), new TimePickerCallback() {
                @Override
                public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
                    end_time.setText(fullTimein12HrFormat);
                }
            });
        }

        else if ( view == updateExcludedItems ){
            new WarehouseMeatItemsSelectorDialog(activity).setDialogTitle("Select Excluded Meat Items").show(getMastaanApplication().getCachedWarehouseMeatItems(), excludedItemsAdapter.getAllItems(), new WarehouseMeatItemsSelectorDialog.Callback() {
                @Override
                public void onComplete(List<String> selectedWarehouseMeatItems) {
                    excludedItemsAdapter.setItems(selectedWarehouseMeatItems);
                }
            });
        }

        else if ( view == addDeliveryFee ){
            new DeliveryFeeDialog(activity).show(new DeliveryFeeDialog.Callback() {
                @Override
                public void onComplete(DeliveryFeeDetails deliveryFeeDetails) {
                    deliveryFeesAdapter.addItem(deliveryFeeDetails);
                }
            });
        }

        else if ( view == actionDone ){
            String eStartTime = start_time.getText();
            start_time.checkError("* Enter start time");
            String eEndTime = end_time.getText();
            end_time.checkError("* Enter end time");
            String eMinDistance = minimum_distance.getText();
            minimum_distance.checkError("* Enter minimum distance");
            String eMaxDistance = maximum_distance.getText();
            maximum_distance.checkError("* Enter maximum distance");
            String eMaxOrders = maximum_orders.getText();
            maximum_orders.checkError("* Enter maximum orders");
            String eCutoffTime = cutoff_time.getText();
            cutoff_time.checkError("* Enter cutoff time");

            List<String> eDisabledCategories = new ArrayList<>();
            if ( chicken.isChecked() ){ eDisabledCategories.add("chicken");  }
            if ( farmchicken.isChecked() ){ eDisabledCategories.add("farmchicken");  }
            if ( countrychicken.isChecked() ){ eDisabledCategories.add("countrychicken");  }
            if ( mutton.isChecked() ){ eDisabledCategories.add("mutton");  }
            if ( goatmutton.isChecked() ){ eDisabledCategories.add("goatmutton");  }
            if ( sheepmutton.isChecked() ){ eDisabledCategories.add("sheepmutton");  }
            if ( seafood.isChecked() ){ eDisabledCategories.add("seafood");  }
            if ( freshwatersf.isChecked() ){ eDisabledCategories.add("freshwatersf");  }
            if ( seawatersf.isChecked() ){ eDisabledCategories.add("seawatersf");  }
            if ( pickles.isChecked() ){ eDisabledCategories.add("pickles");  }
            if ( marinades.isChecked() ){ eDisabledCategories.add("marinades");  }
            if ( eggs.isChecked() ){ eDisabledCategories.add("eggs");  }
            if ( readytoeat.isChecked() ){ eDisabledCategories.add("readytoeat");  }

            List<String> eDisabledDays = new ArrayList<>();
            if ( sunday.isChecked() ){ eDisabledDays.add("sunday");  }
            if ( monday.isChecked() ){ eDisabledDays.add("monday");  }
            if ( tuesday.isChecked() ){ eDisabledDays.add("tuesday");  }
            if ( wednesday.isChecked() ){ eDisabledDays.add("wednesday");  }
            if ( thursday.isChecked() ){ eDisabledDays.add("thursday");  }
            if ( friday.isChecked() ){ eDisabledDays.add("friday");  }
            if ( saturday.isChecked() ){ eDisabledDays.add("saturday");  }

            if ( DateMethods.compareDates(eStartTime, eEndTime) >= 0 ){
                showToastMessage("* End time must be greater than Start time");
                return;
            }

            if ( eStartTime.length() > 0 && eEndTime.length() > 0 && eMaxDistance.length() > 0 && eMinDistance.length() > 0 && eMaxOrders.length() > 0 && eCutoffTime.length() > 0 ){
                if ( CommonMethods.parseDouble(eMaxDistance) == 0 ){
                    maximum_distance.showError("* Maximum distance must be greater than 0");
                    return;
                }
                if ( CommonMethods.parseDouble(eMinDistance) >= CommonMethods.parseDouble(eMaxDistance) ){
                    maximum_distance.showError("* Maximum distance must be greater than Minimum distance");
                    return;
                }
                if ( CommonMethods.parseDouble(eMaxOrders) <= 0 ){
                    maximum_orders.showError("* Maximum orders must be greater than 0");
                    return;
                }

                Calendar startTimeCal = DateMethods.getCalendarFromDate(eStartTime);
                Calendar endTimeCal = DateMethods.getCalendarFromDate(eEndTime);
                DeliverySlotDetails deliverySlotDetails = new DeliverySlotDetails(startTimeCal.get(Calendar.HOUR_OF_DAY), startTimeCal.get(Calendar.MINUTE), endTimeCal.get(Calendar.HOUR_OF_DAY), endTimeCal.get(Calendar.MINUTE));
                deliverySlotDetails.setMinimumDistance(CommonMethods.parseDouble(eMinDistance));
                deliverySlotDetails.setMaximumDistance(CommonMethods.parseDouble(eMaxDistance));
                deliverySlotDetails.setMaximumOrders(CommonMethods.parseInteger(eMaxOrders));
                deliverySlotDetails.setCutoffTime(CommonMethods.parseInteger(eCutoffTime));
                deliverySlotDetails.setDisabledCategories(eDisabledCategories);
                deliverySlotDetails.setDisabledWarehouseMetatItems(excludedItemsAdapter.getAllItems());//CommonMethods.getStringListFromStringArray(new String[]{"57bc29c450ec62e3aa1650be", "57bc29c450ec62e3aa1650c0"}));
                deliverySlotDetails.setDisabledWeekDays(eDisabledDays);
                deliverySlotDetails.setDeliveryFees(deliveryFeesAdapter.getAllItems());

                Intent resultData = new Intent();
                resultData.putExtra(Constants.ACTION_TYPE, getIntent().getStringExtra(Constants.ACTION_TYPE));
                resultData.putExtra(Constants.SLOT_DETAILS, new Gson().toJson(deliverySlotDetails));
                setResult(RESULT_OK, resultData);
                finish();
            }
        }
    }

    //----------

    class ExcludedItemsAdapter extends RecyclerView.Adapter<ExcludedItemsAdapter.ViewHolder> {
        Context context;
        List<String> itemsList;

        public class ViewHolder extends RecyclerView.ViewHolder{
            TextView name;

            public ViewHolder(View itemLayoutView) {
                super(itemLayoutView);
                name = (TextView) itemLayoutView.findViewById(com.aleena.common.R.id.name);
            }
        }

        public ExcludedItemsAdapter(Context context, List<String> itemsList) {
            this.context = context;
            this.itemsList = itemsList;
        }

        @Override
        public int getItemCount() {
            return itemsList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public ExcludedItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list_item, null);
            return (new ExcludedItemsAdapter.ViewHolder(orderItemHolder));
        }

        @Override
        public void onBindViewHolder(final ExcludedItemsAdapter.ViewHolder viewHolder, final int position) {
            WarehouseMeatItemDetails warehouseMeatItemDetails = getMastaanApplication().getCachedWarehouseMeatMeatItemDetails(itemsList.get(position));
            if ( warehouseMeatItemDetails != null ){
                viewHolder.name.setText(warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize());
            }else{
                viewHolder.name.setText(itemsList.get(position));
            }
        }

        //------------

        public void setItems(List<String> items){
            if ( items == null ){ items = new ArrayList<>();    }
            itemsList = items;
            notifyDataSetChanged();
        }

        public List<String> getAllItems(){
            return itemsList;
        }

    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}