package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.MembershipsAdapter;
import com.mastaan.logistics.backend.ConfigurationsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.MembershipDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 09/01/19.
 */

public class MembershipsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    vRecyclerView itemsHolder;
    MembershipsAdapter membershipsAdapter;

    View actionAdd;

    int selectedItemPosition = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memberships);
        hasLoadingView();
        hasSwipeRefresh();

        //--------

        actionAdd = findViewById(R.id.actionAdd);
        actionAdd.setOnClickListener(this);

        itemsHolder = findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        membershipsAdapter = new MembershipsAdapter(context, new ArrayList<MembershipDetails>(), new MembershipsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                selectedItemPosition = position;

                Intent updateAlertMessageAct = new Intent(context, AddOrEditMembershipActivity.class);
                updateAlertMessageAct.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                updateAlertMessageAct.putExtra(Constants.MEMBERSHIP_DETAILS, new Gson().toJson(membershipsAdapter.getItem(position)));
                startActivityForResult(updateAlertMessageAct, Constants.ADD_OR_EDIT_MEMBERSHIP_ACTIVITY_CODE);
            }

            @Override
            public void onDeleteClick(final int position) {
                showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to delete <b>" + membershipsAdapter.getItem(position).getTypeName() + "</b> membership?"), "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES") ){
                            deleteMembership(position);
                        }
                    }
                });
            }
        });
        itemsHolder.setAdapter(membershipsAdapter);

        //--------

        getMemberships();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getMemberships();
    }

    @Override
    public void onClick(View view) {
        if ( view == actionAdd ){
            final List<String> finalOptionsList = new ArrayList<>();

            final List<String> optionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.SILVER, Constants.GOLD, Constants.DIAMOND, Constants.PLATINUM});
            for (int i=0;i<optionsList.size();i++){
                boolean contains = false;
                for (int j=0;j<membershipsAdapter.getItemCount();j++) {
                    if ( membershipsAdapter.getItem(j).getTypeName().equalsIgnoreCase(optionsList.get(i)) ) {
                        contains = true;
                        break;
                    }
                }
                if ( contains == false ) {
                    finalOptionsList.add(optionsList.get(i));
                }
            }

            if ( finalOptionsList.size() == 0 ){
                showToastMessage("No more to add");
            }else{
                showListChooserDialog("Select Membership", finalOptionsList, new ListChooserCallback() {
                    @Override
                    public void onSelect(int position) {
                        String selectedMembership = finalOptionsList.get(position);
                        String membershipType = "";
                        if ( selectedMembership.equalsIgnoreCase(Constants.SILVER) ){
                            membershipType = "sm";
                        }else if ( selectedMembership.equalsIgnoreCase(Constants.GOLD) ){
                            membershipType = "gm";
                        }else if ( selectedMembership.equalsIgnoreCase(Constants.DIAMOND) ){
                            membershipType = "dm";
                        }else if ( selectedMembership.equalsIgnoreCase(Constants.PLATINUM) ){
                            membershipType = "pm";
                        }

                        Intent addMembershipAct = new Intent(context, AddOrEditMembershipActivity.class);
                        addMembershipAct.putExtra(Constants.TYPE, membershipType);
                        addMembershipAct.putExtra(Constants.TYPE_NAME, selectedMembership);
                        startActivityForResult(addMembershipAct, Constants.ADD_OR_EDIT_MEMBERSHIP_ACTIVITY_CODE);
                    }
                });
            }
        }
    }

    //-------

    private void getMemberships(){

        actionAdd.setVisibility(View.GONE);
        showLoadingIndicator("Loading memberships, wait...");
        getBackendAPIs().getConfigurationsAPI().getMemberships(new ConfigurationsAPI.MembershipsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<MembershipDetails> membershipsList) {
                if ( status ){
                    actionAdd.setVisibility(View.VISIBLE);

                    if ( membershipsList.size() > 0 ){
                        membershipsAdapter.setItems(membershipsList);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("Nothing to show");
                    }
                }else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    private void deleteMembership(final int position){

        showLoadingDialog("Deleting membership, wait...");
        getBackendAPIs().getConfigurationsAPI().deleteAlertMessage(membershipsAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Membership deleted successfully");
                    membershipsAdapter.deleteItem(position);
                    if ( membershipsAdapter.getItemCount() == 0 ){
                        showNoDataIndicator("Nothing to show");
                    }

                    showChoiceSelectionDialog(false, "Confirm", "Do you want to update all buyers memberships with latest configuration?\n\nNote: This will take some time & happens in background.", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES") ){
                                updateAllBuyerMemberships();
                            }
                        }
                    });
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    private void updateAllBuyerMemberships(){
        showToastMessage("Updating buyers memberships...");
        getBackendAPIs().getConfigurationsAPI().updateAllBuyersMemberships(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if ( status ){  showToastMessage("Buyers memberships updated successfully");    }
            }
        });
    }

    //==============

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_memberships, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.ADD_OR_EDIT_MEMBERSHIP_ACTIVITY_CODE && resultCode == RESULT_OK ){
                String actionType = data.getStringExtra(Constants.ACTION_TYPE);
                MembershipDetails membershipDetails = new Gson().fromJson(data.getStringExtra(Constants.MEMBERSHIP_DETAILS), MembershipDetails.class);
                if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                    MembershipDetails oldMembershipDetails = membershipsAdapter.getItem(selectedItemPosition);
                    membershipsAdapter.setItem(selectedItemPosition, membershipDetails);

                    if ( membershipDetails.getBuyerTotalOrdersMinimumAmount() != oldMembershipDetails.getBuyerTotalOrdersMinimumAmount()
                            || membershipDetails.getBuyerTotalOrdersMaximumAmount() != oldMembershipDetails.getBuyerTotalOrdersMaximumAmount() ){
                        showChoiceSelectionDialog(false, "Confirm", "Do you want to update all buyers memberships with latest configuration?\n\nNote: This will take some time & happens in background.", "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("YES") ){
                                    updateAllBuyerMemberships();
                                }
                            }
                        });
                    }
                }
                else{
                    membershipsAdapter.addItem(itemsHolder.getLastCompletelyVisibleItemPosition()+1, membershipDetails);
                    switchToContentPage();

                    showChoiceSelectionDialog(false, "Confirm", "Do you want to update all buyers memberships with latest configuration?\n\nNote: This will take some time & happens in background.", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES") ){
                                updateAllBuyerMemberships();
                            }
                        }
                    });
                }
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            showListChooserDialog(new String[]{"Update all buyers memberships"}, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( position == 0 ){
                        showChoiceSelectionDialog("Confirm", "Are you to update all buyers memberships with latest configuration?\n\nNote: This will take some time & happens in background.", "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("YES") ){
                                    updateAllBuyerMemberships();
                                }
                            }
                        });
                    }
                }
            });
        }
        else{
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
