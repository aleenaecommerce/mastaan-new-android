package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.View;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

public class UnprocessedCustomerSupportActivity extends BaseOrderItemsActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();
        setFilterMenuItemVisibility(false);
        hasActionButton("Mark all as processed");
        hasActionMenuItem("Release");

        //-----

        //LOADING UNPROCESSED CUSTOMER SUPPORT ITEMS FROM INTENT AFTER MENU CREATED (onAfterMenuCreated)

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        showLoadingIndicator("Loading your unprocessed customer support items, wait...");
        hideActionButton();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        new AsyncTask<Void, Void, Void>() {
            GroupedOrdersItemsList groupedUnprocessedCustomerSupportItems;
            @Override
            protected Void doInBackground(Void... params) {
                String unprocessed_customer_support_jsons = getIntent().getStringExtra(Constants.UNPROCESSED_CUSTOMER_SUPPORT);
                if ( unprocessed_customer_support_jsons != null && unprocessed_customer_support_jsons.length() > 0 ){
                    groupedUnprocessedCustomerSupportItems = new Gson().fromJson(unprocessed_customer_support_jsons, GroupedOrdersItemsList.class);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if ( groupedUnprocessedCustomerSupportItems != null && groupedUnprocessedCustomerSupportItems.getItems().size() > 0 ){
                    showActionButton();
                    List<GroupedOrdersItemsList> groupedOrdersItemsLists = new ArrayList<GroupedOrdersItemsList>();
                    groupedOrdersItemsLists.add(groupedUnprocessedCustomerSupportItems);
                    displayItems(true, groupedOrdersItemsLists, new ArrayList<OrderDetails>());
                }else {
                    getUnprocessedCustomerSupportItems();
                }
            }
        }.execute();
    }

    @Override
    public void onReloadPage() {
        super.onReloadPage();
        getUnprocessedCustomerSupportItems();
    }

    @Override
    public void onReloadFromDatabase() {
        onReloadPage();
    }

    @Override
    public void onActionMenuItemClick() {
        super.onActionMenuItemClick();
        releaseAllCustomerSupportItems();
    }

    @Override
    public void onActionButtonClick() {
        super.onActionButtonClick();
        showChoiceSelectionDialog(false, "Confirm!", "Are you sure to mark all as processed?", "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if ( choiceName.equalsIgnoreCase("YES")){
                    completeAllCustomerSupportItems();
                }
            }
        });
    }

    private void getUnprocessedCustomerSupportItems() {

        showLoadingIndicator("Loading your unprocessed customer support items, wait...");
        hideActionButton();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        getBackendAPIs().getCustomerSupportAPI().getUnprocessedCustomerSupportItems(localStorageData.getWarehouseLocation(), new OrderItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                if (status) {
                    if ( orderItemsList.size() > 0 ) {
                        showActionButton();
                        List<GroupedOrdersItemsList> groupedOrdersItemsList = new ArrayList<GroupedOrdersItemsList>();
                        groupedOrdersItemsList.add(new GroupedOrdersItemsList("All", orderItemsList));
                        displayItems(true, groupedOrdersItemsList, new ArrayList<OrderDetails>());
                    } else {
                        showNoDataIndicator("No items");
                    }
                } else {
                    showReloadIndicator("Unable to your unprocessed customer support items, try again!");
                }
            }
        });
    }

    public void releaseAllCustomerSupportItems(){

        showLoadingDialog("Releasing all items, wait...");
        getBackendAPIs().getCustomerSupportAPI().releaseAllCustomerSupportItems(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if ( status ){
                    List<OrderItemDetails> orderItems = groupedOrdersItemsLists.get(0).getItems();
                    for (int i=0;i<orderItems.size();i++){
                        orderItems.get(i).setCustomerSupportExecutiveDetails(null);
                    }
                    getOrderItemsDatabase().addOrUpdateOrderItems(orderItems, new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            Intent customerSupportAct = new Intent(context, CustomerSupportActivity.class);
                            startActivity(customerSupportAct);
                            finish();
                        }
                    });
                }else{
                    closeLoadingDialog();
                    showSnackbarMessage("Something went wrong while releasing items, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            releaseAllCustomerSupportItems();
                        }
                    });
                }
            }
        });
    }

    public void completeAllCustomerSupportItems(){
        showLoadingDialog("Marking all as processed, wait...");
        getBackendAPIs().getCustomerSupportAPI().completeAllCustomerSupportItems(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if ( status ){
                    List<OrderItemDetails> orderItems = groupedOrdersItemsLists.get(0).getItems();
                    for (int i=0;i<orderItems.size();i++){
                        orderItems.get(i).setCustomerSupportState(false);
                    }
                    getOrderItemsDatabase().addOrUpdateOrderItems(orderItems, new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            Intent customerSupportAct = new Intent(context, CustomerSupportActivity.class);
                            startActivity(customerSupportAct);
                            finish();
                        }
                    });
                }else{
                    closeLoadingDialog();
                    showSnackbarMessage("Something went wrong while markig all as processed, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            releaseAllCustomerSupportItems();
                        }
                    });
                }
            }
        });
    }
}