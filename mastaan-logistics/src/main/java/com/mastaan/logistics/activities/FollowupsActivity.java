package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.SearchView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.FollowupsAdapter;
import com.mastaan.logistics.backend.FollowupsAPI;
import com.mastaan.logistics.backend.models.RequestAddFollowup;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AnalyseFollowupsDialog;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.DatePeriodSelectorDialog;
import com.mastaan.logistics.dialogs.FollowupDialog;
import com.mastaan.logistics.dialogs.OrderDetailsDialog;
import com.mastaan.logistics.fragments.FilterItemsFragment;
import com.mastaan.logistics.methods.AnalyseFollowupsMethods;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.SearchDetails;

import java.util.ArrayList;
import java.util.List;

public class FollowupsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String buyerID;

    DrawerLayout drawerLayout;
    FilterItemsFragment filterFragment;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    MenuItem searchViewMenuITem;
    SearchView searchView;
    MenuItem filterMenuItem;

    TextView summary_info;
    View dateView;
    TextView from_date;
    TextView to_date;
    View changePeriod;

    vRecyclerView followupsHolder;
    FollowupsAdapter followupsAdapter;

    View showSummary;
    View addFollowup;

    List<FollowupDetails> currentFollowupsList = new ArrayList<>();

    SearchDetails searchDetails;

    FollowupDialog followupDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_followups);
        hasLoadingView();
        hasSwipeRefresh();

        //--------

        filterFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    clearSearch();
                }
                else {
                    performSearch(searchDetails);
                }
            }
        });

        drawerLayout = findViewById(R.id.drawer_layout);

        searchResultsCountIndicator = findViewById(R.id.searchResultsCountIndicator);
        results_count = findViewById(R.id.results_count);
        clearFilter = findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        //---------

        summary_info = findViewById(R.id.summary_info);
        dateView = findViewById(R.id.dateView);
        from_date = findViewById(R.id.from_date);
        to_date = findViewById(R.id.to_date);
        changePeriod = findViewById(R.id.changePeriod);
        changePeriod.setOnClickListener(this);

        showSummary = findViewById(R.id.showSummary);
        showSummary.setOnClickListener(this);
        addFollowup = findViewById(R.id.addFollowup);
        addFollowup.setOnClickListener(this);

        followupsHolder = findViewById(R.id.followupsHolder);
        followupsHolder.setupVerticalOrientation();
        followupsAdapter = new FollowupsAdapter(activity, new ArrayList<FollowupDetails>(), new FollowupsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {}

            @Override
            public void onShowOrder(int position) {
                new OrderDetailsDialog(activity).show(followupsAdapter.getItem(position).getOrderID());
            }

            @Override
            public void callBuyer(int position) {
                callToPhoneNumber(followupsAdapter.getItem(position).getBuyerDetails().getMobileNumber());
            }

            @Override
            public void onShowBuyerHistory(int position) {
                new BuyerOptionsDialog(activity).show(followupsAdapter.getItem(position).getBuyerDetails());
            }
        });
        followupsHolder.setAdapter(followupsAdapter);

        //---------

        if ( getIntent().getStringExtra(Constants.BUYER_ID) != null ){
            if ( getIntent().getStringExtra(Constants.BUYER_NAME) != null ) {
                setToolbarTitle(getIntent().getStringExtra(Constants.BUYER_NAME) + " Followups");
            }
            dateView.setVisibility(View.GONE);
            buyerID = getIntent().getStringExtra(Constants.BUYER_ID);
            getFollowups(null, null);
        }
        else{
            if ( getIntent().getStringExtra(Constants.TITLE) != null ){
                setToolbarTitle(getIntent().getStringExtra(Constants.TITLE));
            }

            showNoDataIndicator("Select date");
            changePeriod.performClick();
        }

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getFollowups(from_date.getText().toString(), to_date.getText().toString());
    }

    @Override
    public void onClick(View view) {

        if ( view == changePeriod ){
            String prefillFromDate = from_date.getText().toString();
            String prefillToDate = to_date.getText().toString();
            if ( prefillFromDate.equalsIgnoreCase("Start")&& prefillToDate.equalsIgnoreCase("End") ){
                prefillFromDate = DateMethods.getCurrentDate();
            }
            new DatePeriodSelectorDialog(activity).show(prefillFromDate, prefillToDate, new DatePeriodSelectorDialog.Callback() {
                @Override
                public void onComplete(String fromDate, String toDate) {
                    if ( (fromDate != null && fromDate.length() > 0) || (toDate != null && toDate.length() > 0) ){
                        from_date.setText(fromDate != null&&fromDate.length()>0?DateMethods.getDateInFormat(fromDate, DateConstants.MMM_DD_YYYY):"Start");
                        to_date.setText(toDate != null&&toDate.length()>0?DateMethods.getDateInFormat(toDate, DateConstants.MMM_DD_YYYY):"End");
                        getFollowups(fromDate, toDate);
                    }else{
                        showToastMessage("* Please select at least one date");
                    }
                }
            });
        }
        else if ( view == clearFilter ){
            try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
            clearSearch();
        }
        else if ( view == showSummary ){
            if ( getIntent().getBooleanExtra(Constants.FIRST_ORDER, false) ){
                new AnalyseFollowupsDialog(activity, "Summary", CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW, Constants.EMPLOYEES, Constants.BUYERS, Constants.COMMENTS}), followupsAdapter.getAllItems()).show();
            }else{
                new AnalyseFollowupsDialog(activity, "Summary", followupsAdapter.getAllItems()).show();
            }
        }
        else if ( view == addFollowup ){
            if ( buyerID != null && buyerID.length() > 0 ){
                followupDialog = new FollowupDialog(activity).show(new BuyerDetails(buyerID, getIntent().getStringExtra(Constants.BUYER_NAME)), new FollowupDialog.Callback() {
                    @Override
                    public void onComplete(RequestAddFollowup requestObject) {
                        addFollowup(requestObject);
                    }
                });
            }
            else{
                showInputTextDialog("Select Buyer", "Buyer mobile / email", new TextInputCallback() {
                    @Override
                    public void onComplete(final String inputText) {
                        new CountDownTimer(200, 100) {
                            @Override
                            public void onTick(long millisUntilFinished) {}

                            @Override
                            public void onFinish() {
                                followupDialog = new FollowupDialog(activity).show(new FollowupDialog.Callback() {
                                    @Override
                                    public void onComplete(RequestAddFollowup requestObject) {
                                        addFollowup(requestObject);
                                    }
                                }).initiateForBuyer(inputText.trim());
                            }
                        }.start();
                    }
                });
            }
        }
    }

    private void getFollowups(String fromDate, String toDate){

        summary_info.setText("");
        changePeriod.setClickable(false);
        searchResultsCountIndicator.setVisibility(View.GONE);
        addFollowup.setVisibility(View.GONE);
        showSummary.setVisibility(View.GONE);
        hideMenuItems();
        showLoadingIndicator("Loading followups, wait...");
        FollowupsAPI.FollowupsListCallback callback = new FollowupsAPI.FollowupsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<FollowupDetails> followupsList) {
                changePeriod.setClickable(true);

                if (status) {
                    addFollowup.setVisibility(View.VISIBLE);

                    currentFollowupsList = followupsList;
                    if ( followupsList.size() > 0 ){
                        new AsyncTask<Void, Void, Void>() {
                            String summary = "";
                            @Override
                            protected Void doInBackground(Void... voids) {
                                summary = new AnalyseFollowupsMethods(followupsList).setSectionsList(new String[]{Constants.OVERVIEW}).analyse().getSummaryInfo();
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);

                                summary_info.setText(CommonMethods.fromHtml(summary));
                                showSummary.setVisibility(View.VISIBLE);
                                showMenuItems();
                                followupsAdapter.setItems(followupsList);
                                switchToContentPage();
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }else{
                        showNoDataIndicator("No followups to show");
                    }
                } else {
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        };

        if ( buyerID != null ){
            getBackendAPIs().getFollowupsAPI().getBuyerFollowups(buyerID, callback);
        }
        else{
            getBackendAPIs().getFollowupsAPI().getFollowups((fromDate!=null&&fromDate.length()>0&&fromDate.equalsIgnoreCase("Start")==false?DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY):null)
                    , (toDate!=null&&toDate.length()>0&&toDate.equalsIgnoreCase("End")==false?DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY):null)
                    , getIntent().getBooleanExtra(Constants.ORDER_RESULTED, false)?"true":null
                    , getIntent().getBooleanExtra(Constants.FIRST_ORDER, false)?"true":null
                    , callback);
        }

    }

    private void addFollowup(final RequestAddFollowup requestAddFollowup){

        activity.showLoadingDialog("Adding followup, wait...");
        activity.getBackendAPIs().getFollowupsAPI().addFollowup(requestAddFollowup, new FollowupsAPI.FollowupDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, FollowupDetails followupDetails) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Followup added successfully");
                    followupsAdapter.addItem(followupsHolder.getLastCompletelyVisibleItemPosition()-1, followupDetails);
                    switchToContentPage();
                }
                else {
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while adding followup.\nPlease try again.", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                addFollowup(requestAddFollowup);
                            }
                        }
                    });
                }
            }
        });
    }

    //----------

    public void performSearch(final SearchDetails search_details){
        this.searchDetails = search_details;

        if ( isShowingLoadingPage() == false && currentFollowupsList != null && currentFollowupsList.size() > 0 ) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
                SearchMethods.searchFollowups(currentFollowupsList, searchDetails, new SearchMethods.SearchFollowupsCallback() {
                    @Override
                    public void onComplete(boolean status, SearchDetails searchDetails, List<FollowupDetails> filteredList) {
                        if (status && searchDetails == search_details ) {
                            summary_info.setVisibility(View.GONE);
                            searchResultsCountIndicator.setVisibility(View.VISIBLE);
                            results_count.setText(filteredList.size() + " result(s) found.\n(" + searchDetails.getSearchString() + ")");
                            if (filteredList.size() > 0) {
                                followupsAdapter.setItems(filteredList);
                                followupsHolder.scrollToPosition(0);
                                switchToContentPage();
                            }
                            else {
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");
                                followupsAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                followupsAdapter.setItems(currentFollowupsList);
            }
        }
    }

    public void clearSearch(){
        this.searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( currentFollowupsList.size() > 0 ) {
            summary_info.setVisibility(View.VISIBLE);
            followupsAdapter.setItems(currentFollowupsList);
            followupsHolder.scrollToPosition(0);
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }
        filterFragment.clearSelection();
    }

    //---------

    public void hideMenuItems(){
        try {
            filterMenuItem.setVisible(false);
            searchViewMenuITem.setVisible(false);//searchViewMenuITem.collapseActionView();
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    public void showMenuItems(){
        try {
            filterMenuItem.setVisible(true);
            searchViewMenuITem.setVisible(true);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }


    //=============

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_orders_by_date, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filterMenuItem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                clearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchDetails searchDetails = new SearchDetails().setConsiderAtleastOneMatch(query);
                performSearch(searchDetails);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( followupDialog != null ){  followupDialog.onActivityResult(requestCode, resultCode, data); }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){

        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            super.onBackPressed();
        }
    }

}