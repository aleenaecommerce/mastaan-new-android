package com.mastaan.logistics.activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.methods.DateMethods;
import com.google.gson.Gson;
import com.mastaan.logistics.MastaanApplication;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.PlaceChooserAdapter;
import com.mastaan.logistics.backend.BackendAPIs;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.fragments.MastaanFragment;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.localdata.OrderItemsDatabase2;
import com.mastaan.logistics.methods.BroadcastReceiversMethods;
import com.mastaan.logistics.models.CustomPlace;
import com.mastaan.logistics.models.DiscountDetails;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 08-01-2016.
 */
public class MastaanToolbarActivity extends vToolBarActivity {

    MastaanApplication mastaanApplication;
    MastaanToolbarActivity activity;

    TextView customActionBarTitle;

    public LocalStorageData localStorageData;

    BroadcastReceiver activityReceiver;
    BroadcastReceiversMethods broadcastReceiversMethods;

    private BackendAPIs backendAPIs;

    private List<DiscountDetails> discountReasons;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        context = this;
        activity = this;
        localStorageData = new LocalStorageData(context);
        broadcastReceiversMethods = new BroadcastReceiversMethods(context);

        //-----

        try {
            View toolbarTitleView = inflater.inflate(R.layout.view_toolbar_title, null);
            customActionBarTitle = toolbarTitleView.findViewById(R.id.toolbar_title);
            customActionBarTitle.setText(actionBar.getTitle() + " ");
            actionBar.setTitle("");
            toolbar.addView(toolbarTitleView);
        }catch (Exception e){}

    }

    public MastaanApplication getMastaanApplication() {
        if ( mastaanApplication == null){
            mastaanApplication = (MastaanApplication) getApplication();       // Accessing Application Class;
        }
        return mastaanApplication;
    }

    public OrderItemsDatabase2 getOrderItemsDatabase() {
        MastaanApplication mastaanApplication = getMastaanApplication();
        try {
            return mastaanApplication.getOrderItemsDatabase();
        }catch (Exception e){
            e.printStackTrace();
            Log.d(Constants.LOG_TAG, "Exception in ApplicationLevel_DATABASE , Exception = "+e);
            return new OrderItemsDatabase2(context);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        long differenceTime = DateMethods.getDifferenceToCurrentTime(new LocalStorageData(this).getHomePageLastLoadedTime());
        //Log.d(Constants.LOG_TAG, (differenceTime/1000)+"  >> last: "+localStorageData.getHomePageLastLoadedTime()+"  , current: "+DateMethods.getCurrentDateAndTime());
        if ( differenceTime > 600000 ) {     // If greater than 10 mins (600,000 millisec) InActiveTime
            Log.d(Constants.LOG_TAG, "Refreshing App as InActive time > 10min");
            onInactiveTimeElapsed();
        }
    }

    public void onInactiveTimeElapsed(){

    }

    @Override
    protected void onPause() {
        try {
            new LocalStorageData(this).setHomePageLastLoadedTime(DateMethods.getCurrentDateAndTime());
        }catch (Exception e){}
        //Log.d(Constants.LOG_TAG, "ON_PAUSE  >> last: "+localStorageData.getHomePageLastLoadedTime()+"  , current: "+DateMethods.getCurrentDateAndTime());

        super.onPause();
    }

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(context, getDeviceID(), getAppVersionCode());
        }
        return backendAPIs;
    }

    public void setToolbarTitle(String title){
        customActionBarTitle.setText(title + " ");
    }

    public void showMessageLong(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void callToPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(intent);
    }

    public void setDiscountReasons(List<DiscountDetails> discountReasons) {
        this.discountReasons = discountReasons;
    }

    public List<DiscountDetails> getDiscountReasons() {
        if ( discountReasons == null || discountReasons.size() == 0 ){
            discountReasons = new LocalStorageData(activity).getDiscountReasons();
            setDiscountReasons(discountReasons);
        }
        return discountReasons;
    }

    public void showListChooserDialog(String title, List<? extends CustomPlace> places, final MastaanFragment.PlaceChooserCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View convertView = LayoutInflater.from(context).inflate(R.layout.dialog_list_items_chooser, null);
        ListView itemsHolder = (ListView) convertView.findViewById(R.id.list_view);
        builder.setView(convertView);
        builder.setCancelable(true);
        final AlertDialog dialog = builder.create();
        TextView titleView = (TextView) convertView.findViewById(R.id.title);
        titleView.setText(title);
        dialog.show();
        itemsHolder.setAdapter(new PlaceChooserAdapter(context, places, new PlaceChooserAdapter.ItemClickListener() {
            @Override
            public void onSelect(int position, CustomPlace place) {
                dialog.dismiss();
                callback.onSelect(place);
            }
        }));
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                callback.onCancel();
            }
        });
    }

    //-------

    public void toggleViewsVisibility(View[]visibleViews, View[]invisibleViews){
        toggleViewsVisibility(visibleViews, View.VISIBLE);
        toggleViewsVisibility(invisibleViews, View.GONE);
    }
    public void toggleViewsVisibility(View[]views, int visibility){
        if ( views != null && views.length > 0 && (visibility == View.VISIBLE || visibility == View.INVISIBLE || visibility == View.GONE) ){
            for (int i=0;i<views.length;i++){
                if ( views[i] != null ){
                    views[i].setVisibility(visibility);
                }
            }
        }
    }

    //-------

    public void goToUnprocessedCustomerSupportItemsPage(final List<OrderItemDetails> unprocessedItems){
        //if ( unprocessedItems != null && unprocessedItems.size() > 0 ){
            new AsyncTask<Void, Void, Void>() {
                String unprocessedCustomerSupportsJSON = "";
                @Override
                protected Void doInBackground(Void... voids) {
                    unprocessedCustomerSupportsJSON = new Gson().toJson(new GroupedOrdersItemsList("", unprocessedItems));
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    //showToastMessage("You have " + unprocessedItems.size() + " unprocessed customer support items.");
                    Intent unProcessedCustomerSupportAct = new Intent(context, UnprocessedCustomerSupportActivity.class);
                    unProcessedCustomerSupportAct.putExtra(Constants.UNPROCESSED_CUSTOMER_SUPPORT, unprocessedCustomerSupportsJSON);
                    startActivity(unProcessedCustomerSupportAct);
                    finish();
                }
            }.execute();
        //}

    }

    //---------

    public void copyTextToClipboard(String text){
        try{
            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboardManager.setText(text);
            showToastMessage(text+" is copied to clipboard");
        }catch (Exception e){}
    }

    //==============

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ( activityReceiver != null ) {
            Log.d(Constants.LOG_TAG, "Un registering reload receiver");
            unregisterReceiver(activityReceiver);
        }
    }

}
