package com.mastaan.logistics.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.ConfigurationsAPI;
import com.mastaan.logistics.models.ReferralOfferDetails;

public class AddOrEditReferralOfferActivity extends MastaanToolbarActivity implements View.OnClickListener {

    String AMOUNT = "Amount";
    String PERCENT = "Percent";

    RadioButton active, inactive;
    vTextInputLayout start_date;
    View clearStartDate;
    vTextInputLayout end_date;
    View clearEndDate;
    vTextInputLayout receiver_minimum_order;
    vTextInputLayout receiver_discount_value_type;
    vTextInputLayout receiver_discount_value;
    vTextInputLayout receiver_maximum_discount;
    vTextInputLayout sender_minimum_order;
    vTextInputLayout sender_discount_value_type;
    vTextInputLayout sender_discount_value;
    vTextInputLayout sender_maximum_discount;

    Button actionDone;

    ReferralOfferDetails referralOfferDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_referral_offer);
        hasLoadingView();

        //---------

        active = (RadioButton) findViewById(R.id.active);
        inactive = (RadioButton) findViewById(R.id.inactive);
        start_date = (vTextInputLayout) findViewById(R.id.start_date);
        start_date.getEditText().setOnClickListener(this);
        clearStartDate = findViewById(R.id.clearStartDate);
        clearStartDate.setOnClickListener(this);
        end_date = (vTextInputLayout) findViewById(R.id.end_date);
        end_date.getEditText().setOnClickListener(this);
        clearEndDate = findViewById(R.id.clearEndDate);
        clearEndDate.setOnClickListener(this);
        receiver_minimum_order = (vTextInputLayout) findViewById(R.id.receiver_minimum_order);
        receiver_discount_value_type = (vTextInputLayout) findViewById(R.id.receiver_discount_value_type);
        receiver_discount_value_type.getEditText().setOnClickListener(this);
        receiver_discount_value = (vTextInputLayout) findViewById(R.id.receiver_discount_value);
        receiver_maximum_discount = (vTextInputLayout) findViewById(R.id.receiver_maximum_discount);
        sender_minimum_order = (vTextInputLayout) findViewById(R.id.sender_minimum_order);
        sender_discount_value_type = (vTextInputLayout) findViewById(R.id.sender_discount_value_type);
        sender_discount_value_type.getEditText().setOnClickListener(this);
        sender_discount_value = (vTextInputLayout) findViewById(R.id.sender_discount_value);
        sender_maximum_discount = (vTextInputLayout) findViewById(R.id.sender_maximum_discount);

        actionDone = (Button) findViewById(R.id.actionDone);
        actionDone.setOnClickListener(this);

        //--------

        getReferralOfferDetails();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getReferralOfferDetails();
    }

    @Override
    public void onClick(View view) {

        if ( view == start_date.getEditText() ){
            String prefillDate = start_date.getText();
            if ( prefillDate == null || prefillDate.length() == 0 ){    prefillDate = DateMethods.getCurrentDate(); }

            showDatePickerDialog("Start Date", null, end_date.getText(), prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    start_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }
        else if ( view == clearStartDate ){
            start_date.setText("");
        }

        else if ( view == end_date.getEditText() ){
            String prefillDate = end_date.getText();
            if ( prefillDate == null || prefillDate.length() == 0 ){    prefillDate = DateMethods.getCurrentDate(); }

            showDatePickerDialog("End Date", start_date.getText(), null, prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    end_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }
        else if ( view == clearEndDate ){
            end_date.setText("");
        }

        else if ( view == receiver_discount_value_type.getEditText() ){
            final String []types = new String[]{AMOUNT, PERCENT};
            showListChooserDialog("Receiver Discount Value Type", types, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    receiver_discount_value_type.setText(types[position]);
                    receiver_maximum_discount.setVisibility(types[position].equalsIgnoreCase(PERCENT)?View.VISIBLE:View.GONE);
                }
            });
        }

        else if ( view == sender_discount_value_type.getEditText() ){
            final String []types = new String[]{AMOUNT, PERCENT};
            showListChooserDialog("Sender Discount Value Type", types, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    sender_discount_value_type.setText(types[position]);
                    sender_maximum_discount.setVisibility(types[position].equalsIgnoreCase(PERCENT)?View.VISIBLE:View.GONE);
                }
            });
        }


        else if ( view == actionDone ) {
            hideSoftKeyBoard(new View[]{receiver_discount_value, receiver_maximum_discount, sender_discount_value, sender_maximum_discount});
            boolean eStatus = false;
            if (active.isChecked()) {
                eStatus = true;
            }
            String eStartDate = DateMethods.getDateInFormat(start_date.getText(), DateConstants.DD_MM_YYYY);
            String eEndDate = DateMethods.getDateInFormat(end_date.getText(), DateConstants.DD_MM_YYYY);

            String eReceiverMinimumOrder = receiver_minimum_order.getText();
            receiver_minimum_order.checkError("* Enter minimum order value");
            String eReceiverDiscountValueType = "v";
            if (receiver_discount_value_type.getText().equalsIgnoreCase(PERCENT)) {
                eReceiverDiscountValueType = "p";
            }
            receiver_discount_value_type.checkError("* Enter discount value type");
            String eReceiverDiscountValue = receiver_discount_value.getText();
            receiver_discount_value.checkError("* Enter discount");
            String eReceiverMaximumDiscount = receiver_maximum_discount.getText();
            if (receiver_maximum_discount.getVisibility() != View.VISIBLE) {
                eReceiverMaximumDiscount = "0";
            }

            String eSenderMinimumOrder = sender_minimum_order.getText();
            sender_minimum_order.checkError("* Enter minimum order value");
            String eSenderDiscountValueType = "v";
            if (sender_discount_value_type.getText().equalsIgnoreCase(PERCENT)) {
                eSenderDiscountValueType = "p";
            }
            sender_discount_value_type.checkError("* Enter discount value type");
            String eSenderDiscountValue = sender_discount_value.getText();
            sender_discount_value.checkError("* Enter discount");
            String eSenderMaximumDiscount = sender_maximum_discount.getText();
            if (sender_maximum_discount.getVisibility() != View.VISIBLE) {
                eSenderMaximumDiscount = "0";
            }

            if (eReceiverMinimumOrder.length() == 0 || eReceiverDiscountValueType.length() == 0 || eReceiverDiscountValue.length() == 0
                    || eSenderMinimumOrder.length() == 0 || eSenderDiscountValueType.length() == 0 || eSenderDiscountValue.length() == 0) {
                return;
            }
            if ( eReceiverDiscountValueType.equalsIgnoreCase("p") && (CommonMethods.parseDouble(eReceiverDiscountValue) <= 0 || CommonMethods.parseDouble(eReceiverDiscountValue) > 100) ){
                receiver_discount_value.showError("* Discount percent must between 0 and 100");
                return;
            }
            if ( eSenderDiscountValueType.equalsIgnoreCase("p") && (CommonMethods.parseDouble(eSenderDiscountValue) <= 0 || CommonMethods.parseDouble(eReceiverDiscountValue) > 100) ){
                receiver_discount_value.showError("* Discount percent must between 0 and 100");
                return;
            }

            ReferralOfferDetails referralOffer = new ReferralOfferDetails(eStatus, eStartDate, eEndDate
                    , CommonMethods.parseDouble(eReceiverMinimumOrder), eReceiverDiscountValueType, CommonMethods.parseDouble(eReceiverDiscountValue), CommonMethods.parseDouble(eReceiverMaximumDiscount)
                    , CommonMethods.parseDouble(eSenderMinimumOrder), eSenderDiscountValueType, CommonMethods.parseDouble(eSenderDiscountValue), CommonMethods.parseDouble(eSenderMaximumDiscount)
            );

            if (this.referralOfferDetails != null && this.referralOfferDetails.getID() != null && this.referralOfferDetails.getID().length() > 0) {
                updateReferralOffer(referralOffer);
            } else {
                addReferralOffer(referralOffer);
            }
        }
    }

    private void display(ReferralOfferDetails referralOfferDetails){
        if ( referralOfferDetails != null && referralOfferDetails.getID() != null && referralOfferDetails.getID().length() > 0 ){
            this.referralOfferDetails = referralOfferDetails;

            if ( referralOfferDetails.getStatus() ){
                active.setChecked(true);
            }else{  inactive.setChecked(true);  }

            start_date.setText(DateMethods.getReadableDateFromUTC(referralOfferDetails.getStartDate(), DateConstants.MMM_DD_YYYY));
            end_date.setText(DateMethods.getReadableDateFromUTC(referralOfferDetails.getExpiryDate(), DateConstants.MMM_DD_YYYY));

            receiver_minimum_order.setText(CommonMethods.getInDecimalFormat(referralOfferDetails.getReceiverMinimumOrder()));
            receiver_discount_value_type.setText(referralOfferDetails.getReceiverDiscountValueType().equalsIgnoreCase("p")?PERCENT:AMOUNT);
            receiver_discount_value.setText(CommonMethods.getInDecimalFormat(referralOfferDetails.getReceiverDiscountValue()));
            if ( referralOfferDetails.getReceiverDiscountValueType().equalsIgnoreCase("p") ){
                receiver_maximum_discount.setVisibility(View.VISIBLE);
                receiver_maximum_discount.setText(referralOfferDetails.getReceiverMaximumDiscount()>0?CommonMethods.getInDecimalFormat(referralOfferDetails.getReceiverMaximumDiscount()):"");
            }else{
                receiver_maximum_discount.setVisibility(View.GONE);
            }

            sender_minimum_order.setText(CommonMethods.getInDecimalFormat(referralOfferDetails.getSenderMinimumOrder()));
            sender_discount_value_type.setText(referralOfferDetails.getSenderDiscountValueType().equalsIgnoreCase("p")?PERCENT:AMOUNT);
            sender_discount_value.setText(CommonMethods.getInDecimalFormat(referralOfferDetails.getSenderDiscountValue()));
            if ( referralOfferDetails.getSenderDiscountValueType().equalsIgnoreCase("p") ){
                sender_maximum_discount.setVisibility(View.VISIBLE);
                sender_maximum_discount.setText(referralOfferDetails.getSenderMaximumDiscount()>0?CommonMethods.getInDecimalFormat(referralOfferDetails.getSenderMaximumDiscount()):"");
            }else{
                sender_maximum_discount.setVisibility(View.GONE);
            }
        }
        switchToContentPage();
    }

    //-----

    private void getReferralOfferDetails(){

        showLoadingIndicator("Loading, wait...");
        getBackendAPIs().getConfigurationsAPI().getReferralOfferDetails(new ConfigurationsAPI.ReferralOfferCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ReferralOfferDetails referralOfferDetails) {
                if ( status ){
                    display(referralOfferDetails);
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }
    private void addReferralOffer(ReferralOfferDetails referralOffer){

        showLoadingDialog("Adding referral offer, wait...");
        getBackendAPIs().getConfigurationsAPI().addReferralOffer(referralOffer, new ConfigurationsAPI.ReferralOfferCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ReferralOfferDetails referralOffer) {
                closeLoadingDialog();
                if ( status ){
                    referralOfferDetails = referralOffer;
                    showToastMessage("Referral offer added successfully");
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    private void updateReferralOffer(ReferralOfferDetails referralOffer){

        showLoadingDialog("Updating referral offer, wait...");
        getBackendAPIs().getConfigurationsAPI().updateReferralOffer(this.referralOfferDetails.getID(), referralOffer, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Referral offer updated successfully");
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}