package com.mastaan.logistics.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.OrientationHelper;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.roboto.RobotoLightCheckBox;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vWrappingLinearLayoutManager;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.OrdersAdapter;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.backend.models.RequestCompleteDelivery;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.ECODDialog;
import com.mastaan.logistics.dialogs.MoneyCollectionDetailsDialog;
import com.mastaan.logistics.flows.RoutesFlow;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.localdata.OrderItemsDatabase2;
import com.mastaan.logistics.models.Job;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class DispatcherActivity extends MastaanToolbarActivity implements View.OnClickListener {

    OrderItemsDatabase2 orderItemsDatabase;

    View billNotRequried;
    TextView area;
    TextView membership_type;
    TextView name;
    TextView location;
    TextView land_mark;
    TextView order_amount;
    //View discountDetailsView;
    //TextView order_discount;
    TextView time;
    View callCustomer;
    View messageCustomer;
    View showDirections;
    TextView buyer_outstanding_amount;
    View startOnlinePaymentFromCustomer;
    View checkPayment;
    View seeMoneyCollection;
    View qrCodes;

    vRecyclerView orderHolder;
    OrdersAdapter ordersAdapter;

    View confirm;
    View cancel;

    Job job;

    Location currentLocation;

    UserDetails userDetails;


    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatcher);
        hasLoadingView();
        hasSwipeRefresh();

        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(" "+actionBar.getTitle());

        String claim_details_string = getIntent().getStringExtra("job");
        job = new Gson().fromJson(claim_details_string, Job.class);

        orderItemsDatabase = getOrderItemsDatabase();
        //orderItemsDatabase.openDatabase();;

        userDetails = localStorageData.getUserDetails();

        //-------------

        billNotRequried = findViewById(R.id.billNotRequried);
        area = findViewById(R.id.area);
        membership_type = findViewById(R.id.membership_type);
        name = findViewById(R.id.name);
        location = findViewById(R.id.location);
        land_mark = findViewById(R.id.land_mark);
        order_amount = findViewById(R.id.order_amount);
        //discountDetailsView = findViewById(R.id.discountDetailsView);
        //order_discount = findViewById(R.id.order_discount);
        callCustomer = findViewById(R.id.callCustomer);
        callCustomer.setOnClickListener(this);
        messageCustomer = findViewById(R.id.messageCustomer);
        messageCustomer.setOnClickListener(this);
        showDirections = findViewById(R.id.showDirections);
        showDirections.setOnClickListener(this);
        time = findViewById(R.id.time);
        buyer_outstanding_amount = findViewById(R.id.buyer_outstanding_amount);
        startOnlinePaymentFromCustomer = findViewById(R.id.startOnlinePaymentFromCustomer);
        startOnlinePaymentFromCustomer.setOnClickListener(this);
        checkPayment = findViewById(R.id.checkPayment);
        checkPayment.setOnClickListener(this);
        seeMoneyCollection = findViewById(R.id.seeMoneyCollection);
        seeMoneyCollection.setOnClickListener(this);
        qrCodes = findViewById(R.id.qrCodes);
        qrCodes.setOnClickListener(this);

        orderHolder = findViewById(R.id.orderHolder);
        vWrappingLinearLayoutManager vWrappingLinearLayoutManager = new vWrappingLinearLayoutManager(context);
        vWrappingLinearLayoutManager.setOrientation(OrientationHelper.VERTICAL);
        orderHolder.setLayoutManager(vWrappingLinearLayoutManager);//new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        orderHolder.setHasFixedSize(false);
        orderHolder.setNestedScrollingEnabled(false);
        orderHolder.setItemAnimator(new DefaultItemAnimator());
        ordersAdapter = new OrdersAdapter(activity, new ArrayList<OrderDetails>(), null);
        ordersAdapter.setShowUserItemsFirst(true);
        ordersAdapter.setShowCustomerDetails(false);
        orderHolder.setAdapter(ordersAdapter);

        confirm = findViewById(R.id.confirm);
        confirm.setOnClickListener(this);
        cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        //--------------

        getOrderDetails();
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getOrderDetails();
    }

    private void displayDetails(OrderDetails orderDetails) {
        switchToContentPage();

        billNotRequried.setVisibility(orderDetails.isBillNotRequired()?View.VISIBLE:View.GONE);

        area.setText(orderDetails!=null?orderDetails.getDeliveryAddressSubLocalityLevel1():job.getArea());

        String membership = orderDetails.getBuyerDetails().getMembershipType();
        if ( membership.equalsIgnoreCase(Constants.BRONZE) == false ){
            membership_type.setVisibility(View.VISIBLE);
            membership_type.setText(membership);
        }else{  membership_type.setVisibility(View.GONE);    }

        name.setText(orderDetails!=null?orderDetails.getCustomerName():job.getCustomer_name());
        location.setText(orderDetails!=null?orderDetails.getDeliveryAddress():job.getAddress_line_1() + "\n" + job.getAddress_line_2());
        land_mark.setText(Html.fromHtml("<b>Landmark: </b>"+orderDetails!=null?orderDetails.getDeliveryAddressLandmark():job.getLandMark()));

        order_amount.setText(orderDetails.getFullTotalAmountSplitString());//job.getFullOrderTotalAmountSplitString());

        /*if ( job.getOrderDiscount() > 0 ){
            discountDetailsView.setVisibility(View.VISIBLE);
            order_discount.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(job.getOrderDiscount()));
        }else{
            discountDetailsView.setVisibility(View.GONE);
        }*/
        time.setText(job.getSlot());

        if ( job.getBuyerOutstanding() != 0 ){
            findViewById(R.id.buyerOutstandingDetails).setVisibility(View.VISIBLE);
            if ( job.getBuyerOutstanding() < 0 ){
                buyer_outstanding_amount.setText(SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(job.getBuyerOutstanding()) + " (Return to customer)");
            }else if  ( job.getBuyerOutstanding() > 0 ){
                buyer_outstanding_amount.setText(SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(job.getBuyerOutstanding()) + " (Collect from customer)");
            }
        }else{
            findViewById(R.id.buyerOutstandingDetails).setVisibility(View.GONE);
        }

        if ( job.getCashAmount() > 0 || job.getBuyerOutstanding() > 0 ){
            startOnlinePaymentFromCustomer.setVisibility(View.VISIBLE);
        }else{
            startOnlinePaymentFromCustomer.setVisibility(View.GONE);
        }

        //---

        //getOrderItems();

        if ( orderDetails != null ) {
            orderHolder.setVisibility(View.VISIBLE);
            List<OrderDetails> ordersList = new ArrayList<>();
            ordersList.add(orderDetails);
            ordersAdapter.setItems(ordersList);
            order_amount.setText(orderDetails.getOrderedItems().get(0).getOrderDetails().getFullTotalAmountSplitString());
            //updateOrderAmount(new OrderDetails(orderDetails.getOrderedItems().get(0).getOrderDetails()));
        }

        //-----

        // HYPERTRACK STARTING TRACKING
        checkLocationAccess(new CheckLocatoinAccessCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if ( status ) {
                    //HyperTrack.startTracking();
                }else{
                    checkLocationAccess(this);
                }
            }
        });

    }

    public void updateOrderAmount(OrderDetails orderDetails){
        if ( orderDetails != null ){
            order_amount.setText(SpecialCharacters.RS + " "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalAmount()) + " (" + orderDetails.getPaymentTypeString() + ")");
            if ( orderDetails.getTotalCashAmount() != 0 && orderDetails.getTotalOnlinePaymentAmount() != 0 ){
                order_amount.setText(SpecialCharacters.RS + " "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalAmount()) + " ["+CommonMethods.getInDecimalFormat(orderDetails.getTotalCashAmount())+"(COD), "+CommonMethods.getInDecimalFormat(orderDetails.getTotalOnlinePaymentAmount())+"(OP)"+"]");
            }
        }
    }

    public void checkPaymentStatus(){

        showLoadingDialog("Checking payment status, wait...");
        getBackendAPIs().getPaymentAPI().checkOrderPaymentStatus(job.getOrderID(), new PaymentAPI.CheckOrderPaymentStatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, OrderDetails orderDetails) {
                closeLoadingDialog();
                if (status){
                    order_amount.setText(orderDetails.getFullTotalAmountSplitString());
                    //updateOrderAmount(orderDetails);
                }else{
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while checking payment status.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                checkPaymentStatus();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onClick(final View view) {

        if (view == callCustomer) {
            final OrderDetails orderDetails = ordersAdapter.getItem(0);
            if ( orderDetails.isAlertnateMobileExists() ){
                showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                    @Override
                    public void onSelect(int nPosition) {
                        callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                    }
                });
            }else{
                callToPhoneNumber(orderDetails.getCustomerMobile());
            }
        }
        else if (view == messageCustomer ) {
            final OrderDetails orderDetails = ordersAdapter.getItem(0);
            if ( orderDetails.isAlertnateMobileExists() ){
                showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                    @Override
                    public void onSelect(int nPosition) {
                        new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                    }
                });
            }else{
                new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
            }
        }
        else if (view == showDirections) {
            getCurrentLocation(new LocationCallback() {
                @Override
                public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                    if (status) {
                        currentLocation = location;
                        Intent showDirectionsIntent = new Intent(context, DirectionsViewerDialogActivity.class);
                        showDirectionsIntent.setAction(Constants.POPUP_ACTION);
                        showDirectionsIntent.putExtra("origin", latLng);
                        showDirectionsIntent.putExtra("destination", job.getLocation());
                        showDirectionsIntent.putExtra("location", job.getCustomer_name() + "\n" + job.getArea()/*.getAddress().getDeliveryAddressLocality()*/);
                        startActivity(showDirectionsIntent);
                    } else {
                        showToastMessage(message);
                    }
                }
            });
        }
        else if ( view == startOnlinePaymentFromCustomer ){
            new ECODDialog(activity).onlyShowLinkPaymentWithPaymentScreen().showForOrder(ordersAdapter.getItem(0)/*job.getOrderID()*/, ordersAdapter.getItem(0).getTotalCashAmount(), new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message) {
                    if ( status ){
                        activity.showToastMessage("Payment success");
                        checkPaymentStatus();
                    }else{
                        activity.showToastMessage("Payment failed");
                    }
                }
            });
        }
        else if ( view == checkPayment ){
            checkPaymentStatus();
        }
        else if ( view == seeMoneyCollection ){
            new MoneyCollectionDetailsDialog(activity).showForOrder(job.getOrderID());
        }
        else if ( view == qrCodes ){
            Intent qrCodesAct = new Intent(context, QRCodesActivity.class);
            startActivity(qrCodesAct);
        }
        else if (view == confirm) {
            onCompleteDelivery();
        }
        else if (view == cancel) {
            final List<String> cancelReasons = localStorageData.getDeliveryFailureReasons();
            cancelReasons.add("Other");
            showListChooserDialog("Select reason!", cancelReasons, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( cancelReasons.get(position).equalsIgnoreCase("Other")){
                        showInputTextDialog("What's the reason!", "Reason", new TextInputCallback() {
                            @Override
                            public void onComplete(String inputText) {
                                cancelDelivery(inputText);
                            }
                        });
                    }else {
                        cancelDelivery(cancelReasons.get(position));
                    }
                }
            });
        }
    }

    private void onCompleteDelivery() {

        OrderDetails orderDetails = ordersAdapter.getItem(0);

        List<OrderItemDetails> itemsList = new ArrayList<>();
        List<OrderItemDetails> orderItemsList = orderDetails.getOrderedItems();
        for (int j=0;j<orderItemsList.size();j++){
            OrderItemDetails orderItemDetails = orderItemsList.get(j);
            if ( orderItemDetails.getStatusString().equalsIgnoreCase(Constants.DELIVERING)
                    && orderItemDetails.getDeliveryBoyID().equals(localStorageData.getUserID()) ){
                itemsList.add(orderItemDetails);
            }
        }

        final List<String> deliveredItems = new ArrayList<>();
        final List<String> rejectedItems = new ArrayList<>();
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_delivery_confirmation, null);
        LinearLayout itemsHolder = (LinearLayout) dialogView.findViewById(R.id.itemsHolder);
        for (int i=0;i<itemsList.size();i++){
            final OrderItemDetails orderItemDetails = itemsList.get(i);

            RobotoLightCheckBox checkBox = new RobotoLightCheckBox(activity);
            itemsHolder.addView(checkBox);

            checkBox.setText(Html.fromHtml("<b>"+CommonMethods.capitalizeStringWords(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+"</b>"
                    + "<br>("+orderItemDetails.getFormattedQuantity()+")<br>"
            ));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if ( isChecked ){
                        deliveredItems.add(orderItemDetails.getID());
                        rejectedItems.remove(orderItemDetails.getID());
                    }else{
                        rejectedItems.add(orderItemDetails.getID());
                        deliveredItems.remove(orderItemDetails.getID());
                    }
                }
            });

            // Default adding to UnPickedUp items
            rejectedItems.add(orderItemDetails.getID());
        }

        dialogView.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
                String confirmationMessage = "";
                if ( deliveredItems.size() > 0 && rejectedItems.size() == 0 ){
                    confirmationMessage = "<b>all the items delivered</b>";
                }else if ( rejectedItems.size() > 0 && deliveredItems.size() == 0 ){
                    confirmationMessage = "<b>all the items rejected</b>";
                }else{
                    confirmationMessage = "<b>"+deliveredItems.size()+" item"+(deliveredItems.size()!=1?"s":"")+" delivered</b> and <b>"+rejectedItems.size()+" item"+(rejectedItems.size()!=1?"s":"")+" rejected"+"</b>";
                }

                showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure "+confirmationMessage+" in this order?"), "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES") ){
                            showInputTextDialog("Type 1 to continue", "Type 1", new TextInputCallback() {
                                @Override
                                public void onComplete(String inputText) {
                                    if ( inputText.equalsIgnoreCase("1") ){
                                        checkAmountTobeCollected(deliveredItems, rejectedItems);     // MARKING AS DELIVERED
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });

        activity.showCustomDialog(dialogView);

        /*showChoiceSelectionDialog("Confirm!", Html.fromHtml("Did you deliver the <b>" + job.getItemName() + "</b> to <b>" + job.getCustomer_name() + "</b> ?"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equals("YES")) {
                    getCurrentLocation(new LocationCallback() {
                        @Override
                        public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, final Location location, LatLng latLng) {
                            if (status) {
                                currentLocation = location;
                                checkAmountTobeCollected();     // MARKING AS DELVIERED
                            } else {
                                showToastMessage(message);
                            }
                        }
                    });
                }
            }
        });*/
    }


    public void checkAmountTobeCollected(final List<String> deliveredItems, final List<String> rejectedItems){

        showLoadingDialog("Loading amount to be collected, wait...");
        getBackendAPIs().getOrdersAPI().checkAmountTobeCollected(job.getItem_id(), new OrdersAPI.CheckAmountTobeCollectedCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, boolean collectAmount, double amountToBeCollected) {
                closeLoadingDialog();
                if (status) {
                    if (collectAmount == false) {
                        completeDelivery(new RequestCompleteDelivery(deliveredItems, rejectedItems, false, 0, null));
                    } else {
                        startCollectAmountFlow(deliveredItems, rejectedItems, amountToBeCollected);
                    }
                } else {
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while getting amount to be collected.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                checkAmountTobeCollected(deliveredItems, rejectedItems);
                            }
                        }
                    });
                }
            }
        });
    }

    public void completeDelivery(final RequestCompleteDelivery requestCompleteDelivery){

        showLoadingDialog("Updating status to delivered, wait...");
        getBackendAPIs().getOrdersAPI().completeDelivery(job.getOrderID(), requestCompleteDelivery, new OrdersAPI.MarkAsDeliveredCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<String> orderItemsMarkedAsDelivered) {
                closeLoadingDialog();
                if (status) {
                    showLoadingDialog("Updating local database, wait...");
                    new AsyncTask<Void, Void, Void>() {
                        final List<OrderItemDetails> orderItemsList = new ArrayList<OrderItemDetails>();

                        @Override
                        protected Void doInBackground(Void... params) {
                            for (int i = 0; i < orderItemsMarkedAsDelivered.size(); i++) {
                                OrderItemDetails orderItemDetails = orderItemsDatabase.getOrderItem(orderItemsMarkedAsDelivered.get(i));
                                if (orderItemDetails != null) {
                                    orderItemDetails.setStatus(4);
                                    orderItemDetails.setDeliveredDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
                                    orderItemsList.add(orderItemDetails);
                                }
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            orderItemsDatabase.addOrUpdateOrderItems(orderItemsList, new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                    closeLoadingDialog();
                                    goToHome("Success", "Order status marked as delivered successfully.", true);
                                }
                            });
                        }
                    }.execute();

                    // HYPERTRACKING ENDING ACTION
                    /*if ( userDetails.isSuperUser() ) {
                        HyperTrack.completeActionWithLookupIdInSync(job.getOrderID(), new HyperTrackCallback() {
                            @Override
                            public void onSuccess(@NonNull SuccessResponse successResponse) {
                                //showToastMessage("Success");
                            }

                            @Override
                            public void onError(@NonNull ErrorResponse errorResponse) {
                                showToastMessage("Action completion failed, Msg: "+errorResponse.getErrorMessage());
                            }
                        });
                    }*/
                    //HyperTrack.stopTracking();//Commented

//                    orderItemsDatabase.deleteOrderItem(job.getItem_id(), null);
//                    if (collectAmount == false) {
//                        goToHome();
//                    } else {
//                        startCollectAmountFlow(amountToBeCollected);
//                    }
                } else {
                    showChoiceSelectionDialog("Failure", "Something went wrong while updating status to delivered.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                completeDelivery(requestCompleteDelivery);
                            }
                        }
                    });
                }
            }
        });
    }

    public void cancelDelivery(final String cancelReason){

        showLoadingDialog("Cancelling delivery, wait...");
        getBackendAPIs().getOrdersAPI().cancelDelivery(job.getItem_id(), cancelReason, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    goToHome("Success", "Delivery cancelled successfully.", false);
                } else {
                    showSnackbarMessage("Unable cancel delivery, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            cancelDelivery(cancelReason);
                        }
                    });
                }
            }
        });
    }

    public void startCollectAmountFlow(final List<String> deliveredItems, final List<String> rejectedItems, double amountToBeCollected){

        View amountCollectedDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_amount_collected, null);
        final EditText amount_collected = (EditText) amountCollectedDialogView.findViewById(R.id.amount_collected);
        TextView dialog_message = amountCollectedDialogView.findViewById(R.id.message);
        final RadioButton collectedAmount = (RadioButton) amountCollectedDialogView.findViewById(R.id.collectedAmount);
        final RadioButton returnedAmount = (RadioButton) amountCollectedDialogView.findViewById(R.id.returnedAmount);
        final CheckBox paytmWallet = (CheckBox) amountCollectedDialogView.findViewById(R.id.paytmWallet);
        final EditText paytm_transaction_id = (EditText) amountCollectedDialogView.findViewById(R.id.paytm_transaction_id);
        collectedAmount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                paytmWallet.setChecked(false);
                if ( isChecked ){
                    paytmWallet.setVisibility(View.VISIBLE);
                }else{
                    paytmWallet.setVisibility(View.GONE);
                }
            }
        });
        paytmWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if ( isChecked ){
                    paytm_transaction_id.setVisibility(View.VISIBLE);
                    Intent paytQRAct = new Intent(context, PaytmQRCodeActivity.class);
                    startActivity(paytQRAct);
                }else{
                    paytm_transaction_id.setVisibility(View.GONE);
                }
            }
        });

        //dialog_message.setText(Html.fromHtml("Order cost: <b>"+SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(job.getOrderAmount())+"</b><br>Item cost: <b>"+SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(job.getAmountOfItem())+"</b>"));
        if ( amountToBeCollected >= 0 ) {
            dialog_message.setText(Html.fromHtml("Amount to be collected: <b>" + SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(amountToBeCollected) + "</b>"));
        }else{
            dialog_message.setText(Html.fromHtml("Return to customer: <b>" + SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(amountToBeCollected) + "</b>"));
        }

        if (job.getBuyerOutstanding() < 0) {
            dialog_message.append(Html.fromHtml("<br>Outstanding: <b>" + SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(job.getBuyerOutstanding()) + " (Return to customer)</b>"));
            double finalAmount = amountToBeCollected + job.getBuyerOutstanding();
            if ( finalAmount >= 0 ){
                dialog_message.append(Html.fromHtml("<br><br>Final amount: <b>" + SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(finalAmount) + " (Collect from customer)</b>"));
            }else{
                dialog_message.append(Html.fromHtml("<br><br>Final amount: <b>" + SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(finalAmount) + " (Return to customer)</b>"));
            }

        } else if (job.getBuyerOutstanding() > 0) {
            dialog_message.append(Html.fromHtml("<br>Outstanding: <b>" + SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(job.getBuyerOutstanding()) + " (Collect from customer)</b>"));
            double finalAmount = amountToBeCollected + job.getBuyerOutstanding();
            if ( finalAmount >= 0 ){
                dialog_message.append(Html.fromHtml("<br><br>Final amount: <b>" + SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(finalAmount) + " (Collect from customer)</b>"));
            }else{
                dialog_message.append(Html.fromHtml("<br><br>Final amount: <b>" + SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(finalAmount) + " (Return to customer)</b>"));
            }
        }

        amountCollectedDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeCustomDialog();
            }
        });

        amountCollectedDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (collectedAmount.isChecked() || returnedAmount.isChecked()) {
                    String amountCollected = amount_collected.getText().toString();
                    if (amountCollected.length() > 0) {
                        if ( paytmWallet.isChecked() == false || paytm_transaction_id.getText().toString().trim().length() > 0 ){
                            closeCustomDialog();
                            double amount = Double.parseDouble(amountCollected);
                            if (returnedAmount.isChecked()) {
                                amount = -1 * amount;
                            }
                            RequestCompleteDelivery requestCompleteDelivery = new RequestCompleteDelivery(deliveredItems, rejectedItems, true, amount, null);
                            if ( paytmWallet.isChecked() ){
                                requestCompleteDelivery.setPaymentType(9).setPaymentID(paytm_transaction_id.getText().toString());
                            }
                            completeDelivery(requestCompleteDelivery);
                        }else{
                            showToastMessage("* Please enter paytm transaction id");
                        }
                    } else {
                        showToastMessage("* Please enter amount collected.");
                    }
                } else {
                    showToastMessage("* Please select collection type.");
                }

            }
        });
        showCustomDialog(amountCollectedDialogView, false);
    }

    /*public void updateAmountCollected(final String amountCollected){
        showLoadingDialog("Updating amount collected, wait...");
        getBackendAPIs().getOrdersAPI().updateOrderAmountCollected(job.getItem_id(), amountCollected, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    goToHome("Success!", "Order status marked as Delivered successfully.");
                } else {
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating amount collected.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equals("YES")) {
                                updateAmountCollected(amountCollected);
                            }
                        }
                    });
                }
            }
        });
    }*/

    public void getOrderDetails(){

        showLoadingIndicator("Loading order details, wait...");
        findViewById(R.id.list_loading_indicator).setVisibility(View.VISIBLE);
        findViewById(R.id.list_reload_indicator).setVisibility(View.GONE);
        orderHolder.setVisibility(View.GONE);
        getBackendAPIs().getOrdersAPI().getOrderItems(job.getOrderID(), new OrdersAPI.OrderDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<OrderItemDetails> orderItemsList) {
                findViewById(R.id.list_loading_indicator).setVisibility(View.GONE);
                if (status) {
                    displayDetails(ordersList!=null&&ordersList.size()>0?ordersList.get(0):null);
                } else {
                    showReloadIndicator(statusCode, "Something went wrong while loading order details, try again!");
                    findViewById(R.id.list_reload_indicator).setVisibility(View.VISIBLE);
                    findViewById(R.id.list_reload_indicator).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getOrderDetails();
                        }
                    });
                }
            }
        });
    }

    //====================

    private void goToHome(String title, String message, final boolean checkHousingSociety) {
        showNotificationDialog(false, title, message, new ActionCallback() {
            @Override
            public void onAction() {
                try{
                    if ( checkHousingSociety && ordersAdapter.getItemCount() > 0 && (ordersAdapter.getItem(0).getHousingSociety() == null || ordersAdapter.getItem(0).getHousingSociety().trim().length() == 0) ){
                        new RoutesFlow(activity).assignOrChangeHousingSociety(ordersAdapter.getItem(0).getID(), ordersAdapter.getItem(0).getDeliveryAddressLatLngString(), false, new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                continueToHomePage();
                            }
                        });
                    }else{
                        continueToHomePage();
                    }
                }catch (Exception e){
                    continueToHomePage();
                }
            }

            private void continueToHomePage(){
                Intent homeAct = new Intent(context, HomeActivity.class);
                homeAct.putExtra(Constants.SHOW_WHERE_TO_NEXT_FLOW, true);
                homeAct.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                homeAct.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeAct);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
    }
}
