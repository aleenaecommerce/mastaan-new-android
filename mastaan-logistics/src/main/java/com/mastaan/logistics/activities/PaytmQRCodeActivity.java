package com.mastaan.logistics.activities;

import android.os.Bundle;
import android.view.MenuItem;

import com.mastaan.logistics.R;


public class PaytmQRCodeActivity extends MastaanToolbarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paytm_qr_code);
    }

    //=========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
