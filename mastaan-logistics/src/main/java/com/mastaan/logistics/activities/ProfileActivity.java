package com.mastaan.logistics.activities;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aleena.common.widgets.vCircularImageView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.DayTasksAdapter;
import com.mastaan.logistics.backend.ProfileAPI;
import com.mastaan.logistics.models.DayTask;
import com.mastaan.logistics.models.UserDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ProfileActivity extends MastaanToolbarActivity implements View.OnClickListener {
    TextView user_name, join_date, address;
    TextView number_of_pickups, number_of_deliveries, number_of_retrievals;
    TextView employee_id, email, user_phone, alternative_phone, licence_number, blood_group, esi;
    TextView vehicle_reg_num, /*ownership,*/
            insurance_provider, insurance_number;
    vCircularImageView image;
    ProgressBar image_loading;
    AlertDialog dialog;
    AlertDialog.Builder builder;
    View dialog_show_details;
    View vehicle_details;
    ListView taskList;
    TextView dialog_title;

    ArrayList<DayTask> pickups;
    ArrayList<DayTask> deliveries;
    ArrayList<DayTask> retrievals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        hasLoadingView();

        //-------

        user_name = (TextView) findViewById(R.id.user_name);
        join_date = (TextView) findViewById(R.id.join_date);
        address = (TextView) findViewById(R.id.address);

        number_of_pickups = (TextView) findViewById(R.id.number_of_pickups);
        number_of_deliveries = (TextView) findViewById(R.id.number_of_deliveries);
        number_of_retrievals = (TextView) findViewById(R.id.number_of_retrievals);

        employee_id = (TextView) findViewById(R.id.employee_id);
        email = (TextView) findViewById(R.id.email);
        user_phone = (TextView) findViewById(R.id.user_phone);
        alternative_phone = (TextView) findViewById(R.id.alternative_phone);
        licence_number = (TextView) findViewById(R.id.licence_number);
        blood_group = (TextView) findViewById(R.id.blood_group);
        esi = (TextView) findViewById(R.id.esi);

        vehicle_details = findViewById(R.id.vehicle_details);
        vehicle_reg_num = (TextView) findViewById(R.id.vehicle_reg_num);
        //ownership = (TextView) findViewById(R.id.ownership);
        insurance_provider = (TextView) findViewById(R.id.insurance_provider);
        insurance_number = (TextView) findViewById(R.id.insurance_number);

        image = (vCircularImageView) findViewById(R.id.image);
        image_loading = (ProgressBar) findViewById(R.id.image_loading);
        image_loading.setVisibility(View.GONE);

        findViewById(R.id.pickups).setOnClickListener(this);
        findViewById(R.id.deliveries).setOnClickListener(this);
        findViewById(R.id.retrievals).setOnClickListener(this);

        builder = new AlertDialog.Builder(context);
        dialog_show_details = LayoutInflater.from(this).inflate(R.layout.dialog_tasks, null);
        taskList = (ListView) dialog_show_details.findViewById(R.id.list_view);
        dialog_title = (TextView) dialog_show_details.findViewById(R.id.title);

        //-------------

        getProfileDetails();

    }

    @Override
    public void onClick(View view) {
        ViewGroup viewGroup;
        switch (view.getId()) {
            case R.id.pickups:
                viewGroup = (ViewGroup) dialog_show_details.getParent();
                if (viewGroup != null) viewGroup.removeView(dialog_show_details);
                showPickups();
                break;
            case R.id.deliveries:
                viewGroup = (ViewGroup) dialog_show_details.getParent();
                if (viewGroup != null) viewGroup.removeView(dialog_show_details);
                showDeliveries();
                break;
            case R.id.retrievals:
                viewGroup = (ViewGroup) dialog_show_details.getParent();
                if (viewGroup != null) viewGroup.removeView(dialog_show_details);
                showRetrievals();
                break;
        }
    }

    private void displayDetails(UserDetails userDetails) {

        switchToContentPage();

        String name = userDetails.getFirstName();
        if (name != null) {
            if (userDetails.getL() != null) {
                name = name + " " + userDetails.getL();
            }
            user_name.setText(name);
        }
        join_date.setText("Joined on " + userDetails.getFormattedJoinDate());
        address.setText(userDetails.getA1() + "\n" + userDetails.getA2() + ", " + userDetails.getAr() + ",\n" + userDetails.getCi() + ", " + userDetails.getS() + ", " + userDetails.getPin());
        employee_id.setText(userDetails.getEid());
        email.setText(userDetails.getEmail());
        user_phone.setText(userDetails.getMobileNumber());
        alternative_phone.setText(userDetails.getEm());
        blood_group.setText(userDetails.getBg());
        esi.setText(userDetails.getEsi());
        licence_number.setText(userDetails.getDl());
        if (userDetails.getVd() != null) {
            if (userDetails.getVd().getOActual().equals("so")) {
                vehicle_details.setVisibility(View.VISIBLE);

                vehicle_reg_num.setText(userDetails.getVd().getRn());
//        ownership.setText(userDetails.getVd().getO());
                insurance_provider.setText(userDetails.getVd().getInsuranceProvider());
                insurance_number.setText(userDetails.getVd().getInsuranceNumber());
            } else {
                vehicle_details.setVisibility(View.INVISIBLE);
            }
        } else {
            vehicle_details.setVisibility(View.INVISIBLE);
        }
        pickups = new ArrayList<>();
        deliveries = new ArrayList<>();
        retrievals = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DayTask task = new DayTask("7/11/2015", 10);
            deliveries.add(task);
            pickups.add(task);
            retrievals.add(task);
        }
        Picasso.get().load(userDetails.getProfilePic()).placeholder(R.drawable.ic_account_circle_grey).error(R.drawable.ic_account_circle_grey).into(image);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getProfileDetails();
    }

    public void getProfileDetails(){

        showLoadingIndicator("Loading profile, wati...");
        getBackendAPIs().getProfileAPI().getProfile(new ProfileAPI.ProfileCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, UserDetails userDetails) {
                if (status) {
                    displayDetails(userDetails);
                } else {
                    showReloadIndicator(statusCode, "Something went wrong while loading your profile, try again!");
                }
            }
        });
    }

    //------

    private void showRetrievals() {
        dialog_title.setText("Retrievals");
        taskList.setAdapter(new DayTasksAdapter(this, R.layout.view_day_task, retrievals));
        builder.setView(dialog_show_details);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    private void showDeliveries() {
        dialog_title.setText("Deliveries");
        taskList.setAdapter(new DayTasksAdapter(this, R.layout.view_day_task, deliveries));
        builder.setView(dialog_show_details);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    private void showPickups() {
        dialog_title.setText("Pickups");
        taskList.setAdapter(new DayTasksAdapter(this, R.layout.view_day_task, pickups));
        builder.setView(dialog_show_details);
        builder.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    //==================

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
