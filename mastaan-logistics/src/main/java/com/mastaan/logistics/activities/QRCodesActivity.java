package com.mastaan.logistics.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.QRCodesAdapter;

/**
 * Created by Venkatesh Uppu on 17/06/18.
 */

public class QRCodesActivity extends MastaanToolbarActivity {

    vRecyclerView itemsHolder;
    QRCodesAdapter qrCodesAdapter;
    WebView qrwebview1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_codes);
        hasLoadingView();
        hasSwipeRefresh();


        qrwebview1 = (WebView) findViewById(R.id.qrwebview);
        qrwebview1.getSettings().setJavaScriptEnabled (true);
        qrwebview1.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        qrwebview1.getSettings().setSupportMultipleWindows (false);
        qrwebview1.getSettings().setSupportZoom (false);
        qrwebview1.setVerticalScrollBarEnabled (false);
        qrwebview1.setHorizontalScrollBarEnabled (false);
        qrwebview1.getSettings().setLoadWithOverviewMode(true);
        qrwebview1.getSettings().setUseWideViewPort(true);
        qrwebview1.getSettings().getTextZoom();
        qrwebview1.loadUrl("https://www.mastaan.com/qr/QR.png");


        //--------

//        List<QRCodeDetails> qrCodes = new ArrayList<>();
//        qrCodes.add(new QRCodeDetails("Paytm", R.drawable.paytm_qr_code));
//        qrCodes.add(new QRCodeDetails("Whatsapp", R.drawable.whatsapp_qr_code));
//        qrCodes.add(new QRCodeDetails("Phonepe", R.drawable.phonepe_qr_code));
//        qrCodes.add(new QRCodeDetails("Tez", R.drawable.tez_qr_code));
//
//        itemsHolder = (vRecyclerView) findViewById(R.id.itemsHolder);
//        itemsHolder.setupVerticalOrientation();
//        qrCodesAdapter = new QRCodesAdapter(activity, qrCodes, new QRCodesAdapter.CallBack() {
//            @Override
//            public void onItemClick(int position) {
//
//            }
//        });
//        itemsHolder.setAdapter(qrCodesAdapter);
    }

    //==============

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
