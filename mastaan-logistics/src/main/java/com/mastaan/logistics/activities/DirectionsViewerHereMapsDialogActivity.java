package com.mastaan.logistics.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.location.GoogleRetrofitInterface;
import com.aleena.common.location.model.DirectionsData;
import com.aleena.common.methods.CommonMethods;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.mastaan.logistics.R;
import com.mastaan.logistics.handlers.GpsTracker;

import retrofit.RestAdapter;

public class DirectionsViewerHereMapsDialogActivity extends MastaanToolbarActivity {
    RestAdapter restAdapter;
    GoogleRetrofitInterface googleRetrofitInterface;
    GooglePlacesAPI googlePlacesAPI;
    //vMapFragment mapFragment;
    //GoogleMap googleMap;
    LatLng origin;
    LatLng destination;
    TextView loading_message;
    String location;
    Marker sourceMarker, destinationMarker;
    double longtitude;
    double latitude;
    private Map m_map;
    private AndroidXMapFragment m_mapFragment;
    private GpsTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directions_heremaps_viewer);

        loading_message = findViewById(R.id.loading_message);
        loading_message.setText("Getting directions, wait...");
        origin = getIntent().getParcelableExtra("origin");

        gpsTracker = new GpsTracker(getApplicationContext());
        latitude = gpsTracker.getLatitude();
        longtitude = gpsTracker.getLongitude();
        destination = getIntent().getParcelableExtra("destination");
        location = getIntent().getStringExtra("location");

        if (CommonMethods.isPackageInstalled(context, "com.google.android.apps.maps")) {
            findViewById(R.id.showInGoogleMaps).setVisibility(View.VISIBLE);
            findViewById(R.id.showInGoogleMaps).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + destination.latitude + "," + destination.longitude);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            });
        }

        m_mapFragment = getMapFragment();


        if (m_mapFragment != null) {
            /* Initialize the AndroidXMapFragment, results will be given via the called back. */
            m_mapFragment.init(new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {

                    if (error == Error.NONE) {
                        /* get the map object */
                        m_map = m_mapFragment.getMap();

                        m_mapFragment.getPositionIndicator().setVisible(true);


                        /*
                         * Set the map center to the south of Berlin.
                         */

                        gpsTracker = new GpsTracker(getApplicationContext());
                        latitude = gpsTracker.getLatitude();
                        longtitude = gpsTracker.getLongitude();
                        if (gpsTracker.canGetLocation()) {

                            // place_info.setText("Lat :" + latitude + "Lng :" + longitude);
                            Log.e("location", "" + latitude + longtitude);
                        } else {
                            gpsTracker.showSettingsAlert();
                        }

                        //  createRoute(Collections.<RoutingZone>emptyList());

                    } else {
                        new AlertDialog.Builder(DirectionsViewerHereMapsDialogActivity.this).setMessage(
                                "Error : " + error.name() + "\n\n" + error.getDetails())
                                .setTitle("error")
                                .setNegativeButton(android.R.string.cancel,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                finish();
                                            }
                                        }).create().show();
                    }
                }
            });
        }

//        mapFragment = ((vMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
//        mapFragment.getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(GoogleMap google_Map) {
//                //googleMap = mapFragment.getMap();
//                googleMap = google_Map;
//
//                googleMap.setMyLocationEnabled(true);
//                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
//                sourceMarker = googleMap.addMarker(new MarkerOptions().title("You").position(origin)
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
//                destinationMarker = googleMap.addMarker(new MarkerOptions().title(location).position(destination)
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
//                restAdapter = new RestAdapter.Builder().setEndpoint("https://maps.googleapis.com/maps/api").setLogLevel(RestAdapter.LogLevel.FULL).build();
//                googleRetrofitInterface = restAdapter.create(GoogleRetrofitInterface.class);
//                googlePlacesAPI = new GooglePlacesAPI(context.getResources().getString(R.string.google_apikey), googleRetrofitInterface);
//                googlePlacesAPI.getDirections(origin, destination, new GooglePlacesAPI.DirectionsCallback() {
//                    @Override
//                    public void onComplete(@Nullable List<DirectionsData.Route.Leg> legs, int status_code) {
//                        if (legs != null && legs.size() > 0) {
//                            updateLegDetails(legs.get(0));
//                            drawRoad(legs.get(0).steps, Color.rgb(127, 0, 255));
//                            findViewById(R.id.loading_layout).setVisibility(View.GONE);
//                        } else if (status_code == 1000) {
//                            showMessageLong("No directions to show");
//                            findViewById(R.id.loading_layout).setVisibility(View.GONE);
//                        } else {
//                            showMessageLong("Error While getting Directions");
//                            finish();
//                        }
//                    }
//                });
//            }
//        });

    }

    private void updateLegDetails(DirectionsData.Route.Leg leg) {
        boolean needLegDetails = getIntent().getBooleanExtra("needLegDetails", false);
        if (!needLegDetails)
            findViewById(R.id.leg_details).setVisibility(View.GONE);
        else {
            findViewById(R.id.leg_details).setVisibility(View.VISIBLE);
            TextView start_address = findViewById(R.id.start_address);
            TextView end_address = findViewById(R.id.end_address);
            start_address.setText(leg.start_address);
            end_address.setText(leg.end_address);
        }
    }


    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }

//    private void drawRoad(List<DirectionsData.Route.Leg.Step> steps, int color) {
//        PolylineOptions lineOptions = new PolylineOptions();
//        final LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
//        boundsBuilder.include(origin);
//        boundsBuilder.include(destination);
//        for (DirectionsData.Route.Leg.Step step : steps) {
//            LatLng nextStep = new LatLng(step.start_location.lat, step.start_location.lng);
//            lineOptions.add(nextStep);
//            boundsBuilder.include(nextStep);
//            try {
//                lineOptions.addAll(step.polyline.getPolyLinePoints());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            lineOptions.add(new LatLng(step.end_location.lat, step.end_location.lng));
//        }
//        final GoogleMap.CancelableCallback cancelableCallback = new GoogleMap.CancelableCallback() {
//            @Override
//            public void onFinish() {
//                findViewById(R.id.loading_layout).setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onCancel() {
//                googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 18));
//                findViewById(R.id.loading_layout).setVisibility(View.GONE);
//            }
//        };
//        try {
//            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 18), cancelableCallback);
//            lineOptions.color(color);
//            googleMap.addPolyline(lineOptions);
//        }catch (Exception e){}
//    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
