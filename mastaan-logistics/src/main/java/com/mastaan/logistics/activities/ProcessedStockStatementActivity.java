package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.SearchView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.ProcessedStocksAdapter;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.dialogs.AddOrEditProcessedStockDialog;
import com.mastaan.logistics.fragments.FilterItemsFragment;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.ProcessedStockDetails;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 29/07/18.
 */

public class ProcessedStockStatementActivity extends MastaanToolbarActivity implements View.OnClickListener{

    MastaanToolbarActivity activity;
    UserDetails userDetails;

    DrawerLayout drawerLayout;
    FilterItemsFragment filterFragment;

    MenuItem searchViewMenuITem;
    SearchView searchView;
    MenuItem filtereMenuItem;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    TextView showing_date;
    View changeDate;
    TextView summary_info;
    vRecyclerView stocksHolder;
    ProcessedStocksAdapter stocksAdapter;

    String showingMonth = "";
    List<ProcessedStockDetails> showingStocksList = new ArrayList<>();

    SearchDetails searchDetails = new SearchDetails();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_processed_stock_statement);
        hasLoadingView();
        hasSwipeRefresh();
        activity = this;
        userDetails = localStorageData.getUserDetails();



        /*filterFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    clearSearch();
                }
                else if (searchDetails.getAction().equalsIgnoreCase(Constants.ANALYSE)) {
                    //new AnalyseOrderItemsDialog(activity, showingStocksList).show();
                    showToastMessage("Under construction / Not yet done");
                }
                else {
                    performSearch(searchDetails);
                }
            }
        });*/

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        //---------

        showing_date = (TextView) findViewById(R.id.showing_date);
        changeDate = findViewById(R.id.changeDate);
        changeDate.setOnClickListener(this);
        summary_info = (TextView) findViewById(R.id.summary_info);
        stocksHolder = (vRecyclerView) findViewById(R.id.stocksHolder);
        setupHolder();

        //---------

        showNoDataIndicator("Select month");

        changeDate.performClick();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getStockStatement(showingMonth);
    }

    @Override
    public void onClick(View view) {
        if ( view == changeDate ){
            String prefillDate = showingMonth;
            if ( prefillDate == null || prefillDate.length() == 0 ){
                prefillDate = DateMethods.getOnlyDate(localStorageData.getServerTime());
            }
            showDatePickerDialog("Select month", null, DateMethods.getOnlyDate(localStorageData.getServerTime()), prefillDate, true, false, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    showing_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_YYYY));
                    getStockStatement(fullDate);
                }
            });
        }
        else if ( view == clearFilter ){
            try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
            clearSearch();
        }
    }

    public void setupHolder(){
        stocksHolder.setupVerticalOrientation();
        stocksAdapter = new ProcessedStocksAdapter(activity, new ArrayList<ProcessedStockDetails>(), new ProcessedStocksAdapter.Callback() {
            @Override
            public void onItemClick(final int position) {
                final List<String> listOptions = new ArrayList<>();
                if ( userDetails.isSuperUser() || stocksAdapter.getItem(position).getCreatedBy().getID().equalsIgnoreCase(userDetails.getID()) ){
                    listOptions.add("Edit");
                }
                if ( userDetails.isSuperUser() ){
                    listOptions.add("Delete");
                }
                if ( listOptions.size() > 0 ){
                    activity.showListChooserDialog(listOptions, new ListChooserCallback() {
                        @Override
                        public void onSelect(int selectedOptionPosition) {
                            if ( listOptions.get(selectedOptionPosition).equalsIgnoreCase("edit") ){
                                new AddOrEditProcessedStockDialog(activity, stocksAdapter.getItem(position), new AddOrEditProcessedStockDialog.Callback() {
                                    @Override
                                    public void onComplete(boolean status, ProcessedStockDetails processedStockDetails) {
                                        if ( status ){
                                            stocksAdapter.setItem(position, processedStockDetails);
                                        }
                                    }
                                }).show();
                            }
                            else if ( listOptions.get(selectedOptionPosition).equalsIgnoreCase("delete") ){
                                showChoiceSelectionDialog("Confirm!", "Are you sure to delete processed stock?", "CANCEL", "YES", new ChoiceSelectionCallback() {
                                    @Override
                                    public void onSelect(int choiceNo, String choiceName) {
                                        if ( choiceName.equalsIgnoreCase("YES")){
                                            deleteStock(position);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
        stocksHolder.setAdapter(stocksAdapter);
    }

    public void deleteStock(final int position){

        showLoadingDialog("Deleting processed stock, wait...");
        getBackendAPIs().getStockAPI().deleteProcessedStock(stocksAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    stocksAdapter.deleteItem(position);
                    showSnackbarMessage("Processed stock deleted successfully");
                }else{
                    showSnackbarMessage("Unable to delete processed stock, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            deleteStock(position);
                        }
                    });
                }
            }
        });
    }
    //----

    public void getStockStatement(final String forMonth){

        changeDate.setVisibility(View.GONE);
        summary_info.setVisibility(View.GONE);
        searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        hideMenuItems();
        showLoadingIndicator("Loading processed stock statement for "+ DateMethods.getDateInFormat(forMonth, DateConstants.MMM_YYYY)+", wait...");
        getBackendAPIs().getStockAPI().getProcessedStockStatement(forMonth, new StockAPI.ProcessedStockStatementCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String loadedMonth, List<ProcessedStockDetails> stocksList) {
                if ( forMonth.equalsIgnoreCase(loadedMonth) ){
                    changeDate.setVisibility(View.VISIBLE);
                    if (status) {
                        displayItems(forMonth, stocksList);
                    } else {
                        showReloadIndicator(statusCode, "Unable to load processed stock statment for "+DateMethods.getDateInFormat(forMonth, DateConstants.MMM_YYYY)+", try again!");
                    }
                }
            }
        });
    }

    public void displayItems(String date, final List<ProcessedStockDetails> stocksList) {
        showingMonth = date;
        showingStocksList = stocksList;

        if (stocksList.size() > 0) {
            showMenuItems();
            stocksAdapter.setItems(stocksList);
            switchToContentPage();
        } else {
            showNoDataIndicator("Nothing to show");
        }
    }


    //----------------

    public void performSearch(final SearchDetails search_details){
        this.searchDetails = search_details;

        if ( isShowingLoadingPage() == false && showingStocksList != null && showingStocksList.size() > 0 ) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
                SearchMethods.searchProcessedStocks(showingStocksList, searchDetails, new SearchMethods.SearchProcessedStocksCallback() {
                    @Override
                    public void onComplete(boolean status, SearchDetails searchDetails, List<ProcessedStockDetails> filteredList) {
                        if (status && searchDetails == search_details ) {
                            if (filteredList.size() > 0) {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText(filteredList.size() + " result(s) found.\n(" + searchDetails.getSearchString() + ")");

                                stocksAdapter.setItems(filteredList);
                                stocksHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText("No Results");
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");
                                stocksAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                stocksAdapter.setItems(showingStocksList);
            }
        }
    }

    public void clearSearch(){
        this.searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( showingStocksList.size() > 0 ) {
            stocksAdapter.setItems(showingStocksList);
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }
        //filterFragment.clearSelection();
    }

    //---------

    public void hideMenuItems(){
        try {
            //filtereMenuItem.setVisible(false);
            searchViewMenuITem.setVisible(false);searchViewMenuITem.collapseActionView();
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    public void showMenuItems(){
        try {
            //filtereMenuItem.setVisible(true);
            searchViewMenuITem.setVisible(true);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_stock_statement, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filtereMenuItem = menu.findItem(R.id.action_filter);
        filtereMenuItem.setVisible(false);      // TEMPORARILY HIDING

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchDetails searchDetails = new SearchDetails().setConsiderAtleastOneMatch(query).setStockType(query);
                performSearch(searchDetails);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){
        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            super.onBackPressed();
        }
    }

}
