package com.mastaan.logistics.activities;

import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.View;

import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;

import java.util.List;

public class CashNCarryActivity extends BaseOrderItemsActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();

        startAutoRefreshTimer(10 * 60);         // 10 Minutes Interval

        //--------

        getAllCashNCarryItems(false);
    }

    @Override
    public void onAutoRefreshTimeElapsed() {
        super.onAutoRefreshTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onInactiveTimeElapsed() {
        super.onInactiveTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onReloadPage() {
        super.onReloadPage();
        getAllCashNCarryItems(false);
    }

    @Override
    public void onBackgroundReloadPressed() {
        super.onBackgroundReloadPressed();
        getAllCashNCarryItems(true);
    }

    private void getAllCashNCarryItems(final boolean loadInBackground) {

        if ( loadInBackground ){
            loadingIndicator.setVisibility(View.VISIBLE);
            reloadIndicator.setVisibility(View.GONE);
        }else {
            showLoadingIndicator("Loading cash n carry items, wait...");
        }
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        getBackendAPIs().getOrdersAPI().getAllCashNCarryItems(localStorageData.getWarehouseLocation(), new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
                loadingIndicator.setVisibility(View.GONE);
                if (status) {
                    displayItems(true, groupedOrderItemsLists, ordersList);
                } else {
                    if (loadInBackground) {
                        reloadIndicator.setVisibility(View.VISIBLE);
                        loadingIndicator.setVisibility(View.GONE);
                    } else {
                        showReloadIndicator("Unable to load cash n carry items, try again!");
                    }
                }
            }
        });
    }

}