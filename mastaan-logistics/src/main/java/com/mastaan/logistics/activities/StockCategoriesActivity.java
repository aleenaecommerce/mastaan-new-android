package com.mastaan.logistics.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.CategoriesAdapter;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.CategoryDetails;

import java.util.ArrayList;
import java.util.List;

public class StockCategoriesActivity extends MastaanToolbarActivity {

    vRecyclerView categoriesHolder;
    CategoriesAdapter categoriesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        hasLoadingView();
        hasSwipeRefresh();

        //--------

        categoriesHolder = findViewById(R.id.categoriesHolder);
        setupHolder();

        //--------

        getCategories();

    }

    public void setupHolder(){

        categoriesHolder.setHasFixedSize(true);
        categoriesHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        categoriesHolder.setItemAnimator(new DefaultItemAnimator());

        categoriesAdapter = new CategoriesAdapter(activity, new ArrayList<CategoryDetails>(), new CategoriesAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                if ( getCallingActivity() != null ){
                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.CATEGORY_DETAILS, new Gson().toJson(categoriesAdapter.getItem(position)));
                    setResult(Activity.RESULT_OK, resultData);
                    finish();
                }
            }

            @Override
            public void onChangeAvailabilityClick(final int position, final boolean isAvailable) {}

            @Override
            public void onChangeVisibilityClick(int position, boolean isVisible) {}

            @Override
            public void onManageSlotsClick(final int position) {}
        });
        categoriesAdapter.hideActionButtons();
        categoriesHolder.setAdapter(categoriesAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getCategories();
    }


    //----------

    public void getCategories(){

        showLoadingIndicator("Loading categories, wait...");
        getBackendAPIs().getStockAPI().getCategoriesForStockManagement(new StockAPI.CategoriesListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<CategoryDetails> categoriesList) {
                if ( status ){
                    if ( categoriesList.size() > 0 ) {
                        for (int i=0;i<categoriesList.size();i++){
                            if ( categoriesList.get(i) != null ){
                                categoriesList.get(i).setVisibility(true);  // TEMP
                            }
                        }
                        categoriesAdapter.setItems(categoriesList);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("No categories");
                    }
                }else{
                    showReloadIndicator(statusCode, "Unable to load categories, try again!");
                }
            }
        });
    }

    //==============

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
