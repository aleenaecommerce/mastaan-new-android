package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.ConfigurationsAPI;
import com.mastaan.logistics.models.ConfigurationDetails;
import com.mastaan.logistics.models.UserDetails;


public class ConfigurationsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    View warehouses;
    View alertMessages;
    View referralOffer;
    View buyerMemberships;

    TextView maximum_serviceable_distance;
    View changeMaximumServiceableDistance;
    TextView threshold_distance;
    View changeThresholdDistance;
    TextView pending_followups_last_order_days;
    View changePendingFollowupsLastOrderDays;

    ConfigurationDetails configurationDetails;

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurations);
        hasLoadingView();

        userDetails = localStorageData.getUserDetails();

        if ( userDetails.isSuperUser() == false ){
            showToastMessage("You are not allowed to access this, please contact super user");
            onBackPressed();
            return;
        }

        //-----------------

        warehouses = findViewById(R.id.warehouses);
        warehouses.setOnClickListener(this);
        alertMessages = findViewById(R.id.alertMessages);
        alertMessages.setOnClickListener(this);
        referralOffer = findViewById(R.id.referralOffer);
        referralOffer.setOnClickListener(this);
        buyerMemberships = findViewById(R.id.buyerMemberships);
        buyerMemberships.setOnClickListener(this);

        maximum_serviceable_distance = findViewById(R.id.maximum_serviceable_distance);
        changeMaximumServiceableDistance = findViewById(R.id.changeMaximumServiceableDistance);
        changeMaximumServiceableDistance.setOnClickListener(this);
        threshold_distance = findViewById(R.id.threshold_distance);
        changeThresholdDistance = findViewById(R.id.changeThresholdDistance);
        changeThresholdDistance.setOnClickListener(this);
        pending_followups_last_order_days = findViewById(R.id.pending_followups_last_order_days);
        changePendingFollowupsLastOrderDays = findViewById(R.id.changePendingFollowupsLastOrderDays);
        changePendingFollowupsLastOrderDays.setOnClickListener(this);

        //------

        getConfiguration();

    }

    @Override
    public void onClick(View view) {

        if ( view == warehouses ){
            Intent warehousesAct = new Intent(context, WarehousesActivity.class);
            startActivity(warehousesAct);
        }

        else if ( view == alertMessages ){
            Intent alertMessagesAct = new Intent(context, AlertMessagesActivity.class);
            startActivity(alertMessagesAct);
        }

        else if ( view == referralOffer ){
            Intent referralOfferAct = new Intent(context, AddOrEditReferralOfferActivity.class);
            startActivity(referralOfferAct);
        }

        else if ( view == buyerMemberships ){
            Intent buyerMembershipsAct = new Intent(context, MembershipsActivity.class);
            startActivity(buyerMembershipsAct);
        }



        else if ( view == changeMaximumServiceableDistance ){
            showInputNumberDialog("Maximum Serviceable Distance", "Distance (in km)", configurationDetails!=null?configurationDetails.getMaximumServiceableDistance()+"":"", new TextInputCallback() {
                @Override
                public void onComplete(final String distance) {
                    if ( CommonMethods.parseInteger(distance) <= 0 ){
                        showToastMessage("* Distance must be greater than 0");
                        return;
                    }
                    updateConfiguration(new ConfigurationDetails().setMaximumServiceableDistance(CommonMethods.parseInteger(distance)), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if ( status ){
                                maximum_serviceable_distance.setText(distance+" km");
                            }
                        }
                    });
                }
            });
        }

        else if ( view == changeThresholdDistance ){
            showInputNumberDialog("Threshold Distance", "Distance (in km)", configurationDetails!=null?configurationDetails.getThresholdDistance()+"":"", new TextInputCallback() {
                @Override
                public void onComplete(final String distance) {
                    if ( CommonMethods.parseInteger(distance) <= 0 ){
                        showToastMessage("* Distance must be greater than 0");
                        return;
                    }

                    updateConfiguration(new ConfigurationDetails().setThresholdDistance(CommonMethods.parseInteger(distance)), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if ( status ){
                                threshold_distance.setText(distance+" km");
                            }
                        }
                    });
                }
            });
        }

        else if ( view == changePendingFollowupsLastOrderDays ){
            showInputNumberDialog("Pending Followups Last Order Days", "Days", configurationDetails!=null?configurationDetails.getPendingFollowupsLastOrderDays()+"":"", new TextInputCallback() {
                @Override
                public void onComplete(final String days) {
                    if ( CommonMethods.parseInteger(days) <= 0 ){
                        showToastMessage("* Days must be greater than 0");
                        return;
                    }

                    updateConfiguration(new ConfigurationDetails().setPendingFollowupsLastOrderDays(CommonMethods.parseInteger(days)), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            if ( status ){
                                pending_followups_last_order_days.setText(days+" days");
                            }
                        }
                    });
                }
            });
        }
    }


    //-----------

    public void getConfiguration(){

        showLoadingIndicator("Loading configuration, wait...");
        getBackendAPIs().getConfigurationsAPI().getConfiguration(new ConfigurationsAPI.ConfigurationCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, ConfigurationDetails configuration) {
                switchToContentPage();
                if ( status ){
                    configurationDetails = configuration;

                    maximum_serviceable_distance.setText(CommonMethods.getIndianFormatNumber(configurationDetails.getMaximumServiceableDistance())+" km");
                    threshold_distance.setText(CommonMethods.getIndianFormatNumber(configurationDetails.getThresholdDistance())+" km");
                    pending_followups_last_order_days.setText(configurationDetails.getPendingFollowupsLastOrderDays()+" days");
                }
                else{
                    maximum_serviceable_distance.setText("ERRROR");
                    threshold_distance.setText("ERRROR");
                    pending_followups_last_order_days.setText("ERRROR");
                }
            }
        });
    }

    public void updateConfiguration(ConfigurationDetails configurationDetails, final StatusCallback callback) {

        showLoadingDialog("Updating configuration, wait...");
        getBackendAPIs().getConfigurationsAPI().updateConfiguration(configurationDetails, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( callback != null ){
                    callback.onComplete(status, statusCode, message);
                }

                if ( status ){
                    showToastMessage("Configuration updated successfully");
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    //================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
