package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.AnalyticsAdapter;
import com.mastaan.logistics.backend.AnalyticsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.DatePeriodSelectorDialog;
import com.mastaan.logistics.models.AnalyticsDetails;
import com.mastaan.logistics.models.AnalyticsInstallSource;
import com.mastaan.logistics.models.BuyerSearchDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 01/06/18.
 */

public class ReportInstallSourcesActivity extends MastaanToolbarActivity implements View.OnClickListener{

    TextView from_date;
    TextView to_date;
    View changePeriod;

    vRecyclerView itemsHolder;
    AnalyticsAdapter analyticsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_install_sources);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        from_date = findViewById(R.id.from_date);
        to_date = findViewById(R.id.to_date);
        changePeriod = findViewById(R.id.changePeriod);
        changePeriod.setOnClickListener(this);

        itemsHolder = findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalGridOrientation(2);
        analyticsAdapter = new AnalyticsAdapter(activity, R.layout.view_analytics_install_source, new ArrayList<AnalyticsDetails>(), new AnalyticsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                AnalyticsInstallSource analyticsInstallSource = analyticsAdapter.getItem(position).getAnalyticsInstallSource();
                Intent buyersAct = new Intent(context, BuyersActivity.class);
                buyersAct.putExtra(Constants.TITLE, "IS: "+analyticsInstallSource.getID()+" Buyers");
                buyersAct.putExtra(Constants.SEARCH_DETAILS, new Gson().toJson(new BuyerSearchDetails().setInstallSource(analyticsInstallSource.getID())));
                startActivity(buyersAct);
            }
            @Override
            public void onActionClick(int position) {

            }
        });
        itemsHolder.setAdapter(analyticsAdapter);

        //----

        getData(null, null);

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getData(from_date.getText().toString(), to_date.getText().toString());
    }

    @Override
    public void onClick(View view) {
        if ( view == changePeriod ){
            String prefillFromDate = from_date.getText().toString();
            String prefillToDate = to_date.getText().toString();
            if ( prefillFromDate.equalsIgnoreCase("Start")&& prefillToDate.equalsIgnoreCase("End") ){
                prefillFromDate = DateMethods.getCurrentDate();
            }
            new DatePeriodSelectorDialog(activity).show(prefillFromDate, prefillToDate, new DatePeriodSelectorDialog.Callback() {
                @Override
                public void onComplete(String fromDate, String toDate) {
                    from_date.setText(fromDate != null&&fromDate.length()>0?DateMethods.getDateInFormat(fromDate, DateConstants.MMM_DD_YYYY):"Start");
                    to_date.setText(toDate != null&&toDate.length()>0?DateMethods.getDateInFormat(toDate, DateConstants.MMM_DD_YYYY):"End");
                    getData(fromDate, toDate);
                }
            });
        }
    }

    public void getData(String fromDate, String toDate){

        changePeriod.setClickable(false);
        showLoadingIndicator("Loading, wait...");
        getBackendAPIs().getAnalyticsAPI().getBuyerInstallSourceAnalytics((fromDate!=null&&fromDate.length()>0&&fromDate.equalsIgnoreCase("Start")==false?DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY):null), (toDate!=null&&toDate.length()>0&&toDate.equalsIgnoreCase("End")==false?DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY):null), new AnalyticsAPI.AnalyticsInstallSourcesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<AnalyticsInstallSource> analyticsInstallSources) {
                changePeriod.setClickable(true);
                if ( status ){
                    if ( analyticsInstallSources.size() > 0 ){
                        List<AnalyticsDetails> analyticsList = new ArrayList<>();
                        for (int i=0;i<analyticsInstallSources.size();i++){
                            analyticsList.add(new AnalyticsDetails().setAnalyticsInstallSource(analyticsInstallSources.get(i)));
                        }
                        analyticsAdapter.setItems(analyticsList);
                        switchToContentPage();
                    }
                    else{
                        showNoDataIndicator("Nothing to show");
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });

    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}