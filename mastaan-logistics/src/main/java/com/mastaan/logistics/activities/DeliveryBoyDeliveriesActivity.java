package com.mastaan.logistics.activities;

import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;

import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;

import java.util.ArrayList;
import java.util.List;

public class DeliveryBoyDeliveriesActivity extends BaseOrderItemsActivity {

    public String deliveryBoyID;
    String deliveryBoyName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();
        setUpContentSwitcher();
        enableContentSwitcher(false);

        deliveryBoyID = getIntent().getStringExtra(Constants.DELIVERY_BOY_ID);
        deliveryBoyName = getIntent().getStringExtra(Constants.DELIVERY_BOY_NAME);

        setToolbarTitle(deliveryBoyName + " Deliveries");

        //------

        getDeliveryBoyItems(deliveryBoyID);

    }

    @Override
    public void onReloadPage() {
        super.onReloadPage();
        getDeliveryBoyItems(deliveryBoyID);
    }

    private void getDeliveryBoyItems(final String deliveryBoyID) {

        hideMenuItems();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        showLoadingIndicator("Loading " + deliveryBoyName + "'s deliveries, wait...");
        getBackendAPIs().getEmployeesAPI().getDeliveryBoyOrders(deliveryBoyID, new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
                if (status) {
                    displayItems(true, groupedOrderItemsLists, new ArrayList<OrderDetails>());
                    switchToOrdersView();
                } else {
                    showReloadIndicator("Unable to load " + deliveryBoyName + "'s deliveries, try again!");
                }
            }
        });

//        getBackendAPIs().getEmployeesAPI().getDeliveryBoyOrders(deliveryBoyID, new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
//            @Override
//            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
//                if (status) {
//                    List<OrderItemDetails> deliveryBoyOrderItems = new ArrayList<OrderItemDetails>();
//                    for (int i=0;i<groupedOrderItemsLists.size();i++){
//                        for (int j=0;j<groupedOrderItemsLists.get(i).getItems().size();j++){
//                            if ( groupedOrderItemsLists.get(i).getItems().get(j).getDeliveryBoyID().equals(deliveryBoyID) ){
//                                deliveryBoyOrderItems.add(groupedOrderItemsLists.get(i).getItems().get(j));
//                            }
//                        }
//                    }
//                    List<GroupedOrdersItemsList> groupedList = new ArrayList<GroupedOrdersItemsList>();
//                    groupedList.add(new GroupedOrdersItemsList("ALL", deliveryBoyOrderItems));
//
//                    displayItems(true, groupedList, new ArrayList<OrderDetails>());
//                } else {
//                    showReloadIndicator("Unable to load " + deliveryBoyName + "'s deliveries, try again!");
//                }
//            }
//        });

    }

}