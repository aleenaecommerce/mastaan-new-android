package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.widgets.vAppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.TabsFragmentAdapter;
import com.mastaan.logistics.backend.FeedbacksAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AnalyseFeedbacksDialog;
import com.mastaan.logistics.fragments.FeedbacksFragment;
import com.mastaan.logistics.fragments.FilterItemsFragment;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.GroupedFeedbacksList;
import com.mastaan.logistics.models.SearchDetails;

import java.util.List;

/**
 * Created by venkatesh on 29/4/16.
 */

public class FeedbacksActivity extends MastaanToolbarActivity implements View.OnClickListener {

    DrawerLayout drawerLayout;
    FilterItemsFragment filterFragment;

    vAppBarLayout appBarLayout;

    MenuItem searchViewMenuITem;
    SearchView searchView;
    MenuItem filterMenuITem;

    TabLayout tabLayout;
    ViewPager viewPager;
    String []tabItemsNames = new String[]{Constants.LOW_RATED_FEEDBACKS, Constants.AVERAGE_RATED_FEEDBACKS, Constants.HIGH_RATED_FEEDBACKS, Constants.PROCESSING_FEEDBACKS, Constants.PROCESSED_FEEDBACKS};
    TabsFragmentAdapter adapter;

    View showSummary;

    SearchDetails searchDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedbacks);
        hasLoadingView();

        filterFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    onClearSearch();
                } else {
                    onPerformSearch(searchDetails);
                }
            }
        });


        //--------

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        appBarLayout = (vAppBarLayout) findViewById(R.id.appBarLayout);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        showSummary = findViewById(R.id.showSummary);
        showSummary.setOnClickListener(this);

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabTextColors(getResources().getColor(R.color.colorPrimaryMain), getResources().getColor(R.color.colorPrimaryDarkMain));
        tabLayout.removeAllTabs();
        for (int i = 0; i < tabItemsNames.length; i++) {
            TabLayout.Tab tab = tabLayout.newTab();
            View rowView = LayoutInflater.from(context).inflate(R.layout.view_tab_list_item, null, false);
            TextView tabTitle = (TextView) rowView.findViewById(R.id.tabTitle);
            tabTitle.setText(tabItemsNames[i]);
            tab.setCustomView(rowView);
            tabLayout.addTab(tab);
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                try {
                    FeedbacksFragment feedbacksFragment = (FeedbacksFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getId() + ":" + viewPager.getCurrentItem());
                    if ( feedbacksFragment.isShowingContentPage() || feedbacksFragment.isShowingSearchIndicator() ){
                        showMenuOptions("Activity");
                    }else{
                        hideMenuOptions("Activity");
                    }
                }catch (Exception e){ e.printStackTrace();  }
                onPerformSearch(searchDetails);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));  // To Sync ViewPager with Tabs.
        viewPager.setOffscreenPageLimit(1);

        //--------

        checkUnprocessedFeedbacks();
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        checkUnprocessedFeedbacks();
    }

    @Override
    public void onClick(View view) {
        if ( view == showSummary ){
            //showToastMessage("Under construction / Not yet done..");
            try {
                FeedbacksFragment feedbacksFragment = (FeedbacksFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getId() + ":" + viewPager.getCurrentItem());
                new AnalyseFeedbacksDialog(activity, feedbacksFragment.getFEEDBACKS_TYPE(), feedbacksFragment.getShowingItemsList()).show();
            }catch (Exception e){}
        }
    }

    //---------------

    public void checkUnprocessedFeedbacks(){

        hideMenuOptions("Activity");
        showLoadingIndicator("Checking your unprocessed feedbacks, wait...");
        getBackendAPIs().getFeedbacksAPI().getUnProcessedFeedbacks(new FeedbacksAPI.FeedbacksCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<FeedbackDetails> feedbacksList) {
                if (status) {
                    if (feedbacksList != null && feedbacksList.size() > 0) {
                        new AsyncTask<Void, Void, Void>() {
                            String unprocessedFeedbacksJSON = "";
                            @Override
                            protected Void doInBackground(Void... voids) {
                                unprocessedFeedbacksJSON = new Gson().toJson(new GroupedFeedbacksList("", "", feedbacksList));
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                showToastMessage("You have " + feedbacksList.size() + " unprocessed feedbacks.");
                                Intent unprocessedFeedbacksAct = new Intent(context, UnProcessedFeedbacksActivity.class);
                                unprocessedFeedbacksAct.putExtra(Constants.UNPROCESSED_FEEDBACKS, unprocessedFeedbacksJSON);
                                startActivity(unprocessedFeedbacksAct);
                                finish();
                            }
                        }.execute();

                    } else {
                        adapter = new TabsFragmentAdapter(context, getSupportFragmentManager(), tabItemsNames);
                        viewPager.setAdapter(adapter);
                        showMenuOptions("Activity");
                        switchToContentPage();
                    }
                } else {
                    showReloadIndicator(statusCode, "Unable to load, try again!");
                }
            }
        });
    }

    //----------------

    public void onPerformSearch(SearchDetails searchDetails){
        this.searchDetails = searchDetails;

        try {
            if (isShowingContentPage()) {
                if ( appBarLayout != null ) {
                    appBarLayout.expandToolbar();   // To Show Tablayout if its hided already
                }
                FeedbacksFragment feedbacksFragment = (FeedbacksFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getId() + ":" + viewPager.getCurrentItem());
                feedbacksFragment.performSearch(searchDetails);
            }
        }catch (Exception e){ e.printStackTrace();  }
    }

    public void onClearSearch(){
        this.searchDetails = new SearchDetails();
        filterFragment.clearSelection();

        try {
            if (isShowingContentPage()) {
                FeedbacksFragment feedbacksFragment = (FeedbacksFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getId() + ":" + viewPager.getCurrentItem());
                feedbacksFragment.clearSearch();
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    public void collapseSearchView(){
        try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
    }

    public String getSearchViewText(){
        String searchViewText = "";
        try{ searchViewText = searchView.getQuery().toString();      }catch (Exception e){}
        return searchViewText;
    }

    //------

    public void hideMenuOptions(String showingPage){
        if ( tabItemsNames[tabLayout.getSelectedTabPosition()].equalsIgnoreCase(showingPage) || showingPage.equalsIgnoreCase("Activity")  ){
            try {
                searchViewMenuITem.setVisible(false);
                filterMenuITem.setVisible(false);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(com.aleena.common.R.id.right_drawer));
            }catch (Exception e){}
        }
    }

    public void showMenuOptions(String showingPage){
        if ( tabItemsNames[tabLayout.getSelectedTabPosition()].equalsIgnoreCase(showingPage) || showingPage.equalsIgnoreCase("Activity") ){
            try {
                searchViewMenuITem.setVisible(true);
                filterMenuITem.setVisible(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(com.aleena.common.R.id.right_drawer));
            }catch (Exception e){}
        }
    }

    public void clearSelection(){
        filterFragment.clearSelection();
        collapseSearchView();
    }



    //=================

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_feedbacks, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filterMenuITem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if ( searchView.getQuery() != null && searchView.getQuery().toString().length() > 0 ) {
                    onClearSearch();
                }
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                filterFragment.updateCustomerName(query);
//                if ( searchDetails != null && searchDetails.isSearchable() ) {
//                    searchDetails.setCustomerName(query);
//                    onPerformSearch(searchDetails);
//                }else{
                    SearchDetails searchDetails = new SearchDetails();
                    searchDetails.setConsiderAtleastOneMatch(query);
                    onPerformSearch(searchDetails);
//                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){
        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            super.onBackPressed();
        }
    }

}