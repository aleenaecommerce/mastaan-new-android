package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vViewPager;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.FeedbacksPagerAdapter;
import com.mastaan.logistics.backend.FeedbacksAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.fragments.FeedbacksFragment;
import com.mastaan.logistics.fragments.FilterItemsFragment;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.GroupedFeedbacksList;
import com.mastaan.logistics.models.SearchDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 29/4/16.
 */

public class UnProcessedFeedbacksActivity extends MastaanToolbarActivity{

    DrawerLayout drawerLayout;
    FilterItemsFragment filterFragment;

    MenuItem releaseFeedbacksMentuItem;
    MenuItem filterMenuITem;

    TabLayout tabLayout;
    vViewPager viewPager;
    FeedbacksPagerAdapter feedbacksPagerAdapter;

    List<GroupedFeedbacksList> groupedFeedbacksLists;

    SearchDetails searchDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unprocessed_feedbacks);
        hasLoadingView();

        filterFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    onClearSearch();
                } else if (searchDetails.getAction().equalsIgnoreCase(Constants.ANALYSE)) {
                    showToastMessage("Under construction / Not yet done..");
                    //new AnalyseOrderItemsDialog(activity, items).show();
                } else {
                    onPerformSearch(searchDetails);
                }
            }
        });


        //---------

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabTextColors(getResources().getColor(R.color.normal_tab_text_color), getResources().getColor(R.color.selected_tab_text_color));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                onPerformSearch(searchDetails);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });

        viewPager = (vViewPager) findViewById(R.id.viewPager);
        //viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    tabLayout.getTabAt(position).select();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));  // To Sync ViewPager with Tabs.


        //------

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(com.aleena.common.R.id.right_drawer));
        new AsyncTask<Void, Void, Void>() {
            List<FeedbackDetails> unprocessedFeedbacksList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {
                String unprocessed_feedbacks_jsons = getIntent().getStringExtra(Constants.UNPROCESSED_FEEDBACKS);
                if ( unprocessed_feedbacks_jsons != null && unprocessed_feedbacks_jsons.length() > 0 ){
                    GroupedFeedbacksList groupedFeedbacksList = new Gson().fromJson(unprocessed_feedbacks_jsons, GroupedFeedbacksList.class);
                    if ( groupedFeedbacksList != null ){
                        unprocessedFeedbacksList = groupedFeedbacksList.getItems();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if ( unprocessedFeedbacksList != null && unprocessedFeedbacksList.size() > 0 ){
                    displayItems(unprocessedFeedbacksList);
                }else {
                    getUnprocessedFeedbacks();
                }
            }
        }.execute();

    }

    public void displayItems(final List<FeedbackDetails> feedbacksList){

        if (feedbacksList.size() > 0) {
            hideMenuOptions("");
            showLoadingIndicator("Displaying, wait...");
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    groupedFeedbacksLists = GroupingMethods.groupFeedbacksByOrders(feedbacksList);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    showMenuOptions("");

                    //markAllProcessed.setVisibility(View.VISIBLE);
                    tabLayout.removeAllTabs();
                    for (int i = 0; i < groupedFeedbacksLists.size(); i++) {
                        //Log.d(Constants.LOG_TAG, "ID: "+groupedFeedbacksLists.get(i).getOrderID()+", Date: "+groupedFeedbacksLists.get(i).getDeliveryDateAndTime()+", with Size: "+groupedFeedbacksLists.get(i).getItems().size());
                        TabLayout.Tab tab = tabLayout.newTab();
                        View rowView = LayoutInflater.from(context).inflate(R.layout.view_tab_list_item, null, false);
                        TextView tabTitle = (TextView) rowView.findViewById(R.id.tabTitle);
                        tabTitle.setText(DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(groupedFeedbacksLists.get(i).getDeliveryDate()), DateConstants.MMM_DD));
                        tab.setCustomView(rowView);
                        tabLayout.addTab(tab);
                    }
                    if (groupedFeedbacksLists.size() == 1) {
                        tabLayout.setVisibility(View.GONE);
                    } else {
                        tabLayout.setVisibility(View.VISIBLE);
                    }

                    feedbacksPagerAdapter = new FeedbacksPagerAdapter(getSupportFragmentManager(), groupedFeedbacksLists);
                    viewPager.setAdapter(feedbacksPagerAdapter);

                    switchToContentPage();
                }
            }.execute();
        } else {
            showNoDataIndicator("No unprocessed feedbacks");
        }

    }

    public void switchToNextTab(){
        if ( groupedFeedbacksLists.size() > 1 ) {
            try {
                int selectedTab = tabLayout.getSelectedTabPosition();
                if ( selectedTab != groupedFeedbacksLists.size()-1 &&groupedFeedbacksLists.get(tabLayout.getSelectedTabPosition()+1).getItems().size() > 0 ){
                    tabLayout.getTabAt(tabLayout.getSelectedTabPosition()+1).select();
                }else if ( selectedTab != 0 && groupedFeedbacksLists.get(tabLayout.getSelectedTabPosition()-1).getItems().size() > 0 ){
                    tabLayout.getTabAt(tabLayout.getSelectedTabPosition()-1).select();
                }
            } catch (Exception e) {}
        }
    }

    public void checkFeedbacksCount(){
        boolean isAllDone = true;
        for (int i=0;i<groupedFeedbacksLists.size();i++){
            if ( groupedFeedbacksLists.get(i).getItems().size() > 0 ){
                isAllDone = false;
                break;
            }
        }
        if ( isAllDone ){
            showToastMessage("All unprocessed feedbacks are completed");
            Intent feedbacksAct = new Intent(context, FeedbacksActivity.class);
            startActivity(feedbacksAct);
            finish();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getUnprocessedFeedbacks();
    }

    //---------------

    public void releaseUnprocessedFeedbacks(){

        showLoadingDialog("Releasing unprocessed feedbacks, wait...");
        getBackendAPIs().getFeedbacksAPI().releaseUnprocessedFeedbacks(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    Intent feedbacksAct = new Intent(context, FeedbacksActivity.class);
                    startActivity(feedbacksAct);
                    finish();
                }else{
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while releasing unprocessed feedbacks.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                releaseUnprocessedFeedbacks();
                            }
                        }
                    });
                }
            }
        });
    }
    public void deleteTab(String orderID){
//        for (int i=0;i<groupedFeedbacksLists.size();i++){
//            if ( groupedFeedbacksLists.get(i).getOrderID().equals(orderID)){
//                groupedFeedbacksLists.remove(i);
//                tabLayout.removeTabAt(i);
//                feedbacksPagerAdapter.deleteItem(viewPager, i);
//                break;
//
//
//            }
//        }
//        if ( feedbacksPagerAdapter.getItemsCount() == 0 ){
//            showNoDataIndicator("No items");
//        }
    }

    public List<FeedbackDetails> getOrderFeedbacks(String orderID) {

        if ( groupedFeedbacksLists != null && groupedFeedbacksLists.size() > 0 ){
            for (int i=0;i<groupedFeedbacksLists.size();i++){
                if ( groupedFeedbacksLists.get(i).getOrderID().equalsIgnoreCase(orderID) ){
                    return groupedFeedbacksLists.get(i).getItems();
                }
            }
        }
        return new ArrayList<>();
    }

    public void getUnprocessedFeedbacks(){

        hideMenuOptions("");
        showLoadingIndicator("Loading unprocessed feedbacks, wait...");
        getBackendAPIs().getFeedbacksAPI().getUnProcessedFeedbacks(new FeedbacksAPI.FeedbacksCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<FeedbackDetails> feedbacksList) {
                if (status) {
                    displayItems(feedbacksList);
                } else {
                    showReloadIndicator(statusCode, "Unable to load unprocessed feedbacks, try again!");
                }
            }
        });
    }

    //----------------

    public void onPerformSearch(SearchDetails searchDetails){
        this.searchDetails = searchDetails;

        try {
            if (isShowingContentPage()) {
                FeedbacksFragment feedbacksFragment = (FeedbacksFragment) feedbacksPagerAdapter.getCurrentFragment(viewPager);//getSupportFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getID() + ":" + viewPager.getCurrentItem());
                feedbacksFragment.performSearch(searchDetails);
            }
        }catch (Exception e){ e.printStackTrace();  }
    }

    public void onClearSearch(){
        this.searchDetails = null;
        filterFragment.clearSelection();

        try {
            if (isShowingContentPage()) {
                FeedbacksFragment feedbacksFragment = (FeedbacksFragment) feedbacksPagerAdapter.getCurrentFragment(viewPager);//getSupportFragmentManager().findFragmentByTag("android:switcher:" + viewPager.getID() + ":" + viewPager.getCurrentItem());
                feedbacksFragment.clearSearch();
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    public void clearSelection(){
        filterFragment.clearSelection();
    }

    //-----

    public void hideMenuOptions(String showingPage){
        try {
            releaseFeedbacksMentuItem.setVisible(false);
            filterMenuITem.setVisible(false);
        }catch (Exception e){}
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(com.aleena.common.R.id.right_drawer));
    }

    public void showMenuOptions(String showingPage){
        try {
            releaseFeedbacksMentuItem.setVisible(true);
            filterMenuITem.setVisible(true);
        }catch (Exception e){}
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(com.aleena.common.R.id.right_drawer));
    }

    //=========

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_unprocessed_feedbacks, menu);

        releaseFeedbacksMentuItem = menu.findItem(R.id.action_release);
        filterMenuITem = menu.findItem(R.id.action_filter);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_release ){
            showChoiceSelectionDialog("Confirm!", "Do you want to release your unprocessed feedbacks?", "NO", "YES", new ChoiceSelectionCallback() {
                @Override
                public void onSelect(int choiceNo, String choiceName) {
                    if ( choiceName.equalsIgnoreCase("YES")){
                        releaseUnprocessedFeedbacks();
                    }
                }
            });
        }
        else if ( menuItem.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }else {
            onBackPressed();
        }
        return true;
    }

}