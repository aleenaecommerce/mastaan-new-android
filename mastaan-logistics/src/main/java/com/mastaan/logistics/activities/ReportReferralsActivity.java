package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.AnalyticsAdapter;
import com.mastaan.logistics.backend.AnalyticsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.DatePeriodSelectorDialog;
import com.mastaan.logistics.models.AnalyticsDetails;
import com.mastaan.logistics.models.AnalyticsReferral;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.BuyerSearchDetails;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 01/01/18.
 */

public class ReportReferralsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    TextView from_date;
    TextView to_date;
    View changePeriod;

    vRecyclerView itemsHolder;
    AnalyticsAdapter analyticsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_referrals);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        from_date = (TextView) findViewById(R.id.from_date);
        to_date = (TextView) findViewById(R.id.to_date);
        changePeriod = (TextView) findViewById(R.id.changePeriod);
        changePeriod.setOnClickListener(this);

        itemsHolder = (vRecyclerView) findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        analyticsAdapter = new AnalyticsAdapter(activity, new ArrayList<AnalyticsDetails>(), new AnalyticsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                AnalyticsReferral analyticsReferral = analyticsAdapter.getItem(position).getAnalyticsReferral();
                String fromDate = from_date.getText().toString();
                String toDate = to_date.getText().toString();

                Intent buyersAct = new Intent(context, BuyersActivity.class);
                buyersAct.putExtra(Constants.ACTION_TYPE, Constants.REFERRAL_BUYERS);
                buyersAct.putExtra(Constants.TITLE, analyticsReferral.getID()+" Referral Buyers");
                buyersAct.putExtra(Constants.SEARCH_DETAILS, new Gson().toJson(new BuyerSearchDetails()
                        .setReferralCode(analyticsReferral.getID())
                        .setFromDate((fromDate!=null&&fromDate.length()>0&&fromDate.equalsIgnoreCase("Start")==false?DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY):null))
                        .setToDate((toDate!=null&&toDate.length()>0&&toDate.equalsIgnoreCase("End")==false?DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY):null))
                ));
                startActivity(buyersAct);
            }
            @Override
            public void onActionClick(int position) {
                AnalyticsReferral analyticsReferral = analyticsAdapter.getItem(position).getAnalyticsReferral();

                new BuyerOptionsDialog(activity).showCallEmailOptions().show(analyticsReferral.getBuyer()).setCallback(new BuyerOptionsDialog.Callback() {
                    @Override
                    public void onDetailsUpdated(BuyerDetails buyerDetails) {}

                    @Override
                    public void onEnabledBuyer() {}
                    @Override
                    public void onDisabledBuyer(String disabledReason) {}

                    @Override
                    public void onFollowupClaimed(UserDetails userDetails) {}

                    @Override
                    public void onFollowupReleased() {}

                    @Override
                    public void onFollowupAdded(FollowupDetails followupDetails) {}

                    @Override
                    public void onInstallSourceChange(String installSource, String installSourceDetails) {}

                    @Override
                    public void onUpdatePromotionalAlertsPreference(boolean preference) {}
                });
            }
        });
        itemsHolder.setAdapter(analyticsAdapter);

        //----

        getData(null, null);

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getData(from_date.getText().toString(), to_date.getText().toString());
    }

    @Override
    public void onClick(View view) {
        if ( view == changePeriod ){
            String prefillFromDate = from_date.getText().toString();
            String prefillToDate = to_date.getText().toString();
            if ( prefillFromDate.equalsIgnoreCase("Start")&& prefillToDate.equalsIgnoreCase("End") ){
                prefillFromDate = DateMethods.getCurrentDate();
            }
            new DatePeriodSelectorDialog(activity).show(prefillFromDate, prefillToDate, new DatePeriodSelectorDialog.Callback() {
                @Override
                public void onComplete(String fromDate, String toDate) {
                    from_date.setText(fromDate != null&&fromDate.length()>0?DateMethods.getDateInFormat(fromDate, DateConstants.MMM_DD_YYYY):"Start");
                    to_date.setText(toDate != null&&toDate.length()>0?DateMethods.getDateInFormat(toDate, DateConstants.MMM_DD_YYYY):"End");
                    getData(fromDate, toDate);
                }
            });
        }
    }

    public void getData(String fromDate, String toDate){

        changePeriod.setClickable(false);
        showLoadingIndicator("Loading, wait...");
        getBackendAPIs().getAnalyticsAPI().getReferralsAnalytics((fromDate!=null&&fromDate.length()>0&&fromDate.equalsIgnoreCase("Start")==false?DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY):null), (toDate!=null&&toDate.length()>0&&toDate.equalsIgnoreCase("End")==false?DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY):null), new AnalyticsAPI.AnalyticsReferralsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<AnalyticsReferral> analyticsReferrals) {
                changePeriod.setClickable(true);
                if ( status ){
                    if ( analyticsReferrals.size() > 0 ){
                        List<AnalyticsDetails> analyticsList = new ArrayList<>();
                        for (int i=0;i<analyticsReferrals.size();i++){
                            analyticsList.add(new AnalyticsDetails().setAnalyticsReferral(analyticsReferrals.get(i)));
                        }
                        analyticsAdapter.setItems(analyticsList);
                        switchToContentPage();
                    }
                    else{
                        showNoDataIndicator("Nothing to show");
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });

    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}