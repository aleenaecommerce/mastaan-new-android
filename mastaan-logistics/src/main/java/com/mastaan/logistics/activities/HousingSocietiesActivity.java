package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.HousingSocietiesAdapter;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.HousingSocietyDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 23/09/18.
 */

public class HousingSocietiesActivity extends MastaanToolbarActivity {

    vRecyclerView itemsHolder;
    HousingSocietiesAdapter housingSocietiesAdapter;

    View addHousingSociety;

    int selectedItemPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_housing_societies);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        itemsHolder = findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        housingSocietiesAdapter = new HousingSocietiesAdapter(context, new ArrayList<HousingSocietyDetails>(), new HousingSocietiesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                selectedItemPosition = position;

                Intent updateHousingSocietyAct = new Intent(context, AddOrEditHousingSocietyActivity.class);
                updateHousingSocietyAct.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                updateHousingSocietyAct.putExtra(Constants.HOUSING_SOCIETY_DETAILS, new Gson().toJson(housingSocietiesAdapter.getItem(position)));
                startActivityForResult(updateHousingSocietyAct, Constants.ADD_OR_EDIT_HOUSING_SOCIETY_ACTIVITY_CODE);
            }

            @Override
            public void onDeleteClick(final int position) {
                showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to delete <b>" + housingSocietiesAdapter.getItem(position).getName() + "</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES") ){
                            deleteHousingSociety(position);
                        }
                    }
                });
            }
        });
        itemsHolder.setAdapter(housingSocietiesAdapter);

        addHousingSociety = findViewById(R.id.addHousingSociety);
        addHousingSociety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addHousingSocietyAct = new Intent(context, AddOrEditHousingSocietyActivity.class);
                startActivityForResult(addHousingSocietyAct, Constants.ADD_OR_EDIT_HOUSING_SOCIETY_ACTIVITY_CODE);
            }
        });

        //----

        getHousingSocieties();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getHousingSocieties();
    }

    public void getHousingSocieties(){

        addHousingSociety.setVisibility(View.GONE);
        showLoadingIndicator("Loading, wait...");
        getBackendAPIs().getRoutesAPI().getHousingSocieties(new RoutesAPI.HousingSocietiesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<HousingSocietyDetails> housingSocieties) {
                if ( status ){
                    addHousingSociety.setVisibility(View.VISIBLE);
                    if ( housingSocieties.size() > 0 ){
                        housingSocietiesAdapter.setItems(housingSocieties);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("Nothing to show");
                    }
                }else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    public void deleteHousingSociety(final int position){

        showLoadingDialog("Deleting housing society, wait...");
        getBackendAPIs().getRoutesAPI().deleteHousingSociety(housingSocietiesAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    housingSocietiesAdapter.deleteItem(position);
                    showToastMessage("Housing society deleted successfully");
                }else {
                    showToastMessage("Something went wrong while deleting housing society, try again!");
                }
            }
        });
    }

    //===========

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.ADD_OR_EDIT_HOUSING_SOCIETY_ACTIVITY_CODE && resultCode == RESULT_OK ){
                String actionType = data.getStringExtra(Constants.ACTION_TYPE);
                HousingSocietyDetails housingSocietyDetails = new Gson().fromJson(data.getStringExtra(Constants.HOUSING_SOCIETY_DETAILS), HousingSocietyDetails.class);

                if ( actionType.equalsIgnoreCase(Constants.ADD) ){
                    housingSocietiesAdapter.addItem(itemsHolder.getLastCompletelyVisibleItemPosition(), housingSocietyDetails);
                    switchToContentPage();
                }
                else  if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                    housingSocietiesAdapter.setItem(selectedItemPosition, housingSocietyDetails);
                }
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}