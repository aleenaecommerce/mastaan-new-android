package com.mastaan.logistics.activities;

import android.os.Bundle;
import android.view.MenuItem;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.FeedbacksAdapter;
import com.mastaan.logistics.backend.FeedbacksAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.OrderDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.OrderDetails2;

import java.util.ArrayList;
import java.util.List;

public class FeedbackDetailsActivity extends MastaanToolbarActivity{

    String id;

    vRecyclerView itemHolder;
    FeedbacksAdapter feedbacskAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_details);
        hasLoadingView();
        hasSwipeRefresh();

        id = getIntent().getStringExtra(Constants.ID);

        //--------

        itemHolder = findViewById(R.id.itemHolder);
        itemHolder.setupVerticalOrientation();
        feedbacskAdapter = new FeedbacksAdapter(activity, new ArrayList<FeedbackDetails>(), false, new FeedbacksAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails2 orderDetails = feedbacskAdapter.getItem(position).getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails2 orderDetails = feedbacskAdapter.getItem(position).getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowOrderDetails(int position) {
                new OrderDetailsDialog(activity).show(feedbacskAdapter.getItem(position).getOrderItemDetails().getOrderID());
            }

            @Override
            public void onShowCustomerOrderHistory(int position) {
                new BuyerOptionsDialog(activity).show(feedbacskAdapter.getItem(position));
            }

            @Override
            public void onShowStockDetails(int position) {
                new StockDetailsDialog(activity).show(feedbacskAdapter.getItem(position).getOrderItemDetails());
            }
        });
        itemHolder.setAdapter(feedbacskAdapter);

        //--------

        getFeedbackDetails();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getFeedbackDetails();
    }

    private void getFeedbackDetails(){

        showLoadingIndicator("Loading feedback details, wait...");
        getBackendAPIs().getFeedbacksAPI().getFeedbackDetails(id, new FeedbacksAPI.FeedbackCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, FeedbackDetails feedbackDetails) {
                if ( status ){
                    List<FeedbackDetails> list = new ArrayList<>();
                    list.add(feedbackDetails);
                    feedbacskAdapter.setItems(list);
                    switchToContentPage();
                }else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    //========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
