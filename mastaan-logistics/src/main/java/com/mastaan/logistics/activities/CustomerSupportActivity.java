package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.View;

import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.List;

public class CustomerSupportActivity extends BaseOrderItemsActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();
        setFilterMenuItemVisibility(false);

        startAutoRefreshTimer(10 * 60);         // 10 Minutes Interval

        Intent fireBaseReceiver = new Intent(Constants.FIREBASE_RECEIVER)
                .putExtra("type", Constants.CLEAR_CS_NOTIFICATIONS_CACHE);
        activity.sendBroadcast(fireBaseReceiver);

        //--------

        checkUnprocessedCustomerSupportItems();
    }

    @Override
    public void onAutoRefreshTimeElapsed() {
        super.onAutoRefreshTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onInactiveTimeElapsed() {
        super.onInactiveTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onReloadPage() {
        super.onReloadPage();
        checkUnprocessedCustomerSupportItems();
    }

    @Override
    public void onBackgroundReloadPressed() {
        super.onBackgroundReloadPressed();
        getCustomerSupportItems(true);
    }

    private void checkUnprocessedCustomerSupportItems() {

        showLoadingIndicator("Checking unprocessed customer support items, wait...");
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        loadingIndicator.setVisibility(View.GONE);
        reloadIndicator.setVisibility(View.GONE);
        hideNavigationButton();
        getBackendAPIs().getCustomerSupportAPI().getUnprocessedCustomerSupportItems(localStorageData.getWarehouseLocation(), new OrderItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<OrderItemDetails> orderItemssList) {
                if (status) {
                    if ( orderItemssList.size() > 0 ){
                        goToUnprocessedCustomerSupportItemsPage(orderItemssList);
                    }else{
                        getCustomerSupportItems(false);
                    }
                } else {
                    showReloadIndicator("Unable to check unprocessed customer support items, try again!");
                }
            }
        });
    }

    private void getCustomerSupportItems(final boolean loadInBackground) {

        if ( loadInBackground ){
            loadingIndicator.setVisibility(View.VISIBLE);
            reloadIndicator.setVisibility(View.GONE);
        }else {
            showLoadingIndicator("Loading customer support items, wait...");
        }
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        hideNavigationButton();
        getBackendAPIs().getCustomerSupportAPI().getCustomerSupportItems(localStorageData.getWarehouseLocation(), new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
                loadingIndicator.setVisibility(View.GONE);
                if (status) {
                    if (loadInBackground) {
                        updateDataAndDisplayItems(groupedOrderItemsLists, ordersList);
                    } else {
                        displayItems(true, groupedOrderItemsLists, ordersList);
                    }
                } else {
                    if (loadInBackground) {
                        reloadIndicator.setVisibility(View.VISIBLE);
                        loadingIndicator.setVisibility(View.GONE);
                    } else {
                        showReloadIndicator("Unable to load customer support items, try again!");
                    }
                }
            }
        });
    }


}