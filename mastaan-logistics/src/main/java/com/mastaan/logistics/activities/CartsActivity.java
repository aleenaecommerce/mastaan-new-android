package com.mastaan.logistics.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.CartsAdapter;
import com.mastaan.logistics.backend.CartsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.BuyerSearchDetails;
import com.mastaan.logistics.models.CartDetails;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.UserDetailsMini;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 03/03/19.
 */

public class CartsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    long pageNumber = 1;
    boolean isLoading;

    CheckBox showClaimed;

    vRecyclerView itemsHolder;
    CartsAdapter cartsAdapter;

    Button loadMore;

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carts);
        hasLoadingView();
        hasSwipeRefresh();

        userDetails = localStorageData.getUserDetails();

        //---------

        showClaimed = findViewById(R.id.showClaimed);
        showClaimed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                pageNumber = 1;
                getCarts();
            }
        });

        itemsHolder = findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        itemsHolder.setScrollListener(new vRecyclerView.ScrollListener() {
            @Override
            public void onScrolled(boolean isScrollEnd) {
                if ( isScrollEnd ){
                    if (isLoading == false && pageNumber > 0 ) {
                        pageNumber += 1;
                        getCarts();
                    }
                }
            }
        });
        cartsAdapter = new CartsAdapter(activity, new ArrayList<CartDetails>(), new CartsAdapter.Callback() {
            @Override
            public void onClick(final int position) {
                final CartDetails cartDetails = cartsAdapter.getItem(position);

                final List<String> optionsList = new ArrayList<>();
                if ( cartDetails.getFollowupEmployee() == null ){
                    optionsList.add("Claim Followup");
                }else{
                    if ( userDetails.isSuperUser() || cartDetails.getFollowupEmployee().getID().equals(userDetails.getID()) ){
                        optionsList.add("Release Followup");
                    }
                    if ( cartDetails.getFollowupEmployee().getID().equals(userDetails.getID()) ){
                        optionsList.add("Delete Cart");
                        optionsList.add("Complete Followup");
                    }
                }
                if ( optionsList.size() > 0 ){
                    showListChooserDialog(optionsList, new ListChooserCallback() {
                        @Override
                        public void onSelect(int oPosition) {
                            if ( optionsList.get(oPosition).equalsIgnoreCase("Claim Followup") ){
                                claimFollowup();
                            }else if ( optionsList.get(oPosition).equalsIgnoreCase("Release Followup") ){
                                releaseFollowup();
                            }else if ( optionsList.get(oPosition).equalsIgnoreCase("Delete Cart") ){
                                deleteCart();
                            }else if ( optionsList.get(oPosition).equalsIgnoreCase("Complete Followup") ){
                                final List<String> cartFollowupReasons = localStorageData.getCartFollowupReasons();
                                cartFollowupReasons.add("Other");
                                showListChooserDialog("Complete Followup", cartFollowupReasons, new ListChooserCallback() {
                                    @Override
                                    public void onSelect(int lPosition) {
                                        if ( cartFollowupReasons.get(lPosition).equalsIgnoreCase("Other") ){
                                            showInputTextDialog("Complete Followup", "Comments", new TextInputCallback() {
                                                @Override
                                                public void onComplete(String comments) {
                                                    completeFollowup(comments);
                                                }
                                            });
                                        }else{
                                            completeFollowup(cartFollowupReasons.get(lPosition));
                                        }
                                    }
                                });
                            }
                        }
                        private void claimFollowup(){
                            showLoadingDialog("Claiming followup, wait...");
                            getBackendAPIs().getCartsAPI().claimBuyerCartFollowup(cartDetails.getID(), new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                    closeLoadingDialog();
                                    if ( status ){
                                        showToastMessage("Followup claimed successfully");
                                        cartDetails.setFollowupEmployee(new UserDetailsMini(userDetails));
                                        cartsAdapter.setItem(position, cartDetails);
                                    }else{
                                        showToastMessage("Something went wrong, try again!");
                                    }
                                }
                            });
                        }
                        private void releaseFollowup(){
                            showLoadingDialog("Releasing followup, wait...");
                            getBackendAPIs().getCartsAPI().releaseBuyerCartFollowup(cartDetails.getID(), new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                    closeLoadingDialog();
                                    if ( status ){
                                        showToastMessage("Followup released successfully");
                                        cartDetails.setFollowupEmployee(null);
                                        cartsAdapter.setItem(position, cartDetails);
                                    }else{
                                        showToastMessage("Something went wrong, try again!");
                                    }
                                }
                            });
                        }
                        private void deleteCart(){
                            showLoadingDialog("Deleting cart, wait...");
                            getBackendAPIs().getCartsAPI().deleteBuyerCart(cartDetails.getID(), new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                    closeLoadingDialog();
                                    if ( status ){
                                        showToastMessage("Cart deleted successfully");
                                        cartsAdapter.deleteItem(position);
                                    }else{
                                        showToastMessage("Something went wrong, try again!");
                                    }
                                }
                            });
                        }
                        private void completeFollowup(String comments){
                            showLoadingDialog("Completing followup, wait...");
                            getBackendAPIs().getCartsAPI().completeBuyerCartFollowup(cartDetails.getID(), comments, new CartsAPI.FollowupCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message, FollowupDetails followupDetails) {
                                    closeLoadingDialog();
                                    if ( status ){
                                        showToastMessage("Followup completed successfully");
                                        cartsAdapter.deleteItem(position);
                                    }else{
                                        showToastMessage("Something went wrong, try again!");
                                    }
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onCallCustomer(final int position) {
                callToPhoneNumber(cartsAdapter.getItem(position).getBuyerDetails().getMobileNumber());
            }
        });
        itemsHolder.setAdapter(cartsAdapter);

        loadMore = findViewById(R.id.loadMore);
        loadMore.setOnClickListener(this);

        //----

        getCarts();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        pageNumber = 1;
        getCarts();
    }

    @Override
    public void onClick(View view) {
        if ( view == loadMore ){
            getCarts();
        }
    }

    public void getCarts(){

        if ( pageNumber == 1 ) {
            loadMore.setVisibility(View.GONE);
            showLoadingIndicator("Loading, wait...");
        }else{
            loadMore.setVisibility(View.VISIBLE);
            loadMore.setText("Loading...");
        }
        isLoading = true;
        showClaimed.setClickable(false);
        getBackendAPIs().getCartsAPI().getInactiveBuyerCarts(new BuyerSearchDetails().setFollowupEmployee(showClaimed.isChecked()?userDetails.getID():null), pageNumber, new CartsAPI.CartsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<CartDetails> cartsList) {
                isLoading = false;
                showClaimed.setClickable(true);
                loadMore.setVisibility(View.GONE);
                if ( status ){
                    if ( cartsList.size() > 0 ){
                        if ( pageNumber == 1 ) {
                            switchToContentPage();
                            cartsAdapter.setItems(cartsList);
                        }else{
                            cartsAdapter.addItems(cartsList);
                        }
                    }
                    else{
                        if ( pageNumber == 1) {
                            showNoDataIndicator("Nothing to show");
                        }else{
                            pageNumber = -1;
                            showToastMessage("No more to load");
                        }
                    }
                }
                else{
                    if ( pageNumber == 1 ) {
                        showReloadIndicator("Something went wrong, try again!");
                    }else{
                        loadMore.setVisibility(View.VISIBLE);
                        loadMore.setText("Load more");
                    }
                }
            }
        });
    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}