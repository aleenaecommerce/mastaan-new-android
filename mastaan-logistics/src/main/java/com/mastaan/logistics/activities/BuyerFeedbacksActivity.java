package com.mastaan.logistics.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.FeedbacksAdapter;
import com.mastaan.logistics.backend.FeedbacksAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AnalyseFeedbacksDialog;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.OrderDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.methods.AnalyseFeedbacksMethods;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.OrderDetails2;

import java.util.ArrayList;
import java.util.List;

public class BuyerFeedbacksActivity extends MastaanToolbarActivity {

    String buyerID;
    String buyerName;

    View detailsView;
    TextView summary_info;
    View showFullAnalysis;

    vRecyclerView feedbacksHolder;
    FeedbacksAdapter feedbacksAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_feedbacks);
        hasLoadingView();
        hasSwipeRefresh();

        buyerID = getIntent().getStringExtra(Constants.BUYER_ID);
        buyerName = getIntent().getStringExtra(Constants.BUYER_NAME);

        setToolbarTitle(buyerName+" feedbacks");

        //---------

        detailsView = findViewById(R.id.detailsView);
        summary_info = (TextView) findViewById(R.id.summary_info);
        showFullAnalysis = findViewById(R.id.showFullAnalysis);
        showFullAnalysis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnalyseFeedbacksDialog(activity, buyerName+"'s Feedbacks", feedbacksAdapter.getAllItems()).show();
            }
        });

        feedbacksHolder = (vRecyclerView) findViewById(R.id.feedbacksHolder);
        feedbacksHolder.setupVerticalOrientation();
        feedbacksAdapter = new FeedbacksAdapter(activity, new ArrayList<FeedbackDetails>(), true, new FeedbacksAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails2 orderDetails = feedbacksAdapter.getItem(position).getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails2 orderDetails = feedbacksAdapter.getItem(position).getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowOrderDetails(int position) {
                new OrderDetailsDialog(activity).show(feedbacksAdapter.getItem(position).getOrderItemDetails().getOrderID());
            }

            @Override
            public void onShowCustomerOrderHistory(int position) {
                new BuyerOptionsDialog(activity).show(feedbacksAdapter.getItem(position));
            }

            @Override
            public void onShowStockDetails(int position) {
                new StockDetailsDialog(activity).show(feedbacksAdapter.getItem(position).getOrderItemDetails());
            }
        });
        feedbacksHolder.setAdapter(feedbacksAdapter);

        //---------

        getBuyerFeedbacks();
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getBuyerFeedbacks();
    }

    public void getBuyerFeedbacks(){

        detailsView.setVisibility(View.GONE);
        showLoadingIndicator("Loading buyer feedbacks, wait...");
        getBackendAPIs().getFeedbacksAPI().getBuyerFeedbacks(buyerID, new FeedbacksAPI.FeedbacksCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<FeedbackDetails> feedbacksList) {
                if ( status ){
                    displayItems(feedbacksList);
                }else{
                    showReloadIndicator("Unable to buyer feedbacks, try again!");
                }
            }
        });
    }

    public void displayItems(final List<FeedbackDetails> feedbacksList){

        if ( feedbacksList.size() > 0 ) {
            showLoadingIndicator("Displaying, wait...");
            new AsyncTask<Void, Void, Void>() {
                String shortSummaryInfo = "";
                @Override
                protected Void doInBackground(Void... voids) {
                    shortSummaryInfo = new AnalyseFeedbacksMethods(feedbacksList).getRatingsSummaryInfo();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    detailsView.setVisibility(View.VISIBLE);
                    summary_info.setText(Html.fromHtml(shortSummaryInfo));
                    feedbacksAdapter.setItems(feedbacksList);
                    switchToContentPage();
                }
            }.execute();
        }else{
            showNoDataIndicator("No feedbacks");
        }
    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}