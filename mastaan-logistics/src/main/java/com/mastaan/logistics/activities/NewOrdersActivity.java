package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import android.util.Log;
import android.view.View;

import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

public class NewOrdersActivity extends BaseOrderItemsActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();
        hasNavigationButton("Go to Pending Processing");

        startAutoRefreshTimer(10 * 60);         // 10 Minutes Interval

        //--------

        getNewOrdersFromDatabase();
    }

    @Override
    public void onAutoRefreshTimeElapsed() {
        super.onAutoRefreshTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onInactiveTimeElapsed() {
        super.onInactiveTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onNavigationButtonClick() {
        super.onNavigationButtonClick();
        Intent pendinProcessingAct = new Intent(context, PendingProcessingActivity.class);
        startActivity(pendinProcessingAct);
        finish();
    }

    @Override
    public void onReloadPage() {
        super.onReloadPage();
        getNewOrders(false);
    }

    @Override
    public void onReloadFromDatabase() {
        super.onReloadFromDatabase();
        getNewOrdersFromDatabase();
    }

    @Override
    public void onBackgroundReloadPressed() {
        super.onBackgroundReloadPressed();
        getNewOrders(true);
    }


    public void getNewOrdersFromDatabase(){

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        hideNavigationButton();
        showLoadingIndicator("Loading new orders from database, wait...");
        getOrderItemsDatabase().getNewOrders(new OrderItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<OrderItemDetails> itemsList) {
                if (itemsList != null && itemsList.size() > 0) {
                    showLoadingIndicator("Displaying items, wait....");
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), itemsList, localStorageData.getWarehouseLocation(), new GroupingMethods.GroupedOrderItemsListCallback() {
                        @Override
                        public void onComplete(List<GroupedOrdersItemsList> groupedOrdersItemsLists) {
                            displayItems(false, groupedOrdersItemsLists, new ArrayList<OrderDetails>());
                            switchToContentPage();

                            getNewOrders(true);
                        }
                    });

                } else {
                    getNewOrders(false);
                }
            }
        });
    }

    private void getNewOrders(final boolean loadInBackground) {

        if ( loadInBackground ){
            loadingIndicator.setVisibility(View.VISIBLE);
            reloadIndicator.setVisibility(View.GONE);
        }else {
            showLoadingIndicator("Loading new orders, wait...");
        }
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        hideNavigationButton();
        getBackendAPIs().getOrdersAPI().getNewOrderItems(localStorageData.getWarehouseLocation(), new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
                loadingIndicator.setVisibility(View.GONE);
                if (status) {
                    if (loadInBackground) {
                        updateDataAndDisplayItems(groupedOrderItemsLists, ordersList);
                        Log.e("groupedOrderItemsLists",""+groupedOrderItemsLists.size());

                    } else {

//                        if(ordersList.size()<1){
//                            Toast.makeText(activity, "No New Orders", Toast.LENGTH_SHORT).show();
                            Log.e("neworders size",""+groupedOrderItemsLists.size());
//
//                        }else {
                            displayItems(true, groupedOrderItemsLists, ordersList);
//                        }

                    }
                } else {
                    if (loadInBackground) {
                        reloadIndicator.setVisibility(View.VISIBLE);
                        loadingIndicator.setVisibility(View.GONE);
                    } else {
                        showReloadIndicator("Unable to load new orders, try again!");
                    }
                }
            }
        });
    }

}