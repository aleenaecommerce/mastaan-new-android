package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vSpinner;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.AttributeOptionsAdapter;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.backend.models.RequestEditOrderItem;
import com.mastaan.logistics.backend.models.SelectedAttributeDetails;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.AttributeOptionCallbak;
import com.mastaan.logistics.models.AttributeDetails;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

public class EditOrderItemActivity extends MastaanToolbarActivity implements View.OnClickListener {

    String serverTime;

    OrderItemDetails orderItemDetails;
    WarehouseMeatItemDetails originalWarehouseMeatItemDetails;
    WarehouseMeatItemDetails warehouseMeatItemDetails;
    String meatItemID = "";
    String selectedDate = "";

    TextView original_item_info;
    View swithToOriginalItem;

    TextView minimum_weight;
    TextView preparation_time;
    TextView weight_vary_info;
    TextView base_price;


    View disableMeatItemSelection;
    LinearLayout meatItemHolder;
    View attributesLoadingIndicator;
    View optionsHolder;
    LinearLayout weightHolder;
    LinearLayout attributesHolder;

    View confirmButton;
    TextView total_price;

    //------

    List<View> savedAttributesViews = new ArrayList<>();

    double meatItemBasePrice;
    double selectedQuantity;
    double totalPrice;
    double totalAttributesPrice;
    double totalWeightDifference;
    String weightQuantityType = "";
    String weightQuantityUnit = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_order_item);
        hasLoadingView();

        serverTime = localStorageData.getServerTime();

        //-----------

        original_item_info = (TextView) findViewById(R.id.original_item_info);
        swithToOriginalItem = findViewById(R.id.swithToOriginalItem);
        swithToOriginalItem.setOnClickListener(this);

        minimum_weight = (TextView) findViewById(R.id.minimum_weight);
        preparation_time = (TextView) findViewById(R.id.preparation_time);
        weight_vary_info = (TextView) findViewById(R.id.weight_vary_info);
        base_price = (TextView) findViewById(R.id.base_price);

        disableMeatItemSelection = findViewById(R.id.disableMeatItemSelection);
        meatItemHolder = (LinearLayout) findViewById(R.id.meatItemHolder);
        attributesLoadingIndicator = findViewById(R.id.attributesLoadingIndicator);
        optionsHolder = findViewById(R.id.optionsHolder);
        weightHolder = (LinearLayout) findViewById(R.id.weightHolder);
        attributesHolder = (LinearLayout) findViewById(R.id.attributesHolder);

        total_price = (TextView) findViewById(R.id.total_price);

        confirmButton =  findViewById(R.id.confirmButton);
        confirmButton.setOnClickListener(this);

        //---------

        orderItemDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.ORDER_ITEM_DETAILS), OrderItemDetails.class);
        meatItemID = orderItemDetails.getMeatItemDetails().getID();
        selectedDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()));

        getWareHouseMeatItemofMeatItemAndProceed();


    }

    public void getWareHouseMeatItemofMeatItemAndProceed() {

//        hideMenuItems();
        showLoadingIndicator("Loading warehouse meat item details, wait...");
        getBackendAPIs().getMeatItemsAPI().getWareHouseMeatItemOfMeatItem(meatItemID, new MeatItemsAPI.WarehouseMeatItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemDetails warehouse_meat_item_details) {
                if (status) {
                    originalWarehouseMeatItemDetails = warehouse_meat_item_details;
                    warehouseMeatItemDetails = warehouse_meat_item_details;
                    //displayItemDetails();
                    getWarthouseMeatItemAttributesAndProceed();
                } else {
                    showReloadIndicator(statusCode, "Unable to load warehouse meat item details, try again!");
                }
            }
        });
    }

    public void getWarthouseMeatItemAttributesAndProceed(){

        showLoadingIndicator("Loading warehouse meat item details, wait...");
        getBackendAPIs().getMeatItemsAPI().getMeatItemAttributes(warehouseMeatItemDetails.getMeatItemID(), new MeatItemsAPI.MeatItemAttributesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<AttributeDetails> attributesList) {
                if ( status ){
                    if ( originalWarehouseMeatItemDetails.getID().equalsIgnoreCase(warehouseMeatItemDetails.getID())){
                        originalWarehouseMeatItemDetails.getMeatItemDetais().setAttributes(attributesList);
                    }
                    warehouseMeatItemDetails.getMeatItemDetais().setAttributes(attributesList);
                    displayItemDetails();
                }else{
                    showReloadIndicator(statusCode, "Unable to load "+getIntent().getStringExtra(Constants.NAME).toLowerCase()+"'s details, try again!");
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        if ( view == confirmButton ) {

            RequestEditOrderItem requestEditOrderItem = new RequestEditOrderItem(warehouseMeatItemDetails, selectedQuantity, getSelectedAttributes());
            updateOrderItem(requestEditOrderItem);

        }
        else if ( view == swithToOriginalItem ){
            warehouseMeatItemDetails = originalWarehouseMeatItemDetails;
            displayItemDetails();
        }
    }

    //-------- UI Methods

    public void displayItemDetails() {

        attributesHolder.removeAllViews();
        weightHolder.removeAllViews();
        meatItemHolder.removeAllViews();
        savedAttributesViews.clear();

        //------------

        String originalItemInfo = "<b>"+orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize().toUpperCase()+"</b>";
        originalItemInfo += "<br>"+CommonMethods.capitalizeFirstLetter(DateMethods.getDateInFormat(orderItemDetails.getDeliveryDate(), DateConstants.MMM_DD_YYYY));
        originalItemInfo += "<br>" + orderItemDetails.getFormattedQuantity();
        originalItemInfo += " ("+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderItemDetails.getAmountOfItem())+")";
        originalItemInfo += "<br>"+orderItemDetails.getFormattedExtras();

        original_item_info.setText(Html.fromHtml(originalItemInfo));

        if ( warehouseMeatItemDetails.getMeatItemDetais().getID().equals(orderItemDetails.getMeatItemDetails().getID())){
            swithToOriginalItem.setVisibility(View.GONE);
        }else{
            swithToOriginalItem.setVisibility(View.VISIBLE);
        }

        //----------

        meatItemBasePrice = warehouseMeatItemDetails.getSellingPriceForDate(selectedDate);
        totalAttributesPrice = 0;
        totalPrice = meatItemBasePrice + totalAttributesPrice;
        selectedQuantity = warehouseMeatItemDetails.getMeatItemDetais().getMinimumWeight();

        //------

        base_price.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(meatItemBasePrice)+"/"+warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit());

        minimum_weight.setText(CommonMethods.getInDecimalFormat(warehouseMeatItemDetails.getMeatItemDetais().getMinimumWeight())+" "+warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+"s");
        if ( warehouseMeatItemDetails.getMeatItemDetais().getMinimumWeight() == 1 ){
            minimum_weight.setText(CommonMethods.getInDecimalFormat(warehouseMeatItemDetails.getMeatItemDetais().getMinimumWeight())+" "+warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit());
        }

        if (warehouseMeatItemDetails.getMeatItemDetais().getPreparationHours() > 0) {
            preparation_time.setText(DateMethods.getTimeStringFromMinutes((int) (warehouseMeatItemDetails.getMeatItemDetais().getPreparationHours() * 60), "hr", "min"));
            findViewById(R.id.preparationTimeView).setVisibility(View.VISIBLE);
        }else{
            findViewById(R.id.preparationTimeView).setVisibility(View.GONE);
        }

        //---------- ITEM VIEW -----------

        final List<AttributeOptionDetails> itemNames = new ArrayList<>();
        itemNames.add(new AttributeOptionDetails("", "", warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize()));

        meatItemHolder.addView(getAttributeView("isv", "Meat item", "", itemNames, "", 0, null));

        //=========== WEIGHT VIEW ==========//

        if (warehouseMeatItemDetails.getMeatItemDetais().getQuantityType().equalsIgnoreCase("n")) {
            weightQuantityType = "No. of pieces";
            weightQuantityUnit = "piece";
        } else if (warehouseMeatItemDetails.getMeatItemDetais().getQuantityType().equalsIgnoreCase("w")) {
            weightQuantityType = "Weight";
            weightQuantityUnit = "kg";
        } else if (warehouseMeatItemDetails.getMeatItemDetais().getQuantityType().equalsIgnoreCase("s")) {
            weightQuantityType = "No. of sets";
            weightQuantityUnit = "set";
        }

        final List<AttributeOptionDetails> weightsStrings = new ArrayList<>();
        double minimumWeight = warehouseMeatItemDetails.getMeatItemDetais().getMinimumWeight();
        weightsStrings.add(new AttributeOptionDetails(minimumWeight+"", minimumWeight+"", CommonMethods.getInDecimalFormat(minimumWeight) + " " + weightQuantityUnit));

        int selectedKgIndex = 0;
        if (orderItemDetails != null && orderItemDetails.getMeatItemDetails().getID().equals(warehouseMeatItemDetails.getMeatItemID())) {
            weightsStrings.add(new AttributeOptionDetails(orderItemDetails.getQuantity()+"", orderItemDetails.getQuantity()+"", CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity()) + " " + weightQuantityUnit));
            selectedKgIndex = weightsStrings.size()-1;
        }

        weightHolder.addView(getAttributeView("wv", weightQuantityType, weightQuantityUnit, weightsStrings, "", selectedKgIndex, new AttributeOptionCallbak() {
            @Override
            public void onSelect(int previousPosition, int currentPosition, AttributeOptionDetails attributeOptionDetails) {
                selectedQuantity = Double.parseDouble(attributeOptionDetails.getValue());//weights.get(currentPosition);
                updatePrice();
            }
        }));

        //=========== ATTRIBUTES VIEW

        selectedDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()));
        displayAttributesForDate(selectedDate);

        //-------

        switchToContentPage();
    }

    //DAFD
    public void displayAttributesForDate(final String date){
        savedAttributesViews.clear();
        attributesHolder.removeAllViews();

        disableMeatItemSelection.setVisibility(View.VISIBLE);
        attributesLoadingIndicator.setVisibility(View.VISIBLE);
        optionsHolder.setVisibility(View.GONE);
        confirmButton.setVisibility(View.GONE);
        new AsyncTask<Void, Void, Void>() {
            List<AttributeDetails> attributesForDate = new ArrayList<>();

            @Override
            protected Void doInBackground(Void... voids) {
                List<AttributeDetails> allAttributes = warehouseMeatItemDetails.getMeatItemDetais().getAttributes();
                for (int i=0;i<allAttributes.size();i++){
                    AttributeDetails availableAttributeDetails = allAttributes.get(i);
                    AttributeDetails attributeDetailsForDate = new AttributeDetails(availableAttributeDetails.getID(), availableAttributeDetails.getName(), new ArrayList<AttributeOptionDetails>(), availableAttributeDetails.getOptionsDisplayType());

                    List<AttributeOptionDetails> allAttributeOptions = availableAttributeDetails.getAvailableOptions();
                    for (int j=0;j<allAttributeOptions.size();j++){
                        AttributeOptionDetails optionDetailsForDate = allAttributeOptions.get(j).getOptionDetailsForDate(date);
                        attributeDetailsForDate.addOption(optionDetailsForDate);
                    }

                    attributesForDate.add(attributeDetailsForDate);
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                disableMeatItemSelection.setVisibility(View.GONE);
                attributesLoadingIndicator.setVisibility(View.GONE);
                optionsHolder.setVisibility(View.VISIBLE);
                confirmButton.setVisibility(View.VISIBLE);

                for (int i = 0; i< attributesForDate.size(); i++){

                    final AttributeDetails attributeDetails = attributesForDate.get(i);
                    attributesHolder.addView(getAttributeView(attributeDetails.getOptionsDisplayType(), attributeDetails.getName(), weightQuantityUnit, attributeDetails.getAvailableOptions(), "+ ", getSelectedOptionIndex(attributeDetails), new AttributeOptionCallbak() {
                        @Override
                        public void onSelect(int previousPosition, int currentPosition, AttributeOptionDetails attributeOptionDetails) {
                            if (previousPosition != -1) {
                                updateAttributes(attributeDetails.getAvailableOptions().get(currentPosition).getDisabledAttributes(), attributeDetails.getAvailableOptions().get(currentPosition).getDisabledAttributeOptions(), attributeDetails.getAvailableOptions().get(previousPosition).getDisabledAttributes(), attributeDetails.getAvailableOptions().get(previousPosition).getDisabledAttributeOptions());
                                updatePrice();
                            } else {
                                updateAttributes(attributeDetails.getAvailableOptions().get(currentPosition).getDisabledAttributes(), attributeDetails.getAvailableOptions().get(currentPosition).getDisabledAttributeOptions(), new ArrayList<String>(), new ArrayList<String>());
                                updatePrice();
                            }
                        }
                    }));
                }
            }
        }.execute();

    }

    public void updateAttributes(List<String> disabledAttributes, List<String> disabledAttributeOptions, List<String> reEnableAttributes, List<String> reEnableAttributeOptions) {

        for (int i = 0; i < savedAttributesViews.size(); i++) {

            final AttributeDetails attributeDetails = warehouseMeatItemDetails.getMeatItemDetais().getAttributes().get(i);
            final List<AttributeOptionDetails> attributeOptions = warehouseMeatItemDetails.getMeatItemDetais().getAttributes().get(i).getAvailableOptions();

            final View attributeView = savedAttributesViews.get(i);
            final TextView attribute_name = (TextView) attributeView.findViewById(R.id.attribute_name);
            final View disabledIndicator = attributeView.findViewById(R.id.disabledIndicator);
            final vSpinner attribute_options = (vSpinner) attributeView.findViewById(R.id.attribute_options);

            for (int a = 0; a < reEnableAttributes.size(); a++) {
                if (attributeDetails.getID().equals(reEnableAttributes.get(a))) {
                    attribute_name.setTextColor(context.getResources().getColor(R.color.black));
                    attribute_options.setVisibility(View.VISIBLE);
                    attributeView.findViewById(R.id.not_applicable).setVisibility(View.GONE);
                    disabledIndicator.setVisibility(View.GONE);
                    break;
                }
            }

            for (int a = 0; a < disabledAttributes.size(); a++) {
                if (attributeDetails.getID().equals(disabledAttributes.get(a))) {
                    attribute_name.setTextColor(context.getResources().getColor(R.color.disabled_color));
                    attribute_options.setVisibility(View.INVISIBLE);
                    attributeView.findViewById(R.id.not_applicable).setVisibility(View.VISIBLE);
                    disabledIndicator.setVisibility(View.VISIBLE);
                    break;
                }
            }

            AttributeOptionsAdapter adapter = (AttributeOptionsAdapter) attribute_options.getAdapter();
            for (int ao=0;ao<attributeOptions.size();ao++){
                if ( disabledAttributeOptions.contains(attributeOptions.get(ao).getID()) ){
                    adapter.disableItem(attributeOptions.get(ao).getID());
                    // SELECTING FIRST ENABLED OPTION IF CURRENT SELECTED OPTION IS DISABLED AFTER CHANGES
                    if ( adapter.getItem(attribute_options.getSelectedItemPosition()).getStatus() == false
                            || adapter.getItem(attribute_options.getSelectedItemPosition()).isEnabled() == false ){
                        attribute_options.setSelection(adapter.getFirstEnabledItemIndex());
                    }
                }
            }

            for (int ao=0;ao<attributeOptions.size();ao++){
                if ( reEnableAttributeOptions.contains(attributeOptions.get(ao).getID()) ){
                    adapter.enableItem(attributeOptions.get(ao));
                    // SELECTING FIRST ENABLED OPTION IF CURRENT SELECTED OPTION IS DISABLED AFTER CHANGES
                    if ( adapter.getItem(attribute_options.getSelectedItemPosition()).getStatus() == false
                            || adapter.getItem(attribute_options.getSelectedItemPosition()).isEnabled() == false ){
                        attribute_options.setSelection(adapter.getFirstEnabledItemIndex());
                    }
                }
            }
        }
    }

    public void updatePrice() {

        totalAttributesPrice = 0;
        totalWeightDifference = 0;

        for (int i = 0; i < savedAttributesViews.size(); i++) {
            final View attributeView = savedAttributesViews.get(i);
            TextView attribute_name = (TextView) attributeView.findViewById(R.id.attribute_name);
            if (attributeView.findViewById(R.id.disabledIndicator).getVisibility() == View.GONE) {
                try {
                    final vSpinner attribute_options = (vSpinner) attributeView.findViewById(R.id.attribute_options);
                    AttributeOptionDetails optionDetails = (AttributeOptionDetails) attribute_options.getSelectedItem();
                    totalAttributesPrice += optionDetails.getPriceDifference();
                    totalWeightDifference += optionDetails.getWeightDifferencePercentage();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(Constants.LOG_TAG, "Error for " + attribute_name.getText().toString().toUpperCase());
                }
            }
        }
        totalPrice = (meatItemBasePrice + totalAttributesPrice) * selectedQuantity;
        total_price.setText("(" + RS + " " + CommonMethods.getInDecimalFormat(Math.round(totalPrice)) + ")");

        // Updating Weight Dfference Info
        if (totalWeightDifference > 0) {
            weight_vary_info.setVisibility(View.VISIBLE);
            weight_vary_info.setText("The final weight of the item will be approximately " + (selectedQuantity - ((selectedQuantity * totalWeightDifference)) / 100) + " kgs");
        } else {
            if (warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getValue().equals("freshwatersf") || warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getValue().equals("seawatersf")) {
                weight_vary_info.setVisibility(View.VISIBLE);
                weight_vary_info.setText("Final weight of the item will be reduced after cleaning and processing");
            } else {
                weight_vary_info.setVisibility(View.GONE);
            }
        }

        Log.d(Constants.LOG_TAG, "Final price is " + totalPrice);
    }

    public View getAttributeView(final String optionsViewType, final String attributeName, final String weightQuantityUnit, final List<AttributeOptionDetails> attributeOptions, final String priceIndicatorText, final int startValue, final AttributeOptionCallbak callback) {

        List<String> availableOptionsNames = new ArrayList<>();
        for (int j = 0; j < attributeOptions.size(); j++) {
            String optionName = attributeOptions.get(j).getName();
            if (attributeOptions.get(j).getPriceDifference() > 0) {
                optionName = attributeOptions.get(j).getName() + " (" + priceIndicatorText + "" + RS + CommonMethods.getInDecimalFormat(attributeOptions.get(j).getPriceDifference()) + "/" + weightQuantityUnit + ")";
            }
            availableOptionsNames.add(optionName);
        }

        //UI

        final View attributeView = inflater.inflate(R.layout.view_attribute_item, null);
        TextView attribute_name = (TextView) attributeView.findViewById(R.id.attribute_name);
        final vSpinner attribute_options = (vSpinner) attributeView.findViewById(R.id.attribute_options);

        attribute_name.setText(attributeName);

        final AttributeOptionsAdapter adapter = new AttributeOptionsAdapter(context, attributeOptions, weightQuantityUnit, new AttributeOptionsAdapter.Callback() {
            @Override
            public void onShowDisabledReason(int position) {
                //showNotificationDialog("Message", attributeOptions.get(position).getAvailabilityReason());
            }
        });//new ArrayAdapter<String> (this,android.R.layout.simple_dropdown_item_1line, attributeOptions);
        adapter.setPriceIndicatorText(priceIndicatorText);
        //adapter.setDropDownViewResource(R.layout.view_popup_list_item);
        attribute_options.setAdapter(adapter);//
        if (adapter.getItem(startValue).getStatus()) {//isEnabled() ) {
            attribute_options.setSelection(startValue);
        } else {
            attribute_options.setSelection(adapter.getFirstEnabledItemIndex());
        }

        attribute_options.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previousPosition = -1;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( callback != null ){    callback.onSelect(previousPosition, position, adapter.getItem(position));  }
                previousPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        attribute_options.setSpinnerEventsListener(new vSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened() {
                attributeView.findViewById(R.id.attribute_opened_indicator).setVisibility(View.VISIBLE);
            }

            @Override
            public void onSpinnerClosed() {
                attributeView.findViewById(R.id.attribute_opened_indicator).setVisibility(View.GONE);
            }
        });

        if (attributeView.findViewById(R.id.attributeSelector) != null) {
            attributeView.findViewById(R.id.attributeSelector).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ( optionsViewType.equalsIgnoreCase("isv") ){
                        Intent meatItemsAct = new Intent(context, MeatItemsActivity.class);
                        meatItemsAct.putExtra(Constants.FOR_STOCK, true);
                        startActivityForResult(meatItemsAct, Constants.MEAT_ITEMS_ACTIVITY_CODE);
                    }
                    else if ( optionsViewType.equalsIgnoreCase("wv") ){
                        View updateQuantiyDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_update_quantity, null);
                        TextView quantity_unit = (TextView) updateQuantiyDialogView.findViewById(R.id.quantity_unit);
                        TextView message = (TextView) updateQuantiyDialogView.findViewById(R.id.message);
                        final EditText new_quantity = (EditText) updateQuantiyDialogView.findViewById(R.id.new_quantity);

                        if ( selectedQuantity > 0 ) {
                            new_quantity.setText(CommonMethods.getInDecimalFormat(selectedQuantity) + "");
                        }
                        message.setText("Minimum weight: "+CommonMethods.getInDecimalFormat(warehouseMeatItemDetails.getMeatItemDetais().getMinimumWeight())+" "+weightQuantityUnit+"(s)");
                        quantity_unit.setText(warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+"(s)");

                        updateQuantiyDialogView.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String inputText = new_quantity.getText().toString();

                                if ( inputText.length() > 0 && Double.parseDouble(inputText) != 0 ){
                                    activity.closeCustomDialog();

                                    int atIndex = adapter.addItem(new AttributeOptionDetails(inputText, inputText, CommonMethods.getInDecimalFormat(Double.parseDouble(inputText)) + " " + weightQuantityUnit));
                                    attribute_options.setSelection(atIndex, true);
                                }else{
                                    activity.showToastMessage("* Please enter quantity");
                                }
                            }
                        });

                        updateQuantiyDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                activity.closeCustomDialog();
                            }
                        });

                        activity.showCustomDialog(updateQuantiyDialogView);
                    }
                    else {
                        attribute_options.performClick();
                    }
                }
            });
        }

        savedAttributesViews.add(attributeView);

        return attributeView;
    }

    //---------------- GetMethods

    public List<SelectedAttributeDetails> getSelectedAttributes() {

        List<SelectedAttributeDetails> selectedAtrributes = new ArrayList<>();

        for (int i = 0; i < savedAttributesViews.size(); i++) {
            final View attributeView = savedAttributesViews.get(i);

            if (attributeView.findViewById(R.id.disabledIndicator).getVisibility() == View.GONE) {
                final vSpinner attribute_options = (vSpinner) attributeView.findViewById(R.id.attribute_options);
                AttributeOptionDetails optionDetails = (AttributeOptionDetails) attribute_options.getSelectedItem();
                selectedAtrributes.add(new SelectedAttributeDetails(warehouseMeatItemDetails.getMeatItemDetais().getAttributes().get(i).getID(), optionDetails.getID(), optionDetails.getPriceDifference()));
            }
        }

        return selectedAtrributes;
    }

    public int getSelectedOptionIndex(AttributeDetails attributeDetails) {

        int index = 0;

        if (orderItemDetails != null && orderItemDetails.getSelectedAttributes() != null && orderItemDetails.getSelectedAttributes().size() > 0) {
            for (int i = 0; i < orderItemDetails.getSelectedAttributes().size(); i++) {
                if (attributeDetails.getID().equals(orderItemDetails.getSelectedAttributes().get(i).getAttributeID())) {
                    String selectedOptionID = orderItemDetails.getSelectedAttributes().get(i).getOptionDetails().getID();
                    for (int j = 0; j < attributeDetails.getAvailableOptions().size(); j++) {
                        if (selectedOptionID.equals(attributeDetails.getAvailableOptions().get(j).getValue()) || selectedOptionID.equals(attributeDetails.getAvailableOptions().get(j).getID())) {
                            index = j;
                            break;
                        }
                    }
                }
            }
        }
        return index;
    }

    //------------------ Backend

    public void updateOrderItem(final RequestEditOrderItem requestEditOrderItem){

        showLoadingDialog("Updating order item, wait...");
        getBackendAPIs().getOrdersAPI().editOrderItem(orderItemDetails.getID(), requestEditOrderItem, new OrdersAPI.EditOrderItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final OrderItemDetails modifiedOrderItemDetails) {

                if ( status ){
                    getOrderItemsDatabase().addOrUpdateOrderItem(modifiedOrderItemDetails, new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showToastMessage("Order item updated successfully");
                                    Intent resultData = new Intent();
                                    resultData.putExtra(Constants.ORDER_ITEM_DETAILS, new Gson().toJson(modifiedOrderItemDetails));
                                    setResult(RESULT_OK, resultData);
                                    finish();
                                }
                            });
                        }
                    });
                }else{
                    closeLoadingDialog();
                    showSnackbarMessage("Something went wrong while updaing order item, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateOrderItem(requestEditOrderItem);
                        }
                    });
                }
            }
        });
    }


    //====================================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.MEAT_ITEMS_ACTIVITY_CODE && resultCode == RESULT_OK) {
                WarehouseMeatItemDetails selectedItemDetails = new Gson().fromJson(data.getStringExtra(Constants.WAREHOUSE_MEAT_ITEM_DETAILS), WarehouseMeatItemDetails.class);
                if ( selectedItemDetails != null ){
                    warehouseMeatItemDetails = selectedItemDetails;
                    //displayItemDetails();
                    getWarthouseMeatItemAttributesAndProceed();
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}