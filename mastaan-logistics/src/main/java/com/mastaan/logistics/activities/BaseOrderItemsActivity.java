package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.core.view.MenuItemCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.widgets.vAppBarLayout;
import com.aleena.common.widgets.vViewPager;
import com.google.android.material.tabs.TabLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.OrderItemsPagerAdapter;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AnalyseOrderItemsDialog;
import com.mastaan.logistics.fragments.BaseOrdersFragment;
import com.mastaan.logistics.fragments.FilterItemsFragment;
import com.mastaan.logistics.fragments.OrderItemsFragment;
import com.mastaan.logistics.methods.ComparisionMethods;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.ChangeDetails;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.SearchDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh on 13-04-2016.
 */

public class BaseOrderItemsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    MastaanToolbarActivity activity;
    BaseOrdersFragment baseOrdersFragment;

    //public OrderItemsDatabase orderItemsDatabase;
    public ProgressBar loadingIndicator;
    public View reloadIndicator;
    public View displayNewItems;
    public View abortNewItems;

    public DrawerLayout drawerLayout;
    public FilterItemsFragment filterItemsFragment;

    vAppBarLayout appBarLayout;

    boolean isHasContentSwitcher;
    boolean contentSwicherMenuItemVisibility;

    MenuItem searchViewMenuITem;
    MenuItem contentSwitcherMenuITem;
    boolean isHasFilterMenuItem = true;
    MenuItem filterMenuITem;
    boolean isHasActionMenuItem;
    String actionMenuItemName = "";
    MenuItem actionMenuItem;
    SearchView searchView;

    boolean isHasNavigationButton;
    Button navigationButton;
    boolean isHasActionButton;
    Button actionButton;

    ViewSwitcher contentSwitcher;
    TabLayout tabLayout;
    vViewPager viewPager;
    OrderItemsPagerAdapter adapter;

    public List<GroupedOrdersItemsList> groupedOrdersItemsLists;
    //public List<OrderDetails> ordersList;

    View showSummary;

    SearchDetails searchDetails;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void initializeUI(){
        setContentView(R.layout.activity_base_order_items);
        hasLoadingView();
        hasSwipeRefresh();
        activity = this;

        baseOrdersFragment = new BaseOrdersFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.ordersView, baseOrdersFragment).commit();

        filterItemsFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterItemsFragment).commit();
        filterItemsFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, final SearchDetails search_details) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (search_details.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    onClearSearch();
                }
                else if ( search_details.getAction().equalsIgnoreCase(Constants.BACKGROUND_RELOAD) ){
                    onBackgroundReloadPressed();
                }
                else {
                    onPerformSearch(search_details);
                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        //orderItemsDatabase = new OrderItemsDatabase(context);
        //orderItemsDatabase.openDatabase();;

        //--------

        appBarLayout = (vAppBarLayout) findViewById(R.id.appBarLayout);

        loadingIndicator = (ProgressBar) findViewById(R.id.loadingIndicator);
        reloadIndicator = findViewById(R.id.reloadIndicator);
        reloadIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackgroundReloadPressed();
            }
        });
        displayNewItems = findViewById(R.id.displayNewItems);
        abortNewItems = findViewById(R.id.abortNewItems);

        showSummary = findViewById(R.id.showSummary);
        showSummary.setOnClickListener(this);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabTextColors(getResources().getColor(R.color.normal_tab_text_color), getResources().getColor(R.color.selected_tab_text_color));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                onPerformSearch(searchDetails);//getSearchViewText());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });

        viewPager = (vViewPager) findViewById(R.id.viewPager);
        //viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try{    tabLayout.getTabAt(position).select();  }catch (Exception e){e.printStackTrace();}
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));  // To Sync ViewPager with Tabs.

        //------------

    }

    @Override
    public void onClick(View view) {
        if ( view == navigationButton ){
            onNavigationButtonClick();
        }
        else if ( view == actionButton ){
            onActionButtonClick();
        }
        else if ( view == showSummary ){
            showLoadingDialog(true, "Analysing, wait...");
            new AsyncTask<Void, Void, Void>() {
                List<GroupedOrdersItemsList> filterdCategoryGroupedOrderItems = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... voids) {
                    for (int i=0;i<groupedOrdersItemsLists.size();i++){
                        GroupedOrdersItemsList groupedOrdersItemsList = new GroupedOrdersItemsList(groupedOrdersItemsLists.get(i).getCategoryName(), SearchMethods.searchOrderItems(groupedOrdersItemsLists.get(i).getItems(), searchDetails, localStorageData.getServerTime()));
                        if ( groupedOrdersItemsList != null && groupedOrdersItemsList.getItems() != null && groupedOrdersItemsList.getItems().size() > 0 ){
                            filterdCategoryGroupedOrderItems.add(groupedOrdersItemsList);
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    new AnalyseOrderItemsDialog(activity, filterdCategoryGroupedOrderItems).show(false);
                }
            }.execute();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        hideMenuItems();
        onReloadPage();
    }

    public void onBackgroundReloadPressed(){}

    public void onReloadPage(){}

    public void onReloadFromDatabase(){}


    //----------------

    public void updateNewDataInDB(final List<GroupedOrdersItemsList> pastGroupedList, final List<GroupedOrdersItemsList> presentGroupedList, final StatusCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                List<OrderItemDetails> pastItems = GroupingMethods.getOrderItemsListFromGroups(pastGroupedList);
                List<OrderItemDetails> presentItems = GroupingMethods.getOrderItemsListFromGroups(presentGroupedList);

                //List<OrderItemDetails> nonExistedItems = GroupingMethods.getOrderItemsListFromGroups(items);
                List < OrderItemDetails > nonExistedItems = ComparisionMethods.getNonExistedOrderItemsList(pastItems, presentItems);

                // DELETING NON EXISTED ITEMS
                getOrderItemsDatabase().deleteOrderItems(nonExistedItems, null);

                // UPDATING EXISTED DATA AFTER DELETING NON EXISTED DATA
                getOrderItemsDatabase().addOrUpdateOrderItems(ComparisionMethods.getModifiedItems(pastItems, presentItems)/*presentItems*/, null);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }
        }.execute();

    }

    public void updateDataAndDisplayItems(final List<GroupedOrdersItemsList> newGroupedOrderItemsLists, final List<OrderDetails> ordersList) {

        new AsyncTask<Void, Void, Void>() {
            //boolean isAnyChanges = true;
            ChangeDetails changeDetails = new ChangeDetails();
            @Override
            protected Void doInBackground(Void... params) {
                //isAnyChanges = ComparisionMethods.isAnyChanges(groupedOrdersItemsLists, newGroupedOrderItemsLists);
                changeDetails = ComparisionMethods.isAnyChanges(groupedOrdersItemsLists, newGroupedOrderItemsLists);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if ( changeDetails.isAnyChanges() ){
                    // UPDATING NEW DATA IN DB
                    updateNewDataInDB(groupedOrdersItemsLists, newGroupedOrderItemsLists, null);
                }

                if ( changeDetails.isAnyVisibleChanges() ){
                    displayNewItems.setVisibility(View.VISIBLE);
                    displayNewItems.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            displayNewItems.setVisibility(View.GONE);
                            displayItems(false, newGroupedOrderItemsLists, ordersList);

                        }
                    });
                    abortNewItems.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            displayNewItems.setVisibility(View.GONE);
                        }
                    });
                }else{
                    displayNewItems.setVisibility(View.GONE);
                }
            }
        }.execute();

    }

    public void displayItems(boolean storeInDB, List<GroupedOrdersItemsList> newGroupedOrderItemsLists, List<OrderDetails> ordersList) {

        try {   // To Handle Crash issue for displaying data after activity destroyed
            if ( newGroupedOrderItemsLists != null && newGroupedOrderItemsLists.size() > 0) {

                // STORING IN LOCAL DATABASE
                if ( storeInDB ){
                    updateNewDataInDB(groupedOrdersItemsLists, newGroupedOrderItemsLists, null);
                }

                //------
                this.groupedOrdersItemsLists = newGroupedOrderItemsLists;   // Storing Order Items List

//                if ( isHasFilterMenuItem ) {
//                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(R.id.right_drawer));
//                }
                switchToContentPage();
                if (isHasContentSwitcher) {
                    switchToOrderItemsView();
                }
                showMenuItems();

                tabLayout.removeAllTabs();
                for (int i = 0; i < newGroupedOrderItemsLists.size(); i++) {
                    TabLayout.Tab tab = tabLayout.newTab();
                    View rowView = LayoutInflater.from(context).inflate(R.layout.view_tab_list_item, null, false);
                    TextView tabTitle = (TextView) rowView.findViewById(R.id.tabTitle);
                    tabTitle.setText(newGroupedOrderItemsLists.get(i).getCategoryName().toUpperCase());
                    tab.setCustomView(rowView);
                    tabLayout.addTab(tab);
                }
                if (newGroupedOrderItemsLists.size() == 1) {
                    tabLayout.setVisibility(View.GONE);
                } else {
                    tabLayout.setVisibility(View.VISIBLE);
                }

                adapter = new OrderItemsPagerAdapter(getSupportFragmentManager(), newGroupedOrderItemsLists);
                viewPager.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            else {
                tabLayout.setVisibility(View.GONE);
                showNoDataIndicator("No items");
                showNavigationButton();
                hideMenuItems();
            }
        }catch (Exception e){   e.printStackTrace();    }

    }

    public void checkItemsCount(){
        int totalCount = 0;
        if ( groupedOrdersItemsLists != null && groupedOrdersItemsLists.size() > 0 ){
            for (int i=0;i<groupedOrdersItemsLists.size();i++){
                totalCount += groupedOrdersItemsLists.get(i).getItems().size();
            }
        }

        if ( totalCount == 0 ){
            tabLayout.setVisibility(View.GONE);
            showNoDataIndicator("No items");
            showNavigationButton();
            hideMenuItems();
        }
    }

    public List<OrderItemDetails> getCategoryOrderItems(String categoryName) {

        if ( groupedOrdersItemsLists != null && groupedOrdersItemsLists.size() > 0 ){
            for (int i=0;i<groupedOrdersItemsLists.size();i++){
                if ( groupedOrdersItemsLists.get(i).getCategoryName().equalsIgnoreCase(categoryName) ){
                    return groupedOrdersItemsLists.get(i).getItems();
                }
            }
        }
        return new ArrayList<>();
    }

    public void setCategoryOrderItems(String categoryName, List<OrderItemDetails> orderItemsList) {

        if ( groupedOrdersItemsLists != null && groupedOrdersItemsLists.size() > 0 ){
            for (int i=0;i<groupedOrdersItemsLists.size();i++){
                if ( groupedOrdersItemsLists.get(i).getCategoryName().equalsIgnoreCase(categoryName) ){
                    groupedOrdersItemsLists.get(i).setItems(orderItemsList);
                    break;
                }
            }
        }
    }

    //----------------

    public void onPerformSearch(SearchDetails search_details){
        this.searchDetails = search_details;
        try {
            if ( searchDetails != null && searchDetails.isSearchable() ){//&& isShowingContentPage()) {
                if ( isHasContentSwitcher && isShowingOrdersView() ) {
                    baseOrdersFragment.onPerformSearch(searchDetails);
                }else{
                    if (appBarLayout != null) {
                        appBarLayout.expandToolbar();   // To Show Tablayout if its hided already
                    }
                    OrderItemsFragment orderItemsFragment = (OrderItemsFragment) adapter.getCurrentFragment(viewPager);//viewPager.getShowingFragment(getSupportFragmentManager());
                    orderItemsFragment.performSearch(searchDetails);
                }
            }else{
                onClearSearch();
            }
        }catch (Exception e){ e.printStackTrace();  }
    }

    public void onClearSearch(){
        this.searchDetails = new SearchDetails();
        filterItemsFragment.clearSelection();
        try {
            if ( isHasContentSwitcher &&  isShowingOrdersView() ) {
                baseOrdersFragment.onClearSearch();
            }else {
                OrderItemsFragment orderItemsFragment = (OrderItemsFragment) adapter.getCurrentFragment(viewPager);//viewPager.getShowingFragment(getSupportFragmentManager());
                orderItemsFragment.clearSearch();
            }
        }catch (Exception e){   e.printStackTrace();}
    }

    public void onSwitchViews(){
        try {
            if (isShowingContentPage()) {
                if (contentSwitcher.getDisplayedChild() == 0) {
                    switchToOrdersView();
                } else {
                    switchToOrderItemsView();
                }
            }
        }catch (Exception e){ e.printStackTrace();  }
    }

    public void switchToOrderItemsView(){
        filterItemsFragment.clearSelection();
        contentSwitcher.setDisplayedChild(0);
        showFilterMenuItem();
        showSearchView();
        collapseSearchView();
    }

    public boolean isShowingOrderItemsView(){
        return contentSwitcher.getDisplayedChild()==0?true:false;
    }
    public void switchToOrdersView(){
        collapseSearchView();
        filterItemsFragment.clearSelection();
        baseOrdersFragment.displayItems();
        contentSwitcher.setDisplayedChild(1);
        //hideFilterMenuItem();
        //drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
    }
    public boolean isShowingOrdersView(){
        return contentSwitcher.getDisplayedChild()==1?true:false;
    }

    //--------------

    public void setFilterMenuItemVisibility(boolean isVisbile){
        this.isHasFilterMenuItem = isVisbile;
    }

    public void hasActionMenuItem(String actionName){
        this.isHasActionMenuItem = true;
        this.actionMenuItemName = actionName;
    }

    public void onActionMenuItemClick(){

    }

    public void hasNavigationButton(String title){
        isHasNavigationButton = true;
        navigationButton = (Button) findViewById(R.id.navigationButton);
        navigationButton.setOnClickListener(this);
        navigationButton.setText(title);
        navigationButton.setVisibility(View.VISIBLE);
    }

    public void showNavigationButton(){
        if ( isHasNavigationButton && navigationButton != null ){
            navigationButton.setVisibility(View.VISIBLE);
        }
    }
    public void hideNavigationButton(){
        if ( navigationButton != null ){    navigationButton.setVisibility(View.GONE);  }
    }

    public void onNavigationButtonClick(){

    }

    public void hasActionButton(String title){
        isHasActionButton = true;
        actionButton = (Button) findViewById(R.id.actionButton);
        actionButton.setOnClickListener(this);
        actionButton.setText(title);
        actionButton.setVisibility(View.VISIBLE);
    }

    public void showActionButton(){
        if ( isHasActionButton && actionButton != null ){
            actionButton.setVisibility(View.VISIBLE);
        }
    }
    public void hideActionButton(){
        if ( navigationButton != null ){    actionButton.setVisibility(View.GONE);  }
    }

    public void onActionButtonClick(){

    }

    public void setUpContentSwitcher(){
        try {
            contentSwitcher = (ViewSwitcher) findViewById(R.id.contentSwitcher);
        }catch (Exception e){}
    }
    public void enableContentSwitcher(){
        enableContentSwitcher(true);
    }
    public void enableContentSwitcher(boolean showMenuItem){
        if( showMenuItem ){ setUpContentSwitcher(); }
        this.isHasContentSwitcher = true;
        this.contentSwicherMenuItemVisibility = showMenuItem;
    }

    public void disableContentSwitcher(){
        this.isHasContentSwitcher = false;
    }

    public void showMenuItems(){
        showSearchView();
        showFilterMenuItem();
        showContentSwitcherMenuItem();
        showActionMenuItem();
    }

    public void hideMenuItems(){
        hideSearchView();
        hideFilterMenuItem();
        hideContentSwitcherMenuItem();
    }

    public void collapseSearchView(){
        try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
    }

    public void showSearchView(){
        try{    searchViewMenuITem.setVisible(true);    }catch (Exception e){}
    }

    public void hideSearchView(){
        try{
            searchViewMenuITem.setVisible(false);
            collapseSearchView();
        }catch (Exception e){}
    }

    public void showContentSwitcherMenuItem(){
        try{    if (contentSwicherMenuItemVisibility) {contentSwitcherMenuITem.setVisible(true);}    }catch (Exception e){}
    }

    public void hideContentSwitcherMenuItem(){
        try{    contentSwitcherMenuITem.setVisible(false);    }catch (Exception e){}
    }

    public void showFilterMenuItem(){
        try{
            if ( isHasFilterMenuItem ){
                filterMenuITem.setVisible(true);
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(R.id.right_drawer));
            }
        }catch (Exception e){}
    }

    public void hideActionMenuItem(){
        try{    actionMenuItem.setVisible(false);    }catch (Exception e){}
    }

    public void showActionMenuItem(){
        try{
            if ( isHasActionMenuItem ){
                actionMenuItem.setTitle(actionMenuItemName);
                actionMenuItem.setVisible(true);
            }
        }catch (Exception e){}
    }

    public void hideFilterMenuItem(){
        try{
            filterMenuITem.setVisible(false);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        }catch (Exception e){}
    }

    public String getSearchViewText(){
        String searchViewText = "";
        try{ searchViewText = searchView.getQuery().toString();      }catch (Exception e){}
        return searchViewText;
    }

    //=====================

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_base_order_items, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        contentSwitcherMenuITem = menu.findItem(R.id.action_switcher);
        filterMenuITem = menu.findItem(R.id.action_filter);
        actionMenuItem = menu.findItem(R.id.action_menu_item);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //filterItemsFragment.updateCustomerName(query);
//                if ( searchDetails != null && searchDetails.isSearchable() ) {
//                    searchDetails.setCustomerName(query);
//                    onPerformSearch(searchDetails);
//                }else{
                    SearchDetails searchDetails = new SearchDetails().setConsiderAtleastOneMatch(query);
                    onPerformSearch(searchDetails);
//                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){
        }
        else if ( item.getItemId() == R.id.action_switcher){
            onSwitchViews();
        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else if ( item.getItemId() == R.id.action_menu_item ){
            onActionMenuItemClick();
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        if (outState.isEmpty()) {
            // Work-around for a pre-Android 4.2 bug
            outState.putBoolean("bug:fix", true);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            super.onBackPressed();
        }
    }


}








//====================  UNUSED CODE =======================

//    public void updateOrderItem(OrderItemDetails modifiedOrderItemDetails){
//        try{
//            if ( groupedOrdersItemsLists != null ){
//                boolean isCategoryExists = false;
//                for (int i=0;i<groupedOrdersItemsLists.size();i++) {
//                    if ( groupedOrdersItemsLists.get(i).getItems().get(0).getMeatItemDetails().getCategoryIdentifier().equals(modifiedOrderItemDetails.getMeatItemDetails().getCategoryIdentifier()) ){
//                        isCategoryExists = true;
//                        break;
//                    }
//                }
//
//                if ( isCategoryExists ) {
//                    for (int i = 0; i < groupedOrdersItemsLists.size(); i++) {
//                        int containedIndex = -1;
//                        for (int j = 0; j < groupedOrdersItemsLists.get(i).getItems().size(); j++) {
//                            if (groupedOrdersItemsLists.get(i).getItems().get(j).getID().equals(modifiedOrderItemDetails.getID())) {
//                                containedIndex = j;
//                            }
//                        }
//                        if (containedIndex != -1) {
//                            // UDPATING ITEM IN CATEGORY LIST ITEMS
//                            if (groupedOrdersItemsLists.get(i).getItems().get(containedIndex).getMeatItemDetails().getCategoryIdentifier().equals(modifiedOrderItemDetails.getMeatItemDetails().getCategoryIdentifier())) {
//                                groupedOrdersItemsLists.get(i).getItems().set(containedIndex, modifiedOrderItemDetails);
//                            }
//                            // REMOVING FROM CATEGORY LIST ITEMS
//                            else {
//                                groupedOrdersItemsLists.get(i).getItems().remove(containedIndex);
//                            }
//                        } else {
//                            // ADDING TO CATEGORY LIST ITMES
//                            if (groupedOrdersItemsLists.get(i).getItems().get(0).getMeatItemDetails().getCategoryIdentifier().equals(modifiedOrderItemDetails.getMeatItemDetails().getCategoryIdentifier())) {
//                                groupedOrdersItemsLists.get(i).getItems().add(modifiedOrderItemDetails);
//                                //UPDATING UI
//                                try {
//                                    OrderItemsFragment orderItemsFragment = (OrderItemsFragment) adapter.instantiateItem(viewPager, i);//adapter.getCurrentFragment(viewPager);//viewPager.getShowingFragment(getSupportFragmentManager());
//                                    orderItemsFragment.notifyDataChange();
//                                } catch (Exception e) {
//                                }
//                            }
//                        }
//                    }
//                }else{
//                    /*List<OrderItemDetails> orderItemsList = new ArrayList<>();
//                    orderItemsList.add(modifiedOrderItemDetails);
//                    groupedOrdersItemsLists.add(new GroupedOrdersItemsList(modifiedOrderItemDetails.getMeatItemDetails().getCategoryDetails().getParentCategoryName(), orderItemsList));
//                    showLoadingIndicator("Displaying items, wait...");
//                    List<GroupedOrdersItemsList> newList = new ArrayList<>();
//                    for (int i=0;i<groupedOrdersItemsLists.size();i++){
//                        if ( groupedOrdersItemsLists.get(i) != null && groupedOrdersItemsLists.get(i).getItems().size() > 0 ){
//                            newList.add(groupedOrdersItemsLists.get(i));
//                        }
//                    }
//                    Collections.sort(newList, new Comparator<GroupedOrdersItemsList>() {
//                        public int compare(GroupedOrdersItemsList item1, GroupedOrdersItemsList item2) {
//                            return item1.getCategoryName().compareToIgnoreCase(item2.getCategoryName());
//                        }
//                    });
//                    displayItems(false, groupedOrdersItemsLists, new ArrayList<OrderDetails>());*/
//                    onReloadFromDatabase();
//                }
//            }
//        }catch (Exception e){
//            showToastMessage("Something went wrong while updating ui, pleae refresh page to view latest data");
//        }
//    }
