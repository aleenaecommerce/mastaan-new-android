package com.mastaan.logistics.activities;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.OrdersAdapter;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.backend.models.RequestUpdateOrderStatus;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.ECODDialog;
import com.mastaan.logistics.dialogs.FollowupDetailsDialog;
import com.mastaan.logistics.dialogs.MoneyCollectionDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;

import java.util.ArrayList;
import java.util.List;

public class PastPendingOrdersActivity extends MastaanToolbarActivity {

    vRecyclerView ordersHolder;
    OrdersAdapter ordersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_pending_orders);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        ordersHolder = (vRecyclerView) findViewById(R.id.ordersHolder);
        setupHolder();

        //---------

        getPastPendingOrders();
    }

    public void setupHolder(){

        ordersHolder.setupVerticalOrientation();
        ordersAdapter = new OrdersAdapter(activity, new ArrayList<OrderDetails>(), new OrdersAdapter.Callback() {
            @Override
            public void onItemClick(final int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);

                final String[] listOptions = new String[]{Constants.MARK_AS_DELIVERED, Constants.MARK_AS_FAILED};
                showListChooserDialog(listOptions, new ListChooserCallback() {
                    @Override
                    public void onSelect(int listPosition) {

                        if ( listOptions[listPosition].equalsIgnoreCase(Constants.MARK_AS_DELIVERED) ){
                            if ( orderDetails.getTotalCashAmount() == 0 ){
                                updatOrderStatus(position, new RequestUpdateOrderStatus(Constants.DELIVERED, 0, "1", null));
                            }
                            else{
                                View amountCollectedDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_amount_collected, null);
                                final EditText amount_collected = (EditText) amountCollectedDialogView.findViewById(R.id.amount_collected);
                                amountCollectedDialogView.findViewById(R.id.message).setVisibility(View.GONE);
                                final RadioButton collectedAmount = (RadioButton) amountCollectedDialogView.findViewById(R.id.collectedAmount);
                                final RadioButton returnedAmount = (RadioButton) amountCollectedDialogView.findViewById(R.id.returnedAmount);
                                final CheckBox paytmWallet = (CheckBox) amountCollectedDialogView.findViewById(R.id.paytmWallet);
                                final EditText paytm_transaction_id = (EditText) amountCollectedDialogView.findViewById(R.id.paytm_transaction_id);
                                collectedAmount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                        paytmWallet.setChecked(false);
                                        if ( isChecked ){
                                            paytmWallet.setVisibility(View.VISIBLE);
                                        }else{
                                            paytmWallet.setVisibility(View.GONE);
                                        }
                                    }
                                });
                                paytmWallet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                                        if ( isChecked ){
                                            paytm_transaction_id.setVisibility(View.VISIBLE);
                                        }else{
                                            paytm_transaction_id.setVisibility(View.GONE);
                                        }
                                    }
                                });

                                amountCollectedDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        closeCustomDialog();
                                    }
                                });

                                amountCollectedDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (collectedAmount.isChecked() || returnedAmount.isChecked()) {
                                            String amountCollected = amount_collected.getText().toString();
                                            if (amountCollected.length() > 0) {
                                                if ( paytmWallet.isChecked() == false || paytm_transaction_id.getText().toString().trim().length() > 0 ){
                                                    closeCustomDialog();
                                                    double amount = Double.parseDouble(amountCollected);
                                                    if (returnedAmount.isChecked()) {
                                                        amount = -1 * amount;
                                                    }
                                                    if ( paytmWallet.isChecked() ){
                                                        updatOrderStatus(position, new RequestUpdateOrderStatus(Constants.DELIVERED, amount, "9", paytm_transaction_id.getText().toString()));
                                                    }else{
                                                        updatOrderStatus(position, new RequestUpdateOrderStatus(Constants.DELIVERED, amount, "1", null));
                                                    }
                                                }else{
                                                    showToastMessage("* Please enter paytm transaction id");
                                                }
                                            } else {
                                                showToastMessage("* Please enter amount collected.");
                                            }
                                        } else {
                                            showToastMessage("* Please select collection type.");
                                        }

                                    }
                                });
                                showCustomDialog(amountCollectedDialogView);
                            }
                        }
                        else if ( listOptions[listPosition].equalsIgnoreCase(Constants.MARK_AS_FAILED) ){
                            showChoiceSelectionDialog("Confirm!", Html.fromHtml("Are you sure mark <b>" + CommonMethods.capitalizeFirstLetter(orderDetails.getCustomerName()) + "'s</b> order as failed?"), "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if ( choiceName.equalsIgnoreCase("YES")){
                                        updatOrderStatus(position, new RequestUpdateOrderStatus(Constants.FAILED));
                                    }
                                }
                            });
                        }
                    }
                });
            }

            @Override
            public void onShowDirections(int position) {

            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowBuyerHistory(int position) {

            }

            @Override
            public void onShowReferralBuyerDetails(int position) {

            }

            @Override
            public void onShowMoneyCollectionDetails(int position) {
                new MoneyCollectionDetailsDialog(activity).showForOrder(ordersAdapter.getItem(position).getID());
            }

            @Override
            public void onCheckPaymentStatus(final int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                showLoadingDialog("Checking payment status, wait...");
                getBackendAPIs().getPaymentAPI().checkOrderPaymentStatus(orderDetails.getID(), new PaymentAPI.CheckOrderPaymentStatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, OrderDetails order_details) {
                        closeLoadingDialog();
                        if (status){
                            orderDetails.setTotalAmount(order_details.getTotalAmount());
                            orderDetails.setTotalCashAmount(order_details.getTotalCashAmount());
                            orderDetails.setTotalOnlinePaymentAmount(order_details.getTotalOnlinePaymentAmount());
                            ordersAdapter.updateItem(position, orderDetails);
                        }else{
                            showToastMessage("Something went wrong while checking payment status.\nPlease try again!");
                        }
                    }
                });
            }

            @Override
            public void onRequestOnlinePayment(final int position) {
                new ECODDialog(activity).showForOrder(ordersAdapter.getItem(position), ordersAdapter.getItem(position).getTotalCashAmount(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if ( status ){
                            activity.showToastMessage("Payment success");
                            onCheckPaymentStatus(position);
                        }else{
                            activity.showToastMessage("Payment failed");
                        }
                    }
                });
            }

            @Override
            public void onShowFollowupResultedInOrder(int position) {
                new FollowupDetailsDialog(activity).show(ordersAdapter.getItem(position).getFollowupResultedInOrder());
            }

            @Override
            public void onPrintBill(int position) {

            }

            @Override
            public void onOrderItemClick(int position, int item_position) {

            }

            @Override
            public void onShowOrderItemStockDetails(int position, int item_position) {
                new StockDetailsDialog(activity).show(ordersAdapter.getItem(position).getOrderedItems().get(item_position));
            }
        }).showCheckPaymentStatus().showRequestOnlinePayment();
        ordersHolder.setAdapter(ordersAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPastPendingOrders();
    }

    public void getPastPendingOrders(){
        showLoadingIndicator("Loading past pending orders, wait...");
        getBackendAPIs().getOrdersAPI().getPastPendingOrders(new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
                if ( status ){
                    if ( ordersList.size() > 0 ) {
                        ordersAdapter.setItems(ordersList);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("No past pending orders.");
                    }
                }else{
                    showReloadIndicator("Unable to load past pending orders, try again!");
                }
            }
        });
    }

    public void updatOrderStatus(final int position, final RequestUpdateOrderStatus requestUpdateOrderStatus){

        showLoadingDialog("Updating order status as "+requestUpdateOrderStatus.getStatus()+", wait...");
        getBackendAPIs().getOrdersAPI().updateOrderStatus(ordersAdapter.getItem(position).getID(), requestUpdateOrderStatus, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Order status marked as "+requestUpdateOrderStatus.getStatus()+" successfully");
                    ordersAdapter.deleteItem(position);
                    if ( ordersAdapter.getItemCount() == 0 ){
                        showNoDataIndicator("No past pending orders");
                    }
                }else{
                    showChoiceSelectionDialog("Failure!", "Something went wrong while updating order status as " + requestUpdateOrderStatus.getStatus() + "\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                updatOrderStatus(position, requestUpdateOrderStatus);
                            }
                        }
                    });

                }
            }
        });

    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}