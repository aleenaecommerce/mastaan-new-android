package com.mastaan.logistics.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.dialogs.DateAndTimePickerDialog;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DateAndTimePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.flows.OrdersActionFlow;
import com.mastaan.logistics.interfaces.UsersCallback;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class OrderDetailsActivity extends BaseOrderItemsActivity {

    MenuItem actionAssignDeliveryBoy;
    MenuItem actionChangeDeliveryDate;
    MenuItem actionUpdateDiscount;

    String orderID;
    OrderDetails2 orderDetails;

    boolean isDoneAnyActions;

    UserDetails userDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();

        orderID = getIntent().getStringExtra(Constants.ORDER_ID);

        userDetails = localStorageData.getUserDetails();

        //---------

        getOrderItems(orderID);

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();
    }

    @Override
    public void onReloadPage() {
        super.onReloadPage();
        getOrderItems(orderID);
    }

    private void getOrderItems(String orderID) {

        hideMenuItems();
        showLoadingIndicator("Loading Order items list, wait...");
        getBackendAPIs().getOrdersAPI().getOrderItems(orderID, new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
                if (status) {
                    orderDetails = groupedOrderItemsLists.get(0).getItems().get(0).getOrderDetails();

                    showMenuItems();
                    displayItems(true, groupedOrderItemsLists, ordersList);

                    //------------
                    boolean isAllItemsProcessed = true;
                    for (int i = 0; i < groupedOrderItemsLists.size(); i++) {
                        List<OrderItemDetails> orderItems = groupedOrderItemsLists.get(i).getItems();
                        for (int j = 0; j < orderItems.size(); j++) {
                            if (orderItems.get(j).getStatusString().equalsIgnoreCase("PICKUP PENDING") == false && orderItems.get(j).getStatusString().equalsIgnoreCase("DELIVERING") == false) {
                                isAllItemsProcessed = false;
                                break;
                            }
                        }
                    }

                    if (isAllItemsProcessed == false) {
                        if (actionAssignDeliveryBoy != null) {
                            actionAssignDeliveryBoy.setVisible(false);
                        }
                    }
                } else {
                    showReloadIndicator("Unable to load Order items list, try again!");
                }
            }
        });

    }

    private void assignToDeliveryBoy(final String deliveryBoyID){

        showLoadingDialog("Assigning to delivery boy, wait...");
        getBackendAPIs().getUsersAPI().assignDeliveryBoyToOrder(orderID, deliveryBoyID, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    isDoneAnyActions = true;
                    showToastMessage("Delivery boy assigned successfully");
                    onReloadPressed();
                } else {
                    if (statusCode == 500) {
                        showNotificationDialog("Failure!", message, new ActionCallback() {
                            @Override
                            public void onAction() {
                                onReloadPressed();
                            }
                        });
                    } else {
                        showChoiceSelectionDialog(false, "Failure!", "Something went wrong while assigning delivery boy.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                assignToDeliveryBoy(deliveryBoyID);
                            }
                        });
                    }
                }
            }
        });
    }


    private void updateOrderDeliveryDate(final String date, final String time){

        showLoadingDialog("Changing delivery date, wait...");
        getBackendAPIs().getOrdersAPI().updateOrderDeliveryDate(orderID, date, time, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    isDoneAnyActions = true;
                    showToastMessage("Delivery date changed successfully");
                    onReloadPressed();
                } else {
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while changing delivery date.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            updateOrderDeliveryDate(date, time);
                        }
                    });
                }
            }
        });
    }


    //=====================

    public void showMenuItems(){
        boolean isCounterSaleOrder = false;
        //try{
            if ( orderDetails.isCounterSaleOrder() ){
                isCounterSaleOrder = true;
            }
        //}catch (Exception e){}

        // SHOWING MENU OPTIONS ONLY FOR NORMAL ORDERS
        if ( isCounterSaleOrder == false ){
            if ( actionAssignDeliveryBoy != null ) {
                actionAssignDeliveryBoy.setVisible(true);
            }
            if (  userDetails.isSuperUser() || userDetails.isProcessingManager() ) {
                if ( actionChangeDeliveryDate != null ) {
                    actionChangeDeliveryDate.setVisible(true);
                }
                if ( actionUpdateDiscount != null ){
                    actionUpdateDiscount.setVisible(true);
                }
            }

        }
    }

    public void hideMenuItems(){
        if ( actionAssignDeliveryBoy != null ) {
            actionAssignDeliveryBoy.setVisible(false);
        }
        if ( actionChangeDeliveryDate != null ) {
            actionChangeDeliveryDate.setVisible(false);
        }
        if ( actionUpdateDiscount != null ){
            actionUpdateDiscount.setVisible(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_order_details, menu);
        actionAssignDeliveryBoy = menu.findItem(R.id.action_assign_delivery_boy);
        actionChangeDeliveryDate = menu.findItem(R.id.action_change_delivery_date);
        actionUpdateDiscount = menu.findItem(R.id.action_update_discount);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_assign_delivery_boy){

            showLoadingDialog("Loading available delivery boys, wait...");
            getBackendAPIs().getEmployeesAPI().getAvailableDeliveryBoys(new UsersCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, final List<UserDetails> deliveryBoysList) {
                    closeLoadingDialog();
                    if (status) {
                        if (deliveryBoysList.size() > 0) {

                            List<String> deliveryBoysStrings = new ArrayList<String>();
                            int hightlightDeliveryBoyIndex = -1;
                            for (int i=0;i<deliveryBoysList.size();i++){
                                String displayName = deliveryBoysList.get(i).getFullName();
                                if ( deliveryBoysList.get(i).getJobsDoneToday() > 0 ){
                                    displayName += "\n( Jobs: "+ CommonMethods.getInDecimalFormat(deliveryBoysList.get(i).getJobsDoneToday());
                                }
                                if ( deliveryBoysList.get(i).getDistanceTravelledToday() > 0 ){
                                    if ( deliveryBoysList.get(i).getJobsDoneToday() == 0 ){ displayName += "\n( ";}
                                    else{   displayName += ", ";    }
                                    if ( deliveryBoysList.get(i).getDistanceTravelledToday() == 1000 ) {
                                        displayName += "Dis: 1km";
                                    }else{
                                        displayName += "Dis: " + CommonMethods.getInDecimalFormat(deliveryBoysList.get(i).getDistanceTravelledToday() / 1000) + "kms";
                                    }
                                }
                                if ( deliveryBoysList.get(i).getJobsDoneToday() > 0 || deliveryBoysList.get(i).getDistanceTravelledToday() > 0 ){
                                    displayName += " )";
                                }

                                deliveryBoysStrings.add(displayName);
                            }

                            ((vToolBarActivity)activity).showListChooserDialog("Select delivery boy!", deliveryBoysStrings, new ListChooserCallback() {
                                @Override
                                public void onSelect(int position) {
                                    UserDetails selectedDeliveryBoyDetails = deliveryBoysList.get(position);
                                    assignToDeliveryBoy(selectedDeliveryBoyDetails.getID());//, selectedDeliveryBoyDetails.getFullName());

                                }
                            });
                        } else {
                            activity.showNotificationDialog("Alert!", "No delivery boys are available or not checked in now.");
                        }
                    } else {
                        showToastMessage("Something went wrong while getting available delivery boys list. Please try again!");
                    }
                }
            });
        }
        else if ( item.getItemId() == R.id.action_change_delivery_date){
            if (  userDetails.isSuperUser() || userDetails.isProcessingManager() ) {
                /*new DateAndTimePickerDialog(context).showDatePickerDialog("Select delivery date", orderDetails.getDeliveryDate(), new DateAndTimePickerCallback() {
                    @Override
                    public void onSelect(String fullDate, int day, int month, int year, String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
                        updateOrderDeliveryDate(DateMethods.getDateInFormat(fullDate, DateConstants.DD_MMM_YYYY), fullTimein12HrFormat);
                    }
                });*/
                showToastMessage("Temporarily disabled, meanwhile please change delivery date of items individually");
            }
        }

        else if ( item.getItemId() == R.id.action_update_discount){
            if (  userDetails.isSuperUser() || userDetails.isProcessingManager() ) {
                new OrdersActionFlow(activity).applyDiscount(GroupingMethods.getOrderItemsListFromGroups(groupedOrdersItemsLists), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        isDoneAnyActions = true;
                        onReloadPressed();
                    }
                });
            }
        }
        else{
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if ( isDoneAnyActions ){
            setResult(Activity.RESULT_OK);
            finish();
        }else{
            super.onBackPressed();
        }
    }
}