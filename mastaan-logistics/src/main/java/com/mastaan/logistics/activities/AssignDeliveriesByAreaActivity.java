package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ViewSwitcher;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vMapFragment;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.DeliveryZonesAdapter;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.DeliveryAreaDetails;
import com.mastaan.logistics.models.DeliveryZoneDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 30/03/17.
 */

public class AssignDeliveriesByAreaActivity extends MastaanToolbarActivity {

    String searchQuery;
    MenuItem switchViewMenuItem;
    MenuItem searchViewMenuITem;
    SearchView searchView;

    ViewSwitcher contentSwitcher;
    vRecyclerView itemsHolder;
    GoogleMap googleMap;

    DeliveryZonesAdapter deliveryZonesAdapter;

    List<DeliveryZoneDetails> originalItemsList = new ArrayList<>();

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_deliveries_by_area);
        hasLoadingView();
        hasSwipeRefresh();

        userDetails = localStorageData.getUserDetails();

        //-----------------

        contentSwitcher = (ViewSwitcher) findViewById(R.id.contentSwitcher);

        itemsHolder = (vRecyclerView) findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        deliveryZonesAdapter = new DeliveryZonesAdapter(activity, new ArrayList<DeliveryZoneDetails>(), new DeliveryZonesAdapter.CallBack() {
            @Override
            public void onItemClick(final int position, final int areaPosition) {
                Intent assignDeliveriesAct = new Intent(context, AssignDeliveriesActivity.class);
                assignDeliveriesAct.putExtra(Constants.ACTION_TYPE, Constants.DELIVERY_GROUP_DELIVERIES);
                assignDeliveriesAct.putExtra(Constants.DELIVERY_AREA_ID, deliveryZonesAdapter.getItem(position).getAreas().get(areaPosition).getID());
                startActivity(assignDeliveriesAct);
                /*switchToMapView();
                CameraPosition.Builder cameraBuilder = new CameraPosition.Builder()
                        .target(deliveryZonesAdapter.getItem(position).getCenterLatLng())      // Sets the center of the google_map to Mountain View
                        .zoom(12)
                        .bearing(0)                         // Sets the orientation of the camera to east
                        .tilt(0);                            // Sets the tilt of the camera to 30 degrees
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraBuilder.build()));
                try {
                    markersList.get(position).showInfoWindow();
                }catch (Exception e){}*/
            }
        });
        itemsHolder.setAdapter(deliveryZonesAdapter);

       //-----

        //LOADING AREAS AFTER MENU CREATED
    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        vMapFragment map_fragment = ((vMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment));
        map_fragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap google_Map) {
                googleMap = google_Map;

                getPendingAreasList();
            }
        });
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPendingAreasList();
    }

    public void getPendingAreasList(){

        switchViewMenuItem.setVisible(false);
        searchViewMenuITem.setVisible(false);
        showLoadingIndicator("Loading pending delivery areas, wait..");
        getBackendAPIs().getOrdersAPI().getPendingDeliveryZones(new OrdersAPI.DeliveryZonesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<DeliveryZoneDetails> areasList) {
                if (status == true) {
                    originalItemsList = areasList;
                    display(areasList);
                } else {
                    showReloadIndicator(statusCode, "Unable to load pending delivery areas, try again!");
                }
            }
        });
    }

    List<Marker> markersList = new ArrayList<>();
    public void display(final List<DeliveryZoneDetails> areasList){
        googleMap.clear();
        markersList.clear();

        if (areasList.size() > 0) {
            switchViewMenuItem.setVisible(true);
            searchViewMenuITem.setVisible(true);
            switchToItemsView();

            deliveryZonesAdapter.setItems(areasList);

            for (int i=0;i<areasList.size();i++){
                DeliveryZoneDetails deliveryZoneDetails = areasList.get(i);
                for (int j=0;j<deliveryZoneDetails.getAreas().size();j++){
                    DeliveryAreaDetails deliveryAreaDetails = deliveryZoneDetails.getAreas().get(j);
                    LatLng latLng = deliveryAreaDetails.getCenterLatLng();
                    Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(deliveryAreaDetails.getID()).snippet(deliveryAreaDetails.getCount()+" item(s)"/* in "+ deliveryZoneDetails.getLatLngs().size()+" area(s)"*/).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)).draggable(true));
                    marker.setTag(i+","+j);
                    markersList.add(marker);
                }
            }
            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    try{
                        String []postions = CommonMethods.getArrayFromString(marker.getTag().toString());
                        Intent assignDeliveriesAct = new Intent(context, AssignDeliveriesActivity.class);
                        assignDeliveriesAct.putExtra(Constants.ACTION_TYPE, Constants.DELIVERY_GROUP_DELIVERIES);
                        assignDeliveriesAct.putExtra(Constants.DELIVERY_AREA_ID, areasList.get(Integer.parseInt(postions[0])).getAreas().get(Integer.parseInt(postions[1])).getID());
                        startActivity(assignDeliveriesAct);
                    }catch (Exception e){}
                }
            });
        } else {
            showNoDataIndicator("No pending delivery areas");
        }
    }

    //-------------

    public void switchToItemsView(){
        switchToContentPage();
        contentSwitcher.setDisplayedChild(0);
    }
    public void switchToMapView(){
        switchToContentPage();
        contentSwitcher.setDisplayedChild(1);
        try {
            LatLngBounds.Builder latlngBoundsBuilder = new LatLngBounds.Builder();
            for (Marker marker : markersList) {
                latlngBoundsBuilder.include(marker.getPosition());
            }
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latlngBoundsBuilder.build(), getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels, 300));
        }catch (Exception e){}
    }

    //--------------

    public void onPerformSearch(final String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchQuery = search_query;
            showLoadingIndicator("Searching for '"+search_query+"', wait...");
            new AsyncTask<Void, Void, Void>() {
                List<DeliveryZoneDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... voids) {
                    for (int i=0;i<originalItemsList.size();i++){
                        if ( originalItemsList.get(i).getID().toLowerCase().contains(searchQuery.toLowerCase()) ){
                            filteredList.add(originalItemsList.get(i));
                        }else{
                            for (int j=0;j<originalItemsList.get(i).getAreas().size();j++){
                                if ( originalItemsList.get(i).getAreas().get(j).getID().toLowerCase().contains(searchQuery.toLowerCase()) ){
                                    filteredList.add(originalItemsList.get(i));
                                    break;
                                }
                            }
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( searchQuery.equalsIgnoreCase(search_query) ){
                        if ( filteredList.size() > 0 ){
                            display(filteredList);
                        }else{
                            showNoDataIndicator("Nothing found for '"+searchQuery+"'");
                        }
                    }
                }
            }.execute();
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        display(originalItemsList);
    }

    //===============

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_assign_deliveries, menu);

        switchViewMenuItem = menu.findItem(R.id.action_switcher);
        searchViewMenuITem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_switcher ){
            if ( contentSwitcher.getDisplayedChild() == 0 ){
                switchToMapView();
            }else{
                switchToItemsView();
            }

        }else if ( menuItem.getItemId() == R.id.action_search ){

        }else {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if ( contentSwitcher.getDisplayedChild() == 1 ){
            switchToItemsView();
        }else {
            super.onBackPressed();
        }
    }
}
