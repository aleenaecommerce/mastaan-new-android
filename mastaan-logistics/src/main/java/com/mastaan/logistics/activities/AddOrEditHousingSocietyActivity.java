package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.activities.LocationSelectorActivity;
import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateHousingSociety;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.HousingSocietyDetails;

/**
 * Created by Venkatesh Uppu on 27/09/18.
 */

public class AddOrEditHousingSocietyActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String actionType = Constants.ADD;

    vTextInputLayout name;
    vTextInputLayout position;

    View done;

    HousingSocietyDetails housingSocietyDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_housing_society);

        if ( getIntent().getStringExtra(Constants.ACTION_TYPE) != null ){
            actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        }
        setToolbarTitle(actionType+" Housing Society");

        //------------

        name = findViewById(R.id.name);
        position = findViewById(R.id.position);
        position.getEditText().setOnClickListener(this);

        done = findViewById(R.id.done);
        done.setOnClickListener(this);

        //-----------

        if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
            housingSocietyDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.HOUSING_SOCIETY_DETAILS), HousingSocietyDetails.class);

            name.setText(housingSocietyDetails.getName());
            position.setText(housingSocietyDetails.getLatLngString());
        }

    }

    @Override
    public void onClick(View view) {

        if ( view == position.getEditText() ){
            Intent selectLocationAct = new Intent(context, LocationSelectorActivity.class);
            selectLocationAct.putExtra(ConstantsCommonLibrary.LATLNG, position.getText());
            startActivityForResult(selectLocationAct, Constants.LOCATION_SELECTION_ACTIVITY_CODE);
        }
        else if ( view == done ){
            String eName = name.getText();
            name.checkError("* Enter name");
            String ePosition = position.getText();
            position.checkError("* Enter position latlng");

            if ( eName.length() > 0 && ePosition.length() > 0 ){
                LatLng eLatLng = LatLngMethods.getLatLngFromString(ePosition);

                if ( eLatLng.latitude !=0 && eLatLng.longitude != 0 ){
                    RequestAddOrUpdateHousingSociety requestAddOrUpdateHousingSociety = new RequestAddOrUpdateHousingSociety(eName, eLatLng);

                    if ( actionType.equalsIgnoreCase(Constants.ADD) ){
                        addHousingSociety(requestAddOrUpdateHousingSociety);
                    }
                    else if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                        updateHousingSociety(requestAddOrUpdateHousingSociety);
                    }
                }
                else{
                    position.showError("* Enter valid position");
                }
            }
        }
    }

    public void addHousingSociety(final RequestAddOrUpdateHousingSociety requestObject){

        showLoadingDialog("Adding housing society, wait...");
        getBackendAPIs().getRoutesAPI().addHousingSociety(requestObject, new RoutesAPI.HousingSocietyCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, HousingSocietyDetails housingSocietyDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Housing society added successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                    resultData.putExtra(Constants.HOUSING_SOCIETY_DETAILS, new Gson().toJson(housingSocietyDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }
                else{
                    showSnackbarMessage("Something went wrong while adding housing society, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            addHousingSociety(requestObject);
                        }
                    });
                }
            }
        });
    }



    public void updateHousingSociety(final RequestAddOrUpdateHousingSociety requestObject){

        showLoadingDialog("Updating housing society, wait...");
        getBackendAPIs().getRoutesAPI().updateHousingSociety(housingSocietyDetails.getID(), requestObject, new RoutesAPI.HousingSocietyCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, HousingSocietyDetails housingSocietyDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Housing society updated successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                    resultData.putExtra(Constants.HOUSING_SOCIETY_DETAILS, new Gson().toJson(housingSocietyDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }
                else{
                    showSnackbarMessage("Something went wrong while updating housing society, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateHousingSociety(requestObject);
                        }
                    });
                }
            }
        });
    }

    //=============


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.LOCATION_SELECTION_ACTIVITY_CODE && resultCode == RESULT_OK ){
                PlaceDetails placeDetails = new Gson().fromJson(data.getStringExtra(ConstantsCommonLibrary.PLACE_DETAILS), PlaceDetails.class);
                position.setText(placeDetails.getLatLng().latitude+","+placeDetails.getLatLng().longitude);
                //name.setText(placeDetails.getPremise()+", ");
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
