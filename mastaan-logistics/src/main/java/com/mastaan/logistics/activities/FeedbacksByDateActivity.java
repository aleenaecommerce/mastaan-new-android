package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.SearchView;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.FeedbacksAdapter;
import com.mastaan.logistics.backend.FeedbacksAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AnalyseFeedbacksDialog;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.OrderDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.fragments.FilterItemsFragment;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.methods.AnalyseFeedbacksMethods;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 29/09/18.
 */

public class FeedbacksByDateActivity extends MastaanToolbarActivity implements View.OnClickListener{

    DrawerLayout drawerLayout;
    FilterItemsFragment filterFragment;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    MenuItem searchViewMenuITem;
    SearchView searchView;
    MenuItem filtereMenuItem;

    CheckBox byOrderDate;
    TextView showing_date;
    View changeDate;
    TextView summary_info;
    vRecyclerView feedbacksHolder;
    FeedbacksAdapter feedbacksAdapter;

    View showSummary;

    String currentDate = "";
    List<FeedbackDetails> currentFeedbacksList = new ArrayList<>();

    SearchDetails searchDetails;

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_feedbacks_by_date);
        hasLoadingView();
        hasSwipeRefresh();

        userDetails = new LocalStorageData(context).getUserDetails();

        //--------

        filterFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    clearSearch();
                }
                else {
                    performSearch(searchDetails);
                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        showSummary = findViewById(R.id.showSummary);
        showSummary.setOnClickListener(this);

        //---------

        showing_date =  findViewById(R.id.showing_date);
        changeDate = findViewById(R.id.changeDate);
        changeDate.setOnClickListener(this);
        summary_info = findViewById(R.id.summary_info);
        byOrderDate = findViewById(R.id.byOrderDate);
        byOrderDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onReloadPressed();
            }
        });

        feedbacksHolder = (vRecyclerView) findViewById(R.id.ordersHolder);
        feedbacksHolder.setupVerticalOrientation();
        feedbacksAdapter = new FeedbacksAdapter(activity, new ArrayList<FeedbackDetails>(), false, new FeedbacksAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails2 orderDetails = feedbacksAdapter.getItem(position).getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails2 orderDetails = feedbacksAdapter.getItem(position).getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowOrderDetails(int position) {
                new OrderDetailsDialog(activity).show(feedbacksAdapter.getItem(position).getOrderItemDetails().getOrderID());
            }

            @Override
            public void onShowCustomerOrderHistory(int position) {
                new BuyerOptionsDialog(activity).show(feedbacksAdapter.getItem(position));
            }

            @Override
            public void onShowStockDetails(int position) {
                new StockDetailsDialog(activity).show(feedbacksAdapter.getItem(position).getOrderItemDetails());
            }
        });
        if ( userDetails.isCustomerRelationshipManager() || userDetails.isDeliveryBoyManager() || userDetails.isProcessingManager() || userDetails.isOrdersManager() ){
            feedbacksAdapter.showMessageCustomerOption();
        }

        feedbacksHolder.setAdapter(feedbacksAdapter);

        //---------

        if ( getIntent().getStringExtra(Constants.DATE) != null ){
            showing_date.setText(DateMethods.getDateInFormat(getIntent().getStringExtra(Constants.DATE), DateConstants.MMM_DD_YYYY));
            getFeedbacks(getIntent().getStringExtra(Constants.DATE));
        }
        else{
            showNoDataIndicator("Select date");
            changeDate.performClick();
        }
    }

    @Override
    public void onClick(View view) {
        if ( view == changeDate ){
            String prefillDate = showing_date.getText().toString();
            if ( prefillDate == null || prefillDate.length() == 0 || prefillDate.equalsIgnoreCase("DATE")  ){
                prefillDate = DateMethods.getOnlyDate(localStorageData.getServerTime());
            }
            showDatePickerDialog("Select date", prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    showing_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                    getFeedbacks(fullDate);
                }
            });
        }
        else if ( view == clearFilter ){
            try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
            clearSearch();
        }
        else if ( view == showSummary ){
            new AnalyseFeedbacksDialog(activity, "Feedbacks Summary", feedbacksAdapter.getAllItems()).show();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        if ( showing_date.getText() != null && showing_date.getText().length() > 0 && showing_date.getText().toString().equalsIgnoreCase("DATE") == false ) {
            getFeedbacks(showing_date.getText().toString());
        }
    }

    private void getFeedbacks(final String forDate) {

        byOrderDate.setClickable(false);
        changeDate.setClickable(false);
        summary_info.setVisibility(View.GONE);
        searchResultsCountIndicator.setVisibility(View.GONE);
        showSummary.setVisibility(View.GONE);
        hideMenuItems();
        showLoadingIndicator("Loading feedbacks, wait...");
        getBackendAPIs().getFeedbacksAPI().getFeedbacksForDate(forDate, byOrderDate.isChecked(), new FeedbacksAPI.FeedbacksCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<FeedbackDetails> feedbacksList) {
                //if ( DateMethods.compareDates(forDate, loadedDate) == 0 ) {
                    byOrderDate.setClickable(true);
                    changeDate.setClickable(true);
                    if ( status ) {
                        displayItems(forDate, feedbacksList);
                    }else{
                        showReloadIndicator("Something went wrong, try again!");
                    }
                //}
            }
        });
    }

    public void displayItems(final String date, final List<FeedbackDetails> feedbacksList){

        currentDate = date;
        currentFeedbacksList = feedbacksList;

        if ( feedbacksList.size() > 0 ){
            showLoadingIndicator("Displaying, wait...");
            new AsyncTask<Void, Void, Void>() {
                String shortSummaryInfo = "";
                @Override
                protected Void doInBackground(Void... voids) {
                    shortSummaryInfo = new AnalyseFeedbacksMethods(feedbacksList).analyse().getRatingsSummaryInfo();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    showMenuItems();
                    summary_info.setVisibility(View.VISIBLE);
                    summary_info.setText(Html.fromHtml(shortSummaryInfo));
                    showSummary.setVisibility(View.VISIBLE);
                    feedbacksAdapter.setItems(feedbacksList);
                    feedbacksHolder.smoothScrollToPosition(0);
                    switchToContentPage();
                }
            }.execute();
        }
        else{
            changeDate.setClickable(true);
            summary_info.setVisibility(View.GONE);
            showNoDataIndicator("No feedbacks  on " + DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY));
        }
    }

    //----------------

    public void performSearch(final SearchDetails search_details){
        this.searchDetails = search_details;

        if ( isShowingLoadingPage() == false && currentFeedbacksList != null && currentFeedbacksList.size() > 0 ) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
                SearchMethods.searchFeedbacks(currentFeedbacksList, searchDetails, new SearchMethods.SearchFeedbacksCallback() {
                    @Override
                    public void onComplete(boolean status, SearchDetails searchDetails, List<FeedbackDetails> filteredList) {
                        if (status && searchDetails == search_details ) {
                            if (filteredList.size() > 0) {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText(filteredList.size() + " result(s) found.\n(" + searchDetails.getSearchString() + ")");

                                feedbacksAdapter.setItems(filteredList);
                                feedbacksHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText("No Results");
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");

                                feedbacksAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                feedbacksAdapter.setItems(currentFeedbacksList);
            }
        }
    }

    public void clearSearch(){
        this.searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( currentFeedbacksList.size() > 0 ) {
            feedbacksAdapter.setItems(currentFeedbacksList);
            feedbacksHolder.scrollToPosition(0);
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }
        filterFragment.clearSelection();
    }


    //---------

    public void hideMenuItems(){
        try {
            filtereMenuItem.setVisible(false);
            searchViewMenuITem.setVisible(false);searchViewMenuITem.collapseActionView();
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    public void showMenuItems(){
        try {
            filtereMenuItem.setVisible(true);
            searchViewMenuITem.setVisible(true);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_orders_by_date, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filtereMenuItem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                clearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //filterFragment.updateCustomerName(query);
//                if ( searchDetails != null && searchDetails.isSearchable() ) {
//                    searchDetails.setCustomerName(query);
//                    performSearch(searchDetails);
//                }else{
                    SearchDetails searchDetails = new SearchDetails().setConsiderAtleastOneMatch(query);
                    performSearch(searchDetails);
//                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){

        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            super.onBackPressed();
        }
    }


}