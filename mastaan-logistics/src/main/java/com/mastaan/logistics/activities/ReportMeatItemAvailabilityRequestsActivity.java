package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.AnalyticsAdapter;
import com.mastaan.logistics.backend.AnalyticsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.DatePeriodSelectorDialog;
import com.mastaan.logistics.models.AnalyticsDetails;
import com.mastaan.logistics.models.AnalyticsInstallSource;
import com.mastaan.logistics.models.AnalyticsMeatItemAvailabilityRequest;
import com.mastaan.logistics.models.BuyerSearchDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 05/06/18.
 */

public class ReportMeatItemAvailabilityRequestsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    TextView from_date;
    TextView to_date;
    View changePeriod;

    vRecyclerView itemsHolder;
    AnalyticsAdapter analyticsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_meat_item_availability_requests);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        from_date = (TextView) findViewById(R.id.from_date);
        to_date = (TextView) findViewById(R.id.to_date);
        changePeriod = (TextView) findViewById(R.id.changePeriod);
        changePeriod.setOnClickListener(this);

        itemsHolder = (vRecyclerView) findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        analyticsAdapter = new AnalyticsAdapter(activity, new ArrayList<AnalyticsDetails>(), new AnalyticsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                AnalyticsMeatItemAvailabilityRequest analyticsMeatItemAvailabilityRequest = analyticsAdapter.getItem(position).getAnalyticsMeatItemAvailabilityRequest();
                String fromDate = from_date.getText().toString();
                String toDate = to_date.getText().toString();

                Intent buyersAct = new Intent(context, BuyersActivity.class);
                buyersAct.putExtra(Constants.ACTION_TYPE, Constants.MEATITEM_AVAILABILITY_REQUESTED_BUYERS);
                buyersAct.putExtra(Constants.TITLE, analyticsMeatItemAvailabilityRequest.getMeatItemName()+" Requested Buyers");
                buyersAct.putExtra(Constants.SEARCH_DETAILS, new Gson().toJson(new BuyerSearchDetails()
                        .setMeatItem(analyticsMeatItemAvailabilityRequest.getWarehouseMeatItemID())
                        .setFromDate((fromDate!=null&&fromDate.length()>0&&fromDate.equalsIgnoreCase("Start")==false?DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY):null))
                        .setToDate((toDate!=null&&toDate.length()>0&&toDate.equalsIgnoreCase("End")==false?DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY):null))
                ));
                startActivity(buyersAct);
            }
            @Override
            public void onActionClick(int position) {

            }
        });
        itemsHolder.setAdapter(analyticsAdapter);

        //----

        getData(null, null);

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getData(from_date.getText().toString(), to_date.getText().toString());
    }

    @Override
    public void onClick(View view) {
        if ( view == changePeriod ){
            String prefillFromDate = from_date.getText().toString();
            String prefillToDate = to_date.getText().toString();
            if ( prefillFromDate.equalsIgnoreCase("Start")&& prefillToDate.equalsIgnoreCase("End") ){
                prefillFromDate = DateMethods.getCurrentDate();
            }
            new DatePeriodSelectorDialog(activity).show(prefillFromDate, prefillToDate, new DatePeriodSelectorDialog.Callback() {
                @Override
                public void onComplete(String fromDate, String toDate) {
                    from_date.setText(fromDate != null&&fromDate.length()>0?DateMethods.getDateInFormat(fromDate, DateConstants.MMM_DD_YYYY):"Start");
                    to_date.setText(toDate != null&&toDate.length()>0?DateMethods.getDateInFormat(toDate, DateConstants.MMM_DD_YYYY):"End");
                    getData(fromDate, toDate);
                }
            });
        }
    }

    public void getData(String fromDate, String toDate){

        setToolbarTitle("Meat Item Availability Requests");
        changePeriod.setClickable(false);
        showLoadingIndicator("Loading, wait...");
        getBackendAPIs().getAnalyticsAPI().getMeatItemAvailabilityRequestsAnalytics((fromDate!=null&&fromDate.length()>0&&fromDate.equalsIgnoreCase("Start")==false?DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY):null), (toDate!=null&&toDate.length()>0&&toDate.equalsIgnoreCase("End")==false?DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY):null), new AnalyticsAPI.AnalyticsMeatItemsAvailabilityRequestsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<AnalyticsMeatItemAvailabilityRequest> analyticsMeatItemAvailabilityRequests) {
                changePeriod.setClickable(true);
                if ( status ){
                    if ( analyticsMeatItemAvailabilityRequests.size() > 0 ){
                        new AsyncTask<Void, Void, Void>() {
                            long count = 0;
                            List<AnalyticsDetails> analyticsList = new ArrayList<>();
                            @Override
                            protected Void doInBackground(Void... voids) {
                                for (int i=0;i<analyticsMeatItemAvailabilityRequests.size();i++){
                                    count += analyticsMeatItemAvailabilityRequests.get(i).getCount();
                                    analyticsList.add(new AnalyticsDetails().setAnalyticsMeatItemAvailabilityRequest(analyticsMeatItemAvailabilityRequests.get(i)));
                                }
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);

                                setToolbarTitle(CommonMethods.getIndianFormatNumber(count)+" Meat Item Availability Request"+(count>1?"s":""));
                                analyticsAdapter.setItems(analyticsList);
                                switchToContentPage();
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                    else{
                        showNoDataIndicator("Nothing to show");
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });

    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}