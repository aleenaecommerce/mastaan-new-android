package com.mastaan.logistics.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.CategoriesAdapter;
import com.mastaan.logistics.adapters.DeliverySlotsCheckBoxAdapter;
import com.mastaan.logistics.backend.CategoriesAPI;
import com.mastaan.logistics.backend.models.RequestUpdateCategoryAvailability;
import com.mastaan.logistics.backend.models.RequestUpdateCategorySlots;
import com.mastaan.logistics.backend.models.RequestUpdateCategoryVisibility;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.DeliverySlotDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class CategoriesActivity extends MastaanToolbarActivity {

    vRecyclerView categoriesHolder;
    CategoriesAdapter categoriesAdapter;

    UserDetails userDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        hasLoadingView();
        hasSwipeRefresh();

        userDetails = localStorageData.getUserDetails();

        //--------

        categoriesHolder = findViewById(R.id.categoriesHolder);
        setupHolder();

        //--------

        getCategories();

    }

    public void setupHolder(){

        categoriesHolder.setHasFixedSize(true);
        categoriesHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        categoriesHolder.setItemAnimator(new DefaultItemAnimator());

        categoriesAdapter = new CategoriesAdapter(activity, new ArrayList<CategoryDetails>(), new CategoriesAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                if ( getCallingActivity() != null ){
                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.CATEGORY_DETAILS, new Gson().toJson(categoriesAdapter.getItem(position)));
                    setResult(Activity.RESULT_OK, resultData);
                    finish();
                }
            }

            @Override
            public void onChangeVisibilityClick(final int position, final boolean isVisible) {
                if ( userDetails.isSuperUser() == false ) {
                    showToastMessage("You are not allowed to perform this operation, please contact super user");
                    return;
                }

                showChoiceSelectionDialog("Confirm!", Html.fromHtml("Are you sure to <b>" + (isVisible ? "show" : "hide") + " " + categoriesAdapter.getItem(position).getName() + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if (choiceName.equalsIgnoreCase("YES")) {
                            updateCategoryVisibility(position, new RequestUpdateCategoryVisibility(isVisible));
                        }
                    }
                });
            }

            @Override
            public void onChangeAvailabilityClick(final int position, final boolean isAvailable) {

                showChoiceSelectionDialog("Confirm!", Html.fromHtml("Are you sure to <b>" + (isAvailable == true ? "enable" : "disable") + " " + categoriesAdapter.getItem(position).getName() + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES")){
                            if ( isAvailable ){
                                RequestUpdateCategoryAvailability requestUpdateCategoryAvailability = new RequestUpdateCategoryAvailability(isAvailable, "", "");
                                updateCategoryAvailability(position, requestUpdateCategoryAvailability);
                            }
                            else{
                                View dialogDisableCategoryView = LayoutInflater.from(context).inflate(R.layout.dialog_disable_category, null);
                                final vTextInputLayout display_title = (vTextInputLayout) dialogDisableCategoryView.findViewById(R.id.display_title);
                                display_title.setText("Disabled. Tap for info.");
                                final vTextInputLayout display_message = (vTextInputLayout) dialogDisableCategoryView.findViewById(R.id.display_message);

                                dialogDisableCategoryView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        final String displayTitle = display_title.getText().trim();
                                        display_title.checkError("* Enter display title");
                                        final String displayMessage = display_message.getText().trim();
                                        display_message.checkError("* Enter display message");

                                        if ( displayTitle.length() > 0 && displayMessage.length() > 0 ){
                                            closeCustomDialog();

                                            RequestUpdateCategoryAvailability requestUpdateCategoryAvailability = new RequestUpdateCategoryAvailability(isAvailable, displayTitle, displayMessage);
                                            updateCategoryAvailability(position, requestUpdateCategoryAvailability);
                                        }
                                    }
                                });
                                showCustomDialog(dialogDisableCategoryView);
                            }
                        }
                    }
                });
            }

            @Override
            public void onManageSlotsClick(final int position) {

                showDatePickerDialog("Select date!", localStorageData.getServerTime(), null, new DatePickerCallback() {
                    @Override
                    public void onSelect(String fullDate, int day, int month, int year) {
                        getCategorySlots(categoriesAdapter.getItem(position), fullDate);
                    }
                });

            }
        });
        if ( getCallingActivity() != null ){    categoriesAdapter.hideActionButtons();  }
        categoriesHolder.setAdapter(categoriesAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getCategories();
    }


    //----------

    public void getCategories(){

        showLoadingIndicator("Loading categories, wait...");
        getBackendAPIs().getCategoriesAPI().getCategories(new CategoriesAPI.CategoriesListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<CategoryDetails> categoriesList) {
                if ( status ){
                    if ( categoriesList.size() > 0 ) {
                        categoriesAdapter.setItems(categoriesList);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("No categories");
                    }
                }else{
                    showReloadIndicator(statusCode, "Unable to load categories, try again!");
                }
            }
        });
    }

    public void getCategorySlots(final CategoryDetails categoryDetails, final String requiredDate){

        showLoadingDialog("Loading "+categoryDetails.getName()+" slots on "+DateMethods.getDateInFormat(requiredDate, DateConstants.MMM_DD_YYYY)+", wait...");
        getBackendAPIs().getCategoriesAPI().getCategorySlots(categoryDetails.getID(), requiredDate, new CategoriesAPI.CategorySlotsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String date, List<DeliverySlotDetails> slotsList) {
                closeLoadingDialog();
                if ( DateMethods.compareDates(date, requiredDate) == 0 ){
                    if ( status ){
                        displaySlots(categoryDetails, date, slotsList);
                    }else{
                        showSnackbarMessage("Unable to load " + categoryDetails.getName() + " slots on " + DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY) + ", try again!", "RETRY", new ActionCallback() {
                            @Override
                            public void onAction() {
                                getCategorySlots(categoryDetails, requiredDate);
                            }
                        });
                    }
                }
            }
        });
    }

    public void displaySlots(final CategoryDetails categoryDetails, final String date, final List<DeliverySlotDetails> slotsList){

        View dialogSlotsView = LayoutInflater.from(context).inflate(R.layout.dialog_category_slots, null);
        TextView title = (TextView) dialogSlotsView.findViewById(R.id.title);
        vRecyclerView slotsHolder = (vRecyclerView) dialogSlotsView.findViewById(R.id.slotsHolder);
        slotsHolder.setHasFixedSize(true);
        slotsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        slotsHolder.setItemAnimator(new DefaultItemAnimator());

        final DeliverySlotsCheckBoxAdapter deliverySlotsCheckBoxAdapter = new DeliverySlotsCheckBoxAdapter(context, slotsList, null);
        slotsHolder.setAdapter(deliverySlotsCheckBoxAdapter);

        title.setText(categoryDetails.getName().toUpperCase()+" slots on "+date.toUpperCase());

        dialogSlotsView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeCustomDialog();
            }
        });
        dialogSlotsView.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeCustomDialog();

                RequestUpdateCategorySlots requestUpdateCategorySlots = new RequestUpdateCategorySlots(categoryDetails.getID(), categoryDetails.getName(), DateMethods.getDateInFormat(date, "dd-MM-yyyy"), deliverySlotsCheckBoxAdapter.getEnableSlots(), deliverySlotsCheckBoxAdapter.getDisabledSlots());
                updateCategorySlots(requestUpdateCategorySlots);
                //showToastMessage("Enabled: "+deliverySlotsCheckBoxAdapter.getEnableSlots().size()+", DIsabled: "+deliverySlotsCheckBoxAdapter.getDisabledSlots().size());
            }
        });

        showCustomDialog(dialogSlotsView);

    }

    public void updateCategorySlots(final RequestUpdateCategorySlots requestUpdateCategorySlots){

        showLoadingDialog("Updating "+requestUpdateCategorySlots.getCategoryName()+" slots on "+requestUpdateCategorySlots.getDate().toLowerCase()+", wait...");
        getBackendAPIs().getCategoriesAPI().updateCategorySlots(requestUpdateCategorySlots, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showSnackbarMessage(CommonMethods.capitalizeFirstLetter(requestUpdateCategorySlots.getCategoryName())+" slot on "+requestUpdateCategorySlots.getDate().toLowerCase()+" updated successfully.");
                }else{
                    showSnackbarMessage("Unable to update "+requestUpdateCategorySlots.getCategoryName()+" slots on "+requestUpdateCategorySlots.getDate().toLowerCase(), "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateCategorySlots(requestUpdateCategorySlots);
                        }
                    });
                }
            }
        });

    }

    public void updateCategoryVisibility(final int position, final RequestUpdateCategoryVisibility requestObject){

        showLoadingDialog("Changing "+categoriesAdapter.getItem(position).getName()+" visibility, wait...");
        getBackendAPIs().getCategoriesAPI().updateCategoryVisibility(categoriesAdapter.getItem(position).getID(), requestObject, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    CategoryDetails categoryDetails = categoriesAdapter.getItem(position);
                    categoryDetails.setVisibility(requestObject.isVisible());
                    categoriesAdapter.setItem(position, categoryDetails);

                    showSnackbarMessage(categoriesAdapter.getItem(position).getName()+" visibility changed successfully.");
                }else{
                    showSnackbarMessage("Unable to change " + categoriesAdapter.getItem(position).getName() + " visibility, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateCategoryVisibility(position, requestObject);
                        }
                    });
                }
            }
        });
    }

    public void updateCategoryAvailability(final int position, final RequestUpdateCategoryAvailability requestObject){

        showLoadingDialog("Changing "+categoriesAdapter.getItem(position).getName()+" availability, wait...");
        getBackendAPIs().getCategoriesAPI().updateCategoryAvailability(categoriesAdapter.getItem(position).getID(), requestObject, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    categoriesAdapter.changeAvailability(position, requestObject.isEnabled(), requestObject.getDisplayTitle(), requestObject.getDisplayMessage());

                    showSnackbarMessage(categoriesAdapter.getItem(position).getName()+" availability changed successfully.");
                }else{
                    showSnackbarMessage("Unable to change " + categoriesAdapter.getItem(position).getName() + " availability, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateCategoryAvailability(position, requestObject);
                        }
                    });
                }
            }
        });
    }

    //==============

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
