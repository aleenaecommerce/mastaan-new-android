package com.mastaan.logistics.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.widget.SearchView;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.OrderItemsAdapter;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

public class CashNCarryByDateActivity extends MastaanToolbarActivity implements View.OnClickListener {

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    MenuItem searchViewMenuITem;
    SearchView searchView;

    TextView showing_date;
    View changeDate;
    TextView summary_info;

    vRecyclerView orderItemsHolder;
    OrderItemsAdapter orderItemsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_n_carry_by_date);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        showing_date = (TextView) findViewById(R.id.showing_date);
        changeDate = findViewById(R.id.changeDate);
        changeDate.setOnClickListener(this);
        summary_info = (TextView) findViewById(R.id.summary_info);

        orderItemsHolder = (vRecyclerView) findViewById(R.id.orderItemsHolder);
        setupHolder();

        //---------

        showNoDataIndicator("Select date");
        changeDate.performClick();

    }


    @Override
    public void onClick(View view) {
        if ( view == changeDate ){
            String prefillDate = showing_date.getText().toString();
            if ( prefillDate == null || prefillDate.length() == 0 || prefillDate.equalsIgnoreCase("DATE") ){
                prefillDate = DateMethods.getOnlyDate(localStorageData.getServerTime());
            }
            showDatePickerDialog("Select date!", null, localStorageData.getServerTime(), prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    showing_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                    getCashNCarryItemsForDate(fullDate);
                }
            });
        }
        else if ( view == clearFilter ){
            try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
            //clearSearch();
        }
    }

    public void setupHolder(){
        orderItemsHolder.setupVerticalOrientation();
        orderItemsAdapter = new OrderItemsAdapter(activity, new ArrayList<OrderItemDetails>(), null);
        orderItemsHolder.setAdapter(orderItemsAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        if ( showing_date.getText().toString().length() > 0 ) {
            getCashNCarryItemsForDate(showing_date.getText().toString());
        }
    }

    public void getCashNCarryItemsForDate(String forDate){

        summary_info.setVisibility(View.GONE);
        changeDate.setClickable(false);
        showLoadingIndicator("Loading cash n carry items, wait...");
        getBackendAPIs().getOrdersAPI().getCashNCarryItemsByDate(forDate, new OrdersAPI.CashNCarryItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<OrderItemDetails> itemsList) {
                if (status) {
                    if ( itemsList.size() > 0 ) {
                        showLoadingIndicator("Displaying, wait...");
                        new AsyncTask<Void, Void, Void>() {
                            String summaryInfo = "";
                            @Override
                            protected Void doInBackground(Void... voids) {
                                long count =0;
                                double itemsTotalAmount = 0, vendorsPaidAmount = 0;
                                for (int i=0;i<itemsList.size();i++){
                                    if ( itemsList.get(i) != null ) {
                                        count += 1;
                                        itemsTotalAmount += itemsList.get(i).getAmountOfItem();
                                        vendorsPaidAmount += itemsList.get(i).getVendorPaidAmount();
                                    }
                                }
                                summaryInfo = "Count: <b>"+ CommonMethods.getIndianFormatNumber(count)
                                        +"<br>Items amount: <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(itemsTotalAmount)+"</b>"
                                        +"<br>Amount paid to vendors: <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(vendorsPaidAmount)+"</b>";
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                orderItemsAdapter.setItems(itemsList);
                                summary_info.setText(Html.fromHtml(summaryInfo));
                                summary_info.setVisibility(View.VISIBLE);
                                changeDate.setClickable(true);
                                switchToContentPage();
                            }
                        }.execute();
                    }else{
                        changeDate.setClickable(true);
                        showNoDataIndicator("No cash n carry items");
                    }
                } else {
                    changeDate.setClickable(true);
                    showReloadIndicator("Unable to load cash n carry items, try again!");
                }
            }
        });
    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}