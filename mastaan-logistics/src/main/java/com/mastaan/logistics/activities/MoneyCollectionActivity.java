package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.SearchView;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.MoneyCollectionsAdapter;
import com.mastaan.logistics.backend.MoneyCollectionAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AnalyseMoneyCollectionsDialog;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.OrderDetailsDialog;
import com.mastaan.logistics.flows.ActionFABsFlow;
import com.mastaan.logistics.flows.MoneyCollectionsFlow;
import com.mastaan.logistics.fragments.FilterItemsFragment;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.methods.AnalyseMoneyCollectionsMethods;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.MoneyCollectionDetails;
import com.mastaan.logistics.models.MoneyCollectionTransactionDetails;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class MoneyCollectionActivity extends MastaanToolbarActivity implements View.OnClickListener{//}, CompoundButton.OnCheckedChangeListener{

    MastaanToolbarActivity activity;
    UserDetails userDetails;

    String actionType = "";
    String deliveryBoyID = "";
    String deliveryBoyName = "";
    String buyerID = "";
    String buyerName = "";

    DrawerLayout drawerLayout;
    FilterItemsFragment filterFragment;

    RadioGroup filterGroup;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    MenuItem searchViewMenuITem;
    SearchView searchView;
    MenuItem filtereMenuItem;

    TextView showing_date;
    View changeDate;
    TextView summary_info;
    vRecyclerView moneyCollectionHolder;
    MoneyCollectionsAdapter moneyCollectionsAdapter;

    View markAllAsWithAccounts;

    View showSummary;

    String currentDate = "";
    List<MoneyCollectionDetails> currentMoneyCollectionsList = new ArrayList<>();

    SearchDetails searchDetails = new SearchDetails();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_money_collection);
        hasLoadingView();
        hasSwipeRefresh();
        activity = this;

        userDetails = new LocalStorageData(context).getUserDetails();

        actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        if ( actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_TODAY) || actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_DATE) ){
            deliveryBoyID = getIntent().getStringExtra(Constants.DELIVERY_BOY_ID);
            deliveryBoyName = getIntent().getStringExtra(Constants.DELIVERY_BOY_NAME);
            setToolbarTitle(deliveryBoyName+" Money collection");
        }else if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION_OF_DELIVERY_BOY) ){
            deliveryBoyID = getIntent().getStringExtra(Constants.DELIVERY_BOY_ID);
            deliveryBoyName = getIntent().getStringExtra(Constants.DELIVERY_BOY_NAME);
            setToolbarTitle(deliveryBoyName+" Pending consolidation");
            findViewById(R.id.dateView).setVisibility(View.GONE);
        }else if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION) ){
            setToolbarTitle("Pending consolidations");
            findViewById(R.id.dateView).setVisibility(View.GONE);
        }else if ( actionType.equalsIgnoreCase(Constants.MONEY_CONSOLIDATION) ){
            setToolbarTitle("Money consolidation");
        }else if ( actionType.equalsIgnoreCase(Constants.BUYER_MONEY_COLLECTION) ){
            buyerID = getIntent().getStringExtra(Constants.BUYER_ID);
            buyerName = getIntent().getStringExtra(Constants.BUYER_NAME);
            setToolbarTitle(buyerName+" Money collection");
            findViewById(R.id.dateView).setVisibility(View.GONE);
        }

        filterFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails search_Details) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (search_Details.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    clearSearch();
                }
                else {
                    if ( searchDetails != null ){   search_Details.setStatus(searchDetails.getStatus());    }
                    performSearch(search_Details);
                }
            }
        });

        //--------------

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        filterGroup = (RadioGroup) findViewById(R.id.filterGroup);

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        //---------

        showing_date = (TextView) findViewById(R.id.showing_date);
        changeDate = findViewById(R.id.changeDate);
        changeDate.setOnClickListener(this);
        summary_info = (TextView) findViewById(R.id.summary_info);

        moneyCollectionHolder = (vRecyclerView) findViewById(R.id.moneyCollectionHolder);
        setupHolder();

        markAllAsWithAccounts = findViewById(R.id.markAllAsConsolidated);
        markAllAsWithAccounts.setOnClickListener(this);

        showSummary = findViewById(R.id.showSummary);
        showSummary.setOnClickListener(this);

        //---------

        if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION) || actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION_OF_DELIVERY_BOY) || actionType.equalsIgnoreCase(Constants.BUYER_MONEY_COLLECTION)){
            getMoneyCollections("all");
        }else{
            if ( actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_TODAY) || actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_TODAY) ){
                showing_date.setText(DateMethods.getDateInFormat(localStorageData.getServerTime(), DateConstants.MMM_DD_YYYY));
                getMoneyCollections(showing_date.getText().toString());
            }else{
                showNoDataIndicator("Select date");
                changeDate.performClick();
            }
        }

    }

    public void setupHolder(){

        moneyCollectionHolder.setupVerticalOrientation();
        moneyCollectionsAdapter = new MoneyCollectionsAdapter(activity, new ArrayList<MoneyCollectionDetails>(), new MoneyCollectionsAdapter.Callback() {
            @Override
            public void onCallCustomer(int position) {
                MoneyCollectionDetails moneyCollectionDetails = moneyCollectionsAdapter.getItem(position);
                if ( moneyCollectionDetails.getType().equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_ORDER) ) {
                    new ActionFABsFlow(activity).call(moneyCollectionDetails.getOrderDetails().getCustomerMobile(), moneyCollectionDetails.getOrderDetails().getCustomerAlternateMobile());
                }
                else if ( moneyCollectionDetails.getType().equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_OUTSTANDING) ) {
                    new ActionFABsFlow(activity).call(moneyCollectionDetails.getBuyerDetails().getMobileNumber(), moneyCollectionDetails.getBuyerDetails().getAlternateMobile());
                }
            }

            @Override
            public void onShowOrderDetails(int position) {
                new OrderDetailsDialog(activity).show(moneyCollectionsAdapter.getItem(position).getOrderDetails().getID());
            }

            @Override
            public void onShowBuyerHistory(int position) {
                new BuyerOptionsDialog(activity).show(moneyCollectionsAdapter.getItem(position));
            }

            @Override
            public void onItemClick(final int position) {

            }

            @Override
            public void onTransactionClick(final int position, final int transactionPosition) {
                if ( userDetails.isOrdersManager() ) {
                    final MoneyCollectionDetails moneyCollectionDetails = moneyCollectionsAdapter.getItem(position);
                    final MoneyCollectionTransactionDetails transactionDetails = moneyCollectionDetails.getTransactions().get(transactionPosition);

                    // PENDING
                    if ( transactionDetails.isConsolidated() == false ){//moneyCollectionDetails.getStatus().equalsIgnoreCase(Constants.WITH_DELIVERBOY) ){
                        final List<String> optionsList = new ArrayList<>();
                        if ( transactionDetails.getPaymentTypeString().equalsIgnoreCase(Constants.ONLINE_PAYMENT) == false ) {
                            optionsList.add(Constants.UPDATE_MONEY_COLLECTION_TRANSACTION_DETAILS);
                        }
                        optionsList.add(Constants.MARK_AS_CONSOLIDATED);

                        showListChooserDialog(null, optionsList, new ListChooserCallback() {
                            @Override
                            public void onSelect(final int optionPosition) {
                                if ( optionsList.get(optionPosition).equalsIgnoreCase(Constants.MARK_AS_CONSOLIDATED) ){
                                    new MoneyCollectionsFlow(activity).markMoneyCollectionTransactionAsConsolidated(moneyCollectionDetails.getID(), transactionDetails, new MoneyCollectionsFlow.MoneyCollectionTransactionCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message, MoneyCollectionTransactionDetails updatedTransactionDetails) {
                                            if ( status ){
                                                moneyCollectionDetails.getTransactions().set(transactionPosition, updatedTransactionDetails);
                                                updateItemInList(position, moneyCollectionDetails);
                                            }
                                        }
                                    });
                                }
                                else if ( optionsList.get(optionPosition).equalsIgnoreCase(Constants.UPDATE_MONEY_COLLECTION_TRANSACTION_DETAILS) ){
                                    new MoneyCollectionsFlow(activity).updateMoneyCollectionTransactionDetails(moneyCollectionDetails.getID(), transactionDetails, new MoneyCollectionsFlow.MoneyCollectionTransactionCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message, MoneyCollectionTransactionDetails updatedTransactionDetails) {
                                            if ( status ){
                                                moneyCollectionDetails.getTransactions().set(transactionPosition, updatedTransactionDetails);
                                                updateItemInList(position, moneyCollectionDetails);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            }
        });
        moneyCollectionHolder.setAdapter(moneyCollectionsAdapter);
    }

    @Override
    public void onClick(View view) {
        if ( view == changeDate ){
            String prefillDate = showing_date.getText().toString();
            if ( prefillDate == null || prefillDate.length() == 0 || prefillDate.equalsIgnoreCase("DATE") ){
                prefillDate = DateMethods.getOnlyDate(localStorageData.getServerTime());
            }
            showDatePickerDialog("Select date!", null, localStorageData.getServerTime(), prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    showing_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                    getMoneyCollections(fullDate);
                }
            });
        }
        else if ( view == markAllAsWithAccounts ){
            //if ( activity.localStorageData.isUserCheckedIn() ) {
                new MoneyCollectionsFlow(activity).markAllMoneyCollectionsAsWithAccounts(moneyCollectionsAdapter.getAllItems(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        onReloadPressed();
                    }
                });
            //}else{
            //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
            //}
        }
        else if ( view == clearFilter ){
            try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
            clearSearch();
        }
        else if ( view == showSummary ){
            if ( actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_TODAY) || actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_DATE) ){
                new AnalyseMoneyCollectionsDialog(activity, showing_date.getText()+"\n"+deliveryBoyName+" MONEY COLLECTION", new String[]{Constants.SUMMARY, Constants.CONSOLIDATOR}, moneyCollectionsAdapter.getAllItems())
                        .setForCollectionDate(showing_date.getText().toString()).setForDeliveryBoy(deliveryBoyID).show();
            }else if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION_OF_DELIVERY_BOY) ){
                new AnalyseMoneyCollectionsDialog(activity, deliveryBoyName+" PENDING CONSOLIDATION", new String[]{Constants.SUMMARY}, moneyCollectionsAdapter.getAllItems())
                        .setForConsolidationDate(showing_date.getText().toString()).setForDeliveryBoy(deliveryBoyID).show();
            }else if( actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_TODAY) || actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_DATE) ){
                new AnalyseMoneyCollectionsDialog(activity, showing_date.getText()+"\nMONEY COLLECTION", null, moneyCollectionsAdapter.getAllItems())
                        .setForCollectionDate(showing_date.getText().toString()).show();
            }else if ( actionType.equalsIgnoreCase(Constants.MONEY_CONSOLIDATION)) {
                new AnalyseMoneyCollectionsDialog(activity, showing_date.getText()+"\nMONEY CONSOLIDATION", null, moneyCollectionsAdapter.getAllItems())
                        .setForConsolidationDate(showing_date.getText().toString()).show();
            }else if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION)) {
                new AnalyseMoneyCollectionsDialog(activity, "PENDING CONSOLIDATION", new String[]{Constants.SUMMARY, Constants.DELIVERY_BOY}, moneyCollectionsAdapter.getAllItems())
                        .setForConsolidationDate(showing_date.getText().toString()).show();
            }else if ( actionType.equalsIgnoreCase(Constants.BUYER_MONEY_COLLECTION) ){
                new AnalyseMoneyCollectionsDialog(activity, buyerName+" MONEY COLLECTION", null, moneyCollectionsAdapter.getAllItems())
                        .show();
            }else {
                showToastMessage(Constants.UNDER_CONSTRUCTION_OR_NOT_YET_DONE);
            }
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        if ( showing_date.getText() != null && showing_date.getText().length() > 0 ) {
            getMoneyCollections(showing_date.getText().toString());
        }
    }

    private void getMoneyCollections(final String forDate) {

        changeDate.setVisibility(View.GONE);
        summary_info.setVisibility(View.GONE);
        showSummary.setVisibility(View.GONE);
        searchDetails = new SearchDetails();
        filterGroup.removeAllViews();
        filterGroup.setVisibility(View.GONE);
        searchResultsCountIndicator.setVisibility(View.GONE);
        markAllAsWithAccounts.setVisibility(View.GONE);
        hideMenuItems();

        if ( actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_TODAY) || actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_DATE) ){

            showLoadingIndicator("Loading money collection on "+ DateMethods.getDateInFormat(forDate, DateConstants.MMM_DD_YYYY)+", wait...");
            getBackendAPIs().getMoneyCollectionAPI().getMoneyCollectionForDate(forDate, new MoneyCollectionAPI.MoneyCollectionsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String loadedDate, List<MoneyCollectionDetails> moneyCollectionsList) {
                    if ( forDate.equalsIgnoreCase(loadedDate) ){
                        changeDate.setVisibility(View.VISIBLE);
                        if (status) {
                            displayItems(loadedDate, moneyCollectionsList);
                        } else {
                            showReloadIndicator("Unable to load money collection on " + DateMethods.getDateInFormat(forDate, DateConstants.MMM_DD_YYYY) + ", try again!");
                        }
                    }
                }
            });
        }

        else if ( actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_TODAY) || actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_DATE) ){

            showLoadingIndicator("Loading money collection of "+deliveryBoyName.toLowerCase()+" on "+ DateMethods.getDateInFormat(forDate, DateConstants.MMM_DD_YYYY)+", wait...");
            getBackendAPIs().getMoneyCollectionAPI().getMoneyCollectionOfDeliveryBoyForDate(deliveryBoyID, forDate, new MoneyCollectionAPI.MoneyCollectionsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String loadedDate, List<MoneyCollectionDetails> moneyCollectionsList) {
                    if ( forDate.equalsIgnoreCase(loadedDate) ){
                        changeDate.setVisibility(View.VISIBLE);
                        filterFragment.hideDeliveryBoyView();
                        if (status) {
                            displayItems(loadedDate, moneyCollectionsList);
                        } else {
                            showReloadIndicator("Unable to load money collection of "+deliveryBoyName.toLowerCase()+" on " + DateMethods.getDateInFormat(forDate, DateConstants.MMM_DD_YYYY) + ", try again!");
                        }
                    }
                }
            });
        }

        else if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION_OF_DELIVERY_BOY) ){

            showLoadingIndicator("Loading pending consolidation of "+deliveryBoyName.toLowerCase()+", wait...");
            getBackendAPIs().getMoneyCollectionAPI().getPendingConsolidationsOfDeliveryBoy(deliveryBoyID, new MoneyCollectionAPI.MoneyCollectionsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String loadedDate, List<MoneyCollectionDetails> moneyCollectionsList) {
                    filterFragment.hideDeliveryBoyView();
                    if (status) {
                        displayItems(loadedDate, moneyCollectionsList);
                    } else {
                        showReloadIndicator("Unable to load pending consolidation of "+deliveryBoyName.toLowerCase() + ", try again!");
                    }
                }
            });
        }

        else if ( actionType.equalsIgnoreCase(Constants.MONEY_CONSOLIDATION) ){

            showLoadingIndicator("Loading money consolidation on "+ DateMethods.getDateInFormat(forDate, DateConstants.MMM_DD_YYYY)+", wait...");
            getBackendAPIs().getMoneyCollectionAPI().getMoneyConsolidationForDate(forDate, new MoneyCollectionAPI.MoneyCollectionsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String loadedDate, List<MoneyCollectionDetails> moneyCollectionsList) {
                    if ( forDate.equalsIgnoreCase(loadedDate) ){
                        changeDate.setVisibility(View.VISIBLE);
                        if (status) {
                            displayItems(loadedDate, moneyCollectionsList);
                        } else {
                            showReloadIndicator("Unable to load money consolidation on " + DateMethods.getDateInFormat(forDate, DateConstants.MMM_DD_YYYY) + ", try again!");
                        }
                    }
                }
            });
        }

        else if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION) ){

            showLoadingIndicator("Loading pending consolidations, wait...");
            getBackendAPIs().getMoneyCollectionAPI().getPendingConsolidations(new MoneyCollectionAPI.MoneyCollectionsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String loadedDate, List<MoneyCollectionDetails> moneyCollectionsList) {
                    if (status) {
                        displayItems(loadedDate, moneyCollectionsList);
                    } else {
                        showReloadIndicator("Unable to load pending consolidations, try again!");
                    }
                }
            });
        }

        else if ( actionType.equalsIgnoreCase(Constants.BUYER_MONEY_COLLECTION) ){

            showLoadingIndicator("Loading money collections of "+buyerName.toLowerCase()+", wait...");
            getBackendAPIs().getMoneyCollectionAPI().getBuyerMoneyCollections(buyerID, new MoneyCollectionAPI.MoneyCollectionsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String loadedDate, List<MoneyCollectionDetails> moneyCollectionsList) {
                    if (status) {
                        filterFragment.hideCustomerView();
                        displayItems(loadedDate, moneyCollectionsList);
                    } else {
                        showReloadIndicator("Unable to load money collections of "+buyerName.toLowerCase() + ", try again!");
                    }
                }
            });
        }
    }

    public void displayItems(String date, final List<MoneyCollectionDetails> moneyCollectionsList){
        currentDate = date;
        currentMoneyCollectionsList = moneyCollectionsList;

        if ( moneyCollectionsList.size() > 0 ) {
            showLoadingIndicator("Displaying, wait...");
            GroupingMethods.getMoneyCollectionsStatusStrings(moneyCollectionsList, new GroupingMethods.MoneyCollectionStatusStringsCallback() {
                @Override
                public void onComplete(boolean status, final List<String> statusStrings) {

                    new AsyncTask<Void, Void, Void>() {
                        String summaryInfo = "";
                        @Override
                        protected Void doInBackground(Void... voids) {
                            AnalyseMoneyCollectionsMethods analyseMoneyCollectionsMethods = new AnalyseMoneyCollectionsMethods(new String[]{Constants.SUMMARY}, moneyCollectionsList);

                            if ( actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_TODAY) || actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_DATE) ){
                                analyseMoneyCollectionsMethods.setForCollectionDate(showing_date.getText().toString()).setForDeliveryBoy(deliveryBoyID);
                            }else if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION_OF_DELIVERY_BOY) ){
                                analyseMoneyCollectionsMethods.setForConsolidationDate(showing_date.getText().toString()).setForDeliveryBoy(deliveryBoyID);
                            }else if( actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_TODAY) || actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_DATE) ){
                                analyseMoneyCollectionsMethods.setForCollectionDate(showing_date.getText().toString());
                            }else if ( actionType.equalsIgnoreCase(Constants.MONEY_CONSOLIDATION)) {
                                analyseMoneyCollectionsMethods.setForConsolidationDate(showing_date.getText().toString());
                            }else if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION)) {
                                analyseMoneyCollectionsMethods.setForConsolidationDate(showing_date.getText().toString());
                            }

                            analyseMoneyCollectionsMethods.analyse();
                            summaryInfo = analyseMoneyCollectionsMethods.getFullSummaryInfo();//getShortSummaryInfo();
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);

                            showMenuItems();
                            showSummary.setVisibility(View.VISIBLE);
                            summary_info.setText(Html.fromHtml(summaryInfo));
                            summary_info.setVisibility(View.VISIBLE);
                            markAllAsWithAccounts.setVisibility(View.GONE);

                            if ( statusStrings.size() > 1 ){
                                statusStrings.add(0, "All");
                                filterGroup.setVisibility(View.VISIBLE);

                                for (int i=0;i<statusStrings.size();i++){
                                    final RadioButton radioButton = new RadioButton(context);
                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    filterGroup.addView(radioButton, layoutParams);
                                    if ( i==0 ){    radioButton.setChecked(true);   }

                                    radioButton.setText(statusStrings.get(i));
                                    radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                        @Override
                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                            if ( isChecked ){
                                                if ( searchDetails == null ){ searchDetails = new SearchDetails(); }
                                                if ( radioButton.getText().toString().equalsIgnoreCase("All") ){
                                                    markAllAsWithAccounts.setVisibility(View.GONE);
                                                    searchDetails.setStatus(null);
                                                }else{
                                                    if ( radioButton.getText().toString().equalsIgnoreCase(Constants.PENDING) ){
                                                        markAllAsWithAccounts.setVisibility(View.VISIBLE);
                                                    }else{
                                                        markAllAsWithAccounts.setVisibility(View.GONE);
                                                    }

                                                    searchDetails.setStatus(radioButton.getText().toString());
                                                }
                                                performSearch(searchDetails);
                                            }
                                        }
                                    });
                                    if ( radioButton.getText().toString().equalsIgnoreCase("All") ){
                                        radioButton.setChecked(true);
                                    }
                                }
                            }else{
                                if ( statusStrings.size() == 1 && statusStrings.get(0).equalsIgnoreCase(Constants.PENDING) ){
                                    markAllAsWithAccounts.setVisibility(View.VISIBLE);
                                }
                                filterGroup.setVisibility(View.GONE);
                            }

                            moneyCollectionsAdapter.setItems(moneyCollectionsList);
                            moneyCollectionHolder.smoothScrollToPosition(0);
                            switchToContentPage();
                        }
                    }.execute();

                }
            });
        }else{
            filterGroup.setVisibility(View.GONE);

            if ( actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION) ){
                showNoDataIndicator("No pending consolidations");
            }else if ( actionType.equalsIgnoreCase(Constants.MONEY_CONSOLIDATION) ){
                showNoDataIndicator("No consolidations");
            } else if (actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_DATE) || actionType.equalsIgnoreCase(Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_TODAY)) {
                showNoDataIndicator("No money collection of " + deliveryBoyName.toLowerCase() + " on " + DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY));
            }else if (actionType.equalsIgnoreCase(Constants.PENDING_CONSOLIDATION_OF_DELIVERY_BOY) ){
                showNoDataIndicator("No pending consolidation of " + deliveryBoyName.toLowerCase());
            }else if (actionType.equalsIgnoreCase(Constants.BUYER_MONEY_COLLECTION) ){
                showNoDataIndicator("No money collections of " + buyerName.toLowerCase());
            }else {
                showNoDataIndicator("No money collection on " + DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY));
            }
        }
    }

    //---------------

    public void updateItemInList(int position, MoneyCollectionDetails moneyCollectionDetails){
        moneyCollectionsAdapter.setItem(position, moneyCollectionDetails);
        for (int i=0;i<currentMoneyCollectionsList.size();i++){
            if ( currentMoneyCollectionsList.get(i).getID().equals(moneyCollectionDetails.getID())){
                currentMoneyCollectionsList.set(i, moneyCollectionDetails);
                break;
            }
        }
    }

    //----------------

    public void performSearch(final SearchDetails search_details){
        this.searchDetails = search_details;

        if ( isShowingLoadingPage() == false && currentMoneyCollectionsList != null && currentMoneyCollectionsList.size() > 0 ) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showSummary.setVisibility(View.GONE);
                showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
                SearchMethods.searchMoneyCollections(currentMoneyCollectionsList, searchDetails, new SearchMethods.SearchMoneyCollectionsCallback() {
                    @Override
                    public void onComplete(boolean status, SearchDetails searchDetails, List<MoneyCollectionDetails> filteredList) {
                        if (status && searchDetails == search_details ) {
                            if (filteredList.size() > 0) {
                                showSummary.setVisibility(View.VISIBLE);
                                if ( searchDetails.isSearchableExcludingStatus() ) {
                                    searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                    results_count.setText(filteredList.size() + " result(s) found.\n(" + searchDetails.getSearchString() + ")");
                                }else{
                                    searchResultsCountIndicator.setVisibility(View.GONE);
                                }

                                moneyCollectionsAdapter.setItems(filteredList);
                                moneyCollectionHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else {
                                if ( searchDetails.isSearchableExcludingStatus() ) {
                                    searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                    results_count.setText("No Results");
                                }else{
                                    searchResultsCountIndicator.setVisibility(View.GONE);
                                }
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");
                                moneyCollectionsAdapter.clearItems();
                            }
                        }
                    }
                });
            } else {
                searchResultsCountIndicator.setVisibility(View.GONE);
                moneyCollectionsAdapter.setItems(currentMoneyCollectionsList);
                moneyCollectionHolder.scrollToPosition(0);
                if ( currentMoneyCollectionsList.size() > 0 ){
                    switchToContentPage();
                }else{
                    showNoDataIndicator("No items");
                }
            }
        }
    }

    public void clearSearch(){
        SearchDetails search_Details = new SearchDetails();
        if ( searchDetails != null ){   search_Details.setStatus(searchDetails.getStatus());    }
        this.searchDetails = search_Details;
        performSearch(search_Details);
        filterFragment.clearSelection();
    }


    //---------

    public void hideMenuItems(){
        try {
            filtereMenuItem.setVisible(false);
            searchViewMenuITem.setVisible(false);searchViewMenuITem.collapseActionView();
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    public void showMenuItems(){
        try {
            filtereMenuItem.setVisible(true);
            searchViewMenuITem.setVisible(true);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_orders_by_date, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filtereMenuItem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                //onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchDetails search_Details = new SearchDetails().setConsiderAtleastOneMatch(query);
                if ( searchDetails != null ){   search_Details.setStatus(searchDetails.getStatus());    }
                performSearch(search_Details);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){

        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            super.onBackPressed();
        }
    }


}