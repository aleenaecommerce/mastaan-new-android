package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.ConfigurationsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.MembershipDetails;

/**
 * Created by Venkatesh Uppu on 11/01/19.
 */

public class AddOrEditMembershipActivity extends MastaanToolbarActivity implements View.OnClickListener {

    String actionType = Constants.ADD;
    String membershipType;
    String membershipID;

    RadioButton active, inactive;
    vTextInputLayout type;
    vTextInputLayout background_color, text_color;
    vTextInputLayout buyer_total_orders_minimum_amount;
    vTextInputLayout buyer_total_orders_maximum_amount;
    vTextInputLayout order_discount;
    vTextInputLayout order_minimum__amount;
    vTextInputLayout order_maximum_discount;
    vTextInputLayout delivery_charges_discount;
    vTextInputLayout delivery_charges_order_minimum__amount;

    Button actionDone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_membership);

        //---------

        active = findViewById(R.id.active);
        inactive = findViewById(R.id.inactive);
        type = findViewById(R.id.type);
        background_color = findViewById(R.id.background_color);
        text_color = findViewById(R.id.text_color);
        buyer_total_orders_minimum_amount = findViewById(R.id.buyer_total_orders_minimum_amount);
        buyer_total_orders_maximum_amount = findViewById(R.id.buyer_total_orders_maximum_amount);
        order_discount = findViewById(R.id.order_discount);
        order_minimum__amount = findViewById(R.id.order_minimum__amount);
        order_maximum_discount = findViewById(R.id.order_maximum_discount);
        delivery_charges_discount = findViewById(R.id.delivery_charges_discount);
        delivery_charges_order_minimum__amount = findViewById(R.id.delivery_charges_order_minimum__amount);

        actionDone = findViewById(R.id.actionDone);
        actionDone.setOnClickListener(this);

        //--------

        actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        if ( actionType == null ){  actionType = Constants.ADD; }

        if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
            MembershipDetails membershipDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.MEMBERSHIP_DETAILS), MembershipDetails.class);
            membershipID = membershipDetails.getID();
            membershipType = membershipDetails.getType();

            //-----

            setToolbarTitle("Edit Membership");
            actionDone.setText("UPDATE");

            if ( membershipDetails.getStatus() ){
                active.setChecked(true);
            }else{  inactive.setChecked(true);  }

            type.setText(membershipDetails.getTypeName());

            background_color.setText(membershipDetails.getBackgroundColor());
            text_color.setText(membershipDetails.getTextColor());

            buyer_total_orders_minimum_amount.setText(CommonMethods.getInDecimalFormat(membershipDetails.getBuyerTotalOrdersMinimumAmount()));
            buyer_total_orders_maximum_amount.setText(CommonMethods.getInDecimalFormat(membershipDetails.getBuyerTotalOrdersMaximumAmount()));

            order_discount.setText(CommonMethods.getInDecimalFormat(membershipDetails.getOrderDiscount()));
            order_minimum__amount.setText(CommonMethods.getInDecimalFormat(membershipDetails.getOrderMinimumAmount()));
            order_maximum_discount.setText(CommonMethods.getInDecimalFormat(membershipDetails.getOrderMaximumDiscount()));

            delivery_charges_discount.setText(CommonMethods.getInDecimalFormat(membershipDetails.getDeliveryChargesDiscount()));
            delivery_charges_order_minimum__amount.setText(CommonMethods.getInDecimalFormat(membershipDetails.getDeliveryChargesOrderMinimumAmount()));
        }
        else{
            membershipType = getIntent().getStringExtra(Constants.TYPE);

            type.setText(getIntent().getStringExtra(Constants.TYPE_NAME)!=null?getIntent().getStringExtra(Constants.TYPE_NAME):membershipType);

            setToolbarTitle("Add Membership");
            actionDone.setText("ADD");
        }

    }

    @Override
    public void onClick(View view) {

        if ( view == actionDone ){
            boolean eStatus = active.isChecked();
            String eBackgroundColor = background_color.getText();
            String eTextColor = text_color.getText();
            String eBuyerTotalOrdersMinimumAmount = buyer_total_orders_minimum_amount.getText();
            buyer_total_orders_minimum_amount.checkError("* Enter minimum amount");
            String eBuyerTotalOrdersMaximumAmount = buyer_total_orders_maximum_amount.getText();
            String eOrderDiscount = order_discount.getText();
            String eOrderMinimumAmount = order_minimum__amount.getText();
            String eOrderMaximumDiscount = order_maximum_discount.getText();
            String eDeliveryChargesDiscount = delivery_charges_discount.getText();
            String eDeliveryChargesOrderMinimumAmount = delivery_charges_order_minimum__amount.getText();

            if ( eBuyerTotalOrdersMinimumAmount.length() > 0 ){
                if ( CommonMethods.parseDouble(eBuyerTotalOrdersMinimumAmount) <= 0 ){
                    buyer_total_orders_minimum_amount.showError("* Enter valid amount (>0)");
                    return;
                }
                if ( CommonMethods.parseDouble(eBuyerTotalOrdersMaximumAmount) > 0 && CommonMethods.parseDouble(eBuyerTotalOrdersMinimumAmount) > CommonMethods.parseDouble(eBuyerTotalOrdersMaximumAmount) ){
                    buyer_total_orders_minimum_amount.showError("* Must be less than "+eBuyerTotalOrdersMaximumAmount);
                    return;
                }
                if ( CommonMethods.parseDouble(eOrderDiscount) < 0 || CommonMethods.parseDouble(eOrderDiscount) > 100 ){
                    order_discount.showError("* Must be between 0 to 100%");
                    return;
                }
                if ( CommonMethods.parseDouble(eDeliveryChargesDiscount) < 0 || CommonMethods.parseDouble(eDeliveryChargesDiscount) > 100 ){
                    delivery_charges_discount.showError("* Must be between 0 to 100%");
                    return;
                }
                if ( eBackgroundColor.length() > 0 && eBackgroundColor.charAt(0) != '#' ){
                    background_color.showError("* Must start with #");
                    return;
                }
                if ( eTextColor.length() > 0 && eTextColor.charAt(0) != '#' ){
                    text_color.showError("* Must start with #");
                    return;
                }

                // Validating
                if ( CommonMethods.parseDouble(eOrderDiscount) <= 0 ){
                    eOrderMinimumAmount = "0";
                    eOrderMaximumDiscount = "0";
                }


                MembershipDetails membershipDetails = new MembershipDetails(eStatus, membershipType
                        , CommonMethods.parseDouble(eBuyerTotalOrdersMinimumAmount), CommonMethods.parseDouble(eBuyerTotalOrdersMaximumAmount)
                        , CommonMethods.parseDouble(eOrderDiscount), CommonMethods.parseDouble(eOrderMinimumAmount), CommonMethods.parseDouble(eOrderMaximumDiscount)
                        , CommonMethods.parseDouble(eDeliveryChargesDiscount), CommonMethods.parseDouble(eDeliveryChargesOrderMinimumAmount)
                ).setBackgroundColor(eBackgroundColor).setTextColor(eTextColor);

                if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                    updateMembership(membershipID, membershipDetails);
                }else{
                    addMembership(membershipDetails);
                }
            }
        }
    }

    //-----

    private void addMembership(MembershipDetails membership_details){

        showLoadingDialog("Adding membership, wait...");
        getBackendAPIs().getConfigurationsAPI().addMembership(membership_details, new ConfigurationsAPI.MembershipCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, MembershipDetails membershipDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Membership added successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                    resultData.putExtra(Constants.MEMBERSHIP_DETAILS, new Gson().toJson(membershipDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    private void updateMembership(final String membershipID, final MembershipDetails membershipDetails){

        showLoadingDialog("Updating membership, wait...");
        getBackendAPIs().getConfigurationsAPI().updateMembership(membershipID, membershipDetails, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Membership updated successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                    resultData.putExtra(Constants.MEMBERSHIP_DETAILS, new Gson().toJson(membershipDetails.setID(membershipID)));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}