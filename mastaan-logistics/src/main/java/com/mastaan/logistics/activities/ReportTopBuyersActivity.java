package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.AnalyticsAdapter;
import com.mastaan.logistics.backend.AnalyticsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.models.AnalyticsDetails;
import com.mastaan.logistics.models.AnalyticsInstallSource;
import com.mastaan.logistics.models.AnalyticsTopBuyer;
import com.mastaan.logistics.models.BuyerSearchDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 18/08/18.
 */

public class ReportTopBuyersActivity extends MastaanToolbarActivity {

    vRecyclerView itemsHolder;
    AnalyticsAdapter analyticsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_top_buyers);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        itemsHolder = (vRecyclerView) findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        analyticsAdapter = new AnalyticsAdapter(activity, new ArrayList<AnalyticsDetails>(), new AnalyticsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                new BuyerOptionsDialog(activity).show(analyticsAdapter.getItem(position).getAnalyticsTopBuyer().getBuyer());
            }
            @Override
            public void onActionClick(int position) {

            }
        });
        itemsHolder.setAdapter(analyticsAdapter);

        //----

        getData();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getData();
    }

    public void getData(){

        showLoadingIndicator("Loading, wait...");
        getBackendAPIs().getAnalyticsAPI().getTopBuyersAnalytics(500, new AnalyticsAPI.AnalyticsTopBuyersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<AnalyticsTopBuyer> analyticsTopBuyers) {
                if ( status ){
                    if ( analyticsTopBuyers.size() > 0 ){
                        List<AnalyticsDetails> analyticsList = new ArrayList<>();
                        for (int i=0;i<analyticsTopBuyers.size();i++){
                            analyticsList.add(new AnalyticsDetails().setAnalyticsTopBuyer(analyticsTopBuyers.get(i)));
                        }
                        analyticsAdapter.setItems(analyticsList);
                        switchToContentPage();
                    }
                    else{
                        showNoDataIndicator("Nothing to show");
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}