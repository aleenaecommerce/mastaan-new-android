package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.google.firebase.BuildConfig;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.CommonAPI;
import com.mastaan.logistics.models.Bootstrap;
import com.mastaan.logistics.services.LogisticsService;
import com.testfairy.TestFairy;

public class LaunchActivity extends MastaanToolbarActivity implements View.OnClickListener{

    Intent logisticsService;

    TextView version_name;

    ProgressBar loading_indicator;
    View reload_indicator;
    TextView loading_message;

    View qrCodes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        TestFairy.begin(this, getString(R.string.testfailry_app_token));    // Initializing Testfairy

        logisticsService = new Intent(context, LogisticsService.class);
        context.stopService(logisticsService);

//        OrderItemsDatabase orderItemsDatabase = new OrderItemsDatabase(context);
//        orderItemsDatabase.openDatabase();
//        orderItemsDatabase.deleteAllOrderItems();

        localStorageData.setHomePageLastLoadedTime(DateMethods.getCurrentDateAndTime());
        localStorageData.setCreateJobRequestID("");
        localStorageData.setWhereToNextRequestID("");

        //--------

        version_name = (TextView) findViewById(R.id.version_name);

        loading_indicator = (ProgressBar) findViewById(R.id.loading_indicator);
        loading_message = (TextView) findViewById(R.id.loading_message);
        reload_indicator = findViewById(R.id.reload_indicator);
        reload_indicator.setOnClickListener(this);

        qrCodes = findViewById(R.id.qrCodes);
        qrCodes.setOnClickListener(this);

        //--------

        version_name.setText("v" + BuildConfig.VERSION_NAME);

        //--------

        checkInternetConnection(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status) {
                    // IF LOGGED IN
                    if (localStorageData.getSessionFlag()) {
                        getBootStrap();
                    }
                    // IF NOT LOGGED IN
                    else {
                        new CountDownTimer(800, 400) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                Intent loginAct = new Intent(context, LoginActivity.class);
                                startActivity(loginAct);
                                finish();
                            }
                        }.start();
                    }
                }
            }
        });

    }

    @Override
    public void onClick(View view) {

        if ( view == reload_indicator ){
            getBootStrap();
        }
        else if ( view == qrCodes ){
            Intent qrCodesAct = new Intent(context, QRCodesActivity.class);
            startActivity(qrCodesAct);
        }
    }

    private void getBootStrap() {

        reload_indicator.setVisibility(View.GONE);
        loading_indicator.setVisibility(View.VISIBLE);
        loading_message.setText("TALKING TO SERVER");

        getBackendAPIs().getCommonAPI().getBootStrap(new CommonAPI.BootstrapCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, Bootstrap bootstrap) {
                if (status) {
                    localStorageData.setServerTime(DateMethods.getReadableDateFromUTC(bootstrap.getServerTime()));

                    localStorageData.setPendingFollowupsLastOrderDays(bootstrap.getConfiguration().getPendingFollowupsLastOrderDays());

                    localStorageData.setUpgradeURL(bootstrap.getUpgradeURL());
                    localStorageData.setUser(bootstrap.getUserDetails());
                    localStorageData.storeWarehouse(bootstrap.getWarehouseDetails());
                    localStorageData.storeWarehousesList(bootstrap.getWarehouses());
                    localStorageData.storeWhereToNextPlaces(bootstrap.getWhereToNextPlaces());
                    localStorageData.storeFailureReasons(bootstrap.getFailureReasons());
                    localStorageData.storeQCFailureReasons(bootstrap.getQcFailureReasons());
                    localStorageData.storeDeliveryFailureReasons(bootstrap.getDeliveryFailureReasons());
                    localStorageData.storeDiscountReasons(bootstrap.getDiscountReasons());
                    localStorageData.storeCustomerSupportReasons(bootstrap.getCustomerSupportReasons());
                    localStorageData.storeFollowupReasons(bootstrap.getFollowupReasons());
                    localStorageData.storeCartFollowupReasons(bootstrap.getCartFollowupReasons());
                    localStorageData.storeItemNotAvailableReasons(bootstrap.getItemNotAvailableReasons());
                    localStorageData.storeInstallSources(bootstrap.getInstallSources());
                    localStorageData.storeCustomerMessages(bootstrap.getCustomerMessages());
                    localStorageData.storeEmployeeViolationsList(bootstrap.getEmployeeViolations());

                    // IF VALID SESSION
                    if ( bootstrap.getUserDetails() != null && bootstrap.getUserDetails().getID() != null && bootstrap.getUserDetails().getID().length() > 0 ){
                        // STARTING SERVICE
                        context.startService(logisticsService);

                        // Link employee to fcm
                        try{
                            if ( FirebaseInstanceId.getInstance().getToken() != null &&
                                    (bootstrap.getUserDetails().getFCMRegistrationID() == null || bootstrap.getUserDetails().getFCMRegistrationID().length() == 0 || bootstrap.getUserDetails().getFCMRegistrationID().equals(FirebaseInstanceId.getInstance().getToken()) == false) ) {
                                getBackendAPIs().getEmployeesAPI().linkToFCM(FirebaseInstanceId.getInstance().getToken(), null);
                            }
                        }catch (Exception e){}

                        if ( bootstrap.getJob() != null && bootstrap.getJob().getType() != null ) {
                            //HyperTrack.startTracking();  // Starting HyperTrack tracking if job exists

                            if ( bootstrap.getJob().getType().equalsIgnoreCase("d")) {
                                Intent confirmDeliveryAct = new Intent(context, DispatcherActivity.class);
                                confirmDeliveryAct.putExtra("job", new Gson().toJson(bootstrap.getJob()));
                                startActivity(confirmDeliveryAct);
                                finish();
                            }else{
                                Intent whereToNextAct = new Intent(context, WhereToNextActivity.class);
                                whereToNextAct.putExtra("jobDetails", new Gson().toJson(bootstrap.getJob()));
                                startActivity(whereToNextAct);
                                finish();
                            }
                        } else {
                            //HyperTrack.stopTracking();  // Stopping HyperTrack tracking if no job exists

                            Intent homeAct = new Intent(context, HomeActivity.class);//IntentCompat.makeRestartActivityTask(componentName);
                            startActivity(homeAct);
                            finish();
                        }
                    }
                    // IF INVALID SESSION
                    else{
                        //HyperTrack.stopTracking();  // Stopping HyperTrack tracking if no job exists

                        localStorageData.clearSession();
                        Intent loginAct = new Intent(context, LoginActivity.class);
                        startActivity(loginAct);
                        finish();
                    }
                }
                else {
                    if (statusCode == 403) {
                        localStorageData.clearSession();
                        Intent loginAct = new Intent(context, LoginActivity.class);
                        startActivity(loginAct);
                        finish();
                    }else if (statusCode == 503 && message != null && message.length() > 0) {
                        loading_indicator.setVisibility(View.INVISIBLE);
                        reload_indicator.setVisibility(View.VISIBLE);
                        loading_message.setText(message);
                    }else {
                        loading_indicator.setVisibility(View.GONE);
                        reload_indicator.setVisibility(View.VISIBLE);
                        loading_message.setText("Error in connecting to server, try again!");
                    }
                }
            }
        });
    }

}
