package com.mastaan.logistics.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.dialogs.ImageDialog;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.FileChooserMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.ConfigurationsAPI;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateAlertMessage;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.MessageDetails;
import com.mastaan.logistics.widgets.TypeCompletionTextView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class AddOrEditAlertMessageActivity extends MastaanToolbarActivity implements View.OnClickListener {

    String actionType = Constants.ADD;
    String messageType;
    String messageID;

    RadioButton active, inactive;
    vTextInputLayout start_date;
    View clearStartDate;
    vTextInputLayout end_date;
    View clearEndDate;
    CheckBox show_once;
    vTextInputLayout typeView;
    TypeCompletionTextView type;
    View showTypes;
    vTextInputLayout title;
    vTextInputLayout details;
    vTextInputLayout url;
    View clearURL;
    vTextInputLayout app_url;
    View clearAppURL;
    vTextInputLayout action;

    ImageView image;
    View addImage;
    View updateImage;
    View deleteImage;

    Button actionDone;

    FileChooserMethods fileChooserMethods;

    String selectedImage;

    File selectedFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_alert_message);

        //---------

        active = findViewById(R.id.active);
        inactive = findViewById(R.id.inactive);
        start_date = findViewById(R.id.start_date);
        start_date.getEditText().setOnClickListener(this);
        clearStartDate = findViewById(R.id.clearStartDate);
        clearStartDate.setOnClickListener(this);
        end_date = findViewById(R.id.end_date);
        end_date.getEditText().setOnClickListener(this);
        clearEndDate = findViewById(R.id.clearEndDate);
        clearEndDate.setOnClickListener(this);
        show_once = findViewById(R.id.show_once);
        typeView = findViewById(R.id.typeView);
        type = findViewById(R.id.type);
        showTypes = findViewById(R.id.showTypes);
        showTypes.setOnClickListener(this);
        title = findViewById(R.id.title);
        details = findViewById(R.id.details);
        url = findViewById(R.id.url);
        clearURL = findViewById(R.id.clearURL);
        clearURL.setOnClickListener(this);
        app_url = findViewById(R.id.app_url);
        app_url.getEditText().setOnClickListener(this);
        clearAppURL = findViewById(R.id.clearAppURL);
        clearAppURL.setOnClickListener(this);
        action = findViewById(R.id.action);

        image = findViewById(R.id.image);
        image.setOnClickListener(this);
        addImage = findViewById(R.id.addImage);
        addImage.setOnClickListener(this);
        updateImage = findViewById(R.id.updateImage);
        updateImage.setOnClickListener(this);
        deleteImage = findViewById(R.id.deleteImage);
        deleteImage.setOnClickListener(this);

        actionDone = findViewById(R.id.actionDone);
        actionDone.setOnClickListener(this);

        type.allowCollapse(false);
        type.setThreshold(1);
        type.performBestGuess(false);
        //type.allowDuplicates(false);

        //--------

        actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        if ( actionType == null ){  actionType = Constants.ADD; }

        if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
            MessageDetails messageDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.MESSAGE_DETAILS), MessageDetails.class);
            messageID = messageDetails.getID();
            messageType = messageDetails.getType();

            selectedImage = messageDetails.getImage();

            //-----

            setToolbarTitle("Edit Alert Message");
            actionDone.setText("UPDATE");

            if ( messageDetails.getStatus() ){
                active.setChecked(true);
            }else{  inactive.setChecked(true);  }

            show_once.setChecked(messageDetails.showOnce());
            start_date.setText(DateMethods.getReadableDateFromUTC(messageDetails.getStartDate(), DateConstants.MMM_DD_YYYY));
            end_date.setText(DateMethods.getReadableDateFromUTC(messageDetails.getEndDate(), DateConstants.MMM_DD_YYYY));

            type.setTypes(messageDetails.getType());

            title.setText(messageDetails.getTitle());
            details.setText(messageDetails.getDetails());

            if ( messageDetails.getImage() != null && messageDetails.getImage().length() > 0 ) {
                Picasso.get()
                        .load(messageDetails.getImage())
                        .placeholder(R.drawable.image_default)
                        .error(R.drawable.image_default)
                        .into(image);
                addImage.setVisibility(View.GONE);
            }

            url.setText(messageDetails.getURL());
            app_url.setText(messageDetails.getAppURL());

            action.setText(messageDetails.getAction());
        }
        else{
            messageType = getIntent().getStringExtra(Constants.TYPE);

            setToolbarTitle("Add Alert Message");
            actionDone.setText("ADD");
        }

        //-------

        if ( messageType != null && messageType.equalsIgnoreCase("launchmessage") ){
            type.setAdapter(new ListArrayAdapter(context, R.layout.view_list_item_dropdown_item, new String[]{"launchmessage"}));
            type.setTypes("launchmessage");
            show_once.setVisibility(View.VISIBLE);
            type.setFocusable(false);
        }else if ( messageType != null && messageType.equalsIgnoreCase("webslider") ){
            type.setAdapter(new ListArrayAdapter(context, R.layout.view_list_item_dropdown_item, new String[]{"webslider"}));
            type.setTypes("webslider");
            toggleViewsVisibility(new View[]{show_once, title, details, app_url, clearAppURL, action}, View.GONE);
            type.setFocusable(false);
        }else{
            List<String> types = CommonMethods.getStringListFromStringArray(Constants.MESSAGE_TYPES);
            types.remove("launchmessage");
            types.remove("webslider");
            type.setAdapter(new ListArrayAdapter(context, R.layout.view_list_item_dropdown_item, types));
            show_once.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View view) {

        if ( view == start_date.getEditText() ){
            String prefillDate = start_date.getText();
            if ( prefillDate == null || prefillDate.length() == 0 ){    prefillDate = DateMethods.getCurrentDate(); }

            showDatePickerDialog("Start Date", null, end_date.getText(), prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    start_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }
        else if ( view == clearStartDate ){
            start_date.setText("");
        }

        else if ( view == end_date.getEditText() ){
            String prefillDate = end_date.getText();
            if ( prefillDate == null || prefillDate.length() == 0 ){    prefillDate = DateMethods.getCurrentDate(); }

            showDatePickerDialog("End Date", start_date.getText(), null, prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    end_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }
        else if ( view == clearEndDate ){
            end_date.setText("");
        }

        else if ( view == showTypes ){
            showListChooserDialog("Supported Types", Constants.MESSAGE_TYPES, null);
        }

        else if ( view == app_url.getEditText() ){
            final String []items = new String[]{"profile", "referral", "loyalty", "meatitems", "playstore", "settings", "legal"};
            showListChooserDialog("Select", items, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( items[position].equalsIgnoreCase("meatitems") ){
                        Intent categoriesAct = new Intent(context, CategoriesActivity.class);
                        startActivityForResult(categoriesAct, Constants.CATEGORIES_ACTIVITY_CODE);
                    }else{
                        app_url.setText(items[position]);
                    }
                }
            });
        }

        else if ( view == clearURL ){
            url.setText("");
        }
        else if ( view == clearAppURL ){
            app_url.setText("");
        }

        else if ( view == image ){
            ImageDialog imageDialog = new ImageDialog(context);
            if ( selectedImage != null ){   imageDialog.show(selectedImage);    }
            else if ( selectedFile != null ){   imageDialog.show(selectedFile);    }
        }
        else if ( view == addImage || view == updateImage ){
            fileChooserMethods = new FileChooserMethods(context).showFileChooser("image/*", new FileChooserMethods.FileChooserCallback() {
                @Override
                public void onComplete(boolean status, Uri uri, File file, String filePath) {
                    selectedImage = "";selectedFile = file;
                    Picasso.get()
                            .load(file)
                            .placeholder(R.drawable.image_default)
                            .error(R.drawable.image_default)
                            .into(image);
                    addImage.setVisibility(View.GONE);
                }
            });
        }
        else if ( view == deleteImage ){
            selectedImage = "";selectedFile = null;
            image.setImageBitmap(null);
            addImage.setVisibility(View.VISIBLE);
        }

        else if ( view == actionDone ){
            hideSoftKeyBoard(new View[]{title, details, action, url});

            inputMethodManager.hideSoftInputFromWindow(title.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            boolean eStatus = false;
            if ( active.isChecked() ){  eStatus = true; }
            String eStartDate = DateMethods.getDateInFormat(start_date.getText(), DateConstants.DD_MM_YYYY);
            String eEndDate = DateMethods.getDateInFormat(end_date.getText(), DateConstants.DD_MM_YYYY);
            boolean eShowOnce = show_once.isChecked()?true:false;
            List<String> eTypes = type.getObjects();
            typeView.checkError("* Enter types");
            String eTitle = title.getText();
            String eDetails = details.getText();
            String eURL = url.getText();
            String eAppURL = app_url.getText();
            String eAction = action.getText();

            if ( eTypes == null || eTypes.size() == 0 ){
                typeView.showError("* Enter types");
                return;
            }

            // Preventing launch message combination
            if ( messageType != null && messageType.equalsIgnoreCase("launchmessage") ){
                eTypes.clear();eTypes.add("launchmessage");
            }else{
                eTypes.remove("launchmessage");
            }

            if ( eTitle.trim().length() > 0 || eDetails.trim().length() > 0
                    || ((selectedImage != null && selectedImage.length() > 0) || selectedFile != null ) ){
                RequestAddOrUpdateAlertMessage requestObject = new RequestAddOrUpdateAlertMessage(eTypes, eStatus, eTitle, eDetails, selectedImage, eURL, eAppURL, eAction, eStartDate, eEndDate, eShowOnce).setAttachment(selectedFile);

                if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                    updateAlertMessage(messageID, requestObject);
                }else{
                    addAlertMessage(requestObject);
                }
            }else{
                showToastMessage("* Give at least (Title or Details or Image)");
            }
        }
    }

    //-----

    private void addAlertMessage(RequestAddOrUpdateAlertMessage requestObject){

        showLoadingDialog("Adding alert message, wait...");
        getBackendAPIs().getConfigurationsAPI().addAlertMessage(requestObject, new ConfigurationsAPI.MessageCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, MessageDetails messageDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Alert message added successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                    resultData.putExtra(Constants.MESSAGE_DETAILS, new Gson().toJson(messageDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    private void updateAlertMessage(String messageID, RequestAddOrUpdateAlertMessage requestObject){

        showLoadingDialog("Updating alert message, wait...");
        getBackendAPIs().getConfigurationsAPI().updateAlertMessage(messageID, requestObject, new ConfigurationsAPI.MessageCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, MessageDetails messageDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Alert message updated successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                    resultData.putExtra(Constants.MESSAGE_DETAILS, new Gson().toJson(messageDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    //===========

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( fileChooserMethods != null ){  fileChooserMethods.onActivityResult(requestCode, resultCode, data); }

        try{
            if ( requestCode == Constants.CATEGORIES_ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {
                CategoryDetails categoryDetails = new Gson().fromJson(data.getStringExtra(Constants.CATEGORY_DETAILS), CategoryDetails.class);
                if ( categoryDetails != null ){
                    app_url.setText("meatitems"+","+categoryDetails.getID()+","+categoryDetails.getValue()+","+categoryDetails.getName());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}