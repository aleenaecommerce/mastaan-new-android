package com.mastaan.logistics.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.ManageDatesAdapter;
import com.mastaan.logistics.backend.DaysAPI;
import com.mastaan.logistics.backend.models.RequestManageDayAvailability;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.DayDetails;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DeliveryDatesActivity extends MastaanToolbarActivity implements View.OnClickListener{

    private static final int MAX_DAYS_COUNT = 42;
    // seasons' rainbow
    int[] rainbow = new int[]{
            R.color.summer,
            R.color.fall,
            R.color.winter,
            R.color.spring
    };
    // month-season association (northern hemisphere, sorry australia :)
    int[] monthSeason = new int[]{2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2};
    ManageDatesAdapter adapter;
    // date format
    private String dateFormat = "MMM yyyy";
    // current displayed month
    private Calendar currentDate = Calendar.getInstance();
    // internal components
    private LinearLayout header;
    private ImageView btnPrev;
    private ImageView btnNext;
    private TextView txtDate;
    private GridView grid;

    List<CategoryDetails> categoriesList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_dates);

        //---------

        header = (LinearLayout) findViewById(R.id.calendar_header);
        btnPrev = (ImageView) findViewById(R.id.calendar_prev_button);
        btnPrev.setOnClickListener(this);
        btnNext = (ImageView) findViewById(R.id.calendar_next_button);
        btnNext.setOnClickListener(this);
        txtDate = (TextView) findViewById(R.id.calendar_date_display);
        txtDate.setText(DateMethods.getDateInFormat(localStorageData.getServerTime(), DateConstants.MMM_YYYY));
        grid = (GridView) findViewById(R.id.calendar_grid);

        btnNext.setVisibility(View.INVISIBLE);

        //--------

        currentDate = DateMethods.getCalendarFromDate(localStorageData.getServerTime());
        updateCalendar(currentDate);
    }

    @Override
    public void onClick(View view) {
        if ( view == btnPrev ){
            updateCalendar(DateMethods.addToCalendarInMonths(currentDate, -1));
        }
        else if ( view == btnNext ){
            updateCalendar(DateMethods.addToCalendarInMonths(currentDate, 1));
        }
    }

    public void updateCalendar(final Calendar selectedDateCalendar) {
        showLoadingDialog("Loading...");
        getBackendAPIs().getDaysAPI().getDisabledDays(selectedDateCalendar.get(Calendar.MONTH) + 1, selectedDateCalendar.get(Calendar.YEAR), new DaysAPI.DisabledDatesListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<DayDetails> daysList, List<CategoryDetails> categories_List) {
                closeLoadingDialog();
                if (status) {
                    categoriesList = categories_List;
                    currentDate = selectedDateCalendar;
                    initOrUpdateTitle();
                    updateCalendar(daysList);
                } else {
                    showToastMessage("Something went wrong , please try again!");
                }
            }
        });
    }

    public void updateCalendar(final List<DayDetails> daysList) {

        showLoadingDialog("Displaying, wait...");
        getFormattedAttendaceReportsList((Calendar) currentDate.clone(), daysList, new FormattedReportsListCallback() {
            @Override
            public void onComplete(boolean status, List<DayDetails> formattedDaysList) {
                closeLoadingDialog();
                int currentVisibleMonth = ((Calendar) currentDate.clone()).get(Calendar.MONTH);
                adapter = new ManageDatesAdapter(activity, formattedDaysList, currentVisibleMonth, new ManageDatesAdapter.Callback() {
                    @Override
                    public void onClick(int position) {
                        showManageDayAvailabilityDialog(position);
                    }
                });
                grid.setAdapter(adapter);
            }
        });

    }

    private void initOrUpdateTitle() {

        Calendar serverTime = DateMethods.getCalendarFromDate(localStorageData.getServerTime());
//        if ( currentDate.get(Calendar.MONTH) == serverTime.get(Calendar.MONTH)
//                && currentDate.get(Calendar.YEAR) == serverTime.get(Calendar.YEAR) ){
//            btnNext.setVisibility(View.INVISIBLE);
//        }else{
//            btnNext.setVisibility(View.VISIBLE);
//        }
        btnPrev.setVisibility(View.VISIBLE);
        btnNext.setVisibility(View.VISIBLE);
        // update title
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        txtDate.setText(sdf.format(currentDate.getTime()));

        // set header color according to current season
        int month = currentDate.get(Calendar.MONTH);
        int season = monthSeason[month];
        int color = rainbow[season];

        header.setBackgroundColor(getResources().getColor(color));
    }



    //-----

    private interface FormattedReportsListCallback{
        void onComplete(boolean status, List<DayDetails> attendanceReportsList);
    }
    private void getFormattedAttendaceReportsList(final Calendar dateCalendar, final List<DayDetails> daysList, final FormattedReportsListCallback callback){

        new AsyncTask<Void, Void, Void>() {
            List<DayDetails> formattedDaysList = new ArrayList<DayDetails>();
            @Override
            protected Void doInBackground(Void... params) {
                //Calendar calendar = (Calendar) currentDate.clone();

                int daysOfMonth = dateCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                int currentVisibleMonth = dateCalendar.get(Calendar.MONTH);
                dateCalendar.set(Calendar.DAY_OF_MONTH, 1);
                int monthBeginningCell = dateCalendar.get(Calendar.DAY_OF_WEEK) - 1;

                // move calendar backwards to the beginning of the week
                dateCalendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);
                int days_count = (daysOfMonth + monthBeginningCell) > 35 ? MAX_DAYS_COUNT : ((daysOfMonth + monthBeginningCell) > 28 ? 35 : 28);
                // fill cells
                ArrayList<Date> cells = new ArrayList<>();
                while (cells.size() < days_count) {
                    cells.add(dateCalendar.getTime());
                    dateCalendar.add(Calendar.DAY_OF_MONTH, 1);
                }
                for (Date cell : cells) {
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(cell);

                    DayDetails dayDetails = null;
                    for (int i=0;i<daysList.size();i++){
                        if (DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getDateFromCalendar(calendar1)), DateMethods.getOnlyDate(daysList.get(i).getDate())) == 0 ) {
                            dayDetails = daysList.get(i);
                            break;
                        }
                    }

                    if ( dayDetails == null ){
                        dayDetails = new DayDetails(DateMethods.getDateInUTCFormat(DateMethods.getDateString(cell)), true, "");
                    }
                    formattedDaysList.add(dayDetails);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, formattedDaysList);
                }
            }
        }.execute();

    }


    //---------

    public void showManageDayAvailabilityDialog(final int position){
        DayDetails dayDetails = adapter.getItem(position);

        if ( dayDetails != null ){
            View manageDayDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_manage_day_availability, null);
            TextView title = (TextView) manageDayDialogView.findViewById(R.id.title);
            final RadioButton enable = (RadioButton) manageDayDialogView.findViewById(R.id.enable);
            final RadioButton disable = (RadioButton) manageDayDialogView.findViewById(R.id.disable);
            final View disabledHolder = manageDayDialogView.findViewById(R.id.disabledHolder);
            final vTextInputLayout disable_message = (vTextInputLayout) manageDayDialogView.findViewById(R.id.disable_message);
            final LinearLayout categoriesHolder = (LinearLayout) manageDayDialogView.findViewById(R.id.categoriesHolder);

            final List<String> disabledCategoriesIDs = new ArrayList<>();
            final List<View> categoryViews = new ArrayList<>();
            for (int i=0;i<categoriesList.size();i++){
                final int index = i;
                View categoryCheckBoxView = LayoutInflater.from(context).inflate(R.layout.view_category_check_box, null);
                categoriesHolder.addView(categoryCheckBoxView);
                categoryViews.add(categoryCheckBoxView);
                CheckBox checkBox = (CheckBox) categoryCheckBoxView.findViewById(R.id.checkbox);

                checkBox.setText(CommonMethods.capitalizeFirstLetter(categoriesList.get(i).getName()+" "+categoriesList.get(i).getParentCategoryName()));
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                        if ( isChecked ){
                            if ( disabledCategoriesIDs.contains(categoriesList.get(index).getID()) == false ){
                                disabledCategoriesIDs.add(categoriesList.get(index).getID());
                            }
                        }
                        else{
                            if ( disabledCategoriesIDs.contains(categoriesList.get(index).getID()) ) {
                                disabledCategoriesIDs.remove(categoriesList.get(index).getID());
                            }
                        }
                    }
                });

                if ( dayDetails.getCategories().contains(categoriesList.get(index).getID()) ){
                    checkBox.setChecked(true);
                }
            }


            title.setText(DateMethods.getDateInFormat(dayDetails.getDate(), DateConstants.MMM_DD_YYYY));
            if ( dayDetails.isEnabled() ){
                enable.setChecked(true);
                disabledHolder.setVisibility(View.GONE);
            }
            else{
                disable.setChecked(true);
                disabledHolder.setVisibility(View.VISIBLE);

                if ( dayDetails.getCategories().size() == 0 ) {
                    for (int i = 0; i < categoryViews.size(); i++) {
                        ((CheckBox) categoryViews.get(i).findViewById(R.id.checkbox)).setChecked(true);
                    }
                }
                disable_message.setText(dayDetails.getMessage());
            }

            enable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if ( isChecked ){
                        disabledHolder.setVisibility(View.GONE);
                    }else{
                        disabledHolder.setVisibility(View.VISIBLE);
                        if ( disabledCategoriesIDs.size() == 0 ){
                            for (int i=0;i<categoryViews.size();i++){
                                ((CheckBox) categoryViews.get(i).findViewById(R.id.checkbox)).setChecked(true);
                            }
                            disable_message.setText("");
                        }
                    }
                }
            });

            manageDayDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeCustomDialog();
                }
            });
            manageDayDialogView.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( enable.isChecked() ){
                        closeCustomDialog();
                        RequestManageDayAvailability requestManageDayAvailability = new RequestManageDayAvailability(true, null, null);
                        manageDayAvailability(position, requestManageDayAvailability);
                    }
                    else {
                        String msg = disable_message.getText();
                        disable_message.checkError("* Enter message");
                        if ( disabledCategoriesIDs.size() > 0 ){
                            if ( msg.trim().length() > 0 ){
                                closeCustomDialog();
                                if ( disabledCategoriesIDs.size() == categoriesList.size() ){
                                    disabledCategoriesIDs.clear();
                                }
                                RequestManageDayAvailability requestManageDayAvailability = new RequestManageDayAvailability(false, disabledCategoriesIDs, msg);
                                manageDayAvailability(position, requestManageDayAvailability);
                            }
                        }else{
                            showToastMessage("* Please select atleast one category.");
                        }
                    }

                }
            });
            showCustomDialog(manageDayDialogView);


        }
    }


    public void manageDayAvailability(final int position, final RequestManageDayAvailability requestManageDayAvailability){
        final DayDetails dayDetails = adapter.getItem(position);

        showLoadingDialog("Updating, wait...");
        getBackendAPIs().getDaysAPI().manageDayAvailability(dayDetails.getDate(), requestManageDayAvailability, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    dayDetails.setEnabled(requestManageDayAvailability.isEnabled());
                    dayDetails.setMessage(requestManageDayAvailability.getMessage());
                    dayDetails.setCategories(requestManageDayAvailability.getCategories());
                    adapter.updateItem(position, dayDetails);
                    showToastMessage("Successfully updated");
                }else{
                    showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES")){
                                manageDayAvailability(position, requestManageDayAvailability);
                            }
                        }
                    });
                }
            }
        });
    }

    //==========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
