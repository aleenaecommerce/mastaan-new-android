package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.HubsAdapter;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateHub;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 07/11/18.
 */

public class HubsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String searchQuery;
    MenuItem searchViewMenuItem;
    SearchView searchView;

    vRecyclerView hubsHolder;
    View addHub;
    HubsAdapter hubsAdapter;

    int selectedItemIndex = -1;
    List<HubDetails> originalItemsList = new ArrayList<>();

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hubs);
        hasLoadingView();
        hasSwipeRefresh();

        userDetails = localStorageData.getUserDetails();

        //-----------------

        addHub = findViewById(R.id.addHub);
        addHub.setOnClickListener(this);

        hubsHolder = findViewById(R.id.hubsHolder);
        hubsHolder.setupVerticalOrientation();
        hubsAdapter = new HubsAdapter(activity, new ArrayList<HubDetails>(), new HubsAdapter.CallBack() {
            @Override
            public void onItemClick(final int position) {
                selectedItemIndex = position;

                final HubDetails hubDetails = hubsAdapter.getItem(position);

                final String []optionsList = new String[]{"Edit", hubDetails.getStatus()?"Disable":"Enable", "Delete"};
                showListChooserDialog(optionsList, new ListChooserCallback() {
                    @Override
                    public void onSelect(final int oPosition) {
                        if ( optionsList[oPosition].equalsIgnoreCase("Edit") ){
                            Intent editHubAct = new Intent(context, AddOrEditHubActivity.class);
                            editHubAct.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                            editHubAct.putExtra(Constants.HUB_DETAILS, new Gson().toJson(hubsAdapter.getItem(position)));
                            startActivityForResult(editHubAct, Constants.ADD_OR_EDIT_HUB_ACTIVITY_CODE);
                        }
                        else if ( optionsList[oPosition].equalsIgnoreCase("Disable") || optionsList[oPosition].equalsIgnoreCase("Enable") ){
                            showLoadingDialog((hubDetails.getStatus()?"Disabling":"Enabling")+" hub, wait...");
                            getBackendAPIs().getRoutesAPI().updateHub(hubDetails.getID(), new RequestAddOrUpdateHub(!hubDetails.getStatus()), new RoutesAPI.HubDetailsCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message, HubDetails hub_details) {
                                    closeLoadingDialog();
                                    if ( status ){
                                        hubDetails.setStatus(!hubDetails.getStatus());
                                        hubsAdapter.setItem(position, hubDetails);
                                        showToastMessage("Hub "+(hubDetails.getStatus()?"enabled":"disabled")+" successfully");
                                    }else{
                                        showToastMessage("Something went wrong, try again!");
                                    }
                                }
                            });
                        }
                        else if ( optionsList[oPosition].equalsIgnoreCase("Delete") ){
                            showLoadingDialog("Deleting hub, wait...");
                            getBackendAPIs().getRoutesAPI().deleteHub(hubDetails.getID(), new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                    closeLoadingDialog();
                                    if ( status ){
                                        hubsAdapter.deleteItem(position);
                                        showToastMessage("Hub deleted successfully");
                                    }else{
                                        showToastMessage("Something went wrong, try again!");
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
        hubsHolder.setAdapter(hubsAdapter);

        //-----

        // LOADING DELIVERY AREAS AFTER MENU CREATED

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        getHubs();
    }

    @Override
    public void onClick(View view) {
        if ( view == addHub){
            Intent addHubAct = new Intent(context, AddOrEditHubActivity.class);
            addHubAct.putExtra(Constants.ACTION_TYPE, Constants.ADD);
            startActivityForResult(addHubAct, Constants.ADD_OR_EDIT_HUB_ACTIVITY_CODE);
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getHubs();
    }

    public void getHubs(){

        searchViewMenuItem.setVisible(false);
        addHub.setVisibility(View.GONE);
        showLoadingIndicator("Loading hubs, wait..");
        getBackendAPIs().getRoutesAPI().getHubs(new RoutesAPI.HubsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<HubDetails> hubsList) {
                if (status == true) {
                    originalItemsList = hubsList;

                    addHub.setVisibility(View.VISIBLE);
                    if (hubsList.size() > 0) {
                        searchViewMenuItem.setVisible(true);
                        switchToContentPage();
                        hubsAdapter.setItems(hubsList);
                    }else {
                        showNoDataIndicator("No hubs");
                    }
                } else {
                    showReloadIndicator(statusCode, "Unable to load hubs, try again!");
                }
            }
        });
    }

    //--------------

    public void onPerformSearch(final String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchQuery = search_query;
            showLoadingIndicator("Searching for '"+search_query+"', wait...");
            new AsyncTask<Void, Void, Void>() {
                List<HubDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... voids) {
                    for (int i=0;i<originalItemsList.size();i++){
                        if ( originalItemsList.get(i).getName().toLowerCase().contains(searchQuery.toLowerCase()) ){
                            filteredList.add(originalItemsList.get(i));
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( searchQuery.equalsIgnoreCase(search_query) ){
                        if ( filteredList.size() > 0 ){
                            hubsAdapter.setItems(filteredList);
                            hubsHolder.smoothScrollToPosition(0);
                            switchToContentPage();
                        }else{
                            showNoDataIndicator("Nothing found for '"+searchQuery+"'");
                        }
                    }
                }
            }.execute();
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        hubsAdapter.setItems(originalItemsList);
        if ( hubsAdapter.getItemCount() > 0 ){
            hubsHolder.smoothScrollToPosition(0);
            switchToContentPage();
        }else{
            showNoDataIndicator("No delivery areas");
        }
    }

    //===============

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchViewMenuItem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.ADD_OR_EDIT_HUB_ACTIVITY_CODE && resultCode == RESULT_OK ){
                String actionType = data.getStringExtra(Constants.ACTION_TYPE);
                HubDetails hubDetails = new Gson().fromJson(data.getStringExtra(Constants.HUB_DETAILS), HubDetails.class);
                if ( actionType.equalsIgnoreCase(Constants.ADD) ){
                    hubsAdapter.addItem(hubDetails);
                    originalItemsList.add(hubDetails);
                    switchToContentPage();
                }
                else  if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                    hubsAdapter.setItem(selectedItemIndex, hubDetails);

                    for (int i=0;i<originalItemsList.size();i++){
                        if ( originalItemsList.get(i).getID().equals(hubDetails.getID())){
                            originalItemsList.set(i, hubDetails);
                            break;
                        }
                    }
                }
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){

        }else {
            onBackPressed();
        }
        return true;
    }


}
