package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.View;

import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

public class PendingQCActivity extends BaseOrderItemsActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();
        hasNavigationButton("Go to Pending Deliveries");

        startAutoRefreshTimer(10 * 60);         // 10 Minutes Interval

        //--------

        getPendingQCItemsFromDatabase();
    }

    @Override
    public void onNavigationButtonClick() {
        super.onNavigationButtonClick();
        Intent pendingDeliveriesAct = new Intent(context, PendingDeliveriesActivity.class);
        startActivity(pendingDeliveriesAct);
        finish();
    }

    @Override
    public void onAutoRefreshTimeElapsed() {
        super.onAutoRefreshTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onInactiveTimeElapsed() {
        super.onInactiveTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onReloadPage() {
        super.onReloadPage();
        getPendingQCItems(false);
    }

    @Override
    public void onReloadFromDatabase() {
        super.onReloadFromDatabase();
        getPendingQCItemsFromDatabase();
    }

    @Override
    public void onBackgroundReloadPressed() {
        super.onBackgroundReloadPressed();
        getPendingQCItems(true);
    }


    public void getPendingQCItemsFromDatabase(){

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        hideNavigationButton();
        showLoadingIndicator("Loading pending QC items from database, wait...");
        getOrderItemsDatabase().getPendingQCItems(new OrderItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<OrderItemDetails> itemsList) {
                if (itemsList != null && itemsList.size() > 0) {
                    showLoadingIndicator("Displaying items, wait....");
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), itemsList, localStorageData.getWarehouseLocation(), new GroupingMethods.GroupedOrderItemsListCallback() {
                        @Override
                        public void onComplete(List<GroupedOrdersItemsList> groupedOrdersItemsLists) {
                            displayItems(false, groupedOrdersItemsLists, new ArrayList<OrderDetails>());
                            switchToContentPage();

                            getPendingQCItems(true);
                        }
                    });

                } else {
                    getPendingQCItems(false);
                }
            }
        });
    }

    private void getPendingQCItems(final boolean loadInBackground) {

        if ( loadInBackground ){
            loadingIndicator.setVisibility(View.VISIBLE);
            reloadIndicator.setVisibility(View.GONE);
        }else {
            showLoadingIndicator("Loading pending QC items, wait...");
        }
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        hideNavigationButton();
        getBackendAPIs().getOrdersAPI().getPendingQCItems(localStorageData.getWarehouseLocation(), new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
                loadingIndicator.setVisibility(View.GONE);
                if (status) {
                    if (loadInBackground) {
                        updateDataAndDisplayItems(groupedOrderItemsLists, ordersList);
                    } else {
                        displayItems(true, groupedOrderItemsLists, ordersList);
                    }
                } else {
                    if (loadInBackground) {
                        reloadIndicator.setVisibility(View.VISIBLE);
                        loadingIndicator.setVisibility(View.GONE);
                    } else {
                        showReloadIndicator("Unable to load pending process items, try again!");
                    }
                }
            }
        });
    }

}
