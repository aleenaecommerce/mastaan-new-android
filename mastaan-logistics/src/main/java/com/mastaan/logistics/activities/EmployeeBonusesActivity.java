package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.EmployeeBonusesAdapter;
import com.mastaan.logistics.backend.EmployeesAPI;
import com.mastaan.logistics.backend.models.RequestEmployeeBonuses;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AnalyseBonusesDialog;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.DatePeriodSelectorDialog;
import com.mastaan.logistics.dialogs.OrderDetailsDialog;
import com.mastaan.logistics.methods.AnalyseBonusesMethods;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.EmployeeBonusDetails;
import com.mastaan.logistics.models.SearchDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 01/06/18.
 */

public class EmployeeBonusesActivity extends MastaanToolbarActivity implements View.OnClickListener{

    MenuItem searchViewMenuITem;
    SearchView searchView;
    SearchDetails searchDetails;

    String employeeID;

    TextView from_date;
    TextView to_date;
    View changePeriod;
    TextView summary_info;

    View showSummary;

    vRecyclerView itemsHolder;
    EmployeeBonusesAdapter employeeBonusesAdapter;

    List<EmployeeBonusDetails> originalBonusesList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_bonuses);
        hasLoadingView();
        hasSwipeRefresh();

        if ( getIntent().getStringExtra(Constants.EMPLOYEE_ID) != null ){
            employeeID = getIntent().getStringExtra(Constants.EMPLOYEE_ID);
        }

        if ( getIntent().getStringExtra(Constants.TITLE) != null ){
            setToolbarTitle(getIntent().getStringExtra(Constants.TITLE));
        }

        //---------

        from_date = findViewById(R.id.from_date);
        to_date = findViewById(R.id.to_date);
        changePeriod = findViewById(R.id.changePeriod);
        changePeriod.setOnClickListener(this);
        summary_info= findViewById(R.id.summary_info);

        showSummary = findViewById(R.id.showSummary);
        showSummary.setOnClickListener(this);

        itemsHolder = findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        employeeBonusesAdapter = new EmployeeBonusesAdapter(activity, new ArrayList<EmployeeBonusDetails>(), new EmployeeBonusesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                final EmployeeBonusDetails employeeBonusDetails = employeeBonusesAdapter.getItem(position);
                final List<String> optionsList = new ArrayList<>();
                if ( employeeBonusDetails.getOrderDetails() != null ){
                    optionsList.add("Order details");
                }
                if ( employeeBonusDetails.getBuyerDetails() != null ){
                    optionsList.add("Buyer history");
                }
                if ( optionsList.size() > 0 ){
                    showListChooserDialog(null, optionsList, new ListChooserCallback() {
                        @Override
                        public void onSelect(int position) {
                            if ( optionsList.get(position).equalsIgnoreCase("Order details") ){
                                new OrderDetailsDialog(activity).show(employeeBonusDetails.getOrderDetails().getID());
                            }
                            else if ( optionsList.get(position).equalsIgnoreCase("Buyer history") ){
                                new BuyerOptionsDialog(activity).show(employeeBonusDetails.getBuyerDetails());
                            }
                        }
                    });
                }
            }
        });
        itemsHolder.setAdapter(employeeBonusesAdapter);

        //----

        //getData(null, null);
        showNoDataIndicator("Select date period");
        changePeriod.performClick();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getData(from_date.getText().toString(), to_date.getText().toString());
    }

    @Override
    public void onClick(View view) {
        if ( view == changePeriod ){
            String prefillFromDate = from_date.getText().toString();
            String prefillToDate = to_date.getText().toString();
            if ( prefillFromDate.equalsIgnoreCase("Start")&& prefillToDate.equalsIgnoreCase("End") ){
                prefillFromDate = DateMethods.getCurrentDate();
            }
            new DatePeriodSelectorDialog(activity).show(prefillFromDate, prefillToDate, new DatePeriodSelectorDialog.Callback() {
                @Override
                public void onComplete(String fromDate, String toDate) {
                    from_date.setText(fromDate != null&&fromDate.length()>0?DateMethods.getDateInFormat(fromDate, DateConstants.MMM_DD_YYYY):"Start");
                    to_date.setText(toDate != null&&toDate.length()>0?DateMethods.getDateInFormat(toDate, DateConstants.MMM_DD_YYYY):"End");
                    getData(fromDate, toDate);
                }
            });
        }
        else if ( view == showSummary ){
            if ( employeeID != null ){
                new AnalyseBonusesDialog(activity, "Employee Summary", CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW, Constants.BUYERS}), employeeBonusesAdapter.getAllItems()).show();
            }else {
                new AnalyseBonusesDialog(activity, "Summary", employeeBonusesAdapter.getAllItems()).show();
            }
        }
    }

    public void getData(String fromDate, String toDate){

        if ( (employeeID == null || employeeID.length() == 0)
                && (fromDate==null||fromDate.length()==0||fromDate.equalsIgnoreCase("Start"))
                && (toDate==null||toDate.length()==0||toDate.equalsIgnoreCase("End"))
                ){
            showNoDataIndicator("Select at least one date");
            return;
        }

        RequestEmployeeBonuses requestEmployeeBonuses = new RequestEmployeeBonuses();
        if ( employeeID != null && employeeID.length() > 0 ){
            requestEmployeeBonuses.setEmployee(employeeID);
        }
        if ( fromDate!=null && fromDate.length()>0 && fromDate.equalsIgnoreCase("Start")==false ){
            requestEmployeeBonuses.setFromDate(DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY));
        }
        if ( toDate!=null && toDate.length()>0 && toDate.equalsIgnoreCase("End")==false ){
            requestEmployeeBonuses.setToDate(DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY));
        }

        changePeriod.setClickable(false);
        summary_info.setVisibility(View.GONE);
        showSummary.setVisibility(View.GONE);
        showLoadingIndicator("Loading, wait...");
        getBackendAPIs().getEmployeesAPI().getEmployeeBonuses(requestEmployeeBonuses, new EmployeesAPI.EmployeeBonusesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<EmployeeBonusDetails> employeeBonuses) {
                changePeriod.setClickable(true);
                if ( status ){
                    originalBonusesList = employeeBonuses;

                    if ( employeeBonuses.size() > 0 ){
                        new AsyncTask<Void, Void, Void>() {
                            String summaryInfo = "";
                            @Override
                            protected Void doInBackground(Void... voids) {
                                summaryInfo = new AnalyseBonusesMethods(employeeBonuses)
                                        .setSectionsList(CommonMethods.getStringListFromStringArray(new String[]{Constants.OVERVIEW}))
                                        .analyse().getSummaryInfo();
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);
                                summary_info.setVisibility(View.VISIBLE);
                                showSummary.setVisibility(View.VISIBLE);
                                summary_info.setText(Html.fromHtml(summaryInfo));
                                employeeBonusesAdapter.setItems(employeeBonuses);
                                switchToContentPage();
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                    else{
                        showNoDataIndicator("Nothing to show");
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    //-------

    private void onPerformSearch(final String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchDetails = new SearchDetails().setConsiderAtleastOneMatch(search_query);

            showLoadingIndicator("Searching for ("+search_query+")");
            SearchMethods.searchBonuses(originalBonusesList, searchDetails, new SearchMethods.SearchEmployeeBonusesCallback() {
                @Override
                public void onComplete(boolean status, SearchDetails search_details, List<EmployeeBonusDetails> filteredList) {
                    if ( searchDetails == search_details ){
                        if ( status ){
                            summary_info.setVisibility(View.GONE);
                            if ( filteredList.size() > 0 ){
                                showToastMessage(filteredList.size()+" results found");
                                employeeBonusesAdapter.setItems(filteredList);
                                itemsHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else{
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");
                                employeeBonusesAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                }
            });
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        employeeBonusesAdapter.setItems(originalBonusesList);
        if ( employeeBonusesAdapter.getItemCount() > 0 ){
            summary_info.setVisibility(View.VISIBLE);
            switchToContentPage();
        }else{
            summary_info.setVisibility(View.GONE);
            showNoDataIndicator("No bonuses");
        }
    }

    //===========

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        searchViewMenuITem.setVisible(true);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){
        }else {
            onBackPressed();
        }
        return true;
    }

}