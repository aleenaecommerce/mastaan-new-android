package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.DeliverySlotsAdapter;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.backend.SlotsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.DeliveryFeeDetails;
import com.mastaan.logistics.models.DeliverySlotDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

public class DeliverySlotsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    MenuItem cutoffTimeMenuITem;
    MenuItem maxDistanceMenuITem;
    MenuItem maxOrdersMenuITem;

    vRecyclerView itemsHolder;
    DeliverySlotsAdapter deliverySlotsAdapter;

    View addSlot;

    int slotCutoffTime;
    double maxDeliveryDistance;
    long maxOrderCount;
    List<String> disabledDaysOfWeek;
    List<String> disabledCategories;
    List<DeliveryFeeDetails> deliveryCharges;

    int selectedItemPosition = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_slots);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        addSlot = findViewById(R.id.addSlot);
        addSlot.setOnClickListener(this);

        itemsHolder = (vRecyclerView) findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();

        deliverySlotsAdapter = new DeliverySlotsAdapter(activity, new ArrayList<DeliverySlotDetails>(), new DeliverySlotsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                selectedItemPosition = position;

                Intent addOrEditDeliverySlotAct = new Intent(context, AddOrEditDeliverySlotActivity.class);
                addOrEditDeliverySlotAct.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                addOrEditDeliverySlotAct.putExtra(Constants.SLOT_DETAILS, new Gson().toJson(deliverySlotsAdapter.getItem(position)));
                startActivityForResult(addOrEditDeliverySlotAct, Constants.ADD_OR_EDIT_DELIVERY_SLOT_ACTIVITY_CODE);
            }

            @Override
            public void onDeleteClick(final int position) {
                showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to delete <b>" + deliverySlotsAdapter.getItem(position).getDisplayText() + "</b> slot?"), "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES") ){
                            deliverySlotsAdapter.deleteItem(position);
                            updateSlots(deliverySlotsAdapter.getAllItems());
                        }
                    }
                });
            }
        });
        itemsHolder.setAdapter(deliverySlotsAdapter);

        //---------

        getSlotsConfig();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getSlotsConfig();
    }

    @Override
    public void onClick(View view) {
        if ( view == addSlot ){
            Intent addOrEditDeliverySlotAct = new Intent(context, AddOrEditDeliverySlotActivity.class);
            addOrEditDeliverySlotAct.putExtra(Constants.ACTION_TYPE, Constants.ADD);
            startActivityForResult(addOrEditDeliverySlotAct, Constants.ADD_OR_EDIT_DELIVERY_SLOT_ACTIVITY_CODE);
        }
    }

    public void getSlotsConfig(){

        addSlot.setVisibility(View.GONE);
        showLoadingIndicator("Loading slots, wait...");
        getBackendAPIs().getSlotsAPI().getSlotsConfig(new SlotsAPI.SlotsConfigCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final int slot_cutoffTime, final double max_DeliveryDistance, final long max_OrderCount, final List<String> disabled_DaysOfWeek, final List<String> disabled_Categories, final List<DeliveryFeeDetails> delivery_charges, final List<DeliverySlotDetails> slots_List) {
                if ( status ){
                    // Loading Warehouse meat items for cache purpose
                    activity.getBackendAPIs().getMeatItemsAPI().getMeatItems(new MeatItemsAPI.WarehouseMeatItemsCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<WarehouseMeatItemDetails> warehouseMeatItems) {
                            getMastaanApplication().setCachedWarehouseMeatItems(warehouseMeatItems);    // Caching Warehouse Meat Items

                            addSlot.setVisibility(View.VISIBLE);

                            displayGlobalSettings(slot_cutoffTime, max_DeliveryDistance, max_OrderCount, disabled_DaysOfWeek, disabled_Categories, delivery_charges);

                            if ( slots_List.size() > 0 ) {
                                deliverySlotsAdapter.setItems(slots_List);
                                switchToContentPage();
                            }else{
                                showNoDataIndicator("No slots to show");
                            }
                        }
                    });
                }else{
                    showReloadIndicator("Something went wrong, try again");
                }
            }
        });
    }

    public void updateSlotCutoffTime(int slotCutoffTime){
        updateSlots(slotCutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, deliverySlotsAdapter.getAllItems());
    }
    public void updateMaxDeliveryDistance(double maxDeliveryDistance){
        updateSlots(slotCutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, deliverySlotsAdapter.getAllItems());
    }
    public void updateMaxOrderCount(long maxOrderCount){
        updateSlots(slotCutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, deliverySlotsAdapter.getAllItems());
    }
    public void updateDisabledDaysOfWeek(List<String> disabledDaysOfWeek){
        updateSlots(slotCutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, deliverySlotsAdapter.getAllItems());
    }
    public void updateDisabledCategories(List<String> disabledCategories){
        updateSlots(slotCutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, deliverySlotsAdapter.getAllItems());
    }
    public void updateSlots(List<DeliverySlotDetails> slotsList){
        updateSlots(slotCutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, slotsList);
    }
    public void updateSlots(int slotCutoffTime, double maxDeliveryDistance, long maxOrderCount, List<String> disabledDaysOfWeek, List<String> disabledCategories, List<DeliverySlotDetails> slotsList){

        showLoadingDialog("Updating slots, wait...");
        getBackendAPIs().getSlotsAPI().updateSlotsConfig(slotCutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, deliveryCharges, slotsList, new SlotsAPI.SlotsConfigCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final int slotCutoffTime, final double maxDeliveryDistance, final long maxOrderCount, final List<String> disabledDaysOfWeek, final List<String> disabledCategories, final List<DeliveryFeeDetails> deliveryCharges, final List<DeliverySlotDetails> slotsList) {
                closeLoadingDialog();
                if ( status ){
                    displayGlobalSettings(slotCutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, deliveryCharges);
                    showNotificationDialog("Success", "Delivery slots updated successfully.");
                }else{
                    showChoiceSelectionDialog(false, "Failure", "Something went wrong while updating delivery slots, please try again.", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                updateSlots(slotCutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, slotsList);
                            }else{
                                onReloadPressed();
                            }
                        }
                    });
                }
            }
        });
    }

    private void displayGlobalSettings(int slotCutoffTime, double maxDeliveryDistance, long maxOrderCount, List<String> disabledDaysOfWeek, List<String> disabledCategories, List<DeliveryFeeDetails> deliveryCharges){
        this.slotCutoffTime = slotCutoffTime;
        this.maxDeliveryDistance = maxDeliveryDistance;
        this.maxOrderCount = maxOrderCount;
        this.disabledDaysOfWeek = disabledDaysOfWeek;
        this.disabledCategories = disabledCategories;
        this.deliveryCharges = deliveryCharges;

        try{
            cutoffTimeMenuITem.setTitle("Cutoff Time ("+slotCutoffTime+" min)");
            maxDistanceMenuITem.setTitle("Max. Distance ("+ CommonMethods.getInDecimalFormat(maxDeliveryDistance)+" kms)");
            maxOrdersMenuITem.setTitle("Max. Orders ("+maxOrderCount+")");
        }catch (Exception e){}
    }

    //===========

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_delivery_slots, menu);
        cutoffTimeMenuITem = menu.findItem(R.id.action_cutoff_time);
        maxDistanceMenuITem = menu.findItem(R.id.action_max_distance);
        maxOrdersMenuITem = menu.findItem(R.id.action_max_orders);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.ADD_OR_EDIT_DELIVERY_SLOT_ACTIVITY_CODE && resultCode == RESULT_OK ){
                DeliverySlotDetails deliverySlotDetails = new Gson().fromJson(data.getStringExtra(Constants.SLOT_DETAILS), DeliverySlotDetails.class);
                // Update
                if ( data.getStringExtra(Constants.ACTION_TYPE) != null && data.getStringExtra(Constants.ACTION_TYPE).equalsIgnoreCase(Constants.UPDATE) ){
                    deliverySlotsAdapter.setItem(selectedItemPosition, deliverySlotDetails);
                    switchToContentPage();
                    updateSlots(deliverySlotsAdapter.getAllItems());
                }
                // Add
                else{
                    deliverySlotsAdapter.addItem(itemsHolder.getLastCompletelyVisibleItemPosition()-1, deliverySlotDetails);
                    switchToContentPage();
                    updateSlots(deliverySlotsAdapter.getAllItems());
                }
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_cutoff_time ) {
            showInputNumberDecimalDialog("Cutoff Time", "Cutoff time (in Minutes)", slotCutoffTime, new TextInputCallback() {
                @Override
                public void onComplete(String inputText) {
                    updateSlotCutoffTime(Integer.parseInt(inputText));
                }
            });
        }else if ( item.getItemId() == R.id.action_max_distance ) {
            showInputNumberDecimalDialog("Maximum distance", "Maximum distance (in km)", maxDeliveryDistance, new TextInputCallback() {
                @Override
                public void onComplete(String inputText) {
                    updateMaxDeliveryDistance(Integer.parseInt(inputText));
                }
            });
        }else if ( item.getItemId() == R.id.action_max_orders ) {
            showInputNumberDecimalDialog("Maximum orders", "Maximum orders", maxOrderCount, new TextInputCallback() {
                @Override
                public void onComplete(String inputText) {
                    updateMaxOrderCount(Integer.parseInt(inputText));
                }
            });
        }else{
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}