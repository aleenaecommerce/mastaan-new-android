package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.WarehousesAdapter;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateWarehouse;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.WarehouseDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 13/04/19.
 */

public class WarehousesActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String searchQuery;
    MenuItem searchViewMenuItem;
    SearchView searchView;

    vRecyclerView warehousesHolder;
    View addWarehouse;
    WarehousesAdapter warehousesAdapter;

    int selectedItemIndex = -1;
    List<WarehouseDetails> originalItemsList = new ArrayList<>();

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warehouses);
        hasLoadingView();
        hasSwipeRefresh();

        userDetails = localStorageData.getUserDetails();

        //-----------------

        addWarehouse = findViewById(R.id.addWarehouse);
        addWarehouse.setOnClickListener(this);

        warehousesHolder = findViewById(R.id.warehousesHolder);
        warehousesHolder.setupVerticalOrientation();
        warehousesAdapter = new WarehousesAdapter(activity, new ArrayList<WarehouseDetails>(), new WarehousesAdapter.CallBack() {
            @Override
            public void onItemClick(final int position) {
                selectedItemIndex = position;

                final WarehouseDetails warehouseDetails = warehousesAdapter.getItem(position);

                final String []optionsList = new String[]{"Edit Name", warehouseDetails.getStatus()?"Disable":"Enable"/*, "Delete"*/};
                showListChooserDialog(optionsList, new ListChooserCallback() {
                    @Override
                    public void onSelect(final int oPosition) {
                        /*if ( optionsList[oPosition].equalsIgnoreCase("Edit") ){
                            Intent editWarehouseAct = new Intent(context, AddOrEditWarehouseActivity.class);
                            editWarehouseAct.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                            editWarehouseAct.putExtra(Constants.WAREHOUSE_DETAILS, new Gson().toJson(warehousesAdapter.getItem(position)));
                            startActivityForResult(editWarehouseAct, Constants.ADD_OR_EDIT_WAREHOUSE_ACTIVITY_CODE);
                        }*/
                        if ( optionsList[oPosition].equalsIgnoreCase("Edit Name") ){
                            showInputTextDialog("Edit Name", "Name", warehouseDetails.getName(), new TextInputCallback() {
                                @Override
                                public void onComplete(final String newName) {
                                    if ( newName.equals(warehouseDetails.getName()) == false ){
                                        showLoadingDialog("Updating warehouse name, wait...");
                                        getBackendAPIs().getRoutesAPI().updateWarehouse(warehouseDetails.getID(), new RequestAddOrUpdateWarehouse(newName), new RoutesAPI.WarehouseDetailsCallback() {
                                            @Override
                                            public void onComplete(boolean status, int statusCode, String message, WarehouseDetails warehouse_details) {
                                                closeLoadingDialog();
                                                if ( status ){
                                                    warehouseDetails.setName(newName);
                                                    warehousesAdapter.setItem(position, warehouseDetails);
                                                    showToastMessage("Warehouse name updated successfully");
                                                }else{
                                                    showToastMessage("Something went wrong, try again!");
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                        else if ( optionsList[oPosition].equalsIgnoreCase("Disable") || optionsList[oPosition].equalsIgnoreCase("Enable") ){
                            showLoadingDialog((warehouseDetails.getStatus()?"Disabling":"Enabling")+" warehouse, wait...");
                            getBackendAPIs().getRoutesAPI().updateWarehouse(warehouseDetails.getID(), new RequestAddOrUpdateWarehouse(!warehouseDetails.getStatus()), new RoutesAPI.WarehouseDetailsCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message, WarehouseDetails warehouse_details) {
                                    closeLoadingDialog();
                                    if ( status ){
                                        warehouseDetails.setStatus(!warehouseDetails.getStatus());
                                        warehousesAdapter.setItem(position, warehouseDetails);
                                        showToastMessage("Warehouse "+(warehouseDetails.getStatus()?"enabled":"disabled")+" successfully");
                                    }else{
                                        showToastMessage("Something went wrong, try again!");
                                    }
                                }
                            });
                        }
                        /*else if ( optionsList[oPosition].equalsIgnoreCase("Delete") ){
                            showLoadingDialog("Deleting warehouse, wait...");
                            getBackendAPIs().getRoutesAPI().deleteWarehouse(warehouseDetails.getID(), new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                    closeLoadingDialog();
                                    if ( status ){
                                        warehousesAdapter.deleteItem(position);
                                        showToastMessage("Warehouse deleted successfully");
                                    }else{
                                        showToastMessage("Something went wrong, try again!");
                                    }
                                }
                            });
                        }*/
                    }
                });
            }
        });
        warehousesHolder.setAdapter(warehousesAdapter);

        //-----

        // LOADING DELIVERY AREAS AFTER MENU CREATED

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        getWarehouses();
    }

    @Override
    public void onClick(View view) {
        if ( view == addWarehouse){
            Intent addWarehouseAct = new Intent(context, AddOrEditWarehouseActivity.class);
            addWarehouseAct.putExtra(Constants.ACTION_TYPE, Constants.ADD);
            startActivityForResult(addWarehouseAct, Constants.ADD_OR_EDIT_WAREHOUSE_ACTIVITY_CODE);
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getWarehouses();
    }

    public void getWarehouses(){

        searchViewMenuItem.setVisible(false);
        addWarehouse.setVisibility(View.GONE);
        showLoadingIndicator("Loading warehouses, wait..");
        getBackendAPIs().getRoutesAPI().getWarehouses(new RoutesAPI.WarehousesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<WarehouseDetails> warehousesList) {
                if (status == true) {
                    originalItemsList = warehousesList;

                    addWarehouse.setVisibility(View.VISIBLE);
                    if (warehousesList.size() > 0) {
                        searchViewMenuItem.setVisible(true);
                        switchToContentPage();
                        warehousesAdapter.setItems(warehousesList);
                    }else {
                        showNoDataIndicator("No warehouses");
                    }
                } else {
                    showReloadIndicator(statusCode, "Unable to load warehouses, try again!");
                }
            }
        });
    }

    //--------------

    public void onPerformSearch(final String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchQuery = search_query;
            showLoadingIndicator("Searching for '"+search_query+"', wait...");
            new AsyncTask<Void, Void, Void>() {
                List<WarehouseDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... voids) {
                    for (int i=0;i<originalItemsList.size();i++){
                        if ( originalItemsList.get(i).getName().toLowerCase().contains(searchQuery.toLowerCase()) ){
                            filteredList.add(originalItemsList.get(i));
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( searchQuery.equalsIgnoreCase(search_query) ){
                        if ( filteredList.size() > 0 ){
                            warehousesAdapter.setItems(filteredList);
                            warehousesHolder.smoothScrollToPosition(0);
                            switchToContentPage();
                        }else{
                            showNoDataIndicator("Nothing found for '"+searchQuery+"'");
                        }
                    }
                }
            }.execute();
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        warehousesAdapter.setItems(originalItemsList);
        if ( warehousesAdapter.getItemCount() > 0 ){
            warehousesHolder.smoothScrollToPosition(0);
            switchToContentPage();
        }else{
            showNoDataIndicator("No delivery areas");
        }
    }

    //===============

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchViewMenuItem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.ADD_OR_EDIT_WAREHOUSE_ACTIVITY_CODE && resultCode == RESULT_OK ){
                String actionType = data.getStringExtra(Constants.ACTION_TYPE);
                WarehouseDetails warehouseDetails = new Gson().fromJson(data.getStringExtra(Constants.WAREHOUSE_DETAILS), WarehouseDetails.class);
                if ( actionType.equalsIgnoreCase(Constants.ADD) ){
                    warehousesAdapter.addItem(warehouseDetails);
                    originalItemsList.add(warehouseDetails);
                    switchToContentPage();
                }
                else  if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                    warehousesAdapter.setItem(selectedItemIndex, warehouseDetails);

                    for (int i=0;i<originalItemsList.size();i++){
                        if ( originalItemsList.get(i).getID().equals(warehouseDetails.getID())){
                            originalItemsList.set(i, warehouseDetails);
                            break;
                        }
                    }
                }
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){

        }else {
            onBackPressed();
        }
        return true;
    }


}
