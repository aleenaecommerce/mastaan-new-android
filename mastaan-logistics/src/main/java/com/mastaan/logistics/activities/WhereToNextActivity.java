package com.mastaan.logistics.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.models.Job;

public class WhereToNextActivity extends MastaanToolbarActivity implements View.OnClickListener{

    TextView destination_info;

    Button confirmResume;
    View cancelTrip;

    Job jobDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_where_to_next);

        jobDetails = new Gson().fromJson(getIntent().getStringExtra("jobDetails"), Job.class);

        actionBar.setDisplayHomeAsUpEnabled(false);

        //----------

        destination_info = (TextView) findViewById(R.id.destination_info);

        confirmResume = (Button) findViewById(R.id.confirmResume);
        confirmResume.setOnClickListener(this);
        cancelTrip =  findViewById(R.id.cancelTrip);
        cancelTrip.setOnClickListener(this);

        //--------

        if ( jobDetails != null ) {
            destination_info.setText(Html.fromHtml("From: <b>" + jobDetails.getStartPoint() + "</b><br><br>To: <b>" + jobDetails.getEndPoint() + "</b>"));

            // HYPERTRACK STARTING TRACKING
            checkLocationAccess(new CheckLocatoinAccessCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message) {
                    if ( status ) {
                        //HyperTrack.startTracking();
                    }else{
                        checkLocationAccess(this);
                    }
                }
            });
        }

    }

    @Override
    public void onClick(View view) {

        if ( view == confirmResume ){
            getCurrentLocation(new LocationCallback() {
                @Override
                public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng currentLocationLatLng) {
                    if ( status ){
                        if ( LatLngMethods.getDistanceBetweenLatLng(jobDetails.getEndPointLatLng(), currentLocationLatLng) <= 300 ){
                            resume();
                        }else{
                            showNotificationDialog("Alert", Html.fromHtml("You can only complete the trip at <b>"+jobDetails.getEndPoint()+"</b>. Please try again after reaching there."));
                        }
                    }else{
                        showToastMessage(message);
                    }
                }
            });
        }
        else if ( view ==  cancelTrip){
            showChoiceSelectionDialog("Confirm!", "Do you want to cancel your ongoing trip?", "NO", "YES", new ChoiceSelectionCallback() {
                @Override
                public void onSelect(int choiceNo, String choiceName) {
                    if (choiceName.equalsIgnoreCase("YES")) {
                        cancelTrip();
                    }
                }
            });
        }
    }

    //--------

    public void resume(){
        getCurrentLocation(new LocationCallback() {
            @Override
            public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                if (status) {
                    showLoadingDialog("Resuming, wait...");
                    getBackendAPIs().getWhereToNextAPI().resume(latLng.latitude, latLng.longitude, new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            closeLoadingDialog();
                            if (status) {
                                //HyperTrack.stopTracking();  // Stopping HyperTrack tracking

                                Intent intent = new Intent(context, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                showNotificationDialog("Failure", message);
                            }
                        }
                    });
                } else {
                    showToastMessage(message);
                }
            }
        });
    }

    public void cancelTrip(){
        getCurrentLocation(new LocationCallback() {
            @Override
            public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                if (status) {
                    showLoadingDialog("Cancelling trip, wait...");
                    getBackendAPIs().getWhereToNextAPI().cancelTrip(latLng.latitude, latLng.longitude, new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            closeLoadingDialog();
                            if (status) {
                                //HyperTrack.stopTracking();  // Stopping HyperTrack tracking

                                Intent intent = new Intent(context, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                showNotificationDialog("Failure", message);
                            }
                        }
                    });
                } else {
                    showToastMessage(message);
                }
            }
        });
    }

    //======

    @Override
    public void onBackPressed() {}
}
