package com.mastaan.logistics.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.BuildConfig;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.services.LogisticsService;


public class SettingsActivity extends MastaanToolbarActivity implements View.OnClickListener{

    View billPrinterView;
    TextView bill_printer_id_address;
    View changeBillPrinterIPAddress;

    View labelPrinterView;
    TextView label_printer_id_address;
    View changeLabelPrinterIPAddress;

    View logout;
    TextView logout_name;

    TextView about;

    UserDetails userDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        userDetails = localStorageData.getUserDetails();

        //-----------------

        billPrinterView = findViewById(R.id.billPrinterView);
        bill_printer_id_address = findViewById(R.id.bill_printer_id_address);
        changeBillPrinterIPAddress = findViewById(R.id.changeBillPrinterIPAddress);
        changeBillPrinterIPAddress.setOnClickListener(this);

        labelPrinterView = findViewById(R.id.labelPrinterView);
        label_printer_id_address = findViewById(R.id.label_printer_id_address);
        changeLabelPrinterIPAddress = findViewById(R.id.changeLabelPrinterIPAddress);
        changeLabelPrinterIPAddress.setOnClickListener(this);

        logout = findViewById(R.id.logout);
        logout.setOnClickListener(this);
        logout_name = findViewById(R.id.logout_name);

        about = findViewById(R.id.about);

        //---------

        if ( userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() || userDetails.isCustomerRelationshipManager() ){
            billPrinterView.setVisibility(View.VISIBLE);
            labelPrinterView.setVisibility(View.VISIBLE);
            bill_printer_id_address.setText(localStorageData.getBillPrinterIPAddress());
            label_printer_id_address.setText(localStorageData.getLabelPrinterIPAddress());
        }

        logout_name.setText(Html.fromHtml("Logout (<b>" + CommonMethods.capitalizeStringWords(userDetails.getFullName()) + "</b>)"));

        about.setText(Html.fromHtml(getString(R.string.app_name)+"<br><b>(v"+ BuildConfig.VERSION_NAME+"</b>)"));

    }

    @Override
    public void onClick(View view) {
        if ( view == changeBillPrinterIPAddress ){
            showInputTextDialog("Bill Printer IP Address", "IP Address", localStorageData.getBillPrinterIPAddress(), new TextInputCallback() {
                @Override
                public void onComplete(String inputText) {
                    localStorageData.setBillPrinterIPAddress(inputText);
                    bill_printer_id_address.setText(inputText);
                    showToastMessage("Bill Printer IP Address changed successfully");
                }
            });
        }

        else if ( view == changeLabelPrinterIPAddress ){
            showInputTextDialog("Label Printer IP Address", "IP Address", localStorageData.getLabelPrinterIPAddress(), new TextInputCallback() {
                @Override
                public void onComplete(String inputText) {
                    localStorageData.setLabelPrinterIPAddress(inputText);
                    label_printer_id_address.setText(inputText);
                    showToastMessage("Label Printer IP Address changed successfully");
                }
            });
        }

        else if ( view == logout ){
            logOut();
        }
    }

    public void logOut() {
        showLoadingDialog("Logging out, wait...");
        getBackendAPIs().getAuthenticationAPI().logout(new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();

                Intent fireBaseReceiver = new Intent(Constants.FIREBASE_RECEIVER)
                        .putExtra("type", Constants.CLEAR_CS_NOTIFICATIONS_CACHE);
                context.sendBroadcast(fireBaseReceiver);
                context.stopService(new Intent(context, LogisticsService.class));      //  Starting FIREBASE SERVICE FOR PENDING ORDERS UPDATES

                localStorageData.clearSession();

                Intent launchAct = new Intent(context, LaunchActivity.class);
                ComponentName componentName = launchAct.getComponent();
                Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                startActivity(mainIntent);
            }
        });
    }

    //================

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
