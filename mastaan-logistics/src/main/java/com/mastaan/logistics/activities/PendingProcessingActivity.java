package com.mastaan.logistics.activities;

import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import android.util.Log;
import android.view.View;

import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

public class PendingProcessingActivity extends BaseOrderItemsActivity {

    String deliveryAreaID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();
        //hasNavigationButton("Go to Pending Quality Check");

        if ( getIntent().getStringExtra(Constants.DELIVERY_AREA_ID) != null ){
            deliveryAreaID = getIntent().getStringExtra(Constants.DELIVERY_AREA_ID);
        }

        startAutoRefreshTimer(10 * 60);         // 10 Minutes Interval

        //--------

        if ( deliveryAreaID != null && deliveryAreaID.length() > 0 ){
            getPendingProcessingItems(false);
        }else{
            getPendingProcessingItemsFromDatabase();
        }
    }

    @Override
    public void onNavigationButtonClick() {
        super.onNavigationButtonClick();
        /*Intent pendingQCAct = new Intent(context, PendingQCActivity.class);
        startActivity(pendingQCAct);
        finish();*/
    }

    @Override
    public void onAutoRefreshTimeElapsed() {
        super.onAutoRefreshTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onInactiveTimeElapsed() {
        super.onInactiveTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onReloadPage() {
        super.onReloadPage();
        getPendingProcessingItems(false);
    }

    @Override
    public void onReloadFromDatabase() {
        super.onReloadFromDatabase();
        getPendingProcessingItemsFromDatabase();
    }

    @Override
    public void onBackgroundReloadPressed() {
        super.onBackgroundReloadPressed();
        getPendingProcessingItems(true);
    }


    public void getPendingProcessingItemsFromDatabase(){

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        hideNavigationButton();
        showLoadingIndicator("Loading pending processing items from database, wait...");
        getOrderItemsDatabase().getPendingProcessingItems(new OrderItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<OrderItemDetails> itemsList) {
                if (itemsList != null && itemsList.size() > 0) {
                    showLoadingIndicator("Displaying items, wait....");
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), itemsList, localStorageData.getWarehouseLocation(), new GroupingMethods.GroupedOrderItemsListCallback() {
                        @Override
                        public void onComplete(List<GroupedOrdersItemsList> groupedOrdersItemsLists) {
                            displayItems(false, groupedOrdersItemsLists, new ArrayList<OrderDetails>());
                            switchToContentPage();

                            getPendingProcessingItems(true);
                        }
                    });

                } else {
                    getPendingProcessingItems(false);
                }
            }
        });
    }

    private void getPendingProcessingItems(final boolean loadInBackground) {

        if ( loadInBackground ){
            loadingIndicator.setVisibility(View.VISIBLE);
            reloadIndicator.setVisibility(View.GONE);
        }else {
            showLoadingIndicator("Loading pending process items, wait...");
        }
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        hideNavigationButton();
        OrdersAPI.OrdersAndGroupedOrderItemsCallback callback = new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
                loadingIndicator.setVisibility(View.GONE);
                if (status) {
                    if (loadInBackground) {
                        updateDataAndDisplayItems(groupedOrderItemsLists, ordersList);
                        Log.e("pp list",""+groupedOrderItemsLists);
                        Log.e("pp size",""+groupedOrderItemsLists.size());
                    } else {
                        displayItems(true, groupedOrderItemsLists, ordersList);
                    }
                } else {
                    if (loadInBackground) {
                        reloadIndicator.setVisibility(View.VISIBLE);
                        loadingIndicator.setVisibility(View.GONE);
                    } else {
                        showReloadIndicator("Unable to load pending process items, try again!");
                    }
                }
            }
        };
        if ( deliveryAreaID != null && deliveryAreaID.length() > 0 ){
            getBackendAPIs().getOrdersAPI().getPendingProcessingGroupItems(deliveryAreaID, localStorageData.getWarehouseLocation(), callback);
        }
        else{
            getBackendAPIs().getOrdersAPI().getPendingProcessingItems(localStorageData.getWarehouseLocation(), callback);
        }

    }

}
