package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vAppBarLayout;
import com.aleena.common.widgets.vViewPager;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.MeatItemsPagerAdapter;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.fragments.MeatItemsFragment;
import com.mastaan.logistics.models.GroupedMeatItemsList;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 29/4/16.
 */

public class MeatItemsActivity extends MastaanToolbarActivity {

    vAppBarLayout appBarLayout;

    MenuItem searchViewMenuITem;
    SearchView searchView;

    TabLayout tabLayout;
    vViewPager viewPager;
    MeatItemsPagerAdapter adapter;

    View updateAvailability;

    List<GroupedMeatItemsList> items;

    UserDetails userDetails;
    String loadedTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meat_items);
        hasLoadingView();

        userDetails = localStorageData.getUserDetails();

        if ( getIntent().getStringExtra(Constants.TITLE) != null ){
            setToolbarTitle(getIntent().getStringExtra(Constants.TITLE));
        }

        //--------

        appBarLayout = (vAppBarLayout) findViewById(R.id.appBarLayout);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabTextColors(getResources().getColor(R.color.normal_tab_text_color), getResources().getColor(R.color.selected_tab_text_color));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                collapseSearchView();
                onPerformSearch(getSearchViewText());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });

        viewPager = (vViewPager) findViewById(R.id.viewPager);
        //viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    tabLayout.getTabAt(position).select();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));  // To Sync ViewPager with Tabs.

        updateAvailability = findViewById(R.id.updateAvailability);
        updateAvailability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //if ( activity.localStorageData.isUserCheckedIn() ) {
                    if ( userDetails.isStockManager() || userDetails.isProcessingManager() || userDetails.isWarehouseManager() ) {
                        try {
                            if (DateMethods.getDifferenceBetweenDatesInMilliSeconds(loadedTime, DateMethods.getCurrentDateAndTime()) > 60 * 1000) { // >1min
                                showToastMessage("Reloading latest data, try again after it.");
                                onReloadPressed();
                                return;
                            }
                            ((MeatItemsFragment) adapter.getCurrentFragment(viewPager)).updateItemsAvailability();
                        } catch (Exception e) {}
                    }else{
                        showToastMessage("You are not allowed to perform this operation, please contact stock/processing/warehouse manager");
                    }
                //}else{
                //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                //}
            }
        });

        //------------

        getMeatItems();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getMeatItems();
    }

    //---------------

    private void getMeatItems() {

        updateAvailability.setVisibility(View.GONE);
        hideSearchView();
        showLoadingIndicator("Loading, wait...");
        boolean forStocks = false;String hubID = null;
        if ( getIntent().getBooleanExtra(Constants.FOR_STOCK, false) ){
            forStocks = true;
            if ( getIntent().getStringExtra(Constants.HUB_DETAILS) != null ) {
                hubID = new Gson().fromJson(getIntent().getStringExtra(Constants.HUB_DETAILS), HubDetails.class).getID();
            }
        }
        getBackendAPIs().getMeatItemsAPI().getGroupedMeatItems(getIntent().getStringExtra(Constants.AVAILABILITY), forStocks, hubID, new MeatItemsAPI.GroupedWarehouseMeatItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<GroupedMeatItemsList> groupedMeatItemsList) {
                if (status) {
                    displayItems(groupedMeatItemsList);
                } else {
                    showReloadIndicator("Unable to load meat items, try again!");
                }
            }
        });
    }

    public void displayItems(List<GroupedMeatItemsList> items) {
        this.items = items;
        this.loadedTime = DateMethods.getCurrentDateAndTime();

        try {   // To Handle Crash issue for displaying data after activity destroyed
            if (items.size() > 0) {

                switchToContentPage();
                showSearchView();
                if ( userDetails.isProcessingManager() || userDetails.isStockManager() ) {
                    updateAvailability.setVisibility(View.VISIBLE);
                }

                tabLayout.removeAllTabs();
                for (int i = 0; i < items.size(); i++) {
                    TabLayout.Tab tab = tabLayout.newTab();
                    View rowView = LayoutInflater.from(context).inflate(R.layout.view_tab_list_item, null, false);
                    TextView tabTitle = (TextView) rowView.findViewById(R.id.tabTitle);
                    tabTitle.setText(items.get(i).getCategoryName().toUpperCase());
                    tab.setCustomView(rowView);
                    tabLayout.addTab(tab);
                }
                if (items.size() == 1) {
                    tabLayout.setVisibility(View.GONE);
                } else {
                    tabLayout.setVisibility(View.VISIBLE);
                }

                adapter = new MeatItemsPagerAdapter(getSupportFragmentManager());
                if ( getIntent().getStringExtra(Constants.HUB_DETAILS) != null && getIntent().getStringExtra(Constants.HUB_DETAILS).trim().length() > 0 ){
                    adapter.setHubDetails(getIntent().getStringExtra(Constants.HUB_DETAILS));
                }
                if ( getIntent().getBooleanExtra(Constants.FOR_STOCK, false) ){
                    adapter.setForStock();
                }
                if ( getIntent().getBooleanExtra(Constants.SHOW_ITEM_LEVEL_ATTRIBUTES_OPTIONS_SELECTION, false) ){
                    adapter.showItemLevelAttributesOptionsSelection();
                }
                adapter.setItems(items);
                viewPager.setAdapter(adapter);
            }
            else {
                tabLayout.setVisibility(View.GONE);
                showNoDataIndicator("No items");
                hideSearchView();
            }
        }catch (Exception e){   e.printStackTrace();    }

    }

    public List<WarehouseMeatItemDetails> getCategoryMeatItems(String categoryName) {

        if ( items != null && items.size() > 0 ){
            for (int i=0;i<items.size();i++){
                if ( items.get(i).getCategoryName().equalsIgnoreCase(categoryName) ){
                    return items.get(i).getItems();
                }
            }
        }
        return new ArrayList<>();
    }

    public List<String> getCategorySubCategories(String categoryName) {

        if ( items != null && items.size() > 0 ){
            for (int i=0;i<items.size();i++){
                if ( items.get(i).getCategoryName().equalsIgnoreCase(categoryName) ){
                    return items.get(i).getSubCategoriesNames();
                }
            }
        }
        return new ArrayList<>();
    }

    //----------------

    public void onPerformSearch(String searchQuery){
        try {
            if (isShowingContentPage()) {
                if ( appBarLayout != null ) {
                    appBarLayout.expandToolbar();   // To Show Tablayout if its hided already
                }
                MeatItemsFragment meatItemsFragment = (MeatItemsFragment) adapter.getCurrentFragment(viewPager);//viewPager.getShowingFragment(getSupportFragmentManager());
                meatItemsFragment.performSearch(searchQuery);
            }
        }catch (Exception e){ e.printStackTrace();  }
    }

    public void onClearSearch(){
        try {
            if (isShowingContentPage()) {
                MeatItemsFragment meatItemsFragment = (MeatItemsFragment) adapter.getCurrentFragment(viewPager);//viewPager.getShowingFragment(getSupportFragmentManager());
                meatItemsFragment.clearSearch();
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    public void collapseSearchView(){
        try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
    }

    public void showSearchView(){
        try{    searchViewMenuITem.setVisible(true);    }catch (Exception e){}
    }

    public void hideSearchView(){
        try{
            searchViewMenuITem.setVisible(false);
            collapseSearchView();
        }catch (Exception e){}
    }

    public String getSearchViewText(){
        String searchViewText = "";
        try{ searchViewText = searchView.getQuery().toString();      }catch (Exception e){}
        return searchViewText;
    }


    //=================

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}