package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.AnalyticsAdapter;
import com.mastaan.logistics.backend.AnalyticsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.DatePeriodSelectorDialog;
import com.mastaan.logistics.models.AnalyticsCouponCode;
import com.mastaan.logistics.models.AnalyticsDetails;
import com.mastaan.logistics.models.BuyerSearchDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 05/06/18.
 */

public class ReportCouponCodesActivity extends MastaanToolbarActivity implements View.OnClickListener{

    MenuItem searchViewMenuITem;
    SearchView searchView;
    String searchQuery;

    TextView from_date;
    TextView to_date;
    View changePeriod;

    vRecyclerView itemsHolder;
    AnalyticsAdapter analyticsAdapter;

    View showSummary;

    List<AnalyticsDetails> originalAnalyticsList = new ArrayList<>();
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_coupon_codes);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        from_date = findViewById(R.id.from_date);
        to_date = findViewById(R.id.to_date);
        changePeriod = findViewById(R.id.changePeriod);
        changePeriod.setOnClickListener(this);

        showSummary = findViewById(R.id.showSummary);
        showSummary.setOnClickListener(this);

        itemsHolder = findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        analyticsAdapter = new AnalyticsAdapter(activity, new ArrayList<AnalyticsDetails>(), new AnalyticsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                AnalyticsCouponCode analyticsCouponCode = analyticsAdapter.getItem(position).getAnalyticsCouponCode();
                String fromDate = from_date.getText().toString();
                String toDate = to_date.getText().toString();

                Intent buyersAct = new Intent(context, BuyersActivity.class);
                buyersAct.putExtra(Constants.ACTION_TYPE, Constants.COUPON_BUYERS);
                buyersAct.putExtra(Constants.TITLE, analyticsCouponCode.getCouponCode()+" Code Buyers");
                buyersAct.putExtra(Constants.SEARCH_DETAILS, new Gson().toJson(new BuyerSearchDetails()
                        .setCouponCode(analyticsCouponCode.getCouponCode())
                        .setFromDate((fromDate!=null&&fromDate.length()>0&&fromDate.equalsIgnoreCase("Start")==false?DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY):null))
                        .setToDate((toDate!=null&&toDate.length()>0&&toDate.equalsIgnoreCase("End")==false?DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY):null))
                ));
                startActivity(buyersAct);
            }

            @Override
            public void onActionClick(int position) {

            }
        });
        itemsHolder.setAdapter(analyticsAdapter);

        //----

        getData(null, null);

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getData(from_date.getText().toString(), to_date.getText().toString());
    }

    @Override
    public void onClick(View view) {
        if ( view == changePeriod ){
            String prefillFromDate = from_date.getText().toString();
            String prefillToDate = to_date.getText().toString();
            if ( prefillFromDate.equalsIgnoreCase("Start")&& prefillToDate.equalsIgnoreCase("End") ){
                prefillFromDate = DateMethods.getCurrentDate();
            }
            new DatePeriodSelectorDialog(activity).show(prefillFromDate, prefillToDate, new DatePeriodSelectorDialog.Callback() {
                @Override
                public void onComplete(String fromDate, String toDate) {
                    from_date.setText(fromDate != null&&fromDate.length()>0?DateMethods.getDateInFormat(fromDate, DateConstants.MMM_DD_YYYY):"Start");
                    to_date.setText(toDate != null&&toDate.length()>0?DateMethods.getDateInFormat(toDate, DateConstants.MMM_DD_YYYY):"End");
                    getData(fromDate, toDate);
                }
            });
        }
        else if ( view == showSummary ){
            onShowSummary();
        }
    }

    public void getData(String fromDate, String toDate){

        changePeriod.setClickable(false);
        showSummary.setVisibility(View.GONE);
        showLoadingIndicator("Loading, wait...");
        getBackendAPIs().getAnalyticsAPI().getCouponCodesAnalytics((fromDate!=null&&fromDate.length()>0&&fromDate.equalsIgnoreCase("Start")==false?DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY):null), (toDate!=null&&toDate.length()>0&&toDate.equalsIgnoreCase("End")==false?DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY):null), new AnalyticsAPI.AnalyticsCouponCodesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<AnalyticsCouponCode> analyticsCouponCodes) {
                changePeriod.setClickable(true);
                if ( status ){
                    if ( analyticsCouponCodes.size() > 0 ){
                        List<AnalyticsDetails> analyticsList = new ArrayList<>();
                        for (int i=0;i<analyticsCouponCodes.size();i++){
                            analyticsList.add(new AnalyticsDetails().setAnalyticsCouponCode(analyticsCouponCodes.get(i)));
                        }
                        analyticsAdapter.setItems(analyticsList);
                        originalAnalyticsList = analyticsList;
                        showSummary.setVisibility(View.VISIBLE);
                        switchToContentPage();
                    }
                    else{
                        showNoDataIndicator("Nothing to show");
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    //----------

    public void onPerformSearch(final String search_query){
        if ( search_query != null && search_query.trim().length() > 0 ){
            this.searchQuery = search_query;

            showLoadingIndicator("Searching for ("+search_query+")...");
            new AsyncTask<Void, Void, Void>() {
                List<AnalyticsDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... voids) {
                    try{
                        for (int i=0;i<originalAnalyticsList.size();i++){
                            AnalyticsCouponCode analyticsCouponCode = originalAnalyticsList.get(i).getAnalyticsCouponCode();
                            if ( analyticsCouponCode.getFormattedTitle().toLowerCase().contains(search_query.toLowerCase()) ){
                                filteredList.add(originalAnalyticsList.get(i));
                            }
                        }
                    }catch (Exception e){}
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( searchQuery.equalsIgnoreCase(search_query) ){
                        if ( filteredList.size() > 0 ) {
                            analyticsAdapter.setItems(filteredList);
                            switchToContentPage();
                            showToastMessage(filteredList.size()+" result(s) found");
                        }else{
                            showNoDataIndicator("No results found\n(" + search_query + ")");
                        }
                    }
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        analyticsAdapter.setItems(originalAnalyticsList);
        if ( analyticsAdapter.getItemCount() > 0 ){
            switchToContentPage();
        }else{
            showNoDataIndicator("Nothing to show");
        }
    }

    public void onShowSummary(){
        showLoadingDialog("Analysing, wait...");
        new AsyncTask<Void, Void, Void>() {
            List<InfoDetails> infosList = new ArrayList<>();

            class InfoDetails {
                String name;
                long count;
                double total;
                double discount;
                InfoDetails(String name){
                    this.name = name;
                }
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    List<AnalyticsDetails> itemsList = analyticsAdapter.getAllItems();
                    for (int i=0;i<itemsList.size();i++){
                        AnalyticsCouponCode analyticsCouponCode = itemsList.get(i).getAnalyticsCouponCode();
                        String offerName = analyticsCouponCode.getCouponDetails().getOfferName();
                        if ( offerName == null ){   offerName = "unknown";  }

                        int containIndex = -1;
                        for (int j=0;j<infosList.size();j++){
                            if ( infosList.get(j).name.equalsIgnoreCase(offerName) ){
                                containIndex = j;
                                break;
                            }
                        }
                        if ( containIndex == -1 ){
                            infosList.add(new InfoDetails(offerName));
                            containIndex = infosList.size()-1;
                        }

                        infosList.get(containIndex).count += analyticsCouponCode.getCount();
                        infosList.get(containIndex).total += analyticsCouponCode.getTotal();
                        infosList.get(containIndex).discount += analyticsCouponCode.getDiscount();
                    }

                    Collections.sort(infosList, new Comparator<InfoDetails>() {
                        public int compare(InfoDetails item1, InfoDetails item2) {
                            if (item1.count > item2.count) {
                                return -1;
                            } else if (item1.count < item2.count) {
                                return 1;
                            }
                            return 0;
                        }
                    });
                }catch (Exception e){}
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                closeLoadingDialog();
                String infosString = "";
                for (int i=0;i<infosList.size();i++){
                    InfoDetails infoDetails = infosList.get(i);
                    infosString += (infosString.length()>0?"<br><br>":"") + ("<b>"+infoDetails.name+"</b>"
                            + (infoDetails.count > 0?"<br>Orders Count: <b>"+ CommonMethods.getIndianFormatNumber(infoDetails.count)+"</b>":"")
                            + (infoDetails.total > 0?"<br>Orders Total: <b>"+ CommonMethods.getIndianFormatNumber(infoDetails.total)+"</b>":"")
                            + (infoDetails.discount > 0?"<br>Orders Discount: <b>"+ CommonMethods.getIndianFormatNumber(infoDetails.discount)+"</b>":"")
                    );
                }

                showNotificationDialog("Summary by Offer Name", CommonMethods.fromHtml(infosString));
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }



    //===========

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        searchViewMenuITem.setVisible(true);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);//new SearchDetails().setConsiderAtleastOneMatch(query));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){
        }else {
            onBackPressed();
        }
        return true;
    }

}