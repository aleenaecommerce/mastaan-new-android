package com.mastaan.logistics.activities;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.OrdersAdapter;
import com.mastaan.logistics.backend.BuyersAPI;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AnalyseOrderItemsDialog;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.ECODDialog;
import com.mastaan.logistics.dialogs.FollowupDetailsDialog;
import com.mastaan.logistics.dialogs.MoneyCollectionDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.methods.AnalyseOrderitemsMethods;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;

import java.util.ArrayList;
import java.util.List;

public class BuyerOrdersActivity extends MastaanToolbarActivity {

    String buyerID;
    String buyerName;

    View detailsView;
    TextView summary_info;
    View showFullAnalysis;

    vRecyclerView ordersHolder;
    OrdersAdapter ordersAdapter;

    List<GroupedOrdersItemsList> groupedOrdersItemsLists = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_orders);
        hasLoadingView();
        hasSwipeRefresh();

        buyerID = getIntent().getStringExtra(Constants.BUYER_ID);
        buyerName = getIntent().getStringExtra(Constants.BUYER_NAME);

        setToolbarTitle(buyerName+" orders");

        //---------

        detailsView = findViewById(R.id.detailsView);
        summary_info = (TextView) findViewById(R.id.summary_info);
        showFullAnalysis = findViewById(R.id.showFullAnalysis);
        showFullAnalysis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnalyseOrderItemsDialog(activity, groupedOrdersItemsLists).show();
            }
        });

        ordersHolder = findViewById(R.id.ordersHolder);
        ordersHolder.setupVerticalOrientation();
        ordersAdapter = new OrdersAdapter(activity, new ArrayList<OrderDetails>(), new OrdersAdapter.Callback() {
            @Override
            public void onItemClick(int position) {

            }

            @Override
            public void onShowDirections(final int position) {
                getCurrentLocation(new LocationCallback() {
                    @Override
                    public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                        if (status) {
                            Intent showDirectionsIntent = new Intent(context, DirectionsViewerHereMapsDialogActivity.class);
                            showDirectionsIntent.setAction(Constants.POPUP_ACTION);
                            showDirectionsIntent.putExtra("origin", latLng);
                            showDirectionsIntent.putExtra("destination", ordersAdapter.getItem(position).getDeliveryAddressLatLng());
                            showDirectionsIntent.putExtra("location", ordersAdapter.getItem(position).getCustomerName() + "\n" + ordersAdapter.getItem(position).getCustomerName()/*.getAddress().getDeliveryAddressLocality()*/);
                            startActivity(showDirectionsIntent);
                        } else {
                            showToastMessage(message);
                        }
                    }
                });
            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowBuyerHistory(int position) {
            }

            @Override
            public void onShowReferralBuyerDetails(int position) {
                new BuyerOptionsDialog(activity).show(ordersAdapter.getItem(position).getReferralBuyer());
            }

            @Override
            public void onShowMoneyCollectionDetails(int position) {
                new MoneyCollectionDetailsDialog(activity).showForOrder(ordersAdapter.getItem(position).getID());
            }

            @Override
            public void onCheckPaymentStatus(final int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                showLoadingDialog("Checking payment status, wait...");
                getBackendAPIs().getPaymentAPI().checkOrderPaymentStatus(orderDetails.getID(), new PaymentAPI.CheckOrderPaymentStatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, OrderDetails order_details) {
                        closeLoadingDialog();
                        if (status){
                            orderDetails.setTotalAmount(order_details.getTotalAmount());
                            orderDetails.setTotalCashAmount(order_details.getTotalCashAmount());
                            orderDetails.setTotalOnlinePaymentAmount(order_details.getTotalOnlinePaymentAmount());
                            ordersAdapter.updateItem(position, orderDetails);
                        }else{
                            showToastMessage("Something went wrong while checking payment status.\nPlease try again!");
                        }
                    }
                });
            }

            @Override
            public void onRequestOnlinePayment(final int position) {
                new ECODDialog(activity).showForOrder(ordersAdapter.getItem(position), ordersAdapter.getItem(position).getTotalCashAmount(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if ( status ){
                            activity.showToastMessage("Payment success");
                            onCheckPaymentStatus(position);
                        }else{
                            activity.showToastMessage("Payment failed");
                        }
                    }
                });
            }

            @Override
            public void onShowFollowupResultedInOrder(int position) {
                new FollowupDetailsDialog(activity).show(ordersAdapter.getItem(position).getFollowupResultedInOrder());
            }

            @Override
            public void onPrintBill(int position) {

            }

            @Override
            public void onOrderItemClick(int position, int item_position) {

            }

            @Override
            public void onShowOrderItemStockDetails(int position, int item_position) {
                new StockDetailsDialog(activity).show(ordersAdapter.getItem(position).getOrderedItems().get(item_position));
            }
        }).showCheckPaymentStatus().showRequestOnlinePayment();
        ordersHolder.setAdapter(ordersAdapter);

        //---------

        getBuyerOrders();
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getBuyerOrders();
    }

    public void getBuyerOrders(){

        detailsView.setVisibility(View.GONE);
        showLoadingIndicator("Loading buyer orders, wait...");
        getBackendAPIs().getBuyersAPI().getBuyerOrders(buyerID, new BuyersAPI.OrdersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList) {
                if ( status ){
                    displayItems(ordersList);
                }else{
                    showReloadIndicator("Unable to load buyer orders, try again!");
                }
            }
        });
    }

    public void displayItems(final List<OrderDetails> ordersList){

        if ( ordersList.size() > 0 ) {
            showLoadingIndicator("Displaying, wait...");
            new AsyncTask<Void, Void, Void>() {
                String shortSummaryInfo = "";
                @Override
                protected Void doInBackground(Void... voids) {
                    groupedOrdersItemsLists = GroupingMethods.groupAndSortOrderItemsByCategory(GroupingMethods.getOrderItemsFromOrders(ordersList));

                    AnalyseOrderitemsMethods analyseOrderitemsMethods = new AnalyseOrderitemsMethods(CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT}), groupedOrdersItemsLists);
                    analyseOrderitemsMethods.analyse();
                    shortSummaryInfo = analyseOrderitemsMethods.getShortAmountInfo();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    detailsView.setVisibility(View.VISIBLE);
                    summary_info.setText(Html.fromHtml(shortSummaryInfo));
                    ordersAdapter.setItems(ordersList);
                    switchToContentPage();
                }
            }.execute();
        }else{
            showNoDataIndicator("No orders");
        }
    }


    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}