package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.DeliveryBoysAdapter;
import com.mastaan.logistics.backend.models.RequestCreateEmployeeReferralCouponCode;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.flows.ViolationsFlow;
import com.mastaan.logistics.interfaces.UsersCallback;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class DeliveryBoysActivity extends MastaanToolbarActivity {

    String searchQuery;
    MenuItem searchViewMenuITem;
    SearchView searchView;

    vRecyclerView deliveryBoysHolder;
    DeliveryBoysAdapter deliveryBoysAdapter;

    List<UserDetails> originalDeliveryBoysList = new ArrayList<>();

    UserDetails userDetails;

    public interface SearchResultsCallback{
        void onComplete(List<UserDetails> searchList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_boys);
        hasLoadingView();
        hasSwipeRefresh();

        userDetails = localStorageData.getUserDetails();

        //-----------------

        deliveryBoysHolder = (vRecyclerView) findViewById(R.id.deliveryBoysHolder);
        setupHolder();

        //------------------

        getDeliveryBoysList();        //

    }

    public void setupHolder(){

        deliveryBoysHolder.setHasFixedSize(true);
        deliveryBoysHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        deliveryBoysHolder.setItemAnimator(new DefaultItemAnimator());

        deliveryBoysAdapter = new DeliveryBoysAdapter(activity, new ArrayList<UserDetails>(), new DeliveryBoysAdapter.CallBack() {
            @Override
            public void onItemClick(final int position) {
                final String []optionsList = new String[]{"Deliveries", "Money collection", "Pending consolidation", "Bonuses"
                        ,(userDetails.isDeliveryBoyManager()||userDetails.isWarehouseManager())?"Add violation":null
                        //, userDetails.isSuperUser()?"Create Referral Coupon Code":null
                };

                showListChooserDialog(null, optionsList, new ListChooserCallback() {
                    @Override
                    public void onSelect(int slectedChoice) {
                        if ( optionsList[slectedChoice].equalsIgnoreCase("Deliveries") ){
                            Intent deliveryBoyItemsAct = new Intent(context, DeliveryBoyDeliveriesActivity.class);
                            deliveryBoyItemsAct.putExtra(Constants.DELIVERY_BOY_ID, deliveryBoysAdapter.getItem(position).getID());
                            deliveryBoyItemsAct.putExtra(Constants.DELIVERY_BOY_NAME, deliveryBoysAdapter.getItem(position).getFirstName());
                            startActivity(deliveryBoyItemsAct);
                        }
                        else if ( optionsList[slectedChoice].equalsIgnoreCase("Money collection") ){
                            Intent moneyCollectionAct = new Intent(context, MoneyCollectionActivity.class);
                            moneyCollectionAct.putExtra(Constants.ACTION_TYPE, Constants.MONEY_COLLECTION_OF_DELIVERY_BOY_FOR_TODAY);
                            moneyCollectionAct.putExtra(Constants.DELIVERY_BOY_ID, deliveryBoysAdapter.getItem(position).getID());
                            moneyCollectionAct.putExtra(Constants.DELIVERY_BOY_NAME, deliveryBoysAdapter.getItem(position).getAvailableName());
                            startActivity(moneyCollectionAct);
                        }
                        else if ( optionsList[slectedChoice].equalsIgnoreCase("Pending consolidation") ){
                            Intent moneyCollectionAct = new Intent(context, MoneyCollectionActivity.class);
                            moneyCollectionAct.putExtra(Constants.ACTION_TYPE, Constants.PENDING_CONSOLIDATION_OF_DELIVERY_BOY);
                            moneyCollectionAct.putExtra(Constants.DELIVERY_BOY_ID, deliveryBoysAdapter.getItem(position).getID());
                            moneyCollectionAct.putExtra(Constants.DELIVERY_BOY_NAME, deliveryBoysAdapter.getItem(position).getAvailableName());
                            startActivity(moneyCollectionAct);
                        }
                        else if ( optionsList[slectedChoice].equalsIgnoreCase("Bonuses") ){
                            Intent employeeBonusesAct = new Intent(context, EmployeeBonusesActivity.class);
                            employeeBonusesAct.putExtra(Constants.EMPLOYEE_ID, deliveryBoysAdapter.getItem(position).getID());
                            employeeBonusesAct.putExtra(Constants.TITLE, deliveryBoysAdapter.getItem(position).getAvailableName()+" Bonuses");
                            startActivity(employeeBonusesAct);
                        }
                        else if ( optionsList[slectedChoice].equalsIgnoreCase("Add violation") ){
                            new ViolationsFlow(activity).addViolation(deliveryBoysAdapter.getItem(position), new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String violationDetails) {
                                    if ( status ){
                                        showNotificationDialog("Success", Html.fromHtml("Violation <b>"+violationDetails+"</b> added to <b>"+deliveryBoysAdapter.getItem(position).getAvailableName()+"</b> successfully."));
                                    }
                                }
                            });
                        }
//                        else if ( optionsList[slectedChoice].equalsIgnoreCase("Create Referral Coupon Code") ){
//                            showInputTextDialog("Create Referral Coupon Code", "Coupon Code", new TextInputCallback() {
//                                @Override
//                                public void onComplete(String inputText) {
//                                    createEmployeeReferralCoupon(deliveryBoysAdapter.getItem(position).getID(), new RequestCreateEmployeeReferralCouponCode(inputText.toUpperCase()));
//                                }
//                            });
//                        }
                    }
                });


            }

            @Override
            public void onTrackDeliveryBoy(int position) {
                UserDetails deliveryBoyDetails = deliveryBoysAdapter.getItem(position);

                Intent trackLocationAct = new Intent(context, TrackHereMapsLocationActivity.class);
                trackLocationAct.putExtra(Constants.USER_ID, deliveryBoyDetails.getID());
                trackLocationAct.putExtra(Constants.USER_NAME, deliveryBoyDetails.getAvailableName());
                if ( deliveryBoyDetails.getJob() != null ) {
                    trackLocationAct.putExtra(Constants.JOB_DETAILS, new Gson().toJson(deliveryBoyDetails.getJob()));
                }
                startActivity(trackLocationAct);
            }

            @Override
            public void onCallDeliveryBoy(int position) {
                callToPhoneNumber(deliveryBoysAdapter.getItem(position).getMobileNumber());
            }

            @Override
            public void onClearJob(final int position) {
                final UserDetails deliveryBoyDetails = deliveryBoysAdapter.getItem(position);

                showChoiceSelectionDialog("Confirm", "Are you sure to clear the job?", "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES") ){
                            showInputTextDialog("Type 1 to continue", "Type 1", new TextInputCallback() {
                                @Override
                                public void onComplete(String inputText) {
                                    if ( inputText.equalsIgnoreCase("1") ){
                                        showLoadingDialog("Clearing job, wait...");
                                        getBackendAPIs().getEmployeesAPI().clearEmployeeBoyJob(deliveryBoyDetails.getID(), new StatusCallback() {
                                            @Override
                                            public void onComplete(boolean status, int statusCode, String message) {
                                                closeLoadingDialog();
                                                if ( status ){
                                                    deliveryBoyDetails.setJob(null);
                                                    deliveryBoysAdapter.setItem(position, deliveryBoyDetails);
                                                    showToastMessage("Job cleared successfully");
                                                }else{
                                                    showToastMessage("Something went wrong, try again!");
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
        if ( userDetails.isDeliveryBoyManager() ){
            deliveryBoysAdapter.showClearJob();
        }
        deliveryBoysHolder.setAdapter(deliveryBoysAdapter);
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getDeliveryBoysList();
    }

    public void getDeliveryBoysList(){

        showLoadingIndicator("Loading delivery boys, wait..");
        getBackendAPIs().getEmployeesAPI().getDeliveryBoys(new UsersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<UserDetails> deliveryBoysList) {
                if (status == true) {
                    originalDeliveryBoysList = deliveryBoysList;
                    if (deliveryBoysList.size() > 0) {
                        deliveryBoysAdapter.setItems(deliveryBoysList);
                        switchToContentPage();
                    } else {
                        showNoDataIndicator("No delivery boys");
                    }
                } else {
                    showReloadIndicator(statusCode, "Unable to load delivery boys, try again!");
                }
            }
        });
    }

    public void createEmployeeReferralCoupon(String employeeID, RequestCreateEmployeeReferralCouponCode requestCreateEmployeeReferralCouponCode){

        showLoadingDialog("Creating employee referral coupon, wait...");
        getBackendAPIs().getEmployeesAPI().createEmployeeReferralCoupon(employeeID, requestCreateEmployeeReferralCouponCode, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Employee referral coupon created successfully");
                }else{
                    showToastMessage(message);
                }
            }
        });
    }

    //--------------

    public void onPerformSearch(String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchQuery = search_query.toLowerCase();

            showLoadingIndicator("Searching for ("+searchQuery+")");
            getSearchList(searchQuery, new SearchResultsCallback() {
                @Override
                public void onComplete(List<UserDetails> searchList) {
                    if ( searchList.size() > 0 ){
                        deliveryBoysAdapter.setItems(searchList);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("No results found for ("+searchQuery+")");
                    }
                }
            });
        }else{
            onClearSearch();
        }
    }

    private void getSearchList(final String searchQuery, final SearchResultsCallback callback){

        new AsyncTask<Void, Void, Void>() {
            List<UserDetails> filteredList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {

                if ( originalDeliveryBoysList != null && originalDeliveryBoysList.size() > 0 ){
                    for (int i=0;i<originalDeliveryBoysList.size();i++){
                        if ( originalDeliveryBoysList.get(i).getName().toLowerCase().contains(searchQuery) ){
                            filteredList.add(originalDeliveryBoysList.get(i));
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(filteredList);
                }
            }
        }.execute();
    }

    public void onClearSearch(){
        deliveryBoysAdapter.setItems(originalDeliveryBoysList);
        if ( deliveryBoysAdapter.getItemCount() > 0 ){
            switchToContentPage();
        }else{
            showNoDataIndicator("No delivery boys");
        }
    }

    //===============

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        searchViewMenuITem.setVisible(true);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){

        }else {
            onBackPressed();
        }
        return true;
    }

}
