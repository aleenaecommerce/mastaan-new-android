package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.OrderItemsAdapter;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.backend.models.RequestAssignDeliveries;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.FollowupDetailsDialog;
import com.mastaan.logistics.dialogs.MoneyCollectionDetailsDialog;
import com.mastaan.logistics.dialogs.OrderDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.flows.DeliveryFlow;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.printers.BillPrinter;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class AssignDeliveriesActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String actionType = Constants.PENDING_DELIVERIES;
    String deliveryAreaID = "";

    MenuItem switchViewMenuItem;
    MenuItem searchViewMenuITem;
    SearchView searchView;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    vRecyclerView itemsHolder;
    OrderItemsAdapter itemsAdapter;

    Button actionAll;
    View bucketView;
    TextView bucket_count;
    View assignBucket;

    List<OrderItemDetails> currentItemsList = new ArrayList<>();
    List<OrderItemDetails> bucketItemsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_deliveries);
        hasLoadingView();
        hasSwipeRefresh();

        if ( getIntent().getStringExtra(Constants.ACTION_TYPE) != null ){
            actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
            deliveryAreaID = getIntent().getStringExtra(Constants.DELIVERY_AREA_ID);
        }

        //-------

        searchResultsCountIndicator = (FrameLayout) findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) findViewById(R.id.results_count);
        clearFilter = (Button) findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        itemsHolder = (vRecyclerView) findViewById(R.id.itemsHolder);
        setUpHolder();

        actionAll = (Button) findViewById(R.id.actionAll);
        actionAll.setText(Constants.ADD_ALL);
        actionAll.setOnClickListener(this);

        bucketView = findViewById(R.id.bucketFABView);
        bucket_count = (TextView) findViewById(R.id.bucket_count);
        findViewById(R.id.bucketFAB).setOnClickListener(this);
        assignBucket = findViewById(R.id.assignBucket);
        assignBucket.setOnClickListener(this);

        // LOADING DATA AFTER MENU CREATED

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        getItems();
    }

    @Override
    public void onClick(View view) {
        if ( view == actionAll ){
            if ( actionAll.getText().toString().equalsIgnoreCase(Constants.ADD_ALL) ){
                //bucketItemsList = new ArrayList<>();
                for (int i=0;i<itemsAdapter.getItemCount();i++){
                    if ( itemsAdapter.getItem(i).getStatusString().equalsIgnoreCase(Constants.PROCESSED) ){
                        itemsAdapter.updateItem(i, itemsAdapter.getItem(i).setInDeliveryBucket(true));
                        if ( bucketItemsList.contains(itemsAdapter.getItem(i)) == false ) {
                            bucketItemsList.add(itemsAdapter.getItem(i));
                        }
                    }
                }
                //actionAll.setText(Constants.REMOVE_ALL);
                if ( bucketItemsList.size() > 0 ) {
                    bucketView.setVisibility(View.VISIBLE);
                    bucket_count.setText(bucketItemsList.size() + "");
                    showToastMessage("All visible items are added to bucket");// after clearing earlier bucket");
                }else{
                    bucketView.setVisibility(View.GONE);
                }
            }
            else if ( actionAll.getText().toString().equalsIgnoreCase(Constants.REMOVE_ALL) ){
                //bucketItemsList = new ArrayList<>();
                for (int i=0;i<itemsAdapter.getItemCount();i++){
                    if ( itemsAdapter.getItem(i).getStatusString().equalsIgnoreCase(Constants.PROCESSED) ){
                        itemsAdapter.updateItem(i, itemsAdapter.getItem(i).setInDeliveryBucket(false));
                        if ( bucketItemsList.contains(itemsAdapter.getItem(i)) == true ){
                            bucketItemsList.remove(itemsAdapter.getItem(i));
                        }
                    }
                }
                if ( bucketItemsList.size() == 0 ) {
                    showOriginalList();
                }else{
                    onClearSearch();
                }
                showToastMessage("All visible items are removed from bucket");
            }
        }
        else if ( view == findViewById(R.id.bucketFAB) ){
            showBucketList();
        }
        else if ( view == assignBucket ){
            assignBucketToDeliveryBoy();
        }
        else if ( view == clearFilter ){
            try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
            onClearSearch();
        }
    }

    public void showOriginalList(){
        try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
        searchResultsCountIndicator.setVisibility(View.GONE);
        showLoadingIndicator("Loading, wait...");
        new CountDownTimer(300, 100) {
            @Override
            public void onTick(long l) {
            }
            @Override
            public void onFinish() {
                for (int i=0;i<currentItemsList.size();i++){
                    currentItemsList.get(i).setShowInCheckBoxView(false);
                }
                actionAll.setText(Constants.ADD_ALL);
                actionAll.setVisibility(View.VISIBLE);       // TODO
                assignBucket.setVisibility(View.GONE);
                setToolbarTitle("ASSIGN DELIVERIES");
                if ( bucketItemsList.size() > 0 ){
                    bucketView.setVisibility(View.VISIBLE);
                }else {
                    bucketView.setVisibility(View.GONE);
                }
                itemsAdapter.setItems(currentItemsList);
                itemsHolder.scrollToPosition(0);
                switchToContentPage();
            }
        }.start();
    }
    public void showBucketList(){
        try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
        searchResultsCountIndicator.setVisibility(View.GONE);
        showLoadingIndicator("Loading bucket, wait...");
        new CountDownTimer(300, 100) {
            @Override
            public void onTick(long l) {
            }
            @Override
            public void onFinish() {
                for (int i=0;i<bucketItemsList.size();i++){
                    bucketItemsList.get(i).setShowInCheckBoxView(true);
                }
                setToolbarTitle("DELIVERY BUCKET");//' ("+bucketItemsList.size()+")");
                actionAll.setText(Constants.REMOVE_ALL);
                actionAll.setVisibility(View.VISIBLE);       // TODO
                assignBucket.setVisibility(View.VISIBLE);
                bucketView.setVisibility(View.GONE);
                itemsAdapter.setItems(bucketItemsList);
                itemsHolder.scrollToPosition(0);
                switchToContentPage();
            }
        }.start();
    }

    public void setUpHolder(){
        itemsHolder.setupVerticalOrientation();
        itemsAdapter = new OrderItemsAdapter(activity, new ArrayList<OrderItemDetails>(), new OrderItemsAdapter.Callback() {
            @Override
            public void onItemClick(final int position) {
                final OrderItemDetails orderItemDetails = itemsAdapter.getItem(position);

                if ( orderItemDetails.getStatusString().equalsIgnoreCase(Constants.PROCESSED) ){
                    final List<String> optionsList = new ArrayList<>();;
                    if ( bucketItemsList.contains(orderItemDetails) ){
                        optionsList.add("Remove from delivery bucket");
                    }else{
                        optionsList.add("Add to delivery bucket");
                    }
                    showListChooserDialog(optionsList, new ListChooserCallback() {
                        @Override
                        public void onSelect(int optionPosition) {
                            if ( optionsList.get(optionPosition).equalsIgnoreCase("Add to delivery bucket")){
                                addToBucketList(position);
                            }
                            else if ( optionsList.get(optionPosition).equalsIgnoreCase("Remove from delivery bucket")){
                                removeFromBucketList(position);
                            }
                        }
                    });
                }
            }

            @Override
            public void onItemLongClick(int position) {

            }

            public void onShowDirections(final int position) {
                getCurrentLocation(new LocationCallback() {
                    @Override
                    public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                        if (status) {
                            Intent showDirectionsIntent = new Intent(context, DirectionsViewerHereMapsDialogActivity.class);
                            showDirectionsIntent.setAction(Constants.POPUP_ACTION);
                            showDirectionsIntent.putExtra("origin", latLng);
                            showDirectionsIntent.putExtra("destination", itemsAdapter.getItem(position).getOrderDetails().getDeliveryAddressLatLng());
                            showDirectionsIntent.putExtra("location", itemsAdapter.getItem(position).getOrderDetails().getCustomerName() + "\n" + itemsAdapter.getItem(position).getOrderDetails().getCustomerName()/*.getAddress().getDeliveryAddressLocality()*/);
                            startActivity(showDirectionsIntent);
                        } else {
                            showToastMessage(message);
                        }
                    }
                });
            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails2 orderDetails = itemsAdapter.getItem(position).getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails2 orderDetails = itemsAdapter.getItem(position).getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowBuyerHistory(int position) {

            }

            @Override
            public void onShowReferralBuyerDetails(int position) {

            }

            @Override
            public void onPrintBill(final int position) {
                new BillPrinter(activity).start(itemsAdapter.getItem(position).getOrderID(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if ( status ) {
                            itemsAdapter.setBillPrinted(position, true);
                            activity.getOrderItemsDatabase().addOrUpdateOrderItem(itemsAdapter.getItem(position), null);
                        }
                    }
                });
            }

            @Override
            public void onTrackOrder(int position) {

            }

            @Override
            public void onShowOrderDetails(int position) {
                new OrderDetailsDialog(activity).show(itemsAdapter.getItem(position).getOrderDetails().getID());
            }

            @Override
            public void onShowMoneyCollectionDetails(int position) {
                new MoneyCollectionDetailsDialog(activity).showForOrder(itemsAdapter.getItem(position).getOrderID());
            }

            @Override
            public void onCheckPaymentStatus(final int position) {
                final OrderItemDetails orderItemDetails = itemsAdapter.getItem(position);

                showLoadingDialog("Checking payment status, wait...");
                getBackendAPIs().getPaymentAPI().checkOrderPaymentStatus(orderItemDetails.getOrderID(), new PaymentAPI.CheckOrderPaymentStatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, OrderDetails orderDetails) {
                        closeLoadingDialog();
                        if (status){
                            orderItemDetails.getOrderDetails().setTotalAmount(orderDetails.getTotalAmount());
                            orderItemDetails.getOrderDetails().setTotalCashAmount(orderDetails.getTotalCashAmount());
                            orderItemDetails.getOrderDetails().setTotalOnlinePaymentAmount(orderDetails.getTotalOnlinePaymentAmount());

                            itemsAdapter.updateItem(position, orderItemDetails);
                        }else{
                            showToastMessage("Something went wrong while checking payment status.\nPlease try again!");
                        }
                    }
                });
            }

            @Override
            public void onRequestOnlinePayment(int position) {

            }

            @Override
            public void onShowItemImage(int position) {

            }

            @Override
            public void onShowStockDetails(int position) {
                new StockDetailsDialog(activity).show(itemsAdapter.getItem(position));
            }

            @Override
            public void onShowFollowupResultedInOrder(int position) {
                new FollowupDetailsDialog(activity).show(itemsAdapter.getItem(position).getOrderDetails().getFollowupResultedInOrder());
            }

            @Override
            public void onAcknowledge(int position) {

            }

            @Override
            public void onStartProcessing(int position) {

            }

            @Override
            public void onCompleteProcessing(int position) {

            }

            @Override
            public void onAssignDeliveryBoy(int position) {

            }

            @Override
            public void onCompleteDelivery(int position) {

            }

            @Override
            public void onReProcess(int position) {

            }

            @Override
            public void onRejectedItemsHandling(int position) {

            }

            @Override
            public void onCompleteCounterSaleOrder(int position) {

            }

            @Override
            public void onClaimForCustomerSupport(int position) {

            }

            @Override
            public void onCompleteCustomerSupport(int position) {

            }
        });
        itemsAdapter.setCheckCallback(new OrderItemsAdapter.CheckCallback() {
            @Override
            public void onCheckChanged(int position, boolean isChecked) {
                if ( isChecked ){
                    addToBucketList(position);
                }else{
                    removeFromBucketList(position);
                }
            }

            @Override
            public void onShowOrderDetails(int position) {
                new OrderDetailsDialog(activity).show(itemsAdapter.getItem(position).getOrderID());
            }
        });
        itemsHolder.setAdapter(itemsAdapter);

    }

    private void addToBucketList(int position){
        OrderItemDetails orderItemDetails = itemsAdapter.getItem(position);
        bucketItemsList.add(orderItemDetails);

        bucketView.setVisibility(View.VISIBLE);
        bucket_count.setText(bucketItemsList.size()+"");

        itemsAdapter.updateItem(position, itemsAdapter.getItem(position).setInDeliveryBucket(true));

        showToastMessage(CommonMethods.capitalizeStringWords(orderItemDetails.getOrderDetails().getCustomerName())+"'s "+CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+" is added to bucket");
    }
    private void removeFromBucketList(int position){
        OrderItemDetails orderItemDetails = itemsAdapter.getItem(position);
        bucketItemsList.remove(orderItemDetails);
        bucket_count.setText(bucketItemsList.size()+"");

        itemsAdapter.updateItem(position, itemsAdapter.getItem(position).setInDeliveryBucket(false));

        if ( assignBucket.getVisibility() == View.VISIBLE ){
            itemsAdapter.deleteItem(position);
            if ( bucketItemsList.size() == 0 ){ showOriginalList();}
        }else{
            if ( bucketItemsList.size() == 0 ){ bucketView.setVisibility(View.GONE);  }
        }
        showToastMessage(CommonMethods.capitalizeStringWords(orderItemDetails.getOrderDetails().getCustomerName())+"'s "+CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+" is removed from bucket");
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        //if ( assignBucket.getVisibility() != View.VISIBLE ) {
            getItems();
        //}
    }

    private void getItems() {

        setToolbarTitle("Assign Deliveries");
        switchViewMenuItem.setVisible(false);
        searchViewMenuITem.setVisible(false);
        searchViewMenuITem.collapseActionView();
        searchResultsCountIndicator.setVisibility(View.GONE);
        bucketView.setVisibility(View.GONE);
        assignBucket.setVisibility(View.GONE);

        if ( actionType.equalsIgnoreCase(Constants.DELIVERY_GROUP_DELIVERIES)){
            showLoadingIndicator("Loading delivery group items, wait...");
            getBackendAPIs().getOrdersAPI().getDeliveryGroupItems(deliveryAreaID, new OrderItemsListCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                    if (status) {
                        display(orderItemsList);
                    } else {
                        showReloadIndicator(statusCode, "Unable to load delivery group items, try again!");
                    }
                }
            });
        }
        else {
            showLoadingIndicator("Loading pending deliveries, wait...");
            getBackendAPIs().getOrdersAPI().getAssignDeliveryItems(new OrderItemsListCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                    if (status) {
                        display(orderItemsList);
                    } else {
                        showReloadIndicator(statusCode, "Unable to load pending deliveries, try again!");
                    }
                }
            });
        }
    }

    public void display(List<OrderItemDetails> orderItemsList){
        currentItemsList = orderItemsList;
        bucketItemsList = new ArrayList<>();

        getOrderItemsDatabase().addOrUpdateOrderItems(orderItemsList, null);

        if ( orderItemsList.size() > 0 ){
            switchViewMenuItem.setVisible(true);
            searchViewMenuITem.setVisible(true);
            itemsAdapter.setItems(orderItemsList);
            actionAll.setVisibility(View.VISIBLE);  // TODO
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }
    }

    public void assignBucketToDeliveryBoy(){
        new DeliveryFlow(activity).showDeliveryBoySelectionDialog(new DeliveryFlow.DeliveryBoySelectionCallback() {
            @Override
            public void onSelect(final UserDetails deliveryBoyDetails) {
                String itemsList = "";
                for (int i=0;i<bucketItemsList.size();i++){
                    if ( itemsList.length() > 0 ){  itemsList += "<br>";    }
                    itemsList += "<b>#</b> "+CommonMethods.capitalizeFirstLetter(bucketItemsList.get(i).getOrderDetails().getCustomerName())+"'s "+CommonMethods.capitalizeFirstLetter(bucketItemsList.get(i).getMeatItemDetails().getNameWithCategoryAndSize());
                }
                showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to assign following <b>" + bucketItemsList.size() + "</b> items to <b>" + CommonMethods.capitalizeStringWords(deliveryBoyDetails.getAvailableName()) + "</b>?<br><br>"+itemsList), "CANCEL", "ASSIGN", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("ASSIGN") ){
                            assignDeliveries(deliveryBoyDetails);
                        }
                    }
                });
            }
        });
    }

    public void assignDeliveries(final UserDetails deliveryBoyDetails){

        showLoadingDialog("Assigning deliveries, wait...");
        getBackendAPIs().getUsersAPI().assignMultipleDeliveries(new RequestAssignDeliveries(deliveryBoyDetails.getID(), bucketItemsList), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    for (int i=0;i<bucketItemsList.size();i++){
                        OrderItemDetails orderItemDetails = bucketItemsList.get(i);
                        orderItemDetails.setInDeliveryBucket(false);
                        orderItemDetails.setStatus(3);
                        orderItemDetails.setDeliveryBoyAssignedDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
                        orderItemDetails.setDeliveryBoyDetails(deliveryBoyDetails);
                        getOrderItemsDatabase().addOrUpdateOrderItem(orderItemDetails);
                    }
                    showToastMessage("Deliveries assigned successfully");
                    if ( message.equalsIgnoreCase("Success") == false ){
                        activity.showNotificationDialog(false, "Alert", Html.fromHtml(message), new ActionCallback() {
                            @Override
                            public void onAction() {
                                onReloadPressed();
                            }
                        });
                    }
                }else {
                    showNotificationDialog("Failure!", message, new ActionCallback() {
                        @Override
                        public void onAction() {
                            onReloadPressed();
                        }
                    });
                }
            }
        });
    }
//    public void assign(final int index, final UserDetails deliveryBoyDetails){
//        final OrderItemDetails orderItemDetails = bucketItemsList.get(index);
//
//        showLoadingDialog("Assigning "+orderItemDetails.getOrderDetails().getCustomerName()+"'s "+orderItemDetails.getMeatItemDetails().getMainName()+" to "+deliveryBoyDetails.getName()+", wait...");
//        getBackendAPIs().getUsersAPI().assignDelivery(orderItemDetails.getID(), deliveryBoyDetails.getID(), new StatusCallback() {
//            @Override
//            public void onComplete(boolean status, int statusCode, String message) {
//                closeLoadingDialog();
//                if ( status ){
//                    orderItemDetails.setInDeliveryBucket(false);
//                    orderItemDetails.setStatus(3);
//                    orderItemDetails.setDeliveryBoyAssignedDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
//                    orderItemDetails.setDeliveryBoyDetails(deliveryBoyDetails);
//                    getOrderItemsDatabase().addOrUpdateOrderItem(orderItemDetails);
//
//                    if ( bucketItemsList.size()-1 == index ){
//                        showToastMessage("ALL DONE");
//                        onReloadPressed();
//                    }else {
//                        showToastMessage("Assigned "+orderItemDetails.getOrderDetails().getCustomerName()+"'s "+orderItemDetails.getMeatItemDetails().getMainName()+" to "+deliveryBoyDetails.getName()+", wait...");
//                        assign(index + 1, deliveryBoyDetails);
//                    }
//                }else{
//                    showNotificationDialog("Failure!", "Something went wrong, going to pending deliveries and clearing current bucket", new ActionCallback() {
//                        @Override
//                        public void onAction() {
//                            onReloadPressed();
//                        }
//                    });
//                }
//            }
//        });
//    }

    //--------
    SearchDetails searchDetails;
    public void onPerformSearch(final SearchDetails search_details){
        this.searchDetails = search_details;

        if ( searchDetails != null && searchDetails.isSearchable() ){
            searchResultsCountIndicator.setVisibility(View.GONE);
            actionAll.setVisibility(View.GONE);
            showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
            SearchMethods.searchOrderItems(assignBucket.getVisibility() == View.VISIBLE ? bucketItemsList : currentItemsList, searchDetails, null, new SearchMethods.SearchOrderItemsCallback() {
                @Override
                public void onComplete(boolean status, SearchDetails searchDetails, List<OrderItemDetails> filteredList) {
                    if (status && searchDetails == search_details ) {
                        if (filteredList.size() > 0) {
                            searchResultsCountIndicator.setVisibility(View.VISIBLE);
                            if ( filteredList.size() == 1 ){
                                results_count.setText("1 result found.\n(" + searchDetails.getSearchString() + ")");
                            }else {
                                results_count.setText(filteredList.size() + " results found.\n(" + searchDetails.getSearchString() + ")");
                            }

                            itemsAdapter.setItems(filteredList);
                            itemsHolder.scrollToPosition(0);
                            switchToContentPage();
                            actionAll.setVisibility(View.VISIBLE);
                        } else {
                            searchResultsCountIndicator.setVisibility(View.VISIBLE);
                            results_count.setText("No Results");
                            showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");

                            itemsAdapter.clearItems();
                        }
                    } else {
                        switchToContentPage();
                        showToastMessage("Something went wrong, try again!");
                    }
                }
            });
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        searchResultsCountIndicator.setVisibility(View.GONE);
        actionAll.setVisibility(View.GONE);
        if ( assignBucket.getVisibility() == View.VISIBLE ){
            if ( bucketItemsList.size() > 0 ) {
                itemsAdapter.setItems(bucketItemsList);
                switchToContentPage();
            }else{
                showNoDataIndicator("No items");
            }
        }else{
            if ( currentItemsList.size() > 0 ) {
                itemsAdapter.setItems(currentItemsList);
                switchToContentPage();
            }else{
                showNoDataIndicator("No items");
            }
        }
    }

    //=========

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_assign_deliveries, menu);

        switchViewMenuItem = menu.findItem(R.id.action_switcher);
        searchViewMenuITem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(new SearchDetails().setConsiderAtleastOneMatch(query));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_switcher){
            if ( itemsAdapter.getItem(0).isShowInCheckBoxView() ){
                itemsAdapter.showInFullDetailsView();
            }else{
                itemsAdapter.showInCheckBoxView();
            }
        }
        else if ( item.getItemId() == R.id.action_search){
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if ( assignBucket.getVisibility() == View.VISIBLE ){
            showOriginalList();
        }else {
            super.onBackPressed();
        }
    }
}
