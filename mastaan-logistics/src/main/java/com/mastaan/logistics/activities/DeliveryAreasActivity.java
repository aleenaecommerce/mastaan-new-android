package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.DeliveryAreasAdapter;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.DeliveryAreaDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 31/03/17.
 */

public class DeliveryAreasActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String searchQuery;
    MenuItem searchViewMenuItem;
    SearchView searchView;

    vRecyclerView deliveryAreasHolder;
    View addDeliveryArea;
    DeliveryAreasAdapter deliveryAreasAdapter;

    int selectedAreaIndex = -1;
    List<DeliveryAreaDetails> originalItemsList = new ArrayList<>();

    UserDetails userDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_areas);
        hasLoadingView();
        hasSwipeRefresh();

        userDetails = localStorageData.getUserDetails();

        //-----------------

        addDeliveryArea = findViewById(R.id.addDeliveryArea);
        addDeliveryArea.setOnClickListener(this);

        deliveryAreasHolder = (vRecyclerView) findViewById(R.id.deliveryAreasHolder);
        deliveryAreasHolder.setupVerticalOrientation();
        deliveryAreasAdapter = new DeliveryAreasAdapter(activity, new ArrayList<DeliveryAreaDetails>(), new DeliveryAreasAdapter.CallBack() {
            @Override
            public void onItemClick(final int position) {
                selectedAreaIndex = position;

                Intent editDeliveryAreaAct = new Intent(context, AddOrEditDeliveryAreaActivity.class);
                editDeliveryAreaAct.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                editDeliveryAreaAct.putExtra(Constants.DELIVERY_AREA_DETAILS, new Gson().toJson(deliveryAreasAdapter.getItem(position)));
                startActivityForResult(editDeliveryAreaAct, Constants.ADD_OR_EDIT_DELIVERY_AREA_ACTIVITY_CODE);
            }
        });
        deliveryAreasHolder.setAdapter(deliveryAreasAdapter);

        //-----

        // LOADING DELIVERY AREAS AFTER MENU CREATED

    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        getDeliveryAreas();
    }

    @Override
    public void onClick(View view) {
        if ( view == addDeliveryArea ){
            Intent addDeliveryAreaAct = new Intent(context, AddOrEditDeliveryAreaActivity.class);
            addDeliveryAreaAct.putExtra(Constants.ACTION_TYPE, Constants.ADD);
            startActivityForResult(addDeliveryAreaAct, Constants.ADD_OR_EDIT_DELIVERY_AREA_ACTIVITY_CODE);
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getDeliveryAreas();
    }

    public void getDeliveryAreas(){

        searchViewMenuItem.setVisible(false);
        addDeliveryArea.setVisibility(View.GONE);
        showLoadingIndicator("Loading delivery areas, wait..");
        getBackendAPIs().getRoutesAPI().getDeliveryAreas(new RoutesAPI.DeliveryAreasCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<DeliveryAreaDetails> areasList) {
                if (status == true) {
                    originalItemsList = areasList;

                    addDeliveryArea.setVisibility(View.VISIBLE);
                    if (areasList.size() > 0) {
                        searchViewMenuItem.setVisible(true);
                        switchToContentPage();
                        deliveryAreasAdapter.setItems(areasList);
                    }else {
                        showNoDataIndicator("No delivery areas");
                    }
                } else {
                    showReloadIndicator(statusCode, "Unable to load delivery areas, try again!");
                }
            }
        });
    }

    //--------------

    public void onPerformSearch(final String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchQuery = search_query;
            showLoadingIndicator("Searching for '"+search_query+"', wait...");
            new AsyncTask<Void, Void, Void>() {
                List<DeliveryAreaDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... voids) {
                    for (int i=0;i<originalItemsList.size();i++){
                        if ( originalItemsList.get(i).getCode().toLowerCase().contains(searchQuery.toLowerCase())
                                || originalItemsList.get(i).getName().toLowerCase().contains(searchQuery.toLowerCase()) ){
                            filteredList.add(originalItemsList.get(i));
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( searchQuery.equalsIgnoreCase(search_query) ){
                        if ( filteredList.size() > 0 ){
                            deliveryAreasAdapter.setItems(filteredList);
                            deliveryAreasHolder.smoothScrollToPosition(0);
                            switchToContentPage();
                        }else{
                            showNoDataIndicator("Nothing found for '"+searchQuery+"'");
                        }
                    }
                }
            }.execute();
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        deliveryAreasAdapter.setItems(originalItemsList);
        if ( deliveryAreasAdapter.getItemCount() > 0 ){
            deliveryAreasHolder.smoothScrollToPosition(0);
            switchToContentPage();
        }else{
            showNoDataIndicator("No delivery areas");
        }
    }

    //===============

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchview, menu);

        searchViewMenuItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) searchViewMenuItem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.ADD_OR_EDIT_DELIVERY_AREA_ACTIVITY_CODE && resultCode == RESULT_OK ){
                String actionType = data.getStringExtra(Constants.ACTION_TYPE);
                DeliveryAreaDetails deliveryAreaDetails = new Gson().fromJson(data.getStringExtra(Constants.DELIVERY_AREA_DETAILS), DeliveryAreaDetails.class);
                if ( actionType.equalsIgnoreCase(Constants.ADD) ){
                    deliveryAreasAdapter.addItem(deliveryAreaDetails);
                    originalItemsList.add(deliveryAreaDetails);
                    switchToContentPage();
                }
                else  if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                    deliveryAreasAdapter.setItem(selectedAreaIndex, deliveryAreaDetails);

                    for (int i=0;i<originalItemsList.size();i++){
                        if ( originalItemsList.get(i).getCode().equals(deliveryAreaDetails.getCode())){
                            originalItemsList.set(i, deliveryAreaDetails);
                            break;
                        }
                    }
                }
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){

        }else {
            onBackPressed();
        }
        return true;
    }


}
