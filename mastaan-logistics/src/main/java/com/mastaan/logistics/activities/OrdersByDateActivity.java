package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.SearchView;
import android.text.Html;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.OrdersAdapter;
import com.mastaan.logistics.backend.FollowupsAPI;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.backend.models.RequestAddFollowup;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AnalyseOrderItemsDialog;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.ECODDialog;
import com.mastaan.logistics.dialogs.FollowupDetailsDialog;
import com.mastaan.logistics.dialogs.FollowupDialog;
import com.mastaan.logistics.dialogs.MoneyCollectionDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.flows.RefundFlow;
import com.mastaan.logistics.fragments.FilterItemsFragment;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.methods.AnalyseOrderitemsMethods;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class OrdersByDateActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String actionType;

    DrawerLayout drawerLayout;
    FilterItemsFragment filterFragment;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    MenuItem searchViewMenuITem;
    SearchView searchView;
    MenuItem filtereMenuItem;

    TextView showing_date;
    View changeDate;
    TextView summary_info;
    vRecyclerView ordersHolder;
    OrdersAdapter ordersAdapter;

    View showSummary;

    String currentDate = "";
    List<GroupedOrdersItemsList> currentOrderItemsList = new ArrayList<>();
    List<OrderDetails> currentOrdersList = new ArrayList<>();

    SearchDetails searchDetails;

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);setContentView(R.layout.activity_orders_by_date);
        hasLoadingView();
        hasSwipeRefresh();

        userDetails = new LocalStorageData(context).getUserDetails();

        if ( getIntent().getStringExtra(Constants.TITLE) != null ){
            setToolbarTitle(getIntent().getStringExtra(Constants.TITLE));
        }

        actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);

        //--------

        filterFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    clearSearch();
                }
                else {
                    performSearch(searchDetails);
                }
            }
        });

        drawerLayout = findViewById(R.id.drawer_layout);

        searchResultsCountIndicator = findViewById(R.id.searchResultsCountIndicator);
        results_count = findViewById(R.id.results_count);
        clearFilter = findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        showSummary = findViewById(R.id.showSummary);
        showSummary.setOnClickListener(this);

        //---------

        showing_date = findViewById(R.id.showing_date);
        changeDate = findViewById(R.id.changeDate);
        changeDate.setOnClickListener(this);
        summary_info = findViewById(R.id.summary_info);

        ordersHolder = findViewById(R.id.ordersHolder);
        ordersHolder.setupVerticalOrientation();
        ordersAdapter = new OrdersAdapter(activity, new ArrayList<OrderDetails>(), new OrdersAdapter.Callback() {
            @Override
            public void onItemClick(final int position) {
                if ( actionType != null && actionType.equalsIgnoreCase(Constants.PENDING_FIRST_ORDER_FOLLOWUPS) ){
                    new FollowupDialog(activity, "Add First Order Followup").setFollowupType(Constants.FIRST_ORDER_FOLLOWUP).show(ordersAdapter.getItem(position).getBuyerDetails(), new FollowupDialog.Callback() {
                        @Override
                        public void onComplete(final RequestAddFollowup requestObject) {
                            requestObject.setOrder(ordersAdapter.getItem(position).getID());    // Assigning order

                            showLoadingDialog("Adding followup, wait...");
                            getBackendAPIs().getFollowupsAPI().addFollowup(requestObject, new FollowupsAPI.FollowupDetailsCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message, FollowupDetails followupDetails) {
                                    activity.closeLoadingDialog();
                                    if (status) {
                                        showToastMessage("Followup added successfully");
                                        ordersAdapter.deleteItem(position);
                                    }
                                    else {
                                        showToastMessage("Something went wrong while adding followup, try again!");
                                    }
                                }
                            });
                        }
                    });
                }
                else {
                    final OrderDetails orderDetails = ordersAdapter.getItem(position);
                    if (orderDetails.getTotalOnlinePaymentAmount() > 0
                            && (orderDetails.getStatus().equalsIgnoreCase(Constants.DELIVERED) || orderDetails.getStatus().equalsIgnoreCase(Constants.FAILED))
                            && userDetails.isAccountsApprover()) {
                        showListChooserDialog(new String[]{Constants.REFUND_AMOUNT}, new ListChooserCallback() {
                            @Override
                            public void onSelect(final int position) {
                                new RefundFlow(activity).start(orderDetails, new RefundFlow.Callback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message, double refundedAmount) {
                                        if (status) {
                                            orderDetails.addToRefundAmount(refundedAmount);
                                            orderDetails.addToTotalCashAmount(refundedAmount);
                                            ordersAdapter.updateItem(position, orderDetails);
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowDirections(final int position) {
                getCurrentLocation(new LocationCallback() {
                    @Override
                    public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                        if (status) {
                            Intent showDirectionsIntent = new Intent(context, DirectionsViewerDialogActivity.class);
                            showDirectionsIntent.setAction(Constants.POPUP_ACTION);
                            showDirectionsIntent.putExtra("origin", latLng);
                            showDirectionsIntent.putExtra("destination", ordersAdapter.getItem(position).getDeliveryAddressLatLng());
                            showDirectionsIntent.putExtra("location", ordersAdapter.getItem(position).getCustomerName() + "\n" + ordersAdapter.getItem(position).getCustomerName()/*.getAddress().getDeliveryAddressLocality()*/);
                            startActivity(showDirectionsIntent);
                        } else {
                            showToastMessage(message);
                        }
                    }
                });
            }

            @Override
            public void onShowBuyerHistory(int position) {
                new BuyerOptionsDialog(activity).show(ordersAdapter.getItem(position));
            }

            @Override
            public void onShowReferralBuyerDetails(int position) {
                new BuyerOptionsDialog(activity).show(ordersAdapter.getItem(position).getReferralBuyer());
            }

            @Override
            public void onShowMoneyCollectionDetails(int position) {
                new MoneyCollectionDetailsDialog(activity).showForOrder(ordersAdapter.getItem(position).getID());
            }

            @Override
            public void onCheckPaymentStatus(final int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                showLoadingDialog("Checking payment status, wait...");
                getBackendAPIs().getPaymentAPI().checkOrderPaymentStatus(orderDetails.getID(), new PaymentAPI.CheckOrderPaymentStatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, OrderDetails order_details) {
                        closeLoadingDialog();
                        if (status){
                            orderDetails.setTotalAmount(order_details.getTotalAmount());
                            orderDetails.setTotalCashAmount(order_details.getTotalCashAmount());
                            orderDetails.setTotalOnlinePaymentAmount(order_details.getTotalOnlinePaymentAmount());
                            ordersAdapter.updateItem(position, orderDetails);
                        }else{
                            showToastMessage("Something went wrong while checking payment status.\nPlease try again!");
                        }
                    }
                });
            }

            @Override
            public void onRequestOnlinePayment(final int position) {
                new ECODDialog(activity).showForOrder(ordersAdapter.getItem(position), ordersAdapter.getItem(position).getTotalCashAmount(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if ( status ){
                            activity.showToastMessage("Payment success");
                            onCheckPaymentStatus(position);
                        }else{
                            activity.showToastMessage("Payment failed");
                        }
                    }
                });
            }

            @Override
            public void onShowFollowupResultedInOrder(int position) {
                new FollowupDetailsDialog(activity).show(ordersAdapter.getItem(position).getFollowupResultedInOrder());
            }

            @Override
            public void onPrintBill(int position) {

            }

            @Override
            public void onOrderItemClick(int position, int item_position) {

            }

            @Override
            public void onShowOrderItemStockDetails(int position, int item_position) {
                new StockDetailsDialog(activity).show(ordersAdapter.getItem(position).getOrderedItems().get(item_position));
            }
        }).showCheckPaymentStatus().showRequestOnlinePayment();
        if ( userDetails.isCustomerRelationshipManager() || userDetails.isDeliveryBoyManager() || userDetails.isProcessingManager() || userDetails.isOrdersManager() ){
            ordersAdapter.showMessageCustomerOption();
        }
        ordersHolder.setAdapter(ordersAdapter);

        //---------

        if ( getIntent().getStringExtra(Constants.DATE) != null ){
            showing_date.setText(DateMethods.getDateInFormat(getIntent().getStringExtra(Constants.DATE), DateConstants.MMM_DD_YYYY));
            getData(getIntent().getStringExtra(Constants.DATE));
        }
        else{
            showNoDataIndicator("Select date");
            changeDate.performClick();
        }
    }

    @Override
    public void onClick(View view) {
        if ( view == changeDate ){
            String prefillDate = showing_date.getText().toString();
            if ( prefillDate == null || prefillDate.length() == 0 || prefillDate.equalsIgnoreCase("DATE")  ){
                prefillDate = DateMethods.getOnlyDate(localStorageData.getServerTime());
            }
            showDatePickerDialog("Select date", prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    showing_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                    getData(fullDate);
                }
            });
        }
        else if ( view == clearFilter ){
            try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
            clearSearch();
        }
        else if ( view == showSummary ){
            showLoadingDialog("Loading, wait...");
            GroupingMethods.getOrderItemsFromOrders(ordersAdapter.getAllItems(), new OrderItemsListCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(null, orderItemsList, null, new GroupingMethods.GroupedOrderItemsListCallback() {
                        @Override
                        public void onComplete(List<GroupedOrdersItemsList> groupedOrdersItemsLists) {
                            closeLoadingDialog();
                            new AnalyseOrderItemsDialog(activity, groupedOrdersItemsLists).show();
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        if ( showing_date.getText() != null && showing_date.getText().length() > 0 && showing_date.getText().toString().equalsIgnoreCase("DATE") == false ) {
            getData(showing_date.getText().toString());
        }
    }

    private void getData(final String forDate) {

        changeDate.setClickable(false);
        summary_info.setVisibility(View.GONE);
        searchResultsCountIndicator.setVisibility(View.GONE);
        showSummary.setVisibility(View.GONE);
        hideMenuItems();
        showLoadingIndicator("Loading "+DateMethods.getDateInFormat(forDate, DateConstants.MMM_DD_YYYY)+" orders, wait...");
        OrdersAPI.OrdersForDateCallback callback = new OrdersAPI.OrdersForDateCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String loadedDate, List<GroupedOrdersItemsList> orderItemsList, List<OrderDetails> ordersList) {
                if ( DateMethods.compareDates(forDate, loadedDate) == 0 ) {
                    changeDate.setClickable(true);
                    if (status) {
                        displayItems(loadedDate, ordersList, orderItemsList);
                    } else {
                        showReloadIndicator("Something went wrong, try again!");
                    }
                }
            }
        };

        //------

        if ( actionType != null && actionType.equalsIgnoreCase(Constants.FIRST_TIME_ORDERS) ){
            getBackendAPIs().getOrdersAPI().getFirstTimeOrdersForDate(forDate, callback);
        }
        else if ( actionType != null && actionType.equalsIgnoreCase(Constants.PENDING_FIRST_ORDER_FOLLOWUPS) ){
            getBackendAPIs().getOrdersAPI().getPendingFollowupFirstOrdersForDeliveryDate(forDate, callback);
        }
        else{
            getBackendAPIs().getOrdersAPI().getOrdersForDate(forDate, localStorageData.getWarehouseLocation(), callback);
        }

    }

    public void displayItems(final String date, final List<OrderDetails> ordersList, final List<GroupedOrdersItemsList> orderItemsList){

        currentDate = date;
        currentOrderItemsList = orderItemsList;
        currentOrdersList = ordersList;

        if ( orderItemsList.size() > 0 ){
            showLoadingIndicator("Displaying, wait...");
            new AsyncTask<Void, Void, Void>() {
                String shortSummaryInfo = "";
                @Override
                protected Void doInBackground(Void... voids) {
                    if ( getIntent().getBooleanExtra(Constants.HIDE_SUMMARY, false) == false ) {
                        AnalyseOrderitemsMethods analyseOrderitemsMethods = new AnalyseOrderitemsMethods(CommonMethods.getStringListFromStringArray(new String[]{Constants.AMOUNT}), currentOrderItemsList);
                        analyseOrderitemsMethods.analyse();
                        shortSummaryInfo = analyseOrderitemsMethods.getShortAmountInfo();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    showMenuItems();
                    if ( getIntent().getBooleanExtra(Constants.HIDE_SUMMARY, false) == false ) {
                        summary_info.setVisibility(View.VISIBLE);
                        summary_info.setText(Html.fromHtml(shortSummaryInfo));

                        showSummary.setVisibility(View.VISIBLE);
                    }

                    ordersAdapter.setItems(ordersList);
                    ordersHolder.smoothScrollToPosition(0);
                    switchToContentPage();
                }
            }.execute();
        }
        else{
            changeDate.setClickable(true);
            summary_info.setVisibility(View.GONE);
            showNoDataIndicator("No orders  on " + DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY));
        }
    }

    //----------------

    public void performSearch(final SearchDetails search_details){
        this.searchDetails = search_details;

        if ( isShowingLoadingPage() == false && currentOrdersList != null && currentOrdersList.size() > 0 ) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showSummary.setVisibility(View.GONE);
                showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
                SearchMethods.searchOrders(currentOrdersList, searchDetails, localStorageData.getServerTime(), new SearchMethods.SearchOrdersCallback() {
                    @Override
                    public void onComplete(boolean status, SearchDetails searchDetails, List<OrderDetails> filteredList) {
                        if (status && searchDetails == search_details ) {
                            if (filteredList.size() > 0) {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                showSummary.setVisibility(View.VISIBLE);
                                results_count.setText(filteredList.size() + " result(s) found.\n(" + searchDetails.getSearchString() + ")");

                                ordersAdapter.setItems(filteredList);
                                ordersHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText("No Results");
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");

                                ordersAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                ordersAdapter.setItems(currentOrdersList);
            }
        }
    }

    public void clearSearch(){
        this.searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( currentOrdersList.size() > 0 ) {
            showSummary.setVisibility(View.VISIBLE);
            ordersAdapter.setItems(currentOrdersList);
            ordersHolder.scrollToPosition(0);
            switchToContentPage();
        }else{
            showSummary.setVisibility(View.GONE);
            showNoDataIndicator("No items");
        }
        filterFragment.clearSelection();
    }


    //---------

    public void hideMenuItems(){
        try {
            filtereMenuItem.setVisible(false);
            searchViewMenuITem.setVisible(false);searchViewMenuITem.collapseActionView();
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    public void showMenuItems(){
        try {
            filtereMenuItem.setVisible(true);
            searchViewMenuITem.setVisible(true);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }


    //============

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_orders_by_date, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filtereMenuItem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                clearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //filterFragment.updateCustomerName(query);
//                if ( searchDetails != null && searchDetails.isSearchable() ) {
//                    searchDetails.setCustomerName(query);
//                    performSearch(searchDetails);
//                }else{
                    SearchDetails searchDetails = new SearchDetails().setConsiderAtleastOneMatch(query);
                    performSearch(searchDetails);
//                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){

        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            super.onBackPressed();
        }
    }


}