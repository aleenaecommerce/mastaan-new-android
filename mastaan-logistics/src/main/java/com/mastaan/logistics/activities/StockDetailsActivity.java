package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vAppCompatEditText;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.AttributeOptionsAdapter;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.backend.models.RequestUpdateHubMeatItemStock;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItemStock;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AddOrEditStockDialog;
import com.mastaan.logistics.interfaces.StockDetailsCallback;
import com.mastaan.logistics.models.AttributeDetails;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.HubMeatItemStockDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;
import com.mastaan.logistics.models.WarehouseMeatItemStockDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by valueduser on 17/10/18.
 */

public class StockDetailsActivity extends MastaanToolbarActivity implements View.OnClickListener {

    boolean isAnyChanges;

    HubDetails hubDetails;

    TextView subtitle;

    LinearLayout attributesHolder;
    View disabledIndicator;

    View stockDetailsHolder;
    vTextInputLayout stock_type;
    View stockDaysView;
    CheckBox sunday, monday, tuesday, wednesday, thursday, friday, saturday;
    vTextInputLayout stock_threshold_quantity;
    vTextInputLayout stock_quantity;

    View showMeatItemDetails;
    View showStockStatement;
    View addStock;
    View addWastageStock;

    TextView stockLoadingIndicator;

    List<View> attributesViews = new ArrayList<>();

    Button update;


    WarehouseMeatItemDetails warehouseMeatItemDetails;

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_details);
        hasLoadingView();
        hasSwipeRefresh();

        setToolbarTitle(getIntent().getStringExtra(Constants.NAME));

        userDetails = localStorageData.getUserDetails();

        if ( getIntent().getStringExtra(Constants.HUB_DETAILS) != null ){
            hubDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.HUB_DETAILS), HubDetails.class);
        }

        //-------------

        subtitle = findViewById(R.id.subtitle);

        attributesHolder = findViewById(R.id.attributesHolder);
        disabledIndicator = findViewById(R.id.disabledIndicator);

        stockDetailsHolder = findViewById(R.id.stockDetailsHolder);
        stock_type = findViewById(R.id.stock_type);
        stock_type.getEditText().setOnClickListener(this);
        stockDaysView = findViewById(R.id.stockDaysView);
        sunday = findViewById(R.id.sunday);
        monday = findViewById(R.id.monday);
        tuesday = findViewById(R.id.tuesday);
        wednesday = findViewById(R.id.wednesday);
        thursday = findViewById(R.id.thursday);
        friday = findViewById(R.id.friday);
        saturday = findViewById(R.id.saturday);
        stock_threshold_quantity = findViewById(R.id.stock_threshold_quantity);
        stock_quantity = findViewById(R.id.stock_quantity);
        /*stock_quantity.getEditText().setOnLongClickListener(new View.OnLongClickListener() {//TEMP in Dev Mode
            @Override
            public boolean onLongClick(View v) {
                if ( BuildConfig.PRODUCTION == false ){
                    showInputNumberDecimalDialog("Available Quantity", "Quantity", CommonMethods.parseDouble(stock_quantity.getText()), new TextInputCallback() {
                        @Override
                        public void onComplete(String inputText) {
                            stock_quantity.setText(inputText);
                        }
                    });
                }
                return false;
            }
        });*/
        showMeatItemDetails = findViewById(R.id.showMeatItemDetails);
        showMeatItemDetails.setOnClickListener(this);
        showStockStatement = findViewById(R.id.showStockStatement);
        showStockStatement.setOnClickListener(this);
        addStock = findViewById(R.id.addStock);
        addStock.setOnClickListener(this);
        addWastageStock = findViewById(R.id.addWastageStock);
        addWastageStock.setOnClickListener(this);
        stockLoadingIndicator = findViewById(R.id.stockLoadingIndicator);
        stockLoadingIndicator.setOnClickListener(this);

        update = findViewById(R.id.update);
        update.setOnClickListener(this);

        ((vAppCompatEditText)stock_quantity.getEditText()).disablePaste();

        //---------

        if ( hubDetails != null ){
            subtitle.setVisibility(View.VISIBLE);
            subtitle.setText("Hub - "+hubDetails.getName());
        }

        getMeatItemDetails();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        if ( isShowingContentPage() ){
            getStockDetails();
        }else {
            getMeatItemDetails();
        }
    }

    @Override
    public void onClick(final View view) {

        if ( view == stock_type.getEditText() ){
            final String[] stockTypes = new String[]{Constants.UNLIMITED, Constants.LIMITED, Constants.DAYWISE};
            showListChooserDialog("Stock Type", stockTypes, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    stock_type.setText(stockTypes[position]);
                    stockDaysView.setVisibility(stockTypes[position].equalsIgnoreCase(Constants.DAYWISE)?View.VISIBLE:View.GONE);
                    stock_threshold_quantity.setVisibility(stockTypes[position].equalsIgnoreCase(Constants.UNLIMITED)?View.VISIBLE:View.GONE);
                }
            });
        }

        else if ( view == showMeatItemDetails ){
            Intent meatItemDetailsAct = new Intent(context, MeatItemDetailsActivity.class);
            meatItemDetailsAct.putExtra(Constants.NAME, warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize());
            meatItemDetailsAct.putExtra(Constants.ID, warehouseMeatItemDetails.getID());
            startActivityForResult(meatItemDetailsAct, Constants.MEAT_ITEM_DETAILS_ACTIVITY_CODE);
        }
        else if ( view == showStockStatement ){
            Intent stockStatementActivity = new Intent(context, StockStatementActivity.class);
            stockStatementActivity.putExtra(Constants.ACTION_TYPE, Constants.MEAT_ITEMS_STOCK_STATEMENT);
            stockStatementActivity.putExtra(Constants.TITLE, warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize()+" Stock Statement");
            if ( hubDetails != null ){
                String []itemLevelAttributesOptionsNames = getItemLevelAttributesOptionsNames();
                stockStatementActivity.putExtra(Constants.SUB_TITLE, (itemLevelAttributesOptionsNames.length>0?CommonMethods.getStringFromStringArray(itemLevelAttributesOptionsNames)+"\n":"")+"Hub - "+hubDetails.getName());
                stockStatementActivity.putExtra(Constants.HUB_DETAILS, new Gson().toJson(hubDetails));
            }else {
                stockStatementActivity.putExtra(Constants.SUB_TITLE, CommonMethods.getStringFromStringArray(getItemLevelAttributesOptionsNames()));
            }
            stockStatementActivity.putExtra(Constants.ATTRIBUTES_OPTIONS, getItemLevelAttributesOptionsIDs());
            stockStatementActivity.putExtra(Constants.WAREHOUSE_MEAT_ITEM, warehouseMeatItemDetails.getID());
            startActivityForResult(stockStatementActivity, Constants.STOCK_STATEMENT_ACTIVITY_CODE);
        }
        else if ( view == addStock ){
            AddOrEditStockDialog addOrEditStockDialog = new AddOrEditStockDialog(activity);
            if ( hubDetails != null ){
                addOrEditStockDialog.setupForHub(hubDetails);
            }else{
                addOrEditStockDialog.setupForMeatItem().redirectToDetailsPageIfApplicable();
            }
            addOrEditStockDialog.show(new StockDetailsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, StockDetails stockDetails) {
                    if ( status ){
                        isAnyChanges = true;
                        getStockDetails();
                    }
                }
            }).setSourceUpdateCallback(new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String newSource) {
//                    isAnyChanges = true;
//                    warehouseMeatItemDetails.setSource(newSource);
//                    source.setText(newSource);
                }
            }).setMarginDetailsCallback(new AddOrEditStockDialog.MarginDetailsCallback() {
                @Override
                public void onComplete(String marginDetails) {
//                    message.setVisibility(View.VISIBLE);
//                    message.setText(CommonMethods.fromHtml(marginDetails));
                }
            }).setWarehouseMeatItemAndAttributeOptions(warehouseMeatItemDetails, getItemLevelAttributesOptions());
        }
        else if ( view == addWastageStock ){
            AddOrEditStockDialog addOrEditStockDialog = new AddOrEditStockDialog(activity);
            if ( hubDetails != null ){
                addOrEditStockDialog.setupForHubWastage(hubDetails);
            }else{
                addOrEditStockDialog.setupForMeatItemWastage();
            }
            addOrEditStockDialog.show(new StockDetailsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, StockDetails stockDetails) {
                    if ( status ){
                        isAnyChanges = true;
                        getStockDetails();
                    }
                }
            }).setWarehouseMeatItemAndAttributeOptions(warehouseMeatItemDetails, getItemLevelAttributesOptions());
        }

        else if ( view == stockLoadingIndicator ){
            getStockDetails();
        }

        else if ( view == update ){
            // Hub stock
            if ( hubDetails != null ){
                String eThresholdQuantity = stock_threshold_quantity.getText();

                updateHubStockDetails(new RequestUpdateHubMeatItemStock(CommonMethods.parseDouble(eThresholdQuantity)));
            }
            // Warehouse stock
            else{
                String eStockType = stock_type.getText().replaceAll(Constants.UNLIMITED, "ul")
                        .replaceAll(Constants.LIMITED, "l")
                        .replaceAll(Constants.DAYWISE, "d");
                stock_type.checkError("* Enter stock type");
                List<String> eAvailableDays = new ArrayList<>();
                if ( eStockType.equalsIgnoreCase("d") ) {
                    if ( sunday.isChecked() ){ eAvailableDays.add("sun");  }
                    if ( monday.isChecked() ){ eAvailableDays.add("mon");  }
                    if ( tuesday.isChecked() ){ eAvailableDays.add("tue");  }
                    if ( wednesday.isChecked() ){ eAvailableDays.add("wed");  }
                    if ( thursday.isChecked() ){ eAvailableDays.add("thu");  }
                    if ( friday.isChecked() ){ eAvailableDays.add("fri");  }
                    if ( saturday.isChecked() ){ eAvailableDays.add("sat");  }
                }
                String eThresholdQuantity = stock_threshold_quantity.getText();

                if ( eStockType.equalsIgnoreCase("d") && eAvailableDays.size() == 0 ){
                    showToastMessage("* Enter available days");
                    return;
                }

                if ( eStockType.length() > 0 ) {
                    updateWarehouseStockDetails(new RequestUpdateWarehouseMeatItemStock(eStockType, eAvailableDays, CommonMethods.parseDouble(eThresholdQuantity)));
                }
            }
        }
    }

    public void displayMeatItemDetails(WarehouseMeatItemDetails warehouseMeatItemDetails){
        this.warehouseMeatItemDetails = warehouseMeatItemDetails;

        List<AttributeDetails> attributesList = warehouseMeatItemDetails.getMeatItemDetais().getAttributes();

        attributesHolder.removeAllViews();
        attributesViews = new ArrayList<>();
        for (int i=0;i<attributesList.size();i++){
            AttributeDetails attributeDetails = attributesList.get(i);
            if ( attributeDetails != null && attributeDetails.isItemLevelAttribute() ){
                final View attributeView = inflater.inflate(R.layout.view_attribute_item, null);
                attributesViews.add(attributeView);
                attributesHolder.addView(attributeView);

                TextView attribute_name = attributeView.findViewById(R.id.attribute_name);
                final vSpinner attribute_options = attributeView.findViewById(R.id.attribute_options);

                attribute_name.setText(attributeDetails.getName());
                attribute_options.setAdapter(new AttributeOptionsAdapter(context, attributeDetails.getAvailableOptions(), null, null));
                attribute_options.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    boolean isInitialized = false;
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if ( isInitialized ) {
                            getStockDetails();
                        }
                        isInitialized = true;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                attributeView.findViewById(R.id.attributeSelector).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        attribute_options.performClick();
                    }
                });
            }
        }
        switchToContentPage();

        // Loading stock details
        getStockDetails();

    }


    //----------

    public void getMeatItemDetails(){

        getBackendAPIs().getMeatItemsAPI().getWarehouseMeatItemDetails(getIntent().getStringExtra(Constants.ID), new MeatItemsAPI.WarehouseMeatItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final WarehouseMeatItemDetails itemDetails) {
                if ( status ){
                    getBackendAPIs().getMeatItemsAPI().getMeatItemAttributes(itemDetails.getMeatItemID(), new MeatItemsAPI.MeatItemAttributesCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<AttributeDetails> attributesList) {
                            if ( status ){
                                itemDetails.getMeatItemDetais().setAttributes(attributesList);
                                displayMeatItemDetails(itemDetails);
                            }
                            else{
                                showReloadIndicator(statusCode, "Unable to load "+getIntent().getStringExtra(Constants.NAME).toLowerCase()+"'s details, try again!");
                            }
                        }
                    });
                }else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    public void getStockDetails(){

        disabledIndicator.setVisibility(View.VISIBLE);
        stockDetailsHolder.setVisibility(View.GONE);
        update.setEnabled(false);
        stockLoadingIndicator.setVisibility(View.VISIBLE);
        stockLoadingIndicator.setText("Loading stock details, wait...");
        stockLoadingIndicator.setClickable(false);

        // Hub stock
        if ( hubDetails != null ){
            getBackendAPIs().getStockAPI().getHubMeatItemStock(hubDetails.getID(), warehouseMeatItemDetails.getID(), getItemLevelAttributesOptionsIDs(), new StockAPI.HubMeatItemStockCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, HubMeatItemStockDetails stockDetails) {
                    disabledIndicator.setVisibility(View.GONE);

                    if ( status ){
                        stockLoadingIndicator.setVisibility(View.GONE);
                        stockDetailsHolder.setVisibility(View.VISIBLE);
                        //update.setEnabled(true);

                        stock_type.setVisibility(View.GONE);
                        stock_threshold_quantity.setVisibility(View.GONE);
                        stockDaysView.setVisibility(View.GONE);

                        stock_quantity.setHint("Available Quantity (in "+warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+"s)");
                        stock_threshold_quantity.setHint("Daily Threshold Quantity (in "+warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+"s)");

                        stock_threshold_quantity.setText(CommonMethods.getInDecimalFormat(stockDetails.getThresholdQuantity()));
                        stock_quantity.setText(CommonMethods.getInDecimalFormat(stockDetails.getQuantity()));
                    }
                    else{
                        stockLoadingIndicator.setClickable(true);
                        stockLoadingIndicator.setText("Tap to load stock details");
                    }
                }
            });
        }
        // Warehouse stock
        else{
            getBackendAPIs().getStockAPI().getWarehouseMeatItemStock(warehouseMeatItemDetails.getID(), getItemLevelAttributesOptionsIDs(), new StockAPI.WarehouseMeatItemStockCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemStockDetails stockDetails) {
                    disabledIndicator.setVisibility(View.GONE);

                    if ( status ){
                        stockLoadingIndicator.setVisibility(View.GONE);
                        stockDetailsHolder.setVisibility(View.VISIBLE);
                        update.setEnabled(true);

                        stock_quantity.setHint("Available Quantity (in "+warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+"s)");
                        stock_threshold_quantity.setHint("Daily Threshold Quantity (in "+warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+"s)");

                        stock_type.setText(stockDetails.getTypeName());
                        if (stockDetails.getTypeName().equalsIgnoreCase(Constants.DAYWISE)) {
                            stockDaysView.setVisibility(View.VISIBLE);
                            sunday.setChecked(stockDetails.getAvailableDays().contains("sun"));
                            monday.setChecked(stockDetails.getAvailableDays().contains("mon"));
                            tuesday.setChecked(stockDetails.getAvailableDays().contains("tue"));
                            wednesday.setChecked(stockDetails.getAvailableDays().contains("wed"));
                            thursday.setChecked(stockDetails.getAvailableDays().contains("thu"));
                            friday.setChecked(stockDetails.getAvailableDays().contains("fri"));
                            saturday.setChecked(stockDetails.getAvailableDays().contains("sat"));
                        } else {
                            stockDaysView.setVisibility(View.GONE);
                        }

                        if ( stockDetails.getTypeName().equalsIgnoreCase(Constants.UNLIMITED) ){
                            stock_threshold_quantity.setVisibility(View.VISIBLE);
                            if ( stockDetails.getThresholdQuantity() > 0 ){ stock_threshold_quantity.setText(CommonMethods.getInDecimalFormat(stockDetails.getThresholdQuantity()));    }
                        }else{  stock_threshold_quantity.setVisibility(View.GONE);  }

                        stock_quantity.setText(CommonMethods.getInDecimalFormat(stockDetails.getQuantity()));
                    }
                    else{
                        stockLoadingIndicator.setClickable(true);
                        stockLoadingIndicator.setText("Tap to load stock details");
                    }
                }
            });
        }
    }

    public void updateWarehouseStockDetails(final RequestUpdateWarehouseMeatItemStock requestUpdateWarehouseMeatItemStock){

        showLoadingDialog("Updating stock details details, wait...");
        getBackendAPIs().getStockAPI().updateWarehouseMeatItemStock(warehouseMeatItemDetails.getID(), getItemLevelAttributesOptionsIDs(), requestUpdateWarehouseMeatItemStock, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    isAnyChanges = true;

                    showToastMessage("Stock details updated successfully");
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    public void updateHubStockDetails(final RequestUpdateHubMeatItemStock requestUpdateWarehouseMeatItemStock){

        showLoadingDialog("Updating stock details details, wait...");
        getBackendAPIs().getStockAPI().updateHubMeatItemStock(hubDetails.getID(), warehouseMeatItemDetails.getID(), getItemLevelAttributesOptionsIDs(), requestUpdateWarehouseMeatItemStock, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    isAnyChanges = true;

                    showToastMessage("Stock details updated successfully");
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    //-------

    private String[] getItemLevelAttributesOptionsIDs(){
        return getItemLevelAttributesOptionsIDsOrNames(false);
    }
    private String[] getItemLevelAttributesOptionsNames(){
        return getItemLevelAttributesOptionsIDsOrNames(true);
    }
    private String[] getItemLevelAttributesOptionsIDsOrNames(boolean forName){
        List<AttributeOptionDetails> selectedOptions = getItemLevelAttributesOptions();
        if ( selectedOptions != null && selectedOptions.size() > 0 ){
            String[] itemLevelAttributesOptions = new String[selectedOptions.size()];
            for (int i=0;i<selectedOptions.size();i++){
                if ( forName ){
                    itemLevelAttributesOptions[i] = selectedOptions.get(i).getName();
                }else{
                    itemLevelAttributesOptions[i] = selectedOptions.get(i).getID();
                }
            }
            return itemLevelAttributesOptions;
        }
        return new String[0];
    }
    private List<AttributeOptionDetails> getItemLevelAttributesOptions(){
        List<AttributeOptionDetails> itemLevelAttributesOptions = new ArrayList<>();
        if ( attributesViews != null && attributesViews.size() > 0 ){
            for (int i=0;i<attributesViews.size();i++){
                itemLevelAttributesOptions.add((AttributeOptionDetails) ((vSpinner) attributesViews.get(i).findViewById(R.id.attribute_options)).getSelectedItem());
            }
        }
        return itemLevelAttributesOptions;
    }

    //==================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.STOCK_STATEMENT_ACTIVITY_CODE && resultCode == RESULT_OK ){
                isAnyChanges = true;

                getStockDetails();
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if ( getCallingActivity() != null && isAnyChanges == true ){
            setResult(RESULT_OK);
            finish();
        }else {
            super.onBackPressed();
        }
    }
}
