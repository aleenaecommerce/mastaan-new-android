package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.View;

import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 13/8/2016.
 */
public class MyDeliveriesActivity extends BaseOrderItemsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeUI();
        setUpContentSwitcher();
        enableContentSwitcher(false);

        startAutoRefreshTimer(10 * 60);         // 10 Minutes Interval

        //--------

        getMyDeliveriesFromDatabase();
    }

    @Override
    public void onAutoRefreshTimeElapsed() {
        super.onAutoRefreshTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onInactiveTimeElapsed() {
        super.onInactiveTimeElapsed();
        onBackgroundReloadPressed();
    }

    @Override
    public void onReloadPage() {
        super.onReloadPage();
        getMyDeliveries(false);
    }

    @Override
    public void onReloadFromDatabase() {
        super.onReloadFromDatabase();
        getMyDeliveriesFromDatabase();
    }

    @Override
    public void onBackgroundReloadPressed() {
        super.onBackgroundReloadPressed();
        getMyDeliveries(true);
    }


    public void getMyDeliveriesFromDatabase(){

        /*drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        showLoadingIndicator("Loading your deliveries from database, wait...");
        orderItemsDatabase.getMyDeliveries(new OrderItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<OrderItemDetails> itemsList) {
                if (itemsList != null && itemsList.size() > 0) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), itemsList, localStorageData.getHubLocation(), new GroupingMethods.GroupedOrderItemsListCallback() {
                        @Override
                        public void onComplete(List<GroupedOrdersItemsList> groupedOrdersItemsLists) {
                            displayItems(false, groupedOrdersItemsLists, new ArrayList<OrderDetails>());
                            switchToContentPage();

                            getMyDeliveries(true);
                        }
                    });

                } else {
                    getMyDeliveries(false);
                }
            }
        });*/
        getMyDeliveries(false);
    }

    private void getMyDeliveries(final boolean loadInBackground) {

        if ( loadInBackground ){
            loadingIndicator.setVisibility(View.VISIBLE);
            reloadIndicator.setVisibility(View.GONE);
        }else {
            showLoadingIndicator("Loading your deliveries, wait...");
        }
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(R.id.right_drawer));
        displayNewItems.setVisibility(View.GONE);
        getBackendAPIs().getEmployeesAPI().getDeliveryBoyOrders(localStorageData.getUserID(), new OrdersAPI.OrdersAndGroupedOrderItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists) {
                //-----------
                loadingIndicator.setVisibility(View.GONE);
                if (status) {
                    if (loadInBackground) {
                        updateDataAndDisplayItems(groupedOrderItemsLists, new ArrayList<OrderDetails>());
                    } else {
                        displayItems(true, groupedOrderItemsLists, new ArrayList<OrderDetails>());
                    }
                    switchToOrdersView();
                } else {
                    if (loadInBackground) {
                        reloadIndicator.setVisibility(View.VISIBLE);
                        loadingIndicator.setVisibility(View.GONE);
                    } else {
                        showReloadIndicator("Unable to load your deliveries, try again!");
                    }
                }
            }
        });
    }

    //============

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if ( requestCode == Constants.COFIRM_DELIVERY_ACTIVITY_CODE || resultCode == Constants.COFIRM_DELIVERY_ACTIVITY_CODE ){
                onReloadPressed();
            }
        }
        catch (Exception e){}
    }

}
