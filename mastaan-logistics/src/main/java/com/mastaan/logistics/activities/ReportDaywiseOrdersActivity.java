package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.AnalyticsAdapter;
import com.mastaan.logistics.backend.AnalyticsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.AnalyticsDetails;
import com.mastaan.logistics.models.AnalyticsOrders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Venkatesh Uppu on 05/02/18.
 */

public class ReportDaywiseOrdersActivity extends MastaanToolbarActivity implements View.OnClickListener {

    TextView showing_month;
    View changeMonth;
    TextView summary_info;

    vRecyclerView itemsHolder;
    AnalyticsAdapter analyticsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_daywise_orders);
        hasLoadingView();
        hasSwipeRefresh();

        //---------

        showing_month = (TextView) findViewById(R.id.showing_month);
        changeMonth = findViewById(R.id.changeMonth);
        changeMonth.setOnClickListener(this);
        summary_info = (TextView) findViewById(R.id.summary_info);

        itemsHolder = (vRecyclerView) findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        analyticsAdapter = new AnalyticsAdapter(activity, new ArrayList<AnalyticsDetails>(), new AnalyticsAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                Intent ordersByDateAct = new Intent(context, OrdersByDateActivity.class);
                if ( getIntent().getBooleanExtra(Constants.FIRST_TIME_ORDERS, false) ){
                    ordersByDateAct.putExtra(Constants.TITLE, "First Time Orders By Date");
                    ordersByDateAct.putExtra(Constants.ACTION_TYPE, Constants.FIRST_TIME_ORDERS);
                }
                ordersByDateAct.putExtra(Constants.DATE, analyticsAdapter.getItem(position).getAnalyticsOrders().getDate());
                startActivity(ordersByDateAct);
            }
            @Override
            public void onActionClick(int position) {

            }
        });
        itemsHolder.setAdapter(analyticsAdapter);

        //---------

        if ( getIntent().getBooleanExtra(Constants.FIRST_TIME_ORDERS, false) ){
            setToolbarTitle("Daywise First Time Orders Report");
        }
        showNoDataIndicator("Select month");
        changeMonth.performClick();

    }


    @Override
    public void onClick(View view) {
        if ( view == changeMonth ){
            String prefillDate = showing_month.getText().toString();
            if ( prefillDate == null || prefillDate.length() == 0 || prefillDate.equalsIgnoreCase("MONTH") ){
                prefillDate = DateMethods.getCurrentDate();
            }
            showDatePickerDialog("Select month", null, localStorageData.getServerTime(), prefillDate, true, false, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    showing_month.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_YYYY));
                    getData(fullDate);
                }
            });
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        if ( showing_month.getText().toString().length() > 0 && showing_month.getText().toString().equalsIgnoreCase("MONTH") == false ) {
            getData(showing_month.getText().toString());
        }
    }

    public void getData(String forMonth){

        changeMonth.setClickable(false);
        summary_info.setVisibility(View.GONE);
        showLoadingIndicator("Loading report, wait...");
        getBackendAPIs().getAnalyticsAPI().getDaywiseOrdersReportForMonth(forMonth, getIntent().getBooleanExtra(Constants.FIRST_TIME_ORDERS, false), new AnalyticsAPI.AnalyticsOrdersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<AnalyticsOrders> ordersStatsList) {
                changeMonth.setClickable(true);
                if ( status ){
                    if ( ordersStatsList.size() > 0 ){
                        new AsyncTask<Void, Void, Void>() {
                            String summaryInfo = "";
                            @Override
                            protected Void doInBackground(Void... voids) {
                                try{
                                    long count = 0;
                                    double amount = 0, final_amount = 0;
                                    double delivery_charges = 0;
                                    double service_charges = 0;
                                    double surcharges = 0;
                                    double taxes = 0;
                                    double discount = 0;
                                    double coupon_discount = 0;
                                    double membership_discount = 0;
                                    double membership_delivery_discount = 0;
                                    double membership_order_discount = 0;

                                    Map<String, Long> sourcesStats = new HashMap<>();
                                    for (int i=0;i<ordersStatsList.size();i++){
                                        count += ordersStatsList.get(i).getCount();
                                        amount += ordersStatsList.get(i).getAmount();
                                        final_amount += ordersStatsList.get(i).getFinalAmount();
                                        delivery_charges += ordersStatsList.get(i).getDeliveryCharges();
                                        service_charges += ordersStatsList.get(i).getServiceCharges();
                                        surcharges += ordersStatsList.get(i).getSurcharges();
                                        taxes += ordersStatsList.get(i).getTaxes();
                                        discount += ordersStatsList.get(i).getDiscount();
                                        coupon_discount += ordersStatsList.get(i).getCouponDiscount();
                                        membership_discount += ordersStatsList.get(i).getMembershipDiscount();
                                        membership_order_discount += ordersStatsList.get(i).getMembershipOrderDiscount();
                                        membership_delivery_discount +=  ordersStatsList.get(i).getMembershipDeliveryDiscount();

                                        for (int j=0;j<ordersStatsList.get(i).getSources().size();j++){
                                            if ( sourcesStats.containsKey(ordersStatsList.get(i).getSources().get(j).getName()) ) {
                                                sourcesStats.put(ordersStatsList.get(i).getSources().get(j).getName(), sourcesStats.get(ordersStatsList.get(i).getSources().get(j).getName())+ordersStatsList.get(i).getSources().get(j).getCount());
                                            }else{
                                                sourcesStats.put(ordersStatsList.get(i).getSources().get(j).getName(), ordersStatsList.get(i).getSources().get(j).getCount());
                                            }
                                        }
                                    }
                                    //summaryInfo += "Count: <b>"+ CommonMethods.getIndianFormatNumber(count)+"</b> , Amount: <b>"+CommonMethods.getIndianFormatNumber(amount+discount)+"</b>"+(discount>0?", Discount: <b>"+CommonMethods.getIndianFormatNumber(discount)+"</b>":"");

                                    summaryInfo += "Count: <b>"+ CommonMethods.getIndianFormatNumber(count)+"</b>"
                                            +" , Amount: <b>"+CommonMethods.getIndianFormatNumber(Math.round(amount))+"</b>";

                                    if ( delivery_charges > 0 || service_charges > 0 || surcharges > 0 || taxes > 0 ){
                                        String amountSplitString = "O: <b>"+CommonMethods.getIndianFormatNumber(Math.round(amount-delivery_charges-service_charges-surcharges-taxes))+"</b>";
                                        if ( delivery_charges > 0 ){
                                            amountSplitString += ", Del: <b>" + CommonMethods.getIndianFormatNumber(Math.round(delivery_charges)) + "</b>";
                                        }
                                        if ( service_charges > 0 ) {
                                            amountSplitString += ", Ser: <b>" + CommonMethods.getIndianFormatNumber(Math.round(service_charges)) + "</b>";
                                        }
                                        if ( surcharges > 0 ) {
                                            amountSplitString += ", Sur: <b>" + CommonMethods.getIndianFormatNumber(Math.round(surcharges)) + "</b>";
                                        }
                                        if ( taxes > 0 ) {
                                            amountSplitString += ", Tax: <b>" + CommonMethods.getIndianFormatNumber(Math.round(taxes)) + "</b>";
                                        }
                                        summaryInfo += " ("+amountSplitString+")";
                                    }

                                    summaryInfo += ", Final Amount: <b>"+CommonMethods.getIndianFormatNumber(Math.round(final_amount))+"</b>";

                                    if ( discount > 0 ){
                                        String discountString = "Dis: <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(discount))+"</b>";
                                        if ( coupon_discount > 0 ){
                                            discountString += " (Cou: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(coupon_discount))+"</b>)";
                                        }
                                        if ( membership_discount > 0 ){
                                            discountString += " (Mem: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(membership_discount))+"</b>"
                                                    +(" ["
                                                    +(membership_order_discount>0?"O: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(membership_order_discount))+"</b>":"")
                                                    +(membership_delivery_discount>0?(membership_order_discount>0?", ":"")+"Del: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(membership_delivery_discount))+"</b>":"")
                                                    +"]")
                                                    +")";
                                        }
                                        summaryInfo += "<br>"+discountString;
                                    }

                                    String sourceStatsInfo = "";
                                    for (Map.Entry<String, Long> entry : sourcesStats.entrySet()) {
                                        sourceStatsInfo += (sourceStatsInfo.length()>0?", ":"")+entry.getKey()+": <b>"+CommonMethods.getIndianFormatNumber(entry.getValue())+"</b>";
                                    }
                                    if ( sourceStatsInfo.length() > 0 ){    summaryInfo += "<br>"+sourceStatsInfo;  }
                                }catch (Exception e){}
                                return null;
                            }

                            @Override
                            protected void onPostExecute(Void aVoid) {
                                super.onPostExecute(aVoid);

                                summary_info.setVisibility(View.VISIBLE);
                                summary_info.setText(Html.fromHtml(summaryInfo));

                                List<AnalyticsDetails> analyticsList = new ArrayList<>();
                                for(int i=0;i<ordersStatsList.size();i++){
                                    analyticsList.add(new AnalyticsDetails().setAnalyticsOrders(ordersStatsList.get(i)));
                                }
                                analyticsAdapter.setItems(analyticsList);
                                itemsHolder.scrollToPosition(0);
                                switchToContentPage();
                            }
                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                    else{
                        showNoDataIndicator("Nothing to show");
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });

    }

    //===========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}