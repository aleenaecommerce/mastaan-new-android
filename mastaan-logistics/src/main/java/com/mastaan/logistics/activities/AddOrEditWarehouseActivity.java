package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.activities.LocationSelectorActivity;
import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateWarehouse;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.WarehouseDetails;

/**
 * Created by Venkatesh Uppu on 13/04/19.
 */

public class AddOrEditWarehouseActivity extends MastaanToolbarActivity implements View.OnClickListener{

    String actionType = Constants.ADD;

    vTextInputLayout name;
    vTextInputLayout position;
    vTextInputLayout address;

    View done;

    WarehouseDetails warehouseDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_warehouse);

        if ( getIntent().getStringExtra(Constants.ACTION_TYPE) != null ){
            actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        }
        setToolbarTitle(actionType+" Warehouse");

        //------------

        name = findViewById(R.id.name);
        position = findViewById(R.id.position);
        position.getEditText().setOnClickListener(this);
        address = findViewById(R.id.address);

        done = findViewById(R.id.done);
        done.setOnClickListener(this);

        //-----------

        if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
            warehouseDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.WAREHOUSE_DETAILS), WarehouseDetails.class);

            name.setText(warehouseDetails.getName());
            position.setText(warehouseDetails.getLocationString());
            address.setText(warehouseDetails.getFullAddress());
        }

    }

    @Override
    public void onClick(View view) {
        if ( view == position.getEditText() ){
            Intent selectLocationAct = new Intent(context, LocationSelectorActivity.class);
            selectLocationAct.putExtra(ConstantsCommonLibrary.LATLNG, position.getText());
            startActivityForResult(selectLocationAct, Constants.LOCATION_SELECTION_ACTIVITY_CODE);
        }

        else if ( view == done ){
            String eName = name.getText();
            name.checkError("* Enter name");
            String ePosition = position.getText();
            position.checkError("* Enter position latlng");
            String eAddress = address.getText();
            address.checkError("* Enter full address");

            if ( eName.length() > 0 && ePosition.length() > 0 && eAddress.length() > 0 ){
                LatLng eLatLng = LatLngMethods.getLatLngFromString(ePosition);

                if ( eLatLng.latitude !=0 && eLatLng.longitude != 0 ){
                    RequestAddOrUpdateWarehouse requestAddOrUpdateWarehouse = new RequestAddOrUpdateWarehouse(eName, eLatLng, eAddress);

                    if ( actionType.equalsIgnoreCase(Constants.ADD) ){
                        addWarehouse(requestAddOrUpdateWarehouse);
                    }
                    else if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                        updateWarehouse(requestAddOrUpdateWarehouse);
                    }
                }
                else{
                    position.showError("* Enter valid position");
                }
            }
        }
    }

    public void addWarehouse(final RequestAddOrUpdateWarehouse requestObject){

        showLoadingDialog("Adding warehouse, wait...");
        getBackendAPIs().getRoutesAPI().addWarehouse(requestObject, new RoutesAPI.WarehouseDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, WarehouseDetails warehouseDetails) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Warehouse added successfully");

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                    resultData.putExtra(Constants.WAREHOUSE_DETAILS, new Gson().toJson(warehouseDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showSnackbarMessage("Something went wrong while adding warehouse, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            addWarehouse(requestObject);
                        }
                    });
                }
            }
        });
    }

    public void updateWarehouse(final RequestAddOrUpdateWarehouse requestObject){

        showLoadingDialog("Updating warehouse, wait...");
        getBackendAPIs().getRoutesAPI().updateWarehouse(warehouseDetails.getID(), requestObject, new RoutesAPI.WarehouseDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, WarehouseDetails warehouse_details) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Warehouse updated successfully");

                    warehouseDetails.setName(requestObject.getName());
                    warehouseDetails.setLocation(requestObject.getLocation());
                    warehouseDetails.setFullAddress(requestObject.getFullAddress());

                    Intent resultData = new Intent();
                    resultData.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                    resultData.putExtra(Constants.WAREHOUSE_DETAILS, new Gson().toJson(warehouseDetails));
                    setResult(RESULT_OK, resultData);
                    finish();
                }else{
                    showSnackbarMessage("Something went wrong while updating warehouse, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateWarehouse(requestObject);
                        }
                    });
                }
            }
        });
    }

    //=============


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.LOCATION_SELECTION_ACTIVITY_CODE && resultCode == RESULT_OK ){
                PlaceDetails placeDetails = new Gson().fromJson(data.getStringExtra(ConstantsCommonLibrary.PLACE_DETAILS), PlaceDetails.class);
                position.setText(placeDetails.getLatLng().latitude+","+placeDetails.getLatLng().longitude);
                address.setText(placeDetails.getFullAddress());
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}
