package com.mastaan.logistics.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TimePickerCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vAppCompatEditText;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vScrollView;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.AvailabilitiesAdapter;
import com.mastaan.logistics.adapters.EditAttributesAdapter;
import com.mastaan.logistics.adapters.ItemLogsAdapter;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.backend.models.RequestItemLogs;
import com.mastaan.logistics.backend.models.RequestUpdateMeatItem;
import com.mastaan.logistics.backend.models.RequestUpdateMeatItemAvaialability;
import com.mastaan.logistics.backend.models.RequestUpdateOptionAvailability2;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItem;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.flows.AttributesFlow;
import com.mastaan.logistics.models.AttributeDetails;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.AvailabilityDetails;
import com.mastaan.logistics.models.ItemLogDetails;
import com.mastaan.logistics.models.MeatItemDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MeatItemDetailsActivity extends MastaanToolbarActivity implements View.OnClickListener {
    MastaanToolbarActivity activity;

    boolean isAnyChanges;

    String []tabItems = new String[]{Constants.BASIC.toUpperCase()/*, Constants.STOCK.toUpperCase()*/, Constants.AVAILABILITY.toUpperCase(), Constants.ATTRIBUTES.toUpperCase(), Constants.LOGS.toUpperCase()};

    TabLayout tabLayout;
    vScrollView basicDetailsView;
    /*View stockDetailsView;*/
    View availabilityDetailsView;
    View attributesView;
    View logsView;

    ImageView item_image;
    vTextInputLayout minimum_weight;
    vTextInputLayout availability_start_time;
    View clearAvailabilityStartTime;
    vTextInputLayout availability_end_time;
    View clearAvailabilityEndTime;
    vTextInputLayout preparation_time;
    RadioButton yesEnabled;
    RadioButton notEnabled;
    RadioButton yesAvailable;
    RadioButton notAvailable;
    RadioButton yesAddon;
    RadioButton notAddon;
    RadioButton yesPreOrder;
    RadioButton notPreOrder;
    vTextInputLayout preorder_cutoff_time;
    View clearPreOrderCutoffTime;
    CheckBox size;
    View sizeView;
    vTextInputLayout minimum_size;
    vTextInputLayout maximum_size;
    vTextInputLayout size_unit;
    vTextInputLayout highlight_text;
    vTextInputLayout source_name;
    vTextInputLayout source_area;
    vTextInputLayout index;

    /*View stockDetailsHolder;
    vTextInputLayout stock_type;
    View stockDaysView;
    CheckBox sunday, monday, tuesday, wednesday, thursday, friday, saturday;
    vTextInputLayout stock_threshold_quantity;
    vTextInputLayout stock_quantity;
    TextView stock_quantity_unit;
    View showStockStatement;
    View addStock;
    TextView stockLoadingIndicator;*/

    vScrollView availabilityDetailsScrollView;
    View cantChangeAvailabilityIndicator;
    vSpinner date_spinner;
    AvailabilitiesAdapter dateSpinnerAdapter;
    vTextInputLayout vendor_price;
    vTextInputLayout buyer_price;
    vTextInputLayout discount;
    RadioButton yesAvailableOnDate;
    RadioButton notAvailableOnDate;
    ListArrayAdapter unavailabilityReasonsAdapter;
    View unavailableReasonsHolder;
    vSpinner unavailable_reasons_spinner;
    vAppCompatEditText unavailable_reason;

    LinearLayout optionsHolder;
    List<View> optionsViews = new ArrayList<>();
    vRecyclerView attributesHolder;
    EditAttributesAdapter attributesAdapter;

    RadioGroup logsRadioGroup;
    RadioButton showMeatItemLogs;
    RadioButton showWarehouseMeatItemLogs;
    vRecyclerView meatItemLogsHolder;
    vRecyclerView warehouseMeatItemLogsHolder;
    TextView logsLoadingIndicator;

    TextView message;

    Button update;

    View disabledIndicator;

    WarehouseMeatItemDetails warehouseMeatItemDetails;
    List<AvailabilityDetails> fullAvailabilitiesList = new ArrayList<>();
    List<String> itemNotAvailableReasonsList = new ArrayList<>();

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meat_item_details);
        hasLoadingView();
        activity = this;

        if ( getIntent().getStringExtra(Constants.NAME) != null ) {
            setToolbarTitle(getIntent().getStringExtra(Constants.NAME));
        }

        itemNotAvailableReasonsList = localStorageData.getItemNotAvailableReasons();
        if ( itemNotAvailableReasonsList.size() == 0 ){
            itemNotAvailableReasonsList.add(Constants.SELECT);
        }else{
            itemNotAvailableReasonsList.add(0, Constants.SELECT);
        }
        itemNotAvailableReasonsList.add(Constants.OTHER);

        userDetails = localStorageData.getUserDetails();

        //-------------

        tabLayout = findViewById(R.id.tabLayout);
        basicDetailsView = findViewById(R.id.basicDetailsView);
        /*stockDetailsView = findViewById(R.id.stockDetailsView);*/
        availabilityDetailsView = findViewById(R.id.availabilityDetailsView);
        attributesView = findViewById(R.id.attributesView);
        logsView = findViewById(R.id.logsView);

        item_image = findViewById(R.id.item_image);
        minimum_weight = findViewById(R.id.minimum_weight);
        availability_start_time = findViewById(R.id.availability_start_time);
        availability_start_time.getEditText().setOnClickListener(this);
        clearAvailabilityStartTime = findViewById(R.id.clearAvailabilityStartTime);
        clearAvailabilityStartTime.setOnClickListener(this);
        availability_end_time = findViewById(R.id.availability_end_time);
        availability_end_time.getEditText().setOnClickListener(this);
        clearAvailabilityEndTime = findViewById(R.id.clearAvailabilityEndTime);
        clearAvailabilityEndTime.setOnClickListener(this);
        preparation_time = findViewById(R.id.preparation_time);
        yesEnabled = findViewById(R.id.yesEnabled);
        notEnabled = findViewById(R.id.notEnabled);
        yesAvailable = findViewById(R.id.yesAvailable);
        notAvailable = findViewById(R.id.notAvailable);
        yesAddon = findViewById(R.id.yesAddon);
        notAddon = findViewById(R.id.notAddon);
        yesPreOrder = findViewById(R.id.yesPreOrder);
        notPreOrder = findViewById(R.id.notPreOrder);
        preorder_cutoff_time = findViewById(R.id.preorder_cutoff_time);
        preorder_cutoff_time.getEditText().setOnClickListener(this);
        clearPreOrderCutoffTime = findViewById(R.id.clearPreOrderCutoffTime);
        clearPreOrderCutoffTime.setOnClickListener(this);
        yesPreOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                preorder_cutoff_time.setVisibility(isChecked?View.VISIBLE:View.GONE);
                clearPreOrderCutoffTime.setVisibility(isChecked?View.VISIBLE:View.GONE);
            }
        });

        size = findViewById(R.id.size);
        sizeView = findViewById(R.id.sizeView);
        minimum_size = findViewById(R.id.minimum_size);
        maximum_size = findViewById(R.id.maximum_size);
        size_unit = findViewById(R.id.size_unit);
        size_unit.getEditText().setOnClickListener(this);
        size.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                sizeView.setVisibility(isChecked?View.VISIBLE:View.GONE);
                basicDetailsView.scrollToEnd();
            }
        });
        highlight_text = findViewById(R.id.highlight_text);
        source_name = findViewById(R.id.source_name);
        source_area = findViewById(R.id.source_area);
        index = findViewById(R.id.index);

        /*stockDetailsHolder = findViewById(R.id.stockDetailsHolder);
        stock_type = findViewById(R.id.stock_type);
        stock_type.getEditText().setOnClickListener(this);
        stockDaysView = findViewById(R.id.stockDaysView);
        sunday = findViewById(R.id.sunday);
        monday = findViewById(R.id.monday);
        tuesday = findViewById(R.id.tuesday);
        wednesday = findViewById(R.id.wednesday);
        thursday = findViewById(R.id.thursday);
        friday = findViewById(R.id.friday);
        saturday = findViewById(R.id.saturday);
        stock_threshold_quantity = findViewById(R.id.stock_threshold_quantity);
        stock_quantity = findViewById(R.id.stock_quantity);
        stock_quantity_unit = findViewById(R.id.stock_quantity_unit);
        showStockStatement = findViewById(R.id.showStockStatement);
        showStockStatement.setOnClickListener(this);
        addStock = findViewById(R.id.addStock);
        addStock.setOnClickListener(this);
        stockLoadingIndicator = findViewById(R.id.stockLoadingIndicator);
        stockLoadingIndicator.setOnClickListener(this);*/

        availabilityDetailsScrollView = findViewById(R.id.availabilityDetailsScrollView);
        cantChangeAvailabilityIndicator = findViewById(R.id.cantChangeAvailabilityIndicator);
        date_spinner = findViewById(R.id.date_spinner);
        vendor_price = findViewById(R.id.vendor_price);
        buyer_price = findViewById(R.id.buyer_price);
        discount = findViewById(R.id.discount);
        yesAvailableOnDate = findViewById(R.id.yesAvailableOnDate);
        notAvailableOnDate = findViewById(R.id.notAvailableOnDate);
        unavailableReasonsHolder = findViewById(R.id.unavailableReasonsHolder);
        unavailable_reasons_spinner = findViewById(R.id.unavailable_reasons_spinner);
        unavailable_reason = findViewById(R.id.unavailable_reason);
        notAvailableOnDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if ( isChecked ){
                    unavailableReasonsHolder.setVisibility(View.VISIBLE);
                    unavailable_reasons_spinner.setSelection(0);
                    unavailable_reason.setText("");
                    unavailable_reason.setVisibility(View.GONE);
                }else{
                    unavailable_reason.setText("");
                    unavailableReasonsHolder.setVisibility(View.GONE);
                }
            }
        });

        optionsHolder = findViewById(R.id.optionsHolder);
        attributesHolder = findViewById(R.id.attributesHolder);

        logsRadioGroup = findViewById(R.id.logsRadioGroup);
        showMeatItemLogs = findViewById(R.id.showMeatItemLogs);
        showWarehouseMeatItemLogs = findViewById(R.id.showWarehouseMeatItemLogs);
        meatItemLogsHolder = findViewById(R.id.meatItemLogsHolder);
        meatItemLogsHolder.setupVerticalOrientation();
        warehouseMeatItemLogsHolder = findViewById(R.id.warehouseMeatItemLogsHolder);
        warehouseMeatItemLogsHolder.setupVerticalOrientation();
        logsLoadingIndicator = findViewById(R.id.logsLoadingIndicator);
        logsLoadingIndicator.setOnClickListener(this);
        logsRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int buttonID) {
                RadioButton checkedButton = radioGroup.findViewById(buttonID);
                if ( checkedButton == showMeatItemLogs ){
                    meatItemLogsHolder.setVisibility(View.VISIBLE);
                    warehouseMeatItemLogsHolder.setVisibility(View.GONE);
                }else{
                    meatItemLogsHolder.setVisibility(View.GONE);
                    warehouseMeatItemLogsHolder.setVisibility(View.VISIBLE);
                }
            }
        });

        message = findViewById(R.id.message);

        update = findViewById(R.id.update);
        update.setOnClickListener(this);

        disabledIndicator = findViewById(R.id.disabledIndicator);
        disabledIndicator.setOnClickListener(this);

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabTextColors(getResources().getColor(R.color.normal_tab_text_color), getResources().getColor(R.color.selected_tab_text_color));
        for (int i = 0; i < tabItems.length; i++) {
            TabLayout.Tab tab = tabLayout.newTab();
            View rowView = LayoutInflater.from(context).inflate(R.layout.view_tab_list_item, null, false);
            TextView tabTitle = rowView.findViewById(R.id.tabTitle);
            tabTitle.setText(tabItems[i]);
            tab.setCustomView(rowView);
            tabLayout.addTab(tab);
        }
        toggleUpdateButtonVisibilityForTab(0);  //  Toggling Update Button Visibility
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //viewsContainer.setDisplayedChild(tab.getPosition());
                showSelectedTabView(tab.getPosition());
            }

            private void showSelectedTabView(int index){
                View []views = new View[]{basicDetailsView/*, stockDetailsView*/, availabilityDetailsView, attributesView, logsView};
                for (int i=0;i<views.length;i++){
                    if ( i == index ){
                        views[i].setVisibility(View.VISIBLE);
                    }else{
                        views[i].setVisibility(View.GONE);
                    }
                }
                toggleUpdateButtonVisibilityForTab(index);  //  Toggling Update Button Visibility
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });

        date_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previousPosition = -1;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( previousPosition != -1 ) {
                    displayAvailabilityDetailsForDate(fullAvailabilitiesList.get(position));
                }
                previousPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        unavailable_reasons_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previousPosition = -1;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( previousPosition != -1 ) {
                    if ( itemNotAvailableReasonsList.get(position).equalsIgnoreCase(Constants.OTHER) ){
                        unavailable_reason.setVisibility(View.VISIBLE);
                    }else{
                        unavailable_reason.setVisibility(View.GONE);
                    }
                }
                previousPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //-------------

        if ( getIntent().getStringExtra(Constants.MESSAGE) != null && getIntent().getStringExtra(Constants.MESSAGE).trim().length() > 0 ){
            message.setVisibility(View.VISIBLE);
            message.setText(getIntent().getStringExtra(Constants.MESSAGE).trim());
        }

        //--------------

        if ( userDetails.isStockManager() == false && userDetails.isProcessingManager() == false ) {
            update.setEnabled(false);
            disabledIndicator.setVisibility(View.VISIBLE);
        }

        getMeatItemDetails();
    }

    private void toggleUpdateButtonVisibilityForTab(int tabIndex){
        if ( tabItems[tabIndex].equalsIgnoreCase(Constants.BASIC) /*|| tabItems[tabIndex].equalsIgnoreCase(Constants.STOCK)*/ || tabItems[tabIndex].equalsIgnoreCase(Constants.AVAILABILITY) ){
            update.setVisibility((userDetails.isStockManager() || userDetails.isWarehouseManager())?View.VISIBLE:View.GONE);
        }else{
            update.setVisibility(View.GONE);
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getMeatItemDetails();
    }

    @Override
    public void onClick(final View view) {

        if ( view == availability_start_time.getEditText() ){
            showTimePickerDialog("Availability Start Time", availability_start_time.getText(), new TimePickerCallback() {
                @Override
                public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
                    availability_start_time.setText(fullTimein12HrFormat);
                }
            });
        }
        else if ( view == clearAvailabilityStartTime ){
            availability_start_time.setText("");
        }

        else if ( view == availability_end_time.getEditText() ){
            showTimePickerDialog("Availability End Time (Only Current Day)", availability_end_time.getText(), new TimePickerCallback() {
                @Override
                public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
                    availability_end_time.setText(fullTimein12HrFormat);
                }
            });
        }
        else if ( view == clearAvailabilityEndTime ){
            availability_end_time.setText("");
        }

        else if ( view == preorder_cutoff_time.getEditText() ){
            showTimePickerDialog("Preorder cutoff Time", preorder_cutoff_time.getText(), new TimePickerCallback() {
                @Override
                public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
                    preorder_cutoff_time.setText(fullTimein12HrFormat);
                }
            });
        }
        else if ( view == clearPreOrderCutoffTime ){
            preorder_cutoff_time.setText("");
        }

        else if ( view == size_unit.getEditText() ){
            final String[] sizeUnits = new String[]{"Kgs", "Count", "Pieces", "Sets", "Units"};
            showListChooserDialog("Size Unit", sizeUnits, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    size_unit.setText(sizeUnits[position].toLowerCase());
                }
            });
        }

        /*else if ( view == stock_type.getEditText() ){
            final String[] stockTypes = new String[]{Constants.UNLIMITED, Constants.LIMITED, Constants.DAYWISE};
            showListChooserDialog("Stock Type", stockTypes, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    stock_type.setText(stockTypes[position]);
                    stockDaysView.setVisibility(stockTypes[position].equalsIgnoreCase(Constants.DAYWISE)?View.VISIBLE:View.GONE);
                    stock_threshold_quantity.setVisibility(stockTypes[position].equalsIgnoreCase(Constants.UNLIMITED)?View.VISIBLE:View.GONE);
                }
            });
        }

        else if ( view == showStockStatement ){
            Intent stockStatementActivity = new Intent(context, StockStatementActivity.class);
            stockStatementActivity.putExtra(Constants.ACTION_TYPE, Constants.MEAT_ITEMS_STOCK_STATEMENT);
            stockStatementActivity.putExtra(Constants.TITLE, warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize()+" Stock Statement");
            stockStatementActivity.putExtra(Constants.WAREHOUSE_MEAT_ITEM, warehouseMeatItemDetails.getID());
            startActivity(stockStatementActivity);
        }
        else if ( view == addStock ){
            new AddOrEditStockDialog(activity, new StockDetailsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, StockDetails stockDetails) {
                    if ( status ){
                        displayStockDetails();
                    }
                }
            }).setSourceUpdateCallback(new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String newSource) {
                    isAnyChanges = true;
                    warehouseMeatItemDetails.setSource(newSource);
                    source.setText(newSource);
                }
            }).setMarginDetailsCallback(new AddOrEditStockDialog.MarginDetailsCallback() {
                @Override
                public void onComplete(String marginDetails) {
                    message.setVisibility(View.VISIBLE);
                    message.setText(CommonMethods.fromHtml(marginDetails));
                }
            }).show().setWarehouseMeatItem(warehouseMeatItemDetails);
        }

        else if ( view == stockLoadingIndicator ){
            displayStockDetails();
        }*/

        else if ( view == logsLoadingIndicator ){
            displayLogs();
        }

        else if ( view == update ){
            //if ( localStorageData.isUserCheckedIn() ) {
                if ( tabItems[tabLayout.getSelectedTabPosition()].equalsIgnoreCase(Constants.BASIC) ) {
                    final String eMinimumWeight = minimum_weight.getText().trim();
                    minimum_weight.checkError("* Enter Minimum weight");
                    final String eAvailabilityStartTime = availability_start_time.getText().trim();
                    final String eAvailabilityEndTime = availability_end_time.getText().trim();
                    final String ePreparationTime = preparation_time.getText().trim();
                    preparation_time.checkError("* Enter Preparation time");
                    boolean isEnabled = false;
                    if ( yesEnabled.isChecked() ){
                        isEnabled = true;
                    }
                    boolean isAvailable = false;
                    if (yesAvailable.isChecked()) {
                        isAvailable = true;
                    }
                    boolean isPreOrderable = false;
                    String ePreOrderCutoffTime = "";
                    if (yesPreOrder.isChecked()) {
                        isPreOrderable = true;
                        ePreOrderCutoffTime = preorder_cutoff_time.getText();
                    }
                    boolean isAddon = false;
                    if (yesAddon.isChecked()) {
                        isAddon = true;
                    }
                    String eSize = "";
                    if ( size.isChecked() ){
                        minimum_size.checkError("* Enter minimum size");
                        maximum_size.checkError("* Enter maximum size");
                        size_unit.checkError("* Enter size unit");
                        eSize = minimum_size.getText()+"-"+maximum_size.getText()+" "+size_unit.getText();
                        if ( minimum_size.getText().trim().length() == 0 || maximum_size.getText().trim().length() == 0 || size_unit.getText().trim().length() == 0 ){
                            return;
                        }
                        if ( Double.parseDouble(minimum_size.getText()) >= Double.parseDouble(maximum_size.getText()) ){
                            maximum_size.showError("* Maximum size must be greater than minimum size");
                            return;
                        }
                    }
                    String eHighlightText = highlight_text.getText().trim();
                    source_name.setText(source_name.getText().replaceAll(",", " ").replaceAll("\\s+", " ").trim());
                    source_area.setText(source_area.getText().replaceAll(",", " ").replaceAll("\\s+", " ").trim());
                    String eSource = source_name.getText().trim();
                    if ( source_area.getText().trim().length() > 0 ){
                        if ( eSource.length() == 0 ) {
                            showToastMessage("* Please provide source name first");
                            return;
                        }
                        eSource += ", "+source_area.getText().trim().replaceAll(",", " ");
                    }

                    String eIndex = index.getText().trim();
                    index.checkError("* Enter index");

                    if (eMinimumWeight.length() > 0 && ePreparationTime.length() > 0 && eIndex.length() > 0 ) {
                        updateMeatItemDetails(new RequestUpdateMeatItem(Double.parseDouble(eMinimumWeight), ((Double.parseDouble(ePreparationTime)) / 60), isPreOrderable, DateMethods.getTimeIn24HrFormat(ePreOrderCutoffTime), isAddon, eSize)
                                            , new RequestUpdateWarehouseMeatItem(isEnabled, isAvailable, DateMethods.getDateInFormat(eAvailabilityStartTime, DateConstants.HH_MM), DateMethods.getDateInFormat(eAvailabilityEndTime, DateConstants.HH_MM), eHighlightText, eSource, Double.parseDouble(eIndex)));
                    }else{
                        showToastMessage("* Enter all fields");
                    }
                }
                /*else if ( tabItems[tabLayout.getSelectedTabPosition()].equalsIgnoreCase(Constants.STOCK) ){

                    if ( stockDetailsHolder.getVisibility() == View.VISIBLE ){
                        String eStockType = stock_type.getText().replaceAll(Constants.UNLIMITED, "ul")
                                .replaceAll(Constants.LIMITED, "l")
                                .replaceAll(Constants.DAYWISE, "d");
                        stock_type.checkError("* Enter stock type");
                        List<String> eAvailableDays = new ArrayList<>();
                        if ( eStockType.equalsIgnoreCase("d") ) {
                            if ( sunday.isChecked() ){ eAvailableDays.add("sun");  }
                            if ( monday.isChecked() ){ eAvailableDays.add("mon");  }
                            if ( tuesday.isChecked() ){ eAvailableDays.add("tue");  }
                            if ( wednesday.isChecked() ){ eAvailableDays.add("wed");  }
                            if ( thursday.isChecked() ){ eAvailableDays.add("thu");  }
                            if ( friday.isChecked() ){ eAvailableDays.add("fri");  }
                            if ( saturday.isChecked() ){ eAvailableDays.add("sat");  }
                        }
                        String eThresholdQuantity = stock_threshold_quantity.getText();

                        if ( eStockType.equalsIgnoreCase("d") && eAvailableDays.size() == 0 ){
                            showToastMessage("* Enter available days");
                            return;
                        }

                        if ( eStockType.length() > 0 ) {
                            updateStockDetails(new RequestUpdateWarehouseMeatItemStock(eStockType, eAvailableDays, CommonMethods.parseDouble(eThresholdQuantity)));
                        }
                    }
                }*/
                else if ( tabItems[tabLayout.getSelectedTabPosition()].equalsIgnoreCase(Constants.AVAILABILITY) ){
                    String selectedDate = "";
                    try{    selectedDate = DateMethods.getDateInFormat(fullAvailabilitiesList.get(date_spinner.getSelectedItemPosition()).getDate(), "dd-MM-yyyy");}catch (Exception e){}
                    String eVendorPrice = vendor_price.getText().trim();
                    vendor_price.checkError("* Enter Vendor price");
                    String eBuyerPrice = buyer_price.getText().trim();
                    buyer_price.checkError("* Enter Buyer price");
                    String eDiscount = discount.getText().trim();
                    boolean isAvailable = false;
                    if (yesAvailableOnDate.isChecked()) {
                        isAvailable = true;
                    }
                    String availabilityReason = "";
                    if ( isAvailable == false ) {
                        availabilityReason = itemNotAvailableReasonsList.get(unavailable_reasons_spinner.getSelectedItemPosition());
                        if (availabilityReason.equalsIgnoreCase(Constants.SELECT)) {
                            availabilityReason = "";
                        } else if (availabilityReason.equalsIgnoreCase(Constants.OTHER)) {
                            availabilityReason = unavailable_reason.getText().toString();
                        }
                    }

                    if ( selectedDate.length() > 0 ) {
                        if ( eVendorPrice.length() > 0 && eBuyerPrice.length() > 0 ){
                            if( (isAvailable || (isAvailable == false && availabilityReason.length() > 0 )) ){
                                if ( eDiscount.length() > 0 && CommonMethods.parseDouble(eDiscount) > CommonMethods.parseDouble(eBuyerPrice) ){
                                    showToastMessage("* Discount must be less than buyer price");
                                    return;
                                }

                                //showToastMessage(selectedDate+" > vP: "+vendorPrice+", bP: "+buyerPrice+", Avl: "+isAvailable);
                                List<RequestUpdateOptionAvailability2> optionsAvailabilitiesRequests = getDaywiseChangeAttributeOptionsFilledRequests();
                                if ( optionsAvailabilitiesRequests != null ){
                                    RequestUpdateMeatItemAvaialability requestUpdateMeatItemAvaialability = new RequestUpdateMeatItemAvaialability(selectedDate, CommonMethods.parseDouble(eVendorPrice), CommonMethods.parseDouble(eBuyerPrice), CommonMethods.parseDouble(eDiscount), isAvailable, availabilityReason, optionsAvailabilitiesRequests);
                                    updateMeatItemAvailabilityDetails(requestUpdateMeatItemAvaialability);
                                }
                            }else{
                                availabilityDetailsScrollView.focusToViewTop(unavailable_reason);         // FOCUSING VIEW
                                showToastMessage("* Select/Enter unavailability reason");
                            }
                        }else{
                            showToastMessage("* Enter all fields");
                        }
                    }else{
                        showToastMessage("* Please select date");
                    }
                }
            //}else{
            //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
            //}
        }

        else if ( view == disabledIndicator ){
            showToastMessage("You are not allowed to perform any operations, please contact stock/processing manager");
        }
    }

    //--------

    private void displayDetails() {
        displayDetails(0);
    }

    private void displayDetails(int prefillDateIndex) {
        displayBasicDetails();
        /*displayStockDetails(false);*/
        displayAvailabilityDetails(prefillDateIndex);
        displayAttributes();
        displayLogs();
        switchToContentPage();
    }

    public void displayBasicDetails(){
        final MeatItemDetails meatItemDetails = warehouseMeatItemDetails.getMeatItemDetais();

        setToolbarTitle(meatItemDetails.getNameWithCategoryAndSize());

        Picasso.get()
                .load(meatItemDetails.getImageURL())
                .placeholder(R.drawable.image_default_mastaan)
                .error(R.drawable.image_default_mastaan)
                .fit()
                .centerCrop()
                .tag(context)
                .into(item_image);

        minimum_weight.setHint("Minimum weight (in" + CommonMethods.capitalizeFirstLetter(meatItemDetails.getQuantityUnit()) + "s)");
        minimum_weight.setText(CommonMethods.getInDecimalFormat(meatItemDetails.getMinimumWeight()));
        availability_start_time.setText(DateMethods.getDateInFormat(warehouseMeatItemDetails.getAvailabilityStartTime(), DateConstants.HH_MM_A));
        availability_end_time.setText(DateMethods.getDateInFormat(warehouseMeatItemDetails.getAvailabilityEndTime(), DateConstants.HH_MM_A));
        preparation_time.setText(CommonMethods.getInDecimalFormat((meatItemDetails.getPreparationHours() * 60))+"");

        if ( warehouseMeatItemDetails.isEnabled() ){
            yesEnabled.setChecked(true);
        }else{
            notEnabled.setChecked(true);
        }
        if ( warehouseMeatItemDetails.isAvailable() ){
            yesAvailable.setChecked(true);
        }else{
            notAvailable.setChecked(true);
        }

        if ( meatItemDetails.isPreOrderable() ){
            yesPreOrder.setChecked(true);
            preorder_cutoff_time.setVisibility(View.VISIBLE);
            clearPreOrderCutoffTime.setVisibility(View.VISIBLE);
            preorder_cutoff_time.setText(DateMethods.getDateInFormat(meatItemDetails.getPreOrderCutoffTime(), DateConstants.HH_MM_A));
        }else{
            notPreOrder.setChecked(true);
            preorder_cutoff_time.setVisibility(View.GONE);
            clearPreOrderCutoffTime.setVisibility(View.GONE);
            preorder_cutoff_time.setText("");
        }

        if ( meatItemDetails.isAddon() ){
            yesAddon.setChecked(true);
        }else{
            notAddon.setChecked(true);
        }

        //-----

        try{
            String sizeString = meatItemDetails.getSize();
            if ( sizeString != null && sizeString.trim().length() > 0 ){
                String[] sizeSplit = sizeString.replaceAll("-", " ").split("\\s+");
                size.setChecked(true);
                sizeView.setVisibility(View.VISIBLE);
                minimum_size.setText(sizeSplit[0]);
                maximum_size.setText(sizeSplit[1]);
                size_unit.setText(sizeSplit[2]);
            }else{  sizeView.setVisibility(View.GONE);  }
        }catch (Exception e){}

        highlight_text.setText(warehouseMeatItemDetails.getHighlightText());
        if ( warehouseMeatItemDetails.getSource() != null && warehouseMeatItemDetails.getSource().trim().length() > 0 ){
            String []sourceSplit = CommonMethods.getStringArrayFromString(warehouseMeatItemDetails.getSource(), ",");
            if ( sourceSplit.length >= 1 ){
                source_name.setText(sourceSplit[0].trim());
            }
            if ( sourceSplit.length >= 2 ){
                source_area.setText(sourceSplit[1].trim());
            }
        }

        index.setText(CommonMethods.getInDecimalFormat(warehouseMeatItemDetails.getIndex()));

    }

    public void displayAvailabilityDetails(int prefillDateIndex){

        //unavailabilityReasonsAdapter = new AvailabilitiesAdapter(activity, fullAvailabilitiesList);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        unavailabilityReasonsAdapter = new ListArrayAdapter(context, R.layout.view_list_item, itemNotAvailableReasonsList);
        unavailabilityReasonsAdapter.setPopupLayoutView(R.layout.view_list_item_dropdown_item);
        unavailable_reasons_spinner.setAdapter(unavailabilityReasonsAdapter);

        fullAvailabilitiesList = warehouseMeatItemDetails.getFullAvlabiltiesList(localStorageData.getServerTime(), 7);
        dateSpinnerAdapter = new AvailabilitiesAdapter(activity, fullAvailabilitiesList);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        dateSpinnerAdapter.setDropDownViewResource(R.layout.view_list_item_dropdown_item);
        date_spinner.setAdapter(dateSpinnerAdapter);

        if ( fullAvailabilitiesList.size() > prefillDateIndex ) {
            date_spinner.setSelection(prefillDateIndex);
            displayAvailabilityDetailsForDate(fullAvailabilitiesList.get(prefillDateIndex));
        }else{
            date_spinner.setSelection(0);
            displayAvailabilityDetailsForDate(fullAvailabilitiesList.get(0));
        }
    }

    public void displayAttributes(){
        final MeatItemDetails meatItemDetails = warehouseMeatItemDetails.getMeatItemDetais();

        attributesHolder.setupVerticalOrientation();
        attributesAdapter = new EditAttributesAdapter(activity, meatItemDetails.getAttributes(), new EditAttributesAdapter.Callback() {
            @Override
            public void onAttributeUpdateClick(final int position) {
                new AttributesFlow(activity).updateAttributeDetails(attributesAdapter.getItem(position), new AttributesFlow.UpdateAttributeCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, AttributeDetails updateAttributeDetails) {
                        if ( status ){
                            attributesAdapter.updateItem(position, updateAttributeDetails);
                            displayDaywiseChangeAttributesOptions(fullAvailabilitiesList.get(date_spinner.getSelectedItemPosition()).getDate());
                        }
                    }
                });
            }

            @Override
            public void onOptionMenuClick(final int attributePosition, final int optionPosition, View view) {
                PopupMenu popup = new PopupMenu(getSupportActionBar().getThemedContext(), view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_attribute_options, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if ( item.getItemId() == R.id.action_update_details ){

                        }

                        else if (item.getItemId() == R.id.action_update_availability) {
                            AttributeOptionDetails attributeOptionDetails = meatItemDetails.getAttributes().get(attributePosition).getAvailableOptions().get(optionPosition);
                            new AttributesFlow(activity).updateAttributeOptionAvailability(attributeOptionDetails, new AttributesFlow.UpdateAttributeOptionCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message, AttributeOptionDetails updateAttributeOptionDetails) {
                                    attributesAdapter.updateAttrbuteOptionDetails(attributePosition, optionPosition, updateAttributeOptionDetails);
                                    displayDaywiseChangeAttributesOptions(fullAvailabilitiesList.get(date_spinner.getSelectedItemPosition()).getDate());
                                }
                            });
                        }

                        return true;
                    }
                });
            }
        });
        attributesHolder.setAdapter(attributesAdapter);
    }

    public void displayDaywiseChangeAttributesOptions(String date){

        optionsHolder.removeAllViews();
        optionsViews = new ArrayList<>();

        final MeatItemDetails meatItemDetails = warehouseMeatItemDetails.getMeatItemDetais();
        List<AttributeOptionDetails> daywiseChangeOptions = new ArrayList<>();
        for (int i=0;i<meatItemDetails.getAttributes().size();i++){
            if ( meatItemDetails.getAttributes().get(i).isAvailabilityChangeDaywise() ) {
                for (int j = 0; j < meatItemDetails.getAttributes().get(i).getAvailableOptions().size(); j++) {
                    daywiseChangeOptions.add(meatItemDetails.getAttributes().get(i).getAvailableOptions().get(j));
                }
            }
        }

        for (int i=0;i<daywiseChangeOptions.size();i++){
            AttributeOptionDetails attributeOptionDetails = daywiseChangeOptions.get(i).getOptionDetailsForDate(date);
            final View itemView = LayoutInflater.from(context).inflate(R.layout.view_update_attribute_option_availability, null);
            optionsHolder.addView(itemView);
            optionsViews.add(itemView);

            TextView option_id = itemView.findViewById(R.id.option_id);
            TextView option_name = itemView.findViewById(R.id.option_name);
            final vTextInputLayout price_difference = itemView.findViewById(R.id.price_difference);
            final RadioButton yesAvailable = itemView.findViewById(R.id.yesAvailable);
            final RadioButton notAvailable = itemView.findViewById(R.id.notAvailable);
            final vTextInputLayout unavilable_reason = itemView.findViewById(R.id.unavilable_reason);

            option_id.setText(attributeOptionDetails.getID());
            option_name.setText(CommonMethods.capitalizeFirstLetter(attributeOptionDetails.getName()));
            price_difference.setText(CommonMethods.getInDecimalFormat(attributeOptionDetails.getPriceDifference()));
            if ( attributeOptionDetails.isEnabled() ){
                yesAvailable.setChecked(true);
            }else{
                notAvailable.setChecked(true);
                unavilable_reason.setText(attributeOptionDetails.getAvailabilityReason());
                unavilable_reason.setVisibility(View.VISIBLE);
            }

            yesAvailable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if ( isChecked ){
                        unavilable_reason.setText("");
                        unavilable_reason.setVisibility(View.GONE);
                    }else{
                        unavilable_reason.setText("Item not available");
                        unavilable_reason.setVisibility(View.VISIBLE);
                        availabilityDetailsScrollView.focusToViewBottom(itemView);
                    }
                }
            });
        }
    }

    public List<RequestUpdateOptionAvailability2> getDaywiseChangeAttributeOptionsFilledRequests(){
        if ( optionsViews != null && optionsViews.size() > 0 ){
            List<RequestUpdateOptionAvailability2> requestsOptionsAvailability = new ArrayList<>();

            for (int i=0;i<optionsViews.size();i++){
                View optionView = optionsViews.get(i);
                TextView option_id = optionView.findViewById(R.id.option_id);
                vTextInputLayout price_difference = optionView.findViewById(R.id.price_difference);
                RadioButton yesAvailable = optionView.findViewById(R.id.yesAvailable);
                vTextInputLayout unavilable_reason = optionView.findViewById(R.id.unavilable_reason);

                String optionID = option_id.getText().toString();
                String priceDifference = price_difference.getText().trim();
                price_difference.checkError("* Enter price difference");
                boolean isAvailable = false;
                String availabilityReason = "";
                if (yesAvailable.isChecked()) {
                    isAvailable = true;
                }else{
                    availabilityReason = unavilable_reason.getText().toString();
                    unavilable_reason.checkError("* Enter unavailable reason");
                }

                if ( optionID.length() > 0 && priceDifference.length() > 0 && (isAvailable || availabilityReason.length() > 0 ) ){
                    requestsOptionsAvailability.add(new RequestUpdateOptionAvailability2(optionID, Double.parseDouble(priceDifference), isAvailable, availabilityReason));
                }else{
                    showToastMessage("* Fill all necessary details");
                    availabilityDetailsScrollView.focusToViewBottom(optionView);         // FOCUSING VIEW
                    return null;
                }
            }

            return requestsOptionsAvailability;
        }
        return new ArrayList<>();
    }


    public void displayAvailabilityDetailsForDate(AvailabilityDetails availabilityDetails){

        vendor_price.setText(CommonMethods.getInDecimalFormat(availabilityDetails.getBuyingPrice()));
        buyer_price.setText(CommonMethods.getInDecimalFormat(availabilityDetails.getActualSellingPrice()));
        discount.setText(CommonMethods.getInDecimalFormat(availabilityDetails.getActualSellingPrice()-availabilityDetails.getSellingPrice()));

        if ( availabilityDetails.isAvailable() ){
            yesAvailableOnDate.setChecked(true);
            unavailable_reason.setText("");
            unavailableReasonsHolder.setVisibility(View.GONE);
        }else{
            notAvailableOnDate.setChecked(true);
            int selecteUnavailableReasonIndex = -1;
            for (int i=0;i<itemNotAvailableReasonsList.size();i++){
                if ( itemNotAvailableReasonsList.get(i).equalsIgnoreCase(availabilityDetails.getAvailabilityReason())){
                    selecteUnavailableReasonIndex = i;
                    break;
                }
            }
            unavailableReasonsHolder.setVisibility(View.VISIBLE);
            if ( selecteUnavailableReasonIndex != -1 ){
                unavailable_reason.setText("");
                unavailable_reason.setVisibility(View.GONE);
                unavailable_reasons_spinner.setSelection(selecteUnavailableReasonIndex);
            }else{
                unavailable_reason.setText(availabilityDetails.getAvailabilityReason());
                unavailable_reason.setVisibility(View.VISIBLE);
                unavailable_reasons_spinner.setSelection(itemNotAvailableReasonsList.size()-1);
            }
        }

        displayDaywiseChangeAttributesOptions(availabilityDetails.getDate());

    }

    /*public void displayStockDetails(){
        displayStockDetails(true);
    }
    public void displayStockDetails(boolean reloadIfPresent){

        stockLoadingIndicator.setVisibility(View.VISIBLE);
        stockLoadingIndicator.setText("Loading...");
        stockLoadingIndicator.setClickable(false);
        logsRadioGroup.setVisibility(View.GONE);
        stockDetailsHolder.setVisibility(View.GONE);
        StockAPI.WarehouseMeatItemStockCallback callback = new StockAPI.WarehouseMeatItemStockCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemStockDetails stockDetails) {
                if ( status ){
                    stockLoadingIndicator.setVisibility(View.GONE);
                    stockDetailsHolder.setVisibility(View.VISIBLE);

                    if ( stockDetails != null ) {
                        isAnyChanges = true;
                        warehouseMeatItemDetails.setStockDetails(stockDetails);

                        stock_type.setText(stockDetails.getTypeName());
                        if (stockDetails.getTypeName().equalsIgnoreCase(Constants.DAYWISE)) {
                            stockDaysView.setVisibility(View.VISIBLE);
                            sunday.setChecked(stockDetails.getAvailableDays().contains("sun"));
                            monday.setChecked(stockDetails.getAvailableDays().contains("mon"));
                            tuesday.setChecked(stockDetails.getAvailableDays().contains("tue"));
                            wednesday.setChecked(stockDetails.getAvailableDays().contains("wed"));
                            thursday.setChecked(stockDetails.getAvailableDays().contains("thu"));
                            friday.setChecked(stockDetails.getAvailableDays().contains("fri"));
                            saturday.setChecked(stockDetails.getAvailableDays().contains("sat"));
                        } else {
                            stockDaysView.setVisibility(View.GONE);
                        }

                        if ( stockDetails.getTypeName().equalsIgnoreCase(Constants.UNLIMITED) ){
                            stock_threshold_quantity.setVisibility(View.VISIBLE);
                            if ( stockDetails.getThresholdQuantity() > 0 ){ stock_threshold_quantity.setText(CommonMethods.getInDecimalFormat(stockDetails.getThresholdQuantity()));    }
                        }else{  stock_threshold_quantity.setVisibility(View.GONE);  }

                        stock_quantity.setText(CommonMethods.getInDecimalFormat(stockDetails.getQuantity()));
                    }
                    stock_quantity_unit.setText(warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+"(s)");
                    stock_threshold_quantity.setHint("Daily Threshold Quantity (in "+warehouseMeatItemDetails.getMeatItemDetais().getQuantityUnit()+"s)");
                }
                else{
                    logsLoadingIndicator.setClickable(true);
                    logsLoadingIndicator.setText("Tap to load");
                }
            }
        };

        if ( reloadIfPresent == false && warehouseMeatItemDetails.getStockDetails() != null ){
            callback.onComplete(true, 200, "Success", warehouseMeatItemDetails.getStockDetails());
        }else{
            getBackendAPIs().getStockAPI().getWarehouseMeatItemStock(warehouseMeatItemDetails.getID(), new ArrayList<String>(), callback);
        }
    }*/

    public void displayLogs(){

        logsLoadingIndicator.setVisibility(View.VISIBLE);
        logsLoadingIndicator.setText("Loading...");
        logsLoadingIndicator.setClickable(false);
        logsRadioGroup.setVisibility(View.GONE);
        meatItemLogsHolder.setVisibility(View.GONE);
        warehouseMeatItemLogsHolder.setVisibility(View.GONE);
        getBackendAPIs().getMeatItemsAPI().getMeatItemLogs(warehouseMeatItemDetails.getMeatItemID(), new RequestItemLogs().setPopulate("emp"), 20, new MeatItemsAPI.ItemLogsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<ItemLogDetails> meatItemLogs) {
                if ( status ){
                    getBackendAPIs().getMeatItemsAPI().getWarehouseMeatItemLogs(warehouseMeatItemDetails.getID(), new RequestItemLogs().setPopulate("emp"), 20, new MeatItemsAPI.ItemLogsCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<ItemLogDetails> warehouseMeatItemLogs) {
                            if ( status ){
                                logsLoadingIndicator.setVisibility(View.GONE);
                                logsRadioGroup.setVisibility(View.VISIBLE);
                                showMeatItemLogs.setChecked(true);
                                meatItemLogsHolder.setVisibility(View.VISIBLE);
                                warehouseMeatItemLogsHolder.setVisibility(View.GONE);
                                meatItemLogsHolder.setAdapter(new ItemLogsAdapter(activity, meatItemLogs, null));
                                warehouseMeatItemLogsHolder.setAdapter(new ItemLogsAdapter(activity, warehouseMeatItemLogs, null));
                            }
                            else{
                                logsLoadingIndicator.setClickable(true);
                                logsLoadingIndicator.setText("Tap to load");
                            }
                        }
                    });
                }else{
                    logsLoadingIndicator.setClickable(true);
                    logsLoadingIndicator.setText("Tap to load");
                }
            }
        });
    }

    //---------

    public void getMeatItemDetails(){

        showLoadingIndicator("Loading, wait...");
        final MeatItemsAPI.WarehouseMeatItemCallback callback = new MeatItemsAPI.WarehouseMeatItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final WarehouseMeatItemDetails itemDetails) {
                if ( status ){
                    getBackendAPIs().getMeatItemsAPI().getMeatItemAttributes(itemDetails.getMeatItemID(), new MeatItemsAPI.MeatItemAttributesCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<AttributeDetails> attributesList) {
                            if ( status ){
                                itemDetails.getMeatItemDetais().setAttributes(attributesList);
                                warehouseMeatItemDetails = itemDetails;
                                displayDetails();
                            }else{
                                showReloadIndicator(statusCode, "Unable to load "+getIntent().getStringExtra(Constants.NAME).toLowerCase()+"'s details, try again!");
                            }
                        }
                    });
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        };
        new AsyncTask<Void, Void, Void>() {
            WarehouseMeatItemDetails itemDetails = null;
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    String warehouse_meat_item_details = getIntent().getStringExtra(Constants.WAREHOUSE_MEAT_ITEM_DETAILS);
                    itemDetails = new Gson().fromJson(warehouse_meat_item_details, WarehouseMeatItemDetails.class);
                }catch (Exception e){}
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( itemDetails != null ){
                    callback.onComplete(true, 200, "Success", itemDetails);
                }else{
                    getBackendAPIs().getMeatItemsAPI().getWarehouseMeatItemDetails(getIntent().getStringExtra(Constants.ID), callback);
                }
            }
        }.execute();
    }

    public void updateMeatItemDetails(final RequestUpdateMeatItem requestUpdateMeatItem, final RequestUpdateWarehouseMeatItem requestUpdateWarehouseMeatItem){

        hideSoftKeyboard();
        showLoadingDialog("Updating meat item details, wait...");
        getBackendAPIs().getMeatItemsAPI().updateMeatItemDetails(warehouseMeatItemDetails.getMeatItemDetais().getID(), requestUpdateMeatItem, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if ( status ){
                    isAnyChanges = true;

                    warehouseMeatItemDetails.getMeatItemDetais().setPreparationTime(requestUpdateMeatItem.getPreparationTime());
                    warehouseMeatItemDetails.getMeatItemDetais().setMinimumWeight(requestUpdateMeatItem.getMinimumWeight());
                    warehouseMeatItemDetails.getMeatItemDetais().setIsPreOrderable(requestUpdateMeatItem.isPreOrderable());
                    warehouseMeatItemDetails.getMeatItemDetais().setPreOrderCutoffTime(requestUpdateMeatItem.getPreOrderCutoffTime());
                    warehouseMeatItemDetails.getMeatItemDetais().setIsAddon(requestUpdateMeatItem.isAddon());
                    warehouseMeatItemDetails.getMeatItemDetais().setSize(requestUpdateMeatItem.getSize());

                    getBackendAPIs().getMeatItemsAPI().updateWarehouseMeatItemDetails(warehouseMeatItemDetails.getID(), requestUpdateWarehouseMeatItem, new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            closeLoadingDialog();
                            displayLogs();
                            if ( status ){
                                isAnyChanges = true;
                                warehouseMeatItemDetails.setEnabled(requestUpdateWarehouseMeatItem.isEnabled());
                                warehouseMeatItemDetails.setAvailability(requestUpdateWarehouseMeatItem.isAvailable());
                                warehouseMeatItemDetails.setAvailabilityStartTime(requestUpdateWarehouseMeatItem.getAvailabilityStartTime());
                                warehouseMeatItemDetails.setAvailabilityEndTime(requestUpdateWarehouseMeatItem.getAvailabilityEndTime());
                                warehouseMeatItemDetails.setHighlightText(requestUpdateWarehouseMeatItem.getHighlightText());
                                warehouseMeatItemDetails.setSource(requestUpdateWarehouseMeatItem.getSource());
                                warehouseMeatItemDetails.setIndex(requestUpdateWarehouseMeatItem.getIndex());
                                showToastMessage("Meat item details updated successfully.");
                            }else{
                                showToastMessage("Unable to update meat item details, try again!");
                            }
                        }
                    });
                }else{
                    closeLoadingDialog();
                    showToastMessage("Unable to update meat item details, try again!");
                }
            }
        });
    }

    public void updateMeatItemAvailabilityDetails(final RequestUpdateMeatItemAvaialability requestObject){

        hideSoftKeyboard();
        showLoadingDialog("Updating meat item availability details, wait...");
        getBackendAPIs().getMeatItemsAPI().updateMeatItemAvailabilityDetails(warehouseMeatItemDetails.getID(), requestObject, new MeatItemsAPI.UpdateMeatItemAvailabilityCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, double latestBuyingPrice, double latestSellingPrice, List<AvailabilityDetails> availabilitesList, List<AttributeDetails> attributesList) {
                closeLoadingDialog();
                if ( status ){
                    isAnyChanges = true;
                    //dateSpinnerAdapter.updateItem(date_spinner.getSelectedItemPosition(), new AvailabilityDetails("id", requestObject.getDate(), requestObject.isAvailable(), requestObject.getAvailabilityReason(), requestObject.getVendorPrice(), requestObject.getBuyerPrice()));
                    warehouseMeatItemDetails.setBuyingPrice(latestBuyingPrice);
                    warehouseMeatItemDetails.setSellingPrice(latestSellingPrice);
                    //warehouseMeatItemDetails.setAvlabiltiesList(dateSpinnerAdapter.getAllItems());
                    warehouseMeatItemDetails.setAvlabiltiesList(availabilitesList);
                    warehouseMeatItemDetails.getMeatItemDetais().setAttributes(attributesList);

                    displayDetails(date_spinner.getSelectedItemPosition());

                    showNotificationDialog("Sucess!", "Item availability details updated successfully");

                }else{
                    showSnackbarMessage("Unable to update meat item availability details, please try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            updateMeatItemAvailabilityDetails(requestObject);
                        }
                    });
                }
            }
        });
    }

    /*public void updateStockDetails(final RequestUpdateWarehouseMeatItemStock requestUpdateWarehouseMeatItemStock){

        showLoadingDialog("Updating stock details details, wait...");
        getBackendAPIs().getStockAPI().updateWarehouseMeatItemStock(warehouseMeatItemDetails.getID(), requestUpdateWarehouseMeatItemStock, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    isAnyChanges = true;
                    warehouseMeatItemDetails.getStockDetails().setType(requestUpdateWarehouseMeatItemStock.getType());
                    warehouseMeatItemDetails.getStockDetails().setAvailableDays(requestUpdateWarehouseMeatItemStock.getAvailableDays());
                    warehouseMeatItemDetails.getStockDetails().setThresholdQuantity(requestUpdateWarehouseMeatItemStock.getThresholdQuantity());

                    displayLogs();
                    showToastMessage("Stock details updated successfully");
                }
                else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }*/

    //==================

    public void hideSoftKeyboard() {
        inputMethodManager.hideSoftInputFromWindow(minimum_weight.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(preparation_time.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(vendor_price.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(buyer_price.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        inputMethodManager.hideSoftInputFromWindow(unavailable_reason.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if ( isAnyChanges ) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra(Constants.WAREHOUSE_MEAT_ITEM_DETAILS, new Gson().toJson(warehouseMeatItemDetails));
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }else{
            super.onBackPressed();
        }
    }
}
