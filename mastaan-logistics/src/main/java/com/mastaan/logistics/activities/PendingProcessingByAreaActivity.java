package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.DeliveryZonesAdapter;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.DeliveryZoneDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 13/08/17.
 */

public class PendingProcessingByAreaActivity extends MastaanToolbarActivity {

    String searchQuery;
    MenuItem searchViewMenuITem;
    SearchView searchView;

    vRecyclerView itemsHolder;

    DeliveryZonesAdapter deliveryZonesAdapter;

    List<DeliveryZoneDetails> originalItemsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_processing_by_area);
        hasLoadingView();
        hasSwipeRefresh();

        //-----------------

        itemsHolder = (vRecyclerView) findViewById(R.id.itemsHolder);
        itemsHolder.setupVerticalOrientation();
        deliveryZonesAdapter = new DeliveryZonesAdapter(activity, new ArrayList<DeliveryZoneDetails>(), new DeliveryZonesAdapter.CallBack() {
            @Override
            public void onItemClick(final int position, final int areaPosition) {
                Intent pendingProcessingAct = new Intent(context, PendingProcessingActivity.class);
                pendingProcessingAct.putExtra(Constants.DELIVERY_AREA_ID, deliveryZonesAdapter.getItem(position).getAreas().get(areaPosition).getID());
                startActivity(pendingProcessingAct);
            }
        });
        itemsHolder.setAdapter(deliveryZonesAdapter);

       //-----

        //LOADING AREAS AFTER MENU CREATED
    }

    @Override
    public void onAfterMenuCreated() {
        super.onAfterMenuCreated();

        getPendingAreasList();
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPendingAreasList();
    }

    public void getPendingAreasList(){

        searchViewMenuITem.setVisible(false);
        showLoadingIndicator("Loading pending processing items, wait..");
        getBackendAPIs().getOrdersAPI().getPendingProcessingZones(new OrdersAPI.DeliveryZonesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<DeliveryZoneDetails> areasList) {
                if (status == true) {
                    originalItemsList = areasList;
                    if ( areasList.size() > 0 ){
                        searchViewMenuITem.setVisible(true);
                        deliveryZonesAdapter.setItems(areasList);
                        itemsHolder.scrollToPosition(0);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("No pending processing items");
                    }
                } else {
                    showReloadIndicator(statusCode, "Unable to load pending processing items, try again!");
                }
            }
        });
    }

    //--------------

    public void onPerformSearch(final String search_query){
        if ( search_query != null && search_query.length() > 0 ){
            this.searchQuery = search_query;
            showLoadingIndicator("Searching for '"+search_query+"', wait...");
            new AsyncTask<Void, Void, Void>() {
                List<DeliveryZoneDetails> filteredList = new ArrayList<>();
                @Override
                protected Void doInBackground(Void... voids) {
                    for (int i=0;i<originalItemsList.size();i++){
                        if ( originalItemsList.get(i).getID().toLowerCase().contains(searchQuery.toLowerCase()) ){
                            filteredList.add(originalItemsList.get(i));
                        }else{
                            for (int j=0;j<originalItemsList.get(i).getAreas().size();j++){
                                if ( originalItemsList.get(i).getAreas().get(j).getID().toLowerCase().contains(searchQuery.toLowerCase()) ){
                                    filteredList.add(originalItemsList.get(i));
                                    break;
                                }
                            }
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( searchQuery.equalsIgnoreCase(search_query) ){
                        if ( filteredList.size() > 0 ){
                            deliveryZonesAdapter.setItems(filteredList);
                            itemsHolder.scrollToPosition(0);
                            switchToContentPage();
                        }else{
                            showNoDataIndicator("Nothing found for '"+searchQuery+"'");
                        }
                    }
                }
            }.execute();
        }else{
            onClearSearch();
        }
    }

    public void onClearSearch(){
        if ( originalItemsList.size() > 0 ){
            searchViewMenuITem.setVisible(true);
            deliveryZonesAdapter.setItems(originalItemsList);
            itemsHolder.scrollToPosition(0);
            switchToContentPage();
        }else{
            showNoDataIndicator("No pending processing items");
        }
    }

    //===============

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_assign_deliveries, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(searchViewMenuITem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onClearSearch();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onPerformSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        onAfterMenuCreated();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if ( menuItem.getItemId() == R.id.action_search ){

        }else {
            onBackPressed();
        }
        return true;
    }

}
