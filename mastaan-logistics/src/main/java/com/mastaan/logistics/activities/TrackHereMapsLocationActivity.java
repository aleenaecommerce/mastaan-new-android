package com.mastaan.logistics.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.PlaceDetails;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.UnintializedMapEngineException;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.GeocodeRequest;
import com.here.android.mpa.search.GeocodeResult;
import com.here.android.mpa.search.ResultListener;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.handlers.GpsTracker;
import com.mastaan.logistics.models.Job;

import java.io.Serializable;
import java.util.List;


public class TrackHereMapsLocationActivity extends MastaanToolbarActivity implements View.OnClickListener {

    String userID;
    //GoogleMap googleMap;
    // Marker mapMarker;
    boolean draggingMap;
    boolean initiallyZoomed;
    View locationDetailsHolder;
    TextView time;
    TextView place_name;
    TextView place_info;
    TextView job_details;
    Firebase firebaseLocationTracking;
    LocationDetails lastPlaceDetailFetchedLocation;
    PlaceDetails lastFetchedPlaceDetails;
    LocationDetails lastKnowLocation;
    Job job;
    double longtitude;
    double latitude;
    private GpsTracker gpsTracker;
    private Map m_map;
    private MapMarker marker;
    private AndroidXMapFragment m_mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_heremaps_track_location);

        userID = getIntent().getStringExtra(Constants.USER_ID);
        setToolbarTitle(getIntent().getStringExtra(Constants.USER_NAME));
        job = new Gson().fromJson(getIntent().getStringExtra(Constants.JOB_DETAILS), Job.class);

        gpsTracker = new GpsTracker(getApplicationContext());
        latitude = gpsTracker.getLatitude();
        longtitude = gpsTracker.getLongitude();
        //------

        locationDetailsHolder = findViewById(R.id.locationDetailsHolder);
        locationDetailsHolder.setOnClickListener(this);
        time = findViewById(R.id.time);
        place_name = findViewById(R.id.place_name);
        place_info = findViewById(R.id.place_info);
        job_details = findViewById(R.id.job_details);

        initMapFragment();
//
//        vMapFragment map_fragment = ((vMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment));
//        map_fragment.setOnDragListener(new vMapWrapperLayout.OnDragListener() {
//            @Override
//            public void onDrag(MotionEvent motionEvent) {
//                draggingMap = motionEvent.getAction() == MotionEvent.ACTION_MOVE;
//            }
//        });
//        map_fragment.getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(GoogleMap google_Map) {
//                //googleMap = map_fragment.getMap();
//                googleMap = google_Map;
//
        firebaseLocationTracking = new Firebase(getString(R.string.firebase_location_tracking_url) + "/" + userID);
        getLastKnownLocation();
        if (job != null) {
            displayJobRoute(job);
        }
//            }
//        });

        //----

        if (job != null) {
            job_details.setVisibility(View.VISIBLE);
            job_details.setText(Html.fromHtml(job.getFormattedJobDetailsString().replaceAll("<br><br>", "<br>")));
        } else {
            job_details.setVisibility(View.GONE);
        }

    }

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }

    @Override
    public void onClick(View view) {
        if (view == locationDetailsHolder) {
            displayLocation(lastKnowLocation, true);
        }
    }

    private void initMapFragment() {
        /* Locate the mapFragment UI element */
        m_mapFragment = getMapFragment();

        firebaseLocationTracking = new Firebase(getString(R.string.firebase_location_tracking_url) + "/" + userID);

        if (job != null) {
            displayJobRoute(job);
        }
        if (m_mapFragment != null) {
            /* Initialize the AndroidXMapFragment, results will be given via the called back. */
            m_mapFragment.init(new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {

                    if (error == Error.NONE) {
                        /* get the map object */
                        m_map = m_mapFragment.getMap();

                        m_mapFragment.getPositionIndicator().setVisible(true);


                        /*
                         * Set the map center to the south of Berlin.
                         */


                        if (gpsTracker.canGetLocation()) {

                            // place_info.setText("Lat :" + latitude + "Lng :" + longitude);
                            Log.e("location", "" + latitude + longtitude);
                        } else {
                            gpsTracker.showSettingsAlert();
                        }
                        getLastKnownLocation();

                        //  createRoute(Collections.<RoutingZone>emptyList());

                    } else {
                        new AlertDialog.Builder(TrackHereMapsLocationActivity.this).setMessage(
                                "Error : " + error.name() + "\n\n" + error.getDetails())
                                .setTitle("error")
                                .setNegativeButton(android.R.string.cancel,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                finish();
                                            }
                                        }).create().show();
                    }
                }
            });
        }
    }

    public void getLastKnownLocation() {

        firebaseLocationTracking.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                    //Log.d(Constants.LOG_TAG, "LKL =======> "+dataSnapshot.getAmount());
                    LocationDetails locationDetails = new LocationDetails(dataSnapshot.child("time").getValue(Long.class), dataSnapshot.child("lat").getValue(Double.class), dataSnapshot.child("lng").getValue(Double.class));
                    displayLocation(locationDetails);
                } else {
                    showToastMessage("No Location information found");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                showToastMessage("Cancelled");
            }
        });
    }

    public void displayLocation(final LocationDetails locationDetails) {
        displayLocation(locationDetails, false);
    }

    public void displayLocation(final LocationDetails locationDetails, final boolean zoomToLocation) {
        try {

            if (locationDetails != null && locationDetails.lat != 0 && locationDetails.lng != 0) {
            marker = new MapMarker();

            if (marker != null) {
                //marker.remove();
            }


//            if (gpsTracker.canGetLocation()) {
//
//                // place_info.setText("Lat :" + latitude + "Lng :" + longitude);
//                Log.e("location", "" + latitude + longitude);
//            } else {
//                gpsTracker.showSettingsAlert();
//            }



                GeoCoordinate tracy = new GeoCoordinate(17.440081, -78.348915);

                GeocodeRequest request = new GeocodeRequest("hyderabad").setSearchArea(tracy, 100);
                request.execute(new ResultListener<List<GeocodeResult>>() {
                    @Override
                    public void onCompleted(List<GeocodeResult> results, ErrorCode error) {
                        if (error != ErrorCode.NONE) {
                            Log.e("HERE", error.toString());
                        } else {
                            try {
                                for (GeocodeResult result : results) {
                                    Image image = new Image();
                                    image.setImageResource(R.mipmap.ic_launcher);
                                    marker.setCoordinate(new GeoCoordinate(result.getLocation().getCoordinate().getLatitude(), result.getLocation().getCoordinate().getLongitude(), 0.0));

                                    Log.e("lat", "" + result.getLocation().getCoordinate().getLatitude());
//                                            latitude = result.getLocation().getCoordinate().getLatitude();
//                                            longtitude = result.getLocation().getCoordinate().getLongitude();


                                    // place_name.setText("" + result.getLocation().getAddress());
                                    // mapMarker.setSnippet("" + result.getLocation().getCoordinate());

                                    /* Trigger the route calculation,results will be called back via the listener */
                                    // place_name.setText("(Lat: " + result.getLocation().getCoordinate().getLatitude() + ", Lng: " + result.getLocation().getCoordinate().getLongitude() + ")");
                                    // time.setText("@ " + DateMethods.getDateInFormat(DateMethods.getDateFromTimeStamp(result.getLocation().getTimeZone()), DateConstants.MMM_DD_YYYY_HH_MM_A));




                                    /* Initialize a CoreRouter */
                                    CoreRouter coreRouter = new CoreRouter();

                                    /* Initialize a RoutePlan */
                                    RoutePlan routePlan = new RoutePlan();

                                    /*
                                     * Initialize a RouteOption. HERE Mobile SDK allow users to define their own parameters for the
                                     * route calculation,including transport modes,route types and route restrictions etc.Please
                                     * refer to API doc for full list of APIs
                                     */
                                    RouteOptions routeOptions = new RouteOptions();
                                    /* Other transport modes are also available e.g Pedestrian */
                                    routeOptions.setTransportMode(RouteOptions.TransportMode.PUBLIC_TRANSPORT);
                                    /* Disable highway in this route. */
                                    routeOptions.setHighwaysAllowed(false);
                                    /* Calculate the shortest route available. */
                                    routeOptions.setRouteType(RouteOptions.Type.SHORTEST);


                                    /* Calculate 1 route. */
                                    routeOptions.setRouteCount(1);
                                    /* Exclude routing zones. */

                                    /* Finally set the route option */
                                    routePlan.setRouteOptions(routeOptions);

                                    /* Define waypoints for the route */
                                    /* START: South of Berlin */
                                    RouteWaypoint startPoint = new RouteWaypoint(new GeoCoordinate(result.getLocation().getCoordinate().getLatitude(), result.getLocation().getCoordinate().getLongitude()));
                                    /* END: North of Berlin */
                                    RouteWaypoint destination = new RouteWaypoint(new GeoCoordinate(locationDetails.lat, locationDetails.lng));

                                    /* Add both waypoints to the route plan */
                                    routePlan.addWaypoint(startPoint);
                                    routePlan.addWaypoint(destination);


                                    m_map.addMapObject(marker);
                                    m_map.setCenter(new GeoCoordinate(locationDetails.lat, locationDetails.lng, 0.0), Map.Animation.NONE);


                                    /* Set the33e zoom level to the average between min and max zoom level. */
                                    m_map.setZoomLevel(17);

//
                                    m_map.setLandmarksVisible(true);
                                    m_map.getPositionIndicator();
                                }

                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    }
                });


//            marker.setCoordinate(new GeoCoordinate(locationDetails.lat, locationDetails.lng, 0.0));
//            m_map.addMapObject(marker);

            // mapMarker = m_map.addMapObjects(new GeoCoordinate( locationDetails.lat, locationDetails.lng,0.0));
            lastKnowLocation = locationDetails;

            if (draggingMap == false) {
                CameraPosition.Builder cameraBuilder = new CameraPosition.Builder()
                        .target(new LatLng(locationDetails.lat, locationDetails.lng))      // Sets the center of the google_map to Mountain View
                        //.zoom(m_map.getCameraPosition().zoom)
                        .bearing(0)                         // Sets the orientation of the camera to east
                        .tilt(0);                            // Sets the tilt of the camera to 30 degrees
                if (initiallyZoomed == false || zoomToLocation) {
                    cameraBuilder.zoom(17);                 // Sets the zoom
                    initiallyZoomed = true;
                }
                //  m_map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraBuilder.build()));
            }

            place_info.setText("(Lat: " + locationDetails.lng + ", Lng: " + locationDetails.lat + ")");
            // time.setText("@ " + DateMethods.getDateInFormat(DateMethods.getDateFromTimeStamp(locationDetails.time), DateConstants.MMM_DD_YYYY_HH_MM_A));

            // If Difference between last place details fetched location and current location > 100m then loading place details
            if (lastPlaceDetailFetchedLocation == null
                    || Math.abs(LatLngMethods.getDistanceBetweenLatLng(new LatLng(locationDetails.lat, locationDetails.lng), new LatLng(locationDetails.lat, locationDetails.lng))) > 100) {
                // To Prevent Loading while request is going on
                if (place_name.getText().toString().toLowerCase().contains("fetching") == false) {
                    place_name.setText("Fetching place details, wait...");
                    getGooglePlacesAPI().getPlaceDetailsByLatLng(new LatLng(locationDetails.lat, locationDetails.lng), new GooglePlacesAPI.PlaceDetailsCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails) {
                            if (status) {
                                lastPlaceDetailFetchedLocation = locationDetails;
                                lastFetchedPlaceDetails = placeDetails;
                                place_name.setText(placeDetails.getFullName());
                                // marker.setTitle(placeDetails.getFullName());
                            } else {
                                place_name.setText("Tap to fetch place name");
                            }
                        }
                    });
                }
            } else {
                if (lastFetchedPlaceDetails != null) {
                    place_name.setText(lastFetchedPlaceDetails.getFullName());
                    //  marker.setTitle(lastFetchedPlaceDetails.getFullName());
                } else {
                    place_name.setText("Tap to fetch place name");
                }
            }
        }
        } catch (UnintializedMapEngineException e) {
            e.printStackTrace();
        }
    }

    public void displayJobRoute(Job jobDetails) {

        if (jobDetails != null) {
            LatLng startPointLatLng = jobDetails.getStartPointLatLng();
            LatLng endPointLatLng = jobDetails.getEndPointLatLng();

            gpsTracker = new GpsTracker(getApplicationContext());
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            if (gpsTracker.canGetLocation()) {

                //place_info.setText("Lat :" + latitude + "Lng :" + longitude);
                Log.e("location", "" + latitude + longitude);
            } else {
                gpsTracker.showSettingsAlert();
            }
//            m_map.addMarker(new MarkerOptions().position(startPointLatLng).title("Job Start Point").snippet(job.getStartPoint() + "\n(Lat: " + latitude + ", Lng: " + longitude + ")").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).draggable(true));
//            m_map.addMarker(new MarkerOptions().position(endPointLatLng).title("Job End Point").snippet(job.getEndPoint() + "\n(Lat: " + latitude + ", Lng: " + longitude + ")").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).draggable(true));

//            getGooglePlacesAPI().getDirections(startPointLatLng, endPointLatLng, new GooglePlacesAPI.DirectionsCallback() {
//                @Override
//                public void onComplete(List<DirectionsData.Route.Leg> legs, int status_code) {
//                    if (legs != null && legs.size() > 0) {
//                        //showToastMessage(legs.get(0).steps.size()+"=======>"+job.getEndPoint()+" "+job.getEndPointLatLng());
//                        PolylineOptions lineOptions = new PolylineOptions();
//                        for (DirectionsData.Route.Leg.Step step : legs.get(0).steps) {
//                            lineOptions.addAll(step.polyline.getPolyLinePoints());
//                        }
//                        lineOptions.color(Color.rgb(127, 0, 255));
//                        // m_map.addPolyline(lineOptions);
//                    }
//                }
//            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_reload, menu);
        return true;
    }

    //=========

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_reload) {
            getLastKnownLocation();
        } else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    class LocationDetails implements Serializable {
        long time;
        double lat;
        double lng;

        LocationDetails(long time, double lat, double lng) {
            this.time = time;
            this.lat = lat;
            this.lng = lng;
        }
    }

}
