package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.mastaan.logistics.BuildConfig;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.FollowupsAPI;
import com.mastaan.logistics.backend.models.RequestAddFollowup;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AddCounterSaleDialog;
import com.mastaan.logistics.dialogs.FollowupDialog;
import com.mastaan.logistics.flows.PlaceOrderFlow;
import com.mastaan.logistics.flows.WhereToNextSelectionFlow;
import com.mastaan.logistics.fragments.SideBarFragment;
import com.mastaan.logistics.models.BuyerSearchDetails;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.printers.LabelPrinter;

public class HomeActivity extends MastaanNavigationDrawerActivity implements View.OnClickListener, View.OnLongClickListener{

    SideBarFragment sideBarFragment;

    View myDeliveries;
    View pendingDeliveries;
    //View pendingQC;
    View pendingProcessing;
    View newOrders;
    View todayOrders;
    View futureOrders;
    //View stockAlerts;
    View pendingCashNCarry;
    View allAashNCarry;
    View customerSupport;
    View rejectedItems;
    View unprintedBills;
    View pendingFollowups;
    View pendingFirstOrderFollowups;
    View pendingInactiveCartsFollowups;

    FloatingActionMenu floatingMenu;
    View createCounterSale;
    View createOrder;
    View assignDeliveries;
    View createFollowup;


    UserDetails userDetails;

    FollowupDialog followupDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        hasLoadingView();

        userDetails = localStorageData.getUserDetails();

        sideBarFragment = new SideBarFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.left_drawer, sideBarFragment).commit();
        sideBarFragment.setSidebarItemSelectedListener(new SideBarFragment.Callback() {
            @Override
            public void onItemSelected(int position, String item_name) {
                //closeDrawer();
            }
        });

        showToastMessage("Logged in as "+userDetails.getFullName().toUpperCase()+" from "+getDeviceName());

        //-----------

        myDeliveries = findViewById(R.id.myDeliveries);
        myDeliveries.setOnClickListener(this);
        pendingDeliveries = findViewById(R.id.pendingDeliveries);
        pendingDeliveries.setOnClickListener(this);
        newOrders = findViewById(R.id.newOrders);
        newOrders.setOnClickListener(this);
        //pendingQC = findViewById(R.id.pendingQC);
        //pendingQC.setOnClickListener(this);
        pendingProcessing = findViewById(R.id.pendingProcessing);
        pendingProcessing.setOnClickListener(this);
        pendingProcessing.setOnLongClickListener(this);
        todayOrders = findViewById(R.id.todayOrders);
        todayOrders.setOnClickListener(this);
        futureOrders = findViewById(R.id.futureOrders);
        futureOrders.setOnClickListener(this);
        //stockAlerts = findViewById(R.id.stockAlerts);
        //stockAlerts.setOnClickListener(this);
        pendingCashNCarry = findViewById(R.id.pendingCashNCarry);
        pendingCashNCarry.setOnClickListener(this);
        allAashNCarry = findViewById(R.id.allAashNCarry);
        allAashNCarry.setOnClickListener(this);
        customerSupport = findViewById(R.id.customerSupport);
        customerSupport.setOnClickListener(this);
        rejectedItems = findViewById(R.id.rejectedItems);
        rejectedItems.setOnClickListener(this);
        unprintedBills = findViewById(R.id.unprintedBills);
        unprintedBills.setOnClickListener(this);
        pendingFollowups = findViewById(R.id.pendingFollowups);
        pendingFollowups.setOnClickListener(this);
        pendingFirstOrderFollowups = findViewById(R.id.pendingFirstOrderFollowups);
        pendingFirstOrderFollowups.setOnClickListener(this);
        pendingInactiveCartsFollowups = findViewById(R.id.pendingInactiveCartsFollowups);
        pendingInactiveCartsFollowups.setOnClickListener(this);

        floatingMenu = findViewById(R.id.floatingMenu);
        floatingMenu.setClosedOnTouchOutside(true);
        createOrder = findViewById(R.id.createOrder);
        createOrder.setOnClickListener(this);
        createCounterSale = findViewById(R.id.createCounterSale);
        createCounterSale.setOnClickListener(this);
        assignDeliveries = findViewById(R.id.assignDeliveries);
        assignDeliveries.setOnClickListener(this);
        createFollowup = findViewById(R.id.createFollowup);
        createFollowup.setOnClickListener(this);

        //-------

        if ( userDetails.isDeliveryBoy() == false ){
            findViewById(R.id.myDeliveriesView).setVisibility(View.GONE);
        }
        if ( userDetails.isDeliveryBoyManager() == false && userDetails.isProcessingManager() == false && userDetails.isOrdersManager() == false ){
            findViewById(R.id.pendingDeliveriesView).setVisibility(View.GONE);
        }
        if ( userDetails.isDeliveryBoy() == false && userDetails.isDeliveryBoyManager() == false && userDetails.isProcessingManager() == false && userDetails.isOrdersManager() == false ){
            findViewById(R.id.deliveriesView).setVisibility(View.GONE);
        }
        //------
        if ( userDetails.isDeliveryBoyManager() == false && userDetails.isProcessingManager() == false && userDetails.isOrdersManager() == false ) {
            findViewById(R.id.processingView).setVisibility(View.GONE);
            //findViewById(R.id.pendingQCView).setVisibility(View.GONE);
        }
        if ( userDetails.isDeliveryBoy() == false && userDetails.isDeliveryBoyManager() == false && userDetails.isProcessingManager() == false && userDetails.isOrdersManager() == false ) {
            findViewById(R.id.cashNCarryView).setVisibility(View.GONE);
        }
        //-------
        if ( userDetails.isDeliveryBoyManager() == false && userDetails.isProcessingManager() == false && userDetails.isOrdersManager() == false && userDetails.isCustomerRelationshipManager() == false ){
            findViewById(R.id.todaysAndFutureOrderView).setVisibility(View.GONE);
        }
        //if ( userDetails.isStockManager() == false && userDetails.isProcessingManager() == false && userDetails.isOrdersManager() == false ){
        //    findViewById(R.id.stockAlertView).setVisibility(View.GONE);
        //}
        //-----
        if ( userDetails.isProcessingManager() == false && userDetails.isDeliveryBoyManager() == false && userDetails.isCustomerRelationshipManager() == false ){
            findViewById(R.id.unprintedBillsView).setVisibility(View.GONE);
        }
        if ( userDetails.isCustomerRelationshipManager() == false ){
            findViewById(R.id.customerSupportView).setVisibility(View.GONE);
            findViewById(R.id.pendingFollowupsView).setVisibility(View.GONE);
            findViewById(R.id.pendingFirstOrderFollowupsView).setVisibility(View.GONE);
            findViewById(R.id.pendingInactiveCartsFollowupsView).setVisibility(View.GONE);
        }
        if ( userDetails.isCustomerRelationshipManager() == false ){
            findViewById(R.id.rejectedItemsView).setVisibility(View.GONE);
        }
        //-----
        if ( userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() || userDetails.isCustomerRelationshipManager() ){
            floatingMenu.setVisibility(View.VISIBLE);
            if ( userDetails.isProcessingManager() || userDetails.isCustomerRelationshipManager() ){   createOrder.setVisibility(View.VISIBLE); }
            if ( userDetails.isProcessingManager() ){   createCounterSale.setVisibility(View.VISIBLE); }
            if ( userDetails.isDeliveryBoyManager() ){   assignDeliveries.setVisibility(View.VISIBLE);   }
            if ( userDetails.isCustomerRelationshipManager() ){ createFollowup.setVisibility(View.VISIBLE); }
        }else{
            floatingMenu.setVisibility(View.GONE);
        }

        //--------

        futureOrders.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new LabelPrinter(activity).printLabel(new String[]{"<b>Farm Whole Chicken (10-15 Pieces)</b>".toUpperCase(), "<b>With Skin, Small Cut, With Bone</b>", "Weight: 0.75 kgs", "5bfa41eb2358cf431fea2c07-05-1238", "A Product of Mastaan"}, new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        showToastMessage(message);
                    }
                });
                return false;
            }
        });

        //HyperTrack.getOrCreateUser(userDetails.getName(), userDetails.getMobileNumber(), userDetails.getID(), null);

        if ( getIntent().getBooleanExtra(Constants.SHOW_WHERE_TO_NEXT_FLOW, false) ){
            new WhereToNextSelectionFlow(this).start();//showWhereToNextSelectionFlow();
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ( intent.getBooleanExtra(Constants.SHOW_WHERE_TO_NEXT_FLOW, false) ){
            new WhereToNextSelectionFlow(this).start();//showWhereToNextSelectionFlow();
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if ( view == pendingProcessing ){
            if ( view == pendingProcessing){
                Intent pendingProcessingByAreaAct = new Intent(context, PendingProcessingByAreaActivity.class);
                startActivity(pendingProcessingByAreaAct);
            }
        }
        return false;
    }

    @Override
    public void onClick(final View view ) {

        if ( view == myDeliveries) {
            Intent myDeliveriesAct = new Intent(context, MyDeliveriesActivity.class);
            startActivity(myDeliveriesAct);
        }
        else if ( view == pendingDeliveries){
            Intent pendingDeliveriesAct = new Intent(context, PendingDeliveriesActivity.class);
            startActivity(pendingDeliveriesAct);
        }
        /*else if ( view == pendingQC){
            Intent pendingQCAct = new Intent(context, PendingQCActivity.class);
            startActivity(pendingQCAct);
        }*/
        else if ( view == pendingProcessing){
            Intent pendingProcessingAct = new Intent(context, PendingProcessingActivity.class);
            startActivity(pendingProcessingAct);
        }
        else if ( view == newOrders ){
            Intent newOrdersAct = new Intent(context, NewOrdersActivity.class);
            startActivity(newOrdersAct);
        }
        else if ( view == todayOrders){
            Intent todaysOrdersAct = new Intent(context, TodaysOrdersActivity.class);
            startActivity(todaysOrdersAct);
        }
        else if ( view == futureOrders){
            Intent futureOrdersAct = new Intent(context, FutureOrdersActivity.class);
            startActivity(futureOrdersAct);
        }
        //else if ( view == stockAlerts ){
        //    Intent stockAlertsAct = new Intent(context, StockAlertsActivity.class);
        //    startActivity(stockAlertsAct);
        //}
        else if ( view == pendingCashNCarry ){
            Intent pendingCashNCarryAct = new Intent(context, PendingCashNCarryActivity.class);
            startActivity(pendingCashNCarryAct);
        }
        else if ( view == allAashNCarry ){
            Intent cashNCarryAct = new Intent(context, CashNCarryActivity.class);
            startActivity(cashNCarryAct);
        }
        else if ( view == customerSupport ){
            Intent customerSupportAct = new Intent(context, CustomerSupportActivity.class);
            startActivity(customerSupportAct);
        }
        else if ( view == rejectedItems ){
            Intent rejectedItemsAct = new Intent(context, RejectedItemsActivity.class);
            startActivity(rejectedItemsAct);
        }
        else if ( view == unprintedBills ){
            Intent unprintedBillsAct = new Intent(context, UnprintedBillsActivity.class);
            startActivity(unprintedBillsAct);
        }
        else if ( view == pendingFollowups ){
            int pendingFollowupsLastOrderDays = localStorageData.getPendingFollowupsLastOrderDays();

            Intent buyersAct = new Intent(context, BuyersActivity.class);
            buyersAct.putExtra(Constants.TITLE, "NOT ORDERED IN LAST "+pendingFollowupsLastOrderDays+" DAY"+(pendingFollowupsLastOrderDays==1?"":"S"));
            buyersAct.putExtra(Constants.SEARCH_DETAILS, new Gson().toJson(new BuyerSearchDetails()
                    .setNotInterested(false)
                    .setRelocated(false)
                    .setLastNotOrderDays(pendingFollowupsLastOrderDays)
                    .setLastNotFollowupDays(pendingFollowupsLastOrderDays))
            );
            buyersAct.putExtra(Constants.FOR_PENDING_FOLLOWUPS, true);
            startActivity(buyersAct);
        }
        else if ( view == pendingFirstOrderFollowups ){
            Intent ordersByDateAct = new Intent(context, OrdersByDateActivity.class);
            ordersByDateAct.putExtra(Constants.TITLE, "Pending First Order Followups");
            ordersByDateAct.putExtra(Constants.ACTION_TYPE, Constants.PENDING_FIRST_ORDER_FOLLOWUPS);
            ordersByDateAct.putExtra(Constants.HIDE_SUMMARY, true);
            startActivity(ordersByDateAct);
        }
        else if ( view == pendingInactiveCartsFollowups ){
            Intent pendingCartsAct = new Intent(context, CartsActivity.class);
            startActivity(pendingCartsAct);
        }

        else if ( view == createOrder ){
            floatingMenu.close(true);
            if ( userDetails.isSuperUser() || userDetails.isCustomerRelationshipManager() || localStorageData.isUserCheckedIn() ){
                new PlaceOrderFlow(activity).start();
            }else{
                activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
            }
        }
        else if ( view == createCounterSale ){
            floatingMenu.close(true);
            if ( userDetails.isSuperUser() || localStorageData.isUserCheckedIn() ){
                showCounterSaleDialog();
            }else{
                activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
            }
        }
        else if ( view == assignDeliveries ){
            floatingMenu.close(true);
            if ( userDetails.isSuperUser() || localStorageData.isUserCheckedIn() ){
                //Intent assignDeliveriesAct = new Intent(context, AssignDeliveriesActivity.class);
                //startActivity(assignDeliveriesAct);
                Intent assignDeliveriesAct = new Intent(context, AssignDeliveriesByAreaActivity.class);
                startActivity(assignDeliveriesAct);
            }else{
                activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
            }
        }
        else if ( view == createFollowup ){
            floatingMenu.close(true);

            showInputTextDialog("Select Buyer", "Buyer mobile / email", new TextInputCallback() {
                @Override
                public void onComplete(final String inputText) {
                    new CountDownTimer(200, 100) {
                        @Override
                        public void onTick(long millisUntilFinished) {}

                        @Override
                        public void onFinish() {
                            followupDialog = new FollowupDialog(activity).show(new FollowupDialog.Callback() {
                                @Override
                                public void onComplete(final RequestAddFollowup requestObject) {
                                    showLoadingDialog("Adding followup, wait...");
                                    getBackendAPIs().getFollowupsAPI().addFollowup(requestObject, new FollowupsAPI.FollowupDetailsCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message, FollowupDetails followupDetails) {
                                            activity.closeLoadingDialog();
                                            if (status) {
                                                showToastMessage("Followup added successfully");
                                            }
                                            else {
                                                showToastMessage("Something went wrong while adding followup, try again!");
                                            }
                                        }
                                    });
                                }
                            }).initiateForBuyer(inputText.trim());
                        }
                    }.start();
                }
            });
        }
    }

    //------------

    AddCounterSaleDialog addCounterSaleDialog;
    public void showCounterSaleDialog(){
        addCounterSaleDialog = new AddCounterSaleDialog(activity);
        addCounterSaleDialog.show();
    }

    //============

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( addCounterSaleDialog != null ){    addCounterSaleDialog.onActivityResult(requestCode, resultCode, data);   }
        if ( followupDialog != null ){  followupDialog.onActivityResult(requestCode, resultCode, data); }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        if ( localStorageData.getUserDetails().isDeliveryBoy() ){
            menu.findItem(R.id.action_goto).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerListener.onOptionsItemSelected(item)) {
            return true;
        }
        else if ( item.getItemId() == R.id.action_goto ) {
            if (BuildConfig.PRODUCTION == false || localStorageData.isUserCheckedIn() ) {
                new WhereToNextSelectionFlow(this).start();//showWhereToNextSelectionFlow();
            }else{
                activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if ( createCounterSale.getVisibility() == View.VISIBLE || assignDeliveries.getVisibility() == View.VISIBLE ){
            floatingMenu.close(true);
        }else{
            super.onBackPressed();
        }
    }

}
