package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.MessagesAdapter;
import com.mastaan.logistics.backend.ConfigurationsAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.GroupedOrdersList;
import com.mastaan.logistics.models.MeatItemDetails;
import com.mastaan.logistics.models.MessageDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AlertMessagesActivity extends MastaanToolbarActivity implements View.OnClickListener{

    vRecyclerView alertMessagesHolder;
    MessagesAdapter messagesAdapter;

    View actionAdd;

    int selectedItemPosition = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_messages);
        hasLoadingView();
        hasSwipeRefresh();

        //--------

        actionAdd = findViewById(R.id.actionAdd);
        actionAdd.setOnClickListener(this);

        alertMessagesHolder = (vRecyclerView) findViewById(R.id.alertMessagesHolder);
        alertMessagesHolder.setupVerticalOrientation();
        messagesAdapter = new MessagesAdapter(context, new ArrayList<MessageDetails>(), new MessagesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                selectedItemPosition = position;

                Intent updateAlertMessageAct = new Intent(context, AddOrEditAlertMessageActivity.class);
                updateAlertMessageAct.putExtra(Constants.ACTION_TYPE, Constants.UPDATE);
                updateAlertMessageAct.putExtra(Constants.MESSAGE_DETAILS, new Gson().toJson(messagesAdapter.getItem(position)));
                startActivityForResult(updateAlertMessageAct, Constants.ADD_OR_EDIT_ALERT_MESSAGE_ACTIVITY_CODE);
            }

            @Override
            public void onDeleteClick(final int position) {
                showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to delete <b>" + messagesAdapter.getItem(position).getType() + "</b> alert message?"), "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES") ){
                            deleteAlertMessage(position);
                        }
                    }
                });
            }
        });
        alertMessagesHolder.setAdapter(messagesAdapter);

        //--------

        getAlertMessages();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getAlertMessages();
    }

    @Override
    public void onClick(View view) {
        if ( view == actionAdd ){
            final List<String> optionsList = CommonMethods.getStringListFromStringArray(new String[]{"launchmessage", "webslider", "other"});
            for (int i=0;i<messagesAdapter.getItemCount();i++){
                if ( messagesAdapter.getItem(i).getType().equalsIgnoreCase("launchmessage") ){
                    optionsList.remove("launchmessage");
                    break;
                }
            }

            if ( optionsList.size() == 1 && optionsList.get(0).equalsIgnoreCase("other") ){
                Intent addAlertMessageAct = new Intent(context, AddOrEditAlertMessageActivity.class);
                addAlertMessageAct.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                startActivityForResult(addAlertMessageAct, Constants.ADD_OR_EDIT_ALERT_MESSAGE_ACTIVITY_CODE);
            }else {
                showListChooserDialog("Select Type", optionsList, new ListChooserCallback() {
                    @Override
                    public void onSelect(int position) {
                        if ( optionsList.get(position).equalsIgnoreCase("other") ){
                            Intent addAlertMessageAct = new Intent(context, AddOrEditAlertMessageActivity.class);
                            addAlertMessageAct.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                            startActivityForResult(addAlertMessageAct, Constants.ADD_OR_EDIT_ALERT_MESSAGE_ACTIVITY_CODE);
                        }else {
                            Intent addAlertMessageAct = new Intent(context, AddOrEditAlertMessageActivity.class);
                            addAlertMessageAct.putExtra(Constants.ACTION_TYPE, Constants.ADD);
                            addAlertMessageAct.putExtra(Constants.TYPE, optionsList.get(position));
                            startActivityForResult(addAlertMessageAct, Constants.ADD_OR_EDIT_ALERT_MESSAGE_ACTIVITY_CODE);
                        }
                    }
                });
            }
        }
    }

    //-------

    private void getAlertMessages(){

        actionAdd.setVisibility(View.GONE);
        showLoadingIndicator("Loading alert messages, wait...");
        getBackendAPIs().getConfigurationsAPI().getAlertMessages(new ConfigurationsAPI.MessagesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<MessageDetails> messagesList) {
                if ( status ){
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            Collections.sort(messagesList, new Comparator<MessageDetails>() {
                                public int compare(MessageDetails item1, MessageDetails item2) {
                                    return item1.getType().toLowerCase().compareToIgnoreCase(item2.getType().toLowerCase());
                                }
                            });
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);

                            actionAdd.setVisibility(View.VISIBLE);

                            if ( messagesList.size() > 0 ){
                                messagesAdapter.setItems(messagesList);
                                switchToContentPage();
                            }else{
                                showNoDataIndicator("Nothing to show");
                            }
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    private void deleteAlertMessage(final int position){

        showLoadingDialog("Deleting alert message, wait...");
        getBackendAPIs().getConfigurationsAPI().deleteAlertMessage(messagesAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    showToastMessage("Alert message deleted successfully");
                    messagesAdapter.deleteItem(position);
                    if ( messagesAdapter.getItemCount() == 0 ){
                        showNoDataIndicator("Nothing to show");
                    }
                }else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    //==============


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.ADD_OR_EDIT_ALERT_MESSAGE_ACTIVITY_CODE && resultCode == RESULT_OK ){
                String actionType = data.getStringExtra(Constants.ACTION_TYPE);
                MessageDetails messageDetails = new Gson().fromJson(data.getStringExtra(Constants.MESSAGE_DETAILS), MessageDetails.class);
                if ( actionType.equalsIgnoreCase(Constants.UPDATE) ){
                    messagesAdapter.setItem(selectedItemPosition, messageDetails);
                }
                else{
                    messagesAdapter.addItem(alertMessagesHolder.getLastCompletelyVisibleItemPosition()+1, messageDetails);
                    switchToContentPage();
                }
            }
        }catch (Exception e){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
