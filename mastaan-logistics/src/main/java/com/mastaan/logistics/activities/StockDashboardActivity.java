package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.ListChooserCallback;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AddOrEditProcessedStockDialog;
import com.mastaan.logistics.dialogs.AddOrEditStockDialog;
import com.mastaan.logistics.dialogs.LabelPrinterDialog;
import com.mastaan.logistics.interfaces.StockDetailsCallback;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class StockDashboardActivity extends MastaanToolbarActivity implements View.OnClickListener{

    TextView title;
    View actionBack;
    View changeHubOrWarehouse;

    FloatingActionMenu floatingMenu;
    View showCategoriesStockStatement;
    View showMeatItemsStockStatement;
    View showProcessedStockStatement;
    View showStocks;
    View showStockAlerts;
    View printStockLabels;

    View addProcessedStock;
    View addStock;
    View addTransferInStock;
    View addTransferOutStock;
    View addWastageStock;

    HubDetails hubDetails;

    AddOrEditStockDialog addOrEditStockDialog;
    LabelPrinterDialog labelPrinterDialog;

    UserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_dashboard);
        hasLoadingView();
        //hasSwipeRefresh();

        //-----------

        View toolbarTitleView = LayoutInflater.from(context).inflate(R.layout.view_toolbar_stock_dashboard, null);
        toolbar.removeAllViews();
        toolbar.addView(toolbarTitleView);

        title = toolbarTitleView.findViewById(R.id.title);
        actionBack = toolbarTitleView.findViewById(R.id.actionBack);
        actionBack.setOnClickListener(this);
        changeHubOrWarehouse = toolbarTitleView.findViewById(R.id.changeHubOrWarehouse);
        changeHubOrWarehouse.setOnClickListener(this);

        floatingMenu = findViewById(R.id.floatingMenu);
        floatingMenu.setClosedOnTouchOutside(true);

        showCategoriesStockStatement = findViewById(R.id.showCategoriesStockStatement);
        showCategoriesStockStatement.setOnClickListener(this);
        showMeatItemsStockStatement = findViewById(R.id.showMeatItemsStockStatement);
        showMeatItemsStockStatement.setOnClickListener(this);
        showProcessedStockStatement = findViewById(R.id.showProcessedStockStatement);
        showProcessedStockStatement.setOnClickListener(this);
        showStocks = findViewById(R.id.showStocks);
        showStocks.setOnClickListener(this);
        showStockAlerts = findViewById(R.id.showStockAlerts);
        showStockAlerts.setOnClickListener(this);
        printStockLabels = findViewById(R.id.printStockLabels);
        printStockLabels.setOnClickListener(this);

        addProcessedStock = findViewById(R.id.addProcessedStock);
        addProcessedStock.setOnClickListener(this);
        addStock = findViewById(R.id.addStock);
        addStock.setOnClickListener(this);
        addTransferInStock = findViewById(R.id.addTransferInStock);
        addTransferInStock.setOnClickListener(this);
        addTransferOutStock = findViewById(R.id.addTransferOutStock);
        addTransferOutStock.setOnClickListener(this);
        addWastageStock = findViewById(R.id.addWastageStock);
        addWastageStock.setOnClickListener(this);

        //------

        userDetails = localStorageData.getUserDetails();

        title.setText("Warehouse Stock");

        switchToContentPage();

    }


    @Override
    public void onClick(View view) {

        if ( view == changeHubOrWarehouse ){
            showWarehouseOrHubSelection();
        }

        else if ( view == actionBack ){
            onBackPressed();
        }

        else if ( view == showCategoriesStockStatement){
            if ( userDetails.isStockManager() ) {
                Intent stockStatementAct = new Intent(context, StockStatementActivity.class);
                stockStatementAct.putExtra(Constants.ACTION_TYPE, Constants.CATEGORIES_STOCK_STATEMENT);
                if (hubDetails != null) {
                    stockStatementAct.putExtra(Constants.SUB_TITLE, "Hub - " + hubDetails.getName());
                    stockStatementAct.putExtra(Constants.HUB_DETAILS, new Gson().toJson(hubDetails));
                }
                startActivity(stockStatementAct);
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }
        else if ( view == showMeatItemsStockStatement){
            if ( userDetails.isStockManager() ) {
                Intent stockStatementAct = new Intent(context, StockStatementActivity.class);
                stockStatementAct.putExtra(Constants.ACTION_TYPE, Constants.MEAT_ITEMS_STOCK_STATEMENT);
                if (hubDetails != null) {
                    stockStatementAct.putExtra(Constants.SUB_TITLE, "Hub - " + hubDetails.getName());
                    stockStatementAct.putExtra(Constants.HUB_DETAILS, new Gson().toJson(hubDetails));
                }
                startActivity(stockStatementAct);
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }
        else if ( view == showProcessedStockStatement){
            if ( userDetails.isStockManager() ) {
                Intent processedStockStatementAct = new Intent(context, ProcessedStockStatementActivity.class);
                startActivity(processedStockStatementAct);
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }
        else if ( view == showStocks){
            final String []list = new String[]{"All", "Available", "Unavailable"};
            showListChooserDialog("Stocks", list, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    Intent meatItemsAct = new Intent(context, MeatItemsActivity.class);
                    meatItemsAct.putExtra(Constants.FOR_STOCK, true);

                    String title = "Stocks - Warehouse";
                    if ( hubDetails != null ){
                        title = "Stocks - Hub - "+hubDetails.getName();
                        meatItemsAct.putExtra(Constants.HUB_DETAILS, new Gson().toJson(hubDetails));
                    }

                    if ( list[position].equalsIgnoreCase("Available") ){
                        title = "Available "+title;
                        meatItemsAct.putExtra(Constants.AVAILABILITY, "true");
                    }else if ( list[position].equalsIgnoreCase("Unavailable") ){
                        title = "Unavailable "+title;
                        meatItemsAct.putExtra(Constants.AVAILABILITY, "false");
                    }

                    meatItemsAct.putExtra(Constants.TITLE, title);

                    startActivity(meatItemsAct);
                }
            });
        }

        else if ( view == showStockAlerts ){
            if ( userDetails.isStockManager() ) {
                Intent stockAlertsAct = new Intent(context, StockAlertsActivity.class);
                startActivity(stockAlertsAct);
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }

        else if ( view == printStockLabels ){
            if ( userDetails.isStockManager() ) {
                labelPrinterDialog = new LabelPrinterDialog(activity).show();
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }

        else if ( view == addProcessedStock ){
            floatingMenu.close(true);
            if ( userDetails.isStockManager() ) {
                new AddOrEditProcessedStockDialog(activity, null).show();
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }
        else if ( view == addStock ){
            floatingMenu.close(true);
            if ( userDetails.isStockManager() ) {
                addOrEditStockDialog = new AddOrEditStockDialog(activity).redirectToDetailsPageIfApplicable().show(null);
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }
        else if ( view == addTransferInStock ){
            floatingMenu.close(true);
            if ( userDetails.isStockManager() ) {
                addOrEditStockDialog = new AddOrEditStockDialog(activity).setupForHubTransferIn(hubDetails).show(null);
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }
        else if ( view == addTransferOutStock ){
            floatingMenu.close(true);
            if ( userDetails.isStockManager() ) {
                if (hubDetails != null) {  // Hub To Hub
                    addOrEditStockDialog = new AddOrEditStockDialog(activity).setupForHubTransferOut(hubDetails).show(null);
                } else {   // Warehouse to Hub
                    showLoadingDialog("Loading hubs, wait...");
                    getHubsList(new RoutesAPI.HubsCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, final List<HubDetails> hubsList) {
                            closeLoadingDialog();
                            if (status) {
                                final List<String> hubsNames = new ArrayList<>();
                                for (int i = 0; i < hubsList.size(); i++) {
                                    hubsNames.add("Hub - " + hubsList.get(i).getName());
                                }
                                showListChooserDialog("Select To hub", hubsNames, new ListChooserCallback() {
                                    @Override
                                    public void onSelect(int position) {
                                        addOrEditStockDialog = new AddOrEditStockDialog(activity).setupForHubTransferIn(hubsList.get(position)).show(null);
                                    }
                                });
                            } else {
                                showToastMessage("Unable to load hubs, try again!");
                            }
                        }
                    });
                }
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }
        else if ( view == addWastageStock ){
            floatingMenu.close(true);
            if ( userDetails.isStockManager() ) {
                if (hubDetails != null) {
                    addOrEditStockDialog = new AddOrEditStockDialog(activity).setupForHubWastage(hubDetails).show(null);
                } else {
                    addOrEditStockDialog = new AddOrEditStockDialog(activity).setupForMeatItemWastage().show(null);
                }
            }else{
                showToastMessage("You are not allowed to perform this operation, please contact stock manager");
            }
        }

    }


    private void showWarehouseOrHubSelection(){
        showLoadingDialog("Loading hubs, wait...");
        getHubsList(new RoutesAPI.HubsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<HubDetails> hubsList) {
                closeLoadingDialog();
                if ( status ){
                    final List<String> hubsNames = new ArrayList<>();
                    hubsNames.add("Warehouse");
                    for (int i=0;i<hubsList.size();i++){
                        hubsNames.add("Hub - "+hubsList.get(i).getName());
                    }

                    showListChooserDialog("Select", hubsNames, new ListChooserCallback() {
                        @Override
                        public void onSelect(int position) {
                            if ( position == 0 ){
                                toggleViewsVisibility(new View[]{showCategoriesStockStatement, showProcessedStockStatement, addStock, addProcessedStock}, View.VISIBLE);
                                toggleViewsVisibility(new View[]{addTransferInStock/*, addTransferOutStock*/}, View.GONE);

                                hubDetails = null;
                                title.setText("Warehouse Stock");
                            }
                            else{
                                toggleViewsVisibility(new View[]{hubDetails==null?addTransferInStock:null, hubDetails==null?addTransferOutStock:null}, View.VISIBLE);
                                toggleViewsVisibility(new View[]{showCategoriesStockStatement, showProcessedStockStatement, addStock, addProcessedStock}, View.GONE);

                                hubDetails = hubsList.get(position-1);
                                title.setText("Hub - "+hubDetails.getName()+" Stock");
                            }
                        }
                    });
                }
                else{
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    List<HubDetails> hubsList;
    private void getHubsList(final RoutesAPI.HubsCallback hubsCallback){
        if ( hubsList != null ){
            hubsCallback.onComplete(true, 200, "Success", hubsList);
        }
        else{
            showLoadingDialog("Loading hubs, wait...");
            getBackendAPIs().getRoutesAPI().getHubs(new RoutesAPI.HubsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<HubDetails> hubs_list) {
                    hubsCallback.onComplete(status, statusCode, message, hubs_list);
                    if ( status ){
                        hubsList = hubs_list;
                    }
                }
            });
        }
    }

    //=========

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( addOrEditStockDialog != null ){
            addOrEditStockDialog.onActivityResult(requestCode, resultCode, data);
        }
        if ( labelPrinterDialog != null ){
            labelPrinterDialog.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if ( addStock.getVisibility() == View.VISIBLE ){
            floatingMenu.close(true);
        }else{
            super.onBackPressed();
        }
    }

}
