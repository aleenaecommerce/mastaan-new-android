package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.backend.AuthenticationAPI;
import com.mastaan.logistics.backend.CommonAPI;
import com.mastaan.logistics.models.Bootstrap;
import com.mastaan.logistics.services.LogisticsService;


public class LoginActivity extends MastaanToolbarActivity implements View.OnClickListener{

    Intent logisticsService;

    vTextInputLayout mobile_or_email;
    vTextInputLayout password;
    View showPassword, hidePassword;
    Button confirmLogin;
    ProgressBar loading_indicator;
    TextView login_status;

    Bootstrap bootstrap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle(" "+actionBar.getTitle());

        logisticsService = new Intent(context, LogisticsService.class);

        //----------

        mobile_or_email = (vTextInputLayout) findViewById(R.id.mobile_or_email);
        password = (vTextInputLayout) findViewById(R.id.password);
        showPassword = findViewById(R.id.showPassword);
        showPassword.setOnClickListener(this);
        hidePassword = findViewById(R.id.hidePassword);
        hidePassword.setOnClickListener(this);
        confirmLogin = (Button) findViewById(R.id.confirmLogin);
        confirmLogin.setOnClickListener(this);
        loading_indicator = (ProgressBar) findViewById(R.id.loading_indicator);
        login_status = (TextView) findViewById(R.id.login_status);


    }

    @Override
    public void onClick(View view) {

        if ( view == confirmLogin){
            String eMobileOrEmail = mobile_or_email.getText();
            mobile_or_email.checkError("* Enter mobile / email");
            String ePassword = password.getText();
            password.checkError("* Enter Password");

            if ( eMobileOrEmail.length() > 0 && ePassword.length() > 0 ){
                inputMethodManager.hideSoftInputFromWindow(mobile_or_email.getWindowToken(), 0);    // Hides Key Board After Item Select..
                inputMethodManager.hideSoftInputFromWindow(password.getWindowToken(), 0);    // Hides Key Board After Item Select..

                login(eMobileOrEmail, ePassword);
            }
        }
        else if ( view == showPassword ){
            showPassword.setVisibility(View.GONE);
            hidePassword.setVisibility(View.VISIBLE);

            password.getEditText().setInputType(InputType.TYPE_CLASS_TEXT);
            password.getEditText().setSelection(password.getText().length());
        }
        else if ( view == hidePassword ){
            showPassword.setVisibility(View.VISIBLE);
            hidePassword.setVisibility(View.GONE);

            password.getEditText().setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
            password.getEditText().setSelection(password.getText().length());
        }
    }

    private void login(String mobileOrEmail, String password) {
        confirmLogin.setEnabled(false);
        loading_indicator.setVisibility(View.VISIBLE);
        login_status.setText("Logging in, wait...");

        getBackendAPIs().getAuthenticationAPI().login(mobileOrEmail, password, new AuthenticationAPI.LoginCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String token){//}, UserDetails user, ArrayList<Cookies> cookies) {
                if (status) {
                    localStorageData.setSession(token, null);
                    getBootStrap();
                } else {
                    confirmLogin.setEnabled(true);
                    loading_indicator.setVisibility(View.GONE);
                    login_status.setText(message);
                }
            }
        });
    }

    public void getBootStrap() {

        confirmLogin.setEnabled(false);
        loading_indicator.setVisibility(View.VISIBLE);
        login_status.setText("Loading, wait...");

        getBackendAPIs().getCommonAPI().getBootStrap(new CommonAPI.BootstrapCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, Bootstrap boot_strap) {
                if (status) {
                    bootstrap = boot_strap;

                    localStorageData.setServerTime(DateMethods.getReadableDateFromUTC(bootstrap.getServerTime()));

                    localStorageData.setPendingFollowupsLastOrderDays(bootstrap.getConfiguration().getPendingFollowupsLastOrderDays());

                    localStorageData.setUpgradeURL(bootstrap.getUpgradeURL());
                    localStorageData.setUser(bootstrap.getUserDetails());
                    localStorageData.storeWarehouse(bootstrap.getWarehouseDetails());
                    localStorageData.storeWarehousesList(bootstrap.getWarehouses());
                    localStorageData.storeWhereToNextPlaces(bootstrap.getWhereToNextPlaces());
                    localStorageData.storeFailureReasons(bootstrap.getFailureReasons());
                    localStorageData.storeQCFailureReasons(bootstrap.getQcFailureReasons());
                    localStorageData.storeDeliveryFailureReasons(bootstrap.getDeliveryFailureReasons());
                    localStorageData.storeDiscountReasons(bootstrap.getDiscountReasons());
                    localStorageData.storeCustomerSupportReasons(bootstrap.getCustomerSupportReasons());
                    localStorageData.storeFollowupReasons(bootstrap.getFollowupReasons());
                    localStorageData.storeCartFollowupReasons(bootstrap.getCartFollowupReasons());
                    localStorageData.storeItemNotAvailableReasons(bootstrap.getItemNotAvailableReasons());
                    localStorageData.storeInstallSources(bootstrap.getInstallSources());
                    localStorageData.storeCustomerMessages(bootstrap.getCustomerMessages());
                    localStorageData.storeEmployeeViolationsList(bootstrap.getEmployeeViolations());

                    checkLocationAccessAndProceedToHome();
                } else {
                    confirmLogin.setEnabled(true);
                    loading_indicator.setVisibility(View.GONE);
                    login_status.setText("Something went wrong, try again!"+message);
                }
            }
        });
    }

    public void checkLocationAccessAndProceedToHome(){
        if ( bootstrap.getUserDetails() != null && bootstrap.getUserDetails().getID() != null && bootstrap.getUserDetails().getID().length() > 0 ){
            // STARTING SERVICE
            context.startService(logisticsService);

            // Link employee to fcm
            try{
                if ( FirebaseInstanceId.getInstance().getToken() != null &&
                        (bootstrap.getUserDetails().getFCMRegistrationID() == null || bootstrap.getUserDetails().getFCMRegistrationID().length() == 0 || bootstrap.getUserDetails().getFCMRegistrationID().equals(FirebaseInstanceId.getInstance().getToken()) == false ) ){
                    getBackendAPIs().getEmployeesAPI().linkToFCM(FirebaseInstanceId.getInstance().getToken(), null);
                }
            }catch (Exception e){}

            if ( bootstrap.getJob() != null && bootstrap.getJob().getType() != null ) {
                if ( bootstrap.getJob().getType().equalsIgnoreCase("d")) {
                    Intent confirmDeliveryAct = new Intent(context, DispatcherActivity.class);
                    confirmDeliveryAct.putExtra("job", new Gson().toJson(bootstrap.getJob()));
                    startActivity(confirmDeliveryAct);
                    finish();
                }else{
                    Intent whereToNextAct = new Intent(context, WhereToNextActivity.class);
                    whereToNextAct.putExtra("jobDetails", new Gson().toJson(bootstrap.getJob()));
                    startActivity(whereToNextAct);
                    finish();
                }
            } else {
                Intent homeAct = new Intent(context, HomeActivity.class);
                startActivity(homeAct);
                finish();
            }
        }
        // IF INVALID SESSION
        else{
            localStorageData.clearSession();
            Intent loginAct = new Intent(context, LoginActivity.class);
            startActivity(loginAct);
            finish();
        }
    }

}
