package com.mastaan.logistics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.StockAlertsAdapter;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.StockAlertDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 27/07/18.
 */

public class StockAlertsActivity extends MastaanToolbarActivity {

    vRecyclerView alertsHolder;
    StockAlertsAdapter stockAlertsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_alerts);
        hasLoadingView();
        hasSwipeRefresh();

        //--------

        alertsHolder = findViewById(R.id.alertsHolder);
        alertsHolder.setupVerticalOrientation();
        stockAlertsAdapter = new StockAlertsAdapter(activity, new ArrayList<StockAlertDetails>(), new StockAlertsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                StockAlertDetails stockAlertDetails = stockAlertsAdapter.getItem(position);

                Intent meatItemDetailsAct = new Intent(context, MeatItemDetailsActivity.class);
                meatItemDetailsAct.putExtra(Constants.NAME, stockAlertDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getNameWithCategoryAndSize());
                meatItemDetailsAct.putExtra(Constants.ID, stockAlertDetails.getWarehouseMeatItemDetails().getID());
                startActivity(meatItemDetailsAct);
            }
        });
        alertsHolder.setAdapter(stockAlertsAdapter);

        //--------

        getStockAlerts();

    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getStockAlerts();
    }

    //-------

    private void getStockAlerts(){

        showLoadingIndicator("Loading stock alerts, wait...");
        getBackendAPIs().getStockAPI().getStockAlerts(new StockAPI.StockAlertsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<StockAlertDetails> stockAlerts) {
                if ( status ){
                    if ( stockAlerts.size() > 0 ) {
                        stockAlertsAdapter.setItems(stockAlerts);
                        switchToContentPage();
                    }else{
                        showNoDataIndicator("Nothing to show");
                    }
                }
                else{
                    showReloadIndicator("Something went wrong, try again!");
                }
            }
        });
    }

    //==============

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
