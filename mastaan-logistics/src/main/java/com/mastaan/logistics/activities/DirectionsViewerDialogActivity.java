package com.mastaan.logistics.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.location.GoogleRetrofitInterface;
import com.aleena.common.location.model.DirectionsData;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mastaan.logistics.R;

import java.util.List;

import retrofit.RestAdapter;

public class DirectionsViewerDialogActivity extends MastaanToolbarActivity {

    RestAdapter restAdapter;
    GoogleRetrofitInterface googleRetrofitInterface;
    GooglePlacesAPI googlePlacesAPI;
    vMapFragment mapFragment;
    GoogleMap googleMap;
    LatLng origin;
    LatLng destination;
    TextView loading_message;
    String location;
    Marker sourceMarker, destinationMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directions_viewer);

        loading_message = (TextView) findViewById(R.id.loading_message);
        loading_message.setText("Getting directions, wait...");
        origin = getIntent().getParcelableExtra("origin");
        destination = getIntent().getParcelableExtra("destination");
        location = getIntent().getStringExtra("location");

        if ( CommonMethods.isPackageInstalled(context, "com.google.android.apps.maps") ) {
            findViewById(R.id.showInGoogleMaps).setVisibility(View.VISIBLE);
            findViewById(R.id.showInGoogleMaps).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + destination.latitude + "," + destination.longitude);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            });
        }

        mapFragment = ((vMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap google_Map) {
                //googleMap = mapFragment.getMap();
                googleMap = google_Map;

                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                sourceMarker = googleMap.addMarker(new MarkerOptions().title("You").position(origin)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                destinationMarker = googleMap.addMarker(new MarkerOptions().title(location).position(destination)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                restAdapter = new RestAdapter.Builder().setEndpoint("https://maps.googleapis.com/maps/api").setLogLevel(RestAdapter.LogLevel.FULL).build();
                googleRetrofitInterface = restAdapter.create(GoogleRetrofitInterface.class);
                googlePlacesAPI = new GooglePlacesAPI(context.getResources().getString(R.string.google_apikey), googleRetrofitInterface);
                googlePlacesAPI.getDirections(origin, destination, new GooglePlacesAPI.DirectionsCallback() {
                    @Override
                    public void onComplete(@Nullable List<DirectionsData.Route.Leg> legs, int status_code) {
                        if (legs != null && legs.size() > 0) {
                            updateLegDetails(legs.get(0));
                            drawRoad(legs.get(0).steps, Color.rgb(127, 0, 255));
                            findViewById(R.id.loading_layout).setVisibility(View.GONE);
                        } else if (status_code == 1000) {
                            showMessageLong("No directions to show");
                            findViewById(R.id.loading_layout).setVisibility(View.GONE);
                        } else {
                            showMessageLong("Error While getting Directions");
                            finish();
                        }
                    }
                });
            }
        });

    }

    private void updateLegDetails(DirectionsData.Route.Leg leg) {
        boolean needLegDetails = getIntent().getBooleanExtra("needLegDetails", false);
        if (!needLegDetails)
            findViewById(R.id.leg_details).setVisibility(View.GONE);
        else {
            findViewById(R.id.leg_details).setVisibility(View.VISIBLE);
            TextView start_address = (TextView) findViewById(R.id.start_address);
            TextView end_address = (TextView) findViewById(R.id.end_address);
            start_address.setText(leg.start_address);
            end_address.setText(leg.end_address);
        }
    }

    private void drawRoad(List<DirectionsData.Route.Leg.Step> steps, int color) {
        PolylineOptions lineOptions = new PolylineOptions();
        final LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        boundsBuilder.include(origin);
        boundsBuilder.include(destination);
        for (DirectionsData.Route.Leg.Step step : steps) {
            LatLng nextStep = new LatLng(step.start_location.lat, step.start_location.lng);
            lineOptions.add(nextStep);
            boundsBuilder.include(nextStep);
            try {
                lineOptions.addAll(step.polyline.getPolyLinePoints());
            } catch (Exception e) {
                e.printStackTrace();
            }
            lineOptions.add(new LatLng(step.end_location.lat, step.end_location.lng));
        }
        final GoogleMap.CancelableCallback cancelableCallback = new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                findViewById(R.id.loading_layout).setVisibility(View.GONE);
            }

            @Override
            public void onCancel() {
                googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 18));
                findViewById(R.id.loading_layout).setVisibility(View.GONE);
            }
        };
        try {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 18), cancelableCallback);
            lineOptions.color(color);
            googleMap.addPolyline(lineOptions);
        }catch (Exception e){}
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
