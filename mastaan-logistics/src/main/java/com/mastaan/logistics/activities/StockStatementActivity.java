package com.mastaan.logistics.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.SearchView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.adapters.StocksAdapter;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.backend.models.RequestStockStatement;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AddOrEditStockDialog;
import com.mastaan.logistics.dialogs.DatePeriodSelectorDialog;
import com.mastaan.logistics.dialogs.OrderItemDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.fragments.FilterItemsFragment;
import com.mastaan.logistics.interfaces.StockDetailsCallback;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

public class StockStatementActivity extends MastaanToolbarActivity implements View.OnClickListener{
    boolean isAnyChanges;

    HubDetails hubDetails;

    MastaanToolbarActivity activity;
    UserDetails userDetails;
    String actionType = "";

    TextView subtitle;

    DrawerLayout drawerLayout;
    FilterItemsFragment filterFragment;

    MenuItem searchViewMenuITem;
    SearchView searchView;
    MenuItem filtereMenuItem;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    TextView from_date;
    TextView to_date;
    View changePeriod;
    TextView summary_info;
    vRecyclerView stocksHolder;
    StocksAdapter stocksAdapter;

    List<StockDetails> showingStocksList = new ArrayList<>();

    SearchDetails searchDetails = new SearchDetails();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_statement);
        hasLoadingView();
        hasSwipeRefresh();
        activity = this;
        userDetails = localStorageData.getUserDetails();

        actionType = getIntent().getStringExtra(Constants.ACTION_TYPE);
        if ( actionType == null ){  actionType = Constants.MEAT_ITEMS_STOCK_STATEMENT;    }
        if ( getIntent().getStringExtra(Constants.TITLE) != null ){
            setToolbarTitle(getIntent().getStringExtra(Constants.TITLE));
        }else{
            if ( actionType.equalsIgnoreCase(Constants.CATEGORIES_STOCK_STATEMENT) ){
                setToolbarTitle("Categories stock statement");
            }else if ( actionType.equalsIgnoreCase(Constants.MEAT_ITEMS_STOCK_STATEMENT) ){
                setToolbarTitle("Meat items stock statement");
            }
        }

        if ( getIntent().getStringExtra(Constants.HUB_DETAILS) != null && getIntent().getStringExtra(Constants.HUB_DETAILS).trim().length() > 0 ){
            hubDetails = new Gson().fromJson(getIntent().getStringExtra(Constants.HUB_DETAILS), HubDetails.class);
        }

        //--------

        filterFragment = new FilterItemsFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.right_drawer, filterFragment).commit();
        filterFragment.setItemSelectionListener(new FilterItemsFragment.Callback() {
            @Override
            public void onItemSelected(int position, SearchDetails searchDetails) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
                if (searchDetails.getAction().equalsIgnoreCase(Constants.CLEAR)) {
                    clearSearch();
                }
                else if (searchDetails.getAction().equalsIgnoreCase(Constants.ANALYSE)) {
                    //new AnalyseOrderItemsDialog(activity, showingStocksList).show();
                    showToastMessage("Under construction / Not yet done");
                }
                else {
                    performSearch(searchDetails);
                }
            }
        });

        drawerLayout = findViewById(R.id.drawer_layout);

        searchResultsCountIndicator = findViewById(R.id.searchResultsCountIndicator);
        results_count = findViewById(R.id.results_count);
        clearFilter = findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        //---------

        subtitle = findViewById(R.id.subtitle);

        from_date = findViewById(R.id.from_date);
        to_date = findViewById(R.id.to_date);
        changePeriod = findViewById(R.id.changePeriod);
        changePeriod.setOnClickListener(this);
        summary_info = findViewById(R.id.summary_info);
        stocksHolder = findViewById(R.id.stocksHolder);
        setupHolder();

        //---------

        if ( getIntent().getStringExtra(Constants.SUB_TITLE) != null && getIntent().getStringExtra(Constants.SUB_TITLE).trim().length() > 0 ){
            subtitle.setVisibility(View.VISIBLE);
            subtitle.setText(getIntent().getStringExtra(Constants.SUB_TITLE));
        }else{  subtitle.setVisibility(View.GONE);  }

        showNoDataIndicator("Select date period");

        changePeriod.performClick();
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getStockStatement(from_date.getText().toString(), to_date.getText().toString());
    }

    @Override
    public void onClick(View view) {
        if ( view == changePeriod ){
            String prefillFromDate = from_date.getText().toString();
            String prefillToDate = to_date.getText().toString();
            if ( prefillFromDate.equalsIgnoreCase("Start")&& prefillToDate.equalsIgnoreCase("End") ){
                prefillFromDate = DateMethods.getCurrentDate();
            }
            new DatePeriodSelectorDialog(activity).show(prefillFromDate, prefillToDate, new DatePeriodSelectorDialog.Callback() {
                @Override
                public void onComplete(String fromDate, String toDate) {
                    from_date.setText(fromDate != null&&fromDate.length()>0?DateMethods.getDateInFormat(fromDate, DateConstants.MMM_DD_YYYY):"Start");
                    to_date.setText(toDate != null&&toDate.length()>0?DateMethods.getDateInFormat(toDate, DateConstants.MMM_DD_YYYY):"End");
                    getStockStatement(fromDate, toDate);
                }
            });
        }
        else if ( view == clearFilter ){
            try{    searchViewMenuITem.collapseActionView();    }catch (Exception e){}
            clearSearch();
        }
    }

    public void setupHolder(){
        stocksHolder.setupVerticalOrientation();
        stocksAdapter = new StocksAdapter(activity, new ArrayList<StockDetails>(), new StocksAdapter.Callback() {
            @Override
            public void onItemClick(final int position) {
                final StockDetails stockDetails = stocksAdapter.getItem(position);

                if ( stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_ORDER) == false
                        && stockDetails.getType().equalsIgnoreCase(Constants.HUB_MEAT_ITEM_ORDER) == false ){
                    if ( stockDetails.getType().equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE) && hubDetails != null ){
                        showToastMessage("Can't edit/delete this stock because this stock is transferred from warehouse (ask them to edit/delete)");
                        return;
                    }
                    if ( stockDetails.getType().equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB) && hubDetails != null && stockDetails.getToHubDetails().getID().equals(hubDetails.getID()) ){
                        showToastMessage("Can't edit/delete this stock because this stock is transferred from other hub (ask them to edit/delete)");
                        return;
                    }
                    if ( stockDetails.getType().equalsIgnoreCase(Constants.HUB_MEAT_ITEM_WASTAGE) && hubDetails == null ){
                        showToastMessage("Can't edit/delete this stock because this wastage is created at the hub (ask them to edit/delete)");
                        return;
                    }

                    final List<String> listOptions = new ArrayList<>();
                    if ( userDetails.isSuperUser() || stockDetails.getCreatedBy().getID().equalsIgnoreCase(userDetails.getID()) ){
                        listOptions.add("Edit");
                    }
                    if ( userDetails.isSuperUser() ){
                        listOptions.add("Delete");
                    }
                    if ( listOptions.size() > 0 ){
                        activity.showListChooserDialog(listOptions, new ListChooserCallback() {
                            @Override
                            public void onSelect(int selectedOptionPosition) {
                                if ( listOptions.get(selectedOptionPosition).equalsIgnoreCase("edit") ){
                                    new AddOrEditStockDialog(activity).setupForHub(hubDetails).show(stockDetails, new StockDetailsCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message, StockDetails stockDetails) {
                                            if ( status ){
                                                isAnyChanges = true;

                                                stocksAdapter.setItem(position, stockDetails);
                                                showSnackbarMessage("Stock updated successfully");
                                            }
                                        }
                                    });
                                }
                                else if ( listOptions.get(selectedOptionPosition).equalsIgnoreCase("delete") ){
                                    showChoiceSelectionDialog("Confirm!", "Are you sure to delete stock?", "CANCEL", "YES", new ChoiceSelectionCallback() {
                                        @Override
                                        public void onSelect(int choiceNo, String choiceName) {
                                            if ( choiceName.equalsIgnoreCase("YES")){
                                                deleteStock(position);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onOrderItemClick(int position) {
                new OrderItemDetailsDialog(activity).show(stocksAdapter.getItem(position).getOrderItemID());
            }

            @Override
            public void onStockClick(int position) {
                StockDetails stockDetails = stocksAdapter.getItem(position);
                if ( stockDetails.getLinkedStock() != null ){
                    new StockDetailsDialog(activity).showStock(stockDetails.getLinkedStock());
                }else {
                    new StockDetailsDialog(activity).showProcessedStock(stockDetails.getLinkedProcessedStock());
                }
            }
        });
        stocksAdapter.setHubDetails(hubDetails);
        stocksHolder.setAdapter(stocksAdapter);
    }

    public void deleteStock(final int position){

        showLoadingDialog("Deleting stock, wait...");
        getBackendAPIs().getStockAPI().deleteStock(stocksAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if ( status ){
                    isAnyChanges = true;

                    stocksAdapter.deleteItem(position);
                    showSnackbarMessage("Stock deleted successfully");
                }else{
                    showSnackbarMessage("Unable to delete stock, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            deleteStock(position);
                        }
                    });
                }
            }
        });
    }
    //----

    public void getStockStatement(final String fromDate, final String toDate){

        if ( (fromDate.length() > 0 && fromDate.equalsIgnoreCase("Start")==false )  || (toDate.length() > 0 && toDate.equalsIgnoreCase("End")==false) ){
            changePeriod.setVisibility(View.GONE);
            summary_info.setVisibility(View.GONE);
            searchDetails = new SearchDetails();
            searchResultsCountIndicator.setVisibility(View.GONE);
            hideMenuItems();

            RequestStockStatement requestStockStatement = new RequestStockStatement();
            requestStockStatement.setFromDate((fromDate!=null&&fromDate.length()>0&&fromDate.equalsIgnoreCase("Start")==false)?DateMethods.getDateInFormat(fromDate, DateConstants.DD_MM_YYYY):null);
            requestStockStatement.setToDate((toDate!=null&&toDate.length()>0&&toDate.equalsIgnoreCase("End")==false)?DateMethods.getDateInFormat(toDate, DateConstants.DD_MM_YYYY):null);
            if ( hubDetails != null ){
                requestStockStatement.setHub(hubDetails.getID());
            }

            if ( actionType.equalsIgnoreCase(Constants.CATEGORIES_STOCK_STATEMENT) ){
                if ( getIntent().getStringExtra(Constants.CATEGORY) != null ){
                    requestStockStatement.setCategory(getIntent().getStringExtra(Constants.CATEGORY));
                }
                showLoadingIndicator("Loading, wait...");
                getBackendAPIs().getStockAPI().getCategoriesStockStatement(requestStockStatement, new StockAPI.StockStatementCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, List<StockDetails> stocksList) {
                        changePeriod.setVisibility(View.VISIBLE);
                        if (status) {
                            displayItems(stocksList);
                        } else {
                            showReloadIndicator(statusCode, "Something went wrong, try again!");
                        }
                    }
                });
            }

            else  if ( actionType.equalsIgnoreCase(Constants.MEAT_ITEMS_STOCK_STATEMENT) ){
                if ( getIntent().getStringExtra(Constants.WAREHOUSE_MEAT_ITEM) != null ){
                    requestStockStatement.setWarehouseMeatItem(getIntent().getStringExtra(Constants.WAREHOUSE_MEAT_ITEM));
                }
                if ( getIntent().getStringArrayExtra(Constants.ATTRIBUTES_OPTIONS) != null ){
                    requestStockStatement.setItemLevelAttributeOptions(getIntent().getStringArrayExtra(Constants.ATTRIBUTES_OPTIONS));
                }

                showLoadingIndicator("Loading, wait...");
                getBackendAPIs().getStockAPI().getMeatItemsStockStatement(requestStockStatement, new StockAPI.StockStatementCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, List<StockDetails> stocksList) {
                        changePeriod.setVisibility(View.VISIBLE);
                        if (status) {
                            displayItems(stocksList);
                        } else {
                            showReloadIndicator(statusCode, "Something went wrong, try again!");
                        }
                    }
                });
            }
        }
        else{
            showNoDataIndicator("* Select at least one date");
            showToastMessage("* Select at least one date");
        }
    }

    public void displayItems(final List<StockDetails> stocksList) {
        showingStocksList = stocksList;

        if (stocksList.size() > 0) {
            showMenuItems();
            stocksAdapter.setItems(stocksList);
            switchToContentPage();
        } else {
            showNoDataIndicator("Nothing to show");
        }
    }


    //----------------

    public void performSearch(final SearchDetails search_details){
        this.searchDetails = search_details;

        if ( isShowingLoadingPage() == false && showingStocksList != null && showingStocksList.size() > 0 ) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
                SearchMethods.searchStocks(showingStocksList, searchDetails, new SearchMethods.SearchStocksCallback() {
                    @Override
                    public void onComplete(boolean status, SearchDetails searchDetails, List<StockDetails> filteredList) {
                        if (status && searchDetails == search_details ) {
                            if (filteredList.size() > 0) {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText(filteredList.size() + " result(s) found.\n(" + searchDetails.getSearchString() + ")");

                                stocksAdapter.setItems(filteredList);
                                stocksHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText("No Results");
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");
                                stocksAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                stocksAdapter.setItems(showingStocksList);
            }
        }
    }

    public void clearSearch(){
        this.searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( showingStocksList.size() > 0 ) {
            stocksAdapter.setItems(showingStocksList);
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }
        filterFragment.clearSelection();
    }

    //---------

    public void hideMenuItems(){
        try {
            filtereMenuItem.setVisible(false);
            searchViewMenuITem.setVisible(false);searchViewMenuITem.collapseActionView();
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    public void showMenuItems(){
        try {
            filtereMenuItem.setVisible(true);
            searchViewMenuITem.setVisible(true);
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, findViewById(com.aleena.common.R.id.right_drawer));
        }catch (Exception e){}
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_stock_statement, menu);

        searchViewMenuITem = menu.findItem(R.id.action_search);
        filtereMenuItem = menu.findItem(R.id.action_filter);

        searchView = (SearchView) searchViewMenuITem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchDetails searchDetails = new SearchDetails().setConsiderAtleastOneMatch(query);
                performSearch(searchDetails);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ( item.getItemId() == R.id.action_search){
        }
        else if ( item.getItemId() == R.id.action_filter){
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }else {
            if ( getCallingActivity() != null && isAnyChanges ){
                setResult(RESULT_OK);
                finish();
            }else {
                super.onBackPressed();
            }
        }
    }
}
