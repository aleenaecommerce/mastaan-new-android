package com.mastaan.logistics.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Venkatesh Uppu on 27/10/18.
 */

public class BangersTextView extends TextView {

    Typeface kaushanFont;

    public BangersTextView(Context context) {
        super(context);
        kaushanFont = Typeface.createFromAsset(context.getAssets(), "fonts/Bangers.ttf");
        this.setTypeface(kaushanFont);
    }

    public BangersTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        kaushanFont = Typeface.createFromAsset(context.getAssets(), "fonts/Bangers.ttf");
        this.setTypeface(kaushanFont);
    }

    public BangersTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        kaushanFont = Typeface.createFromAsset(context.getAssets(), "fonts/Bangers.ttf");
        this.setTypeface(kaushanFont);
    }

}
