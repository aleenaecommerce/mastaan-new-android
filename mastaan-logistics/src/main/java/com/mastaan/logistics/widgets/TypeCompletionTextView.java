package com.mastaan.logistics.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.tokenautocomplete.TokenCompleteTextView;

import java.util.List;

/**
 * Created in vCommonLib on 09/01/17.
 * @author Venkatesh Uppu (c)
 */

public class TypeCompletionTextView extends TokenCompleteTextView<String> {

    Context context;
    TypeCompletionTextView typeCompletionTextView;
    Typeface hintFont;
    Typeface normalFont;

    public TypeCompletionTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.typeCompletionTextView = this;
        setupStyles();
        //tagsCompletionTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf"));
    }

    @Override
    protected View getViewForObject(final String tag) {
        View tagView = LayoutInflater.from(getContext()).inflate(R.layout.view_type, null);
        TextView name = tagView.findViewById(R.id.name);
        name.setText(tag);

        return tagView;
    }

    @Override
    protected String defaultObject(String completionText) {
//        //Stupid simple example of guessing if we have an email or not
//        int index = completionText.indexOf('@');
//        if (index == -1) {
//            return new Person(completionText, completionText.replace(" ", "") + "@example.com");
//        } else {
//            return new Person(completionText.substring(0, index), completionText);
//        }
        return completionText;
    }

    private void setupStyles(){

        hintFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        normalFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");

        typeCompletionTextView.setTypeface(hintFont);
        typeCompletionTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,14);
        typeCompletionTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if ( charSequence.length() == 0 ){
                    typeCompletionTextView.setTypeface(hintFont);
                    typeCompletionTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,14);
                }else{
                    typeCompletionTextView.setTypeface(normalFont);
                    typeCompletionTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,16);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void setTypes(String text){
        String []types = CommonMethods.getStringArrayFromString(text, ",");
        setTypes(types);
    }
    public void setTypes(List<String> types){
        setTypes(CommonMethods.getStringArrayFromStringList(types));
    }
    public void setTypes(String []types){
        if ( types != null ){
            for(int i=0;i<types.length;i++){
                if ( types[i] != null ){
                    typeCompletionTextView.addObjectSync(types[i]);
//                    typeCompletionTextView.addObject(types[i]);
                }
            }
        }
    }
    public void clearTypes(){
        List<String> types = getObjects();
        for(int i=0;i<types.size();i++) {
           // removeObject(types.get(i));
            removeObjectSync(types.get(i));
        }
    }

}