package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mastaan.logistics.R;
import com.mastaan.logistics.models.HousingSocietyDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 23/09/18.
 */

public class HousingSocietiesAdapter extends RecyclerView.Adapter<HousingSocietiesAdapter.ViewHolder> {

    Context context;
    List<HousingSocietyDetails> itemsList;
    CallBack callBack;

    boolean hideActions;
    public HousingSocietiesAdapter hideActions(){
        this.hideActions = true;
        return this;
    }

    public interface CallBack {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemSelector;
        TextView name;
        TextView latlng;
        View delete;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            name = itemLayoutView.findViewById(R.id.name);
            latlng = itemLayoutView.findViewById(R.id.latlng);
            delete = itemView.findViewById(R.id.delete);
        }
    }

    public HousingSocietiesAdapter(Context context, List<HousingSocietyDetails> housingSocietiesList, CallBack callBack) {
        this.context = context;
        this.itemsList = housingSocietiesList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public HousingSocietiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_housing_society, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final HousingSocietyDetails housingSocietyDetails = itemsList.get(position);

        if ( housingSocietyDetails != null ){
            viewHolder.name.setText(housingSocietyDetails.getName());
            viewHolder.latlng.setText(housingSocietyDetails.getLatLngString());

            viewHolder.delete.setVisibility(hideActions?View.GONE:View.VISIBLE);

            if ( callBack != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
                viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onDeleteClick(position);
                    }
                });
            }
        }
    }

    //---------------

    public void setItems(List<HousingSocietyDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItem(HousingSocietyDetails housingSocietyDetails){
        itemsList.add(housingSocietyDetails);
        notifyDataSetChanged();
    }
    public void addItem(int position, HousingSocietyDetails housingSocietyDetails){
        if ( position >= 0 && position < itemsList.size() ) {
            itemsList.add(position, housingSocietyDetails);
        }
        notifyDataSetChanged();
    }

    public void setItem(int position, HousingSocietyDetails housingSocietyDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, housingSocietyDetails);
            notifyDataSetChanged();
        }
    }

    public HousingSocietyDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<HousingSocietyDetails> getAllItems(){
        return itemsList;
    }

}