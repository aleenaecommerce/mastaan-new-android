package com.mastaan.logistics.adapters;

import android.content.Context;

import android.text.ClipboardManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vWrappingLinearLayoutManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.methods.ActivityMethods;
import com.mastaan.logistics.models.AppliedDiscountDetails;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 11/8/16.
 */
public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    LocalStorageData localStorageData;
    UserDetails userDetails;
    List<OrderDetails> itemsList;
    boolean showUserItemsFirst;

    public void setShowUserItemsFirst(boolean showUserItemsFirst) {
        this.showUserItemsFirst = showUserItemsFirst;
    }

    boolean showCustomerDetails = true;
    public void setShowCustomerDetails(boolean showCustomerDetails) {
        this.showCustomerDetails = showCustomerDetails;
    }

    Callback callback;

    public interface Callback{
        void onItemClick(int position);

        void onCallCustomer(int position);
        void onMessageCustomer(int position);
        void onShowDirections(int position);
        void onShowBuyerHistory(int position);

        void onShowReferralBuyerDetails(int position);

        void onShowMoneyCollectionDetails(int position);
        void onCheckPaymentStatus(int position);
        void onRequestOnlinePayment(int position);

        void onShowFollowupResultedInOrder(int position);

        void onPrintBill(int position);

        void onOrderItemClick(int position, int item_position);

        void onShowOrderItemStockDetails(int position, int item_position);
    }

    public OrdersAdapter(MastaanToolbarActivity activity, List<OrderDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.localStorageData = new LocalStorageData(context);
        this.userDetails = localStorageData.getUserDetails();
        this.itemsList = itemsList;
        this.callback = callback;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        //View itemSelector2;
        CardView container;

        LinearLayout headerContainer;
        TextView status_header;
        TextView source;
        TextView card_title;
        TextView items_count;
        View printBill;

        View first_order;
        TextView pre_order_status;
        TextView referral_order;

        View customerDetailsView;
        TextView customer_name;
        TextView membership_type;
        TextView last_followup_date;
        TextView customer_mobile;
        TextView customer_address;
        TextView customer_land_mark;
        FloatingActionButton callCustomer;
        FloatingActionButton messageCustomer;
        FloatingActionButton showDirections;
        FloatingActionButton showBuyerHistory;

        TextView order_cost;
        View checkPaymentStatus;
        View requestOnlinePayment;
        View buyerOutstandingDetails;
        TextView buyer_outstanding;
        //View discountDetailsView;
        //TextView order_discount;
        View orderCreatorDetailsView;
        TextView order_creator;
        View discountsView;
        TextView discount_details;

        vRecyclerView orderItemsHolder;

        TextView order_status;
        TextView delivery_hub;
        TextView delivery_time;

        View showFollowupDetails;;

        public ViewHolder(View itemView) {
            super(itemView);

            itemSelector = itemView.findViewById(R.id.itemSelector);
            //itemSelector2 = itemView.findViewById(R.id.itemSelector2);
            container = itemView.findViewById(R.id.container);

            headerContainer = itemView.findViewById(R.id.headerContainer);
            status_header = itemView.findViewById(R.id.status_header);
            source = itemView.findViewById(R.id.source);
            card_title = itemView.findViewById(R.id.card_title);
            items_count = itemView.findViewById(R.id.items_count);
            printBill = itemView.findViewById(R.id.printBill);

            first_order = itemView.findViewById(R.id.first_order);
            pre_order_status = itemView.findViewById(R.id.pre_order_status);
            referral_order = itemView.findViewById(R.id.referral_order);

            customerDetailsView = itemView.findViewById(R.id.customerDetailsView);
            customer_name = itemView.findViewById(R.id.customer_name);
            membership_type = itemView.findViewById(R.id.membership_type);
            last_followup_date = itemView.findViewById(R.id.last_followup_date);
            customer_mobile = itemView.findViewById(R.id.customer_mobile);
            customer_address = itemView.findViewById(R.id.customer_address);
            customer_land_mark = itemView.findViewById(R.id.customer_land_mark);
            callCustomer = itemView.findViewById(R.id.callCustomer);
            messageCustomer = itemView.findViewById(R.id.messageCustomer);
            showDirections = itemView.findViewById(R.id.showDirections);
            showBuyerHistory = itemView.findViewById(R.id.showBuyerHistory);

            order_cost = itemView.findViewById(R.id.order_cost);
            checkPaymentStatus = itemView.findViewById(R.id.checkPaymentStatus);
            requestOnlinePayment = itemView.findViewById(R.id.requestOnlinePayment);
            buyerOutstandingDetails = itemView.findViewById(R.id.buyerOutstandingDetails);
            buyer_outstanding = itemView.findViewById(R.id.buyer_outstanding);
            //discountDetailsView = itemView.findViewById(R.id.discountDetailsView);
            //order_discount = itemView.findViewById(R.id.order_discount);
            orderCreatorDetailsView = itemView.findViewById(R.id.orderCreatorDetailsView);
            order_creator = itemView.findViewById(R.id.order_creator);
            discountsView = itemView.findViewById(R.id.discountsView);
            discount_details = itemView.findViewById(R.id.discount_details);

            orderItemsHolder = itemView.findViewById(R.id.orderItemsHolder);

            order_status = itemView.findViewById(R.id.order_status);
            delivery_hub = itemView.findViewById(R.id.delivery_hub);
            delivery_time = itemView.findViewById(R.id.delivery_time);

            showFollowupDetails = itemView.findViewById(R.id.showFollowupDetails);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.view_order, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        try{
            final OrderDetails orderDetails = itemsList.get(position);

            if ( orderDetails != null ) {
                BuyerDetails buyerDetails = orderDetails.getBuyerDetails();

                //PENDING
                if ( orderDetails.getStatus().equalsIgnoreCase(Constants.DELIVERED) ){
                    viewHolder.status_header.setText(" A ");
                    setColors(viewHolder, R.color.delivered_color, R.color.delivered_color_fab);
                }
                //FAILED
                else if ( orderDetails.getStatus().equalsIgnoreCase(Constants.FAILED) || orderDetails.getStatus().equalsIgnoreCase("failure")){
                    viewHolder.status_header.setText(" F ");
                    setColors(viewHolder, R.color.failed_color, R.color.failed_color_fab);
                }
                //PENDING
                else{
                    viewHolder.status_header.setText(" O ");
                    setColors(viewHolder, R.color.default_color, R.color.default_color_fab);
                }

                if ( ActivityMethods.isBuyerOrdersActivity(activity) ){
                    viewHolder.showBuyerHistory.setVisibility(View.GONE);
                }

                //--------

                if ( ActivityMethods.isFutureOrdersActivity(activity) || ActivityMethods.isOrdersByDateActivity(activity)
                        || orderDetails.isBillPrinted()
                        || orderDetails.getStatus().equalsIgnoreCase(Constants.DELIVERED)
                        || orderDetails.getStatus().equalsIgnoreCase(Constants.FAILED)
                        || orderDetails.getStatus().equalsIgnoreCase(Constants.REJECTED) ){
                    viewHolder.printBill.setVisibility(View.GONE);
                }else{
                    viewHolder.printBill.setVisibility(View.VISIBLE);
                }

                viewHolder.status_header.setText(""+orderDetails.getStatus().charAt(0));
                viewHolder.items_count.setText(""+orderDetails.getOrderedItems().size());
                viewHolder.source.setText(orderDetails.getSource());
                viewHolder.card_title.setText(orderDetails.getReadableOrderID()+"\n"+orderDetails.getFormattedOrderID()+"("+orderDetails.getInvoiceVersionNumber()+")");

                if ( buyerDetails != null && buyerDetails.getLastFollowupDate() != null && buyerDetails.getLastFollowupDate().length() > 0 ){
                    viewHolder.last_followup_date.setVisibility(View.VISIBLE);
                    viewHolder.last_followup_date.setText(Html.fromHtml("Last followup: <b>"+DateMethods.getReadableDateFromUTC(buyerDetails.getLastFollowupDate(), DateConstants.MMM_DD_YYYY)+"</b>"));
                }else{  viewHolder.last_followup_date.setVisibility(View.GONE); }

                viewHolder.first_order.setVisibility(orderDetails.isFirstOrder()?View.VISIBLE:View.GONE);
                if ( orderDetails.getReferralCode() != null && orderDetails.getReferralCode().length() > 0 ){
                    viewHolder.referral_order.setVisibility(View.VISIBLE);
                    viewHolder.referral_order.setText("Referral Order ("+orderDetails.getReferralCode().toUpperCase()+")");
                }else{
                    viewHolder.referral_order.setVisibility(View.GONE);
                }

                if ( DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate())), DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()))) > 0 ){
                    viewHolder.pre_order_status.setVisibility(View.VISIBLE);
                    viewHolder.pre_order_status.setText("PreOrder: "+DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()), DateConstants.MMM_DD));
                }else{
                    viewHolder.pre_order_status.setVisibility(View.GONE);
                }

                vWrappingLinearLayoutManager vWrappingLinearLayoutManager = new vWrappingLinearLayoutManager(context);
                vWrappingLinearLayoutManager.setOrientation(OrientationHelper.VERTICAL);
                viewHolder.orderItemsHolder.setLayoutManager(vWrappingLinearLayoutManager);//new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
                viewHolder.orderItemsHolder.setHasFixedSize(false);
                viewHolder.orderItemsHolder.setNestedScrollingEnabled(false);
                viewHolder.orderItemsHolder.setItemAnimator(new DefaultItemAnimator());
                OrderItemsMiniAdapter orderItemsMiniAdapter = new OrderItemsMiniAdapter(activity, showUserItemsFirst ? orderDetails.getOrderedItemsWithUserItemsFirst(localStorageData.getUserID()) : orderDetails.getOrderedItems(), new OrderItemsMiniAdapter.Callback() {
                    @Override
                    public void onItemClick(int itemPosition) {
                        if ( callback != null ){
                            callback.onOrderItemClick(position, itemPosition);
                        }
                    }

                    @Override
                    public void onShowStockDetails(int itemPosition) {
                        if ( callback != null ){
                            callback.onShowOrderItemStockDetails(position, itemPosition);
                        }
                    }
                });
                viewHolder.orderItemsHolder.setAdapter(orderItemsMiniAdapter);


                if ( showCustomerDetails ) {
                    viewHolder.customerDetailsView.setVisibility(View.VISIBLE);

                    viewHolder.customer_name.setText(orderDetails.getCustomerName());

                    String membership = orderDetails.getBuyerDetails().getMembershipType();
                    if ( membership.equalsIgnoreCase(Constants.BRONZE) == false ){
                        viewHolder.membership_type.setVisibility(View.VISIBLE);
                        viewHolder.membership_type.setText(membership);
                    }else{  viewHolder.membership_type.setVisibility(View.GONE);    }

                    viewHolder.customer_mobile.setText(CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile()));
                    Log.e("mobile num", ""+orderDetails.getCustomerMobile().toString() + orderDetails.getCustomerAlternateMobile().toString());

                    if ( orderDetails.getCustomerAlternateMobile().length() > 0
                            && orderDetails.getCustomerMobile().equalsIgnoreCase(orderDetails.getCustomerAlternateMobile()) == false ){
                        viewHolder.customer_mobile.setText(CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile())
                                +" / "+CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerAlternateMobile()));
                    }
                    viewHolder.messageCustomer.setVisibility(showMessageCustomer?View.VISIBLE:View.GONE);

                    viewHolder.customer_address.setText(CommonMethods.getStringFromStringArray(new String[]{orderDetails.getDeliveryAddress()}));//orderDetails.getOrderDetails().getDeliveryAddressPremise() + ",\n" + orderDetails.getOrderDetails().getAddressLine2());
                    if (orderDetails.getDeliveryAddressLandmark() != null && orderDetails.getDeliveryAddressLandmark().length() > 0) {
                        viewHolder.customer_land_mark.setText("Landmark : " + orderDetails.getDeliveryAddressLandmark().toUpperCase());
                    } else {
                        viewHolder.customer_land_mark.setVisibility(View.GONE);
                    }

                    viewHolder.order_cost.setText(Html.fromHtml("<u>"+orderDetails.getFullTotalAmountSplitString()+"</u>"));
                    viewHolder.checkPaymentStatus.setVisibility(showCheckPaymentStatus&&orderDetails.getTotalCashAmount()>0?View.VISIBLE:View.GONE);
                    viewHolder.requestOnlinePayment.setVisibility(showRequestOnlinePayment&&orderDetails.getTotalCashAmount()>0?View.VISIBLE:View.GONE);

                    /*if ( orderDetails.getTotalDiscount() > 0 ){
                        viewHolder.discountDetailsView.setVisibility(View.VISIBLE);
                        viewHolder.order_discount.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalDiscount())+(orderDetails.getCouponCode()!=null?" ("+orderDetails.getCouponCode().toUpperCase()+")":""));
                    }else{
                        viewHolder.discountDetailsView.setVisibility(View.GONE);
                    }*/

                    float buyerOutstandingAmount = orderDetails.getBuyerDetails().getOutstandingAmount();
                    if (buyerOutstandingAmount != 0) {
                        viewHolder.buyerOutstandingDetails.setVisibility(View.VISIBLE);
                        if (buyerOutstandingAmount < 0) {
                            viewHolder.buyer_outstanding.setText(SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(buyerOutstandingAmount) + " (Return to customer)");
                        } else if (buyerOutstandingAmount > 0) {
                            viewHolder.buyer_outstanding.setText(SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(buyerOutstandingAmount) + " (Collect from customer)");
                        }
                    } else {
                        viewHolder.buyerOutstandingDetails.setVisibility(View.GONE);
                    }

                    if ( orderDetails.getOrderCreatorName() != null ){
                        viewHolder.orderCreatorDetailsView.setVisibility(View.VISIBLE);
                        viewHolder.order_creator.setText(orderDetails.getOrderCreatorName());
                    }else{
                        viewHolder.orderCreatorDetailsView.setVisibility(View.GONE);
                    }

                    //--------

                    List<AppliedDiscountDetails> appliedDiscounts = orderDetails.getAppliedDiscounts();
                    if ( appliedDiscounts != null && appliedDiscounts.size() > 0 ){
                        String discountsDetailsString = "";
                        for (int i=0;i<appliedDiscounts.size();i++){
                            AppliedDiscountDetails appliedDiscountDetails = appliedDiscounts.get(i);
                            if ( appliedDiscountDetails != null ){
                                if ( discountsDetailsString.length() > 0 ){ discountsDetailsString += "<br>"; }
                                discountsDetailsString += "<b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(appliedDiscountDetails.getAmount())+"</b> for "+appliedDiscountDetails.getDiscountReasonWithComment().toUpperCase()+"</b> by <b>"+appliedDiscountDetails.getDiscountCreatorName().toUpperCase()+"</b>";
                            }
                        }
                        viewHolder.discount_details.setText(Html.fromHtml(discountsDetailsString));
                        viewHolder.discountsView.setVisibility(View.VISIBLE);
                    }else{
                        viewHolder.discountsView.setVisibility(View.GONE);
                    }

                }else{
                    viewHolder.customerDetailsView.setVisibility(View.GONE);
                }

                //---

                viewHolder.order_status.setText("CREATED : "+DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()), DateConstants.MMM_DD_YYYY_HH_MM_A)+"\n"+orderDetails.getStatus());

                viewHolder.delivery_hub.setText(orderDetails.getHubDetails()!=null?"Hub - "+orderDetails.getHubDetails().getName():"HUB NOT ASSIGNED");

                String slotAppendString = "";
                if ( ActivityMethods.isOrdersByDateActivity(activity) == false && orderDetails.deliverEarlyIfPossible() == false ){
                    slotAppendString += "\n(Don't Send Early)";
                }
                if ( orderDetails.isBillNotRequired() ){
                    slotAppendString += "\n(Paper Bill Not Required)";
                }
                // FOR FUTURE ORDERS
                if ( ActivityMethods.isOrdersByDateActivity(activity) || DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate())), DateMethods.getOnlyDate(localStorageData.getServerTime())) == 0 ){
                    viewHolder.delivery_time.setText("Time: " + orderDetails.getDeliveryTimeSlot()+slotAppendString);
                }else {
                    viewHolder.delivery_time.setText(DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate()), DateConstants.MMM_DD) + ", " + orderDetails.getDeliveryTimeSlot()+slotAppendString);
                }

                viewHolder.showFollowupDetails.setVisibility(orderDetails.isFollowupResultedOrder()?View.VISIBLE:View.GONE);

                //-------

                if ( callback != null ){

                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if ( userDetails.isSuperUser() || userDetails.isCustomerRelationshipManager()
                                    || localStorageData.isUserCheckedIn() || ActivityMethods.isCustomerSupportActivity(activity)) {
                                callback.onItemClick(position);
                            }else{
                                activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                            }
                        }
                    });
//                viewHolder.itemSelector2.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        viewHolder.itemSelector.performClick();
//                        //callback.onItemClick(position);
//                    }
//                });

                    viewHolder.showDirections.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callback.onShowDirections(position);
                        }
                    });
                    viewHolder.showBuyerHistory.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callback.onShowBuyerHistory(position);
                        }
                    });

                    viewHolder.referral_order.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callback.onShowReferralBuyerDetails(position);
                        }
                    });

                    viewHolder.order_cost.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onShowMoneyCollectionDetails(position);
                        }
                    });

                    viewHolder.checkPaymentStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onCheckPaymentStatus(position);
                        }
                    });
                    viewHolder.requestOnlinePayment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onRequestOnlinePayment(position);
                        }
                    });

                    viewHolder.showFollowupDetails.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onShowFollowupResultedInOrder(position);
                        }
                    });

                    viewHolder.callCustomer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callback.onCallCustomer(position);
                        }
                    });
                    viewHolder.callCustomer.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                            clipboardManager.setText(CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile()));
                            Toast.makeText(context, orderDetails.getCustomerName() + "'s primary mobile number ( " + orderDetails.getCustomerMobile() + " ) is copied", Toast.LENGTH_SHORT).show();
                            return false;
                        }
                    });

                    viewHolder.messageCustomer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callback.onMessageCustomer(position);
                        }
                    });

                    viewHolder.printBill.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callback.onPrintBill(position);
                        }
                    });
                }
            }

        }catch (Exception e){}
    }

    private void setColors(ViewHolder viewHolder,int normalColor, int fabColor){
        //fabColor = R.color.default_card_fab_color;
        viewHolder.container.setCardBackgroundColor(context.getResources().getColor(normalColor));
//        viewHolder.callCustomer.setColorNormalResId(fabColor);
        //viewHolder.callCustomer.setColorPressedResId(fabColor);
//        viewHolder.callCustomerAleternate.setColorNormalResId(fabColor);
        //viewHolder.callCustomerAleternate.setColorPressedResId(fabColor);
//        viewHolder.showDirections.setColorNormalResId(fabColor);
//        viewHolder.showBuyerHistory.setColorNormalResId(fabColor);
        //viewHolder.showDirections.setColorPressedResId(fabColor);
    }

    boolean showMessageCustomer;
    public void showMessageCustomerOption(){
        this.showMessageCustomer = true;
    }

    //--------------

    public void setBillPrinted(int position, boolean isBillPrinted) {
        itemsList.get(position).setBillPrinted(isBillPrinted);
        notifyDataSetChanged();
    }

    //--------------

    boolean showCheckPaymentStatus;
    public OrdersAdapter showCheckPaymentStatus(){
        this.showCheckPaymentStatus = true;
        return this;
    }

    boolean showRequestOnlinePayment;
    public OrdersAdapter showRequestOnlinePayment(){
        this.showRequestOnlinePayment = true;
        return this;
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<OrderDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<OrderDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItemAt(int position, OrderDetails orderDetails){
        if ( position < itemsList.size() ){
            itemsList.add(position, orderDetails);
            notifyDataSetChanged();
        }
    }

    public OrderDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void updateItem(int position, OrderDetails orderDetails){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.set(position, orderDetails);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<OrderDetails> getAllItems(){
        return itemsList;
    }



}
