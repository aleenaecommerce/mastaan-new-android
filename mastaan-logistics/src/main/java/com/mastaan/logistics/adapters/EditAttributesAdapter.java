package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.AttributeDetails;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.List;

public class EditAttributesAdapter extends RecyclerView.Adapter<EditAttributesAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    UserDetails userDetails;
    List<AttributeDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onAttributeUpdateClick(int position);
        void onOptionMenuClick(int attributePosition, int optionPosition, View view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView attribute_name;
        View update;
        vRecyclerView optionsHolder;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            attribute_name = (TextView) itemLayoutView.findViewById(R.id.attribute_name);
            update = itemLayoutView.findViewById(R.id.update);
            optionsHolder = (vRecyclerView) itemLayoutView.findViewById(R.id.optionsHolder);
        }
    }

    public EditAttributesAdapter(MastaanToolbarActivity activity, List<AttributeDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.userDetails = activity.localStorageData.getUserDetails();
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public EditAttributesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_edit_attribute, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final AttributeDetails attributeDetails = itemsList.get(position);
        if ( attributeDetails != null ){

            viewHolder.attribute_name.setText(attributeDetails.getName().toUpperCase());

            if ( userDetails.isSuperUser() ){
                viewHolder.update.setVisibility(View.VISIBLE);
            }else{
                viewHolder.update.setVisibility(View.GONE);
            }

            viewHolder.optionsHolder.setupVerticalOrientation();
            EditAttributeOptionsAdapter optionsAdapter = new EditAttributeOptionsAdapter(activity, userDetails, attributeDetails.getAvailableOptions(), attributeDetails.isAvailabilityChangeDaywise(), new EditAttributeOptionsAdapter.Callback() {
                @Override
                public void onMenuClick(int optionPosition, View view) {
                    if ( callback != null ){
                        callback.onOptionMenuClick(position, optionPosition, view);
                    }
                }
            });
            viewHolder.optionsHolder.setAdapter(optionsAdapter);

            if ( callback != null ){
                viewHolder.update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //if ( activity.localStorageData.isUserCheckedIn() ) {
                            callback.onAttributeUpdateClick(position);
                        //}else{
                        //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                        //}
                    }
                });
            }

        }
    }

    //--------

    public void updateAttrbuteOptionDetails(int attributePosition, int optionPostion, AttributeOptionDetails updatedAttributeOptionDetails){
        if ( itemsList.size() > 0 && attributePosition < itemsList.size() ){
            itemsList.get(attributePosition).updateOptionDetails(optionPostion, updatedAttributeOptionDetails);
            notifyDataSetChanged();
        }
    }


    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<AttributeDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<AttributeDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void updateItem(int position, AttributeDetails attributeDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, attributeDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public AttributeDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<AttributeDetails> getAllItems(){
        return itemsList;
    }

}