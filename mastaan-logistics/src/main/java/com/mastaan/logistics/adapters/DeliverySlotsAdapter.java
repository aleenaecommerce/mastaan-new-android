package com.mastaan.logistics.adapters;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.DeliveryFeeDetails;
import com.mastaan.logistics.models.DeliverySlotDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 17-01-2018.
 */

public class DeliverySlotsAdapter extends RecyclerView.Adapter<DeliverySlotsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    List<DeliverySlotDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        TextView slot;
        TextView details;
        View actionDelete;
        View fader;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            slot = (TextView) itemLayoutView.findViewById(R.id.slot);
            details = (TextView) itemLayoutView.findViewById(R.id.details);
            actionDelete = itemLayoutView.findViewById(R.id.actionDelete);
            fader = itemLayoutView.findViewById(R.id.fader);
        }
    }

    public DeliverySlotsAdapter(MastaanToolbarActivity activity, List<DeliverySlotDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public DeliverySlotsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_delivery_slot, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final DeliverySlotDetails deliverySlotDetails = itemsList.get(position);
        if ( deliverySlotDetails != null ){

            //viewHolder.fader.setVisibility(deliverySlotDetails.isEnabled()?View.GONE:View.VISIBLE);
            viewHolder.slot.setText(deliverySlotDetails.getDisplayText()+" ("+CommonMethods.getInDecimalFormat(deliverySlotDetails.getMinimumDistance())+" - "+CommonMethods.getInDecimalFormat(deliverySlotDetails.getMaximumDistance())+"kms)");

            String detailsString = "";
            if ( deliverySlotDetails.getMaximumOrders() > 0 ){
                if ( detailsString.length() > 0 ){  detailsString += "<br>";    }
                detailsString += "Maximum Orders: <b>"+CommonMethods.getInDecimalFormat(deliverySlotDetails.getMaximumOrders())+"</b>";
            }
            if ( deliverySlotDetails.getCutoffTime() > 0 ){
                if ( detailsString.length() > 0 ){  detailsString += "<br>";    }
                detailsString += "Cutoff Time: <b>"+ deliverySlotDetails.getCutoffTime()+" mins</b>";
            }
            if ( deliverySlotDetails.getDisabledCategories() != null && deliverySlotDetails.getDisabledCategories().size() > 0 ){
                if ( detailsString.length() > 0 ){  detailsString += "<br>";    }
                detailsString += "Excluded Categories: <b>"+deliverySlotDetails.getDisabledCategoriesString()+"</b>";
            }
            if ( deliverySlotDetails.getDisabledWarehouseMetatItems() != null && deliverySlotDetails.getDisabledWarehouseMetatItems().size() > 0 ){
                if ( detailsString.length() > 0 ){  detailsString += "<br>";    }
                String excludedItemsString = "";
                for (int i=0;i<deliverySlotDetails.getDisabledWarehouseMetatItems().size();i++){
                    if ( excludedItemsString.length() > 0 ){    excludedItemsString += ", "; }
                    WarehouseMeatItemDetails warehouseMeatItemDetails = activity.getMastaanApplication().getCachedWarehouseMeatMeatItemDetails(deliverySlotDetails.getDisabledWarehouseMetatItems().get(i));
                    if ( warehouseMeatItemDetails != null ){
                        excludedItemsString += warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize();
                    }else{
                        excludedItemsString += deliverySlotDetails.getDisabledWarehouseMetatItems().get(i);
                    }
                }
                detailsString += "Excluded Items: <b>"+excludedItemsString+"</b>";
            }
            if ( deliverySlotDetails.getDisabledWeekDays() != null && deliverySlotDetails.getDisabledWeekDays().size() > 0 ){
                if ( detailsString.length() > 0 ){  detailsString += "<br>";    }
                detailsString += "Excluded Days: <b>"+deliverySlotDetails.getDisabledWeekDaysString()+"</b>";
            }
            if ( deliverySlotDetails.getDeliveryFees() != null && deliverySlotDetails.getDeliveryFees().size() > 0 ){
                if ( detailsString.length() > 0 ){  detailsString += "<br>";    }
                detailsString += "<u>Delivery Fees</u>: ";
                for (int i = 0; i< deliverySlotDetails.getDeliveryFees().size(); i++){
                    DeliveryFeeDetails deliveryFee = deliverySlotDetails.getDeliveryFees().get(i);
                    detailsString += "<br>"+CommonMethods.getInDecimalFormat(deliveryFee.getMinDistance())+"km to "+CommonMethods.getInDecimalFormat(deliveryFee.getMaxDistance())+"km : <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(deliveryFee.getFee())+"</b>";
                }
            }
            viewHolder.details.setVisibility(detailsString.length()>0?View.VISIBLE:View.GONE);
            viewHolder.details.setText(Html.fromHtml(detailsString));


            if ( callback != null ) {
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position);
                    }
                });
                viewHolder.actionDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onDeleteClick(position);
                    }
                });
            }
        }
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<DeliverySlotDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<DeliverySlotDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void addItem(DeliverySlotDetails deliverySlotDetails){
        addItem(-1, deliverySlotDetails);
    }
    public void addItem(int position, DeliverySlotDetails deliverySlotDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.add(position, deliverySlotDetails);
        }else{
            itemsList.add(deliverySlotDetails);
        }
        notifyDataSetChanged();
    }

    public void setItem(int position, DeliverySlotDetails deliverySlotDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, deliverySlotDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public DeliverySlotDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<DeliverySlotDetails> getAllItems(){
        return itemsList;
    }

}