package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.widgets.vRecyclerView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.DeliveryZoneDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 30/03/17.
 */

public class DeliveryZonesAdapter extends RecyclerView.Adapter<DeliveryZonesAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<DeliveryZoneDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position, int areaPosition);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        vRecyclerView deliveryAreasHolder;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            name = (TextView) itemLayoutView.findViewById(R.id.name);
            deliveryAreasHolder = (vRecyclerView)  itemView.findViewById(R.id.deliveryAreasHolder);
        }
    }

    public DeliveryZonesAdapter(MastaanToolbarActivity activity, List<DeliveryZoneDetails> areasList, CallBack callBack) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = areasList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public DeliveryZonesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_delivery_zone, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final DeliveryZoneDetails deliveryZoneDetails = itemsList.get(position);

        if ( deliveryZoneDetails != null ){
            viewHolder.name.setText(deliveryZoneDetails.getName()+" ("+ deliveryZoneDetails.getAreas().size()+")");

            /*vWrappingLinearLayoutManager vWrappingLinearLayoutManager = new vWrappingLinearLayoutManager(context);
            vWrappingLinearLayoutManager.setOrientation(OrientationHelper.VERTICAL);
            viewHolder.deliveryAreasHolder.setLayoutManager(vWrappingLinearLayoutManager);//new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
            viewHolder.deliveryAreasHolder.setHasFixedSize(false);
            viewHolder.deliveryAreasHolder.setNestedScrollingEnabled(false);
            viewHolder.deliveryAreasHolder.setItemAnimator(new DefaultItemAnimator());*/
            viewHolder.deliveryAreasHolder.setupVerticalGridOrientation(2);
            DeliveryAreasAdapter deliveryAreasAdapter = new DeliveryAreasAdapter(activity, deliveryZoneDetails.getAreas(), new DeliveryAreasAdapter.CallBack() {
                @Override
                public void onItemClick(int areaPosition) {
                    if ( callBack != null ){
                        callBack.onItemClick(position, areaPosition);
                    }
                }
            });
            viewHolder.deliveryAreasHolder.setAdapter(deliveryAreasAdapter);
        }
    }

    //---------------

    public void setItems(List<DeliveryZoneDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<DeliveryZoneDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(DeliveryZoneDetails deliveryZoneDetails){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(deliveryZoneDetails.getID()) ){
                itemsList.set(i, deliveryZoneDetails);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void setItem(int position, DeliveryZoneDetails deliveryZoneDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, deliveryZoneDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public DeliveryZoneDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<DeliveryZoneDetails> getAllItems(){
        return itemsList;
    }

}