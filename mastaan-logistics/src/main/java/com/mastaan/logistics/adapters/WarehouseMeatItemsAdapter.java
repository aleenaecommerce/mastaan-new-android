package com.mastaan.logistics.adapters;

import android.content.Context;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.AvailabilityDetails;
import com.mastaan.logistics.models.MeatItemDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class WarehouseMeatItemsAdapter extends RecyclerView.Adapter<WarehouseMeatItemsAdapter.ViewHolder> {

    Context context;
    String todaysDate = "";
    List<WarehouseMeatItemDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onRefreshStockDetailsClick(int position);
        void onShowMeatItemDetailsClick(int position);
        void onMarkAsAvailable(int position);
        void onMarkAsUnavailable(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout itemSelector;
        View fader;
        View disabledIndicator;

        TextView index;
        TextView item_name;
        TextView highlight;
        TextView pre_order_status;
        View add_on;
        View unavailable_today_status;
        TextView source;
        TextView multi_item_name;
        ImageView item_image;
        TextView availability_start_time;
        TextView availability_end_time;
        TextView preparation_time;
        TextView minimum_weight;
        TextView vendor_price;
        TextView buyer_price;
        View stockDetailsView;
        TextView stock_details;
        View refreshStockDetails;
        View showMeatItemDetails;
        View availabilityActions;
        View markAsAvailable;
        View markAsUnavailable;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            fader = itemLayoutView.findViewById(R.id.fader);
            disabledIndicator = itemLayoutView.findViewById(R.id.disabledIndicator);

            index = itemLayoutView.findViewById(R.id.index);
            item_name = itemLayoutView.findViewById(R.id.item_name);
            highlight = itemLayoutView.findViewById(R.id.highlight);
            pre_order_status = itemLayoutView.findViewById(R.id.pre_order_status);
            add_on = itemLayoutView.findViewById(R.id.add_on);
            unavailable_today_status = itemLayoutView.findViewById(R.id.unavailable_today_status);
            source = itemLayoutView.findViewById(R.id.source);
            multi_item_name = itemLayoutView.findViewById(R.id.multi_item_name);
            item_image = itemLayoutView.findViewById(R.id.item_image);
            availability_start_time = itemLayoutView.findViewById(R.id.availability_start_time);
            availability_end_time = itemLayoutView.findViewById(R.id.availability_end_time);
            preparation_time = itemLayoutView.findViewById(R.id.preparation_time);
            minimum_weight = itemLayoutView.findViewById(R.id.minimum_weight);
            vendor_price = itemLayoutView.findViewById(R.id.vendor_price);
            buyer_price = itemLayoutView.findViewById(R.id.buyer_price);
            stockDetailsView = itemLayoutView.findViewById(R.id.stockDetailsView);
            stock_details = itemLayoutView.findViewById(R.id.stock_details);
            refreshStockDetails = itemLayoutView.findViewById(R.id.refreshStockDetails);
            showMeatItemDetails = itemLayoutView.findViewById(R.id.showMeatItemDetails);
            availabilityActions = itemLayoutView.findViewById(R.id.availabilityActions);
            markAsAvailable = itemLayoutView.findViewById(R.id.markAsAvailable);
            markAsUnavailable = itemLayoutView.findViewById(R.id.markAsUnavailable);
        }
    }

    public WarehouseMeatItemsAdapter(Context context, List<WarehouseMeatItemDetails> itemsList, Callback callback) {
        this.context = context;
        this.todaysDate = DateMethods.getOnlyDate(new LocalStorageData(context).getServerTime());
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public WarehouseMeatItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_meat_item, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final WarehouseMeatItemDetails warehouseMeatItemDetails = getItem(position);
        if ( warehouseMeatItemDetails != null ){
            final MeatItemDetails meatItemDetails = warehouseMeatItemDetails.getMeatItemDetais();

            if ( meatItemDetails != null ){
                viewHolder.index.setText(CommonMethods.getInDecimalFormat(warehouseMeatItemDetails.getIndex()));
                viewHolder.item_name.setText(CommonMethods.capitalizeStringWords(meatItemDetails.getNameWithCategoryAndSize()));

                if ( warehouseMeatItemDetails.getHighlightText() != null && warehouseMeatItemDetails.getHighlightText().trim().length() > 0 ){
                    viewHolder.highlight.setVisibility(View.VISIBLE);
                    viewHolder.highlight.setText(warehouseMeatItemDetails.getHighlightText());
                }else{  viewHolder.highlight.setVisibility(View.GONE);  }

                if ( meatItemDetails.isPreOrderable() ){
                    viewHolder.pre_order_status.setVisibility(View.VISIBLE);
                    viewHolder.pre_order_status.setText("Preorder"+(meatItemDetails.getPreOrderCutoffTime()!=null&&meatItemDetails.getPreOrderCutoffTime().trim().length()>0?"\n"+DateMethods.getDateInFormat(meatItemDetails.getPreOrderCutoffTime(), DateConstants.HH_MM_A):""));
                }else{  viewHolder.pre_order_status.setVisibility(View.GONE);   }
                viewHolder.add_on.setVisibility(meatItemDetails.isAddon()?View.VISIBLE:View.GONE);

                if ( meatItemDetails.getEnglishNamesList().size() > 1 ){
                    List<String> remainNames = new ArrayList<>();
                    for (int i =1 ;i<meatItemDetails.getEnglishNamesList().size();i++){
                        remainNames.add(meatItemDetails.getEnglishNamesList().get(i));
                    }
                    viewHolder.multi_item_name.setVisibility(View.VISIBLE);
                    viewHolder.multi_item_name.setText(CommonMethods.getStringFromStringList(remainNames));
                }else{
                    viewHolder.multi_item_name.setVisibility(View.GONE);
                }

                Picasso.get()
                        .load(meatItemDetails.getThumbnailImageURL())
                        .placeholder(R.drawable.image_default_mastaan)
                        .error(R.drawable.image_default_mastaan)
                        .fit()
                        .centerCrop()
                        .tag(context)
                        .into(viewHolder.item_image);

                if ( warehouseMeatItemDetails.getAvailabilityStartTime() != null && warehouseMeatItemDetails.getAvailabilityStartTime().trim().length() > 0 ){
                    viewHolder.availability_start_time.setVisibility(View.VISIBLE);
                    viewHolder.availability_start_time.setText("Available from: "+DateMethods.getDateInFormat(warehouseMeatItemDetails.getAvailabilityStartTime(), DateConstants.HH_MM_A));
                }else{  viewHolder.availability_start_time.setVisibility(View.GONE);    }

                if ( warehouseMeatItemDetails.getAvailabilityEndTime() != null && warehouseMeatItemDetails.getAvailabilityEndTime().trim().length() > 0 ){
                    viewHolder.availability_end_time.setVisibility(View.VISIBLE);
                    viewHolder.availability_end_time.setText("Cur.Day Avail. Ends: "+DateMethods.getDateInFormat(warehouseMeatItemDetails.getAvailabilityEndTime(), DateConstants.HH_MM_A));
                }else{  viewHolder.availability_end_time.setVisibility(View.GONE);    }

                if ( meatItemDetails.getPreparationHours() > 0 ){
                    viewHolder.preparation_time.setVisibility(View.VISIBLE);
                    viewHolder.preparation_time.setText("Prep. time: " + DateMethods.getTimeStringFromMinutes((int) (meatItemDetails.getPreparationHours() * 60), "hr", "min"));
                }else{
                    viewHolder.preparation_time.setVisibility(View.GONE);
                }

                if ( warehouseMeatItemDetails.getSource() != null && warehouseMeatItemDetails.getSource().trim().length() > 0 ){
                    viewHolder.source.setVisibility(View.VISIBLE);
                    viewHolder.source.setText(CommonMethods.fromHtml("Source: <b>"+warehouseMeatItemDetails.getSource().trim()+"</b>"));
                }else{  viewHolder.source.setVisibility(View.GONE); }

                AvailabilityDetails availabilityDetails = warehouseMeatItemDetails.getAvailabilityDetailsForDate(todaysDate);
                viewHolder.unavailable_today_status.setVisibility(availabilityDetails.isAvailable()?View.GONE:View.VISIBLE);
                viewHolder.vendor_price.setText(SpecialCharacters.RS+" "+CommonMethods.getInDecimalFormat(availabilityDetails.getBuyingPrice())+"/"+meatItemDetails.getQuantityUnit()+" (V)");
                viewHolder.buyer_price.setText(SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(availabilityDetails.getSellingPrice()) + "/" + meatItemDetails.getQuantityUnit() + " (B)");
                if ( availabilityDetails.getActualSellingPrice() > availabilityDetails.getSellingPrice() ){
                    try{
                        viewHolder.buyer_price.setText(" "+SpecialCharacters.RS + " " +CommonMethods.getInDecimalFormat(availabilityDetails.getActualSellingPrice())+" ", TextView.BufferType.SPANNABLE);
                        ((Spannable) viewHolder.buyer_price.getText()).setSpan(new StrikethroughSpan(), 0, viewHolder.buyer_price.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        viewHolder.buyer_price.append(" "+SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(availabilityDetails.getSellingPrice())+"/"+meatItemDetails.getQuantityUnit()+ " (B)");
                    }catch (Exception e){}
                }

                viewHolder.minimum_weight.setText("Min: "+CommonMethods.getInDecimalFormat(meatItemDetails.getMinimumWeight())+" "+meatItemDetails.getQuantityUnit());

                if ( (warehouseMeatItemDetails.getStocks() != null && warehouseMeatItemDetails.getStocks().size() > 0)
                        || (warehouseMeatItemDetails.getHubStocks() != null && warehouseMeatItemDetails.getHubStocks().size() > 0) ){
                    viewHolder.stockDetailsView.setVisibility(View.VISIBLE);
                    if ( warehouseMeatItemDetails.getStocks() != null && warehouseMeatItemDetails.getStocks().size() > 0 ){
                        viewHolder.stock_details.setText(CommonMethods.fromHtml(warehouseMeatItemDetails.getFormattedStocksInfo()));
                    }else if (warehouseMeatItemDetails.getHubStocks() != null && warehouseMeatItemDetails.getHubStocks().size() > 0){
                        viewHolder.stock_details.setText(CommonMethods.fromHtml(warehouseMeatItemDetails.getFormattedHubStocksInfo()));
                    }else{viewHolder.stock_details.setText("");}
                }else{
                    viewHolder.stockDetailsView.setVisibility(View.GONE);
                }

                if ( showAvailabilityActions ){
                    viewHolder.availabilityActions.setVisibility(View.VISIBLE);
                    if ( warehouseMeatItemDetails.isAvailable() ){
                        viewHolder.markAsUnavailable.setVisibility(View.VISIBLE);
                        viewHolder.markAsAvailable.setVisibility(View.GONE);
                    }else{
                        viewHolder.markAsAvailable.setVisibility(View.VISIBLE);
                        viewHolder.markAsUnavailable.setVisibility(View.GONE);
                    }
                }else{
                    viewHolder.availabilityActions.setVisibility(View.GONE);
                }

                if ( warehouseMeatItemDetails.isEnabled() ){
                    viewHolder.fader.setVisibility(warehouseMeatItemDetails.isAvailable()?View.GONE:View.VISIBLE);
                    viewHolder.disabledIndicator.setVisibility(View.GONE);
                }else{
                    viewHolder.fader.setVisibility(View.VISIBLE);
                    viewHolder.disabledIndicator.setVisibility(View.VISIBLE);
                }

                //-----------------

                if ( viewHolder.itemSelector != null && callback != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onItemClick(position);
                        }
                    });
//                    viewHolder.fader.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            callback.onItemClick(position);
//                        }
//                    });
                    viewHolder.refreshStockDetails.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onRefreshStockDetailsClick(position);
                        }
                    });
                    viewHolder.showMeatItemDetails.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onShowMeatItemDetailsClick(position);
                        }
                    });
                    viewHolder.markAsAvailable.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onMarkAsAvailable(position);
                        }
                    });
                    viewHolder.markAsUnavailable.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onMarkAsUnavailable(position);
                        }
                    });
                }
            }
        }
    }

    public Callback getCallback() {
        return callback;
    }

    boolean showAvailabilityActions = false;
    public WarehouseMeatItemsAdapter showAvailabilityActions() {
        this.showAvailabilityActions = true;
        return this;
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<WarehouseMeatItemDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<WarehouseMeatItemDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(WarehouseMeatItemDetails warehouseMeatItemDetails){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(warehouseMeatItemDetails.getID()) ){
                itemsList.set(i, warehouseMeatItemDetails);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void setItem(int position, WarehouseMeatItemDetails warehouseMeatItemDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, warehouseMeatItemDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public WarehouseMeatItemDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<WarehouseMeatItemDetails> getAllItems(){
        return itemsList;
    }

}