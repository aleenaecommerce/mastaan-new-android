package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.StockAlertDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 27/07/18.
 */

public class StockAlertsAdapter extends RecyclerView.Adapter<StockAlertsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;

    List<StockAlertDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        TextView date;
        TextView details;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            date = (TextView) itemLayoutView.findViewById(R.id.date);
            details = (TextView) itemLayoutView.findViewById(R.id.details);
        }
    }

    public StockAlertsAdapter(MastaanToolbarActivity activity, List<StockAlertDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public StockAlertsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_stock_alert, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final StockAlertDetails stockAlertDetails = itemsList.get(position);
        if ( stockAlertDetails != null ){
            viewHolder.date.setText(DateMethods.getDateInFormat(stockAlertDetails.getDate(), DateConstants.MMM_DD_YYYY));

            String quantityUnit = stockAlertDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getQuantityUnit();
            viewHolder.details.setText(CommonMethods.fromHtml("<b>"+stockAlertDetails.getWarehouseMeatItemDetails().getMeatItemDetais().getNameWithCategoryAndSize()+"</b>'s total quantity <b>"+CommonMethods.getInDecimalFormat(stockAlertDetails.getQuantity())+" "+quantityUnit+(stockAlertDetails.getQuantity()==1?"":"s")+"</b> reached daily threshold of <b>"+CommonMethods.getInDecimalFormat(stockAlertDetails.getThresholdQuantity())+" "+quantityUnit+(stockAlertDetails.getThresholdQuantity()==1?"":"s")+"</b>"));

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position);
                    }
                });
            }
        }
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<StockAlertDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public StockAlertDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

}