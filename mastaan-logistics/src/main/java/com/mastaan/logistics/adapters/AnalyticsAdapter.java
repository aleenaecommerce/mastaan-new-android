package com.mastaan.logistics.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.AnalyticsDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 05/06/18.
 */

public class AnalyticsAdapter extends RecyclerView.Adapter<AnalyticsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    int layoutViewID;
    List<AnalyticsDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
        void onActionClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        TextView title;
        TextView details;
        TextView action;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            title = (TextView) itemLayoutView.findViewById(R.id.title);
            details = (TextView) itemLayoutView.findViewById(R.id.details);
            action = (TextView) itemLayoutView.findViewById(R.id.action);
        }
    }

    public AnalyticsAdapter(MastaanToolbarActivity activity, List<AnalyticsDetails> analyticsList, CallBack callBack) {
        this(activity, R.layout.view_analytics, analyticsList, callBack);
    }
    public AnalyticsAdapter(MastaanToolbarActivity activity, int layoutViewID, List<AnalyticsDetails> analyticsList, CallBack callBack) {
        this.activity = activity;
        this.context = activity;
        this.layoutViewID = layoutViewID;
        this.itemsList = analyticsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AnalyticsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(layoutViewID, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final AnalyticsDetails analyticsDetails = itemsList.get(position);

        viewHolder.title.setText(analyticsDetails.getTitle());
        viewHolder.details.setText(Html.fromHtml(analyticsDetails.getDetails()));

        if ( analyticsDetails.getAction() != null ){
            viewHolder.action.setVisibility(View.VISIBLE);
            viewHolder.action.setText(analyticsDetails.getAction());
        }else{
            viewHolder.action.setVisibility(View.GONE);
        }

        if ( callBack != null ){
            viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemClick(position);
                }
            });
            viewHolder.action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onActionClick(position);
                }
            });
        }
    }

    //---------------

    public void setItems(List<AnalyticsDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<AnalyticsDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public AnalyticsDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public List<AnalyticsDetails> getAllItems(){
        return itemsList;
    }

}