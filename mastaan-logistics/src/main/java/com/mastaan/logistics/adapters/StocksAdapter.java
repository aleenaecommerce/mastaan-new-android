package com.mastaan.logistics.adapters;

import android.content.Context;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.StockDetails;

import java.util.List;

public class StocksAdapter extends RecyclerView.Adapter<StocksAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<StockDetails> itemsList;

    HubDetails hubDetails;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onOrderItemClick(int position);
        void onStockClick(int position);
    }

    public StocksAdapter setHubDetails(HubDetails hubDetails) {
        this.hubDetails = hubDetails;
        return this;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        CardView container;

        TextView type_indicator;
        TextView item_name;
        TextView date;
        TextView order_item;
        TextView item_details;
        TextView stock_details;
        TextView hub_details;
        TextView to_hub_details;
        View amountsView;
        TextView margin_details;
        TextView total_details;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            container = itemView.findViewById(R.id.container);

            type_indicator = itemLayoutView.findViewById(R.id.type_indicator);
            item_name = itemLayoutView.findViewById(R.id.item_name);
            date = itemLayoutView.findViewById(R.id.date);
            order_item = itemLayoutView.findViewById(R.id.order_item);
            item_details = itemLayoutView.findViewById(R.id.item_details);
            stock_details = itemLayoutView.findViewById(R.id.stock_details);
            hub_details = itemLayoutView.findViewById(R.id.hub_details);
            to_hub_details = itemLayoutView.findViewById(R.id.to_hub_details);
            amountsView = itemLayoutView.findViewById(R.id.amountsView);
            margin_details = itemLayoutView.findViewById(R.id.margin_details);
            total_details = itemLayoutView.findViewById(R.id.total_details);
        }
    }

    public StocksAdapter(MastaanToolbarActivity activity, List<StockDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public StocksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_stock, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final StockDetails stockDetails = itemsList.get(position);
        if ( stockDetails != null ){

            if ( stockDetails.getType().toLowerCase().contains("consolidation") ){
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.delivered_color));
                viewHolder.type_indicator.setText("C");
            }else if ( stockDetails.getType().toLowerCase().contains("adjustment") ){
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.picked_up_color));
                viewHolder.type_indicator.setText("A");
            }else if ( stockDetails.getType().toLowerCase().contains("order") ){
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.assigned_color));
                viewHolder.type_indicator.setText("O");
            }else if ( stockDetails.getType().toLowerCase().contains("transfer in") ){
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.delivered_color_fab));
                if ( hubDetails != null && stockDetails.getHubID() != null && hubDetails.getID().equalsIgnoreCase(stockDetails.getHubID()) ){
                    viewHolder.type_indicator.setText("IN");
                }else{
                    viewHolder.type_indicator.setText("OUT");
                }
            }else if ( stockDetails.getType().toLowerCase().contains("transfer out") ){
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.quality_check_completed_color));
                if ( hubDetails != null && stockDetails.getToHubDetails() != null && hubDetails.getID().equalsIgnoreCase(stockDetails.getToHubDetails().getID()) ){
                    viewHolder.type_indicator.setText("IN");
                }else if ( hubDetails != null && stockDetails.getHubID() != null && hubDetails.getID().equalsIgnoreCase(stockDetails.getHubID()) ){
                    viewHolder.type_indicator.setText("OUT");
                }else{
                    viewHolder.type_indicator.setText("TO");
                }
            }else if ( stockDetails.getType().toLowerCase().contains("input") ){
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.acknowledged_color));
                viewHolder.type_indicator.setText("I");
            }else if ( stockDetails.getType().toLowerCase().contains("wastage") ){
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.failed_color));
                viewHolder.type_indicator.setText("W");
            }else{
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.processed_color_fab));
                viewHolder.type_indicator.setText("P");
            }

            if ( stockDetails.getCategoryDetails() != null ) {
                viewHolder.item_name.setText(stockDetails.getCategoryDetails().getNameWithParent());
            }else if ( stockDetails.getWarehouseMeatItemDetails() != null ) {
                viewHolder.item_name.setText(stockDetails.getFormattedMeatItemNameWithAttributeOptions());//getWarehouseMeatItemDetails().getMeatItemDetais().getNameWithCategoryAndSize());
            }else{
                viewHolder.item_name.setText("ITEM NAME");
            }

            viewHolder.date.setText(DateMethods.getReadableDateFromUTC(stockDetails.getDate(), DateConstants.MMM_DD_HH_MM_A).replaceAll(", ", "\n"));

            //-------

            String quantityUnit = stockDetails.getQuantityUnit();

            if ( stockDetails.getOrderItemID() != null ){
                viewHolder.order_item.setVisibility(View.VISIBLE);
                viewHolder.order_item.setText(Html.fromHtml("ORDER ITEM: <b><u>"+stockDetails.getOrderItemID()+"</u></b>"));
            }else{  viewHolder.order_item.setVisibility(View.GONE); }


            String itemDetails =  "QUANTITY:  <b>" + getUnitsString(stockDetails.getQuantity(), quantityUnit)+"</b>";
            if ( stockDetails.getNoOfUnits() != 0 ) {
                itemDetails += "  ("+getUnitsString(stockDetails.getNoOfUnits(), "unit")+")</b>";
            }
            if (stockDetails.getWastageQuantity() > 0){
                itemDetails += "<br>WASTAGE:  <b>" + getUnitsString(stockDetails.getWastageQuantity(), quantityUnit)+"</b> "+("(<b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(stockDetails.getWastageQuantity()*stockDetails.getVendorRate())+"</b>)");
            }

            if ( stockDetails.getPackagingCost() > 0 ){
                itemDetails += "<br>PACKAGING COST:  <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(stockDetails.getPackagingCost())+"</b>"/*+")"*/;
            }
            if ( stockDetails.getShippingCost() > 0 ){
                itemDetails += "<br>SHIPPING COST:  <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(stockDetails.getShippingCost())+"</b>"/*+")"*/;
            }
            if ( stockDetails.getOtherCosts() > 0 ){
                itemDetails += "<br>OTHER COSTS:  <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(stockDetails.getOtherCosts())+"</b>"/*+")"*/;
            }

            if ( stockDetails.getVendorDetails() != null || stockDetails.getVendorRate() > 0 ) {
                String vendorDetails = "";
                if ( stockDetails.getVendorDetails() != null ){    vendorDetails = "<br>VENDOR:  <b>"+stockDetails.getVendorDetails().getName()+"</b>"; }
                if ( stockDetails.getVendorRate() > 0 ){
                    if ( vendorDetails.length() == 0 ){
                        vendorDetails += "<br>VENDOR:  <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(stockDetails.getVendorRate()*stockDetails.getQuantity())+" , "+SpecialCharacters.RS +" "+CommonMethods.getIndianFormatNumber(stockDetails.getVendorRate())+"/"+quantityUnit+"</b>";
                    }else{
                        vendorDetails += " (<b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(stockDetails.getVendorRate()*stockDetails.getQuantity())+" , "+SpecialCharacters.RS +" "+CommonMethods.getIndianFormatNumber(stockDetails.getVendorRate())+"/"+quantityUnit+"</b>)";
                    }
                }
                itemDetails += vendorDetails;
            }

            if ( stockDetails.getFarmRate() > 0 ) {
                itemDetails += "<br>FARM RATE:  <b>" + SpecialCharacters.RS +" "+CommonMethods.getIndianFormatNumber(stockDetails.getFarmRate())+" / "+quantityUnit+"</b>";
            }
            itemDetails = itemDetails.toUpperCase();
            if ( stockDetails.getSource() != null && stockDetails.getSource().trim().length() > 0 ){
                itemDetails += "<br>SOURCE: <b>"+stockDetails.getSource()+"</b>";
            }
            if ( stockDetails.getProcessedBy() != null && stockDetails.getProcessedBy().size() > 0 ){
                itemDetails += "<br>PROCESSED BY: <b>"+stockDetails.getProcessedByNames()+"</b>";
            }
            if ( stockDetails.getQualityCheckBy() != null ){
                itemDetails += "<br>QC BY: <b>"+stockDetails.getQualityCheckBy().getAvailableName()+"</b>";
            }
            if ( stockDetails.getComments() != null && stockDetails.getComments().trim().length() > 0 ){
                itemDetails += "<br>COMMENTS: <b>"+stockDetails.getComments().trim()+"</b>";
            }
            itemDetails += "<br><br>CREATED:  <b>"+stockDetails.getCreatedBy().getAvailableName().toUpperCase()+"</b> ("+DateMethods.getReadableDateFromUTC(stockDetails.getCreatedDate(), DateConstants.MMM_DD_HH_MM_A)+")";
            if ( (stockDetails.getUpdatedBy() != null && stockDetails.getUpdatedBy().getID().length() > 0) || (stockDetails.getType().equalsIgnoreCase(Constants.MEAT_ITEM_ORDER) && stockDetails.isUpdated()) ){
                itemDetails += "<br>UPDATED: <b>"+(stockDetails.getUpdatedBy()!=null?stockDetails.getUpdatedBy().getAvailableName().toUpperCase():"")+"</b> ("+DateMethods.getReadableDateFromUTC(stockDetails.getUpdatedDate(), DateConstants.MMM_DD_HH_MM_A)+")";
            }

            viewHolder.item_details.setText(Html.fromHtml(itemDetails));


            if ( stockDetails.getLinkedStock() != null || stockDetails.getLinkedProcessedStock() != null ){
                viewHolder.stock_details.setVisibility(View.VISIBLE);
                viewHolder.stock_details.setText(CommonMethods.fromHtml("LINKED STOCK: <b><u>"+(stockDetails.getLinkedStock()!=null?stockDetails.getLinkedStock():stockDetails.getLinkedProcessedStock())+"</u></b>"));
            }else{  viewHolder.stock_details.setVisibility(View.GONE);  }

            if ( stockDetails.getHubDetails() != null && (hubDetails == null || hubDetails.getID().equals(stockDetails.getHubDetails().getID()) == false) ){
                viewHolder.hub_details.setVisibility(View.VISIBLE);
                viewHolder.hub_details.setText(CommonMethods.fromHtml("HUB: <b>"+stockDetails.getHubDetails().getName().toUpperCase()+"</u>"));
            }else{  viewHolder.hub_details.setVisibility(View.GONE); }

            if ( stockDetails.getToHubDetails() != null ){
                viewHolder.to_hub_details.setVisibility(View.VISIBLE);
                viewHolder.to_hub_details.setText(CommonMethods.fromHtml("TO HUB: <b>"+stockDetails.getToHubDetails().getName().toUpperCase()+"</b>"
                        +"<br>COLLECTED BY: <b>"+(stockDetails.getCollectedBy()!=null?stockDetails.getCollectedBy().getAvailableName():"")
                ));
            }else{  viewHolder.to_hub_details.setVisibility(View.GONE); }


            if ( stockDetails.getTotalCost() > 0 || stockDetails.getVendorRate() > 0 ){
                viewHolder.amountsView.setVisibility(View.VISIBLE);

                // Total details
                if ( stockDetails.getTotalCost() > 0 ) {
                    String totalDetails = "Total: " + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(stockDetails.getTotalCost());
                    if ( stockDetails.getQuantity() > 0 ) {
                        totalDetails += "\nFinal: " + CommonMethods.getIndianFormatNumber(stockDetails.getTotalCost() / (stockDetails.getQuantity()-stockDetails.getWastageQuantity())) + "/" + quantityUnit;
                    }
                    /*if ( stockDetails.getVendorRate() > 0 ){
                        totalDetails += "\nVendor: "+SpecialCharacters.RS +" "+CommonMethods.getIndianFormatNumber(stockDetails.getVendorRate())+"/"+quantityUnit;
                    }*/
                    viewHolder.total_details.setText(totalDetails);
                }else{
                    viewHolder.total_details.setText("");
                }

                // Margin details
                if ( stockDetails.getMargin() > 0 && stockDetails.getQuantity() > 0 ) {
                    double finalCost = stockDetails.getTotalCost() / (stockDetails.getQuantity()-stockDetails.getWastageQuantity());
                    long marginalCost = Math.round(finalCost + (finalCost*(stockDetails.getMargin()/100)));
                    viewHolder.margin_details.setText("Margin ("+CommonMethods.getInDecimalFormat(stockDetails.getMargin())+"%): "+SpecialCharacters.RS +" "+CommonMethods.getIndianFormatNumber(marginalCost)+"/"+quantityUnit);
                }else{
                    viewHolder.margin_details.setText("");
                }
            }else{
                viewHolder.amountsView.setVisibility(View.GONE);
            }

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position);
                    }
                });
                viewHolder.order_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onOrderItemClick(position);
                    }
                });
                viewHolder.stock_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onStockClick(position);
                    }
                });

                viewHolder.stock_details.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if ( stockDetails.getLinkedStock() != null ) {
                            activity.copyTextToClipboard(stockDetails.getLinkedStock());
                        }else if ( stockDetails.getLinkedProcessedStock() != null ) {
                            activity.copyTextToClipboard(stockDetails.getLinkedProcessedStock());
                        }
                        return false;
                    }
                });
            }
        }
    }

    private String getUnitsString(double number, String unit){
        String string = CommonMethods.getIndianFormatNumber(number)+" "+unit;
        if ( number != 1 ){ string += "s";  }
        return string;
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<StockDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<StockDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(int position, StockDetails stockDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, stockDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public StockDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<StockDetails> getAllItems(){
        return itemsList;
    }

}