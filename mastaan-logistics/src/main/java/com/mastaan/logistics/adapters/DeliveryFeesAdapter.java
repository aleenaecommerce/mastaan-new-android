package com.mastaan.logistics.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.models.DeliveryFeeDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 28-01-2018.
 */

public class DeliveryFeesAdapter extends RecyclerView.Adapter<DeliveryFeesAdapter.ViewHolder> {

    Context context;
    List<DeliveryFeeDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        TextView details;
        View actionDelete;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            details = (TextView) itemLayoutView.findViewById(R.id.details);
            actionDelete = itemLayoutView.findViewById(R.id.actionDelete);
        }
    }

    public DeliveryFeesAdapter(Context context, List<DeliveryFeeDetails> itemsList, Callback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public DeliveryFeesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_delivery_fee, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final DeliveryFeeDetails deliveryFeeDetails = itemsList.get(position);

        if ( deliveryFeeDetails != null ){
            viewHolder.details.setText(Html.fromHtml(CommonMethods.getInDecimalFormat(deliveryFeeDetails.getMinDistance())+"km to "+CommonMethods.getInDecimalFormat(deliveryFeeDetails.getMaxDistance())+"km : <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(deliveryFeeDetails.getFee())+"</b>"));

            if ( callback != null ) {
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position);
                    }
                });
                viewHolder.actionDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onDeleteClick(position);
                    }
                });
            }
        }
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<DeliveryFeeDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItem(DeliveryFeeDetails deliveryFeeDetails){
        itemsList.add(deliveryFeeDetails);
        notifyDataSetChanged();
    }

    public void setItem(int position, DeliveryFeeDetails deliveryFeeDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, deliveryFeeDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public DeliveryFeeDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<DeliveryFeeDetails> getAllItems(){
        return itemsList;
    }

}