package com.mastaan.logistics.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.QRCodeDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 17/06/18.
 */

public class QRCodesAdapter extends RecyclerView.Adapter<QRCodesAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    List<QRCodeDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemSelector;
        TextView name;
        ImageView image;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            image = (ImageView) itemLayoutView.findViewById(R.id.image);
        }
    }

    public QRCodesAdapter(MastaanToolbarActivity activity, List<QRCodeDetails> qrCodesList, CallBack callBack) {
        this.activity = activity;
        this.itemsList = qrCodesList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public QRCodesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_qr_code, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final QRCodeDetails qrCodeDetails = itemsList.get(position);

        if ( qrCodeDetails != null ){
            viewHolder.name.setText(qrCodeDetails.getName());

            Picasso.get().load(qrCodeDetails.getImage()).into(viewHolder.image);

            if ( callBack != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
            }
        }
    }

    //---------------

    public void setItems(List<QRCodeDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public QRCodeDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public List<QRCodeDetails> getAllItems(){
        return itemsList;
    }

}