package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.DeliveryAreaDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 30/03/17.
 */

public class DeliveryAreasAdapter extends RecyclerView.Adapter<DeliveryAreasAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<DeliveryAreaDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        TextView name;
        TextView details;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            details = (TextView) itemLayoutView.findViewById(R.id.details);
        }
    }

    public DeliveryAreasAdapter(MastaanToolbarActivity activity, List<DeliveryAreaDetails> areasList, CallBack callBack) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = areasList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public DeliveryAreasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_delivery_area, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final DeliveryAreaDetails deliveryAreaDetails = itemsList.get(position);

        if ( deliveryAreaDetails != null ){

            if ( deliveryAreaDetails.getCount() > 0 ){      // TODO TEMP
                viewHolder.details.setVisibility(View.GONE);        // TODO TEMP
                viewHolder.name.setText(deliveryAreaDetails.getID()+" ("+deliveryAreaDetails.getCount()+")");
            }else{
                viewHolder.details.setVisibility(View.VISIBLE);        // TODO TEMP
                viewHolder.name.setText(deliveryAreaDetails.getCode());
                viewHolder.details.setText(deliveryAreaDetails.getName());
            }

            if ( callBack != null ) {
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
            }
        }
    }

    //---------------

    public void setItems(List<DeliveryAreaDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<DeliveryAreaDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void addItem(DeliveryAreaDetails deliveryAreaDetails){
        itemsList.add(deliveryAreaDetails);
        notifyDataSetChanged();
    }

    public void setItem(int position, DeliveryAreaDetails deliveryAreaDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, deliveryAreaDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public DeliveryAreaDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<DeliveryAreaDetails> getAllItems(){
        return itemsList;
    }

}