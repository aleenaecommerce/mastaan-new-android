package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.CartDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 03/03/19.
 */

public class CartsAdapter extends RecyclerView.Adapter<CartsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<CartDetails> itemsList;
    Callback callback;

    public interface Callback {
        void onClick(int position);
        void onCallCustomer(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemSelector;
        TextView title;
        TextView subtitle;
        TextView details;
        View callCustomer;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            title = itemLayoutView.findViewById(R.id.title);
            subtitle = itemLayoutView.findViewById(R.id.subtitle);
            details = itemLayoutView.findViewById(R.id.details);
            callCustomer = itemLayoutView.findViewById(R.id.callCustomer);
        }
    }

    public CartsAdapter(MastaanToolbarActivity activity, List<CartDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = itemsList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public CartsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_cart, null);
        return (new ViewHolder(itemView));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final CartDetails cartDetails = itemsList.get(position);
        
        if ( cartDetails != null ){
            viewHolder.title.setText(cartDetails.getBuyerDetails().getAvailableName());

            String datesInfo = "Cart updated: <b>"+DateMethods.getReadableDateFromUTC(cartDetails.getUpdatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b>";
            if ( cartDetails.getBuyerDetails().getLastOrderDate() != null && cartDetails.getBuyerDetails().getLastOrderDate().length() > 0 ){
                datesInfo += "<br>" + "Last order: <b>"+DateMethods.getReadableDateFromUTC(cartDetails.getBuyerDetails().getLastOrderDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b>";
            }
            viewHolder.subtitle.setText(CommonMethods.fromHtml(datesInfo));

            String detailsInfo = "Items (<b>"+cartDetails.getItemsCount()+"</b>): <b>" + cartDetails.getItemsNames() +"</b>";
            if ( cartDetails.getFollowupEmployee() != null ){
                detailsInfo += "<br>"+"Followup claimed: <b>"+cartDetails.getFollowupEmployee().getAvailableName()+"</b>";
            }
            if ( cartDetails.getComments() != null && cartDetails.getComments().trim().length() > 0 ){
                detailsInfo += "<br>"+"Comments: <b>"+cartDetails.getComments().trim()+"</b>";
            }
            viewHolder.details.setText(CommonMethods.fromHtml(detailsInfo));

            //------

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onClick(position);
                    }
                });
                viewHolder.callCustomer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onCallCustomer(position);
                    }
                });
            }
        }
    }

    //---------------

    public void setItems(List<CartDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<CartDetails> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(int position, CartDetails cartDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, cartDetails);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }


    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public CartDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public List<CartDetails> getAllItems(){
        return itemsList;
    }

}