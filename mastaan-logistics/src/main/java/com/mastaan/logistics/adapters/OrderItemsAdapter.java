package com.mastaan.logistics.adapters;

import android.content.Context;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.methods.ActivityMethods;
import com.mastaan.logistics.models.AppliedDiscountDetails;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.UserDetailsMini;
import com.mastaan.logistics.models.Vendor;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 29/1/16.
 */
public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    LocalStorageData localStorageData;
    UserDetails userDetails;
    List<OrderItemDetails> itemsList;

    boolean showOrderDetails = true;
    boolean showOrderStatusTimes = true;

    public void setShowOrderDetails(boolean showOrderDetails) {
        this.showOrderDetails = showOrderDetails;
    }

    public void setShowOrderStatusTimes(boolean showOrderStatusTimes) {
        this.showOrderStatusTimes = showOrderStatusTimes;
    }

    Callback callback;

    CheckCallback checkCallback;

    public interface CheckCallback {
        void onCheckChanged(int position, boolean isChecked);

        void onShowOrderDetails(int position);
    }

    public void setCheckCallback(CheckCallback checkCallback) {
        this.checkCallback = checkCallback;
    }

    public interface Callback {

        void onItemClick(int position);

        void onItemLongClick(int position);

        void onCallCustomer(int position);

        void onMessageCustomer(int position);

        void onShowDirections(int position);

        void onShowBuyerHistory(int position);

        void onShowReferralBuyerDetails(int position);

        void onPrintBill(int position);

        void onTrackOrder(int position);

        void onShowOrderDetails(int position);

        void onShowMoneyCollectionDetails(int position);

        void onCheckPaymentStatus(int position);

        void onRequestOnlinePayment(int position);

        void onShowItemImage(int position);

        void onShowStockDetails(int position);

        void onShowFollowupResultedInOrder(int position);

        void onAcknowledge(int position);

        //void onShowProcessingOptions(int position);
        void onStartProcessing(int position);

        void onCompleteProcessing(int position);

        void onAssignDeliveryBoy(int position);

        void onCompleteDelivery(int position);

        void onReProcess(int position);

        void onRejectedItemsHandling(int position);

        void onCompleteCounterSaleOrder(int position);

        //void onRemoveFromCustomerSupport(int position);
        void onClaimForCustomerSupport(int position);

        void onCompleteCustomerSupport(int position);
    }

    public OrderItemsAdapter(MastaanToolbarActivity activity, List<OrderItemDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.localStorageData = new LocalStorageData(context);
        this.userDetails = localStorageData.getUserDetails();
        this.itemsList = itemsList;
        this.callback = callback;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View fullDetailsView;
        View itemSelector;
        CardView container;

        LinearLayout headerContainer;
        TextView status_header;
        TextView source;
        TextView card_title;
        TextView item_index;
        View printBill;

        View first_order;
        TextView pre_order_status;
        TextView referral_order;

        ImageView item_image;
        TextView item_name;
        TextView item_quantity;
        TextView item_extras;
        View vendorDetailsView;
        TextView vendor_details;
        TextView cash_n_carry_status;
        View stockDetailsView;
        TextView stock_details;

        View customerSupportIndicator;

        View orderDetailsView;
        TextView customer_name;
        TextView membership_type;
        TextView last_followup_date;
        TextView customer_mobile;
        TextView customer_address;
        TextView customer_land_mark;

        View actionHolderSeparator;
        View actionsHolder;
        FloatingActionButton callCustomer;
        FloatingActionButton messageCustomer;
        FloatingActionButton showDirections;
        FloatingActionButton showBuyerHistory;

        TextView item_cost;
        TextView order_cost;
        View checkPaymentStatus;
        View requestOnlinePayment;
        View buyerOutstandingDetails;
        TextView buyer_outstanding;
        //View discountDetailsView;
        //TextView order_discount;
        View orderCreatorDetailsView;
        TextView order_creator;
        View discountsView;
        TextView discount_details;

        View specialInstructionsHolder;
        TextView special_instructions;

        View orderStatusTimesHolder;
        TextView order_status_times;
        View orderStatusReasonsHolder;
        TextView order_status_reasons;

        TextView order_status;
        TextView delivery_hub;
        TextView delivery_time;
        View showFollowupDetails;
        View trackOrder;

        //----

        View checkBoxView;
        CheckBox checkbox;
        TextView checkbox_details;
        TextView checkbox_delivery_hub;
        TextView checkbox_delivery_time;
        View showOrderDetails;

        public ViewHolder(View itemView) {
            super(itemView);

            fullDetailsView = itemView.findViewById(R.id.fullDetailsView);
            itemSelector = itemView.findViewById(R.id.itemSelector);
            container = itemView.findViewById(R.id.container);

            headerContainer = itemView.findViewById(R.id.headerContainer);
            status_header = itemView.findViewById(R.id.status_header);
            source = itemView.findViewById(R.id.source);
            card_title = itemView.findViewById(R.id.card_title);
            item_index = itemView.findViewById(R.id.item_index);
            printBill = itemView.findViewById(R.id.printBill);

            first_order = itemView.findViewById(R.id.first_order);
            pre_order_status = itemView.findViewById(R.id.pre_order_status);
            referral_order = itemView.findViewById(R.id.referral_order);

            orderDetailsView = itemView.findViewById(R.id.orderDetailsView);
            customer_name = itemView.findViewById(R.id.customer_name);
            membership_type = itemView.findViewById(R.id.membership_type);
            last_followup_date = itemView.findViewById(R.id.last_followup_date);
            customer_mobile = itemView.findViewById(R.id.customer_mobile);
            customer_address = itemView.findViewById(R.id.customer_address);
            customer_land_mark = itemView.findViewById(R.id.customer_land_mark);

            actionHolderSeparator = itemView.findViewById(R.id.actionHolderSeparator);
            actionsHolder = itemView.findViewById(R.id.actionsHolder);
            callCustomer = itemView.findViewById(R.id.callCustomer);
            messageCustomer = itemView.findViewById(R.id.messageCustomer);
            showDirections = itemView.findViewById(R.id.showDirections);
            showBuyerHistory = itemView.findViewById(R.id.showBuyerHistory);

            item_cost = itemView.findViewById(R.id.item_cost);
            order_cost = itemView.findViewById(R.id.order_cost);
            checkPaymentStatus = itemView.findViewById(R.id.checkPaymentStatus);
            requestOnlinePayment = itemView.findViewById(R.id.requestOnlinePayment);
            buyerOutstandingDetails = itemView.findViewById(R.id.buyerOutstandingDetails);
            buyer_outstanding = itemView.findViewById(R.id.buyer_outstanding);
            //discountDetailsView = itemView.findViewById(R.id.discountDetailsView);
            //order_discount = itemView.findViewById(R.id.order_discount);
            orderCreatorDetailsView = itemView.findViewById(R.id.orderCreatorDetailsView);
            order_creator = itemView.findViewById(R.id.order_creator);
            discountsView = itemView.findViewById(R.id.discountsView);
            discount_details = itemView.findViewById(R.id.discount_details);

            item_image = itemView.findViewById(R.id.item_image);
            item_name = itemView.findViewById(R.id.item_name);
            item_quantity = itemView.findViewById(R.id.item_quantity);
            item_extras = itemView.findViewById(R.id.item_preferences);
            vendorDetailsView = itemView.findViewById(R.id.vendorDetailsView);
            vendor_details = itemView.findViewById(R.id.vendor_details);
            specialInstructionsHolder = itemView.findViewById(R.id.specialInstructionsHolder);
            special_instructions = itemView.findViewById(R.id.special_instructions);
            cash_n_carry_status = itemView.findViewById(R.id.cash_n_carry_status);
            stockDetailsView = itemView.findViewById(R.id.stockDetailsView);
            stock_details = itemView.findViewById(R.id.stock_details);

            customerSupportIndicator = itemView.findViewById(R.id.customerSupportIndicator);


            orderStatusTimesHolder = itemView.findViewById(R.id.orderStatusTimesHolder);
            order_status_times = itemView.findViewById(R.id.order_status_times);
            orderStatusReasonsHolder = itemView.findViewById(R.id.orderStatusReasonsHolder);
            order_status_reasons = itemView.findViewById(R.id.order_status_reasons);
            order_status = itemView.findViewById(R.id.order_status);
            delivery_hub = itemView.findViewById(R.id.delivery_hub);
            delivery_time = itemView.findViewById(R.id.delivery_time);
            showFollowupDetails = itemView.findViewById(R.id.showFollowupDetails);
            trackOrder = itemView.findViewById(R.id.trackOrder);

            //----

            checkBoxView = itemView.findViewById(R.id.checkBoxView);
            checkbox = itemView.findViewById(R.id.checkbox);
            checkbox_details = itemView.findViewById(R.id.checkbox_details);
            checkbox_delivery_hub = itemView.findViewById(R.id.checkbox_delivery_hub);
            checkbox_delivery_time = itemView.findViewById(R.id.checkbox_delivery_time);
            showOrderDetails = itemView.findViewById(R.id.showOrderDetails);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.view_order_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        try {
            final OrderItemDetails orderItemDetails = itemsList.get(position);

            if (orderItemDetails != null) {
                final String orderItemStatus = orderItemDetails.getStatusString();
                final OrderDetails2 orderDetails = orderItemDetails.getOrderDetails();
                BuyerDetails buyerDetails = orderItemDetails.getOrderDetails().getBuyerDetails();

                //ACKNOWLEDGED
                if (orderItemStatus.equalsIgnoreCase(Constants.ACKNOWLEDGED)) {
                    viewHolder.status_header.setText(" A ");
                    setColors(viewHolder, R.color.acknowledged_color, R.color.acknowledged_color_fab);
                }
                //PROCESSING
                else if (orderItemStatus.equalsIgnoreCase(Constants.PROCESSING)) {
                    viewHolder.status_header.setText(" PR ");
                    setColors(viewHolder, R.color.processing_color, R.color.processing_color_fab);
                }
                //PROCESSED
                else if (orderItemStatus.equalsIgnoreCase(Constants.PROCESSED)) {
                    if (orderItemDetails.getQualityCheckStatus() == Constants.QC_DONE) {
                        viewHolder.status_header.setText(" QCD ");
                        setColors(viewHolder, R.color.quality_check_completed_color, R.color.processed_color_fab);
                    } else if (orderItemDetails.getQualityCheckStatus() == Constants.QC_FAILED) {
                        viewHolder.status_header.setText(" QCF ");
                        setColors(viewHolder, R.color.quality_check_failed_color, R.color.processed_color_fab);
                    } else {
                        viewHolder.status_header.setText(" PRD ");
                        setColors(viewHolder, R.color.processed_color, R.color.processed_color_fab);
                    }
                }
                //ASSIGNED
                else if (orderItemStatus.equalsIgnoreCase(Constants.ASSIGNED)) {
                    viewHolder.status_header.setText(" ASD ");
                    setColors(viewHolder, R.color.assigned_color, R.color.assigned_color_fab);
                }
                //PICKED UP
                else if (orderItemStatus.equalsIgnoreCase(Constants.PICKED_UP)) {
                    viewHolder.status_header.setText(" PUD ");
                    setColors(viewHolder, R.color.picked_up_color, R.color.picked_up_color_fab);
                }
                //DELIVERING
                else if (orderItemStatus.equalsIgnoreCase(Constants.DELIVERING)) {
                    viewHolder.status_header.setText(" DEL ");
                    setColors(viewHolder, R.color.delivering_color, R.color.delivering_color_fab);
                }
                //DELIVERED
                else if (orderItemStatus.equalsIgnoreCase(Constants.DELIVERED)) {
                    viewHolder.status_header.setText(" D ");
                    setColors(viewHolder, R.color.delivered_color, R.color.delivered_color_fab);
                }
                //REJECTED
                else if (orderItemStatus.equalsIgnoreCase(Constants.REJECTED)) {
                    viewHolder.status_header.setText(" R ");
                    setColors(viewHolder, R.color.rejected_color, R.color.rejected_color_fab);
                }
                //FAILED
                else if (orderItemStatus.equalsIgnoreCase(Constants.FAILED)) {
                    viewHolder.status_header.setText(" F ");
                    setColors(viewHolder, R.color.failed_color, R.color.failed_color_fab);
                }
                //ORDERED
                else {
                    viewHolder.status_header.setText(" O ");
                    setColors(viewHolder, R.color.default_color, R.color.default_color_fab);
                }

                if (orderItemDetails.isInDeliveryBucket() && ActivityMethods.isAssignDeliveriesActivity(activity)) {
                    setColors(viewHolder, R.color.delivery_bucket_color, R.color.delivery_bucket_fab_color);
                }

                //--------

                if (orderItemDetails.isShowInCheckBoxView() && ActivityMethods.isAssignDeliveriesActivity(activity)) {
                    viewHolder.checkBoxView.setVisibility(View.VISIBLE);
                    viewHolder.fullDetailsView.setVisibility(View.GONE);
                    viewHolder.checkbox.setText(orderDetails.getFormattedOrderID() + "  (" + orderItemDetails.getItemLocation() + ")  " + orderItemDetails.getMeatItemDetails().getParentCategory());
                    viewHolder.checkbox_details.setText(Html.fromHtml("<b><u>" + orderDetails.getCustomerName() + "'s</u></b> " + orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize() + "<br>" + orderDetails.getDeliveryAddress()));

                    viewHolder.delivery_hub.setText(orderDetails.getHubDetails() != null ? "Hub - " + orderDetails.getHubDetails().getName() : "HUB NOT ASSIGNED");

                    // FOR FUTURE ORDERS
                    if (ActivityMethods.isCustomerSupportActivity(activity) || ActivityMethods.isUnprocessedCustomerSupportActivity(activity)
                            || DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate())), DateMethods.getOnlyDate(localStorageData.getServerTime())) > 0) {
                        viewHolder.checkbox_delivery_time.setText(DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()), DateConstants.MMM_DD) + ", " + orderItemDetails.getDeliveryTimeSlot());
                    } else {
                        viewHolder.checkbox_delivery_time.setText("Time: " + orderItemDetails.getDeliveryTimeSlot());
                    }

                    viewHolder.checkbox.setChecked(orderItemDetails.isInDeliveryBucket());
                    if (checkCallback != null) {
                        viewHolder.checkBoxView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (orderItemStatus.equalsIgnoreCase(Constants.PROCESSED)) {
                                    if (viewHolder.checkbox.isChecked()) {
                                        viewHolder.checkbox.setChecked(false);
                                        checkCallback.onCheckChanged(position, false);
                                    } else {
                                        viewHolder.checkbox.setChecked(true);
                                        checkCallback.onCheckChanged(position, true);
                                    }
                                    //viewHolder.checkbox.setChecked(viewHolder.checkbox.isChecked()?false:true);
                                }
                            }
                        });
                        viewHolder.showOrderDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                checkCallback.onShowOrderDetails(position);
                            }
                        });
                    }

                } else {
                    viewHolder.fullDetailsView.setVisibility(View.VISIBLE);
                    viewHolder.checkBoxView.setVisibility(View.GONE);

                    if (ActivityMethods.isFutureOrdersActivity(activity) || ActivityMethods.isOrdersByDateActivity(activity)
                            || orderItemDetails.getOrderDetails().isBillPrinted()
                            || orderItemStatus.equalsIgnoreCase(Constants.DELIVERED)
                            || orderItemStatus.equalsIgnoreCase(Constants.FAILED)
                            || orderItemStatus.equalsIgnoreCase(Constants.REJECTED)) {
                        //viewHolder.headerContainer.setBackgroundResource(android.R.color.transparent);
                        viewHolder.printBill.setVisibility(View.GONE);
                    } else {
                        //viewHolder.headerContainer.setBackgroundResource(R.color.bill_print_color);
                        viewHolder.printBill.setVisibility(View.VISIBLE);
                    }
                    if (ActivityMethods.isCustomerSupportActivity(activity)) {
                        viewHolder.actionsHolder.setVisibility(View.GONE);
                        viewHolder.actionHolderSeparator.setVisibility(View.GONE);
                    }

                    //------------

                    viewHolder.item_index.setText(orderItemDetails.getItemLocation());
                    viewHolder.source.setText(orderDetails.getSource());
                    viewHolder.card_title.setText(orderDetails.getReadableOrderID() + "\n" + orderDetails.getFormattedOrderID() +
                            "(" + orderDetails.getInvoiceVersionNumber() + ")");

                    Log.e("orderid", orderDetails.getReadableOrderID());
                    Log.e("order versn", "" + orderDetails.getInvoiceVersionNumber());
                    Log.e("order frmt", "" + orderDetails.getInvoiceVersionNumber());
                    //---------

                    if (orderDetails.getInvoiceVersionNumber() > 0) {
                        viewHolder.orderDetailsView.setVisibility(View.GONE);

                    }
                    if (showOrderDetails) {
                        viewHolder.orderDetailsView.setVisibility(View.VISIBLE);

                        viewHolder.customer_name.setText(orderDetails.getCustomerName());
                        String membership = orderDetails.getBuyerDetails().getMembershipType();
                        if (membership.equalsIgnoreCase(Constants.BRONZE) == false) {
                            viewHolder.membership_type.setVisibility(View.VISIBLE);
                            viewHolder.membership_type.setText(membership);
                        } else {
                            viewHolder.membership_type.setVisibility(View.GONE);
                        }

                        if (buyerDetails != null && buyerDetails.getLastFollowupDate() != null && buyerDetails.getLastFollowupDate().length() > 0) {
                            viewHolder.last_followup_date.setVisibility(View.VISIBLE);
                            viewHolder.last_followup_date.setText(Html.fromHtml("Last followup: <b>" + DateMethods.getReadableDateFromUTC(buyerDetails.getLastFollowupDate(), DateConstants.MMM_DD_YYYY) + "</b>"));
                        } else {
                            viewHolder.last_followup_date.setVisibility(View.GONE);
                        }

                        viewHolder.first_order.setVisibility(orderDetails.isFirstOrder() ? View.VISIBLE : View.GONE);
                        if (orderDetails.getReferralCode() != null && orderDetails.getReferralCode().length() > 0) {
                            viewHolder.referral_order.setVisibility(View.VISIBLE);
                            viewHolder.referral_order.setText("Referral Order (" + orderDetails.getReferralCode().toUpperCase() + ")");
                        } else {
                            viewHolder.referral_order.setVisibility(View.GONE);
                        }


                        if (orderDetails.getCustomerAlternateMobile().length() > 0 || (orderDetails.getCustomerAlternateMobile().isEmpty()) ||
                                (orderDetails.getCustomerAlternateMobile() == null) || (orderDetails.getCustomerAlternateMobile().equals("")) ||
                                (orderDetails.getCustomerMobile().equals("TypedByteArray[length=651850]")) ||
                                (orderDetails.getCustomerMobile() == "TypedByteArray[length=651850]") || (orderDetails.getCustomerMobile().equals("TypedByteArray[length=654509]"))
                                && orderDetails.getCustomerMobile().equalsIgnoreCase(orderDetails.getCustomerAlternateMobile()) == false) {
                            viewHolder.customer_mobile.setText(CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile())
                                    + " / " + CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerAlternateMobile()));
                        }

                        if (orderDetails.getCustomerMobile().length() > 0 || (orderDetails.getCustomerMobile().isEmpty()) ||
                                (orderDetails.getCustomerMobile() == null) || (orderDetails.getCustomerMobile().equals("") || (orderDetails.getCustomerMobile().equals("TypedByteArray[length=651850]"))
                                || (orderDetails.getCustomerMobile() == "TypedByteArray[length=651850]")) || (orderDetails.getCustomerMobile().equals("TypedByteArray[length=654509]"))

                                && orderDetails.getCustomerMobile().equalsIgnoreCase(orderDetails.getCustomerMobile()) == false) {
                            viewHolder.customer_mobile.setText(CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile()));

                        }

                        Log.e("mobilenum", CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile()));
                        Log.e("al mobilenum", CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile()));
                        viewHolder.messageCustomer.setVisibility(showMessageCustomer ? View.VISIBLE : View.GONE);

                        viewHolder.customer_address.setText(CommonMethods.getStringFromStringArray(new String[]{orderDetails.getDeliveryAddress()}));//orderDetails.getDeliveryAddressPremise() + ",\n" + orderDetails.getAddressLine2());
                        if (orderDetails.getDeliveryAddressLandmark() != null && orderDetails.getDeliveryAddressLandmark().length() > 0) {
                            viewHolder.customer_land_mark.setText("Landmark : " + orderDetails.getDeliveryAddressLandmark().toUpperCase());
                        } else {
                            viewHolder.customer_land_mark.setVisibility(View.GONE);
                        }

                        viewHolder.item_cost.setText(SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(orderItemDetails.getAmountOfItem()));

                        viewHolder.order_cost.setText(Html.fromHtml("<u>" + orderDetails.getFullTotalAmountSplitString() + "</u>"));

                        viewHolder.checkPaymentStatus.setVisibility((showCheckPaymentStatus && orderDetails.getTotalCashAmount() > 0 && orderDetails.getStatus().equalsIgnoreCase(Constants.PENDING)) ? View.VISIBLE : View.GONE);
                        viewHolder.requestOnlinePayment.setVisibility((showRequestOnlinePayment && orderDetails.getTotalCashAmount() > 0 && orderDetails.getStatus().equalsIgnoreCase(Constants.PENDING)) ? View.VISIBLE : View.GONE);

                        /*if ( orderItemDetails.getTotalOrderDiscount() > 0 ){
                            viewHolder.discountDetailsView.setVisibility(View.VISIBLE);
                            viewHolder.order_discount.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderItemDetails.getTotalOrderDiscount())+(orderDetails.getCouponCode()!=null?" ("+orderDetails.getCouponCode().toUpperCase()+")":""));
                        }else{
                            viewHolder.discountDetailsView.setVisibility(View.GONE);
                        }*/

                        float buyerOutstandingAmount = orderDetails.getBuyerDetails().getOutstandingAmount();
                        if (buyerOutstandingAmount != 0) {
                            viewHolder.buyerOutstandingDetails.setVisibility(View.VISIBLE);
                            if (buyerOutstandingAmount < 0) {
                                viewHolder.buyer_outstanding.setText(SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(buyerOutstandingAmount) + " (Return to customer)");
                            } else if (buyerOutstandingAmount > 0) {
                                viewHolder.buyer_outstanding.setText(SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(buyerOutstandingAmount) + " (Collect from customer)");
                            }
                        } else {
                            viewHolder.buyerOutstandingDetails.setVisibility(View.GONE);
                        }

                        if (orderDetails.getOrderCreatorName() != null) {
                            viewHolder.orderCreatorDetailsView.setVisibility(View.VISIBLE);
                            viewHolder.order_creator.setText(orderDetails.getOrderCreatorName());
                        } else {
                            viewHolder.orderCreatorDetailsView.setVisibility(View.GONE);
                        }

                        //--------

                        List<AppliedDiscountDetails> appliedDiscounts = orderDetails.getAppliedDiscounts();
                        if (appliedDiscounts != null && appliedDiscounts.size() > 0) {
                            String discountsDetailsString = "";
                            for (int i = 0; i < appliedDiscounts.size(); i++) {
                                AppliedDiscountDetails appliedDiscountDetails = appliedDiscounts.get(i);
                                if (appliedDiscountDetails != null) {
                                    if (discountsDetailsString.length() > 0) {
                                        discountsDetailsString += "<br>";
                                    }
                                    if (appliedDiscountDetails.isActive()) {
                                        discountsDetailsString += "<b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(appliedDiscountDetails.getAmount()) + "</b> for " + appliedDiscountDetails.getDiscountReasonWithComment().toUpperCase() + "</b> by <b>" + appliedDiscountDetails.getDiscountCreatorName().toUpperCase() + "</b>";
                                    } else {
                                        discountsDetailsString += "CANCELLED " + "<strike><b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(appliedDiscountDetails.getAmount()) + "</b> for " + appliedDiscountDetails.getDiscountReasonWithComment().toUpperCase() + "</b> by <b>" + appliedDiscountDetails.getDiscountCreatorName().toUpperCase() + "</b></strike>";
                                    }
                                }
                            }
                            viewHolder.discount_details.setText(Html.fromHtml(discountsDetailsString));
                            viewHolder.discountsView.setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.discountsView.setVisibility(View.GONE);
                        }

                    } else {
                        viewHolder.orderDetailsView.setVisibility(View.GONE);
                    }

                    //---------

                    viewHolder.item_name.setText(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize());

                    Picasso.get()
                            .load(orderItemDetails.getMeatItemDetails().getPicture())
                            .placeholder(R.drawable.image_default)
                            .error(R.drawable.image_default_mastaan)
                            .into(viewHolder.item_image);

                    if (DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate())), DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()))) > 0) {
                        viewHolder.pre_order_status.setVisibility(View.VISIBLE);
                        viewHolder.pre_order_status.setText("PreOrder: " + DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()), DateConstants.MMM_DD));
                    } else {
                        viewHolder.pre_order_status.setVisibility(View.GONE);
                    }

                    if (orderItemDetails.isCashNCarryItem()) {
                        viewHolder.cash_n_carry_status.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.cash_n_carry_status.setVisibility(View.GONE);
                    }

                    if (orderItemDetails.isInCustomerSupportState()) {
                        viewHolder.customerSupportIndicator.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.customerSupportIndicator.setVisibility(View.GONE);
                    }

                    viewHolder.item_quantity.setText(orderItemDetails.getFormattedQuantity());
                    viewHolder.item_extras.setText(Html.fromHtml(orderItemDetails.getFormattedHtmlExtras().toUpperCase()));

                    if (orderItemDetails.getVendorDetails() != null) {
                        viewHolder.vendorDetailsView.setVisibility(View.VISIBLE);
                        String vendorDetailsString = orderItemDetails.getVendorDetails().getName().toUpperCase();
                        vendorDetailsString += " (" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(orderItemDetails.getVendorPaidAmount());
                        vendorDetailsString += ", " + orderItemDetails.getVendorPaymentType().toUpperCase() + ")";
                        if (orderItemDetails.getVendorComments() != null && orderItemDetails.getVendorComments().trim().length() > 0) {
                            vendorDetailsString += "\n" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getVendorComments());
                        }
                        viewHolder.vendor_details.setText(vendorDetailsString);
                    } else {
                        viewHolder.vendorDetailsView.setVisibility(View.GONE);
                    }

                    if (orderItemDetails.getStock() != null || orderItemDetails.getProcessedStock() != null) {
                        viewHolder.stockDetailsView.setVisibility(View.VISIBLE);
                        if (orderItemDetails.getStock() != null) {
                            viewHolder.stock_details.setText(CommonMethods.fromHtml("<b><u>" + orderItemDetails.getStock() + "</u></b>"));
                        } else if (orderItemDetails.getProcessedStock() != null) {
                            viewHolder.stock_details.setText(CommonMethods.fromHtml("<b><u>" + orderItemDetails.getProcessedStock() + "</u></b>"));
                        } else {
                            viewHolder.stock_details.setText("");
                        }
                    } else {
                        viewHolder.stockDetailsView.setVisibility(View.GONE);
                    }


                    if (orderItemDetails.getCombinedSpecialInstructions().length() > 0) {
                        viewHolder.special_instructions.setText(orderItemDetails.getCombinedSpecialInstructions());
                    } else {
                        viewHolder.special_instructions.setText("");
                    }

                    //--------

                    viewHolder.delivery_hub.setText(orderDetails.getHubDetails() != null ? "Hub - " + orderDetails.getHubDetails().getName() : "HUB NOT ASSIGNED");

                    String slotAppendString = "";
                    if (ActivityMethods.isOrdersByDateActivity(activity) == false && orderDetails.deliverEarlyIfPossible() == false) {
                        slotAppendString += "\n(Don't Send Early)";
                    }
                    if (orderDetails.isBillNotRequired()) {
                        slotAppendString += "\n(Paper Bill Not Required)";
                    }
                    // FOR FUTURE ORDERS
                    if (ActivityMethods.isCustomerSupportActivity(activity) || ActivityMethods.isUnprocessedCustomerSupportActivity(activity)
                            || DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate())), DateMethods.getOnlyDate(localStorageData.getServerTime())) > 0) {
                        viewHolder.delivery_time.setText(DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()), DateConstants.MMM_DD) + ", " + orderItemDetails.getDeliveryTimeSlot() + slotAppendString);
                    } else {
                        viewHolder.delivery_time.setText("Time: " + orderItemDetails.getDeliveryTimeSlot() + slotAppendString);
                    }

                    //viewHolder.delivery_time.setText("Act: " + orderItemDetails.getDeliveryBoyDetails()+" with "+DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()));

                    viewHolder.showFollowupDetails.setVisibility(orderDetails.isFollowupResultedOrder() ? View.VISIBLE : View.GONE);

                    //----------

                    viewHolder.order_status.setText("CREATED : " + DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()), DateConstants.MMM_DD_YYYY_HH_MM_A)
                            + "\n" + orderItemDetails.getFormattedStatusString(localStorageData.getUserID()));

                    if (orderItemStatus.equalsIgnoreCase(Constants.DELIVERING) && orderItemDetails.getDeliveryBoyID() != null) {
                        viewHolder.trackOrder.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.trackOrder.setVisibility(View.GONE);
                    }

                    //---

                    if (showOrderStatusTimes) {
                        if (orderItemDetails.getProcessingClaimDate().length() > 0 || orderItemDetails.getProcessingCompletedDate().length() > 0 || orderItemDetails.getDeliveryBoyAssignedDate().length() > 0 || orderItemDetails.getDeliveryClaimedDate().length() > 0 || orderItemDetails.getDeliveredDate().length() > 0) {
                            viewHolder.orderStatusTimesHolder.setVisibility(View.VISIBLE);
                            viewHolder.order_status_times.setText(orderItemDetails.getStatusTimesString());
                        } else {
                            viewHolder.orderStatusTimesHolder.setVisibility(View.GONE);
                        }
                    } else {
                        viewHolder.orderStatusTimesHolder.setVisibility(View.GONE);
                    }

                    String statusReasons = "";
                    if (orderItemDetails.getStatusReasons() != null && orderItemDetails.getStatusReasons().length() > 0) {
                        statusReasons = (orderItemDetails.isSupportRequestedByCustomer() ? "Customer Request: " : "Status reason: ")
                                + orderItemDetails.getStatusReasons();
                    }
                    if (orderItemDetails.getCustomerSupporAssignerDetails() != null && orderItemDetails.getCustomerSupporAssignerID().length() > 0) {
                        if (statusReasons.length() > 0) {
                            statusReasons += "\n";
                        }
                        statusReasons += "Assigned to CS by: ";
                        if (orderItemDetails.getCustomerSupporAssignerID().equals(localStorageData.getUserID())) {
                            statusReasons += "You";
                        } else {
                            statusReasons += orderItemDetails.getCustomerSupporAssignerDetails().getAvailableName();
                        }
                    }
                    if (orderItemDetails.getCustomerSupportExecutiveDetails() != null) {
                        if (statusReasons.length() > 0) {
                            statusReasons += "\n";
                        }
                        statusReasons += "CS exec: ";
                        if (orderItemDetails.getCustomerSupportExecutiveID().equals(localStorageData.getUserID())) {
                            statusReasons += "You";
                        } else {
                            statusReasons += orderItemDetails.getCustomerSupportExecutiveDetails().getAvailableName();
                        }
                    }
                    if (orderItemDetails.getCustomerSupportComment() != null && orderItemDetails.getCustomerSupportComment().length() > 0) {
                        if (statusReasons.length() > 0) {
                            statusReasons += "\n";
                        }
                        statusReasons += "CS resolution: " + orderItemDetails.getCustomerSupportComment();
                    }
                    if (orderItemDetails.getQualityCheckStatus() == Constants.QC_FAILED && orderItemDetails.getQualityCheckComments() != null && orderItemDetails.getQualityCheckComments().trim().length() > 0) {
                        statusReasons += "QC Failed: " + orderItemDetails.getQualityCheckComments();
                    }

                    if (statusReasons != null && statusReasons.trim().length() > 0) {
                        viewHolder.orderStatusReasonsHolder.setVisibility(View.VISIBLE);
                        viewHolder.order_status_reasons.setText(statusReasons);
                    } else {
                        viewHolder.orderStatusReasonsHolder.setVisibility(View.GONE);
                    }

                    //---------

                    if (callback != null) {

                        viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (userDetails.isSuperUser() || userDetails.isCustomerRelationshipManager()
                                        || localStorageData.isUserCheckedIn() || ActivityMethods.isCustomerSupportActivity(activity)) {
                                    callback.onItemClick(position);
                                    //Toast.makeText(context, orderItemDetails.getStatus()+" >"+orderItemStatus, Toast.LENGTH_LONG).show();
                                    if (ActivityMethods.isCustomerSupportActivity(activity) || ActivityMethods.isUnprocessedCustomerSupportActivity(activity)) {
                                        if (orderItemDetails.isInCustomerSupportState()) {
                                            if (orderItemDetails.getCustomerSupportExecutiveDetails() != null) {
                                                if (localStorageData.getUserID().equalsIgnoreCase(orderItemDetails.getCustomerSupportExecutiveID())) {
                                                    callback.onCompleteCustomerSupport(position);
                                                }
                                            } else {
                                                callback.onClaimForCustomerSupport(position);
                                            }
                                        }
                                    } else {
                                        // FOR COUNTER SALE ORDER
                                        if (orderItemStatus.equalsIgnoreCase(Constants.DELIVERED) == false && orderItemDetails.isCounterSaleEmployeeOrder()) {
                                            callback.onCompleteCounterSaleOrder(position);
                                        }
                                        // FOR NORMAL ORDERS
                                        else {
                                            if (orderItemStatus.equalsIgnoreCase(Constants.ORDERED)) {
                                                callback.onAcknowledge(position);
                                            } else if (orderItemStatus.equalsIgnoreCase(Constants.ACKNOWLEDGED)) {
                                                callback.onStartProcessing(position);
                                            } else if (orderItemStatus.equalsIgnoreCase(Constants.PROCESSING)) {
                                                callback.onCompleteProcessing(position);
                                            } else if (orderItemStatus.equalsIgnoreCase(Constants.PROCESSED)) {
                                                callback.onAssignDeliveryBoy(position);
                                            } else if (orderItemStatus.equalsIgnoreCase(Constants.ASSIGNED)
                                                    || orderItemStatus.equalsIgnoreCase(Constants.PICKED_UP)
                                                    || orderItemStatus.equalsIgnoreCase(Constants.DELIVERING)) {//orderItemDetails.getStatus() == 3 || orderItemDetails.getStatus() == 8) {
                                                callback.onCompleteDelivery(position);
                                            } else if (orderItemStatus.equalsIgnoreCase(Constants.DELIVERED)) {
                                                callback.onReProcess(position);
                                            } else if (orderItemStatus.equalsIgnoreCase(Constants.REJECTED)) {
                                                callback.onRejectedItemsHandling(position);
                                            } else if (orderItemStatus.equalsIgnoreCase(Constants.FAILED)) {
                                                callback.onReProcess(position);
                                            }
                                        }
                                    }
                                } else {
                                    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                                }
                            }
                        });

                        viewHolder.itemSelector.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View view) {
                                callback.onItemLongClick(position);
                                return false;
                            }
                        });

                        viewHolder.showDirections.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onShowDirections(position);
                            }
                        });

                        viewHolder.showBuyerHistory.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onShowBuyerHistory(position);
                            }
                        });

                        viewHolder.referral_order.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onShowReferralBuyerDetails(position);
                            }
                        });

                        viewHolder.order_cost.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callback.onShowMoneyCollectionDetails(position);
                            }
                        });

                        viewHolder.checkPaymentStatus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callback.onCheckPaymentStatus(position);
                            }
                        });
                        viewHolder.requestOnlinePayment.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callback.onRequestOnlinePayment(position);
                            }
                        });



                        viewHolder.showFollowupDetails.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callback.onShowFollowupResultedInOrder(position);
                            }
                        });

                        viewHolder.callCustomer.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onCallCustomer(position);
                            }
                        });
                        viewHolder.callCustomer.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                activity.copyTextToClipboard(orderDetails.getCustomerMobile());
                                /*ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                clipboardManager.setText(CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile()));
                                Toast.makeText(context, orderDetails.getCustomerName() + "'s primary mobile number ( " + orderDetails.getCustomerMobile() + " ) is copied", Toast.LENGTH_SHORT).show();*/
                                return false;
                            }
                        });

                        viewHolder.messageCustomer.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onMessageCustomer(position);
                            }
                        });

                        viewHolder.stock_details.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callback.onShowStockDetails(position);
                            }
                        });
                        viewHolder.stock_details.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                if (orderItemDetails.getStock() != null) {
                                    activity.copyTextToClipboard(orderItemDetails.getStock());
                                } else if (orderItemDetails.getProcessedStock() != null) {
                                    activity.copyTextToClipboard(orderItemDetails.getProcessedStock());
                                }
                                return false;
                            }
                        });

                        viewHolder.referral_order.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onShowReferralBuyerDetails(position);
                            }
                        });

                        //if ( orderItemDetails.getOrderItemsCount() > 1 ){
                        viewHolder.item_index.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callback.onShowOrderDetails(position);
                            }
                        });
                        //}

                        viewHolder.printBill.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onPrintBill(position);
                            }
                        });

                        viewHolder.trackOrder.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onTrackOrder(position);
                            }
                        });
                    }
                }

            }
        } catch (Exception e) {
        }
    }

    private void setColors(ViewHolder viewHolder, int normalColor, int fabColor) {
        //fabColor = R.color.default_card_fab_color;
        viewHolder.container.setCardBackgroundColor(context.getResources().getColor(normalColor));
//        viewHolder.callCustomer.setColorNormalResId(fabColor);
        //viewHolder.callCustomer.setColorPressedResId(fabColor);
//        viewHolder.callCustomerAleternate.setColorNormalResId(fabColor);
        //viewHolder.callCustomerAleternate.setColorPressedResId(fabColor);
//        viewHolder.showDirections.setColorNormalResId(fabColor);
//        viewHolder.showBuyerHistory.setColorNormalResId(fabColor);
        //viewHolder.showDirections.setColorPressedResId(fabColor);
    }

    boolean showMessageCustomer;

    public void showMessageCustomerOption() {
        this.showMessageCustomer = true;
    }

    //--------------

    public void updateQuantity(int position, double newQuantity) {
        OrderItemDetails orderItemDetails = itemsList.get(position);
        double newItemPrice = (orderItemDetails.getAmountOfItem() * newQuantity) / orderItemDetails.getQuantity();
        double newOrderPrice = (orderItemDetails.getTotalOrderAmount() - orderItemDetails.getAmountOfItem()) + newItemPrice;

        orderItemDetails.setQuantity(newQuantity);
        orderItemDetails.getOrderDetails().setBillPrinted(false);
        orderItemDetails.setAmountOfItem(newItemPrice);
        orderItemDetails.setTotalOrderAmount(newOrderPrice);
        itemsList.set(position, orderItemDetails);

        notifyDataSetChanged();
    }

    public void updateDeliveryDate(int position, String fullDate) {
        itemsList.get(position).setDeliveryDate(fullDate);
        notifyDataSetChanged();
    }

    public void updateProcessingBy(int position, UserDetails processingBY) {
        itemsList.get(position).setStatus(1);
        itemsList.get(position).setProcessingClaimDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
        itemsList.get(position).setProcessorDetails(processingBY);
        notifyDataSetChanged();
    }

    public void updateDeliveringBy(int position, UserDetails deliveryBoy) {
        itemsList.get(position).setStatus(8);
        itemsList.get(position).setDeliveryBoyAssignedDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
        itemsList.get(position).setDeliveryBoyDetails(deliveryBoy);
        itemsList.get(position).setDeliveryAssigner(new UserDetailsMini(userDetails.getID(), userDetails.getAvailableName()));
        notifyDataSetChanged();
    }

    public void updateDeliveryClaimDate(int position) {
        itemsList.get(position).setDeliveryClaimedDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
        notifyDataSetChanged();
    }

    public void setBillPrinted(int position, boolean isBillPrinted) {
        itemsList.get(position).getOrderDetails().setBillPrinted(isBillPrinted);
        notifyDataSetChanged();
    }

    public void markAsNew(int position) {
        itemsList.get(position).setStatus(0);
        notifyDataSetChanged();
    }

    public void markAsAcknowledged(int position) {
        itemsList.get(position).setStatus(7);
        itemsList.get(position).setAcknowledgedBy(new UserDetailsMini(userDetails.getID(), userDetails.getAvailableName()));
        itemsList.get(position).setProcessingClaimDate(null);
        itemsList.get(position).setProcessorDetails(null);
        notifyDataSetChanged();
    }

    public void markAsProcessing(int position) {
        itemsList.get(position).setStatus(1);
        notifyDataSetChanged();
    }

    public void markAsDelivered(int position, UserDetails deliveryBoyDetails, String deliveryTime) {
        itemsList.get(position).setStatus(4);
        itemsList.get(position).setDeliveryBoyDetails(deliveryBoyDetails);
        itemsList.get(position).setDeliveredDate(DateMethods.getDateInUTCFormat(deliveryTime));
        notifyDataSetChanged();
    }

    public void updateVendorDetails(int positon, Vendor vendorDetails) {
        itemsList.get(positon).setVendorDetails(vendorDetails);
        notifyDataSetChanged();
    }

    public void markAsPickupPending(int position) {
        itemsList.get(position).setStatus(2);
        itemsList.get(position).setProcessingCompletedDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
        notifyDataSetChanged();
    }

    public void resetToPickupPending(int position) {
        itemsList.get(position).setStatus(2);
        itemsList.get(position).setDeliveryBoyDetails(null);
        itemsList.get(position).setDeliveryBoyAssignedDate(null);
        notifyDataSetChanged();
    }

    public void markAsFailed(int position, String failureReason) {
        itemsList.get(position).setStatus(6);
        itemsList.get(position).setOrderStatusReason(failureReason);
        notifyDataSetChanged();
    }

    public void setCashNCarryItem(int position, boolean isCashNCarryItem) {
        itemsList.get(position).setCashNCarry(isCashNCarryItem);
        notifyDataSetChanged();
    }

    public void assignToCustomerSupport(int position, String reason) {
        itemsList.get(position).setCustomerSupportState(true);
        itemsList.get(position).setCustomerSupporAssignerDetails(userDetails);
        itemsList.get(position).setCustomerSupportExecutiveDetails(null);//Temp CODE
        itemsList.get(position).setOrderStatusReason(reason);
        notifyDataSetChanged();
    }

    public void removeFromCustomerSupport(int position) {
        itemsList.get(position).setCustomerSupportState(false);
        itemsList.get(position).setCustomerSupporAssignerDetails(null);
        itemsList.get(position).setOrderStatusReason(null);
        notifyDataSetChanged();
    }

    public void setCustomerSupportComment(int postion, String customerSupportComment) {
        itemsList.get(postion).setCustomerSupportComment(customerSupportComment);
        notifyDataSetChanged();
    }

    public void setDelayedDeliveryComment(int postion, String delayedDeliveryComment) {
        itemsList.get(postion).setDelayedDeliveryComment(delayedDeliveryComment);
        notifyDataSetChanged();
    }

    // TEMP
    public void showInCheckBoxView() {
        for (int i = 0; i < itemsList.size(); i++) {
            itemsList.get(i).setShowInCheckBoxView(true);
        }
        notifyDataSetChanged();
    }

    public void showInFullDetailsView() {
        for (int i = 0; i < itemsList.size(); i++) {
            itemsList.get(i).setShowInCheckBoxView(false);
        }
        notifyDataSetChanged();
    }

    //--------------

    boolean showCheckPaymentStatus;

    public OrderItemsAdapter showCheckPaymentStatus() {
        this.showCheckPaymentStatus = true;
        return this;
    }

    boolean showRequestOnlinePayment;

    public OrderItemsAdapter showRequestOnlinePayment() {
        this.showRequestOnlinePayment = true;
        return this;
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<OrderItemDetails> items) {

        itemsList.clear();
        ;
        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<OrderItemDetails> items) {

        if (items != null && items.size() > 0) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems() {
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItemAt(int position, OrderItemDetails orderItemDetails) {
        if (position < itemsList.size()) {
            itemsList.add(position, orderItemDetails);
            notifyDataSetChanged();
        }
    }

    public OrderItemDetails getItem(int position) {
        if (position >= 0 && position < itemsList.size()) {
            return itemsList.get(position);
        }
        return null;
    }

    public void updateItem(int position, OrderItemDetails orderItemDetails) {
        if (position >= 0 && position < itemsList.size()) {
            itemsList.set(position, orderItemDetails);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(int position) {
        if (position >= 0 && position < itemsList.size()) {
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id) {
        for (int i = 0; i < itemsList.size(); i++) {
            if (itemsList.get(i).getID().equals(id)) {
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<OrderItemDetails> getAllItems() {
        return itemsList;
    }


}
