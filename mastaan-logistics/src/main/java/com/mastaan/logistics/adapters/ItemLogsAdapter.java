package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.ItemLogDetails;

import java.util.List;

public class ItemLogsAdapter extends RecyclerView.Adapter<ItemLogsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<ItemLogDetails> itemsList;

    Callback callback;

    public interface Callback{
        void onClick(int position);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemSelector;
        TextView employee;
        TextView date;
        TextView details;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            employee= (TextView) itemLayoutView.findViewById(R.id.employee);
            date = (TextView) itemLayoutView.findViewById(R.id.date);
            details = (TextView) itemLayoutView.findViewById(R.id.details);
        }
    }

    public ItemLogsAdapter(MastaanToolbarActivity activity, List<ItemLogDetails> buyersList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = buyersList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ItemLogsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_log, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final ItemLogDetails itemLogDetails = itemsList.get(position);

        if ( itemLogDetails != null ){
            viewHolder.employee.setText(itemLogDetails.getEmployeeDetails().getAvailableName());
            viewHolder.date.setText(itemLogDetails.getCreatedDate());
            viewHolder.details.setText(CommonMethods.fromHtml(itemLogDetails.getDetails()));

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onClick(position);
                    }
                });
            }
        }
    }

    //---------------

    public void setItems(List<ItemLogDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public ItemLogDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public List<ItemLogDetails> getAllItems(){
        return itemsList;
    }

}