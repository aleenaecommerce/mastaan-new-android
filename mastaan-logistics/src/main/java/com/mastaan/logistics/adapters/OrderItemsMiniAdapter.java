package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 29/1/16.
 */
public class OrderItemsMiniAdapter extends RecyclerView.Adapter<OrderItemsMiniAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<OrderItemDetails> itemsList;
    String userID;

    Callback callback;

    public interface Callback{
        void onItemClick(int position);

        void onShowStockDetails(int position);
    }

    public OrderItemsMiniAdapter(MastaanToolbarActivity activity, List<OrderItemDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.userID = new LocalStorageData(activity).getUserID();
        this.itemsList = itemsList;
        this.callback = callback;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        //CardView container;

        View userIndicatorView;
        TextView user_indicator;
        TextView status_header;
        TextView item_name;
        TextView item_info;
        TextView status_info;
        TextView times_info;
        TextView stock_details;
        View cashNCarryIndicator;
        View customerSupportIndicator;

        public ViewHolder(View itemView) {
            super(itemView);

            itemSelector = itemView.findViewById(R.id.itemSelector);
            //container = (CardView) itemView.findViewById(R.id.container);

            userIndicatorView = itemView.findViewById(R.id.userIndicatorView);
            user_indicator = (TextView) itemView.findViewById(R.id.user_indicator);
            status_header = (TextView) itemView.findViewById(R.id.status_header);
            item_name = (TextView) itemView.findViewById(R.id.item_name);
            item_info = (TextView) itemView.findViewById(R.id.item_info);
            status_info = (TextView) itemView.findViewById(R.id.status_info);
            times_info = (TextView) itemView.findViewById(R.id.times_info);
            stock_details = (TextView) itemView.findViewById(R.id.stock_details);

            cashNCarryIndicator = itemView.findViewById(R.id.cashNCarryIndicator);
            customerSupportIndicator = itemView.findViewById(R.id.customerSupportIndicator);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.view_order_item_mini, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final OrderItemDetails orderItemDetails = itemsList.get(position);
        final String orderItemStatus = orderItemDetails.getStatusString();

        if ( orderItemDetails != null ) {

            //ACKNOWLEDGED
            if ( orderItemStatus.equalsIgnoreCase(Constants.ACKNOWLEDGED) ){
                viewHolder.status_header.setText(" A ");
                viewHolder.status_header.setBackgroundResource(R.color.acknowledged_color);
            }
            //PROCESSING
            else if ( orderItemStatus.equalsIgnoreCase(Constants.PROCESSING) ) {
                viewHolder.status_header.setText(" PR ");
                viewHolder.status_header.setBackgroundResource(R.color.processing_color);
            }
            //PROCESSED
            else if ( orderItemStatus.equalsIgnoreCase(Constants.PROCESSED) ) {
                if ( orderItemDetails.getQualityCheckStatus() == Constants.QC_DONE ){
                    viewHolder.status_header.setText(" QCD ");
                    viewHolder.status_header.setBackgroundResource(R.color.quality_check_completed_color);
                }else if ( orderItemDetails.getQualityCheckStatus() == Constants.QC_FAILED ){
                    viewHolder.status_header.setText(" QCF ");
                    viewHolder.status_header.setBackgroundResource(R.color.quality_check_failed_color);
                }else{
                    viewHolder.status_header.setText(" PRD ");
                    viewHolder.status_header.setBackgroundResource(R.color.processed_color);
                }
            }
            //ASSIGNED
            else if ( orderItemStatus.equalsIgnoreCase(Constants.ASSIGNED) ){
                viewHolder.status_header.setText(" ASD ");
                viewHolder.status_header.setBackgroundResource(R.color.assigned_color);
            }
            //PICKED UP
            else if ( orderItemStatus.equalsIgnoreCase(Constants.PICKED_UP) ){
                viewHolder.status_header.setText(" PUD ");
                viewHolder.status_header.setBackgroundResource(R.color.picked_up_color);
            }
            //DELIVERING
            else if ( orderItemStatus.equalsIgnoreCase(Constants.DELIVERING) ){
                viewHolder.status_header.setText(" DEL ");
                viewHolder.status_header.setBackgroundResource(R.color.delivering_color);
            }
            //DELIVERED
            else if ( orderItemStatus.equalsIgnoreCase(Constants.DELIVERED) ){
                viewHolder.status_header.setText(" D ");
                viewHolder.status_header.setBackgroundResource(R.color.delivered_color);
            }
            //REJECTED
            else if ( orderItemStatus.equalsIgnoreCase(Constants.REJECTED) ){
                viewHolder.status_header.setText(" R ");
                viewHolder.status_header.setBackgroundResource(R.color.rejected_color);
            }
            //FAILED
            else if ( orderItemStatus.equalsIgnoreCase(Constants.FAILED) ){
                viewHolder.status_header.setText(" F ");
                viewHolder.status_header.setBackgroundResource(R.color.failed_color);
            }
            //ORDERED
            else{
                viewHolder.status_header.setText(" O ");
                viewHolder.status_header.setBackgroundResource(R.color.default_color);
            }

            //-----

            viewHolder.item_name.setText(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize());
            String itemInfoString = SpecialCharacters.RS + " "+CommonMethods.getIndianFormatNumber(orderItemDetails.getAmountOfItem());
            itemInfoString += " ("+orderItemDetails.getFormattedQuantity()+")";

            if ( orderItemDetails.getFormattedExtras().length() > 0 ){
                itemInfoString += "\n"+orderItemDetails.getFormattedExtras();
            }
            if ( orderItemDetails.getCombinedSpecialInstructions().length() > 0 ){
                itemInfoString += "\n\nInstructions: "+orderItemDetails.getCombinedSpecialInstructions();
            }
            viewHolder.item_info.setText(itemInfoString);

            if ( orderItemDetails.isCashNCarryItem() ){
                viewHolder.cashNCarryIndicator.setVisibility(View.VISIBLE);
            }else{
                viewHolder.cashNCarryIndicator.setVisibility(View.GONE);
            }

            if ( orderItemDetails.isInCustomerSupportState() ){
                viewHolder.customerSupportIndicator.setVisibility(View.VISIBLE);
            }else{
                viewHolder.customerSupportIndicator.setVisibility(View.GONE);
            }

            if ( orderItemDetails.getStock() != null || orderItemDetails.getProcessedStock() != null ){
                viewHolder.stock_details.setVisibility(View.VISIBLE);
                if ( orderItemDetails.getStock() != null ){
                    viewHolder.stock_details.setText(CommonMethods.fromHtml("STOCK: <b><u>"+orderItemDetails.getStock()+"</u></b>"));
                }else if ( orderItemDetails.getProcessedStock() != null ){
                    viewHolder.stock_details.setText(CommonMethods.fromHtml("STOCK: <b><u>"+orderItemDetails.getProcessedStock()+"</u></b>"));
                }else{  viewHolder.stock_details.setText("");   }
            }else{  viewHolder.stock_details.setVisibility(View.GONE);   }

            //----

           /* String orderStatusString = "";
            if ( orderItemDetails.getVendorDetails() != null ){
                orderStatusString += "VENDOR: "+orderItemDetails.getVendorDetails().getName();
            }
            if ( orderItemDetails.getButcherDetails() != null ){
                if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
                orderStatusString += "BUTCHER: "+orderItemDetails.getButcherDetails().getAvailableName();
            }
            if ( orderItemDetails.getProcessorDetails() != null ){
                if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }

                String string = "PROCESSING";
                if ( orderItemDetails.getStatusString().equalsIgnoreCase("DELIVERED") || orderItemDetails.getStatusString().equalsIgnoreCase("DELIVERING") || orderItemDetails.getStatusString().equalsIgnoreCase("PICKUP PENDING") ){
                    string = "PROCESSED";
                }
                if ( orderItemDetails.getProcesserID().equals(localStorageData.getUserID()) ){
                    orderStatusString += string+" : Me";
                }else{
                    orderStatusString += string+" : "+ orderItemDetails.getProcessorDetails().getAvailableName();
                }
            }
            if ( orderItemDetails.getDeliveryAssigner() != null ){
                if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
                if ( orderItemDetails.getDeliveryAssigner().getID().equals(localStorageData.getUserID()) ){
                    orderStatusString += "DELIVERY ASSIGNER : Me";
                }else{
                    orderStatusString += "DELIVERY ASSIGNER : " + orderItemDetails.getDeliveryAssigner().getAvailableName();
                }
            }
            if ( orderItemDetails.getDeliveryBoyDetails() != null ){
                if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
                if ( orderItemDetails.getDeliveryBoyID().equals(localStorageData.getUserID()) ){
                    viewHolder.userIndicatorView.setVisibility(View.VISIBLE);
                    viewHolder.user_indicator.setText(SpecialCharacters.CHECK_MARK);
                    orderStatusString += orderItemDetails.getStatusString()+" : Me";
                }else{
                    viewHolder.userIndicatorView.setVisibility(View.GONE);
                    viewHolder.user_indicator.setText("");
                    orderStatusString += orderItemDetails.getStatusString()+" : " + orderItemDetails.getDeliveryBoyDetails().getAvailableName();
                }
            }else{
                viewHolder.userIndicatorView.setVisibility(View.GONE);
                viewHolder.user_indicator.setText("");
                if ( orderStatusString.length() > 0 && orderStatusString.toLowerCase().contains(orderItemDetails.getStatusString().toLowerCase()) == false ){
                    orderStatusString += "\n"+ orderItemDetails.getStatusString();
                }
            }
            if ( orderStatusString.length() > 0 ){
                viewHolder.status_info.setText(orderStatusString);
            }else{
                if ( orderItemDetails.getStatusString().equalsIgnoreCase(Constants.FAILED) && orderItemDetails.getFailedBy() != null ){
                    viewHolder.status_info.setText("FAILED by : "+orderItemDetails.getFailedBy().getAvailableName()+"\n"+orderItemDetails.getStatusReasons());
                }else {
                    viewHolder.status_info.setText(orderItemDetails.getStatusString());
                }
            }*/
           viewHolder.status_info.setText(orderItemDetails.getFormattedStatusString(userID));

            if ( orderItemDetails.getDeliveryBoyID() != null && orderItemDetails.getDeliveryBoyID().equals(userID) ){
                viewHolder.userIndicatorView.setVisibility(View.VISIBLE);
                viewHolder.user_indicator.setText(SpecialCharacters.CHECK_MARK);
            }else{
                viewHolder.userIndicatorView.setVisibility(View.GONE);
                viewHolder.user_indicator.setText("");
            }

            String statusTimesString = orderItemDetails.getStatusTimesString();
            /*TEMP*/if ( orderItemDetails.getQualityCheckStatus() == Constants.QC_FAILED && orderItemDetails.getQualityCheckComments() != null && orderItemDetails.getQualityCheckComments().trim().length() > 0 ){
                statusTimesString += (statusTimesString.length()>0?"\n\n":"")+"QC Failed: "+orderItemDetails.getQualityCheckComments();
            }

            if ( statusTimesString != null && statusTimesString.length() > 0 ){
                viewHolder.times_info.setVisibility(View.VISIBLE);
                viewHolder.times_info.setText(statusTimesString);
            }else{  viewHolder.times_info.setVisibility(View.GONE); }

            //-----

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position);
                    }
                });

                viewHolder.stock_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onShowStockDetails(position);
                    }
                });
                viewHolder.stock_details.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if ( orderItemDetails.getStock() != null ) {
                            activity.copyTextToClipboard(orderItemDetails.getStock());
                        }else if ( orderItemDetails.getProcessedStock() != null ) {
                            activity.copyTextToClipboard(orderItemDetails.getProcessedStock());
                        }
                        return false;
                    }
                });
            }

        }

    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<OrderItemDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<OrderItemDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItemAt(int position, OrderItemDetails orderItemDetails){
        if ( position < itemsList.size() ){
            itemsList.add(position, orderItemDetails);
            notifyDataSetChanged();
        }
    }

    public OrderItemDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void updateItem(int position, OrderItemDetails orderItemDetails){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.set(position, orderItemDetails);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<OrderItemDetails> getAllItems(){
        return itemsList;
    }



}
