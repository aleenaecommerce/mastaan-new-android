package com.mastaan.logistics.adapters;

import android.annotation.SuppressLint;
import android.content.Context;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vWrappingLinearLayoutManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.AppliedDiscountDetails;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.MoneyCollectionDetails;
import com.mastaan.logistics.models.OrderDetails;

import java.util.List;

public class MoneyCollectionsAdapter extends RecyclerView.Adapter<MoneyCollectionsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<MoneyCollectionDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
        void onTransactionClick(int position, int transactionPosition);
        void onCallCustomer(int position);
        void onShowOrderDetails(int position);
        void onShowBuyerHistory(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CardView container;

        View itemSelector;
        TextView status_header;
        TextView card_title;

        TextView customer_details;
        TextView order_details;
        FloatingActionButton callCustomer;
        FloatingActionButton showOrderDetails;
        FloatingActionButton showBuyerHistory;

        vRecyclerView transactionsHolder;

        LinearLayout amountCollectionView;
        TextView amount_collected;
        TextView order_amount;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            container = itemLayoutView.findViewById(R.id.container);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            status_header = itemLayoutView.findViewById(R.id.status_header);
            card_title = itemLayoutView.findViewById(R.id.card_title);

            customer_details = itemLayoutView.findViewById(R.id.customer_details);
            order_details = itemLayoutView.findViewById(R.id.order_details);
            callCustomer =  itemLayoutView.findViewById(R.id.callCustomer);
            showOrderDetails = itemLayoutView.findViewById(R.id.showOrderDetails);
            showBuyerHistory = itemLayoutView.findViewById(R.id.showBuyerHistory);

            transactionsHolder = itemLayoutView.findViewById(R.id.transactionsHolder);

            amountCollectionView = itemLayoutView.findViewById(R.id.amountCollectionView);
            amount_collected = itemLayoutView.findViewById(R.id.amount_collected);
            order_amount = itemLayoutView.findViewById(R.id.order_amount);
        }
    }

    public MoneyCollectionsAdapter(MastaanToolbarActivity activity, List<MoneyCollectionDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MoneyCollectionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_money_collection, parent, false);
        return (new ViewHolder(itemHolder));
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, @SuppressLint("RecyclerView") final int position) {

        final MoneyCollectionDetails moneyCollectionDetails = itemsList.get(position);
        if ( moneyCollectionDetails != null ){
            if ( moneyCollectionDetails.isConsolidated() ){//moneyCollectionDetails.getStatus().equalsIgnoreCase(Constants.WITH_ACCOUNTS) ){
                viewHolder.status_header.setText(" C ");
                setColors(viewHolder, R.color.delivered_color, R.color.delivered_color_fab);
            }else{
                viewHolder.status_header.setText(" P ");
                setColors(viewHolder, R.color.default_color, R.color.default_color_fab);
            }

            //---

            // FOR ORDER MONEY COLLECTION
            if ( moneyCollectionDetails.getType().equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_ORDER) ){
                OrderDetails orderDetails = moneyCollectionDetails.getOrderDetails();

                viewHolder.showOrderDetails.setVisibility(View.VISIBLE);
                viewHolder.order_details.setVisibility(View.VISIBLE);
                viewHolder.amountCollectionView.setVisibility(View.VISIBLE);

                viewHolder.card_title.setText(orderDetails.getReadableOrderID().toUpperCase());

                String customerDetailsString = "<b>"+orderDetails.getCustomerName()+"</b>";
                String membership = orderDetails.getBuyerDetails().getMembershipType();
                if ( membership.equalsIgnoreCase(Constants.BRONZE) == false ){   customerDetailsString += "  [<b>"+membership+"</b>]";    }

                customerDetailsString += "<br>"+orderDetails.getDeliveryAddress();
                Log.e("mobile num",orderDetails.m + orderDetails.getCustomerMobile() + orderDetails.getCustomerAlternateMobile());
                if ( orderDetails.getCustomerAlternateMobile() != null && orderDetails.getCustomerAlternateMobile().length() > 0 ){
                    customerDetailsString += "<br>MOBILE: <b>"+CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile())+" / "+CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerAlternateMobile())+"</b>";
                }else{
                    customerDetailsString += "<br>MOBILE: <b>"+CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile())+"</b>";
                }
                viewHolder.customer_details.setText(Html.fromHtml(customerDetailsString.toUpperCase()));

                String orderDetailsString = "ORDER DATE: <b>"+ DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate()), DateConstants.MMM_DD_YYYY)+"</b>";
                if ( orderDetails.getTotalDiscount() > 0 || (orderDetails.getTotalCashAmount() != 0 && orderDetails.getTotalOnlinePaymentAmount() != 0 )){
                    orderDetailsString += "<br>ORDER TOTAL: <b>"+SpecialCharacters.RS + ""+CommonMethods.getIndianFormatNumber(orderDetails.getTotalAmountIncludingDiscount())+"</b>"+"<br>[";

                    if ( orderDetails.getTotalDiscount() > 0 ){
                        orderDetailsString += "<b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalDiscount())+"</b>(DIS)"+(orderDetails.getCouponCode()!=null?"("+orderDetails.getCouponCode().toUpperCase()+")":"")+", ";
                    }
                    if ( orderDetails.getTotalCashAmount() != 0 && orderDetails.getTotalOnlinePaymentAmount() != 0 ){
                        orderDetailsString += "<b>"+SpecialCharacters.RS+""+CommonMethods.getIndianFormatNumber(orderDetails.getTotalOnlinePaymentAmount()) + "</b>(OP), <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalCashAmount()) + "</b>(COD" + (orderDetails.getTotalCashAmount() < 0 ? ",Return)" : ")");
                    }else{
                        if ( orderDetails.getTotalCashAmount() != 0 ){
                            orderDetailsString += "<b>"+SpecialCharacters.RS+""+CommonMethods.getIndianFormatNumber(orderDetails.getTotalCashAmount()) + "</b>(COD" + (orderDetails.getTotalCashAmount() < 0 ? ",Return)" : ")");
                        }else if ( orderDetails.getTotalOnlinePaymentAmount() != 0 ){
                            orderDetailsString += "<b>"+SpecialCharacters.RS+""+CommonMethods.getIndianFormatNumber(orderDetails.getTotalOnlinePaymentAmount()) + "</b>(OP)";
                        }
                    }
                    orderDetailsString += "]";
                }else{
                    orderDetailsString += "<br>ORDER TOTAL: <b>"+SpecialCharacters.RS + ""+CommonMethods.getIndianFormatNumber(orderDetails.getTotalAmountIncludingDiscount())+"</b> (" + orderDetails.getPaymentTypeString() + ")";
                }

                List<AppliedDiscountDetails> appliedDiscounts = orderDetails.getAppliedDiscounts();
                if ( appliedDiscounts != null && appliedDiscounts.size() > 0 ){
                    String discountsDetailsString = "<br><br><u>DISCOUNT DETAILS:</u>";
                    for (int i=0;i<appliedDiscounts.size();i++){
                        AppliedDiscountDetails appliedDiscountDetails = appliedDiscounts.get(i);
                        if ( appliedDiscountDetails != null ){
                            if ( discountsDetailsString.length() > 0 ){ discountsDetailsString += "<br>"; }
                            if ( appliedDiscountDetails.isActive() ) {
                                discountsDetailsString += "<b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(appliedDiscountDetails.getAmount()) + "</b> for " + appliedDiscountDetails.getDiscountReasonWithComment().toUpperCase() + "</b> by <b>" + appliedDiscountDetails.getDiscountCreatorName().toUpperCase() + "</b>";
                            }else{
                                discountsDetailsString += "CANCELLED "+"<strike><b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(appliedDiscountDetails.getAmount()) + "</b> for " + appliedDiscountDetails.getDiscountReasonWithComment().toUpperCase() + "</b> by <b>" + appliedDiscountDetails.getDiscountCreatorName().toUpperCase() + "</b></strike>";
                            }
                        }
                    }
                    orderDetailsString += discountsDetailsString;
                }
                if ( orderDetails.getBuyerDetails().getOutstandingAmount() < 0 ){
                    orderDetailsString += "<br><br>CUR. OUTSTANDING:  <b>- "+SpecialCharacters.RS + "" + CommonMethods.getIndianFormatNumber(orderDetails.getBuyerDetails().getOutstandingAmount()) + " (Return)</b>";
                }else if  ( orderDetails.getBuyerDetails().getOutstandingAmount() > 0 ) {
                    orderDetailsString += "<br><br>CUR. OUTSTANDING:  <b>"+SpecialCharacters.RS + "" + CommonMethods.getIndianFormatNumber(Math.abs(orderDetails.getBuyerDetails().getOutstandingAmount()))+" (Collect)</b>";
                }

                viewHolder.order_details.setText(Html.fromHtml(orderDetailsString.toUpperCase()));

                //------

                if ( moneyCollectionDetails.getCollectedAmount() >= 0 ) {
                    viewHolder.amount_collected.setText(Html.fromHtml("<b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(moneyCollectionDetails.getCollectedAmount()) + "</b><br>(TOTAL)"));
                }else{
                    viewHolder.amount_collected.setText(Html.fromHtml("<b>" + SpecialCharacters.RS + " -" + CommonMethods.getIndianFormatNumber(Math.abs(moneyCollectionDetails.getCollectedAmount())) + "</b><br>(TOTAL)"));
                }
                viewHolder.order_amount.setText(Html.fromHtml("<b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(orderDetails.getTotalAmount()) + "</b><br>(ORDER TOTAL)"));

                if ( Math.abs(orderDetails.getTotalAmount() - moneyCollectionDetails.getCollectedAmount()) >= 1 ){
                    viewHolder.amountCollectionView.setBackgroundResource(R.color.money_mismatch_color);
                }else{
                    viewHolder.amountCollectionView.setBackgroundResource(android.R.color.transparent);
                }
            }
            // FOR OUTSTANDING MONEY COLLECTION
            else if ( moneyCollectionDetails.getType().equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_OUTSTANDING) ){
                BuyerDetails buyerDetails = moneyCollectionDetails.getBuyerDetails();

                viewHolder.showOrderDetails.setVisibility(View.GONE);
                viewHolder.order_details.setVisibility(View.GONE);
                viewHolder.amountCollectionView.setVisibility(View.GONE);

                viewHolder.card_title.setText("OUTSTANDING");

                String customerDetailsString = "<b>"+buyerDetails.getAvailableName()+"</b>";
                if ( buyerDetails.getAlternateMobile() != null && buyerDetails.getAlternateMobile().length() > 0 ){
                    customerDetailsString += "<br>MOBILE: <b>"+CommonMethods.removeCountryCodeFromNumber(buyerDetails.getMobileNumber())+" / "+CommonMethods.removeCountryCodeFromNumber(buyerDetails.getAlternateMobile())+"</b>";
                }else{
                    customerDetailsString += "<br>MOBILE: <b>"+CommonMethods.removeCountryCodeFromNumber(buyerDetails.getMobileNumber())+"</b>";
                }
                if ( buyerDetails.getEmail() != null && buyerDetails.getEmail().length() > 0 ){
                    customerDetailsString += "<br>EMAIL: <b>"+buyerDetails.getEmail()+"</b>";
                }
                if ( buyerDetails.getOutstandingAmount() < 0 ){
                    customerDetailsString += "<br><br>CURRENT OUTSTANDING:  <b>-"+SpecialCharacters.RS + "" + CommonMethods.getIndianFormatNumber(Math.abs(buyerDetails.getOutstandingAmount())) + "</b> (Return)";
                }else if  ( buyerDetails.getOutstandingAmount() > 0 ) {
                    customerDetailsString += "<br><br>CURRENT OUTSTANDING:  <b>"+SpecialCharacters.RS + "" + CommonMethods.getIndianFormatNumber(buyerDetails.getOutstandingAmount())+"</b> (Collect)";
                }
                viewHolder.customer_details.setText(Html.fromHtml(customerDetailsString.toUpperCase()));

            }

            //----

            vWrappingLinearLayoutManager vWrappingLinearLayoutManager = new vWrappingLinearLayoutManager(context);
            vWrappingLinearLayoutManager.setOrientation(OrientationHelper.VERTICAL);
            viewHolder.transactionsHolder.setLayoutManager(vWrappingLinearLayoutManager);//new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
            viewHolder.transactionsHolder.setHasFixedSize(false);
            viewHolder.transactionsHolder.setNestedScrollingEnabled(false);
            viewHolder.transactionsHolder.setItemAnimator(new DefaultItemAnimator());
            MoneyCollectionTransactionsAdapter moneyCollectionTransactionsAdapter = new MoneyCollectionTransactionsAdapter(activity, moneyCollectionDetails.getTransactions(), new MoneyCollectionTransactionsAdapter.Callback() {
                @Override
                public void onItemClick(int transactionPosition) {
                    if ( callback != null ){
                        callback.onTransactionClick(position, transactionPosition);
                    }
                }
            });
            viewHolder.transactionsHolder.setAdapter(moneyCollectionTransactionsAdapter);


            //------

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //if ( activity.localStorageData.isUserCheckedIn() ) {
                            callback.onItemClick(position);
                        //}else{
                        //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                        //}
                    }
                });
                viewHolder.callCustomer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onCallCustomer(position);
                    }
                });
                viewHolder.showOrderDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onShowOrderDetails(position);
                    }
                });
                viewHolder.showBuyerHistory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onShowBuyerHistory(position);
                    }
                });
            }
        }
    }

    private void setColors(ViewHolder viewHolder,int normalColor, int fabColor){
        viewHolder.container.setCardBackgroundColor(context.getResources().getColor(normalColor));
//        viewHolder.callCustomer.setColorNormalResId(fabColor);
//        viewHolder.showOrderDetails.setColorNormalResId(fabColor);
//        viewHolder.showBuyerHistory.setColorNormalResId(fabColor);
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<MoneyCollectionDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<MoneyCollectionDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(int position, MoneyCollectionDetails moneyCollectionDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, moneyCollectionDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public MoneyCollectionDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<MoneyCollectionDetails> getAllItems(){
        return itemsList;
    }

}