package com.mastaan.logistics.adapters;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.aleena.common.utils.vFragmentStatePagerAdapter;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.fragments.MeatItemsFragment;
import com.mastaan.logistics.models.GroupedMeatItemsList;

import java.util.List;

/**
 * Created by venkatesh on 29/4/16.
 */

public class MeatItemsPagerAdapter extends vFragmentStatePagerAdapter{//vFragmentStatePagerAdapter {
    List<GroupedMeatItemsList> item_list;

    public MeatItemsPagerAdapter(FragmentManager fm/*, List<GroupedMeatItemsList> item_list*/) {
        super(fm);
        /*this.item_list = item_list;*/
    }

    public MeatItemsPagerAdapter setItems(List<GroupedMeatItemsList> item_list){
        this.item_list = item_list;
        notifyDataSetChanged();
        return this;
    }

    @Override
    public Fragment getItem(int position) {
        MeatItemsFragment fragment = new MeatItemsFragment();
        Bundle bundle = new Bundle();
        if ( hubDetails != null && hubDetails.trim().length() > 0 ){
            bundle.putString(Constants.HUB_DETAILS, hubDetails);
        }
        if ( forStock ) {
            bundle.putBoolean(Constants.FOR_STOCK, true);
        }
        if ( showItemLevelAttributesOptionsSelection ){
            bundle.putBoolean(Constants.SHOW_ITEM_LEVEL_ATTRIBUTES_OPTIONS_SELECTION, true);
        }
        bundle.putString("categoryName", item_list.get(position).getCategoryName());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return item_list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return item_list.get(position).getItems().get(0).getMeatItemDetais().getCategoryDetails().getBelongsCategoryName();
    }

    //------

    String hubDetails;
    public MeatItemsPagerAdapter setHubDetails(String hubDetails) {
        this.hubDetails = hubDetails;
        return this;
    }

    boolean forStock;
    public MeatItemsPagerAdapter setForStock() {
        this.forStock = true;
        return this;
    }

    boolean showItemLevelAttributesOptionsSelection;
    public MeatItemsPagerAdapter showItemLevelAttributesOptionsSelection() {
        this.showItemLevelAttributesOptionsSelection = true;
        return this;
    }
}
