package com.mastaan.logistics.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.MoneyCollectionTransactionDetails;

import java.util.List;

public class MoneyCollectionTransactionsAdapter extends RecyclerView.Adapter<MoneyCollectionTransactionsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<MoneyCollectionTransactionDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        View container;
        View itemSelector;

        TextView payment_mode;
        TextView details;
        TextView amount_collected;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            container = itemLayoutView.findViewById(R.id.container);
            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);

            payment_mode = (TextView) itemLayoutView.findViewById(R.id.payment_mode);
            details = (TextView) itemLayoutView.findViewById(R.id.details);
            amount_collected = (TextView) itemLayoutView.findViewById(R.id.amount_collected);
        }
    }

    public MoneyCollectionTransactionsAdapter(MastaanToolbarActivity activity, List<MoneyCollectionTransactionDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MoneyCollectionTransactionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_money_collection_transaction, parent, false);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final MoneyCollectionTransactionDetails transactionDetails = itemsList.get(position);
        if ( transactionDetails != null ){

            if ( transactionDetails.isConsolidated() ){
                viewHolder.container.setBackgroundColor(context.getResources().getColor(R.color.delivered_color));
            }else{
                viewHolder.container.setBackgroundColor(context.getResources().getColor(R.color.default_color));
            }

            viewHolder.payment_mode.setText(transactionDetails.getPaymentTypeString().replaceAll(Constants.CASH_ON_DELIVERY, "CASH").replaceAll(Constants.ONLINE_PAYMENT, "ONLINE PAYMENT"));

            String infoDetails = "";
            if ( transactionDetails.getPaymentID()!=null ){
                infoDetails += "PAYMENT ID:  <b>"+transactionDetails.getPaymentID()+"</b>";
            }

            if ( transactionDetails.getUpdatedBy() != null ){
                if ( infoDetails.length() > 0 ){    infoDetails += "<br>";  }
                infoDetails += "UPDATED: <b>"+transactionDetails.getUpdatedBy().getAvailableName().toUpperCase()+"</b>";
            }

            if ( infoDetails.length() > 0 ){    infoDetails += "<br>";  }
            infoDetails += (transactionDetails.getAmount()<0?"RETURNED":"COLLECTED")+":  <b>"+DateMethods.getReadableDateFromUTC(transactionDetails.getCollectionDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b>";
            /*if ( transactionDetails.getConsolidationDate().length() > 0 ){
                infoDetails += "<br>CONSOLIDATED:  <b>"+ DateMethods.getDateInFormat(transactionDetails.getConsolidationDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b>";
            }*/

            String paymentType = transactionDetails.getPaymentTypeString();
            if ( paymentType.equalsIgnoreCase(Constants.ONLINE_PAYMENT) == false
                    && paymentType.equalsIgnoreCase(Constants.PAYTM_WALLET) == false
                    && paymentType.equalsIgnoreCase(Constants.WALLET) == false ){
                infoDetails += "<br>DELIVERY: <b>"+transactionDetails.getDeliveryBoyDetails().getAvailableName().toUpperCase()+"</b>";
                if ( transactionDetails.isConsolidated() ){
                    infoDetails += "<br>CONSOLIDATOR:  <b>"+transactionDetails.getConsolidatorDetails().getAvailableName().toUpperCase()+"</b> (<b>"+DateMethods.getReadableDateFromUTC(transactionDetails.getConsolidationDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b>)";
                }
            }
            if ( transactionDetails.getOutstandingAtThatTime() > 0 ){
                if ( infoDetails.length() > 0 ){    infoDetails += "<br>";  }
                infoDetails += "OUTSTANDING:  <b>"+SpecialCharacters.RS+""+CommonMethods.getIndianFormatNumber(transactionDetails.getOutstandingAtThatTime())+" (COLLECT) </b>";
            }else if ( transactionDetails.getOutstandingAtThatTime() < 0 ){
                if ( infoDetails.length() > 0 ){    infoDetails += "<br>";  }
                infoDetails += "OUTSTANDING:  <b>- "+SpecialCharacters.RS+""+CommonMethods.getIndianFormatNumber(Math.abs(transactionDetails.getOutstandingAtThatTime()))+" (RETURN)</b>";
            }

            if ( transactionDetails.getAmount() >= 0 ) {
                infoDetails += "<br><br><b>" + transactionDetails.getStatus() + "</b>";
            }

            viewHolder.details.setText(Html.fromHtml(infoDetails));

            if ( transactionDetails.getAmount() >= 0 ) {
                viewHolder.amount_collected.setText(SpecialCharacters.RS + "" + CommonMethods.getIndianFormatNumber(transactionDetails.getAmount()));
            }else{
                viewHolder.amount_collected.setText("- "+SpecialCharacters.RS+""+ CommonMethods.getIndianFormatNumber(Math.abs(transactionDetails.getAmount())));
            }

            //--------

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //if ( activity.localStorageData.isUserCheckedIn() ) {
                            callback.onItemClick(position);
                        //}else{
                        //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                        //}
                    }
                });
            }
        }
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<MoneyCollectionTransactionDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<MoneyCollectionTransactionDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(int position, MoneyCollectionTransactionDetails moneyCollectionTransactionDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, moneyCollectionTransactionDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public MoneyCollectionTransactionDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<MoneyCollectionTransactionDetails> getAllItems(){
        return itemsList;
    }

}