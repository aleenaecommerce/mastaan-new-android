package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.FollowupDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 09/02/2018.
 */

public class FollowupsAdapter extends RecyclerView.Adapter<FollowupsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<FollowupDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
        void onShowOrder(int position);
        void callBuyer(int position);
        void onShowBuyerHistory(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemSelector;
        ImageView buyer_image;
        TextView buyer_name;
        TextView type;
        TextView buyer_last_order_date;
        TextView date;
        TextView comments;
        TextView employee_name;
        TextView order;
        View callBuyer;
        View buyerHistory;
        View excluded_indicator;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            buyer_image = itemLayoutView.findViewById(R.id.buyer_image);
            buyer_name = itemLayoutView.findViewById(R.id.buyer_name);
            type = itemLayoutView.findViewById(R.id.type);
            buyer_last_order_date = itemLayoutView.findViewById(R.id.buyer_last_order_date);
            date = itemLayoutView.findViewById(R.id.date);
            comments = itemLayoutView.findViewById(R.id.comments);
            employee_name = itemLayoutView.findViewById(R.id.employee_name);
            order = itemLayoutView.findViewById(R.id.order);
            callBuyer = itemLayoutView.findViewById(R.id.callBuyer);
            buyerHistory = itemLayoutView.findViewById(R.id.buyerHistory);
            excluded_indicator = itemLayoutView.findViewById(R.id.excluded_indicator);
        }
    }

    public FollowupsAdapter(MastaanToolbarActivity activity, List<FollowupDetails> followupsList, CallBack callBack) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = followupsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public FollowupsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_followup, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        try{
            final FollowupDetails followupDetails = getItem(position);
            if ( followupDetails != null ){
                viewHolder.buyer_name.setText(followupDetails.getBuyerName());

                viewHolder.type.setText(followupDetails.getTypeName());

                if ( followupDetails.getBuyerDetails() != null && followupDetails.getBuyerDetails().getLastOrderDate() != null && followupDetails.getBuyerDetails().getLastOrderDate().length() > 0 ){
                    viewHolder.buyer_last_order_date.setVisibility(View.VISIBLE);
                    viewHolder.buyer_last_order_date.setText("Last order: "+DateMethods.getReadableDateFromUTC(followupDetails.getBuyerDetails().getLastOrderDate(), DateConstants.MMM_DD_YYYY));
                }else{  viewHolder.buyer_last_order_date.setVisibility(View.GONE);  }

                viewHolder.date.setText(DateMethods.getReadableDateFromUTC(followupDetails.getCreatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A));
                viewHolder.comments.setText(CommonMethods.fromHtml(followupDetails.getComments()));
                viewHolder.employee_name.setText("By "+followupDetails.getEmployeeName());

                if ( followupDetails.getOrderID() != null && followupDetails.getOrderID().length() > 0 ){
                    viewHolder.order.setVisibility(View.VISIBLE);
                    viewHolder.order.setText(CommonMethods.fromHtml("<u>"+(followupDetails.isResultedInOrder()?"Followup resulted order":"Linked order")+"</u"));
                }else{  viewHolder.order.setVisibility(View.GONE);  }

                viewHolder.callBuyer.setVisibility(followupDetails.getBuyerDetails()!=null&&followupDetails.getBuyerDetails().getMobileNumber()!=null?View.VISIBLE:View.GONE);

                viewHolder.excluded_indicator.setVisibility(followupDetails.isExcluded()?View.VISIBLE:View.GONE);

                if ( callBack != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callBack.onItemClick(position);
                        }
                    });
                    viewHolder.order.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callBack.onShowOrder(position);
                        }
                    });
                    viewHolder.callBuyer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callBack.callBuyer(position);
                        }
                    });
                    viewHolder.buyerHistory.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callBack.onShowBuyerHistory(position);
                        }
                    });
                }
            }
        }catch (Exception e){}
    }

    //---------------

    public void setItems(List<FollowupDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItem(FollowupDetails followupDetails){
        addItem(-1, followupDetails);
    }
    public void addItem(int position, FollowupDetails followupDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.add(position, followupDetails);
        }else{
            itemsList.add(followupDetails);
        }
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public FollowupDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<FollowupDetails> getAllItems(){
        return itemsList;
    }

}