package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.models.DeliverySlotDetails;

import java.util.ArrayList;
import java.util.List;

public class DeliverySlotsCheckBoxAdapter extends RecyclerView.Adapter<DeliverySlotsCheckBoxAdapter.ViewHolder> {

    Context context;
    List<DeliverySlotDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position, boolean isEnabled);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        CheckBox checkbox;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            checkbox = (CheckBox) itemLayoutView.findViewById(R.id.checkbox);
        }
    }

    public DeliverySlotsCheckBoxAdapter(Context context, List<DeliverySlotDetails> itemsList, Callback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public DeliverySlotsCheckBoxAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_check_item, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final DeliverySlotDetails deliverySlotDetails = itemsList.get(position);
        if ( deliverySlotDetails != null ){

            viewHolder.checkbox.setText(
                    DateMethods.getTimeIn12HrFormat(deliverySlotDetails.getStartTime())+" to "+DateMethods.getTimeIn12HrFormat(deliverySlotDetails.getEndTime())
                    + " ["+ CommonMethods.getIndianFormatNumber(deliverySlotDetails.getMinimumDistance())+" kms to "+CommonMethods.getIndianFormatNumber(deliverySlotDetails.getMaximumDistance())+" kms]"
                    + " ("+ deliverySlotDetails.getOrdersCount()+" of "+ deliverySlotDetails.getMaximumOrders()+")"
            );
            if ( deliverySlotDetails.isEnabled() ){
                viewHolder.checkbox.setChecked(true);
            }else{
                viewHolder.checkbox.setChecked(false);
            }

            viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( viewHolder.checkbox.isChecked() == false) {
                        viewHolder.checkbox.setChecked(true);
                        setItem(position, deliverySlotDetails.setEnabled(true));

                        if ( callback != null ) {
                            callback.onItemClick(position, true);
                        }
                    } else {
                        viewHolder.checkbox.setChecked(false);
                        setItem(position, deliverySlotDetails.setEnabled(false));

                        if ( callback != null ) {
                            callback.onItemClick(position, false);
                        }
                    }
                }
            });

        }
    }

    public List<String> getEnableSlots(){
        List<String> enabledSlots = new ArrayList<>();
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).isEnabled() ){
                enabledSlots.add(itemsList.get(i).getID());
            }
        }
        return enabledSlots;
    }

    public List<String> getDisabledSlots(){
        List<String> disabledSlots = new ArrayList<>();
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).isEnabled() == false ){
                disabledSlots.add(itemsList.get(i).getID());
            }
        }
        return disabledSlots;
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<DeliverySlotDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<DeliverySlotDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(int position, DeliverySlotDetails deliverySlotDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, deliverySlotDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public DeliverySlotDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<DeliverySlotDetails> getAllItems(){
        return itemsList;
    }

}