package com.mastaan.logistics.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.AttributeOptionAvailabilityDetails;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.List;

public class EditAttributeOptionsAdapter extends RecyclerView.Adapter<EditAttributeOptionsAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    UserDetails userDetails;
    String todaysDate = "";
    List<AttributeOptionDetails> itemsList;
    boolean isAvailabilityChangeDaywise;

    Callback callback;

    public interface Callback {
        void onMenuClick(int position, View view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView option_name;
        View actionsView;
        View menuOptions;
        TextView option_details;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            option_name = (TextView) itemLayoutView.findViewById(R.id.option_name);
            actionsView = itemLayoutView.findViewById(R.id.actionsView);
            actionsView = itemLayoutView.findViewById(R.id.actionsView);
            menuOptions = itemLayoutView.findViewById(R.id.menuOptions);
            option_details = (TextView) itemLayoutView.findViewById(R.id.option_details);
        }
    }

    public EditAttributeOptionsAdapter(MastaanToolbarActivity activity, UserDetails userDetails, List<AttributeOptionDetails> itemsList, boolean isAvailabilityChangeDaywise, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.userDetails = userDetails;
        this.todaysDate = DateMethods.getOnlyDate(activity.localStorageData.getServerTime());
        this.itemsList = itemsList;
        this.isAvailabilityChangeDaywise = isAvailabilityChangeDaywise;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public EditAttributeOptionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_edit_attribute_option, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final AttributeOptionDetails attributeOptionDetails = itemsList.get(position);
        if ( attributeOptionDetails != null ){

            viewHolder.option_name.setText(attributeOptionDetails.getName().toUpperCase());

            if ( isAvailabilityChangeDaywise ) {
                if ( userDetails.isProcessingManager() ){
                    viewHolder.actionsView.setVisibility(View.VISIBLE);
                }else{
                    viewHolder.actionsView.setVisibility(View.GONE);
                }
                viewHolder.option_details.setVisibility(View.VISIBLE);

                viewHolder.option_details.setText(
                        "\n" +
                                "\n" +
                                "\n" +
                                "Loading details, wait...\n" +
                                "\n" +
                                "\n" +
                                "");
                new AsyncTask<Void, Void, Void>() {
                    List<AttributeOptionAvailabilityDetails> availabilitiesList;

                    @Override
                    protected Void doInBackground(Void... voids) {
                        availabilitiesList = attributeOptionDetails.getFullAvlabilitiesList(todaysDate, 7);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        String optionDetails = "";
                        for (int i = 0; i < availabilitiesList.size(); i++) {
                            if (optionDetails.length() > 0) {
                                optionDetails += "\n";
                            }
                            AttributeOptionAvailabilityDetails availabilityDetails = availabilitiesList.get(i);
                            if (availabilityDetails.isAvailable()) {
                                optionDetails += CommonMethods.capitalizeFirstLetter(DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD_YYYY)) + ",  Price difference: " + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(availabilityDetails.getPriceDifference());
                            } else {
                                optionDetails += CommonMethods.capitalizeFirstLetter(DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD_YYYY)) + ",  Unavailable";
                            }
                        }
                        viewHolder.option_details.setText(optionDetails);
                    }
                }.execute();
            }else{
                viewHolder.actionsView.setVisibility(View.GONE);
                if ( attributeOptionDetails.getPriceDifference() > 0 ){
                    viewHolder.option_details.setVisibility(View.VISIBLE);
                    viewHolder.option_details.setText("Price difference: "+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(attributeOptionDetails.getPriceDifference()));
                }else{
                    viewHolder.option_details.setVisibility(View.GONE);
                }
            }

            if ( callback != null ){
                viewHolder.menuOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //if ( activity.localStorageData.isUserCheckedIn() ) {
                            callback.onMenuClick(position, viewHolder.menuOptions);
                        //}else{
                        //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                        //}
                    }
                });
            }


        }
    }


    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<AttributeOptionDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<AttributeOptionDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(int position, AttributeOptionDetails attributeOptionDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, attributeOptionDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public AttributeOptionDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<AttributeOptionDetails> getAllItems(){
        return itemsList;
    }

}