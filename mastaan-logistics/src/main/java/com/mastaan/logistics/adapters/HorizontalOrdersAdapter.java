package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.SpecialCharacters;
import com.mastaan.logistics.R;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.OrderItemDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by aleena on 29/1/16.
 */
public class HorizontalOrdersAdapter extends RecyclerView.Adapter<HorizontalOrdersAdapter.ViewHolder> {

    Context context;
    LocalStorageData localStorageData;
    List<OrderItemDetails> itemsList;

    public HorizontalOrdersAdapter(Context context, List<OrderItemDetails> itemsList){//}, Callback callback) {
        this.context = context;
        this.localStorageData = new LocalStorageData(context);
        this.itemsList = itemsList;
        //this.callback = callback;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView item_image;
        TextView item_name;
        TextView item_price;
        TextView item_quantity;

        public ViewHolder(View itemView) {
            super(itemView);
            item_image = (ImageView) itemView.findViewById(R.id.item_image);
            item_name = (TextView) itemView.findViewById(R.id.item_name);
            item_price = (TextView) itemView.findViewById(R.id.item_price);
            item_quantity = (TextView) itemView.findViewById(R.id.item_quantity);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.view_horizontal_order_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final OrderItemDetails orderItemDetails = itemsList.get(position);

        if ( orderItemDetails != null ) {

            viewHolder.item_name.setText(orderItemDetails.getMeatItemDetails().getName());

            Picasso.get()
                    .load(orderItemDetails.getMeatItemDetails().getPicture())
                    .placeholder(R.drawable.image_default)
                    .error(R.drawable.image_default)
                    .into(viewHolder.item_image);

            viewHolder.item_quantity.setText(orderItemDetails.getFormattedQuantity() + ", "+ orderItemDetails.getFormattedExtras());
            viewHolder.item_price.setText(SpecialCharacters.RS + orderItemDetails.getAmountOfItem() + " ("+ orderItemDetails.getPaymentTypeString()+")");

        }

    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<OrderItemDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<OrderItemDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public OrderItemDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<OrderItemDetails> getAllItems(){
        return itemsList;
    }



}
