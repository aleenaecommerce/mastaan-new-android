package com.mastaan.logistics.adapters;

import android.content.Context;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.AvailabilityDetails;

import java.util.ArrayList;
import java.util.List;

public class AvailabilitiesAdapter extends ArrayAdapter<AvailabilityDetails>{

    Context context;
    String todaysDate = "";
    String tomorrowsDate = "";
    LayoutInflater inflater;
    List<AvailabilityDetails> itemsList;

    class ViewHolder {          // Food Item View Holder
        TextView name;
    }

    public AvailabilitiesAdapter(Context context, List<AvailabilityDetails> itemsList) {
        super(context, R.layout.view_list_item, itemsList);
        this.context = context;
        this.todaysDate = DateMethods.getOnlyDate(new LocalStorageData(context).getServerTime());
        this.tomorrowsDate = DateMethods.addToDateInDays(todaysDate, 1);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.itemsList = itemsList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if ( convertView == null ) {
            convertView = inflater.inflate(R.layout.view_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        setupView(position, viewHolder, false);


        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if ( convertView == null ) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_list_item_dropdown_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        setupView(position, viewHolder, true);        // Display Details

        return convertView;
    }

    public void setupView(int position, ViewHolder viewHolder, boolean showInfo){

        AvailabilityDetails availabilityDetails = getItem(position);

        if ( availabilityDetails != null ){
            String date = DateMethods.getWeekDayFromDate(availabilityDetails.getDate());
            if ( DateMethods.compareDates(todaysDate, availabilityDetails.getDate()) == 0 ){
                date = "Today";
            }else if ( DateMethods.compareDates(tomorrowsDate, availabilityDetails.getDate()) == 0 ){
                date = "Tomorrow";
            }
            date += " ("+DateMethods.getDateInFormat(availabilityDetails.getDate(), DateConstants.MMM_DD)+")";

            // FOR DROP DOWN
            if ( showInfo ) {
                if (availabilityDetails.isAvailable()) {
                    viewHolder.name.setText(date+"   [" + SpecialCharacters.RS + "" + CommonMethods.getIndianFormatNumber(availabilityDetails.getSellingPrice())+"]");
                    if ( availabilityDetails.getActualSellingPrice() > availabilityDetails.getSellingPrice() ){
                        try{
                            viewHolder.name.setText(date+"   [", TextView.BufferType.SPANNABLE);
                            int strikeStart = viewHolder.name.getText().toString().length();
                            viewHolder.name.append(SpecialCharacters.RS + CommonMethods.getInDecimalFormat(availabilityDetails.getActualSellingPrice())+" ");
                            ((Spannable) viewHolder.name.getText()).setSpan(new StrikethroughSpan(), strikeStart, viewHolder.name.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            viewHolder.name.append(" "+SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(availabilityDetails.getSellingPrice())+ "]");
                        }catch (Exception e){}
                    }
                } else {
                    viewHolder.name.setText(date+"  [Unavailable]");
                }
            }else{
                viewHolder.name.setText(date);
            }
        }
    }

    //===============

    public void setItems(List<AvailabilityDetails> listItems) {
        this.itemsList = listItems;
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItem(AvailabilityDetails item){
        itemsList.add(item);
        notifyDataSetChanged();
    }

    public void updateItem(int position, AvailabilityDetails availabilityDetails){
        itemsList.set(position, availabilityDetails);
        notifyDataSetChanged();
    }

    public void deleteItem(int position){
        itemsList.remove(position);
        notifyDataSetChanged();
    }

    public List<AvailabilityDetails> getAllItems(){
        if ( itemsList != null ){
            return itemsList;
        }
        return new ArrayList<>();
    }

}
