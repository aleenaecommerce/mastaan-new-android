package com.mastaan.logistics.adapters;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.utils.vFragmentStatePagerAdapter;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.fragments.FeedbacksFragment;
import com.mastaan.logistics.models.GroupedFeedbacksList;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 12/7/16.
 */

public class FeedbacksPagerAdapter extends vFragmentStatePagerAdapter {
    List<GroupedFeedbacksList> item_list;

    public FeedbacksPagerAdapter(FragmentManager fm, List<GroupedFeedbacksList> item_list) {
        super(fm);
        this.item_list = item_list;
    }

    @Override
    public Fragment getItem(int position) {
        FeedbacksFragment fragment = new FeedbacksFragment();
        Bundle bundle = new Bundle();
        bundle.putString("feedbacksType", Constants.GROUPED_FEEDBACKS);
        bundle.putString("orderID", item_list.get(position).getOrderID());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return item_list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(item_list.get(position).getDeliveryDate()), DateConstants.MMM_DD);
    }

    //--------

    public long getItemsCount(){
        return item_list.size();
    }

    public void deleteItem(ViewPager viewPager, int position){
        if ( item_list.size() > position ){

            item_list.remove(position);
            notifyDataSetChanged();

            if (position == viewPager.getCurrentItem()) {
                if(position == (item_list.size()-1)) {
                    viewPager.setCurrentItem(position-1);
                } else if (position == 0){
                    viewPager.setCurrentItem(1);
                }
            }
        }
    }
}
