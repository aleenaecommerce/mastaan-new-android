package com.mastaan.logistics.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.DayDetails;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by Venkatesh Uppu on 09/09/2016.
 */
public class ManageDatesAdapter extends ArrayAdapter<DayDetails> {
    MastaanToolbarActivity activity;
    Context context;
    LayoutInflater inflater;
    List<DayDetails> itemsList;
    Callback callback;

    Calendar todayCalendar;
    int currentViewingMonth;

    public interface Callback {
        void onClick(int position);
    }

    public class ViewHolder{
        View itemSelector;
        CardView cardView;
        TextView date;
        TextView info;
    }

    public ManageDatesAdapter(MastaanToolbarActivity activity, List<DayDetails> daysList, int currentViewingMonth, Callback callback) {
        super(activity, R.layout.view_calender_day, daysList);
        this.activity = activity;
        this.context = activity;
        inflater = LayoutInflater.from(activity);
        this.itemsList = daysList;
        this.todayCalendar = DateMethods.getCalendarFromDate(new LocalStorageData(context).getServerTime());
        this.currentViewingMonth = currentViewingMonth;
        this.callback = callback;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if ( convertView == null ) {
            convertView = inflater.inflate(R.layout.view_calender_day, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.itemSelector = convertView.findViewById(R.id.button);
            viewHolder.cardView = (CardView) convertView.findViewById(R.id.item_card);
            viewHolder.date = (TextView) convertView.findViewById(R.id.date);
            viewHolder.info = (TextView) convertView.findViewById(R.id.number_of_km);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //-----------

        DayDetails dayDetails = getItem(position);

        if ( dayDetails != null ){
            Date date = DateMethods.getDateFromString(dayDetails.getDate());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            if (calendar.get(Calendar.MONTH) != currentViewingMonth) {
                viewHolder.cardView.setVisibility(View.INVISIBLE);
            }
            else if ( calendar.get(Calendar.DAY_OF_MONTH) == todayCalendar.get(Calendar.DAY_OF_MONTH) && calendar.get(Calendar.MONTH) == todayCalendar.get(Calendar.MONTH) && calendar.get(Calendar.YEAR) == todayCalendar.get(Calendar.YEAR)) {
                viewHolder.cardView.setVisibility(View.VISIBLE);
                viewHolder.date.setTypeface(null, Typeface.BOLD);
                viewHolder.date.setTextColor(context.getResources().getColor(R.color.calendar_today));
            }
            else{
                viewHolder.cardView.setVisibility(View.VISIBLE);
                viewHolder.date.setTypeface(null, Typeface.NORMAL);
                viewHolder.date.setTextColor(context.getResources().getColor(R.color.white));
            }

            //------

            viewHolder.date.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
            viewHolder.info.setText("");
            if ( dayDetails.isEnabled() ){
                viewHolder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.calendar_normal));
            }else{
                viewHolder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.calendar_disabled));
            }

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //if ( activity.localStorageData.isUserCheckedIn() ) {
                            callback.onClick(position);
                        //}else{
                        //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                        //}
                    }
                });
            }
        }


        return convertView;
    }

    //--------

    public void setItems(List<DayDetails> daysList) {
        if ( daysList != null ) {
            this.itemsList = daysList;
            notifyDataSetChanged();
        }
    }

    public void addItems(List<DayDetails> daysList) {
        if ( daysList != null && daysList.size() > 0 ){
            for (int i=0;i<daysList.size();i++){
                itemsList.add(daysList.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void updateItem(int position, DayDetails dayDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, dayDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void deleteItem(int position){
        itemsList.remove(position);
        notifyDataSetChanged();
    }


}
