package com.mastaan.logistics.adapters;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.aleena.common.utils.vFragmentStatePagerAdapter;
import com.mastaan.logistics.fragments.OrdersFragment;
import com.mastaan.logistics.models.GroupedOrdersList;

import java.util.List;

/**
 * Created by Venkatesh on 12/8/16.
 */
public class OrdersPagerAdapter extends vFragmentStatePagerAdapter{//vFragmentStatePagerAdapter {
    List<GroupedOrdersList> item_list;

    public OrdersPagerAdapter(FragmentManager fm, List<GroupedOrdersList> item_list) {
        super(fm);
        this.item_list = item_list;
    }

    @Override
    public Fragment getItem(int position) {
        OrdersFragment fragment = new OrdersFragment();
        Bundle bundle = new Bundle();
        bundle.putString("statusType", item_list.get(position).getCategoryName());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return item_list.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return item_list.get(position).getCategoryName();
    }
}
