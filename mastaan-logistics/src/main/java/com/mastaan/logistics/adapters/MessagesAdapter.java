package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.models.MessageDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {
    Context context;
    List<MessageDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        View fader;
        TextView type;
        ImageView image;
        TextView title;
        TextView details;
        View actionDelete;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            fader= itemLayoutView.findViewById(R.id.fader);
            type = itemLayoutView.findViewById(R.id.type);
            image = itemLayoutView.findViewById(R.id.image);
            title = itemLayoutView.findViewById(R.id.title);
            details = itemLayoutView.findViewById(R.id.details);
            actionDelete = itemLayoutView.findViewById(R.id.actionDelete);
        }
    }

    public MessagesAdapter(Context context, List<MessageDetails> messagesList, CallBack callBack) {
        this.context = context;
        this.itemsList = messagesList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_message, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final MessageDetails messageDetails = itemsList.get(position);

        try{
            if ( messageDetails != null ){

                viewHolder.fader.setVisibility(messageDetails.getStatus()?View.GONE:View.VISIBLE);

                viewHolder.type.setText(messageDetails.getType());

                if ( messageDetails.getImage() != null && messageDetails.getImage().length() > 0 ){
                    viewHolder.image.setVisibility(View.VISIBLE);
                    Picasso.get()
                            .load(messageDetails.getImage())
                            .into(viewHolder.image);
                }else{  viewHolder.image.setVisibility(View.GONE);  }

                if ( messageDetails.getTitle() != null && messageDetails.getTitle().length() > 0 ){
                    viewHolder.title.setVisibility(View.VISIBLE);
                    viewHolder.title.setText(CommonMethods.fromHtml(messageDetails.getTitle()));
                }else{  viewHolder.title.setVisibility(View.GONE);  }

                String details = "";
                if ( messageDetails.getDetails() != null && messageDetails.getDetails().length() > 0 ){
                    details += messageDetails.getDetails();
                }
                if ( messageDetails.getStartDate() != null && messageDetails.getStartDate().length() > 0 ){
                    if ( details.length() > 0 ){    details += "<br>";    }
                    details += "Start Date: <b>"+ DateMethods.getReadableDateFromUTC(messageDetails.getStartDate(), DateConstants.MMM_DD_YYYY)+"</b>";
                }
                if ( messageDetails.getEndDate() != null && messageDetails.getEndDate().length() > 0 ){
                    if ( details.length() > 0 ){    details += "<br>";    }
                    details += "End Date: <b>"+ DateMethods.getReadableDateFromUTC(messageDetails.getEndDate(), DateConstants.MMM_DD_YYYY)+"</b>";
                }
                if ( messageDetails.getAction() != null && messageDetails.getAction().length() > 0 ){
                    if ( details.length() > 0 ){    details += "<br>";    }
                    details += "Action: <b>"+ messageDetails.getAction()+"</b>";
                }
                if ( messageDetails.getURL() != null && messageDetails.getURL().length() > 0 ){
                    if ( details.length() > 0 ){    details += "<br>";    }
                    details += "URL: <b>"+ messageDetails.getURL()+"</b>";
                }
                if ( messageDetails.getAppURL() != null && messageDetails.getAppURL().length() > 0 ){
                    if ( details.length() > 0 ){    details += "<br>";    }
                    details += "App URL: <b>"+ messageDetails.getAppURL()+"</b>";
                }
                if ( messageDetails.showOnce() ){
                    if ( details.length() > 0 ){    details += "<br>";    }
                    details += "<b>Show only once</b>";
                }

                if ( details.length() > 0 ){
                    viewHolder.details.setVisibility(View.VISIBLE);
                    viewHolder.details.setText(CommonMethods.fromHtml(details));
                }else{  viewHolder.details.setVisibility(View.GONE);    }

                if ( callBack != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callBack.onItemClick(position);
                        }
                    });

                    viewHolder.actionDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callBack.onDeleteClick(position);
                        }
                    });
                }
            }
        }catch (Exception e){}

    }

    //---------------

    public void setItems(List<MessageDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<MessageDetails> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }
    public void addItem(MessageDetails messageDetails){
        itemsList.add(messageDetails);
        notifyDataSetChanged();
    }
    public void addItem(int position, MessageDetails messageDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.add(position, messageDetails);
        }else{
            itemsList.add(messageDetails);
        }
        notifyDataSetChanged();
    }

    public void setItem(int position, MessageDetails messageDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, messageDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public MessageDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<MessageDetails> getAllItems(){
        return itemsList;
    }

}