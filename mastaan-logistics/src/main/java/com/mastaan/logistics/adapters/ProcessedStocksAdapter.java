package com.mastaan.logistics.adapters;

import android.content.Context;
import android.graphics.Color;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.ProcessedStockDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 29/07/18.
 */

public class ProcessedStocksAdapter extends RecyclerView.Adapter<ProcessedStocksAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;

    List<ProcessedStockDetails> itemsList;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        CardView container;

        TextView type;
        TextView date;
        TextView details;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            container = (CardView) itemView.findViewById(R.id.container);

            type = (TextView) itemLayoutView.findViewById(R.id.type);
            date = (TextView) itemLayoutView.findViewById(R.id.date);
            details = (TextView) itemLayoutView.findViewById(R.id.details);
        }
    }

    public ProcessedStocksAdapter(MastaanToolbarActivity activity, List<ProcessedStockDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ProcessedStocksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_processed_stock, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final ProcessedStockDetails stockDetails = itemsList.get(position);
        if ( stockDetails != null ){
            if ( stockDetails.getType().equalsIgnoreCase(Constants.FARM_CHICKEN) || stockDetails.getType().equalsIgnoreCase(Constants.COUNTRY_CHICKEN) ){
                viewHolder.container.setCardBackgroundColor(Color.parseColor("#FB8C00"));
            }else if ( stockDetails.getType().equalsIgnoreCase(Constants.GOAT_MUTTON) || stockDetails.getType().equalsIgnoreCase(Constants.SHEEP_MUTTON) ){
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.color_mutton));
            }else{
                viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.default_card_color));
            }

            viewHolder.type.setText(stockDetails.getType().replaceAll(Constants.FARM_CHICKEN, "FARM CHICKEN")
                    .replaceAll(Constants.COUNTRY_CHICKEN, "COUNTRY CHICKEN")
                    .replaceAll(Constants.GOAT_MUTTON, "GOAT MUTTON")
                    .replaceAll(Constants.SHEEP_MUTTON, "SHEEP MUTTON"));

            viewHolder.date.setText(DateMethods.getReadableDateFromUTC(stockDetails.getDate(), DateConstants.MMM_DD_HH_MM_A).replaceAll(", ", "\n"));

            String beforeProcessingInfo = "";
            if ( stockDetails.getBeforeProcessingUnits() > 0 ){
                beforeProcessingInfo += "UNITS: <b>"+CommonMethods.getIndianFormatNumber(stockDetails.getBeforeProcessingUnits())+"</b>";
            }
            if ( stockDetails.getBeforeProcessingWeight() > 0 ){
                if ( beforeProcessingInfo.length() > 0 ){    beforeProcessingInfo += "<br>";  }
                beforeProcessingInfo += "WEIGHT: <b>"+getUnitsString(stockDetails.getBeforeProcessingWeight(), "kg")+"</b>";
            }

            String afterProcessingInfo = "";
            if ( stockDetails.getAfterProcessingWeight() > 0 ){
                afterProcessingInfo += "WEIGHT: <b>"+getUnitsString(stockDetails.getAfterProcessingWeight(), "kg")+"</b>";
            }
            if ( stockDetails.getAfterProcessingDressedWeight() > 0 ){
                if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += "<br>";  }
                afterProcessingInfo += "DRESSED WEIGHT: <b>"+getUnitsString(stockDetails.getAfterProcessingDressedWeight(), "kg")+"</b>";
            }
            if ( stockDetails.getAfterProcessingSkinlessWeight() > 0 ){
                if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += "<br>";  }
                afterProcessingInfo += "SKINLESS WEIGHT: <b>"+getUnitsString(stockDetails.getAfterProcessingSkinlessWeight(), "kg")+"</b>";
            }
            if ( stockDetails.getAfterProcessingLiverWeight() > 0 ){
                if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += "<br>";  }
                afterProcessingInfo += "LIVER WEIGHT: <b>"+getUnitsString(stockDetails.getAfterProcessingLiverWeight(), "kg")+"</b>";
            }
            if ( stockDetails.getAfterProcessingGizzardWeight() > 0 ){
                if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += "<br>";  }
                afterProcessingInfo += "GIZZARD WEIGHT: <b>"+getUnitsString(stockDetails.getAfterProcessingGizzardWeight(), "kg")+"</b>";
            }
            if ( stockDetails.getAfterProcessingBoteeWeight() > 0 ){
                if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += "<br>";  }
                afterProcessingInfo += "BOTEE WEIGHT: <b>"+getUnitsString(stockDetails.getAfterProcessingBoteeWeight(), "kg")+"</b>";
            }
            if ( stockDetails.getAfterProcessingHeadPieces() > 0 ){
                if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += "<br>";  }
                afterProcessingInfo += "HEADS: <b>"+getUnitsString(stockDetails.getAfterProcessingHeadPieces(), "piece")+"</b>";
            }
            if ( stockDetails.getAfterProcessingBrainPieces() > 0 ){
                if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += "<br>";  }
                afterProcessingInfo += "BRAINS: <b>"+getUnitsString(stockDetails.getAfterProcessingBrainPieces(), "piece")+"</b>";
            }
            if ( stockDetails.getAfterProcessingTrotterSets() > 0 ){
                if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += "<br>";  }
                afterProcessingInfo += "TROTTERS: <b>"+getUnitsString(stockDetails.getAfterProcessingTrotterSets(), "set")+"</b>";
            }
            if ( stockDetails.getAfterProcessingTilliPieces() > 0 ){
                if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += "<br>";  }
                afterProcessingInfo += "TILLIES: <b>"+getUnitsString(stockDetails.getAfterProcessingTilliPieces(), "piece")+"</b>";
            }

            String spoiledInfo = "";
            if ( stockDetails.getSpoiledUnits() > 0 ){
                spoiledInfo += "UNITS: <b>"+CommonMethods.getIndianFormatNumber(stockDetails.getSpoiledUnits())+"</b>";
            }
            if ( stockDetails.getSpoiledWeight() > 0 ){
                if ( spoiledInfo.length() > 0 ){    spoiledInfo += "<br>";  }
                spoiledInfo += "WEIGHT: <b>"+getUnitsString(stockDetails.getSpoiledWeight(), "kg")+"</b>";
            }

            String moreInfo = "";
            if ( stockDetails.getComments() != null && stockDetails.getComments().trim().length() > 0 ){
                moreInfo += "COMMENTS: <b>"+stockDetails.getComments()+"</b>";
            }
            if ( stockDetails.getProcessedBy() != null ){
                if ( moreInfo.length() > 0 ){    moreInfo += "<br>";  }
                moreInfo += "PROCESSED BY: <b>"+stockDetails.getProcessedByNames().toUpperCase()+"</b>";
            }

            String usersInfo = "";
            if ( stockDetails.getCreatedBy() != null && stockDetails.getCreatedBy().getID().length() > 0 ){
                usersInfo += "CREATED BY: <b>"+stockDetails.getCreatedBy().getAvailableName().toUpperCase()+"</b> ("+DateMethods.getReadableDateFromUTC(stockDetails.getCreatedDate(), DateConstants.MMM_DD_HH_MM_A)+")";
            }
            if ( stockDetails.getUpdatedBy() != null && stockDetails.getUpdatedBy().getID().length() > 0 ){
                if ( usersInfo.length() > 0 ){    usersInfo += "<br>";  }
                usersInfo += "UPDATED BY: <b>"+stockDetails.getUpdatedBy().getAvailableName().toUpperCase()+"</b> ("+DateMethods.getReadableDateFromUTC(stockDetails.getUpdatedDate(), DateConstants.MMM_DD_HH_MM_A)+")";
            }

            String detailsInfo = "";
            if ( beforeProcessingInfo.length() > 0 ){
                detailsInfo += "<b><u>BEFORE PROCESSING</u></b><br>"+beforeProcessingInfo;
            }
            if ( afterProcessingInfo.length() > 0 ){
                if (detailsInfo.length() > 0 ){ detailsInfo += "<br><br>";  }
                detailsInfo += "<b><u>AFTER PROCESSING</u></b><br>"+afterProcessingInfo;
            }
            if ( spoiledInfo.length() > 0 ){
                if (detailsInfo.length() > 0 ){ detailsInfo += "<br><br>";  }
                detailsInfo += "<b><u>SPOILED</u></b><br>"+spoiledInfo;
            }
            if ( moreInfo.length() > 0 ){
                if (detailsInfo.length() > 0 ){ detailsInfo += "<br><br>";  }
                detailsInfo += moreInfo;
            }
            if ( usersInfo.length() > 0 ){
                if (detailsInfo.length() > 0 ){ detailsInfo += "<br><br>";  }
                detailsInfo += usersInfo;
            }

            viewHolder.details.setText(CommonMethods.fromHtml(detailsInfo.toUpperCase()));

            //-------

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position);
                    }
                });
            }
        }
    }

    private String getUnitsString(double number, String unit){
        String string = CommonMethods.getIndianFormatNumber(number)+" "+unit;
        if ( number != 1 ){ string += "s";  }
        return string;
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<ProcessedStockDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<ProcessedStockDetails> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(int position, ProcessedStockDetails stockDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, stockDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public ProcessedStockDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<ProcessedStockDetails> getAllItems(){
        return itemsList;
    }

}