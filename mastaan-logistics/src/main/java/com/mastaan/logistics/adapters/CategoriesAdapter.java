package com.mastaan.logistics.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.UserDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    LocalStorageData localStorageData;

    UserDetails userDetails;

    List<CategoryDetails> itemsList;
    boolean hideActionButtons;

    Callback callback;

    public CategoriesAdapter hideActionButtons() {
        hideActionButtons = true;
        return this;
    }

    public interface Callback {
        void onItemClick(int position);
        void onChangeAvailabilityClick(int position, boolean isAvailable);
        void onChangeVisibilityClick(int position, boolean isVisible);
        void onManageSlotsClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;

        FrameLayout category_background;
        ImageView category_image;
        ImageView category_icon;
        TextView category_name;
        TextView toggleAvailability;
        View manageSlots;
        TextView toggleVisibility;
        View hiddenIndicator;

        View fader;
        TextView disabled_message;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);

            category_background = itemLayoutView.findViewById(R.id.category_background);
            category_image = itemLayoutView.findViewById(R.id.category_image);
            category_icon = itemLayoutView.findViewById(R.id.category_icon);
            category_name = itemLayoutView.findViewById(R.id.category_name);
            toggleAvailability = itemLayoutView.findViewById(R.id.toggleAvailability);
            manageSlots =  itemLayoutView.findViewById(R.id.manageSlots);
            toggleVisibility = itemLayoutView.findViewById(R.id.toggleVisibility);
            hiddenIndicator = itemLayoutView.findViewById(R.id.hiddenIndicator);

            fader = itemLayoutView.findViewById(R.id.fader);
            disabled_message = itemLayoutView.findViewById(R.id.disabled_message);
        }
    }

    public CategoriesAdapter(MastaanToolbarActivity activity, List<CategoryDetails> itemsList, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.localStorageData = new LocalStorageData(activity);
        this.itemsList = itemsList;
        this.callback = callback;

        this.userDetails = localStorageData.getUserDetails();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_category, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final CategoryDetails categoryDetails = itemsList.get(position);
        if ( categoryDetails != null ){

            try {
                GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                        new int[]{CommonMethods.parseColor(categoryDetails.getBackgroundColor()), CommonMethods.parseColor(categoryDetails.getGradientColor())});
                gradientDrawable.setCornerRadius(0f);
                viewHolder.category_background.setBackground(gradientDrawable);
            }catch (Exception e){
                viewHolder.category_background.setBackgroundColor(CommonMethods.parseColor(categoryDetails.getBackgroundColor()));
            }

            try {
                Picasso.get()
                        .load(categoryDetails.getIconURL())
                        .tag(context)
                        .into(viewHolder.category_icon);
            }catch (Exception e){}

            //---------

            viewHolder.fader.setVisibility(categoryDetails.isEnabled()&&categoryDetails.isVisible()?View.GONE:View.VISIBLE);

            viewHolder.manageSlots.setVisibility(categoryDetails.isEnabled()&&categoryDetails.isVisible()?View.VISIBLE:View.GONE);
            viewHolder.toggleVisibility.setVisibility(userDetails.isSuperUser()?View.VISIBLE:View.GONE);

            viewHolder.category_name.setText(categoryDetails.getName().toUpperCase()+" ");

            viewHolder.toggleAvailability.setText(categoryDetails.isEnabled()?"Disable":"Enable");
            if ( !categoryDetails.isEnabled() ){
                viewHolder.disabled_message.setVisibility(View.VISIBLE);
                viewHolder.disabled_message.setText("Disabled : "+categoryDetails.getDisabledMessage());
            }else{  viewHolder.disabled_message.setVisibility(View.GONE);   }

            viewHolder.toggleVisibility.setText(categoryDetails.isVisible()?"Hide":"Show");
            viewHolder.hiddenIndicator.setVisibility(categoryDetails.isVisible()?View.GONE:View.VISIBLE);


            if ( hideActionButtons ){
                viewHolder.toggleAvailability.setVisibility(View.GONE);
                viewHolder.manageSlots.setVisibility(View.GONE);
                viewHolder.toggleVisibility.setVisibility(View.GONE);
            }

            //--------

            if ( callback != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onItemClick(position);
                    }
                });
                viewHolder.toggleAvailability.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //if ( localStorageData.isUserCheckedIn() ) {
                        callback.onChangeAvailabilityClick(position, categoryDetails.isEnabled()?false:true);
                        //}else{
                        //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                        //}
                    }
                });
                viewHolder.toggleVisibility.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //if ( localStorageData.isUserCheckedIn() ) {
                        callback.onChangeVisibilityClick(position, categoryDetails.isVisible()?false:true);
                        //}else{
                        //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                        //}
                    }
                });
                viewHolder.manageSlots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //if ( localStorageData.isUserCheckedIn() ) {
                            callback.onManageSlotsClick(position);
                        //}else{
                        //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                        //}
                    }
                });
            }

        }
    }

    public void changeAvailability(int position, boolean isAvailable, String displayTitle, String displayMessage){
        if ( position < itemsList.size() ){
            itemsList.get(position).setEnabled(isAvailable);
            itemsList.get(position).setDisabledTitle(displayTitle);
            itemsList.get(position).setDisabledMessage(displayMessage);
            notifyDataSetChanged();
        }
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<CategoryDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<CategoryDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(int position, CategoryDetails categoryDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, categoryDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public CategoryDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<CategoryDetails> getAllItems(){
        return itemsList;
    }

}