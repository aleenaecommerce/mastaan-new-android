package com.mastaan.logistics.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.models.EmployeeBonusDetails;
import com.mastaan.logistics.models.OrderDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 12/10/18.
 */

public class EmployeeBonusesAdapter extends RecyclerView.Adapter<EmployeeBonusesAdapter.ViewHolder> {

    Context context;
    List<EmployeeBonusDetails> itemsList;
    CallBack callBack;


    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View itemSelector;
        TextView type;
        TextView employee_name;
        TextView date;
        TextView amount;
        TextView details;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            type = itemLayoutView.findViewById(R.id.type);
            employee_name = itemLayoutView.findViewById(R.id.employee_name);
            date = itemLayoutView.findViewById(R.id.date);
            amount = itemLayoutView.findViewById(R.id.amount);
            details = itemLayoutView.findViewById(R.id.details);
        }
    }

    public EmployeeBonusesAdapter(Context context, List<EmployeeBonusDetails> itemsList, CallBack callBack) {
        this.context = context;
        this.itemsList = itemsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public EmployeeBonusesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_employee_bonus, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final EmployeeBonusDetails employeeBonusDetails = itemsList.get(position);

        if ( employeeBonusDetails != null ){
            viewHolder.type.setText(employeeBonusDetails.getType().toUpperCase());
            viewHolder.employee_name.setText(employeeBonusDetails.getEmployeeDetails().getAvailableName());
            viewHolder.date.setText(DateMethods.getReadableDateFromUTC(employeeBonusDetails.getCreatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A));
            viewHolder.amount.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(employeeBonusDetails.getAmount()));

            String detailsString = employeeBonusDetails.getDetails();
            if ( employeeBonusDetails.getOrderDetails() != null ){
                if ( detailsString.length() > 0 ){  detailsString += "<br>";    }

                OrderDetails orderDetails = employeeBonusDetails.getOrderDetails();
                detailsString += "For <b>"+CommonMethods.capitalizeStringWords(orderDetails.getCustomerName())+"'s</b> order on <b>"+ DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b> of amount <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalAmount()+orderDetails.getTotalDiscount())+"</b>";
            }

            viewHolder.details.setText(Html.fromHtml(detailsString));

            if ( callBack != null ){
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
            }
        }
    }

    //---------------

    public void setItems(List<EmployeeBonusDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItem(EmployeeBonusDetails employeeBonusDetails){
        itemsList.add(employeeBonusDetails);
        notifyDataSetChanged();
    }
    public void addItem(int position, EmployeeBonusDetails employeeBonusDetails){
        if ( position >= 0 && position < itemsList.size() ) {
            itemsList.add(position, employeeBonusDetails);
        }
        notifyDataSetChanged();
    }

    public void setItem(int position, EmployeeBonusDetails employeeBonusDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, employeeBonusDetails);
            notifyDataSetChanged();
        }
    }

    public EmployeeBonusDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<EmployeeBonusDetails> getAllItems(){
        return itemsList;
    }

}