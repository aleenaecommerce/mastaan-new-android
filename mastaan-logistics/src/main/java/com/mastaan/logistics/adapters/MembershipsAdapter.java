package com.mastaan.logistics.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.models.MembershipDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 09/01/19.
 */

public class MembershipsAdapter extends RecyclerView.Adapter<MembershipsAdapter.ViewHolder> {
    Context context;
    List<MembershipDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        View fader;
        View titleView;
        TextView title;
        TextView details;
        View actionDelete;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            fader= itemLayoutView.findViewById(R.id.fader);
            titleView = itemLayoutView.findViewById(R.id.titleView);
            title = itemLayoutView.findViewById(R.id.title);
            details = itemLayoutView.findViewById(R.id.details);
            actionDelete = itemLayoutView.findViewById(R.id.actionDelete);
        }
    }

    public MembershipsAdapter(Context context, List<MembershipDetails> membershipsList, CallBack callBack) {
        this.context = context;
        this.itemsList = membershipsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public MembershipsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_membership, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final MembershipDetails membershipDetails = itemsList.get(position);

        try{
            if ( membershipDetails != null ){

                try{
                    viewHolder.titleView.setBackgroundColor(Color.parseColor(membershipDetails.getBackgroundColor()));
                }catch (Exception e){   viewHolder.titleView.setBackgroundColor(Color.WHITE);   }

                try{
                    viewHolder.title.setTextColor(Color.parseColor(membershipDetails.getTextColor()));
                }catch (Exception e){   viewHolder.title.setTextColor(Color.BLACK); }

                viewHolder.fader.setVisibility(membershipDetails.getStatus()?View.GONE:View.VISIBLE);

                viewHolder.title.setText(membershipDetails.getTypeName());

                viewHolder.details.setText(CommonMethods.fromHtml(membershipDetails.getFormattedDetails()));

                if ( callBack != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callBack.onItemClick(position);
                        }
                    });

                    viewHolder.actionDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callBack.onDeleteClick(position);
                        }
                    });
                }
            }
        }catch (Exception e){}

    }

    //---------------

    public void setItems(List<MembershipDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<MembershipDetails> items){
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }
    public void addItem(MembershipDetails membershipDetails){
        itemsList.add(membershipDetails);
        notifyDataSetChanged();
    }
    public void addItem(int position, MembershipDetails membershipDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.add(position, membershipDetails);
        }else{
            itemsList.add(membershipDetails);
        }
        notifyDataSetChanged();
    }

    public void setItem(int position, MembershipDetails membershipDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, membershipDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public MembershipDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<MembershipDetails> getAllItems(){
        return itemsList;
    }

}