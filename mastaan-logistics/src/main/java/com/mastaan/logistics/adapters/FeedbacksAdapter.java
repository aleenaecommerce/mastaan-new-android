package com.mastaan.logistics.adapters;

import android.content.Context;

import android.text.ClipboardManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.AppliedDiscountDetails;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

public class FeedbacksAdapter extends RecyclerView.Adapter<FeedbacksAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    LocalStorageData localStorageData;
    List<FeedbackDetails> itemsList;
    boolean showClaimedFeedbacks;

    Callback callback;

    public interface Callback {
        void onItemClick(int position);

        void onCallCustomer(int position);
        void onMessageCustomer(int position);
        void onShowOrderDetails(int position);
        void onShowCustomerOrderHistory(int position);
        void onShowStockDetails(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        CardView container;

        TextView feedback_type;
        RatingBar rating_bar;
        TextView feedback_rating;
        View testimonial;

        TextView customer_name;
        TextView membership_type;
        TextView customer_mobile;
        TextView customer_address;
        TextView order_cost;

        TextView item_name;
        TextView item_quantity;
        TextView item_extras;
        TextView item_cost;
        View vendorDetailsView;
        TextView vendor_details;
        TextView stock_details;
        View specialInstructionsHolder;
        TextView special_instructions;

        View first_order;
        TextView pre_order_status;
        TextView cash_n_carry_status;
        //View discountDetailsView;
        //TextView order_discount;
        View orderCreatorDetailsView;
        TextView order_creator;
        View discountsView;
        TextView discount_details;

        TextView feedback_date;
        TextView customer_comments;
        View feedbackProcessorDetails;
        TextView feedback_processor_name;
        TextView feedback_processor_comments;

        TextView order_status;
        View orderStatusTimesHolder;
        TextView order_status_times;

        TextView delivery_time;

        FloatingActionButton callCustomer;
        FloatingActionButton messageCustomer;
        FloatingActionButton showOrderDetails;
        FloatingActionButton showOrderHistory;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            container = itemLayoutView.findViewById(R.id.container);

            feedback_type = itemLayoutView.findViewById(R.id.feedback_type);
            rating_bar = (RatingBar) itemLayoutView.findViewById(R.id.rating_bar);
            feedback_rating = itemLayoutView.findViewById(R.id.feedback_rating);
            testimonial = itemLayoutView.findViewById(R.id.testimonial);

            customer_name = itemLayoutView.findViewById(R.id.customer_name);
            membership_type = itemLayoutView.findViewById(R.id.membership_type);
            customer_mobile = itemLayoutView.findViewById(R.id.customer_mobile);
            customer_address = itemLayoutView.findViewById(R.id.customer_address);
            order_cost = itemLayoutView.findViewById(R.id.order_cost);

            item_name = itemLayoutView.findViewById(R.id.item_name);
            item_quantity = itemLayoutView.findViewById(R.id.item_quantity);
            item_cost = itemLayoutView.findViewById(R.id.item_cost);
            item_extras = itemLayoutView.findViewById(R.id.item_preferences);
            vendorDetailsView = itemLayoutView.findViewById(R.id.vendorDetailsView);
            vendor_details = itemLayoutView.findViewById(R.id.vendor_details);
            stock_details = itemLayoutView.findViewById(R.id.stock_details);
            vendor_details = itemLayoutView.findViewById(R.id.vendor_details);
            specialInstructionsHolder = itemLayoutView.findViewById(R.id.specialInstructionsHolder);
            special_instructions = itemLayoutView.findViewById(R.id.special_instructions);

            first_order = itemView.findViewById(R.id.first_order);
            pre_order_status = itemView.findViewById(R.id.pre_order_status);
            cash_n_carry_status = itemView.findViewById(R.id.cash_n_carry_status);

            //discountDetailsView = itemView.findViewById(R.id.discountDetailsView);
            //order_discount = itemView.findViewById(R.id.order_discount);
            orderCreatorDetailsView = itemView.findViewById(R.id.orderCreatorDetailsView);
            order_creator = itemView.findViewById(R.id.order_creator);
            discountsView = itemView.findViewById(R.id.discountsView);
            discount_details = itemView.findViewById(R.id.discount_details);

            feedback_date = itemLayoutView.findViewById(R.id.feedback_date);
            customer_comments = itemLayoutView.findViewById(R.id.customer_comments);
            feedbackProcessorDetails = itemLayoutView.findViewById(R.id.feedbackProcessorDetails);
            feedback_processor_name = itemLayoutView.findViewById(R.id.feedback_processor_name);
            feedback_processor_comments = itemLayoutView.findViewById(R.id.feedback_processor_comments);

            callCustomer = itemLayoutView.findViewById(R.id.callCustomer);
            messageCustomer = itemLayoutView.findViewById(R.id.messageCustomer);
            showOrderDetails = itemLayoutView.findViewById(R.id.showOrderDetails);
            showOrderHistory = itemView.findViewById(R.id.showOrderHistory);

            order_status = itemLayoutView.findViewById(R.id.order_status);
            orderStatusTimesHolder = itemLayoutView.findViewById(R.id.orderStatusTimesHolder);
            order_status_times = itemLayoutView.findViewById(R.id.order_status_times);

            delivery_time = itemLayoutView.findViewById(R.id.delivery_time);
        }
    }

    public FeedbacksAdapter(MastaanToolbarActivity activity, List<FeedbackDetails> itemsList, boolean showClaimedFeedbacks, Callback callback) {
        this.activity = activity;
        this.context = activity;
        this.localStorageData = new LocalStorageData(context);
        this.itemsList = itemsList;
        this.showClaimedFeedbacks = showClaimedFeedbacks;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public FeedbacksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_feedback, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {



        try{
            final FeedbackDetails feedbackDetails = getItem(position);
            if ( feedbackDetails != null && feedbackDetails.getOrderItemDetails() != null ){

                final OrderItemDetails orderItemDetails = feedbackDetails.getOrderItemDetails();
                final OrderDetails2 orderDetails = orderItemDetails.getOrderDetails();

                if ( feedbackDetails.getCustomerRating() <= 1 ){
                    viewHolder.feedback_type.setText("L");
                }else if ( feedbackDetails.getCustomerRating() <= 3 ){
                    viewHolder.feedback_type.setText("A");
                }else if ( feedbackDetails.getCustomerRating() <= 5 ){
                    viewHolder.feedback_type.setText("G");
                }

                viewHolder.testimonial.setVisibility(feedbackDetails.isTestimonial()?View.VISIBLE:View.GONE);

                viewHolder.feedback_date.setText(DateMethods.getDateInFormat(feedbackDetails.getCreatedDate(), DateConstants.MMM_DD_YYYY));
                viewHolder.rating_bar.setRating(feedbackDetails.getCustomerRating());
                viewHolder.feedback_rating.setText(feedbackDetails.getCustomerRating()+"");
                if ( feedbackDetails.getCustomerComments().length() > 0 ) {
                    viewHolder.customer_comments.setVisibility(View.VISIBLE);
                    viewHolder.customer_comments.setText(feedbackDetails.getCustomerComments());
                }else{
                    viewHolder.customer_comments.setVisibility(View.GONE);
                }

                if ( feedbackDetails.getProssesorID() != null && feedbackDetails.getProssesorID().length() > 0 ){
                    viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.processing_color));
                    viewHolder.feedbackProcessorDetails.setVisibility(View.VISIBLE);
                    viewHolder.feedback_processor_comments.setText(feedbackDetails.getProcessorComments());
                    if ( activity.localStorageData.getUserID().equals(feedbackDetails.getProssesorID()) ){
                        viewHolder.feedback_processor_name.setText("YOU");
                    }else {
                        viewHolder.feedback_processor_name.setText(feedbackDetails.getProcessorDetails().getAvailableName());
                    }
                }else{
                    viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.default_color));
                    viewHolder.feedbackProcessorDetails.setVisibility(View.GONE);
                }
                if ( feedbackDetails.isCompleted() ){
                    viewHolder.container.setCardBackgroundColor(context.getResources().getColor(R.color.delivered_color));
                }

                viewHolder.customer_name.setText(orderDetails.getCustomerName());

                String membership = orderDetails.getBuyerDetails().getMembershipType();
                if ( membership.equalsIgnoreCase(Constants.BRONZE) == false ){
                    viewHolder.membership_type.setVisibility(View.VISIBLE);
                    viewHolder.membership_type.setText(membership);
                }else{  viewHolder.membership_type.setVisibility(View.GONE);    }

                viewHolder.first_order.setVisibility(orderItemDetails.getOrderDetails().isFirstOrder()?View.VISIBLE:View.GONE);

                viewHolder.customer_mobile.setText(CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile()));
                if ( orderDetails.getCustomerAlternateMobile().length() > 0
                        && orderDetails.getCustomerMobile().equalsIgnoreCase(orderDetails.getCustomerAlternateMobile()) == false ){
                    viewHolder.customer_mobile.setText(CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerMobile())
                            +" / "+CommonMethods.removeCountryCodeFromNumber(orderDetails.getCustomerAlternateMobile()));
                }
                viewHolder.messageCustomer.setVisibility(showMessageCustomer?View.VISIBLE:View.GONE);

                if ( showClaimedFeedbacks == false ){
//                viewHolder.showOrderHistory.setVisibility(View.GONE);
                    viewHolder.callCustomer.setVisibility(View.GONE);
                    viewHolder.messageCustomer.setVisibility(View.GONE);
                }

                viewHolder.customer_address.setText(CommonMethods.getStringFromStringArray(new String[]{orderItemDetails.getOrderDetails().getDeliveryAddress()}));//orderItemDetails.getOrderDetails().getDeliveryAddressPremise() + ",\n" + orderItemDetails.getOrderDetails().getAddressLine2());
                viewHolder.order_cost.setText(SpecialCharacters.RS + " "+CommonMethods.getIndianFormatNumber(orderItemDetails.getTotalOrderAmount()) + " (" + orderItemDetails.getPaymentTypeString() + ")");

                /*if ( orderItemDetails.getTotalOrderDiscount() > 0 ){
                    viewHolder.discountDetailsView.setVisibility(View.VISIBLE);
                    viewHolder.order_discount.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderItemDetails.getTotalOrderDiscount())+(orderDetails.getCouponCode()!=null?" ("+orderDetails.getCouponCode().toUpperCase()+")":""));
                }else{
                    viewHolder.discountDetailsView.setVisibility(View.GONE);
                }*/

                if ( showClaimedFeedbacks ){
                    List<AppliedDiscountDetails> appliedDiscounts = orderItemDetails.getOrderDetails().getAppliedDiscounts();
                    if ( appliedDiscounts != null && appliedDiscounts.size() > 0 ){
                        String discountsDetailsString = "";
                        for (int i=0;i<appliedDiscounts.size();i++){
                            AppliedDiscountDetails appliedDiscountDetails = appliedDiscounts.get(i);
                            if ( appliedDiscountDetails != null ){
                                if ( discountsDetailsString.length() > 0 ){ discountsDetailsString += "<br>"; }
                                if ( appliedDiscountDetails.isActive() ) {
                                    discountsDetailsString += "<b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(appliedDiscountDetails.getAmount()) + "</b> for " + appliedDiscountDetails.getDiscountReasonWithComment().toUpperCase() + "</b> by <b>" + appliedDiscountDetails.getDiscountCreatorName().toUpperCase() + "</b>";
                                }else{
                                    discountsDetailsString += "CANCELLED "+"<strike><b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(appliedDiscountDetails.getAmount()) + "</b> for " + appliedDiscountDetails.getDiscountReasonWithComment().toUpperCase() + "</b> by <b>" + appliedDiscountDetails.getDiscountCreatorName().toUpperCase() + "</b></strike>";
                                }
                            }
                        }
                        viewHolder.discount_details.setText(Html.fromHtml(discountsDetailsString));
                        viewHolder.discountsView.setVisibility(View.VISIBLE);
                    }else{
                        viewHolder.discountsView.setVisibility(View.GONE);
                    }
                }else{
                    viewHolder.discountsView.setVisibility(View.GONE);
                }

                if ( orderItemDetails.getOrderDetails().getOrderCreatorName() != null ){
                    viewHolder.orderCreatorDetailsView.setVisibility(View.VISIBLE);
                    viewHolder.order_creator.setText(orderItemDetails.getOrderDetails().getOrderCreatorName());
                }else{
                    viewHolder.orderCreatorDetailsView.setVisibility(View.GONE);
                }


                viewHolder.item_name.setText(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize());
                viewHolder.item_quantity.setText(orderItemDetails.getFormattedQuantity());
                viewHolder.item_cost.setText("("+SpecialCharacters.RS + " "+CommonMethods.getIndianFormatNumber(orderItemDetails.getAmountOfItem())+")");

                viewHolder.item_extras.setText(orderItemDetails.getFormattedExtras());
                if ( orderItemDetails.getVendorDetails() != null ){
                    viewHolder.vendorDetailsView.setVisibility(View.VISIBLE);

                    String vendorDetailsString = orderItemDetails.getVendorDetails().getName().toUpperCase();
                    vendorDetailsString += " ("+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderItemDetails.getVendorPaidAmount());
                    vendorDetailsString += ", "+orderItemDetails.getVendorPaymentType().toUpperCase()+")";
                    if ( orderItemDetails.getVendorComments() != null && orderItemDetails.getVendorComments().trim().length() > 0 ){
                        vendorDetailsString += "\n"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getVendorComments());
                    }
                    viewHolder.vendor_details.setText(vendorDetailsString);
                }else{
                    viewHolder.vendorDetailsView.setVisibility(View.GONE);
                }

                if ( orderItemDetails.getStock() != null ){
                    viewHolder.stock_details.setText(CommonMethods.fromHtml("<b><u>"+orderItemDetails.getStock()+"</u></b>"));
                }else if ( orderItemDetails.getProcessedStock() != null ){
                    viewHolder.stock_details.setText(CommonMethods.fromHtml("<b><u>"+orderItemDetails.getProcessedStock()+"</u></b>"));
                }else{  viewHolder.stock_details.setText("");   }

                if ( orderItemDetails.getOrderDetails().getSpecialInstructions() != null && orderItemDetails.getOrderDetails().getSpecialInstructions().length() > 0 ){
                    viewHolder.specialInstructionsHolder.setVisibility(View.VISIBLE);
                    viewHolder.special_instructions.setText(orderItemDetails.getOrderDetails().getSpecialInstructions());
                }else{
                    viewHolder.specialInstructionsHolder.setVisibility(View.GONE);
                }

                if ( DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getDeliveryDate())), DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getCreatedDate()))) > 0 ){
                    viewHolder.pre_order_status.setVisibility(View.VISIBLE);
                    viewHolder.pre_order_status.setText("PreOrder: "+DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderItemDetails.getOrderDetails().getCreatedDate()), DateConstants.MMM_DD));
                }else{
                    viewHolder.pre_order_status.setVisibility(View.GONE);
                }

                if ( orderItemDetails.isCashNCarryItem() ){
                    viewHolder.cash_n_carry_status.setVisibility(View.VISIBLE);
                }else{
                    viewHolder.cash_n_carry_status.setVisibility(View.GONE);
                }

                viewHolder.delivery_time.setText(DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate()), DateConstants.MMM_DD) + ", " + orderItemDetails.getDeliveryTimeSlot());

                //----------

                viewHolder.order_status.setText(orderItemDetails.getFormattedStatusString(localStorageData.getUserID()));

                //-----------

                if ( showClaimedFeedbacks ){
                    if ( orderItemDetails.getProcessingClaimDate().length() > 0 || orderItemDetails.getProcessingCompletedDate().length() > 0 || orderItemDetails.getDeliveryBoyAssignedDate().length() > 0 || orderItemDetails.getDeliveryClaimedDate().length() > 0 || orderItemDetails.getDeliveredDate().length() > 0 ){
                        viewHolder.orderStatusTimesHolder.setVisibility(View.VISIBLE);
                        viewHolder.order_status_times.setText(orderItemDetails.getStatusTimesString());
                    }else{
                        viewHolder.orderStatusTimesHolder.setVisibility(View.GONE);
                    }
                }else{
                    viewHolder.orderStatusTimesHolder.setVisibility(View.GONE);
                }

                //-----------------

                if ( callback != null ){
                    viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onItemClick(position);
                        }
                    });

                    viewHolder.showOrderDetails.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callback.onShowOrderDetails(position);
                        }
                    });

                    viewHolder.showOrderHistory.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callback.onShowCustomerOrderHistory(position);
                        }
                    });

                    viewHolder.stock_details.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.onShowStockDetails(position);
                        }
                    });

                    if ( showClaimedFeedbacks ){
                        viewHolder.callCustomer.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onCallCustomer(position);
                            }
                        });
                        viewHolder.callCustomer.setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                if ( orderItemDetails != null ){
                                    ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                    clipboardManager.setText(CommonMethods.removeCountryCodeFromNumber(orderItemDetails.getOrderDetails().getCustomerMobile()));
                                    Toast.makeText(context, orderItemDetails.getOrderDetails().getCustomerName() + "'s primary mobile number ( " + orderItemDetails.getOrderDetails().getCustomerMobile() + " ) is copied", Toast.LENGTH_SHORT).show();
                                }

                                return false;
                            }
                        });

                        viewHolder.messageCustomer.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callback.onMessageCustomer(position);
                            }
                        });
                    }
                }
            }
        }catch (Exception e){}
    }

    boolean showMessageCustomer;
    public void showMessageCustomerOption(){
        this.showMessageCustomer = true;
    }

    //-------------

    public void addFeedbackComment(int position, String comments){
        if ( itemsList.size() > position && itemsList.get(position) != null ){
            itemsList.get(position).setProcessorComments(comments);
            notifyDataSetChanged();
        }
    }

    public void setTestimonial(int position, boolean testimonial){
        if ( itemsList.size() > position && itemsList.get(position) != null ){
            itemsList.get(position).setTestimonial(testimonial);
            notifyDataSetChanged();
        }
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<FeedbackDetails> items){
        itemsList.clear();
        addItems(items);
    }

    public void addItems(List<FeedbackDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                if ( items.get(i) != null ) {
                    itemsList.add(items.get(i));
                }
            }
        }
        notifyDataSetChanged();
    }

    public void addItem(FeedbackDetails item){
        if ( itemsList == null ){   itemsList = new ArrayList<>();  }
        itemsList.add(item);
        notifyDataSetChanged();
    }

    public void setItem(int position, FeedbackDetails feedbackDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, feedbackDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public FeedbackDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id)){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<FeedbackDetails> getAllItems(){
        return itemsList;
    }

}