package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.WarehouseDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 13/04/19.
 */

public class WarehousesAdapter extends RecyclerView.Adapter<WarehousesAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<WarehouseDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        TextView name;
        TextView details;
        View fader;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            name = itemLayoutView.findViewById(R.id.name);
            details = itemLayoutView.findViewById(R.id.details);
            fader = itemLayoutView.findViewById(R.id.fader);
        }
    }

    public WarehousesAdapter(MastaanToolbarActivity activity, List<WarehouseDetails> hubsList, CallBack callBack) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = hubsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public WarehousesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_warehouse, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final WarehouseDetails warehouseDetails = itemsList.get(position);

        if ( warehouseDetails != null ){
            viewHolder.name.setText(warehouseDetails.getName());
            viewHolder.details.setText(warehouseDetails.getLocationString()+"\n"+warehouseDetails.getFullAddress());
            viewHolder.fader.setVisibility(warehouseDetails.getStatus()?View.GONE:View.VISIBLE);

            if ( callBack != null ) {
                viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
            }
        }
    }

    //---------------

    public void setItems(List<WarehouseDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<WarehouseDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void addItem(WarehouseDetails warehouseDetails){
        itemsList.add(warehouseDetails);
        notifyDataSetChanged();
    }

    public void setItem(int position, WarehouseDetails warehouseDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, warehouseDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public WarehouseDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<WarehouseDetails> getAllItems(){
        return itemsList;
    }

}