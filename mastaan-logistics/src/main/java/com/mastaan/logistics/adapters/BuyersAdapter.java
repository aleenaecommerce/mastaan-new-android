package com.mastaan.logistics.adapters;

import android.content.Context;
import android.text.ClipboardManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vCircularImageView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.BuyerDetails;

import java.util.List;

public class BuyersAdapter extends RecyclerView.Adapter<BuyersAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<BuyerDetails> itemsList;
    CallBack callBack;

    boolean showRequestOnlinePayment;

    public interface CallBack {
        void onItemClick(int position);
        void onReloadClick(int position);
        void onRequestOnlinePayment(int position);
    }

    public BuyersAdapter showRequestOnlinePayment() {
        this.showRequestOnlinePayment = true;
        return this;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        View fader;
        vCircularImageView image;
        TextView name;
        TextView membership_type;
        TextView details;
        View reloadDetails;
        View requestOnlinePayment;
        View not_interested_indicator;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            fader= itemLayoutView.findViewById(R.id.fader);
            image = itemLayoutView.findViewById(R.id.image);
            name = itemLayoutView.findViewById(R.id.name);
            membership_type = itemLayoutView.findViewById(R.id.membership_type);
            details = itemLayoutView.findViewById(R.id.details);
            reloadDetails = itemLayoutView.findViewById(R.id.reloadDetails);
            requestOnlinePayment = itemLayoutView.findViewById(R.id.requestOnlinePayment);
            not_interested_indicator = itemLayoutView.findViewById(R.id.not_interested_indicator);
        }
    }

    public BuyersAdapter(MastaanToolbarActivity activity, List<BuyerDetails> buyersList, CallBack callBack) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = buyersList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public BuyersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_buyer, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final BuyerDetails buyerDetails = itemsList.get(position);    // Getting Food Items Details using Position...

        viewHolder.name.setText(buyerDetails.getAvailableName());

        String membership = buyerDetails.getMembershipType();
        if ( membership.equalsIgnoreCase(Constants.BRONZE) == false ){
            viewHolder.membership_type.setVisibility(View.VISIBLE);
            viewHolder.membership_type.setText(membership);
        }else{  viewHolder.membership_type.setVisibility(View.GONE);    }

        String detailsString = "";
        if ( buyerDetails.getMobileNumber() != null && buyerDetails.getMobileNumber().trim().length() > 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + CommonMethods.removeCountryCodeFromNumber(buyerDetails.getMobileNumber());
        }
        if ( buyerDetails.getEmail() != null && buyerDetails.getEmail().trim().length() > 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + buyerDetails.getEmail();
        }
        if ( buyerDetails.getInstallSource() != null && buyerDetails.getInstallSource().trim().length() > 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + "Install Source: <b>"+buyerDetails.getInstallSource()+"</b>";
        }
        if ( buyerDetails.getOutstandingAmount() > 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + "Outstanding: <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(buyerDetails.getOutstandingAmount())+"</b> (Collect)";
        }else if ( buyerDetails.getOutstandingAmount() < 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + "Outstanding: <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(buyerDetails.getOutstandingAmount()*-1)+"</b> (Return)";
        }

        if ( buyerDetails.getLastOrderDate() != null && buyerDetails.getLastOrderDate().length() > 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + "Last Order: <b>"+CommonMethods.capitalizeFirstLetter(DateMethods.getReadableDateFromUTC(buyerDetails.getLastOrderDate(), DateConstants.MMM_DD_YYYY))+"</b>";
        }

        if ( buyerDetails.getLastFollowupDate() != null && buyerDetails.getLastFollowupDate().length() > 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + "Last Followup: <b>"+CommonMethods.capitalizeFirstLetter(DateMethods.getReadableDateFromUTC(buyerDetails.getLastFollowupDate(), DateConstants.MMM_DD_YYYY))+"</b>"+(buyerDetails.getLastFollowupComments()!=null?" (<b>"+buyerDetails.getLastFollowupComments()+"</b>)":"");
        }

        if ( buyerDetails.isEnabled() ){
            viewHolder.fader.setVisibility(View.GONE);
            if ( buyerDetails.getPromotionalAlertsPreference() == false ){
                detailsString += (detailsString.length()>0?"<br>":"") + "<b>Promotional alerts disabled</b>";
            }
        }else{
            viewHolder.fader.setVisibility(View.VISIBLE);
            detailsString += (detailsString.length()>0?"<br>":"") + "Disable Reason: <b>"+buyerDetails.getDisabledReason().replaceAll("Your account is disabled. Reason - ", "").replaceAll("Please contact our customer support for further details. Thank you.", "")+"</b>";
        }

        if ( buyerDetails.getFollowupEmployee() != null ){
            detailsString += (detailsString.length()>0?"<br>":"") + "Followup claimed: <b>"+buyerDetails.getFollowupEmployee().getAvailableName()+"</b>";
        }

        if ( buyerDetails.getComments() != null && buyerDetails.getComments().trim().length() > 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + "Comments: <b>"+buyerDetails.getComments()+"</b>";
        }

        viewHolder.details.setText(Html.fromHtml(detailsString));

        if ( showRequestOnlinePayment ){
            viewHolder.reloadDetails.setVisibility(View.VISIBLE);
            if ( buyerDetails.getOutstandingAmount() > 0 ){
                viewHolder.requestOnlinePayment.setVisibility(View.VISIBLE);
            }else{
                viewHolder.requestOnlinePayment.setVisibility(View.GONE);
            }
        }else{
            viewHolder.reloadDetails.setVisibility(View.GONE);
            viewHolder.requestOnlinePayment.setVisibility(View.GONE);
        }

        viewHolder.not_interested_indicator.setVisibility(buyerDetails.isNotInterestedAnymore()?View.VISIBLE:View.GONE);

        //--------

        if ( callBack != null ){
            viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //if ( activity.localStorageData.isUserCheckedIn() ) {
                        callBack.onItemClick(position);
                    //}else{
                    //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                    //}
                }
            });
            viewHolder.itemSelector.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboardManager.setText(CommonMethods.removeCountryCodeFromNumber(buyerDetails.getMobileNumber()));
                    Toast.makeText(context, buyerDetails.getAvailableName()+"'s mobile number ( " + buyerDetails.getMobileNumber() + " ) is copied", Toast.LENGTH_SHORT).show();
                    return false;
                }
            });
            viewHolder.reloadDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBack.onReloadClick(position);
                }
            });
            viewHolder.requestOnlinePayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //if ( activity.localStorageData.isUserCheckedIn() ) {
                        callBack.onRequestOnlinePayment(position);
                    //}else{
                    //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                    //}
                }
            });

        }
    }

    public void enableBuyer(int position){
        itemsList.get(position).setEnabled(true);
        notifyDataSetChanged();
    }
    public void disableBuyer(int position, String disableReason){
        itemsList.get(position).setEnabled(false);
        itemsList.get(position).setDisabledReason(disableReason);
        notifyDataSetChanged();
    }

    //---------------

    public void setItems(List<BuyerDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<BuyerDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(BuyerDetails buyerDetails){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(buyerDetails.getID()) ){
                itemsList.set(i, buyerDetails);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void setItem(int position, BuyerDetails buyerDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, buyerDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public BuyerDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<BuyerDetails> getAllItems(){
        return itemsList;
    }

}