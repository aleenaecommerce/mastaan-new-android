package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.AppliedDiscountDetails;

import java.util.List;

public class AppliedDiscountsAdapter extends RecyclerView.Adapter<AppliedDiscountsAdapter.ViewHolder> {

    Context context;
    String userID;
    List<AppliedDiscountDetails> itemsList;
    CallBack callBack;

    public interface CallBack {
        void onUpdateClick(int position, AppliedDiscountDetails appliedDiscountDetails);
        void onDisableClick(int position, AppliedDiscountDetails appliedDiscountDetails);
        void onEnableClick(int position, AppliedDiscountDetails appliedDiscountDetails);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View disabledIndicator;
        TextView discount_reason;
        TextView discount_amount;
        TextView discount_info;
        View update;
        View disable;
        View enable;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            disabledIndicator =  itemLayoutView.findViewById(R.id.disabledIndicator);
            discount_reason = (TextView) itemLayoutView.findViewById(R.id.discount_reason);
            discount_info = (TextView) itemLayoutView.findViewById(R.id.discount_info);
            discount_amount = (TextView) itemLayoutView.findViewById(R.id.discount_amount);
            update = itemLayoutView.findViewById(R.id.update);
            disable = itemLayoutView.findViewById(R.id.disable);
            enable = itemLayoutView.findViewById(R.id.enable);
        }
    }

    public AppliedDiscountsAdapter(Context context, List<AppliedDiscountDetails> appliedDiscounts, CallBack callBack) {
        this.context = context;
        this.userID = new LocalStorageData(context).getUserID();
        this.itemsList = appliedDiscounts;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public AppliedDiscountsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_applied_discount, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final AppliedDiscountDetails appliedDiscountDetails = itemsList.get(position);    // Getting Food Items Details using Position...

        if ( appliedDiscountDetails != null ){

            viewHolder.discount_reason.setText(appliedDiscountDetails.getDiscountReasonWithComment());
            viewHolder.discount_info.setText("By: "+appliedDiscountDetails.getDiscountCreatorName().toUpperCase());
            viewHolder.discount_amount.setText(SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(appliedDiscountDetails.getAmount()));

            if ( appliedDiscountDetails.getDiscountCreatorID().equals(userID) ){
                if ( appliedDiscountDetails.isActive() ) {
                    viewHolder.disabledIndicator.setVisibility(View.GONE);
                    viewHolder.update.setVisibility(View.VISIBLE);
                    viewHolder.disable.setVisibility(View.VISIBLE);
                    viewHolder.enable.setVisibility(View.GONE);
                }else {
                    viewHolder.disabledIndicator.setVisibility(View.VISIBLE);
                    viewHolder.update.setVisibility(View.GONE);
                    viewHolder.enable.setVisibility(View.VISIBLE);
                    viewHolder.disable.setVisibility(View.GONE);
                }
            }else{
                if ( appliedDiscountDetails.isActive() ){
                    viewHolder.disabledIndicator.setVisibility(View.GONE);
                }else{
                    viewHolder.disabledIndicator.setVisibility(View.VISIBLE);
                }
                viewHolder.update.setVisibility(View.GONE);
                viewHolder.disable.setVisibility(View.GONE);
                viewHolder.enable.setVisibility(View.GONE);
            }


            if ( callBack != null ){
                viewHolder.update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callBack.onUpdateClick(position, itemsList.get(position));
                    }
                });
                viewHolder.disable.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callBack.onDisableClick(position, itemsList.get(position));
                    }
                });
                viewHolder.enable.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callBack.onEnableClick(position, itemsList.get(position));
                    }
                });
            }

        }
    }

    //---------------

    public void setItems(List<AppliedDiscountDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<AppliedDiscountDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(AppliedDiscountDetails appliedDiscountDetails){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(appliedDiscountDetails.getID()) ){
                itemsList.set(i, appliedDiscountDetails);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void setItem(int position, AppliedDiscountDetails appliedDiscountDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, appliedDiscountDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public AppliedDiscountDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<AppliedDiscountDetails> getAllItems(){
        return itemsList;
    }

}