package com.mastaan.logistics.adapters;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aleena.common.fragments.NotYetFragment;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.fragments.FeedbacksFragment;

public class TabsFragmentAdapter extends FragmentPagerAdapter {

    Context context;
    String[] itemsList;

    public TabsFragmentAdapter(Context context, FragmentManager fragmentManager, String[] itemsList) {
        super(fragmentManager);
        this.context = context;
        this.itemsList =  itemsList;
    }

    @Override
    public Fragment getItem(int position) {

        if ( itemsList[position].equalsIgnoreCase(Constants.LOW_RATED_FEEDBACKS) ){
            FeedbacksFragment fragment = new FeedbacksFragment();
            Bundle bundle = new Bundle();
            bundle.putString("feedbacksType", Constants.LOW_RATED_FEEDBACKS);
            fragment.setArguments(bundle);
            return fragment;
        }
        else if ( itemsList[position].equalsIgnoreCase(Constants.AVERAGE_RATED_FEEDBACKS) ){
            FeedbacksFragment fragment = new FeedbacksFragment();
            Bundle bundle = new Bundle();
            bundle.putString("feedbacksType", Constants.AVERAGE_RATED_FEEDBACKS);
            fragment.setArguments(bundle);
            return fragment;
        }
        else if ( itemsList[position].equalsIgnoreCase(Constants.HIGH_RATED_FEEDBACKS) ){
            FeedbacksFragment fragment = new FeedbacksFragment();
            Bundle bundle = new Bundle();
            bundle.putString("feedbacksType", Constants.HIGH_RATED_FEEDBACKS);
            fragment.setArguments(bundle);
            return fragment;
        }
        else if ( itemsList[position].equalsIgnoreCase(Constants.PROCESSING_FEEDBACKS) ){
            FeedbacksFragment fragment = new FeedbacksFragment();
            Bundle bundle = new Bundle();
            bundle.putString("feedbacksType", Constants.PROCESSING_FEEDBACKS);
            fragment.setArguments(bundle);
            return fragment;
        }
        else if ( itemsList[position].equalsIgnoreCase(Constants.PROCESSED_FEEDBACKS) ){
            FeedbacksFragment fragment = new FeedbacksFragment();
            Bundle bundle = new Bundle();
            bundle.putString("feedbacksType", Constants.PROCESSED_FEEDBACKS);
            fragment.setArguments(bundle);
            return fragment;
        }

        return new NotYetFragment();
    }

    @Override
    public int getCount() {
        return itemsList.length;
    }

}
