package com.mastaan.logistics.adapters;

import android.content.Context;
import android.text.ClipboardManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vCircularImageView;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.models.UserDetails;

import java.util.List;

public class DeliveryBoysAdapter extends RecyclerView.Adapter<DeliveryBoysAdapter.ViewHolder> {

    MastaanToolbarActivity activity;
    Context context;
    List<UserDetails> itemsList;
    CallBack callBack;

    boolean showClearJob;
    public DeliveryBoysAdapter showClearJob() {
        this.showClearJob = true;
        return this;
    }

    public interface CallBack {
        void onItemClick(int position);
        void onTrackDeliveryBoy(int position);
        void onCallDeliveryBoy(int position);
        void onClearJob(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View itemSelector;
        vCircularImageView image;
        TextView name;
        TextView mobile;
        TextView details;
        View trackDeliveryBoy;
        View callDeliveryBoy;
        View clearJob;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            image = itemLayoutView.findViewById(R.id.image);
            name = itemLayoutView.findViewById(R.id.name);
            mobile = itemLayoutView.findViewById(R.id.mobile);
            details = itemLayoutView.findViewById(R.id.details);
            trackDeliveryBoy = itemLayoutView.findViewById(R.id.trackDeliveryBoy);
            callDeliveryBoy = itemLayoutView.findViewById(R.id.callDeliveryBoy);
            clearJob = itemLayoutView.findViewById(R.id.clearJob);
        }
    }

    public DeliveryBoysAdapter(MastaanToolbarActivity activity, List<UserDetails> deliveryBoysList, CallBack callBack) {
        this.activity = activity;
        this.context = activity;
        this.itemsList = deliveryBoysList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public DeliveryBoysAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View foodItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_delivery_boy, null);
        return (new ViewHolder(foodItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final UserDetails deliveryBoyDetails = itemsList.get(position);    // Getting Food Items Details using Position...

        viewHolder.name.setText(new CommonMethods().capitalizeStringWords(deliveryBoyDetails.getName()));
        /*Picasso.with(context)
                .load(deliveryBoyDetails.getImage())
                .placeholder(R.drawable.ic_chef)
                .error(R.drawable.ic_chef)
                .fit()
                .centerCrop()
                .tag(context)
                .into(viewHolder.chef_image);*/

        viewHolder.mobile.setText(CommonMethods.removeCountryCodeFromNumber(deliveryBoyDetails.getMobileNumber()));

        viewHolder.clearJob.setVisibility(showClearJob&&deliveryBoyDetails.getJob()!=null?View.VISIBLE:View.GONE);

        String detailsString = "";
        if ( deliveryBoyDetails.getJob() != null ){
            detailsString = deliveryBoyDetails.getJob().getFormattedJobDetailsString();
        }else if (deliveryBoyDetails.getLastJobTime() != null
                    && DateMethods.compareDates(DateMethods.getOnlyDate(deliveryBoyDetails.getLastJobTime()), DateMethods.getOnlyDate(activity.localStorageData.getServerTime())) == 0 ) {
            detailsString += "Idle time: <b>"+ DateMethods.getTimeFromMilliSeconds(DateMethods.getDifferenceBetweenDatesInMilliSeconds(activity.localStorageData.getServerTime(), deliveryBoyDetails.getLastJobTime()))+"</b>";
        }

        if ( detailsString.length() > 0 ){
            viewHolder.details.setText(Html.fromHtml(detailsString));
            viewHolder.details.setVisibility(View.VISIBLE);
        }else{
            viewHolder.details.setVisibility(View.GONE);
        }

        if ( callBack != null ){
            viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //if ( activity.localStorageData.isUserCheckedIn() ) {
                        callBack.onItemClick(position);
                    //}else{
                    //    activity.showNotificationDialog("Alert", Constants.CHECKIN_REQUIRED_ALERT_MESSAGE);
                    //}
                }
            });
            viewHolder.itemSelector.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    clipboardManager.setText(CommonMethods.removeCountryCodeFromNumber(deliveryBoyDetails.getMobileNumber()));
                    Toast.makeText(context, deliveryBoyDetails.getName()+"'s mobile number ( " + deliveryBoyDetails.getMobileNumber() + " ) is copied", Toast.LENGTH_SHORT).show();
                    return false;
                }
            });
            viewHolder.trackDeliveryBoy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onTrackDeliveryBoy(position);
                }
            });
            viewHolder.callDeliveryBoy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onCallDeliveryBoy(position);
                }
            });
            viewHolder.clearJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onClearJob(position);
                }
            });
        }
    }

    //---------------

    public void setItems(List<UserDetails> items){

        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void addItems(List<UserDetails> items){

        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void setItem(UserDetails deliveryBoyDetails){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(deliveryBoyDetails.getID()) ){
                itemsList.set(i, deliveryBoyDetails);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void setItem(int position, UserDetails deliveryBoyDetails){
        if ( position >= 0 && position < itemsList.size() ){
            itemsList.set(position, deliveryBoyDetails);
            notifyDataSetChanged();
        }
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public UserDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public void deleteItem(String id){
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).getID().equals(id) ){
                itemsList.remove(i);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<UserDetails> getAllItems(){
        return itemsList;
    }

}