package com.mastaan.logistics.adapters;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.aleena.common.utils.vFragmentStatePagerAdapter;
import com.mastaan.logistics.fragments.OrderItemsFragment;
import com.mastaan.logistics.models.GroupedOrdersItemsList;

import java.util.List;

/**
 * Created by aleena on 29/1/16.
 */
public class OrderItemsPagerAdapter extends vFragmentStatePagerAdapter{//vFragmentStatePagerAdapter {
    List<GroupedOrdersItemsList> itemsList;

    public OrderItemsPagerAdapter(FragmentManager fm, List<GroupedOrdersItemsList> item_list) {
        super(fm);
        this.itemsList = item_list;
    }

    @Override
    public Fragment getItem(int position) {
        OrderItemsFragment fragment = new OrderItemsFragment();
        Bundle bundle = new Bundle();
        //bundle.putString("orderItemsListString", new Gson().toJson(item_list.get(position)));
        bundle.putString("categoryName", itemsList.get(position).getCategoryName());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return itemsList.get(position).getItems().get(0).getMeatItemDetails().getParentCategory();
    }

    public void setItems(List<GroupedOrdersItemsList> item_list) {
        this.itemsList.clear();
        if ( item_list != null ) {
            this.itemsList = item_list;
        }
        notifyDataSetChanged();
    }
}
