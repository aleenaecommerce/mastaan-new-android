package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.R;
import com.mastaan.logistics.models.CustomPlace;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 06-01-2016.
 */

public class PlaceChooserAdapter extends ArrayAdapter {
    Context context;
    List<? extends CustomPlace> itemList;
    ItemClickListener mItemClickListener;

    public PlaceChooserAdapter(Context context, List<? extends CustomPlace> itemList, ItemClickListener mItemClickListener) {
        super(context, R.layout.view_place_item, itemList);
        this.context = context;
        this.itemList = itemList;
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_place_item, parent, false);
        }
        TextView name = (TextView) convertView.findViewById(R.id.name);
//        View selector = convertView.findViewById(R.id.item_selector);
        String displayName = itemList.get(position).getName();
        if ( itemList.get(position).getJobsDoneToday() > 0 ){
            displayName += "\n( Jobs: "+ CommonMethods.getInDecimalFormat(itemList.get(position).getJobsDoneToday());
        }
        if ( itemList.get(position).getDistanceTravelledToday() > 0 ){
            if ( itemList.get(position).getJobsDoneToday() == 0 ){ displayName += "\n( ";}
            else{   displayName += ", ";    }
            if ( itemList.get(position).getDistanceTravelledToday() == 1000 ) {
                displayName += "Dis: 1km";
            }else{
                displayName += "Dis: " + CommonMethods.getInDecimalFormat(itemList.get(position).getDistanceTravelledToday() / 1000) + "kms";
            }
        }
        if ( itemList.get(position).getJobsDoneToday() > 0 || itemList.get(position).getDistanceTravelledToday() > 0 ){
            displayName += " )";
        }
        name.setText(displayName);
        /*if (position % 2 == 0)
            name.setBackgroundColor(Color.parseColor("#99999999"));
        else
            name.setBackgroundColor(Color.parseColor("#ffffffff"));*/
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null)
                    mItemClickListener.onSelect(position, itemList.get(position));
            }
        });
        if (position == itemList.size() - 1)
            convertView.findViewById(R.id.divider).setVisibility(View.GONE);
        else
            convertView.findViewById(R.id.divider).setVisibility(View.VISIBLE);
        return convertView;
    }

    public interface ItemClickListener {
        void onSelect(int position, CustomPlace place);
    }
}
