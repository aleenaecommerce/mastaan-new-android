package com.mastaan.logistics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.recyclerview.widget.RecyclerView;

import com.mastaan.logistics.R;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 15/04/18.
 */

public class WarehouseMeatItemsCheckBoxAdapter extends RecyclerView.Adapter<WarehouseMeatItemsCheckBoxAdapter.ViewHolder> {

    Context context;
    List<WarehouseMeatItemDetails> itemsList;

    List<String> selectedItems = new ArrayList<>();

    Callback callback;

    public interface Callback {
        void onItemClick(int position, boolean isEnabled);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        View itemSelector;
        CheckBox checkbox;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            itemSelector =  itemLayoutView.findViewById(R.id.itemSelector);
            checkbox = (CheckBox) itemLayoutView.findViewById(R.id.checkbox);
        }
    }

    public WarehouseMeatItemsCheckBoxAdapter(Context context, List<WarehouseMeatItemDetails> itemsList, Callback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public WarehouseMeatItemsCheckBoxAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_check_item, null);
        return (new ViewHolder(itemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        final WarehouseMeatItemDetails warehouseMeatItemDetails = itemsList.get(position);
        if ( warehouseMeatItemDetails != null ){
            viewHolder.checkbox.setText(warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize());
            viewHolder.checkbox.setChecked(selectedItems.contains(warehouseMeatItemDetails.getID()));

            viewHolder.itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( viewHolder.checkbox.isChecked() == false) {
                        viewHolder.checkbox.setChecked(true);
                        selectedItems.add(warehouseMeatItemDetails.getID());
                    } else {
                        viewHolder.checkbox.setChecked(false);
                        selectedItems.remove(warehouseMeatItemDetails.getID());
                    }
                    //--------
                    if ( callback != null ) {
                        callback.onItemClick(position, false);
                    }
                }
            });

        }
    }

    public void setSelectedItems(List<String> selectedItems) {
        if ( selectedItems == null ){   selectedItems = new ArrayList<>();  }
        this.selectedItems = selectedItems;
    }

    public List<String> getSelectedItems() {
        if ( selectedItems == null ){   selectedItems = new ArrayList<>();  }
        return selectedItems;
    }

    //---------------

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setItems(List<WarehouseMeatItemDetails> items){
        itemsList.clear();;
        if ( items != null && items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public WarehouseMeatItemDetails getItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            return itemsList.get(position);
        }
        return null;
    }

    public void deleteItem(int position){
        if ( position >= 0 && position < itemsList.size()){
            itemsList.remove(position);
            notifyDataSetChanged();
        }
    }

    public List<WarehouseMeatItemDetails> getAllItems(){
        return itemsList;
    }

}