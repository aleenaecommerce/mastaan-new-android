package com.mastaan.logistics.localdata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.google.gson.Gson;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemDetailsCallback;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.localdata.models.DBOrderItemDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 10/7/15.
 */

public class OrderItemsDatabase extends SQLiteOpenHelper {

    final String TODAY = "TODAY";
    final String FUTURE = "FUTURE";

    String serverDate;
    String myID;

    Context context;
    LocalStorageData localStorageData;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;

    private static final String DB_NAME = "OrderItemsDatabase";
    private static final int DB_VERSION = 1;

    private static final String ORDER_ITEMS_TABLE = "orderItemsTable";

    private static final String ORDER_ITEM_ID = "order_item_id";
    private static final String ORDER_ITEM_STATUS = "order_item_status";
    private static final String ORDER_ITEM_DATE = "order_item_date";
    private static final String ORDER_ITEM_JSON = "order_item_json";

    public interface OrderItemsDBCallback{
        void onComplete(List<DBOrderItemDetails> itemsList);
    }

    private final String CREATE_TABLE="CREATE TABLE IF NOT EXISTS "+ ORDER_ITEMS_TABLE +" ( "+
            ORDER_ITEM_ID +" TEXT ,"+
            ORDER_ITEM_STATUS +" TEXT ,"+
            ORDER_ITEM_DATE +" TEXT ,"+
            ORDER_ITEM_JSON +" TEXT ,"+
            "PRIMARY KEY ("+ ORDER_ITEM_ID +")"+
            ")";


    public OrderItemsDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        this.localStorageData = new LocalStorageData(context);
        this.serverDate = DateMethods.getOnlyDate(localStorageData.getServerTime());
        this.myID = localStorageData.getUserID();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ ORDER_ITEMS_TABLE);
        onCreate(sqLiteDatabase);
    }

    public void openDatabase(){
        sqLiteDatabase = this.getWritableDatabase();
    }

    public void closeDatabase(){
        sqLiteDatabase.close();
    }

    public void addOrUpdateOrderItems(final List<OrderItemDetails> itemsList, final StatusCallback callback){
        if (itemsList != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    for (int j = 0; j < itemsList.size(); j++) {
                        addOrUpdateOrderItem(itemsList.get(j));
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ){
                        callback.onComplete(true, 200, "Success");
                    }
                }
            }.execute();

        }
    }


    public void addOrUpdateOrderItem(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                boolean status = addOrUpdateOrderItem(orderItemDetails);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
                return null;
            }
        }.execute();
    }
    public boolean addOrUpdateOrderItem(final OrderItemDetails orderItemDetails){

        String deliveryDate = TODAY;
        if (DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate())), serverDate) > 0) {
            deliveryDate = FUTURE;
        }

        if (orderItemDetails != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ORDER_ITEM_ID, orderItemDetails.getID());
            contentValues.put(ORDER_ITEM_STATUS, orderItemDetails.getStatusString());
            contentValues.put(ORDER_ITEM_DATE, deliveryDate);//orderItemDetails.getOrderDetails().getDeliveryDateAndTime());
            contentValues.put(ORDER_ITEM_JSON, new Gson().toJson(orderItemDetails));

            try {
                sqLiteDatabase.insertWithOnConflict(ORDER_ITEMS_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
                //sqLiteDatabase.insertOrThrow(ORDER_ITEMS_TABLE, null, contentValues);
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {ADD_OR_UPDATE_ORDER_ITEM} , Success for orderID = " + orderItemDetails.getID());
                return true;

            }catch (Exception e){
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {ADD_OR_UPDATE_ORDER_ITEM} , Error for orderID = "+orderItemDetails.getID());
            }
        }
        return false;
    }

    public void updateOrderItem(final OrderItemDetails orderItemDetails, final StatusCallback callback){

        new AsyncTask<Void, Void, Void>() {
            boolean status;

            @Override
            protected Void doInBackground(Void... voids) {

                ContentValues contentValues = new ContentValues();
                contentValues.put(ORDER_ITEM_ID, orderItemDetails.getID());
                contentValues.put(ORDER_ITEM_STATUS, orderItemDetails.getStatusString());
                contentValues.put(ORDER_ITEM_DATE, orderItemDetails.getDeliveryDate());
                contentValues.put(ORDER_ITEM_JSON, new Gson().toJson(orderItemDetails));

                try {
                    sqLiteDatabase.update(ORDER_ITEMS_TABLE, contentValues, ORDER_ITEM_ID + " = " + "\"" + orderItemDetails.getID() + "\"", null);
                    status = true;
                    Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {UPDATE_ORDER_ITEM} , Success for orderID = "+orderItemDetails.getID());
                }catch (Exception e){
                    Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {UPDATE_ORDER_ITEM} , Error for orderID = "+orderItemDetails.getID());
                    status = false;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, 200, "");
                }
            }
        }.execute();

    }

    public void deleteOrderItem(final String orderItemID, final StatusCallback callback){

        new AsyncTask<Void, Void, Void>() {

            boolean status;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    sqLiteDatabase.execSQL("delete from " + ORDER_ITEMS_TABLE + " where " + ORDER_ITEM_ID + " = " + "\"" + orderItemID + "\"");
                    status = true;
                    Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {DELETE_ORDER_ITEM} , Success for orderItemID = "+orderItemID);
                }catch (Exception e){
                    Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {DELETE_ORDER_ITEM} , Error for orderItemID = "+orderItemID);
                    status = false;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, 200, "");
                }
            }
        }.execute();

    }

    public void getOrderItem(final String orderItemID, final OrderItemDetailsCallback callback){

        new AsyncTask<Void, Void, Void>() {

            OrderItemDetails orderItemDetails = null;

            @Override
            protected Void doInBackground(Void... voids) {
                orderItemDetails = getOrderItem(orderItemID);
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ) {
                    callback.onComplete(true, 200, "Success", orderItemDetails);
                }
            }
        }.execute();
    }

    public OrderItemDetails getOrderItem(final String orderItemID){

        OrderItemDetails orderItemDetails = null;
        try{
            cursor = sqLiteDatabase.rawQuery("select " + ORDER_ITEM_JSON +  " from " + ORDER_ITEMS_TABLE + " where " + ORDER_ITEM_ID + " = " + "\"" + orderItemID + "\"", null);

            if(cursor!=null){
                if( cursor.moveToFirst() ) {
                    orderItemDetails = new Gson().fromJson(cursor.getString(0), OrderItemDetails.class);
                }
                cursor.close();
            }
        }catch (Exception e){e.printStackTrace();}

        return orderItemDetails;
    }

    public int getOrderItemsCount(){
        return ((int) DatabaseUtils.longForQuery(sqLiteDatabase, "SELECT COUNT(*) FROM "+ ORDER_ITEMS_TABLE, null));
    }

    public void getAllOrderItems(final OrderItemsDBCallback callback){

        new AsyncTask<Void, Void, Void>() {

            List<DBOrderItemDetails> orderItems = new ArrayList<>();

            @Override
            protected Void doInBackground(Void... voids) {

                cursor = sqLiteDatabase.query(ORDER_ITEMS_TABLE, new String[]{ORDER_ITEM_ID, ORDER_ITEM_STATUS, ORDER_ITEM_DATE, ORDER_ITEM_JSON}, null, null, null, null, null);
                //String query = "Select * from "+DB_TABLE+ "Where" +column1 + "=" + t +"and"+ column2 + "=" +tt

                if ( cursor != null ){
                    if( cursor.moveToFirst() ){
                        do{
                            DBOrderItemDetails DBOrderItemDetails = new DBOrderItemDetails(cursor.getString(0), cursor.getString(1), cursor.getString(2),cursor.getString(3));
                            orderItems.add(DBOrderItemDetails);
                        }while( cursor.moveToNext() );
                    }
                    cursor.close();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_ALL_ORDER_ITEMS} , Found = " + orderItems.size());
                if ( callback != null ){
                    callback.onComplete(orderItems);
                }
            }
        }.execute();

    }

    public void deleteAllOrderItems(){
        sqLiteDatabase.execSQL("delete from "+ ORDER_ITEMS_TABLE);
        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {DELTE_ALL_ORDER_ITEMS} , Success");
    }

    public void deleteOrderItems(final List<OrderItemDetails> orderItemsList, final StatusCallback callback){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                for (int i=0;i<orderItemsList.size();i++){
                    deleteOrderItem(orderItemsList.get(i).getID(), null);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }
        }.execute();
    }


    // CUSTOM METHODS //

    public void getTodaysOrders(final OrderItemsDBCallback callback){

        getAllOrderItems(new OrderItemsDBCallback() {
            @Override
            public void onComplete(final List<DBOrderItemDetails> itemsList) {
                new AsyncTask<Void, Void, Void>() {
                    List<DBOrderItemDetails> todaysOrders = new ArrayList<DBOrderItemDetails>();

                    @Override
                    protected Void doInBackground(Void... params) {
                        if (itemsList != null && itemsList.size() > 0) {
                            for (int i = 0; i < itemsList.size(); i++) {
                                //String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(itemsList.get(i).getDeliveryDateAndTime()));
                                if (itemsList.get(i).getDeliveryDate().equalsIgnoreCase(TODAY)) {//DateMethods.compareDates(deliveryDate, serverDate) == 0) {
                                    todaysOrders.add(itemsList.get(i));
                                }
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_TODAYS_ORDERS(DB_MODEL)} , Found = " + todaysOrders.size());
                        if (callback != null) {
                            callback.onComplete(todaysOrders);
                        }
                    }
                }.execute();
            }
        });
    }

    public void getTodaysOrders(final OrderItemsListCallback callback){
        getTodaysOrders(new OrderItemsDBCallback() {
            @Override
            public void onComplete(final List<DBOrderItemDetails> itemsList) {

                new AsyncTask<Void, Void, Void>() {
                    List<OrderItemDetails> todaysOrders = new ArrayList<OrderItemDetails>();

                    @Override
                    protected Void doInBackground(Void... params) {
                        for (int i = 0; i < itemsList.size(); i++) {
                            todaysOrders.add(itemsList.get(i).getOrderItemDetails());
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_TODAYS_ORDERS} , Found = " + todaysOrders.size());
                        if (callback != null) {
                            callback.onComplete(true, 200, "Success", todaysOrders);
                        }
                    }
                }.execute();
            }
        });
    }

    public void getFutureOrders(final OrderItemsListCallback callback){
        getAllOrderItems(new OrderItemsDBCallback() {
            @Override
            public void onComplete(final List<DBOrderItemDetails> itemsList) {
                new AsyncTask<Void, Void, Void>() {
                    List<OrderItemDetails> futureOrders = new ArrayList<OrderItemDetails>();

                    @Override
                    protected Void doInBackground(Void... params) {
                        if (itemsList != null && itemsList.size() > 0) {
                            for (int i = 0; i < itemsList.size(); i++) {
                                //String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(itemsList.get(i).getDeliveryDateAndTime()));
                                if (itemsList.get(i).getDeliveryDate().equalsIgnoreCase(FUTURE)) {//DateMethods.compareDates(deliveryDate, serverDate) > 0) {
                                    futureOrders.add(itemsList.get(i).getOrderItemDetails());
                                }
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_FUTURE_ORDERS} , Found = " + futureOrders.size());
                        if (callback != null) {
                            callback.onComplete(true, 200, "Success", futureOrders);
                        }
                    }
                }.execute();
            }
        });
    }

    //=========

    public void getNewOrders(final OrderItemsListCallback callback){

        getAllOrderItems(new OrderItemsDBCallback() {
            @Override
            public void onComplete(final List<DBOrderItemDetails> itemsList) {
                new AsyncTask<Void, Void, Void>() {
                    List<OrderItemDetails> newOrders = new ArrayList<OrderItemDetails>();

                    @Override
                    protected Void doInBackground(Void... params) {
                        if (itemsList != null && itemsList.size() > 0) {
                            for (int i = 0; i < itemsList.size(); i++) {
                                if (itemsList.get(i).getDeliveryDate().equalsIgnoreCase(TODAY) && itemsList.get(i).getStatus().equalsIgnoreCase("ORDERED")) {
                                    newOrders.add(itemsList.get(i).getOrderItemDetails());
                                }
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_NEW_ORDERS} , Found = " + newOrders.size());
                        if (callback != null) {
                            callback.onComplete(true, 200, "Success", newOrders);
                        }
                    }
                }.execute();
            }
        });
    }

    public void getPendingProcessingItems(final OrderItemsListCallback callback){

        getAllOrderItems(new OrderItemsDBCallback() {
            @Override
            public void onComplete(final List<DBOrderItemDetails> itemsList) {
                new AsyncTask<Void, Void, Void>() {
                    List<OrderItemDetails> pendingProcessingItems = new ArrayList<OrderItemDetails>();

                    @Override
                    protected Void doInBackground(Void... params) {
                        if (itemsList != null && itemsList.size() > 0) {
                            for (int i = 0; i < itemsList.size(); i++) {
                                if (itemsList.get(i).getDeliveryDate().equalsIgnoreCase(TODAY) && itemsList.get(i).getStatus().equalsIgnoreCase("ACKNOWLEDGED") || itemsList.get(i).getStatus().equalsIgnoreCase("PROCESSING")) {
                                    pendingProcessingItems.add(itemsList.get(i).getOrderItemDetails());
                                }
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_PENDING_PROCESSING} , Found = " + pendingProcessingItems.size());
                        if (callback != null) {
                            callback.onComplete(true, 200, "Success", pendingProcessingItems);
                        }
                    }
                }.execute();
            }
        });
    }

    public void getPendingDeliveries(final OrderItemsListCallback callback){
        getAllOrderItems(new OrderItemsDBCallback() {
            @Override
            public void onComplete(final List<DBOrderItemDetails> itemsList) {
                new AsyncTask<Void, Void, Void>() {
                    List<OrderItemDetails> pendingDeliveries = new ArrayList<OrderItemDetails>();

                    @Override
                    protected Void doInBackground(Void... params) {
                        if (itemsList != null && itemsList.size() > 0) {
                            for (int i = 0; i < itemsList.size(); i++) {
                                if (itemsList.get(i).getDeliveryDate().equalsIgnoreCase(TODAY) && itemsList.get(i).getStatus().equalsIgnoreCase("PICKUP PENDING")) {
                                    pendingDeliveries.add(itemsList.get(i).getOrderItemDetails());
                                }
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_PENDING_DELIVERIES} , Found = " + pendingDeliveries.size());
                        if (callback != null) {
                            callback.onComplete(true, 200, "Success", pendingDeliveries);
                        }
                    }
                }.execute();
            }
        });
    }

    public void getMyDeliveries(final OrderItemsListCallback callback){
        getAllOrderItems(new OrderItemsDBCallback() {
            @Override
            public void onComplete(final List<DBOrderItemDetails> itemsList) {
                new AsyncTask<Void, Void, Void>() {
                    List<OrderItemDetails> myDeliveries = new ArrayList<OrderItemDetails>();

                    @Override
                    protected Void doInBackground(Void... params) {
                        if (itemsList != null && itemsList.size() > 0) {
                            for (int i = 0; i < itemsList.size(); i++) {
                                if (itemsList.get(i).getDeliveryDate().equalsIgnoreCase(TODAY) && itemsList.get(i).getStatus().equalsIgnoreCase("DELIVERING")) {
                                    if (itemsList.get(i).getOrderItemDetails().getDeliveryBoyID().equals(myID)) {
                                        myDeliveries.add(itemsList.get(i).getOrderItemDetails());
                                    }
                                }
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_TMY_DELIVERIES} , Found = " + myDeliveries.size());
                        if (callback != null) {
                            callback.onComplete(true, 200, "Success", myDeliveries);
                        }
                    }
                }.execute();
            }
        });
    }

    //--------


}