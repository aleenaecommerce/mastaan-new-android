package com.mastaan.logistics.localdata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.models.DBRequestObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 10/7/15.
 */

public class BackendRequestsDatabase extends SQLiteOpenHelper {

    Context context;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;

    private static final String DB_NAME = "BackendRequestDatabase";
    private static final int DB_VERSION = 1;

    private static final String BACKEND_REQUESTS_TABLE = "backendRequestsTable";

    private static final String TYPE = "type";
    private static final String RETRY_COUNT = "retry_count";
    private static final String ITEM_ID = "item_id";
    private static final String USER_ID = "user_id";
    private static final String REQUEST_JSON = "request_json";

    public interface DBRequestObjectCallback{
        void onComplete(DBRequestObject DBRequestObject);
    }

    public interface DBRequestObjectsListCallback{
        void onComplete(List<DBRequestObject> DBRequestObjectsList);
    }

    private final String CREATE_TABLE="CREATE TABLE IF NOT EXISTS "+ BACKEND_REQUESTS_TABLE +" ( "+
            TYPE+" TEXT ,"+
            ITEM_ID +" TEXT ,"+
            USER_ID +" TEXT ,"+
            RETRY_COUNT+" INTEGER ,"+
            REQUEST_JSON +" TEXT ,"+
            "PRIMARY KEY ("+ ITEM_ID +")"+
            ")";


    public BackendRequestsDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ BACKEND_REQUESTS_TABLE);
        onCreate(sqLiteDatabase);
    }

    public void openDatabase(){
        sqLiteDatabase = this.getWritableDatabase();
    }

    public void closeDatabase(){
        sqLiteDatabase.close();
    }

    public void addItem(final String type, final String itemID, final String userID, final String requestJSONString, final StatusCallback callback){


        new AsyncTask<Void, Void, Void>() {
            boolean status;

            @Override
            protected Void doInBackground(Void... voids) {

                ContentValues contentValues = new ContentValues();
                contentValues.put(TYPE, type);
                contentValues.put(ITEM_ID, itemID);
                contentValues.put(USER_ID, userID);
                contentValues.put(RETRY_COUNT, 0);
                contentValues.put(REQUEST_JSON, requestJSONString);

                try {
                    sqLiteDatabase.insertOrThrow(BACKEND_REQUESTS_TABLE, null, contentValues);
                    status = true;
                    Log.d(Constants.LOG_TAG, "BackendRequestsDatabase : addRequestItem , Success for itemID = "+itemID);
                }catch (Exception e){
                    Log.d(Constants.LOG_TAG, "BackendRequestsDatabase : addRequestItem , Error for itemID = "+itemID);
                    status = false;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, 200, "");
                }
            }
        }.execute();

    }

    public void increateRetryCount(final DBRequestObject DBRequestObject, final StatusCallback callback){

        new AsyncTask<Void, Void, Void>() {
            boolean status;

            @Override
            protected Void doInBackground(Void... voids) {

                ContentValues contentValues = new ContentValues();
                contentValues.put(TYPE, DBRequestObject.getType());
                contentValues.put(ITEM_ID, DBRequestObject.getItemID());
                contentValues.put(USER_ID, DBRequestObject.getUserID());
                contentValues.put(RETRY_COUNT, DBRequestObject.getRetryCount()+1);
                contentValues.put(REQUEST_JSON, DBRequestObject.getOrderFeedbackJSON());

                try {
                    sqLiteDatabase.update(BACKEND_REQUESTS_TABLE, contentValues, ITEM_ID + " = " + "\"" + DBRequestObject.getItemID() + "\"", null);
                    status = true;
                    Log.d(Constants.LOG_TAG, "BackendRequestsDatabase : IncreaseRetryCount , Success for itemID = "+ DBRequestObject.getItemID());
                }catch (Exception e){
                    Log.d(Constants.LOG_TAG, "BackendRequestsDatabase : IncreaseRetryCount , Error for itemID = "+ DBRequestObject.getItemID());
                    status = false;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, 200, "");
                }
            }
        }.execute();

    }

    public void deleteItem(final String itemID, final StatusCallback callback){

        new AsyncTask<Void, Void, Void>() {

            boolean status;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    sqLiteDatabase.execSQL("delete from " + BACKEND_REQUESTS_TABLE + " where " + ITEM_ID + " = " + "\"" + itemID + "\"");
                    status = true;
                    Log.d(Constants.LOG_TAG, "FeedbackDatabase : DeleteItem , Success for itemID = "+itemID);
                }catch (Exception e){
                    Log.d(Constants.LOG_TAG, "FeedbackDatabase : DeleteItem , Error for itemID = "+itemID);
                    status = false;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, 200, "");
                }
            }
        }.execute();

    }

    public void getItem(final String itemID, final DBRequestObjectCallback callback){

        new AsyncTask<Void, Void, Void>() {

            DBRequestObject DBRequestObject = null;

            @Override
            protected Void doInBackground(Void... voids) {
                cursor = sqLiteDatabase.rawQuery("select " + TYPE + " , " + ITEM_ID + " , " + USER_ID + " , " + RETRY_COUNT + " , " + REQUEST_JSON +  " from " + BACKEND_REQUESTS_TABLE + " where " + ITEM_ID + " = " + "\"" + itemID + "\"", null);

                if(cursor!=null){
                    if( cursor.moveToFirst() ) {
                        DBRequestObject = new DBRequestObject(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4));
                    }
                    cursor.close();
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ) {
                    callback.onComplete(DBRequestObject);
                }
            }
        }.execute();
    }

    public int getFeedbackItemsCount(){
        return ((int) DatabaseUtils.longForQuery(sqLiteDatabase, "SELECT COUNT(*) FROM "+ BACKEND_REQUESTS_TABLE, null));
    }

    public void getAllItems(final DBRequestObjectsListCallback callback){

        new AsyncTask<Void, Void, Void>() {

            List<DBRequestObject> requestObjectsList = new ArrayList<>();

            @Override
            protected Void doInBackground(Void... voids) {

                cursor = sqLiteDatabase.query(BACKEND_REQUESTS_TABLE, new String[]{TYPE, ITEM_ID, USER_ID, RETRY_COUNT, REQUEST_JSON}, null, null, null, null, null);

                if ( cursor != null ){
                    if( cursor.moveToFirst() ){
                        do{
                            DBRequestObject DBRequestObject = new DBRequestObject(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4));
                            requestObjectsList.add(DBRequestObject);
                        }while( cursor.moveToNext() );
                    }
                    cursor.close();
                }

                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(requestObjectsList);
                }
            }
        }.execute();

    }

    public void deleteAllItems(){
        sqLiteDatabase.execSQL("delete from "+ BACKEND_REQUESTS_TABLE);
        Log.d(Constants.LOG_TAG, "FeedbackDatabase : DeleteALL , Success");
    }
}