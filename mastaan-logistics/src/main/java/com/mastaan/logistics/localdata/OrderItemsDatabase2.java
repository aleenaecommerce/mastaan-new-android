package com.mastaan.logistics.localdata;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 29/9/16.
 */
public class OrderItemsDatabase2 {

    final String TODAY = "TODAY";
    final String FUTURE = "FUTURE";

    String serverDate;
    String myID;

    Context context;
    LocalStorageData localStorageData;

    List<DBOrderItemDetails> itemsList = new ArrayList<>();

    private class DBOrderItemDetails{
        String date;
        OrderItemDetails orderItemDetails;
        DBOrderItemDetails(String date, OrderItemDetails orderItemDetails){
            this.date = date;
            this.orderItemDetails = orderItemDetails;
        }

        public String getDate() {
            if ( date != null ){    return date;    }
            return "";
        }

        public OrderItemDetails getOrderItemDetails() {
            return orderItemDetails;
        }
    }


    public OrderItemsDatabase2(Context context) {
        this.context = context;
        this.localStorageData = new LocalStorageData(context);
        this.serverDate = DateMethods.getOnlyDate(localStorageData.getServerTime());
        this.myID = localStorageData.getUserID();
    }

    public void addOrUpdateOrderItems(final List<OrderItemDetails> itemsList, final StatusCallback callback){
        if (itemsList != null) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    for (int j = 0; j < itemsList.size(); j++) {
                        addOrUpdateOrderItem(itemsList.get(j));
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if ( callback != null ){
                        callback.onComplete(true, 200, "Success");
                    }
                }
            }.execute();

        }
    }

    public void addOrUpdateOrderItem(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                boolean status = addOrUpdateOrderItem(orderItemDetails);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }
        }.execute();
    }
    public boolean addOrUpdateOrderItem(final OrderItemDetails orderItemDetails){
        orderItemDetails.setShowInCheckBoxView(false);

        String deliveryDate = TODAY;
        if (DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate())), serverDate) > 0) {
            deliveryDate = FUTURE;
        }

        if (orderItemDetails != null) {
            int containIndex = -1;
            for (int i = 0; i< itemsList.size(); i++){
                if ( itemsList.get(i).getOrderItemDetails().getID().equals(orderItemDetails.getID())){
                    containIndex = i;
                    break;
                }
            }
            if ( containIndex != -1 ){
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {UPDATE_ORDER_ITEM} , Success for orderID = " + orderItemDetails.getID());
                itemsList.set(containIndex, new DBOrderItemDetails(deliveryDate, orderItemDetails));
            }else{
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {ADD_ORDER_ITEM} , Success for orderID = " + orderItemDetails.getID());
                itemsList.add(new DBOrderItemDetails(deliveryDate, orderItemDetails));
            }
        }
        return true;
    }

    public void deleteOrderItem(final String orderItemID, final StatusCallback callback){

        new AsyncTask<Void, Void, Void>() {

            boolean status;

            @Override
            protected Void doInBackground(Void... voids) {
                for (int i = 0; i< itemsList.size(); i++){
                    if ( itemsList.get(i).getOrderItemDetails().getID().equals(orderItemID)){
                        itemsList.remove(i);
                        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {DELETE_ORDER_ITEM} , Success for orderItemID = "+orderItemID);
                        break;
                    }
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, 200, "");
                }
            }
        }.execute();

    }

    public OrderItemDetails getOrderItem(final String orderItemID){

        OrderItemDetails orderItemDetails = null;
        try{
            for (int i = 0; i< itemsList.size(); i++){
                if ( itemsList.get(i).getOrderItemDetails().getID().equals(orderItemID)){
                    return itemsList.get(i).getOrderItemDetails();
                }
            }
        }catch (Exception e){e.printStackTrace();}

        return orderItemDetails;
    }

    public int getOrderItemsCount(){
        return itemsList.size();
    }


    public void deleteAllOrderItems(){
        itemsList.clear();
        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {DELTE_ALL_ORDER_ITEMS} , Success");
    }

    public void deleteOrderItems(final List<OrderItemDetails> orderItemsList, final StatusCallback callback){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                for (int i=0;i<orderItemsList.size();i++){
                    deleteOrderItem(orderItemsList.get(i).getID(), null);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }
        }.execute();
    }


    // CUSTOM METHODS

    public void getTodaysOrders(final OrderItemsListCallback callback){
        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_TODAYS_ORDERS} , Requesting");
        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> todaysOrders = new ArrayList<OrderItemDetails>();

            @Override
            protected Void doInBackground(Void... params) {
                if ( itemsList != null && itemsList.size() > 0 ) {
                    for (int i = 0; i < itemsList.size(); i++) {
                        if (itemsList.get(i).getDate().equalsIgnoreCase(TODAY)) {
                            todaysOrders.add(itemsList.get(i).getOrderItemDetails());
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_TODAYS_ORDERS} , Found = " + todaysOrders.size());
                if (callback != null) {
                    callback.onComplete(true, 200, "Success", todaysOrders);
                }
            }
        }.execute();
    }

    public void getFutureOrders(final OrderItemsListCallback callback){
        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_FUTURE_ORDERS} , Requesting");
        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> futureOrders = new ArrayList<OrderItemDetails>();

            @Override
            protected Void doInBackground(Void... params) {
                if (itemsList != null && itemsList.size() > 0) {
                    for (int i = 0; i < itemsList.size(); i++) {
                        //String deliveryDate = DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(itemsList.get(i).getDeliveryDate()));
                        if (itemsList.get(i).getDate().equalsIgnoreCase(FUTURE)) {//DateMethods.compareDates(deliveryDate, serverDate) > 0) {
                            futureOrders.add(itemsList.get(i).getOrderItemDetails());
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_FUTURE_ORDERS} , Found = " + futureOrders.size());
                if (callback != null) {
                    callback.onComplete(true, 200, "Success", futureOrders);
                }
            }
        }.execute();
    }

    //=========

    public void getNewOrders(final OrderItemsListCallback callback){

        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_NEW_ORDERS} , Requesting");
        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> newOrders = new ArrayList<OrderItemDetails>();

            @Override
            protected Void doInBackground(Void... params) {
                if (itemsList != null && itemsList.size() > 0) {
                    for (int i = 0; i < itemsList.size(); i++) {
                        if (itemsList.get(i).getDate().equalsIgnoreCase(TODAY) && itemsList.get(i).getOrderItemDetails().getStatusString().equalsIgnoreCase(Constants.ORDERED)) {
                            newOrders.add(itemsList.get(i).getOrderItemDetails());

                            if(newOrders.get(i).getOrderID().isEmpty()||(newOrders.get(i).getOrderID().equals(null)||(newOrders.get(i).getOrderID()==null))){
                                Log.e("new ordersid ",""+newOrders.get(i).getOrderID());
                            }else {
//                                    validnewOrders.add(newOrders.get(i).getOrderItemDetails());
                                Log.e("valid ordersids ",""+newOrders.get(i).getOrderID());
                            }

                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_NEW_ORDERS} , Found = " + newOrders.size());
                if (callback != null) {
                    callback.onComplete(true, 200, "Success", newOrders);
                }
            }
        }.execute();
    }

    public void getPendingProcessingItems(final OrderItemsListCallback callback){

        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_PENDING_PROCESSING} , Requesting");
        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> pendingProcessingItems = new ArrayList<OrderItemDetails>();

            @Override
            protected Void doInBackground(Void... params) {
                if (itemsList != null && itemsList.size() > 0) {
                    for (int i = 0; i < itemsList.size(); i++) {
                        if (itemsList.get(i).getDate().equalsIgnoreCase(TODAY) && (itemsList.get(i).getOrderItemDetails().getStatusString().equalsIgnoreCase(Constants.ACKNOWLEDGED) || itemsList.get(i).getOrderItemDetails().getStatusString().equalsIgnoreCase(Constants.PROCESSING) || itemsList.get(i).getOrderItemDetails().getQualityCheckStatus() == Constants.QC_FAILED) ) {
                            pendingProcessingItems.add(itemsList.get(i).getOrderItemDetails());
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_PENDING_PROCESSING} , Found = " + pendingProcessingItems.size());
                if (callback != null) {
                    callback.onComplete(true, 200, "Success", pendingProcessingItems);
                }
            }
        }.execute();
    }

    public void getPendingQCItems(final OrderItemsListCallback callback){

        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_PENDING_QC} , Requesting");
        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> pendingQCItems = new ArrayList<OrderItemDetails>();

            @Override
            protected Void doInBackground(Void... params) {
                if (itemsList != null && itemsList.size() > 0) {
                    for (int i = 0; i < itemsList.size(); i++) {
                        if (itemsList.get(i).getDate().equalsIgnoreCase(TODAY) && itemsList.get(i).getOrderItemDetails().getStatusString().equalsIgnoreCase(Constants.PROCESSED) && itemsList.get(i).getOrderItemDetails().getQualityCheckStatus() == Constants.QC_PENDING ) {
                            pendingQCItems.add(itemsList.get(i).getOrderItemDetails());
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_PENDING_QC} , Found = " + pendingQCItems.size());
                if (callback != null) {
                    callback.onComplete(true, 200, "Success", pendingQCItems);
                }
            }
        }.execute();
    }

    public void getPendingDeliveries(final OrderItemsListCallback callback){

        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_PENDING_DELIVERIES} , Requesting");
        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> pendingDeliveries = new ArrayList<OrderItemDetails>();

            @Override
            protected Void doInBackground(Void... params) {
                if (itemsList != null && itemsList.size() > 0) {
                    for (int i = 0; i < itemsList.size(); i++) {
                        if (itemsList.get(i).getDate().equalsIgnoreCase(TODAY) && itemsList.get(i).getOrderItemDetails().getStatusString().equalsIgnoreCase(Constants.PROCESSED) && itemsList.get(i).getOrderItemDetails().getQualityCheckStatus() == Constants.QC_DONE ) {
                            pendingDeliveries.add(itemsList.get(i).getOrderItemDetails());
                            Log.e("items pending",""+itemsList.get(i).orderItemDetails.getItemLocation());
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_PENDING_DELIVERIES} , Found = " + pendingDeliveries.size());
                if (callback != null) {
                    callback.onComplete(true, 200, "Success", pendingDeliveries);
                }
            }
        }.execute();
    }

    public void getMyDeliveries(final OrderItemsListCallback callback){

        Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_TMY_DELIVERIES} , Requesting");
        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> myDeliveries = new ArrayList<OrderItemDetails>();

            @Override
            protected Void doInBackground(Void... params) {
                if (itemsList != null && itemsList.size() > 0) {
                    for (int i = 0; i < itemsList.size(); i++) {
                        if (itemsList.get(i).getDate().equalsIgnoreCase(TODAY) && itemsList.get(i).getOrderItemDetails().getStatusString().equalsIgnoreCase(Constants.DELIVERING)) {
                            if (itemsList.get(i).getOrderItemDetails().getDeliveryBoyID().equals(myID)) {
                                myDeliveries.add(itemsList.get(i).getOrderItemDetails());
                            }
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d(Constants.LOG_TAG, "ORDER_ITEMS_DATABASE : {GET_TMY_DELIVERIES} , Found = " + myDeliveries.size());
                if (callback != null) {
                    callback.onComplete(true, 200, "Success", myDeliveries);
                }
            }
        }.execute();
    }

}
