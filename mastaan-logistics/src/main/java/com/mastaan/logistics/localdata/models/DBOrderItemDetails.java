package com.mastaan.logistics.localdata.models;

import com.google.gson.Gson;
import com.mastaan.logistics.models.OrderItemDetails;

/**
 * Created by venkatesh on 16/5/16.
 */
public class DBOrderItemDetails {
    String id;
    String status;
    String deliveryDate;
    String item_json;

    public DBOrderItemDetails(String id, String status, String deliveryDate, String item_json){
        this.id = id;
        this.status = status;
        this.deliveryDate = deliveryDate;
        this.item_json = item_json;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public OrderItemDetails getOrderItemDetails() {
        return new Gson().fromJson(item_json, OrderItemDetails.class);
    }
}
