package com.mastaan.logistics.localdata.models;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */

public class DBRequestObject {

    String type;
    String itemID;
    String userID;
    int retryCount;
    String request_object_json;

    public DBRequestObject(String type, String itemID, String userID, int retryCount, String request_object_json){
        this.type = type;
        this.itemID = itemID;
        this.userID = userID;
        this.retryCount = retryCount;
        this.request_object_json = request_object_json;
    }

    public String getItemID() {
        return itemID;
    }

    public String getType() {
        return type;
    }

    public String getUserID() {
        return userID;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public String getOrderFeedbackJSON() {
        return request_object_json;
    }

    public String getRequestObjectJSONString() {
        return request_object_json;
    }
}
