package com.mastaan.logistics.localdata;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.models.DiscountDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.ViolationDetails;
import com.mastaan.logistics.models.WarehouseDetails;
import com.mastaan.logistics.models.WhereToNextItem;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class LocalStorageData {

    public SharedPreferences prefs;
    public SharedPreferences.Editor editor;
    Context context;

    public LocalStorageData(Context context) {
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        editor = prefs.edit();
    }

    public void setSession(String accessToken, UserDetails userDetails){
        editor.putBoolean("sessionFlag", true);
        editor.putString("token", accessToken);
        setUser(userDetails);
        editor.commit();
    }

    public void clearSession(){
        editor.putBoolean("sessionFlag", false);
        editor.putString("token", "");
        editor.putString("user_details_json", "");
        editor.putString("user_id", "");
        editor.putString("user_name", "");
        editor.putString("create_job_request_id", "");
        editor.putString("where_to_next_request_id", "");
        editor.commit();
    }

    public boolean getSessionFlag(){
        return prefs.getBoolean("sessionFlag", false);
    }

    public String getAccessToken() {
        return prefs.getString("token", "");
    }

    public String getUserID() {
        return prefs.getString("user_id", "");
    }

    public String getUserName() {
        return prefs.getString("user_name", "");
    }

    public boolean isUserCheckedIn() {
        return prefs.getBoolean("user_checkin_status", false);
    }

    public void setUser(UserDetails userDetails) {
        if ( userDetails != null ) {
            editor.putString("user_details_json", new Gson().toJson(userDetails));
            editor.putString("user_id", userDetails.getID());
            editor.putString("user_name", userDetails.getName());
            editor.putBoolean("user_checkin_status", userDetails.isCheckedIn(getServerTime()));
        }else{
            editor.putString("user_details_json", "");
            editor.putString("user_id", "");
            editor.putString("user_name", "");
            editor.putBoolean("user_checkin_status", false);
        }
        editor.commit();
    }

    public UserDetails getUserDetails() {
        String user_details_json = prefs.getString("user_details_json", "");
        return new Gson().fromJson(user_details_json, UserDetails.class);
    }

    //-----

    public void setServerTime(String serverTime){
        editor.putString("server_time", serverTime);
        editor.commit();
    }

    public String getServerTime(){
        return prefs.getString("server_time", "");
    }

    public void setUpgradeURL(String upgradeURL){
        editor.putString("upgrade_url", upgradeURL);
        editor.commit();
    }

    public String getUpgradeURL(){
        return prefs.getString("upgrade_url", "");
    }

    public void storeWhereToNextPlaces(List<WhereToNextItem> whereToNextList) {
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < whereToNextList.size(); i++) {
            jsons_list.add(new Gson().toJson(whereToNextList.get(i)));
        }
        editor.putString("where_to_next_places", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }

    public List<WhereToNextItem> getWhereToNextPlaces() {
        List<WhereToNextItem> whereToNextList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("where_to_next_places", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                whereToNextList.add(new Gson().fromJson(jsonArray.get(i).toString(), WhereToNextItem.class));
            }
        } catch (Exception e) {}

        return whereToNextList;
    }

    public void storeFailureReasons(List<String> failureReasons){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < failureReasons.size(); i++) {
            jsons_list.add(new Gson().toJson(failureReasons.get(i)));
        }
        editor.putString("failure_reasons", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }

    public List<String> getFailureReasons(){
        List<String> failureReasons = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("failure_reasons", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                failureReasons.add(new Gson().fromJson(jsonArray.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return failureReasons;
    }

    public void storeQCFailureReasons(List<String> qcFailureReasons){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < qcFailureReasons.size(); i++) {
            jsons_list.add(new Gson().toJson(qcFailureReasons.get(i)));
        }
        editor.putString("qc_failure_reasons", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }

    public List<String> getQCFailureReasons(){
        List<String> qcFailureReasons = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("qc_failure_reasons", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                qcFailureReasons.add(new Gson().fromJson(jsonArray.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return qcFailureReasons;
    }

    public void storeDeliveryFailureReasons(List<String> deliveryFailureReasons){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < deliveryFailureReasons.size(); i++) {
            jsons_list.add(new Gson().toJson(deliveryFailureReasons.get(i)));
        }
        editor.putString("delivery_failure_reasons", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }

    public List<String> getDeliveryFailureReasons(){
        List<String> deliveryFailureReasons = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("delivery_failure_reasons", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                deliveryFailureReasons.add(new Gson().fromJson(jsonArray.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return deliveryFailureReasons;
    }

    public void storeDiscountReasons(List<DiscountDetails> discountReasons){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < discountReasons.size(); i++) {
            jsons_list.add(new Gson().toJson(discountReasons.get(i)));
        }
        editor.putString("discount_reasons", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }

    public List<DiscountDetails> getDiscountReasons(){
        List<DiscountDetails> discountReasons = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("discount_reasons", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                discountReasons.add(new Gson().fromJson(jsonArray.get(i).toString(), DiscountDetails.class));
            }
        } catch (Exception e) {}
        return discountReasons;
    }

    public void storeCustomerSupportReasons(List<String> customerSupportReasons){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < customerSupportReasons.size(); i++) {
            jsons_list.add(new Gson().toJson(customerSupportReasons.get(i)));
        }
        editor.putString("customer_support_reasons", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }
    public List<String> getCustomerSupportReasons(){
        List<String> customerSupportReasons = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("customer_support_reasons", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                customerSupportReasons.add(new Gson().fromJson(jsonArray.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return customerSupportReasons;
    }

    public void storeFollowupReasons(List<String> followupReasons){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < followupReasons.size(); i++) {
            jsons_list.add(new Gson().toJson(followupReasons.get(i)));
        }
        editor.putString("followup_reasons", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }
    public List<String> getFollowupReasons(){
        List<String> followupReasons = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("followup_reasons", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                followupReasons.add(new Gson().fromJson(jsonArray.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return followupReasons;
    }

    public void storeCartFollowupReasons(List<String> followupReasons){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < followupReasons.size(); i++) {
            jsons_list.add(new Gson().toJson(followupReasons.get(i)));
        }
        editor.putString("cart_followup_reasons", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }
    public List<String> getCartFollowupReasons(){
        List<String> cartFollowupReasons = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("cart_followup_reasons", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                cartFollowupReasons.add(new Gson().fromJson(jsonArray.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return cartFollowupReasons;
    }

    public void storeItemNotAvailableReasons(List<String> itemNotAvailableReasons){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < itemNotAvailableReasons.size(); i++) {
            jsons_list.add(new Gson().toJson(itemNotAvailableReasons.get(i)));
        }
        editor.putString("item_not_available_reasons", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }

    public List<String> getItemNotAvailableReasons(){
        List<String> itemNotAvailableReasons = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("item_not_available_reasons", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                itemNotAvailableReasons.add(new Gson().fromJson(jsonArray.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return itemNotAvailableReasons;
    }

    public void storeEmployeeViolationsList(List<ViolationDetails> employeeViolationsList){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < employeeViolationsList.size(); i++) {
            jsons_list.add(new Gson().toJson(employeeViolationsList.get(i)));
        }
        editor.putString("employee_violations", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }

    public List<ViolationDetails> getEmployeeViolationsList(){
        List<ViolationDetails> employeeViolations = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("employee_violations", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                employeeViolations.add(new Gson().fromJson(jsonArray.get(i).toString(), ViolationDetails.class));
            }
        } catch (Exception e) {}
        return employeeViolations;
    }

    public void storeInstallSources(List<String> installSources){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < installSources.size(); i++) {
            jsons_list.add(new Gson().toJson(installSources.get(i)));
        }
        editor.putString("install_sources", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }

    public List<String> getInstallSources(){
        List<String> installSources = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("install_sources", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                installSources.add(new Gson().fromJson(jsonArray.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return installSources;
    }

    public void storeCustomerMessages(List<String> customerMessages){
        List<String> jsons_list = new ArrayList<String>();
        for (int i = 0; i < customerMessages.size(); i++) {
            jsons_list.add(new Gson().toJson(customerMessages.get(i)));
        }
        editor.putString("customer_messages", CommonMethods.getJSONArryString(jsons_list));
        editor.commit();
    }

    public List<String> getCustomerMessages(){
        List<String> customerMessages = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(prefs.getString("customer_messages", "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                customerMessages.add(new Gson().fromJson(jsonArray.get(i).toString(), String.class));
            }
        } catch (Exception e) {}
        return customerMessages;
    }

    public void storeWarehouse(WarehouseDetails warehouseDetails){
        editor.putString("warehouse", warehouseDetails!=null?new Gson().toJson(warehouseDetails):"");
        editor.putString("warehouse_id", warehouseDetails!=null?warehouseDetails.getID():"");
        editor.putString("warehouse_lat", warehouseDetails!=null?warehouseDetails.getLocation().latitude+"":"");
        editor.putString("warehouse_lng", warehouseDetails!=null?warehouseDetails.getLocation().longitude+"":"");
        editor.commit();
    }

    public WarehouseDetails getWarehouse(){
        return new Gson().fromJson(prefs.getString("warehouse", ""), WarehouseDetails.class);
    }

    public String getWarehouseID(){
        return prefs.getString("warehouse_id", "");
    }

    public LatLng getWarehouseLocation(){
        try{
            return new LatLng(Double.parseDouble(prefs.getString("warehouse_lat", "0")), Double.parseDouble(prefs.getString("warehouse_lng", "0")));
        }catch (Exception e){}
        return new LatLng(0,0);
    }

    public void storeWarehousesList(List<WarehouseDetails> warehousesList) {
        if ( warehousesList != null && warehousesList.size() > 0 ){
            List<String> warehouses_jsons_list = new ArrayList<String>();
            for (int i = 0; i < warehousesList.size(); i++) {
                warehouses_jsons_list.add(new Gson().toJson(warehousesList.get(i)));
            }
            editor.putString("warehouses_list", CommonMethods.getJSONArryString(warehouses_jsons_list));

        }else{
            editor.putString("warehouses_list", "[]");
        }
        editor.commit();
    }

    public List<WarehouseDetails> getWarehousesList() {
        List<WarehouseDetails> warehousesList = new ArrayList<>();
        try {
            JSONArray warehouses_jsons_list = new JSONArray(prefs.getString("warehouses_list", "[]"));
            for (int i = 0; i < warehouses_jsons_list.length(); i++) {
                warehousesList.add(new Gson().fromJson(warehouses_jsons_list.get(i).toString(), WarehouseDetails.class));
            }
        } catch (Exception e) {}

        return warehousesList;
    }

    //---------- TOKENS DATA -------------//

    public void setFireBaseToken(String fireBaseToken){
        editor.putString("firebase_token", fireBaseToken);
        editor.putString("firebase_token_generated_time", DateMethods.getCurrentDateAndTime());
        editor.commit();
    }

    public String getFireBaseToken(){
        long firebaseTokenElapsedTime = DateMethods.getDifferenceToCurrentTime(prefs.getString("firebase_token_generated_time", ""));
        if ( firebaseTokenElapsedTime == -1 || firebaseTokenElapsedTime >= 86400000 ){
            return "";
        }else{
            return prefs.getString("firebase_token", "");
        }
    }

    //--------------

    public void setCreateJobRequestID(String createJobRequstID){
        editor.putString("create_job_request_id", createJobRequstID);
        editor.commit();
    }

    public String getCreateJobRequestID(){
        return  prefs.getString("create_job_request_id", null);
    }

    public void setWhereToNextRequestID(String whereToNextRequstID){
        editor.putString("where_to_next_request_id", whereToNextRequstID);
        editor.commit();
    }

    public String getWhereToNextRequestID(){
        return  prefs.getString("where_to_next_request_id", null);
    }

    //--------------- SETTINGS --------------//

    public void setBillPrinterIPAddress(String billPrinterIPAddress){
        editor.putString("bill_printer_ip_address", billPrinterIPAddress);
        editor.commit();
    }
    public String getBillPrinterIPAddress(){
        return  prefs.getString("bill_printer_ip_address", "192.168.0.12");
    }

    public void setLabelPrinterIPAddress(String labelPrinterIPAddress){
        editor.putString("label_printer_ip_address", labelPrinterIPAddress);
        editor.commit();
    }
    public String getLabelPrinterIPAddress(){
        return  prefs.getString("label_printer_ip_address", "192.168.0.38");
    }

    //--------------- UX IMPROVE DATA --------------//

    public void setHomePageLastLoadedTime(String homePageLastLoadedTime){
        editor.putString("home_page_last_loaded_time", homePageLastLoadedTime);
        editor.commit();
    }
    public String getHomePageLastLoadedTime(){
        return  prefs.getString("home_page_last_loaded_time", "NA");
    }


    //========

    public void setPendingFollowupsLastOrderDays(int pendingFollowupsLastOrderDays){
        if ( pendingFollowupsLastOrderDays > 0 ) {
            editor.putInt("pending_followups_last_order_days", pendingFollowupsLastOrderDays);
            editor.commit();
        }
    }
    public int getPendingFollowupsLastOrderDays(){
        return  prefs.getInt("pending_followups_last_order_days", 30);
    }

}
