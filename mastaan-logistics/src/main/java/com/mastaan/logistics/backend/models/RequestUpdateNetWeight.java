package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 20/04/18.
 */
public class RequestUpdateNetWeight {
    double weight;

    public RequestUpdateNetWeight(double weight){
        this.weight = weight;
    }
}
