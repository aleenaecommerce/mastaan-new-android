package com.mastaan.logistics.backend.models;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 5/6/17.
 */

public class RequestConfirmItemsPickup {
    List<String> pickedup;
    List<String> pending;

    public RequestConfirmItemsPickup(List<String> pickedupItems, List<String> unpickedupItems){
        this.pickedup = pickedupItems;
        this.pending = unpickedupItems;
    }

}
