package com.mastaan.logistics.backend.methods;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.ClipboardManager;
import android.text.Html;
import android.widget.Toast;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.activities.LaunchActivity;
import com.mastaan.logistics.activities.LoginActivity;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.localdata.LocalStorageData;

import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 15/05/16.
 */

public  class CheckError {

    Context context;
    MastaanToolbarActivity activity;

    boolean ignoreAlertMessage;
    boolean ignoreSessionValidity;
    boolean ignoreMaintenance;
    boolean ignoreUpgrade;

    public CheckError(Context context){
        this.context = context;
        try{    activity = (MastaanToolbarActivity) context;  }catch (Exception e){}
    }

    public CheckError ignoreAlertMessage() {
        this.ignoreAlertMessage = true;
        return this;
    }

    public CheckError ignoreMaintenance(){
        this.ignoreMaintenance = true;
        return this;
    }

    public CheckError ignoreUpgrade(){
        this.ignoreUpgrade = true;
        return this;
    }

    public CheckError ignoreSessionValidity(){
        this.ignoreSessionValidity = true;
        return this;
    }

    public boolean check(RetrofitError error){

        if ( error != null ) {
            try {
                String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                JSONObject jsonResponse = new JSONObject(response_json_string);

                String alertMessage = "";
                try{    alertMessage = jsonResponse.getString("msg");  }catch (Exception e){}
                boolean isInMintainance = false;
                try{    isInMintainance = jsonResponse.getBoolean("maintenance");  }catch (Exception e){}
                boolean isUpgradeAvailable = false;
                try{    isUpgradeAvailable = jsonResponse.getBoolean("upgrade");  }catch (Exception e){}
                //final String updateURL = jsonResponse.getString("url");

                if ( !ignoreAlertMessage && alertMessage != null && alertMessage.trim().length() > 0 ){
                    if ( activity != null ){
                        //activity.showNotificationDialog("Alert!", CommonMethods.capitalizeFirstLetter(alertMessage));
                        activity.showToastMessage(CommonMethods.capitalizeFirstLetter(alertMessage));
                    }
                    return false;
                }
                else if ( !ignoreSessionValidity && error.getResponse().getStatus() == 403 ){
                    if ( activity != null ){
                        Toast.makeText(activity, "Your session has expired, please login to continue.", Toast.LENGTH_LONG).show();
                        new LocalStorageData(activity).clearSession();

                        Intent loginAct = new Intent(context, LoginActivity.class);
                        ComponentName componentName = loginAct.getComponent();
                        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                        activity.startActivity(mainIntent);
                    }
                    return true;
                }
                else if ( !ignoreMaintenance && isInMintainance ) {
                    if ( activity != null ) {
                        Intent launchAct = new Intent(context, LaunchActivity.class);
                        ComponentName componentName = launchAct.getComponent();
                        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                        activity.startActivity(mainIntent);
                    }
                    return true;
                }
                else if ( !ignoreUpgrade && isUpgradeAvailable) {
                    if ( activity != null ) {
                        final String upgradeURL = new LocalStorageData(activity).getUpgradeURL();

                        ClipboardManager clipboardManager = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboardManager.setText(upgradeURL);
                        Toast.makeText(activity, "Download URL ( " + upgradeURL + " ) is copied", Toast.LENGTH_SHORT).show();

                        activity.showChoiceSelectionDialog(false, "Update Available!", Html.fromHtml("This version is no longer compatible. Please update to new version to continue.<br><br>URL = <b>" + upgradeURL + "</b>"), "EXIT", "UPDATE", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("UPDATE")) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(upgradeURL));
                                    activity.startActivity(browserIntent);
                                    activity.finishAffinity();
                                    System.exit(0);
                                } else {
                                    activity.finishAffinity();
                                    System.exit(0);
                                }
                            }
                        });
                    }
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
