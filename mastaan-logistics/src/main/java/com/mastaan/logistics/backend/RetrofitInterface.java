package com.mastaan.logistics.backend;

import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.models.RequestAddComment;
import com.mastaan.logistics.backend.models.RequestAddEmployeeViolation;
import com.mastaan.logistics.backend.models.RequestAddFeedbackResolutionComment;
import com.mastaan.logistics.backend.models.RequestAddFollowup;
import com.mastaan.logistics.backend.models.RequestAddOnlinePayment;
import com.mastaan.logistics.backend.models.RequestAddOrEditProcessedStock;
import com.mastaan.logistics.backend.models.RequestAddOrEditStock;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateDeliveryArea;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateDeliveryZone;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateHousingSociety;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateHub;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateWarehouse;
import com.mastaan.logistics.backend.models.RequestAddRefundMoneyCollection;
import com.mastaan.logistics.backend.models.RequestAmountCollected;
import com.mastaan.logistics.backend.models.RequestAssignDeliveries;
import com.mastaan.logistics.backend.models.RequestAssignDeliveryArea;
import com.mastaan.logistics.backend.models.RequestAssignHousingSociety;
import com.mastaan.logistics.backend.models.RequestAssignToCustomerService;
import com.mastaan.logistics.backend.models.RequestCancelDelivery;
import com.mastaan.logistics.backend.models.RequestCaptureRazorpayPayment;
import com.mastaan.logistics.backend.models.RequestChangeWarehouse;
import com.mastaan.logistics.backend.models.RequestCheckForCancelOrderItem;
import com.mastaan.logistics.backend.models.RequestCompleteDelivery;
import com.mastaan.logistics.backend.models.RequestConfirmItemsPickup;
import com.mastaan.logistics.backend.models.RequestCreateCounterSaleOrder;
import com.mastaan.logistics.backend.models.RequestCreateEmployeeReferralCouponCode;
import com.mastaan.logistics.backend.models.RequestCreateJob;
import com.mastaan.logistics.backend.models.RequestCreateNewOrderForOrderItem;
import com.mastaan.logistics.backend.models.RequestDisableBuyer;
import com.mastaan.logistics.backend.models.RequestEditOrderItem;
import com.mastaan.logistics.backend.models.RequestEmployeeBonuses;
import com.mastaan.logistics.backend.models.RequestFailOrderItem;
import com.mastaan.logistics.backend.models.RequestFailQualityCheck;
import com.mastaan.logistics.backend.models.RequestFromDateToDate;
import com.mastaan.logistics.backend.models.RequestHousingSocieties;
import com.mastaan.logistics.backend.models.RequestItemLogs;
import com.mastaan.logistics.backend.models.RequestLinkToFCM;
import com.mastaan.logistics.backend.models.RequestLinkVendorAndButcherToOrderItem;
import com.mastaan.logistics.backend.models.RequestManageDayAvailability;
import com.mastaan.logistics.backend.models.RequestMarkAllMoneyCollectionsAsConsolidated;
import com.mastaan.logistics.backend.models.RequestMarkAsBillsPrinted;
import com.mastaan.logistics.backend.models.RequestObjectLogin;
import com.mastaan.logistics.backend.models.RequestRazorpayInvoiceID;
import com.mastaan.logistics.backend.models.RequestRazorpayRefundAmount;
import com.mastaan.logistics.backend.models.RequestReplaceOrderItem;
import com.mastaan.logistics.backend.models.RequestSendMessage;
import com.mastaan.logistics.backend.models.RequestSlotsConfig;
import com.mastaan.logistics.backend.models.RequestStockStatement;
import com.mastaan.logistics.backend.models.RequestUpdateAttributeDetails;
import com.mastaan.logistics.backend.models.RequestUpdateBuyerDetails;
import com.mastaan.logistics.backend.models.RequestUpdateCategoryAvailability;
import com.mastaan.logistics.backend.models.RequestUpdateCategorySlots;
import com.mastaan.logistics.backend.models.RequestUpdateCategoryVisibility;
import com.mastaan.logistics.backend.models.RequestUpdateCounterSaleOrderItem;
import com.mastaan.logistics.backend.models.RequestUpdateDiscountForOrder;
import com.mastaan.logistics.backend.models.RequestUpdateHubMeatItemStock;
import com.mastaan.logistics.backend.models.RequestUpdateInstallSource;
import com.mastaan.logistics.backend.models.RequestUpdateMeatItem;
import com.mastaan.logistics.backend.models.RequestUpdateMeatItemAvaialability;
import com.mastaan.logistics.backend.models.RequestUpdateMeatItemsAvailability;
import com.mastaan.logistics.backend.models.RequestUpdateMoneyCollectionTransactionDetails;
import com.mastaan.logistics.backend.models.RequestUpdateNetWeight;
import com.mastaan.logistics.backend.models.RequestUpdateOptionAvailability;
import com.mastaan.logistics.backend.models.RequestUpdateOrderDeliveryDate;
import com.mastaan.logistics.backend.models.RequestUpdateOrderItemButcher;
import com.mastaan.logistics.backend.models.RequestUpdateOrderItemStock;
import com.mastaan.logistics.backend.models.RequestUpdateOrderItemVendor;
import com.mastaan.logistics.backend.models.RequestUpdateOrderStatus;
import com.mastaan.logistics.backend.models.RequestUpdateSpecialInstructions;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItem;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItemAvailability;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItemSource;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItemStock;
import com.mastaan.logistics.backend.models.RequestWhereToNext;
import com.mastaan.logistics.backend.models.RequetPaytmPayment;
import com.mastaan.logistics.backend.models.ResponseAddStock;
import com.mastaan.logistics.backend.models.ResponseAffectedMeatItems;
import com.mastaan.logistics.backend.models.ResponseAssignDelivery;
import com.mastaan.logistics.backend.models.ResponseAssignDeliveryArea;
import com.mastaan.logistics.backend.models.ResponseBuyerAccessToken;
import com.mastaan.logistics.backend.models.ResponseCheckAmountTobeCollected;
import com.mastaan.logistics.backend.models.ResponseDBY;
import com.mastaan.logistics.backend.models.ResponseDisabledDates;
import com.mastaan.logistics.backend.models.ResponseFirebaseToken;
import com.mastaan.logistics.backend.models.ResponseMarkAsDelivered;
import com.mastaan.logistics.backend.models.ResponseMeatItemAttributes;
import com.mastaan.logistics.backend.models.ResponseObjectLogin;
import com.mastaan.logistics.backend.models.ResponseOrdersStats;
import com.mastaan.logistics.backend.models.ResponseRazorpayInvoiceID;
import com.mastaan.logistics.backend.models.ResponseRazorpayRefundAmount;
import com.mastaan.logistics.backend.models.ResponseSlotsConfig;
import com.mastaan.logistics.backend.models.ResponseUpdateAttributeDetails;
import com.mastaan.logistics.backend.models.ResponseUpdateMeatItemAvailability;
import com.mastaan.logistics.backend.models.ResponseUpdateOptionAvailability;
import com.mastaan.logistics.backend.models.ResponseUpdateStock;
import com.mastaan.logistics.backend.models.TempOrder;
import com.mastaan.logistics.models.AnalyticsCouponCode;
import com.mastaan.logistics.models.AnalyticsInstallSource;
import com.mastaan.logistics.models.AnalyticsMeatItemAvailabilityRequest;
import com.mastaan.logistics.models.AnalyticsReferral;
import com.mastaan.logistics.models.AnalyticsTopBuyer;
import com.mastaan.logistics.models.Bootstrap;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.BuyerSearchDetails;
import com.mastaan.logistics.models.CartDetails;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.ConfigurationDetails;
import com.mastaan.logistics.models.DeliveryAreaDetails;
import com.mastaan.logistics.models.DeliverySlotDetails;
import com.mastaan.logistics.models.DeliveryZoneDetails;
import com.mastaan.logistics.models.EmployeeBonusDetails;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.FollowupDetails;
import com.mastaan.logistics.models.HousingSocietyDetails;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.HubMeatItemStockDetails;
import com.mastaan.logistics.models.ItemLogDetails;
import com.mastaan.logistics.models.Job;
import com.mastaan.logistics.models.KmLogItem;
import com.mastaan.logistics.models.MembershipDetails;
import com.mastaan.logistics.models.MessageDetails;
import com.mastaan.logistics.models.MoneyCollectionDetails;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.PaymentDetails;
import com.mastaan.logistics.models.ProcessedStockDetails;
import com.mastaan.logistics.models.ReferralOfferDetails;
import com.mastaan.logistics.models.StockAlertDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.UserDetailsMini;
import com.mastaan.logistics.models.Vendor;
import com.mastaan.logistics.models.WarehouseDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;
import com.mastaan.logistics.models.WarehouseMeatItemStockDetails;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public interface RetrofitInterface {

    // COMMON API

    @GET("/bootstrap")
    void getBootstrap(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<Bootstrap> callback);

    @POST("/changewarehouse")
    void changeWarehouse(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestChangeWarehouse requestChangeWarehouse, Callback<ResponseStatus> callback);

    @GET("/slotsforcategory/{category_id}/{date}/{month}/{year}")
    void getSlotsForCategoryForDate(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("category_id") String category_id, @Path("date") int date, @Path("month") int month, @Path("year") int year, Callback<List<DeliverySlotDetails>> callback);

    @GET("/getrttoken")
    void getFireBaseToken(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<ResponseFirebaseToken> callback);

    @POST("/employee/{employee_id}/violations/add")
    void addEmployeeViolation(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("employee_id") String employee_id, @Body RequestAddEmployeeViolation requestAddEmployeeViolation, Callback<ResponseStatus> callback);

    @GET("/vendors/category/{category_id}")
    void getVendorsForCategory(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("category_id") String categoryID, Callback<List<Vendor>> callback);

    @GET("/vendors/livestock/{category_id}")
    void getLivestockVendorsForCategory(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("category_id") String categoryID, Callback<List<Vendor>> callback);

    @POST("/sendmessage")
    void sendMessage(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestSendMessage requestSendMessage, Callback<ResponseStatus> callback);

    // AUTHENTICATION API

    @POST("/connect")
    void login(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestObjectLogin user, Callback<ResponseObjectLogin> callback);

    @GET("/disconnect")
    void confirmLogout(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<ResponseStatus> callback);


    // ORDERS API

    @GET("/order/{order_id}")
    void getOrderDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String orderItemID, Callback<OrderDetails> callback);

    @GET("/orderitem/{order_item_id}")
    void getOrderItemDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID, Callback<OrderItemDetails> callback);

    @GET("/orderitemsfororder/{order_id}")
    void getOrderItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String order_id, Callback<List<OrderItemDetails>> callback);

    @GET("/deliveryboyrelatedorderitems/{order_id}")
    void getDeliveryBoyRelatedItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String order_id, Callback<List<OrderItemDetails>> callback);

    @GET("/deliveryboypendingdeliveries")
    void getMyPendigDeliveryOrderItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/pendingdeliveryorderitems")
    void getPendingDeliveryOrderItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/pendingdeliveryorderitems")
    void getAssignDeliveryItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<OrderItemDetails>> callback);

    @GET("/pendingqcorderitems")
    void getPendingQCItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/pendingorderitems")
    void getPendingProcessingItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/orderitems/pendingprocessing/deliveryareas")
    void getPendingProcessingZones(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<DeliveryZoneDetails>> callback);

    @GET("/orderitems/pendingprocessing/deliverygroup/{group_id}")
    void getPendingProcessingGroupItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("group_id") String group_id, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/neworderitems")
    void getNewOrderItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/ordersfortoday")
    void getTodaysOrders(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderDetails>> callback);

    @GET("/orderitemsfortoday")
    void getTodaysOrderItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/ordersdby/{day}/{month}/{year}")
    void getOrdersForDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("day") int day, @Path("month") int month, @Path("year") int year, @Query("loc") String location, Callback<List<OrderDetails>> callback);

    @GET("/firstordersdby/{day}/{month}/{year}")
    void getFirstTimeOrdersForDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("day") int day, @Path("month") int month, @Path("year") int year, Callback<List<OrderDetails>> callback);

    @GET("/pendingfollowupfirstordersdby/{day}/{month}/{year}")
    void getPendingFollowupFirstOrdersForDeliveryDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("day") int day, @Path("month") int month, @Path("year") int year, Callback<List<OrderDetails>> callback);

    @GET("/ordersforfuture")
    void getFutureOrders(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderDetails>> callback);

    @GET("/orderitemsforfuture")
    void getFutureOrderItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/pendingcashncarryitems")
    void getPendingCashNCarryItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/allcashncarryitems")
    void getAllCashNCarryItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/orderitems/cashncarry/{day}/{month}/{year}")
    void getCashNCarryItemsByDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("day") int day, @Path("month") int month, @Path("year") int year, Callback<List<OrderItemDetails>> callback);

    @GET("/orderitems/rejected")
    void getRejectedOrderItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<OrderItemDetails>> callback);

    @GET("/orders/pastpending")
    void getPastPendingOrders(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<OrderDetails>> callback);

    @GET("/orderitems/pending/deliveryareas")
    void getPendingDeliveryZones(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<DeliveryZoneDetails>> callback);

    @GET("/orderitems/pending/deliverygroup/{group_id}")
    void getDeliveryGroupItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("group_id") String groupID, Callback<List<OrderItemDetails>> callback);

    @GET("/orders/todayunprinted")
    void getUnprintedOrders(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<OrderDetails>> callback);

    @POST("/invoices/markasprinted")
    void markAsBillsPrinted(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestMarkAsBillsPrinted requestMarkAsBillsPrinted, Callback<ResponseStatus> callback);

    //----

    @POST("/orderitem/assigntocustomersupport/{order_item_id}")
    void assignToCustomerSupport(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @Body RequestAssignToCustomerService requestAssignToCustomerService, Callback<ResponseStatus> callback);

    @GET("/orderitem/removefromcustomersupport/{order_item_id}")
    void removeFromCustomerSupport(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<ResponseStatus> callback);

    @GET("/orderitems/checkunprocessedforcustomersupport")
    void getUnprocessedCustomerSupportItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/orderitems/assignedtocustomersupport")
    void getCustomerSupportItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<List<OrderItemDetails>> callback);

    @GET("/orderitem/claimforcustomersupport/{order_item_id}")
    void claimCustomerSupportItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<List<OrderItemDetails>> callback);

    @POST("/orderitem/addcommentforcustomersupport/{order_item_id}")
    void addCommentForCustomerSupport(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @Body RequestAddComment requestAddComment, Callback<ResponseStatus> callback);

    @GET("/orderitems/releasefromcustomersupport")
    void releaseAllCustomerSupportItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<ResponseStatus> callback);

    @GET("/orderitems/markasprocessedbycustomersupport")
    void completeAllCustomerSupportItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<ResponseStatus> callback);

    //---

    @GET("/assignhubtoorder/{order_id}/{hub_id}")
    void assignHubToOrder(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String orderID, @Path("hub_id") String hubID, Callback<ResponseStatus> callback);

    @GET("/markasacknowledged/{item_id}")
    void markAsAcknowledged(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id/*, @Query("loc") String location*/, Callback<ResponseStatus> callback);

    @GET("/deliveries/assign/{item_id}/{boy_id}")
    void assignDelivery(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Path("boy_id") String boy_id, Callback<ResponseAssignDelivery> callback);

    @POST("/deliveries/assignmultiple")
    void assignMultipleDeliveries(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestAssignDeliveries requestAssignDeliveries, Callback<List<ResponseAssignDelivery>> callback);

    @POST("/deliveries/confirmpickup")
    void confirmItemsPickup(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestConfirmItemsPickup requestObject, Callback<ResponseStatus> callback);

    @POST("/deliveries/claim/{order_id}")
    void claimDelivery(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String order_id, @Body RequestCreateJob requestObject, Callback<Job> callback);

    @POST("/deliveries/markdelivery/{order_id}")
    void completeDelivery(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String orderID, @Body RequestCompleteDelivery requestObject, Callback<ResponseMarkAsDelivered> callback);

//    @POST("/deliveries/markasdelivered/{item_id}")
//    void completeDelivery(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestCompleteDelivery requestObject, Callback<ResponseMarkAsDelivered> callback);

    @POST("/deliveries/markasundelivered/{item_id}")
    void cancelDelivery(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestCancelDelivery requestObject, Callback<ResponseMarkAsDelivered> callback);

    @GET("/orderitem/amounttocollect/{order_item_id}")
    void checkAmountTobeCollected(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<ResponseCheckAmountTobeCollected> callback);

    @POST("/orderitem/markmoneycollection/{item_id}")
    void updateOrderAmountCollected(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestAmountCollected requestObject, Callback<ResponseStatus> callback);

    @GET("/markasprocessing/{order_item_id}")
    void markAsProcessing(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id/*, @QueryMap Map<String, Object> map*/, Callback<ResponseStatus> callback);

    @GET("/markascashncarry/{order_item_id}")
    void markAsCashNCarryItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<ResponseStatus> callback);

    @GET("/unmarkascashncarry/{order_item_id}")
    void unmarkAsCashNCarryItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<ResponseStatus> callback);

    @POST("/updatenetweight/{order_item_id}")
    void updateNetWeight(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @Body RequestUpdateNetWeight requestUpdateNetWeight, Callback<ResponseStatus> callback);

    @GET("/resetqualitycheck/{order_item_id}")
    void resetQualityCheck(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<ResponseStatus> callback);

    @GET("/completequalitycheck/{order_item_id}")
    void completeQualityCheck(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<ResponseStatus> callback);

    @POST("/failqualitycheck/{order_item_id}")
    void failQualityCheck(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @Body RequestFailQualityCheck requestFailQualityCheck, Callback<ResponseStatus> callback);

    @GET("/markaspickuppending/{order_item_id}")
    void markAsPickUpPending(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID/*, @QueryMap Map<String, Object> query*/, Callback<ResponseStatus> callback);

    @POST("/linkorderitemtovendorandbutcher/{order_item_id}")
    void linkVendorAndButcherToOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID, @Body RequestLinkVendorAndButcherToOrderItem requestObject, Callback<ResponseStatus> callback);

    @POST("/orderitem/updatevendor/{order_item_id}")
    void updateVendorDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID, @Body RequestUpdateOrderItemVendor requestObject, Callback<ResponseStatus> callback);

    @POST("/orderitem/updatebutcher/{order_item_id}")
    void updateButcherDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID, @Body RequestUpdateOrderItemButcher requestObject, Callback<ResponseStatus> callback);

    @POST("/orderitem/updatestock/{order_item_id}")
    void updateStockDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID, @Body RequestUpdateOrderItemStock requestObject, Callback<ResponseStatus> callback);

    @GET("/order/updatealternaternatemobile/{order_id}/{alternate_mobile}")
    void updateAlternateMobile(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String orderID, @Path("alternate_mobile") String alternate_mobile, Callback<ResponseStatus> callback);

    @POST("/createjob")
    void createJob(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestCreateJob requestObject, Callback<Job> callback);

    @GET("/order/markcountersaleorderasdelivered/{order_id}")
    void markCounterSaleAsDelivered(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String orderID, Callback<ResponseStatus> callback);

    @POST("/orderitem/updatecountersaleitem/{order_item_id}")
    void updateCounterSaleOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID, @Body RequestUpdateCounterSaleOrderItem requestUpdateCounterSaleOrderItem, Callback<ResponseStatus> callback);


    @GET("/orderitem/markasnew/{order_item_id}")
    void markAsNew(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<ResponseStatus> callback);

    @GET("/orderitem/removeprocesedby/{order_item_id}")
    void removeProcessingBy(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<ResponseStatus> callback);

    @GET("/orderitem/removedeliveryboy/{order_item_id}")
    void removeDeliveryBoy(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<ResponseStatus> callback);


    @POST("/orderitem/update/{order_item_id}/")
    void editOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @Body RequestEditOrderItem requestEditOrderItem, Callback<OrderItemDetails> callback);

    @GET("/orderitem/updatequantity/{item_id}/{new_quantity}")
    void updateOrderItemQuantity(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Path("new_quantity") double new_quantity, Callback<ResponseStatus> callback);

    @POST("/orderitem/requestreplace/{item_id}")
    void replaceOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestReplaceOrderItem requestReplaceOrderItem, Callback<ResponseStatus> callback);

    @POST("/orderitem/updatespecialinstructions/{order_item_id}")
    void updateSpecialInstructionsoOfOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @Body RequestUpdateSpecialInstructions requestUpdateSpecialInstructions, Callback<ResponseStatus> callback);

    @GET("/orderitem/remove/{item_id}")
    void deleteOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, Callback<ResponseStatus> callback);

    @POST("/orderitem/{order_item_id}/checkforcancel")
    void checkForCancelOrderItem(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Path("order_item_id") String orderItemID, @Body RequestCheckForCancelOrderItem requestCheckForCancelOrderItem, Callback<ResponseStatus> callback);

    @POST("/orderitem/markasfailed/{item_id}")
    void failOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestFailOrderItem requestFailOrderItem, Callback<ResponseStatus> callback);

    @GET("/orderitem/reprocess/{order_item_id}")
    void reProcessOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, Callback<OrderItemDetails> callback);

    @POST("/deliveries/adddelayeddeliverycomment/{order_item_id}")
    void addDelayedDeliveryComment(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @Body RequestAddComment requestAddComment, Callback<ResponseStatus> callback);

    //----

    @GET("/deliveries/assignfororder/{order_id}/{boy_id}")
    void assignDeliveryBoyToOrder(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String order_id, @Path("boy_id") String boy_id, Callback<ResponseStatus> callback);

    @POST("/order/updatedeliverydate/{order_id}")
    void updateOrderDeliveryDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String order_id, @Body RequestUpdateOrderDeliveryDate requestObject, Callback<ResponseStatus> callback);

    @GET("/pendingordersforbuyerfordate/{buyer_id}/{day}/{month}/{year}")
    void getBuyerOrdersForDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyer_id, @Path("day") int day, @Path("month") int month, @Path("year") int year, Callback<List<TempOrder>> callback);

    @GET("/orderitem/mergewithorder/{order_item_id}/{order_id}")
    void mergeOrderItemToOrder(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @Path("order_id") String order_id, Callback<ResponseDBY> callback);

    @POST("/order/createwithorderitem/{order_item_id}")
    void createNewOrderForOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @Body RequestCreateNewOrderForOrderItem requestObject, Callback<OrderItemDetails> callback);

    @POST("/order/updatediscount/{order_id}")
    void updateDiscountForOrder(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String order_id, @Body RequestUpdateDiscountForOrder requestObject, Callback<ResponseStatus> callback);

    @GET("/order/removediscount/{order_id}/{discount_id}")
    void removeDiscount(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String order_id, @Path("discount_id") String discount_id, Callback<ResponseStatus> callback);

    @POST("/order/updatestatus/{order_id}")
    void updateOrderStatus(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String order_id, @Body RequestUpdateOrderStatus requestUpdateOrderStatus, Callback<ResponseStatus> callback);

    @POST("/orders/createcountersaleorder/")
    void createCounterSaleOrder(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestCreateCounterSaleOrder requestObject, Callback<OrderItemDetails> callback);

    //-----------

    @POST("/next/{next_id}")
    void whereToNext(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("next_id") String id, @Body RequestWhereToNext requestWhereToNext, Callback<Job> callback);

    @GET("/resume")
    void resume(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<ResponseStatus> callback);

    @GET("/canceltrip")
    void cancelTrip(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("loc") String location, Callback<ResponseStatus> callback);

    @GET("/vendors")
    void getVendors(@Query("i") String deviceID, @Query("appVersion") int appVersion/*, @QueryMap Map<String, Object> query*/, Callback<List<Vendor>> callback);

    @GET("/vendors/{order_item_id}")
    void getVendorsForOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id, @QueryMap Map<String, Object> query, Callback<List<Vendor>> callback);

    @GET("/butchersforcategory/{category_id}")
    void getButchersForCategory(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("category_id") String category_id, Callback<List<UserDetailsMini>> callback);

    @GET("/butchers/{order_item_id}")
    void getButchersForOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String order_item_id/*, @QueryMap Map<String, Object> query*/, Callback<List<UserDetailsMini>> callback);

//    @GET("/hubs")
//    void getHubs(@Query("i") String deviceID, @Query("appVersion") int appVersion, @QueryMap Map<String, Object> query, Callback<List<Hub>> callback);

    //-------

    @GET("/categories")
    void getCategories(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<CategoryDetails>> callback);

    @GET("/slotsforcategory/{category_id}/{day}/{month}/{year}")
    void getCategorySlots(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("category_id") String categoryID, @Path("day") int day, @Path("month") int month, @Path("year") int year, Callback<List<DeliverySlotDetails>> callback);

    @POST("/categoryslots/updateavailability")
    void updateCategorySlots(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestUpdateCategorySlots requestUpdateCategorySlots, Callback<ResponseStatus> callback);

    @POST("/category/update/{category_id}")
    void updateCategoryVisibility(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("category_id") String categoryID, @Body RequestUpdateCategoryVisibility requestUpdateCategoryVisibility, Callback<ResponseStatus> callback);

    @POST("/category/update/{category_id}")
    void updateCategoryAvailability(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("category_id") String categoryID, @Body RequestUpdateCategoryAvailability requestUpdateCategoryAvailability, Callback<ResponseStatus> callback);

    //---------

    @GET("/disableddeliverydates/{month}/{year}")
    void getDisabledDays(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("month") int month, @Path("year") int year, Callback<ResponseDisabledDates> callback);

    @POST("/updatedisableddeliverydate/{day}/{month}/{year}")
    void manageDayAvailability(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("day") int day, @Path("month") int month, @Path("year") int year, @Body RequestManageDayAvailability requestManageDayAvailability, Callback<ResponseStatus> callback);

    //-------

    @GET("/warehousemeatitemfor/{meatitem_id}")
    void getWareHouseMeatItemOfMeatItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("meatitem_id") String meatItemID, Callback<WarehouseMeatItemDetails> callback);

    @GET("/meatitems")
    void getMeatItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("availability") String availability, @Query("forStocks") boolean forStocks, @Query("hubID") String hubID, Callback<List<WarehouseMeatItemDetails>> callback);

    @GET("/whmeatitem/{id}")
    void getWarehouseMeatItemDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("id") String warehouseMeatItemID, @Query("forStocks") boolean forStocks, @Query("hubID") String hubID, Callback<WarehouseMeatItemDetails> callback);

    @GET("/meatitem/attributes/{item_id}")
    void getMeatItemAttributes(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String meatItemID, Callback<ResponseMeatItemAttributes> callback);

    @POST("/meatitem/update/{item_id}")
    void updateMeatItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestUpdateMeatItem requestUpdateMeatItem, Callback<ResponseStatus> callback);

    @POST("/whmeatitem/update/{item_id}")
    void updateWarehouseMeatItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestUpdateWarehouseMeatItem requestUpdateWarehouseMeatItem, Callback<ResponseStatus> callback);

    @POST("/whmeatitem/update/{item_id}")
    void updateWarehouseMeatItemAvailability(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestUpdateWarehouseMeatItemAvailability requestUpdateWarehouseMeatItemAvailability, Callback<ResponseStatus> callback);

    @POST("/meatitem/updateavailability/{item_id}")
    void updateMeatItemAvailability(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String item_id, @Body RequestUpdateMeatItemAvaialability requestUpdateMeatItemAvaialability, Callback<ResponseUpdateMeatItemAvailability> callback);

    @POST("/meatitems/updateavailability")
    void updateMeatItemsAvailabilities(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestUpdateMeatItemsAvailability requestUpdateMeatItemsAvailability, Callback<ResponseStatus> callback);

    @POST("/attribute/update/{attribute_id}")
    void updateAttributeDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("attribute_id") String attributeID, @Body RequestUpdateAttributeDetails requestUpdateAttributeDetails, Callback<ResponseUpdateAttributeDetails> callback);

    @POST("/meatitemattributeoption/updateavailability/{option_id}")
    void updateAttributeOptionAvailability(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("option_id") String optionID, @Body RequestUpdateOptionAvailability requestUpdateOptionAvailability, Callback<ResponseUpdateOptionAvailability> callback);

    @GET("/meatitemattributeoption/dependentmeatitems/{option_id}")
    void getAfftecedMeatItems(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("option_id") String optionID, Callback<ResponseAffectedMeatItems> callback);

    @POST("/meatitemlogs/{item_id}")
    void getMeatItemLogs(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String meatItemID, @Body RequestItemLogs requestItemLogs, @Query("perPage") int perPage, Callback<List<ItemLogDetails>> callback);

    @POST("/warehousemeatitemlogs/{item_id}")
    void getWarehouseMeatItemLogs(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String warehouseMeatItemID, @Body RequestItemLogs requestItemLogs, @Query("perPage") int perPage, Callback<List<ItemLogDetails>> callback);

    //-------

    @GET("/trips/{month}/{year}")
    void getKmLogTripList(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("month") int month, @Path("year") int year, @QueryMap Map<String, Object> options, Callback<List<KmLogItem>> callback);

    @GET("/profile")
    void getProfile(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<UserDetails> userCallback);

    //--------

    @POST("/employee/linktofcm")
    void linkToFCM(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Query("accessToken") String accessToken, @Body RequestLinkToFCM requestObj, Callback<ResponseStatus> callback);

    @GET("/employees")
    void getEmployees(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<UserDetails>> callback);

    @GET("/deliveryboys")
    void getDeliveryBoys(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<UserDetails>> callback);

    @GET("/processingmanagers")
    void getProcessingManagers(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<UserDetails>> callback);

    @GET("/availabledeliveryboys")
    void getAvailableDeliveryBoys(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<UserDetails>> callback);

    @GET("/deliveryboyalldeliveries/{delivery_boy_id}")
    void getDeliveryBoyOrders(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("delivery_boy_id") String delivery_boy_id, Callback<List<OrderDetails>> callback);

    @GET("/deliveryboy/currentjob/{delivery_boy_id}")
    void getDeliveryBoyCurrentJob(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("delivery_boy_id") String delivery_boy_id, Callback<Job> callback);

    @GET("/deliveryboy/clearjob/{employee_id}")
    void clearEmployeeBoyJob(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("employee_id") String employeeID, Callback<ResponseStatus> callback);

    @POST("/createemployeereferralcoupon/{employee_id}")
    void createEmployeeReferralCoupon(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("employee_id") String employee_id, @Body RequestCreateEmployeeReferralCouponCode requestObject, Callback<ResponseStatus> callback);

    @POST("/employeebonuses")
    void getEmployeeBonuses(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestEmployeeBonuses requestEmployeeBonuses, Callback<List<EmployeeBonusDetails>> callback);

    // FEEDBACKS API

    @GET("/feedbackorderitem/{id}")
    void getFeedbackDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("id") String feedbackID, Callback<FeedbackDetails> callback);

    @GET("/feedbackorderitems/{day}/{month}/{year}")
    void getFeedbacksForDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("day") int day, @Path("month") int month, @Path("year") int year, @Query("byorderdate") boolean byOrderDate, Callback<List<FeedbackDetails>> callback);

    @GET("/feedbackorderitems/lowrated")
    void getLowRatedFeedbacks(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<FeedbackDetails>> callback);

    @GET("/feedbackorderitems/averagerated")
    void getAverageRatedFeedbacks(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<FeedbackDetails>> callback);

    @GET("/feedbackorderitems/highrated")
    void getHighRatedFeedbacks(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<FeedbackDetails>> callback);

    @GET("/feedbackorderitems/processing")
    void getProcessingFeedbacks(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<FeedbackDetails>> callback);

    @GET("/feedbackorderitems/processed/{day}/{month}/{year}")
    void getProcessedFeedbacksForDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("day") int day, @Path("month") int month, @Path("year") int year, Callback<List<FeedbackDetails>> callback);

    @GET("/feedbackorderitem/checkunprocessed")
    void getUnProcessedFeedbacks(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<FeedbackDetails>> callback);

    @GET("/buyer/feedbacks/{buyer_id}")
    void getBuyerFeedbacks(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, Callback<List<FeedbackDetails>> callback);

    @GET("/feedbackorderitem/claim/{feedback_id}")
    void claimFeedback(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("feedback_id") String feedback_id, Callback<List<FeedbackDetails>> callback);

    @POST("/feedbackorderitem/addcomment/{feedback_id}")
    void addFeedbackResolutionComment(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("feedback_id") String feedback_id, @Body RequestAddFeedbackResolutionComment requestObject, Callback<ResponseStatus> callback);

    @GET("/feedbackorderitem/markasprocessed/{feedback_id}")
    void completeFeedback(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("feedback_id") String feedback_id, Callback<ResponseStatus> callback);

    @GET("/feedbackorderitem/markallprocessedfororder/{order_id}")
    void completeAllFeedbacksForOrder(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String order_id, Callback<ResponseStatus> callback);

    @GET("/feedbackorderitem/releaseunprocessed")
    void releaseUnprocessedFeedbacks(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<ResponseStatus> callback);

    @GET("/feedbackorderitem/addtotestimonials/{feedback_id}")
    void addToTestimonials(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("feedback_id") String feedbackID, Callback<ResponseStatus> callback);

    @GET("/feedbackorderitem/removefromtestimonials/{feedback_id}")
    void removeFromTestimonials(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("feedback_id") String feedbackID, Callback<ResponseStatus> callback);

    // FOLLOWUPS API

    @POST("/buyer/followups")
    void getFollowups(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Query("onlyOrderResulted") String onlyOrderResulted, @Query("onlyFirstOrderFollowups") String onlyFirstOrderFollowups, @Body RequestFromDateToDate requestFromDateToDate, Callback<List<FollowupDetails>> callback);

    @GET("/buyer/followup/{followup_id}")
    void getFollowupDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("followup_id") String followupID, Callback<FollowupDetails> callback);

    @GET("/buyer/followups/{buyer_id}")
    void getBuyerFollowups(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, Callback<List<FollowupDetails>> callback);

    @POST("/buyer/followup")
    void addFollowup(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestAddFollowup requestAddFollowup, Callback<FollowupDetails> callback);

    @GET("/buyer/followup/claim/{buyer_id}")
    void claimBuyerFollowup(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, Callback<ResponseStatus> callback);

    @GET("/buyer/followup/release/{buyer_id}")
    void releaseBuyerFollowup(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, Callback<ResponseStatus> callback);

    // MONEY COLLECTION API

    @GET("/moneycollectionfororder/{order_id}")
    void getMoneyCollectionForOrder(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_id") String orderID, Callback<MoneyCollectionDetails> callback);

    @GET("/moneycollection/{date}/{month}/{year}")
    void getMoneyCollectionForDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("date") int date, @Path("month") int month, @Path("year") int year, Callback<List<MoneyCollectionDetails>> callback);

    @GET("/deliveryboy/moneycollection/{delivery_boy_id}/{date}/{month}/{year}")
    void getMoneyCollectionOfDeliveryBoyForDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("delivery_boy_id") String deliveryBoyID, @Path("date") int date, @Path("month") int month, @Path("year") int year, Callback<List<MoneyCollectionDetails>> callback);

    @GET("/moneyconsolidation/{date}/{month}/{year}")
    void getMoneyConsolidationForDate(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("date") int date, @Path("month") int month, @Path("year") int year, Callback<List<MoneyCollectionDetails>> callback);

    @GET("/moneycollection/pending")
    void getPendingConsolidations(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<MoneyCollectionDetails>> callback);

    @GET("/deliveryboy/moneycollection/pending/{delivery_boy_id}")
    void getPendingConsolidationOfDeliveryBoy(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("delivery_boy_id") String deliveryBoyID, Callback<List<MoneyCollectionDetails>> callback);

    @GET("/buyer/moneycollection/{buyer_id}")
    void getBuyerMoneyCollections(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, Callback<List<MoneyCollectionDetails>> callback);

    @GET("/moneycollection/markasconsolidated/{collection_id}/{transaction_id}")
    void markMoneyCollectionTransactionAsConsolidated(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("collection_id") String collectionID, @Path("transaction_id") String transactionID, Callback<ResponseStatus> callback);

    @POST("/moneycollection/update/{collection_id}/{transaction_id}")
    void updateMoneyCollectionDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("collection_id") String collectionID, @Path("transaction_id") String transactionID, @Body RequestUpdateMoneyCollectionTransactionDetails requestUpdateMoneyCollectionTransactionDetails, Callback<ResponseStatus> callback);

    @POST("/moneycollection/markasconsolidated")
    void markAllMoneyCollectionsAsConsolidated(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestMarkAllMoneyCollectionsAsConsolidated requestMarkAllMoneyCollectionsAsConsolidated, Callback<ResponseStatus> callback);

    @POST("/moneycollection/addrefund")
    ResponseStatus addRefundMoneyCollection(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestAddRefundMoneyCollection requestAddRefundMoneyCollection);

    //------

    @GET("/stock/whmeatitem/{whmeatitem_id_with_item_level_attrribute_options}")
    void getWarehouseMeatItemStock(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("whmeatitem_id_with_item_level_attrribute_options") String warehouseMeatItemIDWithItemLevelAttributeOptions, Callback<WarehouseMeatItemStockDetails> callback);

    @GET("/hubstock/{hub_id}/{whmeatitem_id_with_item_level_attrribute_options}")
    void getHubMeatItemStock(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("hub_id") String hubID, @Path("whmeatitem_id_with_item_level_attrribute_options") String warehouseMeatItemIDWithItemLevelAttributeOptions, Callback<HubMeatItemStockDetails> callback);

    @POST("/stock/whmeatitem/update/{whmeatitem_id_with_item_level_attrribute_options}")
    void updateWarehouseMeatItemStock(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("whmeatitem_id_with_item_level_attrribute_options") String warehouseMeatItemIDWithItemLevelAttributeOptions, @Body RequestUpdateWarehouseMeatItemStock requestObject, Callback<ResponseStatus> callback);

    @POST("/hubstock/{hub_id}/whmeatitem/update/{whmeatitem_id_with_item_level_attrribute_options}")
    void updateHubMeatItemStock(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("hub_id") String hubID, @Path("whmeatitem_id_with_item_level_attrribute_options") String warehouseMeatItemIDWithItemLevelAttributeOptions, @Body RequestUpdateHubMeatItemStock requestObject, Callback<ResponseStatus> callback);

    @GET("/categoriesforstock")
    void getCategoriesForStockManagement(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<CategoryDetails>> callback);

    @POST("/stock/categorystatement")
    void getCategoriesStockStatement(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestStockStatement requestStockStatement, Callback<List<StockDetails>> callback);

    @POST("/stock/meatitemsstatement")
    void getMeatItemsStockStatement(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestStockStatement requestStockStatement,  Callback<List<StockDetails>> callback);


    @POST("/stock/create")
    void addStock(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestAddOrEditStock requestAddOrEditStock, Callback<ResponseAddStock> callback);

    @POST("/stock/update/{stock_id}")
    void updateStock(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("stock_id") String stock_id, @Body RequestAddOrEditStock requestAddOrEditStock, Callback<ResponseUpdateStock> callback);

    @GET("/stock/delete/{stock_id}")
    void deleteStock(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("stock_id") String stock_id, Callback<ResponseStatus> callback);

    @GET("/stock/{stock_id}")
    void getStockDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("stock_id") String stock_id, Callback<StockDetails> callback);

    @GET("/stock/alerts")
    void getStockAlerts(@Query("i") String deviceID, @Query("appVersion") int appVersion, Callback<List<StockAlertDetails>> callback);

    @POST("/stock/whmeatitem/updatesource/{warehouse_meat_item_id}")
    void updateWarehouseMeatItemSource(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("warehouse_meat_item_id") String warehouseMeatItemID, @Body RequestUpdateWarehouseMeatItemSource requestObject, Callback<ResponseStatus> callback);

    @POST("/processedstock/create")
    void addProcessedStock(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Body RequestAddOrEditProcessedStock requestObject, Callback<ProcessedStockDetails> callback);

    @POST("/processedstock/update/{stock_id}")
    void updateProcessedStock(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("stock_id") String stock_id, @Body RequestAddOrEditProcessedStock requestObject, Callback<ProcessedStockDetails> callback);

    @GET("/processedstock/delete/{stock_id}")
    void deleteProcessedStock(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("stock_id") String stock_id, Callback<ResponseStatus> callback);

    @GET("/processedstock/{stock_id}")
    void getProcessedStockDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("stock_id") String stock_id, Callback<ProcessedStockDetails> callback);

    @GET("/processedstock/{month}/{year}")
    void getProcessedStockStatement(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("month") int month, @Path("year") int year, Callback<List<ProcessedStockDetails>> callback);

    @GET("/processedstocksforcategory/{category_id}")
    void getProcessedStocksForCategory(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("category_id") String categoryID, Callback<List<ProcessedStockDetails>> callback);

    @GET("/warehousemeatitemstockfromorderitem/{order_item_id}")
    void getWarehouseMeatItemStockFromOrderItem(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID, Callback<WarehouseMeatItemStockDetails> callback);

    @GET("/hubmeatitemstockfromorderitem/{order_item_id}")
    void getHubMeatItemStockFromOrderItem(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID, Callback<HubMeatItemStockDetails> callback);

    @GET("/stocksforwarehousemeatitem/{whmeatitem_id_with_item_level_attrribute_options}")
    void getStocksForWarehouseMeatItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("whmeatitem_id_with_item_level_attrribute_options") String warehouseMeatItemIDWithItemLevelAttributeOptions, Callback<List<StockDetails>> callback);

    @GET("/stocksfororderitem/{order_item_id}")
    void getStocksForOrderItem(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("order_item_id") String orderItemID, Callback<List<StockDetails>> callback);

    //------

    @GET("/buyeraddressbook/{buyer_id}")
    void getBuyerAddressBook(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyer_id, Callback<List<AddressBookItemDetails>> callback);

    @GET("/buyerdetails/{buyer_id}")
    void getBuyerDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyer_id, Callback<BuyerDetails> callback);

    @POST("/buyer/update/{buyer_id}")
    void updateBuyerDetails(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyer_id, @Body RequestUpdateBuyerDetails requestUpdateBuyerDetails, Callback<ResponseStatus> callback);

    @GET("/buyer/enable/{buyer_id}")
    void enableBuyer(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyer_id, Callback<ResponseStatus> callback);

    @POST("/buyer/disable/{buyer_id}")
    void disableBuyer(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyer_id, @Body RequestDisableBuyer requestDisableBuyer, Callback<ResponseStatus> callback);

    @POST("/buyers/{page_number}")
    void getBuyers(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("page_number") long pageNumber, @Body BuyerSearchDetails searchDetails, Callback<List<BuyerDetails>> callback);

    @POST("/couponcodes/buyers/{coupon_code}")
    void getCouponBuyers(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("coupon_code") String couponCode, @Body BuyerSearchDetails searchDetails, Callback<List<BuyerDetails>> callback);

    @POST("/referrals/buyers/{referral_code}")
    void getReferralBuyers(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("referral_code") String referralCode, @Body BuyerSearchDetails searchDetails, Callback<List<BuyerDetails>> callback);

    @POST("/meatitemavailabilityrequests/buyers/{item_id}")
    void getMeatItemAvailabilityRequestedBuyers(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("item_id") String warehouseMeatItemID, @Body BuyerSearchDetails searchDetails, Callback<List<BuyerDetails>> callback);

    @GET("/buyers/search/{query}")
    void searchBuyers(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("query") String query, Callback<List<BuyerDetails>> callback);

    @GET("/buyer/orderhistory/{buyer_id}")
    void getBuyerOrders(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyer_id, Callback<List<OrderDetails>> callback);

    @GET("/buyer/accesstoken/{buyer_id}")
    void getBuyerAccessTokenUsingID(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("buyer_id") String buyer_id, Callback<ResponseBuyerAccessToken> callback);

    @GET("/buyer/accesstokenformobile/{mobile_number}")
    void getBuyerAccessTokenUsingMobile(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("mobile_number") String mobile_number, Callback<ResponseBuyerAccessToken> callback);

    @POST("/buyer/updateinstallsource/{buyer_id}")
    void updateBuyerInstallSource(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, @Body RequestUpdateInstallSource requestUpdateInstallSource, Callback<ResponseStatus> callback);

    @GET("/buyer/promotioanlalerts/{buyer_id}/{preference}")
    void updateBuyerPromotionalAlertsPreference(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, @Path("preference") boolean preference, Callback<ResponseStatus> callback);

    //-------------- CARTS API ----------------//

    @POST("/buyer/inactivecarts/{page_number}")
    void getInactiveBuyerCarts(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("page_number") long pageNumber, @Body BuyerSearchDetails buyerSearchDetails, Callback<List<CartDetails>> callback);

    @GET("/buyer/cart/followup/claim/{cart_id}")
    void claimBuyerCartFollowup(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("cart_id") String cartID, Callback<ResponseStatus> callback);

    @GET("/buyer/cart/followup/release/{cart_id}")
    void releaseBuyerCartFollowup(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("cart_id") String cartID, Callback<ResponseStatus> callback);

    @GET("/buyer/cart/delete/{cart_id}")
    void deleteBuyerCart(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("cart_id") String cartID, Callback<ResponseStatus> callback);

    @POST("/buyer/cart/followup/complete/{cart_id}")
    void completeBuyerCartFollowup(@Query("i") String deviceID, @Query("appVersion") int appVersion, @Path("cart_id") String cartID, @Body RequestAddComment requestAddComment, Callback<FollowupDetails> callback);

    //-------------- PAYMENTS API ----------------//

    @POST("/capturerazorpaypayment")
    void captureRazorpayPayment(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestCaptureRazorpayPayment requestCaptureRazorpayPayment, Callback<ResponseStatus> callback);

    @POST("/order/initiaterazorpaypayment/{order_id}")
    void getRazorpayInvoiceIDForOrder(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("order_id") String orderID, @Body RequestRazorpayInvoiceID requestRazorpayInvoiceID, Callback<ResponseRazorpayInvoiceID> callback);

    @POST("/buyer/initiaterazorpaypaymentforoutstanding/{buyer_id}")
    void getRazorpayInvoiceIDForBuyer(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, @Body RequestRazorpayInvoiceID requestRazorpayInvoiceID, Callback<ResponseRazorpayInvoiceID> callback);

    @POST("/buyer/initiateoutstandingpaymentviapaytm/{buyer_id}")
    void initiatePaytmePayment(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, @Body RequetPaytmPayment requetPaytmPayment, Callback<ResponseStatus> callback);

    @POST("/order/addonlinepayment/{order_id}")
    void addOnlinePaymentForOrder(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("order_id") String orderID, @Body RequestAddOnlinePayment requestAddOnlinePayment, Callback<ResponseStatus> callback);

    @GET("/order/checkraozrpaypayments/{order_id}")
    void checkOrderPaymentStatus(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("order_id") String orderID, Callback<OrderDetails> callback);

    @GET("/buyer/checkraozrpaypaymentsforoutstanding/{buyer_id}")
    void checkBuyerPaymentStatus(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("buyer_id") String buyerID, Callback<BuyerDetails> callback);


    // ROUTES API

    @GET("/warehouses")
    void getWarehouses(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<List<WarehouseDetails>> callback);

    @POST("/warehouses")
    void addWarehouse(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestAddOrUpdateWarehouse requestObject, Callback<WarehouseDetails> callback);

    @POST("/warehouses/{warehouse_id}")
    void updateWarehouse(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("warehouse_id") String warehouseID, @Body RequestAddOrUpdateWarehouse requestObject, Callback<WarehouseDetails> callback);

    @GET("/hubs")
    void getHubs(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<List<HubDetails>> callback);

    @POST("/hubs")
    void addHub(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestAddOrUpdateHub requestObject, Callback<HubDetails> callback);

    @POST("/hubs/{hub_id}")
    void updateHub(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("hub_id") String hubID, @Body RequestAddOrUpdateHub requestObject, Callback<HubDetails> callback);

    @GET("/hub/delete/{hub_id}")
    void deleteHub(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("hub_id") String hubID, Callback<ResponseStatus> callback);

    @GET("/deliverygroups")
    void getDeliveryGroups(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<List<String>> callback);

    @GET("/deliveryareas")
    void getDeliveryAreas(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<List<DeliveryAreaDetails>> callback);

    @POST("/deliverygroups")
    void addDeliveryZone(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestAddOrUpdateDeliveryZone requestAddOrUpdateDeliveryZone, Callback<String> callback);

    @POST("/deliveryareas")
    void addDeliveryArea(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestAddOrUpdateDeliveryArea requestAddOrUpdateDeliveryArea, Callback<DeliveryAreaDetails> callback);

    @POST("/deliveryareas/{area_id}")
    void updateDeliveryArea(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("area_id") String areaID, @Body RequestAddOrUpdateDeliveryArea requestAddOrUpdateDeliveryArea, Callback<DeliveryAreaDetails> callback);

    @POST("/orders/assigndeliveryarea/{order_id}")
    void assignOrderToDeliveryArea(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("order_id") String orderID, @Body RequestAssignDeliveryArea requestAssignDeliveryArea, Callback<ResponseAssignDeliveryArea> callback);

    @POST("/housingsocieties")
    void getHousingSocieties(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestHousingSocieties requestHousingSocieties, Callback<List<HousingSocietyDetails>> callback);

    @POST("/housingsocieties/add")
    void addHousingSociety(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestAddOrUpdateHousingSociety requestObject, Callback<HousingSocietyDetails> callback);

    @POST("/housingsocieties/{id}")
    void updateHousingSociety(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("id") String housingSocietyID, @Body RequestAddOrUpdateHousingSociety requestObject, Callback<HousingSocietyDetails> callback);

    @GET("/housingsocieties/delete/{id}")
    void deleteHousingSociety(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("id") String housingSocietyID, Callback<ResponseStatus> callback);

    @POST("/orders/assignhousingsociety/{order_id}")
    void assignOrderToHousingSociety(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("order_id") String orderID, @Body RequestAssignHousingSociety requestAssignHousingSociety, Callback<ResponseStatus> callback);

    // SLOTS API

    @GET("/slotsconfig")
    void getSlotsConfig(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<ResponseSlotsConfig> callback);

    @POST("/slotsconfig")
    void updateSlotsConfig(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestSlotsConfig requestSlotsConfig, Callback<ResponseStatus> callback);


    // ANALYTICS API

    @GET("/analytics/topbuyers/{count}")
    void getTopBuyersAnalytics(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("count") int count, Callback<List<AnalyticsTopBuyer>> callback);

    @POST("/analytics/installsource")
    void getBuyerInstallSourceAnalytics(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestFromDateToDate requestFromDateToDate, Callback<List<AnalyticsInstallSource>> callback);

    @POST("/analytics/couponcodes")
    void getCouponCodesAnalytics(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestFromDateToDate requestFromDateToDate, Callback<List<AnalyticsCouponCode>> callback);

    @POST("/analytics/referrals")
    void getReferralsAnalytics(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestFromDateToDate requestFromDateToDate, Callback<List<AnalyticsReferral>> callback);

    @POST("/analytics/meatitemavailabilityrequests")
    void getMeatItemAvailabilityRequestsAnalytics(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body RequestFromDateToDate requestFromDateToDate, Callback<List<AnalyticsMeatItemAvailabilityRequest>> callback);

    @GET("/daywiseorders/{year}/{month}")
    void getDaywiseOrdersReportForMonth(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("year") int year, @Path("month") int month, @Query("fto") boolean firstTimeOrdersOnly, Callback<ResponseOrdersStats> callback);


    // CONFIGURATIONS API

    @GET("/configuration")
    void getConfiguration(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<ConfigurationDetails> callback);

    @POST("/configuration")
    void updateConfiguration(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body ConfigurationDetails requestObject, Callback<ResponseStatus> callback);

    @GET("/buyer/alertmessages")
    void getAlertMessages(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<List<MessageDetails>> callback);

    @Multipart
    @POST("/buyer/alertmessages")
    void addAlertMessage(@Query("i") String deviceID , @Query("appVersion") int appVersion
            , @Part("type") String type
            , @Part("status") boolean status, @Part("title") String title, @Part("details") String details, @Part("image") String image
            , @Part("url") String url, @Part("app_url") String app_url, @Part("action") String action
            , @Part("start_date") String start_date, @Part("end_date") String end_date, @Part("show_once") boolean show_once
            , @Part("attachment") TypedFile typedFile
            , Callback<MessageDetails> callback);

    @Multipart
    @POST("/buyer/alertmessages/{message_id}")//5a9bbfb5ba8c246c0515ff0d
    void updateAlertMessage(@Query("i") String deviceID , @Query("appVersion") int appVersion
            , @Path("message_id") String messageID
            , @Part("type") String type
            , @Part("status") boolean status, @Part("title") String title, @Part("details") String details, @Part("image") String image
            , @Part("url") String url, @Part("app_url") String app_url, @Part("action") String action
            , @Part("start_date") String start_date, @Part("end_date") String end_date, @Part("show_once") boolean show_once
            , @Part("attachment") TypedFile typedFile
            , Callback<MessageDetails> callback);

    @GET("/buyer/deletealertmessage/{message_id}")
    void deleteAlertMessage(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("message_id") String messageID, Callback<ResponseStatus> callback);


    @GET("/buyer/referraloffer")
    void getReferralOfferDetails(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<ReferralOfferDetails> callback);

    @POST("/buyer/referraloffer")
    void addReferralOffer(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body ReferralOfferDetails referralOfferDetails, Callback<ReferralOfferDetails> callback);

    @POST("/buyer/referraloffer/{referral_offer_id}")
    void updateReferralOffer(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("referral_offer_id") String referralOfferID, @Body ReferralOfferDetails referralOfferDetails, Callback<ResponseStatus> callback);

    @GET("/updateallbuyersmemberships")
    void updateAllBuyersMemberships(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<ResponseStatus> callback);

    @GET("/buyer/memberships")
    void getMemberships(@Query("i") String deviceID , @Query("appVersion") int appVersion, Callback<List<MembershipDetails>> callback);

    @POST("/buyer/membership")
    void addMembership(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Body MembershipDetails membershipDetails, Callback<MembershipDetails> callback);

    @POST("/buyer/membership/{membership_id}")
    void updateMembership(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("membership_id") String membershipID, @Body MembershipDetails membershipDetails, Callback<ResponseStatus> callback);

    @GET("/buyer/membership/delete/{membership_id}")
    void deleteMembership(@Query("i") String deviceID , @Query("appVersion") int appVersion, @Path("membership_id") String membershipID, Callback<ResponseStatus> callback);


    // RAZORPAY API

    @POST("/razorpay/payments/{payment_id}/refund")
    ResponseRazorpayRefundAmount refundAmount(@Path("payment_id") String paymentID, @Body RequestRazorpayRefundAmount requestRazorpayRefundAmount);

    @GET("/razorpay/payments/{payment_id}")
    PaymentDetails getPaymentDetails(@Path("payment_id") String paymentID);


}