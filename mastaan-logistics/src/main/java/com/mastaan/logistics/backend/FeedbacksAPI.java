package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.models.RequestAddFeedbackResolutionComment;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.models.FeedbackDetails;

import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class FeedbacksAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface FeedbackCallback {
        void onComplete(boolean status, int statusCode, String message, FeedbackDetails feedbackDetails);
    }
    public interface FeedbacksCallback {
        void onComplete(boolean status, int statusCode, String message, List<FeedbackDetails> feedbacksList);
    }


    public FeedbacksAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }


    public void getFeedbackDetails(final String feedbackID, final FeedbackCallback callback) {
        retrofitInterface.getFeedbackDetails(deviceID, appVersion, feedbackID, new Callback<FeedbackDetails>() {
            @Override
            public void success(FeedbackDetails feedbackDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ FEEDBACK_DETAILS ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbackDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ FEEDBACK_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ FEEDBACK_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getFeedbacksForDate(final String date, final boolean byOrderDate, final FeedbacksCallback callback) {
        final Calendar calendar = DateMethods.getCalendarFromDate(date);

        retrofitInterface.getFeedbacksForDate(deviceID, appVersion, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR), byOrderDate, new Callback<List<FeedbackDetails>>() {
            @Override
            public void success(List<FeedbackDetails> feedbacksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ FEEDBACKS_FOR_DATE ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbacksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ FEEDBACKS_FOR_DATE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ FEEDBACKS_FOR_DATE ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getLowRatedFeedbacks(final FeedbacksCallback callback) {

        retrofitInterface.getLowRatedFeedbacks(deviceID, appVersion, new Callback<List<FeedbackDetails>>() {
            @Override
            public void success(List<FeedbackDetails> feedbacksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ LOW_RATED_FEEDBACKS ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbacksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ LOW_RATED_FEEDBACKS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ LOW_RATED_FEEDBACKS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getAverageRatedFeedbacks(final FeedbacksCallback callback) {

        retrofitInterface.getAverageRatedFeedbacks(deviceID, appVersion, new Callback<List<FeedbackDetails>>() {
            @Override
            public void success(List<FeedbackDetails> feedbacksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ AVERAGE_RATED_FEEDBACKS ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbacksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ AVERAGE_RATED_FEEDBACKS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ AVERAGE_RATED_FEEDBACKS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getHighRatedFeedbacks(final FeedbacksCallback callback) {

        retrofitInterface.getHighRatedFeedbacks(deviceID, appVersion, new Callback<List<FeedbackDetails>>() {
            @Override
            public void success(List<FeedbackDetails> feedbacksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ HIGH_RATED_FEEDBACKS ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbacksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ HIGH_RATED_FEEDBACKS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ HIGH_RATED_FEEDBACKS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }

    public void getProcessingFeedbacks(final FeedbacksCallback callback) {

        retrofitInterface.getProcessingFeedbacks(deviceID, appVersion, new Callback<List<FeedbackDetails>>() {
            @Override
            public void success(List<FeedbackDetails> feedbacksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ PROCESSING_FEEDBACKS ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbacksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ PROCESSING_FEEDBACKS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ PROCESSING_FEEDBACKS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }

    public void getProcessedFeedbacksForDate(final String date, final FeedbacksCallback callback) {

        Calendar calendar = DateMethods.getCalendarFromDate(date);
        final int day = calendar.get(Calendar.DATE);
        int month = calendar.get(Calendar.MONTH)+1;
        int year = calendar.get(Calendar.YEAR);

        retrofitInterface.getProcessedFeedbacksForDate(deviceID, appVersion, day, month, year, new Callback<List<FeedbackDetails>>() {
            @Override
            public void success(List<FeedbackDetails> feedbacksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ PROCESSED_FEEDBACKS ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbacksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ PROCESSED_FEEDBACKS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ PROCESSED_FEEDBACKS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }


    public void getUnProcessedFeedbacks(final FeedbacksCallback callback) {

        retrofitInterface.getUnProcessedFeedbacks(deviceID, appVersion, new Callback<List<FeedbackDetails>>() {
            @Override
            public void success(List<FeedbackDetails> feedbacksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ UN_PROCESSED_FEEDBACKS ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbacksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ UN_PROCESSED_FEEDBACKS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ UN_PROCESSED_FEEDBACKS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }

    public void getBuyerFeedbacks(final String buyerID, final FeedbacksCallback callback) {

        retrofitInterface.getBuyerFeedbacks(deviceID, appVersion, buyerID, new Callback<List<FeedbackDetails>>() {
            @Override
            public void success(List<FeedbackDetails> feedbacksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ BUYER_FEEDBACKS ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbacksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ BUYER_FEEDBACKS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ BUYER_FEEDBACKS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }

    public void claimFeedback(final String feedbackID, final FeedbacksCallback callback) {

        retrofitInterface.claimFeedback(deviceID, appVersion, feedbackID, new Callback<List<FeedbackDetails>>() {
            @Override
            public void success(List<FeedbackDetails> feedbacksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ CLAIM_FEEDBACK ]  >>  Success");
                callback.onComplete(true, 200, "Success", feedbacksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ CLAIM_FEEDBACK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for fID: " + feedbackID);
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ CLAIM_FEEDBACK ]  >>  Error = " + error.getKind() + " for fID: " + feedbackID);
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }

    public void addFeedbackResolutionComment(final String feedbackID, final String resolutionComment, final StatusCallback callback) {

        retrofitInterface.addFeedbackResolutionComment(deviceID, appVersion, feedbackID, new RequestAddFeedbackResolutionComment(resolutionComment), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ ADD_FEEDBACK_RESOLUTION_COMMENT ]  >>  Success, for fID: " + feedbackID);
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ ADD_FEEDBACK_RESOLUTION_COMMENT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for fID: " + feedbackID);
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ ADD_FEEDBACK_RESOLUTION_COMMENT ]  >>  Error = " + error.getKind() + " for fID: " + feedbackID);
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });

    }

    public void completeFeedback(final String feedbackID, final StatusCallback callback) {

        retrofitInterface.completeFeedback(deviceID, appVersion, feedbackID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ COMPLETE_FEEDBACK ]  >>  Success, for fID: " + feedbackID);
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ COMPLETE_FEEDBACK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for fID: " + feedbackID);
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ COMPLETE_FEEDBACK ]  >>  Error = " + error.getKind() + " for fID: " + feedbackID);
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });

    }

    public void completeAllFeedbacksForOrder(final String orderID, final StatusCallback callback) {

        retrofitInterface.completeAllFeedbacksForOrder(deviceID, appVersion, orderID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ COMPLETE_ALL_FEEDBACK_FOR_ORDER ]  >>  Success, for oID: " + orderID);
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ COMPLETE_ALL_FEEDBACK_FOR_ORDER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for fID: " + orderID);
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ COMPLETE_ALL_FEEDBACK_FOR_ORDER ]  >>  Error = " + error.getKind() + " for fID: " + orderID);
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });

    }

    public void releaseUnprocessedFeedbacks(final StatusCallback callback) {

        retrofitInterface.releaseUnprocessedFeedbacks(deviceID, appVersion, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ RELEASE_UNPROCESSED_FEEDBACKS ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ RELEASE_UNPROCESSED_FEEDBACKS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ RELEASE_UNPROCESSED_FEEDBACKS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void addToTestimonials(final String feedbackID, final StatusCallback callback) {

        retrofitInterface.addToTestimonials(deviceID, appVersion, feedbackID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ ADD_TO_TESTIMONIALS ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ ADD_TO_TESTIMONIALS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ ADD_TO_TESTIMONIALS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void removeFromTestimonials(final String feedbackID, final StatusCallback callback) {

        retrofitInterface.removeFromTestimonials(deviceID, appVersion, feedbackID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ REMOVE_FROM_TESTIMONIALS ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ REMOVE_FROM_TESTIMONIALS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFEEDBACKS_API : [ REMOVE_FROM_TESTIMONIALS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}
