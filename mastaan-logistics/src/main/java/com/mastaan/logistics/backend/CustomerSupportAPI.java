package com.mastaan.logistics.backend;

import android.content.Context;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.backend.models.RequestAddComment;
import com.mastaan.logistics.backend.models.RequestAssignToCustomerService;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class CustomerSupportAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public CustomerSupportAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        this.localStorageData = new LocalStorageData(context);
    }

    public void assignToCustomerSupport(final String orderItemID, final String reason, final StatusCallback callback){

        retrofitInterface.assignToCustomerSupport(deviceID, appVersion, orderItemID, new RequestAssignToCustomerService(reason), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void removeFromCustomerSupport(final String orderItemID, final StatusCallback callback){

        retrofitInterface.removeFromCustomerSupport(deviceID, appVersion, orderItemID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getUnprocessedCustomerSupportItems(final LatLng location, final OrderItemsListCallback callback) {

        retrofitInterface.getUnprocessedCustomerSupportItems(deviceID, appVersion, location.latitude + "," + location.longitude, new Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    callback.onComplete(true, 200, "Success", items);
                } else {
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getCustomerSupportItems(final LatLng location, final OrdersAPI.OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getCustomerSupportItems(deviceID, appVersion, location.latitude + "," + location.longitude, new Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, null, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void claimCustomerSupportItem(final String orderItemID, final OrderItemsListCallback callback){

        retrofitInterface.claimCustomerSupportItem(deviceID, appVersion, orderItemID, new Callback<List<OrderItemDetails>>() {
            @Override
            public void success(List<OrderItemDetails> orderItemsList, Response response) {
                if ( orderItemsList != null ) {
                    callback.onComplete(true, 200, "Success", orderItemsList);
                }else{
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addCommentForCustomerSupport(final String orderItemID, final String comment, final StatusCallback callback){

        retrofitInterface.addCommentForCustomerSupport(deviceID, appVersion, orderItemID, new RequestAddComment(comment), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void releaseAllCustomerSupportItems(final StatusCallback callback){

        retrofitInterface.releaseAllCustomerSupportItems(deviceID, appVersion, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void completeAllCustomerSupportItems(final StatusCallback callback){

        retrofitInterface.completeAllCustomerSupportItems(deviceID, appVersion, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}

