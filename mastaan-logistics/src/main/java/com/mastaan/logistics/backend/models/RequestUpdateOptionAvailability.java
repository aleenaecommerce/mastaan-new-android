package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 22/10/16.
 */
public class RequestUpdateOptionAvailability {
    String d;
    double pd;
    boolean a;
    String ar;

    public RequestUpdateOptionAvailability(String date, double priceDifference, boolean availability, String availabilityReason){
        this.d = date;
        this.pd = priceDifference;
        this.a = availability;
        this.ar = availabilityReason;
    }

    public String getDate() {
        return d;
    }
}
