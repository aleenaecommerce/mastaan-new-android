package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.models.RequestObjectLogin;
import com.mastaan.logistics.backend.models.ResponseObjectLogin;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.backend.methods.CheckError;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class AuthenticationAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface LoginCallback {
        void onComplete(boolean status, int statusCode, String message, String token);//, UserDetails user, ArrayList<Cookies> cookies);
    }

    public AuthenticationAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void login(final String email, final String password, final LoginCallback callback) {

        retrofitInterface.login(deviceID, appVersion, new RequestObjectLogin(email, password), new Callback<ResponseObjectLogin>() {
            @Override
            public void success(ResponseObjectLogin responseObjectLogin, Response response) {
                if (responseObjectLogin != null && responseObjectLogin.getToken() != null) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  Success");
                    callback.onComplete(true, 200, "Success", responseObjectLogin.getToken());//, responseObjectLogin.getUserDetails(), responseObjectLogin.getCookies());
                }else {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  Success");
                    callback.onComplete(false, 500, "Something went wrong, try again!", null);//, null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).ignoreSessionValidity().check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);//, null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGIN ]  >>  ERROR");
                        callback.onComplete(false, -1, "Error", null);//, null, null);
                    }
                }
            }
        });
    }


    public void logout(final StatusCallback callback) {

        retrofitInterface.confirmLogout(deviceID, appVersion, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGOUT ]  >>  Success");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGOUT ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGOUT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nAUTHENTICATION_API : [ LOGOUT ]  >>  ERROR");
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}
