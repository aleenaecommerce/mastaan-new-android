package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 30/06/2018.
 */

public class RequestItemLogs {
    String populate;
    String fromDate;
    String toDate;

    public RequestItemLogs setPopulate(String populate) {
        this.populate = populate;
        return this;
    }

    public RequestItemLogs setFromDate(String fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public RequestItemLogs setToDate(String toDate) {
        this.toDate = toDate;
        return this;
    }

}
