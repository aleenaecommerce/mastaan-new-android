package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.DeliveryFeeDetails;
import com.mastaan.logistics.models.DeliverySlotDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 31/01/18.
 */

public class RequestSlotsConfig {
    int slotCutoffTime;
    double maxDeliveryDistance;
    long maxOrderCount;
    List<String> disabledDaysOfWeek;
    List<String> disabledCategories;
    List<DeliveryFeeDetails> deliveryCharges;

    List<SlotDetails> slots = new ArrayList<>();

    class SlotDetails{
        public int sh;
        public int sm;
        public int eh;
        public int em;

        String mindist;                        // Maximum Distance
        String maxdist;                        // Minimum Distance

        String mnum;                            // Maximum orders count
        String ctime;                          // Cut off time
        List<DeliveryFeeDetails> delfees;   // Delivery Fees
        List<String> dcats;                 //  Disabled Categories
        List<String> dwms;                  // Disabled Warehouse Meat Items
        List<String> ddow;                  // Disabled Days of the week
    }


    public RequestSlotsConfig(int slotCutoffTime, double maxDeliveryDistance, long maxOrderCount, List<String> disabledDaysOfWeek, List<String> disabledCategories, List<DeliveryFeeDetails> deliveryCharges, List<DeliverySlotDetails> slotsList) {
        this.slotCutoffTime = slotCutoffTime;this.slots = new ArrayList<>();
        this.maxDeliveryDistance = maxDeliveryDistance;
        this.maxOrderCount = maxOrderCount;
        this.disabledDaysOfWeek = disabledDaysOfWeek;
        this.disabledCategories = disabledCategories;
        this.deliveryCharges = deliveryCharges;

        if ( slotsList != null ){
            for (int i=0;i<slotsList.size();i++){
                if ( slotsList.get(i) != null ){
                    SlotDetails slotDetails = new SlotDetails();
                    slotDetails.sh = slotsList.get(i).getStartHour();
                    slotDetails.sm = slotsList.get(i).getStartMinute();
                    slotDetails.eh = slotsList.get(i).getEndHour();
                    slotDetails.em = slotsList.get(i).getEndMinute();

                    slotDetails.mindist = slotsList.get(i).getMinimumDistance() + "";
                    slotDetails.maxdist = slotsList.get(i).getMaximumDistance() + "";

                    if ( slotsList.get(i).getMaximumOrders() > 0 ) {
                        slotDetails.mnum = slotsList.get(i).getMaximumOrders()+"";
                    }
                    if ( slotsList.get(i).getCutoffTime() > 0 ) {
                        slotDetails.ctime = slotsList.get(i).getCutoffTime()+"";
                    }
                    slotDetails.delfees = slotsList.get(i).getDeliveryFees();
                    slotDetails.dcats = slotsList.get(i).getDisabledCategories();
                    slotDetails.dwms = slotsList.get(i).getDisabledWarehouseMetatItems();
                    slotDetails.ddow = slotsList.get(i).getDisabledWeekDays();

                    this.slots.add(slotDetails);
                }
            }
        }
    }
}
