package com.mastaan.logistics.backend.models;

import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.models.UserDetailsMini;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 28/07/18.
 */

public class RequestAddOrEditProcessedStock {
    String ty;              // Type : chicken / mutton
    String d;               // Date

    double bpu;              //  Before Processing Units
    double bpw;              //  Before Processing Weight

    double apw;              //  After Processing Weight
    double apdw;             //  After Processing Dressed Weight
    double apsw;             //  After Processing Skinless Weight
    double aplw;             //  After Processing Liver Weight
    double apgw;             //  After Processing Gizzard Weight
    double apbw;             //  After Processing Botee Weight
    long aphp;               //  After processing head pieces
    long apbp;               //  After processing brain pieces
    long apts;               //  After processing trotter sets
    long aptp;               //  After processing tilli pieces

    double su;              //  Spoiled Units
    double sw;              //  Spoiled Weight

    String cmt;             //  Comments

    List<String> prb;       //  Processed By


    public RequestAddOrEditProcessedStock(String type, String date){
        this.ty = type;
        this.d = date;
    }

    public RequestAddOrEditProcessedStock(String type, String date, double beforeProcessingUnits, double beforeProcessingWeight,
        double afterProcessingWeight, double afterProcessingDressedWeight, double afterProcessingSkinlessWeight, double afterProcessingLiverWeight, double afterProcessingGizzardWeight, double afterProcessingBoteeWeight, long afterProcessingHeadPieces, long afterProcessingBrainPieces, long afterProcessingTrotterSets, long afterProcessingTilliPieces,
        double spoiledUnits, double spoiledWeight
    ){
        this.ty = type;
        this.d = date;

        this.bpu = beforeProcessingUnits;
        this.bpw = beforeProcessingWeight;

        this.apw = afterProcessingWeight;
        this.apdw = afterProcessingDressedWeight;
        this.apsw = afterProcessingSkinlessWeight;
        this.aplw = afterProcessingLiverWeight;
        this.apgw = afterProcessingGizzardWeight;
        this.apbw = afterProcessingBoteeWeight;
        this.aphp = afterProcessingHeadPieces;
        this.apbp = afterProcessingBrainPieces;
        this.apts = afterProcessingTrotterSets;
        this.aptp = afterProcessingTilliPieces;

        this.su = spoiledUnits;
        this.sw = spoiledWeight;
    }


    public void setType(String type) {
        this.ty = type;
    }
    public String getType() {
        return ty!=null?ty:"";
    }

    public void setDate(String date) {
        this.d = date;
    }
    public String getDate() {
        return DateMethods.getReadableDateFromUTC(d);
    }


    public void setBeforeProcessingUnits(double beforeProcessingUnits) {
        this.bpu = beforeProcessingUnits;
    }
    public double getBeforeProcessingUnits() {
        return bpu;
    }

    public void setBeforeProcessingWeight(double beforeProcessingWeight) {
        this.bpw = beforeProcessingWeight;
    }
    public double getBeforeProcessingWeight() {
        return bpw;
    }


    public void setAfterProcessingWeight(double afterProcessingWeight) {
        this.apw = afterProcessingWeight;
    }
    public double getAfterProcessingWeight() {
        return apw;
    }

    public void setAfterProcessingDressedWeight(double afterProcessingDressedWeight) {
        this.apdw = afterProcessingDressedWeight;
    }
    public double getAfterProcessingDressedWeight() {
        return apdw;
    }

    public void setAfterProcessingSkinlessWeight(double afterProcessingSkinlessWeight) {
        this.apsw = afterProcessingSkinlessWeight;
    }
    public double getAfterProcessingSkinlessWeight() {
        return apsw;
    }

    public void setAfterProcessingLiverWeight(double afterProcessingLiverWeight) {
        this.aplw = afterProcessingLiverWeight;
    }
    public double getAfterProcessingLiverWeight() {
        return aplw;
    }

    public void setAfterProcessingGizzardWeight(double afterProcessingGizzardWeight) {
        this.apgw = afterProcessingGizzardWeight;
    }
    public double getAfterProcessingGizzardWeight() {
        return apgw;
    }

    public void setAfterProcessingBoteeWeight(double afterProcessingBoteeWeight) {
        this.apbw = afterProcessingBoteeWeight;
    }
    public double getAfterProcessingBoteeWeight() {
        return apbw;
    }

    public void setAfterProcessingHeadPieces(long afterProcessingHeadPieces) {
        this.aphp = afterProcessingHeadPieces;
    }
    public long getAfterProcessingHeadPieces() {
        return aphp;
    }

    public void setAfterProcessingBrainPieces(long afterProcessingBrainPieces) {
        this.apbp = afterProcessingBrainPieces;
    }
    public long getAfterProcessingBrainPieces() {
        return apbp;
    }

    public void setAfterProcessingTrotterSets(long afterProcessingTrotterSets) {
        this.apts = afterProcessingTrotterSets;
    }
    public long getAfterProcessingTrotterSets() {
        return apts;
    }

    public void setAfterProcessingTilliPieces(long afterProcessingTilliPieces) {
        this.aptp = afterProcessingTilliPieces;
    }
    public long getAfterProcessingTilliPieces() {
        return aptp;
    }


    public void setSpoiledUnits(double spoiledUnits) {
        this.su = spoiledUnits;
    }
    public double getSpoiledUnits() {
        return su;
    }

    public void setSpoiledWeight(double spoiledWeight) {
        this.sw = spoiledWeight;
    }
    public double getSpoiledWeight() {
        return sw;
    }

    public void setComments(String comments) {
        this.cmt = comments;
    }

    public void setProcessedBy(List<UserDetailsMini> processedBy) {
        this.prb = new ArrayList<>();
        if ( processedBy != null && processedBy.size() > 0 ){
            for (int i=0;i<processedBy.size();i++){
                this.prb.add(processedBy.get(i).getID());
            }
        }
    }

}
