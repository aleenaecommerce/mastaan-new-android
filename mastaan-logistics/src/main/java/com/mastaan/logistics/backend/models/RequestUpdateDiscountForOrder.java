package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.DiscountDetails;

/**
 * Created by venkatesh on 11/6/16.
 */
public class RequestUpdateDiscountForOrder {
    double amt;
    String rid;
    String r;
    String c;
    String i;

    public RequestUpdateDiscountForOrder(DiscountDetails discountDetails, double discountAmount, String discountComment, String orderItemID){
        this.amt = discountAmount;
        this.rid = discountDetails.getID();
        this.r = discountDetails.getReason();
        this.c = discountComment;
        this.i = orderItemID;
    }
}
