package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAddEmployeeViolation;
import com.mastaan.logistics.backend.models.RequestChangeWarehouse;
import com.mastaan.logistics.backend.models.RequestSendMessage;
import com.mastaan.logistics.backend.models.ResponseFirebaseToken;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.Bootstrap;
import com.mastaan.logistics.models.Vendor;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class CommonAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface BootstrapCallback {
        void onComplete(boolean status, int statusCode, String message, Bootstrap bootstrap);
    }

    public interface FirebaseTokenCallback{
        void onComplete(boolean status, int statusCode, String firebaseToken);
    }

    public interface VendorsCallback {
        void onComplete(boolean status, int statusCode, String message, List<Vendor> vendorsList);
    }

    public CommonAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getBootStrap(final BootstrapCallback callback) {

        retrofitInterface.getBootstrap(deviceID, appVersion, new Callback<Bootstrap>() {
            @Override
            public void success(Bootstrap bootstrap, Response response) {
                Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ GET_BOOTSTRAP ]  >>  Success");
                callback.onComplete(true, 200, "Success", bootstrap);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).ignoreMaintenance().ignoreSessionValidity().check(error) == false ) {
                    String message = "Error";
                    int statusCode = -1;
                    try { statusCode = error.getResponse().getStatus(); }catch (Exception e){}
                    try{
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);
                        if (jsonResponse.getBoolean("maintenance")) {
                            message = jsonResponse.getString("message");
                        }
                    } catch (Exception e) {}

                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ BOOTSTRAP ]  >>  Error = " + error.getKind() + " , StatusCode = " + statusCode);
                    callback.onComplete(false, statusCode, message, null);

//                    try {
//                        Log.d(Constants.LOG_TAG, "\nBOOTSTRAP_API : [ GET_BOOTSTRAP ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
//                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
//                    } catch (Exception e) {
//                        callback.onComplete(false, -1, "Error", null);
//                    }
                }
            }
        });
    }

    public void changeWarehouse(String warehouseID, final StatusCallback callback){
        retrofitInterface.changeWarehouse(deviceID, appVersion, new RequestChangeWarehouse(warehouseID), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseObject, Response response) {

                if (responseObject != null && responseObject.getCode() != null && responseObject.getCode().equalsIgnoreCase("OK")) {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ CHANGE_WAREHOUSE ]  >>  Success");
                    callback.onComplete(true, 200, "Success");
                } else {
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API : [ CHANGE_WAREHOUSE ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API:  [ CHANGE_WAREHOUSE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API:  [ CHANGE_WAREHOUSE ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getFirebaseToken(final FirebaseTokenCallback callback){

        retrofitInterface.getFireBaseToken(deviceID, appVersion, new Callback<ResponseFirebaseToken>() {
            @Override
            public void success(ResponseFirebaseToken responseFirebase, Response response) {
                if (responseFirebase != null && responseFirebase.getFirebaseToken() != null && responseFirebase.getFirebaseToken().length() > 0) {
                    callback.onComplete(true, 200, responseFirebase.getFirebaseToken());
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API:  GetToken : Success , Token = " + responseFirebase.getFirebaseToken());
                } else {
                    callback.onComplete(false, 500, null);
                    Log.d(Constants.LOG_TAG, "\nCOMMON_API:  GetToken : Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API:  GetToken : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API:  GetToken : Error = " + error.getKind());
                        callback.onComplete(false, -1, null);
                    }
                }
            }
        });
    }

    public void getVendorsForCategory(final String categoryID, final VendorsCallback callback) {

        retrofitInterface.getVendorsForCategory(deviceID, appVersion, categoryID, new Callback<List<Vendor>>() {
            @Override
            public void success(List<Vendor> vendorsList, Response response) {
                callback.onComplete(true, 200, "Success", vendorsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getLivestockVendorsForCategory(final String categoryID, final VendorsCallback callback) {

        retrofitInterface.getLivestockVendorsForCategory(deviceID, appVersion, categoryID, new Callback<List<Vendor>>() {
            @Override
            public void success(List<Vendor> vendorsList, Response response) {
                callback.onComplete(true, 200, "Success", vendorsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addEmployeeViolation(final String employeeID, final RequestAddEmployeeViolation requestAddEmployeeViolation, final StatusCallback callback) {

        retrofitInterface.addEmployeeViolation(deviceID, appVersion, employeeID, requestAddEmployeeViolation, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void sendMessage(final RequestSendMessage requestSendMessage, final StatusCallback callback) {

        retrofitInterface.sendMessage(deviceID, appVersion, requestSendMessage, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        String message = "Error";
                        try {
                            message = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())).getString("msg");
                        } catch (Exception e) {}

                        callback.onComplete(false, error.getResponse().getStatus(), message);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}
