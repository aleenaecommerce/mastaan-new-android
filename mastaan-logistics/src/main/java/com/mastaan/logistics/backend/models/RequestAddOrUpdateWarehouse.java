package com.mastaan.logistics.backend.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Venkatesh Uppu on 13/04/19.
 */

public class RequestAddOrUpdateWarehouse {
    String en;
    String n;
    Double []loc;
    String fa;

    public RequestAddOrUpdateWarehouse(String name, LatLng latLng, String fullAddress){
        this.en = true+"";
        this.n = name;
        this.loc = new Double[]{latLng.longitude, latLng.latitude};
        this.fa = fullAddress;
    }
    public RequestAddOrUpdateWarehouse(boolean enable){
        this.en = enable+"";
    }
    public RequestAddOrUpdateWarehouse(String name) {
        this.n = name;
    }


    public String getName() {
        return n;
    }

    public String getFullAddress() {
        return fa;
    }

    public LatLng getLocation() {
        return loc!=null&&loc.length==2?new LatLng(loc[1], loc[0]):new LatLng(0, 0);
    }

}
