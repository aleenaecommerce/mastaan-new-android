package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */
public class RequestCreateCounterSaleOrder {
    String wmi;
    double q;
    double pr;
    boolean mc;
    String emp;
    String n;
    String si;

    public RequestCreateCounterSaleOrder(String warehouseMeatItemID, double quantity, double price, boolean isAmountCollected, String employeeID, String customerName, String comments){
        this.wmi = warehouseMeatItemID;
        this.q = quantity;
        this.pr = price;
        this.mc = isAmountCollected;
        this.emp = employeeID;
        this.n = customerName;
        this.si = comments;
    }


}
