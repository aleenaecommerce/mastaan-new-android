package com.mastaan.logistics.backend.models;

import com.aleena.common.methods.CommonMethods;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 23/10/16.
 */
public class ResponseAffectedMeatItems {
    List<Item> items;

    class Item{
        String i;
    }

    public String getAffectedMeatItems(){
        if ( items != null && items.size() > 0 ){
            String affectedMeatItemsList = "";
            for (int i=0;i<items.size();i++){
                if ( affectedMeatItemsList.length() > 0 ){  affectedMeatItemsList += "<br>";  }
                affectedMeatItemsList += "<b>"+ CommonMethods.capitalizeFirstLetter(items.get(i).i)+"</b>";
            }
            return affectedMeatItemsList;
        }
        return "";
    }
}
