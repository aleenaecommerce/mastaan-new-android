package com.mastaan.logistics.backend.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 23/09/18.
 */

public class RequestAddOrUpdateHousingSociety {
    String n;
    String pos;

    public RequestAddOrUpdateHousingSociety(String name, LatLng latLng) {
        this.n = name;
        this.pos = latLng.latitude+","+latLng.longitude;
    }
}
