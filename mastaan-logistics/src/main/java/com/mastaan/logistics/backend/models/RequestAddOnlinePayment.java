package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public class RequestAddOnlinePayment {
    double amt;
    String tid;
    String pid;

    public RequestAddOnlinePayment(double amount, String transactionID, String paymentID){
        this.amt = amount;
        this.tid = transactionID;
        this.pid = paymentID;
    }

}
