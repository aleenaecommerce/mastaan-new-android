package com.mastaan.logistics.backend.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 9/9/16.
 */
public class RequestManageDayAvailability {
    boolean en;
    String msg;
    List<String> c;

    public RequestManageDayAvailability(){}

    public RequestManageDayAvailability(boolean isEnabled, List<String> categories, String message){
        this.en = isEnabled;
        this.msg = message;
        this.c = categories;
    }

    public boolean isEnabled() {
        return en;
    }

    public String getMessage() {
        return msg;
    }

    public List<String> getCategories() {
        if ( c != null ){   return c;   }
        return new ArrayList<>();
    }
}
