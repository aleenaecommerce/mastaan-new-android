package com.mastaan.logistics.backend;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestSlotsConfig;
import com.mastaan.logistics.backend.models.ResponseSlotsConfig;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.DeliveryFeeDetails;
import com.mastaan.logistics.models.DeliverySlotDetails;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 27/01/18.
 */

public class SlotsAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface SlotsConfigCallback {
        void onComplete(boolean status, int statusCode, String message, int slotCutoffTime, double maxDeliveryDistance, long maxOrderCount, List<String> disabledDaysOfWeek, List<String> disabledCategories, List<DeliveryFeeDetails> deliveryCharges, List<DeliverySlotDetails> slotsList);
    }


    public SlotsAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getSlotsConfig(final SlotsConfigCallback callback){

        retrofitInterface.getSlotsConfig(deviceID, appVersion, new Callback<ResponseSlotsConfig>() {
            @Override
            public void success(final ResponseSlotsConfig responseSlotsConfig, Response response) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        Collections.sort(responseSlotsConfig.getSlots(), new Comparator<DeliverySlotDetails>() {
                            public int compare(DeliverySlotDetails item1, DeliverySlotDetails item2) {
                                return DateMethods.compareDates(item1.getStartTime(), item2.getStartTime());
                            }
                        });
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "\nSLOTS_API : [ DELIVERY_SLOTS ]  >>  Success");
                        callback.onComplete(true, 200, "Success", responseSlotsConfig.getSlotCutoffTime(), responseSlotsConfig.getMaxDeliveryDistance(), responseSlotsConfig.getMaxOrderCount(), responseSlotsConfig.getDisabledDaysOfWeek(), responseSlotsConfig.getDisabledCategories(), responseSlotsConfig.getDeliveryCharges(), responseSlotsConfig.getSlots());
                    }
                }.execute();
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSLOTS_API : [ DELIVERY_SLOTS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), 0, 0, 0, null, null, null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSLOTS_API : [ DELIVERY_SLOTS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", 0, 0, 0, null, null, null, null);
                    }
                }
            }
        });
    }

    public void updateSlotsConfig(final int cutoffTime, final double maxDeliveryDistance, final long maxOrderCount, final List<String> disabledDaysOfWeek, final List<String> disabledCategories, final List<DeliveryFeeDetails> deliveryCharges, final List<DeliverySlotDetails> slotsList, final SlotsConfigCallback callback){

        retrofitInterface.updateSlotsConfig(deviceID, appVersion, new RequestSlotsConfig(cutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, deliveryCharges, slotsList), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nSLOTS_API : [ UPDATE_DELIVERY_SLOTS ]  >>  Success");
                callback.onComplete(true, 200, "Success", cutoffTime, maxDeliveryDistance, maxOrderCount, disabledDaysOfWeek, disabledCategories, deliveryCharges, slotsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSLOTS_API : [ UPDATE_DELIVERY_SLOTS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), 0, 0, 0, null, null, null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSLOTS_API : [ UPDATE_DELIVERY_SLOTS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", 0, 0, 0, null, null, null, null);
                    }
                }
            }
        });
    }

}
