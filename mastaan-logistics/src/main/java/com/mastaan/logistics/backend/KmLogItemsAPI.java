package com.mastaan.logistics.backend;

import android.content.Context;

import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.models.KmLogItem;
import com.mastaan.logistics.models.UserDay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class KmLogItemsAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface Callback {
        void onComplete(ArrayList<UserDay> details);
    }

    public interface KmLogItemListCallback {
        void onComplete(boolean status, Map<Integer, List<KmLogItem>> kmLogMap, int status_code);
    }

    public KmLogItemsAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getKmLogItems(int month, int year, final KmLogItemListCallback callback) {

        retrofitInterface.getKmLogTripList(deviceID, appVersion, month, year, new HashMap<String, Object>(), new retrofit.Callback<List<KmLogItem>>() {
            @Override
            public void success(List<KmLogItem> kmLogItems, Response response) {
                if (kmLogItems != null && kmLogItems.size() > 0) {
                    Map<Integer, List<KmLogItem>> kmLogMap = new HashMap<>();
                    for (KmLogItem item : kmLogItems) {
                        int year = DateMethods.getYearFromDate(item.getTripDate());
                        int month = DateMethods.getMonthFromDate(item.getTripDate());
                        int day = DateMethods.getDayFromDate(item.getTripDate());
                        Integer key = year * 10000 + month * 100 + day;
                        if (!kmLogMap.containsKey(key)) {
                            kmLogMap.put(key, new ArrayList<KmLogItem>());
                            kmLogMap.get(key).add(item);
                        } else
                            kmLogMap.get(key).add(item);
                    }
                    callback.onComplete(true, kmLogMap, response.getStatus());
                } else if (kmLogItems != null && kmLogItems.size() <= 0)
                    callback.onComplete(false, null, Constants.code.response.NO_DATA);
                else callback.onComplete(false, null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, null, error.getResponse().getStatus());
                    } catch (Exception e) {
                        callback.onComplete(false, null, Constants.code.response.UN_KNOWN);
                    }
                }
            }
        });
    }
}
