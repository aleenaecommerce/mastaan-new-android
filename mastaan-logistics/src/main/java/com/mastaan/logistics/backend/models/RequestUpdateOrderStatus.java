package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 12/1/17.
 */
public class RequestUpdateOrderStatus {
    String status;
    double moneycollected;
    String paymentType;
    String paymentid;

    public RequestUpdateOrderStatus(String status){
        this.status = status;
    }
    public RequestUpdateOrderStatus(String status, double moneycollected, String paymentType, String paymentid){
        this.status = status;
        this.moneycollected = moneycollected;
        this.paymentType = paymentType;
        this.paymentid = paymentid;
    }

    public String getStatus() {
        return status;
    }
}
