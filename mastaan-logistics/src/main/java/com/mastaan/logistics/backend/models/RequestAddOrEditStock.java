package com.mastaan.logistics.backend.models;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.UserDetailsMini;
import com.mastaan.logistics.models.Vendor;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */
public class RequestAddOrEditStock {
    String ty;
    String c;
    String wmi;
    List<String> ilao;
    String mi;
    String d;
    double qu;
    double wa;
    long num;
    double vr;
    double pco;
    double sco;
    double oco;
    double fr;
    double ma;
    String v;
    String so;
    String cmt;

    String hu;
    String sto;
    String psto;

    String thu;
    String cemp;

    List<String> prb;
    String qcb;


    public RequestAddOrEditStock(String stockType, String date, CategoryDetails categoryDetails, Vendor vendor, String totalQuantity, String totalWastageQuantity, String totalNoOfUnits, String vendorRate, String packagingCost, String shippingCost, String otherCost, String farmRate, String margin, String source, String comments){
        setStockType(stockType);
        this.d = date;
        this.c = categoryDetails.getID();
        if ( vendor != null ){   this.v = vendor.getID();    }
        if ( totalQuantity.length() > 0 ){  this.qu = Double.parseDouble(totalQuantity);    }
        if ( totalWastageQuantity.length() > 0 ){  this.wa = Double.parseDouble(totalWastageQuantity); }
        if ( totalNoOfUnits.length() > 0 ){  this.num = Long.parseLong(totalNoOfUnits);  }
        if ( vendorRate.length() > 0 ){  this.vr = Double.parseDouble(vendorRate);    }
        if ( packagingCost.length() > 0 ){  this.pco = Double.parseDouble(packagingCost);   }
        if ( shippingCost.length() > 0 ){  this.sco = Double.parseDouble(shippingCost);   }
        if ( otherCost.length() > 0 ){  this.oco = Double.parseDouble(otherCost);   }
        if ( farmRate.length() > 0 ){  this.fr = Double.parseDouble(farmRate); }
        if ( margin.length() > 0 ){ this.ma = Double.parseDouble(margin);   }
        this.so = source;
        this.cmt = comments;
    }

    public RequestAddOrEditStock(String stockType, String date, WarehouseMeatItemDetails warehouseMeatItemDetails, List<AttributeOptionDetails> attributeOptions, Vendor vendor, String totalQuantity, String totalWastageQuantity, String totalNoOfUnits, String vendorRate, String packagingCost, String shippingCost, String otherCost, String farmRate, String margin, String source, String comments){
        setStockType(stockType);
        this.d = date;
        this.wmi = warehouseMeatItemDetails.getID();
        this.mi = warehouseMeatItemDetails.getMeatItemID();
        if ( attributeOptions != null && attributeOptions.size() > 0 ){
            this.ilao = new ArrayList<>();
            for (int i=0;i<attributeOptions.size();i++){
                this.ilao.add(attributeOptions.get(i).getID());
            }
        }
        if ( vendor != null ){   this.v = vendor.getID();    }
        if ( totalQuantity.length() > 0 ){  this.qu = Double.parseDouble(totalQuantity);    }
        if ( totalWastageQuantity.length() > 0 ){  this.wa = Double.parseDouble(totalWastageQuantity); }
        if ( totalNoOfUnits.length() > 0 ){  this.num = Long.parseLong(totalNoOfUnits);  }
        if ( vendorRate.length() > 0 ){  this.vr = Double.parseDouble(vendorRate);    }
        if ( packagingCost.length() > 0 ){  this.pco = Double.parseDouble(packagingCost);   }
        if ( shippingCost.length() > 0 ){  this.sco = Double.parseDouble(shippingCost);   }
        if ( otherCost.length() > 0 ){  this.oco = Double.parseDouble(otherCost);   }
        if ( farmRate.length() > 0 ){  this.fr = Double.parseDouble(farmRate); }
        if ( margin.length() > 0 ){ this.ma = Double.parseDouble(margin);   }
        this.so = source;
        this.cmt = comments;
    }

    private void setStockType(String stockType){
        if (stockType.equalsIgnoreCase(Constants.CATEGORY_PURCHASE)){
            this.ty = "cp";
        }else if (stockType.equalsIgnoreCase(Constants.CATEGORY_CONSOLIDATION)){
            this.ty = "cc";
        }else if (stockType.equalsIgnoreCase(Constants.CATEGORY_ADJUSTMENT)){
            this.ty = "ca";
        }else if (stockType.equalsIgnoreCase(Constants.MEAT_ITEM_INPUT)){
            this.ty = "mi";
        }else if (stockType.equalsIgnoreCase(Constants.MEAT_ITEM_PURCHASE)){
            this.ty = "mp";
        }else if (stockType.equalsIgnoreCase(Constants.MEAT_ITEM_CONSOLIDATION)){
            this.ty = "mc";
        }else if (stockType.equalsIgnoreCase(Constants.MEAT_ITEM_ADJUSTMENT)){
            this.ty = "ma";
        }else if (stockType.equalsIgnoreCase(Constants.MEAT_ITEM_ORDER)){
            this.ty = "mo";
        }else if (stockType.equalsIgnoreCase(Constants.MEAT_ITEM_WASTAGE)){
            this.ty = "mw";
        }else if (stockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE)){
            this.ty = "hmti";
        }else if (stockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB)){
            this.ty = "hmto";
        }else if (stockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_ORDER)){
            this.ty = "hmo";
        }else if (stockType.equalsIgnoreCase(Constants.HUB_MEAT_ITEM_WASTAGE)){
            this.ty = "hmw";
        }else{
            this.ty = stockType;
        }
    }

    public String getTypeName() {
        if ( ty != null && ty.length() > 0 ){
            if (ty.equalsIgnoreCase("cp")){
                return Constants.CATEGORY_PURCHASE;
            }else if (ty.equalsIgnoreCase("cc")){
                return Constants.CATEGORY_CONSOLIDATION;
            }else if (ty.equalsIgnoreCase("ca")){
                return Constants.CATEGORY_ADJUSTMENT;
            }else if (ty.equalsIgnoreCase("mi")){
                return Constants.MEAT_ITEM_INPUT;
            }else if (ty.equalsIgnoreCase("mp")){
                return Constants.MEAT_ITEM_PURCHASE;
            }else if (ty.equalsIgnoreCase("mc")){
                return Constants.MEAT_ITEM_CONSOLIDATION;
            }else if (ty.equalsIgnoreCase("ma")){
                return Constants.MEAT_ITEM_ADJUSTMENT;
            }else if (ty.equalsIgnoreCase("mo") ){
                return Constants.MEAT_ITEM_ORDER;
            }else if (ty.equalsIgnoreCase("mw") ){
                return Constants.MEAT_ITEM_WASTAGE;
            }else if (ty.equalsIgnoreCase("hmti") ){
                return Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE;
            }else if (ty.equalsIgnoreCase("hmto") ){
                return Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB;
            }else if (ty.equalsIgnoreCase("hmo") ){
                return Constants.HUB_MEAT_ITEM_ORDER;
            }else if (ty.equalsIgnoreCase("hmw") ){
                return Constants.HUB_MEAT_ITEM_WASTAGE;
            }
        }
        return ty!=null?ty:"";
    }

    public String getDate() {
        return d;
    }

    public long getNoOfUnits() {
        return num;
    }

    public double getQuantity() {
        return qu;
    }

    public double getWastageQuantity() {
        return wa;
    }


    public double getPackagingCost() {
        return pco;
    }

    public double getShippingCost() {
        return sco;
    }

    public double getOtherCost() {
        return oco;
    }

    public double getVendorRate() {
        return vr;
    }

    public double getFarmRate() {
        return fr;
    }

    public String getWarehouseMeatItemID() {
        return wmi;
    }

    public double getMargin() {
        return ma;
    }

    public String getSource() {
        return so;
    }

    public String getComments() {
         return cmt;
    }

    public RequestAddOrEditStock setHub(String hub) {
        this.hu = hub;
        return this;
    }
    public String getHub() {
        return hu;
    }

    public RequestAddOrEditStock setStock(String stock) {
        this.sto = stock;
        return this;
    }
    public String getStock() {
        return sto;
    }

    public RequestAddOrEditStock setProcessedStock(String processedStock) {
        this.psto = processedStock;
        return this;
    }
    public String getProcessedStock() {
        return psto;
    }

    public RequestAddOrEditStock setToHub(String toHub) {
        this.thu = toHub;
        return this;
    }
    public String getToHub() {
        return thu;
    }

    public RequestAddOrEditStock setCollectedBy(String collectedBy) {
        this.cemp = collectedBy;
        return this;
    }
    public String getCollectedBy() {
        return cemp;
    }

    public RequestAddOrEditStock setProcessedBy(List<UserDetailsMini> processedBy) {
        this.prb = new ArrayList<>();
        if ( processedBy != null && processedBy.size() > 0 ){
            for (int i=0;i<processedBy.size();i++){
                this.prb.add(processedBy.get(i).getID());
            }
        }
        return this;
    }

    public RequestAddOrEditStock setQualityCheckBy(UserDetailsMini qualityCheckBy) {
        this.qcb = qualityCheckBy!=null?qualityCheckBy.getID():null;
        return this;
    }

    public String getMarginDetails(){
        double totalCost = (getQuantity()*getVendorRate())+getPackagingCost()+getShippingCost()+getOtherCost();
        String vendorDetails = "";
        if ( getVendorRate() > 0 ){
            vendorDetails += "Vendor: "+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getVendorRate()*getQuantity())+" , "+SpecialCharacters.RS +" "+CommonMethods.getIndianFormatNumber(getVendorRate())+"/kg/unit/piece";
        }
        String totalDetails = "Final Total: " + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(totalCost);
        if ( getQuantity() > 0 ) {
            totalDetails += ", " + CommonMethods.getIndianFormatNumber(totalCost / (getQuantity()-getWastageQuantity())) + "/kg/unit/piece";
        }
        String marginDetails = "";
        if ( getMargin() > 0 && getQuantity() > 0 ) {
            double finalCost = totalCost / (getQuantity()-getWastageQuantity());
            long marginalCost = Math.round(finalCost + (finalCost*(getMargin()/100)));
            marginDetails += "Margin ("+ CommonMethods.getInDecimalFormat(getMargin())+"%): "+ SpecialCharacters.RS +" "+CommonMethods.getIndianFormatNumber(marginalCost)+"/kg/unit/piece";
        }
        String details = vendorDetails;
        if ( details.length() > 0 ){    details += "\n";    }
        details += totalDetails;
        if ( details.length() > 0 ){    details += "\n";    }
        details += marginDetails;

        return details;
    }
}
