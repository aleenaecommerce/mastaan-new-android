package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAddFollowup;
import com.mastaan.logistics.backend.models.RequestFromDateToDate;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.FollowupDetails;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 09/02/18.
 */

public class FollowupsAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface FollowupDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, FollowupDetails followupDetails);
    }

    public interface FollowupsListCallback {
        void onComplete(boolean status, int statusCode, String message, List<FollowupDetails> followupsList);
    }


    public FollowupsAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }


    public void claimBuyerFollowup(final String buyerID, final StatusCallback callback) {

        retrofitInterface.claimBuyerFollowup(deviceID, appVersion, buyerID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ CLAIM_BUYER_FOLLOWUP ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        String message = "Error";
                        try {
                            message = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())).getString("msg");
                        } catch (Exception e) {}

                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ CLAIM_BUYER_FOLLOWUP ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), message);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ CLAIM_BUYER_FOLLOWUP ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void releaseBuyerFollowup(final String buyerID, final StatusCallback callback) {

        retrofitInterface.releaseBuyerFollowup(deviceID, appVersion, buyerID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ RELEASE_BUYER_FOLLOWUP ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        String message = "Error";
                        try {
                            message = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())).getString("msg");
                        } catch (Exception e) {}

                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ RELEASE_BUYER_FOLLOWUP ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), message);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ RELEASE_BUYER_FOLLOWUP ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void addFollowup(final RequestAddFollowup requestAddFollowup, final FollowupDetailsCallback callback) {

        retrofitInterface.addFollowup(deviceID, appVersion, requestAddFollowup, new Callback<FollowupDetails>() {
            @Override
            public void success(FollowupDetails followupDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ ADD_FOLLOWUP ]  >>  Success");
                callback.onComplete(true, 200, "Success", followupDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ ADD_FOLLOWUP ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ ADD_FOLLOWUP ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getFollowupDetails(final String followupID, final FollowupDetailsCallback callback) {

        retrofitInterface.getFollowupDetails(deviceID, appVersion, followupID, new Callback<FollowupDetails>() {
            @Override
            public void success(FollowupDetails followupDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ FOLLOWUP_DETAILS ]  >>  Success");
                callback.onComplete(true, 200, "Success", followupDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ FOLLOWUP_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ FOLLOWUP_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }

    public void getFollowups(final String fromDate, final String toDate, final FollowupsListCallback callback) {
        getFollowups(fromDate, toDate, null, null, callback);
    }
    public void getFollowups(final String fromDate, final String toDate, final String onlyOrderResulted, final String onlyFirstOrderFollowups, final FollowupsListCallback callback) {

        retrofitInterface.getFollowups(deviceID, appVersion, onlyOrderResulted, onlyFirstOrderFollowups, new RequestFromDateToDate(fromDate, toDate), new Callback<List<FollowupDetails>>() {
            @Override
            public void success(List<FollowupDetails> followupsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ FOLLOWUPS ]  >>  Success");
                callback.onComplete(true, 200, "Success", followupsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ FOLLOWUPS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ FOLLOWUPS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }

    public void getBuyerFollowups(final String buyerID, final FollowupsListCallback callback) {

        retrofitInterface.getBuyerFollowups(deviceID, appVersion, buyerID, new Callback<List<FollowupDetails>>() {
            @Override
            public void success(List<FollowupDetails> conversationsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ BUYER_FOLLOWUPS ]  >>  Success");
                callback.onComplete(true, 200, "Success", conversationsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ BUYER_FOLLOWUPS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nFOLLOWUPS_API : [ BUYER_FOLLOWUPS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    /*
    List<FollowupDetails> list = new ArrayList<>();
        list.add(new FollowupDetails("1", DateMethods.getCurrentDate(), new BuyerDetails("1", "Buyer-1"), new UserDetailsMini("2", "Employee-1"), "Followup details-1"));
        list.add(new FollowupDetails("1", DateMethods.getCurrentDate(), new BuyerDetails("1", "Buyer-2"), new UserDetailsMini("2", "Employee-2"), "Followup details-2"));
        list.add(new FollowupDetails("1", DateMethods.getCurrentDate(), new BuyerDetails("1", "Buyer-3"), new UserDetailsMini("2", "Employee-3"), "Followup details-3"));

        callback.onComplete(true, 200, "Success", null, list);
     */

}
