package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.mastaan.logistics.backend.models.RequestRazorpayRefundAmount;
import com.mastaan.logistics.backend.models.ResponseRazorpayRefundAmount;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.PaymentDetails;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 21/03/18.
 */

public class RazorpayAPI {

    Context context;
    String deviceID;
    int appVersion;

    //RestAdapter restAdapter;
    RetrofitInterface retrofitInterface;

    public interface RefundCallback {
        void onComplete(boolean status, int statusCode, String message, String refundID);
    }

    public RazorpayAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public com.mastaan.logistics.models.Response<String> refundAmount(final String paymentID, final double amount){
        try {
            ResponseRazorpayRefundAmount responseRefundAmount = retrofitInterface.refundAmount(paymentID, new RequestRazorpayRefundAmount((long)amount*100));
            Log.d(Constants.LOG_TAG, "{RAZORPAY_API}, REFUND_AMOUNT, Success");
            return new com.mastaan.logistics.models.Response<String>(true, 200, "Success", responseRefundAmount.getId());
        }
        catch (RetrofitError error){
            int code = -1;
            String message = "Error";
            try {
                code = error.getResponse().getStatus();
                ResponseRazorpayRefundAmount responseRefundAmount = new Gson().fromJson(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()), ResponseRazorpayRefundAmount.class);
                message = responseRefundAmount.getErrorDescription();
            }catch (Exception e){}
            Log.d(Constants.LOG_TAG, "{RAZORPAY_API}, PAYMENT_DETAILS, Error: {c:"+code+", m:"+message+"}");
            return new com.mastaan.logistics.models.Response<String>(false, code, message);
        }
    }

    public com.mastaan.logistics.models.Response<PaymentDetails> getPaymentDetails(String paymentID){
        try {
            PaymentDetails paymentDetails = retrofitInterface.getPaymentDetails(paymentID);
            Log.d(Constants.LOG_TAG, "{RAZORPAY_API}, PAYMENT_DETAILS, Success");
            return new com.mastaan.logistics.models.Response<PaymentDetails>(true, 200, "Success", paymentDetails);
        }
        catch (RetrofitError error){
            Log.d(Constants.LOG_TAG, "{RAZORPAY_API}, PAYMENT_DETAILS, Error");
            return new com.mastaan.logistics.models.Response<PaymentDetails>(false, error!=null&&error.getResponse()!=null?error.getResponse().getStatus():-1, "Error");
        }
    }

}


/*public RazorpayAPI(final Context context) {
        this.context = context;

        restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.razorpay.com/v1")
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Authorization", "Basic " + Base64.encodeToString((context.getString(R.string.razorpay_api_key)+":"+context.getString(R.string.razorpay_api_secret)).getBytes(), Base64.NO_WRAP));
                    }
                })
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public com.mastaan.logistics.models.Response<String> refundAmount(final String paymentID, final double amount){
        try {
            ResponseRazorpayRefundAmount responseRefundAmount = retrofitInterface.refundAmount(paymentID, new RequestRazorpayRefundAmount((long)amount*100));
            Log.d(Constants.LOG_TAG, "{RAZORPAY_API}, REFUND_AMOUNT, Success");
            return new com.mastaan.logistics.models.Response<String>(true, 200, "Success", responseRefundAmount.getId());
        }
        catch (RetrofitError error){
            int code = -1;
            String message = "Error";
            try {
                code = error.getResponse().getStatus();
                ResponseRazorpayRefundAmount responseRefundAmount = new Gson().fromJson(new String(((TypedByteArray) error.getResponse().getBody()).getBytes()), ResponseRazorpayRefundAmount.class);
                message = responseRefundAmount.getErrorDescription();
            }catch (Exception e){}
            Log.d(Constants.LOG_TAG, "{RAZORPAY_API}, PAYMENT_DETAILS, Error: {c:"+code+", m:"+message+"}");
            return new com.mastaan.logistics.models.Response<String>(false, code, message);
        }
    }

    public com.mastaan.logistics.models.Response<PaymentDetails> getPaymentDetails(String paymentID){
        try {
            PaymentDetails paymentDetails = retrofitInterface.getPaymentDetails(paymentID);
            Log.d(Constants.LOG_TAG, "{RAZORPAY_API}, PAYMENT_DETAILS, Success");
            return new com.mastaan.logistics.models.Response<PaymentDetails>(true, 200, "Success", paymentDetails);
        }
        catch (RetrofitError error){
            Log.d(Constants.LOG_TAG, "{RAZORPAY_API}, PAYMENT_DETAILS, Error");
            return new com.mastaan.logistics.models.Response<PaymentDetails>(false, error!=null&&error.getResponse()!=null?error.getResponse().getStatus():-1, "Error");
        }
    }

    //------

    private interface RetrofitInterface{
        //@POST("/payments/{payment_id}/refund")
        //void refundAmount(@Path("payment_id") String paymentID, @Body RequestRefundAmount requestRefundAmount, Callback<ResponseRefundAmount> callback);

        @POST("/payments/{payment_id}/refund")
        ResponseRazorpayRefundAmount refundAmount(@Path("payment_id") String paymentID, @Body RequestRazorpayRefundAmount requestRazorpayRefundAmount);

        @GET("/payments/{payment_id}")
        PaymentDetails getPaymentDetails(@Path("payment_id") String paymentID);
    }*/
