package com.mastaan.logistics.backend.models;

/**
 * Created by venkatesh on 1/5/16.
 */
public class RequestUpdateMeatItem {
    double mw;
    double pt;
    boolean po;
    String pot;
    boolean adn;
    String size;

    public RequestUpdateMeatItem(double minimumWeight, double preparationTime, boolean isPreorderable, String preorderCutoffTime, boolean isAddon, String size){
        this.mw = minimumWeight;
        this.pt = preparationTime;
        this.po = isPreorderable;
        this.pot = preorderCutoffTime;
        this.adn = isAddon;
        this.size = size;
    }

    public double getMinimumWeight() {
        return mw;
    }

    public double getPreparationTime() {
        return pt;
    }

    public boolean isPreOrderable() {
        return po;
    }

    public String getPreOrderCutoffTime() {
        return pot;
    }

    public boolean isAddon() {
        return adn;
    }

    public String getSize() {
        return size;
    }

}
