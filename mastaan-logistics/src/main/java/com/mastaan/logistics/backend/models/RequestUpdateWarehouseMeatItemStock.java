package com.mastaan.logistics.backend.models;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 20/07/18.
 */

public class RequestUpdateWarehouseMeatItemStock {
    String ty;
    List<String> ad;
    double tqu;

    public RequestUpdateWarehouseMeatItemStock(String stockType, List<String> availableDays, double thresholdQuantity){
        this.ty = stockType;
        this.ad = availableDays;
        this.tqu = thresholdQuantity;
    }

    public String getType() {
        return ty;
    }

    public List<String> getAvailableDays() {
        return ad;
    }

    public double getThresholdQuantity() {
        return tqu;
    }
}
