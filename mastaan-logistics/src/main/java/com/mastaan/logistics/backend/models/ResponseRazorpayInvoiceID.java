package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public class ResponseRazorpayInvoiceID {
    String id;

    public String getInvoiceID() {
        return id;
    }
}
