package com.mastaan.logistics.backend;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAddOrEditProcessedStock;
import com.mastaan.logistics.backend.models.RequestAddOrEditStock;
import com.mastaan.logistics.backend.models.RequestStockStatement;
import com.mastaan.logistics.backend.models.RequestUpdateHubMeatItemStock;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItemSource;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItemStock;
import com.mastaan.logistics.backend.models.RequestWarehouseMeatItemStock;
import com.mastaan.logistics.backend.models.ResponseAddStock;
import com.mastaan.logistics.backend.models.ResponseUpdateStock;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.HubMeatItemStockDetails;
import com.mastaan.logistics.models.ProcessedStockDetails;
import com.mastaan.logistics.models.StockAlertDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.WarehouseMeatItemStockDetails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 09/11/16.
 */

public class StockAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface WarehouseMeatItemStockCallback {
        void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemStockDetails warehouseMeatItemStockDetails);
    }

    public interface HubMeatItemStockCallback {
        void onComplete(boolean status, int statusCode, String message, HubMeatItemStockDetails hubMeatItemStockDetails);
    }

    public interface CategoriesListCallback {
        void onComplete(boolean status, int statusCode, String message, List<CategoryDetails> categoriesList);
    }

    public interface StockDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, StockDetails stockDetails);
    }

    public interface StockStatementCallback {
        void onComplete(boolean status, int statusCode, String message, List<StockDetails> stocksList);
    }

    public interface StocksListCallback {
        void onComplete(boolean status, int statusCode, String message, List<StockDetails> stocksList);
    }

    public interface UpdateStockCallback{
        void onComplete(boolean status, int statusCode, String message, double cost, double totalCost);
    }

    public interface StockAlertsCallback{
        void onComplete(boolean status, int statusCode, String message, List<StockAlertDetails> stockAlerts);
    }

    public interface ProcessedStockDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, ProcessedStockDetails stockDetails);
    }

    public interface ProcessedStockStatementCallback {
        void onComplete(boolean status, int statusCode, String message, String loadedMonth, List<ProcessedStockDetails> stocksList);
    }

    public interface ProcessedStocksListCallback {
        void onComplete(boolean status, int statusCode, String message, List<ProcessedStockDetails> stocksList);
    }

    public interface ProcessedStockCallback{
        void onComplete(boolean status, int statusCode, String message, ProcessedStockDetails processedStockDetails);
    }



    public StockAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }


    public void getWarehouseMeatItemStock(final String warehouseMeatItemID, final String[] itemLevelAttributeOptions, final WarehouseMeatItemStockCallback callback){
        String warehouseMeatItemIDWithItemLevelAttributeOptions = warehouseMeatItemID + (itemLevelAttributeOptions.length>0?","+ CommonMethods.getStringFromStringArray(itemLevelAttributeOptions,  ","):"");

        retrofitInterface.getWarehouseMeatItemStock(deviceID, appVersion, warehouseMeatItemIDWithItemLevelAttributeOptions, new Callback<WarehouseMeatItemStockDetails>() {
            @Override
            public void success(WarehouseMeatItemStockDetails warehouseMeatItemStockDetails, Response response) {
                Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success", warehouseMeatItemStockDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getHubMeatItemStock(final String hubID, final String warehouseMeatItemID, final String[] itemLevelAttributeOptions, final HubMeatItemStockCallback callback){
        String warehouseMeatItemIDWithItemLevelAttributeOptions = warehouseMeatItemID + (itemLevelAttributeOptions.length>0?","+ CommonMethods.getStringFromStringArray(itemLevelAttributeOptions,  ","):"");

        retrofitInterface.getHubMeatItemStock(deviceID, appVersion, hubID, warehouseMeatItemIDWithItemLevelAttributeOptions, new Callback<HubMeatItemStockDetails>() {
            @Override
            public void success(HubMeatItemStockDetails hubMeatItemStockDetails, Response response) {
                Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ HUB_MEAT_ITEM_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success", hubMeatItemStockDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ HUB_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ HUB_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateWarehouseMeatItemStock(final String warehouseMeatItemID, final String[] itemLevelAttributeOptions, final RequestUpdateWarehouseMeatItemStock requestObject, final StatusCallback callback){
        String warehouseMeatItemIDWithItemLevelAttributeOptions = warehouseMeatItemID + (itemLevelAttributeOptions.length>0?","+ CommonMethods.getStringFromStringArray(itemLevelAttributeOptions, ","):"");

        retrofitInterface.updateWarehouseMeatItemStock(deviceID, appVersion, warehouseMeatItemIDWithItemLevelAttributeOptions, requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_WAREHOUSE_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateHubMeatItemStock(final String hubID, final String warehouseMeatItemID, final String[] itemLevelAttributeOptions, final RequestUpdateHubMeatItemStock requestObject, final StatusCallback callback){
        String warehouseMeatItemIDWithItemLevelAttributeOptions = warehouseMeatItemID + (itemLevelAttributeOptions.length>0?","+ CommonMethods.getStringFromStringArray(itemLevelAttributeOptions, ","):"");

        retrofitInterface.updateHubMeatItemStock(deviceID, appVersion, hubID, warehouseMeatItemIDWithItemLevelAttributeOptions, requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_HUB_MEAT_ITEM_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_HUB_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_HUB_MEAT_ITEM_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getCategoriesStockStatement(final RequestStockStatement requestStockStatement, final StockStatementCallback callback){

        retrofitInterface.getCategoriesStockStatement(deviceID, appVersion, requestStockStatement, new Callback<List<StockDetails>>() {
            @Override
            public void success(List<StockDetails> stocksList, Response response) {
                if ( stocksList != null ){
                    Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ CATEGORY_STATEMENT ]  >>  Success");
                    callback.onComplete(true, 200, "Success", stocksList);
                }else{
                    Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ CATEGORY_STATEMENT ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ CATEGORY_STATEMENT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ CATEGORY_STATEMENT ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getMeatItemsStockStatement(final RequestStockStatement requestStockStatement, final StockStatementCallback callback){

        retrofitInterface.getMeatItemsStockStatement(deviceID, appVersion, requestStockStatement, new Callback<List<StockDetails>>() {
            @Override
            public void success(List<StockDetails> stocksList, Response response) {
                if ( stocksList != null ){
                    Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ MEAT_ITEMS_STATEMENT ]  >>  Success");
                    callback.onComplete(true, 200, "Success", stocksList);
                }else{
                    Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ MEAT_ITEMS_STATEMENT ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ MEAT_ITEMS_STATEMENT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ MEAT_ITEMS_STATEMENT ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }


    public void addStock(final RequestAddOrEditStock requestAddOrEditStock, final StockDetailsCallback callback){

        retrofitInterface.addStock(deviceID, appVersion, requestAddOrEditStock, new Callback<ResponseAddStock>() {
            @Override
            public void success(ResponseAddStock responseAddStock, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ ADD_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success", new StockDetails(responseAddStock.getStockID()).setQuantity(requestAddOrEditStock.getQuantity()));
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ ADD_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ ADD_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateStock(final String stockID, RequestAddOrEditStock requestAddOrEditStock, final UpdateStockCallback callback){

        retrofitInterface.updateStock(deviceID, appVersion, stockID, requestAddOrEditStock, new Callback<ResponseUpdateStock>() {
            @Override
            public void success(ResponseUpdateStock responseUpdateStock, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success", responseUpdateStock.getCost(), responseUpdateStock.getTotalCost());
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", 0, 0);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", 0, 0);
                    }
                }
            }
        });
    }

    public void deleteStock(final String stockID, final StatusCallback callback){

        retrofitInterface.deleteStock(deviceID, appVersion, stockID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ DELETE_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ DELETE_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ DELETE_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getStockDetails(final String stockID, final StockDetailsCallback callback){

        retrofitInterface.getStockDetails(deviceID, appVersion, stockID, new Callback<StockDetails>() {
            @Override
            public void success(StockDetails stockDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCK_DETAILS ]  >>  Success");
                callback.onComplete(true, 200, "Success", stockDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCK_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCK_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getStockAlerts(final StockAlertsCallback callback){

        retrofitInterface.getStockAlerts(deviceID, appVersion, new Callback<List<StockAlertDetails>>() {
            @Override
            public void success(final List<StockAlertDetails> stockAlerts, Response response) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        Collections.sort(stockAlerts, new Comparator<StockAlertDetails>() {
                            public int compare(StockAlertDetails item1, StockAlertDetails item2) {
                                return DateMethods.compareDates(item1.getDate(), item2.getDate());
                            }
                        });
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCK_ALERTS ]  >>  Success");
                        callback.onComplete(true, 200, "Success", stockAlerts);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCK_ALERTS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCK_ALERTS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateWarehouseMeatItemSource(final String warehouseMeatItemID, final String source, final StatusCallback callback){

        retrofitInterface.updateWarehouseMeatItemSource(deviceID, appVersion, warehouseMeatItemID, new RequestUpdateWarehouseMeatItemSource(source), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_WHMEATITEM_SOURCE ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_WHMEATITEM_SOURCE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_WHMEATITEM_SOURCE ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


    public void addProcessedStock(RequestAddOrEditProcessedStock requestObject, final ProcessedStockCallback callback){

        retrofitInterface.addProcessedStock(deviceID, appVersion, requestObject, new Callback<ProcessedStockDetails>() {
            @Override
            public void success(ProcessedStockDetails processedStockDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ ADD_PROCESSED_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success", processedStockDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ ADD_PROCESSED_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ ADD_PROCESSED_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateProcessedStock(final String stockID, RequestAddOrEditProcessedStock requestObject, final ProcessedStockCallback callback){

        retrofitInterface.updateProcessedStock(deviceID, appVersion, stockID, requestObject, new Callback<ProcessedStockDetails>() {
            @Override
            public void success(ProcessedStockDetails processedStockDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_PROCESSED_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success", processedStockDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_PROCESSED_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ UPDATE_PROCESSED_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void deleteProcessedStock(final String stockID, final StatusCallback callback){

        retrofitInterface.deleteProcessedStock(deviceID, appVersion, stockID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ DELETE_PROCESSED_STOCK ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ DELETE_PROCESSED_STOCK ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ DELETE_PROCESSED_STOCK ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getProcessedStockDetails(final String stockID, final ProcessedStockDetailsCallback callback){

        retrofitInterface.getProcessedStockDetails(deviceID, appVersion, stockID, new Callback<ProcessedStockDetails>() {
            @Override
            public void success(ProcessedStockDetails stockDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ PROCESSED_STOCK_DETAILS ]  >>  Success");
                callback.onComplete(true, 200, "Success", stockDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ PROCESSED_STOCK_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ PROCESSED_STOCK_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getProcessedStockStatement(final String date, final ProcessedStockStatementCallback callback){

        int month=0,year=0;
        try{
            Calendar calendar = DateMethods.getCalendarFromDate(date);
            month = calendar.get(Calendar.MONTH)+1;
            year = calendar.get(Calendar.YEAR);
        }catch (Exception e){}

        retrofitInterface.getProcessedStockStatement(deviceID, appVersion, month, year, new Callback<List<ProcessedStockDetails>>() {
            @Override
            public void success(List<ProcessedStockDetails> stocksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ PROCESSED_STOCKS_STATEMENT ]  >>  Success");
                callback.onComplete(true, 200, "Success", date, stocksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ PROCESSED_STOCKS_STATEMENT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), date, "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ PROCESSED_STOCKS_STATEMENT ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", date, null);
                    }
                }
            }
        });
    }



    public void getStocksForOrderItem(final String orderItemID, final StocksListCallback callback){

        retrofitInterface.getStocksForOrderItem(deviceID, appVersion, orderItemID, new Callback<List<StockDetails>>() {
            @Override
            public void success(List<StockDetails> stocksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCKS_FOR_ORDER_ITEM ]  >>  Success");
                callback.onComplete(true, 200, "Success", stocksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCKS_FOR_ORDER_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCKS_FOR_ORDER_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getCategoriesForStockManagement(final CategoriesListCallback callback){

        retrofitInterface.getCategoriesForStockManagement(deviceID, appVersion, new Callback<List<CategoryDetails>>() {
            @Override
            public void success(List<CategoryDetails> categoriesList, Response response) {
                if ( categoriesList != null ){
                    Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ GET_CATEGORIES ]  >>  Success");
                    callback.onComplete(true, 200, "Success", categoriesList);
                }else{
                    Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ GET_CATEGORIES ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ GET_CATEGORIES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ GET_CATEGORIES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getWarehouseMeatItemStockFromOrderItem(final String orderItemID, final WarehouseMeatItemStockCallback callback){

        retrofitInterface.getWarehouseMeatItemStockFromOrderItem(deviceID, appVersion, orderItemID, new Callback<WarehouseMeatItemStockDetails>() {
            @Override
            public void success(WarehouseMeatItemStockDetails warehouseMeatItemStockDetails, Response response) {
                Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK_FROM_ORDER_ITEM ]  >>  Success");
                callback.onComplete(true, 200, "Success", warehouseMeatItemStockDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK_FROM_ORDER_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ WAREHOUSE_MEAT_ITEM_STOCK_FROM_ORDER_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getHubMeatItemStockFromOrderItem(final String orderItemID, final HubMeatItemStockCallback callback){

        retrofitInterface.getHubMeatItemStockFromOrderItem(deviceID, appVersion, orderItemID, new Callback<HubMeatItemStockDetails>() {
            @Override
            public void success(HubMeatItemStockDetails hubMeatItemStockDetails, Response response) {
                Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ HUB_MEAT_ITEM_STOCK_FROM_ORDER_ITEM ]  >>  Success");
                callback.onComplete(true, 200, "Success", hubMeatItemStockDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ HUB_MEAT_ITEM_STOCK_FROM_ORDER_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.i(Constants.LOG_TAG, "\nSTOCK_API : [ HUB_MEAT_ITEM_STOCK_FROM_ORDER_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getStocksForWarehouseMeatItem(final List<AttributeOptionDetails> itemLevelAttributeOptions, final String warehouseMeatItemID, final StocksListCallback callback){
        List<String> itemLevelAttributeOptionsIDs = new ArrayList<>();
        if ( itemLevelAttributeOptions != null && itemLevelAttributeOptions.size() > 0 ) {
            for (int i = 0; i < itemLevelAttributeOptions.size(); i++) {
                itemLevelAttributeOptionsIDs.add(itemLevelAttributeOptions.get(i).getID());
            }
        }
        getStocksForWarehouseMeatItem(warehouseMeatItemID, itemLevelAttributeOptionsIDs, callback);
    }
    public void getStocksForWarehouseMeatItem(final String warehouseMeatItemID, final List<String> itemLevelAttributeOptions, final StocksListCallback callback){
        String warehouseMeatItemIDWithItemLevelAttributeOptions = warehouseMeatItemID + (itemLevelAttributeOptions!=null&&itemLevelAttributeOptions.size()>0?","+ CommonMethods.getStringFromStringList(itemLevelAttributeOptions, ","):"");

        retrofitInterface.getStocksForWarehouseMeatItem(deviceID, appVersion, warehouseMeatItemIDWithItemLevelAttributeOptions, new Callback<List<StockDetails>>() {
            @Override
            public void success(List<StockDetails> stocksList, Response response) {
                if ( stocksList != null ){
                    Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCKS_FOR_WAREHOUSE_MEAT_ITEM ]  >>  Success");
                    callback.onComplete(true, 200, "Success", stocksList);
                }else{
                    Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCKS_FOR_WAREHOUSE_MEAT_ITEM ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCKS_FOR_WAREHOUSE_MEAT_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ STOCKS_FOR_WAREHOUSE_MEAT_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getProcessedStocksForCategory(final String categoryID, final ProcessedStocksListCallback callback){

        retrofitInterface.getProcessedStocksForCategory(deviceID, appVersion, categoryID, new Callback<List<ProcessedStockDetails>>() {
            @Override
            public void success(List<ProcessedStockDetails> stocksList, Response response) {
                Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ PROCESSED_STOCKS_CATEGORY ]  >>  Success");
                callback.onComplete(true, 200, "Success", stocksList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ PROCESSED_STOCKS_CATEGORY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nSTOCK_API : [ PROCESSED_STOCKS_CATEGORY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

}
