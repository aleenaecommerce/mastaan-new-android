package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.Job;

/**
 * Created by Venkatesh Uppu on 12/12/15.
 */
public class RequestCreateJob {
    String req_id;
    String loc;
    Job job;

    public RequestCreateJob(String req_id, double latitude, double longitude, Job job) {
        this.req_id = req_id;
        this.loc = latitude + "," + longitude;
        this.job = job;
    }
}
