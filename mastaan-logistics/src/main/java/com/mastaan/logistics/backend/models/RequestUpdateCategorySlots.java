package com.mastaan.logistics.backend.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;

import java.util.List;

/**
 * Created by Venakatesh Uppu on 2/9/16.
 */
public class RequestUpdateCategorySlots {

    String c;
    String cn;
    String d;
    List<String> enabled;
    List<String> disabled;

    public RequestUpdateCategorySlots(String categoryID, String categoryName, String date, List<String> enabledSlots, List<String> disabledSlots){
        this.c = categoryID;
        this.cn = categoryName;
        this.d = date;
        this.enabled = enabledSlots;
        this.disabled = disabledSlots;
    }

    public String getCategoryName() {
        return cn;
    }

    public String getDate() {
        return DateMethods.getDateInFormat(d, DateConstants.MMM_DD_YYYY).toUpperCase();
    }
}
