package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 21/03/18.
 */

public class RequestAddRefundMoneyCollection {
    String pid;     // Payment ID
    String rid;     // Refund ID
    double amt;     // Amount
    String date;    // Date

    public RequestAddRefundMoneyCollection(String paymentID, String refundID, double refundAmount, String refundDate){
        this.pid = paymentID;
        this.rid = refundID;
        this.amt = refundAmount;
        this.date = refundDate;
    }
}
