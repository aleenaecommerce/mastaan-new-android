package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 10/11/16.
 */
public class RequestUpdateCounterSaleOrderItem {
    double n;
    double ta;
    String si;

    public RequestUpdateCounterSaleOrderItem(double quantity, double price, String comments){
        this.n = quantity;
        this.ta = price;
        this.si = comments;
    }

    public double getQuantity() {
        return n;
    }

    public double getAmount() {
        return ta;
    }

    public String getSpecialInstructions() {
        return si;
    }
}
