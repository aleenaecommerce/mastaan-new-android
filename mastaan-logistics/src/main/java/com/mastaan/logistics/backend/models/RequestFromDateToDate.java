package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 05/06/18.
 */

public class RequestFromDateToDate {
    String fromDate;
    String toDate;

    public RequestFromDateToDate(String fromDate, String toDate){
        this.fromDate = fromDate;
        this.toDate = toDate;
    }
}
