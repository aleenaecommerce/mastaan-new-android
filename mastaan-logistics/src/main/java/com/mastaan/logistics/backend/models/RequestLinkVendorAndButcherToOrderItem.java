package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 30/1/16.
 */
public class RequestLinkVendorAndButcherToOrderItem {
    String m;       // vendor_id
    String mpt;     // payment_type
    double apm;     // amount
    String mtc;     // Comments

    String but;     //  Butcher
    double fnw;     // Final net weight

    String sto;     // Stock
    String psto;    // Processed Stock

    public RequestLinkVendorAndButcherToOrderItem(String vendor_id, String payment_type, double amount, String comments, String butcher_id, double finalNetWeight, String stock_id, String processed_stock_id) {
        this.m = vendor_id;
        this.mpt = payment_type;
        this.apm = amount;
        this.mtc = comments;

        this.but = butcher_id;
        this.fnw = finalNetWeight;

        this.sto = stock_id;
        this.psto = processed_stock_id;
    }

}
