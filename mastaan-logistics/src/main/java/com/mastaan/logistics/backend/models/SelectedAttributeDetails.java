package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 1/10/16.
 */
public class SelectedAttributeDetails{
    String at;
    String opt;
    double opd;

    public SelectedAttributeDetails(String attributeID, String attributeOptionID, double attributeOptionPriceDifference){
        this.at = attributeID;
        this.opt = attributeOptionID;
        this.opd = attributeOptionPriceDifference;
    }

    public String getAttributeID() {
        return at;
    }

    public String getAttributeOptionID() {
        return opt;
    }

    public double getAttributeOptionPriceDifference() {
        return opd;
    }
}
