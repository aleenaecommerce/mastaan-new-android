package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAddOnlinePayment;
import com.mastaan.logistics.backend.models.RequestCaptureRazorpayPayment;
import com.mastaan.logistics.backend.models.RequestRazorpayInvoiceID;
import com.mastaan.logistics.backend.models.RequetPaytmPayment;
import com.mastaan.logistics.backend.models.ResponseRazorpayInvoiceID;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.OrderDetails;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by venkatesh on 13/7/15.
 */

public class PaymentAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface CheckOrderPaymentStatusCallback {
        void onComplete(boolean status, int statusCode, String message, OrderDetails orderDetails);
    }

    public interface CheckBuyerPaymentStatusCallback {
        void onComplete(boolean status, int statusCode, String message, BuyerDetails buyerDetails);
    }

    public PaymentAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getRazorpayInvoiceIDForOrder(final String orderID, final RequestRazorpayInvoiceID requestRazorpayInvoiceID, final StatusCallback callback) {

        retrofitInterface.getRazorpayInvoiceIDForOrder(deviceID, appVersion, orderID, requestRazorpayInvoiceID, new Callback<ResponseRazorpayInvoiceID>() {
            @Override
            public void success(ResponseRazorpayInvoiceID responseRazorpayInvoiceID, Response response) {
                if (responseRazorpayInvoiceID.getInvoiceID() != null) {
                    Log.e(Constants.LOG_TAG, "\nPAYMENTS_API : [ RAZORPAY_INVOICE_ID_FOR_ORDER ]  >>  Success");
                    Log.e("shorturl", "url if :" + response);
                    callback.onComplete(true, 200, responseRazorpayInvoiceID.getInvoiceID());
                } else {
                    Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ RAZORPAY_INVOICE_ID_FOR_ORDER ]  >>  Failure");
                    callback.onComplete(false, 500, responseRazorpayInvoiceID.getInvoiceID());
                    Log.e("shorturl", "url else fail :" + response);

                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.e(Constants.LOG_TAG, "\nPAYMENTS_API : [ RAZORPAY_INVOICE_ID_FOR_ORDER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                        Log.e("shorturl", "url else faaa :" + error.getResponse().getStatus());

                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ RAZORPAY_INVOICE_ID_FOR_ORDER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getRazorpayInvoiceIDForBuyer(final String buyerID, final RequestRazorpayInvoiceID requestRazorpayInvoiceID, final StatusCallback callback){

        retrofitInterface.getRazorpayInvoiceIDForBuyer(deviceID, appVersion, buyerID, requestRazorpayInvoiceID, new Callback<ResponseRazorpayInvoiceID>() {
            @Override
            public void success(ResponseRazorpayInvoiceID responseRazorpayInvoiceID, Response response) {
                if ( responseRazorpayInvoiceID.getInvoiceID() != null ) {
                    Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ RAZORPAY_INVOICE_ID_FOR_BUYER ]  >>  Success");
                    callback.onComplete(true, 200, responseRazorpayInvoiceID.getInvoiceID());
                }else{
                    Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ RAZORPAY_INVOICE_ID_FOR_BUYER ]  >>  Failure");
                    callback.onComplete(false, 500, responseRazorpayInvoiceID.getInvoiceID());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ RAZORPAY_INVOICE_ID_FOR_BUYER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ RAZORPAY_INVOICE_ID_FOR_BUYER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void captureRazorpayPayment(final String paymentID, final double amount, final StatusCallback callback){

        retrofitInterface.captureRazorpayPayment(deviceID, appVersion, new RequestCaptureRazorpayPayment(paymentID, amount), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CAPTURE_RAZORPAY ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CAPTURE_RAZORPAY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CAPTURE_RAZORPAY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void addOnlinePaymentForOrder(String orderID, double amount, String transactionID, String paymentID, final StatusCallback callback){

        retrofitInterface.addOnlinePaymentForOrder(deviceID, appVersion, orderID, new RequestAddOnlinePayment(amount, transactionID, paymentID), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_TO_ORDER ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_TO_ORDER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ ADD_OP_TO_ORDER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void checkOrderPaymentStatus(final String orderID, final CheckOrderPaymentStatusCallback callback){

        retrofitInterface.checkOrderPaymentStatus(deviceID, appVersion, orderID, new Callback<OrderDetails>() {
            @Override
            public void success(OrderDetails orderDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CHECK_ORDER_PAYMENT_STATUS ]  >>  Success");
                callback.onComplete(true, 200, "Success", orderDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CHECK_ORDER_PAYMENT_STATUS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CHECK_ORDER_PAYMENT_STATUS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void checkBuyerPaymentStatus(final String buyerID, final CheckBuyerPaymentStatusCallback callback){

        retrofitInterface.checkBuyerPaymentStatus(deviceID, appVersion, buyerID, new Callback<BuyerDetails>() {
            @Override
            public void success(BuyerDetails buyerDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CHECK_BUYER_PAYMENT_STATUS ]  >>  Success");
                callback.onComplete(true, 200, "Success", buyerDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CHECK_BUYER_PAYMENT_STATUS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ CHECK_BUYER_PAYMENT_STATUS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void initiatePaytmePayment(final String buyerID, final RequetPaytmPayment requetPaytmPayment, final StatusCallback callback){
        retrofitInterface.initiatePaytmePayment(deviceID, appVersion, buyerID, requetPaytmPayment, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ INITIATE_PAYTM_PAYMENT ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ INITIATE_PAYTM_PAYMENT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nPAYMENTS_API : [ INITIATE_PAYTM_PAYMENT ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });

    }


}
