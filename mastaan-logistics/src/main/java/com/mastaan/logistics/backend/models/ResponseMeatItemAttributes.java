package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.AttributeDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 23/10/16.
 */
public class ResponseMeatItemAttributes {
    List<AttributeDetails> ma;

    public List<AttributeDetails> getAttributes() {
        if ( ma != null ){  return ma;  }
        return new ArrayList<>();
    }
}
