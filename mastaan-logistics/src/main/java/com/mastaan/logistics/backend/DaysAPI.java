package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.models.RequestManageDayAvailability;
import com.mastaan.logistics.backend.models.ResponseDisabledDates;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.DayDetails;

import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class DaysAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface DisabledDatesListCallback {
        void onComplete(boolean status, int statusCode, String message, List<DayDetails> daysList, List<CategoryDetails> categoriesList);
    }

    public DaysAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getDisabledDays(final int month, final int year, final DisabledDatesListCallback callback){

        retrofitInterface.getDisabledDays(deviceID, appVersion, month, year, new Callback<ResponseDisabledDates>() {
            @Override
            public void success(ResponseDisabledDates responseDisabledDates, Response response) {
                if ( responseDisabledDates != null ){
                    Log.d(Constants.LOG_TAG, "\nDAYS_API : [ GET_DISABLED_DATES ]  >>  Success");
                    callback.onComplete(true, 200, "Success", responseDisabledDates.getDisaledDates(), responseDisabledDates.getCategories());
                }else{
                    Log.d(Constants.LOG_TAG, "\nDAYS_API : [ GET_DISABLED_DATES ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nDAYS_API : [ GET_DISABLED_DATES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nDAYS_API : [ GET_DISABLED_DATES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void manageDayAvailability(String date, final RequestManageDayAvailability requestManageDayAvailability, final StatusCallback callback){

        Calendar calendar = DateMethods.getCalendarFromDate(date);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH)+1;
        int year = calendar.get(Calendar.YEAR);

        retrofitInterface.manageDayAvailability(deviceID, appVersion, day, month, year, requestManageDayAvailability, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nDAYS_API : [ MANAGE_DAY_AVAILABILITY ]  >> Success");
                callback.onComplete(true, 200, "Succes");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nDAYS_API : [ MANAGE_DAY_AVAILABILITY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nDAYS_API : [ MANAGE_DAY_AVAILABILITY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}
