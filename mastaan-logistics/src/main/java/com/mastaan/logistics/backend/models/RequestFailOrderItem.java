package com.mastaan.logistics.backend.models;

/**
 * Created by venkatesh on 14/5/16.
 */
public class RequestFailOrderItem {
    String reason;
    boolean reset_stock;

    public RequestFailOrderItem(String reason, boolean resetStock){
        this.reason = reason;
        this.reset_stock = resetStock;
    }
}
