package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.OrderDetails2;

/**
 * Created by venkatesh on 16/6/16.
 */
public class ResponseDBY {
    String dby;
    OrderDetails2 o;

    public String getDeliveryDate() {
        return dby;
    }

    public OrderDetails2 getOrderDetails() {
        return o;
    }
}
