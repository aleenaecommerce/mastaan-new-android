package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 2/9/16.
 */

public class RequestUpdateCategoryVisibility {
    boolean vis;

    public RequestUpdateCategoryVisibility(boolean isVisible){
        this.vis = isVisible;
    }

    public boolean isVisible() {
        return vis;
    }

}
