package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 3/12/16.
 */
public class ResponseUpdateStock {
    double co;
    double tco;

    public double getCost() {
        return co;
    }

    public double getTotalCost() {
        return tco;
    }
}
