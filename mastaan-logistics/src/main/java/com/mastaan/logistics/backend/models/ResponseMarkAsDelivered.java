package com.mastaan.logistics.backend.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 7/4/16.
 */
public class ResponseMarkAsDelivered {

    List<String> orderItemsMarkedAsDelivered;

    public List<String> getOrderItemsMarkedAsDelivered() {
        if ( orderItemsMarkedAsDelivered == null ){ orderItemsMarkedAsDelivered = new ArrayList<>();    }
        return orderItemsMarkedAsDelivered;
    }

    //    String code;
//    boolean collectPayment;
//    double collectAmount;
//
//    public String getCode() {
//        return code;
//    }
//
//    public boolean getCollecteAmountStatus() {
//        return collectPayment;
//    }
//
//    public double getAmountToBeCollected() {
//        return collectAmount;
//    }
}
