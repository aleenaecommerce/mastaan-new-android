package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 1/10/16.
 */
public class RequestEditOrderItem {

    String wmi;     // Warehouse Meat Item ID
    String i;       // Meat Item ID
    //String wh;      // Warehouse Meat Item ID
    double n;       // Quantity
    List<SelectedAttributeDetails> a;   // Selected Attributes
    double tpd;     // Total Price Difference

    public RequestEditOrderItem(WarehouseMeatItemDetails warehouseMeatItemDetails, double quantity, List<SelectedAttributeDetails> attributes){
        this.wmi = warehouseMeatItemDetails.getID();
        this.i = warehouseMeatItemDetails.getMeatItemID();
        //this.wh = warehouseMeatItemDetails.getID();
        this.n = quantity;
        if ( attributes == null ){  a = new ArrayList<>();  }
        this.a = attributes;
        for (int i=0;i<attributes.size();i++){
            tpd += n*attributes.get(i).getAttributeOptionPriceDifference();
        }
    }


}
