package com.mastaan.logistics.backend.models;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 28/06/17.
 */

public class RequestCompleteDelivery {
    List<String> delivered;
    List<String> rejected;

    boolean collectPayment;
    String amount;
    String paymentType = "1";
    String paymentID;

    String comments;

    public RequestCompleteDelivery(List<String> deliveredItems, List<String> rejectedItems, boolean isMoneyCollected, double amountCollected, String comments){
        this.delivered = deliveredItems;
        this.rejected = rejectedItems;

        this.collectPayment = isMoneyCollected;
        this.amount = amountCollected+"";

        this.comments = comments;
    }

    public RequestCompleteDelivery setPaymentType(int paymentType) {
        this.paymentType = paymentType+"";
        return this;
    }

    public RequestCompleteDelivery setPaymentID(String paymentID) {
        this.paymentID = paymentID;
        return this;
    }

}
