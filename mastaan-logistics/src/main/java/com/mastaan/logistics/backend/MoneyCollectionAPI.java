package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAddRefundMoneyCollection;
import com.mastaan.logistics.backend.models.RequestMarkAllMoneyCollectionsAsConsolidated;
import com.mastaan.logistics.backend.models.RequestUpdateMoneyCollectionTransactionDetails;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.MoneyCollectionDetails;

import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class MoneyCollectionAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface MoneyCollectionsCallback {
        void onComplete(boolean status, int statusCode, String message, String date, List<MoneyCollectionDetails> moneyCollectionsList);
    }
    public interface MoneyCollectionCallback {
        void onComplete(boolean status, int statusCode, String message, MoneyCollectionDetails moneyCollectionDetails);
    }


    public MoneyCollectionAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }


    public void getMoneyCollectionForOrder(String orderID, final MoneyCollectionCallback callback){

        retrofitInterface.getMoneyCollectionForOrder(deviceID, appVersion, orderID, new Callback<MoneyCollectionDetails>() {
            @Override
            public void success(MoneyCollectionDetails moneyCollectionDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Collection_For_Order] : Success");
                callback.onComplete(true, 200, "Success", moneyCollectionDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Collection_For_Order] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Collection_For_Order] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getMoneyCollectionForDate(final String date, final MoneyCollectionsCallback callback){

        int day=0,month=0,year=0;
        try{
            Calendar calendar = DateMethods.getCalendarFromDate(date);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH)+1;
            year = calendar.get(Calendar.YEAR);
        }catch (Exception e){}

        retrofitInterface.getMoneyCollectionForDate(deviceID, appVersion, day, month, year, new Callback<List<MoneyCollectionDetails>>() {
            @Override
            public void success(List<MoneyCollectionDetails> moneyCollectionsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Collection_For_Date] : Success");
                callback.onComplete(true, 200, "Success", date, moneyCollectionsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Collection_For_Date] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", date, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Collection_For_Date] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", date, null);
                    }
                }
            }
        });
    }

    public void getMoneyConsolidationForDate(final String date, final MoneyCollectionsCallback callback){

        int day=0,month=0,year=0;
        try{
            Calendar calendar = DateMethods.getCalendarFromDate(date);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH)+1;
            year = calendar.get(Calendar.YEAR);
        }catch (Exception e){}

        retrofitInterface.getMoneyConsolidationForDate(deviceID, appVersion, day, month, year, new Callback<List<MoneyCollectionDetails>>() {
            @Override
            public void success(List<MoneyCollectionDetails> moneyCollectionsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Consolidations_For_Date] : Success");
                callback.onComplete(true, 200, "Success", date, moneyCollectionsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Consolidations_For_Date] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", date, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Consolidations_For_Date] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", date, null);
                    }
                }
            }
        });
    }

    public void getMoneyCollectionOfDeliveryBoyForDate(final String deliveryBoyID, final String date, final MoneyCollectionsCallback callback){

        int day=0,month=0,year=0;
        try{
            Calendar calendar = DateMethods.getCalendarFromDate(date);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH)+1;
            year = calendar.get(Calendar.YEAR);
        }catch (Exception e){}

        retrofitInterface.getMoneyCollectionOfDeliveryBoyForDate(deviceID, appVersion, deliveryBoyID, day, month, year, new Callback<List<MoneyCollectionDetails>>() {
            @Override
            public void success(List<MoneyCollectionDetails> moneyCollectionsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Collection_Of_DeliveryBoy_For_Date] : Success");
                callback.onComplete(true, 200, "Success", date, moneyCollectionsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Collection_Of_DeliveryBoy_For_Date] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", date, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Money_Collection_Of_DeliveryBoy_For_Date] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", date, null);
                    }
                }
            }
        });
    }

    public void getPendingConsolidations(final MoneyCollectionsCallback callback){

        retrofitInterface.getPendingConsolidations(deviceID, appVersion, new Callback<List<MoneyCollectionDetails>>() {
            @Override
            public void success(List<MoneyCollectionDetails> moneyCollectionsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Pending_Consolidations] : Success");
                callback.onComplete(true, 200, "Success", "all", moneyCollectionsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Pending_Consolidations] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", "all", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Pending_Consolidations] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", "all", null);
                    }
                }
            }
        });
    }

    public void getPendingConsolidationsOfDeliveryBoy(final String deliveryBoyID, final MoneyCollectionsCallback callback){

        retrofitInterface.getPendingConsolidationOfDeliveryBoy(deviceID, appVersion, deliveryBoyID, new Callback<List<MoneyCollectionDetails>>() {
            @Override
            public void success(List<MoneyCollectionDetails> moneyCollectionsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Pending_Consolidations_of_Delivery_Boy] : Success");
                callback.onComplete(true, 200, "Success", "all", moneyCollectionsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Pending_Consolidations_of_Delivery_Boy] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", "all", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Pending_Consolidations_of_Delivery_Boy] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", "all", null);
                    }
                }
            }
        });
    }

    public void getBuyerMoneyCollections(final String buyerID, final MoneyCollectionsCallback callback){

        retrofitInterface.getBuyerMoneyCollections(deviceID, appVersion, buyerID, new Callback<List<MoneyCollectionDetails>>() {
            @Override
            public void success(List<MoneyCollectionDetails> moneyCollectionsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Buyer_Money_Collections] : Success");
                callback.onComplete(true, 200, "Success", "all", moneyCollectionsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Buyer_Money_Collections] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", "all", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Buyer_Money_Collections] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", "all", null);
                    }
                }
            }
        });
    }


    public void markMoneyCollectionTransactionAsConsolidated(String collectionID, String transactionID, final StatusCallback callback){

        retrofitInterface.markMoneyCollectionTransactionAsConsolidated(deviceID, appVersion, collectionID, transactionID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Mark_Money_Collection_Transaction_As_Consolidated] : Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Mark_Money_Collection_Transaction_As_Consolidated] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Mark_Money_Collection_Transaction_As_Consolidated] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateMoneyCollectionTransactionDetails(String collectionID, final String transactionID, RequestUpdateMoneyCollectionTransactionDetails requestUpdateMoneyCollectionTransactionDetails, final StatusCallback callback){

        retrofitInterface.updateMoneyCollectionDetails(deviceID, appVersion, collectionID, transactionID, requestUpdateMoneyCollectionTransactionDetails, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Update_Money_Collection_Transaction] : Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Update_Money_Collection_Transaction] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Update_Money_Collection_Transaction] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void markAllMoneyCollectionsAsConsolidated(List<String> collectionIDs, final StatusCallback callback){

        retrofitInterface.markAllMoneyCollectionsAsConsolidated(deviceID, appVersion, new RequestMarkAllMoneyCollectionsAsConsolidated(collectionIDs), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Mark_All_Money_Collections_As_Consolidated] : Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Mark_All_Money_Collections_As_Consolidated] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Mark_All_Money_Collections_As_Consolidated] : Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public com.mastaan.logistics.models.Response<String> addRefundMoneyCollection(RequestAddRefundMoneyCollection requestAddRefundMoneyCollection){
        try {
            ResponseStatus responseStatus = retrofitInterface.addRefundMoneyCollection(deviceID, appVersion, requestAddRefundMoneyCollection);
            Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Add_Refund] : Success");
            return new com.mastaan.logistics.models.Response<String>(true, 200, "Success");
        }
        catch (RetrofitError error){
            if (new CheckError(context).check(error) == false) {
                try {
                    Log.d(Constants.LOG_TAG, "\nMONEY_COLLECTION_API:  [Add_Refund] : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                    return new com.mastaan.logistics.models.Response<String>(false, error.getResponse().getStatus(), "Error");
                } catch (Exception e) {e.printStackTrace();}
            }
        }
        return new com.mastaan.logistics.models.Response<String>(false, -1, "Error");
    }

}
