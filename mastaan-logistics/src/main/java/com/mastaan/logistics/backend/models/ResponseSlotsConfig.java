package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.DeliveryFeeDetails;
import com.mastaan.logistics.models.DeliverySlotDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 31/01/18.
 */

public class ResponseSlotsConfig {
    int slotCutoffTime;
    double maxDeliveryDistance;
    long maxOrderCount;
    List<String> disabledDaysOfWeek;
    List<String> disabledCategories;
    List<DeliveryFeeDetails> deliveryCharges;

    List<DeliverySlotDetails> slots = new ArrayList<>();


    public ResponseSlotsConfig(List<DeliverySlotDetails> slots, int slotCutoffTime) {
        this.slots = slots;
        this.slotCutoffTime = slotCutoffTime;
    }

    public int getSlotCutoffTime() {
        return slotCutoffTime;
    }

    public double getMaxDeliveryDistance() {
        return maxDeliveryDistance;
    }

    public long getMaxOrderCount() {
        return maxOrderCount;
    }

    public List<String> getDisabledCategories() {
        if ( disabledCategories == null ){  disabledCategories = new ArrayList<>(); }
        return disabledCategories;
    }

    public List<String> getDisabledDaysOfWeek() {
        if ( disabledDaysOfWeek == null ){  disabledDaysOfWeek = new ArrayList<>(); }
        return disabledDaysOfWeek;
    }

    public List<DeliveryFeeDetails> getDeliveryCharges() {
        if ( deliveryCharges == null ){ deliveryCharges = new ArrayList<>();    }
        return deliveryCharges;
    }

    public List<DeliverySlotDetails> getSlots() {
        if ( slots == null ){   slots = new ArrayList<>();  }
        return slots;
    }

}
