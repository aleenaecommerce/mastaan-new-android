package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.BuyerDetails;

/**
 * Created by Venkatesh Uppu on 30/12/16.
 */
public class ResponseBuyerAccessToken {
    String token;
    BuyerDetails user;

    public String getToken() {
        return token;
    }

    public BuyerDetails getBuyerDetails() {
        if ( user != null ){    return user;    }
        return new BuyerDetails();
    }
}
