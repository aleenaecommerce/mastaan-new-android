package com.mastaan.logistics.backend.models;

/**
 * Created by venkatesh on 22/4/16.
 */
public class RequestUpdateOrderDeliveryDate {

    String date;
    String time;

    public RequestUpdateOrderDeliveryDate(String date, String time){
        this.date = date;
        this.time = time;
    }
}
