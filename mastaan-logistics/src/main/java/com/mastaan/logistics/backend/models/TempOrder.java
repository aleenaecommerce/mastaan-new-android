package com.mastaan.logistics.backend.models;

/**
 * Created by venkatesh on 15/6/16.
 */
public class TempOrder {
    String _id;
    String dby;

    public String getID() {
        return _id;
    }

    public String getDeliveryDate() {
        return dby;
    }
}
