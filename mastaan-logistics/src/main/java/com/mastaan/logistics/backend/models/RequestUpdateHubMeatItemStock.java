package com.mastaan.logistics.backend.models;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 29/10/18.
 */

public class RequestUpdateHubMeatItemStock {
    double tqu;

    public RequestUpdateHubMeatItemStock(double thresholdQuantity){
        this.tqu = thresholdQuantity;
    }

    public double getThresholdQuantity() {
        return tqu;
    }
}
