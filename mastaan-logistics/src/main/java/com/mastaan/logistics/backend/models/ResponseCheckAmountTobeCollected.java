package com.mastaan.logistics.backend.models;

/**
 * Created by venkatesh on 7/4/16.
 */
public class ResponseCheckAmountTobeCollected {

    boolean collectPayment;
    double collectAmount;

    public boolean getCollecteAmountStatus() {
        return collectPayment;
    }

    public double getAmountToBeCollected() {
        return collectAmount;
    }
}
