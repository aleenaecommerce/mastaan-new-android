package com.mastaan.logistics.backend;

import android.content.Context;

import com.mastaan.logistics.R;
import com.mastaan.logistics.localdata.LocalStorageData;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class BackendAPIs {

    Context context;
    String deviceID;
    int appVersion;

    RestAdapter restAdapter;
    //RetrofitInterface retrofitInterface;

    CommonAPI commonAPI;
    AuthenticationAPI authenticationAPI;
    UsersAPI usersAPI;
    ProfileAPI profileAPI;
    OrdersAPI ordersAPI;
    CustomerSupportAPI customerSupportAPI;
    VendorsAndButchersAPI vendorsAndButchersAPI;
    WhereToNextAPI whereToNextAPI;
    KmLogItemsAPI kmLogItemsAPI;
    MeatItemsAPI meatItemsAPI;
    CategoriesAPI categoriesAPI;
    DaysAPI daysAPI;
    FeedbacksAPI feedbacksAPI;
    FollowupsAPI followupsAPI;
    EmployeesAPI employeesAPI;
    MoneyCollectionAPI moneyCollectionAPI;
    StockAPI stockAPI;
    BuyersAPI buyersAPI;
    CartsAPI cartsAPI;
    PaymentAPI paymentAPI;
    RoutesAPI routesAPI;
    SlotsAPI slotsAPI;
    AnalyticsAPI analyticsAPI;
    ConfigurationsAPI configurationsAPI;

    RazorpayAPI razorpayAPI;


    public BackendAPIs(Context context, String deviceID, int appVersion){
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;
    }

    private RestAdapter getRestAdapter(){
        if ( restAdapter == null ){
            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(context.getString(R.string.base_url))
                    .setRequestInterceptor(new RequestInterceptor() {
                        @Override
                        public void intercept(RequestFacade request) {
                            request.addHeader("x-access-token", new LocalStorageData(context).getAccessToken());
                        }
                    })
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            //retrofitInterface = restAdapter.create(RetrofitInterface.class);
        }
        return restAdapter;
    }

    public CommonAPI getCommonAPI() {
        if ( commonAPI == null ){
            commonAPI = new CommonAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return commonAPI;
    }

    public AuthenticationAPI getAuthenticationAPI() {
        if ( authenticationAPI == null ){
            authenticationAPI = new AuthenticationAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return authenticationAPI;
    }

    public UsersAPI getUsersAPI() {
        if ( usersAPI == null ){
            usersAPI = new UsersAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return usersAPI;
    }

    public ProfileAPI getProfileAPI() {
        if ( profileAPI == null ){
            profileAPI = new ProfileAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return profileAPI;
    }

    public OrdersAPI getOrdersAPI() {
        if ( ordersAPI == null ){
            ordersAPI = new OrdersAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return ordersAPI;
    }

    public CustomerSupportAPI getCustomerSupportAPI() {
        if ( customerSupportAPI == null ){
            customerSupportAPI = new CustomerSupportAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return customerSupportAPI;
    }

    public VendorsAndButchersAPI getVendorsAndButchersAPI() {
        if ( vendorsAndButchersAPI == null ){
            vendorsAndButchersAPI = new VendorsAndButchersAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return vendorsAndButchersAPI;
    }

    public WhereToNextAPI getWhereToNextAPI() {
        if ( whereToNextAPI == null ){
            whereToNextAPI = new WhereToNextAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return whereToNextAPI;
    }

    public KmLogItemsAPI getKmLogItemsAPI() {
        if ( kmLogItemsAPI == null ){
            kmLogItemsAPI = new KmLogItemsAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return kmLogItemsAPI;
    }

    public MeatItemsAPI getMeatItemsAPI() {
        if ( meatItemsAPI == null ){
            meatItemsAPI = new MeatItemsAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return meatItemsAPI;
    }

    public CategoriesAPI getCategoriesAPI() {
        if ( categoriesAPI == null ){
            categoriesAPI = new CategoriesAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return categoriesAPI;
    }

    public DaysAPI getDaysAPI() {
        if ( daysAPI == null ){
            daysAPI = new DaysAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return daysAPI;
    }

    public FeedbacksAPI getFeedbacksAPI() {
        if ( feedbacksAPI == null ){
            feedbacksAPI = new FeedbacksAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return feedbacksAPI;
    }

    public FollowupsAPI getFollowupsAPI() {
        if ( followupsAPI == null ){
            followupsAPI = new FollowupsAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return followupsAPI;
    }

    public EmployeesAPI getEmployeesAPI() {
        if ( employeesAPI == null ){
            employeesAPI = new EmployeesAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return employeesAPI;
    }

    public MoneyCollectionAPI getMoneyCollectionAPI() {
        if ( moneyCollectionAPI == null ){
            moneyCollectionAPI = new MoneyCollectionAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return moneyCollectionAPI;
    }

    public StockAPI getStockAPI() {
        if ( stockAPI == null ){
            stockAPI = new StockAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return stockAPI;
    }

    public BuyersAPI getBuyersAPI() {
        if ( buyersAPI == null ){
            buyersAPI = new BuyersAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return buyersAPI;
    }

    public CartsAPI getCartsAPI() {
        if ( cartsAPI == null ){
            cartsAPI = new CartsAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return cartsAPI;
    }

    public PaymentAPI getPaymentAPI() {
        if ( paymentAPI == null ){
            paymentAPI = new PaymentAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return paymentAPI;
    }

    public RoutesAPI getRoutesAPI() {
        if ( routesAPI == null ){
            routesAPI = new RoutesAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return routesAPI;
    }

    public SlotsAPI getSlotsAPI() {
        if ( slotsAPI == null ){
            slotsAPI = new SlotsAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return slotsAPI;
    }

    public AnalyticsAPI getAnalyticsAPI() {
        if ( analyticsAPI == null ){
            analyticsAPI = new AnalyticsAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return analyticsAPI;
    }

    public ConfigurationsAPI getConfigurationsAPI() {
        if ( configurationsAPI == null ){
            configurationsAPI = new ConfigurationsAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return configurationsAPI;
    }

    public RazorpayAPI getRazorpayAPI() {
        if ( razorpayAPI == null ){
            razorpayAPI = new RazorpayAPI(context, deviceID, appVersion, getRestAdapter());
        }
        return razorpayAPI;
    }
}
