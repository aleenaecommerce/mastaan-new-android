package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 25/3/17.
 */
public class RequestAssignDeliveryArea {
    String area;

    public RequestAssignDeliveryArea(String areaID){
        this.area = areaID;
    }
}
