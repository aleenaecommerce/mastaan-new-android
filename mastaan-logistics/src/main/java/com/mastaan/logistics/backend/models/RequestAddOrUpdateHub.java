package com.mastaan.logistics.backend.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 07/11/18.
 */

public class RequestAddOrUpdateHub {
    String en;
    String n;
    Double []loc;
    String fa;

    public RequestAddOrUpdateHub(String name, LatLng latLng, String fullAddress){
        this.en = true+"";
        this.n = name;
        this.loc = new Double[]{latLng.longitude, latLng.latitude};
        this.fa = fullAddress;
    }
    public RequestAddOrUpdateHub(boolean enable){
        this.en = enable+"";
    }


    public String getName() {
        return n;
    }

    public String getFullAddress() {
        return fa;
    }

    public LatLng getLocation() {
        return loc!=null&&loc.length==2?new LatLng(loc[1], loc[0]):new LatLng(0, 0);
    }

}
