package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 19/02/17.
 */
public class RequestAddEmployeeViolation {
    String date;
    String type;
    String com;

    public RequestAddEmployeeViolation(String date, String type, String com){
        this.date = date;
        this.type = type;
        this.com = com;
    }
}
