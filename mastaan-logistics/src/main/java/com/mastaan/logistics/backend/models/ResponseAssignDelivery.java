package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.BuyerDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 2/6/17.
 */
public class ResponseAssignDelivery {
    BuyerDetails buyer;
    List<PendingItems> pending;

    public class PendingItems{
        String _id;
        int count;

        public String getCategoryName() {
            return _id;
        }

        public int getCount() {
            return count;
        }
    }

    public BuyerDetails getBuyerDetails() {
        if ( buyer == null ){   buyer = new BuyerDetails(); }
        return buyer;
    }

    public List<PendingItems> getPendingAssignments() {
        if ( pending == null ){ pending = new ArrayList<>();    }
        return pending;
    }

}
