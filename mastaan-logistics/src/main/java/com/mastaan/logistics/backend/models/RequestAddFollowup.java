package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 09/02/18.
 */

public class RequestAddFollowup {
    String ty;  // Type
    String b;   // Buyer ID
    String c;   // Comments

    String o;   // Order

    String pf;  // Pending Followup

    public RequestAddFollowup(String type, String buyerID, String comments) {
        this.ty = type;
        this.b = buyerID;
        this.c = comments;
    }

    public RequestAddFollowup setOrder(String orderID) {
        this.o = orderID;
        return this;
    }

    public RequestAddFollowup setForPendingFollowup(boolean forPendingFollowup) {
        this.pf = forPendingFollowup+"";
        return this;
    }

}
