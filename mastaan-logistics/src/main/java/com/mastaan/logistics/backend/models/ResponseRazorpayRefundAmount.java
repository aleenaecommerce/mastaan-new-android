package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 28/03/18.
 */

public class ResponseRazorpayRefundAmount {
    String id;  // Refund ID
    Error error;

    class Error{
        String code;
        String description;
    }

    public String getId() {
        return id;
    }

    public String getErrorCode() {
        return error!=null?error.code:"";
    }

    public String getErrorDescription() {
        return error!=null?error.description:"";
    }
}
