package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 23/9/16.
 */

public class RequestSendMessage {
    String m;
    String msg;

    public RequestSendMessage(String mobile, String message){
        this.m = mobile;
        this.msg = message;
    }

}
