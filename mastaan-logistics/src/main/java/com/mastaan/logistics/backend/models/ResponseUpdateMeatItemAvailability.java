package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.AttributeDetails;
import com.mastaan.logistics.models.AvailabilityDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 3/9/16.
 */
public class ResponseUpdateMeatItemAvailability {
    double pr;
    double opr;
    List<AvailabilityDetails> avl;
    List<AttributeDetails> ma;

    public double getBuyingPrice() {
        return pr;
    }

    public double getSellingPrice() {
        return opr;
    }

    public List<AvailabilityDetails> getAvailabilitiesList() {
        if ( avl != null ){ return avl; }
        return new ArrayList<>();
    }

    public List<AttributeDetails> getAttributesList() {
        if ( ma != null ){ return ma; }
        return ma;
    }
}
