package com.mastaan.logistics.backend;

import android.content.Context;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateAlertMessage;
import com.mastaan.logistics.models.ConfigurationDetails;
import com.mastaan.logistics.models.MembershipDetails;
import com.mastaan.logistics.models.MessageDetails;
import com.mastaan.logistics.models.ReferralOfferDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by Venkatesh Uppu on 19/02/18.
 */

public class ConfigurationsAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface ConfigurationCallback {
        void onComplete(boolean status, int statusCode, String message, ConfigurationDetails configurationDetails);
    }

    public interface MessageCallback {
        void onComplete(boolean status, int statusCode, String message, MessageDetails messageDetails);
    }

    public interface MessagesCallback {
        void onComplete(boolean status, int statusCode, String message, List<MessageDetails> messagesList);
    }

    public interface ReferralOfferCallback{
        void onComplete(boolean status, int statusCode, String message, ReferralOfferDetails referralOfferDetails);
    }

    public interface MembershipsCallback{
        void onComplete(boolean status, int statusCode, String message, List<MembershipDetails> membershipsList);
    }
    public interface MembershipCallback{
        void onComplete(boolean status, int statusCode, String message, MembershipDetails membershipDetails);
    }


    public ConfigurationsAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }


    public void getConfiguration(final ConfigurationCallback callback){

        retrofitInterface.getConfiguration(deviceID, appVersion, new Callback<ConfigurationDetails>() {
            @Override
            public void success(ConfigurationDetails configurationDetails, Response response) {
                callback.onComplete(true, 200, "Success", configurationDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateConfiguration(final ConfigurationDetails configurationDetails, final StatusCallback callback){

        retrofitInterface.updateConfiguration(deviceID, appVersion, configurationDetails, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getAlertMessages(final MessagesCallback callback){

        retrofitInterface.getAlertMessages(deviceID, appVersion, new Callback<List<MessageDetails>>() {
            @Override
            public void success(List<MessageDetails> messagesList, Response response) {
                callback.onComplete(true, 200, "Success", messagesList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addAlertMessage(RequestAddOrUpdateAlertMessage requestObject, final MessageCallback callback){

        retrofitInterface.addAlertMessage(deviceID, appVersion
                , requestObject.getType()
                , requestObject.getStatus(), requestObject.getTitle(), requestObject.getDetails(), requestObject.getImage()
                , requestObject.getURL(), requestObject.getAppURL(), requestObject.getAction()
                , requestObject.getStartDate(), requestObject.getEndDate(), requestObject.showOnce()
                , requestObject.getAttachment()!=null?new TypedFile("multipart/form-data", requestObject.getAttachment()):null
                , new Callback<MessageDetails>() {
                    @Override
                    public void success(MessageDetails messageDetails, Response response) {
                        callback.onComplete(true, 200, "Success", messageDetails);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (new CheckError(context).check(error) == false) {
                            try {
                                callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                            } catch (Exception e) {
                                callback.onComplete(false, -1, "Error", null);
                            }
                        }
                    }
                });
    }

    public void updateAlertMessage(String messageID, RequestAddOrUpdateAlertMessage requestObject, final MessageCallback callback){

        retrofitInterface.updateAlertMessage(deviceID, appVersion, messageID
                , requestObject.getType()
                , requestObject.getStatus(), requestObject.getTitle(), requestObject.getDetails(), requestObject.getImage()
                , requestObject.getURL(), requestObject.getAppURL(), requestObject.getAction()
                , requestObject.getStartDate(), requestObject.getEndDate(), requestObject.showOnce()
                , requestObject.getAttachment()!=null?new TypedFile("multipart/form-data", requestObject.getAttachment()):null
                , new Callback<MessageDetails>() {
                    @Override
                    public void success(MessageDetails messageDetails, Response response) {
                        callback.onComplete(true, 200, "Success", messageDetails);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (new CheckError(context).check(error) == false) {
                            try {
                                callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                            } catch (Exception e) {
                                callback.onComplete(false, -1, "Error", null);
                            }
                        }
                    }
                });
    }

    public void deleteAlertMessage(String messageID, final StatusCallback callback){

        retrofitInterface.deleteAlertMessage(deviceID, appVersion, messageID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getReferralOfferDetails(final ReferralOfferCallback callback){

        retrofitInterface.getReferralOfferDetails(deviceID, appVersion, new Callback<ReferralOfferDetails>() {
            @Override
            public void success(ReferralOfferDetails referralOfferDetails, Response response) {
                callback.onComplete(true, 200, "Success", referralOfferDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addReferralOffer(final ReferralOfferDetails referralOffer, final ReferralOfferCallback callback){

        retrofitInterface.addReferralOffer(deviceID, appVersion, referralOffer, new Callback<ReferralOfferDetails>() {
            @Override
            public void success(ReferralOfferDetails referralOfferDetails, Response response) {
                callback.onComplete(true, 200, "Success", referralOfferDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateReferralOffer(final String referralOfferID, final ReferralOfferDetails referralOffer, final StatusCallback callback){

        retrofitInterface.updateReferralOffer(deviceID, appVersion, referralOfferID, referralOffer, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


    public void updateAllBuyersMemberships(final StatusCallback callback){

        retrofitInterface.updateAllBuyersMemberships(deviceID, appVersion, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getMemberships(final MembershipsCallback callback){

        retrofitInterface.getMemberships(deviceID, appVersion, new Callback<List<MembershipDetails>>() {
            @Override
            public void success(List<MembershipDetails> membershipsList, Response response) {
                callback.onComplete(true, 200, "Success", membershipsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addMembership(final MembershipDetails membership, final MembershipCallback callback){

        retrofitInterface.addMembership(deviceID, appVersion, membership, new Callback<MembershipDetails>() {
            @Override
            public void success(MembershipDetails membershipDetails, Response response) {
                callback.onComplete(true, 200, "Success", membershipDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateMembership(final String membershipID, final MembershipDetails membershipDetails, final StatusCallback callback){

        retrofitInterface.updateMembership(deviceID, appVersion, membershipID, membershipDetails, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void deleteMembership(final String membershipID, final StatusCallback callback){

        retrofitInterface.deleteMembership(deviceID, appVersion, membershipID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}
