package com.mastaan.logistics.backend;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAddComment;
import com.mastaan.logistics.backend.models.RequestAmountCollected;
import com.mastaan.logistics.backend.models.RequestCancelDelivery;
import com.mastaan.logistics.backend.models.RequestCheckForCancelOrderItem;
import com.mastaan.logistics.backend.models.RequestCompleteDelivery;
import com.mastaan.logistics.backend.models.RequestConfirmItemsPickup;
import com.mastaan.logistics.backend.models.RequestCreateCounterSaleOrder;
import com.mastaan.logistics.backend.models.RequestCreateJob;
import com.mastaan.logistics.backend.models.RequestCreateNewOrderForOrderItem;
import com.mastaan.logistics.backend.models.RequestEditOrderItem;
import com.mastaan.logistics.backend.models.RequestFailOrderItem;
import com.mastaan.logistics.backend.models.RequestFailQualityCheck;
import com.mastaan.logistics.backend.models.RequestMarkAsBillsPrinted;
import com.mastaan.logistics.backend.models.RequestReplaceOrderItem;
import com.mastaan.logistics.backend.models.RequestUpdateCounterSaleOrderItem;
import com.mastaan.logistics.backend.models.RequestUpdateDiscountForOrder;
import com.mastaan.logistics.backend.models.RequestUpdateNetWeight;
import com.mastaan.logistics.backend.models.RequestUpdateOrderDeliveryDate;
import com.mastaan.logistics.backend.models.RequestUpdateOrderItemButcher;
import com.mastaan.logistics.backend.models.RequestUpdateOrderItemStock;
import com.mastaan.logistics.backend.models.RequestUpdateOrderItemVendor;
import com.mastaan.logistics.backend.models.RequestUpdateOrderStatus;
import com.mastaan.logistics.backend.models.RequestUpdateSpecialInstructions;
import com.mastaan.logistics.backend.models.ResponseCheckAmountTobeCollected;
import com.mastaan.logistics.backend.models.ResponseDBY;
import com.mastaan.logistics.backend.models.ResponseMarkAsDelivered;
import com.mastaan.logistics.backend.models.TempOrder;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemDetailsCallback;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.interfaces.OrdersListCallback;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.DeliveryZoneDetails;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.Job;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class OrdersAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

//    public interface GroupedDeliveriesCallback {
//        void onComplete(boolean status, List<GroupedOrdersItemsList> items, int status_code);
//    }

    public interface OrdersAndGroupedOrderItemsCallback {
        void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<GroupedOrdersItemsList> groupedOrderItemsLists);
    }

    public interface OrderDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<OrderItemDetails> orderItemsList);
    }

    public interface MarkAsDeliveredCallback{
        void onComplete(boolean status, int statusCode, String message, List<String> orderItemsMarkedAsDelivered);
    }

    public interface CheckAmountTobeCollectedCallback{
        void onComplete(boolean status, int statusCode, String message, boolean collectAmount, double amountToBeCollected);
    }

    public interface BuyerOrdersListCallback{
        void onComplete(boolean status, int statusCode, String message, List<TempOrder> ordersList);
    }

    public interface CreateNewOrderForOrderItem{
        void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails);//String deliveryDate, OrderDetails2 orderDetails2);
    }

    public interface MergeOrderItemToOrderCallback{
        void onComplete(boolean status, int statusCode, String message, String deliveryDate, OrderDetails2 orderDetails2);
    }

    public interface ReProcessOrderItemCallback {
        void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails);
    }

    public  interface EditOrderItemCallback{
        void onComplete(boolean status, int statusCode, String message, OrderItemDetails modifiedOrderItemDetails);
    }

    public interface ClaimDeliveryCallback{
        void onComplete(boolean status, int statusCode, String message, Job jobDetails);
    }

    public interface CashNCarryItemsCallback{
        void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> itemsList);
    }

    public interface DeliveryZonesCallback {
        void onComplete(boolean status, int statusCode, String message, List<DeliveryZoneDetails> itemsList);
    }


    public OrdersAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        this.localStorageData = new LocalStorageData(context);
    }


    public void getOrderItemDetails(final String orderItemID, final OrderItemDetailsCallback callback){

        retrofitInterface.getOrderItemDetails(deviceID, appVersion, orderItemID, new retrofit.Callback<OrderItemDetails>() {
            @Override
            public void success(final OrderItemDetails orderItemDetails, final Response response) {

                callback.onComplete(true, 200, "Success", orderItemDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getOrderItems(final String orderID, final OrdersAndGroupedOrderItemsCallback callback){

        retrofitInterface.getOrderItems(deviceID, appVersion, orderID, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, null, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                }else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getOrderItems(final String orderID, final OrderDetailsCallback callback){

        retrofitInterface.getOrderItems(deviceID, appVersion, orderID, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> orderItems, final Response response) {
                if (orderItems != null) {
                    GroupingMethods.getOrdersFromOrderItems(orderItems, new OrdersListCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList) {
                            if (status) {
                                callback.onComplete(true, 200, "Success", ordersList, orderItems);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, Constants.code.response.UN_KNOWN, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getDeliveryBoyRelatedItems(final String orderID, final OrderItemsListCallback callback){

        retrofitInterface.getDeliveryBoyRelatedItems(deviceID, appVersion, orderID, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(List<OrderItemDetails> items, Response response) {
                if (items != null) {
                    callback.onComplete(true, 200, "Success", items);
                } else {
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getMyPendigDeliveryOrderItems(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getMyPendigDeliveryOrderItems(deviceID, appVersion, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    if ( items.size() > 0 ){
                        final List<OrderItemDetails> myPendringDeliveryOrderItems = new ArrayList<>();
                        String myID = localStorageData.getUserID();
                        for (OrderItemDetails orderItemDetails : items) {
                            if (orderItemDetails.getDeliveryBoyID() != null && orderItemDetails.getDeliveryBoyID().equals(myID)) {
                                myPendringDeliveryOrderItems.add(orderItemDetails);
                            }
                        }
                        GroupingMethods.sortOrderItems(localStorageData.getServerTime(), myPendringDeliveryOrderItems, location, new OrderItemsListCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                                List<GroupedOrdersItemsList> groupedOrdersItemsLists = new ArrayList<GroupedOrdersItemsList>();
                                groupedOrdersItemsLists.add(new GroupedOrdersItemsList("ALL", myPendringDeliveryOrderItems));

                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsLists);
                            }
                        });
                    }else{
                        callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), new ArrayList<GroupedOrdersItemsList>());
                    }

                } else {
                    callback.onComplete(false, response.getStatus(), "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getPendingDeliveryOrderItems(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback){

        retrofitInterface.getPendingDeliveryOrderItems(deviceID, appVersion, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getAssignDeliveryItems(final OrderItemsListCallback callback){

        retrofitInterface.getAssignDeliveryItems(deviceID, appVersion, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> itemsList, final Response response) {
                if (itemsList != null) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            // SORTING GROUPS BY DATE
                            Collections.sort(itemsList, new Comparator<OrderItemDetails>() {
                                public int compare(OrderItemDetails item1, OrderItemDetails item2) {
                                    return item1.getOrderDetails().getFormattedOrderID().compareToIgnoreCase(item2.getOrderDetails().getFormattedOrderID());
                                }
                            });
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            callback.onComplete(true, response.getStatus(), "Success", itemsList);
                        }
                    }.execute();
                } else {
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getPendingQCItems(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getPendingQCItems(deviceID, appVersion, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getPendingProcessingItems(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getPendingProcessingItems(deviceID, appVersion, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getPendingProcessingZones(final DeliveryZonesCallback callback) {

        retrofitInterface.getPendingProcessingZones(deviceID, appVersion, new Callback<List<DeliveryZoneDetails>>() {
            @Override
            public void success(final List<DeliveryZoneDetails> pendingDeliveryZonesList, final Response response) {
                callback.onComplete(true, 200, "Success", pendingDeliveryZonesList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getPendingProcessingGroupItems(final String groupID, final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getPendingProcessingGroupItems(deviceID, appVersion, groupID, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getNewOrderItems(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getNewOrderItems(deviceID, appVersion, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getTodaysOrders(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getTodaysOrders(deviceID, appVersion, location.latitude + "," + location.longitude, new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> ordersList, final Response response) {
                if (ordersList != null) {
                    GroupingMethods.getOrderItemsFromOrders(ordersList, new OrderItemsListCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                            if (status) {
                                GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), orderItemsList, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                                    @Override
                                    public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsLists) {
                                        if (status) {
                                            callback.onComplete(true, response.getStatus(), "Success", ordersList, groupedOrdersItemsLists);
                                            Log.e("mobile num",""+response.getBody().toString());
                                        } else {
                                            callback.onComplete(false, 500, "Failure", null, null);
                                        }
                                    }
                                });
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getTodaysOrderItems(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getTodaysOrderItems(deviceID, appVersion, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getUnprintedOrders(final OrdersListCallback callback){
        retrofitInterface.getUnprintedOrders(deviceID, appVersion, new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> ordersList, Response response) {
                new AsyncTask<Void, Void, Void>() {
                    List<OrderDetails> unprintedBills = new ArrayList<>();
                    @Override
                    protected Void doInBackground(Void... voids) {
                        if ( ordersList != null && ordersList.size() > 0 ){
                            for (int i=0;i<ordersList.size();i++){
                                try{
                                    OrderDetails orderDetails = ordersList.get(i);
                                    if ( orderDetails != null ){
                                        if ( orderDetails.getStatus().equalsIgnoreCase(Constants.PENDING) ){
                                            boolean isUnprintedBill = false;
                                            for (int j=0;j<orderDetails.getOrderedItems().size();j++){
                                                if ( orderDetails.getOrderedItems().get(j) != null ){
                                                    String orderItemStatus = orderDetails.getOrderedItems().get(j).getStatusString();
                                                    if ( /*orderItemStatus.equalsIgnoreCase(Constants.PROCESSING)
                                                            || orderItemStatus.equalsIgnoreCase(Constants.PROCESSED)
                                                            || */orderItemStatus.equalsIgnoreCase(Constants.ASSIGNED)
                                                            || orderItemStatus.equalsIgnoreCase(Constants.PICKED_UP)
                                                            || orderItemStatus.equalsIgnoreCase(Constants.DELIVERING)
                                                            ){
                                                        isUnprintedBill = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            // ADDING TO UNPRINTED BILLS IF ATLEASE ONE ITEM STATUS > PROCESSING
                                            if ( isUnprintedBill ){
                                                unprintedBills.add(orderDetails);
                                            }
                                        }
                                    }
                                }catch (Exception e){}
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        callback.onComplete(true, 200, "Success", unprintedBills);
                    }
                }.execute();
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void markAsBillsPrinted(final RequestMarkAsBillsPrinted markAsBillsPrinted, final StatusCallback callback){
        retrofitInterface.markAsBillsPrinted(deviceID, appVersion, markAsBillsPrinted, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getFutureOrders(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getFutureOrders(deviceID, appVersion, location.latitude + "," + location.longitude, new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> ordersList, final Response response) {
                if (ordersList != null) {
                    GroupingMethods.getOrderItemsFromOrders(ordersList, new OrderItemsListCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                            if (status) {
                                GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), orderItemsList, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                                    @Override
                                    public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsLists) {
                                        if (status) {
                                            callback.onComplete(true, response.getStatus(), "Success", ordersList, groupedOrdersItemsLists);
                                        } else {
                                            callback.onComplete(false, 500, "Failure", null, null);
                                        }
                                    }
                                });
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getFutureOrderItems(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getFutureOrderItems(deviceID, appVersion, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            }else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                }else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getRejectedOrderItems(final OrderItemsListCallback callback) {

        retrofitInterface.getRejectedOrderItems(deviceID, appVersion, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    callback.onComplete(true, 200, "Success", items);
                } else {
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }



    public interface OrdersForDateCallback{
        void onComplete(boolean status, int statusCode, String message, String loadedDate, List<GroupedOrdersItemsList> orderItemsList , List<OrderDetails> ordersList);
    }
    public void getOrdersForDate(final String date, final LatLng location, final OrdersForDateCallback callback) {

        final Calendar calendar = DateMethods.getCalendarFromDate(date);

        retrofitInterface.getOrdersForDate(deviceID, appVersion, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR), location.latitude + "," + location.longitude, new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> ordersList, final Response response) {
                if (ordersList != null) {
                    GroupingMethods.getOrderItemsFromOrders(ordersList, new OrderItemsListCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                            if (status) {
                                GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), orderItemsList, null, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                                    @Override
                                    public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                                        if (status) {
                                            callback.onComplete(true, 200, "Success", date, groupedOrdersItemsListList, ordersList);
                                        }else {
                                            callback.onComplete(false, 500, "Failure", date, null, null);
                                        }
                                    }
                                });
                            } else {
                                callback.onComplete(false, 500, "Failure", date, null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", date, null, null);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", date, null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", date, null, null);
                    }
                }
            }
        });
    }

    public void getFirstTimeOrdersForDate(final String date, final OrdersForDateCallback callback) {

        final Calendar calendar = DateMethods.getCalendarFromDate(date);

        retrofitInterface.getFirstTimeOrdersForDate(deviceID, appVersion, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR), new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> ordersList, final Response response) {
                if (ordersList != null) {
                    GroupingMethods.getOrderItemsFromOrders(ordersList, new OrderItemsListCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                            if (status) {
                                GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), orderItemsList, null, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                                    @Override
                                    public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                                        if (status) {
                                            callback.onComplete(true, 200, "Success", date, groupedOrdersItemsListList, ordersList);
                                        }else {
                                            callback.onComplete(false, 500, "Failure", date, null, null);
                                        }
                                    }
                                });
                            } else {
                                callback.onComplete(false, 500, "Failure", date, null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", date, null, null);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", date, null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", date, null, null);
                    }
                }
            }
        });
    }

    public void getPendingFollowupFirstOrdersForDeliveryDate(final String date, final OrdersForDateCallback callback) {

        final Calendar calendar = DateMethods.getCalendarFromDate(date);

        retrofitInterface.getPendingFollowupFirstOrdersForDeliveryDate(deviceID, appVersion, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH)+1, calendar.get(Calendar.YEAR), new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> ordersList, final Response response) {
                if (ordersList != null) {
                    GroupingMethods.getOrderItemsFromOrders(ordersList, new OrderItemsListCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                            if (status) {
                                GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), orderItemsList, null, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                                    @Override
                                    public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                                        if (status) {
                                            callback.onComplete(true, 200, "Success", date, groupedOrdersItemsListList, ordersList);
                                        }else {
                                            callback.onComplete(false, 500, "Failure", date, null, null);
                                        }
                                    }
                                });
                            } else {
                                callback.onComplete(false, 500, "Failure", date, null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", date, null, null);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", date, null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", date, null, null);
                    }
                }
            }
        });
    }

    public void getPendingCashNCarryItems(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getPendingCashNCarryItems(deviceID, appVersion, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getAllCashNCarryItems(final LatLng location, final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getAllCashNCarryItems(deviceID, appVersion, location.latitude + "," + location.longitude, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, location, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", new ArrayList<OrderDetails>(), groupedOrdersItemsListList);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getCashNCarryItemsByDate(final String date, final CashNCarryItemsCallback callback) {

        int day=0,month=0,year=0;
        try{
            Calendar calendar = DateMethods.getCalendarFromDate(date);
            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH)+1;
            year = calendar.get(Calendar.YEAR);
        }catch (Exception e){}

        retrofitInterface.getCashNCarryItemsByDate(deviceID, appVersion, day, month, year, new retrofit.Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> items, final Response response) {
                if (items != null) {
                    GroupingMethods.groupAndSortOrderItemsByCategory(localStorageData.getServerTime(), items, null, new GroupingMethods.GroupAndSortOrderItemsByCategoryCallback() {
                        @Override
                        public void onComplete(boolean status, List<GroupedOrdersItemsList> groupedOrdersItemsListList) {
                            if (status) {
                                callback.onComplete(true, response.getStatus(), "Success", items);
                            } else {
                                callback.onComplete(false, 500, "Failure", null);
                            }
                        }
                    });
                } else {
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getPastPendingOrders(final OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getPastPendingOrders(deviceID, appVersion, new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> ordersList, final Response response) {
                if (ordersList != null) {
                    callback.onComplete(true, 200, "Success", ordersList, null);
                } else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getPendingDeliveryZones(final DeliveryZonesCallback callback) {

        retrofitInterface.getPendingDeliveryZones(deviceID, appVersion, new Callback<List<DeliveryZoneDetails>>() {
            @Override
            public void success(final List<DeliveryZoneDetails> pendingDeliveryZonesList, final Response response) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            Collections.sort(pendingDeliveryZonesList, new Comparator<DeliveryZoneDetails>() {
                                public int compare(DeliveryZoneDetails item1, DeliveryZoneDetails item2) {
                                    return item1.getName().compareToIgnoreCase(item2.getName());
                                }
                            });
                        }catch (Exception e){}
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        callback.onComplete(true, 200, "Success", pendingDeliveryZonesList);
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getDeliveryGroupItems(final String groupID, final OrderItemsListCallback callback) {

        retrofitInterface.getDeliveryGroupItems(deviceID, appVersion, groupID, new Callback<List<OrderItemDetails>>() {
            @Override
            public void success(final List<OrderItemDetails> orderItemsList, final Response response) {
                GroupingMethods.sortOrderItems(localStorageData.getServerTime(), orderItemsList, null, new OrderItemsListCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                        callback.onComplete(true, 200, "Success", orderItemsList);
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    //----

    public void assignHubToOrder(final String orderID, final String hubID, final StatusCallback callback) {

        retrofitInterface.assignHubToOrder(deviceID, appVersion, orderID, hubID, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void markAsAcknowledged(final String itemID/*, final Location location*/, final StatusCallback callback) {

        retrofitInterface.markAsAcknowledged(deviceID, appVersion, itemID/*, location.getLatitude()+","+location.getLongitude()*/, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void markAsProcessing(/*final double latitude, final double longitude, */String order_item_id, final StatusCallback callback) {
//        Map<String, Object> map = new HashMap<>();
//        map.put("loc", latitude + "," + longitude);

        retrofitInterface.markAsProcessing(deviceID, appVersion, order_item_id/*, map*/, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void confirmItemsPickup(RequestConfirmItemsPickup requestConfirmItemsPickup, final StatusCallback callback){

        retrofitInterface.confirmItemsPickup(deviceID, appVersion, requestConfirmItemsPickup, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void claimDelivery(final String orderID, final Location location, Job job, final ClaimDeliveryCallback callback) {

        final LocalStorageData localStorageData = new LocalStorageData(context);
        String requestID = localStorageData.getCreateJobRequestID();
        if ( requestID == null || requestID.length() == 0 ) {
            requestID = Base64.encodeToString((localStorageData.getUserID() + ":CJ" + System.currentTimeMillis()).getBytes(), Base64.NO_WRAP);
            localStorageData.setCreateJobRequestID(requestID);     // Storing RequestID in LocalStorage
        }

        retrofitInterface.claimDelivery(deviceID, appVersion, orderID, new RequestCreateJob(requestID, location.getLatitude(), location.getLongitude(), job), new Callback<Job>() {
            @Override
            public void success(Job jobDetails, Response response) {
                localStorageData.setCreateJobRequestID("");        // Clearing RequestID from LocalStorage
                callback.onComplete(true, 200, "Success", jobDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void markAsNew(String order_item_id, final StatusCallback callback) {

        retrofitInterface.markAsNew(deviceID, appVersion, order_item_id, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void removeProcessingBy(String order_item_id, final StatusCallback callback) {

        retrofitInterface.removeProcessingBy(deviceID, appVersion, order_item_id, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void removeDeliveryBoy(String order_item_id, final StatusCallback callback) {

        retrofitInterface.removeDeliveryBoy(deviceID, appVersion, order_item_id, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }
    public void markAsCashNCarryItem(String order_item_id, final StatusCallback callback) {

        retrofitInterface.markAsCashNCarryItem(deviceID, appVersion, order_item_id, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void unmarkAsCashNCarryItem(String order_item_id, final StatusCallback callback) {

        retrofitInterface.unmarkAsCashNCarryItem(deviceID, appVersion, order_item_id, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateNetWeight(String orderItemID, double netWeight, final StatusCallback callback) {

        retrofitInterface.updateNetWeight(deviceID, appVersion, orderItemID, new RequestUpdateNetWeight(netWeight), new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void resetQualityCheck(String orderItemID, final StatusCallback callback) {

        retrofitInterface.resetQualityCheck(deviceID, appVersion, orderItemID, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void completeQualityCheck(String orderItemID, final StatusCallback callback) {

        retrofitInterface.completeQualityCheck(deviceID, appVersion, orderItemID, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void failQualityCheck(String orderItemID, String failReason, final StatusCallback callback) {

        retrofitInterface.failQualityCheck(deviceID, appVersion, orderItemID, new RequestFailQualityCheck(failReason), new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }


    public void updateAlternateMobile(String orderID, String alternateMobile, final StatusCallback callback) {

        retrofitInterface.updateAlternateMobile(deviceID, appVersion, orderID, alternateMobile, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void markAsPickUpPending(String orderitem_id/*, double latitude, double longitude*/, final StatusCallback callback) {
//        Map<String, Object> map = new HashMap<>();
//        map.put(Constants.param.Location, latitude + "," + longitude);

        retrofitInterface.markAsPickUpPending(deviceID, appVersion, orderitem_id/*, map*/, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK"))
                    callback.onComplete(true, response.getStatus(), "Success");
                else if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("delivery_already_claimed"))
                    callback.onComplete(false, Constants.code.response.NOT_AVAILABLE, "Failure");
                else
                    callback.onComplete(false, response.getStatus(), "Failure");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        ResponseStatus responseStatus = new Gson().fromJson(json, ResponseStatus.class);
                        if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("delivery_already_claimed"))
                            callback.onComplete(false, Constants.code.response.NOT_AVAILABLE, "Failure");
                        else
                            callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void completeDelivery(String orderID, RequestCompleteDelivery requestCompleteDelivery, final MarkAsDeliveredCallback callback) {

        retrofitInterface.completeDelivery(deviceID, appVersion, orderID, requestCompleteDelivery, new retrofit.Callback<ResponseMarkAsDelivered>() {
            @Override
            public void success(ResponseMarkAsDelivered responseObject, Response response) {
                callback.onComplete(true, response.getStatus(), "Success", responseObject.getOrderItemsMarkedAsDelivered());
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void cancelDelivery(final String orderItemID, final String cancelReason, final StatusCallback callback){

        retrofitInterface.cancelDelivery(deviceID, appVersion, orderItemID, new RequestCancelDelivery(cancelReason), new Callback<ResponseMarkAsDelivered>() {
            @Override
            public void success(ResponseMarkAsDelivered responseMarkAsDelivered, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void checkAmountTobeCollected(String orderitem_id, final CheckAmountTobeCollectedCallback callback) {

        retrofitInterface.checkAmountTobeCollected(deviceID, appVersion, orderitem_id, new retrofit.Callback<ResponseCheckAmountTobeCollected>() {
            @Override
            public void success(ResponseCheckAmountTobeCollected responseObject, Response response) {
                callback.onComplete(true, response.getStatus(), "Success", responseObject.getCollecteAmountStatus(), responseObject.getAmountToBeCollected());
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", false, 0);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", false, 0);
                    }
                }
            }
        });
    }

    public void updateOrderAmountCollected(String orderitem_id, String amountCollected, final StatusCallback callback) {

        retrofitInterface.updateOrderAmountCollected(deviceID, appVersion, orderitem_id, new RequestAmountCollected(amountCollected), new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void markCounterSaleAsDelivered(final String orderID, final StatusCallback callback){

        retrofitInterface.markCounterSaleAsDelivered(deviceID, appVersion, orderID, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });

    }

    public void updateCounterSaleOrderItem(final String orderItemID, final RequestUpdateCounterSaleOrderItem requestUpdateCounterSaleOrderItem, final StatusCallback callback){

        retrofitInterface.updateCounterSaleOrderItem(deviceID, appVersion, orderItemID, requestUpdateCounterSaleOrderItem, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });

    }


//    public void claimDelivery(String item_id, double latitude, double longitude, Job job, String last_loc, final StatusCallback callback) {
//
//        retrofitInterface.claimDelivery(deviceID, appVersion, item_id, new RequestCreateJob(latitude, longitude, last_loc), new retrofit.Callback<ResponseStatus>() {
//            @Override
//            public void success(ResponseStatus responseStatus, Response response) {
//                if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("OK"))
//                    callback.onComplete(true, response.getStatus(), "Success");
//                else if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("delivery_already_claimed"))
//                    callback.onComplete(false, Constants.code.response.NOT_AVAILABLE, "Failure");
//                else
//                    callback.onComplete(false, response.getStatus(), "Failure");
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                if (new CheckError(context).check(error) == false) {
//                    try {
//                        String json = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
//                        ResponseStatus responseStatus = new Gson().fromJson(json, ResponseStatus.class);
//                        if (responseStatus != null && responseStatus.getCode() != null && responseStatus.getCode().equalsIgnoreCase("delivery_already_claimed"))
//                            callback.onComplete(false, Constants.code.response.NOT_AVAILABLE, "Failure");
//                        else
//                            callback.onComplete(false, error.getResponse().getStatus(), "Error");
//                    } catch (Exception e) {
//                        callback.onComplete(false, -1, "Error");
//                    }
//                }
//            }
//        });
//    }

    //-------

    public void editOrderItem(final String orderItemID, final RequestEditOrderItem requestEditOrderItem, final EditOrderItemCallback callback){

        retrofitInterface.editOrderItem(deviceID, appVersion, orderItemID, requestEditOrderItem, new Callback<OrderItemDetails>() {
            @Override
            public void success(OrderItemDetails orderItemDetails, Response response) {
                if ( orderItemDetails != null ) {
                    callback.onComplete(true, 200, "Success", orderItemDetails);
                }else{
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateOrderItemQuantity(final String itemID, final double newQuantity, final StatusCallback callback) {

        retrofitInterface.updateOrderItemQuantity(deviceID, appVersion, itemID, newQuantity, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void replaceOrderItem(final String itemID, final String replaceInstructions, final StatusCallback callback) {

        retrofitInterface.replaceOrderItem(deviceID, appVersion, itemID, new RequestReplaceOrderItem(replaceInstructions), new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void deleteOrderItem(final String itemID, final StatusCallback callback) {

        retrofitInterface.deleteOrderItem(deviceID, appVersion, itemID, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void checkForCancelOrderItem(String orderItemID, String buyerID, final StatusCallback callback){

        retrofitInterface.checkForCancelOrderItem(deviceID, appVersion, localStorageData.getAccessToken(), orderItemID, new RequestCheckForCancelOrderItem(buyerID), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if ( responseStatus.getCode().equalsIgnoreCase("OK") ){
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ CHECK_FOR_CANCEL_ORDER_ITEM ]  >>  Success");
                    callback.onComplete(true, 200, "Success");
                }else{
                    Log.d(Constants.LOG_TAG, "\nORDER_API : [ CHECK_FOR_CANCEL_ORDER_ITEM ]  >>  Failure");
                    callback.onComplete(false, 500, responseStatus.getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ CHECK_FOR_CANCEL_ORDER_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDER_API : [ CHECK_FOR_CACNEL_ORDER_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void failOrderItem(final String itemID, final RequestFailOrderItem requestFailOrderItem, final StatusCallback callback) {

        retrofitInterface.failOrderItem(deviceID, appVersion, itemID, requestFailOrderItem, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateOrderDeliveryDate(final String orderID, final String date, final String time, final StatusCallback callback) {

        retrofitInterface.updateOrderDeliveryDate(deviceID, appVersion, orderID, new RequestUpdateOrderDeliveryDate(date, time), new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateSpecialInstructionsoOfOrderItem(final String orderItemID, final String specialInstructions, final StatusCallback callback) {

        retrofitInterface.updateSpecialInstructionsoOfOrderItem(deviceID, appVersion, orderItemID, new RequestUpdateSpecialInstructions(specialInstructions), new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void reProcessOrderItem(final String orderItemID, final ReProcessOrderItemCallback callback) {

        retrofitInterface.reProcessOrderItem(deviceID, appVersion, orderItemID, new retrofit.Callback<OrderItemDetails>() {

            @Override
            public void success(OrderItemDetails orderItemDetails, Response response) {
                if (orderItemDetails != null)
                    callback.onComplete(true, 200, "Success", orderItemDetails);
                else {
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getBuyerOrdersForDate(final String buyerID, final int day, final int month, final int year, final BuyerOrdersListCallback callback){

        retrofitInterface.getBuyerOrdersForDate(deviceID, appVersion, buyerID, day, month, year, new Callback<List<TempOrder>>() {
            @Override
            public void success(List<TempOrder> ordersList, Response response) {
                callback.onComplete(true, 200, "Success", ordersList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }

    public void mergeOrderItemToOrder(final String orderItemID, final String orderID, final  MergeOrderItemToOrderCallback callback){

        retrofitInterface.mergeOrderItemToOrder(deviceID, appVersion, orderItemID, orderID, new Callback<ResponseDBY>() {
            @Override
            public void success(ResponseDBY responseDBY, Response response) {
                callback.onComplete(true, 200, "Success", responseDBY.getDeliveryDate(), responseDBY.getOrderDetails());
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void createNewOrderForOrderItem(final String orderItemID, final RequestCreateNewOrderForOrderItem requestObject, final CreateNewOrderForOrderItem callback) {

        retrofitInterface.createNewOrderForOrderItem(deviceID, appVersion, orderItemID, requestObject, new retrofit.Callback<OrderItemDetails>() {
            @Override
            public void success(OrderItemDetails orderItemDetails, Response response) {
                callback.onComplete(true, 200, "Success", orderItemDetails);//responseDBY.getDeliveryDate(), responseDBY.getOrderDetails());
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);//null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);//null, null);
                    }
                }
            }
        });
    }

    public void updateDiscountForOrder(final String orderID, RequestUpdateDiscountForOrder requestUpdateDiscountForOrder, final StatusCallback callback) {

        retrofitInterface.updateDiscountForOrder(deviceID, appVersion, orderID, requestUpdateDiscountForOrder, new retrofit.Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API : [ UPDATE_DISCOUNT ]  >>  Success");
                    callback.onComplete(true, 200, "Success");
                }else {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API : [ UPDATE_DISCOUNT ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        String response_message = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        if ( response_message == null || response_message.length() == 0 ){
                            response_message = "Error";
                        }
                        Log.d(Constants.LOG_TAG, "\nORDERS_API : [ UPDATE_DISCOUNT ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), response_message);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API : [ UPDATE_DISCOUNT ]  >>  Error");
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void removeDiscount(final String orderID, final String discountID, final StatusCallback callback){

        retrofitInterface.removeDiscount(deviceID, appVersion, orderID, discountID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateOrderStatus(final String orderID, final RequestUpdateOrderStatus requestUpdateOrderStatus, final StatusCallback callback){

        retrofitInterface.updateOrderStatus(deviceID, appVersion, orderID, requestUpdateOrderStatus, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok"))
                    callback.onComplete(true, 200, "Success");
                else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateVendorDetails(String orderItemID, RequestUpdateOrderItemVendor requestObject, final StatusCallback callback) {

        retrofitInterface.updateVendorDetails(deviceID, appVersion, orderItemID, requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, response.getStatus(), "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateButcherDetails(String orderItemID, String butcherID, final StatusCallback callback) {

        retrofitInterface.updateButcherDetails(deviceID, appVersion, orderItemID, new RequestUpdateOrderItemButcher(butcherID), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, response.getStatus(), "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateStockDetails(String orderItemID, RequestUpdateOrderItemStock requestObject, final StatusCallback callback) {

        retrofitInterface.updateStockDetails(deviceID, appVersion, orderItemID, requestObject, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, response.getStatus(), "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    //----------

    public void createCounterSaleOrder(final RequestCreateCounterSaleOrder requestCreateCounterSaleOrder, final OrderItemDetailsCallback callback){

        retrofitInterface.createCounterSaleOrder(deviceID, appVersion, requestCreateCounterSaleOrder, new Callback<OrderItemDetails>() {
            @Override
            public void success(OrderItemDetails orderItemDetails, Response response) {
                if (orderItemDetails != null ) {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API : [ CREATE_COUNTER_SALE_ORDER ]  >>  Success, oID: "+orderItemDetails.getID());
                    callback.onComplete(true, 200, "Success", orderItemDetails);
                }else {
                    Log.d(Constants.LOG_TAG, "\nORDERS_API : [ CREATE_COUNTER_SALE_ORDER ]  >>  Success");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API : [ CREATE_COUNTER_SALE_ORDER ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nORDERS_API : [ CREATE_COUNTER_SALE_ORDER ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }


    //-----

    public void addDelayedDeliveryComment(final String orderItemID, final String comment, final StatusCallback callback){

        retrofitInterface.addDelayedDeliveryComment(deviceID, appVersion, orderItemID, new RequestAddComment(comment), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("ok")) {
                    callback.onComplete(true, response.getStatus(), "Success");
                }else {
                    callback.onComplete(false, response.getStatus(), "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}

