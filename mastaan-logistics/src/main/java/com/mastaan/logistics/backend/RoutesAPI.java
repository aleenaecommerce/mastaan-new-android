package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateDeliveryArea;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateDeliveryZone;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateHousingSociety;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateHub;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateWarehouse;
import com.mastaan.logistics.backend.models.RequestAssignDeliveryArea;
import com.mastaan.logistics.backend.models.RequestAssignHousingSociety;
import com.mastaan.logistics.backend.models.RequestHousingSocieties;
import com.mastaan.logistics.backend.models.ResponseAssignDeliveryArea;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.DeliveryAreaDetails;
import com.mastaan.logistics.models.HousingSocietyDetails;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.WarehouseDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class RoutesAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface WarehousesCallback {
        void onComplete(boolean status, int statusCode, String message, List<WarehouseDetails> hubsList);
    }
    public interface WarehouseDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, WarehouseDetails warehouseDetails);
    }
    public interface HubsCallback {
        void onComplete(boolean status, int statusCode, String message, List<HubDetails> hubsList);
    }
    public interface HubDetailsCallback {
        void onComplete(boolean status, int statusCode, String message, HubDetails hubDetails);
    }
    public interface DeliveryZonesCallback {
        void onComplete(boolean status, int statusCode, String message, List<String> deliveryZones);
    }
    public interface DeliveryAreasCallback {
        void onComplete(boolean status, int statusCode, String message, List<DeliveryAreaDetails> deliveryAreas);
    }
    public interface DeliveryZoneCallback {
        void onComplete(boolean status, int statusCode, String message, String deliveryZone);
    }
    public interface DeliveryAreaCallback{
        void onComplete(boolean status, int statusCode, String message, DeliveryAreaDetails deliveryAreaDetails);
    }
    public interface AssignDeliveryAreaCallback {
        void onComplete(boolean status, int statusCode, String message, String deliveryID, String areaID);
    }
    public interface HousingSocietiesCallback {
        void onComplete(boolean status, int statusCode, String message, List<HousingSocietyDetails> housingSocieties);
    }
    public interface HousingSocietyCallback {
        void onComplete(boolean status, int statusCode, String message, HousingSocietyDetails housingSocietyDetails);
    }


    public RoutesAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }


    public void getWarehouses(final WarehousesCallback callback){

        retrofitInterface.getWarehouses(deviceID, appVersion, new Callback<List<WarehouseDetails>>() {
            @Override
            public void success(final List<WarehouseDetails> warehousesList, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ WAREHOUSES ]  >>  Success");
                callback.onComplete(true, 200, "Success", warehousesList);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ WAREHOUSES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ WAREHOUSES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addWarehouse(final RequestAddOrUpdateWarehouse requestObject, final WarehouseDetailsCallback callback){

        retrofitInterface.addWarehouse(deviceID, appVersion, requestObject, new Callback<WarehouseDetails>() {
            @Override
            public void success(WarehouseDetails warehouseDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_WAREHOUSE ]  >>  Success");
                callback.onComplete(true, 200, "Success", warehouseDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_WAREHOUSE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_WAREHOUSE ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateWarehouse(final String hubID, final RequestAddOrUpdateWarehouse requestObject, final WarehouseDetailsCallback callback){

        retrofitInterface.updateWarehouse(deviceID, appVersion, hubID, requestObject, new Callback<WarehouseDetails>() {
            @Override
            public void success(WarehouseDetails warehouseDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_WAREHOUSE ]  >>  Success");
                callback.onComplete(true, 200, "Success", warehouseDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_WAREHOUSE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_WAREHOUSE ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }


    public void getHubs(final HubsCallback callback){

        retrofitInterface.getHubs(deviceID, appVersion, new Callback<List<HubDetails>>() {
            @Override
            public void success(final List<HubDetails> hubsList, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ HUBS ]  >>  Success");
                callback.onComplete(true, 200, "Success", hubsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ HUBS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ HUBS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addHub(final RequestAddOrUpdateHub requestObject, final HubDetailsCallback callback){

        retrofitInterface.addHub(deviceID, appVersion, requestObject, new Callback<HubDetails>() {
            @Override
            public void success(HubDetails hubDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_HUB ]  >>  Success");
                callback.onComplete(true, 200, "Success", hubDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_HUB ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_HUB ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateHub(final String hubID, final RequestAddOrUpdateHub requestObject, final HubDetailsCallback callback){

        retrofitInterface.updateHub(deviceID, appVersion, hubID, requestObject, new Callback<HubDetails>() {
            @Override
            public void success(HubDetails hubDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_HUB ]  >>  Success");
                callback.onComplete(true, 200, "Success", hubDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_HUB ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_HUB ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void deleteHub(final String hubID, final StatusCallback callback){

        retrofitInterface.deleteHub(deviceID, appVersion, hubID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELETE_HUB ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELETE_HUB ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELETE_HUB ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getDeliveryZones(final DeliveryZonesCallback callback){

        retrofitInterface.getDeliveryGroups(deviceID, appVersion, new Callback<List<String>>() {
            @Override
            public void success(List<String> deliveryZonesList, Response response) {
                if ( deliveryZonesList != null ){
                    Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELIVERY_ZONES ]  >>  Success");
                    callback.onComplete(true, 200, "Success", deliveryZonesList);
                }else{
                    Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELIVERY_ZONES ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELIVERY_ZONES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELIVERY_ZONES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getDeliveryAreas(final DeliveryAreasCallback callback){

        retrofitInterface.getDeliveryAreas(deviceID, appVersion, new Callback<List<DeliveryAreaDetails>>() {
            @Override
            public void success(List<DeliveryAreaDetails> deliveryAreasList, Response response) {
                if ( deliveryAreasList != null ){
                    Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELIVERY_AREAS ]  >>  Success");
                    callback.onComplete(true, 200, "Success", deliveryAreasList);
                }else{
                    Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELIVERY_AREAS ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELIVERY_AREAS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELIVERY_AREAS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addDeliveryZone(final RequestAddOrUpdateDeliveryZone requestAddOrUpdateDeliveryZone, final DeliveryZoneCallback callback){

        retrofitInterface.addDeliveryZone(deviceID, appVersion, requestAddOrUpdateDeliveryZone, new Callback<String>() {
            @Override
            public void success(String deliveryGroupCode, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_DELIVERY_ZONE ]  >>  Success");
                callback.onComplete(true, 200, "Success", deliveryGroupCode);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_DELIVERY_ZONE ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_DELIVERY_ZONE ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addDeliveryArea(final RequestAddOrUpdateDeliveryArea requestAddOrUpdateDeliveryArea, final DeliveryAreaCallback callback){

        retrofitInterface.addDeliveryArea(deviceID, appVersion, requestAddOrUpdateDeliveryArea, new Callback<DeliveryAreaDetails>() {
            @Override
            public void success(DeliveryAreaDetails deliveryAreaDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_DELIVERY_AREA ]  >>  Success");
                callback.onComplete(true, 200, "Success", deliveryAreaDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_DELIVERY_AREA ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_DELIVERY_AREA ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateDeliveryArea(final String deliveryAreaID, final RequestAddOrUpdateDeliveryArea requestAddOrUpdateDeliveryArea, final DeliveryAreaCallback callback){

        retrofitInterface.updateDeliveryArea(deviceID, appVersion, deliveryAreaID, requestAddOrUpdateDeliveryArea, new Callback<DeliveryAreaDetails>() {
            @Override
            public void success(DeliveryAreaDetails deliveryAreaDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_DELIVERY_AREA ]  >>  Success");
                callback.onComplete(true, 200, "Success", deliveryAreaDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_DELIVERY_AREA ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_DELIVERY_AREA ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void assignOrderToDeliveryArea(final String orderID, final String deliveryAreaID, final AssignDeliveryAreaCallback callback){

        retrofitInterface.assignOrderToDeliveryArea(deviceID, appVersion, orderID, new RequestAssignDeliveryArea(deliveryAreaID), new Callback<ResponseAssignDeliveryArea>() {
            @Override
            public void success(ResponseAssignDeliveryArea responseAssignDeliveryArea, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ASSIGN_ORDER_TO_DELIVERY_AREA ]  >>  Success");
                callback.onComplete(true, 200, "Success", responseAssignDeliveryArea.getDeliveryID(), responseAssignDeliveryArea.getAreaID());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ASSIGN_ORDER_TO_DELIVERY_AREA ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ASSIGN_ORDER_TO_DELIVERY_AREA ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }


    public void getHousingSocieties(final HousingSocietiesCallback callback){
        getHousingSocieties(new RequestHousingSocieties(), callback);
    }
    public void getHousingSocieties(final RequestHousingSocieties requestHousingSocieties, final HousingSocietiesCallback callback){

        retrofitInterface.getHousingSocieties(deviceID, appVersion, requestHousingSocieties, new Callback<List<HousingSocietyDetails>>() {
            @Override
            public void success(List<HousingSocietyDetails> housingSocieties, Response response) {
                if ( housingSocieties != null ){
                    Log.d(Constants.LOG_TAG, "\nROUTES_API : [ HOUSING_SOCIETIES ]  >>  Success");
                    callback.onComplete(true, 200, "Success", housingSocieties);
                }else{
                    Log.d(Constants.LOG_TAG, "\nROUTES_API : [ HOUSING_SOCIETIES ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ HOUSING_SOCIETIES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ HOUSING_SOCIETIES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void addHousingSociety(final RequestAddOrUpdateHousingSociety requestObject, final HousingSocietyCallback callback){

        retrofitInterface.addHousingSociety(deviceID, appVersion, requestObject, new Callback<HousingSocietyDetails>() {
            @Override
            public void success(HousingSocietyDetails housingSocietyDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_HOUSING_SOCIETY ]  >>  Success");
                callback.onComplete(true, 200, "Success", housingSocietyDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_HOUSING_SOCIETY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ADD_HOUSING_SOCIETY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateHousingSociety(final String housingSocietyID, final RequestAddOrUpdateHousingSociety requestObject, final HousingSocietyCallback callback){

        retrofitInterface.updateHousingSociety(deviceID, appVersion, housingSocietyID, requestObject, new Callback<HousingSocietyDetails>() {
            @Override
            public void success(HousingSocietyDetails housingSocietyDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_HOUSING_SOCIETY ]  >>  Success");
                callback.onComplete(true, 200, "Success", housingSocietyDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_HOUSING_SOCIETY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ UPDATE_HOUSING_SOCIETY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void deleteHousingSociety(final String housingSocietyID, final StatusCallback callback){

        retrofitInterface.deleteHousingSociety(deviceID, appVersion, housingSocietyID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELETE_HOUSING_SOCIETY ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELETE_HOUSING_SOCIETY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ DELETE_HOUSING_SOCIETY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void assignOrderToHousingSociety(final String orderID, final String housingSocietyName, final StatusCallback callback){

        retrofitInterface.assignOrderToHousingSociety(deviceID, appVersion, orderID, new RequestAssignHousingSociety(housingSocietyName), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ASSIGN_ORDER_TO_HOUSING_SOCIETY ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ASSIGN_ORDER_TO_HOUSING_SOCIETY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nROUTES_API : [ ASSIGN_ORDER_TO_HOUSING_SOCIETY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}
