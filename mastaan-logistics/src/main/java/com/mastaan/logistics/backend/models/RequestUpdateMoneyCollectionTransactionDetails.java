package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 4/11/16.
 */
public class RequestUpdateMoneyCollectionTransactionDetails {
    double amt;
    int pt;
    String pid;

    public RequestUpdateMoneyCollectionTransactionDetails(double amount, int paymentType, String paymentID){
        this.amt = amount;
        this.pt = paymentType;
        this.pid = paymentID;
    }

    public double getAmount() {
        return amt;
    }

    public int getPaymentType() {
        return pt;
    }

    public String getPaymentID() {
        return pid;
    }
}
