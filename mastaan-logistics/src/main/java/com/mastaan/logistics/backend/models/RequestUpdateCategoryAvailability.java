package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 2/9/16.
 */

public class RequestUpdateCategoryAvailability {
    boolean en;
    String dt;
    String dm;

    public RequestUpdateCategoryAvailability(boolean isAvailable, String displayTitle, String displayMessage){
        this.en = isAvailable;
        this.dt = displayTitle;
        this.dm = displayMessage;
    }

    public boolean isEnabled() {
        return en;
    }

    public String getDisplayTitle() {
        return dt;
    }

    public String getDisplayMessage() {
        return dm;
    }
}
