package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 28/03/18.
 */

public class RequestRazorpayRefundAmount {
    long amount;

    public RequestRazorpayRefundAmount(long amount){
        this.amount = amount;
    }
}
