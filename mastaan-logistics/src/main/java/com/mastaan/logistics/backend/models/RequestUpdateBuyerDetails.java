package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 15/12/16.
 */
public class RequestUpdateBuyerDetails {
    String f;
    String e;

    public RequestUpdateBuyerDetails(String name, String email){
        this.f = name;
        this.e = email;
    }

    public String getName() {
        return f;
    }

    public String getEmail() {
        return e;
    }

}
