package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 21/01/18.
 */

public class RequestUpdateWarehouseMeatItem {
    boolean en;
    boolean a;
    String avst;
    String avet;
    String hlt;
    String so;
    double idx;

    public RequestUpdateWarehouseMeatItem(boolean enable, boolean availability, String availabilityStartTime, String availabilityEndTime, String highlightText, String source, double index){
        this.en = enable;
        this.a = availability;
        this.avst = availabilityStartTime==null?"":availabilityStartTime;
        this.avet = availabilityEndTime==null?"":availabilityEndTime;
        this.hlt = highlightText;
        this.so = source;
        this.idx = index;
    }

    public boolean isEnabled() {
        return en;
    }

    public boolean isAvailable() {
        return a;
    }

    public String getAvailabilityStartTime() {
        return avst;
    }

    public String getAvailabilityEndTime() {
        return avet;
    }

    public String getHighlightText() {
        return hlt;
    }

    public String getSource() {
        return so;
    }

    public double getIndex() {
        return idx;
    }

}
