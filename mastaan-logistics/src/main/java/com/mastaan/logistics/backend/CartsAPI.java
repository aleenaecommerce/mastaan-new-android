package com.mastaan.logistics.backend;

import android.content.Context;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAddComment;
import com.mastaan.logistics.models.BuyerSearchDetails;
import com.mastaan.logistics.models.CartDetails;
import com.mastaan.logistics.models.FollowupDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 12/11/16.
 */

public class CartsAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;


    public interface CartsCallback{
        void onComplete(boolean status, int statusCode, String message, List<CartDetails> cartsList);
    }
    public interface FollowupCallback{
        void onComplete(boolean status, int statusCode, String message, FollowupDetails followupDetails);
    }


    public CartsAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }


    public void getInactiveBuyerCarts(final BuyerSearchDetails buyerSearchDetails, final long pageNumber, final CartsCallback callback){

        retrofitInterface.getInactiveBuyerCarts(deviceID, appVersion, pageNumber, buyerSearchDetails, new Callback<List<CartDetails>>() {
            @Override
            public void success(List<CartDetails> cartsList, Response response) {
                callback.onComplete(true, 200, "Success", cartsList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void claimBuyerCartFollowup(final String cartID, final StatusCallback callback){

        retrofitInterface.claimBuyerCartFollowup(deviceID, appVersion, cartID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void releaseBuyerCartFollowup(final String cartID, final StatusCallback callback){

        retrofitInterface.releaseBuyerCartFollowup(deviceID, appVersion, cartID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void deleteBuyerCart(final String cartID, final StatusCallback callback){

        retrofitInterface.deleteBuyerCart(deviceID, appVersion, cartID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void completeBuyerCartFollowup(final String cartID, final String comments, final FollowupCallback callback){

        retrofitInterface.completeBuyerCartFollowup(deviceID, appVersion, cartID, new RequestAddComment(comments), new Callback<FollowupDetails>() {
            @Override
            public void success(FollowupDetails followupDetails, Response response) {
                callback.onComplete(true, 200, "Success", followupDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

}
