package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 22/10/16.
 */
public class RequestUpdateOptionAvailability2 {
    String _id;
    double pd;
    boolean a;
    String ar;

    public RequestUpdateOptionAvailability2(String optionID, double priceDifference, boolean availability, String availabilityReason){
        this._id = optionID;
        this.pd = priceDifference;
        this.a = availability;
        this.ar = availabilityReason;
    }
}
