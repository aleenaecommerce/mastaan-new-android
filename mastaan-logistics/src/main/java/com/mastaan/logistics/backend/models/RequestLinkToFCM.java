package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 31/08/18.
 */

public class RequestLinkToFCM {

    public String rid;

    public RequestLinkToFCM(String fcmRegistrationID){
        rid = fcmRegistrationID;
    }

}
