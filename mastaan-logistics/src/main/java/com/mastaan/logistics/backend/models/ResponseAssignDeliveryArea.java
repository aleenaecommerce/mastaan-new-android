package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 25/3/17.
 */
public class ResponseAssignDeliveryArea {
    String did;
    String ar;

    public String getDeliveryID() {
        return did;
    }

    public String getAreaID() {
        return ar;
    }
}
