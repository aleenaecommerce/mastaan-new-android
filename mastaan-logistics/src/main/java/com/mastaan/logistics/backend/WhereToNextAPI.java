package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Base64;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestWhereToNext;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.Job;

import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class WhereToNextAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface WhereToNextCallback{
        void onComplete(boolean status, int statusCode, String message, Job jobDetails);
    }

    public WhereToNextAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void whereToNext(String id, double latitude, double longitude, String my_id, final WhereToNextCallback callback) {

        final LocalStorageData localStorageData = new LocalStorageData(context);
        String requestID = localStorageData.getWhereToNextRequestID();
        if ( requestID == null || requestID.length() == 0 ) {
            requestID = Base64.encodeToString((localStorageData.getUserID()+":WTN"+System.currentTimeMillis()).getBytes(), Base64.NO_WRAP);
            localStorageData.setWhereToNextRequestID(requestID);     // Storing RequestID in LocalStorage
        }

        RequestWhereToNext requestObject = new RequestWhereToNext(requestID, latitude, longitude, new Job(my_id));
        retrofitInterface.whereToNext(deviceID, appVersion, id, requestObject, new Callback<Job>() {
            @Override
            public void success(Job jobDetails, Response response) {
                localStorageData.setWhereToNextRequestID("");        // Clearing RequestID from LocalStorage
                callback.onComplete(true, 200, "Success", jobDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        String error_message = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        if ( error_message == null || error_message.length() == 0 ){
                            error_message = "Error";
                        }
                        callback.onComplete(false, error.getResponse().getStatus(), error_message, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void resume(final double latitude, final double longitude, final StatusCallback callback) {

        retrofitInterface.resume(deviceID, appVersion, latitude + "," + longitude, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null) {
                    if (responseStatus.getCode().equalsIgnoreCase("ok"))
                        callback.onComplete(true, response.getStatus(), "Success");
                    else callback.onComplete(false, response.getStatus(), "Failure");
                } else callback.onComplete(false, response.getStatus(), "Failure");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    int statusCode = -1;
                    String message = "Unable to resume try again";
                    try {
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);
                        message = jsonResponse.getString("msg");
                    } catch (Exception e) {
                    }
                    try {
                        statusCode = error.getResponse().getStatus();
                    } catch (Exception e) {
                    }

                    callback.onComplete(false, statusCode, message);
                }
            }
        });
    }

    public void cancelTrip(final double latitude, final double longitude, final StatusCallback callback) {

        retrofitInterface.cancelTrip(deviceID, appVersion, latitude + "," + longitude, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null) {
                    if (responseStatus.getCode().equalsIgnoreCase("ok"))
                        callback.onComplete(true, response.getStatus(), "Success");
                    else callback.onComplete(false, response.getStatus(), "Failure");
                } else callback.onComplete(false, response.getStatus(), "Failure");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    int statusCode = -1;
                    String message = "Unable to cancel trip try again";
                    try {
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);
                        message = jsonResponse.getString("msg");
                    } catch (Exception e) {
                    }
                    try {
                        statusCode = error.getResponse().getStatus();
                    } catch (Exception e) {
                    }

                    callback.onComplete(false, statusCode, message);
                }
            }
        });
    }

}
