package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.models.RequestUpdateCategoryAvailability;
import com.mastaan.logistics.backend.models.RequestUpdateCategorySlots;
import com.mastaan.logistics.backend.models.RequestUpdateCategoryVisibility;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.DeliverySlotDetails;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class CategoriesAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface CategoriesListCallback {
        void onComplete(boolean status, int statusCode, String message, List<CategoryDetails> categoriesList);
    }

    public interface CategorySlotsListCallback {
        void onComplete(boolean status, int statusCode, String message, String date, List<DeliverySlotDetails> slotsList);
    }

    public interface SlotsForDateCallback{
        void onComplete(boolean status, int statusCode, String message, String loadedDate, List<DeliverySlotDetails> slotsList);
    }


    public CategoriesAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getCategories(final CategoriesListCallback callback){

        retrofitInterface.getCategories(deviceID, appVersion, new Callback<List<CategoryDetails>>() {
            @Override
            public void success(List<CategoryDetails> categoriesList, Response response) {
                if ( categoriesList != null ){
                    Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ GET_CATEGORIES ]  >>  Success");
                    callback.onComplete(true, 200, "Success", categoriesList);
                }else{
                    Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ GET_CATEGORIES ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ GET_CATEGORIES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ GET_CATEGORIES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getCategorySlots(final String categoryID, final String requiredDate, final CategorySlotsListCallback callback){

        Date date = DateMethods.getDateFromString(requiredDate);

        if ( date != null ){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            final int day = calendar.get(Calendar.DATE);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);

            retrofitInterface.getCategorySlots(deviceID, appVersion, categoryID, day, month, year, new Callback<List<DeliverySlotDetails>>() {
                @Override
                public void success(List<DeliverySlotDetails> slotsList, Response response) {
                    if ( slotsList != null ){
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ CATEGORY_SLOTS ]  >>  Success");
                        callback.onComplete(true, 200, "Success", requiredDate, slotsList);
                    }else{
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ CATEGORY_SLOTS ]  >>  Failure");
                        callback.onComplete(false, 500, "Failure", requiredDate, null);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if ( new CheckError(context).check(error) == false ) {
                        try {
                            Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ CATEGORY_SLOTS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                            callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), requiredDate, null);
                        } catch (Exception e) {
                            Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ CATEGORY_SLOTS ]  >>  Error = " + error.getKind());
                            callback.onComplete(false, -1, "Error", requiredDate, null);
                        }
                    }
                }
            });
        }
        else{
            Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : CATEGORY_SLOTS : Error = DATE_FORMAT_EXCEPTION");
            callback.onComplete(false, -1, "Error", requiredDate, null);
        }
    }

    public void updateCategorySlots(final RequestUpdateCategorySlots requestUpdateCategorySlots, final StatusCallback callback){

        retrofitInterface.updateCategorySlots(deviceID, appVersion, requestUpdateCategorySlots, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ UPDATE_CATEGORY_SLOTS ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ UPDATE_CATEGORY_SLOTS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ UPDATE_CATEGORY_SLOTS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateCategoryVisibility(final String categoryID, final RequestUpdateCategoryVisibility requestUpdateCategoryVisibility, final StatusCallback callback){

        retrofitInterface.updateCategoryVisibility(deviceID, appVersion, categoryID, requestUpdateCategoryVisibility, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ UPDATE_CATEGORY_VISIBILITY ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ UPDATE_CATEGORY_VISIBILITY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ UPDATE_CATEGORY_VISIBILITY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateCategoryAvailability(final String categoryID, final RequestUpdateCategoryAvailability requestUpdateCategoryAvailability, final StatusCallback callback){

        retrofitInterface.updateCategoryAvailability(deviceID, appVersion, categoryID, requestUpdateCategoryAvailability, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ UPDATE_CATEGORY_AVAILABILITY ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ UPDATE_CATEGORY_AVAILABILITY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString());
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nCATEGORIES_API : [ UPDATE_CATEGORY_AVAILABILITY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getSlotsForCategoryForDate(final String categoryID, final String requriedDate, final SlotsForDateCallback callback){

        Date date = DateMethods.getDateFromString(requriedDate);

        if ( date != null ){

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            final int day = calendar.get(Calendar.DATE);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);

            retrofitInterface.getSlotsForCategoryForDate(deviceID, appVersion, categoryID, day, month, year, new Callback<List<DeliverySlotDetails>>() {
                @Override
                public void success(List<DeliverySlotDetails> slotsList, Response response) {
                    if (slotsList != null) {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : GET_SLOTS_FOR_DATE : Success");
                        callback.onComplete(true, 200, "Success", requriedDate, slotsList);
                    } else {
                        Log.d(Constants.LOG_TAG, "\nCOMMON_API : GET_SLOTS_FOR_DATE : Failure");
                        callback.onComplete(false, 500, "Failure", requriedDate, null);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if ( new CheckError(context).check(error) == false ) {
                        try {
                            Log.d(Constants.LOG_TAG, "\nCOMMON_API : GET_SLOTS_FOR_DATE : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                            callback.onComplete(false, error.getResponse().getStatus(), "Error", requriedDate, null);
                        } catch (Exception e) {
                            Log.d(Constants.LOG_TAG, "\nCOMMON_API : GET_SLOTS_FOR_DATE : Error = " + error.getKind());
                            callback.onComplete(false, -1, "Error", requriedDate, null);
                        }
                    }
                }
            });
        }
        else{
            Log.d(Constants.LOG_TAG, "\nCOMMON_API : BuyerPushNotificationsRegistration : Error = DATE_FORMAT_EXCEPTION");
            callback.onComplete(false, -1, "Error", requriedDate, null);
        }
    }

}
