package com.mastaan.logistics.backend.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 15/6/16.
 */
public class ResponseBuyerOrdersForDate {

    List<TempOrder> items;

    class TempOrder{
        String dby;
    }

    public List<String> getOrdersList(){
        List<String> ordersList = new ArrayList<>();
        if ( items != null && items.size() > 0 ){
            for (int i=0;i<items.size();i++){
                ordersList.add(items.get(i).dby);
            }
        }
        return ordersList;
    }

}
