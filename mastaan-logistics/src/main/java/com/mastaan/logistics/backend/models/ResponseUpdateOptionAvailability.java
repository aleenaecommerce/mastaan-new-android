package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.AttributeOptionAvailabilityDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 22/10/16.
 */
public class ResponseUpdateOptionAvailability {
    double pd;
    List<AttributeOptionAvailabilityDetails> avl;

    public double getPriceDifference() {
        return pd;
    }

    public List<AttributeOptionAvailabilityDetails> getAvailabilitiesList() {
        if ( avl == null ){ avl = new ArrayList<>();    }
        return avl;
    }
}
