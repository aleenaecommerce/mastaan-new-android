package com.mastaan.logistics.backend;

import android.content.Context;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.BuildConfig;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestCreateEmployeeReferralCouponCode;
import com.mastaan.logistics.backend.models.RequestEmployeeBonuses;
import com.mastaan.logistics.backend.models.RequestLinkToFCM;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.interfaces.UsersCallback;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.EmployeeBonusDetails;
import com.mastaan.logistics.models.GroupedOrdersItemsList;
import com.mastaan.logistics.models.Job;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class EmployeesAPI {

    Context context;
    String deviceID;
    int appVersion;

    LocalStorageData localStorageData;
    RetrofitInterface retrofitInterface;

    public interface EmployeeBonusesCallback{
        void onComplete(boolean status, int statusCode, String message, List<EmployeeBonusDetails> employeeBonuses);
    }
    public interface JobDetailsCallback{
        void onComplete(boolean status, int statusCode, String message, Job jobDetails);
    }

    public EmployeesAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
        this.localStorageData = new LocalStorageData(context);
    }


    public void linkToFCM(String fcmRegistrationID, final StatusCallback callback){

        retrofitInterface.linkToFCM(deviceID, appVersion, localStorageData.getAccessToken(), new RequestLinkToFCM(fcmRegistrationID), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        if ( callback != null ){
                            callback.onComplete(false, error.getResponse().getStatus(), "Error");
                        }
                    } catch (Exception e) {
                        if ( callback != null ){
                            callback.onComplete(false, -1, "Error");
                        }
                    }
                }
            }
        });
    }

    public void getEmployees(final UsersCallback callback) {

        retrofitInterface.getEmployees(deviceID, appVersion, new Callback<List<UserDetails>>() {
            @Override
            public void success(List<UserDetails> userDetailses, Response response) {
                callback.onComplete(true, 200, "Success", userDetailses);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }


    public void getProcessingManagers(final UsersCallback callback) {

        retrofitInterface.getProcessingManagers(deviceID, appVersion, new Callback<List<UserDetails>>() {
            @Override
            public void success(List<UserDetails> userDetailses, Response response) {
                callback.onComplete(true, 200, "Success", userDetailses);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }


    public void getDeliveryBoys(final UsersCallback callback) {

        retrofitInterface.getDeliveryBoys(deviceID, appVersion, new Callback<List<UserDetails>>() {
            @Override
            public void success(List<UserDetails> userDetailses, Response response) {
                callback.onComplete(true, 200, "Success", userDetailses);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getAvailableDeliveryBoys(final UsersCallback callback) {

        if ( BuildConfig.PRODUCTION ) { /* TODO TEMP */
            retrofitInterface.getAvailableDeliveryBoys(deviceID, appVersion, new Callback<List<UserDetails>>() {
                @Override
                public void success(List<UserDetails> userDetailses, Response response) {
                    callback.onComplete(true, 200, "Success", userDetailses);
                }

                @Override
                public void failure(RetrofitError error) {
                    if (new CheckError(context).check(error) == false) {
                        try {
                            callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                        } catch (Exception e) {
                            callback.onComplete(false, -1, "Error", null);
                        }
                    }
                }
            });
        }else{
            getDeliveryBoys(callback);
        }
    }

    public void getDeliveryBoyOrders(final String deliveryBoyID, final OrdersAPI.OrdersAndGroupedOrderItemsCallback callback) {

        retrofitInterface.getDeliveryBoyOrders(deviceID, appVersion, deliveryBoyID, new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> deliverBoyOrders, final Response response) {
                if ( deliverBoyOrders != null ) {
                    GroupingMethods.getOrderItemsFromOrders(deliverBoyOrders, new OrderItemsListCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                            if (status) {
                                List<GroupedOrdersItemsList> groupedOrdersItemsLists = new ArrayList<GroupedOrdersItemsList>();
                                if ( orderItemsList != null && orderItemsList.size() > 0 ) {
                                    groupedOrdersItemsLists.add(new GroupedOrdersItemsList("ALL", orderItemsList));
                                }
                                callback.onComplete(true, response.getStatus(), "Success", deliverBoyOrders, groupedOrdersItemsLists);
                            } else {
                                callback.onComplete(false, 500, "Failure", null, null);
                            }
                        }
                    });
                }else {
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }


    public void getDeliveryBoyCurrentJob(final String deliveryBoyID, final JobDetailsCallback callback) {

        retrofitInterface.getDeliveryBoyCurrentJob(deviceID, appVersion, deliveryBoyID, new Callback<Job>() {
            @Override
            public void success(final Job jobDetails, final Response response) {
                callback.onComplete(true, 200, "Success", jobDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void clearEmployeeBoyJob(String employeeID, final StatusCallback callback){

        retrofitInterface.clearEmployeeBoyJob(deviceID, appVersion, employeeID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void createEmployeeReferralCoupon(String employeeID, RequestCreateEmployeeReferralCouponCode requestCreateEmployeeReferralCouponCode, final StatusCallback callback){

        retrofitInterface.createEmployeeReferralCoupon(deviceID, appVersion, employeeID, requestCreateEmployeeReferralCouponCode, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getEmployeeBonuses(final RequestEmployeeBonuses requestEmployeeBonuses, final EmployeeBonusesCallback callback) {

        retrofitInterface.getEmployeeBonuses(deviceID, appVersion, requestEmployeeBonuses, new Callback<List<EmployeeBonusDetails>>() {
            @Override
            public void success(final List<EmployeeBonusDetails> employeeBonuses, final Response response) {
                callback.onComplete(true, 200, "Success", employeeBonuses);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }


}
