package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 23/10/16.
 */
public class RequestUpdateAttributeDetails {
    String n;
    boolean ac;

    public RequestUpdateAttributeDetails(String name, boolean isAvailabilityChangeDaywise){
        this.n = name;
        this.ac = isAvailabilityChangeDaywise;
    }

    public String getName() {
        return n;
    }

    public boolean isAvailabilityChangeDaywise() {
        return ac;
    }
}
