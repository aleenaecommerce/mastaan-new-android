package com.mastaan.logistics.backend;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestFromDateToDate;
import com.mastaan.logistics.backend.models.ResponseOrdersStats;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.AnalyticsCouponCode;
import com.mastaan.logistics.models.AnalyticsInstallSource;
import com.mastaan.logistics.models.AnalyticsMeatItemAvailabilityRequest;
import com.mastaan.logistics.models.AnalyticsOrders;
import com.mastaan.logistics.models.AnalyticsReferral;
import com.mastaan.logistics.models.AnalyticsTopBuyer;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 12/01/18.
 */

public class AnalyticsAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface AnalyticsOrdersCallback {
        void onComplete(boolean status, int statusCode, String message, List<AnalyticsOrders> analyticsOrders);
    }
    public interface AnalyticsTopBuyersCallback {
        void onComplete(boolean status, int statusCode, String message, List<AnalyticsTopBuyer> analyticsTopBuyers);
    }
    public interface AnalyticsInstallSourcesCallback {
        void onComplete(boolean status, int statusCode, String message, List<AnalyticsInstallSource> analyticsInstallSources);
    }
    public interface AnalyticsCouponCodesCallback {
        void onComplete(boolean status, int statusCode, String message, List<AnalyticsCouponCode> analyticsCouponCodes);
    }
    public interface AnalyticsReferralsCallback {
        void onComplete(boolean status, int statusCode, String message, List<AnalyticsReferral> analyticsReferrals);
    }
    public interface AnalyticsMeatItemsAvailabilityRequestsCallback {
        void onComplete(boolean status, int statusCode, String message, List<AnalyticsMeatItemAvailabilityRequest> analyticsMeatItemAvailabilityRequests);
    }


    public AnalyticsAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getDaywiseOrdersReportForMonth(final String month, final boolean firstTimeOrdersOnly, final AnalyticsOrdersCallback callback){

        int eMonth=0,eYear=0;
        try{
            Calendar calendar = DateMethods.getCalendarFromDate(month);
            eMonth = calendar.get(Calendar.MONTH)+1;
            eYear = calendar.get(Calendar.YEAR);
        }catch (Exception e){}

        retrofitInterface.getDaywiseOrdersReportForMonth(deviceID, appVersion, eYear, eMonth, firstTimeOrdersOnly, new Callback<ResponseOrdersStats>() {
            @Override
            public void success(final ResponseOrdersStats responseOrdersStats, Response response) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        Collections.sort(responseOrdersStats.getStats(), new Comparator<AnalyticsOrders>() {
                            public int compare(AnalyticsOrders item1, AnalyticsOrders item2) {
                                return DateMethods.compareDates(item1.getDate(), item2.getDate());
                            }
                        });
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ FIRST_TIME_ORDERS_STATS ]  >>  Success");
                        callback.onComplete(true, 200, "Success", responseOrdersStats.getStats());
                    }
                }.execute();
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ FIRST_TIME_ORDERS_STATS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ FIRST_TIME_ORDERS_STATS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getTopBuyersAnalytics(final int topCount, final AnalyticsTopBuyersCallback callback){

        retrofitInterface.getTopBuyersAnalytics(deviceID, appVersion, topCount, new Callback<List<AnalyticsTopBuyer>>() {
            @Override
            public void success(final List<AnalyticsTopBuyer> analyticsTopBuyers, Response response) {
                Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ TOP_BUYERS ]  >>  Success");
                callback.onComplete(true, 200, "Success", analyticsTopBuyers);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ TOP_BUYERS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ TOP_BUYERS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getBuyerInstallSourceAnalytics(final String fromDate, final String toDate, final AnalyticsInstallSourcesCallback callback){

        retrofitInterface.getBuyerInstallSourceAnalytics(deviceID, appVersion, new RequestFromDateToDate(fromDate, toDate), new Callback<List<AnalyticsInstallSource>>() {
            @Override
            public void success(final List<AnalyticsInstallSource> analyticsInstallSources, Response response) {
                Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ INSTALL_SOURCES ]  >>  Success");
                callback.onComplete(true, 200, "Success", analyticsInstallSources);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ INSTALL_SOURCES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ INSTALL_SOURCES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getCouponCodesAnalytics(final String fromDate, final String toDate, final AnalyticsCouponCodesCallback callback){

        retrofitInterface.getCouponCodesAnalytics(deviceID, appVersion, new RequestFromDateToDate(fromDate, toDate), new Callback<List<AnalyticsCouponCode>>() {
            @Override
            public void success(final List<AnalyticsCouponCode> analyticsCouponCodes, Response response) {
                Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ COUPON_CODES ]  >>  Success");
                callback.onComplete(true, 200, "Success", analyticsCouponCodes);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ COUPON_CODES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ COUPON_CODES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getReferralsAnalytics(final String fromDate, final String toDate, final AnalyticsReferralsCallback callback){

        retrofitInterface.getReferralsAnalytics(deviceID, appVersion, new RequestFromDateToDate(fromDate, toDate), new Callback<List<AnalyticsReferral>>() {
            @Override
            public void success(final List<AnalyticsReferral> analyticsReferrals, Response response) {
                Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ REFERRALS ]  >>  Success");
                callback.onComplete(true, 200, "Success", analyticsReferrals);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ REFERRALS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ REFERRALS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getMeatItemAvailabilityRequestsAnalytics(final String fromDate, final String toDate, final AnalyticsMeatItemsAvailabilityRequestsCallback callback){

        retrofitInterface.getMeatItemAvailabilityRequestsAnalytics(deviceID, appVersion, new RequestFromDateToDate(fromDate, toDate), new Callback<List<AnalyticsMeatItemAvailabilityRequest>>() {
            @Override
            public void success(final List<AnalyticsMeatItemAvailabilityRequest> analyticsMeatItemAvailabilityRequests, Response response) {
                Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ MEATITEM_AVAILABILITY_REQUEST ]  >>  Success");
                callback.onComplete(true, 200, "Success", analyticsMeatItemAvailabilityRequests);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ MEATITEM_AVAILABILITY_REQUEST ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), error.getKind().toString(), null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nANALYTICS_API : [ MEATITEM_AVAILABILITY_REQUEST ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

}
