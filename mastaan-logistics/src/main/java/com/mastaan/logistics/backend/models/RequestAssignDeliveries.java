package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 10/2/17.
 */
public class RequestAssignDeliveries {
    String emp;
    List<String> items = new ArrayList<>();

    public RequestAssignDeliveries(String deliveryboyID, List<OrderItemDetails> orderItems){
        this.emp = deliveryboyID;
        if ( orderItems != null && orderItems.size() > 0 ){
            for (int i=0;i<orderItems.size();i++){
                items.add(orderItems.get(i).getID());
            }
        }
    }

}
