package com.mastaan.logistics.backend;

import android.content.Context;

import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.models.UserDetails;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class ProfileAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface ProfileCallback {
        void onComplete(boolean status, int statusCode, String message, UserDetails userDetails);
    }

    public ProfileAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getProfile(final ProfileCallback callback) {
        retrofitInterface.getProfile(deviceID, appVersion, new Callback<UserDetails>() {
            @Override
            public void success(UserDetails userDetails, Response response) {
                callback.onComplete(true, 200, "Success", userDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }
}
