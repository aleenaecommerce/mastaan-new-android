package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 12/10/18.
 */

public class RequestEmployeeBonuses {
    String employee;
    String fromDate;
    String toDate;


    public RequestEmployeeBonuses(){}
    public RequestEmployeeBonuses(String fromDate, String toDate){
        this(null, fromDate, toDate);
    }
    public RequestEmployeeBonuses(String employee, String fromDate, String toDate){
        this.employee = employee;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }


    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
