package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 11/25/2016.
 */
public class RequestRazorpayInvoiceID {
    String mobile;
    String paymentType;
    double amount;

    public RequestRazorpayInvoiceID(String mobile, String paymentType, double amount){
        this.mobile = mobile;
        this.paymentType = paymentType;
        this.amount = amount;
    }

}
