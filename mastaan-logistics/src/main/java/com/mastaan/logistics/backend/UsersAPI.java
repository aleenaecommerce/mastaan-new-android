package com.mastaan.logistics.backend;

import android.content.Context;
import android.location.Location;
import android.util.Base64;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestAssignDeliveries;
import com.mastaan.logistics.backend.models.RequestCreateJob;
import com.mastaan.logistics.backend.models.ResponseAssignDelivery;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.Job;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class UsersAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface CreateJobCallback{
        void onComplete(boolean status, int statusCode, String message, Job jobDetails);
    }

    public UsersAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void assignDeliveryBoyToOrder(String orderID, String userID, final StatusCallback callback) {

        retrofitInterface.assignDeliveryBoyToOrder(deviceID, appVersion, orderID, userID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus.getCode().equalsIgnoreCase("OK")) {
                    callback.onComplete(true, 200, "Success");
                } else if (responseStatus.getCode().equalsIgnoreCase("delivery_already_claimed")) {
                    callback.onComplete(false, 500, "Delivery already claimed");
                } else {
                    callback.onComplete(false, 500, "Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    int statusCode = -1;
                    String message = "Error";
                    try {
                        statusCode = error.getResponse().getStatus();
                    } catch (Exception e) {}

                    try {
                        String response_json_string = new String(((TypedByteArray) error.getResponse().getBody()).getBytes());
                        JSONObject jsonResponse = new JSONObject(response_json_string);
                        message = jsonResponse.getString("code");
                    } catch (Exception e) {}

                    callback.onComplete(false, statusCode, message);
                }
            }
        });
    }

    public void assignDelivery(String item_id, String boy_id, final StatusCallback callback) {

        retrofitInterface.assignDelivery(deviceID, appVersion, item_id, boy_id, new Callback<ResponseAssignDelivery>() {
            @Override
            public void success(ResponseAssignDelivery responseAssignDelivery, Response response) {
                String message = "Success";
                if ( responseAssignDelivery.getPendingAssignments() != null && responseAssignDelivery.getPendingAssignments().size() > 0 ){
                    message = "<b>"+ CommonMethods.capitalizeStringWords(responseAssignDelivery.getBuyerDetails().getName())+"</b> has <b>"+responseAssignDelivery.getPendingAssignments().size()+"</b> pending item(s)<br>";
                    for (int i=0;i<responseAssignDelivery.getPendingAssignments().size();i++){
                        message += "<br>"+CommonMethods.capitalizeStringWords(responseAssignDelivery.getPendingAssignments().get(i).getCategoryName())+" - <b>"+responseAssignDelivery.getPendingAssignments().get(i).getCount()+"</b> item(s)";
                    }
                }
                callback.onComplete(true, 200, message);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        String errorMessage = "Error";
                        if ( error.getResponse().getStatus() == 500 ) {
                            try { errorMessage = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())).getString("msg"); } catch (Exception e) {}
                        }
                        Log.d(Constants.LOG_TAG, "\nUSERS_API : [ ASSIGN_DELIVERY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), errorMessage);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nUSERS_API : [ ASSIGN_DELIVERY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void assignMultipleDeliveries(final RequestAssignDeliveries requestAssignDeliveries, final StatusCallback callback){

        retrofitInterface.assignMultipleDeliveries(deviceID, appVersion, requestAssignDeliveries, new Callback<List<ResponseAssignDelivery>>() {

            @Override
            public void success(List<ResponseAssignDelivery> responseAssignDeliveries, Response response) {
                String pendingAssignmentsInfo = "";
                for (int i=0;i<responseAssignDeliveries.size();i++){
                    if ( responseAssignDeliveries.get(i).getPendingAssignments().size() > 0 ){
                        if ( pendingAssignmentsInfo.length() == 0 ){ pendingAssignmentsInfo += "<b><u>Pending items</u></b>";   }
                        pendingAssignmentsInfo += "<br><br><b>"+CommonMethods.capitalizeStringWords(responseAssignDeliveries.get(i).getBuyerDetails().getName())+"</b>";
                        for (int j=0;j<responseAssignDeliveries.get(i).getPendingAssignments().size();j++){
                            pendingAssignmentsInfo += "<br>"+CommonMethods.capitalizeStringWords(responseAssignDeliveries.get(i).getPendingAssignments().get(j).getCategoryName())+" - <b>"+responseAssignDeliveries.get(i).getPendingAssignments().get(j).getCount()+"</b> item(s)";
                        }
                    }
                }
                callback.onComplete(true, 200, pendingAssignmentsInfo.length()>0?pendingAssignmentsInfo:"Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        String errorMessage = "Error";
                        if ( error.getResponse().getStatus() == 500 ) {
                            try { errorMessage = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())).getString("msg"); } catch (Exception e) {}
                        }
                        Log.d(Constants.LOG_TAG, "\nUSERS_API : [ ASSIGN_MULTIPLE_DELIVERIES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), errorMessage);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nUSERS_API : [ ASSIGN_MULTIPLE_DELIVERIES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void createJob(final Location location, Job job, final CreateJobCallback callback) {

        final LocalStorageData localStorageData = new LocalStorageData(context);
        String requestID = localStorageData.getCreateJobRequestID();
        if ( requestID == null || requestID.length() == 0 ) {
            requestID = Base64.encodeToString((localStorageData.getUserID() + ":CJ" + System.currentTimeMillis()).getBytes(), Base64.NO_WRAP);
            localStorageData.setCreateJobRequestID(requestID);     // Storing RequestID in LocalStorage
        }

        retrofitInterface.createJob(deviceID, appVersion, new RequestCreateJob(requestID, location.getLatitude(), location.getLongitude(), job), new Callback<Job>() {
            @Override
            public void success(Job jobDetails, Response response) {
                localStorageData.setCreateJobRequestID("");        // Clearing RequestID from LocalStorage
                callback.onComplete(true, 200, "Success", jobDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }
}
