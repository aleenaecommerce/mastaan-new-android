package com.mastaan.logistics.backend.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Venkatesh Uppu on 25/3/17.
 */
public class RequestAddOrUpdateDeliveryArea {
    String code;
    String name;
    String pos;
    String grp;

    public RequestAddOrUpdateDeliveryArea(String code, String name, LatLng latLng, String deliveryGroup){
        this.code = code;
        this.name = name;
        this.pos = latLng.latitude+","+latLng.longitude;
        this.grp = deliveryGroup;
    }

    public RequestAddOrUpdateDeliveryArea(String code, String name, String latLng, String deliveryGroup){
        this.code = code;
        this.name = name;
        this.pos = latLng;
        this.grp = deliveryGroup;
    }
}
