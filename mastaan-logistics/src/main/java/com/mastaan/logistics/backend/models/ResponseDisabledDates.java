package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.CategoryDetails;
import com.mastaan.logistics.models.DayDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by valueduser on 14/9/16.
 */
public class ResponseDisabledDates {
    List<CategoryDetails> categories;
    List<DayDetails> disabledDates;


    public List<CategoryDetails> getCategories() {
        if ( categories != null ){  return categories;  }
        return new ArrayList<>();
    }

    public List<DayDetails> getDisaledDates() {
        if ( disabledDates != null ){
            for (int i=0;i<disabledDates.size();i++){
                disabledDates.get(i).setEnabled(false);
            }
            return disabledDates;
        }
        return new ArrayList<>();
    }
}
