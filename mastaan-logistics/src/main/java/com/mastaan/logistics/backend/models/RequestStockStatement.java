package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 17/08/18.
 */

public class RequestStockStatement {
    String fd;
    String td;
    String c;
    String wmi;
    String[] ilao;

    String hu;


    public void setFromDate(String fromDate) {
        this.fd = fromDate;
    }

    public void setToDate(String toDate) {
        this.td = toDate;
    }

    public void setCategory(String category) {
        this.c = category;
    }

    public void setWarehouseMeatItem(String warehouseMeatItem) {
        this.wmi = warehouseMeatItem;
    }

    public void setItemLevelAttributeOptions(String[] itemLevelAttributeOptions) {
        this.ilao = itemLevelAttributeOptions;
    }

    public void setHub(String hub) {
        this.hu = hub;
    }
}
