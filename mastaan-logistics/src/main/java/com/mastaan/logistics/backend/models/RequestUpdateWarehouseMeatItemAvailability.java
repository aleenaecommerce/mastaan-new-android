package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 21/01/18.
 */

public class RequestUpdateWarehouseMeatItemAvailability {
    boolean a;

    public RequestUpdateWarehouseMeatItemAvailability(boolean availability){
        this.a = availability;
    }

    public boolean isAvailable() {
        return a;
    }

}
