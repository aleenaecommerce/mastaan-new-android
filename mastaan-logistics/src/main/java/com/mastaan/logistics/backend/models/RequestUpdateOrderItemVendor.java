package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 27/06/18.
 */

public class RequestUpdateOrderItemVendor {
    String m;       // vendor_id
    String mpt;     // payment_type
    double apm;     // amount
    String mtc;     // Comments

    public RequestUpdateOrderItemVendor(String vendor_id, String payment_type, double amount, String comments) {
        this.m = vendor_id;
        this.mpt = payment_type;
        this.apm = amount;
        this.mtc = comments;
    }
}
