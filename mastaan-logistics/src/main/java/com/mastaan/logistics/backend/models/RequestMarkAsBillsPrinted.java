package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.OrderDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 8/7/17.
 */

public class RequestMarkAsBillsPrinted {
    List<String> ids;

    public RequestMarkAsBillsPrinted(List<OrderDetails> ordersList){
        List<String> printedOrdersIDs = new ArrayList<>();
        if ( ordersList != null && ordersList.size() > 0 ){
            for (int i=0;i<ordersList.size();i++){
                if ( ordersList.get(i) != null && ordersList.get(i).getID() != null ){
                    printedOrdersIDs.add(ordersList.get(i).getID());
                }
            }
        }
        this.ids = printedOrdersIDs;
    }
}
