package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.Job;

/**
 * Created by Venaktesh Uppu on 25/06/16.
 */
public class RequestWhereToNext {
    String req_id;
    String loc;
    Job job;

    public RequestWhereToNext(String req_id, double latitude, double longitude, Job job) {
        this.req_id = req_id;
        this.loc = latitude + "," + longitude;
        this.job = job;
    }
}
