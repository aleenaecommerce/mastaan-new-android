package com.mastaan.logistics.backend.models;

import java.util.List;

/**
 * Created by venkatesh on 1/9/16.
 */
public class RequestUpdateMeatItemAvaialability {
    String d;
    double pr;
    double opr;
    double apr;
    boolean a;
    String ar;

    List<RequestUpdateOptionAvailability2> opts;

    /*public RequestUpdateMeatItemAvaialability(String date, double vendorPrice, double buyerPrice, boolean isAvailable, String availabilityReason, List<RequestUpdateOptionAvailability2> optionsAvailabilityRequests){
        this(date, vendorPrice, buyerPrice, 0, isAvailable, availabilityReason, optionsAvailabilityRequests);
    }*/
    public RequestUpdateMeatItemAvaialability(String date, double vendorPrice, double buyerPrice, double discount, boolean isAvailable, String availabilityReason, List<RequestUpdateOptionAvailability2> optionsAvailabilityRequests){
        this.d = date;
        this.pr = vendorPrice;
        this.apr = buyerPrice;
        if ( discount>=0 && discount <= buyerPrice ){   this.opr = buyerPrice - discount;   }
        else{   this.opr = apr; }
        this.a = isAvailable;
        this.ar = availabilityReason;

        this.opts = optionsAvailabilityRequests;
    }

    public String getDate() {
        return d;
    }

    public double getVendorPrice() {
        return pr;
    }

    public double getBuyerPrice() {
        return opr;
    }

    public boolean isAvailable() {
        return a;
    }

    public String getAvailabilityReason() {
        return ar;
    }
}
