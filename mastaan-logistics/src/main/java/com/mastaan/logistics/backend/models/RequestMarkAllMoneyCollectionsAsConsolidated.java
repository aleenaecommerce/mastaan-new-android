package com.mastaan.logistics.backend.models;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 4/11/16.
 */
public class RequestMarkAllMoneyCollectionsAsConsolidated {
    List<String> collections;

    public RequestMarkAllMoneyCollectionsAsConsolidated(List<String> collectionIDs){
        this.collections = collectionIDs;
    }
}
