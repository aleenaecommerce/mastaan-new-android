package com.mastaan.logistics.backend;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.ResponseStatus;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestLinkVendorAndButcherToOrderItem;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.UserDetailsMini;
import com.mastaan.logistics.models.Vendor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class VendorsAndButchersAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface VendorsCallback {
        void onComplete(boolean status, int statusCode, String message, List<Vendor> vendorsList);
    }
    public interface ButchersCallback {
        void onComplete(boolean status, int statusCode, String message, List<UserDetailsMini> butchersList);
    }


    public VendorsAndButchersAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getVendors(/*double latitude, double longitude, */final VendorsCallback callback) {

        /*Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("loc", latitude + "," + longitude);*/
        retrofitInterface.getVendors(deviceID, appVersion/*, queryMap*/, new Callback<List<Vendor>>() {
            @Override
            public void success(List<Vendor> vendorsList, Response response) {
                if ( vendorsList != null ){
                    callback.onComplete(true, 200, "Success", vendorsList);
                }else{
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getVendorsForOrderItem(final String orderItemID, final Location currentLocation, final VendorsCallback callback) {

        Map<String, Object> map = new HashMap<>();
        if ( currentLocation !=  null ) {
            map.put("loc", currentLocation.getLatitude() + "," + currentLocation.getLongitude());
        }
        retrofitInterface.getVendorsForOrderItem(deviceID, appVersion, orderItemID, map, new Callback<List<Vendor>>() {
            @Override
            public void success(final List<Vendor> vendorsList, Response response) {
                if ( vendorsList != null ){
                    new AsyncTask<Void, Void, Void>() {
                        List<Vendor> sortedVendorsList = new ArrayList<Vendor>();
                        @Override
                        protected Void doInBackground(Void... voids) {
                            sortedVendorsList = sortVendorsByDistance(vendorsList, currentLocation);
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            callback.onComplete(true, 200, "Success", sortedVendorsList);
                        }
                    }.execute();
                }else{
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getButchers(final String categoryID, final ButchersCallback callback) {

        retrofitInterface.getButchersForCategory(deviceID, appVersion, categoryID, new Callback<List<UserDetailsMini>>() {
            @Override
            public void success(List<UserDetailsMini> butchersList, Response response) {
                if ( butchersList != null ){
                    callback.onComplete(true, 200, "Success", butchersList);
                }else{
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getButchersForOrderItem(final String orderItemID/*, final Location currentLocation*/, final ButchersCallback callback) {

        /*Map<String, Object> map = new HashMap<>();
        if ( currentLocation !=  null ) {
            map.put("loc", currentLocation.getLatitude() + "," + currentLocation.getLongitude());
        }*/
        retrofitInterface.getButchersForOrderItem(deviceID, appVersion, orderItemID/*, map*/, new Callback<List<UserDetailsMini>>() {
            @Override
            public void success(List<UserDetailsMini> butchersList, Response response) {
                if ( butchersList != null ){
                    callback.onComplete(true, 200, "Success", butchersList);
                }else{
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }


    public void linkVendorToOrderItem(String orderItemID, RequestLinkVendorAndButcherToOrderItem requestLinkVendorToOrderItem, final StatusCallback callback) {

        retrofitInterface.linkVendorAndButcherToOrderItem(deviceID, appVersion, orderItemID, requestLinkVendorToOrderItem, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                if (responseStatus != null && responseStatus.getCode() != null) {
                    if (responseStatus.getCode().equalsIgnoreCase("ok"))
                        callback.onComplete(true, response.getStatus(), "Success");
                    else callback.onComplete(false, response.getStatus(), "Failure");
                } else callback.onComplete(false, response.getStatus(), "Failure");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    //===========

    class Ven{
        Vendor vendor;
        double dis;
        Ven(Vendor vendor, double dis){
            this.vendor = vendor;
            this.dis = dis;
        }
    }

    private List<Vendor> sortVendorsByDistance(List<Vendor> vendorsList, Location currentLocation) {
        List<Vendor> distanceSortedVendors = new ArrayList<>();
        if ( currentLocation != null ){
            LatLng currentLocationLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

            List<Ven> disVendorsList = new ArrayList<>();
            for (int i=0;i<vendorsList.size();i++){
                if ( vendorsList.get(i) != null ){
                    disVendorsList.add(new Ven(vendorsList.get(i), Math.abs(LatLngMethods.getDistanceBetweenLatLng(vendorsList.get(i).getLocation(), currentLocationLatLng))));
                }
            }

            // SORTING GROUPS BY DATE
            Collections.sort(disVendorsList, new Comparator<Ven>() {
                public int compare(Ven item1, Ven item2) {
                    if (item1.dis > item2.dis) {
                        return 1;
                    } else if (item2.dis > item1.dis) {
                        return -1;
                    }
                    return 0;
                }
            });

            for (int i=0;i<disVendorsList.size();i++){
                Log.d(Constants.LOG_TAG, "V: "+disVendorsList.get(i).vendor.getName()+" dis: "+disVendorsList.get(i).dis+" with "+currentLocationLatLng);
                distanceSortedVendors.add(disVendorsList.get(i).vendor);
            }
        }
        else{
            distanceSortedVendors = vendorsList;
        }

        //--------

        List<Vendor> mastaanVendors = new ArrayList<>();
        List<Vendor> nonMastaanVendors = new ArrayList<>();
        for (int i=0;i<distanceSortedVendors.size();i++){
            if ( distanceSortedVendors.get(i).getName() != null && distanceSortedVendors.get(i).getName().toLowerCase().contains("mastaan") ){
                mastaanVendors.add(distanceSortedVendors.get(i));
            }else{
                nonMastaanVendors.add(distanceSortedVendors.get(i));
            }
        }

        List<Vendor> finalSortedVendors = new ArrayList<>();
        for (int i=0;i<mastaanVendors.size();i++){  finalSortedVendors.add(mastaanVendors.get(i));  }
        for (int i=0;i<nonMastaanVendors.size();i++){  finalSortedVendors.add(nonMastaanVendors.get(i));  }

        return finalSortedVendors;

        /*final CommonMethods commonMethods = new CommonMethods();
        Collections.sort(vendorsList, new Comparator<Vendor>() {
            @Override
            public int compare(Vendor lhs, Vendor rhs) {
                if (lhs.getDeliveryAddressLatLng() == null || rhs.getDeliveryAddressLatLng() == null)
                    return 0;
                float ld = LatLngMethods.getDistanceBetweenLatLng(lhs.getDeliveryAddressLatLng(), hubLocation);
                float rd = LatLngMethods.getDistanceBetweenLatLng(rhs.getDeliveryAddressLatLng(), hubLocation);
                return ld > rd ? 1 : -1;
            }
        });
        return vendorsList;*/
    }


}
