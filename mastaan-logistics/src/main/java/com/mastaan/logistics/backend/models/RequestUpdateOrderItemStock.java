package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 08/08/18.
 */

public class RequestUpdateOrderItemStock {
    String sto;     //  Stock
    String psto;    //  Processed Stock

    public RequestUpdateOrderItemStock(String stockID, String processedStockID) {
        this.sto = stockID;
        this.psto = processedStockID;
    }

}
