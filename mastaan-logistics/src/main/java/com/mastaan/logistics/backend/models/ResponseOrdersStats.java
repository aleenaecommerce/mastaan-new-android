package com.mastaan.logistics.backend.models;

import com.mastaan.logistics.models.AnalyticsOrders;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 12/01/18.
 */

public class ResponseOrdersStats {
    long n;
    double v;
    List<AnalyticsOrders> stats;

    public long getCount() {
        return n;
    }

    public double getAmount() {
        return v;
    }

    public List<AnalyticsOrders> getStats() {
        if ( stats == null ){   stats = new ArrayList<>();  }
        return stats;
    }

}
