package com.mastaan.logistics.backend.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 26/1/17.
 */
public class RequestUpdateMeatItemsAvailability {
    String d;
    String ar;
    List<Availability> items = new ArrayList<>();

    class Availability{
        String _id;
        boolean a;
        double pr;
        double apr;
        double opr;
        Availability(String itemID, boolean availability, double buyingPrice, double sellingPrice, double discount){
            this._id = itemID;
            this.a = availability;
            this.pr = buyingPrice;
            this.apr = sellingPrice;
            this.opr = (discount>=0&&discount<=sellingPrice)?(sellingPrice-discount):sellingPrice;
        }
    }

    public RequestUpdateMeatItemsAvailability(String date){
        this.d = date;
    }

    public void setUnavailbaleReason(String unavilableReason){
        this.ar = unavilableReason;
    }

    public void addItem(String itemID, boolean availability, double buyingPrice, double sellingPrice, double discount){
        items.add(new Availability(itemID, availability, buyingPrice, sellingPrice, discount));
    }

    public int getItemsCount(){
        return items.size();
    }

}
