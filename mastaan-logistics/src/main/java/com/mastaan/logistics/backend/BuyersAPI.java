package com.mastaan.logistics.backend;

import android.content.Context;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.AddressBookItemDetails;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestDisableBuyer;
import com.mastaan.logistics.backend.models.RequestUpdateBuyerDetails;
import com.mastaan.logistics.backend.models.RequestUpdateInstallSource;
import com.mastaan.logistics.backend.models.ResponseBuyerAccessToken;
import com.mastaan.logistics.models.BuyerDetails;
import com.mastaan.logistics.models.BuyerSearchDetails;
import com.mastaan.logistics.models.OrderDetails;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Venkatesh Uppu on 12/11/16.
 */

public class BuyersAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface AddressBookCallback{
        void onComplete(boolean status, int statusCode, String message, List<AddressBookItemDetails> addressesList);
    }

    public interface BuyerDetailsCallback{
        void onComplete(boolean status, int statusCode, String message, BuyerDetails buyerDetails);
    }

    public interface BuyersCallback{
        void onComplete(boolean status, int statusCode, String message, BuyerSearchDetails searchDetails, long pageNumber, List<BuyerDetails> buyersList);
    }

    public interface SearchBuyersCallback{
        void onComplete(boolean status, int statusCode, String message, String query, List<BuyerDetails> buyersList);
    }

    public interface OrdersCallback {
        void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList);
    }

    public interface BuyerAccessTokenCallback{
        void onComplete(boolean status, int statusCode, String message, String accessToken, BuyerDetails buyerDetails);
    }


    public BuyersAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }


    public void getBuyerAddressBook(String buyerID, final AddressBookCallback callback){

        retrofitInterface.getBuyerAddressBook(deviceID, appVersion, buyerID, new Callback<List<AddressBookItemDetails>>() {
            @Override
            public void success(List<AddressBookItemDetails> addressesList, Response response) {
                callback.onComplete(true, 200, "Success", addressesList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getBuyerDetails(String buyerID, final BuyerDetailsCallback callback){

        retrofitInterface.getBuyerDetails(deviceID, appVersion, buyerID, new Callback<BuyerDetails>() {
            @Override
            public void success(BuyerDetails buyerDetails, Response response) {
                callback.onComplete(true, 200, "Success", buyerDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getBuyerAccessTokenUsingID(String buyerID, final BuyerAccessTokenCallback callback){

        retrofitInterface.getBuyerAccessTokenUsingID(deviceID, appVersion, buyerID, new Callback<ResponseBuyerAccessToken>() {
            @Override
            public void success(ResponseBuyerAccessToken responseBuyerAccessToken, Response response) {
                if ( responseBuyerAccessToken.getToken() != null ){
                    callback.onComplete(true, 200, "Success", responseBuyerAccessToken.getToken(), responseBuyerAccessToken.getBuyerDetails());
                }else{
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void getBuyerAccessTokenUsingMobile(String mobileNumber, final BuyerAccessTokenCallback callback){

        retrofitInterface.getBuyerAccessTokenUsingMobile(deviceID, appVersion, mobileNumber, new Callback<ResponseBuyerAccessToken>() {
            @Override
            public void success(ResponseBuyerAccessToken responseBuyerAccessToken, Response response) {
                if ( responseBuyerAccessToken.getToken() != null ){
                    callback.onComplete(true, 200, "Success", responseBuyerAccessToken.getToken(), responseBuyerAccessToken.getBuyerDetails());
                }else{
                    callback.onComplete(false, 500, "Failure", null, null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null, null);
                    }
                }
            }
        });
    }

    public void updateBuyerDetails(final String buyerID, final RequestUpdateBuyerDetails requestUpdateBuyerDetails, final StatusCallback callback){

        retrofitInterface.updateBuyerDetails(deviceID, appVersion, buyerID, requestUpdateBuyerDetails, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void enableBuyer(final String buyerID, final StatusCallback callback){

        retrofitInterface.enableBuyer(deviceID, appVersion, buyerID, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void disableBuyer(final String buyerID, final String disableReason, final StatusCallback callback){

        retrofitInterface.disableBuyer(deviceID, appVersion, buyerID, new RequestDisableBuyer(disableReason), new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getBuyers(final BuyerSearchDetails searchDetails, final long pageNumber, final BuyersCallback callback) {

        retrofitInterface.getBuyers(deviceID, appVersion, pageNumber, searchDetails, new Callback<List<BuyerDetails>>() {
            @Override
            public void success(List<BuyerDetails> buyersList, Response response) {
                callback.onComplete(true, 200, "Success", searchDetails, pageNumber, buyersList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", searchDetails, pageNumber, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", searchDetails, pageNumber, null);
                    }
                }
            }
        });
    }

    public void getCouponBuyers(final String couponCode, final BuyerSearchDetails searchDetails, final BuyersCallback callback) {

        retrofitInterface.getCouponBuyers(deviceID, appVersion, couponCode, searchDetails, new Callback<List<BuyerDetails>>() {
            @Override
            public void success(List<BuyerDetails> buyersList, Response response) {
                callback.onComplete(true, 200, "Success", searchDetails, 1, buyersList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", searchDetails, 0, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", searchDetails, 0, null);
                    }
                }
            }
        });
    }

    public void getReferralBuyers(final String referralCode, final BuyerSearchDetails searchDetails, final BuyersCallback callback) {

        retrofitInterface.getReferralBuyers(deviceID, appVersion, referralCode, searchDetails, new Callback<List<BuyerDetails>>() {
            @Override
            public void success(List<BuyerDetails> buyersList, Response response) {
                callback.onComplete(true, 200, "Success", searchDetails, 1, buyersList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", searchDetails, 0, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", searchDetails, 0, null);
                    }
                }
            }
        });
    }

    public void getMeatItemAvailabilityRequestedBuyers(final String warehouseMeatItemID, final BuyerSearchDetails searchDetails, final BuyersCallback callback) {

        retrofitInterface.getMeatItemAvailabilityRequestedBuyers(deviceID, appVersion, warehouseMeatItemID, searchDetails, new Callback<List<BuyerDetails>>() {
            @Override
            public void success(List<BuyerDetails> buyersList, Response response) {
                callback.onComplete(true, 200, "Success", searchDetails, 1, buyersList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", searchDetails, 0, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", searchDetails, 0, null);
                    }
                }
            }
        });
    }

    public void searchBuyers(final String query, final SearchBuyersCallback callback) {

        retrofitInterface.searchBuyers(deviceID, appVersion, query, new Callback<List<BuyerDetails>>() {
            @Override
            public void success(List<BuyerDetails> buyersList, Response response) {
                callback.onComplete(true, 200, "Success", query, buyersList);
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", query, null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", query, null);
                    }
                }
            }
        });
    }

    public void getBuyerOrders(final String buyerID, final OrdersCallback callback) {

        retrofitInterface.getBuyerOrders(deviceID, appVersion, buyerID, new Callback<List<OrderDetails>>() {
            @Override
            public void success(final List<OrderDetails> ordersList, final Response response) {
                if (ordersList != null) {
                    callback.onComplete(true, 200, "Success", ordersList);
                } else {
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateBuyerInstallSource(final String buyerID, RequestUpdateInstallSource requestUpdateInstallSource, final StatusCallback callback){

        retrofitInterface.updateBuyerInstallSource(deviceID, appVersion, buyerID, requestUpdateInstallSource, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateBuyerPromotionalAlertsPreference(final String buyerID, final boolean preference, final StatusCallback callback){

        retrofitInterface.updateBuyerPromotionalAlertsPreference(deviceID, appVersion, buyerID, preference, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if (new CheckError(context).check(error) == false) {
                    try {
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

}
