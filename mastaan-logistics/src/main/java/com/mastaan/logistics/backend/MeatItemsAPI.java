package com.mastaan.logistics.backend;

import android.content.Context;
import android.util.Log;

import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.models.ResponseStatus;
import com.mastaan.logistics.backend.methods.CheckError;
import com.mastaan.logistics.backend.models.RequestItemLogs;
import com.mastaan.logistics.backend.models.RequestUpdateAttributeDetails;
import com.mastaan.logistics.backend.models.RequestUpdateMeatItem;
import com.mastaan.logistics.backend.models.RequestUpdateMeatItemAvaialability;
import com.mastaan.logistics.backend.models.RequestUpdateMeatItemsAvailability;
import com.mastaan.logistics.backend.models.RequestUpdateOptionAvailability;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItem;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItemAvailability;
import com.mastaan.logistics.backend.models.ResponseAffectedMeatItems;
import com.mastaan.logistics.backend.models.ResponseMeatItemAttributes;
import com.mastaan.logistics.backend.models.ResponseUpdateAttributeDetails;
import com.mastaan.logistics.backend.models.ResponseUpdateMeatItemAvailability;
import com.mastaan.logistics.backend.models.ResponseUpdateOptionAvailability;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.AttributeDetails;
import com.mastaan.logistics.models.AttributeOptionAvailabilityDetails;
import com.mastaan.logistics.models.AvailabilityDetails;
import com.mastaan.logistics.models.GroupedMeatItemsList;
import com.mastaan.logistics.models.ItemLogDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Venkatesh Uppu on 01/03/16.
 */

public class MeatItemsAPI {

    Context context;
    String deviceID;
    int appVersion;

    RetrofitInterface retrofitInterface;

    public interface WarehouseMeatItemCallback{
        void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemDetails warehouseMeatItemDetails);
    }
    public interface WarehouseMeatItemsCallback{
        void onComplete(boolean status, int statusCode, String message, List<WarehouseMeatItemDetails> warehouseMeatItems);
    }

    public interface GroupedWarehouseMeatItemsCallback {
        void onComplete(boolean status, int statusCode, String message, List<GroupedMeatItemsList> groupedMeatItemsList);
    }

    public interface MeatItemAttributesCallback{
        void onComplete(boolean status, int statusCode, String message, List<AttributeDetails> attributesList);
    }

    public interface UpdateMeatItemAvailabilityCallback {
        void onComplete(boolean status, int statusCode, String message, double latestBuyingPrice, double latestSellingPrice, List<AvailabilityDetails> availabilitiesList, List<AttributeDetails> attributesList);
    }

    public interface UpdateAttributeDetailsCallback{
        void onComplete(boolean status, int statusCode, String message, AttributeDetails updatedAttributeDetails);
    }

    public interface UpdateAttributeOptionAvailabilityCallback{
        void onComplete(boolean status, int statusCode, String message, double latestPriceDifference, List<AttributeOptionAvailabilityDetails> availabilitiesList);
    }

    public interface ItemLogsCallback{
        void onComplete(boolean status, int statusCode, String message, List<ItemLogDetails> itemLogs);
    }


    public MeatItemsAPI(Context context, String deviceID, int appVersion, RestAdapter restAdapter) {
        this.context = context;
        this.deviceID = deviceID;
        this.appVersion = appVersion;

        this.retrofitInterface = restAdapter.create(RetrofitInterface.class);
    }

    public void getWareHouseMeatItemOfMeatItem(final String meatItemID, final WarehouseMeatItemCallback callback){

        retrofitInterface.getWareHouseMeatItemOfMeatItem(deviceID, appVersion, meatItemID, new Callback<WarehouseMeatItemDetails>() {
            @Override
            public void success(WarehouseMeatItemDetails warehouseMeatItemDetails, Response response) {
                if ( warehouseMeatItemDetails != null ){
                    Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WAREHOUSE_ITEM_OF_MEAT_ITEM ]  >>  Success");
                    callback.onComplete(true, 200, "Success", warehouseMeatItemDetails);
                }else{
                    Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WAREHOUSE_ITEM_OF_MEAT_ITEM ]  >>  Failure");
                    callback.onComplete(false, 500, "Failure", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WAREHOUSE_ITEM_OF_MEAT_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WAREHOUSE_ITEM_OF_MEAT_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });

    }

    public void getMeatItems(final WarehouseMeatItemsCallback callback) {
        getMeatItems(Constants.ALL.toLowerCase(), false, null, callback);
    }
    public void getMeatItems(String availability, boolean forStocks, String hubID, final WarehouseMeatItemsCallback callback) {
        retrofitInterface.getMeatItems(deviceID, appVersion, availability, forStocks, hubID, new Callback<List<WarehouseMeatItemDetails>>() {
            @Override
            public void success(final List<WarehouseMeatItemDetails> warehouseMeatItems, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ GET_MEAT_ITEMS ]  >>  Success");
                callback.onComplete(true, 200, "Success", warehouseMeatItems);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ GET_MEAT_ITEMS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ GET_MEAT_ITEMS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getWarehouseMeatItemDetails(final String warehouseMeatItemID, final WarehouseMeatItemCallback callback) {
        getWarehouseMeatItemDetails(warehouseMeatItemID, false, "", callback);
    }
    public void getWarehouseMeatItemDetails(final String warehouseMeatItemID, boolean forStocks, String hubID, final WarehouseMeatItemCallback callback) {

        retrofitInterface.getWarehouseMeatItemDetails(deviceID, appVersion, warehouseMeatItemID, forStocks, hubID, new Callback<WarehouseMeatItemDetails>() {
            @Override
            public void success(final WarehouseMeatItemDetails warehouseMeatItemDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WH_MEAT_ITEM_DETAILS ]  >>  Success");
                callback.onComplete(true, 200, "Success", warehouseMeatItemDetails);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WH_MEAT_ITEM_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WH_MEAT_ITEM_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getGroupedMeatItems(final GroupedWarehouseMeatItemsCallback callback) {
        getGroupedMeatItems(Constants.ALL.toLowerCase(), false, null, callback);
    }
    public void getGroupedMeatItems(String availability, boolean forStocks, String hubID, final GroupedWarehouseMeatItemsCallback callback) {

        getMeatItems(availability, forStocks, hubID, new WarehouseMeatItemsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<WarehouseMeatItemDetails> warehouseMeatItems) {
                if ( status ){
                    callback.onComplete(status, statusCode, message, groupByCategory(warehouseMeatItems));
                }else{
                    callback.onComplete(status, statusCode, message, null);
                }
            }
        });
    }

    public void getMeatItemAttributes(String meatItemID, final MeatItemAttributesCallback callback){

        retrofitInterface.getMeatItemAttributes(deviceID, appVersion, meatItemID, new Callback<ResponseMeatItemAttributes>() {
            @Override
            public void success(ResponseMeatItemAttributes responseMeatItemAttributes, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ MEAT_ITEM_ATTRIBUTES ]  >> Success");
                callback.onComplete(true, 200, "Success", responseMeatItemAttributes.getAttributes());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ MEAT_ITEM_ATTRIBUTES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ MEAT_ITEM_ATTRIBUTES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateMeatItemDetails(final String meatItemID, final RequestUpdateMeatItem requestUpdateMeatItem, final StatusCallback callback) {

        retrofitInterface.updateMeatItem(deviceID, appVersion, meatItemID, requestUpdateMeatItem, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_MEAT_ITEM ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_MEAT_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_MEAT_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateWarehouseMeatItemDetails(final String warehouseMeatItemID, final RequestUpdateWarehouseMeatItem requestUpdateWarehouseMeatItem, final StatusCallback callback) {

        retrofitInterface.updateWarehouseMeatItem(deviceID, appVersion, warehouseMeatItemID, requestUpdateWarehouseMeatItem, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_WAREHOUSE_MEAT_ITEM ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_WAREHOUSE_MEAT_ITEM ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_WAREHOUSE_MEAT_ITEM ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateWarehouseMeatItemAvailability(final String warehouseMeatItemID, final RequestUpdateWarehouseMeatItemAvailability requestUpdateWarehouseMeatItemAvailability, final StatusCallback callback) {

        retrofitInterface.updateWarehouseMeatItemAvailability(deviceID, appVersion, warehouseMeatItemID, requestUpdateWarehouseMeatItemAvailability, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_WAREHOUSE_MEAT_ITEM_AVAILABILITY ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_WAREHOUSE_MEAT_ITEM_AVAILABILITY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_WAREHOUSE_MEAT_ITEM_AVAILABILITY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateMeatItemAvailabilityDetails(final String meatItemID, final RequestUpdateMeatItemAvaialability requestUpdateMeatItemAvaialability, final UpdateMeatItemAvailabilityCallback callback) {

        retrofitInterface.updateMeatItemAvailability(deviceID, appVersion, meatItemID, requestUpdateMeatItemAvaialability, new Callback<ResponseUpdateMeatItemAvailability>() {
            @Override
            public void success(ResponseUpdateMeatItemAvailability responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_MEAT_ITEM_AVAILABILITY ]  >>  Success");
                callback.onComplete(true, 200, "Success", responseObject.getBuyingPrice(), responseObject.getSellingPrice(), responseObject.getAvailabilitiesList(), responseObject.getAttributesList());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_MEAT_ITEM_AVAILABILITY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", 0, 0, null, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_MEAT_ITEM_AVAILABILITY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", 0, 0, null, null);
                    }
                }
            }
        });
    }

    public void updateMeatItemsAvailabilities(RequestUpdateMeatItemsAvailability requestUpdateMeatItemsAvailability, final StatusCallback callback){

        retrofitInterface.updateMeatItemsAvailabilities(deviceID, appVersion, requestUpdateMeatItemsAvailability, new Callback<ResponseStatus>() {
            @Override
            public void success(ResponseStatus responseStatus, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_MEAT_ITEMS_AVAILABILITIES ]  >>  Success");
                callback.onComplete(true, 200, "Success");
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        String errorMessage = "Error";
                        if ( error.getResponse().getStatus() == 500 ) {
                            try { errorMessage = new JSONObject(new String(((TypedByteArray) error.getResponse().getBody()).getBytes())).getString("msg"); } catch (Exception e) {}
                        }
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_MEAT_ITEMS_AVAILABILITIES ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), errorMessage);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_MEAT_ITEMS_AVAILABILITIES ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void updateAttributeDetails(String attributeID, RequestUpdateAttributeDetails requestUpdateAttributeDetails, final UpdateAttributeDetailsCallback callback){

        retrofitInterface.updateAttributeDetails(deviceID, appVersion, attributeID, requestUpdateAttributeDetails, new Callback<ResponseUpdateAttributeDetails>() {
            @Override
            public void success(ResponseUpdateAttributeDetails responseUpdateAttributeDetails, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_ATTRIBUTE_DETAILS ]  >>  Success");
                callback.onComplete(true, 200, "Success", null);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_ATTRIBUTE_DETAILS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_ATTRIBUTE_DETAILS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void updateAttributeOptionAvailability(String optionID, RequestUpdateOptionAvailability requestUpdateOptionAvailability, final UpdateAttributeOptionAvailabilityCallback callback){

        retrofitInterface.updateAttributeOptionAvailability(deviceID, appVersion, optionID, requestUpdateOptionAvailability, new Callback<ResponseUpdateOptionAvailability>() {
            @Override
            public void success(ResponseUpdateOptionAvailability responseObject, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_ATTRIBUTE_OPTION_AVAILABILITY ]  >>  Success");
                callback.onComplete(true, 200, "Success", responseObject.getPriceDifference(), responseObject.getAvailabilitiesList());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_ATTRIBUTE_OPTION_AVAILABILITY ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", 0, null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ UPDATE_ATTRIBUTE_OPTION_AVAILABILITY ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", 0, null);
                    }
                }
            }
        });
    }


    public void getAfftecedMeatItems(String optionID, final StatusCallback callback){

        retrofitInterface.getAfftecedMeatItems(deviceID, appVersion, optionID, new Callback<ResponseAffectedMeatItems>() {
            @Override
            public void success(ResponseAffectedMeatItems responseAffectedMeatItems, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ AFFECTED_MEAT_ITEMS ]  >>  Success");
                callback.onComplete(true, 200, responseAffectedMeatItems.getAffectedMeatItems());
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ AFFECTED_MEAT_ITEMS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error");
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ AFFECTED_MEAT_ITEMS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error");
                    }
                }
            }
        });
    }

    public void getMeatItemLogs(String meatItemID, RequestItemLogs requestItemLogs, int perPage, final ItemLogsCallback callback){

        retrofitInterface.getMeatItemLogs(deviceID, appVersion, meatItemID, requestItemLogs, perPage, new Callback<List<ItemLogDetails>>() {
            @Override
            public void success(List<ItemLogDetails> itemLogs, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ MEAT_ITEM_LOGS ]  >>  Success");
                callback.onComplete(true, 200, "Success", itemLogs);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ MEAT_ITEM_LOGS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ MEAT_ITEM_LOGS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }

    public void getWarehouseMeatItemLogs(String warehouseMeatItemID, RequestItemLogs requestItemLogs, int perPage, final ItemLogsCallback callback){

        retrofitInterface.getWarehouseMeatItemLogs(deviceID, appVersion, warehouseMeatItemID, requestItemLogs, perPage, new Callback<List<ItemLogDetails>>() {
            @Override
            public void success(List<ItemLogDetails> itemLogs, Response response) {
                Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WAREHOUSE_MEAT_ITEM_LOGS ]  >>  Success");
                callback.onComplete(true, 200, "Success", itemLogs);
            }

            @Override
            public void failure(RetrofitError error) {
                if ( new CheckError(context).check(error) == false ) {
                    try {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WAREHOUSE_MEAT_ITEM_LOGS ]  >>  Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus());
                        callback.onComplete(false, error.getResponse().getStatus(), "Error", null);
                    } catch (Exception e) {
                        Log.d(Constants.LOG_TAG, "\nMEAT_ITEMS_API : [ WAREHOUSE_MEAT_ITEM_LOGS ]  >>  Error = " + error.getKind());
                        callback.onComplete(false, -1, "Error", null);
                    }
                }
            }
        });
    }







    //--

    public List<GroupedMeatItemsList> groupByCategory(List<WarehouseMeatItemDetails> itemsList){

        List<WarehouseMeatItemDetails> enabledItems = new ArrayList<>();
        List<WarehouseMeatItemDetails> disabledItems = new ArrayList<>();
        for (int i=0;i<itemsList.size();i++){
            if ( itemsList.get(i).isEnabled() ){
                enabledItems.add(itemsList.get(i));
            }else{
                disabledItems.add(itemsList.get(i));
            }
        }

        List<WarehouseMeatItemDetails> warehouseMeatItemsList = new ArrayList<>();
        for (int i=0;i<enabledItems.size();i++){
            warehouseMeatItemsList.add(enabledItems.get(i));
        }
        for (int i=0;i<disabledItems.size();i++){
            warehouseMeatItemsList.add(disabledItems.get(i));
        }

        //-----

        List<GroupedMeatItemsList> groupedMeatItemsList = new ArrayList<>();
        Map<String, GroupedMeatItemsList> keyMapSet = new HashMap<>();
        for (int i=0;i<warehouseMeatItemsList.size();i++){
            final WarehouseMeatItemDetails warehouseMeatItemDetails = warehouseMeatItemsList.get(i);
            if ( warehouseMeatItemDetails != null && warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails() != null ){
                if ( keyMapSet.containsKey(warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getBelongsCategoryName()) ) {
                    keyMapSet.get(warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getBelongsCategoryName()).addItem(warehouseMeatItemDetails);
                }
                else {
                    GroupedMeatItemsList groupedList = new GroupedMeatItemsList(warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getBelongsCategoryName(), new ArrayList<WarehouseMeatItemDetails>());
                    keyMapSet.put(warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getBelongsCategoryName(), groupedList);
                    keyMapSet.get(warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getBelongsCategoryName()).addItem(warehouseMeatItemDetails);
                }
            }
        }

        for (String key : keyMapSet.keySet()) {
            groupedMeatItemsList.add(keyMapSet.get(key));
        }

        Collections.sort(groupedMeatItemsList, new Comparator<GroupedMeatItemsList>() {
            public int compare(GroupedMeatItemsList item1, GroupedMeatItemsList item2) {
                return item1.getCategoryName().compareToIgnoreCase(item2.getCategoryName());
            }
        });

        return groupedMeatItemsList;
    }


}
