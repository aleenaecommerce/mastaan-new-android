package com.mastaan.logistics.backend.models;

/**
 * Created by venkatesh on 22/4/16.
 */
public class RequestCreateNewOrderForOrderItem {

    String date;
    String time;
    String wcs;

    public RequestCreateNewOrderForOrderItem(String date, String time, String slotID){
        this.date = date;
        this.time = time;
        this.wcs = slotID;
    }
}
