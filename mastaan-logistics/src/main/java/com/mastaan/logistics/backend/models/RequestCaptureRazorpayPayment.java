package com.mastaan.logistics.backend.models;

/**
 * Created by Venkatesh Uppu on 11/23/2016.
 */
public class RequestCaptureRazorpayPayment {
    String pid;
    double amt;

    public RequestCaptureRazorpayPayment(String paymentID, double amount){
        this.pid = paymentID;
        this.amt = amount;
    }
}
