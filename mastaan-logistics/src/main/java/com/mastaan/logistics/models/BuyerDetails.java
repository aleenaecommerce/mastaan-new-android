package com.mastaan.logistics.models;

import com.mastaan.logistics.constants.Constants;

import java.io.Serializable;

public class BuyerDetails implements Serializable{

    String _id;     //  ID
    String cd;
    String ud;

    String m;       // Mobile Number
    String am;      // Alternate Mobile Number
    String e;       //  Email Address
    String f;       //  First Name
    String l;       //  Last Name
    String d;       //  DateOfBirth
    boolean mr;     //  Matrial Status
    String a;       //  Anniversary Date if Married

    long lp;        //  Loyalty Points
    String r;       // Referral ID

    String mt;      // Membership Type

    float amt;      // Outstanding Amount

    String lo;      // Last Order Date
    String lf;      // Last Followup Date
    String lfc;     // Last Followup Comments

    boolean en;     //  Enabled or Disabled
    String dir;     //  Disabled reason

    String is;      // Install Source Ex: Friend, News Paper etc.
    String isd;     // Install source details

    boolean ros;    // Rated on Playstore

    boolean palt;   // Promotional Alerts Preference

    boolean nia;    // Not interested anymore

    UserDetailsMini foe;    // Followup employee

    String cmt;     // Comments


    public BuyerDetails(){}

    public BuyerDetails(String id) {
        this._id = id;
    }

    public BuyerDetails(String id, String name) {
        this._id = id;
        this.f = name;
    }

    public BuyerDetails(String ID, String joinedDate, String mobileNumber, String name, String email, String dateOfBirth, boolean matrialStatus, String anniversaryDate, String referralID){
        this._id = ID;
        this.cd = joinedDate;
        this.m = mobileNumber;
        this.f = name;
        this.e = email;
        this.d = dateOfBirth;
        this.mr = matrialStatus;
        this.a = anniversaryDate;
        this.r = referralID;
    }

    public String getID() {    return _id;     }

    public BuyerDetails setMobileNumber(String mobileNumber) {
        this.m = mobileNumber;
        return this;
    }
    public String getMobileNumber() {      return m;   }

    public String getAlternateMobile() {
        return am;
    }


    public BuyerDetails setEmail(String email){
        this.e = email;
        return this;
    }
    public String getEmail() {
        return e;
    }

    public String getName() {
        if ( l != null && l.length() > 0 ){
            return f+" "+l;
        }
        return f;
    }

    public String getAvailableName(){
        if ( getName() != null ){
            return getName();
        }else if ( m != null && m.length() > 0 ){
            return getMobileNumber();
        }else if ( e != null && e.length() > 0 ){
            return getEmail();
        }
        return "";
    }
    public BuyerDetails setName(String name){
        this.f = name;
        return this;
    }

    public String getDOB() {
        return d;
    }
    public void setDOB(String dob){  this.d = dob;   }

    public boolean getMatrialStatus() {
        return mr;
    }
    public void setMatrialStatus(boolean mr) {
        this.mr = mr;
    }

    public String getAnniversary() {
        return a;
    }
    public void setAnniversary(String anniversary){  this.a = anniversary;   }

    public long getLoyaltyPoints() {
        return lp;
    }

    public String getReferralID() {
        return r;
    }

    public String getMembershipType() {
        if ( mt != null && mt.length() > 0 ){
            if ( mt.equalsIgnoreCase("bm") ){
                return Constants.BRONZE;
            }else if ( mt.equalsIgnoreCase("sm") ){
                return Constants.SILVER;
            }else if ( mt.equalsIgnoreCase("gm") ){
                return Constants.GOLD;
            }else if ( mt.equalsIgnoreCase("dm") ){
                return Constants.DIAMOND;
            }else if ( mt.equalsIgnoreCase("pm") ){
                return Constants.PLATINUM;
            }
        }
        return "Bronze";
    }

    public String getJoinedDate() {     return cd;      }

    public BuyerDetails setOutstandingAmount(float outstandingAmount) {
        this.amt = outstandingAmount;
        return this;
    }

    public float getOutstandingAmount() {
        return amt;
    }

    public String getLastOrderDate() {
        return lo;
    }

    public BuyerDetails setLastFollowupDate(String lastFollowupDate) {
        this.lf = lastFollowupDate;
        return this;
    }
    public String getLastFollowupDate() {
        return lf;
    }

    public BuyerDetails setLastFollowupComments(String lastFollowupComments) {
        this.lfc = lastFollowupComments;
        return this;
    }
    public String getLastFollowupComments() {
        return lfc;
    }

    public boolean isEnabled() {
        return en;
    }
    public void setEnabled(boolean isEnabled) {
        this.en = isEnabled;
    }

    public String getDisabledReason() {
        if ( dir == null ){ dir = "";   }
        return dir;
    }
    public void setDisabledReason(String dir) {
        this.dir = dir;
    }

    public BuyerDetails setInstallSource(String installSource) {
        this.is = installSource;
        return this;
    }
    public String getInstallSource() {
        if ( is == null ){  is = "";    }
        return is;
    }

    public BuyerDetails setInstallSourceDetails(String installSourceDetails) {
        this.isd = installSourceDetails;
        return this;
    }
    public String getInstallSourceDetails() {
        return isd;
    }

    public boolean isRatedOnPlaystore() {
        return ros;
    }

    public void setPromotionalAlertsPreference(boolean promotionalAlertsPreference) {
        this.palt = promotionalAlertsPreference;
    }
    public boolean getPromotionalAlertsPreference() {
        return palt;
    }

    public boolean isNotInterestedAnymore() {
        return nia;
    }

    public BuyerDetails setFollowupEmployee(UserDetailsMini followupEmployee) {
        this.foe = followupEmployee;
        return this;
    }
    public UserDetailsMini getFollowupEmployee() {
        return foe;
    }

    public BuyerDetails setComments(String comments) {
        this.cmt = comments;
        return this;
    }
    public String getComments() {
        return cmt;
    }

}
