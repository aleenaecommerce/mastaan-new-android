package com.mastaan.logistics.models;

/**
 * Created by Naresh-Crypsis on 08-11-2015.
 */
public class DayTask {
    String date;
    int task_count;

    DayTask() {
    }

    public DayTask(String date, int task_count) {
        this.date = date;
        this.task_count = task_count;
    }

    public String getDate() {
        return date;
    }

    public int getTaskCount() {
        return task_count;
    }
}
