package com.mastaan.logistics.models;

/**
 * Created by venkatesh on 16/3/16.
 */
public class CouponDetails {

    String _id;     //  ID
    String cc;      //  CouponCode

    String std;     // StartDate
    String exd;     // ExpiredDate

    String refc;    // Referral Code
    String refb;  // Referral Buyer

    String ofn;     // Offer Name


    public String getID() {
        return _id;
    }

    public String getCouponCode() {
        return cc;
    }

    public String getReferralCode() {
        return refc;
    }

    public String getReferralBuyer() {
        return refb;
    }

    public String getOfferName() {
        return ofn;
    }
}
