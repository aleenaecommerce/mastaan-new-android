package com.mastaan.logistics.models;

import java.util.ArrayList;
import java.util.List;

public class AttributeDetails {
    String _id;     //  Attribute ID
    String n;       //  Attribute Name
    String opt;     // Display type
    List<AttributeOptionDetails> op;        //  Available Options

    boolean ila;        // Item Level Attribute
    boolean ac;     //  Is change daily basis


    public AttributeDetails(String id, String name, List<AttributeOptionDetails> options, String optionsDisplayType){
        this._id = id;
        this.n = name;
        this.op = options;
        this.opt = optionsDisplayType;
    }



    public String getID() {
        return _id;
    }

    public void setName(String name) {
        this.n = name;
    }
    public String getName() {
        if ( n != null ){   return n;   }
        return "";
    }

    public List<AttributeOptionDetails> getAvailableOptions() {
        if ( op != null ){
            for (int i=0;i<op.size();i++){
                op.get(i).setStatus(true);
            }
            return op;
        }
        return new ArrayList<>();
    }
    public void updateOptionDetails(int index, AttributeOptionDetails attributeOptionDetails){
        if ( op != null && op.size() > 0 && index < op.size() ){
            op.set(index, attributeOptionDetails);
        }
    }

    public void addOption(AttributeOptionDetails attributeOptionDetails){
        if ( op == null ){  op = new ArrayList<>(); }
        op.add(attributeOptionDetails);
    }

    public String getOptionsDisplayType() {
        return opt;
    }

    public void setAvailabilityChangeDaywise(boolean isAvailabilityChangeDaywise) {
        this.ac = isAvailabilityChangeDaywise;
    }

    public boolean isAvailabilityChangeDaywise() {
        return ac;
    }

    public boolean isItemLevelAttribute() {
        return ila;
    }

}
