package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 05-01-2016.
 */

public class PaymentDetails {
    String id;
    String status;
    double amount;
    String currency;

    double amount_refunded;


    public String getID() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public double getAmount() {
        return amount/100;
    }

    public String getCurrency() {
        return currency;
    }

    public double getAmountRefunded() {
        return amount_refunded/100;
    }

    public double getAvailableAmount(){
        return (amount-amount_refunded)/100;
    }

}
