package com.mastaan.logistics.models;

public class MessageDetails {

    String _id;

    String type;

    boolean status;

    String title;
    String details;

    String image;

    String url;
    String app_url;

    String action;

    String start_date;
    String end_date;

    boolean show_once;


    public MessageDetails(String type, boolean status, String title, String details, String image, String url, String app_url, String action, String start_date, String end_date, boolean show_once){
        this.type = type;
        this.status = status;
        this.title = title;
        this.details = details;
        this.image = image;
        this.url = url;
        this.app_url = app_url;
        this.action = action;
        this.start_date = start_date;
        this.end_date = end_date;
        this.show_once = show_once;
    }


    public String getID() {
        if ( _id == null ){ _id = "";   }
        return _id;
    }

    public String getType() {
        if ( type == null ){  type = "";    }
        return type;
    }
    public MessageDetails setType(String type) {
        this.type = type;
        return this;
    }

    public boolean getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public String getDetails() {
        return details;
    }

    public String getImage() {
        return image;
    }

    public String getURL() {
        return url;
    }

    public String getAppURL() {
        return app_url;
    }

    public String getAction() {
        return action;
    }

    public String getStartDate() {
        return start_date;
    }

    public String getEndDate() {
        return end_date;
    }

    public boolean showOnce() {
        return show_once;
    }

}
