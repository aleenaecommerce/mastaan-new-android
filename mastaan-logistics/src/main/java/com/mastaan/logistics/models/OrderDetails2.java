package com.mastaan.logistics.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 28/1/16.
 */
public class OrderDetails2 extends BaseOrderDetails{

    List<String> i = new ArrayList<>();     // Order Items List


    public OrderDetails2(){}

    public OrderDetails2(OrderDetails orderDetails){
        super(orderDetails);
        List<OrderItemDetails> orderItems = orderDetails.getOrderedItems();
        List<String> orderItemsStrings = new ArrayList<>();
        for (int i=0;i<orderItems.size();i++){
            if ( orderItems.get(i) != null ){
                orderItemsStrings.add(orderItems.get(i)._id);
            }
        }
        this.i = orderItemsStrings;
    }

    public List<String> getOrderedItems() {
        if (i == null) { i = new ArrayList<>();}
        return i;
    }

}
