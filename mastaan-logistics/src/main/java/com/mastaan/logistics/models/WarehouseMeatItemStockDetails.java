package com.mastaan.logistics.models;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 20/07/18.
 */

public class WarehouseMeatItemStockDetails {

    String _id;         // ID
    WarehouseMeatItemDetails wmi;         // Warehouse Meat Item
    List<AttributeOptionDetails> ilao;  // Item Level Attribute Options
    String wh;          // Warehouse

    String ty;          // Stock Type ('ul' - Unlimited, 'l' - Limited, 'd' - daily )
    double qu;          // In Kgs or Units or Pieces
    List<String> ad;    // Available Days for Daywise stock ('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat')
    double tqu;         // Threshold quantity for unlimited stock


    public String getID() {
        return _id;
    }

    public WarehouseMeatItemDetails getWarehouseMeatItemDetails() {
        return wmi;
    }
    public String getWarehouseMeatItemID() {
        return wmi!=null?wmi.getID():null;
    }

    public List<AttributeOptionDetails> getItemLevelAttributeOptions() {
        return ilao;
    }
    public List<String> getItemLevelAttributeOptionsIDs() {
        List<String> itemLevelAttributeOptions = new ArrayList<>();
        if ( ilao != null && ilao.size() > 0 ) {
            for (int i = 0; i < ilao.size(); i++) {
                if ( ilao.get(i) != null ) {
                    itemLevelAttributeOptions.add(ilao.get(i).getID());
                }
            }
        }
        return itemLevelAttributeOptions;
    }
    public List<String> getItemLevelAttributeOptionsNames() {
        List<String> itemLevelAttributeOptionsNames = new ArrayList<>();
        if ( ilao != null && ilao.size() > 0 ) {
            for (int i = 0; i < ilao.size(); i++) {
                if ( ilao.get(i) != null ) {
                    itemLevelAttributeOptionsNames.add(ilao.get(i).getName()!=null?ilao.get(i).getName():ilao.get(i).getID());
                }
            }
        }
        return itemLevelAttributeOptionsNames;
    }

    public String getWarehouse() {
        return wh;
    }

    public void setType(String type) {
        this.ty = type;
    }
    public String getType() {
        if ( ty == null ){  ty = "";    }
        return ty;
    }
    public String getTypeName() {
        if ( getType().equalsIgnoreCase("ul") ){
            return Constants.UNLIMITED;
        }else if ( getType().equalsIgnoreCase("l") ){
            return Constants.LIMITED;
        }else if ( getType().equalsIgnoreCase("d") ){
            return Constants.DAYWISE;
        }
        return getType();
    }

    public double getQuantity() {
        return qu;
    }

    public void setAvailableDays(List<String> availableDays) {
        this.ad = availableDays;
    }
    public List<String> getAvailableDays() {
        if ( ad == null ){  ad = new ArrayList<>(); }
        return ad;
    }

    public void setThresholdQuantity(double thresholdQuantity) {
        this.tqu = thresholdQuantity;
    }
    public double getThresholdQuantity() {
        return tqu;
    }

    public String getFormattedInfo(String quantityUnit){
        return "<b>"+getTypeName()+""+(getTypeName().equalsIgnoreCase(Constants.DAYWISE)?" ("+ CommonMethods.getStringFromStringList(getAvailableDays())+")":"")+"</b>"
                + (getTypeName().equalsIgnoreCase(Constants.UNLIMITED)&&getThresholdQuantity()>0?" (DailyTQ: <b>"+CommonMethods.getInDecimalFormat(getThresholdQuantity())+" "+quantityUnit+(getThresholdQuantity()==1?"":"s")+"</b>)":"")
                +"  available <b>"+CommonMethods.getInDecimalFormat(getQuantity())+" "+quantityUnit+(getQuantity()==1?"":"s")+"</b>";
    }

}
