package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.constants.Constants;

/**
 * Created by Naresh-Crypsis on 17-12-2015.
 */
public class Job {
    String trip_type;
    String cd;

    String customer_id;
    String order_id;
    String item_id;
    String area;
    String customer_name;
    String address_line_1;
    String address_line_2;
    String land_mark;
    String item_name;
    String phone_number;
    String alternative_number;
    String slot;
    String location;
    //float total_amount_of_order;
    double item_amount;
    double order_amount;
    double delivery_charges;
    double service_charges;
    double surcharges;
    double govt_taxes;
    double order_discount;
    double cash_amount;
    double online_payment_amount;
    int payment_type;
    String image_url;
    Double quantity;
    String formatted_extras;
    String quantity_type;

    float buyer_outstanding;

    Trip trip;

    class Trip{
        String sname;
        String sloc;
        String ename;
        String eloc;
        long pdt;
        long dis;
    }

    public Job(String customer_id) {
        this.customer_id = customer_id;
    }

    public Job(OrderItemDetails orderItemDetails, String itemName) {

        OrderDetails2 customer = orderItemDetails.getOrderDetails();
        MeatItemDetails meatItemDetails = orderItemDetails.getMeatItemDetails();
        order_id = orderItemDetails.getOrderID();
        item_id = orderItemDetails.getID();
        try{customer_id = orderItemDetails.getOrderDetails().getBuyerDetails().getID();}catch (Exception e){}
        customer_name = customer.getCustomerName();
        image_url = meatItemDetails.getPicture();
        quantity = orderItemDetails.getQuantity();
        quantity_type = orderItemDetails.getQuantityUnit();
        formatted_extras = orderItemDetails.getFormattedExtras();
        item_name = itemName != null ?itemName:meatItemDetails.getNameWithCategoryAndSize();
        land_mark = customer.getDeliveryAddressLandmark();
        location = customer.getDeliveryAddressLatLngString();
        phone_number = customer.getCustomerMobile();
        alternative_number = customer.getCustomerAlternateMobile();
        item_amount = orderItemDetails.getAmountOfItem();
        order_amount = orderItemDetails.getTotalOrderAmount();
        delivery_charges = orderItemDetails.getOrderDetails().getDeliveryCharges();
        service_charges = orderItemDetails.getOrderDetails().getServiceCharges();
        surcharges = orderItemDetails.getOrderDetails().getSurcharges();
        govt_taxes = orderItemDetails.getOrderDetails().getGovernmentTaxes();
        order_discount = orderItemDetails.getTotalOrderDiscount();
        cash_amount = orderItemDetails.getTotalOrderCashAmount();
        online_payment_amount = orderItemDetails.getTotalOrderOnlinePaymentAmount();
        payment_type = orderItemDetails.getPaymentType();
        address_line_1 = CommonMethods.getStringFromStringArray(new String[]{customer.getDeliveryAddressPremise(), customer.getDeliveryAddressSubLocalityLevel2()});
        address_line_2 = CommonMethods.getStringFromStringArray(new String[]{customer.getDeliveryAddressSubLocalityLevel1(), customer.getDeliveryAddressLocality(), customer.getDeliveryAddressState(), customer.getDeliveryAddressPinCode()});
        slot = orderItemDetails.getDeliveryTimeSlot();
        area = customer.getDeliveryAddressSubLocalityLevel1();
        if ( area == null || area.length() == 0 ){
            area = customer.getDeliveryAddressSubLocalityLevel2();
        }

        buyer_outstanding = customer.getBuyerDetails().getOutstandingAmount();
    }

    public String getCreatedDate() {
        return DateMethods.getReadableDateFromUTC(cd);
    }

    public String getType() {
        return trip_type;
    }

    public String getQuantity_type() {
        return quantity_type;
    }

    public LatLng getLocation() {
        return LatLngMethods.getLatLngFromString(location);
    }

    public String getCustomer_id() {
        return customer_id;
    }


    public Double getQuantity() {
        return quantity;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public String getAddress_line_2() {
        return address_line_2;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public String getFormatted_extras() {
        return formatted_extras;
    }

    public String getAlternative_number() {
        return alternative_number;
    }

    public String getArea() {
        return area;
    }

    public String getImage_url() {
        return "" + image_url;
    }

    public String getOrderID() {
        return order_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public String getItemName() {
        return item_name;
    }

    public String getLandMark() {
        return land_mark;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getSlot() {
        return slot;
    }

    public double getAmountOfItem() {
        return item_amount;
    }

    public double getOrderDiscount() {
        return order_discount;
    }

    public double getOrderAmount() {
        return order_amount;
    }

    public double getDeliveryCharges() {
        return delivery_charges;
    }

    public double getServiceCharges() {
        return service_charges;
    }

    public double getSurcharges() {
        return surcharges;
    }

    public double getGovtTaxes() {
        return govt_taxes;
    }

    public double getCashAmount() {
        return cash_amount;
    }

    public double getOnlinePaymentAmount() {
        return online_payment_amount;
    }

    public String getFullOrderTotalAmountSplitString(){
        String orderAmountString = SpecialCharacters.RS + " " + CommonMethods.getInDecimalFormat(getOrderAmount()) + " (" + getPaymentTypeString() + ")";
        if ( getCashAmount() != 0 && getOnlinePaymentAmount() != 0 ){
            orderAmountString = SpecialCharacters.RS + " "+CommonMethods.getIndianFormatNumber(getOrderAmount()) + " \n[ "+CommonMethods.getInDecimalFormat(getOnlinePaymentAmount())+"(OP), "+CommonMethods.getInDecimalFormat(getCashAmount())+"(COD"+(getCashAmount()<0?",Return)":")")+" ]";
        }
        if ( getDeliveryCharges() > 0 || getServiceCharges() > 0 || getSurcharges() > 0 || getGovtTaxes() > 0 ){
            orderAmountString += "\n[";
            orderAmountString += CommonMethods.getIndianFormatNumber(getOrderAmount()-getDeliveryCharges()-getServiceCharges()-getSurcharges()-getGovtTaxes()+getOrderDiscount())+" (OA)";
            if ( getDeliveryCharges() > 0 ){
                orderAmountString += ", "+CommonMethods.getIndianFormatNumber(getDeliveryCharges())+" (DC)";
            }
            if ( getServiceCharges() > 0 ){
                orderAmountString += ", "+CommonMethods.getIndianFormatNumber(getServiceCharges())+" (SvC)";
            }
            if ( getSurcharges() > 0 ){
                orderAmountString += ", "+CommonMethods.getIndianFormatNumber(getSurcharges())+" (SuC)";
            }
            if ( getGovtTaxes() > 0 ){
                orderAmountString += ", "+CommonMethods.getIndianFormatNumber(getGovtTaxes())+" (GT)";
            }
            orderAmountString += "]";
        }
        return orderAmountString;
    }

    public int getPaymentType() {
        return payment_type;
    }

    public String getPaymentTypeString() {
        if (payment_type == 1) {
            return Constants.CASH_ON_DELIVERY;
        } else if (payment_type == 6||payment_type == 7) {
            return Constants.ONLINE_PAYMENT;
        }else if (payment_type == 8) {
            return Constants.PAYTM_WALLET;
        }
        return "";
    }

    public float getBuyerOutstanding() {
        return buyer_outstanding;
    }

    public String getStartPoint(){
        if ( trip != null && trip.sname != null && trip.sname.length() > 0 ){ return trip.sname;    }
        return "NA";
    }
    public LatLng getStartPointLatLng(){
        if (trip != null && trip.sloc != null ){
            return LatLngMethods.getLatLngFromString(trip.sloc);
        }
        return new LatLng(0,0);
    }
    public String getEndPoint(){
        if ( trip != null && trip.ename != null && trip.ename.length() > 0 ){ return trip.ename;    }
        return "NA";
    }
    public LatLng getEndPointLatLng(){
        if (trip != null && trip.eloc != null ){
            return LatLngMethods.getLatLngFromString(trip.eloc);
        }
        return new LatLng(0,0);
    }

    public long getEstimatedTripTimeInSeconds(){
        if ( trip != null ){
            return trip.pdt;
        }
        return 0;
    }

    public long getEstimatedTripDistanceInMeters(){
        if ( trip != null ){
            return trip.dis;
        }
        return 0;
    }

    //---

    public String getFormattedJobDetailsString(){
        try{
            String detailsString = "";
            if ( getType().equalsIgnoreCase("d")) {
                detailsString += ("<b><u>Delivering:</u> " + CommonMethods.capitalizeFirstLetter(getCustomer_name()) + "'s order </b><br>"
                        + getAddress_line_1() + getAddress_line_2()
                );
            }else{
                detailsString += ("<b><u>Travelling to:</u></b><br>"+getEndPoint()
                );
            }
            detailsString += "<br><br>Started: <b>"+DateMethods.getDateInFormat(getCreatedDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</b>";
            detailsString += "<br>Estimated: <b>"+CommonMethods.getIndianFormatNumber(getEstimatedTripDistanceInMeters()/1000)+" kms</b> [<b>"+DateMethods.getTimeStringFromMinutes(getEstimatedTripTimeInSeconds()/60)+"</b>]";

            return detailsString;
        }catch (Exception e){}
        return "";
    }



}
