package com.mastaan.logistics.models;

/**
 * Created by Venkatesh on 17/10/15.
 */
public class OrderItemAttributeDetails {
    String at;      //  AttributeID
    AttributeOptionDetails opt;     //  OptionDetails

    public String getAttributeID() {
        return at;
    }
    public AttributeOptionDetails getOptionDetails() {
        return opt;
    }
}
