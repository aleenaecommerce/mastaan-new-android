package com.mastaan.logistics.models;

import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venaktesh Uppu on 9/9/16.
 */
public class DayDetails {
    String date;
    boolean e;
    List<String> c;
    String msg;

    public DayDetails(String date, boolean isEnabled, String message){
        this.date = date;
        this.e = isEnabled;
        this.msg = message;
    }

    public String getDate() {
        return DateMethods.getReadableDateFromUTC(date);
    }

    public void setEnabled(boolean isEnabled) {
        this.e = isEnabled;
    }

    public boolean isEnabled() {
        return e;
    }

    public void setMessage(String message) {
        this.msg = message;
    }

    public void setCategories(List<String> categories) {
        this.c = categories;
    }

    public List<String> getCategories() {
        if ( c != null ){   return c;   }
        return new ArrayList<>();
    }

    public String getMessage() {
        return msg;
    }
}
