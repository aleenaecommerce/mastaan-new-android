package com.mastaan.logistics.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleena on 29/1/16.
 */
public class GroupedOrdersItemsList {

    String categoryName;
    List<OrderItemDetails> itemsList;

    public GroupedOrdersItemsList(){}

    public GroupedOrdersItemsList(String categoryName, List<OrderItemDetails> itemsList) {
        this.categoryName = categoryName;
        this.itemsList = itemsList;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        if ( categoryName == null ){    categoryName ="";   }
        return categoryName;
    }

    public List<OrderItemDetails> getItems() {
        if ( itemsList == null ){ itemsList = new ArrayList<>();    }
        return itemsList;
    }

    public void setItems(List<OrderItemDetails> itemsList) {
        this.itemsList = itemsList;
    }

    public void addItem(OrderItemDetails orderItemDetails) {
        if ( itemsList == null ){ itemsList = new ArrayList<>();  }
        itemsList.add(orderItemDetails);
    }
}
