package com.mastaan.logistics.models;

import android.util.Log;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 09/02/18.
 */

public class FollowupDetails {
    String _id;

    String cd;              // Created date
    String ty;              // Type
    BuyerDetails b;         // Buyer
    UserDetailsMini cb;     // Created by
    String c;               // Comments

    boolean exc;            // Excluded

    String o;               // Order

    boolean rio;            // Resulted in order


    public String getID() {
        return _id;
    }

    public String getCreatedDate() {
        return cd;
    }

    public String getType() {
        if ( ty == null ){  ty = "";    }
        return ty;
    }
    public String getTypeName() {
        if ( ty == null ){  ty = "";    }

        if ( ty.equalsIgnoreCase(Constants.GENERAL_FOLLOWUP) ){
            return Constants.GENERAL;
        }else if ( ty.equalsIgnoreCase(Constants.FEEDBACK_FOLLOWUP) ){
            return Constants.FEEDBACK;
        }else if ( ty.equalsIgnoreCase(Constants.ORDER_FOLLOWUP) ){
            return Constants.ORDER;
        }else if ( ty.equalsIgnoreCase(Constants.FIRST_ORDER_FOLLOWUP) ){
            return Constants.FIRST_ORDER;
        }else if ( ty.equalsIgnoreCase(Constants.CART_FOLLOWUP) ){
            return Constants.CART;
        }
        return ty;
    }

    public BuyerDetails getBuyerDetails() {
        return b;
    }
    public String getBuyerName(){
        if ( b != null ){   return b.getAvailableName();   }
        return "Buyer Name";
    }

    public UserDetailsMini getEmployeeDetails() {
        return cb;
    }
    public String getEmployeeName(){
        if ( cb != null ){   return cb.getAvailableName();   }
        return "Employee Name";
    }

    public String getComments() {
        if ( c == null ){   c = ""; }
        return c;
    }

    public boolean isExcluded() {
        return exc;
    }

    public String getOrderID() {
        return o;
    }


    public boolean isResultedInOrder() {
        return rio;
    }
}
