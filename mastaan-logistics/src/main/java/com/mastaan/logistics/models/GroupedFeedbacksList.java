package com.mastaan.logistics.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 10/07/16.
 */
public class GroupedFeedbacksList {

    String orderID;
    String deliveryDate;
    List<FeedbackDetails> itemsList;

    public GroupedFeedbacksList(){}

    public GroupedFeedbacksList(String orderID, String deliveryDate, List<FeedbackDetails> itemsList) {
        this.orderID = orderID;
        this.deliveryDate = deliveryDate;
        this.itemsList = itemsList;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public List<FeedbackDetails> getItems() {
        if ( itemsList == null ){ itemsList = new ArrayList<>();    }
        return itemsList;
    }

    public void setItems(List<FeedbackDetails> itemsList) {
        this.itemsList = itemsList;
    }

    public void addItem(FeedbackDetails feedbackDetails) {
        if ( itemsList == null ){ itemsList = new ArrayList<>();  }
        itemsList.add(feedbackDetails);
    }
}
