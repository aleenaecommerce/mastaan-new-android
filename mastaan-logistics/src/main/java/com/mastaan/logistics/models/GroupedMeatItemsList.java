package com.mastaan.logistics.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleena on 29/1/16.
 */
public class GroupedMeatItemsList {

    String categoryName;
    List<String> subCategoriesNames;
    List<WarehouseMeatItemDetails> itemsList;

    public GroupedMeatItemsList(){}

    public GroupedMeatItemsList(String categoryName, List<WarehouseMeatItemDetails> itemsList) {
        this.categoryName = categoryName;
        this.itemsList = itemsList;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        if ( categoryName != null ){    return categoryName;    }
        return "";
    }

    public List<String> getSubCategoriesNames() {
        return subCategoriesNames;
    }

    public List<WarehouseMeatItemDetails> getItems() {
        if ( itemsList == null ){ itemsList = new ArrayList<>();    }
        return itemsList;
    }

    public void setItems(List<WarehouseMeatItemDetails> itemsList) {
        this.itemsList = itemsList;
    }

    public void addItem(WarehouseMeatItemDetails warehouseMeatItemDetails) {
        if ( itemsList == null ){ itemsList = new ArrayList<>();  }
        if ( subCategoriesNames == null ){  subCategoriesNames = new ArrayList<>(); }
        if ( subCategoriesNames.contains(warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getParentCategoryName()) == false ){
            subCategoriesNames.add(warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getParentCategoryName());
        }
        itemsList.add(warehouseMeatItemDetails);
    }

}
