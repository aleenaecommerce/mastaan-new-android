package com.mastaan.logistics.models;

/**
 * Created by venkatesh on 17/8/16.
 */
public class ChangeDetails {
    boolean visibleChanges;
    boolean nonVisibleChanges;

    public boolean isAnyChanges(){
        if ( visibleChanges || nonVisibleChanges ){
            return true;
        }
        return false;
    }

    public void setVisibleChanges(boolean visibleChanges) {
        this.visibleChanges = visibleChanges;
    }

    public boolean isAnyVisibleChanges() {
        return visibleChanges;
    }

    public void setNonVisibleChanges(boolean nonVisibleChanges) {
        this.nonVisibleChanges = nonVisibleChanges;
    }

    public boolean isAnyNonVisibleChanges() {
        return nonVisibleChanges;
    }
}
