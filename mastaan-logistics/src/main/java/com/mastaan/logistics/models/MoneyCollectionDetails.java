package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 27/10/16.
 */
public class MoneyCollectionDetails {

    String _id;

    String ty;          // o - Order, amt - Outstanding

    OrderDetails o;     // For type 'o'   > Order
    BuyerDetails b;     // For type 'amt' > Outstanding

    List<MoneyCollectionTransactionDetails> trs;        // Transactions List

    String c;           // Comments


    public String getID() {
        return _id;
    }

    public String getType() {
        if ( ty == null ){  ty = "";    }
        return ty;
    }

    public OrderDetails getOrderDetails() {
        if ( o == null && getType().equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_ORDER)){   o = new OrderDetails();  }
        return o;
    }

    public BuyerDetails getBuyerDetails() {
        if ( b == null && getType().equalsIgnoreCase(Constants.MONEY_COLLECTION_FOR_OUTSTANDING)){   b = new BuyerDetails();  }
        return b;
    }

    public double getCollectedAmount() {
        return getCollectedAmount(null);
    }
    public double getCollectedAmount(String date) {
        double amount = 0;
        if ( trs != null && trs.size() > 0 ){
            for (int i=0;i<trs.size();i++){
                MoneyCollectionTransactionDetails transactionDetails = trs.get(i);
                if ( transactionDetails != null ) {
                    if ( date == null || date.trim().length() == 0 || DateMethods.compareDates(date.trim(), DateMethods.getReadableDateFromUTC(transactionDetails.getCollectionDate(), DateConstants.DD_MM_YYYY)) == 0 ) {
                        amount += transactionDetails.getAmount();
                    }
                }
            }
        }
        return amount;
    }

    public boolean isConsolidated() {
        boolean isConsolidated = true;
        for (int i=0;i<getTransactions().size();i++){
            if ( getTransactions().get(i).isConsolidated() == false ){
                isConsolidated = false;
                break;
            }
        }
        return isConsolidated;
    }

    public List<MoneyCollectionTransactionDetails> getTransactions() {
        if ( trs == null ){ trs = new ArrayList<>();    }
        return trs;
    }

    public String getComments() {
        return c;
    }
}
