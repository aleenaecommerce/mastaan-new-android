package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 12/10/18.
 */

public class EmployeeBonusDetails {
    String ty;            // Type
    String st;          // Status
    UserDetailsMini emp;   // Employee
    String cd;              // Created Date
    double amt;         // Amount

    OrderDetails o;     // Order
    BuyerDetails b;     // Buyer

    String det;         // Details


    public String getType() {
        return ty!=null?ty:"";
    }

    public String getStatus() {
        return st!=null?st:"";
    }

    public UserDetailsMini getEmployeeDetails() {
        return emp!=null?emp:new UserDetailsMini();
    }

    public String getCreatedDate() {
        return cd!=null?cd:"";
    }

    public double getAmount() {
        return amt;
    }

    public OrderDetails getOrderDetails() {
        return o;
    }

    public BuyerDetails getBuyerDetails() {
        return b;
    }

    public String getDetails() {
        return det!=null?det:"";
    }
}
