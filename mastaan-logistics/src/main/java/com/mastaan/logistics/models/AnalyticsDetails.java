package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;

/**
 * Created by Venkatesh Uppu on 05/06/18.
 */

public class AnalyticsDetails {
    String title;
    String details;

    AnalyticsOrders analyticsOrders;
    AnalyticsTopBuyer analyticsTopBuyer;
    AnalyticsInstallSource analyticsInstallSource;
    AnalyticsCouponCode analyticsCouponCode;
    AnalyticsReferral analyticsReferral;
    AnalyticsMeatItemAvailabilityRequest analyticsMeatItemAvailabilityRequest;


    public String getTitle() {
        if ( analyticsOrders != null ){
            return DateMethods.getDateInFormat(analyticsOrders.getDate(), DateConstants.MMM_DD_YYYY);
        }
        else if ( analyticsTopBuyer != null ){
            return analyticsTopBuyer.getBuyer().getAvailableName();
        }
        else if ( analyticsInstallSource != null ){
            return analyticsInstallSource.getID()==null||analyticsInstallSource.getID().trim().length()==0?"Unknown":analyticsInstallSource.getID();
        }
        else if ( analyticsCouponCode != null ){
            return analyticsCouponCode.getFormattedTitle();
        }
        else if ( analyticsReferral != null ){
            return analyticsReferral.getFormattedTitle();
        }
        else if ( analyticsMeatItemAvailabilityRequest != null ){
            return analyticsMeatItemAvailabilityRequest.getMeatItemName();
        }
        return title;
    }

    public String getDetails() {
        if ( analyticsOrders != null ){
            return analyticsOrders.getFormattedDetails();
        }
        else if ( analyticsTopBuyer != null ){
            return analyticsTopBuyer.getFormattedDetails();
        }
        else if ( analyticsInstallSource != null ){
            return analyticsInstallSource.getCount()+"";
        }
        else if ( analyticsCouponCode != null ){
            return analyticsCouponCode.getFormattedDetails();
        }
        else if ( analyticsReferral != null ){
            return analyticsReferral.getFormattedDetails();
        }
        else if ( analyticsMeatItemAvailabilityRequest != null ){
            return analyticsMeatItemAvailabilityRequest.getFormattedDetails();
        }
        return details;
    }

    public String getAction(){
        if ( analyticsReferral != null ){
            return analyticsReferral.getBuyer()!=null?analyticsReferral.getBuyer().getAvailableName():null;
        }
        return null;
    }


    public AnalyticsDetails setAnalyticsOrders(AnalyticsOrders analyticsOrders) {
        this.analyticsOrders = analyticsOrders;
        return this;
    }
    public AnalyticsOrders getAnalyticsOrders() {
        return analyticsOrders;
    }

    public AnalyticsDetails setAnalyticsTopBuyer(AnalyticsTopBuyer analyticsTopBuyer) {
        this.analyticsTopBuyer = analyticsTopBuyer;
        return this;
    }
    public AnalyticsTopBuyer getAnalyticsTopBuyer() {
        return analyticsTopBuyer;
    }

    public AnalyticsDetails setAnalyticsInstallSource(AnalyticsInstallSource analyticsInstallSource) {
        this.analyticsInstallSource = analyticsInstallSource;
        return this;
    }
    public AnalyticsInstallSource getAnalyticsInstallSource() {
        return analyticsInstallSource;
    }


    public AnalyticsDetails setAnalyticsCouponCode(AnalyticsCouponCode analyticsCouponCode) {
        this.analyticsCouponCode = analyticsCouponCode;
        return this;
    }
    public AnalyticsCouponCode getAnalyticsCouponCode() {
        return analyticsCouponCode;
    }


    public AnalyticsDetails setAnalyticsReferral(AnalyticsReferral analyticsReferral) {
        this.analyticsReferral = analyticsReferral;
        return this;
    }
    public AnalyticsReferral getAnalyticsReferral() {
        return analyticsReferral;
    }

    public AnalyticsDetails setAnalyticsMeatItemAvailabilityRequest(AnalyticsMeatItemAvailabilityRequest analyticsMeatItemAvailabilityRequest) {
        this.analyticsMeatItemAvailabilityRequest = analyticsMeatItemAvailabilityRequest;
        return this;
    }
    public AnalyticsMeatItemAvailabilityRequest getAnalyticsMeatItemAvailabilityRequest() {
        return analyticsMeatItemAvailabilityRequest;
    }

}
