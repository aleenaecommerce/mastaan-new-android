package com.mastaan.logistics.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 08/08/16.
 */
public class OrderDetails extends BaseOrderDetails{

    public List<OrderItemDetails> i;        // Ordered Meat Items.


    public OrderDetails(){}

    public OrderDetails(String id){
        this._id = id;
    }

    public OrderDetails(OrderDetails2 orderDetails){
        super(orderDetails);
    }

    public void setOrderedItems(List<OrderItemDetails> orderedItems) {
        this.i = orderedItems;
    }

    public List<OrderItemDetails> getOrderedItems() {
        if (i != null && i.size() > 0 ) {
            return i;
        }
        return new ArrayList<>();
    }

    public List<OrderItemDetails> getOrderedItemsWithUserItemsFirst(String userID) {
        if (i != null && i.size() > 0 ) {
            if ( userID != null && userID.length() > 0 ){
                List<OrderItemDetails> userClaimedItems = new ArrayList<OrderItemDetails>();
                List<OrderItemDetails> otherItems = new ArrayList<OrderItemDetails>();
                for (int i = 0; i < getOrderedItems().size(); i++) {
                    if (getOrderedItems().get(i).getDeliveryBoyID().equals(userID)) {
                        userClaimedItems.add(getOrderedItems().get(i));
                    } else {
                        otherItems.add(getOrderedItems().get(i));
                    }
                }
                List<OrderItemDetails> sortedOrderItemsList = new ArrayList<>();
                for (int i = 0; i < userClaimedItems.size(); i++) {
                    sortedOrderItemsList.add(userClaimedItems.get(i));
                }
                for (int i = 0; i < otherItems.size(); i++) {
                    sortedOrderItemsList.add(otherItems.get(i));
                }
                return sortedOrderItemsList;
            }else {
                return i;
            }
        }
        return new ArrayList<>();
    }

    public void addOrderItem(OrderItemDetails orderItemDetails){
        if ( i == null ){
            i = new ArrayList<>();
        }
        i.add(orderItemDetails);
    }

}
