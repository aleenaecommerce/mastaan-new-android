package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 25-11-2015.
 */

public class UserDetailsMini {
    String _id;
    String f;//firstName;
    String l; //last name
    String e;//email
    String m;// mobileNumber

    public UserDetailsMini(){}

    public UserDetailsMini(UserDetails userDetails){
        this(userDetails.getID(), userDetails.getAvailableName());
    }
    public UserDetailsMini(String id, String name){
        this._id = id;
        this.f = name;
    }

    public String getID() {
        if ( _id == null ){ _id = "";   }
        return _id;
    }

    public String getName() {
        if ( l != null && l.length() > 0 ){
            return f+" "+l;
        }
        return f;
    }

    public String getAvailableName(){
        if ( getName() != null ){
            return getName();
        }else if ( m != null && m.length() > 0 ){
            return getMobileNumber();
        }else if ( e != null && e.length() > 0 ){
            return getEmail();
        }
        return "No Name";
    }

    public UserDetailsMini setMobileNumber(String mobileNumber) {
        this.m = mobileNumber;
        return this;
    }
    public String getMobileNumber() {
        return m;
    }

    public UserDetailsMini setEmail(String email) {
        this.e = email;
        return this;
    }
    public String getEmail() {
        return e;
    }

}
