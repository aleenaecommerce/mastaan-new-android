package com.mastaan.logistics.models;

import androidx.annotation.NonNull;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 28/1/16.
 */
public class OrderItemDetails {
    public String _id;                     //  ID

    public MeatItemDetails i;                        //  Meat Item details
    public String wmi;                      // Warehouse Meat Item
    public List<OrderItemAttributeDetails> a;       //  Attribute details
    public double n;                       //  Quantity

    public String dsy;                     //  Delivery Start by Date
    public String dby;                     //  Deliver by Date
    public String pby;                     // Prepare by Date
    public OrderDetails2 o;              //  Order details

    public int status;                     //  Status
    public String sr;                      //  Status Reason
    public boolean csbyr;                   // Is Support requested by Customer

    public UserDetailsMini fdb;            //  Failed By
    public UserDetailsMini upb;            //  Updated by
    public String upc;                      // Updated by comments

    public double ta;                      //  Total Amount

    public boolean cnc;                    //  Cash N Carry
    public boolean cs;                      //  Customer Support status
    public UserDetailsMini csb;                      // Customer Support Assigner ID
    public String csc;                      //  Customer Support comment
    public UserDetailsMini cse;                      //  Customer Support executive

    public String pd;                      // Processing Claim Date
    public String ppd;                     // Processing Complete Date
    public String asd;                      // Delivery Assigned Date
    public String pdd;                      // Picked up Date
    public String dcd;                     // Delivery Claim Date
    public String sd;                      // Delivered Date

    public UserDetailsMini adb;                      // Acknowledged By
    public UserDetailsMini prdb;                      // Processor Details
    public UserDetailsMini ddb;                       // Delivery Boy Details
    public UserDetailsMini dadb;                    // Delivery Assigned By

    public Vendor m;                        // Vendor Details
    public UserDetailsMini but;             //  Butcher Details
    public String mpt;                      //  Vendor Payment Type
    public double apm;                      //  Vendor Amount
    public String mtc;                     // Vendor Comments
    public double fnw;                      // Final Net Weight

    public String sto;            // Stock
    public String psto;  // Processed Stock

    public int qcs;                     // Quality Check
    public String qcc;                  // Quality Check Comments
    public UserDetailsMini qcb;         // Quality Check By
    public String qcd;                  // Quality Check Date

    public String ddc;                      // Delayed Delivery Comment

    public String si;              // Special Instructions


    public String getID() {
        return _id;
    }

    public String getOrderID(){
        return getOrderDetails().getID();
    }

    public String getReadableOrderID() {
        return getOrderDetails().getReadableOrderID();
    }

    public void setHubDetails(HubDetails hubDetails) {
        getOrderDetails().setHubDetails(hubDetails);
    }
    public HubDetails getHubDetails(){
        return getOrderDetails().getHubDetails();
    }

    public int getPaymentType(){
        return getOrderDetails().getPaymentType();
    }

    public String getPaymentTypeString() {
        return getOrderDetails().getPaymentTypeString();
    }

    public double getTotalOrderAmount() {
        return getOrderDetails().getTotalAmount();
    }

    public void setTotalOrderAmount(double orderAmount) {
        getOrderDetails().setTotalAmount(orderAmount);
    }

    public double getTotalOrderCashAmount() {
        return getOrderDetails().getTotalCashAmount();
    }

    public double getTotalOrderDiscount(){   return getOrderDetails().getTotalDiscount();  }

    public double getOrderCouponDiscount(){   return getOrderDetails().getCouponDiscount();  }

    public double getOrderMembershipDiscount(){   return getOrderDetails().getMembershipDiscount();  }

    public double getOrderMembershipOrderDiscount() {
        return getOrderDetails().getMembershipOrderDiscount();
    }

    public double getOrderMembershipDeliveryChargesDiscount() {
        return getOrderDetails().getMembershipDeliveryChargesDiscount();
    }

    public double getOrderCouponCashback() {
        return getOrderDetails().getCouponCashback();
    }

    public double getTotalOrderOnlinePaymentAmount() {
        return getOrderDetails().getTotalOnlinePaymentAmount();
    }

    public double getTotalOrderWalletAmount() {
        return getOrderDetails().getTotalWalletAmount();
    }

    public double getTotalOrderDeliveryCharges() {
        return getOrderDetails().getDeliveryCharges();
    }

    public double getTotalOrderSurcharges() {
        return getOrderDetails().getSurcharges();
    }

    public double getTotalOrderServiceCharges() {
        return getOrderDetails().getServiceCharges();
    }

    public double getTotalOrderGovernmentTaxes() {
        return getOrderDetails().getGovernmentTaxes();
    }

    public double getTotalOrderRefundAmount() {
        return getOrderDetails().getRefundAmount();
    }


    public void setCashNCarry(boolean isCashNCarryItem) {
        this.cnc = isCashNCarryItem;
    }

    public boolean isCashNCarryItem() {
        return cnc;
    }

    public void setCustomerSupportState(boolean isInCustomerSupportState) {
        this.cs = isInCustomerSupportState;
    }

    public boolean isInCustomerSupportState() {
        return cs;
    }

    public boolean isCounterSaleEmployeeOrder() {
        return o.isCounterSaleOrder();
    }

    public void setCustomerSupporAssignerDetails(UserDetails userDetails) {
        if (userDetails != null) {
            this.csb = new UserDetailsMini(userDetails.getID(), userDetails.getName());
        } else {
            this.csb = null;
        }
    }

    public UserDetailsMini getCustomerSupporAssignerDetails() {
        return csb;
    }

    public String getCustomerSupporAssignerID() {
        if ( csb != null ){ return csb.getID(); }
        return "";
    }

    public void setCustomerSupportComment(String customerSupportComment) {
        this.csc = customerSupportComment;
    }
    public String getCustomerSupportComment() {
        if ( csc != null ){ return csc; }
        return "";
    }

    public void setCustomerSupportExecutiveDetails(UserDetailsMini customerSupportExecutiveDetails) {
        this.cse = customerSupportExecutiveDetails;
    }

    public UserDetailsMini getCustomerSupportExecutiveDetails(){
        return cse;
    }

    public String getCustomerSupportExecutiveID(){
        if ( cse != null ){ return cse.getID(); }
        return "";
    }

    //----

    public MeatItemDetails getMeatItemDetails() {
        if ( i != null ){   return i;   }
        return new MeatItemDetails();
    }

    public String getWarehouseMeatItemID() {
        return wmi;
    }

    public double getQuantity() {
        return n;
    }

    public void setQuantity(Double n) {
        this.n = n;
    }

    public double getAmountOfItem() {
        return ta;
    }

    public void setAmountOfItem(double ta) {
        this.ta = ta;
    }

    public double getVendorPrice(){
        try {
            if (i != null) {
                double basePrice = getMeatItemDetails().getOriginalPrice();
                if (a != null && a.size() > 0) {
                    for (int k = 0; k < a.size(); k++) {
                        if (a.get(k) != null && a.get(k).getOptionDetails() != null) {
                            basePrice += a.get(k).getOptionDetails().getPriceDifference();
                        }
                    }
                }
                return basePrice * n;
            }
        }catch (Exception e){}
        return ta;
    }

    public String getFormattedExtras() {
        try {
            if (a != null && a.size() > 0) {
                String formattedExtras = "";
                for (int i = 0; i < a.size(); i++) {
                    if (formattedExtras.length() > 0) {
                        formattedExtras += ", ";
                    }
                    String extraName = "";
                    try {
                        extraName = CommonMethods.capitalizeFirstLetter(a.get(i).getOptionDetails().getName());
                    } catch (Exception e) {
                    }
                    formattedExtras += extraName;
                }
                return formattedExtras;
            }
        }catch (Exception e){}
        return "";
    }

    public String getFormattedHtmlExtras() {
        try {
            if (a != null && a.size() > 0) {
                String formattedExtras = "";
                for (int i = 0; i < a.size(); i++) {
                    if (formattedExtras.length() > 0) {
                        formattedExtras += ", ";
                    }
                    String extraName = "";
                    try {
                        extraName = a.get(i).getOptionDetails().getName();
                    } catch (Exception e) {
                    }
                    if (extraName.toLowerCase().contains(" cut") || extraName.toLowerCase().contains("skin") || extraName.toLowerCase().contains("bone")) {
                        formattedExtras += ("<u>" + extraName + "</u>");
                    } else {
                        formattedExtras += extraName;
                    }
                }
                return formattedExtras;
            }
        }catch (Exception e){}
        return "";
    }

    public List<OrderItemAttributeDetails> getSelectedAttributes() {
        if ( a == null ){
            a = new ArrayList<>();
        }
        return a;
    }



    public String getQuantityType(){
        return getMeatItemDetails().getQuantityType();
    }

    public String getQuantityUnit() {
        return getMeatItemDetails().getQuantityUnit();
    }

    public String getFormattedQuantity(){
        return CommonMethods.getInDecimalFormat(getQuantity()) + " " + getQuantityUnit()+(getQuantity()!=1?"s":"")+(getFinalNetWeight()>0?" (Net Wt: "+CommonMethods.getInDecimalFormat(getFinalNetWeight())+"kgs)":"");
    }

    //----

    public OrderDetails2 getOrderDetails() {
        if ( o == null ){   o = new OrderDetails2();  }
        return o;
    }
    public OrderItemDetails setOrderDetails(OrderDetails2 orderDetails2) {
        this.o = orderDetails2;
        return this;
    }

    public String getDeliveryStartByDate() {
        return dsy;
    }

    public String getDeliveryDate() {
        if ( dby == null ){ dby="";}
        return dby;
        //return o!=null?o.getDeliveryDate():"";
    }
    public void setDeliveryDate(String deliveryDate) {
        this.dby = deliveryDate;
        /*if ( o != null ) {
            o.setDeliveryDate(deliveryDate);
        }*/
    }
    public String getDeliveryTimeSlot() {
        if ( dsy != null && dsy.trim().length() > 0 ){
            return DateMethods.getReadableDateFromUTC(dsy, DateConstants.HH_MM_A) + " to " + DateMethods.getReadableDateFromUTC(dby, DateConstants.HH_MM_A);
        }else{
            return DateMethods.getReadableDateFromUTC(dby, DateConstants.HH_MM_A);
        }
        //return o!=null?o.getDeliveryTimeSlot():"";
    }

    public String getPrepareByDate() {
        return (pby!=null&&pby.trim().length()>0)?pby:getDeliveryDate();
    }

    //-----

    public String getItemLocation() {
        if (getOrderDetails().getOrderedItems().size() <= 0)
            return null;
        String il = "";
        int size = getOrderDetails().getOrderedItems().size();
        int location = 0;
        for (int j = 0; j < size; j++)
            if (getOrderDetails().getOrderedItems().get(j).equals(_id)) {
                location = j + 1;
            }
        return location + "/" + size;
    }

    public int getOrderItemsCount(){
        if ( o != null && getOrderDetails().i != null ){
            return getOrderDetails().getOrderedItems().size();
        }
        return 1;
    }

    //------

    public int getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusString() {
        if ( status == 0 ){
            return Constants.ORDERED;
        }else if(status == 7) {
            return Constants.ACKNOWLEDGED;
        }else if(status == 1) {
            return Constants.PROCESSING;
        }else if(status == 2){
            return Constants.PROCESSED;
        }else if(status == 8) {
            return Constants.ASSIGNED;
        }else if(status == 9) {
            return Constants.PICKED_UP;
        }else if(status == 3) {
            return Constants.DELIVERING;
        }else if(status == 4) {
            return Constants.DELIVERED;
        }else if(status == 5) {
            return Constants.REJECTED;
        }else if(status == 6) {
            return Constants.FAILED;
        }
        return "";
    }

    public boolean isSuccess(){
        if ( status == 5 || status == 6 ){
            return false;
        }
        return true;
    }
    public boolean isProcessed(){
        if ( status == 2 || status == 8 || status == 9 || status == 3 || status == 4 ){
            return true;
        }
        return false;
    }
    public boolean isDeliveryAssigned(){
        if ( status == 8 || status == 9 || status == 3 || status == 4 ){
            return true;
        }
        return false;
    }
    public boolean isDelivered(){
        if ( status == 4 ){
            return false;
        }
        return true;
    }

    public String getPrevCompletedStatusString(){
        if ( getStatusString().equalsIgnoreCase(Constants.DELIVERED) || getStatusString().equalsIgnoreCase(Constants.DELIVERING) || getStatusString().equalsIgnoreCase(Constants.PROCESSED) ){
            return Constants.PROCESSED;
        }
        return "";
    }

    public void setOrderStatusReason(String statusReasons) {
        this.sr = statusReasons;
    }

    public String getStatusReasons() {
        if ( sr != null){   return sr;  }
        return "";
    }

    public boolean isSupportRequestedByCustomer() {
        return csbyr;
    }

    public UserDetailsMini getFailedBy() {
        return fdb;
    }

    public UserDetailsMini getUpdatedBy() {
        return upb;
    }
    public String getUpdatedByComments() {
        return upc!=null?upc:"";
    }

    //-------

    public void setVendorDetails(Vendor vendorDetails) {
        this.m = vendorDetails;
    }
    public Vendor getVendorDetails() {
//        if ( m == null ){   m = new Vendor();   }
        return m;
    }
    public String getVendorID() {
        if ( m != null ){   return m.getID();   }
        return "";
    }

    public void setButcherDetails(UserDetailsMini butcherDetails) {
        this.but = butcherDetails;
    }
    public UserDetailsMini getButcherDetails() {
//        if ( but == null ){ but = new UserDetailsMini();    }
        return but;
    }
    public String getButcherID() {
        if ( but != null ){   return but.getID();   }
        return "";
    }

    public void setStock(StockDetails stock) {
        this.sto = stock!=null?stock.getID():null;
    }
    public String getStock() {
        return sto;
    }

    public void setProcessedStock(ProcessedStockDetails processedStock) {
        this.psto = processedStock!=null?processedStock.getID():null;
    }
    public String getProcessedStock() {
        return psto;
    }

    public void setVendorPaymentType(String vendorPaymentType) {
        this.mpt = vendorPaymentType;
    }
    public String getVendorPaymentType() {
        if ( mpt == null ){ mpt = "";   }

        if ( mpt.equals("cr")){
            return Constants.CREDIT;
        }else if ( mpt.equals("ca")){
            return Constants.CASH;
        }else {
            return mpt;
        }
    }

    public void setVendorAmount(double vendorAmount) {
        this.apm = vendorAmount;
    }

    public double getVendorPaidAmount() {
        return apm;
    }

    public void setVendorComments(String vendorComments) {
        this.mtc = vendorComments;
    }
    public String getVendorComments() {
        if ( mtc == null ){ mtc = ""; }
        return mtc;
    }

    public void setFinalNetWeight(double finalNetWeight) {
        this.fnw = finalNetWeight;
    }
    public double getFinalNetWeight() {
        return fnw;
    }

    public void setQualityCheckStatus(int qualityCheckStatus) {
        this.qcs = qualityCheckStatus;
    }
    public int getQualityCheckStatus() {
        return qcs;
    }

    public void setQualityCheckComments(String qcc) {
        this.qcc = qcc;
    }
    public String getQualityCheckComments() {
        if ( qcc == null ){ qcc = "";   }
        return qcc;
    }

    public void setQualityChecker(UserDetailsMini qualityCheckBy) {
        this.qcb = qualityCheckBy;
    }
    public UserDetailsMini getQualityChecker() {
        return qcb;
    }
    public String getQualityCheckerID() {
        return qcb!=null?qcb.getID():"";
    }

    public void setQualityCheckDate(String qualityCheckDate) {
        this.qcd = qualityCheckDate;
    }
    public String getQualityCheckDate() {
        return DateMethods.getReadableDateFromUTC(qcd);
    }

    public void setDelayedDeliveryComment(String delayedDeliveryComment) {
        this.ddc = delayedDeliveryComment;
    }
    public String getDelayedDeliveryComment() {
        if ( ddc == null ){ ddc = ""; }
        return ddc;
    }

    public String getProcesserID() {
        if ( prdb != null ) {
            return prdb.getID();
        }
        return "";
    }
    public UserDetails getProcessorDetails() {
        if ( prdb != null ) {
            return new UserDetails(prdb.getID(), prdb.getName());
        }
        return null;
    }
    public void setProcessorDetails(UserDetails processingBy) {
        if ( processingBy != null ) {
            this.prdb = new UserDetailsMini(processingBy.getID(), processingBy.getName());
        }else{
            this.prdb = null;
        }
    }

    public String getDeliveryBoyID() {
        if ( ddb != null ) {
            return ddb.getID();
        }
        return "";
    }

    public UserDetails getDeliveryBoyDetails() {
        if ( ddb != null ) {
            return new UserDetails(ddb.getID(), ddb.getName());
        }
        return null;
    }

    public void setDeliveryBoyDetails(UserDetails deliveryBoyDetails) {
        if ( deliveryBoyDetails != null ){
            this.ddb = new UserDetailsMini(deliveryBoyDetails.getID(), deliveryBoyDetails.getName());
        }else{
            this.ddb = null;
        }
    }

    //---

    public String getProcessingClaimDate() {
        return DateMethods.getReadableDateFromUTC(pd);
    }

    public void setProcessingClaimDate(String processingClaimDate) {
        this.pd = processingClaimDate;
    }

    public String getProcessingCompletedDate() {
        return DateMethods.getReadableDateFromUTC(ppd);
    }
    public void setProcessingCompletedDate(String processingCompletedDate) {
        this.ppd = processingCompletedDate;
    }

    public String getDeliveryBoyAssignedDate() {
        return DateMethods.getReadableDateFromUTC(asd);
    }
    public void setDeliveryBoyAssignedDate(String deliveryBoyAssignedDate) {
        this.asd = deliveryBoyAssignedDate;
    }

    public String getPickupDoneDate() {
        return DateMethods.getReadableDateFromUTC(pdd);
    }
    public void setPickupDoneDate(String pickupDoneDate) {
        this.pdd = pickupDoneDate;
    }

    public String getDeliveryClaimedDate() {
        return DateMethods.getReadableDateFromUTC(dcd);
    }

    public void setDeliveryClaimedDate(String deliveryClaimedDate) {
        this.dcd = deliveryClaimedDate;
    }

    public String getDeliveredDate() {
        return DateMethods.getReadableDateFromUTC(sd);
    }

    public void setDeliveredDate(String deliveredDate) {
        this.sd = deliveredDate;
    }

    public String getStatusTimesString(){
        String statusTimesString = "";
        if ( getProcessingClaimDate().length() > 0 ){
            statusTimesString += "Processing claimed: "+DateMethods.getDateInFormat(getProcessingClaimDate(), DateConstants.HH_MM_A);
        }
        if ( getProcessingCompletedDate().length() > 0 ){
            if ( statusTimesString.length() > 0 ){  statusTimesString += "\n";  }
            statusTimesString += "Processing completed: "+DateMethods.getDateInFormat(getProcessingCompletedDate(), DateConstants.HH_MM_A);
        }
        if ( getQualityCheckDate().length() > 0 ){
            if ( statusTimesString.length() > 0 ){  statusTimesString += "\n";  }
            statusTimesString += "QC "+(getQualityCheckStatus()==Constants.QC_FAILED?"failed":"completed")+": "+DateMethods.getDateInFormat(getQualityCheckDate(), DateConstants.HH_MM_A);
        }
        if ( getDeliveryBoyAssignedDate().length() > 0 ){
            if ( statusTimesString.length() > 0 ){  statusTimesString += "\n";  }
            statusTimesString += "Delivery assigned: "+DateMethods.getDateInFormat(getDeliveryBoyAssignedDate(), DateConstants.HH_MM_A);
        }
        if ( getPickupDoneDate().length() > 0 ){
            if ( statusTimesString.length() > 0 ){  statusTimesString += "\n";  }
            statusTimesString += "Picked up: "+DateMethods.getDateInFormat(getPickupDoneDate(), DateConstants.HH_MM_A);
        }
        if ( getDeliveryClaimedDate().length() > 0 ){
            if ( statusTimesString.length() > 0 ){  statusTimesString += "\n";  }
            statusTimesString += "Delivery claimed: "+DateMethods.getDateInFormat(getDeliveryClaimedDate(), DateConstants.HH_MM_A);
        }
        if ( getDeliveredDate().length() > 0 ){
            if ( statusTimesString.length() > 0 ){  statusTimesString += "\n";  }
            statusTimesString += "Delivered: "+DateMethods.getDateInFormat(getDeliveredDate(), DateConstants.HH_MM_A);
        }
        return statusTimesString;
    }
    
    public String getFormattedStatusString(@NonNull String userID){
        String orderItemStatus = getStatusString();

        String orderStatusString = "";

        if ( getUpdatedBy() != null ){
            if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
            orderStatusString += "UPDATED "+(getUpdatedByComments().length()>0?" ("+getUpdatedByComments()+")":"")+" BY : ";
            if ( userID != null && getUpdatedBy().getID().equals(userID) ){
                orderStatusString += "Me";
            }else{
                orderStatusString += getUpdatedBy().getAvailableName();
            }
        }
        if ( getAcknowledgedBy() != null ){
            if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
            if ( userID != null && getAcknowledgedBy().getID().equals(userID) ){
                orderStatusString += "ACKNOWLEDGED BY : Me";
            }else{
                orderStatusString += "ACKNOWLEDGED BY : " + getAcknowledgedBy().getAvailableName();
            }
        }

        if ( getButcherDetails() != null ){
            if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
            if ( userID != null && getButcherID().equals(userID) ){
                orderStatusString += "BUTCHER : Me";
            }else{
                orderStatusString += "BUTCHER : " + getButcherDetails().getAvailableName();
            }
        }
        if ( getProcessorDetails() != null ){
            if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }

            String string = "PROCESSING";
            if ( orderItemStatus.equalsIgnoreCase(Constants.PROCESSED) || orderItemStatus.equalsIgnoreCase(Constants.ASSIGNED) || orderItemStatus.equalsIgnoreCase(Constants.PICKED_UP) || orderItemStatus.equalsIgnoreCase(Constants.DELIVERING) || orderItemStatus.equalsIgnoreCase(Constants.DELIVERED) || orderItemStatus.equalsIgnoreCase(Constants.REJECTED) ){
                string = "PROCESSED";
            }
            if ( userID != null && getProcesserID().equals(userID) ){
                orderStatusString += string+" : Me";
            }else{
                orderStatusString += string+" : "+ getProcessorDetails().getAvailableName();
            }
        }
        if ( getQualityChecker() != null ){
            if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
            if ( userID != null && getQualityCheckerID().equals(userID) ){
                orderStatusString += "QC "+(getQualityCheckStatus()==Constants.QC_FAILED?"FAILED":"")+": Me";
            }else{
                orderStatusString += "QC "+(getQualityCheckStatus()==Constants.QC_FAILED?"FAILED ":"")+": "+ getQualityChecker().getAvailableName();
            }
        }
        if ( getDeliveryAssigner() != null ){
            if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
            if ( userID != null && getDeliveryAssigner().getID().equals(userID) ){
                orderStatusString += "DELIVERY ASSIGNER : Me";
            }else{
                orderStatusString += "DELIVERY ASSIGNER : " + getDeliveryAssigner().getAvailableName();
            }
        }
        if ( getFailedBy() != null ){
            if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
            if ( userID != null && getFailedBy().getID().equals(userID) ){
                orderStatusString += "FAILED BY : Me";
            }else{
                orderStatusString += "FAILED BY : " + getFailedBy().getAvailableName();
            }
        }
        if ( getDeliveryBoyDetails() != null ){
            if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
            if ( userID != null && getDeliveryBoyID().equals(userID) ){
                orderStatusString += orderItemStatus.replaceAll(Constants.ASSIGNED, "Delivery")+" : Me";
            }else{
                orderStatusString += orderItemStatus.replaceAll(Constants.ASSIGNED, "Delivery")+" : " + getDeliveryBoyDetails().getAvailableName();
            }
        }else{
            if ( orderStatusString.length() > 0 && orderStatusString.toLowerCase().contains(orderItemStatus.toLowerCase()) == false ){
                orderStatusString += "\n"+ orderItemStatus;
            }
        }

        /*if ( orderStatusString == null || orderStatusString.length() == 0 ){
            orderStatusString = orderItemStatus;
            if ( orderItemStatus.equalsIgnoreCase(Constants.FAILED) && getFailedBy() != null ){
                orderStatusString = "FAILED by : "+getFailedBy().getAvailableName();
            }
        }*/
        if ( getDelayedDeliveryComment() != null && getDelayedDeliveryComment().trim().length() > 0 ){
            if ( orderStatusString.length() > 0 ){ orderStatusString += "\n";    }
            orderStatusString += "Delay Reason: "+getDelayedDeliveryComment();
        }
        return orderStatusString;
    }


    public void setAcknowledgedBy(UserDetailsMini acknowledgedBy) {
        this.adb = acknowledgedBy;
    }
    public UserDetailsMini getAcknowledgedBy() {
        return adb;
    }

    public void setDeliveryAssigner(UserDetailsMini assignerDetails) {
        this.dadb = assignerDetails;
    }
    public UserDetailsMini getDeliveryAssigner() {
        return dadb;
    }


    // RUNTIME / TEMP VARIABLES
    public boolean isInDeliveryBucket;

    public OrderItemDetails setInDeliveryBucket(boolean inDeliveryBucket) {
        isInDeliveryBucket = inDeliveryBucket;
        return this;
    }

    public boolean isInDeliveryBucket() {
        return isInDeliveryBucket;
    }



    //=======

    public String getFormattedMeatItemNameUsingAttributeOptions(){
        String formattedExtras = getFormattedExtras().toLowerCase();

        String itemName = getMeatItemDetails().getNameWithCategoryAndSize();
        if ( getMeatItemDetails().getParentCategory().toLowerCase().contains("chicken") ){
            itemName = itemName.replaceAll("Whole", "");
            itemName = itemName.replaceAll("whole", "");
        }

//        if ( formattedExtras.contains("skinless") ){
//            itemName += " (Skinless)";
//        }else if ( formattedExtras.contains("with skin") ){
//            itemName += " (With Skin)";
//        }else if ( formattedExtras.contains("burnt") ){
//            itemName += " (With Skin Burnt)";
//        }

        if ( formattedExtras.contains("small, ")
                || formattedExtras.contains("small (")){
            itemName += " (Small)";
        }else if ( formattedExtras.contains("medium, ")
                || formattedExtras.contains("medium (")){
            itemName += " (Medium)";
        }else if ( formattedExtras.contains("jumbo, ")
                || formattedExtras.contains("jumbo (")){
            itemName += " (Jumbo)";
        }

        if ( formattedExtras.contains("with bone") ){
            itemName += " (With Bone)";
        }else if ( formattedExtras.contains("boneless") ){
            itemName += " (Boneless)";
        }

        if ( formattedExtras.contains("with shell") ){
            itemName += " (With Shell)";
        }else if ( formattedExtras.contains("without shell") ){
            itemName += " (Without Shell)";
        }

        if ( formattedExtras.contains("devein") ){
            itemName += " (Devein)";
        }else if ( formattedExtras.contains("do not devin") ){
            itemName += " (No Devin)";
        }

//        if ( formattedExtras.contains("small cut") ){
//            itemName += " (Small Cut)";
//        }else if ( formattedExtras.contains("medium cut") ){
//            itemName += " (Medium Cut)";
//        }else if ( formattedExtras.contains("large cut") ){
//            itemName += " (Large Cut)";
//        }else if ( formattedExtras.contains("no cut") ){
//            itemName += " (No Cut)";
//        }

        return itemName;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.si = specialInstructions;
    }
    public String getSpecialInstructions() {
        return si;
    }
    public String getCombinedSpecialInstructions() {
        /*if ( si == null ){  si = "";    }
        return si;*/
        return (si!=null&&si.length()>0?si+"\n":"")+(getOrderDetails()!=null?getOrderDetails().getSpecialInstructions():"");
    }

    public DeliveryAreaDetails getDeliveryArea(){
        return getOrderDetails().getDeliveryArea();
    }


    // RUNTIME
    boolean showInCheckBoxView;

    public OrderItemDetails setShowInCheckBoxView(boolean showInCheckBoxView) {
        this.showInCheckBoxView = showInCheckBoxView;
        return this;
    }
    public boolean isShowInCheckBoxView() {
        return showInCheckBoxView;
    }

}
