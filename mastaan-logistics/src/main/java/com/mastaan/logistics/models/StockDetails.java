package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 9/11/16.
 */
public class StockDetails {
    String _id;             //  ID
    String cd;              //  Created Date
    String ud;              //  Updated Date

    String wh;              // Warehouse
    HubDetails hu;              // Hub

    String ty;              // Type : Livestock purchase / Livestock consolidation / Meat item purchase / Meat item consolidation
    String d;               // Date

    CategoryDetails c;      //  Category
    WarehouseMeatItemDetails wmi;    //  MeatItem
    List<AttributeOptionDetails> ilao;  // Item level Attribute Options
    String oi;              // Order item details
    Vendor v;               //  Vendor
    long num;               //  Units
    double qu;              //  Quantity
    double wa;              //  Wastage Quantity
    double co;              //  Cost
    double pco;             //  Packaging cost
    double sco;             //  Shipping cost
    double oco;             //  Other cost
    double tco;             //  Total Cost
    double vr;              //  Vendor rate
    double fr;              //  Farm Rate
    String so;              // Source
    String cmt;             //  Comments

    double ma;              // Margin

    String sto;       // Liked Stock Details
    String psto; // Linked Processed Stock Details

    HubDetails thu;             // To hub (for stock transfers)
    UserDetailsMini cemp;       // Collected employee (for stock transfers)

    List<UserDetailsMini> prb;    //  Processed By
    UserDetailsMini qcb;            // Quality Check By
    UserDetailsMini emp;    //  Created By
    UserDetailsMini ube;    //  Update By


    public StockDetails(){}

    public StockDetails(String id){
        this._id = id;
    }


    public String getID() {
        return _id;
    }

    public String getCreatedDate() {
        return cd;
    }
    public String getUpdatedDate() {
        return ud;
    }

    public String getWarehouseID() {
        return wh;
    }

    public HubDetails getHubDetails() {
        return hu;
    }
    public String getHubID() {
        return hu!=null?hu.getID():null;
    }

    public String getType() {
        if ( ty != null && ty.length() > 0 ){
            if (ty.equalsIgnoreCase("cp")){
                return Constants.CATEGORY_PURCHASE;
            }else if (ty.equalsIgnoreCase("cc")){
                return Constants.CATEGORY_CONSOLIDATION;
            }else if (ty.equalsIgnoreCase("ca")){
                return Constants.CATEGORY_ADJUSTMENT;
            }else if (ty.equalsIgnoreCase("mi")){
                return Constants.MEAT_ITEM_INPUT;
            }else if (ty.equalsIgnoreCase("mp")){
                return Constants.MEAT_ITEM_PURCHASE;
            }else if (ty.equalsIgnoreCase("mc")){
                return Constants.MEAT_ITEM_CONSOLIDATION;
            }else if (ty.equalsIgnoreCase("ma")){
                return Constants.MEAT_ITEM_ADJUSTMENT;
            }else if (ty.equalsIgnoreCase("mo")){
                return Constants.MEAT_ITEM_ORDER;
            }else if (ty.equalsIgnoreCase("mw")){
                return Constants.MEAT_ITEM_WASTAGE;
            }else if (ty.equalsIgnoreCase("hmti")){
                return Constants.HUB_MEAT_ITEM_TRANSFER_IN_FROM_WAREHOUSE;
            }else if (ty.equalsIgnoreCase("hmto")){
                return Constants.HUB_MEAT_ITEM_TRANSFER_OUT_TO_HUB;
            }else if (ty.equalsIgnoreCase("hmo")){
                return Constants.HUB_MEAT_ITEM_ORDER;
            }else if (ty.equalsIgnoreCase("hmw")){
                return Constants.HUB_MEAT_ITEM_WASTAGE;
            }
        }
        return ty!=null?ty:"";
    }

    public void setDate(String date) {
        this.d = date;
    }

    public String getDate() {
        return d;
    }

    public void setCategoryDetails(CategoryDetails categoryDetails) {
        this.c = categoryDetails;
    }

    public CategoryDetails getCategoryDetails() {
        return c;
//        if ( c != null ){   return c;   }
//        return new CategoryDetails();
    }

    public void setWarehouseMeatItemDetails(WarehouseMeatItemDetails warehouseMeatItemDetails) {
        this.wmi = warehouseMeatItemDetails;
    }
    public WarehouseMeatItemDetails getWarehouseMeatItemDetails() {
        return wmi;
//        if ( wmi != null ){ return wmi; }
//        return new WarehouseMeatItemDetails();
    }

    public void setAttributeOptions(List<AttributeOptionDetails> itemLevelAttributeOptions) {
        this.ilao = itemLevelAttributeOptions;
    }
    public List<AttributeOptionDetails> getAttributeOptions() {
        return ilao;
    }
    public String getAttributeOptionsNames(){
        if ( ilao != null && ilao.size() > 0 ){
            String optionsNames = "";
            for (int i=0;i<ilao.size();i++){
                if ( ilao.get(i) != null ) {
                    optionsNames += (optionsNames.length() > 0 ? ", " : "") + ilao.get(i).getName();
                }
            }
            return optionsNames;
        }
        return "";
    }

    public String getFormattedMeatItemNameWithAttributeOptions() {
        if ( wmi != null ){
            return wmi.getMeatItemDetais().getNameWithCategoryAndSize() + (ilao!=null && ilao.size() > 0 ? " ("+getAttributeOptionsNames()+")":"");
        }
        return null;
    }

    public void setOrderItemDetails(String orderItemDetails) {
        this.oi = orderItemDetails;
    }
    public String getOrderItemID() {
        return oi;
    }

    public String getQuantityUnit(){
        String quantityUnit = "Kg";
        if ( getWarehouseMeatItemDetails() != null && getWarehouseMeatItemDetails().getMeatItemDetais() != null ) {
            quantityUnit = getWarehouseMeatItemDetails().getMeatItemDetais().getQuantityUnit();
        }
        return quantityUnit;
    }

    public void setVendorDetails(Vendor vendorDetails) {
        this.v = vendorDetails;
    }

    public Vendor getVendorDetails() {
        return v;
    }

    public void setNoOfUnits(long noOfUnits) {
        this.num = noOfUnits;
    }

    public long getNoOfUnits() {
        return num;
    }

    public StockDetails setQuantity(double quantity) {
        this.qu = quantity;
        return this;
    }

    public double getQuantity() {
        return qu;
    }

    public void setWastageQuantity(double wastageQuantity) {
        this.wa = wastageQuantity;
    }
    public double getWastageQuantity() {
        return wa;
    }

    public void setCost(double cost) {
        this.co = cost;
    }
    public double getCost() {
        return co;
    }

    public void setPackagingCost(double packagingCost) {
        this.pco = packagingCost;
    }
    public double getPackagingCost() {
        return pco;
    }

    public void setShippingCost(double shippingCost) {
        this.sco = shippingCost;
    }
    public double getShippingCost() {
        return sco;
    }

    public void setOtherCost(double otherCost) {
        this.oco = otherCost;
    }
    public double getOtherCosts() {
        return oco;
    }

    public void setTotalCost(double totalCost) {
        this.tco = totalCost;
    }
    public double getTotalCost() {
        return tco;
    }

    public void setVendorRate(double vendorRate) {
        this.vr = vendorRate;
    }
    public double getVendorRate() {
        return vr;
    }

    public void setFarmRate(double farmRate) {
        this.fr = farmRate;
    }
    public double getFarmRate() {
        return fr;
    }

    public void setSource(String source) {
        this.so = source;
    }
    public String getSource() {
        return so;
    }

    public void setComments(String comments) {
        this.cmt = comments;
    }
    public String getComments() {
        return cmt;
    }

    public void setMargin(double margin) {
        this.ma = margin;
    }
    public double getMargin() {
        return ma;
    }

    public void setLinkedStock(String linkedStock) {
        this.sto = linkedStock;
    }
    public String getLinkedStock() {
        return sto;
    }

    public void setLinkedProcessedStock(String linkedProcessedStock) {
        this.psto = linkedProcessedStock;
    }
    public String getLinkedProcessedStock() {
        return psto;
    }

    public void setToHubDetails(HubDetails toHubDetails) {
        this.thu = toHubDetails;
    }
    public HubDetails getToHubDetails() {
        return thu;
    }

    public void setCollectedBy(UserDetailsMini collectedBy) {
        this.cemp = collectedBy;
    }
    public UserDetailsMini getCollectedBy() {
        return cemp;
    }

    public void setProcessedBy(List<UserDetailsMini> processedBy) {
        this.prb = processedBy;
    }
    public List<UserDetailsMini> getProcessedBy() {
        return prb!=null?prb:new ArrayList<UserDetailsMini>();
    }
    public String getProcessedByNames() {
        String names = "";
        if ( prb != null && prb.size() > 0 ){
            for (int i=0;i<prb.size();i++){
                if ( prb.get(i) != null ){
                    if ( names.length() > 0 ){  names += ", ";   }
                    names += prb.get(i).getAvailableName();
                }
            }
        }
        return names;
    }

    public void setQualityCheckBy(UserDetailsMini qualityCheckBy) {
        this.qcb = qualityCheckBy;
    }
    public UserDetailsMini getQualityCheckBy() {
        return qcb;
    }

    public UserDetailsMini getCreatedBy() {
        if ( emp != null ){ return emp; }
        return new UserDetailsMini();
    }
    public UserDetailsMini getUpdatedBy() {
        return ube;
    }


    public String getName(){
        return getFormattedName().replaceAll("<b>", "").replaceAll("</b>", "").replaceAll("<u>", "").replaceAll("</u>", "").replaceAll("<br>", "\n");
    }
    public String getFormattedName(){
        return "<b><u>" + DateMethods.getReadableDateFromUTC(getDate(), DateConstants.MMM_DD_YYYY_HH_MM_A) + "</u></b>"
                + "<br>"+getID()
                + "<br>Quantity: <b>" + CommonMethods.getIndianFormatNumber(getQuantity()) + " " + getQuantityUnit() + (getQuantity() == 1 ? "" : "s") + "</b>" + (getNoOfUnits() > 0 ? "  (<b>" + getNoOfUnits() + "unit" + (getNoOfUnits() == 1 ? "" : "s") + "</b>)" : "")
                + (getProcessedBy() != null && getProcessedBy().size() > 0? ", Processed by: <b>" + getProcessedByNames() + "</b>" : "")
                + (getQualityCheckBy() != null? ", QC by: <b>" + getQualityCheckBy().getAvailableName() + "</b>" : "")
                + (getVendorDetails() != null ? ", Vendor: <b>" + getVendorDetails().getName() + "</b>" : "");
    }


    // Runtime
    int isUpdated = -1;
    public boolean isUpdated(){
        try{
            if ( isUpdated < 0 ){
                isUpdated = DateMethods.getDifferenceBetweenDatesInMilliSeconds(cd, ud) > 5 * 1000 ? 1: 0;
            }
        }catch (Exception e){}
        return isUpdated==1?true:false;
    }

}
