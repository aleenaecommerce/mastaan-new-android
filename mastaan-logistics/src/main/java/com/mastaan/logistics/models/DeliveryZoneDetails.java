package com.mastaan.logistics.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 25/3/17.
 */
public class DeliveryZoneDetails {
    String group;
    List<DeliveryAreaDetails> areas;

    public String getID(){
        return group;
    }
    public String getName() {
        return group;
    }

    public List<DeliveryAreaDetails> getAreas() {
        if ( areas == null ){   areas = new ArrayList<>();  }
        return areas;
    }
}
