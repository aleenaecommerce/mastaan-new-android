package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 01/06/18.
 */

public class AnalyticsInstallSource {
    String _id;
    long count;

    public String getID() {
        return _id;
    }

    public long getCount() {
        return count;
    }

}
