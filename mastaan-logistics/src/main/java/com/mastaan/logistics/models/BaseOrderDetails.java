package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 08/08/16.
 */
public class BaseOrderDetails {

    public String _id;      // OrderAccessID
    public String cd;       // Created Date
    public String so;       // Order Source a-Android
    public String oid;      //  Order ID
    public String did;      //  FormattedID
    public DeliveryAreaDetails ar;       //  Delivery Area
    public int invv;        // Invoice Version Number

    public String wh;       // Warehouse
    public HubDetails hu;   // Hub Details

    public int pt;          // Payment Type
    //public String tid;      //  Transaction ID
    public List<String> pid;

    public double ita;      // Initial Total Amount
    public double ta;       // Total Amount
    public double caa;      // Total Cash Amount
    public double wa;       // Total Wallet Amount
    public double opa;      // Total OnlinePayment Amount
    public double delfee;   // Delivery Charges
    public double svchg;    // Service Charges
    public double suchg;    // Surcharges
    public double govtax;   // Govt Taxes
    public double di;       // Discount
    public double cba;      // Cashback
    public double mdi;       // Membership Total Discount
    public double mdcdi;    // Membership Delivery Charges Discount
    public double modi;     // Membership Order Discount
    public double ref;      // Refund Amount

    public List<AppliedDiscountDetails> dia;   // Discounts List
    //UserDetailsMini dib;    //  Discount Created By
    public String dir;             //  Discount Reason
    public CouponDetails cc;       //  Coupon code

    public String dsy;      // Delivery Start By Date
    public String dby;      // Delivery By Date
    public double mpt;         // Temp Value

    public String os;       // Order Status
    boolean csale;             //  Counter sale flag

    public BuyerDetails b;         // Buyer Details

    public BuyerDetails ob;        //  Order Created By
    public UserDetails cb;         //  Order Created By

    public String m;               // Customer Mobile
    public String am;              // Customer ALternate Mobile
    public String e;        // Customer Email
    public String n;        // Customer Name

    public String ad;       // AddressBookItemID
    public String da;       // Full Address
    public String pos;      // LatLng
    public String lm;       // Landmark
    public String p;        //  Premise
    public String hs;       // Housing Society
    public String s1;       //  SubLocality-1
    public String s2;       //  SubLocality-2
    public String r;        //  Road No
    public String l;        //  Locality
    public String s;        //  State
    public String c;        //  Country
    public String pin;     // PinCode

    public String si;              // Special Instructions

    public boolean de;      //  Deliver Early
    public boolean bnr;     // Bill Not Required

    public boolean pri;     //  Is Bill printed
    public boolean fo;      // Is First Order

    public String fol;      // Followup resulted in order


    public BaseOrderDetails(){}

    public BaseOrderDetails(BaseOrderDetails orderDetails){
        this._id = orderDetails._id;
        this.cd = orderDetails.cd;
        this.so = orderDetails.so;
        this.oid = orderDetails.oid;
        this.did = orderDetails.did;
        this.ar = orderDetails.ar;
        this.invv = orderDetails.invv;

        this.wh = orderDetails.wh;
        this.hu = orderDetails.hu;

        this.pt = orderDetails.pt;
        //this.tid = orderDetails.tid;
        this.pid = orderDetails.pid;
        this.ita = orderDetails.ita;
        this.ta = orderDetails.ta;
        this.caa = orderDetails.caa;
        this.wa = orderDetails.wa;
        this.opa = orderDetails.opa;
        this.delfee = orderDetails.delfee;
        this.svchg = orderDetails.svchg;
        this.suchg = orderDetails.suchg;
        this.govtax = orderDetails.govtax;
        this.di = orderDetails.di;
        this.mdi = orderDetails.mdi;
        this.mdcdi = orderDetails.mdcdi;
        this.modi = orderDetails.modi;
        this.cba = orderDetails.cba;
        this.ref = orderDetails.ref;

        this.dia = orderDetails.dia;
        //this.dib = orderDetails.dib;
        this.dir = orderDetails.dir;
        this.cc = orderDetails.cc;

        this.dsy = orderDetails.dsy;
        this.dby = orderDetails.dby;
        this.mpt = orderDetails.mpt;

        this.os = orderDetails.os;
        this.csale = orderDetails.csale;

        this.b = orderDetails.b;

        this.ob = orderDetails.ob;
        this.cb = orderDetails.cb;

        this.m = orderDetails.m;
        this.am = orderDetails.am;
        this.e = orderDetails.e;
        this.n = orderDetails.n;

        this.ad = orderDetails.ad;
        this.da = orderDetails.da;
        this.pos = orderDetails.pos;
        this.lm = orderDetails.lm;
        this.p = orderDetails.p;
        this.hs = orderDetails.hs;
        this.s1 = orderDetails.s1;
        this.s2 = orderDetails.s2;
        this.r = orderDetails.r;
        this.l = orderDetails.l;
        this.s = orderDetails.s;
        this.c = orderDetails.c;
        this.pin = orderDetails.pin;

        this.si = orderDetails.si;

        this.de = orderDetails.de;
        this.bnr = orderDetails.bnr;

        this.pri = orderDetails.pri;

        this.fo = orderDetails.fo;

        this.fol = orderDetails.fol;
    }

    //----

    public String getID() {
        return _id;
    }

    public String getCreatedDate() {
        return cd;
    }

    public String getSource() {
        return so!=null?so:"";
    }

    public String getReadableOrderID() {
        if ( oid == null ){  oid=""; }
        return oid;
    }

    public void setFormattedOrderID(String formattedID){
        this.did = formattedID;
    }
    public String getFormattedOrderID(){
        /*String formattedOrderID = "";
        if ( s1 != null && s1.length() > 0 ){
            formattedOrderID += CommonMethods.getStringFromTo(s1, 0, 2);
            formattedOrderID += "-"+String.format("%02d", DateMethods.getHourFromDateIn24HrFormat(DateMethods.getReadableDateFromUTC(getDeliveryDate())));
            formattedOrderID += "-"+CommonMethods.getStringFromTo(getReadableOrderID(), getReadableOrderID().length()-7, getReadableOrderID().length()-2);
            formattedOrderID += "\n"+s1;
        }else if ( s2 != null && s2.length() > 0 ){
            formattedOrderID += CommonMethods.getStringFromTo(s2, 0, 2);
            formattedOrderID += "-"+String.format("%02d", DateMethods.getHourFromDateIn24HrFormat(DateMethods.getReadableDateFromUTC(getDeliveryDate())));
            formattedOrderID += "-"+CommonMethods.getStringFromTo(oid, getReadableOrderID().length()-7, getReadableOrderID().length()-2);
            formattedOrderID += "\n"+s2;
        }else{
            formattedOrderID = getReadableOrderID();
        }
        return formattedOrderID;*/
        if ( did != null && did.length() > 0 ){ return did;   }
        return getReadableOrderID();
    }

    public void setDeliveryArea(DeliveryAreaDetails ar) {
        this.ar = ar;
    }
    public DeliveryAreaDetails getDeliveryArea() {
        return ar;
    }

    public void setInvoiceVersionNumber(int invoiceVersionNumber) {
        this.invv = invoiceVersionNumber;
    }
    public int getInvoiceVersionNumber(){
        return invv;
    }

    public void setHubDetails(HubDetails hubDetails) {
        this.hu = hubDetails;
    }
    public HubDetails getHubDetails() {
        return hu;
    }


    //-----

    public int getPaymentType() {
        return pt;
    }

    public String getPaymentTypeString() {
        if (pt == 1) {
            return Constants.CASH_ON_DELIVERY;
        } else if (pt == 2) {
            return Constants.CREDIT_CARD;
        } else if (pt == 3) {
            return Constants.DEBIT_CARD;
        } else if (pt == 4) {
            return Constants.NET_BANKING;
        } else if (pt == 6||pt == 7) {
            return Constants.ONLINE_PAYMENT;
        }else if (pt == 8) {
            return Constants.PAYTM_WALLET;
        }else if (pt == 9) {
            return Constants.PAYTM_QR;
        }else if (pt == 10) {
            return Constants.BANK_TRANSFER;
        }else if (pt == 11) {
            return Constants.UPI;
        }else if (pt == 12) {
            return Constants.WRITEOFF;
        }else if (pt == 13) {
            return Constants.WALLET;
        }
        return "";
    }

    public void setTotalAmount(double ta) {
        this.ta = ta;
    }
    public double getTotalAmount() {
        return ta;
    }

    public double getTotalAmountIncludingDiscount() {
        return ta+di+mdi;
    }

    public void setRefundAmount(double refundAmount) {
        this.ref = refundAmount;
    }
    public void addToRefundAmount(double refundAmount) {
        this.ref += refundAmount;
    }
    public double getRefundAmount() {
        return ref;
    }

    public List<AppliedDiscountDetails> getAppliedDiscounts() {
        return dia;
    }

    public void setTotalCashAmount(double cashAmount) {
        this.caa = cashAmount;
    }
    public void addToTotalCashAmount(double cashAmount) {
        this.caa += cashAmount;
    }
    public double getTotalCashAmount() {
        return caa;
    }

    public double getTotalWalletAmount() {
        return wa;
    }

    public void setTotalOnlinePaymentAmount(double onlinePaymentAmount) {
        this.opa = onlinePaymentAmount;
    }
    public double getTotalOnlinePaymentAmount() {
        return opa;
    }

    public double getSubTotal() {
        return (ta+di+mdi-delfee-svchg-suchg-govtax);
    }

    public double getDeliveryCharges() {
        return delfee;
    }

    public double getSurcharges() {
        return suchg;
    }

    public double getServiceCharges() {
        return svchg;
    }

    public double getGovernmentTaxes() {
        return govtax;
    }

    public double getTotalDiscount(){  return di+mdi;  }

    public double getCouponDiscount(){   return di;  }

    public double getCouponCashback(){   return cba;  }

    public double getMembershipDiscount() {
        return mdi;
    }

    public double getMembershipOrderDiscount() {
        return modi;
    }

    public double getMembershipDeliveryChargesDiscount() {
        return mdcdi;
    }

    public String getCouponCode() {
        return cc!=null?cc.getCouponCode():null;
    }
    public String getReferralCode() {
        return cc!=null?cc.getReferralCode():null;
    }
    public String getReferralBuyer() {
        return cc!=null?cc.getReferralBuyer():null;
    }

    public List<String> getPaymentIDs() {
        if ( pid == null ){ pid = new ArrayList<>();    }
        return pid;
    }

    public String getFullTotalAmountSplitString(){
        String paymentTypeSplit = getTotalAmountPaymentTypeSplitString();
        String amountSplit = getTotalAmountSplitString();
        if ( amountSplit != null && amountSplit.length() > 0 ){
            return paymentTypeSplit+"\n{"+amountSplit+"}";
        }else{
            return paymentTypeSplit;
        }
    }
    public String getTotalAmountPaymentTypeSplitString(){
        String totalPaymentSplit = "";
        if ( getTotalCashAmount() > 0 && (getTotalCashAmount() == getTotalAmount() || getTotalOnlinePaymentAmount() == getTotalAmount() || getTotalWalletAmount() == getTotalAmount()) ){
            totalPaymentSplit = SpecialCharacters.RS + " "+CommonMethods.getIndianFormatNumber(getTotalAmount()) + " (" + getPaymentTypeString() + ")";
        }else{
            String paymentSplit = "";
            if (getTotalCashAmount() != 0) {
                paymentSplit += SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getTotalCashAmount())+" (COD"+(getTotalCashAmount()<0?", Return)":")");
            }
            if (getTotalOnlinePaymentAmount() > 0) {
                if ( paymentSplit.length() > 0 ){   paymentSplit += " + ";  }
                paymentSplit += SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getTotalOnlinePaymentAmount())+" (OP)";
            }
            if (getTotalWalletAmount() > 0) {
                if ( paymentSplit.length() > 0 ){   paymentSplit += " + ";  }
                paymentSplit += SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getTotalWalletAmount())+" (W)";
            }
            totalPaymentSplit = SpecialCharacters.RS + " " +CommonMethods.getIndianFormatNumber(getTotalAmount()) + " ["+paymentSplit+"]";
        }

        if ( getRefundAmount() > 0 ){
            totalPaymentSplit += "\n[ REFUNDED: "+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getRefundAmount())+" ]";
        }

        return totalPaymentSplit;
    }
    public String getTotalAmountSplitString(){
        String totalAmountSplit = CommonMethods.getIndianFormatNumber(getSubTotal())+" (OA)";
        if ( getDeliveryCharges() > 0 ){
            totalAmountSplit += " + "+CommonMethods.getIndianFormatNumber(getDeliveryCharges())+" (DC)";
        }
        if ( getServiceCharges() > 0 ){
            totalAmountSplit += " + "+CommonMethods.getIndianFormatNumber(getServiceCharges())+" (SvC)";
        }
        if ( getSurcharges() > 0 ){
            totalAmountSplit += " + "+CommonMethods.getIndianFormatNumber(getSurcharges())+" (SuC)";
        }
        if ( getGovernmentTaxes() > 0 ){
            totalAmountSplit += " + "+CommonMethods.getIndianFormatNumber(getGovernmentTaxes())+" (GT)";
        }
        if ( getCouponDiscount() > 0 ){
            totalAmountSplit += " - "+CommonMethods.getIndianFormatNumber(getCouponDiscount())+" "+(getCouponCode()!=null?"["+getCouponCode().toUpperCase()+"]":"")+"(CDis)";
        }
        if ( getMembershipDiscount() > 0 ){
            if ( getMembershipOrderDiscount() > 0 ){
                totalAmountSplit += " - "+CommonMethods.getIndianFormatNumber(getMembershipOrderDiscount())+" (MODis)";
            }
            if ( getMembershipDeliveryChargesDiscount() > 0 ){
                totalAmountSplit += " - "+CommonMethods.getIndianFormatNumber(getMembershipDeliveryChargesDiscount())+" (MDcDis)";
            }
        }

        if ( getCouponCashback() > 0 ){
            totalAmountSplit += " [CB: "+CommonMethods.getIndianFormatNumber(getCouponCashback())+"]";
        }
        return totalAmountSplit;
    }

    //----

    public String getStatus() {
        if ( os != null && os.length() > 1 ){   return os;  }
        return "  ";
    }

    public void setDeliveryStartDate(String deliveryStartDate) {
        this.dsy = deliveryStartDate;
    }
    public String getDeliveryStartDate() {
        return dsy;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.dby = deliveryDate;
    }
    public String getDeliveryDate() {
        if ( dby == null ){ dby="";}
        return dby;
    }

    public String getDeliveryTimeSlot() {
        if ( dsy != null && dsy.trim().length() > 0 ){
            return DateMethods.getReadableDateFromUTC(dsy, DateConstants.HH_MM_A) + " to " + DateMethods.getReadableDateFromUTC(dby, DateConstants.HH_MM_A);
        }else{
            return DateMethods.getReadableDateFromUTC(dby, DateConstants.HH_MM_A);
        }
        //return DateMethods.getReadableDateFromUTC(dby, DateConstants.HH_MM_A);
    }

    //----

    public BuyerDetails getBuyerDetails() {
        if ( b != null ){   return b;  }
        return new BuyerDetails();
    }
    public String getBuyerID() {
        if ( b != null ){   return b.getID();  }
        return "";
    }

    public double getOutstandingAmount(){
        if ( b != null ){   return b.getOutstandingAmount();  }
        return 0;
    }

    public String getOrderCreatorName(){
        if ( ob != null ){
            return ob.getAvailableName();
        }else if ( cb != null ){
            return cb.getAvailableName();
        }
        return null;
    }

    public UserDetailsMini getDiscountCreatorDetails() {
        return null;
    }

    public String getDiscountReason() {
        return dir;
    }

    public String getCustomerMobile() {
        if ( m != null ){   return m;   }
        return "";
    }

    public void setCustomerAlternateMobile(String alternateMobile) {
        this.am = alternateMobile;
    }
    public String getCustomerAlternateMobile() {
        if ( am != null ){  return am;  }
        return "";
    }
    public boolean isAlertnateMobileExists(){
        return am!=null&&am.trim().length()>0?true:false;
    }

    public String getCustomerEmail() {
        return e;
    }

    public String getCustomerName() {
        if ( n ==  null ){  n = ""; }
        return n;
    }

    //-----

    public LatLng getDeliveryAddressLatLng() {
        return LatLngMethods.getLatLngFromString(pos);
    }

    public String getDeliveryAddressLatLngString() {
        return pos;
    }

    public String getDeliveryAddress() {
        return CommonMethods.getStringFromStringArray(new String[]{p, s2, s1, l, s, pin, (hs!=null&&hs.trim().length()>0)?"\nH.Society: "+hs:null});
    }

    public String getDeliveryAddressPremise() {
        return p;
    }

    public void setHousingSociety(String housingSocietyName) {
        this.hs = housingSocietyName;
    }
    public String getHousingSociety() {
        return hs;
    }

    public String getDeliveryAddressLandmark() {
        return lm;
    }

    public String getDeliveryAddressSubLocalityLevel1() {
        return s1;
    }

    public String getDeliveryAddressSubLocalityLevel2() {
        return s2;
    }

    public String getDeliveryAddressLocality() {
        return l;
    }

    public String getDeliveryAddressState() {
        return s;
    }

    public String getDeliveryAddressCountry() {
        return c;
    }

    public String getDeliveryAddressPinCode() {
        return pin;
    }

    //------

    public void setSpecialInstructions(String specialInstructions) {
        this.si = specialInstructions;
    }

    public String getSpecialInstructions() {
        if ( si == null ){  si = "";    }
        return si;
    }

    public boolean deliverEarlyIfPossible() {
        return de;
    }

    public boolean isBillNotRequired(){
        return bnr;
    }

    public void setOrderStaus(String orderStaus) {
        this.os = orderStaus;
    }

    public boolean isCounterSaleOrder() {
        return csale;
    }

    public double getMaxCartPreparationTime() {
        return mpt;
    }

    public void setBillPrinted(boolean isBillPrinted) {
        this.pri = isBillPrinted;
    }
    public boolean isBillPrinted() {
        return pri;
    }

    public void setFirstOrder(boolean isFirstOrder) {
        this.fo = isFirstOrder;
    }
    public boolean isFirstOrder() {
        return fo;
    }

    public boolean isReferralOrder(){
        return (cc!=null && cc.getReferralCode() != null && cc.getCouponCode().length() > 0)?true:false;
    }

    public String getFollowupResultedInOrder(){
        return fol;
    }
    public boolean isFollowupResultedOrder(){
        return (fol!=null&&fol.length()>0)?true:false;
    }

}
