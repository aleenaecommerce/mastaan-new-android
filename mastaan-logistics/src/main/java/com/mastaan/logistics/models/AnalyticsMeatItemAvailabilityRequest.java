package com.mastaan.logistics.models;

import com.aleena.common.methods.CommonMethods;

/**
 * Created by Venkatesh Uppu on 05/06/18.
 */

public class AnalyticsMeatItemAvailabilityRequest {
    String _id;
    long count;
    WHMeatItemDetails item;

    class WHMeatItemDetails {
        String _id;
        MeatItemDetails i;
        class MeatItemDetails{
            String _id;
            String i;

            String getMainName() {
                if ( i==null ){ i="";   }
                return i;
            }
        }
    }

    public String getWarehouseMeatItemID() {
        return _id;
    }

    public String getMeatItemName() {
        return item.i.getMainName();
    }

    public long getCount() {
        return count;
    }

    public String getFormattedDetails(){
        String details = "";
        if ( count > 0 ){
            details += "Count: <b>"+ CommonMethods.getIndianFormatNumber(count)+"</b>";
        }
        return details;
    }
}
