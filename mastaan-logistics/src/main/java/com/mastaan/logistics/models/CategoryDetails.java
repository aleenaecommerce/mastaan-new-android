package com.mastaan.logistics.models;

import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleena on 29/1/16.
 */
public class CategoryDetails {

    String _id;         //  ID

    String c;           //  Value
    String n;           //  Name
    String col1;        //  StatusBarColor
    String col2;        //  BackgroundColor
    String col3;        //  GradientColor
    String bg;          //  BackgroundPattern
    String ic;          //  Icon

    boolean vis;        //  Visibility
    boolean en;         //  Available
    String dt;          //
    String dm;          //

    List<CategoryDetails> p;
    List<CategoryDetails> s;


    public String getID() {
        return _id;
    }

    public String getValue() {
        if ( c != null ){   return c;   }
        return "";
    }

    public String getBelongsCategoryName(){
        if ( getValue().equalsIgnoreCase(Constants.CHICKEN)  || getValue().equalsIgnoreCase(Constants.FARM_CHICKEN) || getValue().equalsIgnoreCase(Constants.COUNTRY_CHICKEN) ){
            return Constants.CHICKEN;
        }
        else if ( getValue().equalsIgnoreCase(Constants.MUTTON) || getValue().equalsIgnoreCase(Constants.SHEEP_MUTTON) || getValue().equalsIgnoreCase(Constants.GOAT_MUTTON) ){
            return Constants.MUTTON;
        }
        else if ( getValue().equalsIgnoreCase(Constants.SEAFOOD) || getValue().equalsIgnoreCase(Constants.FRESH_WATER_SF) || getValue().equalsIgnoreCase(Constants.SEA_WATER_SF)) {
            return Constants.SEAFOOD;
        }
        else if ( getValue().equalsIgnoreCase(Constants.OTHERS) || getValue().equalsIgnoreCase(Constants.PICKLES) || getValue().equalsIgnoreCase(Constants.MARINADES) || getValue().equalsIgnoreCase(Constants.EGGS) || getValue().equalsIgnoreCase(Constants.READY_TO_EAT) ) {
            return Constants.OTHERS;
        }
        return n;
    }

    public String getName() {
        if ( n!= null ){    return n;   }
        return "";
    }

    public String getNameWithParent() {
        if ( getName().toLowerCase().contains(getParentCategoryName().toLowerCase()) ==false ){
            return getName()+" "+getParentCategoryName();
        }else {
            return getName();
        }
    }

    public String getStatusBarColor() {
        return col1;
    }

    public String getBackgroundColor() {
        return col2;
    }

    public String getGradientColor() {
        return col3!=null?col3:getStatusBarColor();
    }


    public String getPatternURL() {
        return bg;
    }

    public String getIconURL() {
        return ic;
    }

    public int getSubCategoriesCount() {
        if ( s != null ) {
            return s.size();
        }else{
            return 0;
        }
    }

    public List<CategoryDetails> getSubCategories() {
        if ( s == null ){   return  new ArrayList<>();  }
        return s;
    }

    public void setVisibility(boolean visibility) {
        this.vis = visibility;
    }
    public boolean isVisible() {
        return vis;
    }

    public void setEnabled(boolean isEnabled) {
        this.en = isEnabled;
    }
    public boolean isEnabled(){
        return en;
    }

    public void setDisabledTitle(String displayTitle) {
        this.dt = displayTitle;
    }

    public String getDisabledTitle() {
        return dt;
    }

    public void setDisabledMessage(String displayMessage) {
        this.dm = displayMessage;
    }

    public String getDisabledMessage() {
        return dm;
    }

    public String getParentCategoryName() {
        return (p == null || p.size() == 0) ? (n==null?"":n) : p.get(0).getParentCategoryName();
    }

    public String getParentCategoryValue() {
        return (p == null || p.size() == 0) ? getCategoryValue() : p.get(0).c;
    }

    public String getCategoryValue() {
        if ( c != null ){   return c;   }
        return "";
    }

    public String getCategoryIdentifier() {
        if ( p != null && p.size() > 0 && p.get(0).n != null){
            return p.get(0).n.replaceAll("\\s+", "");
        }
        return c!=null?c:"";//(p == null || p.size() == 0) ? c : p.get(0).n;
    }

}
