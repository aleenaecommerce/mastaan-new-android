package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 24/8/16.
 */
public class AttributeOptionAvailabilityDetails {
    String _id;
    String dstr;
    boolean a;
    double pd;
    String ar;

    public AttributeOptionAvailabilityDetails(){}

    public AttributeOptionAvailabilityDetails(String id, String date, boolean availability, double priceDifference, String availabilityReason){
        this._id = id;
        this.dstr = date;
        this.a = availability;
        this.pd = priceDifference;
        this.ar = availabilityReason;
    }

    public String getID() {
        return _id;
    }

    public String getDate() {
        return dstr;
    }

    public void setAvailability(boolean availability) {
        this.a = availability;
    }

    public boolean isAvailable() {
        return a;
    }

    public double getPriceDifference() {
        return pd;
    }

    public void setAvailabilityReason(String availabilityReason) {
        this.ar = availabilityReason;
    }

    public String getAvailabilityReason() {
        if ( ar != null ){  return ar;  }
        return "";
    }
}
