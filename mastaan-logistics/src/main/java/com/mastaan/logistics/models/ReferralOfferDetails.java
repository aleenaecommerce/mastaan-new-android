package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 14/7/18.
 */

public class ReferralOfferDetails {

    String _id;

    boolean st;     // Status

    String std;     // StartDate
    String exd;     // ExpiredDate

    double rmo;      // Receiver Minimum order value
    double rdi;      // Receiver Discount value
    String rdt;      // Receiver Discount value type v - value (Rs. 20), p - percentage (20%)
    double rmdi;     // Receiver Maximum discount

    double smo;      // Sender Minimum order value
    double sdi;      // Sender Discount value
    String sdt;      // Sender Discount value type v - value (Rs. 20), p - percentage (20%)
    double smdi;     // Sender Maximum discount


    public ReferralOfferDetails(boolean status, String startDate, String endDate, double receiverMinimumOrder, String receiverDiscountValueType, double receiverDiscountValue, double receiverMaximumDiscount, double senderMinimumOrder, String senderDiscountValueType, double senderDiscountValue, double senderMaximumDiscount){
        this.st = status;
        this.std = startDate;
        this.exd = endDate;

        this.rmo = receiverMinimumOrder;
        this.rdt = receiverDiscountValueType;
        this.rdi = receiverDiscountValue;
        this.rmdi = receiverMaximumDiscount;

        this.smo = senderMinimumOrder;
        this.sdt = senderDiscountValueType;
        this.sdi = senderDiscountValue;
        this.smdi = senderMaximumDiscount;
    }

    public String getID() {
        return _id;
    }

    public boolean getStatus() {
        return st;
    }

    public String getStartDate() {
        return std;
    }

    public String getExpiryDate() {
        return exd;
    }

    public double getReceiverMinimumOrder() {
        return rmo;
    }

    public double getReceiverDiscountValue() {
        return rdi;
    }

    public String getReceiverDiscountValueType() {
        return rdt!=null?rdt:"";
    }

    public double getReceiverMaximumDiscount() {
        return rmdi;
    }


    public double getSenderMinimumOrder() {
        return smo;
    }

    public double getSenderDiscountValue() {
        return sdi;
    }

    public String getSenderDiscountValueType() {
        return sdt!=null?sdt:"";
    }

    public double getSenderMaximumDiscount() {
        return smdi;
    }

}
