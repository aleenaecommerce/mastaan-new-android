package com.mastaan.logistics.models;

import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 25/3/17.
 */
public class DeliveryAreaDetails {
    String _id;
    String name;
    String code;
    String pos;
    String grp;

    long count;
    List<String> areas;

    public DeliveryAreaDetails(String id, String name, String code){
        this._id = id;
        this.name = name;
        this.code = code;
    }

    public String getID() {
        if ( _id == null ){    _id = "";  }
        return _id;
    }

    public String getName() {
        if ( name == null ){    name = "";  }
        return name;
    }

    public String getCode() {
        if ( code == null ){    code = "";  }
        return code;
    }

    public LatLng getLatLng() {
        return LatLngMethods.getLatLngFromString(pos);
    }
    public String getLatLngString() {
        if ( pos != null && pos.length() > 0 ){
            return pos;
        }
        return "0,0";
    }

    public String getDeliveryZoneID() {
        if ( grp == null ){ grp = "";   }
        return grp;
    }

    //----

    public long getCount() {
        return count;
    }

    public List<LatLng> getLatLngs(){
        List<LatLng> latLngs = new ArrayList<>();
        if ( areas != null && areas.size() > 0 ) {
            for (int i = 0; i < areas.size(); i++) {
                latLngs.add(LatLngMethods.getLatLngFromString(areas.get(i)));
            }
        }
        return latLngs;
    }
    public LatLng getCenterLatLng(){
        List<LatLng> latLngs = getLatLngs();
        if ( latLngs != null && latLngs.size() > 0 ){
            double sumLats = 0;
            double sumLngs = 0;
            for (int i=0;i<latLngs.size();i++){
                sumLats += latLngs.get(i).latitude;
                sumLngs += latLngs.get(i).longitude;
            }
            return new LatLng(sumLats/latLngs.size(), sumLngs/latLngs.size());
        }
        return new LatLng(0,0);
    }

}
