package com.mastaan.logistics.models;

import com.aleena.common.methods.CommonMethods;

/**
 * Created by Venkatesh Uppu on 05/06/18.
 */

public class AnalyticsCouponCode {
    CouponDetails coupon;
    long count;
    double total;
    double discount;


    public CouponDetails getCouponDetails() {
        return coupon!=null?coupon:new CouponDetails();
    }
    public String getCouponCode(){
        return coupon!=null?coupon.getCouponCode():"";
    }

    public long getCount() {
        return count;
    }

    public double getTotal() {
        return total;
    }

    public double getDiscount() {
        return discount;
    }

    public String getFormattedTitle(){
        String title = "";
        if ( coupon != null ){
            title += coupon.getCouponCode().toUpperCase();
            if ( coupon.getOfferName() != null && coupon.getOfferName().trim().length() > 0 ){
                title += " ("+coupon.getOfferName()+")";
            }
        }
        return title;
    }

    public String getFormattedDetails(){
        String details = "";
        if ( count > 0 ){
            details += "Orders Count: <b>"+ CommonMethods.getIndianFormatNumber(count)+"</b>";
        }
        if ( total > 0 ){
            details += (details.length()>0?"<br>":"")+"Orders Total: <b>"+ CommonMethods.getIndianFormatNumber(total)+"</b>";
        }
        if ( discount > 0 ){
            details += (details.length()>0?"<br>":"")+"Total Discount: <b>"+ CommonMethods.getIndianFormatNumber(discount)+"</b>";
        }
        return details;
    }
}
