package com.mastaan.logistics.models;

import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;

/**
 * Created by Venkatesh Uppu on 6/1/17.
 */
public class MoneyCollectionTransactionDetails {

    String _id;

    String ty;              //  Type    'col' > Collection, 'ref' > Refund , 'ret' > Return

    int pt;                 //  Payment Type
    String pid;             //  Payment ID

    boolean cons;           //  Consolidated or not

    double amt;             //  Collected Amount

    double amts;            //  Outstanding At that time

    String s;               //  Status

    UserDetailsMini d;      //  Delivery Boy Details
    UserDetailsMini cemp;   //  Consolidator Details
    UserDetailsMini upb;    //  Updated by employee details

    String cdate;             // Money consolidation date
    String cd;              // Money collection date

    String c;               //  Comments


    public String getID() {
        return _id;
    }

    public String getType() {
        if ( ty == null ){  ty = "";    }
        return ty;
    }

    public int getPaymentType() {
        return pt;
    }

    public void setPaymentType(int pt) {
        this.pt = pt;
    }

    public String getPaymentTypeString() {
        if (pt == 1) {
            return Constants.CASH_ON_DELIVERY;
        } else if (pt == 2) {
            return Constants.CREDIT_CARD;
        } else if (pt == 3) {
            return Constants.DEBIT_CARD;
        } else if (pt == 4) {
            return Constants.NET_BANKING;
        } else if (pt == 6||pt == 7) {
            return Constants.ONLINE_PAYMENT;
        }else if (pt == 8) {
            return Constants.PAYTM_WALLET;
        }else if (pt == 9) {
            return Constants.PAYTM_QR;
        }else if (pt == 10) {
            return Constants.BANK_TRANSFER;
        }else if (pt == 11) {
            return Constants.UPI;
        }else if (pt == 12) {
            return Constants.WRITEOFF;
        }else if (pt == 13) {
            return Constants.WALLET;
        }
        return "";
    }

    public String getPaymentID() {
        return pid;
    }
    public void setPaymetnID(String paymetnID) {
        this.pid = paymetnID;
    }

    public void setAmount(double amount) {
        this.amt = amount;
    }
    public double getAmount() {
        return amt;
    }

    public double getOutstandingAtThatTime() {
        return amts;
    }

    public void setStatus(String status) {
        if ( status.equalsIgnoreCase(Constants.WITH_ACCOUNTS) ){
            this.s = "wa";
        }else if ( status.equalsIgnoreCase(Constants.WITH_DELIVERBOY) ){
            this.s = "wd";
        }else if ( status.equalsIgnoreCase(Constants.WITH_BANK) ){
            this.s = "wb";
        }
        else{
            this.s = status;
        }
    }

    public String getStatus() {
        if ( s != null ) {
            if ( s.equalsIgnoreCase("wa") ){
                return Constants.WITH_ACCOUNTS;
            }else if ( s.equalsIgnoreCase("wd") ){
                return Constants.WITH_DELIVERBOY;
            }else if ( s.equalsIgnoreCase("wb") ){
                return Constants.WITH_BANK;
            }else{
                return s;
            }
        }
        return "";
    }

    public UserDetailsMini getDeliveryBoyDetails() {
        if ( d != null ) {
            return d;
        }
        return new UserDetailsMini();
    }
    public String getDeliveryBoyID(){
        if ( d != null && d.getID() != null ){   return d.getID();   }
        return "";
    }

    public void setConsolidatorDetails(UserDetails consolidatorDetails) {
        if ( consolidatorDetails != null ){
            this.cemp = new UserDetailsMini(consolidatorDetails.getID(), consolidatorDetails.getFullName());
        }
    }

    public UserDetailsMini getConsolidatorDetails() {
        if ( cemp != null ) {
            return cemp;
        }
        return new UserDetailsMini();
    }
    public String getConsolidatorID(){
        if ( cemp != null && cemp.getID() != null ){   return cemp.getID();   }
        return "";
    }

    public void getUpdatedBy(UserDetailsMini updatedBy) {
        this.upb = updatedBy;
    }
    public UserDetailsMini getUpdatedBy() {
        return upb;
    }

    public void setConsolidated(boolean isConsolidated) {
        this.cons = isConsolidated;
    }

    public boolean isConsolidated() {
        return cons;
    }

    public String getCollectionDate() {
        return cd;
    }

    public String getConsolidationDate() {
        return cdate!=null?cdate:"";
    }

}
