package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 28/07/18.
 */

public class ProcessedStockDetails {
    String _id;             //  ID
    String cd;              //  Created Date
    String ud;              //  Updated Date

    String ty;              // Type : chicken / mutton
    String d;               // Date

    double bpu;              //  Before Processing Units
    double bpw;              //  Before Processing Weight

    double apw;              //  After Processing Weight
    double apdw;             //  After Processing Dressed Weight
    double apsw;             //  After Processing Skinless Weight
    double aplw;             //  After Processing Liver Weight
    double apgw;             //  After Processing Gizzard Weight
    double apbw;             //  After Processing Botee Weight
    long aphp;               //  After processing head pieces
    long apbp;               //  After processing brain pieces
    long apts;               //  After processing trotter sets
    long aptp;               //  After processing tilli pieces

    double su;              //  Spoiled Units
    double sw;              //  Spoiled Weight

    String cmt;             //  Comments

    List<UserDetailsMini> prb;    //  Processed By

    UserDetailsMini cbe;    //  Creator By
    UserDetailsMini ube;    //  Update By


    public ProcessedStockDetails(String id){
        this._id = id;
    }


    public String getID() {
        return _id;
    }

    public String getCreatedDate() {
        return cd;
    }

    public String getUpdatedDate() {
        return ud;
    }

    public String getType() {
        return ty!=null?ty:"";
    }

    public void setDate(String date) {
        this.d = date;
    }
    public String getDate() {
        return d;
    }


    public void setBeforeProcessingUnits(double beforeProcessingUnits) {
        this.bpu = beforeProcessingUnits;
    }
    public double getBeforeProcessingUnits() {
        return bpu;
    }

    public void setBeforeProcessingWeight(double beforeProcessingWeight) {
        this.bpw = beforeProcessingWeight;
    }
    public double getBeforeProcessingWeight() {
        return bpw;
    }


    public void setAfterProcessingWeight(double afterProcessingWeight) {
        this.apw = afterProcessingWeight;
    }
    public double getAfterProcessingWeight() {
        return apw;
    }

    public void setAfterProcessingDressedWeight(double afterProcessingDressedWeight) {
        this.apdw = afterProcessingDressedWeight;
    }
    public double getAfterProcessingDressedWeight() {
        return apdw;
    }

    public void setAfterProcessingSkinlessWeight(double afterProcessingSkinlessWeight) {
        this.apsw = afterProcessingSkinlessWeight;
    }
    public double getAfterProcessingSkinlessWeight() {
        return apsw;
    }

    public void setAfterProcessingLiverWeight(double afterProcessingLiverWeight) {
        this.aplw = afterProcessingLiverWeight;
    }
    public double getAfterProcessingLiverWeight() {
        return aplw;
    }

    public void setAfterProcessingGizzardWeight(double afterProcessingGizzardWeight) {
        this.apgw = afterProcessingGizzardWeight;
    }
    public double getAfterProcessingGizzardWeight() {
        return apgw;
    }

    public void setAfterProcessingBoteeWeight(double afterProcessingBoteeWeight) {
        this.apbw = afterProcessingBoteeWeight;
    }
    public double getAfterProcessingBoteeWeight() {
        return apbw;
    }

    public void setAfterProcessingHeadPieces(long afterProcessingHeadPieces) {
        this.aphp = afterProcessingHeadPieces;
    }
    public long getAfterProcessingHeadPieces() {
        return aphp;
    }

    public void setAfterProcessingBrainPieces(long afterProcessingBrainPieces) {
        this.apbp = afterProcessingBrainPieces;
    }
    public long getAfterProcessingBrainPieces() {
        return apbp;
    }

    public void setAfterProcessingTrotterSets(long afterProcessingTrotterSets) {
        this.apts = afterProcessingTrotterSets;
    }
    public long getAfterProcessingTrotterSets() {
        return apts;
    }

    public void setAfterProcessingTilliPieces(long afterProcessingTilliPieces) {
        this.aptp = afterProcessingTilliPieces;
    }
    public long getAfterProcessingTilliPieces() {
        return aptp;
    }

    public void setSpoiledUnits(double spoiledUnits) {
        this.su = spoiledUnits;
    }
    public double getSpoiledUnits() {
        return su;
    }

    public void setSpoiledWeight(double spoiledWeight) {
        this.sw = spoiledWeight;
    }
    public double getSpoiledWeight() {
        return sw;
    }

    public void setComments(String cmt) {
        this.cmt = cmt;
    }
    public String getComments() {
        return cmt;
    }


    public void setProcessedBy(List<UserDetailsMini> processedBy) {
        this.prb = processedBy;
    }
    public List<UserDetailsMini> getProcessedBy() {
        return prb!=null?prb:new ArrayList<UserDetailsMini>();
    }
    public String getProcessedByNames() {
        String names = "";
        if ( prb != null && prb.size() > 0 ){
            for (int i=0;i<prb.size();i++){
                if ( prb.get(i) != null ){
                    if ( names.length() > 0 ){  names += ", ";   }
                    names += prb.get(i).getAvailableName();
                }
            }
        }
        return names;
    }

    public void setCreatedBy(UserDetailsMini createdBy) {
        this.cbe = createdBy;
    }
    public UserDetailsMini getCreatedBy() {
        return cbe!=null?cbe:new UserDetailsMini();
    }

    public void setUpdatedBy(UserDetailsMini updatedBy) {
        this.ube = updatedBy;
    }
    public UserDetailsMini getUpdatedBy() {
        return ube;
    }


    public String getName(){
        return getFormattedName().replaceAll("<b>", "").replaceAll("</b>", "").replaceAll("<u>", "").replaceAll("</u>", "").replaceAll("<br>", "\n");
    }
    public String getFormattedName(){
        return "<b><u>" + DateMethods.getReadableDateFromUTC(getDate(), DateConstants.MMM_DD_YYYY_HH_MM_A) + "</u></b>"
                + "<br>" + getID()
                + "<br>" + getAfterProcessingInfoString()
                + (getProcessedBy()!=null&&getProcessedBy().size()>0?"<br>Processing: <b>"+getProcessedByNames()+"</b>":"");
    }

    public String getAfterProcessingInfoString(){
        String afterProcessingInfo = "";//"After Processing:";
        if ( getAfterProcessingWeight() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Weight: <b>"+ CommonMethods.getIndianFormatNumber(getAfterProcessingWeight())+" kgs</b>";
        }
        if ( getAfterProcessingDressedWeight() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Dressed: <b>"+CommonMethods.getIndianFormatNumber(getAfterProcessingDressedWeight())+" kgs</b>";
        }
        if ( getAfterProcessingSkinlessWeight() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Skinless: <b>"+CommonMethods.getIndianFormatNumber(getAfterProcessingSkinlessWeight())+" kgs</b>";
        }
        if ( getAfterProcessingLiverWeight() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Liver: <b>"+CommonMethods.getIndianFormatNumber(getAfterProcessingLiverWeight())+" kgs</b>";
        }
        if ( getAfterProcessingGizzardWeight() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Gizzard: <b>"+CommonMethods.getIndianFormatNumber(getAfterProcessingGizzardWeight())+" kgs</b>";
        }
        if ( getAfterProcessingBoteeWeight() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Botee: <b>"+CommonMethods.getIndianFormatNumber(getAfterProcessingBoteeWeight())+" kgs</b>";
        }
        if ( getAfterProcessingHeadPieces() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Heads: <b>"+CommonMethods.getIndianFormatNumber(getAfterProcessingHeadPieces())+" pieces</b>";
        }
        if ( getAfterProcessingBrainPieces() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Brains: <b>"+CommonMethods.getIndianFormatNumber(getAfterProcessingBrainPieces())+" pieces</b>";
        }
        if ( getAfterProcessingTrotterSets() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Trotters: <b>"+CommonMethods.getIndianFormatNumber(getAfterProcessingTrotterSets())+" sets</b>";
        }
        if ( getAfterProcessingTilliPieces() > 0 ){
            if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
            afterProcessingInfo += "Tillies: <b>"+CommonMethods.getIndianFormatNumber(getAfterProcessingTilliPieces())+" pieces</b>";
        }
        return afterProcessingInfo;
    }

}
