package com.mastaan.logistics.models;

import com.aleena.common.methods.CommonMethods;

/**
 * Created by Venkatesh Uppu on 8/10/16.
 */

public class AppliedDiscountDetails {
    String _id;         //  ID
    String uid;         //  Unique ID
    String rid;         //  Reason ID
    String r;           //  Reason
    String c;           //  Comment

    UserDetailsMini dib;         //  Discout Created By
    boolean en;
    double amt;         //  Discount Amount


    public String getID() {
        return _id;
    }

    public boolean isActive() {
        return en;
    }

    public String getUniqueID() {
        return uid;
    }

    public String getReasonID() {
        return rid;
    }

    public String getReason() {
        if ( r != null ){   return r.trim();   }
        return "";
    }

    public UserDetailsMini getDiscountCreatorDetails() {
        return dib;
    }

    public String getDiscountCreatorID() {
        if ( dib != null ){ return dib.getID();  }
        return "";
    }

    public String getDiscountCreatorName() {
        if ( dib != null ){ return dib.getAvailableName();  }
        return "";
    }

    public double getAmount() {
        return amt;
    }

    public String getComment() {
        return c;
    }

    public String getDiscountReasonWithComment(){
        String discountDetails = getReason();
        if ( getComment() != null && getComment().trim().length() > 0 ){
            discountDetails += ": "+ CommonMethods.capitalizeFirstLetter(getComment());
        }
        return discountDetails;
    }
}
