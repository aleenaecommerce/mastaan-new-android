package com.mastaan.logistics.models;

import android.util.Log;

import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AttributeOptionDetails {

    String _id;
    String an;

    String v;       //  Option ID
    String n;       //  Option Name
    double pd;      //  Price Difference
    double wd;      // Weight Difference (In Percentage)

    String ic;      //  ImageURL

    List<String> da;    //  Disabled Attributes
    List<String> dao;   // Disabled AttributeOptions

    boolean en;     // Enabled Status
    String ar;      //  Availability Reason

    List<AttributeOptionAvailabilityDetails> avl;
    //RUN TIME
    List<AttributeOptionAvailabilityDetails> fullAvailabilitiesList;

    //RUNTIME

    String date;            // Date
    boolean status = true;      // Dummy To Handle Enabling/Disabling
    String disabledTag;


    public AttributeOptionDetails(AttributeOptionDetails attributeOptionDetails){
        this._id = attributeOptionDetails._id;
        this.v = attributeOptionDetails.v;
        this.n = attributeOptionDetails.n;
        this.ic = attributeOptionDetails.ic;
        this.pd = attributeOptionDetails.pd;
        this.wd = attributeOptionDetails.wd;

        this.da = attributeOptionDetails.da;
        this.dao = attributeOptionDetails.dao;

        this.en = attributeOptionDetails.en;
        this.ar = attributeOptionDetails.ar;
    }

    public AttributeOptionDetails(String ID, String value, String name){
        this._id = ID;
        this.v = value;
        this.n = name;
        this.en = true;
        this.status = true;
    }

    public AttributeOptionDetails(String ID, String name, String image, double price, boolean status){
        this._id = ID;
        this.n = name;
        this.ic = image;
        this.pd = price;
        this.en = status;
        this.status = status;
    }


    public String getID() {
        return _id;
    }

    public String getValue() {
        return v;
    }

    public String getName() {
        if ( n != null ){   return n;   }
        return "";
    }

    public void setPriceDifference(double priceDifference) {
        this.pd = priceDifference;
    }

    public double getPriceDifference() {
        return pd;
    }

    public double getWeightDifferencePercentage() {
        return wd;
    }

    public String getImageURL() {
        return "http:"+ic;
    }

    public List<String> getDisabledAttributes() {
        if ( da == null ){  da = new ArrayList<>(); }
        return da;
    }

    public List<String> getDisabledAttributeOptions() {
        if ( dao == null ){  dao = new ArrayList<>(); }
        return dao;
    }

    public void setEnablity(boolean en) {
        this.en = en;
    }

    public boolean isEnabled() {
        return en;
    }

    public void setAvailabilityReason(String ar) {
        this.ar = ar;
    }

    public String getAvailabilityReason() {
        if ( ar == null ){  return "";  }
        return ar;
    }

    public String getDate() {
        if ( date != null ){    return date;    }
        return "";
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDisabledTag() {
        return disabledTag;
    }




    public void setAvailabilitiesList(List<AttributeOptionAvailabilityDetails> availabilitiesList) {
        this.avl = availabilitiesList;
        this.fullAvailabilitiesList = null;
    }

    public List<AttributeOptionAvailabilityDetails> getFullAvailabilitiesList() {
        if ( avl == null ){ avl = new ArrayList<>();    }
        return avl;
    }

    public List<AttributeOptionAvailabilityDetails> getFullAvlabilitiesList(String startDate, int daysToCount) {
        if ( fullAvailabilitiesList == null || fullAvailabilitiesList.size() == 0 ) {
            fullAvailabilitiesList = new ArrayList<>();
            if (avl == null) {
                avl = new ArrayList<>();
            }
            Collections.sort(avl, new Comparator<AttributeOptionAvailabilityDetails>() {
                public int compare(AttributeOptionAvailabilityDetails item1, AttributeOptionAvailabilityDetails item2) {
                    return DateMethods.compareDates(item1.getDate(), item2.getDate());
                }
            });
            //------

            for (int x = 0; x < daysToCount; x++) {
                String date = DateMethods.addToDateInDays(startDate, x);

                AttributeOptionAvailabilityDetails availabilityDetailsForDate = null;
                for (int i = avl.size() - 1; i >= 0; i--) {
                    int dateComparisionValue = DateMethods.compareDates(date, avl.get(i).getDate());
                    if ( dateComparisionValue == 0 ){
                        availabilityDetailsForDate = new AttributeOptionAvailabilityDetails("id", date, avl.get(i).isAvailable(), avl.get(i).getPriceDifference(), avl.get(i).getAvailabilityReason());;
                        break;
                    }else if ( dateComparisionValue > 0){
                        availabilityDetailsForDate = new AttributeOptionAvailabilityDetails("id", date, true, avl.get(i).getPriceDifference(), "");;
                        break;
                    }
                }
                if (availabilityDetailsForDate == null) {
                    availabilityDetailsForDate = new AttributeOptionAvailabilityDetails("id", date, en, pd, ar);
                }
                fullAvailabilitiesList.add(availabilityDetailsForDate);
            }

            return fullAvailabilitiesList;
        }else{
            return fullAvailabilitiesList;
        }
    }

    public AttributeOptionDetails getOptionDetailsForDate(String date){
        AttributeOptionDetails optionDetailsForDate = new AttributeOptionDetails(this);
        optionDetailsForDate.setStatus(true);

        if ( avl != null && avl.size() > 0 ){
            for (int i = avl.size()-1; i>=0; i--){
                int dateComparisionValue = DateMethods.compareDates(date, avl.get(i).getDate());
                if ( dateComparisionValue >= 0 ){
                    if ( optionDetailsForDate.isEnabled() ){
                        optionDetailsForDate.setPriceDifference(avl.get(i).getPriceDifference());
                        //Setting Availability if dates match
                        if ( dateComparisionValue == 0 ){
                            optionDetailsForDate.setEnablity(avl.get(i).isAvailable());
                            optionDetailsForDate.setAvailabilityReason(avl.get(i).getAvailabilityReason());
                        }
                    }
                    break;
                }
            }
        }
        return optionDetailsForDate;
    }


}
