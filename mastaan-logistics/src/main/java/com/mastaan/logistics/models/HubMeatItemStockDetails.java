package com.mastaan.logistics.models;

import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 25/10/18.
 */

public class HubMeatItemStockDetails {

    String _id;         // ID
    String wh;          // Warehouse
    HubDetails hu;          // Hub
    WarehouseMeatItemDetails wmi;         // Warehouse Meat Item
    List<AttributeOptionDetails> ilao;  // Item Level Attribute Options

    double qu;          // In Kgs or Units or Pieces
    double tqu;         // Threshold quantity


    public String getID() {
        return _id;
    }

    public String getHubID() {
        return hu!=null?hu.getID():null;
    }

    public String getWarehouseID() {
        return wh;
    }

    public WarehouseMeatItemDetails getWarehouseMeatItemDetails() {
        return wmi;
    }
    public String getWarehouseMeatItemID() {
        return wmi!=null?wmi.getID():null;
    }

    public List<AttributeOptionDetails> getItemLevelAttributeOptions() {
        return ilao;
    }
    public List<String> getItemLevelAttributeOptionsIDs() {
        List<String> itemLevelAttributeOptions = new ArrayList<>();
        if ( ilao != null && ilao.size() > 0 ) {
            for (int i = 0; i < ilao.size(); i++) {
                if ( ilao.get(i) != null ) {
                    itemLevelAttributeOptions.add(ilao.get(i).getID());
                }
            }
        }
        return itemLevelAttributeOptions;
    }
    public List<String> getItemLevelAttributeOptionsNames() {
        List<String> itemLevelAttributeOptionsNames = new ArrayList<>();
        if ( ilao != null && ilao.size() > 0 ) {
            for (int i = 0; i < ilao.size(); i++) {
                if ( ilao.get(i) != null ) {
                    itemLevelAttributeOptionsNames.add(ilao.get(i).getName()!=null?ilao.get(i).getName():ilao.get(i).getID());
                }
            }
        }
        return itemLevelAttributeOptionsNames;
    }

    public double getQuantity() {
        return qu;
    }

    public void setThresholdQuantity(double thresholdQuantity) {
        this.tqu = thresholdQuantity;
    }
    public double getThresholdQuantity() {
        return tqu;
    }

    public String getFormattedInfo(String quantityUnit){
        return (getThresholdQuantity()>0?"DailyTQ: <b>"+CommonMethods.getInDecimalFormat(getThresholdQuantity())+" "+quantityUnit+(getThresholdQuantity()==1?"":"s")+"</b>":"")
                +"  available <b>"+CommonMethods.getInDecimalFormat(getQuantity())+" "+quantityUnit+(getQuantity()==1?"":"s")+"</b>";
    }

}
