package com.mastaan.logistics.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 15/12/15.
 */

public class Bootstrap {
    String serverTime;

    ConfigurationDetails configuration;

    List<WhereToNextItem> nextPlaces;
    List<UserDetails> deliveryBoys;

    List<String> failureReasons;
    List<String> qcFailureReasons;
    List<String> deliveryFailureReasons;
    List<DiscountDetails> discountReasons;
    List<String> customerSupportReasons;
    List<String> followUpReasons;
    List<String> cartFollowupReasons;
    List<String> itemNotAvailableReasons;
    List<String> installSources;
    List<String> customerMessages;
    List<ViolationDetails> employeeViolations;

    UserDetails user;
    Job job;

    String upgradeURL;

    WarehouseDetails warehouse;

    List<WarehouseDetails> warehouses;


    public String getServerTime() {
        return serverTime;
    }

    public ConfigurationDetails getConfiguration() {
        if ( configuration == null ){   configuration = new ConfigurationDetails(); }
        return configuration;
    }

    public String getUpgradeURL() {
        return upgradeURL;
    }

    public UserDetails getUserDetails() {
        return user;
    }

    public Job getJob() {
        return job;
    }

    public List<WhereToNextItem> getWhereToNextPlaces() {
        return nextPlaces;
    }

    public List<UserDetails> getDeliveryBoys() {
        return deliveryBoys;
    }

    public List<String> getFailureReasons() {
        if ( failureReasons == null ){  failureReasons = new ArrayList<>();  }
        return failureReasons;
    }

    public List<String> getQcFailureReasons() {
        if ( qcFailureReasons == null ){  qcFailureReasons = new ArrayList<>();  }
        return qcFailureReasons;
    }

    public List<String> getDeliveryFailureReasons() {
        if ( deliveryFailureReasons == null ){  deliveryFailureReasons = new ArrayList<>();  }
        return deliveryFailureReasons;
    }

    public List<DiscountDetails> getDiscountReasons() {
        if ( discountReasons == null ){ discountReasons = new ArrayList<>();    }
        return discountReasons;
    }

    public List<String> getCustomerSupportReasons() {
        if ( customerSupportReasons == null ){  customerSupportReasons = new ArrayList<>();  }
        return customerSupportReasons;
    }

    public List<String> getFollowupReasons() {
        if ( followUpReasons == null ){  followUpReasons = new ArrayList<>();  }
        return followUpReasons;
    }

    public List<String> getCartFollowupReasons() {
        if ( cartFollowupReasons == null ){  cartFollowupReasons = new ArrayList<>();  }
        return cartFollowupReasons;
    }

    public List<String> getItemNotAvailableReasons() {
        if ( itemNotAvailableReasons == null ){ itemNotAvailableReasons = new ArrayList<>(); }
        return itemNotAvailableReasons;
    }

    public List<String> getInstallSources() {
        if ( installSources == null ){  installSources = new ArrayList<>(); }
        return installSources;
    }

    public List<String> getCustomerMessages() {
        if ( customerMessages == null ){  customerMessages = new ArrayList<>(); }
        return customerMessages;
    }

    public List<ViolationDetails> getEmployeeViolations() {
        if ( employeeViolations == null ){  employeeViolations = new ArrayList<>(); }
        return employeeViolations;
    }

    public WarehouseDetails getWarehouseDetails() {
        return warehouse;
    }
    public String getWarehouseID() {
        return warehouse!=null?warehouse.getID():"";
    }
    public LatLng getWarehouseLocation() {
        return warehouse!=null?warehouse.getLocation():new LatLng(0, 0);
    }


    public List<WarehouseDetails> getWarehouses() {
        List<WarehouseDetails> warehousesList = new ArrayList<>();
        if ( warehouses != null ) {
            for (int i = 0; i < warehouses.size(); i++) {
                if (warehouses.get(i) != null) {
                    warehousesList.add(warehouses.get(i));
                }
            }
        }
        return warehousesList;
    }

}
