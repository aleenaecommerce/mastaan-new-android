package com.mastaan.logistics.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Naresh-Crypsis on 25-11-2015.
 */
public class UserDetails extends CustomPlace {
    String _id;
    String e;
    String __v;
    String cd;
    String f;//firstName;
    String l; //last name
    String eid;//email
    String m;// mobileNumber
    String em; //emergencyNumber
    String bg;//blood group
    String esi;
    String dl;
    String a1;
    String a2;
    String lmk;
    String ar;
    String ci;
    String pin;
    String s;
    String t;
    VehicleDetails vd;

    String fcm;     // FCM Registration id

    boolean cis;
    String att;

    String value;

    List<String> r;     // Roles List

    Status st;
    Job job;
    String jobt;

    class Status{
        double dis;
        int jobs;
    }


    public UserDetails(){}

    public UserDetails(String id, String name){
        this._id = id;
        this.f = name;
    }

    public UserDetails(String id, String name, String value){
        this._id = id;
        this.f = name;
        this.value = value;
    }

    public String getID() {
        return _id;
    }

    public String getMobileNumber() {
        return m;
    }

    public String getEmail() {
        return e;
    }

    public String getFullName() {
        if ( l != null && l.length() > 0 ){
            return f+" "+l;
        }
        return f;
    }

    public String getFirstName() {
        return f == null ? "" : f;
    }

    public String getName() {
        if ( l != null && l.length() > 0 ){
            return f+" "+l;
        }
        return getFirstName();
    }

    public String getAvailableName(){
        if ( getName() != null ){
            return getName();
        }else if ( m != null && m.length() > 0 ){
            return getMobileNumber();
        }else if ( e != null && e.length() > 0 ){
            return getEmail();
        }
        return "No Name";
    }
    

    public String getFormattedJoinDate() {
        if (cd == null)
            return null;
        String sub = cd.substring(0, cd.indexOf("T"));
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = originalFormat.parse(sub);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            SimpleDateFormat targetFormat = new SimpleDateFormat("MMM dd");
            return targetFormat.format(date);
        } catch (ParseException e1) {
            e1.printStackTrace();
            return cd;
        }
    }



    public String getA1() {
        return a1;
    }

    public String getA2() {
        return a2;
    }

    public String getAr() {
        return ar;
    }

    public String getBg() {
        return bg;
    }

    public String getCi() {
        return ci;
    }

    public String getDl() {
        return dl;
    }

    public String getEid() {
        return eid;
    }

    public String getEm() {
        return em;
    }

    public String getEsi() {
        return esi;
    }

    public String getL() {
        return l == null ? "" : l;
    }

    public String getLmk() {
        return lmk;
    }


    public String getPin() {
        return pin;
    }

    public String getS() {
        return s;
    }

    public VehicleDetails getVd() {
        return vd;
    }

    public String getProfilePic() {
        return t;
    }

    public String getValue() {
        return value;
    }

    public String getFCMRegistrationID() {
        return fcm;
    }

    public UserDetails setCheckIn(boolean checkIn, String serverTime) {
        this.cis = checkIn;
        this.att = serverTime;
        return this;
    }
    public boolean isCheckedIn(String serverTime) {
        if ( cis == true && DateMethods.compareDates(DateMethods.getOnlyDate(serverTime), DateMethods.getOnlyDate(getLastAttendanceEntryTime())) == 0 ){
            return true;
        }
        return false;
    }

    public void setLastAttendanceEntryTime(String lastAttendanceEntryTime) {
        this.att = lastAttendanceEntryTime;
    }
    public String getLastAttendanceEntryTime() {
        if ( att == null ){ att = "";   }
        return DateMethods.getReadableDateFromUTC(att);
    }

    public List<String> getRoles() {
        if ( r == null ){ return new ArrayList<>(); }
        return r;
    }

    public boolean isReadOnly(){
        if ( getRoles().contains("r") ){    return  true;   }
        return false;
    }

    public boolean isDeliveryBoy(){
        if ( getRoles().contains("db") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }

    public boolean isStockManager(){
        if ( getRoles().contains("sm") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }

    public boolean isProcessingManager(){
        if ( getRoles().contains("pm") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }

    public boolean isDeliveryBoyManager(){
        if ( getRoles().contains("dbm") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }

    public boolean isWarehouseManager(){
        if ( getRoles().contains("wm") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }

    public boolean isAccountsApprover(){
        if ( getRoles().contains("ac.ap") ){    return  true;   }
        else if ( getRoles().contains("ac.su") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }

    public boolean isCustomerRelationshipManager(){
        if ( getRoles().contains("crm") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }

    public boolean isOrdersManager(){
        if ( getRoles().contains("om") ){    return  true;   }
        else if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }

    public boolean isSuperUser(){
        if ( getRoles().contains("su") ){    return  true;   }
        return false;
    }

    public double getDistanceTravelledToday(){
        if ( st != null ){
            return st.dis;
        }
        return 0;
    }

    public double getJobsDoneToday(){
        if ( st != null ){
            return st.jobs;
        }
        return 0;
    }

    public void setJob(Job job) {
        this.job = job;
    }
    public Job getJob() {
        return job;
    }

    public String getLastJobTime() {
        if ( jobt != null ){
            return DateMethods.getReadableDateFromUTC(jobt);
        }
        return null;
    }

}
