package com.mastaan.logistics.models;

import com.aleena.common.methods.DateMethods;

/**
 * Created by venkatesh on 9/7/16.
 */

public class FeedbackDetails {
    String _id;
    String cd;

    OrderItemDetails o;

    float r;
    String c;
    String pmc;

    UserDetailsMini prb;
    boolean p;

    boolean tes;            // Testimonial


    public String getID() {
        return _id;
    }

    public String getCreatedDate() {
        return DateMethods.getReadableDateFromUTC(cd);
    }

    public float getCustomerRating() {
        return r;
    }

    public String getCustomerComments() {
        if ( c == null ){   return  "";  }
        return c.trim();
    }

    //public void setProssesorID(String prossesorID) {
    //    this.prb = prossesorID;
    //}

    public String getProssesorID() {
        if ( prb != null ){     return prb.getID(); }
        return "";
    }

    public UserDetailsMini getProcessorDetails() {
        if ( prb != null ){ return prb; }
        return new UserDetailsMini();
    }

    public void isCompleted(boolean isCompleted) {
        this.p = isCompleted;
    }

    public boolean isCompleted() {
        return p;
    }

    public String getProcessorComments() {
        return pmc;
    }

    public void setProcessorComments(String processorComments) {
        this.pmc = processorComments;
    }

    public OrderItemDetails getOrderItemDetails() {
        return o;
    }

    public void setTestimonial(boolean testimonial) {
        this.tes = testimonial;
    }
    public boolean isTestimonial() {
        return tes;
    }
}
