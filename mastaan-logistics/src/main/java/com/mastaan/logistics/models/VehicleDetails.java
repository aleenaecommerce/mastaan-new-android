package com.mastaan.logistics.models;

/**
 * Created by Naresh-Crypsis on 25-11-2015.
 */
public class VehicleDetails {
    String rn;
    String o;
    String ip;//insurance provider
    String in;//insurance Number

    public String getInsuranceNumber() {
        return in;
    }

    public String getInsuranceProvider() {
        return ip;
    }

    public String getRn() {
        return rn;
    }

    public String getO() {
        if (o.equalsIgnoreCase("so"))
            return "Self owned";
        else if (o.equalsIgnoreCase("co"))
            return "Company owned";
        else if (o.equalsIgnoreCase("oo"))
            return "Office owned";
        return o;
    }

    public String getOActual() {
        return o;
    }
}
