package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 7/10/16.
 */
public class DiscountDetails {
    String rid;
    String r;
    boolean oi;

    public DiscountDetails(){}

    public DiscountDetails(String id, String name, boolean isLinkedToOrderItem){
        this.rid = id;
        this.r = name;
        this.oi = isLinkedToOrderItem;
    }

    public String getID() {
        if ( rid != null ){   return rid;   }
        return "";
    }

    public String getReason() {
        if ( r != null ){   return r;   }
        return "";
    }

    public boolean isLinkedToOrderItem() {
        return oi;
    }
}
