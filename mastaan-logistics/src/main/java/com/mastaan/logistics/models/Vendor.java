package com.mastaan.logistics.models;

import com.google.android.gms.maps.model.LatLng;
import com.mastaan.logistics.constants.Constants;

import java.util.List;

/**
 * Created by Naresh-Crypsis on 05-01-2016.
 */
public class Vendor extends CustomPlace {
    String _id;
    String f;
    String e;
    String m;
    String g;
    String r;
    List<Double> loc;
    String cd;
    String ud;
    Boolean vb;
    Boolean st;
    String l;
    List<String> cat;

    boolean own;    // Own Vendor

    String pt;      //  Payment Type

    public Vendor(){}

    public Vendor(String id, String name){
        this._id = id;
        this.f = name;
    }

    public String getLoc() {
        return loc == null ? null : (loc.size() > 1 ? loc.get(0) + "," + loc.get(1) : null);
    }

    public LatLng getLocation() {
        return loc == null ? null : (loc.size() > 1 ? new LatLng(loc.get(0), loc.get(1)) : null);
    }

    public List<String> getCat() {
        return cat;
    }

    @Override
    public String getName() {
        return getF() + " " + getL();
    }

    public String getF() {
        return f == null ? "" : f;
    }

    public String getL() {
        return l == null ? "" : l;
    }

    public String getID() {
        if ( _id != null ){ return _id; }
        return "";
    }

    public boolean isOwn() {
        return own;
    }

    @Override
    public String toString() {
        return "vendor_" + _id;
    }

    public String getPaymentType() {
        if ( pt == null ){  pt = "ca";  }

        if ( pt.equals("cr")){
            return Constants.CREDIT;
        }else if ( pt.equals("ca")){
            return Constants.CASH;
        }else {
            return pt;
        }
    }
}
