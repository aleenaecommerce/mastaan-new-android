package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 27/07/18.
 */

public class StockAlertDetails {
    String d;                       // Date
    WarehouseMeatItemDetails wmi;   // Warehouse Meat Item
    double qu;                      // Total Quantity
    double tqu;                     // Threshold Quantity


    public String getDate() {
        return d;
    }

    public WarehouseMeatItemDetails getWarehouseMeatItemDetails() {
        return wmi;
    }

    public double getQuantity() {
        return qu;
    }

    public double getThresholdQuantity() {
        return tqu;
    }
}
