package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 28/01/18.
 */
public class DeliveryFeeDetails {

    double mindist;
    double maxdist;
    double fee;

    public DeliveryFeeDetails(double mindist, double maxdist, double fee){
        this.mindist = mindist;
        this.maxdist = maxdist;
        this.fee = fee;
    }

    public void setMinDistance(double minDistance) {
        this.mindist = minDistance;
    }
    public double getMinDistance() {
        return mindist;
    }

    public void setMaxDistance(double maxDistance) {
        this.maxdist = maxDistance;
    }
    public double getMaxDistance() {
        return maxdist;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }
    public double getFee() {
        return fee;
    }

}
