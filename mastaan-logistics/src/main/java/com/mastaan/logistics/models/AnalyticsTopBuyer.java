package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;

/**
 * Created by Venkatesh Uppu on 01/06/18.
 */

public class AnalyticsTopBuyer {
    String _id;
    long count;
    double total;
    double discount;
    BuyerDetails buyer;

    public String getID() {
        return _id;
    }

    public long getCount() {
        return count;
    }

    public double getTotal() {
        return total;
    }

    public double getDiscount() {
        return discount;
    }

    public BuyerDetails getBuyer() {
        return buyer!=null?buyer:new BuyerDetails();
    }

    public String getFormattedDetails(){
        String details = "Count: <b>"+ CommonMethods.getIndianFormatNumber(getCount())+"</b>"
                +" , Amount: <b>"+CommonMethods.getIndianFormatNumber(getTotal()+getDiscount())+"</b>"
                +(getDiscount()>0?" , Discount: <b>"+CommonMethods.getIndianFormatNumber(getDiscount())+"</b>":"");
        if ( buyer != null ){
            details += "<br>Last order: <b>"+ DateMethods.getReadableDateFromUTC(buyer.getLastOrderDate(), DateConstants.MMM_DD_YYYY)+"</b>";
        }
        return details;
    }

}
