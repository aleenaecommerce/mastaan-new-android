package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 19/2/17.
 */
public class ViolationDetails {
    String id;
    String value;

    public String getID() {
        return id;
    }

    public String getValue() {
        return value;
    }

}
