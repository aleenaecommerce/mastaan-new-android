package com.mastaan.logistics.models;

/**
 * Created by valueduser on 24/8/16.
 */
public class AvailabilityDetails {
    String _id;
    String dstr;
    boolean a;
    double pr;      // Vendor Price
    double opr;     // Selling Price After Discount
    double apr;     // Actual Selling Price Before Discount

    String ar;


    public AvailabilityDetails(AvailabilityDetails availabilityDetails){
        this(availabilityDetails.getID(), availabilityDetails.getDate(), availabilityDetails.isAvailable(), availabilityDetails.getAvailabilityReason(), availabilityDetails.getBuyingPrice(), availabilityDetails.getSellingPrice(), availabilityDetails.getActualSellingPrice());
    }
    public AvailabilityDetails(String id, String date, boolean availability, String availabilityReason, double vendorPrice, double buyerPrice, double actualBuyerPrice){
        this._id = id;
        this.dstr = date;
        this.a = availability;
        this.ar = availabilityReason;
        this.pr = vendorPrice;
        this.opr = buyerPrice;
        this.apr = actualBuyerPrice;
    }

    public String getID() {
        return _id;
    }

    public String getDate() {
        return dstr;
    }

    public AvailabilityDetails setAvailability(boolean isAvailable) {
        this.a = isAvailable;
        return this;
    }
    public boolean isAvailable() {
        return a;
    }

    public double getBuyingPrice() {
        return pr;
    }

    public double getSellingPrice() {
        return opr;
    }

    public double getActualSellingPrice() {
        if ( apr <= 0 ){    return  opr;    }
        return apr;
    }

    public String getAvailabilityReason() {
        if ( ar != null ){  return ar;  }
        return "";
    }
}
