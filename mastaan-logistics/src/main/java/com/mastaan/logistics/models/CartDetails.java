package com.mastaan.logistics.models;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 03/03/19.
 */

public class CartDetails {

    String _id;

    String cd;                  // Created Date
    String ud;                  // Updated Date

    BuyerDetails b;             // Buyer Details

    List<CartItem> i;    // Cart Items

    UserDetailsMini foe;        // Followup employee
    String cmt;                 // Comments

    class CartItem{
        WarehouseMeatItemDetails i;

        public WarehouseMeatItemDetails getWarehouseMeatItemDetails() {
            return i;
        }
    }


    public String getID() {
        return _id;
    }

    public String getCreatedDate() {
        return cd;
    }

    public String getUpdatedDate() {
        return ud;
    }

    public BuyerDetails getBuyerDetails() {
        return b;
    }

    public int getItemsCount(){
        return i!=null?i.size():0;
    }
    public String getItemsNames(){
        String items = "";
        if ( i != null && i.size() > 0 ){
            for (int a=0;a<i.size();a++){
                if ( i.get(a) != null && i.get(a).getWarehouseMeatItemDetails() != null && i.get(a).getWarehouseMeatItemDetails().getMeatItemDetais() != null ){
                    items += (items.length()>0?", ":"") + i.get(a).getWarehouseMeatItemDetails().getMeatItemDetais().getNameWithCategoryAndSize();
                }
            }
        }
        return items;
    }

    public CartDetails setFollowupEmployee(UserDetailsMini followupEmployee) {
        this.foe = followupEmployee;
        return this;
    }
    public UserDetailsMini getFollowupEmployee() {
        return foe;
    }

    public CartDetails setComments(String comments) {
        this.cmt = comments;
        return this;
    }
    public String getComments() {
        return cmt;
    }

}
