package com.mastaan.logistics.models;

import com.aleena.common.methods.CommonMethods;

/**
 * Created by Venkatesh Uppu on 01/09/18.
 */

public class AnalyticsReferral {
    String _id;
    long count;
    double total;
    double discount;
    BuyerDetails buyer;


    public String getID() {
        return _id;
    }

    public long getCount() {
        return count;
    }

    public double getTotal() {
        return total;
    }

    public double getDiscount() {
        return discount;
    }

    public BuyerDetails getBuyer() {
        return buyer;
    }

    public String getFormattedTitle(){
        if ( buyer != null ){
            return _id;//+" of "+buyer.getAvailableName();
        }
        return _id;
    }
    public String getFormattedDetails(){
        String details = "";
        if ( count > 0 ){
            details += "Orders Count: <b>"+ CommonMethods.getIndianFormatNumber(count)+"</b>";
        }
        if ( total > 0 ){
            details += (details.length()>0?"<br>":"")+"Orders Total: <b>"+ CommonMethods.getIndianFormatNumber(total)+"</b>";
        }
        if ( discount > 0 ){
            details += (details.length()>0?"<br>":"")+"Total Discount: <b>"+ CommonMethods.getIndianFormatNumber(discount)+"</b>";
        }
        return details;
    }
}
