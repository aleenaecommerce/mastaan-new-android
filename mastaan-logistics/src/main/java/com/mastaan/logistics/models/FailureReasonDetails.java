package com.mastaan.logistics.models;

/**
 * Created by venkatesh on 14/5/16.
 */
public class FailureReasonDetails extends CustomPlace {
    String name;

    public FailureReasonDetails(String name){
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
