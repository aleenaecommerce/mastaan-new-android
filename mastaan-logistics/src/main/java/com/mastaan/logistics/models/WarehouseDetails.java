package com.mastaan.logistics.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 04/04/18.
 */

public class WarehouseDetails {

    String _id;     // Warehouse ID
    boolean en;         // Status
    String n;       // Name
    String fa;      // Full Address
    List<Double> loc;   // LatLng


    public String getID() {
        return _id;
    }

    public void setStatus(boolean status) {
        this.en = status;
    }
    public boolean getStatus() {
        return en;
    }

    public void setName(String name) {
        this.n = name;
    }
    public String getName() {
        return n;
    }

    public void setFullAddress(String fullAddress) {
        this.fa = fullAddress;
    }
    public String getFullAddress() {
        return fa;
    }

    public void setLocation(LatLng location) {
        List<Double> loc = new ArrayList<>();
        loc.add((location.longitude));
        loc.add((location.latitude));
        this.loc = loc;
    }
    public LatLng getLocation() {
        return loc!=null&&loc.size()==2?new LatLng(loc.get(1), loc.get(0)):new LatLng(0, 0);
    }
    public String getLocationString(){
        return loc!=null && loc.size()==2?loc.get(1)+","+loc.get(0):"0,0";
    }

}
