package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 19/06/18.
 */

public class BuyerSearchDetails {

    String id;

    String query;

    String fromDate;
    String toDate;

    String is;

    String meatItem;

    String couponCode;

    String referralCode;

    String membership;

    String outstanding; // true/false

    String orderedOnce; // true/false

    String notInterested; // true/false

    String relocated;    // true/false

    String willOrderSoon; // true/false

    String followupEmployee;

    int lastOrderDays;
    int lastNotOrderDays;

    int lastFollowupDays;
    int lastNotFollowupDays;


    public BuyerSearchDetails(){}
    
    public BuyerSearchDetails(BuyerSearchDetails buyerSearchDetails){
        this.id = buyerSearchDetails.id;

        this.query = buyerSearchDetails.query;

        this.fromDate = buyerSearchDetails.fromDate;
        this.toDate = buyerSearchDetails.toDate;

        this.is = buyerSearchDetails.is;

        this.meatItem = buyerSearchDetails.meatItem;

        this.couponCode = buyerSearchDetails.couponCode;

        this.referralCode = buyerSearchDetails.referralCode;

        this.membership = buyerSearchDetails.membership;

        this.outstanding = buyerSearchDetails.outstanding;

        this.orderedOnce = buyerSearchDetails.orderedOnce;

        this.notInterested = buyerSearchDetails.notInterested;

        this.relocated = buyerSearchDetails.relocated;

        this.willOrderSoon = buyerSearchDetails.willOrderSoon;

        this.followupEmployee = buyerSearchDetails.followupEmployee;

        this.lastOrderDays = buyerSearchDetails.lastOrderDays;
        this.lastNotOrderDays = buyerSearchDetails.lastNotOrderDays;

        this.lastFollowupDays = buyerSearchDetails.lastFollowupDays;
        this.lastNotFollowupDays = buyerSearchDetails.lastNotFollowupDays;
    }

    public BuyerSearchDetails setID(String id) {
        this.id = id;
        return this;
    }

    public BuyerSearchDetails setQuery(String query) {
        this.query = query;
        return this;
    }
    public String getQuery() {
        return query;
    }


    public BuyerSearchDetails setInstallSource(String installSource) {
        this.is = installSource;
        return this;
    }

    public BuyerSearchDetails setMeatItem(String meatItem) {
        this.meatItem = meatItem;
        return this;
    }
    public String getMeatItem() {
        return meatItem;
    }
    public BuyerSearchDetails setFromDate(String fromDate) {
        this.fromDate = fromDate;
        return this;
    }
    public BuyerSearchDetails setToDate(String toDate) {
        this.toDate = toDate;
        return this;
    }

    public BuyerSearchDetails setCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }
    public String getCouponCode() {
        return couponCode;
    }

    public BuyerSearchDetails setReferralCode(String referralCode) {
        this.referralCode = referralCode;
        return this;
    }
    public String getReferralCode() {
        return referralCode;
    }

    public BuyerSearchDetails setMembership(String membership) {
        this.membership = membership;
        return this;
    }
    public String getMembership() {
        return membership;
    }

    public BuyerSearchDetails setOutstanding(boolean outstanding) {
        this.outstanding = outstanding+"";
        return this;
    }

    public BuyerSearchDetails setOrderedOnce(boolean orderedOnce) {
        this.orderedOnce = orderedOnce+"";
        return this;
    }

    public BuyerSearchDetails setNotInterested(boolean notInterested) {
        this.notInterested = notInterested+"";
        return this;
    }

    public BuyerSearchDetails setRelocated(boolean relocated) {
        this.relocated = relocated+"";
        return this;
    }

    public BuyerSearchDetails setWillOrderSoon(boolean willOrderSoon) {
        this.willOrderSoon = willOrderSoon+"";
        return this;
    }

    public BuyerSearchDetails setFollowupEmployee(String followupEmployee) {
        this.followupEmployee = followupEmployee;
        return this;
    }

    public BuyerSearchDetails setLastOrderDays(int lastOrderDays) {
        this.lastOrderDays = lastOrderDays;
        return this;
    }

    public BuyerSearchDetails setLastNotOrderDays(int lastNotOrderDays) {
        this.lastNotOrderDays = lastNotOrderDays;
        return this;
    }

    public BuyerSearchDetails setLastFollowupDays(int lastFollowupDays) {
        this.lastFollowupDays = lastFollowupDays;
        return this;
    }

    public BuyerSearchDetails setLastNotFollowupDays(int lastNotFollowupDays) {
        this.lastNotFollowupDays = lastNotFollowupDays;
        return this;
    }

}
