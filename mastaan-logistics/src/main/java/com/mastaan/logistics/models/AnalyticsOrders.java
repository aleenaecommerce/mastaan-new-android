package com.mastaan.logistics.models;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 12/01/18.
 */

public class AnalyticsOrders {
    String _id;

    String date;

    long count;
    double total;
    double final_total;
    double delivery_charges;
    double service_charges;
    double surcharges;
    double taxes;
    double discount;
    double coupon_discount;
    double membership_discount;
    double membership_order_discount;
    double membership_delivery_discount;

    List<SourceStats> sources;

    public class SourceStats{
        String _id;
        long n;

        public String getID() {
            return _id;
        }

        public String getName(){
            if ( _id != null ){
                if ( _id.equalsIgnoreCase("a") ){
                    return "Android";
                }else if ( _id.equalsIgnoreCase("i") ){
                    return "iOS";
                }else if ( _id.equalsIgnoreCase("w") ){
                    return "Web";
                }else if ( _id.equalsIgnoreCase("p") ){
                    return "Phone";
                }
            }
            return "Unknown";
        }

        public long getCount() {
            return n;
        }
    }

    public String getDate() {
        return date;
    }

    public long getCount() {
        return count;
    }

    public double getAmount() {
        return total;
    }

    public double getFinalAmount() {
        return final_total;
    }

    public double getDeliveryCharges() {
        return delivery_charges;
    }

    public double getServiceCharges() {
        return service_charges;
    }

    public double getSurcharges() {
        return surcharges;
    }

    public double getTaxes() {
        return taxes;
    }

    public double getDiscount() {
        return discount;
    }

    public double getCouponDiscount() {
        return coupon_discount;
    }

    public double getMembershipDiscount() {
        return membership_discount;
    }

    public double getMembershipOrderDiscount() {
        return membership_order_discount;
    }

    public double getMembershipDeliveryDiscount() {
        return membership_delivery_discount;
    }

    public List<SourceStats> getSources() {
        if ( sources == null ){    sources = new ArrayList<>(); }
        return sources;
    }

    public String getFormattedDetails(){
        String details = "Count: <b>"+ CommonMethods.getIndianFormatNumber(getCount())+"</b>"
                +" , Amount: <b>"+CommonMethods.getIndianFormatNumber(Math.round(getAmount()))+"</b>";

        if ( getDeliveryCharges() > 0 || getServiceCharges() > 0 || getSurcharges() > 0 || getTaxes() > 0 ){
            String amountSplitString = "O: <b>"+CommonMethods.getIndianFormatNumber(Math.round(getAmount()-getDeliveryCharges()-getServiceCharges()-getSurcharges()-getTaxes()))+"</b>";
            if ( getDeliveryCharges() > 0 ){
                amountSplitString += ", Del: <b>" + CommonMethods.getIndianFormatNumber(Math.round(getDeliveryCharges())) + "</b>";
            }
            if ( getServiceCharges() > 0 ) {
                amountSplitString += ", Ser: <b>" + CommonMethods.getIndianFormatNumber(Math.round(getServiceCharges())) + "</b>";
            }
            if ( getSurcharges() > 0 ) {
                amountSplitString += ", Sur: <b>" + CommonMethods.getIndianFormatNumber(Math.round(getSurcharges())) + "</b>";
            }
            if ( getTaxes() > 0 ) {
                amountSplitString += ", Tax: <b>" + CommonMethods.getIndianFormatNumber(Math.round(getTaxes())) + "</b>";
            }
            details += " ("+amountSplitString+")";
        }

        details += ", Final Amount: <b>"+CommonMethods.getIndianFormatNumber(Math.round(getFinalAmount()))+"</b>";

        if ( getDiscount() > 0 ){
            String discountString = "Dis: <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(getDiscount()))+"</b>";
            if ( getCouponDiscount() > 0 ){
                discountString += " (Cou: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(getCouponDiscount()))+"</b>)";
            }
            if ( getMembershipDiscount() > 0 ){
                discountString += " (Mem: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(getMembershipDiscount()))+"</b>"
                        +(" ["
                        +(getMembershipOrderDiscount()>0?"O: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(getMembershipOrderDiscount()))+"</b>":"")
                        +(getMembershipDeliveryDiscount()>0?(getMembershipOrderDiscount()>0?", ":"")+"Del: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(Math.round(getMembershipDeliveryDiscount()))+"</b>":"")
                        +"]")
                        +")";
            }
            details += "<br>"+discountString;
        }

        String sourceStatsDetails = "";
        for (int i=0;i<getSources().size();i++){
            SourceStats sourceStats = getSources().get(i);
            sourceStatsDetails += (sourceStatsDetails.length()>0?", ":"")+sourceStats.getName()+": <b>"+CommonMethods.getIndianFormatNumber(sourceStats.getCount())+"</b>";
        }
        if ( sourceStatsDetails.length() > 0 ){
            details += "<br>"+sourceStatsDetails;
        }
        return details;
    }
}
