package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 08/02/19.
 */

public class ConfigurationDetails {

    Integer maximum_serviceable_distance;
    Integer threshold_distance;

    Integer pending_followups_last_order_days;

    String bill_printer_ip_address;
    String label_printer_ip_address;


    public ConfigurationDetails setMaximumServiceableDistance(int maximum_serviceable_distance) {
        this.maximum_serviceable_distance = maximum_serviceable_distance;
        return this;
    }
    public Integer getMaximumServiceableDistance() {
        return maximum_serviceable_distance;
    }

    public ConfigurationDetails setThresholdDistance(int threshold_distance) {
        this.threshold_distance = threshold_distance;
        return this;
    }
    public Integer getThresholdDistance() {
        return threshold_distance;
    }

    public ConfigurationDetails setPendingFollowupsLastOrderDays(int pending_followups_last_order_days) {
        this.pending_followups_last_order_days = pending_followups_last_order_days;
        return this;
    }
    public int getPendingFollowupsLastOrderDays() {
        if ( pending_followups_last_order_days != null ){
            return pending_followups_last_order_days;
        }
        return 0;
    }

    public void setBillPrinterIpAddress(String bill_printer_ip_address) {
        this.bill_printer_ip_address = bill_printer_ip_address;
    }
    public String getBillPrinterIpAddress() {
        return bill_printer_ip_address;
    }

    public ConfigurationDetails setLabelPrinterIpAddress(String label_printer_ip_address) {
        this.label_printer_ip_address = label_printer_ip_address;
        return this;
    }
    public String getLabelPrinterIpAddress() {
        return label_printer_ip_address;
    }

}
