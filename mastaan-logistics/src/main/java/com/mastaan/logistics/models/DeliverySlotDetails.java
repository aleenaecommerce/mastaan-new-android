package com.mastaan.logistics.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 1/2/16.
 */
public class DeliverySlotDetails {

    String _id;

    public int sh;                      // Start hour
    public int sm;                      // Start minutes
    public int eh;                      // End hour
    public int em;                      // End minutes

    double mindist;                      // Minimum Distance
    double maxdist;                      // Maximum Distance

    long mnum;                          // Maximum orders count
    int ctime;                          //  Cutt off time
    List<DeliveryFeeDetails> delfees;   // Delivery Fees
    List<String> dcats;                 //  Disabled Categories
    List<String> dwms;                  // Disabled Warehouse Meat Items
    List<String> ddow;                  // Disabled Days of the week

    boolean en;                         // Enabled or Disabled
    long inum;                          // Orders count

    public DeliverySlotDetails(int fromHour, int fromMinutes, int toHour, int toMinutes){
        this.sh = fromHour;
        this.sm = fromMinutes;
        this.eh = toHour;
        this.em = toMinutes;
    }

    public String getID() {
        return _id;
    }

    public int getStartHour() {
        return sh;
    }
    public int getStartMinute() {
        return sm;
    }
    public String getStartTime() {
        return String.format("%02d", sh)+":"+String.format("%02d", sm);
    }

    public int getEndHour() {
        return eh;
    }
    public int getEndMinute() {
        return em;
    }
    public String getEndTime() {
        return String.format("%02d", eh)+":"+String.format("%02d", em);
    }

    public String getDisplayText() {
        return DateMethods.getTimeIn12HrFormat(getStartTime())+" to "+DateMethods.getTimeIn12HrFormat(getEndTime());
    }

    public void setMinimumDistance(double minimumDistance) {
        this.mindist = minimumDistance;
    }
    public double getMinimumDistance() {
        return mindist;
    }

    public void setMaximumDistance(double maximumDistance) {
        this.maxdist = maximumDistance;
    }
    public double getMaximumDistance() {
        return maxdist;
    }

    public void setMaximumOrders(long maximumOrders) {
        this.mnum = maximumOrders;
    }
    public long getMaximumOrders() {
        return mnum;
    }

    public void setCutoffTime(int cutoffTime) {
        this.ctime = cutoffTime;
    }
    public int getCutoffTime() {
        return ctime;
    }

    public void setDeliveryFees(List<DeliveryFeeDetails> deliveryFees) {
        this.delfees = deliveryFees;
    }
    public List<DeliveryFeeDetails> getDeliveryFees() {
        if ( delfees == null ){ delfees = new ArrayList<>();
//            delfees.add(new DeliveryFeeDetails(1, 3, 10));
//            delfees.add(new DeliveryFeeDetails(3, 5, 20));
//            delfees.add(new DeliveryFeeDetails(5, 7, 30));
//            delfees.add(new DeliveryFeeDetails(7, 9, 40));
//            delfees.add(new DeliveryFeeDetails(9, 11, 50));
        }
        return delfees;
    }

    public void setDisabledCategories(List<String> disabledCategories) {
        this.dcats = disabledCategories;
    }
    public List<String> getDisabledCategories() {
        if ( dcats == null ){    dcats = new ArrayList<>();   }
        return dcats;
    }
    public String getDisabledCategoriesString(){
        return CommonMethods.capitalizeStringWords(CommonMethods.getStringFromStringList(getDisabledCategories(), ", ")
                .replaceAll("countrychicken", "Country Chicken")
                .replaceAll("farmchicken", "Farm Chicken")
                .replaceAll("goatmutton", "Goat Mutton")
                .replaceAll("sheepmutton", "Sheep Mutton")
                .replaceAll("freshwatersf", "Fresh Water SF")
                .replaceAll("seawatersf", "Sea  Water SF")
        );
    }

    public void setDisabledWarehouseMetatItems(List<String> disabledWarehouseMetatItems) {
        this.dwms = disabledWarehouseMetatItems;
    }
    public List<String> getDisabledWarehouseMetatItems() {
        if ( dwms == null ){    dwms = new ArrayList<>();   }
        return dwms;
    }

    public void setDisabledWeekDays(List<String> disabledWeekDays) {
        this.ddow = disabledWeekDays;
    }
    public List<String> getDisabledWeekDays() {
        if ( ddow == null ){    ddow = new ArrayList<>();   }
        return ddow;
    }
    public String getDisabledWeekDaysString(){
        return CommonMethods.capitalizeStringWords(CommonMethods.getStringFromStringList(getDisabledWeekDays(), ", "));
    }

    public DeliverySlotDetails setEnabled(boolean en) {
        this.en = en;
        return this;
    }
    public boolean isEnabled() {
        return en;
    }

    public long getOrdersCount() {
        return inum;
    }

}
