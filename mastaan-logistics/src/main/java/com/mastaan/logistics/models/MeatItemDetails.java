package com.mastaan.logistics.models;

import java.util.ArrayList;
import java.util.List;

public class MeatItemDetails {

    public String _id;
    public String i;
    public String t;
    public String p;
    public double pr;      //  Vendor Price
    public double opr;     //  Buyer Price
    public String qt;
    public double mw;
    public double pt;
    public List<CategoryDetails> c;
    public List<String> n;
    public List<String> enn;

    String size;

    boolean po;     //  Pre order?
    String pot;     //  Pre order cut off time
    boolean adn;

    List<AttributeDetails> ma;      //     Available Attributes


    public String getID() {
        return _id;
    }

    public String getMainName() {
        if ( i==null ){ i="";   }
        return i;
    }
    public CategoryDetails getCategoryDetails() {
        if ( c != null && c.size() > 0 ) {
            return c.get(0);
        }
        return new CategoryDetails();
    }


    public String getPicture() {
        return "http:" + t;
    }

    public String getOriginalPicture() {
        return t;
    }

    public String getQuantityType() {
        return qt;
    }

    public String getQuantityUnit() {
        if ( qt == null){ qt ="";}
        if ( qt.equalsIgnoreCase("w") ){
            return "kg";
        }else if ( qt.equalsIgnoreCase("n") ){
            return  "piece";
        }else if ( qt.equalsIgnoreCase("s") ){
            return "set";
        }
        return "";
    }


    public String getName() {
        if ( n != null && n.size() > 0 ){
            return n.get(0);
        }else{
            return "";
        }
    }

    public String getNameWithCategory() {
        return getCategoryDetails().getName().trim()+" " + getMainName().replaceAll(getCategoryDetails().getName(), "").trim();
    }
    public String getNameWithSize() {
        return getMainName()+(size!=null && size.trim().length() > 0?" ("+size+")":"");
    }
    public String getNameWithCategoryAndSize() {
        return getNameWithCategory()+(size!=null && size.trim().length() > 0?" ("+size+")":"");
    }

    public List<String> getMultiLingualNamesList() {
        if ( n == null ){ n = new ArrayList<>();    }
        return n;
    }

    public List<String> getEnglishNamesList() {
        if ( enn == null ){ enn = new ArrayList<>();    }
        return enn;
    }

    public String getImageURL() {
        return "http:"+p;
    }

    public String getThumbnailImageURL() {
        return "http:"+t;
    }

    public double getVendorPrice() {
        return pr;
    }
    public void setVendorPrice(double vendorPrice) {
        this.pr = vendorPrice;
    }

    public double getBuyerPrice() {
        return opr;
    }
    public void setBuyerPrice(double buyerPrice) {
        this.opr = buyerPrice;
    }

    public boolean isPreOrderable() {
        return po;
    }
    public void setIsPreOrderable(boolean preOrderable) {
        this.po = preOrderable;
    }

    public String getPreOrderCutoffTime() {
        return pot;
    }
    public void setPreOrderCutoffTime(String preOrderCutoffTime) {
        this.pot = preOrderCutoffTime;
    }

    public boolean isAddon() {
        return adn;
    }
    public void setIsAddon(boolean isAddon) {
        this.adn = isAddon;
    }

    public double getPreparationHours() {
        return pt;
    }
    public void setPreparationTime(double preparationTime) {
        this.pt = preparationTime;
    }

    public double getMinimumWeight() {
        return mw;
    }
    public void setMinimumWeight(double minimumWeight) {
        this.mw = minimumWeight;
    }

    public void setSize(String size) {
        this.size = size;
    }
    public String getSize() {
        return size;
    }


    public double getPrice() {
        return pr;
    }

    public double getOriginalPrice() {
        return pr;
    }

    public List<CategoryDetails> getC() {
        return c;
    }

    public String getParentCategory() {
        return (getC() == null || getC().size() == 0) ? "Title" : c.get(0).getParentCategoryName();
    }

    public String getCategoryIdentifier() {
        return (getC() == null || getC().size() == 0) ? "Title" : c.get(0).getCategoryIdentifier();
    }

    public void setAttributes(List<AttributeDetails> attributesList) {
        this.ma = attributesList;
    }

    public List<AttributeDetails> getAttributes() {
        if ( ma != null ){
            return ma;
        }
        return new ArrayList<>();
    }

}
