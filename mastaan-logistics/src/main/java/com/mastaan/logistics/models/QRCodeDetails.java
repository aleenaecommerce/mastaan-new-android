package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;

/**
 * Created by Venkatesh Uppu on 05/06/18.
 */

public class QRCodeDetails {
    String name;
    int image;

    public QRCodeDetails(String name, int image){
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

}
