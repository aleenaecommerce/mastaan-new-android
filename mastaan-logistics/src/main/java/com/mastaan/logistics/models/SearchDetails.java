package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;

/**
 * Created by venkatesh on 12/7/16.
 */
public class SearchDetails {

    String action;

    String id;

    String status;

    String customerName;
    String itemName;
    String itemPreferences;
    String employeeName;
    String processorName;
    String deliveryBoyName;
    String consolidatorName;
    String vendorName;
    String butcherName;
    String stockID;

    String deliveryDate;
    String deliveryTime;
    String feedbackDate;
    String collectionDate;
    String consolidationDate;

    String housingSociety;
    String deliveryArea;
    String deliveryZone;

    String collectionType;
    String matchType;
    String moneyCollectionType;
    String stockType;

    double minAmount = -1;
    double maxAmount = -1;
    String discountType;
    String outstandingType;

    String source;

    String paymentType;
    String orderDayType;

    String firstTimeOrderType;
    String referralOrderType;
    String followupType;
    String followupResultedOrderType;
    String billPrintType;
    String deliveryAreaAssignType;
    String customerSupportType;
    String cashNCarryType;
    String instructionsType;
    String delayType;
    String createdBy;

    String category_type;
    double rating_type = -1;
    String comment_type;

    String comments;

    boolean considerAtleastOneMatch;

    public SearchDetails(){}

    public  SearchDetails(String customerName) {
        this.customerName = customerName;
    }


    public boolean isSearchableExcludingStatus(){
        return isSearchable(false);
    }
    public boolean isSearchable(){
        return isSearchable(true);
    }
    private boolean isSearchable(boolean includeStatus){
        if ( (includeStatus && (status != null && status.trim().length() > 0))
                || (id != null && id.trim().length() > 0)
                || (customerName != null && customerName.trim().length() > 0)
                || (itemName != null && itemName.trim().length() > 0)
                || (itemPreferences != null && itemPreferences.trim().length() > 0)
                || (category_type != null && category_type.trim().length() > 0)
                || rating_type != -1
                || (comment_type != null && comment_type.trim().length() > 0)
                || (employeeName != null && employeeName.trim().length() > 0)
                || (processorName != null && processorName.trim().length() > 0)
                || (consolidatorName != null && consolidatorName.trim().length() > 0)
                || (deliveryBoyName != null && deliveryBoyName.trim().length() > 0)
                || (vendorName != null && vendorName.trim().length() > 0)
                || (butcherName != null && butcherName.trim().length() > 0)
                || (stockID != null && stockID.trim().length() > 0)
                || (deliveryDate != null && deliveryDate.trim().length() > 0)
                || (deliveryTime != null && deliveryTime.trim().length() > 0)
                || (feedbackDate != null && feedbackDate.trim().length() > 0)
                || (collectionDate != null && collectionDate.trim().length() > 0)
                || (consolidationDate != null && consolidationDate.trim().length() > 0)
                || (housingSociety != null && housingSociety.trim().length() > 0)
                || (deliveryArea != null && deliveryArea.trim().length() > 0)
                || (deliveryZone != null && deliveryZone.trim().length() > 0)
                || (collectionType != null && collectionType.trim().length() > 0)
                || (matchType != null && matchType.trim().length() > 0)
                || (moneyCollectionType != null && moneyCollectionType.trim().length() > 0)
                || (stockType != null && stockType.trim().length() > 0)
                || minAmount != -1
                || maxAmount != -1
                || (discountType != null && discountType.trim().length() > 0)
                || (outstandingType != null && outstandingType.trim().length() > 0)
                || (source != null && source.trim().length() > 0)
                || (paymentType != null && paymentType.trim().length() > 0)
                || (orderDayType != null && orderDayType.trim().length() > 0)
                || (firstTimeOrderType != null && firstTimeOrderType.trim().length() > 0)
                || (referralOrderType != null && referralOrderType.trim().length() > 0)
                || (followupType != null && followupType.trim().length() > 0)
                || (followupResultedOrderType != null && followupResultedOrderType.trim().length() > 0)
                || (billPrintType != null && billPrintType.trim().length() > 0)
                || (deliveryAreaAssignType!= null && deliveryAreaAssignType.trim().length()>0)
                || (customerSupportType != null && customerSupportType.trim().length() > 0)
                || (cashNCarryType != null && cashNCarryType.trim().length() > 0)
                || (delayType != null && delayType.trim().length() > 0)
                || (instructionsType != null && instructionsType.trim().length() > 0)
                || (createdBy != null && createdBy.trim().length() > 0)
                || (comments != null && comments.trim().length() > 0)
                ){
            return true;
        }
        return false;
    }

    public boolean isConsiderAtleastOneMatch() {
        return considerAtleastOneMatch;
    }

    public SearchDetails setConsiderAtleastOneMatch(boolean considerAtleastOneMatch) {
        this.considerAtleastOneMatch = considerAtleastOneMatch;
        return this;
    }
    public SearchDetails setConsiderAtleastOneMatch(String searchName) {
        this.considerAtleastOneMatch = true;
        this.customerName = searchName;
        this.id = searchName;
        this.itemName = searchName;
        this.itemPreferences = searchName;
        this.housingSociety = searchName;
        this.deliveryArea = searchName;
        this.deliveryZone = searchName;
        this.vendorName = searchName;
        this.butcherName = searchName;
        this.stockID = searchName;
        this.employeeName = searchName;
        this.processorName = searchName;
        this.deliveryBoyName = searchName;
        this.consolidatorName = searchName;
        this.comments = searchName;
        return this;
    }

    public String getSearchString(){
        String searchString = "";

        // FOR ATLEAST ONE MATCH
        if ( considerAtleastOneMatch ){
            searchString = CommonMethods.capitalizeFirstLetter(customerName);
        }
        // FOR COMBINED MATCH
        else{
            String separatorText = ", ";
//            if ( isConsiderAtleastOneMatch() ){ separatorText = " (or) ";   }

            if ( (id != null && id.trim().length() > 0) ){
                searchString += "ID: "+CommonMethods.capitalizeFirstLetter(id.trim());//.toUpperCase();
            }
            if ( (status != null && status.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Status: "+CommonMethods.capitalizeFirstLetter(status.trim());//.toUpperCase();
            }
            if ( (customerName != null && customerName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Customer: "+CommonMethods.capitalizeFirstLetter(customerName.trim());//.toUpperCase();
            }
            if ( (itemName != null && itemName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Item: "+CommonMethods.capitalizeFirstLetter(itemName.trim());//.toUpperCase();
            }
            if ( (itemPreferences != null && itemPreferences.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Item prefs: "+CommonMethods.capitalizeFirstLetter(itemPreferences.trim());//.toUpperCase();
            }
            if ( (category_type != null && category_type.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(category_type.trim());////.toUpperCase();
            }
            if ( (rating_type != -1) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                if ( rating_type == Constants.LOW_RATED ) {
                    searchString += "Low rated";
                }else if ( rating_type == Constants.AVERAGE_RATED ) {
                    searchString += "Average rated";
                }else if ( rating_type == Constants.HIGH_RATED ) {
                    searchString += "High rated";
                }
            }
            if ( (comment_type != null && comment_type.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += /*"Comments: "+*/CommonMethods.capitalizeFirstLetter(comment_type.trim());////.toUpperCase();
            }

            if ( (employeeName != null && employeeName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Employee: "+CommonMethods.capitalizeFirstLetter(employeeName.toLowerCase().trim());//.toUpperCase();
            }
            if ( (processorName != null && processorName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Processor: "+CommonMethods.capitalizeFirstLetter(processorName.toLowerCase().trim());//.toUpperCase();
            }
            if ( (deliveryBoyName != null && deliveryBoyName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Delivery boy: "+CommonMethods.capitalizeFirstLetter(deliveryBoyName.toLowerCase().trim());//.toUpperCase();
            }
            if ( (consolidatorName != null && consolidatorName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Consolidator: "+CommonMethods.capitalizeFirstLetter(consolidatorName.toLowerCase().trim());//.toUpperCase();
            }
            if ( (vendorName != null && vendorName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Vendor: "+CommonMethods.capitalizeFirstLetter(vendorName.toLowerCase().trim());//.toUpperCase();
            }
            if ( (butcherName != null && butcherName.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Butcher: "+CommonMethods.capitalizeFirstLetter(butcherName.toLowerCase().trim());//.toUpperCase();
            }
            if ( (stockID != null && stockID.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Stock: "+stockID;//.toUpperCase();
            }
            if ( (deliveryDate != null && deliveryDate.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Date: "+ DateMethods.getDateInFormat(deliveryDate, DateConstants.MMM_DD_YYYY).toUpperCase();//.toUpperCase();
            }
            if ( (deliveryTime != null && deliveryTime.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Time: "+ DateMethods.getTimeIn12HrFormat(deliveryTime).toUpperCase();//.toUpperCase();
            }
            if ( (feedbackDate != null && feedbackDate.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Feedback date: "+ DateMethods.getDateInFormat(feedbackDate, DateConstants.MMM_DD_YYYY).toUpperCase();//.toUpperCase();
            }
            if ( (collectionDate != null && collectionDate.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Collection date: "+ DateMethods.getDateInFormat(collectionDate, DateConstants.MMM_DD_YYYY).toUpperCase();//.toUpperCase();
            }
            if ( (consolidationDate != null && consolidationDate.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Consolidation date: "+ DateMethods.getDateInFormat(consolidationDate, DateConstants.MMM_DD_YYYY).toUpperCase();//.toUpperCase();
            }
            if ( (housingSociety != null && housingSociety.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Housing Society: "+ CommonMethods.capitalizeFirstLetter(housingSociety.trim());
            }
            if ( (deliveryArea != null && deliveryArea.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Area: "+ CommonMethods.capitalizeFirstLetter(deliveryArea.trim());
            }
            if ( (deliveryZone != null && deliveryZone.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Zone: "+ CommonMethods.capitalizeFirstLetter(deliveryZone.trim());
            }
            if ( (collectionType != null && collectionType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Collection type: "+ CommonMethods.capitalizeFirstLetter(collectionType.trim());
            }
            if ( (matchType != null && matchType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Match type: "+ CommonMethods.capitalizeFirstLetter(matchType.trim());
            }
            if ( (moneyCollectionType != null && moneyCollectionType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Money collection type: "+ CommonMethods.capitalizeFirstLetter(moneyCollectionType.trim());
            }
            if ( (stockType != null && stockType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Stock type: "+ CommonMethods.capitalizeFirstLetter(stockType.trim());
            }
            if ( (minAmount != -1) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Min.order Amt: "+ CommonMethods.getIndianFormatNumber(minAmount);
            }
            if ( (maxAmount != -1) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Max.order Amt: "+ CommonMethods.getIndianFormatNumber(maxAmount);
            }
            if ( (source != null && source.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Source: "+ source.trim().toUpperCase();
            }
            if ( (paymentType != null && paymentType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Payment mode: "+ paymentType.trim().toUpperCase();
            }
            if ( (orderDayType != null && orderDayType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(orderDayType.trim());
            }
            if ( (discountType != null && discountType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(discountType.trim());
            }
            if ( (outstandingType != null && outstandingType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(outstandingType.trim());
            }
            if ( (firstTimeOrderType != null && firstTimeOrderType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(firstTimeOrderType.trim());
            }
            if ( (referralOrderType != null && referralOrderType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(referralOrderType.trim());
            }
            if ( (followupType != null && followupType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Followup type: "+CommonMethods.capitalizeFirstLetter(followupType.trim());
            }
            if ( (followupResultedOrderType != null && followupResultedOrderType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(followupResultedOrderType.trim());
            }
            if ( (billPrintType != null && billPrintType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(billPrintType.trim());
            }
            if ( (deliveryAreaAssignType != null && deliveryAreaAssignType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(deliveryAreaAssignType.trim());
            }
            if ( (customerSupportType != null && customerSupportType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(customerSupportType.trim());
            }
            if ( (cashNCarryType != null && cashNCarryType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(cashNCarryType.trim());
            }
            if ( (delayType != null && delayType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(delayType.trim());
            }
            if ( (instructionsType != null && instructionsType.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += CommonMethods.capitalizeFirstLetter(instructionsType.trim());
            }
            if ( (createdBy != null && createdBy.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Created by: "+CommonMethods.capitalizeFirstLetter(createdBy.trim());
            }
            if ( (comments != null && comments.trim().length() > 0) ){
                if ( searchString.length() > 0 ){   searchString += separatorText;    }
                searchString += "Comments: "+CommonMethods.capitalizeFirstLetter(comments.trim());
            }
        }

        return searchString;
    }

    public SearchDetails setAction(String action) {
        this.action = action;
        return this;
    }
    public String getAction() {
        if ( action == null ){  return "";  }
        return action;
    }

    public SearchDetails setID(String id) {
        this.id = id;
        return this;
    }
    public String getID() {
        return id;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getStatus() {
        return status;
    }

    public SearchDetails setCustomerName(String name) {
        this.customerName = name;
        return this;
    }
    public String getCustomerName() {
        return customerName;
    }

    public SearchDetails setItemName(String itemName) {
        this.itemName = itemName;
        return this;
    }
    public String getItemName() {
        return itemName;
    }

    public SearchDetails setItemPreferences(String itemPreferences) {
        this.itemPreferences = itemPreferences;
        return this;
    }
    public String getItemPreferences() {
        return itemPreferences;
    }

    public SearchDetails setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
        return this;
    }
    public String getEmployeeName() {
        return employeeName;
    }

    public SearchDetails setProcessorName(String processorName) {
        this.processorName = processorName;
        return this;
    }
    public String getProcessorName() {
        return processorName;
    }

    public SearchDetails setDeliveryBoyName(String deliveryBoyName) {
        this.deliveryBoyName = deliveryBoyName;
        return this;
    }
    public String getDeliveryBoyName() {
        return deliveryBoyName;
    }

    public SearchDetails setConsolidatorName(String consolidatorName) {
        this.consolidatorName = consolidatorName;
        return this;
    }
    public String getConsolidatorName() {
        return consolidatorName;
    }


    public String getVendorName() {
        return vendorName;
    }
    public SearchDetails setVendorName(String vendorName) {
        this.vendorName = vendorName;
        return this;
    }

    public String getButcherName() {
        return butcherName;
    }
    public SearchDetails setButcherName(String butcherName) {
        this.butcherName = butcherName;
        return this;
    }

    public String getStockID() {
        return stockID;
    }
    public SearchDetails setStockID(String stockID) {
        this.stockID = stockID;
        return this;
    }

    public String getCategoryType() {
        return category_type;
    }
    public SearchDetails setCategoryType(String category_type) {
        this.category_type = category_type;
        return this;
    }

    public SearchDetails setRatingType(double rating) {
        this.rating_type = rating;
        return this;
    }
    public double getRatingType() {
        return rating_type;
    }

    public SearchDetails setCommentsType(String comment_type) {
        this.comment_type = comment_type;
        return this;
    }
    public String getCommentsType() {
        return comment_type;
    }

    public SearchDetails setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
        return this;
    }
    public String getDeliveryDate() {
        return deliveryDate;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }
    public SearchDetails setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
        return this;
    }

    public SearchDetails setFeedbackDate(String feedbackDate) {
        this.feedbackDate = feedbackDate;
        return this;
    }
    public String getFeedbackDate() {
        return feedbackDate;
    }

    public SearchDetails setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
        return this;
    }
    public String getCollectionDate() {
        return collectionDate;
    }

    public SearchDetails setConsolidationDate(String consolidationDate) {
        this.consolidationDate = consolidationDate;
        return this;
    }
    public String getConsolidationDate() {
        return consolidationDate;
    }

    public String getHousingSociety() {
        return housingSociety;
    }
    public SearchDetails setHousingSociety(String housingSociety) {
        this.housingSociety = housingSociety;
        return this;
    }

    public String getDeliveryArea() {
        return deliveryArea;
    }
    public SearchDetails setDeliveryArea(String deliveryArea) {
        this.deliveryArea = deliveryArea;
        return this;
    }

    public String getDeliveryZone() {
        return deliveryZone;
    }
    public SearchDetails setDeliveryZone(String deliveryZone) {
        this.deliveryZone = deliveryZone;
        return this;
    }

    public SearchDetails setCollectionType(String collectionType) {
        this.collectionType = collectionType;
        return this;
    }
    public String getCollectionType() {
        return collectionType;
    }

    public SearchDetails setMatchType(String matchType) {
        this.matchType = matchType;
        return this;
    }
    public String getMatchType() {
        return matchType;
    }

    public SearchDetails setMoneyCollectionType(String moneyCollectionType) {
        this.moneyCollectionType = moneyCollectionType;
        return this;
    }
    public String getMoneyCollectionType() {
        return moneyCollectionType;
    }

    public SearchDetails setStockType(String stockType) {
        this.stockType = stockType;
        return this;
    }
    public String getStockType() {
        return stockType;
    }

    public double getMinAmount() {
        return minAmount;
    }
    public void setMinAmount(double minAmount) {
        this.minAmount = minAmount;
    }

    public double getMaxAmount() {
        return maxAmount;
    }
    public void setMaxAmount(double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public String getPaymentType() {
        return paymentType;
    }
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getOrderDayType() {
        return orderDayType;
    }
    public SearchDetails setOrderDayType(String orderDayType) {
        this.orderDayType = orderDayType;
        return this;
    }

    public String getDiscountType() {
        return discountType;
    }
    public SearchDetails setDiscountType(String discountType) {
        this.discountType = discountType;
        return this;
    }

    public String getOutstandingType() {
        return outstandingType;
    }
    public SearchDetails setOutstandingType(String outstandingType) {
        this.outstandingType = outstandingType;
        return this;
    }

    public SearchDetails setFirstTimeOrderType(String firstTimeOrderType) {
        this.firstTimeOrderType = firstTimeOrderType;
        return this;
    }
    public String getFirstTimeOrderType() {
        return firstTimeOrderType;
    }

    public SearchDetails setReferralOrderType(String referralOrderType) {
        this.referralOrderType = referralOrderType;
        return this;
    }
    public String getReferralOrderType() {
        return referralOrderType;
    }

    public SearchDetails setFollowupType(String followupType) {
        this.followupType = followupType;
        return this;
    }
    public String getFollowupType() {
        return followupType;
    }

    public SearchDetails setFollowupResultedOrderType(String followupResultedOrderType) {
        this.followupResultedOrderType = followupResultedOrderType;
        return this;
    }
    public String getFollowupResultedOrderType() {
        return followupResultedOrderType;
    }

    public String getBillPrintType() {
        return billPrintType;
    }
    public SearchDetails setBillPrintType(String billPrintType) {
        this.billPrintType = billPrintType;
        return this;
    }

    public String getDeliveryAreaAssignType() {
        return deliveryAreaAssignType;
    }
    public SearchDetails setDeliveryAreaAssignType(String deliveryAreaAssignType) {
        this.deliveryAreaAssignType = deliveryAreaAssignType;
        return this;
    }

    public String getCustomerSupportType() {
        return customerSupportType;
    }
    public SearchDetails setCustomerSupportType(String customerSupportType) {
        this.customerSupportType = customerSupportType;
        return this;
    }

    public String getCashNCarryType() {
        return cashNCarryType;
    }
    public SearchDetails setCashNCarryType(String cashNCarryType) {
        this.cashNCarryType = cashNCarryType;
        return this;
    }

    public String getDelayType() {
        return delayType;
    }
    public SearchDetails setDelayType(String delayType) {
        this.delayType = delayType;
        return this;
    }

    public String getInstructionsType() {
        return instructionsType;
    }
    public SearchDetails setInstructionsType(String instructionsType) {
        this.instructionsType = instructionsType;
        return this;
    }

    public SearchDetails setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public String getCreatedBy() {
        return createdBy;
    }

    public SearchDetails setComments(String comments) {
        this.comments = comments;
        return this;
    }
    public String getComments() {
        return comments;
    }
}
