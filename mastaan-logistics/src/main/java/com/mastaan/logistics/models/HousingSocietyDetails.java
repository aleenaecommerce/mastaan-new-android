package com.mastaan.logistics.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 23/09/18.
 */

public class HousingSocietyDetails {
    String _id;
    String n;
    List<Double> pos;

    public String getID() {
        if ( _id == null ){    _id = "";  }
        return _id;
    }

    public String getName() {
        if ( n == null ){    n = "";  }
        return n;
    }

    public LatLng getLatLng() {
        if ( pos != null && pos.size() == 2 ){
            return new LatLng(pos.get(1), pos.get(0));
        }
        return new LatLng(0, 0);
    }
    public String getLatLngString() {
        LatLng latLng = getLatLng();
        return latLng.latitude+","+latLng.longitude;
    }

}
