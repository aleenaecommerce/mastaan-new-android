package com.mastaan.logistics.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 12/8/16.
 */
public class GroupedOrdersList {

    String categoryName;
    List<OrderDetails> itemsList;

    public GroupedOrdersList(){}

    public GroupedOrdersList(String categoryName, List<OrderDetails> itemsList) {
        this.categoryName = categoryName;
        this.itemsList = itemsList;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<OrderDetails> getItems() {
        if ( itemsList == null ){ itemsList = new ArrayList<>();    }
        return itemsList;
    }

    public void setItems(List<OrderDetails> itemsList) {
        this.itemsList = itemsList;
    }

    public void addItem(OrderDetails orderDetails) {
        if ( itemsList == null ){ itemsList = new ArrayList<>();  }
        itemsList.add(orderDetails);
    }
}
