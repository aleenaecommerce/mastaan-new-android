package com.mastaan.logistics.models;

/**
 * Created by venkatesh on 6/5/16.
 */
public class SidebarItemDetails {
    CharSequence name;
    int icon;

    public SidebarItemDetails(CharSequence name, int icon){
        this.name = name;
        this.icon = icon;
    }

    public CharSequence getName() {
        return name;
    }

    public int getIcon() {
        return icon;
    }
}
