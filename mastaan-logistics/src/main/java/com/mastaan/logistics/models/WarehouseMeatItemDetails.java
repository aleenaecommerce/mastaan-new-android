package com.mastaan.logistics.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.constants.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by venkatesh on 12/10/15.
 */

public class WarehouseMeatItemDetails {

    String _id;                         // Warehouse Meat Item ID
    boolean en;                         // Enabled / Disabled
    String wh;                          // Warehouse
    double pr;                          // Buying Price
    double opr;                         // Selling Price
    MeatItemDetails i;                  // Meat Items Details
    List<AvailabilityDetails> avl;      // Availability Details
    boolean a;                          // Available or not
    String avst;                        // Availability start time
    String avet;                        // Availability end time (only current day)
    double idx;                         // Index
    String hlt;                         // Highlight text
    String so;                          // Source

    List<WarehouseMeatItemStockDetails> stocks; // Warehouse Stocks
    List<HubMeatItemStockDetails> hub_stocks; // Warehouse Stocks


    public WarehouseMeatItemDetails(){}
    public WarehouseMeatItemDetails(String id){
        this._id = id;
    }

    public String getID() {
        return _id;
    }

    public String getMeatItemID() {
        return i.getID();
    }

//    public String getCategoryID(){
//        return i.getCategoryID();
//    }


    public void setEnabled(boolean en) {
        this.en = en;
    }
    public boolean isEnabled() {
        return en;
    }

    public void setBuyingPrice(double buyingPrice) {
        this.pr = buyingPrice;
    }

    public double getBuyingPrice() {
        return pr;
    }

    public double getBuyingPriceForDate(String date) {
        AvailabilityDetails availabilityDetails = getAvailabilityDetailsForDate(date);
        if ( availabilityDetails != null ){
            availabilityDetails.getBuyingPrice();
        }
        return pr;
    }

    public void setSellingPrice(double sellingPrice) {
        this.opr = sellingPrice;
    }

    public double getSellingPrice() {
        return opr;
    }

    public double getSellingPriceForDate(String date) {
        AvailabilityDetails availabilityDetails = getAvailabilityDetailsForDate(date);
        if ( availabilityDetails != null ){
            availabilityDetails.getSellingPrice();
        }
        return opr;
    }
    public AvailabilityDetails getAvailabilityDetailsForDate(String date){
        if ( avl == null ){ avl = new ArrayList<>();    }
        // SORTING GROUPS BY DATE
        Collections.sort(avl, new Comparator<AvailabilityDetails>() {
            public int compare(AvailabilityDetails item1, AvailabilityDetails item2) {
                return DateMethods.compareDates(item1.getDate(), item2.getDate());
            }
        });

        for (int i=avl.size()-1;i>=0;i--){
            //Log.d(Constants.LOG_TAG, i+getMeatItemDetais().getNameWithCategoryAndSize()+ " Date = " + date + " > " + new Gson().toJson(avl.get(i)) + " : " + compare);
            int compare = DateMethods.compareDates(date, avl.get(i).getDate());
            if ( compare == 0 ){
                //Log.d(Constants.LOG_TAG, "Date =======> Exact match of " + date + " is " + avl.get(i).getDate());
                return avl.get(i);
            }
            else if ( compare > 0 ){
                //Log.d(Constants.LOG_TAG, "Date =======> Prev of " + date + " is " + avl.get(i).getDate());
                return new AvailabilityDetails(avl.get(i)).setAvailability(true);
            }
        }
        return new AvailabilityDetails("", date, true, "", pr, opr, opr);
    }

    public WarehouseMeatItemDetails setMeatItemDetails(MeatItemDetails meatItemDetails) {
        this.i = meatItemDetails;
        return this;
    }

    public void setMeatItemDetais(MeatItemDetails meatItemDetais) {
        this.i = meatItemDetais;
    }
    public MeatItemDetails getMeatItemDetais() {
        if ( i!= null ){    return i;}
        return new MeatItemDetails();
    }

    public void setAvailability(boolean isAvailable) {
        this.a = isAvailable;
    }
    public boolean isAvailable() {
        return a;
    }

    public void setAvailabilityStartTime(String availabilityStartTime) {
        this.avst = availabilityStartTime;
    }
    public String getAvailabilityStartTime() {
        return avst;
    }

    public void setAvailabilityEndTime(String availabilityEndTime) {
        this.avet = availabilityEndTime;
    }
    public String getAvailabilityEndTime() {
        return avet;
    }

    public void setHighlightText(String highlightText) {
        this.hlt = highlightText;
    }
    public String getHighlightText() {
        return hlt;
    }

    public void setSource(String source) {
        this.so = source;
    }
    public String getSource() {
        return so;
    }

    public double getIndex() {
        return idx;
    }
    public void setIndex(double index) {
        this.idx = index;
    }

    public List<AvailabilityDetails> getAvlabiltiesList() {
        if ( avl == null ){     avl = new ArrayList<>();    }
        return avl;
    }

    public void setAvlabiltiesList(List<AvailabilityDetails> availabilitiesList) {
        this.avl = availabilitiesList;
    }

    public List<AvailabilityDetails> getFullAvlabiltiesList(String startDate, int daysToCount) {
        startDate = DateMethods.getOnlyDate(startDate);
        if ( startDate != null && startDate.length() > 0 ) {
            if (avl != null) {
                List<AvailabilityDetails> availabilitiesList = new ArrayList<>();
                for (int x = 0; x < daysToCount; x++) {
                    String date = DateMethods.addToDateInDays(startDate, x);
                    AvailabilityDetails availabilityDetails = getExactAvaialbilityDetailsOfDate(date);

                    if ( availabilityDetails == null ){
                        if ( availabilitiesList.size() > 0 ){
                            availabilityDetails = new AvailabilityDetails("", date, true, "", availabilitiesList.get(availabilitiesList.size()-1).getBuyingPrice(), availabilitiesList.get(availabilitiesList.size()-1).getSellingPrice(), availabilitiesList.get(availabilitiesList.size()-1).getActualSellingPrice());
                        }else{
                            if ( x == 0 ) {
                                AvailabilityDetails tempAvailDetails = getAvaialbilityDetailsOfLatestPreviousDate(date);
                                if ( tempAvailDetails != null ) {
                                    availabilityDetails = new AvailabilityDetails("", date, true, "", tempAvailDetails.getBuyingPrice(), tempAvailDetails.getSellingPrice(), tempAvailDetails.getActualSellingPrice());
                                }else{
                                    availabilityDetails = new AvailabilityDetails("", date, true, "", pr, opr, opr);
                                }
                            }else{
                                availabilityDetails = new AvailabilityDetails("", date, true, "", pr, opr, opr);
                            }
                        }
                    }
                    availabilitiesList.add(availabilityDetails);
                }
                return availabilitiesList;
            }
        }
        return new ArrayList<>();
    }

    private AvailabilityDetails getExactAvaialbilityDetailsOfDate(String date){
        List<AvailabilityDetails> availabilities = getAvlabiltiesList();
        for (int i=0;i<availabilities.size();i++){
            if ( availabilities.get(i) != null &&
                    DateMethods.compareDates(availabilities.get(i).getDate(), date) == 0 ){
                return availabilities.get(i);
            }
        }
        return null;
    }

    private AvailabilityDetails getAvaialbilityDetailsOfLatestPreviousDate(String date){
        List<AvailabilityDetails> availabilities = getAvlabiltiesList();
        // SORTING GROUPS BY DATE
        Collections.sort(availabilities, new Comparator<AvailabilityDetails>() {
            public int compare(AvailabilityDetails item1, AvailabilityDetails item2) {
                return DateMethods.compareDates(item1.getDate(), item2.getDate());
            }
        });

        for (int i=availabilities.size()-1;i>=0;i--){
            //Log.d(Constants.LOG_TAG, i+" Date = "+availabilities.get(i).getDate());
            if ( DateMethods.compareDates(date, availabilities.get(i).getDate()) >= 0 ){
                //Log.d(Constants.LOG_TAG, "Prev of "+date+" is "+availabilities.get(i).getDate());
                return availabilities.get(i);
            }
        }
        return null;
    }

    public boolean isAvailableOnDate(String date){
        AvailabilityDetails availabilityDetails = getExactAvaialbilityDetailsOfDate(date);
        if ( availabilityDetails == null ){
            return true;
        }else{
            return availabilityDetails.isAvailable();
        }
    }

    public List<WarehouseMeatItemStockDetails> getStocks() {
        return stocks;
    }
    public String getFormattedStocksInfo(){
        String stocksInfo = "";
        if ( stocks != null && stocks.size() > 0 ){
            for (int i=0;i<stocks.size();i++){
                WarehouseMeatItemStockDetails stockDetails = stocks.get(i);
                stocksInfo += (stocksInfo.length()>0?"<br>":"") + "<u>"+CommonMethods.getStringFromStringList(stockDetails.getItemLevelAttributeOptionsNames()) + "</u><br>" + stockDetails.getFormattedInfo(getMeatItemDetais().getQuantityUnit());
            }
        }
        return stocksInfo;
    }

    public List<HubMeatItemStockDetails> getHubStocks() {
        return hub_stocks;
    }
    public String getFormattedHubStocksInfo(){
        String stocksInfo = "";
        if ( hub_stocks != null && hub_stocks.size() > 0 ){
            for (int i=0;i<hub_stocks.size();i++){
                HubMeatItemStockDetails stockDetails = hub_stocks.get(i);
                stocksInfo += (stocksInfo.length()>0?"<br>":"") + "<u>"+CommonMethods.getStringFromStringList(stockDetails.getItemLevelAttributeOptionsNames()) + "</u><br>" + stockDetails.getFormattedInfo(getMeatItemDetais().getQuantityUnit());
            }
        }
        return stocksInfo;
    }

}
