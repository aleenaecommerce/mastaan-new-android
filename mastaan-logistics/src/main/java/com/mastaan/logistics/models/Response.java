package com.mastaan.logistics.models;

/**
 * Created by Venkatesh Uppu on 21/03/18.
 */

public class Response<T> {
    boolean status;
    int statusCode;
    String message;

    T data;


    public Response(boolean status, int statusCode, String message){
        this(status, statusCode, message, null);
    }
    public Response(boolean status, int statusCode, String message, T data){
        this.status = status;
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
    }

    public boolean getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Response setData(T data) {
        this.data = data;
        return this;
    }
    public T getData() {
        return data;
    }

}
