package com.mastaan.logistics.models;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.methods.DateMethods;

/**
 * Created by Venkatesh Uppu on 30/06/18.
 */

public class ItemLogDetails {
    String cd;  // Created Date
    //String i;   // Item
    UserDetailsMini emp;     // Employee
    String det;     // Details


    public String getCreatedDate() {
        return DateMethods.getReadableDateFromUTC(cd, DateConstants.MMM_DD_YYYY_HH_MM_A);
    }

    public UserDetailsMini getEmployeeDetails() {
        return emp!=null?emp:new UserDetailsMini("", "-");
    }

    public String getDetails() {
        return det!=null?det:"-";
    }
}
