package com.mastaan.logistics.models;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.constants.Constants;

/**
 * Created by Venkatesh Uppu on 10/01/19.
 */

public class MembershipDetails {

    String _id;

    boolean st;         // Status

    String ty;          // Type

    double bo_min;      // Buyer total orders minimum amount
    double bo_max;      // Buyer total orders maximum amount

    double o_di;        // order - discount percent
    double o_min;       // order - minimum  amount
    double o_max_di;    // order - maximum discount

    double dc_di;       // delivery charge - discount percent
    double dc_o_min;    // delivery charge - order - minimum amount

    String col1;        // Background Color
    String col2;        // Text Color


    public MembershipDetails(boolean status, String type, double buyerTotalOrdersMinimumAmount, double buyerTotalOrdersMaximumAmount, double orderDiscount, double orderMinimumAmount, double orderMaximumDiscount, double deliveryChargesDiscount, double deliveryChargesOrderMinimumAmount){
        this.st = status;
        this.ty = type;
        this.bo_min = buyerTotalOrdersMinimumAmount;
        this.bo_max = buyerTotalOrdersMaximumAmount;
        this.o_di = orderDiscount;
        this.o_min = orderMinimumAmount;
        this.o_max_di = orderMaximumDiscount;
        this.dc_di = deliveryChargesDiscount;
        this.dc_o_min = deliveryChargesOrderMinimumAmount;
    }

    public MembershipDetails setID(String id) {
        this._id = id;
        return this;
    }
    public String getID() {
        return _id;
    }

    public boolean getStatus() {
        return st;
    }

    public String getType() {
        return ty!=null?ty:"";
    }

    public String getTypeName() {
        if ( ty != null && ty.length() > 0 ){
            if ( ty.equalsIgnoreCase("bm") ){
                return Constants.BRONZE;
            }else if ( ty.equalsIgnoreCase("sm") ){
                return Constants.SILVER;
            }else if ( ty.equalsIgnoreCase("gm") ){
                return Constants.GOLD;
            }else if ( ty.equalsIgnoreCase("dm") ){
                return Constants.DIAMOND;
            }else if ( ty.equalsIgnoreCase("pm") ){
                return Constants.PLATINUM;
            }
        }
        return "-";
    }

    public MembershipDetails setBackgroundColor(String backgroundColor) {
        this.col1 = backgroundColor;
        return this;
    }
    public String getBackgroundColor() {
        if ( col1 != null && col1.trim().length() > 0 ){
            if ( col1.charAt(0) != '#' ){
                col1 = "#"+col1;
            }
            return col1;
        }
        return "#ffffff";
    }

    public MembershipDetails setTextColor(String textColor) {
        this.col2 = textColor;
        return this;
    }
    public String getTextColor(){
        if ( col2 != null && col2.trim().length() > 0 ){
            if ( col2.charAt(0) != '#' ){
                col2 = "#"+col2;
            }
            return col2;
        }
        return "#000000";
    }

    public double getBuyerTotalOrdersMinimumAmount() {
        return bo_min;
    }
    public double getBuyerTotalOrdersMaximumAmount() {
        return bo_max;
    }

    public double getOrderDiscount() {
        return o_di;
    }

    public double getOrderMinimumAmount() {
        return o_min;
    }

    public double getOrderMaximumDiscount() {
        return o_max_di;
    }

    public double getDeliveryChargesDiscount() {
        return dc_di;
    }

    public double getDeliveryChargesOrderMinimumAmount() {
        return dc_o_min;
    }


    public String getFormattedDetails(){
        String detailsString = "";

        if ( getBuyerTotalOrdersMinimumAmount() > 0 ){
            detailsString += (detailsString.length()>0?"<br>":"") + "<b>Eligibility</b><br>";

            if ( getBuyerTotalOrdersMaximumAmount() > 0 ){
                detailsString += "Total ordered value between <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getBuyerTotalOrdersMinimumAmount())+"</b>"
                        + " and " + "<b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getBuyerTotalOrdersMaximumAmount())+"</b>";
            }else{
                detailsString += "Total ordered value exceeds <b>"+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getBuyerTotalOrdersMinimumAmount())+"</b>";
            }
        }

        if ( getDeliveryChargesDiscount() > 0 || getOrderDiscount() > 0 ){
            detailsString += (detailsString.length()>0?"<br><br>":"") + "<b>Benefits</b><br>";

            if ( getDeliveryChargesDiscount() > 0 ){
                detailsString += "<b>#</b> Delivery Charges: <b>"+CommonMethods.getInDecimalFormat(getDeliveryChargesDiscount())+"%"+"</b> off"
                        + (getDeliveryChargesOrderMinimumAmount()>0?" (min order: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getDeliveryChargesOrderMinimumAmount())+"</b>)":"")
                ;
            }

            if ( getOrderDiscount() > 0 ){
                detailsString += (getDeliveryChargesDiscount()>0?"<br>":"") + "<b>#</b> Discount: <b>"+CommonMethods.getInDecimalFormat(getOrderDiscount())+"%"+"</b> off";
                if ( getOrderMinimumAmount() > 0 || getOrderMaximumDiscount() > 0 ){
                    detailsString += " ("
                            + (getOrderMinimumAmount()>0?"min order: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getOrderMinimumAmount())+"</b>":"")
                            + (getOrderMaximumDiscount()>0?(getOrderMinimumAmount()>0?", ":"")+"max discount: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(getOrderMaximumDiscount())+"</b>":"")
                            + ")";
                }
            }
        }

        return detailsString;
    }

}
