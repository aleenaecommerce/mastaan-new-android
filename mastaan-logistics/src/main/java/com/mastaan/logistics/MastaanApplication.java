package com.mastaan.logistics;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.view.View;

import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.localdata.OrderItemsDatabase2;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.List;

/**
 * Created by venkatesh on 15/9/15.
 */

public class MastaanApplication extends Application{//MultiDexApplication {

    OrderItemsDatabase2 orderItemsDatabase;

    @Override
    public void onCreate() {
        super.onCreate();

        // Initializing HyperTrack
        //HyperTrack.initialize(this, getString(R.string.hypertrack_api_key));

    }

    public OrderItemsDatabase2 getOrderItemsDatabase() {
        if ( orderItemsDatabase == null ){
            orderItemsDatabase = new OrderItemsDatabase2(getApplicationContext());
        }
        return orderItemsDatabase;
    }

    //------

    List<WarehouseMeatItemDetails> warehouseMeatItems;
    public void setCachedWarehouseMeatItems(List<WarehouseMeatItemDetails> warehouseMeatItems) {
        this.warehouseMeatItems = warehouseMeatItems;
    }
    public List<WarehouseMeatItemDetails> getCachedWarehouseMeatItems() {
        return warehouseMeatItems;
    }

    public WarehouseMeatItemDetails getCachedWarehouseMeatMeatItemDetails(String warehouseMeatItemID){
        try{
            if ( warehouseMeatItems != null ){
                for (int i=0;i<warehouseMeatItems.size();i++){
                    if ( warehouseMeatItems.get(i) != null && warehouseMeatItems.get(i).getID().equalsIgnoreCase(warehouseMeatItemID) ){
                        return warehouseMeatItems.get(i);
                    }
                }
            }
        }catch (Exception e){}
        return null;
    }
}
