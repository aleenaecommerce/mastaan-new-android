package com.mastaan.logistics.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aleena.common.widgets.vViewPager;
import com.google.android.material.tabs.TabLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.BaseOrderItemsActivity;
import com.mastaan.logistics.activities.DeliveryBoyDeliveriesActivity;
import com.mastaan.logistics.adapters.OrdersPagerAdapter;
import com.mastaan.logistics.interfaces.OrdersListCallback;
import com.mastaan.logistics.methods.ActivityMethods;
import com.mastaan.logistics.methods.GroupingMethods;
import com.mastaan.logistics.models.GroupedOrdersList;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.SearchDetails;

import java.util.ArrayList;
import java.util.List;


public class BaseOrdersFragment extends MastaanFragment {

    BaseOrderItemsActivity activity;

//    vAppBarLayout appBarLayout;

    TabLayout tabLayout;
    vViewPager viewPager;
    OrdersPagerAdapter adapter;

    List<GroupedOrdersList> groupedOrdersLists;

    SearchDetails searchDetails;

    public BaseOrdersFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_base_orders, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();

        activity = (BaseOrderItemsActivity) getActivity();

        //--------

//        appBarLayout = (vAppBarLayout) view.findViewById(R.id.appBarLayout);

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabTextColors(getResources().getColor(R.color.normal_tab_text_color), getResources().getColor(R.color.selected_tab_text_color));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                onPerformSearch(searchDetails);//getSearchViewText());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });

        viewPager = (vViewPager) view.findViewById(R.id.viewPager);
        //viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    tabLayout.getTabAt(position).select();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));  // To Sync ViewPager with Tabs.


    }

    @Override
    public void onReloadPressed() {
        activity.onReloadPressed();
    }

    public List<OrderDetails> getOrders(String statusType){
        if ( groupedOrdersLists != null ) {
            for (int i = 0; i < groupedOrdersLists.size(); i++) {
                if (groupedOrdersLists.get(i).getCategoryName().equalsIgnoreCase(statusType)) {
                    return groupedOrdersLists.get(i).getItems();
                }
            }
        }
        return new ArrayList<>();
    }

    public void displayItems(){

        activity.hideSearchView();
        activity.hideFilterMenuItem();
        showLoadingIndicator("Loading, wait...");
        GroupingMethods.getOrdersFromOrderItems(GroupingMethods.getOrderItemsListFromGroups(activity.groupedOrdersItemsLists), new OrdersListCallback() {
            @Override
            public void onComplete(boolean status, final int statusCode, String message, List<OrderDetails> ordersList) {
                String deliveryBoyID = "";
                if (ActivityMethods.isMyDeliveriesActivity(activity)) {
                    deliveryBoyID = localStorageData.getUserID();
                }else if (ActivityMethods.isDeliveryBoyDeliveriesActivity(activity)) {
                    try{deliveryBoyID = ((DeliveryBoyDeliveriesActivity)activity).deliveryBoyID;}catch (Exception e){}
                }

                GroupingMethods.groupAndSortOrdersByStatus(localStorageData.getServerTime(), ordersList, deliveryBoyID, localStorageData.getWarehouseLocation(), new GroupingMethods.GroupAndSortOrdersByStatusCallback() {
                    @Override
                    public void onComplete(boolean status, List<GroupedOrdersList> groupedOrdersLists) {
                        if (status) {
                            displayItems(groupedOrdersLists);
                        } else {
                            showReloadIndicator("Something went wrong, try again!");
                        }
                    }
                });
            }
        });
    }

    public void displayItems(List<GroupedOrdersList> groupedOrdersLists){
        try {
            this.groupedOrdersLists = groupedOrdersLists;

            if (groupedOrdersLists.size() > 0) {
                tabLayout.removeAllTabs();
                for (int i = 0; i < groupedOrdersLists.size(); i++) {
                    TabLayout.Tab tab = tabLayout.newTab();
                    View rowView = LayoutInflater.from(context).inflate(R.layout.view_tab_list_item, null, false);
                    TextView tabTitle = (TextView) rowView.findViewById(R.id.tabTitle);
                    tabTitle.setText(groupedOrdersLists.get(i).getCategoryName().toUpperCase());
                    tab.setCustomView(rowView);
                    tabLayout.addTab(tab);
                }
                if (groupedOrdersLists.size() == 1) {
                    tabLayout.setVisibility(View.GONE);
                } else {
                    tabLayout.setVisibility(View.VISIBLE);
                }

                adapter = new OrdersPagerAdapter(getChildFragmentManager(), groupedOrdersLists);
                viewPager.setAdapter(adapter);
                switchToContentPage();
                activity.showSearchView();
                activity.showFilterMenuItem();
            } else {
                showNoDataIndicator("No items");
                activity.hideSearchView();
                activity.hideFilterMenuItem();
            }
        }catch (Exception e){e.printStackTrace();}
    }

    //----------------

    public void onPerformSearch(SearchDetails search_details){
        this.searchDetails = search_details;
        try {
            if ( searchDetails != null && searchDetails.isSearchable() ){//&& isShowingContentPage()) {
//                if ( appBarLayout != null ) {
//                    appBarLayout.expandToolbar();   // To Show Tablayout if its hided already
//                }
                OrdersFragment ordersFragment = (OrdersFragment) adapter.getCurrentFragment(viewPager);//viewPager.getShowingFragment(getSupportFragmentManager());
                ordersFragment.performSearch(searchDetails);
            }else{
                onClearSearch();
            }
        }catch (Exception e){ e.printStackTrace();  }
    }

    public void onClearSearch(){
        this.searchDetails = null;
        try {
            OrdersFragment ordersFragment = (OrdersFragment) adapter.getCurrentFragment(viewPager);//viewPager.getShowingFragment(getSupportFragmentManager());
            ordersFragment.clearSearch();
        }catch (Exception e){   e.printStackTrace();}
    }

    //---------

    public void collapseSearchView(){
        activity.collapseSearchView();
    }

}
