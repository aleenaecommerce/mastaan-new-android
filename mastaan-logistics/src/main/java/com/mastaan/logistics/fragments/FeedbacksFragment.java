package com.mastaan.logistics.fragments;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vAppCompatEditText;
import com.aleena.common.widgets.vEditText;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.FeedbacksActivity;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.activities.UnProcessedFeedbacksActivity;
import com.mastaan.logistics.adapters.FeedbacksAdapter;
import com.mastaan.logistics.backend.FeedbacksAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.OrderDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.methods.ActivityMethods;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.FeedbackDetails;
import com.mastaan.logistics.models.GroupedFeedbacksList;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 29/4/16.
 */

public class FeedbacksFragment extends MastaanFragment implements View.OnClickListener {

    MastaanToolbarActivity activity;
    String FEEDBACKS_TYPE = Constants.LOW_RATED_FEEDBACKS;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    vEditText date;
    vRecyclerView feedbacksHolder;
    FeedbacksAdapter feedbacksAdapter;
    List<FeedbackDetails> originalFeedbacksList;

    View markAllProcessed;

    SearchDetails searchDetails;

    UserDetails userDetails;

    public FeedbacksFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feedbacks, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();
        hasSwipeRefresh();
        activity = (MastaanToolbarActivity) getActivity();

        FEEDBACKS_TYPE = getArguments().getString("feedbacksType");

        userDetails = localStorageData.getUserDetails();

        //---------

        searchResultsCountIndicator = (FrameLayout) view.findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) view.findViewById(R.id.results_count);
        clearFilter = (Button) view.findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        markAllProcessed = view.findViewById(R.id.markAllProcessed);
        markAllProcessed.setOnClickListener(this);

        date = (vEditText) view.findViewById(R.id.date);
        date.setOnClickListener(this);
        feedbacksHolder = (vRecyclerView) view.findViewById(R.id.feedbacksHolder);
        setupHolder();

        //------

        if ( FEEDBACKS_TYPE.equalsIgnoreCase(Constants.PROCESSED_FEEDBACKS) ){
            date.setVisibility(View.VISIBLE);
            date.setText(DateMethods.getDateInFormat(localStorageData.getServerTime(), DateConstants.MMM_DD_YYYY));
        }else{
            date.setVisibility(View.GONE);
        }

        getFeedbacks();

    }

    public String getFEEDBACKS_TYPE() {
        return FEEDBACKS_TYPE;
    }

    public List<FeedbackDetails> getItemsList() {
        if ( originalFeedbacksList == null ){ return  new ArrayList<>();    }
        return originalFeedbacksList;
    }
    public List<FeedbackDetails> getShowingItemsList() {
        return feedbacksAdapter.getAllItems();
    }

    @Override
    public void onClick(View view) {
        if ( view == clearFilter ){
            if ( ActivityMethods.isFeedbacksActivity(activity) ) {
                ((FeedbacksActivity) activity).clearSelection();
            }else if ( ActivityMethods.isUnProcessedFeedbacksActivity(activity) ) {
                ((UnProcessedFeedbacksActivity) activity).clearSelection();
            }
            clearSearch();
        }
        else if ( view == markAllProcessed ){
            showChoiceSelectionDialog("Confirm!", "Do you want to mark all feedbacks of this order to processed?", "NO", "YES", new ChoiceSelectionCallback() {
                @Override
                public void onSelect(int choiceNo, String choiceName) {
                    if (choiceName.equalsIgnoreCase("YES")) {
                        completeAllFeedbacksForOrder();
                    }
                }
            });
        }
        else if ( view == date ){
            showDatePickerDialog("Processed date!", null, DateMethods.getCurrentDate(), date.getText().toString(), new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                    getFeedbacks();
                }
            });
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        if ( FEEDBACKS_TYPE.equalsIgnoreCase(Constants.GROUPED_FEEDBACKS) ){
            activity.onReloadPressed();
        }else {
            getFeedbacks();
        }
    }

    public void setupHolder() {

        feedbacksHolder.setHasFixedSize(true);
        feedbacksHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        feedbacksHolder.setItemAnimator(new DefaultItemAnimator());

        feedbacksAdapter = new FeedbacksAdapter(activity, new ArrayList<FeedbackDetails>(), ActivityMethods.isUnProcessedFeedbacksActivity(getActivity()) == true? true:false, new FeedbacksAdapter.Callback() {
            @Override
            public void onItemClick(final int position) {
                FeedbackDetails feedbackDetails = feedbacksAdapter.getItem(position);

                final List<String> optionsList = new ArrayList<>();
                if ( feedbackDetails.isCompleted() == false ) {
                    if (feedbackDetails.getProssesorID() != null && feedbackDetails.getProssesorID().length() > 0) {
                        optionsList.add(Constants.ADD_COMMENT);
                        optionsList.add(Constants.MARK_AS_PROCESSED);
                    }else{
                        optionsList.add(Constants.CLAIM_FEEDBACK);
                    }
                }
                if ( feedbackDetails.isCompleted() && feedbackDetails.getCustomerRating() == 5 ){
                    optionsList.add(feedbackDetails.isTestimonial()?Constants.REMOVE_FROM_TESTIMONIALS:Constants.ADD_TO_TESTIMONIALS);
                }

                ListChooserCallback callback = new ListChooserCallback() {
                    @Override
                    public void onSelect(final int optionPosition) {
                        String selectedOption = optionsList.get(optionPosition);

                        if ( selectedOption.equalsIgnoreCase(Constants.CLAIM_FEEDBACK) ){
                            showChoiceSelectionDialog("Confirm!", "Do you want to claim feedback for processing?", "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if (choiceName.equalsIgnoreCase("YES")) {
                                        claimFeedback(feedbacksAdapter.getItem(position).getID());
                                    }
                                }
                            });
                        }

                        else if ( selectedOption.equalsIgnoreCase(Constants.ADD_COMMENT) ){
                            View feedbackAddCommentView = LayoutInflater.from(context).inflate(R.layout.dialog_feedback_add_comment, null);
                            final vAppCompatEditText feedback_comment = (vAppCompatEditText) feedbackAddCommentView.findViewById(R.id.feedback_comment);
                            feedback_comment.setText(feedbacksAdapter.getItem(position).getProcessorComments());

                            feedbackAddCommentView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    closeCustomDialog();
                                }
                            });

                            feedbackAddCommentView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String feedbackComment = feedback_comment.getText().toString();
                                    if (feedbackComment.length() > 0) {
                                        closeCustomDialog();
                                        addFeedbackComment(position, feedbackComment);
                                    } else {
                                        showToastMessage("* Please enter comments.");
                                    }
                                }
                            });
                            showCustomDialog(feedbackAddCommentView);
                        }

                        else if ( selectedOption.equalsIgnoreCase(Constants.MARK_AS_PROCESSED) ){
                            completeFeedback(position);
                        }

                        else if ( selectedOption.equalsIgnoreCase(Constants.ADD_TO_TESTIMONIALS) ){
                            showChoiceSelectionDialog("Confirm", "Are you sure to add the feedback to testimonials?", "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if (choiceName.equalsIgnoreCase("YES")) {
                                        addToTestimonials(position);
                                    }
                                }
                            });
                        }

                        else if ( selectedOption.equalsIgnoreCase(Constants.REMOVE_FROM_TESTIMONIALS) ){
                            removeFromTestimonials(position);
                        }
                    }
                };
                if ( optionsList.size() > 0 ){
                    if ( optionsList.size() == 1 && optionsList.get(0).equalsIgnoreCase(Constants.CLAIM_FEEDBACK) ){
                        callback.onSelect(0);
                    }else{
                        showListChooserDialog(optionsList, callback);
                    }
                }
            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails2 orderDetails = feedbacksAdapter.getItem(position).getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails2 orderDetails = feedbacksAdapter.getItem(position).getOrderItemDetails().getOrderDetails();
                if ( orderDetails.isAlertnateMobileExists() ){
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile()+" (Primary)", orderDetails.getCustomerAlternateMobile()+" (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition==1?orderDetails.getCustomerAlternateMobile():orderDetails.getCustomerMobile());
                        }
                    });
                }else{
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowOrderDetails(int position) {
                new OrderDetailsDialog(activity).show(feedbacksAdapter.getItem(position).getOrderItemDetails().getOrderID());
            }

            @Override
            public void onShowCustomerOrderHistory(int position) {
                new BuyerOptionsDialog(activity).show(feedbacksAdapter.getItem(position));
            }

            @Override
            public void onShowStockDetails(int position) {
                new StockDetailsDialog(activity).show(feedbacksAdapter.getItem(position).getOrderItemDetails());
            }
        });
        if ( userDetails.isCustomerRelationshipManager() || userDetails.isDeliveryBoyManager() || userDetails.isProcessingManager() || userDetails.isOrdersManager() ){
            feedbacksAdapter.showMessageCustomerOption();
        }

        feedbacksHolder.setAdapter(feedbacksAdapter);
    }

    public long getItemsCount() {
        return feedbacksAdapter.getItemCount();
    }

    //---------------

    public void getFeedbacks(){

        hideMenuOptions();
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( ActivityMethods.isFeedbacksActivity(activity) ) {
            ((FeedbacksActivity) activity).clearSelection();
        }else if ( ActivityMethods.isUnProcessedFeedbacksActivity(activity) ) {
            ((UnProcessedFeedbacksActivity) activity).clearSelection();
        }
        originalFeedbacksList = new ArrayList<>();

        if ( FEEDBACKS_TYPE.equalsIgnoreCase(Constants.LOW_RATED_FEEDBACKS) ){
            showLoadingIndicator("Loading low rated feedbacks, wait...");
            getBackendAPIs().getFeedbacksAPI().getLowRatedFeedbacks(new FeedbacksAPI.FeedbacksCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<FeedbackDetails> feedbacksList) {
                    if ( status ){
                        if ( feedbacksList.size() > 0 ){
                            originalFeedbacksList = feedbacksList;
                            feedbacksAdapter.setItems(feedbacksList);
                            showMenuOptions();
                            switchToContentPage();
                        }else{
                            showNoDataIndicator("No low rated feedbacks");
                        }
                    }else{
                        showReloadIndicator(statusCode, "Unable to load low rated feedbacks, try again!");
                    }
                }
            });
        }
        else if ( FEEDBACKS_TYPE.equalsIgnoreCase(Constants.AVERAGE_RATED_FEEDBACKS) ){
            showLoadingIndicator("Loading average rated feedbacks, wait...");
            getBackendAPIs().getFeedbacksAPI().getAverageRatedFeedbacks(new FeedbacksAPI.FeedbacksCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<FeedbackDetails> feedbacksList) {
                    if (status) {
                        if (feedbacksList.size() > 0) {
                            originalFeedbacksList = feedbacksList;
                            feedbacksAdapter.setItems(feedbacksList);
                            showMenuOptions();
                            switchToContentPage();
                        } else {
                            showNoDataIndicator("No average rated feedbacks");
                        }
                    } else {
                        showReloadIndicator(statusCode, "Unable to load average rated feedbacks, try again!");
                    }
                }
            });
        }
        else if ( FEEDBACKS_TYPE.equalsIgnoreCase(Constants.HIGH_RATED_FEEDBACKS) ){
            showLoadingIndicator("Loading high rated feedbacks, wait...");
            getBackendAPIs().getFeedbacksAPI().getHighRatedFeedbacks(new FeedbacksAPI.FeedbacksCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<FeedbackDetails> feedbacksList) {
                    if (status) {
                        if (feedbacksList.size() > 0) {
                            originalFeedbacksList = feedbacksList;
                            feedbacksAdapter.setItems(feedbacksList);
                            showMenuOptions();
                            switchToContentPage();
                        } else {
                            showNoDataIndicator("No high rated feedbacks");
                        }
                    } else {
                        showReloadIndicator(statusCode, "Unable to load high rated feedbacks, try again!");
                    }
                }
            });
        }
        else if ( FEEDBACKS_TYPE.equalsIgnoreCase(Constants.PROCESSING_FEEDBACKS) ){
            showLoadingIndicator("Loading processing feedbacks, wait...");
            getBackendAPIs().getFeedbacksAPI().getProcessingFeedbacks(new FeedbacksAPI.FeedbacksCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<FeedbackDetails> feedbacksList) {
                    if (status) {
                        if (feedbacksList.size() > 0) {
                            originalFeedbacksList = feedbacksList;
                            feedbacksAdapter.setItems(feedbacksList);
                            showMenuOptions();
                            switchToContentPage();
                        } else {
                            showNoDataIndicator("No processing feedbacks");
                        }
                    } else {
                        showReloadIndicator(statusCode, "Unable to load processing feedbacks, try again!");
                    }
                }
            });
        }
        else if ( FEEDBACKS_TYPE.equalsIgnoreCase(Constants.PROCESSED_FEEDBACKS) ){
            String forDate = date.getText().toString();
            if ( forDate == null || forDate.length() == 0  ){
                forDate = DateMethods.getDateInFormat(localStorageData.getServerTime(), DateConstants.MMM_DD_YYYY);
            }
            date.setText(forDate);

            date.setVisibility(View.GONE);
            showLoadingIndicator("Loading processed feedbacks on "+date.getText().toString()+", wait...");
            getBackendAPIs().getFeedbacksAPI().getProcessedFeedbacksForDate(forDate, new FeedbacksAPI.FeedbacksCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<FeedbackDetails> feedbacksList) {
                    date.setVisibility(View.VISIBLE);
                    if (status) {
                        if (feedbacksList.size() > 0) {
                            originalFeedbacksList = feedbacksList;
                            feedbacksAdapter.setItems(feedbacksList);
                            showMenuOptions();
                            switchToContentPage();
                        } else {
                            showNoDataIndicator("No processed feedbacks on "+date.getText().toString());
                        }
                    } else {
                        showReloadIndicator(statusCode, "Unable to load processed feedbacks on "+date.getText().toString()+", try again!");
                    }
                }
            });


        }
        else if ( FEEDBACKS_TYPE.equalsIgnoreCase(Constants.GROUPED_FEEDBACKS) ){

            markAllProcessed.setVisibility(View.GONE);
            hideMenuOptions();
            searchResultsCountIndicator.setVisibility(View.GONE);
            originalFeedbacksList = new ArrayList<>();
            showLoadingIndicator("Loading feedbacks, wait...");
            new AsyncTask<Void, Void, Void>() {
                List<FeedbackDetails> feedbacksList = new ArrayList<FeedbackDetails>();
                @Override
                protected Void doInBackground(Void... params) {
                    feedbacksList = ((UnProcessedFeedbacksActivity)activity).getOrderFeedbacks(getArguments().getString("orderID"));
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    originalFeedbacksList = feedbacksList;
                    //Log.d(Constants.LOG_TAG, "ID: "+getArguments().getString("orderID")+", with Size: "+feedbacksList.size());
                    if ( feedbacksList.size() > 0 ) {
                        feedbacksAdapter.setItems(feedbacksList);
                        markAllProcessed.setVisibility(View.VISIBLE);
                        showMenuOptions();
                        switchToContentPage();
                    }else{
                        markAllProcessed.setVisibility(View.GONE);
                        showNoDataIndicator("No items");
                    }
                }
            }.execute();
        }
    }

    public void claimFeedback(final String feedbackID){

        showLoadingDialog("Claiming feedback for processing, wait...");
        getBackendAPIs().getFeedbacksAPI().claimFeedback(feedbackID, new FeedbacksAPI.FeedbacksCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<FeedbackDetails> feedbacksList) {
                if (status) {
                    new AsyncTask<Void, Void, Void>() {
                        String unProcessedFeedbacksJSONS = "";

                        @Override
                        protected Void doInBackground(Void... params) {
                            unProcessedFeedbacksJSONS = new Gson().toJson(new GroupedFeedbacksList("", "", feedbacksList));
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            Intent unprocessedFeedbacksAct = new Intent(context, UnProcessedFeedbacksActivity.class);
                            unprocessedFeedbacksAct.putExtra("unProcessedFeedbacks", unProcessedFeedbacksJSONS);
                            startActivity(unprocessedFeedbacksAct);
                            activity.finish();
                            //closeLoadingDialog();
                        }
                    }.execute();

                } else {
                    closeLoadingDialog();
                    showChoiceSelectionDialog("Failure!", "Something went wrong while claiming feedback for processing.\nDo you want to try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                claimFeedback(feedbackID);
                            }
                        }
                    });
                }
            }
        });
    }

    public void addFeedbackComment(final int position, final String feedbackComment){

        showLoadingDialog("Adding comments, wait...");
        getBackendAPIs().getFeedbacksAPI().addFeedbackResolutionComment(feedbacksAdapter.getItem(position).getID(), feedbackComment, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    showToastMessage("Comments added successfully.");
                    feedbacksAdapter.addFeedbackComment(position, feedbackComment);
                } else {
                    showChoiceSelectionDialog(false, "Failure!", "Unable to add feedback comments.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                addFeedbackComment(position, feedbackComment);
                            }
                        }
                    });
                }
            }
        });
    }

    public void completeFeedback(final int position){

        showLoadingDialog("Marking feedback as processed, wait...");
        getBackendAPIs().getFeedbacksAPI().completeFeedback(feedbacksAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    showToastMessage("Feedback marked as processed.");
                    deleteFromList(feedbacksAdapter.getItem(position).getID());
                } else {
                    showChoiceSelectionDialog(false, "Failure!", "Unable to mark feedback as processed.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                completeFeedback(position);
                            }
                        }
                    });
                }
            }
        });
    }

    public void addToTestimonials(final int position){

        showLoadingDialog("Adding to testimonials, wait...");
        getBackendAPIs().getFeedbacksAPI().addToTestimonials(feedbacksAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    showToastMessage("Feedback added to testimonials");
                    feedbacksAdapter.setTestimonial(position, true);
                } else {
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }

    public void removeFromTestimonials(final int position){

        showLoadingDialog("Removing from testimonials, wait...");
        getBackendAPIs().getFeedbacksAPI().removeFromTestimonials(feedbacksAdapter.getItem(position).getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                closeLoadingDialog();
                if (status) {
                    showToastMessage("Feedback removed from testimonials");
                    feedbacksAdapter.setTestimonial(position, false);
                } else {
                    showToastMessage("Something went wrong, try again!");
                }
            }
        });
    }


    public void completeAllFeedbacksForOrder(){

        if ( feedbacksAdapter.getItemCount() > 0 ){
            showLoadingDialog("Marking all feedbacks to processed, wait...");
            getBackendAPIs().getFeedbacksAPI().completeAllFeedbacksForOrder(feedbacksAdapter.getItem(0).getOrderItemDetails().getOrderID(), new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message) {
                    closeLoadingDialog();
                    if (status) {
                        showToastMessage("Successfully marked all feedbacks to processed.");
                        feedbacksAdapter.clearItems();
                        originalFeedbacksList.clear();
                        markAllProcessed.setVisibility(View.GONE);
                        showNoDataIndicator("No items");

                        if ( ActivityMethods.isUnProcessedFeedbacksActivity(activity)) {
                            UnProcessedFeedbacksActivity unProcessedFeedbacksActivity = (UnProcessedFeedbacksActivity) activity;
                            unProcessedFeedbacksActivity.switchToNextTab();
                            unProcessedFeedbacksActivity.checkFeedbacksCount();
                        }
                    } else {
                        showChoiceSelectionDialog(false, "Failure!", "Unable to mark all feedbacks of this order to processed.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                    completeAllFeedbacksForOrder();
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    //--------------

    public void deleteFromList(final String id){

        feedbacksAdapter.deleteItem(id);   // Removing From Adapter
        for (int i=0;i<originalFeedbacksList.size();i++){
            if ( originalFeedbacksList.get(i).getID().equals(id) ){
                originalFeedbacksList.remove(i);        // Removing ItemsList
                break;
            }
        }

        if ( searchResultsCountIndicator.getVisibility() == View.VISIBLE ){
            if ( feedbacksAdapter.getItemCount() > 0 ) {
                results_count.setText(feedbacksAdapter.getItemCount() + " result(s) found.\n(" + searchDetails.getCustomerName() + ")");
            }else{
                results_count.setText("No Results");
            }
        }

        if ( originalFeedbacksList.size() == 0 ){
            searchResultsCountIndicator.setVisibility(View.GONE);
            showNoDataIndicator("No items");
            if ( ActivityMethods.isUnProcessedFeedbacksActivity(getActivity())){
                markAllProcessed.setVisibility(View.GONE);
                UnProcessedFeedbacksActivity unProcessedFeedbacksActivity = (UnProcessedFeedbacksActivity) activity;
                unProcessedFeedbacksActivity.switchToNextTab();
                unProcessedFeedbacksActivity.checkFeedbacksCount();
            }
        }
    }

    //----------------

    public void performSearch(final SearchDetails searchDetails){
        this.searchDetails = searchDetails;

        if ( isShowingLoadingPage() == false && originalFeedbacksList.size() > 0 ) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
                SearchMethods.searchFeedbacks(originalFeedbacksList, searchDetails, new SearchMethods.SearchFeedbacksCallback() {
                    @Override
                    public void onComplete(boolean status, SearchDetails search_details, List<FeedbackDetails> filteredList) {
                        if (status && searchDetails == search_details ) {
                            if (filteredList.size() > 0) {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText(filteredList.size() + " result(s) found.\n(" + searchDetails.getSearchString() + ")");

                                feedbacksAdapter.setItems(filteredList);
                                switchToContentPage();
                            } else {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText("No Results");
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");

                                feedbacksAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                feedbacksAdapter.setItems(originalFeedbacksList);
            }
        }

    }

    public void clearSearch(){
        searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        feedbacksAdapter.setItems(originalFeedbacksList);
        switchToContentPage();
        if (feedbacksAdapter.getItemCount() == 0) {
            showNoDataIndicator("No items");
        }
    }

    public boolean isShowingSearchIndicator(){
        if ( searchResultsCountIndicator.getVisibility() == View.VISIBLE ){
            return true;
        }else{
            return false;
        }
    }

    //------

    public void hideMenuOptions(){
        if ( ActivityMethods.isFeedbacksActivity(activity)){
            ((FeedbacksActivity)activity).hideMenuOptions(FEEDBACKS_TYPE);
        }else if ( ActivityMethods.isUnProcessedFeedbacksActivity(activity)){
            ((UnProcessedFeedbacksActivity)activity).hideMenuOptions(FEEDBACKS_TYPE);
        }
    }

    public void showMenuOptions(){
        if ( ActivityMethods.isFeedbacksActivity(activity)){
            ((FeedbacksActivity)activity).showMenuOptions(FEEDBACKS_TYPE);
        }else if ( ActivityMethods.isUnProcessedFeedbacksActivity(activity)){
            ((UnProcessedFeedbacksActivity)activity).showMenuOptions(FEEDBACKS_TYPE);
        }
    }

}