package com.mastaan.logistics.fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.google.gson.Gson;
import com.mastaan.logistics.BuildConfig;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.BuyersActivity;
import com.mastaan.logistics.activities.CashNCarryByDateActivity;
import com.mastaan.logistics.activities.CategoriesActivity;
import com.mastaan.logistics.activities.ConfigurationsActivity;
import com.mastaan.logistics.activities.DeliveryAreasActivity;
import com.mastaan.logistics.activities.DeliveryBoysActivity;
import com.mastaan.logistics.activities.DeliveryDatesActivity;
import com.mastaan.logistics.activities.DeliverySlotsActivity;
import com.mastaan.logistics.activities.EmployeeBonusesActivity;
import com.mastaan.logistics.activities.FeedbacksActivity;
import com.mastaan.logistics.activities.FeedbacksByDateActivity;
import com.mastaan.logistics.activities.FollowupsActivity;
import com.mastaan.logistics.activities.HousingSocietiesActivity;
import com.mastaan.logistics.activities.HubsActivity;
import com.mastaan.logistics.activities.KMLogActivity;
import com.mastaan.logistics.activities.MeatItemsActivity;
import com.mastaan.logistics.activities.MoneyCollectionActivity;
import com.mastaan.logistics.activities.OrdersByDateActivity;
import com.mastaan.logistics.activities.PastPendingOrdersActivity;
import com.mastaan.logistics.activities.ProfileActivity;
import com.mastaan.logistics.activities.QRCodesActivity;
import com.mastaan.logistics.activities.ReportCouponCodesActivity;
import com.mastaan.logistics.activities.ReportDaywiseOrdersActivity;
import com.mastaan.logistics.activities.ReportInstallSourcesActivity;
import com.mastaan.logistics.activities.ReportMeatItemAvailabilityRequestsActivity;
import com.mastaan.logistics.activities.ReportReferralsActivity;
import com.mastaan.logistics.activities.ReportTopBuyersActivity;
import com.mastaan.logistics.activities.SettingsActivity;
import com.mastaan.logistics.activities.StockDashboardActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.models.BuyerSearchDetails;
import com.mastaan.logistics.models.SidebarItemDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.WarehouseDetails;

import java.util.ArrayList;
import java.util.List;


public class SideBarFragment extends MastaanFragment {

    UserDetails userDetails;

    LinearLayout itemsHolder;
    TextView version_name;
    TextView warehouse;
    Callback mCallback;

    List<SidebarItemDetails> sidebarItems = new ArrayList<>();

    public interface Callback {
        void onItemSelected(int position, String item_name);
    }

    public SideBarFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sidebar, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userDetails = localStorageData.getUserDetails();

        //--------

        itemsHolder = view.findViewById(R.id.itemsHolder);
        version_name = view.findViewById(R.id.version_name);
        warehouse = view.findViewById(R.id.warehouse);
        warehouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final List<WarehouseDetails> warehouses = localStorageData.getWarehousesList();
                List<String> warehousesStrings = new ArrayList<>();
                for (int i=0;i<warehouses.size();i++){
                    if ( warehouses.get(i) != null ) {
                        warehousesStrings.add(warehouses.get(i).getName());
                    }
                }

                showListChooserDialog("Select Warehouse", warehousesStrings, new ListChooserCallback() {
                    @Override
                    public void onSelect(final int position) {
                        showLoadingDialog("Changing warehouse, wait...");
                        getBackendAPIs().getCommonAPI().changeWarehouse(warehouses.get(position).getID(), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                closeLoadingDialog();
                                if ( status ){
                                    localStorageData.storeWarehouse(warehouses.get(position));
                                    displayWarehouseDetails();
                                    getOrderItemsDatabase().deleteAllOrderItems();
                                    showToastMessage("Warehouse changed successfully");
                                }
                                else{
                                    showToastMessage("Something went wrong, try again!");
                                }
                            }
                        });
                    }
                });
            }
        });

        //--------

        version_name.setText("v" + BuildConfig.VERSION_NAME);


        displaySideBarItems();
        displayWarehouseDetails();

    }

    public void displaySideBarItems() {
        sidebarItems.add(new SidebarItemDetails(Constants.PROFILE, R.drawable.ic_profile));
        if ( userDetails.isDeliveryBoy() ) {
            sidebarItems.add(new SidebarItemDetails(Constants.KM_DIARY, R.drawable.ic_kmlog));
        }
        if ( userDetails.isStockManager() || userDetails.isCustomerRelationshipManager() ) {
            sidebarItems.add(new SidebarItemDetails(Constants.STOCK, R.drawable.ic_store_24_hour_grey));
        }
        if ( userDetails.isStockManager() || userDetails.isProcessingManager() || userDetails.isWarehouseManager() ) {
            sidebarItems.add(new SidebarItemDetails(Constants.CATEGORIES, R.drawable.ic_file_tree_grey));
        }
        if ( userDetails.isStockManager() || userDetails.isProcessingManager() || userDetails.isWarehouseManager() || userDetails.isCustomerRelationshipManager() ) {
            sidebarItems.add(new SidebarItemDetails(Constants.MEAT_ITEMS, R.drawable.ic_fish_grey));
        }
        if ( userDetails.isWarehouseManager() || userDetails.isStockManager() || userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() ) {
            sidebarItems.add(new SidebarItemDetails(Constants.DELIVERY_DATES, R.drawable.ic_calendar));
        }
        if ( userDetails.isWarehouseManager() ) {
            sidebarItems.add(new SidebarItemDetails(Constants.DELIVERY_SLOTS, R.drawable.ic_calendar));
        }
        if ( userDetails.isOrdersManager() ) {
            sidebarItems.add(new SidebarItemDetails(Constants.MONEY_COLLECTION, R.drawable.ic_cash_grey));
            sidebarItems.add(new SidebarItemDetails(Constants.PAST_PENDING_ORDES, R.drawable.ic_history));
            sidebarItems.add(new SidebarItemDetails(Constants.EMPLOYEE_BONUSES, R.drawable.ic_gift_grey));
        }
        sidebarItems.add(new SidebarItemDetails(Constants.DELIVERY_BOYS, R.drawable.ic_people_grey));
        if ( userDetails.isCustomerRelationshipManager() ) {
            sidebarItems.add(new SidebarItemDetails(Constants.FEEDBACKS, R.drawable.ic_flower_grey));
            sidebarItems.add(new SidebarItemDetails(Constants.FOLLOWUPS, R.drawable.ic_flower_grey));
        }
        if ( userDetails.isSuperUser() ){
            sidebarItems.add(new SidebarItemDetails(Constants.HUBS, R.drawable.ic_map_pointer_grey));
        }
        if ( userDetails.isDeliveryBoyManager() || userDetails.isOrdersManager() ){
            sidebarItems.add(new SidebarItemDetails(Constants.DELIVERY_AREAS, R.drawable.ic_map_pointer_grey));
            sidebarItems.add(new SidebarItemDetails(Constants.HOUSING_SOCIETIES, R.drawable.ic_map_pointer_grey));
        }
        if ( userDetails.isWarehouseManager() || userDetails.isDeliveryBoyManager() || userDetails.isProcessingManager() || userDetails.isOrdersManager() ){
            sidebarItems.add(new SidebarItemDetails(Constants.CASH_N_CARRY_BY_DATE, R.drawable.ic_history));
            sidebarItems.add(new SidebarItemDetails(Constants.ORDERS_BY_DATE, R.drawable.ic_history));
            sidebarItems.add(new SidebarItemDetails(Constants.FEEDBACKS_BY_DATE, R.drawable.ic_flower_grey));
            sidebarItems.add(new SidebarItemDetails(Constants.REPORTS, R.drawable.ic_report_grey));
            sidebarItems.add(new SidebarItemDetails(Constants.BUYERS, R.drawable.ic_people_grey));
        }
        sidebarItems.add(new SidebarItemDetails(Constants.QR_CODES, R.drawable.ic_qr_code_grey));
        if ( userDetails.isSuperUser() ) {
            sidebarItems.add(new SidebarItemDetails(Constants.CONFIGURATIONS, R.drawable.ic_settings));
        }
        sidebarItems.add(new SidebarItemDetails(Constants.SETTINGS, R.drawable.ic_settings));
        //sidebarItems.add(new SidebarItemDetails(Html.fromHtml(Constants.LOGOUT+" (<b>" + localStorageData.getUserName() + "</b>)"), R.drawable.ic_logout));

        for (int i = 0; i < sidebarItems.size(); i++) {
            final int position = i;
            View itemView = inflater.inflate(R.layout.view_sidebar_item, null, false);
            itemsHolder.addView(itemView);

            ImageView image = itemView.findViewById(R.id.image);
            image.setBackgroundResource((int) sidebarItems.get(position).getIcon());
            TextView title = itemView.findViewById(R.id.title);
            title.setText(sidebarItems.get(position).getName());

            Button itemSelector = itemView.findViewById(R.id.itemSelector);
            itemSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSideBarItemSelected(position);
                }
            });
        }
    }

    public void displayWarehouseDetails(){
        WarehouseDetails warehouseDetails = localStorageData.getWarehouse();
        warehouse.setText(warehouseDetails.getName());
    }

    public void onSideBarItemSelected(int position) {

        String selectedItem = sidebarItems.get(position).getName().toString();
        if (mCallback != null){
            mCallback.onItemSelected(position, selectedItem);
        }

        if (selectedItem.equalsIgnoreCase(Constants.PROFILE)) {
            Intent profileAct = new Intent(context, ProfileActivity.class);
            startActivity(profileAct);
        }
        else if (selectedItem.equalsIgnoreCase(Constants.KM_DIARY)) {
            Intent kmDiaryAct = new Intent(context, KMLogActivity.class);
            startActivity(kmDiaryAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.DELIVERY_BOYS) ){
            Intent deliveryBoysAct = new Intent(context, DeliveryBoysActivity.class);
            startActivity(deliveryBoysAct);
        }
        else if (selectedItem.equalsIgnoreCase(Constants.STOCK)) {
            if ( userDetails.isStockManager() ) {
                Intent stockAct = new Intent(context, StockDashboardActivity.class);
                startActivity(stockAct);
            }else{
                final String []list = new String[]{"All", "Available", "Unavailable"};
                showListChooserDialog("Stocks", list, new ListChooserCallback() {
                    @Override
                    public void onSelect(int position) {
                        Intent meatItemsAct = new Intent(context, MeatItemsActivity.class);
                        meatItemsAct.putExtra(Constants.FOR_STOCK, true);

                        String title = "Stocks - Warehouse";
                        if ( list[position].equalsIgnoreCase("Available") ){
                            title = "Available "+title;
                            meatItemsAct.putExtra(Constants.AVAILABILITY, "true");
                        }else if ( list[position].equalsIgnoreCase("Unavailable") ){
                            title = "Unavailable "+title;
                            meatItemsAct.putExtra(Constants.AVAILABILITY, "false");
                        }

                        meatItemsAct.putExtra(Constants.TITLE, title);
                        startActivity(meatItemsAct);
                    }
                });
            }
        }
        else if (selectedItem.equalsIgnoreCase(Constants.CATEGORIES)) {
            Intent categoriesAct = new Intent(context, CategoriesActivity.class);
            startActivity(categoriesAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.MEAT_ITEMS) ){
            final String []list = new String[]{"All", "Available", "Unavailable"};
            showListChooserDialog("Meat Items", list, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    Intent meatItemsAct = new Intent(context, MeatItemsActivity.class);
                    if ( list[position].equalsIgnoreCase("Available") ){
                        meatItemsAct.putExtra(Constants.TITLE, "Available Meat Items");
                        meatItemsAct.putExtra(Constants.AVAILABILITY, "true");
                    }else if ( list[position].equalsIgnoreCase("Unavailable") ){
                        meatItemsAct.putExtra(Constants.TITLE, "Unavailable Meat Items");
                        meatItemsAct.putExtra(Constants.AVAILABILITY, "false");
                    }
                    startActivity(meatItemsAct);
                }
            });
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.DELIVERY_DATES) ){
            Intent deliveryDatesAct = new Intent(context, DeliveryDatesActivity.class);
            startActivity(deliveryDatesAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.DELIVERY_SLOTS) ){
            Intent deliverySlotsAct = new Intent(context, DeliverySlotsActivity.class);
            startActivity(deliverySlotsAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.MONEY_COLLECTION) ){
            showListChooserDialog(null, new String[]{Constants.MONEY_COLLECTION, Constants.MONEY_CONSOLIDATION, Constants.PENDING_CONSOLIDATION}, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( position == 0 ){
                        Intent moneyCollectionAct = new Intent(context, MoneyCollectionActivity.class);
                        moneyCollectionAct.putExtra(Constants.ACTION_TYPE, Constants.MONEY_COLLECTION_FOR_DATE);
                        startActivity(moneyCollectionAct);
                    }
                    else if ( position == 1 ){
                        Intent moneyCollectionAct = new Intent(context, MoneyCollectionActivity.class);
                        moneyCollectionAct.putExtra(Constants.ACTION_TYPE, Constants.MONEY_CONSOLIDATION);
                        startActivity(moneyCollectionAct);
                    }
                    else if ( position == 2 ){
                        Intent moneyCollectionAct = new Intent(context, MoneyCollectionActivity.class);
                        moneyCollectionAct.putExtra(Constants.ACTION_TYPE, Constants.PENDING_CONSOLIDATION);
                        startActivity(moneyCollectionAct);
                    }
                }
            });
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.EMPLOYEE_BONUSES) ){
            Intent employeeBonusesAct = new Intent(context, EmployeeBonusesActivity.class);
            startActivity(employeeBonusesAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.PAST_PENDING_ORDES) ){
            Intent pastPendingOrdersAct = new Intent(context, PastPendingOrdersActivity.class);
            startActivity(pastPendingOrdersAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.FEEDBACKS) ){
            Intent feedbacksAct = new Intent(context, FeedbacksActivity.class);
            startActivity(feedbacksAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.FOLLOWUPS) ){
            final String[] optionsList = new String[]{"All", "Resulted in order", "First order"};
            showListChooserDialog("Followups", optionsList, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( optionsList[position].equalsIgnoreCase("All") ){
                        Intent followupsAct = new Intent(context, FollowupsActivity.class);
                        startActivity(followupsAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Resulted in order") ){
                        Intent followupsAct = new Intent(context, FollowupsActivity.class);
                        followupsAct.putExtra(Constants.TITLE, "Order resulted followups");
                        followupsAct.putExtra(Constants.ORDER_RESULTED, true);
                        startActivity(followupsAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("First order") ){
                        Intent followupsAct = new Intent(context, FollowupsActivity.class);
                        followupsAct.putExtra(Constants.TITLE, "First order followups");
                        followupsAct.putExtra(Constants.FIRST_ORDER, true);
                        startActivity(followupsAct);
                    }
                }
            });
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.HUBS) ){
            Intent hubsAct = new Intent(context, HubsActivity.class);
            startActivity(hubsAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.DELIVERY_AREAS) ){
            Intent deliveryAreasAct = new Intent(context, DeliveryAreasActivity.class);
            startActivity(deliveryAreasAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.HOUSING_SOCIETIES) ){
            Intent housingSocietiesAct = new Intent(context, HousingSocietiesActivity.class);
            startActivity(housingSocietiesAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.CASH_N_CARRY_BY_DATE) ){
            Intent cashNCarryByDateAct = new Intent(context, CashNCarryByDateActivity.class);
            startActivity(cashNCarryByDateAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.ORDERS_BY_DATE) ){
            Intent ordersByDateAct = new Intent(context, OrdersByDateActivity.class);
            startActivity(ordersByDateAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.FEEDBACKS_BY_DATE) ){
            Intent feedbacksByDateAct = new Intent(context, FeedbacksByDateActivity.class);
            startActivity(feedbacksByDateAct);
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.REPORTS) ){
            final String[] optionsList = new String[]{"Buyer install sources", "Daywise orders", "Daywise first time orders", "Top buyers", "Coupon codes", "Referrals", "Meat Item Availability Requests"};
            showListChooserDialog("Select Report", optionsList, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( optionsList[position].equalsIgnoreCase("Buyer install sources") ){
                        Intent reportBuyerInstallSourcesAct = new Intent(context, ReportInstallSourcesActivity.class);
                        startActivity(reportBuyerInstallSourcesAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Daywise orders") ){
                        Intent reportFirstTimeOrdersAct = new Intent(context, ReportDaywiseOrdersActivity.class);
                        startActivity(reportFirstTimeOrdersAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Daywise first time orders") ){
                        Intent reportFirstTimeOrdersAct = new Intent(context, ReportDaywiseOrdersActivity.class);
                        reportFirstTimeOrdersAct.putExtra(Constants.FIRST_TIME_ORDERS, true);
                        startActivity(reportFirstTimeOrdersAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Top buyers") ){
                        Intent reportTopBuyersAct = new Intent(context, ReportTopBuyersActivity.class);
                        startActivity(reportTopBuyersAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Coupon codes") ){
                        Intent reportCouponCodesAct = new Intent(context, ReportCouponCodesActivity.class);
                        startActivity(reportCouponCodesAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Referrals") ){
                        Intent reportReferralsAct = new Intent(context, ReportReferralsActivity.class);
                        startActivity(reportReferralsAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Meat Item Availability Requests") ){
                        Intent reportMeatItemAvailabilityRequest = new Intent(context, ReportMeatItemAvailabilityRequestsActivity.class);
                        startActivity(reportMeatItemAvailabilityRequest);
                    }
                }
            });
        }
        else if ( selectedItem.equalsIgnoreCase(Constants.BUYERS) ){
            final String[] optionsList = new String[]{"All", "With outstanding", "Will order soon", "Not interested anymore"};
            showListChooserDialog("Buyers", optionsList, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( optionsList[position].equalsIgnoreCase("All") ){
                        Intent buyersAct = new Intent(context, BuyersActivity.class);
                        buyersAct.putExtra(Constants.ACTION_TYPE, Constants.BUYERS);
                        startActivity(buyersAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("With outstanding") ){
                        Intent buyersAct = new Intent(context, BuyersActivity.class);
                        buyersAct.putExtra(Constants.TITLE, "Outstanding buyers");
                        buyersAct.putExtra(Constants.SEARCH_DETAILS, new Gson().toJson(new BuyerSearchDetails().setOutstanding(true)));
                        startActivity(buyersAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Will order soon") ){
                        Intent buyersAct = new Intent(context, BuyersActivity.class);
                        buyersAct.putExtra(Constants.TITLE, "Will order soon");
                        buyersAct.putExtra(Constants.SEARCH_DETAILS, new Gson().toJson(new BuyerSearchDetails().setWillOrderSoon(true)));
                        startActivity(buyersAct);
                    }
                    else if ( optionsList[position].equalsIgnoreCase("Not interested anymore") ){
                        Intent buyersAct = new Intent(context, BuyersActivity.class);
                        buyersAct.putExtra(Constants.TITLE, "Not interested anymore");
                        buyersAct.putExtra(Constants.SEARCH_DETAILS, new Gson().toJson(new BuyerSearchDetails().setNotInterested(true)));
                        startActivity(buyersAct);
                    }
                }
            });
        }
        else if (selectedItem.contains(Constants.QR_CODES)) {
            Intent qrCodesAct = new Intent(context, QRCodesActivity.class);
            startActivity(qrCodesAct);
        }
        else if (selectedItem.contains(Constants.CONFIGURATIONS)) {
            Intent configurationsAct = new Intent(context, ConfigurationsActivity.class);
            startActivity(configurationsAct);
        }
        else if (selectedItem.contains(Constants.SETTINGS)) {
            Intent settingsAct = new Intent(context, SettingsActivity.class);
            startActivity(settingsAct);
        }
    }

    public void setSidebarItemSelectedListener(Callback mCallback) {
        this.mCallback = mCallback;
    }




}
