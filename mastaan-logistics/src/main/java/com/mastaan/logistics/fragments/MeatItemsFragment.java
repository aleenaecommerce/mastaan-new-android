package com.mastaan.logistics.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MeatItemDetailsActivity;
import com.mastaan.logistics.activities.MeatItemsActivity;
import com.mastaan.logistics.activities.StockDetailsActivity;
import com.mastaan.logistics.adapters.WarehouseMeatItemsAdapter;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.backend.models.RequestUpdateWarehouseMeatItemAvailability;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.AttributeOptionsSelectionDialog;
import com.mastaan.logistics.flows.UpdateMeatItemsAvailabilityFlow;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 29/4/16.
 */

public class MeatItemsFragment extends MastaanFragment implements View.OnClickListener {

    MeatItemsActivity activity;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    RadioGroup filterGroup;
    vRecyclerView meatItemsHolder;
    WarehouseMeatItemsAdapter warehouseMeatItemsAdapter;

    String searchQuery;
    String filterTag = "All";

    //GroupedMeatItemsList groupedMeatItemsList;
    String categoryName;
    List<WarehouseMeatItemDetails> meatItems = new ArrayList<>();
    List<String> subCategoryNames = new ArrayList<>();

    UserDetails userDetails;

    int selectedItemPosition = -1;

    public MeatItemsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_meat_items, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();
        hasSwipeRefresh();

        activity = (MeatItemsActivity) getActivity();

        userDetails = localStorageData.getUserDetails();

        //---------

        /*// ON SAVEDINSTANCE STATE
        if (savedInstanceState != null) {
            Log.d(Constants.LOG_TAG, ">>>>> MEAT_ITEMS_FRAGMENT: Called SavedInstanceState");
            String meatItemsListString = savedInstanceState.getString("meatItemsListString");
            groupedMeatItemsList = new Gson().fromJson(meatItemsListString, GroupedMeatItemsList.class);
        }
        // ON LAUNCH
        else {
            String meatItemsListString = getArguments().getString("meatItemsListString");
            groupedMeatItemsList = new Gson().fromJson(meatItemsListString, GroupedMeatItemsList.class);
        }*/

        categoryName = getArguments().getString("categoryName");
        meatItems = new ArrayList<>(activity.getCategoryMeatItems(categoryName));
        subCategoryNames = activity.getCategorySubCategories(categoryName);

        //---------

        searchResultsCountIndicator = view.findViewById(R.id.searchResultsCountIndicator);
        results_count = view.findViewById(R.id.results_count);
        clearFilter = view.findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        filterGroup = view.findViewById(R.id.filterGroup);

        meatItemsHolder = view.findViewById(R.id.meatItemsHolder);
        setupHolder();

        //---------

        if ( subCategoryNames != null && subCategoryNames.size() > 1 ){
            filterGroup.setVisibility(View.VISIBLE);
            final List<String> filterList = new ArrayList<>();
            filterList.add("All");
            for (int i=0;i<subCategoryNames.size();i++){
                filterList.add(subCategoryNames.get(i));
            }

            for (int i=0;i<filterList.size();i++){
                final RadioButton radioButton = new RadioButton(context);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                filterGroup.addView(radioButton, layoutParams);

                radioButton.setText(filterList.get(i));
                radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if ( isChecked ){
                            performSearch(searchQuery, radioButton.getText().toString());
                        }
                    }
                });
                if ( radioButton.getText().toString().equalsIgnoreCase("All") ){
                    radioButton.setChecked(true);
                }
            }
        }else{
            filterGroup.setVisibility(View.GONE);
        }

        //------

        warehouseMeatItemsAdapter.setItems(meatItems);
        switchToContentPage();

    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("meatItemsListString", new Gson().toJson(groupedMeatItemsList));
    }*/

    @Override
    public void onClick(View view) {
        if ( view == clearFilter ){
            activity.collapseSearchView();
            clearSearch();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        activity.onReloadPressed();
    }

    public void updateItemsAvailability(){
        List<WarehouseMeatItemDetails> enabledItems = new ArrayList<>();
        for (int i = 0; i< warehouseMeatItemsAdapter.getItemCount(); i++){
            if ( warehouseMeatItemsAdapter.getItem(i).isAvailable() ){
                enabledItems.add(warehouseMeatItemsAdapter.getItem(i));
            }
        }
        if ( enabledItems.size() > 0 ){
            UpdateMeatItemsAvailabilityFlow updateMeatItemsAvailabilityFlow = new UpdateMeatItemsAvailabilityFlow(activity);
            if ( categoryName.equalsIgnoreCase("chicken") ){
                updateMeatItemsAvailabilityFlow.hasPaperRateView();
            }
            updateMeatItemsAvailabilityFlow.start(enabledItems, new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message) {
                    if ( status ){
                        showToastMessage(message);
                    }
                    activity.onReloadPressed();
                }
            });
        }else{
            activity.showToastMessage("No enabled items");
        }
    }

    public void setupHolder() {

        meatItemsHolder.setHasFixedSize(true);
        meatItemsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        meatItemsHolder.setItemAnimator(new DefaultItemAnimator());

        warehouseMeatItemsAdapter = new WarehouseMeatItemsAdapter(context, new ArrayList<WarehouseMeatItemDetails>(), new WarehouseMeatItemsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
                selectedItemPosition = position;
                final WarehouseMeatItemDetails warehouseMeatItemDetails = warehouseMeatItemsAdapter.getItem(position);

                if ( activity.getCallingActivity() != null ){
                    if ( getArguments().getBoolean(Constants.SHOW_ITEM_LEVEL_ATTRIBUTES_OPTIONS_SELECTION, false) ){
                        new AttributeOptionsSelectionDialog(activity).show(warehouseMeatItemDetails.getMeatItemID(), true, new AttributeOptionsSelectionDialog.Callback() {
                            @Override
                            public void onComplete(List<AttributeOptionDetails> attributeOptions) {
                                Intent resultData = new Intent();
                                resultData.putExtra(Constants.WAREHOUSE_MEAT_ITEM_DETAILS, new Gson().toJson(warehouseMeatItemDetails));
                                resultData.putExtra(Constants.ATTRIBUTES_OPTIONS, new Gson().toJson(attributeOptions));
                                activity.setResult(Activity.RESULT_OK, resultData);
                                activity.finish();
                            }
                        });
                    }else{
                        Intent resultData = new Intent();
                        resultData.putExtra(Constants.WAREHOUSE_MEAT_ITEM_DETAILS, new Gson().toJson(warehouseMeatItemDetails));
                        activity.setResult(Activity.RESULT_OK, resultData);
                        activity.finish();
                    }
                }
                else {
                    if ( getArguments().getBoolean(Constants.FOR_STOCK, false) ){
                        if ( userDetails.isStockManager() ) {
                            Intent stockDetailsAct = new Intent(context, StockDetailsActivity.class);
                            stockDetailsAct.putExtra(Constants.NAME, "Stock - " + warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize());
                            if (getArguments().getString(Constants.HUB_DETAILS) != null && getArguments().getString(Constants.HUB_DETAILS).trim().length() > 0) {
                                stockDetailsAct.putExtra(Constants.HUB_DETAILS, getArguments().getString(Constants.HUB_DETAILS));
                            }
                            stockDetailsAct.putExtra(Constants.ID, warehouseMeatItemDetails.getID());
                            startActivityForResult(stockDetailsAct, Constants.STOCK_DETAILS_ACTIVITY_CODE);
                        }
                    }
                    else {
                        Intent meatItemDetailsAct = new Intent(context, MeatItemDetailsActivity.class);
                        meatItemDetailsAct.putExtra(Constants.NAME, warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize());
                        meatItemDetailsAct.putExtra(Constants.ID, warehouseMeatItemDetails.getID());//(Constants.WAREHOUSE_MEAT_ITEM_DETAILS, new Gson().toJson(warehouseMeatItemDetails));
                        startActivityForResult(meatItemDetailsAct, Constants.MEAT_ITEM_DETAILS_ACTIVITY_CODE);
                    }
                }
            }

            @Override
            public void onRefreshStockDetailsClick(final int position) {
                if ( getArguments().getBoolean(Constants.FOR_STOCK, false) ){
                    String hubID = "";
                    if ( getArguments().getString(Constants.HUB_DETAILS) != null  ){
                        hubID = new Gson().fromJson(getArguments().getString(Constants.HUB_DETAILS), HubDetails.class).getID();
                    }
                    showLoadingDialog("Refreshing stock details, wait...");
                    getBackendAPIs().getMeatItemsAPI().getWarehouseMeatItemDetails(warehouseMeatItemsAdapter.getItem(position).getID(), true, hubID, new MeatItemsAPI.WarehouseMeatItemCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemDetails warehouseMeatItemDetails) {
                            closeLoadingDialog();
                            if ( status ){
                                warehouseMeatItemsAdapter.setItem(position, warehouseMeatItemDetails);
                            }else{
                                showToastMessage("Unable to refresh stock details, try again!");
                            }
                        }
                    });
                }
            }

            @Override
            public void onShowMeatItemDetailsClick(int position) {
                selectedItemPosition = position;
                final WarehouseMeatItemDetails warehouseMeatItemDetails = warehouseMeatItemsAdapter.getItem(position);

                Intent meatItemDetailsAct = new Intent(context, MeatItemDetailsActivity.class);
                meatItemDetailsAct.putExtra(Constants.NAME, warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize());
                meatItemDetailsAct.putExtra(Constants.ID, warehouseMeatItemDetails.getID());//(Constants.WAREHOUSE_MEAT_ITEM_DETAILS, new Gson().toJson(warehouseMeatItemDetails));
                startActivityForResult(meatItemDetailsAct, Constants.MEAT_ITEM_DETAILS_ACTIVITY_CODE);
            }

            @Override
            public void onMarkAsAvailable(final int position) {
                if ( userDetails.isStockManager() || userDetails.isProcessingManager() ) {
                    WarehouseMeatItemDetails warehouseMeatItemDetails = warehouseMeatItemsAdapter.getItem(position);

                    showChoiceSelectionDialog("Confirm", CommonMethods.fromHtml("Are you sure to mark <b>" + warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize() + "</b> as <b>available</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES") ){
                                updateWarehouseMeatItemAvailability(position, true);
                            }
                        }
                    });
                }else{
                    showToastMessage("You are not allowed to perform this operation, please contact stock/processing manager");
                }
            }

            @Override
            public void onMarkAsUnavailable(final int position) {
                if ( userDetails.isStockManager() || userDetails.isProcessingManager() ) {
                    WarehouseMeatItemDetails warehouseMeatItemDetails = warehouseMeatItemsAdapter.getItem(position);

                    showChoiceSelectionDialog("Confirm", CommonMethods.fromHtml("Are you sure to mark <b>" + warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize() + "</b> as <b>unavailable</b>?"), "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES") ){
                                updateWarehouseMeatItemAvailability(position, false);
                            }
                        }
                    });
                }else{
                    showToastMessage("You are not allowed to perform this operation, please contact stock/processing manager");
                }
            }

            private void updateWarehouseMeatItemAvailability(final int position, final boolean availability){
                final WarehouseMeatItemDetails warehouseMeatItemDetails = warehouseMeatItemsAdapter.getItem(position);

                showLoadingDialog("Updating availability, wait...");
                getBackendAPIs().getMeatItemsAPI().updateWarehouseMeatItemAvailability(warehouseMeatItemDetails.getID(), new RequestUpdateWarehouseMeatItemAvailability(availability), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        closeLoadingDialog();
                        if ( status ){
                            showToastMessage("Avaialbility updated successfully");
                            warehouseMeatItemDetails.setAvailability(availability);
                            warehouseMeatItemsAdapter.setItem(position, warehouseMeatItemDetails);
                        }else{
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            }
        });
        if ( getArguments().getBoolean(Constants.FOR_STOCK, false)
                && (getArguments().getString(Constants.HUB_DETAILS) == null || getArguments().getString(Constants.HUB_DETAILS).trim().length() == 0 ) ) {
            warehouseMeatItemsAdapter.showAvailabilityActions();
        }

        meatItemsHolder.setAdapter(warehouseMeatItemsAdapter);
    }

    //----------------

    public void performSearch(String search_query){
        performSearch(search_query, filterTag);
    }

    private void performSearch(String search_query, String filterTag){
        if ( search_query != null ){ search_query = search_query.toLowerCase(); }
        this.searchQuery = search_query;
        this.filterTag = filterTag;

        List<WarehouseMeatItemDetails> filteredList = new ArrayList<>();

        if ( meatItems != null && meatItems.size() > 0 ){
            List<WarehouseMeatItemDetails> searchedList = new ArrayList<>();
            for (int i=0;i<meatItems.size();i++){
                WarehouseMeatItemDetails warehouseMeatItemDetails = meatItems.get(i);

                if ( searchQuery != null && searchQuery.length() > 0 ){
                    if ( (warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize() != null && warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize().toLowerCase().contains(searchQuery)) ){
                        searchedList.add(warehouseMeatItemDetails);
                    }
                }else{
                    searchedList.add(warehouseMeatItemDetails);
                }
            }

            //---- FILTERING

            if ( filterTag.equalsIgnoreCase("All") ){
                for (int i=0;i<searchedList.size();i++){
                    filteredList.add(searchedList.get(i));
                }
            }else{
                for (int i=0;i<searchedList.size();i++){
                    WarehouseMeatItemDetails warehouseMeatItemDetails = searchedList.get(i);
                    if ( warehouseMeatItemDetails.getMeatItemDetais().getCategoryDetails().getName().equals(filterTag) ){
                        filteredList.add(searchedList.get(i));
                    }
                }
            }
        }

        if ( filteredList.size() > 0 ){
            if ( searchQuery != null && searchQuery.length() > 0 ) {
                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                results_count.setText(filteredList.size() + " result(s) found.\n(" + searchQuery+" : "+filterTag + ")");
            }else{
                searchResultsCountIndicator.setVisibility(View.GONE);
            }
            warehouseMeatItemsAdapter.setItems(filteredList);
            meatItemsHolder.smoothScrollToPosition(0);
            switchToContentPage();
        }
        else{
            if ( searchQuery != null && searchQuery.length() > 0 ) {
                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                results_count.setText("No Results");
                showNoDataIndicator("No Results found for (" + searchQuery + " : " +filterTag + ")");
            }else{
                searchResultsCountIndicator.setVisibility(View.GONE);
            }
            warehouseMeatItemsAdapter.clearItems();
        }
    }

    public void clearSearch(){
        performSearch("", filterTag);
        /*this.searchDetails = "";
        this.filterTag = "All";
        if ( statusFilterGroup.getChildCount() > 0 ){
            RadioButton radioButton = (RadioButton) statusFilterGroup.getChildAt(0);
            radioButton.setChecked(true);
        }
        searchResultsCountIndicator.setVisibility(View.GONE);
        if ( groupedMeatItemsList.getItems().size() > 0 ) {
            warehouseMeatItemsAdapter.setItems(groupedMeatItemsList.getItems());
            switchToContentPage();
        }else{
            showNoDataIndicator("No items");
        }*/
    }

    //===========


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if ( requestCode == Constants.MEAT_ITEM_DETAILS_ACTIVITY_CODE && resultCode == Activity.RESULT_OK ){
                WarehouseMeatItemDetails warehouseMeatItemDetails = new Gson().fromJson(data.getStringExtra(Constants.WAREHOUSE_MEAT_ITEM_DETAILS), WarehouseMeatItemDetails.class);
                warehouseMeatItemsAdapter.setItem(selectedItemPosition, warehouseMeatItemDetails);

                for (int i=0;i<meatItems.size();i++){
                    if ( meatItems.get(i).getID().equals(warehouseMeatItemDetails.getID()) ){
                        meatItems.set(i, warehouseMeatItemDetails);
                        break;
                    }
                }
            }
            else if ( requestCode == Constants.STOCK_DETAILS_ACTIVITY_CODE && resultCode == Activity.RESULT_OK ){
                warehouseMeatItemsAdapter.getCallback().onRefreshStockDetailsClick(selectedItemPosition);
            }
        }catch (Exception e){}
    }
}