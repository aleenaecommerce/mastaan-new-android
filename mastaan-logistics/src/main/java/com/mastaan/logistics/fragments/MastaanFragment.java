package com.mastaan.logistics.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.aleena.common.fragments.vFragment;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.BackendAPIs;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.localdata.OrderItemsDatabase2;
import com.mastaan.logistics.methods.BroadcastReceiversMethods;
import com.mastaan.logistics.models.CustomPlace;

/**
 * Created by Naresh-Crypsis on 08-01-2016.
 */
public class MastaanFragment extends vFragment {

    Context context;
    LocalStorageData localStorageData;
    BroadcastReceiversMethods broadcastReceiversMethods;
    private BackendAPIs backendAPIs;

    public MastaanFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();

        localStorageData = new LocalStorageData(context);
        broadcastReceiversMethods = new BroadcastReceiversMethods(context);
    }

    public BackendAPIs getBackendAPIs() {
        if ( backendAPIs == null ){
            backendAPIs = new BackendAPIs(getActivity(), getDeviceID(), getAppVersionCode());
        }
        return backendAPIs;
    }

    public OrderItemsDatabase2 getOrderItemsDatabase() {
        return ((MastaanToolbarActivity)getActivity()).getOrderItemsDatabase();
    }

    protected void callToPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));

        if (ActivityCompat.checkSelfPermission(getActivity()
                ,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 1);

        }
        startActivity(intent);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission granted", Toast.LENGTH_SHORT).show();
                    ///call_action();
                } else {
                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public interface PlaceChooserCallback {
        void onSelect(CustomPlace place);
        void onCancel();
    }

}
