package com.mastaan.logistics.fragments;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.BaseOrderItemsActivity;
import com.mastaan.logistics.activities.DirectionsViewerHereMapsDialogActivity;
import com.mastaan.logistics.activities.DispatcherActivity;
import com.mastaan.logistics.activities.EditOrderItemActivity;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.activities.OrderDetailsActivity;
import com.mastaan.logistics.activities.TrackHereMapsLocationActivity;
import com.mastaan.logistics.adapters.OrderItemsAdapter;
import com.mastaan.logistics.backend.EmployeesAPI;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.ECODDialog;
import com.mastaan.logistics.dialogs.EditCounterSaleDialog;
import com.mastaan.logistics.dialogs.FollowupDetailsDialog;
import com.mastaan.logistics.dialogs.LabelPrinterDialog;
import com.mastaan.logistics.dialogs.MoneyCollectionDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.flows.AcknowledgeFlow;
import com.mastaan.logistics.flows.CustomerSupportFlow;
import com.mastaan.logistics.flows.DeliveryFlow;
import com.mastaan.logistics.flows.OrderItemActionsFlow;
import com.mastaan.logistics.flows.ProcessingFlow;
import com.mastaan.logistics.flows.RefundFlow;
import com.mastaan.logistics.flows.RoutesFlow;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.interfaces.OrderItemDetailsCallback;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.localdata.OrderItemsDatabase2;
import com.mastaan.logistics.methods.ActivityMethods;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.AttributeDetails;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.DeliveryAreaDetails;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.Job;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.UserDetailsMini;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;
import com.mastaan.logistics.printers.BillPrinter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 28/1/16.
 */
public class OrderItemsFragment extends MastaanFragment implements View.OnClickListener {

    OrderItemsDatabase2 orderItemsDatabase;

    BaseOrderItemsActivity activity;
    boolean isHasActions = true;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    vRecyclerView orderItemsHolder;
    OrderItemsAdapter orderItemsAdapter;
    String versionnum;
    SearchDetails searchDetails;

    //GroupedOrdersItemsList groupedOrdersItemsList;
    String categoryName;
    List<OrderItemDetails> orderItems = new ArrayList<>();

    UserDetails userDetails;
    //----------
    OrdersAPI.EditOrderItemCallback editOrderItemCallback;

    public OrderItemsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order_items, container, false);
    }

    /*@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("orderItemsListString", new Gson().toJson(groupedOrdersItemsList));
    }*/

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();
        hasSwipeRefresh();

        activity = (BaseOrderItemsActivity) getActivity();

        userDetails = localStorageData.getUserDetails();

        orderItemsDatabase = ((MastaanToolbarActivity) activity).getOrderItemsDatabase();//new OrderItemsDatabase2(context);
        //orderItemsDatabase.openDatabase();

        if (ActivityMethods.isDeliveryBoyDeliveriesActivity(getActivity())) {//ActivityMethods.isFutureOrdersActivity(getActivity()) || ActivityMethods.isOrderDetailsActivity(getActivity()) ){
            isHasActions = false;
        }

        /*
        // ON SAVEDINSTANCE STATE
        if ( savedInstanceState != null ){
            Log.d(Constants.LOG_TAG, ">>>>> ORDER_FRAGMENT: Called SavedInstanceState");
            String orderItemsListString = savedInstanceState.getString("orderItemsListString");
            groupedOrdersItemsList = new Gson().fromJson(orderItemsListString, GroupedOrdersItemsList.class);
        }
        // ON LAUNCH
        else{
            String orderItemsListString = getArguments().getString("orderItemsListString");
            groupedOrdersItemsList = new Gson().fromJson(orderItemsListString, GroupedOrdersItemsList.class);
        }*/

        categoryName = getArguments().getString("categoryName");
        orderItems = activity.getCategoryOrderItems(categoryName);

        //---------

        searchResultsCountIndicator = (FrameLayout) view.findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) view.findViewById(R.id.results_count);
        clearFilter = (Button) view.findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        orderItemsHolder = (vRecyclerView) view.findViewById(R.id.orderItemsHolder);
        setupHolder();

        //---------

        orderItemsAdapter.addItems(orderItems);//groupedOrdersItemsList.getItems());
        switchToContentPage();

    }

    @Override
    public void onClick(View view) {
        if (view == clearFilter) {
            activity.collapseSearchView();
            clearSearch();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        activity.onReloadPressed();
    }


    //-------------------------------------------------

    public void setupHolder() {

        orderItemsHolder.setHasFixedSize(true);
        orderItemsHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        orderItemsHolder.setItemAnimator(new DefaultItemAnimator());

        orderItemsAdapter = new OrderItemsAdapter(activity, new ArrayList<OrderItemDetails>(), new OrderItemsAdapter.Callback() {
            @Override
            public void onItemClick(int position) {
            }

            @Override
            public void onItemLongClick(int position) {
                OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
                //new OrderItemTimelineDialog(activity).show(orderItemsAdapter.getItem(position));
                showToastMessage("Delivery location (" + orderItemDetails.getOrderDetails().getDeliveryAddressLatLng().latitude + "," + orderItemDetails.getOrderDetails().getDeliveryAddressLatLng().longitude + ") is approximately " + CommonMethods.getInDecimalFormat(LatLngMethods.getDistanceBetweenLatLng(orderItemsAdapter.getItem(position).getOrderDetails().getDeliveryAddressLatLng(), localStorageData.getWarehouseLocation()) / 1000) + " kms from warehouse (" + localStorageData.getWarehouseLocation().latitude + "," + localStorageData.getWarehouseLocation().longitude + ")"
                        + "\n\n" + "Prepare by: " + DateMethods.getReadableDateFromUTC(orderItemDetails.getPrepareByDate()));
            }

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails2 orderDetails = orderItemsAdapter.getItem(position).getOrderDetails();
                if (orderDetails.isAlertnateMobileExists()) {
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile() + " (Primary)", orderDetails.getCustomerAlternateMobile() + " (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition == 1 ? orderDetails.getCustomerAlternateMobile() : orderDetails.getCustomerMobile());
                        }
                    });
                } else {
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails2 orderDetails = orderItemsAdapter.getItem(position).getOrderDetails();
                if (orderDetails.isAlertnateMobileExists()) {
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile() + " (Primary)", orderDetails.getCustomerAlternateMobile() + " (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition == 1 ? orderDetails.getCustomerAlternateMobile() : orderDetails.getCustomerMobile());
                        }
                    });
                } else {
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowDirections(final int position) {
                getCurrentLocation(new LocationCallback() {
                    @Override
                    public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                        if (status) {
                            Intent showDirectionsIntent = new Intent(context, DirectionsViewerHereMapsDialogActivity.class);
                            showDirectionsIntent.setAction(Constants.POPUP_ACTION);
                            showDirectionsIntent.putExtra("origin", latLng);
                            showDirectionsIntent.putExtra("destination", orderItemsAdapter.getItem(position).getOrderDetails().getDeliveryAddressLatLng());
                            showDirectionsIntent.putExtra("location", orderItemsAdapter.getItem(position).getOrderDetails().getCustomerName() + "\n" + orderItemsAdapter.getItem(position).getOrderDetails().getCustomerName()/*.getAddress().getDeliveryAddressLocality()*/);
                            startActivity(showDirectionsIntent);
                        } else {
                            showToastMessage(message);
                        }
                    }
                });
            }

            @Override
            public void onShowBuyerHistory(int position) {
                new BuyerOptionsDialog(activity).show(orderItemsAdapter.getItem(position));
            }

            @Override
            public void onShowReferralBuyerDetails(int position) {
                new BuyerOptionsDialog(activity).show(orderItemsAdapter.getItem(position).getOrderDetails().getReferralBuyer());
            }

            @Override
            public void onTrackOrder(final int position) {
                final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                showLoadingDialog("Tracking, wait...");
                getBackendAPIs().getEmployeesAPI().getDeliveryBoyCurrentJob(orderItemsAdapter.getItem(position).getDeliveryBoyID(), new EmployeesAPI.JobDetailsCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, Job jobDetails) {
                        closeLoadingDialog();
                        if (status) {
                            if (jobDetails != null && jobDetails.getOrderID() != null && jobDetails.getOrderID().equals(orderItemDetails.getOrderID())) {
                                UserDetails deliveryBoyDetails = orderItemDetails.getDeliveryBoyDetails();

                                Intent trackLocationAct = new Intent(context, TrackHereMapsLocationActivity.class);
                                trackLocationAct.putExtra(Constants.USER_ID, deliveryBoyDetails.getID());
                                trackLocationAct.putExtra(Constants.USER_NAME, deliveryBoyDetails.getAvailableName());
                                trackLocationAct.putExtra(Constants.JOB_DETAILS, new Gson().toJson(jobDetails));
                                startActivity(trackLocationAct);
                            } else {
                                showNotificationDialog("Status", Html.fromHtml("<b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getDeliveryBoyDetails().getAvailableName()) + "</b> not yet started <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "'s " + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize()) + "</b> delivery"));
                            }
                        } else {
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            }

            @Override
            public void onShowItemImage(int position) {
                View imageHolderView = LayoutInflater.from(context).inflate(R.layout.dialog_meat_item_image, null);
                ImageView image = (ImageView) imageHolderView.findViewById(R.id.image);
                Picasso.get()
                        .load(orderItemsAdapter.getItem(position).getMeatItemDetails().getPicture())
                        .placeholder(R.drawable.image_default)
                        .error(R.drawable.image_default)
                        .into(image);
                showCustomDialog(imageHolderView);
            }

            @Override
            public void onShowStockDetails(int position) {
                new StockDetailsDialog(activity).show(orderItemsAdapter.getItem(position));
            }

            @Override
            public void onShowFollowupResultedInOrder(int position) {
                new FollowupDetailsDialog(activity).show(orderItemsAdapter.getItem(position).getOrderDetails().getFollowupResultedInOrder());
            }

            @Override
            public void onShowOrderDetails(int position) {
                if (ActivityMethods.isOrderDetailsActivity(activity) == false) {
                    Intent orderDetailsAct = new Intent(context, OrderDetailsActivity.class);
                    orderDetailsAct.putExtra(Constants.ORDER_ID, orderItemsAdapter.getItem(position).getOrderID());
                    startActivityForResult(orderDetailsAct, Constants.ORDER_DETAILS_ACTIVITY_CODE);
                }
            }

            @Override
            public void onShowMoneyCollectionDetails(int position) {
                new MoneyCollectionDetailsDialog(activity).showForOrder(orderItemsAdapter.getItem(position).getOrderID());
            }

            @Override
            public void onCheckPaymentStatus(final int position) {
                final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                showLoadingDialog("Checking payment status, wait...");
                getBackendAPIs().getPaymentAPI().checkOrderPaymentStatus(orderItemDetails.getOrderID(), new PaymentAPI.CheckOrderPaymentStatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, OrderDetails order_details) {
                        closeLoadingDialog();
                        if (status) {
                            orderItemDetails.getOrderDetails().setTotalAmount(order_details.getTotalAmount());
                            orderItemDetails.getOrderDetails().setTotalCashAmount(order_details.getTotalCashAmount());
                            orderItemDetails.getOrderDetails().setTotalOnlinePaymentAmount(order_details.getTotalOnlinePaymentAmount());

                            orderItemsAdapter.updateItem(position, orderItemDetails);
                        } else {
                            showToastMessage("Something went wrong while checking payment status.\nPlease try again!");
                        }
                    }
                });
            }

            @Override
            public void onRequestOnlinePayment(final int position) {
                final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                new ECODDialog(activity).showForOrder(new OrderDetails(orderItemDetails.getOrderDetails()), orderItemDetails.getTotalOrderCashAmount(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if (status) {
                            activity.showToastMessage("Payment success");
                            onCheckPaymentStatus(position);
                        } else {
                            activity.showToastMessage("Payment failed");
                        }
                    }
                });
            }

            @Override
            public void onPrintBill(int position) {
                if (ActivityMethods.isFutureOrdersActivity(activity) == false
                        && (userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() || userDetails.isCustomerRelationshipManager())) {
                    handleOnItemClick(position, new String[]{Constants.PRINT_BILL, Constants.PRINT_LABEL});
                }
            }

            @Override
            public void onAcknowledge(final int position) {

                if (isHasActions) {
                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                    if (orderItemsAdapter.getItem(position).isInCustomerSupportState()) {
                        handleOnItemClick(position, new String[]{(userDetails.isSuperUser() || orderItemsAdapter.getItem(position).getCustomerSupporAssignerID().equals(userDetails.getID())) ? Constants.REMOVE_FROM_CUSTOMER_SUPPORT : null
                                , userDetails.isDeliveryBoyManager() ? (orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA) : null
                                , userDetails.isDeliveryBoyManager() ? Constants.UPDATE_HOUSING_SOCIETY : null
                                , Constants.UPDATE_ALTERNATE_NUMBER});
                    } else {
                        List<String> optionsList = new ArrayList<>();
                        if (userDetails.isProcessingManager() || userDetails.isCustomerRelationshipManager()) {
                            optionsList = CommonMethods.getStringListFromStringArray(new String[]{Constants.ACKNOWLEDGE,
                                    Constants.UPDATE_SPECIAL_INSTRUCTIONS,
                                    //ActivityMethods.isFutureOrdersActivity(getActivity()) ? null : Constants.MARK_AS_PROCESSING,
                                    //orderItemsAdapter.getItem(position).isCashNCarryItem() ? Constants.UNMARK_AS_CASH_N_CARRY : Constants.MARK_AS_CASH_N_CARRY,
                                    Constants.UPDATE_QUANTITY, Constants.CHANGE_DELIVERY_DATE/*, Constants.REPLACE_ITEM*/, Constants.MARK_AS_FAILED,
                                    Constants.UPDATE_ALTERNATE_NUMBER,
                                    Constants.ASSIGN_TO_CUSTOMER_SUPPORT});

                            if (optionsList.size() > 2) {
                                optionsList.add(2, Constants.EDIT_ORDER_ITEM);
                            } else {
                                optionsList.add(Constants.EDIT_ORDER_ITEM);
                            }
                        }
                        if ((userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager()) && orderItemDetails.getHubDetails() == null) {
                            optionsList.remove(Constants.ACKNOWLEDGE);
                            optionsList.add(0, Constants.ASSIGN_HUB);
                        }
                        if (userDetails.isDeliveryBoyManager()) {
                            optionsList.add(orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA);
                            optionsList.add(Constants.UPDATE_HOUSING_SOCIETY);
                        }

                        if (ActivityMethods.isFutureOrdersActivity(activity) == false
                                && (userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() || userDetails.isCustomerRelationshipManager())) {
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_LABEL);
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_BILL);
                        }

                        handleOnItemClick(position, optionsList);
                    }
                }
            }

            @Override
            public void onStartProcessing(final int position) {

                if (isHasActions) {
                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                    List<String> optionsList = new ArrayList<>();
                    if (orderItemsAdapter.getItem(position).isInCustomerSupportState()) {
                        handleOnItemClick(position, new String[]{(userDetails.isSuperUser() || orderItemsAdapter.getItem(position).getCustomerSupporAssignerID().equals(userDetails.getID())) ? Constants.REMOVE_FROM_CUSTOMER_SUPPORT : null
                                , userDetails.isDeliveryBoyManager() ? (orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA) : null
                                , userDetails.isDeliveryBoyManager() ? Constants.UPDATE_HOUSING_SOCIETY : null
                                , Constants.UPDATE_ALTERNATE_NUMBER});
                    } else {
                        if (userDetails.isProcessingManager() || userDetails.isCustomerRelationshipManager()) {
                            if (userDetails.isProcessingManager()) {
                                optionsList = CommonMethods.getStringListFromStringArray(new String[]{
                                        Constants.START_PROCESING,
                                        Constants.UPDATE_SPECIAL_INSTRUCTIONS,
                                        Constants.RESET_TO_NEW,
                                        //orderItemsAdapter.getItem(position).isCashNCarryItem() ? Constants.UNMARK_AS_CASH_N_CARRY : Constants.MARK_AS_CASH_N_CARRY,
                                        Constants.UPDATE_QUANTITY, Constants.CHANGE_DELIVERY_DATE/*, Constants.REPLACE_ITEM*/, Constants.MARK_AS_FAILED,
                                        Constants.UPDATE_ALTERNATE_NUMBER,
                                        Constants.ASSIGN_TO_CUSTOMER_SUPPORT}, true);
                            }
                            if (optionsList.size() > 2) {
                                optionsList.add(2, Constants.EDIT_ORDER_ITEM);
                            } else {
                                optionsList.add(Constants.EDIT_ORDER_ITEM);
                            }

                            if ((userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager()) && orderItemDetails.getHubDetails() == null) {
                                optionsList.add(0, Constants.ASSIGN_HUB);
                            }

                            if (ActivityMethods.isFutureOrdersActivity(getActivity())
                                    || (ActivityMethods.isOrderDetailsActivity(getActivity()) && DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemsAdapter.getItem(position).getDeliveryDate())), DateMethods.getOnlyDate(localStorageData.getServerTime())) > 0)) {
                                optionsList.remove(Constants.START_PROCESING);
                            }
                        }
                        if (userDetails.isDeliveryBoyManager()) {
                            optionsList.add(orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA);
                            optionsList.add(Constants.UPDATE_HOUSING_SOCIETY);
                        }

                        if (ActivityMethods.isFutureOrdersActivity(activity) == false
                                && (userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() || userDetails.isCustomerRelationshipManager())) {
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_LABEL);
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_BILL);
                        }

                        handleOnItemClick(position, optionsList);
                    }
                }
            }

            @Override
            public void onCompleteProcessing(final int position) {

                if (isHasActions) {
                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                    if (orderItemsAdapter.getItem(position).isInCustomerSupportState()) {
                        handleOnItemClick(position, new String[]{(userDetails.isSuperUser() || orderItemsAdapter.getItem(position).getCustomerSupporAssignerID().equals(userDetails.getID())) ? Constants.REMOVE_FROM_CUSTOMER_SUPPORT : null
                                , userDetails.isDeliveryBoyManager() ? (orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA) : null
                                , userDetails.isDeliveryBoyManager() ? Constants.UPDATE_HOUSING_SOCIETY : null
                                , Constants.UPDATE_ALTERNATE_NUMBER});
                    } else {
                        List<String> optionsList = new ArrayList<>();
                        if (orderItemDetails.getProcesserID() != null && orderItemDetails.getProcesserID().equals(localStorageData.getUserID())) {
                            optionsList = CommonMethods.getStringListFromStringArray(new String[]{
                                    Constants.COMPLETE_PROCESING, Constants.REMOVE_PROCESSING_INFO,
                                    //orderItemsAdapter.getItem(position).isCashNCarryItem() ? Constants.UNMARK_AS_CASH_N_CARRY : Constants.MARK_AS_CASH_N_CARRY,
                                    Constants.EDIT_ORDER_ITEM, Constants.UPDATE_QUANTITY, Constants.CHANGE_DELIVERY_DATE/*, Constants.REPLACE_ITEM*/, Constants.MARK_AS_FAILED,
                                    Constants.UPDATE_ALTERNATE_NUMBER,
                                    Constants.ASSIGN_TO_CUSTOMER_SUPPORT}, true);
                        }

                        if ((userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager()) && orderItemDetails.getHubDetails() == null) {
                            optionsList.add(0, Constants.ASSIGN_HUB);
                        }

                        if (userDetails.isProcessingManager()/*userDetails.isSuperUser()*/ && optionsList.contains(Constants.REMOVE_PROCESSING_INFO) == false) {
                            optionsList.add(Constants.REMOVE_PROCESSING_INFO);
                        }
                        if (userDetails.isDeliveryBoyManager()) {
                            optionsList.add(orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA);
                            optionsList.add(Constants.UPDATE_HOUSING_SOCIETY);
                        }

                        if (ActivityMethods.isFutureOrdersActivity(getActivity())
                                || (ActivityMethods.isOrderDetailsActivity(getActivity()) && DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemsAdapter.getItem(position).getDeliveryDate())), DateMethods.getOnlyDate(localStorageData.getServerTime())) > 0)) {
                            optionsList.remove(Constants.COMPLETE_PROCESING);
                            optionsList.remove(Constants.REMOVE_PROCESSING_INFO);
                        }

                        if (ActivityMethods.isFutureOrdersActivity(activity) == false
                                && (userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() || userDetails.isCustomerRelationshipManager())) {
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_LABEL);
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_BILL);
                        }

                        handleOnItemClick(position, optionsList);
                    }
                }
            }

            @Override
            public void onAssignDeliveryBoy(final int position) {

                if (isHasActions
                        && (ActivityMethods.isPendingDeliveriesActivity(getActivity()) || ActivityMethods.isPendingProcessingActivity(activity) || ActivityMethods.isPendingQCActivity(activity) || ActivityMethods.isTodaysOrdersActivity(getActivity()) || ActivityMethods.isCashNCarryItemsActivity(activity)) || ActivityMethods.isOrderDetailsActivity(activity)) {
                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                    if (orderItemsAdapter.getItem(position).isInCustomerSupportState()) {
                        handleOnItemClick(position, new String[]{(userDetails.isSuperUser() || orderItemsAdapter.getItem(position).getCustomerSupporAssignerID().equals(userDetails.getID())) ? Constants.REMOVE_FROM_CUSTOMER_SUPPORT : null
                                , userDetails.isDeliveryBoyManager() ? (orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA) : null
                                , userDetails.isDeliveryBoyManager() ? Constants.UPDATE_HOUSING_SOCIETY : null
                                , Constants.UPDATE_ALTERNATE_NUMBER});
                    } else {
                        List<String> optionsList = new ArrayList<>();

                        if (userDetails.isDeliveryBoyManager()) {
                            optionsList.add(orderItemDetails.getQualityCheckStatus() == Constants.QC_DONE ? Constants.ASSIGN_DELIVERY_BOY : null);
                            optionsList.add(Constants.MARK_AS_FAILED);
                            optionsList.add(Constants.ASSIGN_TO_CUSTOMER_SUPPORT);
                            optionsList.add(orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA);
                            optionsList.add(Constants.UPDATE_HOUSING_SOCIETY);
                        }

                        if ((userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager()) && orderItemDetails.getHubDetails() == null) {
                            optionsList.add(0, Constants.ASSIGN_HUB);
                        }

                        if (userDetails.isProcessingManager() && ActivityMethods.isPendingDeliveriesActivity(activity) == false) {
                            if (orderItemDetails.getQualityCheckStatus() != Constants.QC_DONE) {
                                optionsList.add(0, orderItemDetails.getQualityCheckStatus() == Constants.QC_PENDING ? Constants.FAIL_QUALITY_CHECK : null);
                                optionsList.add(0, orderItemDetails.getQualityCheckStatus() == Constants.QC_PENDING ? Constants.COMPLETE_QUALITY_CHECK : null);
                                optionsList.add(0, orderItemDetails.getQualityCheckStatus() == Constants.QC_FAILED ? Constants.FIX_QUALITY_CHECK : null);
                            }
                            //optionsList.add(Constants.UPDATE_VENDOR_DETAILS);
                            //optionsList.add(Constants.UPDATE_BUTCHER_DETAILS);
                            optionsList.add(Constants.UPDATE_STOCK_DETAILS);
                            optionsList.add(Constants.REPROCESS_ITEM);
                            optionsList.add(Constants.UPDATE_NET_WEIGHT);
                        }

                        if (orderItemDetails.getTotalOrderOnlinePaymentAmount() > 0 && userDetails.isAccountsApprover()) {
                            optionsList.add(Constants.REFUND_AMOUNT);
                        }

                        optionsList.add(Constants.UPDATE_ALTERNATE_NUMBER);

                        if (ActivityMethods.isFutureOrdersActivity(activity) == false
                                && (userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() || userDetails.isCustomerRelationshipManager())) {
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_LABEL);
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_BILL);
                        }

                        handleOnItemClick(position, optionsList);
                    }
                }
            }

            @Override
            public void onCompleteDelivery(final int position) {

                if (isHasActions) {
                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                    if (orderItemsAdapter.getItem(position).isInCustomerSupportState()) {
                        handleOnItemClick(position, new String[]{(userDetails.isSuperUser() || orderItemsAdapter.getItem(position).getCustomerSupporAssignerID().equals(userDetails.getID())) ? Constants.REMOVE_FROM_CUSTOMER_SUPPORT : null
                                , userDetails.isDeliveryBoyManager() ? (orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA) : null
                                , userDetails.isDeliveryBoyManager() ? Constants.UPDATE_HOUSING_SOCIETY : null
                                , Constants.UPDATE_ALTERNATE_NUMBER});
                    } else {
                        List<String> optionsList = new ArrayList<>();

                        if (ActivityMethods.isTodaysOrdersActivity(getActivity()) || ActivityMethods.isCashNCarryItemsActivity(activity)) {
                            if (userDetails.isDeliveryBoyManager()) {
                                //optionsList.add(orderItemDetails.getStatusString().equalsIgnoreCase(Constants.DELIVERING)?null:Constants.REASSIGN_DELIVERY_BOY);// TODO
                                optionsList.add(orderItemDetails.getDeliveryArea() != null ? Constants.CHANGE_DELIVERY_AREA : Constants.ASSIGN_TO_DELIVERY_AREA);
                                optionsList.add(Constants.UPDATE_HOUSING_SOCIETY);
                                optionsList.add(Constants.MARK_AS_FAILED);
                                optionsList.add(Constants.ASSIGN_TO_CUSTOMER_SUPPORT);
                                optionsList.add(orderItemDetails.getStatusString().equalsIgnoreCase(Constants.DELIVERING) ? null : Constants.REMOVE_DELIVERY_BOY_INFO);// TODO
                            }
                            if (userDetails.isProcessingManager()) {
                                //optionsList.add(Constants.UPDATE_VENDOR_DETAILS);
                                //optionsList.add(Constants.UPDATE_BUTCHER_DETAILS);
                                optionsList.add(Constants.UPDATE_STOCK_DETAILS);
                                optionsList.add(Constants.UPDATE_NET_WEIGHT);
                            }
                            if (orderItemDetails.getTotalOrderOnlinePaymentAmount() > 0 && userDetails.isAccountsApprover()) {
                                optionsList.add(Constants.REFUND_AMOUNT);
                            }

                            if ((userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager()) && orderItemDetails.getHubDetails() == null) {
                                optionsList.add(0, Constants.ASSIGN_HUB);
                            }
                        } else {
                            if (userDetails.getID().equals(orderItemsAdapter.getItem(position).getDeliveryBoyID())) {
                                optionsList.add(Constants.START_DELIVERY);
                            }
                        }

                        optionsList.add(Constants.UPDATE_ALTERNATE_NUMBER);

                        if (ActivityMethods.isFutureOrdersActivity(activity) == false
                                && (userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager() || userDetails.isCustomerRelationshipManager())) {
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_LABEL);
                            optionsList.add(ActivityMethods.isFutureOrdersActivity(activity) ? null : Constants.PRINT_BILL);
                        }

                        handleOnItemClick(position, optionsList);
                    }
                }
            }

            @Override
            public void onRejectedItemsHandling(int position) {
                if (isHasActions) {
                    if (orderItemsAdapter.getItem(position).isInCustomerSupportState() == false) {
                        List<String> actionsList = new ArrayList<>();
                        if (userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager()) {
                            actionsList.add(Constants.MARK_AS_FAILED);
                        }
//                        if ( userDetails.isProcessingManager() ){
//                            actionsList.add(Constants.REPROCESS_ITEM);
//                        }
                        if (userDetails.isDeliveryBoyManager()) {
                            actionsList.add(Constants.REMOVE_DELIVERY_BOY_INFO);
                        }
                        if ((userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager()) && orderItemsAdapter.getItem(position).getHubDetails() == null) {
                            actionsList.add(Constants.ASSIGN_HUB);
                        }
                        handleOnItemClick(position, actionsList);
                    }
                }
            }

            @Override
            public void onReProcess(final int position) { // ITEM STATUS: DELIVERED OR FAILED
                OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                if (isHasActions) {
                    List<String> optionsList = new ArrayList<>();
                    if (userDetails.isProcessingManager()) {
                        //optionsList.add(Constants.UPDATE_VENDOR_DETAILS);
                        //optionsList.add(Constants.UPDATE_BUTCHER_DETAILS);
                        optionsList.add(Constants.UPDATE_STOCK_DETAILS);
                        optionsList.add(orderItemsAdapter.getItem(position).getStatus() != 6 ? Constants.REPROCESS_ITEM : null);
                        if (orderItemDetails.getTotalOrderOnlinePaymentAmount() > 0 && userDetails.isAccountsApprover()) {
                            optionsList.add(Constants.REFUND_AMOUNT);
                        }
                        optionsList.add(Constants.PRINT_LABEL);
                        optionsList.add(Constants.PRINT_BILL);
                    }
                    if (userDetails.isDeliveryBoyManager() || userDetails.isCustomerRelationshipManager() || userDetails.isOrdersManager()) {
                        optionsList.add(Constants.ADD_DELAYED_DELIVERY_COMMENT);
                    }
                    if (userDetails.isDeliveryBoyManager()) {
                        optionsList.add(Constants.UPDATE_HOUSING_SOCIETY);
                    }
                    if ((userDetails.isProcessingManager() || userDetails.isDeliveryBoyManager()) && orderItemDetails.getHubDetails() == null) {
                        optionsList.add(0, Constants.ASSIGN_HUB);
                    }
                    //----
                    if (optionsList != null && optionsList.size() > 0) {
                        handleOnItemClick(position, optionsList);
                    }
                }
            }

            @Override
            public void onCompleteCounterSaleOrder(int position) {

                if (isHasActions) {
                    if (orderItemsAdapter.getItem(position).isInCustomerSupportState()) {
                        //handleOnItemClick(position, new String[]{orderItemsAdapter.getItem(position).getCustomerSupporAssignerID().equals(userDetails.getID()) ? Constants.REMOVE_FROM_CUSTOMER_SUPPORT : null});
                    } else {
                        if (userDetails.isProcessingManager()) {
                            if (userDetails.isProcessingManager()) {
                                handleOnItemClick(position, new String[]{Constants.COMPLETE_COUNTER_SALE_ORDER/*, Constants.EDIT_COUNTER_SALE_ORDER*/, Constants.MARK_AS_FAILED, Constants.PRINT_LABEL, Constants.PRINT_BILL});
                            }
                        }
                    }
                }
            }

            @Override
            public void onClaimForCustomerSupport(int position) {
                handleOnItemClick(position, new String[]{Constants.CLAIM_CUSTOMER_SUPPORT}, true);
            }

            @Override
            public void onCompleteCustomerSupport(int position) {
                OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                if (orderItemDetails.isCounterSaleEmployeeOrder()) {
                    handleOnItemClick(position, new String[]{Constants.ADD_CUSTOMER_SUPPORT_COMMENT, Constants.EDIT_COUNTER_SALE_ORDER, Constants.MARK_AS_FAILED/*Constants.COMPLETE_CUSTOMER_SUPPORT*/});
                } else {
                    handleOnItemClick(position, new String[]{Constants.ADD_CUSTOMER_SUPPORT_COMMENT,
                            orderItemDetails.getStatus() == 7/*Acknowledged*/ ? Constants.RESET_TO_NEW : null,
                            Constants.EDIT_ORDER_ITEM, Constants.UPDATE_QUANTITY, Constants.CHANGE_DELIVERY_DATE, Constants.MARK_AS_FAILED/*Constants.COMPLETE_CUSTOMER_SUPPORT*/});
                }
            }
        }).showCheckPaymentStatus().showRequestOnlinePayment();
        if (userDetails.isCustomerRelationshipManager() || userDetails.isDeliveryBoyManager() || userDetails.isProcessingManager() || userDetails.isOrdersManager()) {
            orderItemsAdapter.showMessageCustomerOption();
        }

        orderItemsHolder.setAdapter(orderItemsAdapter);


    }

    public void handleOnItemClick(final int position, String[] optionsList) {
        handleOnItemClick(position, optionsList, false);
    }

    public void handleOnItemClick(final int position, String[] optionsList, boolean showSingleItemSelectionAlert) {
        handleOnItemClick(position, CommonMethods.getStringListFromStringArray(optionsList, true), showSingleItemSelectionAlert);
    }

    public void handleOnItemClick(final int position, final List<String> optionsList) {
        handleOnItemClick(position, optionsList, false);
    }

    public void handleOnItemClick(final int position, final List<String> optionsList, boolean showSingleItemSelectionAlert) {

        final List<String> clickOptionsList = new ArrayList<>();
        if (optionsList != null) {
            for (int i = 0; i < optionsList.size(); i++) {
                if (optionsList.get(i) != null) {
                    clickOptionsList.add(optionsList.get(i));
                }
            }
        }
        if (clickOptionsList.size() > 0) {
            final ListChooserCallback listChooserCallback = new ListChooserCallback() {
                @Override
                public void onSelect(int clickedItemPostion) {
                    final String clickedOption = clickOptionsList.get(clickedItemPostion);

                    if (clickedOption.equalsIgnoreCase(Constants.PRINT_BILL)) {
                        new BillPrinter(activity).start(orderItemsAdapter.getItem(position).getOrderID(), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.setBillPrinted(position, true);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.PRINT_LABEL)) {
                        final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                        showLoadingDialog("Loading meat item details, wait...");
                        activity.getBackendAPIs().getMeatItemsAPI().getMeatItemAttributes(orderItemDetails.getMeatItemDetails().getID(), new MeatItemsAPI.MeatItemAttributesCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, List<AttributeDetails> attributesList) {
                                closeLoadingDialog();
                                if (status) {
                                    List<AttributeOptionDetails> itemLevelAttributeOptions = new ArrayList<>();
                                    if (orderItemDetails.getSelectedAttributes() != null && orderItemDetails.getSelectedAttributes().size() > 0) {
                                        for (int i = 0; i < orderItemDetails.getSelectedAttributes().size(); i++) {
                                            for (int j = 0; j < attributesList.size(); j++) {
                                                if (orderItemDetails.getSelectedAttributes().get(i).getAttributeID().equals(attributesList.get(j).getID())
                                                        && attributesList.get(j).isItemLevelAttribute()) {
                                                    itemLevelAttributeOptions.add(orderItemDetails.getSelectedAttributes().get(i).getOptionDetails());
                                                }
                                            }
                                        }
                                    }
                                    new LabelPrinterDialog(activity).show()
                                            .disableItemChange()
                                            .setSelectedItem(new WarehouseMeatItemDetails(orderItemDetails.getWarehouseMeatItemID()).setMeatItemDetails(orderItemDetails.getMeatItemDetails()), itemLevelAttributeOptions)
                                            .setStock(new StockDetails(orderItemDetails.getStock()));
                                } else {
                                    showToastMessage("Something went wrong, try again!");
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.ASSIGN_HUB)) {
                        new AcknowledgeFlow(activity).assignHubToOrder(orderItemsAdapter.getItem(position), new AcknowledgeFlow.AssignHubCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, HubDetails hubDetails) {
                                if (status) {
                                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
                                    orderItemDetails.setHubDetails(hubDetails);
                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails, null);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.ACKNOWLEDGE)) {
                        new AcknowledgeFlow(activity).markAsAcknowledged(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.markAsAcknowledged(position);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                    if (ActivityMethods.isNewOrdersActivity(getActivity())) {
                                        deleteFromItemsList(orderItemsAdapter.getItem(position).getID());
                                    }
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.UPDATE_SPECIAL_INSTRUCTIONS)) {
                        final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
                        new ProcessingFlow(activity).updateSpecialInstructions(orderItemDetails, new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String specialInstructions) {
                                if (status) {
                                    orderItemDetails.setSpecialInstructions(specialInstructions);

                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                    updateItemInList(orderItemDetails);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.MARK_AS_PROCESSING)) {
                        new ProcessingFlow(activity).claimProcessing(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.updateProcessingBy(position, new Gson().fromJson(message, UserDetails.class));
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                    if (ActivityMethods.isNewOrdersActivity(getActivity())) {
                                        deleteFromItemsList(orderItemsAdapter.getItem(position).getID());
                                    }
                                }
                            }
                        });
                    }

                    /*else if ( clickedOption.equalsIgnoreCase(Constants.UPDATE_VENDOR_DETAILS) ){
                        new ProcessingFlow(activity).updateVendorDetails(orderItemsAdapter.getItem(position), new OrderItemDetailsCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails) {
                                if ( status ){
                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                    updateItemInList(orderItemDetails);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails);
                                }
                            }
                        });
                    }

                    else if ( clickedOption.equalsIgnoreCase(Constants.UPDATE_BUTCHER_DETAILS) ){
                        new ProcessingFlow(activity).updateButcherDetails(orderItemsAdapter.getItem(position), new OrderItemDetailsCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails) {
                                if ( status ){
                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                    updateItemInList(orderItemDetails);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails);
                                }
                            }
                        });
                    }*/

                    else if (clickedOption.equalsIgnoreCase(Constants.UPDATE_STOCK_DETAILS)) {
                        new ProcessingFlow(activity).updateStockDetails(orderItemsAdapter.getItem(position), new OrderItemDetailsCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails) {
                                if (status) {
                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                    updateItemInList(orderItemDetails);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.RESET_TO_NEW)) {
                        new AcknowledgeFlow(activity).markAsNew(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.markAsNew(position);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                }
                            }
                        });

                    } else if (clickedOption.equalsIgnoreCase(Constants.REMOVE_PROCESSING_INFO)) {
                        new ProcessingFlow(activity).removeProcessingBy(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
                                    orderItemDetails.setStatus(7);
                                    orderItemDetails.setProcessingClaimDate(null);
                                    orderItemDetails.setProcessorDetails(null);

                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails, null);
                                }
                            }
                        });

                    } else if (clickedOption.equalsIgnoreCase(Constants.REMOVE_DELIVERY_BOY_INFO)) {
                        new DeliveryFlow(activity).removeDeliveryBoy(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.resetToPickupPending(position);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                }
                            }
                        });
                    }

//                    else if ( clickedOption.equalsIgnoreCase(Constants.CLAIM_PROCESSING) ){
//                        new ProcessingFlow(activity).claimProcessing(orderItemsAdapter.getItem(position), new StatusCallback() {
//                            @Override
//                            public void onComplete(boolean status, int statusCode, String message) {
//                                if (status) {
//                                    orderItemsAdapter.updateProcessingBy(position, new Gson().fromJson(message, UserDetails.class));
//                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
//                                    if ( ActivityMethods.isNewOrdersActivity(getActivity()) ){
//                                        deleteFromItemsList(orderItemsAdapter.getItem(position).getID());
//                                    }
//                                }
//                            }
//                        });
//                    }

                    else if (clickedOption.equalsIgnoreCase(Constants.START_PROCESING)) {
                        new ProcessingFlow(activity).claimProcessing(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.updateProcessingBy(position, new Gson().fromJson(message, UserDetails.class));
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                    if (ActivityMethods.isNewOrdersActivity(getActivity())) {
                                        deleteFromItemsList(orderItemsAdapter.getItem(position).getID());
                                    }
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.COMPLETE_PROCESING)) {
                        new ProcessingFlow(activity).completeProcessing(orderItemsAdapter.getItem(position), new OrderItemDetailsCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails) {
                                orderItemsAdapter.updateItem(position, orderItemDetails);
                                updateItemInList(orderItemDetails);
                                orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                if (ActivityMethods.isPendingProcessingActivity(getActivity()) || ActivityMethods.isPendingCashNCarryItemsActivity(activity)) {
                                    deleteFromItemsList(orderItemsAdapter.getItem(position).getID());
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.FIX_QUALITY_CHECK)) {
                        final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                        showChoiceSelectionDialog("Confirm", "Do you fixed quality check issues?", "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("YES")) {
                                    showLoadingDialog("Fixing quality check, wait...");
                                    getBackendAPIs().getOrdersAPI().resetQualityCheck(orderItemDetails.getID(), new StatusCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message) {
                                            closeLoadingDialog();
                                            if (status) {
                                                orderItemDetails.setQualityCheckStatus(Constants.QC_PENDING);
                                                orderItemDetails.setQualityCheckComments("");
                                                orderItemDetails.setQualityChecker(null);
                                                orderItemDetails.setQualityCheckDate("");
                                                if (ActivityMethods.isPendingProcessingActivity(activity)) {
                                                    orderItemsAdapter.deleteItem(position);
                                                } else {
                                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                                }
                                                orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails);
                                                showToastMessage(orderItemDetails.getMeatItemDetails().getMainName() + "'s quality issues fixed");
                                            } else {
                                                showToastMessage("Something went wrong, try again!");
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.COMPLETE_QUALITY_CHECK)) {
                        final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                        showChoiceSelectionDialog("Confirm", "Are you sure the item quantity and quality are good?", "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("YES")) {
                                    showLoadingDialog("Completing quality check, wait...");
                                    getBackendAPIs().getOrdersAPI().completeQualityCheck(orderItemDetails.getID(), new StatusCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message) {
                                            closeLoadingDialog();
                                            if (status) {
                                                orderItemDetails.setQualityCheckStatus(Constants.QC_DONE);
                                                orderItemDetails.setQualityChecker(new UserDetailsMini(userDetails.getID(), userDetails.getAvailableName()));
                                                orderItemDetails.setQualityCheckDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
                                                if (ActivityMethods.isPendingQCActivity(activity) || ActivityMethods.isPendingProcessingActivity(activity)) {
                                                    orderItemsAdapter.deleteItem(position);
                                                } else {
                                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                                }
                                                orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails);
                                                showToastMessage(orderItemDetails.getMeatItemDetails().getMainName() + "'s quality check completed");
                                            } else {
                                                showToastMessage("Something went wrong, try again!");
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.FAIL_QUALITY_CHECK)) {
                        final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                        final List<String> qcFailureReasons = localStorageData.getQCFailureReasons();
                        qcFailureReasons.add("Other");
                        showListChooserDialog("QC Failure reason", qcFailureReasons, new ListChooserCallback() {
                            @Override
                            public void onSelect(int optionPosition) {
                                if (qcFailureReasons.get(optionPosition).equalsIgnoreCase("Other")) {
                                    showInputTextDialog("QC Failure Reason", "Reason", new TextInputCallback() {
                                        @Override
                                        public void onComplete(final String failureReason) {
                                            continueWithFailingQC(failureReason);
                                        }
                                    });
                                } else {
                                    continueWithFailingQC(qcFailureReasons.get(optionPosition));
                                }
                            }

                            private void continueWithFailingQC(final String qcFailureReason) {
                                showLoadingDialog("Failing quality check, wait...");
                                getBackendAPIs().getOrdersAPI().failQualityCheck(orderItemDetails.getID(), qcFailureReason, new StatusCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message) {
                                        closeLoadingDialog();
                                        if (status) {
                                            orderItemDetails.setQualityCheckStatus(Constants.QC_FAILED);
                                            orderItemDetails.setQualityChecker(new UserDetailsMini(userDetails.getID(), userDetails.getAvailableName()));
                                            orderItemDetails.setQualityCheckDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
                                            orderItemDetails.setQualityCheckComments(qcFailureReason);
                                            if (ActivityMethods.isPendingQCActivity(activity)) {
                                                orderItemsAdapter.deleteItem(position);
                                            } else {
                                                orderItemsAdapter.updateItem(position, orderItemDetails);
                                            }
                                            orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails);
                                            showToastMessage(orderItemDetails.getMeatItemDetails().getMainName() + "'s quality check failed");
                                        } else {
                                            showToastMessage("Something went wrong, try again!");
                                        }
                                    }
                                });
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.ASSIGN_DELIVERY_BOY)) {
                        new DeliveryFlow(activity).assignDeliveryBoy(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.updateDeliveringBy(position, new Gson().fromJson(message, UserDetails.class));
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                    if (ActivityMethods.isPendingDeliveriesActivity(getActivity())) {
                                        deleteFromItemsList(orderItemsAdapter.getItem(position).getID());
                                    }
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.START_DELIVERY)) {
                        new DeliveryFlow(activity).claimDelivery(orderItemsAdapter.getItem(position), new DeliveryFlow.ClaimDeliveryCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, Job jobDetails) {
                                if (status) {
                                    Intent intent = new Intent(context, DispatcherActivity.class);
                                    intent.putExtra("job", new Gson().toJson(jobDetails));//new Job(orderItemsAdapter.getItem(position), 2)));
                                    startActivityForResult(intent, Constants.COFIRM_DELIVERY_ACTIVITY_CODE);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.REPROCESS_ITEM)) {
                        new OrderItemActionsFlow(activity).reProcessOrderItem(orderItemsAdapter.getItem(position), new OrderItemActionsFlow.ReProcessOrderItemCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails) {
                                if (status) {
                                    orderItemsAdapter.markAsFailed(position, "");
                                    orderItemsAdapter.addItemAt(position + 1, orderItemDetails);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.COMPLETE_COUNTER_SALE_ORDER)) {
                        new OrderItemActionsFlow(activity).markCounterSaleAsDelivered(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.markAsDelivered(position, /*localStorageData.getUserDetails()*/null, localStorageData.getServerTime());
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);

                                    if (ActivityMethods.isTodaysOrdersActivity(activity) == false) {
                                        orderItemsAdapter.deleteItem(position);
                                    }
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.EDIT_COUNTER_SALE_ORDER)) {
                        new EditCounterSaleDialog(activity).show(orderItemsAdapter.getItem(position), new EditCounterSaleDialog.Callback() {
                            @Override
                            public void onComplete(boolean status, double newQuantity, double newPrice, String newSpecialInstrutions) {
                                if (status) {
                                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
                                    double newOrderAmount = orderItemDetails.getTotalOrderAmount() - orderItemDetails.getAmountOfItem() + newPrice;
                                    orderItemDetails.setAmountOfItem(newPrice);
                                    orderItemDetails.getOrderDetails().setTotalAmount(newOrderAmount);
                                    orderItemDetails.setQuantity(newQuantity);
                                    orderItemDetails.getOrderDetails().setSpecialInstructions(newSpecialInstrutions);

                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.REASSIGN_DELIVERY_BOY)) {
                        new DeliveryFlow(activity).reAssignDeliveryBoy(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.updateDeliveringBy(position, new Gson().fromJson(message, UserDetails.class));
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                    //activity.onReloadPressed();
                                }
                            }
                        });
                    }

                    /*else if ( clickedOption.equalsIgnoreCase(Constants.MARK_AS_CASH_N_CARRY) ){
                        new ProcessingFlow(activity).markAsCashNCarryItem(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                orderItemsAdapter.setCashNCarryItem(position, true);
                                orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                            }
                        });
                    }

                    else if ( clickedOption.equalsIgnoreCase(Constants.UNMARK_AS_CASH_N_CARRY) ){
                        new ProcessingFlow(activity).unmarkAsCashNCarryItem(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                orderItemsAdapter.setCashNCarryItem(position, false);
                                orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);

                                if ( ActivityMethods.isCashNCarryItemsActivity(activity) ){
                                    orderItemsAdapter.deleteItem(position);
                                }
                            }
                        });
                    }*/

                    else if (clickedOption.equalsIgnoreCase(Constants.UPDATE_ALTERNATE_NUMBER)) {
                        final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);

                        showInputNumberDialog("Alternate Mobile", "Mobile number", orderItemDetails.getOrderDetails().getCustomerAlternateMobile(), 10, new TextInputCallback() {
                            @Override
                            public void onComplete(final String inputText) {
                                if (CommonMethods.isValidPhoneNumber(inputText)) {
                                    showLoadingDialog("Updating alternate number, wait...");
                                    getBackendAPIs().getOrdersAPI().updateAlternateMobile(orderItemDetails.getOrderID(), inputText, new StatusCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message) {
                                            closeLoadingDialog();
                                            if (status) {
                                                orderItemDetails.getOrderDetails().setCustomerAlternateMobile(inputText);
                                                orderItemsAdapter.updateItem(position, orderItemDetails);
                                                showToastMessage("Alternate number updated successfully");
                                            } else {
                                                showToastMessage("Something went wrong, try again!");
                                            }
                                        }
                                    });
                                } else {
                                    showToastMessage("* Invalid mobile number");
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.UPDATE_NET_WEIGHT)) {
                        final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
                        showInputNumberDecimalDialog("Final net weight", "Weight (in kgs)", orderItemDetails.getFinalNetWeight(), new TextInputCallback() {
                            @Override
                            public void onComplete(final String inputText) {
                                if (CommonMethods.parseDouble(inputText) > 0) {
                                    showLoadingDialog("Updating net weight, wait...");
                                    getBackendAPIs().getOrdersAPI().updateNetWeight(orderItemDetails.getID(), CommonMethods.parseDouble(inputText), new StatusCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message) {
                                            closeLoadingDialog();
                                            if (status) {
                                                orderItemDetails.setFinalNetWeight(CommonMethods.parseDouble(inputText));
                                                orderItemsAdapter.updateItem(position, orderItemDetails);
                                                showToastMessage("Final net weight updated successfully");
                                            } else {
                                                showToastMessage("Something went wrong, try again!");
                                            }
                                        }
                                    });
                                } else {
                                    activity.showToastMessage("Final net weight must be greater than 0");
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.UPDATE_QUANTITY)) {
                        new OrderItemActionsFlow(activity).updateQuantity(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    activity.showLoadingDialog("Loading, wait...");
                                    getBackendAPIs().getOrdersAPI().getOrderItems(orderItemsAdapter.getItem(position).getOrderID(), new OrdersAPI.OrderDetailsCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<OrderItemDetails> orderItemsList) {
                                            activity.closeLoadingDialog();
                                            if (status) {
                                                for (int i = 0; i < orderItemsList.size(); i++) {
                                                    if (orderItemsList.get(i).getID().equals(orderItemsAdapter.getItem(position).getID())) {
                                                        orderItemsAdapter.updateItem(position, orderItemsList.get(i));
                                                        break;
                                                    }
                                                }
                                                orderItemsDatabase.addOrUpdateOrderItems(orderItemsList, null);
                                            } else {
                                                orderItemsAdapter.updateQuantity(position, Double.parseDouble(message));
                                                orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                                activity.onBackgroundReloadPressed();
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.EDIT_ORDER_ITEM)) {
                        editOrderItem(position, new OrdersAPI.EditOrderItemCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails modifiedOrderItemDetails) {
                                if (status) {
                                    if (orderItemsAdapter.getItem(position).getMeatItemDetails().getCategoryIdentifier().equalsIgnoreCase(modifiedOrderItemDetails.getMeatItemDetails().getCategoryIdentifier())) {
                                        orderItemsAdapter.updateItem(position, modifiedOrderItemDetails);
                                    } else {
                                        /*orderItemsAdapter.deleteItem(position);
                                        activity.updateOrderItem(modifiedOrderItemDetails);*/
                                        activity.onReloadFromDatabase();
                                    }
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.CHANGE_DELIVERY_DATE)) {
                        new OrderItemActionsFlow(activity).changeDeliveryDate(orderItemsAdapter.getItem(position), new OrderItemActionsFlow.ChangeOrderItemDateCallback() {
                            @Override
                            public void onComplete(boolean status, OrderItemDetails modifiedOrderItemDetails) {//String deliveryDate, OrderDetails2 orderDetails) {
                                if (status) {
                                    showToastMessage("Delivery date successfully changed to " + DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(modifiedOrderItemDetails.getDeliveryDate())/*fullDate*/, DateConstants.MMM_DD_YYYY_HH_MM_A));

//                                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
//                                    orderItemDetails.setDeliveryDate(deliveryDate);//DateMethods.getInUTCFormat(fullDate));
//                                    orderItemDetails.setOrderDetails(orderDetails);
                                    // UPDATING DB
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);

                                    if (DateMethods.compareDates(DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(modifiedOrderItemDetails.getDeliveryDate())), DateMethods.getOnlyDate(localStorageData.getServerTime())) == 0) {
                                        if (ActivityMethods.isFutureOrdersActivity(activity)) {
                                            deleteFromItemsList(orderItemsAdapter.getItem(position).getID());//orderItemsAdapter.deleteItem(position);
                                        } else {
                                            orderItemsAdapter.updateItem(position, modifiedOrderItemDetails);
                                        }
                                    } else {
                                        if (ActivityMethods.isFutureOrdersActivity(activity) || ActivityMethods.isUnprocessedCustomerSupportActivity(activity)) {
                                            orderItemsAdapter.updateItem(position, modifiedOrderItemDetails);
                                        } else {
                                            deleteFromItemsList(orderItemsAdapter.getItem(position).getID());//orderItemsAdapter.deleteItem(position);
                                        }
                                    }
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.REPLACE_ITEM)) {
                        new OrderItemActionsFlow(activity).replaceItem(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    deleteFromItemsList(orderItemsAdapter.getItem(position).getID());
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.MARK_AS_FAILED)) {
                        new OrderItemActionsFlow(activity).failItem(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    /*activity.showLoadingDialog("Loading, wait...");
                                    getBackendAPIs().getOrdersAPI().getOrderItems(orderItemsAdapter.getItem(position).getOrderID(), new OrdersAPI.OrderDetailsCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<OrderItemDetails> orderItemsList) {
                                            activity.closeLoadingDialog();
                                            if ( status ){
                                                for (int i=0;i<orderItemsList.size();i++){
                                                    if ( orderItemsList.get(i).getID().equals(orderItemsAdapter.getItem(position).getID()) ){
                                                        orderItemsAdapter.updateItem(position, orderItemsList.get(i));
                                                        break;
                                                    }
                                                }
                                                orderItemsDatabase.addOrUpdateOrderItems(orderItemsList, null);
                                            }else{*/
                                    orderItemsAdapter.markAsFailed(position, message);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                                /*activity.onBackgroundReloadPressed();
                                            }*/
                                    if (ActivityMethods.isTodaysOrdersActivity(getActivity()) == false && ActivityMethods.isFutureOrdersActivity(getActivity()) == false && ActivityMethods.isUnprocessedCustomerSupportActivity(activity) == false) {
                                        deleteFromItemsList(orderItemsAdapter.getItem(position).getID());
                                    }
                                        /*}
                                    });*/
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.ASSIGN_TO_CUSTOMER_SUPPORT)) {
                        new CustomerSupportFlow(activity).assignToCustomerService(context, orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String assignedReason) {
                                if (status) {
                                    orderItemsAdapter.assignToCustomerSupport(position, assignedReason);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.REMOVE_FROM_CUSTOMER_SUPPORT)) {
                        new CustomerSupportFlow(activity).removeFromCustomerSupport(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.removeFromCustomerSupport(position);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                }
                            }
                        });

                    } else if (clickedOption.equalsIgnoreCase(Constants.CLAIM_CUSTOMER_SUPPORT)) {
                        new CustomerSupportFlow(activity).claimCustomerSupportItem(orderItemsAdapter.getItem(position), new OrderItemsListCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                                if (status) {
                                    activity.goToUnprocessedCustomerSupportItemsPage(orderItemsList);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.ADD_CUSTOMER_SUPPORT_COMMENT)) {
                        new CustomerSupportFlow(activity).addCommentForCustomerSupport(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.setCustomerSupportComment(position, message);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.ADD_DELAYED_DELIVERY_COMMENT)) {
                        new OrderItemActionsFlow(activity).addDelayedDeliveryComment(orderItemsAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    orderItemsAdapter.setDelayedDeliveryComment(position, message);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemsAdapter.getItem(position), null);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.ASSIGN_TO_DELIVERY_AREA)
                            || clickedOption.equalsIgnoreCase(Constants.CHANGE_DELIVERY_AREA)) {
                        new RoutesFlow(activity).assignOrChangeDeliveryArea(orderItemsAdapter.getItem(position), new RoutesFlow.RouteSelectionCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, String deliveryID, DeliveryAreaDetails deliveryArea) {
                                if (status) {
                                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
                                    orderItemDetails.getOrderDetails().setDeliveryArea(deliveryArea);
                                    orderItemDetails.getOrderDetails().setFormattedOrderID(deliveryID);
                                    orderItemDetails.getOrderDetails().setInvoiceVersionNumber(orderItemDetails.getOrderDetails().getInvoiceVersionNumber() + 1);
                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails, null);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.UPDATE_HOUSING_SOCIETY)) {
                        new RoutesFlow(activity).assignOrChangeHousingSociety(orderItemsAdapter.getItem(position).getOrderID(), orderItemsAdapter.getItem(position).getOrderDetails().getDeliveryAddressLatLngString(), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String housingSocietyName) {
                                if (status) {
                                    OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
                                    orderItemDetails.getOrderDetails().setHousingSociety(housingSocietyName);
                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                    orderItemsDatabase.addOrUpdateOrderItem(orderItemDetails, null);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.REFUND_AMOUNT)) {
                        final OrderItemDetails orderItemDetails = orderItemsAdapter.getItem(position);
                        new RefundFlow(activity).start(orderItemDetails.getOrderDetails(), new RefundFlow.Callback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, double refundedAmount) {
                                if (status) {
                                    orderItemDetails.getOrderDetails().addToRefundAmount(refundedAmount);
                                    orderItemDetails.getOrderDetails().addToTotalCashAmount(refundedAmount);
                                    orderItemsAdapter.updateItem(position, orderItemDetails);
                                }
                            }
                        });
                    }
                }
            };

            if (clickOptionsList.size() == 1) {
                if (showSingleItemSelectionAlert) {
                    activity.showChoiceSelectionDialog("Confirm!", "Do you want " + clickOptionsList.get(0).toLowerCase() + "?", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                listChooserCallback.onSelect(0);
                            }
                        }
                    });
                } else {
                    listChooserCallback.onSelect(0);
                    //((vToolBarActivity) activity).showListChooserDialog(null, clickOptionsList, listChooserCallback);
                }

            } else {
                activity.showListChooserDialog(null, clickOptionsList, listChooserCallback);
            }

        }
    }

    public void editOrderItem(final int position, OrdersAPI.EditOrderItemCallback callback) {
        this.editOrderItemCallback = callback;
        Intent editOrderItemAct = new Intent(context, EditOrderItemActivity.class);
        editOrderItemAct.putExtra(Constants.ORDER_ITEM_DETAILS, new Gson().toJson(orderItemsAdapter.getItem(position)));
        startActivityForResult(editOrderItemAct, Constants.EDIT_ORDER_ITEM_ACTIVITY_CODE);
    }

    //--------------------------

    public void updateItemInList(OrderItemDetails orderItemDetails) {
        for (int i = 0; i < orderItems.size(); i++) {
            if (orderItems.get(i).getID().equals(orderItemDetails.getID())) {
                orderItems.set(i, orderItemDetails);        // Removing ItemsList
                break;
            }
        }
    }

    public void deleteFromItemsList(String id) {
        orderItemsAdapter.deleteItem(id);   // Removing From Adapter

        if (searchResultsCountIndicator.getVisibility() == View.VISIBLE) {
            if (orderItemsAdapter.getItemCount() > 0) {
                results_count.setText(orderItemsAdapter.getItemCount() + " result(s) found.\n(" + searchDetails + ")");
            } else {
                results_count.setText("No Results");
            }
        }

        for (int i = 0; i < orderItems.size(); i++) {
            if (orderItems.get(i).getID().equals(id)) {
                orderItems.remove(i);        // Removing ItemsList
                break;
            }
        }

        if (orderItemsAdapter.getItemCount() == 0) {
            showNoDataIndicator("No items");
            activity.checkItemsCount();
        }
    }

    public void notifyDataChange() {
        orderItemsAdapter.notifyDataSetChanged();
    }

    //----------------

    public void performSearch(final SearchDetails search_details) {
        this.searchDetails = search_details;

        if (isShowingLoadingPage() == false && orderItems != null && orderItems.size() > 0) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
                SearchMethods.searchOrderItems(orderItems, searchDetails, localStorageData.getServerTime(), new SearchMethods.SearchOrderItemsCallback() {
                    @Override
                    public void onComplete(boolean status, SearchDetails searchDetails, List<OrderItemDetails> filteredList) {
                        if (status && searchDetails == search_details) {
                            if (filteredList.size() > 0) {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                if (filteredList.size() == 1) {
                                    results_count.setText("1 result found.\n(" + searchDetails.getSearchString() + ")");
                                } else {
                                    results_count.setText(filteredList.size() + " results found.\n(" + searchDetails.getSearchString() + ")");
                                }

                                orderItemsAdapter.setItems(filteredList);
                                orderItemsHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText("No Results");
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");

                                orderItemsAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                orderItemsAdapter.setItems(orderItems);
                orderItemsHolder.scrollToPosition(0);
            }
        }
    }

    public void clearSearch() {
        this.searchDetails = new SearchDetails();
        searchResultsCountIndicator.setVisibility(View.GONE);
        if (orderItems.size() > 0) {
            orderItemsAdapter.setItems(orderItems);
            orderItemsHolder.scrollToPosition(0);
            switchToContentPage();
        } else {
            showNoDataIndicator("No items");
        }
        activity.filterItemsFragment.clearSelection();
    }


    //==================

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == Constants.ORDER_DETAILS_ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {
                activity.onReloadPressed();
            } else if (requestCode == Constants.EDIT_ORDER_ITEM_ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {
                final OrderItemDetails modifiedOrderItemDetails = new Gson().fromJson(data.getStringExtra(Constants.ORDER_ITEM_DETAILS), OrderItemDetails.class);
                if (editOrderItemCallback != null) {
                    editOrderItemCallback.onComplete(true, 200, "Success", modifiedOrderItemDetails);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}




/*  NOT USED CODE

public interface SearchResultsCallback{
        void onComplete(List<OrderItemDetails> searchList);
    }

    private void getSearchList(final String searchQuery, final SearchResultsCallback callback){


        new AsyncTask<Void, Void, Void>() {
            List<OrderItemDetails> filteredList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {

                if ( orderItems != null && orderItems.size() > 0 ){
                    for (int i=0;i<orderItems.size();i++){
                        boolean isMatched = false;
                        OrderItemDetails orderItemDetails = orderItems.get(i);
                        if ( orderItemDetails != null && orderItemDetails.getOrderDetails() != null ){
                            OrderDetails2 deliveryDetails = orderItemDetails.getOrderDetails();

                            if ( searchQuery.replaceAll("[()]","").equalsIgnoreCase("pre_order") ){
                                if ( DateMethods.compareDates(DateMethods.getOnlyDate(localStorageData.getServerTime()), DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(deliveryDetails.getCreatedDate()))) > 0 ){
                                    isMatched = true;
                                }
                            }
                            else if ( searchQuery.replaceAll("[()]","").equalsIgnoreCase("non_pre_order") ){
                                if ( DateMethods.compareDates(DateMethods.getOnlyDate(localStorageData.getServerTime()), DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(deliveryDetails.getCreatedDate()))) == 0 ){
                                    isMatched = true;
                                }
                            }
                            else{
                                if ( searchQuery.equalsIgnoreCase("(" + orderItemDetails.getPaymentTypeString() + ")")
                                        || searchQuery.equalsIgnoreCase("("+orderItemDetails.getStatusString()+")")
                                        || searchQuery.equalsIgnoreCase("("+orderItemDetails.getPrevCompletedStatusString()+")")
                                        || ( deliveryDetails.getCustomerName() != null && deliveryDetails.getCustomerName().toLowerCase().contains(searchQuery) )
                                        || (deliveryDetails.getDeliveryAddressPremise() != null && deliveryDetails.getDeliveryAddressPremise().toLowerCase().contains(searchQuery))
                                        || (deliveryDetails.getDeliveryAddressSubLocalityLevel2() != null && deliveryDetails.getDeliveryAddressSubLocalityLevel2().toLowerCase().contains(searchQuery))
                                        || (deliveryDetails.getDeliveryAddressSubLocalityLevel1() != null && deliveryDetails.getDeliveryAddressSubLocalityLevel1().toLowerCase().contains(searchQuery))
                                        || (deliveryDetails.getDeliveryAddressLocality() != null && deliveryDetails.getDeliveryAddressLocality().toLowerCase().contains(searchQuery))
                                        || (deliveryDetails.getDeliveryAddressLandmark() != null && deliveryDetails.getDeliveryAddressLandmark().toLowerCase().contains(searchQuery))
                                        || (deliveryDetails.getDeliveryAddressState() != null && deliveryDetails.getDeliveryAddressState().toLowerCase().contains(searchQuery))
                                        || (deliveryDetails.getDeliveryAddressPinCode() != null && deliveryDetails.getDeliveryAddressPinCode().contains(searchQuery))
                                        || (orderItemDetails.getMeatItemDetails().getName() != null && orderItemDetails.getMeatItemDetails().getName().toLowerCase().contains(searchQuery))
                                        || ( DateMethods.getHourFromDateIn24HrFormat(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate())) == DateMethods.getHourFromDateIn24HrFormat(searchQuery.replaceAll("[()]","")) )
                                        || (orderItemDetails.getDeliveryBoyDetails() != null && orderItemDetails.getDeliveryBoyDetails().getName().toLowerCase().contains(searchQuery.replaceAll("[()]","")))
                                        || (orderItemDetails.getProcessorDetails() != null && orderItemDetails.getProcessorDetails().getName().toLowerCase().contains(searchQuery.replaceAll("[()]","")))
                                        ){
                                    isMatched = true;
                                }
                            }

                        }
                        if ( isMatched ){
                            filteredList.add(orderItemDetails);
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(filteredList);
                }
            }
        }.execute();
    }

 */