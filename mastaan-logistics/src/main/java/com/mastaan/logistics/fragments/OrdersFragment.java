package com.mastaan.logistics.fragments;


import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.BaseOrderItemsActivity;
import com.mastaan.logistics.activities.DirectionsViewerDialogActivity;
import com.mastaan.logistics.activities.DirectionsViewerHereMapsDialogActivity;
import com.mastaan.logistics.activities.DispatcherActivity;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.OrdersAdapter;
import com.mastaan.logistics.backend.PaymentAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.BuyerOptionsDialog;
import com.mastaan.logistics.dialogs.ECODDialog;
import com.mastaan.logistics.dialogs.FollowupDetailsDialog;
import com.mastaan.logistics.dialogs.MoneyCollectionDetailsDialog;
import com.mastaan.logistics.dialogs.StockDetailsDialog;
import com.mastaan.logistics.flows.DeliveryFlow;
import com.mastaan.logistics.flows.RefundFlow;
import com.mastaan.logistics.handlers.MessageHandler;
import com.mastaan.logistics.methods.ActivityMethods;
import com.mastaan.logistics.methods.SearchMethods;
import com.mastaan.logistics.models.Job;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.printers.BillPrinter;

import java.util.ArrayList;
import java.util.List;


public class OrdersFragment extends MastaanFragment implements View.OnClickListener {

    MastaanToolbarActivity activity;

    String statusType;
    BaseOrdersFragment fragment;

    FrameLayout searchResultsCountIndicator;
    TextView results_count;
    Button clearFilter;

    vRecyclerView ordersHolder;
    OrdersAdapter ordersAdapter;

    List<OrderDetails> ordersList = new ArrayList<OrderDetails>();

    SearchDetails searchDetails;

    UserDetails userDetails;

    public OrdersFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_orders, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hasLoadingView();
        hasSwipeRefresh();
        activity = (MastaanToolbarActivity) getActivity();

        statusType = getArguments().getString("statusType");
        fragment = (BaseOrdersFragment) getParentFragment();

        userDetails = localStorageData.getUserDetails();

        //--------

        searchResultsCountIndicator = (FrameLayout) view.findViewById(R.id.searchResultsCountIndicator);
        results_count = (TextView) view.findViewById(R.id.results_count);
        clearFilter = (Button) view.findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);

        ordersHolder = (vRecyclerView) view.findViewById(R.id.ordersHolder);
        setupHolder();

        //------

        getOrders();

    }

    @Override
    public void onClick(View view) {
        if (view == clearFilter) {
            fragment.collapseSearchView();
            clearSearch();
        }
    }

    @Override
    public void onReloadPressed() {
        fragment.onReloadPressed();
    }

    public void setupHolder() {

        ordersHolder.setHasFixedSize(true);
        ordersHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        ordersHolder.setItemAnimator(new DefaultItemAnimator());

        ordersAdapter = new OrdersAdapter(activity, new ArrayList<OrderDetails>(), new OrdersAdapter.Callback() {

            @Override
            public void onCallCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if (orderDetails.isAlertnateMobileExists()) {
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile() + " (Primary)", orderDetails.getCustomerAlternateMobile() + " (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            callToPhoneNumber(nPosition == 1 ? orderDetails.getCustomerAlternateMobile() : orderDetails.getCustomerMobile());
                        }
                    });
                } else {
                    callToPhoneNumber(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onMessageCustomer(int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                if (orderDetails.isAlertnateMobileExists()) {
                    showListChooserDialog("Select Number", new String[]{orderDetails.getCustomerMobile() + " (Primary)", orderDetails.getCustomerAlternateMobile() + " (Alternate)"}, new ListChooserCallback() {
                        @Override
                        public void onSelect(int nPosition) {
                            new MessageHandler(activity).sendMessage(nPosition == 1 ? orderDetails.getCustomerAlternateMobile() : orderDetails.getCustomerMobile());
                        }
                    });
                } else {
                    new MessageHandler(activity).sendMessage(orderDetails.getCustomerMobile());
                }
            }

            @Override
            public void onShowBuyerHistory(int position) {
                new BuyerOptionsDialog(activity).show(ordersAdapter.getItem(position));
            }

            @Override
            public void onShowReferralBuyerDetails(int position) {
                new BuyerOptionsDialog(activity).show(ordersAdapter.getItem(position).getReferralBuyer());
            }

            @Override
            public void onShowMoneyCollectionDetails(int position) {
                new MoneyCollectionDetailsDialog(activity).showForOrder(ordersAdapter.getItem(position).getID());
            }

            @Override
            public void onCheckPaymentStatus(final int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);
                showLoadingDialog("Checking payment status, wait...");
                getBackendAPIs().getPaymentAPI().checkOrderPaymentStatus(orderDetails.getID(), new PaymentAPI.CheckOrderPaymentStatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, OrderDetails orderDetails) {
                        closeLoadingDialog();
                        if (status) {
                            orderDetails.setTotalAmount(orderDetails.getTotalAmount());
                            orderDetails.setTotalCashAmount(orderDetails.getTotalCashAmount());
                            orderDetails.setTotalOnlinePaymentAmount(orderDetails.getTotalOnlinePaymentAmount());
                            ordersAdapter.updateItem(position, orderDetails);
                        } else {
                            showToastMessage("Something went wrong while checking payment status.\nPlease try again!");
                        }
                    }
                });
            }

            @Override
            public void onRequestOnlinePayment(final int position) {
                new ECODDialog(activity).showForOrder(ordersAdapter.getItem(position), ordersAdapter.getItem(position).getTotalCashAmount(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        if (status) {
                            activity.showToastMessage("Payment success");
                            onCheckPaymentStatus(position);
                        } else {
                            activity.showToastMessage("Payment failed");
                        }
                    }
                });
            }

            @Override
            public void onShowFollowupResultedInOrder(int position) {
                new FollowupDetailsDialog(activity).show(ordersAdapter.getItem(position).getFollowupResultedInOrder());
            }


            @Override
            public void onPrintBill(final int position) {
                handleOnItemClick(position, new String[]{Constants.PRINT_BILL});
            }

            @Override
            public void onOrderItemClick(final int position, int item_position) {
                final OrderItemDetails orderItemDetails = ordersAdapter.getItem(position).getOrderedItems().get(item_position);

                if (userDetails.isDeliveryBoyManager() && ActivityMethods.isDeliveryBoyDeliveriesActivity(activity)
                        && orderItemDetails.getStatusString().equalsIgnoreCase(Constants.ASSIGNED)) {
                    activity.showListChooserDialog(new String[]{Constants.REMOVE_DELIVERY_BOY_INFO}, new ListChooserCallback() {
                        @Override
                        public void onSelect(final int pos) {
                            new DeliveryFlow(activity).removeDeliveryBoy(orderItemDetails, new StatusCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message) {
                                    if (status) {
                                        orderItemDetails.setStatus(2);
                                        orderItemDetails.setDeliveryBoyDetails(null);
                                        orderItemDetails.setDeliveryBoyAssignedDate(null);
                                        ordersAdapter.notifyDataSetChanged();

                                        activity.getOrderItemsDatabase().addOrUpdateOrderItem(orderItemDetails, null);
                                    }
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onShowOrderItemStockDetails(int position, int item_position) {
                new StockDetailsDialog(activity).show(ordersAdapter.getItem(position).getOrderedItems().get(item_position));
            }

            @Override
            public void onShowDirections(final int position) {
                getCurrentLocation(new LocationCallback() {
                    @Override
                    public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                        if (status) {
                            Intent showDirectionsIntent = new Intent(context, DirectionsViewerHereMapsDialogActivity.class);
                            showDirectionsIntent.setAction(Constants.POPUP_ACTION);
                            showDirectionsIntent.putExtra("origin", latLng);
                            showDirectionsIntent.putExtra("destination", ordersAdapter.getItem(position).getDeliveryAddressLatLng());
                            showDirectionsIntent.putExtra("location", ordersAdapter.getItem(position).getCustomerName() + "\n" + ordersAdapter.getItem(position).getCustomerName()/*.getAddress().getDeliveryAddressLocality()*/);
                            startActivity(showDirectionsIntent);
                        } else {
                            showToastMessage(message);
                        }
                    }
                });
            }

            @Override
            public void onItemClick(final int position) {
                final OrderDetails orderDetails = ordersAdapter.getItem(position);

                if (ActivityMethods.isMyDeliveriesActivity(getActivity()) && orderDetails.getStatus().equalsIgnoreCase(Constants.PENDING)) {
                    List<OrderDetails> ordersList = new ArrayList<>(ordersAdapter.getAllItems());
                    ordersList.remove(position);
                    ordersList.add(0, orderDetails);

                    new DeliveryFlow(activity).confirmItemsPickup(ordersList, new DeliveryFlow.CofirmItemsPickupCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, final List<String> pickedupItems, List<String> unpickedupItems) {
                            if (status) {
                                List<OrderItemDetails> myUndeliveredOrderItems = new ArrayList<>();
                                for (int x = 0; x < orderDetails.getOrderedItems().size(); x++) {
                                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(x);
                                    if (orderItemDetails.getDeliveryBoyID().equals(localStorageData.getUserID()) &&
                                            (orderItemDetails.getStatusString().equalsIgnoreCase(Constants.ASSIGNED) || orderItemDetails.getStatusString().equalsIgnoreCase(Constants.PICKED_UP) || orderItemDetails.getStatusString().equalsIgnoreCase(Constants.DELIVERING))) {
                                        myUndeliveredOrderItems.add(orderItemDetails);
                                    }
                                }

                                if (myUndeliveredOrderItems.size() > 0) {
                                    new DeliveryFlow(activity).claimDelivery(myUndeliveredOrderItems.get(0), null, new DeliveryFlow.ClaimDeliveryCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message, Job jobDetails) {
                                            if (status) {
                                                if (isAdded()) {
                                                    //HyperTrack.startTracking();
                                                    /*if ( userDetails.isSuperUser() ){
                                                        ActionParams actionParams = new ActionParamsBuilder()
                                                                .setExpectedPlace(new Place().setLocation(jobDetails.getLocation().latitude, jobDetails.getLocation().longitude))
                                                                .setType(Action.ACTION_TYPE_DELIVERY)
                                                                //.setExpectedAt(expectedAtTimestamp)
                                                                .setLookupId(jobDetails.getOrderID())
                                                                .build();
                                                        HyperTrack.createAndAssignAction(actionParams, new HyperTrackCallback() {
                                                            @Override
                                                            public void onSuccess(@NonNull SuccessResponse successResponse) {
                                                                //showToastMessage("Successfully started action "+((Action)successResponse.getResponseObject()).getLookupId());
                                                            }

                                                            @Override
                                                            public void onError(@NonNull ErrorResponse errorResponse) {
                                                                showToastMessage("Action creation failed, Msg: "+errorResponse.getErrorMessage());
                                                            }
                                                        });
                                                    }*/

                                                    Intent dispatcherAct = new Intent(context, DispatcherActivity.class);
                                                    dispatcherAct.putExtra("job", new Gson().toJson(jobDetails));//new Job(orderItemsAdapter.getItem(position), 2)));
                                                    //startActivityForResult(dispatcherAct, Constants.COFIRM_DELIVERY_ACTIVITY_CODE);
                                                    startActivity(dispatcherAct);
                                                }
                                            } else {
                                                if (pickedupItems != null && pickedupItems.size() > 0) {
                                                    activity.onReloadPressed();
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    showToastMessage("Nothing to deliver");
                                }
                            } else {
                                if (statusCode != -1) {
                                    activity.onReloadPressed();
                                }
                            }
                        }
                    });

                        /*List<OrderItemDetails> myUndeliveredOrderItems = new ArrayList<>();
                        for (int x=0;x<orderDetails.getOrderedItems().size();x++){
                            OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(x);
                            if ( orderItemDetails.getDeliveryBoyID().equals(localStorageData.getUserID()) && orderItemDetails.getStatusString().equalsIgnoreCase(Constants.DELIVERING) ){
                                myUndeliveredOrderItems.add(orderItemDetails);
                            }
                        }
                        if ( myUndeliveredOrderItems.size() > 0 ){
                            String itemsListNames = "";
                            for (int i=0;i<myUndeliveredOrderItems.size();i++){
                                if ( i == 0 ){  itemsListNames += myUndeliveredOrderItems.get(i).getMeatItemDetails().getMainName(); }
                                else{  itemsListNames += ", "+myUndeliveredOrderItems.get(i).getMeatItemDetails().getMainName(); }
                            }

                            new DeliveryFlow(activity).claimDelivery(myUndeliveredOrderItems.get(0), itemsListNames, new DeliveryFlow.ClaimDeliveryCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message, Job jobDetails) {
                                    if (status) {
                                        if ( isAdded() ) {
                                            Intent confirmDeliveryAct = new Intent(context, DispatcherActivity.class);
                                            confirmDeliveryAct.putExtra("job", new Gson().toJson(jobDetails));//new Job(orderItemsAdapter.getItem(position), 2)));
                                            //startActivityForResult(confirmDeliveryAct, Constants.COFIRM_DELIVERY_ACTIVITY_CODE);
                                            startActivity(confirmDeliveryAct);
                                        }
                                    }
                                }
                            });
                        }*/

                } else {
                    List<String> optionsList = new ArrayList<>();
                    optionsList.add(Constants.UPDATE_ALTERNATE_NUMBER);
                    if (orderDetails.getTotalOnlinePaymentAmount() > 0
                            && (orderDetails.getStatus().equalsIgnoreCase(Constants.DELIVERED) || orderDetails.getStatus().equalsIgnoreCase(Constants.FAILED))
                            && userDetails.isAccountsApprover()) {
                        optionsList.add(Constants.REFUND_AMOUNT);
                    }
                    if (ActivityMethods.isFutureOrdersActivity(activity) == false) {
                        optionsList.add(Constants.PRINT_BILL);
                    }
                    handleOnItemClick(position, optionsList, true);
                }
            }

        }).showCheckPaymentStatus().showRequestOnlinePayment();
        if (userDetails.isCustomerRelationshipManager() || userDetails.isDeliveryBoyManager() || userDetails.isProcessingManager() || userDetails.isOrdersManager()) {
            ordersAdapter.showMessageCustomerOption();
        }
        if (ActivityMethods.isMyDeliveriesActivity(activity)) {
            ordersAdapter.setShowUserItemsFirst(true);
        }
        ordersHolder.setAdapter(ordersAdapter);
    }

    //-----

    public void handleOnItemClick(final int position, String[] optionsList) {
        handleOnItemClick(position, optionsList, false);
    }

    public void handleOnItemClick(final int position, String[] optionsList, boolean showSingleItemSelectionAlert) {
        handleOnItemClick(position, CommonMethods.getStringListFromStringArray(optionsList, true), showSingleItemSelectionAlert);
    }

    public void handleOnItemClick(final int position, final List<String> optionsList) {
        handleOnItemClick(position, optionsList, false);
    }

    public void handleOnItemClick(final int position, final List<String> optionsList, boolean showSingleItemSelectionAlert) {
        final List<String> clickOptionsList = new ArrayList<>();
        if (optionsList != null) {
            for (int i = 0; i < optionsList.size(); i++) {
                if (optionsList.get(i) != null) {
                    clickOptionsList.add(optionsList.get(i));
                }
            }
        }
        if (clickOptionsList.size() > 0) {
            final ListChooserCallback listChooserCallback = new ListChooserCallback() {
                @Override
                public void onSelect(int clickedItemPostion) {
                    final String clickedOption = clickOptionsList.get(clickedItemPostion);

                    if (clickedOption.equalsIgnoreCase(Constants.PRINT_BILL)) {
                        new BillPrinter(activity).start(ordersAdapter.getItem(position), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message) {
                                if (status) {
                                    ordersAdapter.setBillPrinted(position, true);
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.UPDATE_ALTERNATE_NUMBER)) {
                        final OrderDetails orderDetails = ordersAdapter.getItem(position);
                        showInputNumberDialog("Alternate Mobile", "Mobile number", orderDetails.getCustomerAlternateMobile(), 10, new TextInputCallback() {
                            @Override
                            public void onComplete(final String inputText) {
                                if (CommonMethods.isValidPhoneNumber(inputText)) {
                                    showLoadingDialog("Updating alternate number, wait...");
                                    getBackendAPIs().getOrdersAPI().updateAlternateMobile(orderDetails.getID(), inputText, new StatusCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message) {
                                            closeLoadingDialog();
                                            if (status) {
                                                orderDetails.setCustomerAlternateMobile(inputText);
                                                ordersAdapter.updateItem(position, orderDetails);
                                                showToastMessage("Alternate number updated successfully");
                                            } else {
                                                showToastMessage("Something went wrong, try again!");
                                            }
                                        }
                                    });
                                } else {
                                    showToastMessage("* Invalid mobile number");
                                }
                            }
                        });
                    } else if (clickedOption.equalsIgnoreCase(Constants.REFUND_AMOUNT)) {
                        final OrderDetails orderDetails = ordersAdapter.getItem(position);
                        new RefundFlow(activity).start(orderDetails, new RefundFlow.Callback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, String message, double refundedAmount) {
                                if (status) {
                                    orderDetails.addToRefundAmount(refundedAmount);
                                    orderDetails.addToTotalCashAmount(refundedAmount);
                                    ordersAdapter.updateItem(position, orderDetails);
                                }
                            }
                        });
                    }
                }
            };

            if (clickOptionsList.size() == 1) {
                if (showSingleItemSelectionAlert) {
                    activity.showChoiceSelectionDialog("Confirm!", "Do you want " + clickOptionsList.get(0).toLowerCase() + "?", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                listChooserCallback.onSelect(0);
                            }
                        }
                    });
                } else {
                    listChooserCallback.onSelect(0);
                    //((vToolBarActivity) activity).showListChooserDialog(null, clickOptionsList, listChooserCallback);
                }

            } else {
                activity.showListChooserDialog(null, clickOptionsList, listChooserCallback);
            }

        }
    }

    //-----

    public void getOrders() {
        showLoadingIndicator("Loading, wait...");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ordersList = fragment.getOrders(statusType);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (ordersList != null && ordersList.size() > 0) {
                    ordersAdapter.setItems(ordersList);
                    ordersHolder.scrollToPosition(0);
                    switchToContentPage();
                } else {
                    showNoDataIndicator("No items");
                }
            }
        }.execute();
    }

    //------------------

    public void performSearch(final SearchDetails search_details) {
        this.searchDetails = search_details;

        if (isShowingLoadingPage() == false && ordersList != null && ordersList.size() > 0) {
            if (searchDetails != null && searchDetails.isSearchable()) {
                searchResultsCountIndicator.setVisibility(View.GONE);
                showLoadingIndicator("Searching\n(" + searchDetails.getSearchString() + "), wait...");
                SearchMethods.searchOrders(ordersList, searchDetails, localStorageData.getServerTime(), new SearchMethods.SearchOrdersCallback() {
                    @Override
                    public void onComplete(boolean status, SearchDetails searchDetails, List<OrderDetails> filteredList) {
                        if (status && searchDetails == search_details) {
                            if (filteredList.size() > 0) {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                if (filteredList.size() == 1) {
                                    results_count.setText("1 result found.\n(" + searchDetails.getSearchString() + ")");
                                } else {
                                    results_count.setText(filteredList.size() + " results found.\n(" + searchDetails.getSearchString() + ")");
                                }

                                ordersAdapter.setItems(filteredList);
                                ordersHolder.scrollToPosition(0);
                                switchToContentPage();
                            } else {
                                searchResultsCountIndicator.setVisibility(View.VISIBLE);
                                results_count.setText("No Results");
                                showNoDataIndicator("No results found\n(" + searchDetails.getSearchString() + ")");

                                ordersAdapter.clearItems();
                            }
                        } else {
                            switchToContentPage();
                            showToastMessage("Something went wrong, try again!");
                        }
                    }
                });
            } else {
                ordersAdapter.setItems(ordersList);
                ordersHolder.scrollToPosition(0);
            }
        }
    }

    public void clearSearch() {
        this.searchDetails = new SearchDetails();

        searchResultsCountIndicator.setVisibility(View.GONE);
        if (ordersList.size() > 0) {
            ordersAdapter.setItems(ordersList);
            ordersHolder.scrollToPosition(0);
            switchToContentPage();
        } else {
            showNoDataIndicator("No items");
        }
        try {
            ((BaseOrderItemsActivity) activity).filterItemsFragment.clearSelection();
        } catch (Exception e) {
        }
    }

}





    /*private interface SearchResultsCallback{
        void onComplete(List<OrderDetails> searchList);
    }
    private void getSearchList(final String searchQuery, final SearchResultsCallback callback){


        new AsyncTask<Void, Void, Void>() {
            List<OrderDetails> filteredList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... params) {

                if ( ordersList != null && ordersList.size() > 0 ){
                    for (int i=0;i<ordersList.size();i++){
                        boolean isMatched = false;
                        OrderDetails orderDetails = ordersList.get(i);
                        if ( orderDetails != null ){
                            if ( searchQuery.replaceAll("[()]","").equalsIgnoreCase("pre_order") ){
                                if ( DateMethods.compareDates(DateMethods.getOnlyDate(localStorageData.getServerTime()), DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()))) > 0 ){
                                    isMatched = true;
                                }
                            }
                            else if ( searchQuery.replaceAll("[()]","").equalsIgnoreCase("non_pre_order") ){
                                if ( DateMethods.compareDates(DateMethods.getOnlyDate(localStorageData.getServerTime()), DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderDetails.getCreatedDate()))) == 0 ){
                                    isMatched = true;
                                }
                            }
                            else{
                                if ( searchQuery.equalsIgnoreCase("(" + orderDetails.getPaymentTypeString() + ")")
                                        || searchQuery.equalsIgnoreCase("("+orderDetails.getStatus()+")")
                                        || ( orderDetails.getCustomerName() != null && orderDetails.getCustomerName().toLowerCase().contains(searchQuery) )
                                        || (orderDetails.getDeliveryAddressPremise() != null && orderDetails.getDeliveryAddressPremise().toLowerCase().contains(searchQuery))
                                        || (orderDetails.getDeliveryAddressSubLocalityLevel2() != null && orderDetails.getDeliveryAddressSubLocalityLevel2().toLowerCase().contains(searchQuery))
                                        || (orderDetails.getDeliveryAddressSubLocalityLevel1() != null && orderDetails.getDeliveryAddressSubLocalityLevel1().toLowerCase().contains(searchQuery))
                                        || (orderDetails.getDeliveryAddressLocality() != null && orderDetails.getDeliveryAddressLocality().toLowerCase().contains(searchQuery))
                                        || (orderDetails.getDeliveryAddressLandmark() != null && orderDetails.getDeliveryAddressLandmark().toLowerCase().contains(searchQuery))
                                        || (orderDetails.getDeliveryAddressState() != null && orderDetails.getDeliveryAddressState().toLowerCase().contains(searchQuery))
                                        || (orderDetails.getDeliveryAddressPinCode() != null && orderDetails.getDeliveryAddressPinCode().contains(searchQuery))
                                        || ( DateMethods.getHourFromDateIn24HrFormat(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate())) == DateMethods.getHourFromDateIn24HrFormat(searchQuery.replaceAll("[()]", "")) )
                                        ){
                                    isMatched = true;
                                }
                            }

                        }
                        if ( isMatched ){
                            filteredList.add(orderDetails);
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(filteredList);
                }
            }
        }.execute();
    }*/
