package com.mastaan.logistics.fragments;


import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.TimePickerCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vEditText;
import com.aleena.common.widgets.vSpinner;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.VendorsAndButchersAPI;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.flows.RoutesFlow;
import com.mastaan.logistics.methods.ActivityMethods;
import com.mastaan.logistics.models.SearchDetails;
import com.mastaan.logistics.models.Vendor;

import java.util.ArrayList;
import java.util.List;


public class FilterItemsFragment extends MastaanFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{

    MastaanToolbarActivity activity;

    Callback callback;

    View layoutView;

    vSpinner status_dropdown;
    List<String> statusStrings;

    vSpinner source_dropdown;
    List<String> sourcesStrings;

    CheckBox chickenItems;
    CheckBox muttonItems;
    CheckBox seafoodItems;

    CheckBox orderMoneyCollection;
    CheckBox outstandingMoneyCollection;

    CheckBox inputStock;
    CheckBox purchaseStock;
    CheckBox consolidationStock;
    CheckBox adjustmentStock;
    CheckBox wastageStock;
    CheckBox orderStock;
    CheckBox notOrderStock;

    CheckBox lowRated;
    CheckBox averageRated;
    CheckBox highRated;

    CheckBox withComments;
    CheckBox withoutComments;

    vEditText customer_name;
    View clearCustomerName;
    vEditText item_name;
    View clearItemName;
    vEditText item_prefs;
    View clearItemPrefs;
    vEditText processor_name;
    View clearProcessorName;
    vEditText delivery_boy_name;
    View clearDeliveryBoyName;
    vEditText approver_name;
    View clearApproverName;
    vEditText vendor_name;
    View clearVendorName;
    vEditText butcher_name;
    View clearButcherName;

    TextView area_date_indicator;
    vEditText housing_society;
    View clearHousingSociety;
    vEditText delivery_area;
    View clearDeliveryArea;
    vEditText delivery_zone;
    View clearDeliveryZone;
    vEditText delivery_date;
    View clearDeliveryDate;
    vEditText delivery_time;
    View clearDeliveryTime;
    vEditText feedback_date;
    View clearFeedbackDate;
    vEditText collection_date;
    View clearCollectionDate;
    vEditText consolidation_date;
    View clearConsolidationDate;

    CheckBox collectAmount;
    CheckBox returnAmount;
    CheckBox collectionMatched;
    CheckBox collectionMismatched;

    vEditText min_amount;
    View clearMinAmount;
    vEditText max_amount;
    View clearMaxAmount;
    CheckBox withDiscount;
    CheckBox withOuststanding;

    CheckBox onlinePayment;
    CheckBox cashOnDelivery;
    CheckBox cashAndOnlinePayment;
    CheckBox negativeCOD;

    CheckBox preOrder;
    CheckBox sameDayOrder;

    CheckBox referralOrder;
    CheckBox firstOrderFollowup;
    CheckBox followupResultedOrder;
    CheckBox firstTimeOrder;
    CheckBox pendingBillPrint;
    CheckBox withDeliveryAreaUnAssigned;
    CheckBox inCustomerSupport;
    CheckBox cashNCarryItems;
    CheckBox withDelay;
    CheckBox withSpecialInstructions;
    CheckBox createdBySuperUser;

    vEditText comments;
    View clearComments;

    View clearFilter;
    View backgroundReload;
    View filterData;


    public interface Callback {
        void onItemSelected(int position, SearchDetails searchDetails);
    }

    public FilterItemsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_filter_items, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutView = view;
        activity = (MastaanToolbarActivity) getActivity();

        statusStrings = CommonMethods.getStringListFromStringArray(new String[]{Constants.ALL, Constants.ORDERED, Constants.ACKNOWLEDGED, Constants.PROCESSING, Constants.PROCESSED, Constants.ASSIGNED, Constants.PICKED_UP, Constants.DELIVERING, Constants.DELIVERED, Constants.REJECTED, Constants.FAILED});
        if (ActivityMethods.isFutureOrdersActivity(getActivity())){
            statusStrings = CommonMethods.getStringListFromStringArray(new String[]{Constants.ALL, Constants.ORDERED, Constants.ACKNOWLEDGED, Constants.FAILED});
        }else if (ActivityMethods.isPendingProcessingActivity(getActivity())){
            statusStrings = CommonMethods.getStringListFromStringArray(new String[]{Constants.ALL, Constants.ACKNOWLEDGED, Constants.PROCESSING});
        }else if (ActivityMethods.isOrdersByDateActivity(activity) ){
            statusStrings = CommonMethods.getStringListFromStringArray(new String[]{Constants.ALL, Constants.ORDERED, Constants.ACKNOWLEDGED, Constants.PROCESSING, Constants.PROCESSED, Constants.ASSIGNED, Constants.PICKED_UP, Constants.DELIVERING, Constants.DELIVERED, Constants.PENDING, Constants.REJECTED, Constants.FAILED});
        }
        
        sourcesStrings = CommonMethods.getStringListFromStringArray(new String[]{Constants.ALL, Constants.ANDROID, Constants.IOS, Constants.WEB, Constants.PHONE});

        //--------

        status_dropdown = view.findViewById(R.id.status_dropdown);
        ListArrayAdapter statusesAdapter = new ListArrayAdapter(context, R.layout.view_spinner_popup, statusStrings);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        statusesAdapter.setPopupLayoutView(R.layout.view_spinner_popup);
        status_dropdown.setAdapter(statusesAdapter);
        status_dropdown.setSelection(0);

        source_dropdown = view.findViewById(R.id.source_dropdown);
        ListArrayAdapter sourcesAdapter = new ListArrayAdapter(context, R.layout.view_spinner_popup, sourcesStrings);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        sourcesAdapter.setPopupLayoutView(R.layout.view_spinner_popup);
        source_dropdown.setAdapter(sourcesAdapter);
        source_dropdown.setSelection(0);

        area_date_indicator = view.findViewById(R.id.area_date_indicator);
        housing_society = view.findViewById(R.id.housing_society);
        //housing_society.setOnClickListener(this);
        clearHousingSociety = view.findViewById(R.id.clearHousingSociety);
        delivery_area = view.findViewById(R.id.delivery_area);
        //delivery_area.setOnClickListener(this);
        clearDeliveryArea = view.findViewById(R.id.clearDeliveryArea);
        clearDeliveryArea.setOnClickListener(this);
        delivery_zone = view.findViewById(R.id.delivery_zone);
        delivery_zone.setOnClickListener(this);
        clearDeliveryZone = view.findViewById(R.id.clearDeliveryZone);
        clearDeliveryZone.setOnClickListener(this);
        delivery_date = view.findViewById(R.id.delivery_date);
        delivery_date.setOnClickListener(this);
        clearDeliveryDate = view.findViewById(R.id.clearDeliveryDate);
        clearDeliveryDate.setOnClickListener(this);
        delivery_time = view.findViewById(R.id.delivery_time);
        delivery_time.setOnClickListener(this);
        clearDeliveryTime = view.findViewById(R.id.clearDeliveryTime);
        clearDeliveryTime.setOnClickListener(this);
        feedback_date = view.findViewById(R.id.feedback_date);
        feedback_date.setOnClickListener(this);
        clearFeedbackDate = view.findViewById(R.id.clearFeedbackDate);
        clearFeedbackDate.setOnClickListener(this);
        collection_date = view.findViewById(R.id.collection_date);
        collection_date.setOnClickListener(this);
        clearCollectionDate = view.findViewById(R.id.clearCollectionDate);
        clearCollectionDate.setOnClickListener(this);
        consolidation_date = view.findViewById(R.id.consolidation_date);
        consolidation_date.setOnClickListener(this);
        clearConsolidationDate = view.findViewById(R.id.clearConsolidationDate);
        clearConsolidationDate.setOnClickListener(this);

        chickenItems = view.findViewById(R.id.chickenItems);
        chickenItems.setOnCheckedChangeListener(this);
        muttonItems = view.findViewById(R.id.muttonItems);
        muttonItems.setOnCheckedChangeListener(this);
        seafoodItems = view.findViewById(R.id.seafoodItems);
        seafoodItems.setOnCheckedChangeListener(this);

        orderMoneyCollection = view.findViewById(R.id.orderMoneyCollection);
        orderMoneyCollection.setOnCheckedChangeListener(this);
        outstandingMoneyCollection = view.findViewById(R.id.outstandingMoneyCollection);
        outstandingMoneyCollection.setOnCheckedChangeListener(this);

        inputStock = view.findViewById(R.id.inputStock);
        inputStock.setOnCheckedChangeListener(this);
        purchaseStock = view.findViewById(R.id.purchaseStock);
        purchaseStock.setOnCheckedChangeListener(this);
        consolidationStock = view.findViewById(R.id.consolidationStock);
        consolidationStock.setOnCheckedChangeListener(this);
        adjustmentStock = view.findViewById(R.id.adjustmentStock);
        adjustmentStock.setOnCheckedChangeListener(this);
        wastageStock = view.findViewById(R.id.wastageStock);
        wastageStock.setOnCheckedChangeListener(this);
        orderStock = view.findViewById(R.id.orderStock);
        orderStock.setOnCheckedChangeListener(this);
        notOrderStock = view.findViewById(R.id.notOrderStock);
        notOrderStock.setOnCheckedChangeListener(this);

        lowRated = view.findViewById(R.id.lowRated);
        lowRated.setOnCheckedChangeListener(this);
        averageRated = view.findViewById(R.id.averageRated);
        averageRated.setOnCheckedChangeListener(this);
        highRated = view.findViewById(R.id.highRated);
        highRated.setOnCheckedChangeListener(this);

        withComments = view.findViewById(R.id.withComments);
        withComments.setOnCheckedChangeListener(this);
        withoutComments = view.findViewById(R.id.withoutComments);
        withoutComments.setOnCheckedChangeListener(this);

        customer_name = view.findViewById(R.id.customer_name);
        clearCustomerName = view.findViewById(R.id.clearCustomerName);
        clearCustomerName.setOnClickListener(this);
        item_name = view.findViewById(R.id.item_name);
        clearItemName = view.findViewById(R.id.clearItemName);
        clearItemName.setOnClickListener(this);
        item_prefs = view.findViewById(R.id.item_prefs);
        clearItemPrefs = view.findViewById(R.id.clearItemPrefs);
        clearItemPrefs.setOnClickListener(this);
        processor_name = view.findViewById(R.id.processor_name);
        clearProcessorName = view.findViewById(R.id.clearProcessorName);
        clearProcessorName.setOnClickListener(this);
        delivery_boy_name = view.findViewById(R.id.delivery_boy_name);
        clearDeliveryBoyName = view.findViewById(R.id.clearDeliveryBoyName);
        clearDeliveryBoyName.setOnClickListener(this);
        approver_name = view.findViewById(R.id.consolidator_name);
        clearApproverName = view.findViewById(R.id.clearApproverName);
        clearApproverName.setOnClickListener(this);
        vendor_name = view.findViewById(R.id.vendor_name);
        vendor_name.setOnClickListener(this);
        clearVendorName = view.findViewById(R.id.clearVendorName);
        clearVendorName.setOnClickListener(this);
        butcher_name = view.findViewById(R.id.butcher_name);
        clearButcherName = view.findViewById(R.id.clearButcherName);
        clearButcherName.setOnClickListener(this);

        collectAmount = view.findViewById(R.id.collectAmount);
        collectAmount.setOnCheckedChangeListener(this);
        returnAmount = view.findViewById(R.id.returnAmount);
        returnAmount.setOnCheckedChangeListener(this);

        collectionMatched = view.findViewById(R.id.collectionMatched);
        collectionMatched.setOnCheckedChangeListener(this);
        collectionMismatched = view.findViewById(R.id.collectionMismatched);
        collectionMismatched.setOnCheckedChangeListener(this);

        min_amount = view.findViewById(R.id.min_amount);
        clearMinAmount = view.findViewById(R.id.clearMinAmount);
        clearMinAmount.setOnClickListener(this);
        max_amount = view.findViewById(R.id.max_amount);
        clearMaxAmount = view.findViewById(R.id.clearMaxAmount);
        clearMaxAmount.setOnClickListener(this);
        withDiscount = view.findViewById(R.id.withDiscount);
        withOuststanding = view.findViewById(R.id.withOuststanding);

        onlinePayment = view.findViewById(R.id.onlinePayment);
        onlinePayment.setOnCheckedChangeListener(this);
        cashOnDelivery = view.findViewById(R.id.cashOnDelivery);
        cashOnDelivery.setOnCheckedChangeListener(this);
        cashAndOnlinePayment = view.findViewById(R.id.cashAndOnlinePayment);
        cashAndOnlinePayment.setOnCheckedChangeListener(this);
        negativeCOD = view.findViewById(R.id.negativeCOD);
        negativeCOD.setOnCheckedChangeListener(this);

        preOrder = view.findViewById(R.id.preOrder);
        preOrder.setOnCheckedChangeListener(this);
        sameDayOrder = view.findViewById(R.id.sameDayOrder);
        sameDayOrder.setOnCheckedChangeListener(this);

        referralOrder = view.findViewById(R.id.referralOrder);
        firstOrderFollowup = view.findViewById(R.id.firstOrderFollowup);
        followupResultedOrder = view.findViewById(R.id.followupResultedOrder);
        firstTimeOrder = view.findViewById(R.id.firstTimeOrder);
        pendingBillPrint = view.findViewById(R.id.pendingBillPrint);
        withDeliveryAreaUnAssigned = view.findViewById(R.id.withDeliveryAreaUnAssigned);
        inCustomerSupport = view.findViewById(R.id.inCustomerSupport);
        cashNCarryItems = view.findViewById(R.id.cashNCarryItems);
        withDelay = view.findViewById(R.id.withDelay);
        withSpecialInstructions = view.findViewById(R.id.withSpecialInstructions);
        createdBySuperUser = view.findViewById(R.id.createdBySuperUser);

        comments = view.findViewById(R.id.comments);
        clearComments = view.findViewById(R.id.clearComments);
        clearComments.setOnClickListener(this);

        clearFilter = view.findViewById(R.id.clearFilter);
        clearFilter.setOnClickListener(this);
        backgroundReload = view.findViewById(R.id.backgroundReload);
        backgroundReload.setOnClickListener(this);
        filterData = view.findViewById(R.id.filterData);
        filterData.setOnClickListener(this);


        //----------

        // IF FEEDBACKS PAGE
        if ( ActivityMethods.isFeedbacksActivity(getActivity()) || ActivityMethods.isFeedbacksByDateActivity(getActivity()) ){
            area_date_indicator.setText("Date");
            toggleVisibility(new View[]{backgroundReload, view.findViewById(R.id.statusView), view.findViewById(R.id.sourceView), view.findViewById(R.id.deliveryAreaView), view.findViewById(R.id.deliveryZoneView), view.findViewById(R.id.housingSocietyView),
                    view.findViewById(R.id.deliveryTimeView), view.findViewById(R.id.collectionDateView), view.findViewById(R.id.consolidationDateView),
                    view.findViewById(R.id.withOuststanding), view.findViewById(R.id.consolidatorView),
                    view.findViewById(R.id.moneyCollectionTypeView), view.findViewById(R.id.stockTypeView),
                    view.findViewById(R.id.collectionView), view.findViewById(R.id.matchView),
                    firstOrderFollowup, pendingBillPrint, withDeliveryAreaUnAssigned, inCustomerSupport}, false);
        }

        // IF FOLLOWUPS PAGE
        else if ( ActivityMethods.isFollowupsActivity(getActivity()) ){
            //area_date_indicator.setText("Date");
            //feedback_date.setHint("Date");
            processor_name.setHint("Employee Name");
            toggleVisibility(new View[]{backgroundReload, view.findViewById(R.id.areaDateTimeView), view.findViewById(R.id.statusView), view.findViewById(R.id.sourceView)
                    //, view.findViewById(R.id.deliveryZoneView), view.findViewById(R.id.deliveryAreaView), view.findViewById(R.id.housingSocietyView), view.findViewById(R.id.deliveryDateView) , view.findViewById(R.id.deliveryTimeView)
                    , view.findViewById(R.id.withOuststanding)
                    , view.findViewById(R.id.categoryView), view.findViewById(R.id.moneyCollectionTypeView), view.findViewById(R.id.stockTypeView), view.findViewById(R.id.ratingView), view.findViewById(R.id.commentsView)
                    , view.findViewById(R.id.itemNameView), view.findViewById(R.id.itemPrefsView), view.findViewById(R.id.deliveryBoyView), view.findViewById(R.id.consolidatorView), view.findViewById(R.id.vendorView), view.findViewById(R.id.butcherView)
                    , view.findViewById(R.id.amountGroup), view.findViewById(R.id.paymentFilterGroup), view.findViewById(R.id.orderDayFilterView), view.findViewById(R.id.collectionView), view.findViewById(R.id.matchView)
                    , /*view.findViewById(R.id.moreView)*/referralOrder, firstTimeOrder, withDeliveryAreaUnAssigned, pendingBillPrint, inCustomerSupport, cashNCarryItems, withDelay, withSpecialInstructions, createdBySuperUser
                    , view.findViewById(R.id.detailsView)}, false);
        }

        // IF MONEY COLLECTION PAGE
        else if ( ActivityMethods.isMoneyCollectionActivity(getActivity()) ){
            area_date_indicator.setText("Date:");
            delivery_date.setHint("Order date");
            toggleVisibility(new View[]{backgroundReload, view.findViewById(R.id.statusView), view.findViewById(R.id.sourceView), view.findViewById(R.id.deliveryZoneView), view.findViewById(R.id.deliveryAreaView), view.findViewById(R.id.housingSocietyView),
                    view.findViewById(R.id.feedbackDateView), view.findViewById(R.id.deliveryTimeView),view.findViewById(R.id.stockTypeView),
                    view.findViewById(R.id.categoryView), view.findViewById(R.id.ratingView), view.findViewById(R.id.commentsView),
                    view.findViewById(R.id.itemNameView), view.findViewById(R.id.itemPrefsView),view.findViewById(R.id.processorView), view.findViewById(R.id.vendorView), view.findViewById(R.id.butcherView),
                    view.findViewById(R.id.orderDayFilterView), view.findViewById(R.id.moreView), view.findViewById(R.id.detailsView)}, false);

        }

        // IF STOCK PAGE
        else if ( ActivityMethods.isStockStatementActivity(getActivity()) ){
            area_date_indicator.setText("Date:");
            delivery_date.setHint("Date");
            toggleVisibility(new View[]{backgroundReload, view.findViewById(R.id.statusView), view.findViewById(R.id.sourceView), view.findViewById(R.id.deliveryZoneView), view.findViewById(R.id.deliveryAreaView), view.findViewById(R.id.housingSocietyView),
                    view.findViewById(R.id.feedbackDateView), view.findViewById(R.id.deliveryTimeView), view.findViewById(R.id.collectionDateView), view.findViewById(R.id.consolidationDateView),
                    view.findViewById(R.id.collectionView), view.findViewById(R.id.moneyCollectionTypeView), view.findViewById(R.id.matchView),
                    view.findViewById(R.id.ratingView), view.findViewById(R.id.commentsView),
                    view.findViewById(R.id.customerNameView), view.findViewById(R.id.processorView), view.findViewById(R.id.deliveryBoyView),view.findViewById(R.id.consolidatorView),
                    view.findViewById(R.id.paymentFilterGroup), view.findViewById(R.id.amountGroup), view.findViewById(R.id.processorView),
                    view.findViewById(R.id.orderDayFilterView), view.findViewById(R.id.moreView), view.findViewById(R.id.detailsView)}, false);
        }

        // IF ORDERS/ORDER ITEMS PAGES
        else {
            toggleVisibility(new View[]{view.findViewById(R.id.categoryView), view.findViewById(R.id.ratingView), view.findViewById(R.id.collectionView), view.findViewById(R.id.moneyCollectionTypeView), view.findViewById(R.id.matchView),
                    view.findViewById(R.id.feedbackDateView), view.findViewById(R.id.collectionDateView), view.findViewById(R.id.consolidationDateView),
                    view.findViewById(R.id.commentsView), view.findViewById(R.id.consolidatorView), view.findViewById(R.id.stockTypeView), view.findViewById(R.id.detailsView),
                    firstOrderFollowup}, false);

            //--------

            if (ActivityMethods.isOrdersByDateActivity(getActivity())) {
                backgroundReload.setVisibility(View.GONE);
            }
            if (ActivityMethods.isNewOrdersActivity(getActivity())
                    || ActivityMethods.isPendingDeliveriesActivity(getActivity())
                    || ActivityMethods.isMyDeliveriesActivity(getActivity())) {
                view.findViewById(R.id.statusView).setVisibility(View.GONE);
            }
            if (ActivityMethods.isFutureOrdersActivity(getActivity())
                    || ActivityMethods.isNewOrdersActivity(getActivity())
                    || ActivityMethods.isPendingProcessingActivity(getActivity()) ) {
                view.findViewById(R.id.vendorView).setVisibility(View.GONE);
                view.findViewById(R.id.butcherView).setVisibility(View.GONE);
                view.findViewById(R.id.deliveryBoyView).setVisibility(View.GONE);
                view.findViewById(R.id.processorView).setVisibility(View.GONE);
            }

            if ( ActivityMethods.isFutureOrdersActivity(getActivity()) == false ) {
                area_date_indicator.setText("Zone-Area-Time");
                view.findViewById(R.id.deliveryDateView).setVisibility(View.GONE);
            }

            if (ActivityMethods.isNewOrdersActivity(getActivity())
                    || ActivityMethods.isPendingProcessingActivity(getActivity())
                    || ActivityMethods.isMyDeliveriesActivity(getActivity())) {
                view.findViewById(R.id.statusView).setVisibility(View.GONE);
            }

            if (ActivityMethods.isFutureOrdersActivity(getActivity())) {
                pendingBillPrint.setVisibility(View.GONE);
            }
            /*if (ActivityMethods.isMyDeliveriesActivity(getActivity())) {//TODO
                analyseData.setVisibility(View.GONE);
            }*/
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton view, boolean isChecked) {

        if ( isChecked ){
            if ( view == inputStock || view == purchaseStock || view == consolidationStock || view == adjustmentStock || view == wastageStock || view == orderStock || view == notOrderStock ){
                List<CheckBox> views = new ArrayList<>();
                views.add(inputStock);
                views.add(purchaseStock);
                views.add(consolidationStock);
                views.add(adjustmentStock);
                views.add(wastageStock);
                views.add(orderStock);
                views.add(notOrderStock);

                views.remove(view);
                for (int i=0;i<views.size();i++){
                    views.get(i).setChecked(false);
                }
            }

            else if ( view == orderMoneyCollection ){
                outstandingMoneyCollection.setChecked(false);
            }
            else if ( view == outstandingMoneyCollection ){
                orderMoneyCollection.setChecked(false);
            }

            else if ( view == chickenItems ){
                muttonItems.setChecked(false);
                seafoodItems.setChecked(false);
            }else if ( view == muttonItems ){
                chickenItems.setChecked(false);
                seafoodItems.setChecked(false);
            }else if ( view == seafoodItems ){
                chickenItems.setChecked(false);
                muttonItems.setChecked(false);
            }

            if ( view == lowRated ){
                averageRated.setChecked(false);
                highRated.setChecked(false);
            }else if ( view == averageRated ){
                lowRated.setChecked(false);
                highRated.setChecked(false);
            }else if ( view == highRated ){
                lowRated.setChecked(false);
                averageRated.setChecked(false);
            }

            if ( view == withComments ){
                withoutComments.setChecked(false);
            }else if ( view == withoutComments ){
                withComments.setChecked(false);
            }

            if ( view == collectAmount ){
                returnAmount.setChecked(false);
            }else if ( view == returnAmount ){
                collectAmount.setChecked(false);
            }

            if ( view == collectionMatched ){
                collectionMismatched.setChecked(false);
            }else if ( view == collectionMismatched ){
                collectionMatched.setChecked(false);
            }

            if ( view == onlinePayment ){
                cashOnDelivery.setChecked(false);
                cashAndOnlinePayment.setChecked(false);
                negativeCOD.setChecked(false);
            }else if ( view == cashOnDelivery ){
                onlinePayment.setChecked(false);
                cashAndOnlinePayment.setChecked(false);
                negativeCOD.setChecked(false);
            }else if ( view == cashAndOnlinePayment ){
                onlinePayment.setChecked(false);
                cashOnDelivery.setChecked(false);
                negativeCOD.setChecked(false);
            }else if ( view == negativeCOD ){
                onlinePayment.setChecked(false);
                cashOnDelivery.setChecked(false);
                cashAndOnlinePayment.setChecked(false);
            }

            else if ( view == preOrder ){
                sameDayOrder.setChecked(false);
            }else if ( view == sameDayOrder ){
                preOrder.setChecked(false);
            }
        }

    }


    @Override
    public void onClick(View view) {

        if ( view == clearCustomerName ){
            customer_name.setText("");
        }
        else if ( view == clearItemName ){
            item_name.setText("");
        }
        else if ( view == clearItemPrefs ){
            item_prefs.setText("");
        }
        else if ( view == clearProcessorName ){
            processor_name.setText("");
        }
        else if ( view == clearDeliveryBoyName ){
            delivery_boy_name.setText("");
        }
        else if ( view == clearApproverName){
            approver_name.setText("");
        }
        else if ( view == clearVendorName ){
            vendor_name.setText("");
        }
        else if ( view == clearButcherName ){
            butcher_name.setText("");
        }


        else if ( view == clearHousingSociety ){
            housing_society.setText("");
        }
        else if ( view == clearDeliveryArea ){
            delivery_area.setText("");
        }
        else if ( view == clearDeliveryZone ){
            delivery_zone.setText("");
        }
        else if ( view == clearDeliveryDate ){
            delivery_date.setText("");
        }
        else if ( view == clearDeliveryTime ){
            delivery_time.setText("");
        }
        else if ( view == clearFeedbackDate ){
            feedback_date.setText("");
        }
        else if ( view == clearCollectionDate ){
            collection_date.setText("");
        }
        else if ( view == clearConsolidationDate ){
            consolidation_date.setText("");
        }

        else if ( view == clearMinAmount ){
            min_amount.setText("");
        }
        else if ( view == clearMaxAmount ){
            max_amount.setText("");
        }

        else if ( view == clearComments ){
            comments.setText("");
        }


        else if ( view == delivery_date ){
            String initialDate = delivery_date.getText().toString();
            if ( initialDate == null || initialDate.length() == 0 ) {
                initialDate = DateMethods.getCurrentDate();
            }
            showDatePickerDialog(delivery_date.getHint().toString(), initialDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    delivery_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }

        else if ( view == delivery_time ){
            /*String initialTime = delivery_time.getText().toString();
            if ( initialTime == null || initialTime.length() == 0 ) {
                initialTime = DateMethods.getOnlyTime(DateMethods.getCurrentDateAndTime());
            }*/
            showTimePickerDialog("Delivery slot end time\nEx: 10AM for 8AM to 10AM slot", /*initialTime*/"10:00 AM", new TimePickerCallback() {
                @Override
                public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
                    delivery_time.setText(fullTimein12HrFormat);
                }
            });
        }

        else if ( view == delivery_zone ){
            new RoutesFlow(activity).selectDeliveryZone(false, delivery_zone.getText().toString(), new RoutesFlow.DeliveryZoneSelectionCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, String zoneID) {
                    delivery_zone.setText(zoneID);
                }
            });
        }

        /*else if ( view == delivery_area ){
            new RoutesFlow(activity).selectDeliveryArea(null, new RoutesFlow.DeliveryAreaSelectionCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, DeliveryAreaDetails deliveryAreaDetails) {
                    delivery_area.setText(deliveryAreaDetails.getID());
                }
            });
        }*/

        else if ( view == feedback_date ){
            String initialDate = feedback_date.getText().toString();
            if ( feedback_date == null || feedback_date.length() == 0 ) {
                initialDate = DateMethods.getCurrentDate();
            }
            showDatePickerDialog("Feedback date", null, DateMethods.getCurrentDate(), initialDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    feedback_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }

        else if ( view == collection_date ){
            String initialDate = collection_date.getText().toString();
            if ( collection_date == null || collection_date.length() == 0 ) {
                initialDate = DateMethods.getCurrentDate();
            }
            showDatePickerDialog("Collection date", null, DateMethods.getCurrentDate(), initialDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    collection_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }

        else if ( view == consolidation_date ){
            String initialDate = consolidation_date.getText().toString();
            if ( consolidation_date == null || consolidation_date.length() == 0 ) {
                initialDate = DateMethods.getCurrentDate();
            }
            showDatePickerDialog("Consolidation date", null, DateMethods.getCurrentDate(), initialDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    consolidation_date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                }
            });
        }

        else if ( view == vendor_name ){
            showLoadingDialog("Loading vendors, wait...");
            getBackendAPIs().getVendorsAndButchersAPI().getVendors(new VendorsAndButchersAPI.VendorsCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message, List<Vendor> vendorsList) {
                    closeLoadingDialog();
                    if ( status ){
                        final List<String> vendorsStrings = new ArrayList<>();
                        for (int i=0;i<vendorsList.size();i++){
                            vendorsStrings.add(vendorsList.get(i).getName());
                        }
                        showListChooserDialog("Select vendor", vendorsStrings, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                vendor_name.setText(vendorsStrings.get(position));
                            }
                        });
                    }else{
                        showToastMessage("Unable to load vendors try again");
                    }
                }
            });
        }

        else if ( view == clearFilter ){
            clearFilter.setVisibility(View.GONE);
            clearSelection();
            if (callback != null) {
                callback.onItemSelected(-1, new SearchDetails().setAction(Constants.CLEAR));
            }
        }
        else if ( view == backgroundReload ){
            if (callback != null) {
                callback.onItemSelected(-1, new SearchDetails().setAction(Constants.BACKGROUND_RELOAD));
            }
        }

        else if ( view == filterData){
            inputMethodManager.hideSoftInputFromWindow(customer_name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(processor_name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
            inputMethodManager.hideSoftInputFromWindow(delivery_boy_name.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..

            if ( callback != null ){
                SearchDetails searchDetails = new SearchDetails();
                searchDetails.setAction(Constants.FILTER);

                if ( statusStrings.get(status_dropdown.getSelectedItemPosition()).equalsIgnoreCase(Constants.ALL) == false ){
                    searchDetails.setStatus(statusStrings.get(status_dropdown.getSelectedItemPosition()));
                }

                if ( sourcesStrings.get(source_dropdown.getSelectedItemPosition()).equalsIgnoreCase(Constants.ALL) == false ){
                    searchDetails.setSource(sourcesStrings.get(source_dropdown.getSelectedItemPosition()));
                }

                if ( housing_society.getText() != null && housing_society.getText().toString().trim().length() > 0 ){
                    searchDetails.setHousingSociety(housing_society.getText().toString());
                }
                if ( delivery_area.getText() != null && delivery_area.getText().toString().trim().length() > 0 ){
                    searchDetails.setDeliveryArea(delivery_area.getText().toString());
                }
                if ( delivery_zone.getText() != null && delivery_zone.getText().toString().trim().length() > 0 ){
                    searchDetails.setDeliveryZone(delivery_zone.getText().toString());
                }
                if ( delivery_date.getText() != null && delivery_date.getText().toString().trim().length() > 0 ){
                    searchDetails.setDeliveryDate(delivery_date.getText().toString());
                }
                if ( delivery_time.getText() != null && delivery_time.getText().toString().trim().length() > 0 ){
                    searchDetails.setDeliveryTime(delivery_time.getText().toString());
                }
                if ( feedback_date.getText() != null && feedback_date.getText().toString().trim().length() > 0 ){
                    searchDetails.setFeedbackDate(feedback_date.getText().toString());
                }
                if ( collection_date.getText() != null && collection_date.getText().toString().trim().length() > 0 ){
                    searchDetails.setCollectionDate(collection_date.getText().toString());
                }
                if ( consolidation_date.getText() != null && consolidation_date.getText().toString().trim().length() > 0 ){
                    searchDetails.setConsolidationDate(consolidation_date.getText().toString());
                }

                if ( chickenItems.isChecked() ){
                    searchDetails.setCategoryType(Constants.CHICKEN);
                }else if ( muttonItems.isChecked() ){
                    searchDetails.setCategoryType(Constants.MUTTON);
                }else if ( seafoodItems.isChecked() ){
                    searchDetails.setCategoryType(Constants.SEAFOOD);
                }

                if ( orderMoneyCollection.isChecked() ){
                    searchDetails.setMoneyCollectionType(Constants.ORDER);
                }else if ( outstandingMoneyCollection.isChecked() ){
                    searchDetails.setMoneyCollectionType(Constants.OUTSTANDING);
                }

                if ( inputStock.isChecked() ){
                    searchDetails.setStockType(Constants.INPUT);
                }else if ( purchaseStock.isChecked() ){
                    searchDetails.setStockType(Constants.PURCHASE);
                }else if ( consolidationStock.isChecked() ){
                    searchDetails.setStockType(Constants.CONSOLIDATION);
                }else if ( adjustmentStock.isChecked() ){
                    searchDetails.setStockType(Constants.ADJUSTMENT);
                }else if ( wastageStock.isChecked() ){
                    searchDetails.setStockType(Constants.WASTAGE);
                }else if ( orderStock.isChecked() ){
                    searchDetails.setStockType(Constants.ORDER);
                }else if ( notOrderStock.isChecked() ){
                    searchDetails.setStockType(Constants.NOT_ORDER);
                }

                if ( lowRated.isChecked() ){
                    searchDetails.setRatingType(1);
                }else if ( averageRated.isChecked() ){
                    searchDetails.setRatingType(3);
                }else if ( highRated.isChecked() ){
                    searchDetails.setRatingType(5);
                }

                if ( withComments.isChecked() ){
                    searchDetails.setCommentsType(Constants.WITH_COMMENTS);
                }else if ( withoutComments.isChecked() ){
                    searchDetails.setCommentsType(Constants.WITHOUT_COMMENTS);
                }

                if ( customer_name.getText() != null && customer_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setCustomerName(customer_name.getText().toString());
                }
                if ( item_name.getText() != null && item_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setItemName(item_name.getText().toString());
                }
                if ( item_prefs.getText() != null && item_prefs.getText().toString().trim().length() > 0 ){
                    searchDetails.setItemPreferences(item_prefs.getText().toString());
                }
                if ( processor_name.getText() != null && processor_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setProcessorName(processor_name.getText().toString());
                }
                if ( delivery_boy_name.getText() != null && delivery_boy_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setDeliveryBoyName(delivery_boy_name.getText().toString());
                }
                if ( approver_name.getText() != null && approver_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setConsolidatorName(approver_name.getText().toString());
                }
                if ( vendor_name.getText() != null && vendor_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setVendorName(vendor_name.getText().toString());
                }
                if ( butcher_name.getText() != null && butcher_name.getText().toString().trim().length() > 0 ){
                    searchDetails.setButcherName(butcher_name.getText().toString());
                }

                if ( collectAmount.isChecked() ){
                    searchDetails.setCollectionType(Constants.COLLECT);
                }else if ( returnAmount.isChecked() ){
                    searchDetails.setCollectionType(Constants.RETURN);
                }

                if ( collectionMatched.isChecked() ){
                    searchDetails.setMatchType(Constants.MATCHED);
                }else if ( collectionMismatched.isChecked() ){
                    searchDetails.setMatchType(Constants.MIS_MATCHED);
                }

                if ( min_amount.getText() != null && min_amount.getText().toString().trim().length() > 0 ){
                    searchDetails.setMinAmount(Double.parseDouble(min_amount.getText().toString()));
                }
                if ( max_amount.getText() != null && max_amount.getText().toString().trim().length() > 0 ){
                    searchDetails.setMaxAmount(Double.parseDouble(max_amount.getText().toString()));
                }
                if ( withDiscount.isChecked() ){
                    searchDetails.setDiscountType(Constants.WITH_DISCOUNT);
                }
                if ( withOuststanding.isChecked() ){
                    searchDetails.setOutstandingType(Constants.WITH_OUTSTANDING);
                }

                if ( onlinePayment.isChecked() ){
                    searchDetails.setPaymentType(Constants.ONLINE_PAYMENT);
                }else if ( cashOnDelivery.isChecked() ){
                    searchDetails.setPaymentType(Constants.CASH_ON_DELIVERY);
                }else if ( cashAndOnlinePayment.isChecked() ){
                    searchDetails.setPaymentType(Constants.COD_AND_OP);
                }else if ( negativeCOD.isChecked() ){
                    searchDetails.setPaymentType(Constants.NEGATIVE_COD);
                }

                if ( preOrder.isChecked() ){
                    searchDetails.setOrderDayType(Constants.PRE_ORDER);
                }else if ( sameDayOrder.isChecked() ){
                    searchDetails.setOrderDayType(Constants.SAME_DAY_ORDER);
                }

                if ( referralOrder.isChecked() ){
                    searchDetails.setReferralOrderType(Constants.REFERRAL_ORDERS);
                }
                if ( firstOrderFollowup.isChecked() ){
                    searchDetails.setFollowupType(Constants.FIRST_ORDER);
                }
                if ( followupResultedOrder.isChecked() ){
                    searchDetails.setFollowupResultedOrderType(Constants.FOLLOWUP_RESULTED_ORDERS);
                }
                if ( firstTimeOrder.isChecked() ){
                    searchDetails.setFirstTimeOrderType(Constants.FIRST_TIME_ORDERS);
                }
                if ( pendingBillPrint.isChecked() ){
                    searchDetails.setBillPrintType(Constants.BILL_NOT_PRINTED);
                }
                if ( withDeliveryAreaUnAssigned.isChecked() ){
                    searchDetails.setDeliveryAreaAssignType(Constants.UNASSIGNED_DELIVERY_AREA);
                }
                if ( inCustomerSupport.isChecked() ){
                    searchDetails.setCustomerSupportType(Constants.IN_CUSTOMER_SUPPORT);
                }
                if ( cashNCarryItems.isChecked() ){
                    searchDetails.setCashNCarryType(Constants.CASH_N_CARRY);
                }
                if ( withDelay.isChecked() ){
                    searchDetails.setDelayType(Constants.WITH_DELAY);
                }
                if ( withSpecialInstructions.isChecked() ){
                    searchDetails.setInstructionsType(Constants.WITH_INSTRUCTIONS);
                }
                if ( createdBySuperUser.isChecked() ){
                    searchDetails.setCreatedBy(Constants.SUPER_USER);
                }

                if ( comments.getText() != null && comments.getText().toString().trim().length() > 0 ){
                    searchDetails.setComments(comments.getText().toString());
                }

                if ( searchDetails.isSearchable() ) {
                    clearFilter.setVisibility(View.VISIBLE);
                    callback.onItemSelected(-1, searchDetails);
                }else{
                    clearFilter.setVisibility(View.GONE);
                    showToastMessage("Please select your choices.");
                }
            }
        }
    }

    public void setItemSelectionListener(Callback mCallback) {
        this.callback = mCallback;
    }

    public void updateCustomerName(String customerName){
        customer_name.setText(customerName);
    }

    public void clearSelection(){

        try {
            clearFilter.setVisibility(View.GONE);

            status_dropdown.setSelection(0);

            source_dropdown.setSelection(0);


            housing_society.setText("");
            delivery_area.setText("");
            delivery_date.setText("");
            delivery_time.setText("");
            feedback_date.setText("");
            collection_date.setText("");
            consolidation_date.setText("");

            chickenItems.setChecked(false);
            muttonItems.setChecked(false);
            seafoodItems.setChecked(false);

            orderMoneyCollection.setChecked(false);
            outstandingMoneyCollection.setChecked(false);

            inputStock.setChecked(false);
            purchaseStock.setChecked(false);
            consolidationStock.setChecked(false);
            adjustmentStock.setChecked(false);
            wastageStock.setChecked(false);
            orderStock.setChecked(false);
            notOrderStock.setChecked(false);

            lowRated.setChecked(false);
            averageRated.setChecked(false);
            highRated.setChecked(false);

            withComments.setChecked(false);
            withoutComments.setChecked(false);

            customer_name.setText("");
            item_name.setText("");
            item_prefs.setText("");
            processor_name.setText("");
            delivery_boy_name.setText("");
            approver_name.setText("");
            vendor_name.setText("");

            collectAmount.setChecked(false);
            returnAmount.setChecked(false);

            collectionMatched.setChecked(false);
            collectionMismatched.setChecked(false);

            min_amount.setText("");
            max_amount.setText("");
            withDiscount.setChecked(false);
            withOuststanding.setChecked(false);

            onlinePayment.setChecked(false);
            cashOnDelivery.setChecked(false);
            cashAndOnlinePayment.setChecked(false);

            preOrder.setChecked(false);
            sameDayOrder.setChecked(false);

            referralOrder.setChecked(false);
            firstOrderFollowup.setChecked(false);
            followupResultedOrder.setChecked(false);
            firstTimeOrder.setChecked(false);
            pendingBillPrint.setChecked(false);
            inCustomerSupport.setChecked(false);
            cashNCarryItems.setChecked(false);
            withDelay.setChecked(false);
            withSpecialInstructions.setChecked(false);
            createdBySuperUser.setChecked(false);

        }catch (Exception e){}
    }

    //----------

    public void hideDeliveryBoyView(){
        try{    layoutView.findViewById(R.id.deliveryBoyView).setVisibility(View.GONE); }catch (Exception e){}
    }
    public void hideCustomerView(){
        try{    layoutView.findViewById(R.id.customerNameView).setVisibility(View.GONE); }catch (Exception e){}
    }

    private void toggleVisibility(View []viewsList, boolean visibility){
        if ( viewsList != null && viewsList.length > 0 ){
            for (int i=0;i<viewsList.length;i++){
                viewsList[i].setVisibility(visibility?View.VISIBLE:View.GONE);
            }
        }
    }


}
