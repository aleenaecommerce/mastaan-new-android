package com.mastaan.logistics.printers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.brother.ptouch.sdk.LabelInfo;
import com.brother.ptouch.sdk.Printer;
import com.brother.ptouch.sdk.PrinterInfo;
import com.brother.ptouch.sdk.PrinterStatus;
import com.mastaan.logistics.localdata.LocalStorageData;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 24/11/18.
 */

public class LabelPrinter {

    vToolBarActivity activity;
    Context context;
    LocalStorageData localStorageData;
    String serverTime;


    public LabelPrinter(vToolBarActivity activity) {
        this.activity = activity;
        this.context = activity;
        this.localStorageData = new LocalStorageData(context);
        serverTime = DateMethods.getDateInFormat(localStorageData.getServerTime(), DateConstants.HH_MM_A);
    }


    public void printLabel(final String labelText, final StatusCallback callback){
        printLabel(labelText, 1, callback);
    }
    public void printLabel(final String labelText, int noOfCopies, final StatusCallback callback){
        printLabel(new String[]{labelText}, noOfCopies, callback);
    }

    public void printLabel(final String []labelTexts, final StatusCallback callback){
        printLabel(labelTexts, 1, callback);
    }
    public void printLabel(final String []labelTexts, int noOfCopies, final StatusCallback callback){
        printLabel(CommonMethods.getStringListFromStringArray(labelTexts), noOfCopies, callback);
    }

    public void printLabel(final List<String> labelTexts, final StatusCallback callback){
        printLabel(labelTexts, 1, callback);
    }
    public void printLabel(final List<String> labelTexts, final int noOfCopies, final StatusCallback callback){
        new AsyncTask<Void, Void, Void>() {
            boolean status = false;
            int statusCode = -2;
            String message = "Error";
            Bitmap labelBitmap;

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    if ( labelTexts != null && labelTexts.size() > 0 ){
                        String maxLengthText = labelTexts.get(0);
                        if ( labelTexts.size() > 1 ) {
                            for (int i = 1; i < labelTexts.size(); i++) {
                                if ( labelTexts.get(i).length() >= maxLengthText.length() ){
                                    maxLengthText = labelTexts.get(i);
                                }
                            }
                        }

                        int lineSize = 200;

                        Paint paint = new Paint();
                        paint.setTextSize(96);
                        paint.setColor(Color.WHITE);
                        paint.setTextAlign(Paint.Align.LEFT);
                        float baseline = -paint.ascent();   // ascent() is negative
                        int width = (int) (paint.measureText(maxLengthText) + 0.5f);  // round
                        int height = (int) (baseline + paint.descent() + 0.5f);

                        labelBitmap = Bitmap.createBitmap(width+1200, height+(labelTexts.size()*lineSize), Bitmap.Config.ARGB_8888);
                        Canvas canvas = new Canvas(labelBitmap);
                        canvas.drawRect(0, 0, width+1200, height+(labelTexts.size()*lineSize), paint);
                        paint.setColor(Color.BLACK);
                        for (int i=0;i<labelTexts.size();i++) {
                            boolean boldText = labelTexts.get(i).toLowerCase().contains("<b>")?true:false;
                            paint.setTextSize(boldText?(i==0?175:165):160);
                            paint.setTypeface(Typeface.create(Typeface.DEFAULT, boldText?Typeface.BOLD:Typeface.NORMAL));

                            canvas.drawText(labelTexts.get(i)
                                            .replaceAll("<b>","").replaceAll("<B>","")
                                            .replaceAll("</b>", "").replaceAll("</B>", "")
                                    , 0, baseline+100 + (i * lineSize), paint);
                        }
                    }
                }catch (Exception e){
                    status = false;
                    statusCode = -1;
                    message = "Error in creating label bitmap";
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( labelBitmap != null ){
                    printLabel(labelBitmap, noOfCopies, callback);
                }else{
                    if ( callback != null ){
                        callback.onComplete(status, statusCode, message);
                    }
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void printLabel(final Bitmap labelBitmap, final StatusCallback callback){
        printLabel(labelBitmap, 1, callback);
    }
    public void printLabel(final Bitmap labelBitmap, final int noOfCopies, final StatusCallback callback){
        new AsyncTask<Void, Void, Void>() {
            boolean status = false;
            int statusCode = -2;
            String mesage = "Error";

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    Printer myPrinter = new Printer();

                    PrinterInfo myPrinterInfo = new PrinterInfo();
                    myPrinterInfo.printerModel = PrinterInfo.Model.QL_720NW;
                    myPrinterInfo.ipAddress = new LocalStorageData(activity).getLabelPrinterIPAddress();//"192.168.0.38";
                    //myPrinterInfo.macAddress = "00:80:92:E0:B5:6D";
                    myPrinterInfo.port = PrinterInfo.Port.NET;
                    myPrinterInfo.paperSize = PrinterInfo.PaperSize.CUSTOM;
                    myPrinterInfo.orientation = PrinterInfo.Orientation.PORTRAIT;
                    myPrinterInfo.valign = PrinterInfo.VAlign.TOP;
                    myPrinterInfo.align = PrinterInfo.Align.LEFT;
                    //myPrinterInfo.printMode = PrinterInfo.PrintMode.ORIGINAL;
                    myPrinterInfo.printMode=PrinterInfo.PrintMode.FIT_TO_PAGE;
                    myPrinterInfo.numberOfCopies = noOfCopies;
                    myPrinterInfo.labelNameIndex = LabelInfo.QL700.W62H29.ordinal();//LabelInfo.QL700.W62H100.ordinal();
                    myPrinterInfo.isAutoCut = false;
                    myPrinterInfo.isCutAtEnd = true;
                    myPrinterInfo.isHalfCut = false;
                    myPrinterInfo.isSpecialTape = false;

                    myPrinter.setPrinterInfo(myPrinterInfo);

                    //----------

                    myPrinter.startCommunication();

                    PrinterStatus printResult = myPrinter.printImage(labelBitmap);
                    if ( printResult.errorCode == PrinterInfo.ErrorCode.ERROR_NONE ){
                        status = true;
                        statusCode = 200;
                        mesage = "Success";
                    }else{
                        status = false;
                        statusCode = -1;
                        mesage = printResult.errorCode.toString();
                    }

                    myPrinter.endCommunication();
                }
                catch (Exception e){
                    e.printStackTrace();
                    status = false;
                    statusCode = -1;
                    mesage = e.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(status, statusCode, mesage);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

}