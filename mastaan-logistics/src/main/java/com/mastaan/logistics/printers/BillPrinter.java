package com.mastaan.logistics.printers;

import android.content.Context;
import android.os.AsyncTask;
import androidx.annotation.NonNull;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.backend.models.RequestMarkAsBillsPrinted;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 05/07/17
 */

public class BillPrinter {

    MastaanToolbarActivity activity;
    Context context;
    LocalStorageData localStorageData;

    Printer printer;

    StatusCallback callback;

    String footerText;
    String serverTime;

    public BillPrinter(MastaanToolbarActivity activity) {
        this.activity = activity;
        this.context = activity;
        this.localStorageData = new LocalStorageData(context);
        serverTime = DateMethods.getDateInFormat(localStorageData.getServerTime(), DateConstants.HH_MM_A);
    }

    public BillPrinter setFooterText(String footerText) {
        this.footerText = footerText;
        return this;
    }

    public void start(@NonNull String orderID){
        start(orderID, null);
    }
    public void start(@NonNull String orderID, final StatusCallback callback){
        activity.showLoadingDialog(true, "Loading order details, wait...");
        activity.getBackendAPIs().getOrdersAPI().getOrderItems(orderID, new OrdersAPI.OrderDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderDetails> ordersList, List<OrderItemDetails> orderItemsList) {
                activity.closeLoadingDialog();
                if ( status ){
                    start(ordersList.get(0), callback);
                }else{
                    activity.showToastMessage("Something went wrong while loading order details, try again");
                    if ( callback != null ){
                        callback.onComplete(false, 500, "Error while loading order details");
                    }
                }
            }
        });
    }
    public void start(@NonNull OrderDetails orderDetails){
        start(orderDetails, null);
    }
    public void start(@NonNull OrderDetails orderDetails, StatusCallback callback){
        List<OrderDetails> ordersList = new ArrayList<>();
        if ( orderDetails != null ){
            ordersList.add(orderDetails);
        }
        start(ordersList, callback);
    }
    public void start(@NonNull List<OrderDetails> ordersList, StatusCallback callback){
        this.callback = callback;
        printBill(ordersList, Printer.TM_T88, localStorageData.getBillPrinterIPAddress()/*"192.168.0.12"*/);
    }


    //-------

    private void printBill(final List<OrderDetails> ordersList, final int printerModel, final String printerIPAddress){

        if ( ordersList != null && ordersList.size() > 0 ){
            activity.showLoadingDialog(true, "Printing bill"+(ordersList.size()>1?"s":"")+", wait...");
            new AsyncTask<Void, Void, Void>() {
                Response response = new Response(false, 500, "Error");

                @Override
                protected Void doInBackground(Void... voids) {

                    // CLOSE PRINTER IF ALREADY OPENED
                    closePrinter();

                    // CREATING PRINTER OBJECT
                    try {
                        printer = new Printer(printerModel/*Printer.TM_T88*/, Printer.MODEL_ANK, context);
                    }catch (Exception e){
                        response = new Response(false, 500, "Bill"+(ordersList.size()>1?"s":"")+" not printed, Message: "+getErrorMessage(e));
                        return null;
                    }

                    // SETTING PRINT LISTENER
                    printer.setReceiveEventListener(new ReceiveListener() {
                        @Override
                        public void onPtrReceive(final Printer prntr, int i, PrinterStatusInfo printerStatusInfo, String s) {
                            closePrinter();
                        }
                    });

                    // CREATING BILLS FOR ORDERS
                    for (int i=0;i<ordersList.size();i++){
                        // CREATING BILL FOR ORDER IF FAILS CLOSING OPERATION
                        if ( createBillForOrder(printer, ordersList.get(i)) == false ){
                            response = new Response(false, 500, "Bill"+(ordersList.size()>1?"s":"")+" not printed, Message: Error while creating bill"+(ordersList.size()>1?"s":"")+" template");
                            return null;
                        }
                    }

                    // CONNECTING PRINTER
                    try{
                        printer.connect("TCP:"+printerIPAddress/*"TCP:192.168.0.12"*/, Printer.PARAM_DEFAULT);
                    }catch (Exception e){
                        response = new Response(false, 500, "Bill"+(ordersList.size()>1?"s":"")+" not printed, Message: "+getErrorMessage(e));
                        return null;
                    }

                    // BEGINING TRANSACTION
                    try{
                        printer.beginTransaction();
                    }catch (Exception e){
                        response = new Response(false, 500, "Bill"+(ordersList.size()>1?"s":"")+" not printed, Message: "+getErrorMessage(e));
                        return null;
                    }

                    // IF PRINTER CONNECTED
                    if ( printer.getStatus().getConnection() != Printer.FALSE || printer.getStatus().getOnline() != Printer.FALSE ){
                        // SENDING DATA TO PRINTER
                        try{
                            printer.sendData(Printer.PARAM_DEFAULT);
                            response = new Response(true, 200, "Bill"+(ordersList.size()>1?"s":"")+" printed successfully");
                        }catch (Exception e){
                            response = new Response(false, 500, "Bill"+(ordersList.size()>1?"s":"")+" not printed, Message: "+getErrorMessage(e));
                            return null;
                        }
                    }
                    // IF PRINTER NOT CONNECTED
                    else{
                        response = new Response(false, 500, "Bill"+(ordersList.size()>1?"s":"")+" not printed, Message: Unable to establish connection with printer");
                        return null;
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    // CLOSING PRINTER
                    closePrinter();

                    StatusCallback statusCallback = new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            activity.closeLoadingDialog();
                            activity.showToastMessage(response.message);
                            if ( callback != null ){
                                callback.onComplete(response.status, response.statusCode, response.message);
                            }

                            if ( status == false ){
                                activity.showToastMessage("Somethig went wrong while marking bill as printed in the server");
                            }
                        }
                    };
                    if ( response.status ){
                        // MARKING AS BILL PRINTED IN THE SERVER
                        activity.getBackendAPIs().getOrdersAPI().markAsBillsPrinted(new RequestMarkAsBillsPrinted(ordersList), statusCallback);
                    }else{
                        statusCallback.onComplete(true, -1, "");
                    }
                }

                //------
                class Response{
                    boolean status;
                    int statusCode;
                    String message;
                    Response(boolean status, int statusCode, String message){
                        this.status = status;
                        this.statusCode = statusCode;
                        this.message = message;
                    }
                }
            }.execute();
        }
        else{
            activity.showToastMessage("No bill"+(ordersList.size()>1?"s":"")+" to print");
        }
    }

    private boolean createBillForOrder(Printer printer, OrderDetails orderDetails){
        try{
            if ( orderDetails.getStatus().equalsIgnoreCase(Constants.FAILED) == false ){
                //Bitmap iconData = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_bill_logo);
                printer.addTextAlign(Printer.ALIGN_CENTER);
                //printer.addImage(iconData, 0, 0, iconData.getWidth(), iconData.getHeight(), Printer.COLOR_1, Printer.MODE_MONO, Printer.HALFTONE_DITHER, Printer.PARAM_DEFAULT, Printer.COMPRESS_AUTO);
                printer.addLogo(32, 33);
                printer.addFeedLine(1);
                //printer.addTextFont(Printer.FONT_C);
                printer.addTextSize(3, 2);
                printer.addTextStyle(Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT, Printer.TRUE, Printer.PARAM_DEFAULT);
                printer.addText(new StringBuilder()
                        .append("MASTAAN\n")
                        .toString());
                printer.addTextSize(Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT);
                printer.addTextStyle(Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT);
                printer.addText(new StringBuilder()
                        .append("Meat Delivery Ka Superhero\n")
                        .append("------------------------------------------------\n")
                        .append("Sales Invoice\n")
                        .append("------------------------------------------------\n")
                        .toString());


                printer.addTextAlign(Printer.ALIGN_LEFT);
                printer.addText(new StringBuilder()
                        .append(String.format("%-27s%-32s\n", orderDetails.getReadableOrderID(), DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate(), DateConstants.MMM_DD_YYYY)+" "+orderDetails.getDeliveryTimeSlot())/*DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(orderDetails.getDeliveryDate()), DateConstants.MMM_DD_YYYY_HH_MM_A))*/).toString());

                printer.addText(new StringBuilder()
                        .append(CommonMethods.capitalizeStringWords(orderDetails.getCustomerName())+"\n")
                        .append(orderDetails.getCustomerMobile()+(orderDetails.getCustomerAlternateMobile().length()>0?", "+orderDetails.getCustomerAlternateMobile():"")+"\n")
                        .append(orderDetails.getCustomerEmail().length()>0?(orderDetails.getCustomerEmail()+"\n"):"")
                        .append(orderDetails.getDeliveryAddress()+"\n")
                        .append(orderDetails.getDeliveryAddressLandmark().trim().length()>0?("Landmark: "+orderDetails.getDeliveryAddressLandmark().trim()+"\n"):"")
                        .toString());

                printer.addText(new StringBuilder()
                        .append("------------------------------------------------\n")
                        .append("Item                  Rate     Qty      Amount\n")
                        .append("------------------------------------------------\n")
                        .toString());

                List<String> deliveryBoys = new ArrayList<>();
                for(int i=0;i<orderDetails.getOrderedItems().size();i++) {
                    OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(i);

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(String.format("%-22s%-9s%-9s%-17s\n"
                            , CommonMethods.getStringOfMaxSize(orderItemDetails.getMeatItemDetails().getNameWithSize(), 21)
                            , CommonMethods.getIndianFormatNumber(orderItemDetails.getAmountOfItem()/orderItemDetails.getQuantity())
                            , CommonMethods.getIndianFormatNumber(orderItemDetails.getQuantity())//+orderItemDetails.getMeatItemDetails().getQuantityUnit()+(orderItemDetails.getQuantity()!=1?"s":"")
                            , CommonMethods.getIndianFormatNumber(orderItemDetails.getAmountOfItem())
                                    +"\n"+orderItemDetails.getFormattedExtras()+(orderItemDetails.getStatusString().equalsIgnoreCase(Constants.FAILED)?" [FAILED]":""))
                    );
                    if ( orderItemDetails.getFinalNetWeight() > 0 ){
                        stringBuilder.append("Net weight: "+CommonMethods.getInDecimalFormat(orderItemDetails.getFinalNetWeight())+" kg"+(orderItemDetails.getFinalNetWeight()!=1?"s":"")+"\n");
                    }
                    printer.addText(stringBuilder.toString());
                    if ( i < orderDetails.getOrderedItems().size()-1 ) {
                        printer.addFeedLine(1);
                    }
                    if ( orderItemDetails.getDeliveryBoyDetails() != null && orderItemDetails.getStatusString().equalsIgnoreCase(Constants.DELIVERED) == false ){
                        if ( deliveryBoys.contains(orderItemDetails.getDeliveryBoyDetails().getName().toLowerCase()) == false ){
                            deliveryBoys.add(orderItemDetails.getDeliveryBoyDetails().getName().toLowerCase());
                        }
                    }
                }

                printer.addText(new StringBuilder()
                        .append("------------------------------------------------\n")
                        .append("                        Order Amount:   "+CommonMethods.getIndianFormatNumber(orderDetails.getSubTotal())+"\n")
                        .toString());
                if ( orderDetails.getTotalDiscount() > 0 ){
                printer.addText(new StringBuilder()
                        .append("                            Discount:   -"+CommonMethods.getIndianFormatNumber(orderDetails.getTotalDiscount())+"\n")
                        .toString());
                }
                if ( orderDetails.getMembershipDiscount() > 0 ){
                printer.addText(new StringBuilder()
                        .append("                 Membership Discount:   -"+CommonMethods.getIndianFormatNumber(orderDetails.getMembershipDiscount())+"\n")
                        .toString());
                }
                printer.addText(new StringBuilder()
                        .append("                    Delivery Charges:   "+CommonMethods.getIndianFormatNumber(orderDetails.getDeliveryCharges())+"\n")
                        .append("                     Service Charges:   "+CommonMethods.getIndianFormatNumber(orderDetails.getServiceCharges())+"\n")
                        .toString());

                if ( orderDetails.getSurcharges() > 0 ) {
                printer.addText(new StringBuilder()
                        .append("                      Surcharges Fee:   " + CommonMethods.getIndianFormatNumber(orderDetails.getSurcharges()) + "\n")
                        .toString());
                }
                printer.addText(new StringBuilder()
                        .append("                    Government Taxes:   "+CommonMethods.getIndianFormatNumber(orderDetails.getGovernmentTaxes())+"\n")
                        .toString());

                printer.addTextStyle(Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT, Printer.TRUE, Printer.PARAM_DEFAULT);
                printer.addText(new StringBuilder()
                        .append("                               TOTAL:   "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalAmount())+"\n")
                        .toString());
                printer.addTextStyle(Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT);

                if ( orderDetails.getTotalOnlinePaymentAmount() > 0 ){
                    printer.addText(new StringBuilder()
                            .append("                      Online Payment:   "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalOnlinePaymentAmount())+"\n")
                            .toString());
                }
                if ( orderDetails.getTotalCashAmount() > 0 ){
                    printer.addText(new StringBuilder()
                            .append("                    Cash on Delivery:   "+CommonMethods.getIndianFormatNumber(orderDetails.getTotalCashAmount())+"\n")
                            .toString());
                }

                if ( orderDetails.getTotalCashAmount() < 0 ){
                    printer.addText(new StringBuilder()
                            .append("                              Refund:   "+CommonMethods.getIndianFormatNumber(Math.abs(orderDetails.getTotalCashAmount()))+"\n")
                            .toString());
                }

                /*if ( orderDetails.getTotalCashAmount() > 0 ){
                    printer.addTextAlign(Printer.ALIGN_CENTER);
                    printer.addText(new StringBuilder()
                            .append("------------------------------------------------\n")
                            .toString());

                    printer.addText(new StringBuilder()
                            .append("Pay using PAYTM\n")
                            .toString());
                    Bitmap paytmQRImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.bill_phonepe_qr_code);
                    printer.addImage(paytmQRImage, 0, 0, paytmQRImage.getWidth(), paytmQRImage.getHeight(), Printer.COLOR_1, Printer.MODE_MONO, Printer.HALFTONE_DITHER, Printer.PARAM_DEFAULT, Printer.COMPRESS_AUTO);
                    printer.addFeedLine(1);

                    printer.addText(new StringBuilder()
                            .append("Pay using PHONEPE\n")
                            .toString());
                    Bitmap phonepeQRImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.bill_phonepe_qr_code);
                    printer.addImage(phonepeQRImage, 0, 0, phonepeQRImage.getWidth(), phonepeQRImage.getHeight(), Printer.COLOR_1, Printer.MODE_MONO, Printer.HALFTONE_DITHER, Printer.PARAM_DEFAULT, Printer.COMPRESS_AUTO);
                    printer.addFeedLine(1);

                    printer.addText(new StringBuilder()
                            .append("Pay using TEZ\n")
                            .toString());
                    Bitmap tezQRImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.bill_phonepe_qr_code);
                    printer.addImage(tezQRImage, 0, 0, tezQRImage.getWidth(), tezQRImage.getHeight(), Printer.COLOR_1, Printer.MODE_MONO, Printer.HALFTONE_DITHER, Printer.PARAM_DEFAULT, Printer.COMPRESS_AUTO);
                }*/

                printer.addTextAlign(Printer.ALIGN_CENTER);
                printer.addText(new StringBuilder()
                                .append("------------------------------------------------\n")
                                .append("Not satisfied?\n").toString());
                printer.addTextAlign(Printer.ALIGN_LEFT);
                printer.addText(new StringBuilder()
                        .append("Replacement:\n")
                        .append("Kindly check the items delivered to you and if\n")
                        .append("you are not satisfied, we will replace the item\n")
                        .append("within 1 hr, provided it is still available in\n")
                        .append("our inventory. If it is not available, we will\n")
                        .append("refund for the same after the items are picked\n")
                        .append("up by our delivery team.\n")
                        .append("\n")
                        .append("Return/refund:\n")
                        .append("If you want the items to be returned and your\n")
                        .append("payment refunded, kindly call us on 91824 41244\n")
                        .append("within 1hr after delivery, and we can have our\n")
                        .append("delivery team pick up the item and arrange for\n")
                        .append("the refund.\n")
                        .toString());

                printer.addTextAlign(Printer.ALIGN_CENTER);
                printer.addText(new StringBuilder()
                        .append("------------------------------------------------\n")
                        .append("Thank you.\n")
                        .append("Enjoy your cooking!\n")
                        .append("------------------------------------------------\n")
                        .append("For questions:\n")
                        .append("Call "+"9100104884 / 9100104885\n")
                        .append("Email support@mastaan.com\n")
                        .append("------------------------------------------------\n")
                        .toString());

                printer.addTextSize(3, 2);
                printer.addTextStyle(Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT, Printer.TRUE, Printer.PARAM_DEFAULT);
                printer.addText(new StringBuilder()
                        .append(orderDetails.getFormattedOrderID()+"\n")
                        .toString());

                //-----

                String deliveryBoysDetails = "";
                for(int i=0;i<deliveryBoys.size();i++){
                    if ( deliveryBoysDetails.length() > 0 ){    deliveryBoysDetails += "|"; }
                    deliveryBoysDetails += CommonMethods.capitalizeStringWords(deliveryBoys.get(i));
                }

                printer.addFeedLine(1);
                printer.addTextSize(Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT);
                printer.addTextStyle(Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT, Printer.PARAM_DEFAULT);
                printer.addText(new StringBuilder()
                        .append(((deliveryBoysDetails!=null&&deliveryBoysDetails.trim().length()>0)?deliveryBoysDetails+"|":"")
                                +serverTime+"\n")
                        .toString());

                printer.addFeedLine(1);
                printer.addCut(Printer.CUT_FEED);
            }
            return true;
        }catch (Exception e){
            return false;
        }
    }

    private void closePrinter(){
        closePrinter(null);
    }
    private void closePrinter(final StatusCallback callback){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                if ( printer != null ) {
                    try {
                        printer.endTransaction();
                    } catch (final Exception e) {}
                    try {
                        printer.disconnect();
                    } catch (final Exception e) {}
                    try {
                        printer.clearCommandBuffer();
                    } catch (final Exception e) {}
                    try {
                        printer.setReceiveEventListener(null);/*printer = null;*/
                    } catch (final Exception e) {}
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if ( callback != null ){
                    callback.onComplete(true, 200, "Success");
                }
            }
        }.execute();
    }

    private String getErrorMessage(Exception e) {
        String return_text = "";
        try{
            return_text = e.getMessage();
            if (e instanceof Epos2Exception) {
                int state = ((Epos2Exception) e).getErrorStatus();
                switch (state) {
                    case    Epos2Exception.ERR_PARAM:
                        return_text = "ERR_PARAM";
                        break;
                    case    Epos2Exception.ERR_CONNECT:
                        return_text = "ERR_CONNECT";
                        break;
                    case    Epos2Exception.ERR_TIMEOUT:
                        return_text = "ERR_TIMEOUT";
                        break;
                    case    Epos2Exception.ERR_MEMORY:
                        return_text = "ERR_MEMORY";
                        break;
                    case    Epos2Exception.ERR_ILLEGAL:
                        return_text = "ERR_ILLEGAL";
                        break;
                    case    Epos2Exception.ERR_PROCESSING:
                        return_text = "ERR_PROCESSING";
                        break;
                    case    Epos2Exception.ERR_NOT_FOUND:
                        return_text = "ERR_NOT_FOUND";
                        break;
                    case    Epos2Exception.ERR_IN_USE:
                        return_text = "ERR_IN_USE";
                        break;
                    case    Epos2Exception.ERR_TYPE_INVALID:
                        return_text = "ERR_TYPE_INVALID";
                        break;
                    case    Epos2Exception.ERR_DISCONNECT:
                        return_text = "ERR_DISCONNECT";
                        break;
                    case    Epos2Exception.ERR_ALREADY_OPENED:
                        return_text = "ERR_ALREADY_OPENED";
                        break;
                    case    Epos2Exception.ERR_ALREADY_USED:
                        return_text = "ERR_ALREADY_USED";
                        break;
                    case    Epos2Exception.ERR_BOX_COUNT_OVER:
                        return_text = "ERR_BOX_COUNT_OVER";
                        break;
                    case    Epos2Exception.ERR_BOX_CLIENT_OVER:
                        return_text = "ERR_BOX_CLIENT_OVER";
                        break;
                    case    Epos2Exception.ERR_UNSUPPORTED:
                        return_text = "ERR_UNSUPPORTED";
                        break;
                    case    Epos2Exception.ERR_FAILURE:
                        return_text = "ERR_FAILURE";
                        break;
                    default:
                        return_text = String.format("%d", state);
                        break;
                }
            }
        }catch (Exception ee){}
        return return_text;
    }


}