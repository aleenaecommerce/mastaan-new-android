package com.mastaan.logistics.handlers;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.models.RequestSendMessage;
import com.mastaan.logistics.models.UserDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 27/4/16.
 */

public class MessageHandler {

    MastaanToolbarActivity activity;

    UserDetails userDetails;

    public MessageHandler(MastaanToolbarActivity activity){
        this.activity = activity;
        this.userDetails = activity.localStorageData.getUserDetails();
    }

    public void sendMessage(final String mobileNumber){

        final List<String> messages = activity.localStorageData.getCustomerMessages();
        if ( userDetails.isCustomerRelationshipManager() ){//|| userDetails.isProcessingManager()  || userDetails.isDeliveryBoyManager() || userDetails.isOrdersManager() ){
            messages.add("Other");
        }
        activity.showListChooserDialog("Select Message", messages, new ListChooserCallback() {
            @Override
            public void onSelect(int position) {
                if ( messages.get(position).equalsIgnoreCase("Other") ){
                    activity.showInputTextDialog("Enter message", "message", "Dear Customer, ", new TextInputCallback() {
                        @Override
                        public void onComplete(String message) {
                            continueWithMessage(message);
                        }
                    });
                }else{
                    continueWithMessage(messages.get(position));
                }
            }
            private void continueWithMessage(final String messageText){

                activity.showChoiceSelectionDialog("Confirm", CommonMethods.fromHtml("Are you sure to send the message <b>" + messageText.trim() + "</b> to the customer (<b>" + mobileNumber + "</b>)?"), "NO", "YES", new ChoiceSelectionCallback() {
                    @Override
                    public void onSelect(int choiceNo, String choiceName) {
                        if ( choiceName.equalsIgnoreCase("YES") ){
                            sendMessage();
                        }
                    }

                    private void sendMessage(){
                        activity.showLoadingDialog("Sending message, wait...");
                        activity.getBackendAPIs().getCommonAPI().sendMessage(new RequestSendMessage(mobileNumber, messageText), new StatusCallback() {
                            @Override
                            public void onComplete(boolean status, int statusCode, final String message) {
                                activity.closeLoadingDialog();
                                if ( status ){
                                    activity.showToastMessage("Message sent successfully");
                                }else{
                                    if ( message.equalsIgnoreCase("Error") ) {
                                        activity.showChoiceSelectionDialog("Failure", "Something went wrong while sending message, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                                            @Override
                                            public void onSelect(int choiceNo, String choiceName) {
                                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                                    sendMessage();
                                                }
                                            }
                                        });
                                    }else {
                                        activity.showToastMessage(message);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });
    }

}
