package com.mastaan.logistics.flows;

import android.view.LayoutInflater;
import android.view.View;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.models.RequestAddEmployeeViolation;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.ViolationDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 19/02/17.
 */
public class ViolationsFlow {

    MastaanToolbarActivity activity;
    UserDetails userDetails;
    StatusCallback callback;

    public ViolationsFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
    }

    ViolationDetails selectedVioationDetails;
    public void addViolation(final UserDetails userDetails, final StatusCallback callback) {
        this.userDetails = userDetails;
        this.callback = callback;

        final List<ViolationDetails> violationsList = activity.localStorageData.getEmployeeViolationsList();
        final List<String> violationsStrings = new ArrayList<>();
        for (int i=0;i<violationsList.size();i++){
            violationsStrings.add(CommonMethods.capitalizeFirstLetter(violationsList.get(i).getValue()));
        }

        View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_add_violation, null);
        final vTextInputLayout date = (vTextInputLayout) dialogView.findViewById(R.id.date);
        final vTextInputLayout vioation = (vTextInputLayout) dialogView.findViewById(R.id.vioation);
        final vTextInputLayout comments = (vTextInputLayout) dialogView.findViewById(R.id.comments);

        date.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String prefillDate = date.getText();
                if ( prefillDate == null || prefillDate.length() == 0 ){
                    prefillDate = DateMethods.getCurrentDate();
                }
                activity.showDatePickerDialog("Select date", null, DateMethods.getCurrentDate(), prefillDate, new DatePickerCallback() {
                    @Override
                    public void onSelect(String fullDate, int day, int month, int year) {
                        date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                    }
                });
            }
        });

        vioation.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedViolationIndex = -1;
                if ( vioation.getText().length() > 0 ) {
                    for (int i = 0; i < violationsList.size(); i++) {
                        if (violationsList.get(i).getValue().equalsIgnoreCase(vioation.getText())) {
                            selectedViolationIndex = i;
                            break;
                        }
                    }
                }

                activity.showListChooserDialog("Select violation", violationsStrings, selectedViolationIndex, new ListChooserCallback() {
                    @Override
                    public void onSelect(int position) {
                        selectedVioationDetails = violationsList.get(position);
                        vioation.setText(selectedVioationDetails.getValue());
                    }
                });
            }
        });

        dialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String eDate = date.getText();
                date.checkError("* Select date");
                String eComments = comments.getText();

                if ( eDate.length() == 0 ){
                    return;
                }
                if ( selectedVioationDetails == null ){
                    vioation.showError("* Select violation");
                    return;
                }else{  vioation.hideError();   }

                activity.closeCustomDialog();

                addViolation(DateMethods.getDateInFormat(eDate, DateConstants.DD_MM_YYYY), selectedVioationDetails, eComments);
            }
        });

        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });

        activity.showCustomDialog(dialogView);

    }

    private void addViolation(final String date, final ViolationDetails violationDetails, final String comments){

        activity.showLoadingDialog("Adding violation, wait...");
        activity.getBackendAPIs().getCommonAPI().addEmployeeViolation(userDetails.getID(), new RequestAddEmployeeViolation(date, violationDetails.getID(), comments), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    callback.onComplete(true, 200, CommonMethods.capitalizeFirstLetter(violationDetails.getValue())+(comments.length()>0?(" - "+CommonMethods.capitalizeFirstLetter(comments)):"")+" on "+ DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY));
                }
                else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while adding violation to employee.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                addViolation(date, violationDetails, comments);
                            }
                        }
                    });
                }
            }
        });
    }

}