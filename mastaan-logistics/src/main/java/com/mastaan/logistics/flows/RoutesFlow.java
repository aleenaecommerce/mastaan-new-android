package com.mastaan.logistics.flows;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.backend.models.RequestAddOrUpdateDeliveryZone;
import com.mastaan.logistics.backend.models.RequestHousingSocieties;
import com.mastaan.logistics.models.DeliveryAreaDetails;
import com.mastaan.logistics.models.HousingSocietyDetails;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 24/03/17.
 */

public class RoutesFlow {

    MastaanToolbarActivity activity;
    OrderItemDetails orderItemDetails;

    public interface RouteSelectionCallback {
        void onComplete(boolean status, int statusCode, String message, String deliveryID, DeliveryAreaDetails deliveryAreaDetails);
    }
    public interface DeliveryAreaSelectionCallback {
        void onComplete(boolean status, int statusCode, String message, DeliveryAreaDetails deliveryAreaDetails);
    }
    public interface DeliveryZoneSelectionCallback {
        void onComplete(boolean status, int statusCode, String message, String zoneID);
    }

    public RoutesFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
    }


    public void assignOrChangeDeliveryArea(final OrderItemDetails orderItemDetails, final RouteSelectionCallback callback){
        this.orderItemDetails = orderItemDetails;
        selectDeliveryArea(orderItemDetails.getDeliveryArea(), new DeliveryAreaSelectionCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, DeliveryAreaDetails deliveryAreaDetails) {
                if ( status ){
                    assignOrderToDeliveryArea(deliveryAreaDetails, callback);
                }
            }
        });
    }
    private void assignOrderToDeliveryArea(final DeliveryAreaDetails deliveryAreaDetails, final RouteSelectionCallback callback){
        activity.showLoadingDialog("Assigning delivery area, wait...");
        activity.getBackendAPIs().getRoutesAPI().assignOrderToDeliveryArea(orderItemDetails.getOrderID(), deliveryAreaDetails.getID(), new RoutesAPI.AssignDeliveryAreaCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String deliveryID, String areaID) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Delivery area assigned successfully");
                    if ( callback != null ) {
                        callback.onComplete(true, 200, "Success", deliveryID, deliveryAreaDetails);
                    }
                }else{
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while assigning delivery area.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                assignOrderToDeliveryArea(deliveryAreaDetails, callback);
                            }
                        }
                    });
                }
            }
        });
    }




    //=======================

    public void selectDeliveryArea(final DeliveryAreaDetails selectedDeliveryArea, final DeliveryAreaSelectionCallback callback){
        activity.showLoadingDialog("Loading delivery areas, wait...");
        activity.getBackendAPIs().getRoutesAPI().getDeliveryAreas(new RoutesAPI.DeliveryAreasCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<DeliveryAreaDetails> deliveryAreas) {
                activity.closeLoadingDialog();
                if ( status ){
                    selectDeliveryArea(deliveryAreas, selectedDeliveryArea, callback);
                }else{
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while loading delivery areas.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                selectDeliveryArea(selectedDeliveryArea, callback);
                            }
                        }
                    });
                }
            }
        });
    }
    private void selectDeliveryArea(final List<DeliveryAreaDetails> deliveryAreasList, DeliveryAreaDetails selectedDeliveryArea, final DeliveryAreaSelectionCallback callback){
        final List<String> routeStrings = new ArrayList<>();
        //routeStrings.add("Add new delivery area");
        int selectedRouteIndex = -1;
        for (int i=0;i<deliveryAreasList.size();i++){
            routeStrings.add(deliveryAreasList.get(i).getCode() + " - " + deliveryAreasList.get(i).getName());
            if ( selectedDeliveryArea != null && selectedDeliveryArea.getID().equals(deliveryAreasList.get(i).getID()) ){
                selectedRouteIndex = i;
            }
        }

        activity.showListChooserDialog("Select route", routeStrings, selectedRouteIndex, new ListChooserCallback() {
            @Override
            public void onSelect(int position) {
                if ( routeStrings.get(position).equalsIgnoreCase("Add new delivery area") ){
                    activity.showInputTextDialog("Add delivery area", "Route name", new TextInputCallback() {
                        @Override
                        public void onComplete(String inputText) {
                            addDeliveryArea(inputText, callback);
                        }
                    });
                }else {
                    callback.onComplete(true, 200, "Success", deliveryAreasList.get(position/*-1*/));
                }
            }
        });
    }

    private void addDeliveryArea(final String routeName, final DeliveryAreaSelectionCallback callback){
        /*activity.showLoadingDialog("Adding delivery area, wait...");
        activity.getBackendAPIs().getRoutesAPI().addDeliveryArea(routeName, new RoutesAPI.DeliveryAreaCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, DeliveryZoneDetails areaDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    callback.onComplete(status, statusCode, message, areaDetails);
                }else{
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while adding delivery area.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                addDeliveryArea(routeName, callback);
                            }
                        }
                    });
                }
            }
        });*/
        activity.showToastMessage("Not yet done/Under construction...");
    }


    //-----

    public void selectDeliveryZone(final String selectedDeliveryZone, final DeliveryZoneSelectionCallback callback){
        selectDeliveryZone(true, selectedDeliveryZone, callback);
    }
    public void selectDeliveryZone(final boolean showAddZoneOption, final String selectedDeliveryZone, final DeliveryZoneSelectionCallback callback){

        activity.showLoadingDialog("Loading delivery zones, wait...");
        activity.getBackendAPIs().getRoutesAPI().getDeliveryZones(new RoutesAPI.DeliveryZonesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<String> deliveryZones) {
                activity.closeLoadingDialog();
                if ( status ){
                    showDeliveryZones(showAddZoneOption, deliveryZones, selectedDeliveryZone, callback);
                }else{
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while loading delivery zone.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                selectDeliveryZone(showAddZoneOption, selectedDeliveryZone, callback);
                            }
                        }
                    });
                }
            }
        });
    }

    private void showDeliveryZones(boolean showAddZoneOption, final List<String> deliveryZonesList, String selectedDeliveryZone, final DeliveryZoneSelectionCallback callback){
        final List<String> deliveryZonesStrings = new ArrayList<>();
        if ( showAddZoneOption ) {
            deliveryZonesStrings.add("Add new delivery zone");
        }

        int selectedRouteIndex = -1;
        for (int i=0;i<deliveryZonesList.size();i++){
            if ( deliveryZonesList.get(i) != null ){
                deliveryZonesStrings.add(deliveryZonesList.get(i));
                if ( selectedDeliveryZone != null && selectedDeliveryZone.equals(deliveryZonesList.get(i)) ) {
                    selectedRouteIndex = i+(showAddZoneOption?1:0);
                }
            }
        }

        activity.showListChooserDialog("Select delivery zone", deliveryZonesStrings, selectedRouteIndex, new ListChooserCallback() {
            @Override
            public void onSelect(int position) {
                if ( deliveryZonesStrings.get(position).equalsIgnoreCase("Add new delivery zone") ){
                    activity.showInputTextDialog("Add delivery zone", "Delivery zone id", new TextInputCallback() {
                        @Override
                        public void onComplete(String inputText) {
                            /*addDeliveryZone(inputText, new RoutesAPI.DeliveryZoneCallback() {
                                @Override
                                public void onComplete(boolean status, int statusCode, String message, String deliveryZone) {
                                    if ( status ){*/
                            callback.onComplete(true, 200, "Success", inputText.toUpperCase());
                                    /*}
                                }
                            });*/
                        }
                    });
                }else {
                    callback.onComplete(true, 200, "Success", deliveryZonesStrings.get(position));
                }
            }
        });
    }
    private void addDeliveryZone(final String deliveryZoneID, final RoutesAPI.DeliveryZoneCallback callback){

        activity.showLoadingDialog("Adding delivery zone, wait...");
        activity.getBackendAPIs().getRoutesAPI().addDeliveryZone(new RequestAddOrUpdateDeliveryZone(deliveryZoneID), new RoutesAPI.DeliveryZoneCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String deliveryZone) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Delivery zone added successfully.");
                    callback.onComplete(status, statusCode, message, deliveryZoneID);
                }else{
                    activity.showChoiceSelectionDialog("Failure", "Something went wrong while adding delivery zone.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                addDeliveryZone(deliveryZoneID, callback);
                            }
                        }
                    });
                }
            }
        });
    }

    //-------

    public void assignOrChangeHousingSociety(final String orderID, final String orderLatLng, final StatusCallback callback){
        assignOrChangeHousingSociety(orderID, orderLatLng, true, callback);
    }
    public void assignOrChangeHousingSociety(final String orderID, final String orderLatLng, final boolean isCancellable, final StatusCallback callback){
        activity.showLoadingDialog("Loading housing societies, wait...");
        activity.getBackendAPIs().getRoutesAPI().getHousingSocieties(new RequestHousingSocieties(orderLatLng), new RoutesAPI.HousingSocietiesCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<HousingSocietyDetails> housingSocietiesList) {
                activity.closeLoadingDialog();
                if ( status ) {
                    if ( housingSocietiesList.size() > 0 ){
                        final List<String> housingSocieties = new ArrayList<>();
                        housingSocieties.add("No housing society / None of the below");
                        for (int i=0;i<housingSocietiesList.size();i++){
                            housingSocieties.add(housingSocietiesList.get(i).getName());
                        }

                        activity.showListChooserDialog(isCancellable, "Select housing society", housingSocieties, -1, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                if ( housingSocieties.get(position).toLowerCase().contains("no housing society") ){
                                    if ( callback != null ){
                                        callback.onComplete(false, -1, "");
                                    }
                                }else{
                                    updateHousingSociety(housingSocieties.get(position));
                                }
                            }
                        });
                    }
                    else{
                        activity.showToastMessage("No housing societies to show");
                        if ( callback != null ){
                            callback.onComplete(false, -1, "");
                        }
                    }
                }
                else{
                    activity.showToastMessage("Failed to load housing societies");
                    if ( callback != null ){
                        callback.onComplete(false, -1, "");
                    }
                }
            }

            private void updateHousingSociety(final String housingSocietyName){

                activity.showLoadingDialog("Assigning housing society to order, wait...");
                activity.getBackendAPIs().getRoutesAPI().assignOrderToHousingSociety(orderID, housingSocietyName, new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            activity.showToastMessage("Housing society assigned successfully");
                            if ( callback != null ){
                                callback.onComplete(true, 200, housingSocietyName);
                            }
                        }else{
                            activity.showChoiceSelectionDialog(false,"Failure", "Something went wrong while assiging housing society to order. Please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if ( choiceName.equalsIgnoreCase("RETRY") ){
                                        updateHousingSociety(housingSocietyName);
                                    }else{
                                        if ( callback != null ){
                                            callback.onComplete(false, -1, "");
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

}
