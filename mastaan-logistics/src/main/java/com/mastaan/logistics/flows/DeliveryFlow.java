package com.mastaan.logistics.flows;

import android.location.Location;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.roboto.RobotoLightCheckBox;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.backend.models.RequestConfirmItemsPickup;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.UsersCallback;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.Job;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 27/4/16.
 */
public class DeliveryFlow {

    MastaanToolbarActivity activity;
    OrderItemDetails orderItemDetails;
    Location currentLocation;
    StatusCallback callback;

    public DeliveryFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
    }

    public void assignDeliveryBoy(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        showDeliveryBoySelectionDialog(orderItemDetails.getDeliveryBoyDetails(), new DeliveryBoySelectionCallback() {
            @Override
            public void onSelect(UserDetails deliveryBoyDetails) {
                assignToDeliveryBoy(deliveryBoyDetails.getID(), deliveryBoyDetails.getFullName());
            }
        });

        /*final UserDetails highlightDeliveryBoyDetails = orderItemDetails.getDeliveryBoyDetails();

        activity.showLoadingDialog("Loading available delivery boys, wait...");
        activity.getBackendAPIs().getEmployeesAPI().getAvailableDeliveryBoys(new UsersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<UserDetails> deliveryBoysList) {
                activity.closeLoadingDialog();
                if ( status ){
                    if ( deliveryBoysList.size() > 0 ) {
                        List<String> deliveryBoysStrings = new ArrayList<String>();
                        int hightlightDeliveryBoyIndex = -1;
                        for (int i=0;i<deliveryBoysList.size();i++){
                            String displayName = deliveryBoysList.get(i).getName();
                            if ( deliveryBoysList.get(i).getJobsDoneToday() > 0 ){
                                displayName += "\n( Jobs: "+ CommonMethods.getInDecimalFormat(deliveryBoysList.get(i).getJobsDoneToday());
                            }
                            if ( deliveryBoysList.get(i).getDistanceTravelledToday() > 0 ){
                                if ( deliveryBoysList.get(i).getJobsDoneToday() == 0 ){ displayName += "\n( ";}
                                else{   displayName += ", ";    }
                                if ( deliveryBoysList.get(i).getDistanceTravelledToday() == 1000 ) {
                                    displayName += "Dis: 1km";
                                }else{
                                    displayName += "Dis: " + CommonMethods.getInDecimalFormat(deliveryBoysList.get(i).getDistanceTravelledToday() / 1000) + "kms";
                                }
                            }
                            if ( deliveryBoysList.get(i).getJobsDoneToday() > 0 || deliveryBoysList.get(i).getDistanceTravelledToday() > 0 ){
                                displayName += " )";
                            }

                            deliveryBoysStrings.add(displayName);

                            if ( highlightDeliveryBoyDetails != null && hightlightDeliveryBoyIndex == -1 && deliveryBoysList.get(i).getID().equals(highlightDeliveryBoyDetails.getID()) ){
                                hightlightDeliveryBoyIndex = i;
                            }
                        }

                        ((vToolBarActivity)activity).showListChooserDialog("Select delivery boy!", deliveryBoysStrings, hightlightDeliveryBoyIndex, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                UserDetails selectedDeliveryBoyDetails = deliveryBoysList.get(position);
                                assignToDeliveryBoy(selectedDeliveryBoyDetails.getID(), selectedDeliveryBoyDetails.getName());

                            }
                        });

                    }else{
                        activity.showNotificationDialog("Alert!", "No delivery boys are available or not checked in now.");
                    }
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while getting available delivery boys list.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                assignDeliveryBoy(orderItemDetails, callback);
                            }
                        }
                    });
                }
            }
        });*/

    }

    public interface DeliveryBoySelectionCallback{
        void onSelect(UserDetails deliveryBoyDetails);
    }
    public void showDeliveryBoySelectionDialog(final DeliveryBoySelectionCallback callback){
        showDeliveryBoySelectionDialog(null, callback);
    }
    public void showDeliveryBoySelectionDialog(final UserDetails highlightDeliveryBoyDetailss, final DeliveryBoySelectionCallback callback){
        //callback.onSelect(new UserDetails("573318b52b3d306a4f825f12", "User"));//59744b7a5f016e2a2e86af63, 572dc1375e9c4a2d9eef1fcf
        activity.showLoadingDialog("Loading available delivery boys, wait...");
        activity.getBackendAPIs().getEmployeesAPI().getAvailableDeliveryBoys(new UsersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<UserDetails> deliveryBoysList) {
                activity.closeLoadingDialog();
                if ( status ){
                    if ( deliveryBoysList.size() > 0 ) {
                        List<String> deliveryBoysStrings = new ArrayList<String>();
                        int hightlightDeliveryBoyIndex = -1;
                        for (int i=0;i<deliveryBoysList.size();i++){
                            String displayName = "<b>"+deliveryBoysList.get(i).getFullName()+"</b>";
                            if ( deliveryBoysList.get(i).getJobsDoneToday() > 0 ){
                                displayName += "<br>Jobs: <b>"+ CommonMethods.getInDecimalFormat(deliveryBoysList.get(i).getJobsDoneToday())+"</b>";
                            }
                            if ( deliveryBoysList.get(i).getDistanceTravelledToday() > 0 ){
                                if ( deliveryBoysList.get(i).getJobsDoneToday() == 0 ){ displayName += "<br>";}
                                else{   displayName += ", ";    }
                                displayName += "Dis: <b>" + CommonMethods.getInDecimalFormat(deliveryBoysList.get(i).getDistanceTravelledToday() / 1000) + "kms</b>";
                            }

                            if ( deliveryBoysList.get(i).getJob() == null && deliveryBoysList.get(i).getLastJobTime() != null
                                    && DateMethods.compareDates(DateMethods.getOnlyDate(deliveryBoysList.get(i).getLastJobTime()), DateMethods.getOnlyDate(activity.localStorageData.getServerTime())) == 0 ) {
                                displayName += "<br>Idle time: <b>"+ DateMethods.getTimeFromMilliSeconds(DateMethods.getDifferenceBetweenDatesInMilliSeconds(activity.localStorageData.getServerTime(), deliveryBoysList.get(i).getLastJobTime()))+"</b>";
                            }

                            deliveryBoysStrings.add(displayName);

                            if ( highlightDeliveryBoyDetailss != null && hightlightDeliveryBoyIndex == -1 && deliveryBoysList.get(i).getID().equals(highlightDeliveryBoyDetailss.getID()) ){
                                hightlightDeliveryBoyIndex = i;
                            }
                        }

                        ((vToolBarActivity)activity).showListChooserDialog("Select delivery boy!", deliveryBoysStrings, hightlightDeliveryBoyIndex, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                if ( callback != null ){
                                    callback.onSelect(deliveryBoysList.get(position));
                                }

                            }
                        });

                    }else{
                        activity.showNotificationDialog("Alert!", "No delivery boys are available or not checked in now.");
                    }
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while getting available delivery boys list.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                showDeliveryBoySelectionDialog(highlightDeliveryBoyDetailss, callback);
                            }
                        }
                    });
                }
            }
        });
    }

    public void reAssignDeliveryBoy(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Do you want to reassign <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize()) +
                "</b> of <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "</b> to another delivery boy?"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equalsIgnoreCase("YES")) {
                    assignDeliveryBoy(orderItemDetails, callback);
                }
            }
        });
    }

    private void assignToDeliveryBoy(final String deliveryBoyID, final String deliveryBoyName){

        activity.showLoadingDialog("Assigning to delivery boy, wait...");
        activity.getBackendAPIs().getUsersAPI().assignDelivery(orderItemDetails.getID(), deliveryBoyID, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    callback.onComplete(true, 200, new Gson().toJson(new UserDetails(deliveryBoyID, deliveryBoyName)));
                    activity.showToastMessage("Delivery boy assigned successfully");
                    if ( message.equalsIgnoreCase("Success") == false ){
                        activity.showNotificationDialog(false, "Alert", Html.fromHtml(message));
                    }
                    /*deleteFromItemsList(orderItemsAdapter.getItem(position).getID());*/
                    //activity.onReloadPressed();
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while assigning delivery boy\nMsg:"+message+".\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                assignToDeliveryBoy(deliveryBoyID, deliveryBoyName);
                            }
                        }
                    });
                }
            }
        });
    }

    public interface CofirmItemsPickupCallback{
        void onComplete(boolean status, int statusCode, String message, List<String> confirmedItems, List<String> unconfirmedItems);
    }
    public void confirmItemsPickup(List<OrderDetails> ordersList, final CofirmItemsPickupCallback callback){
        LocalStorageData localStorageData = new LocalStorageData(activity);

        List<OrderItemDetails> itemsList = new ArrayList<>();
        for (int i=0;i<ordersList.size();i++){
            List<OrderItemDetails> orderItemsList = ordersList.get(i).getOrderedItems();
            for (int j=0;j<orderItemsList.size();j++){
                OrderItemDetails orderItemDetails = orderItemsList.get(j);
                if ( orderItemDetails.getStatusString().equalsIgnoreCase(Constants.ASSIGNED)
                        && orderItemDetails.getDeliveryBoyID().equals(localStorageData.getUserID()) ){
                    itemsList.add(orderItemDetails);
                }
            }
        }

        // IF NO PICK UP CONFIRMATION ITEMS
        if ( itemsList.size() == 0 ){
            callback.onComplete(true, 200, "Success", new ArrayList<String>(), new ArrayList<String>());
        }
        // IF ANY PICK UP CONFIRMATION ITEMS
        else{
            final List<String> pickedUpItems = new ArrayList<>();
            final List<String> unpickedUpItems = new ArrayList<>();
            View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_pickup_confirmation, null);
            LinearLayout itemsHolder = (LinearLayout) dialogView.findViewById(R.id.itemsHolder);
            for (int i=0;i<itemsList.size();i++){
                final OrderItemDetails orderItemDetails = itemsList.get(i);

                RobotoLightCheckBox checkBox = new RobotoLightCheckBox(activity);
                itemsHolder.addView(checkBox);

                checkBox.setText(Html.fromHtml("<b>"+CommonMethods.capitalizeStringWords(orderItemDetails.getOrderDetails().getCustomerName())+" ("+orderItemDetails.getOrderDetails().getFormattedOrderID().toUpperCase()+")'s</b> "
                        + CommonMethods.capitalizeStringWords(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())
                        + "<br>("+orderItemDetails.getFormattedQuantity()+")<br>"
                ));
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                        if ( isChecked ){
                            pickedUpItems.add(orderItemDetails.getID());
                            unpickedUpItems.remove(orderItemDetails.getID());
                        }else{
                            unpickedUpItems.add(orderItemDetails.getID());
                            pickedUpItems.remove(orderItemDetails.getID());
                        }
                    }
                });
                // Default adding to UnPickedUp itmes
                unpickedUpItems.add(orderItemDetails.getID());
            }

            dialogView.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( pickedUpItems.size() > 0 ){
                        activity.closeCustomDialog();
                        confirmItemsPickup(pickedUpItems, unpickedUpItems, callback);
                    }else{
                        activity.showToastMessage("* Please confirm picked up items");
                    }
                }
            });
            dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.closeCustomDialog();
                }
            });

            activity.showCustomDialog(dialogView);
        }

    }
    private void confirmItemsPickup(final List<String> pickedUpItems, final List<String> unpickedUpItems, final CofirmItemsPickupCallback callback){
        activity.showLoadingDialog("Please wait...");
        activity.getBackendAPIs().getOrdersAPI().confirmItemsPickup(new RequestConfirmItemsPickup(pickedUpItems, /*unpickedUpItems*/new ArrayList<String>()), new StatusCallback() {
            @Override
            public void onComplete(final boolean status, final int statusCode, final String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    if ( unpickedUpItems.size() > 0 ){
                        activity.showNotificationDialog(false, "Alert", Html.fromHtml("You did not pickup all items assigned to you. Kindly check with your dispatcher for unavailable items, <b>Do Not leave until cleared</b>."), new ActionCallback() {
                            @Override
                            public void onAction() {
                                callback.onComplete(false, 500, "Confirmation(s) pending", pickedUpItems, unpickedUpItems);
                            }
                        });
                    }else{
                        activity.showToastMessage("Items pickup confirmation successful");
                        callback.onComplete(true, 200, "Success", pickedUpItems, unpickedUpItems);
                    }
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure", "Something went wrong, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                confirmItemsPickup(pickedUpItems, unpickedUpItems, callback);
                            }else{
                                callback.onComplete(false, statusCode, message, null, null);
                            }
                        }
                    });
                }
            }
        });
    }


    public interface ClaimDeliveryCallback {
        void onComplete(boolean status, int statusCode, String message, Job jobDetails);
    }
    public void claimDelivery(final OrderItemDetails orderItemDetails, final ClaimDeliveryCallback callback) {
        /*activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Can you deliver <b>" + orderItemDetails.getMeatItemDetails().getMainName() + "</b> to <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equalsIgnoreCase("YES")) {*/
                    claimDelivery(orderItemDetails, null, callback);
                /*}
            }
        });*/
    }
    public void claimDelivery(final OrderItemDetails orderItemDetails, final String itemsNamesList, final ClaimDeliveryCallback callback) {
        this.orderItemDetails = orderItemDetails;

        activity.showChoiceSelectionDialog(false, "Confirm!", Html.fromHtml("Can you deliver <b>" + (itemsNamesList!=null?itemsNamesList:orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize()) + "</b> to <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equalsIgnoreCase("YES")) {
                    activity.getCurrentLocation(new LocationCallback() {
                        @Override
                        public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, final Location location, LatLng latLng) {
                            if (status) {
                                activity.showLoadingDialog("Creating delivery job for you, wait...");
                                activity.getBackendAPIs().getOrdersAPI().claimDelivery(orderItemDetails.getOrderID(), location, new Job(orderItemDetails, itemsNamesList), new OrdersAPI.ClaimDeliveryCallback() {
                                    @Override
                                    public void onComplete(boolean status, final int statusCode, final String message, Job jobDetails) {
                                        activity.closeLoadingDialog();
                                        if (status) {
                                            activity.showToastMessage("Created delivery job for you successfully");
                                            callback.onComplete(true, 200, "Success", jobDetails);
                                        } else {
                                            activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while creating delivery job for you.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                                                @Override
                                                public void onSelect(int choiceNo, String choiceName) {
                                                    if (choiceName.equalsIgnoreCase("YES")) {
                                                        claimDelivery(orderItemDetails, itemsNamesList, callback);
                                                    }else{
                                                        callback.onComplete(false, statusCode, message, null);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                activity.showToastMessage(message);
                            }
                        }
                    });
                }else{
                    callback.onComplete(false, 0, "", null);
                }
            }
        });
    }

    public void removeDeliveryBoy(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Are you sure to remove delivery boy info of <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+"</b> of <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equalsIgnoreCase("YES")) {
                    activity.showLoadingDialog("Removing delivery boy info, wait...");
                    activity.getBackendAPIs().getOrdersAPI().removeDeliveryBoy(orderItemDetails.getID(), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            activity.closeLoadingDialog();
                            if (status) {
                                activity.showToastMessage("Delivery boy info removoed successfully");
                                callback.onComplete(true, 200, "Success");
                            } else {
                                activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while removing delivery boy info.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                                    @Override
                                    public void onSelect(int choiceNo, String choiceName) {
                                        if (choiceName.equalsIgnoreCase("YES")) {
                                            removeDeliveryBoy(orderItemDetails, callback);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

}
