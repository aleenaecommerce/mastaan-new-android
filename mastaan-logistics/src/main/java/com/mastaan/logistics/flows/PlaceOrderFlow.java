package com.mastaan.logistics.flows;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.BuyersAPI;
import com.mastaan.logistics.backend.models.RequestUpdateBuyerDetails;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.dialogs.UpdateBuyerDetailsDialog;
import com.mastaan.logistics.models.BuyerDetails;

/**
 * Created by Venkatesh Uppu on 4/1/17.
 */
public class PlaceOrderFlow {
    MastaanToolbarActivity activity;
    String accessToken;
    BuyerDetails buyerDetails;

    public PlaceOrderFlow(MastaanToolbarActivity activity){
        this.activity = activity;
    }

    public void start(final String buyerID){

        activity.showLoadingDialog("Initiating, wait...");
        activity.getBackendAPIs().getBuyersAPI().getBuyerAccessTokenUsingID(buyerID, new BuyersAPI.BuyerAccessTokenCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String accessToken, BuyerDetails buyerDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    checkBuyerProfileAndProceedToInternalApp(accessToken, buyerDetails);
                }else{
                    activity.showChoiceSelectionDialog("Confirm!", "Something went wrong, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                start(buyerID);
                            }
                        }
                    });
                }
            }
        });
    }

    public void start(){
        View dialogPhoneNumberView = LayoutInflater.from(activity).inflate(R.layout.dialog_create_order, null);
        final vTextInputLayout phone_number = dialogPhoneNumberView.findViewById(R.id.phone_number);
        dialogPhoneNumberView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });
        dialogPhoneNumberView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ePhone = phone_number.getText();
                phone_number.checkError("* Enter phone number");
                if ( ePhone.length() > 0 ){
                    if ( CommonMethods.isValidPhoneNumber(ePhone) ){
                        activity.closeCustomDialog();
                        getBuyerAccessTokenUsingMobileAndProceed(ePhone);
                    }else{
                        phone_number.showError("* Enter valid phone number");
                    }
                }
            }
        });
        activity.showCustomDialog(dialogPhoneNumberView);
    }

    private void getBuyerAccessTokenUsingMobileAndProceed(final String phoneNumber){

        activity.showLoadingDialog("Initiating, wait...");
        activity.getBackendAPIs().getBuyersAPI().getBuyerAccessTokenUsingMobile(phoneNumber, new BuyersAPI.BuyerAccessTokenCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String accessToken, BuyerDetails buyerDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    checkBuyerProfileAndProceedToInternalApp(accessToken, buyerDetails);
                }else{
                    activity.showChoiceSelectionDialog("Confirm!", "Something went wrong, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                getBuyerAccessTokenUsingMobileAndProceed(phoneNumber);
                            }
                        }
                    });
                }
            }
        });
    }


    private void checkBuyerProfileAndProceedToInternalApp(final String accessToken, final BuyerDetails buyerDetails){
        this.accessToken = accessToken;
        this.buyerDetails = buyerDetails;

        if ( buyerDetails.getName() == null || buyerDetails.getName().trim().length() == 0
                || buyerDetails.getEmail() == null || buyerDetails.getEmail().trim().length() == 0 ){
            new UpdateBuyerDetailsDialog(activity).show(buyerDetails, new UpdateBuyerDetailsDialog.Callback() {
                @Override
                public void onComplete(RequestUpdateBuyerDetails requestUpdateBuyerDetails) {
                    updateBuyerProfileAndProceedToInternalApp(requestUpdateBuyerDetails);
                }
            });
        }else{
            proceedToBuyerInternalApp();
        }
    }

    private void updateBuyerProfileAndProceedToInternalApp(final RequestUpdateBuyerDetails requestUpdateBuyerDetails){

        activity.showLoadingDialog("Updating buyer details, wait...");
        activity.getBackendAPIs().getBuyersAPI().updateBuyerDetails(buyerDetails.getID(), requestUpdateBuyerDetails, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCosde, String message) {
                activity.closeLoadingDialog();
                if ( status ) {
                    proceedToBuyerInternalApp();
                }else{
                    activity.showChoiceSelectionDialog("Confirm!", "Something went wrong while updating buyer details, please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY") ){
                                updateBuyerProfileAndProceedToInternalApp(requestUpdateBuyerDetails);
                            }
                        }
                    });
                }
            }
        });
    }

    private void proceedToBuyerInternalApp(){
        try {
            Intent internalBuyerAppLaunchAct = new Intent();
            internalBuyerAppLaunchAct.setComponent(new ComponentName(activity.getResources().getString(R.string.internal_buyer_app_package_name), "com.mastaan.buyer.activities.LaunchActivity"));
            internalBuyerAppLaunchAct.putExtra(Constants.ACCESS_TOKEN, accessToken);
            internalBuyerAppLaunchAct.putExtra(Constants.EMPLOYEE_ID, activity.localStorageData.getUserID());
            activity.startActivity(internalBuyerAppLaunchAct);
        }catch (ActivityNotFoundException e){
            String downloadURL = activity.getResources().getString(R.string.internal_buyer_app_download_url);//"https://tsfr.io/zjxfSr";
            activity.showToastMessage("Please install 'Mastaan Internal' app from URL ("+downloadURL+") to continue...");
            ClipboardManager clipboardManager = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboardManager.setText(downloadURL);

            Intent browserIntent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(downloadURL));
            activity.startActivity(browserIntent);
        }catch (Exception e){
            activity.showToastMessage("Something went wrong, try again!");
        }
    }

}
