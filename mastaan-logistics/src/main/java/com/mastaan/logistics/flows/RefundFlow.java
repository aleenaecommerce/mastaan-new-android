package com.mastaan.logistics.flows;

import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.models.RequestAddRefundMoneyCollection;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderDetails2;
import com.mastaan.logistics.models.PaymentDetails;
import com.mastaan.logistics.models.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 21/03/18.
 */

public class RefundFlow {

    String TAG = "{REFUND_FLOW} : ";

    MastaanToolbarActivity activity;
    OrderDetails orderDetails;
    Callback callback;

    public interface Callback{
        void onComplete(boolean status, int statusCode, String message, double refundedAmount);
    }



    public RefundFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
    }

    public void start(final OrderDetails2 orderDetails, Callback callback){
        start(new OrderDetails(orderDetails), callback);
    }
    public void start(final OrderDetails orderDetails, Callback callback){
        this.orderDetails = orderDetails;
        this.callback = callback;

        if ( orderDetails.getPaymentTypeString().equalsIgnoreCase(Constants.ONLINE_PAYMENT) ) {
            activity.showInputNumberDecimalDialog("Refund Amount", "Amount", new TextInputCallback() {
                @Override
                public void onComplete(String inputText) {
                    double eAmount = Double.parseDouble(inputText);
                    if (eAmount <= 0) {
                        activity.showToastMessage("* Refund amount must be greater than 0");
                        return;
                    }
                    if (eAmount > orderDetails.getTotalAmount() + (orderDetails.getTotalCashAmount() < 0 ? Math.abs(orderDetails.getTotalCashAmount()) : 0)) {
                        activity.showToastMessage("* Refund amount must be less than or equals to order amount");
                        return;
                    }
                    checkPaymentsAndProceedToRefund(eAmount);
                }
            });
        }else{
            activity.showToastMessage("Currently refunds only supported for Razorpay online payments.");
        }
    }

    private void checkPaymentsAndProceedToRefund(double refundAmount){
        List<String> uniquePayments = new ArrayList<>();
        for (int i=0;i<orderDetails.getPaymentIDs().size();i++){
            if ( orderDetails.getPaymentIDs().get(i) != null && orderDetails.getPaymentIDs().get(i).trim().length() > 0 ){
                uniquePayments.add(orderDetails.getPaymentIDs().get(i));
            }
        }

        if ( uniquePayments.size() < 0 ){
            activity.showToastMessage("* No online payments found");
            callback.onComplete(false, 500, "No online payments found", 0);
            return;
        }

        getPaymentsAndProceedToRefund(uniquePayments, refundAmount);
    }

    private void getPaymentsAndProceedToRefund(final List<String> paymentIDs, final double toBeRefundAmount){

        activity.showLoadingDialog("Loading payments information, wait...");
        new AsyncTask<Void, Void, Void>() {
            boolean status = false;
            String message = "Error";
            List<PaymentDetails> paymentsList = new ArrayList<>();
            double totalPaymentsAmount = 0;
            double totalRefundedAmount = 0;
            String paymentsInformation = "";

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    for (int i=0;i<paymentIDs.size();i++){
                        Response<PaymentDetails> responsePaymentDetails = activity.getBackendAPIs().getRazorpayAPI().getPaymentDetails(paymentIDs.get(i));
                        if ( responsePaymentDetails.getStatus() && responsePaymentDetails.getData() != null ){
                            PaymentDetails paymentDetails = responsePaymentDetails.getData();
                            paymentsList.add(paymentDetails);

                            totalPaymentsAmount += paymentDetails.getAmount();
                            totalRefundedAmount += paymentDetails.getAmountRefunded();
                            paymentsInformation += (paymentsInformation.length()>0?"<br>":"")+"<b># </b> Payment <b>"+paymentDetails.getID()+"</b> of <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(paymentDetails.getAmount())+"</b>"+(paymentDetails.getAmountRefunded()>0?" with <b>refund "+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(paymentDetails.getAmountRefunded())+"</b>":"");
                        }
                    }
                    status = true;
                    message = "Success";
                }catch (Exception e){ e.printStackTrace();}
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                activity.closeLoadingDialog();

                if ( status ){
                    if ( paymentsList.size() > 0 ){
                        if ( toBeRefundAmount <= totalPaymentsAmount-totalRefundedAmount ){
                            String confirmMessage = "";
                            if ( totalRefundedAmount > 0 ){ confirmMessage = "Already Refunded: <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(totalRefundedAmount)+"</b><br><br>";  }
                            confirmMessage += "<b><u>Payments:</u></b><br>" + paymentsInformation;
                            confirmMessage += "<br><br>Do u want to continue with refund of <b>"+SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(toBeRefundAmount)+"</b>?";

                            activity.showChoiceSelectionDialog("Confirm", CommonMethods.fromHtml(confirmMessage), "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if ( choiceName.equalsIgnoreCase("YES") ){
                                        proceedToRefund(paymentsList, toBeRefundAmount);
                                    }
                                }
                            });
                        }else{
                            activity.showToastMessage("Refund amount is greater than total available payments amounts");
                            if ( callback != null ){    callback.onComplete(false, 500, "Refund amount is greater than total available payments amounts", 0); }
                        }
                    }else{
                        activity.showToastMessage("No Razorpay payments found.");
                        if ( callback != null ){    callback.onComplete(false, 500, "No Razorpay payments found.", 0); }
                    }
                }
            }
        }.execute();
    }

    private void proceedToRefund(final List<PaymentDetails> paymentsList, final double toBeRefundAmount){

        activity.showLoadingDialog("Refunding amount, wait...");
        new AsyncTask<Void, Void, Void>() {
            String message = "";
            double refundedAmount = 0;

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    for (int i=0;i<paymentsList.size();i++){
                        if ( refundedAmount < toBeRefundAmount ){
                            PaymentDetails paymentDetails = paymentsList.get(i);
                            if ( paymentDetails != null ){
                                double paymentRefund = toBeRefundAmount-refundedAmount;
                                if ( paymentRefund > paymentDetails.getAvailableAmount() ){    paymentRefund = paymentDetails.getAvailableAmount();   }

                                if ( paymentRefund > 0 ){
                                    Response<String> responseRefund = activity.getBackendAPIs().getRazorpayAPI().refundAmount(paymentDetails.getID(), paymentRefund);
                                    if ( responseRefund.getStatus() ){
                                        Log.d(Constants.LOG_TAG, TAG+"Refunded "+paymentRefund+" for payment "+paymentDetails.getID()+"("+paymentDetails.getAmount()+")");

                                        refundedAmount += paymentRefund;

                                        Response<String> responeAddRefundMoneyCollection = activity.getBackendAPIs().getMoneyCollectionAPI().addRefundMoneyCollection(new RequestAddRefundMoneyCollection(paymentDetails.getID(), responseRefund.getData(), paymentRefund, new LocalStorageData(activity).getServerTime()));
                                        if ( responeAddRefundMoneyCollection.getStatus() ){
                                            Log.d(Constants.LOG_TAG, TAG+"Money collection created for Refund "+paymentRefund+" for payment "+paymentDetails.getID()+"("+paymentDetails.getAmount()+")");
                                        }else{
                                            Log.d(Constants.LOG_TAG, TAG+"Money collection creation failed for Refund "+paymentRefund+" for payment "+paymentDetails.getID()+"("+paymentDetails.getAmount()+")");
                                        }
                                    }else{
                                        message += "<br>"+responseRefund.getMessage();
                                        Log.d(Constants.LOG_TAG, TAG+"Refund  "+paymentRefund+" failed for payment "+paymentDetails.getID()+"(Rs."+paymentDetails.getAmount()+"), Message: "+message);
                                    }
                                }else{
                                    Log.d(Constants.LOG_TAG, TAG+"Can't refund for payment "+paymentDetails.getID()+"(Rs."+paymentDetails.getAmount()+", ref: "+paymentDetails.getAmountRefunded()+"), Message: No available balance");
                                }
                            }
                        }
                    }
                }catch (Exception e){ e.printStackTrace(); }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                activity.closeLoadingDialog();

                if ( callback != null ){
                    if ( refundedAmount > 0 ){
                        if ( refundedAmount == toBeRefundAmount ){
                            activity.showToastMessage("Refund successful");
                            callback.onComplete(true, 200, "Success", refundedAmount);
                        }else{
                            activity.showNotificationDialog(false, "Alert", Html.fromHtml("Only <b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(refundedAmount) + "</b> is refunded instead of <b>" + SpecialCharacters.RS + " " + CommonMethods.getIndianFormatNumber(toBeRefundAmount) + "</b> refund"+(message!=null&&message.length()>0?"<br><br><b><u>Message</u></b><br>" + message:"")), new ActionCallback() {
                                @Override
                                public void onAction() {
                                    activity.showToastMessage("Partial amount refunded");
                                    callback.onComplete(true, 200, "Success", refundedAmount);
                                }
                            });
                        }
                    }else{
                        activity.showNotificationDialog(false, "Failure", Html.fromHtml(message), new ActionCallback() {
                            @Override
                            public void onAction() {
                                callback.onComplete(false, 500, message, 0);
                            }
                        });
                    }
                }
            }
        }.execute();
    }

}