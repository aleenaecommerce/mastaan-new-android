package com.mastaan.logistics.flows;

import android.view.LayoutInflater;
import android.view.View;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vRecyclerView;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.AppliedDiscountsAdapter;
import com.mastaan.logistics.backend.models.RequestUpdateDiscountForOrder;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.AppliedDiscountDetails;
import com.mastaan.logistics.models.DiscountDetails;
import com.mastaan.logistics.models.OrderDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 7/10/16.
 */
public class OrdersActionFlow {

    MastaanToolbarActivity activity;
    OrderDetails orderDetails;
    StatusCallback callback;

    public OrdersActionFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
    }


    // APPLY DISCOUNT

    public void applyDiscount(final List<OrderItemDetails> orderItemsList, final StatusCallback callback) {
        OrderDetails orderDetails = new OrderDetails(orderItemsList.get(0).getOrderDetails());
        for (int i=0;i<orderItemsList.size();i++){
            orderDetails.addOrderItem(orderItemsList.get(i));
        }
        applyDiscount(orderDetails, callback);
    }
    public void applyDiscount(final OrderDetails orderDetails, final StatusCallback callback) {
        this.callback = callback;
        this.orderDetails = orderDetails;

        final List<AppliedDiscountDetails> appliedDiscounts = orderDetails.getAppliedDiscounts();
        if ( appliedDiscounts != null && appliedDiscounts.size() > 0 ){
            View appliedDiscountsView = LayoutInflater.from(activity).inflate(R.layout.dialog_applied_discounts, null);
            vRecyclerView appliedDiscountsHolder = (vRecyclerView) appliedDiscountsView.findViewById(R.id.appliedDiscountsHolder);
            appliedDiscountsHolder.setupVerticalOrientation();
            final AppliedDiscountsAdapter appliedDiscountsAdapter = new AppliedDiscountsAdapter(activity, appliedDiscounts, new AppliedDiscountsAdapter.CallBack() {
                @Override
                public void onUpdateClick(int position, AppliedDiscountDetails appliedDiscountDetails) {
                    activity.closeCustomDialog();
                    showAddOrUpdateDiscountDialogAndProceed(appliedDiscountDetails, appliedDiscounts, false);
                }

                @Override
                public void onEnableClick(int position, AppliedDiscountDetails appliedDiscountDetails) {
                    activity.closeCustomDialog();
                    showAddOrUpdateDiscountDialogAndProceed(appliedDiscountDetails, appliedDiscounts, true);
                }

                @Override
                public void onDisableClick(int position, AppliedDiscountDetails appliedDiscountDetails) {
                    activity.closeCustomDialog();
                    removeDiscount(appliedDiscountDetails.getID());
                }
            });
            appliedDiscountsHolder.setAdapter(appliedDiscountsAdapter);

            appliedDiscountsView.findViewById(R.id.addDiscount).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.closeCustomDialog();
                    showAddOrUpdateDiscountDialogAndProceed(null, appliedDiscounts, false);
                }
            });
            activity.showCustomDialog(appliedDiscountsView);
        }else{
            showAddOrUpdateDiscountDialogAndProceed(null, null, false);
        }

    }

    private void showAddOrUpdateDiscountDialogAndProceed(AppliedDiscountDetails prefillAppliedDiscountDetails, List<AppliedDiscountDetails> alreadyAppliedDiscounts, boolean performAutoCilck){

        int prefillReasonIndex = 0;
        final List<DiscountDetails> availableDiscountReasons = new LocalStorageData(activity).getDiscountReasons();//activity.getDiscountReasons();
        final List<DiscountDetails> showingDiscounts = new ArrayList<>();
        List<String> showingDiscountsReasonStrings = new ArrayList<>();
        if ( availableDiscountReasons.size() == 0 ){
            availableDiscountReasons.add(new DiscountDetails("Select", "Select discount reason", false));
        }else {
            availableDiscountReasons.add(0, new DiscountDetails("Select", "Select discount reason", false));
        }
        for (int i=0;i<availableDiscountReasons.size();i++){
            boolean isAlreadyAppliedDiscount = false;
            if ( alreadyAppliedDiscounts != null && alreadyAppliedDiscounts.size() > 0 ){
                for (int j=0;j<alreadyAppliedDiscounts.size();j++){
                    if ( availableDiscountReasons.get(i).getID().equals(alreadyAppliedDiscounts.get(j).getReasonID()) ){
                        isAlreadyAppliedDiscount = true;
                        break;
                    }
                }
            }

            //---------

            if ( isAlreadyAppliedDiscount ){
                if ( prefillAppliedDiscountDetails != null
                        && availableDiscountReasons.get(i).getID().equals(prefillAppliedDiscountDetails.getReasonID())){
                    showingDiscounts.add(availableDiscountReasons.get(i));
                    showingDiscountsReasonStrings.add(availableDiscountReasons.get(i).getReason());
                    prefillReasonIndex = showingDiscounts.size()-1;
                }
            }else{
                showingDiscounts.add(availableDiscountReasons.get(i));
                showingDiscountsReasonStrings.add(availableDiscountReasons.get(i).getReason());
            }
        }

        //------

        View discountDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_discount, null);
        final vTextInputLayout discount_amount = (vTextInputLayout) discountDialogView.findViewById(R.id.discount_amount);
        final vSpinner reasonsSpinner = (vSpinner) discountDialogView.findViewById(R.id.reasonsSpinner);
        final vTextInputLayout discount_comment = (vTextInputLayout) discountDialogView.findViewById(R.id.discount_comment);

        ListArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_spinner, showingDiscountsReasonStrings);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setPopupLayoutView(R.layout.view_spinner_popup);
        reasonsSpinner.setAdapter(adapter);
        reasonsSpinner.setSelection(prefillReasonIndex);
        if ( prefillReasonIndex != 0 ){
            discountDialogView.findViewById(R.id.disableReasonsSpinner).setVisibility(View.VISIBLE);
            discount_amount.setText(CommonMethods.getInDecimalFormat(prefillAppliedDiscountDetails.getAmount()));
            discount_comment.setText(prefillAppliedDiscountDetails.getComment());
        }

        discountDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String discountAmount = discount_amount.getText().toString();
                discount_amount.checkError("* Enter discount amount");
                DiscountDetails selectedDiscountDetails = showingDiscounts.get(reasonsSpinner.getSelectedItemPosition());
                String discountComment = discount_comment.getText().toString().trim();

                if ( discountAmount.length() > 0 && Double.parseDouble(discountAmount) != 0 ){
                    if ( selectedDiscountDetails.getID().equalsIgnoreCase("Select") == false) {
                        activity.closeCustomDialog();

                        if ( selectedDiscountDetails.isLinkedToOrderItem() ){
                            applyDiscountSelectOrderItemAndProceed(selectedDiscountDetails, Double.parseDouble(discountAmount), discountComment);
                        }else{
                            applyDiscount(selectedDiscountDetails, Double.parseDouble(discountAmount), discountComment, null);
                        }
                    }else{
                        activity.showToastMessage("* Select discount reason");
                    }
                }
            }
        });
        if ( performAutoCilck ){
            discountDialogView.findViewById(R.id.done).performClick();
        }else {
            activity.showCustomDialog(discountDialogView);
        }
    }

    private void applyDiscountSelectOrderItemAndProceed(final DiscountDetails discountDetails, final double discountAmount, final String discountComment){

        List<String> orderItemsStrings = new ArrayList<>();
        for (int i=0;i<orderDetails.getOrderedItems().size();i++){
            OrderItemDetails orderItemDetails = orderDetails.getOrderedItems().get(i);
            orderItemsStrings.add(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize()+" ("+orderItemDetails.getFormattedQuantity()+" , "+ SpecialCharacters.RS+" "+CommonMethods.getIndianFormatNumber(orderItemDetails.getAmountOfItem())+")");
        }

        ((vToolBarActivity)activity).showListChooserDialog("Select order item for discount!", orderItemsStrings, new ListChooserCallback() {
            @Override
            public void onSelect(int position) {
                applyDiscount(discountDetails, discountAmount, discountComment, orderDetails.getOrderedItems().get(position));
            }
        });

    }

    private void applyDiscount(final DiscountDetails discountDetails, final double discountAmount, final String discountComment, final OrderItemDetails selectedOrderItemDetails){

        RequestUpdateDiscountForOrder requestUpdateDiscountForOrder = new RequestUpdateDiscountForOrder(discountDetails, discountAmount, discountComment, selectedOrderItemDetails!=null?selectedOrderItemDetails.getID():null);

        activity.showLoadingDialog("Updating discount, wait...");
        activity.getBackendAPIs().getOrdersAPI().updateDiscountForOrder(orderDetails.getID(), requestUpdateDiscountForOrder, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Discount updated successfully");
                    if ( callback != null ){
                        callback.onComplete(true, 200, "Success");
                    }
                }else{
                    if ( message.equalsIgnoreCase("Error") ) {
                        activity.showChoiceSelectionDialog("Failure!", "Something went wrong while updating discount.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                    applyDiscount(discountDetails, discountAmount, discountComment, selectedOrderItemDetails);
                                }
                            }
                        });
                    }else{
                        activity.showNotificationDialog("Failure!", message);
                    }
                }
            }
        });
    }

    private void removeDiscount(final String discountID){

        activity.showLoadingDialog("Disabling discount, wait...");
        activity.getBackendAPIs().getOrdersAPI().removeDiscount(orderDetails.getID(), discountID, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Discount disabled successfully");
                    if ( callback != null ){
                        callback.onComplete(true, 200, "Success");
                    }
                }else{
                    activity.showChoiceSelectionDialog("Failure!", "Something went wrong while disabling discount.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                removeDiscount(discountID);
                            }
                        }
                    });
                }
            }
        });
    }


    // UPDATE SPECIAL INSTRUCTIONS



}
