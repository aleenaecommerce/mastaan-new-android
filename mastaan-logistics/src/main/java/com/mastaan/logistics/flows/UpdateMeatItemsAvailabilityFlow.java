package com.mastaan.logistics.flows;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vAppCompatEditText;
import com.aleena.common.widgets.vScrollView;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.models.RequestUpdateMeatItemsAvailability;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.AvailabilityDetails;
import com.mastaan.logistics.models.WarehouseMeatItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 25/1/17.
 */

public class UpdateMeatItemsAvailabilityFlow implements View.OnClickListener {
    MastaanToolbarActivity activity;
    Context context;

    View dialogView;
    vScrollView scrollView;
    vTextInputLayout date;
    LinearLayout itemsHolder;
    View unavailableReasonsHolder;
    vSpinner unavailable_reasons_spinner;
    vAppCompatEditText unavailable_reason;
    View cancel;
    View update;

    View showUpdateUsingPaperRateView;
    View paperRatesView;
    vTextInputLayout dressed_paper_rate;
    vTextInputLayout skinless_paper_rate;
    vTextInputLayout farm_paper_rate;
    View updateItemPricesUsingPaperRates;
    View updatePricesUsingPaperRatesIndicator;

    List<String> itemNotAvailableReasonsList = new ArrayList<>();
    ListArrayAdapter unavailabilityReasonsAdapter;

    Typeface robotoLightFont;

    List<WarehouseMeatItemDetails> warehouseMeatItemsList = new ArrayList<>();

    StatusCallback callback;

    public UpdateMeatItemsAvailabilityFlow(@NonNull final MastaanToolbarActivity activity){
        this.activity = activity;
        this.context = activity;

        robotoLightFont = Typeface.createFromAsset(activity.getAssets(), "fonts/Roboto-Light.ttf");

        dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_update_meat_items_availability, null);
        scrollView = (vScrollView) dialogView.findViewById(R.id.scrollView);
        date = (vTextInputLayout) dialogView.findViewById(R.id.date);
        date.getEditText().setOnClickListener(this);
        itemsHolder = (LinearLayout) dialogView.findViewById(R.id.itemsHolder);
        unavailableReasonsHolder = dialogView.findViewById(R.id.unavailableReasonsHolder);
        unavailable_reasons_spinner = (vSpinner) dialogView.findViewById(R.id.unavailable_reasons_spinner);
        unavailable_reason = (vAppCompatEditText) dialogView.findViewById(R.id.unavailable_reason);
        cancel = dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        update = dialogView.findViewById(R.id.update);
        update.setOnClickListener(this);

        showUpdateUsingPaperRateView = dialogView.findViewById(R.id.showUpdateUsingPaperRateView);
        showUpdateUsingPaperRateView.setOnClickListener(this);
        paperRatesView = dialogView.findViewById(R.id.paperRatesView);
        dressed_paper_rate = (vTextInputLayout) dialogView.findViewById(R.id.dressed_paper_rate);
        skinless_paper_rate = (vTextInputLayout) dialogView.findViewById(R.id.skinless_paper_rate);
        farm_paper_rate = (vTextInputLayout) dialogView.findViewById(R.id.farm_paper_rate);
        updateItemPricesUsingPaperRates = dialogView.findViewById(R.id.updateItemPricesUsingPaperRates);
        updateItemPricesUsingPaperRates.setOnClickListener(this);
        updatePricesUsingPaperRatesIndicator = dialogView.findViewById(R.id.updatePricesUsingPaperRatesIndicator);

        //-----

        itemNotAvailableReasonsList = new LocalStorageData(context).getItemNotAvailableReasons();
        if ( itemNotAvailableReasonsList.size() == 0 ){
            itemNotAvailableReasonsList.add(Constants.SELECT);
        }else{
            itemNotAvailableReasonsList.add(0, Constants.SELECT);
        }
        itemNotAvailableReasonsList.add(Constants.OTHER);

        unavailabilityReasonsAdapter = new ListArrayAdapter(context, R.layout.view_list_item, itemNotAvailableReasonsList);
        unavailabilityReasonsAdapter.setPopupLayoutView(R.layout.view_list_item_dropdown_item);
        unavailable_reasons_spinner.setAdapter(unavailabilityReasonsAdapter);

        unavailable_reasons_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int previousPosition = -1;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( previousPosition != -1 ) {
                    if ( itemNotAvailableReasonsList.get(position).equalsIgnoreCase(Constants.OTHER) ){
                        unavailable_reason.setVisibility(View.VISIBLE);
                        scrollView.focusToViewTop(unavailableReasonsHolder);
                    }else{
                        unavailable_reason.setVisibility(View.GONE);
                    }
                }
                previousPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View view) {

        if ( view == date.getEditText() ){
            String prefillDate = DateMethods.getCurrentDate();
            if ( date.getText().length() > 0 ){ prefillDate = date.getText();   }
            activity.showDatePickerDialog("Select date", DateMethods.getCurrentDate(), null, prefillDate, new DatePickerCallback() {
                @Override
                public void onSelect(String fullDate, int day, int month, int year) {
                    date.hideError();
                    date.setText(DateMethods.getDateInFormat(fullDate, DateConstants.MMM_DD_YYYY));
                    itemsHolder.setVisibility(View.VISIBLE);
                    showItemsListForDate(fullDate);
                    unavailableReasonsHolder.setVisibility(View.VISIBLE);
                    unavailable_reason.setText("");
                    unavailable_reasons_spinner.setSelection(0);
                    update.setVisibility(View.VISIBLE);
                }
            });
        }

        else if ( view == showUpdateUsingPaperRateView ){
            if ( date.getText().trim().length() > 0 ) {
                paperRatesView.setVisibility(View.VISIBLE);
                showUpdateUsingPaperRateView.setVisibility(View.GONE);
            }else{
                activity.showToastMessage("* Select date first");
            }
        }
        else if ( view == updateItemPricesUsingPaperRates ){
            String eDressedPaperRate = dressed_paper_rate.getText();
            dressed_paper_rate.checkError("* Enter dressed rate");
            String eSkinlessPaperRate = skinless_paper_rate.getText();
            skinless_paper_rate.checkError("* Enter skinless rate");
            String eFarmPaperRate = farm_paper_rate.getText();
            farm_paper_rate.checkError("* Enter farm rate");
            if ( eDressedPaperRate.length() > 0 && eSkinlessPaperRate.length() > 0 && eFarmPaperRate.length() > 0 ){
                if ( CommonMethods.parseDouble(eDressedPaperRate) > 0 && CommonMethods.parseDouble(eSkinlessPaperRate) > 0 && CommonMethods.parseDouble(eFarmPaperRate) > 0 ){
                    activity.hideSoftKeyBoard(new View[]{dressed_paper_rate, skinless_paper_rate});

                    updateChickenItemsPriceUsingPaperRate(CommonMethods.parseDouble(eDressedPaperRate), CommonMethods.parseDouble(eSkinlessPaperRate), CommonMethods.parseDouble(eFarmPaperRate));
                }else{
                    activity.showToastMessage("* Enter valid paper rates");
                }
            }
        }

        else if ( view == cancel ){
            activity.closeCustomDialog();
        }
        else if ( view == update ){
            String eDate = date.getText();
            date.checkError("* Enter date");
            String eUnavailableReason = itemNotAvailableReasonsList.get(unavailable_reasons_spinner.getSelectedItemPosition());
            if (eUnavailableReason.equalsIgnoreCase(Constants.SELECT)) {
                eUnavailableReason = "";
            } else if (eUnavailableReason.equalsIgnoreCase(Constants.OTHER)) {
                eUnavailableReason = unavailable_reason.getText().toString().trim();
            }

            RequestUpdateMeatItemsAvailability requestUpdateMeatItemsAvailability = new RequestUpdateMeatItemsAvailability(DateMethods.getDateInFormat(eDate, DateConstants.DD_MM_YYYY)/*date*/);
            requestUpdateMeatItemsAvailability.setUnavailbaleReason(eUnavailableReason);

            boolean isAnyUnavailableItemPresent = false;
            for (int i=0;i<itemsViews.size();i++){
                View item_view = itemsViews.get(i);
                TextView item_id = (TextView) item_view.findViewById(R.id.item_id);
                CheckBox check_box = (CheckBox) item_view.findViewById(R.id.check_box);
                vTextInputLayout buyer_price = (vTextInputLayout) item_view.findViewById(R.id.buyer_price);
                buyer_price.checkError("* Enter buyer price");
                vTextInputLayout discount = (vTextInputLayout) item_view.findViewById(R.id.discount);
                vTextInputLayout vendor_price = (vTextInputLayout) item_view.findViewById(R.id.vendor_price);
                buyer_price.checkError("* Enter vendor price");

                if ( buyer_price.getText().toString().length() == 0 || vendor_price.getText().toString().length() == 0 ){
                    scrollView.focusToViewTop(item_view);
                    return;
                }
                requestUpdateMeatItemsAvailability.addItem(item_id.getText().toString(), check_box.isChecked(), Double.parseDouble(vendor_price.getText()), Double.parseDouble(buyer_price.getText()), Double.parseDouble(discount.getText()));

                if ( check_box.isChecked() == false ){  isAnyUnavailableItemPresent = true; }
            }

            if ( isAnyUnavailableItemPresent && eUnavailableReason.length() == 0 ){
                activity.showToastMessage("* Enter unavailable reason");
                scrollView.focusToViewTop(unavailableReasonsHolder);
                return;
            }

            activity.closeCustomDialog();
            updateMeatItemsAvailability(requestUpdateMeatItemsAvailability);
        }
    }

    public void start(@NonNull List<WarehouseMeatItemDetails> meatItemsList, @NonNull StatusCallback callback){
        this.warehouseMeatItemsList = meatItemsList;
        this.callback = callback;

        activity.showCustomDialog(dialogView);

        update.setVisibility(View.GONE);
        date.getEditText().performClick();
    }

    List<View> itemsViews = new ArrayList<>();
    private void showItemsListForDate(final String date){
        itemsHolder.removeAllViews();
        itemsViews.clear();

        activity.showLoadingDialog("Loading items, wait....");
        new AsyncTask<Void, Void, Void>() {
            List<TempMeatItem> meatItemsList = new ArrayList<>();
            @Override
            protected Void doInBackground(Void... voids) {
                for (int i = 0; i< warehouseMeatItemsList.size(); i++){
                    WarehouseMeatItemDetails warehouseMeatItemDetails = warehouseMeatItemsList.get(i);
                    AvailabilityDetails availabilityDetails = warehouseMeatItemDetails.getAvailabilityDetailsForDate(date);
                    meatItemsList.add(new TempMeatItem(warehouseMeatItemDetails.getID()
                            , warehouseMeatItemDetails.getMeatItemDetais().getNameWithCategoryAndSize()
                            , availabilityDetails.isAvailable()//warehouseMeatItemDetails.isAvailableOnDate(date)
                            , availabilityDetails.getActualSellingPrice()//warehouseMeatItemDetails.getSellingPriceForDate(date)
                            , availabilityDetails.getActualSellingPrice()-availabilityDetails.getSellingPrice()
                            , availabilityDetails.getBuyingPrice()//warehouseMeatItemDetails.getBuyingPriceForDate(date))
                    ));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                activity.closeLoadingDialog();

                for (int i = 0; i< meatItemsList.size(); i++){
                    final TempMeatItem meatItemDetails = meatItemsList.get(i);

                    View item_view = LayoutInflater.from(activity).inflate(R.layout.view_meat_item_availability, null);
                    itemsHolder.addView(item_view);
                    itemsViews.add(item_view);
                    TextView item_id = (TextView) item_view.findViewById(R.id.item_id);
                    CheckBox check_box = (CheckBox) item_view.findViewById(R.id.check_box);
                    vTextInputLayout buyer_price = (vTextInputLayout) item_view.findViewById(R.id.buyer_price);
                    vTextInputLayout discount = (vTextInputLayout) item_view.findViewById(R.id.discount);
                    vTextInputLayout vendor_price = (vTextInputLayout) item_view.findViewById(R.id.vendor_price);

                    item_id.setText(meatItemDetails.id);
                    check_box.setText(meatItemDetails.name);
                    check_box.setChecked(meatItemDetails.availability);
                    buyer_price.setText(CommonMethods.getInDecimalFormat(meatItemDetails.buyer_price));
                    discount.setText(CommonMethods.getInDecimalFormat(meatItemDetails.discount));
                    vendor_price.setText(CommonMethods.getInDecimalFormat(meatItemDetails.vendor_price));
                }
            }
        }.execute();

    }

    //---------

    public UpdateMeatItemsAvailabilityFlow hasPaperRateView(){
        showUpdateUsingPaperRateView.setVisibility(View.VISIBLE);
        return this;
    }

    private String getInDecimalFormat(double inputDouble){
        return CommonMethods.getInDecimalFormat(inputDouble);
    }

    private void updateChickenItemsPriceUsingPaperRate(double dressedPaperRate, double skinlessPaperRate, double farmPaperRate){
        activity.showShortToastMessage("Updating items prices, wait...");
        updateItemPricesUsingPaperRates.setEnabled(false);
        updatePricesUsingPaperRatesIndicator.setVisibility(View.VISIBLE);

        double marginRate = 35;
        double burntTurmericCharge = 15;
        double wholeBirdCharge = 40;
        double bonelessCharge = 150;
        double bonelessBreastCharge = 200;
        double breastBoneCharge = 150;
        double legsCharge = 150;
        double thighsCharge = 120;
        double drumsticksCharge = 180;
        double wingsCharge = 120;
        double lollipopsCharge = 140;
        double keemaCharge = 10;
        double liverCharge = 70;
        double gizzardCharge = 70;
        double neckCharge = 100;
        double bonesandFatCharge = 150;

        double dressedVendorWeight = 1.35;
        double skinlessVendorWeight = 1.55;
        double breastBoneVendorWeight = 3.05;
        double wholeBirdVendorWeight = 1.45;
        double breastBonelessVendorWeight = 5;
        double bonelessVendorWeight = 5;
        double legsVendorWeight = 4;
        double thighsVendorWeight = 5;
        double thighsBonelessVendorWeight = 8;
        double drumsticksVendorWeight = 5;
        double wingsVendorWeight = 4;
        double lollipopsVendorWeight = 4.25;
        double keemaVendorWeight = 2.85;
        double keemaBreastVendorWeight = 5;
        double liverVendorWeight = 2.5;
        double gizzardVendorWeight = 2.5;
        double neckVendorWeight = 1.5;
        double bonesandFatVendorWeight = 1.5;

        int updatedCount = 0;
        for (int i=0;i<itemsViews.size();i++){
            View item_view = itemsViews.get(i);
            String itemName = ((CheckBox) item_view.findViewById(R.id.check_box)).getText().toString();
            vTextInputLayout buyer_price = item_view.findViewById(R.id.buyer_price);
            vTextInputLayout vendor_price = item_view.findViewById(R.id.vendor_price);

            if ( itemName.toLowerCase().contains("farm") ){
                if ( itemName.toLowerCase().contains("skin-on chicken") && itemName.toLowerCase().contains("curry cut and burnt with turmeric") ){
                    buyer_price.setText(round(dressedPaperRate + burntTurmericCharge + marginRate)+"");
                    vendor_price.setText(round(farmPaperRate * dressedVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("whole bird skin-on chicken") ){
                    buyer_price.setText(round(dressedPaperRate + marginRate + wholeBirdCharge)+"");
                    vendor_price.setText(round(farmPaperRate * wholeBirdVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("skin-on chicken") ){
                    buyer_price.setText(round(dressedPaperRate + marginRate)+"");
                    vendor_price.setText(round(farmPaperRate * dressedVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("chicken skinless") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate)+"");
                    vendor_price.setText(round(farmPaperRate * skinlessVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("chicken boneless") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate + bonelessCharge)+"");
                    vendor_price.setText(round(farmPaperRate * bonelessVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("breast with bone") ){// && itemName.toLowerCase().contains("curry cut") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate + breastBoneCharge)+"");
                    vendor_price.setText(round(farmPaperRate * breastBoneVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("breast mince") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate + bonelessBreastCharge + keemaCharge)+"");
                    vendor_price.setText(round(farmPaperRate * keemaBreastVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("breast") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate + bonelessBreastCharge)+"");
                    vendor_price.setText(round(farmPaperRate * breastBonelessVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("legs") ){
                    buyer_price.setText(round(dressedPaperRate + marginRate + legsCharge)+"");
                    vendor_price.setText(round(farmPaperRate * legsVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("thighs") && itemName.toLowerCase().contains("boneless") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate + bonelessCharge + thighsCharge)+"");
                    vendor_price.setText(round(farmPaperRate * thighsBonelessVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("thighs") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate + thighsCharge)+"");
                    vendor_price.setText(round(farmPaperRate * thighsVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("drumsticks") ){
                    buyer_price.setText(round(dressedPaperRate + marginRate + drumsticksCharge)+"");
                    vendor_price.setText(round(farmPaperRate * drumsticksVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("wings") ){
                    buyer_price.setText(round(dressedPaperRate + marginRate + wingsCharge)+"");
                    vendor_price.setText(round(farmPaperRate * wingsVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("lollipops") ){
                    buyer_price.setText(round(dressedPaperRate + marginRate + lollipopsCharge)+"");
                    vendor_price.setText(round(farmPaperRate * lollipopsVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("mince") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate + bonelessCharge + keemaCharge)+"");
                    vendor_price.setText(round(farmPaperRate * keemaVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("liver") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate - liverCharge)+"");
                    vendor_price.setText(round(farmPaperRate * liverVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("gizzard") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate - gizzardCharge)+"");
                    vendor_price.setText(round(farmPaperRate * gizzardVendorWeight)+"");
                    updatedCount ++;
                }
                else if ( itemName.toLowerCase().contains("neck") ){
                    buyer_price.setText(round(skinlessPaperRate + marginRate - neckCharge)+"");
                    vendor_price.setText(round(farmPaperRate * neckVendorWeight)+"");
                    updatedCount ++;
                }
            }
        }
        updateItemPricesUsingPaperRates.setEnabled(true);
        updatePricesUsingPaperRatesIndicator.setVisibility(View.GONE);
        activity.showToastMessage(updatedCount+" out of "+itemsViews.size()+" items prices updated using paper rate");

    }

    private long round(double number){
        long rNumber = Math.round(number);

        long tN = rNumber - (rNumber%10);
        long r = rNumber%10;
        if ( r == 0 || r == 1 || r== 2 ){
            return tN;
        }else if ( r == 3 || r == 4 || r == 5 || r == 6 || r == 7 ){
            return (tN + 5);
        }else {
            return (tN + 10);
        }
    }

    //----------

    public void updateMeatItemsAvailability(final RequestUpdateMeatItemsAvailability requestUpdateMeatItemsAvailability){

        activity.showLoadingDialog("Updating meat items availabilities, wait...");
        activity.getBackendAPIs().getMeatItemsAPI().updateMeatItemsAvailabilities(requestUpdateMeatItemsAvailability, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    callback.onComplete(status, statusCode, "Items availability updated successfully");
                }else{
                    if ( statusCode == 500 && message.equalsIgnoreCase("Error") == false ){
                        callback.onComplete(false, statusCode, message);
                    }else {
                        activity.showChoiceSelectionDialog(false, "Failure!", "Somthing went wrong while updating meat items availabilities.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                    updateMeatItemsAvailability(requestUpdateMeatItemsAvailability);
                                }
                            }
                        });
                    }
                }
            }
        });
    }


    //=======

    class TempMeatItem{
        String id;
        String name;
        boolean availability;
        double buyer_price;
        double discount;
        double vendor_price;
        TempMeatItem(String id, String name, boolean availability, double buyer_price, double discount, double vendor_price){
            this.id = id;
            this.name = name;
            this.availability = availability;
            this.buyer_price = buyer_price;
            this.discount = discount>=0?discount:0;
            this.vendor_price = vendor_price;
        }
    }

}
