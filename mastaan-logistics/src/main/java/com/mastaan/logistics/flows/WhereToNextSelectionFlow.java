package com.mastaan.logistics.flows;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.methods.CommonMethods;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.activities.WhereToNextActivity;
import com.mastaan.logistics.adapters.PlaceChooserAdapter;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.backend.VendorsAndButchersAPI;
import com.mastaan.logistics.backend.WhereToNextAPI;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.CustomPlace;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.Job;
import com.mastaan.logistics.models.Vendor;
import com.mastaan.logistics.models.WhereToNextItem;

import java.util.List;

/**
 * Created by venkatesh on 10/3/16.
 */

public class WhereToNextSelectionFlow {

    MastaanToolbarActivity activity;
    Context context;

    Location currentLocation;
    WhereToNextItem selectedPlace;

    List<WhereToNextItem> whereToNextPlaces;

//    Callback callback;
//
//    public interface Callback{
//        void onComplete(String whereTo, Location currentLocation);
//    }

    public WhereToNextSelectionFlow(MastaanToolbarActivity activity){
        this.activity = activity;
        this.context = activity;

        whereToNextPlaces = new LocalStorageData(activity).getWhereToNextPlaces();
        whereToNextPlaces.add(new WhereToNextItem("to_hub", "Hub"));
        whereToNextPlaces.add(new WhereToNextItem("to_vendor", "Vendor"));
    }

    public void start(){//final Callback call_back){
        //this.callback = call_back;

        View whereToNextDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_where_to_next, null);
        final RadioGroup itemsHolder = (RadioGroup) whereToNextDialogView.findViewById(R.id.itemsHolder);

        for (int i=0;i<whereToNextPlaces.size();i++){
            final WhereToNextItem itemDetails = whereToNextPlaces.get(i);
            RadioButton itemView = (RadioButton) LayoutInflater.from(context).inflate(R.layout.radio_button, null);
            itemsHolder.addView(itemView);
            itemView.setText(itemDetails.getName());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPlace = itemDetails;
                }
            });
        }

        whereToNextDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();
            }
        });
        whereToNextDialogView.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedPlace != null) {

                    activity.checkLocationAccess(new CheckLocatoinAccessCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {

                            // IF LOCATION ENABLED
                            if (status) {
                                activity.closeCustomDialog();
                                activity.showLoadingDialog("Fetching your current location, wait...");
                                new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                                    @Override
                                    public void onComplete(final Location location) {

                                        activity.closeLoadingDialog();

                                        if (location != null) {

                                            currentLocation = location;

                                            // IF TO HUB
                                            if (selectedPlace.getId().equalsIgnoreCase("to_hub")) {
                                                onHubsSelect();
                                            }
                                            // IF TO VENDOR
                                            else if (selectedPlace.getId().equalsIgnoreCase("to_vendor")) {
                                                onVendorsSelect();
                                            }
                                            // ELSE
                                            else {
                                                //callback.onComplete(selectedPlace.getId(), location);
                                                updateWhereToNext(selectedPlace.getId(), location);
                                            }

                                        } else {
                                            activity.showToastMessage("Unable to fetch your current location, try again!");
                                        }
                                    }
                                });
                            } else {
                                activity.showToastMessage("* Location access is required to complete your action.");
                            }
                        }
                    });


                } else {
                    activity.showToastMessage("*Please select place.");
                }
            }
        });

        activity.showCustomDialog(whereToNextDialogView);
    }

    private void onHubsSelect(){

        activity.showLoadingDialog("Getting hubs, wait...");
        activity.getBackendAPIs().getRoutesAPI().getHubs(new RoutesAPI.HubsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<HubDetails> hubsList) {
                activity.closeLoadingDialog();
                if (status) {
                    onPlaceChooseSelectionFlow("Select Hub", hubsList);
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while fetching hubs list.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                onHubsSelect();
                            }
                        }
                    });
                }
            }
        });
    }

    private void onVendorsSelect(){

        activity.showLoadingDialog("Getting vendors, wait...");
        activity.getBackendAPIs().getVendorsAndButchersAPI().getVendors(/*currentLocation.getLatitude(), currentLocation.getLongitude(), */new VendorsAndButchersAPI.VendorsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<Vendor> vendorsList) {
                activity.closeLoadingDialog();
                if (status) {
                    onPlaceChooseSelectionFlow("Select Vendor", vendorsList);
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while fetching Vendors list.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                onVendorsSelect();
                            }
                        }
                    });
                }
            }
        });
    }

    private void onPlaceChooseSelectionFlow(final String dialogTitle, final List<? extends CustomPlace> places){

        View hubSelectionDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_list_items_chooser, null);
        TextView title = (TextView) hubSelectionDialogView.findViewById(R.id.title);
        ListView itemsHolder = (ListView) hubSelectionDialogView.findViewById(R.id.list_view);

        title.setText(dialogTitle);
        itemsHolder.setAdapter(new PlaceChooserAdapter(context, places, new PlaceChooserAdapter.ItemClickListener() {
            @Override
            public void onSelect(int position, CustomPlace place) {
                activity.closeCustomDialog();
                //callback.onComplete(place.toString(), currentLocation);
                updateWhereToNext(place.toString(), currentLocation);
            }
        }));

        activity.showCustomDialog(hubSelectionDialogView);
    }

    private void updateWhereToNext(final String whereTo, final Location currentLocation){

        activity.showLoadingDialog("Updating your choice, wait...");
        activity.getBackendAPIs().getWhereToNextAPI().whereToNext(whereTo, currentLocation.getLatitude(), currentLocation.getLongitude(), activity.localStorageData.getUserID(), new WhereToNextAPI.WhereToNextCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, Job jobDetails) {
                activity.closeLoadingDialog();
                if (status) {
                    Intent whereToNextAct = new Intent(context, WhereToNextActivity.class);
                    whereToNextAct.putExtra("jobDetails", new Gson().toJson(jobDetails));
                    activity.startActivity(whereToNextAct);
                    //finish();
                }
                else {
                    if ( message.equalsIgnoreCase("Error") ) {
                        activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating your choice.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                    updateWhereToNext(whereTo, currentLocation);
                                }
                            }
                        });
                    }else{
                        activity.showNotificationDialog("Failure!", CommonMethods.capitalizeFirstLetter(message));
                    }
                }
            }
        });
    }
}
