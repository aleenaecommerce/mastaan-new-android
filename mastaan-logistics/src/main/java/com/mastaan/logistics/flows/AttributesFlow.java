package com.mastaan.logistics.flows;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.aleena.common.activities.vToolBarActivity;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.AttributeOptionsAvailabilitiesAdapter;
import com.mastaan.logistics.backend.MeatItemsAPI;
import com.mastaan.logistics.backend.models.RequestUpdateAttributeDetails;
import com.mastaan.logistics.backend.models.RequestUpdateOptionAvailability;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.AttributeDetails;
import com.mastaan.logistics.models.AttributeOptionAvailabilityDetails;
import com.mastaan.logistics.models.AttributeOptionDetails;

import java.util.List;

/**
 * Created by Venkatesh Uppu on 22/10/16.
 */
public class AttributesFlow {
    MastaanToolbarActivity activity;
    LocalStorageData localStorageData;

    AttributeDetails attributeDetails;
    UpdateAttributeCallback updateAttributeCallback;

    AttributeOptionDetails attributeOptionDetails;
    UpdateAttributeOptionCallback updateAttributeOptionCallback;

    public interface UpdateAttributeCallback{
        void onComplete(boolean status, int statusCode, String message, AttributeDetails updateAttributeDetails);
    }

    public interface UpdateAttributeOptionCallback{
        void onComplete(boolean status, int statusCode, String message, AttributeOptionDetails updateAttributeOptionDetails);
    }

    public AttributesFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
        this.localStorageData = new LocalStorageData(activity);
    }


    // UDPATE ATTRIBUTE DETAILS

    public void updateAttributeDetails(final AttributeDetails attributeDetails, final UpdateAttributeCallback updateAttributeCallback){
        this.attributeDetails = attributeDetails;
        this.updateAttributeCallback = updateAttributeCallback;

        View dialogUpdateAttributeDetailsView = LayoutInflater.from(activity).inflate(R.layout.dialog_update_attribute_details, null);
        final TextView title = (TextView) dialogUpdateAttributeDetailsView.findViewById(R.id.title);
        final vTextInputLayout attribute_name = (vTextInputLayout) dialogUpdateAttributeDetailsView.findViewById(R.id.attribute_name);
        final RadioButton changeDaywise = (RadioButton) dialogUpdateAttributeDetailsView.findViewById(R.id.changeDaywise);
        final RadioButton notChangeDaywise = (RadioButton) dialogUpdateAttributeDetailsView.findViewById(R.id.notChangeDaywise);

        title.setText("Update "+attributeDetails.getName().toLowerCase());
        attribute_name.setText(attributeDetails.getName());
        if ( attributeDetails.isAvailabilityChangeDaywise() ){
            changeDaywise.setChecked(true);
        }else{
            notChangeDaywise.setChecked(true);
        }

        dialogUpdateAttributeDetailsView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String attributeName = attribute_name.getText();
                attribute_name.checkError("* Enter attribute name");
                boolean isAvailabilityChangeDayWise = false;
                if ( changeDaywise.isChecked() ){   isAvailabilityChangeDayWise = true; }

                if ( attributeName.length() > 0 ){
                    activity.closeCustomDialog();

                    RequestUpdateAttributeDetails requestUpdateAttributeDetails = new RequestUpdateAttributeDetails(attributeName, isAvailabilityChangeDayWise);
                    updateAttributeDetails(requestUpdateAttributeDetails);
                }

            }
        });

        dialogUpdateAttributeDetailsView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });
        activity.showCustomDialog(dialogUpdateAttributeDetailsView);

    }

    private void updateAttributeDetails(final RequestUpdateAttributeDetails requestUpdateAttributeDetails){

        activity.showLoadingDialog("Updating " + attributeDetails.getName().toLowerCase() + "'s details, wait...");
        activity.getBackendAPIs().getMeatItemsAPI().updateAttributeDetails(attributeDetails.getID(), requestUpdateAttributeDetails, new MeatItemsAPI.UpdateAttributeDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, AttributeDetails updatedAttributeDetails) {
                activity.closeLoadingDialog();
                if ( status ){
                    attributeDetails.setName(requestUpdateAttributeDetails.getName());
                    attributeDetails.setAvailabilityChangeDaywise(requestUpdateAttributeDetails.isAvailabilityChangeDaywise());

                    if ( updateAttributeCallback != null ){
                        updateAttributeCallback.onComplete(true, 200, "Success", attributeDetails);
                    }
                }else{
                    activity.showChoiceSelectionDialog("Failure!", "Something went wrong while updating " + attributeDetails.getName().toLowerCase() + "'s details.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                updateAttributeDetails(requestUpdateAttributeDetails);
                            }
                        }
                    });
                }
            }
        });
    }

    // UPDATE ATTRIBUTE OPTION AVAILABILITY

    public void updateAttributeOptionAvailability(final AttributeOptionDetails attributeOptionDetails, final UpdateAttributeOptionCallback callback){
        this.attributeOptionDetails = attributeOptionDetails;
        this.updateAttributeOptionCallback = callback;

        final List<AttributeOptionAvailabilityDetails> availabilitiesList = attributeOptionDetails.getFullAvlabilitiesList(DateMethods.getOnlyDate(localStorageData.getServerTime()), 7);

        View dialogUpdateAttributeOptionAvailabilityView = LayoutInflater.from(activity).inflate(R.layout.dialog_update_attribute_option_availability, null);
        final TextView title = (TextView) dialogUpdateAttributeOptionAvailabilityView.findViewById(R.id.title);
        final vSpinner date_spinner = (vSpinner) dialogUpdateAttributeOptionAvailabilityView.findViewById(R.id.date_spinner);
        final RadioButton yesAvailable = (RadioButton) dialogUpdateAttributeOptionAvailabilityView.findViewById(R.id.yesAvailable);
        final RadioButton notAvailable = (RadioButton) dialogUpdateAttributeOptionAvailabilityView.findViewById(R.id.notAvailable);
        final vTextInputLayout unavilable_reason = (vTextInputLayout) dialogUpdateAttributeOptionAvailabilityView.findViewById(R.id.unavilable_reason);
        final vTextInputLayout price_difference = (vTextInputLayout) dialogUpdateAttributeOptionAvailabilityView.findViewById(R.id.price_difference);

        title.setText("Update "+attributeOptionDetails.getName()+" availability");

        notAvailable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if ( isChecked ){
                    unavilable_reason.setVisibility(View.VISIBLE);
                }else{
                    unavilable_reason.setText("");
                    unavilable_reason.setVisibility(View.GONE);
                }
            }
        });

        AdapterView.OnItemSelectedListener spinnerItemSelectedListener = new AdapterView.OnItemSelectedListener() {
            int previousPosition = -1;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ( previousPosition != -1 ) {
                    AttributeOptionsAvailabilitiesAdapter adapter = (AttributeOptionsAvailabilitiesAdapter) date_spinner.getAdapter();
                    AttributeOptionAvailabilityDetails availabilityDetailsForDate = adapter.getItem(position);

                    price_difference.setText(CommonMethods.getInDecimalFormat(availabilityDetailsForDate.getPriceDifference()));
                    if ( availabilityDetailsForDate.isAvailable() ){
                        yesAvailable.setChecked(true);
                        unavilable_reason.setVisibility(View.GONE);
                        unavilable_reason.setText("");
//                                            price_difference.setVisibility(View.VISIBLE);
                    }else{
                        notAvailable.setChecked(true);
                        unavilable_reason.setVisibility(View.VISIBLE);
                        unavilable_reason.setText(availabilityDetailsForDate.getAvailabilityReason());
//                                            price_difference.setVisibility(View.GONE);
                    }
                }
                previousPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
        date_spinner.setOnItemSelectedListener(spinnerItemSelectedListener);


        AttributeOptionsAvailabilitiesAdapter dateSpinnerAdapter = new AttributeOptionsAvailabilitiesAdapter(activity, availabilitiesList);
        dateSpinnerAdapter.setDropDownViewResource(R.layout.view_list_item_dropdown_item);
        date_spinner.setAdapter(dateSpinnerAdapter);
        date_spinner.setSelection(0);
        spinnerItemSelectedListener.onItemSelected(null, null, 0, 0);

        dialogUpdateAttributeOptionAvailabilityView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String selectedDate = "";
                try{    selectedDate = DateMethods.getDateInFormat(availabilitiesList.get(date_spinner.getSelectedItemPosition()).getDate(), "dd-MM-yyyy");}catch (Exception e){}
                String priceDifference = price_difference.getText().trim();
                price_difference.checkError("* Enter price difference");
                boolean isAvailable = false;
                String availabilityReason = "";
                if (yesAvailable.isChecked()) {
                    isAvailable = true;
                }else{
                    availabilityReason = unavilable_reason.getText().toString();
                    unavilable_reason.checkError("* Enter unavailable reason");
                }

                if ( selectedDate.length() > 0 && priceDifference.length() > 0 && (isAvailable || availabilityReason.length() > 0) ){
                    activity.closeCustomDialog();

                    RequestUpdateOptionAvailability requestUpdateOptionAvailability = new RequestUpdateOptionAvailability(selectedDate, Double.parseDouble(priceDifference), isAvailable, availabilityReason);
                    checkAfftecedMeatItemsAndProceedUpdateOptionAvailability(requestUpdateOptionAvailability);
                }

            }
        });
        dialogUpdateAttributeOptionAvailabilityView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });
        activity.showCustomDialog(dialogUpdateAttributeOptionAvailabilityView);
    }

    private void checkAfftecedMeatItemsAndProceedUpdateOptionAvailability(final RequestUpdateOptionAvailability requestUpdateOptionAvailability){

        activity.showLoadingDialog("Updating "+attributeOptionDetails.getName().toLowerCase()+"'s availability on "+DateMethods.getDateInFormat(requestUpdateOptionAvailability.getDate(), DateConstants.MMM_DD_YYYY)+", wait...");
        activity.getBackendAPIs().getMeatItemsAPI().getAfftecedMeatItems(attributeOptionDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("The following meat items will be affected after change. confirm to proceed.<br><br>"+message), "CANCEL", "PROCEED", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("PROCEED")){
                                updateAttributeOptionAvailability(requestUpdateOptionAvailability);
                            }
                        }
                    });
                }else{
                    activity.showChoiceSelectionDialog("Failure!", "Something went wrong while updating " + attributeOptionDetails.getName().toLowerCase() + "'s availability on " + DateMethods.getDateInFormat(requestUpdateOptionAvailability.getDate(), DateConstants.MMM_DD_YYYY) + "\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                checkAfftecedMeatItemsAndProceedUpdateOptionAvailability(requestUpdateOptionAvailability);
                            }
                        }
                    });
                }
            }
        });
    }

    private void updateAttributeOptionAvailability(final RequestUpdateOptionAvailability requestUpdateOptionAvailability){

        activity.showLoadingDialog("Updating "+attributeOptionDetails.getName().toLowerCase()+"'s availability on "+DateMethods.getDateInFormat(requestUpdateOptionAvailability.getDate(), DateConstants.MMM_DD_YYYY)+", wait...");
        activity.getBackendAPIs().getMeatItemsAPI().updateAttributeOptionAvailability(attributeOptionDetails.getID(), requestUpdateOptionAvailability, new MeatItemsAPI.UpdateAttributeOptionAvailabilityCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, double latestPriceDifference, List<AttributeOptionAvailabilityDetails> availabilitiesList) {
                activity.closeLoadingDialog();
                if ( status ) {
                    activity.showToastMessage("Successfully updated availability details");

                    attributeOptionDetails.setPriceDifference(latestPriceDifference);
                    attributeOptionDetails.setAvailabilitiesList(availabilitiesList);
                    if ( updateAttributeOptionCallback != null ){
                        updateAttributeOptionCallback.onComplete(true, 200, "Success", attributeOptionDetails);
                    }
                }else{
                    activity.showChoiceSelectionDialog("Failure!", "Something went wrong while updating " + attributeOptionDetails.getName().toLowerCase() + "'s availability on " + DateMethods.getDateInFormat(requestUpdateOptionAvailability.getDate(), DateConstants.MMM_DD_YYYY) + "\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                updateAttributeOptionAvailability(requestUpdateOptionAvailability);
                            }
                        }
                    });
                }
            }
        });
    }
}
