package com.mastaan.logistics.flows;

import android.text.Html;

import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.RoutesAPI;
import com.mastaan.logistics.models.HubDetails;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 27/4/16.
 */

public class AcknowledgeFlow {

    MastaanToolbarActivity activity;
    OrderItemDetails orderItemDetails;
    StatusCallback callback;

    public interface AssignHubCallback{
        void onComplete(boolean status, int statusCode, String message, HubDetails hubDetails);
    }


    public AcknowledgeFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
    }

    public void assignHubToOrder(final  OrderItemDetails orderItemDetails, final AssignHubCallback callback){
        activity.showLoadingDialog("Loading hubs, wait...");
        activity.getBackendAPIs().getRoutesAPI().getHubs(new RoutesAPI.HubsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<HubDetails> hubsList) {
                activity.closeLoadingDialog();
                if ( status ){
                    List<String> hubsNames = new ArrayList<>();
                    for (int i=0;i<hubsList.size();i++){
                        hubsNames.add(hubsList.get(i).getName());
                    }

                    activity.showListChooserDialog("Select hub", hubsNames, new ListChooserCallback() {
                        @Override
                        public void onSelect(int position) {
                            assignHub(hubsList.get(position));
                        }
                    });
                }else{
                    activity.showToastMessage("Unable to load hubs, try again!");
                }
            }

            private void assignHub(final HubDetails hubDetails){
                activity.showLoadingDialog("Assigning hub, wait...");
                activity.getBackendAPIs().getOrdersAPI().assignHubToOrder(orderItemDetails.getOrderID(), hubDetails.getID(), new StatusCallback() {
                    @Override
                    public void onComplete(final boolean status, final int statusCode, final String message) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            if ( callback != null ){
                                callback.onComplete(status, statusCode, message, hubDetails);
                            }
                        }else{
                            activity.showChoiceSelectionDialog(false, "Failure", "Unable to update hub details.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if ( choiceName.equalsIgnoreCase("YES") ){
                                        assignHub(hubDetails);
                                    }else{
                                        if ( callback != null ){
                                            callback.onComplete(status, statusCode, message, null);
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    public void markAsAcknowledged(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        /*long difBetweenCurrentTimeAndDeliverySlotInMinutes = DateMethods.getDifferenceBetweenDatesInMilliSecondsWithSign(DateMethods.getCurrentTime(), orderItemDetails.getDeliveryTimeSlot())/(60*1000);
        if ( difBetweenCurrentTimeAndDeliverySlotInMinutes >= -1*(orderItemDetails.getOrderDetails().deliverEarlyIfPossible()?7:5)*60 ){*/
            activity.showLoadingDialog("Marking as acknowledged, wait...");
            activity.getBackendAPIs().getOrdersAPI().markAsAcknowledged(orderItemDetails.getID()/*, location*/, new StatusCallback() {
                @Override
                public void onComplete(boolean status, int statusCode, String message) {
                    activity.closeLoadingDialog();
                    if (status) {
                        activity.showToastMessage("Status updated to acknowledged successfully.");
                        callback.onComplete(true, 200, "Success");
                    }
                    else {
                        activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while marking status as acknowledged.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if (choiceName.equalsIgnoreCase("RETRY")) {
                                    markAsAcknowledged(orderItemDetails, callback);
                                }
                            }
                        });
                    }
                }
            });
        /*}
        else{
            long remainTimeToAcknowledge = Math.abs(difBetweenCurrentTimeAndDeliverySlotInMinutes) - (orderItemDetails.getOrderDetails().deliverEarlyIfPossible()?7:5)*60;
            activity.showNotificationDialog("Alert", CommonMethods.fromHtml("You can not acknowledge the item at this time. Try again after <b>"+remainTimeToAcknowledge+" mins</b>."));
        }*/
    }

    public void markAsNew(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Are you sure to mark <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+"</b> of <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + " as <b>New</b>?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {

                if (choiceName.equalsIgnoreCase("YES")) {
                    activity.showLoadingDialog("Removing processing info, wait...");
                    activity.getBackendAPIs().getOrdersAPI().markAsNew(orderItemDetails.getID(), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            activity.closeLoadingDialog();
                            if (status) {
                                activity.showToastMessage("Marked as new successfully");
                                callback.onComplete(true, 200, "Success");
                            } else {
                                activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while marking as new.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                                    @Override
                                    public void onSelect(int choiceNo, String choiceName) {
                                        if (choiceName.equalsIgnoreCase("RETRY")) {
                                            markAsNew(orderItemDetails, callback);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });

    }

}