package com.mastaan.logistics.flows;

import android.location.Location;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.constants.ResponseConstants;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vAppCompatEditText;
import com.aleena.common.widgets.vRecyclerView;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.adapters.StocksAdapter;
import com.mastaan.logistics.backend.StockAPI;
import com.mastaan.logistics.backend.VendorsAndButchersAPI;
import com.mastaan.logistics.backend.models.RequestUpdateOrderItemStock;
import com.mastaan.logistics.backend.models.RequestUpdateOrderItemVendor;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemDetailsCallback;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.AttributeOptionDetails;
import com.mastaan.logistics.models.HubMeatItemStockDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.ProcessedStockDetails;
import com.mastaan.logistics.models.StockDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.UserDetailsMini;
import com.mastaan.logistics.models.Vendor;
import com.mastaan.logistics.models.WarehouseMeatItemStockDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 27/4/16.
 */
public class ProcessingFlow {

    MastaanToolbarActivity activity;
    LocalStorageData localStorageData;
    OrderItemDetails orderItemDetails;
    Location currentLocation;
    StatusCallback callback;

    UserDetails userDetails;


    private interface VendorSelectionCallback{
        void onComplete(final Vendor vendorDetails, final String paymentType, final double amountPaid, final String comments);
    }

    private interface ButcherSelectionCallback{
        void onComplete(final UserDetailsMini butcherDetails);
    }

    private interface StockSelectionCallback{
        void onComplete(final StockDetails stockDetails, final ProcessedStockDetails processedStockDetails);
    }


    public ProcessingFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
        this.localStorageData = new LocalStorageData(activity);
        this.userDetails = localStorageData.getUserDetails();
    }

    public void updateSpecialInstructions(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        View specialInstructionsDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_update_special_instructions, null);
        final vAppCompatEditText special_instructions = (vAppCompatEditText) specialInstructionsDialogView.findViewById(R.id.special_instructions);

        special_instructions.setText(orderItemDetails.getSpecialInstructions());

        specialInstructionsDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();
            }
        });

        specialInstructionsDialogView.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();
                String specialInstructions = special_instructions.getText().toString();
                updateSpecialInstructions(specialInstructions);
            }
        });
        activity.showCustomDialog(specialInstructionsDialogView);
    }

    private void updateSpecialInstructions(final String specialInstructions){

        activity.showLoadingDialog("Updating special instructions, wait...");
        activity.getBackendAPIs().getOrdersAPI().updateSpecialInstructionsoOfOrderItem(orderItemDetails.getID(), specialInstructions, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Special instructions updated successfully.");
                    if ( callback != null ){
                        callback.onComplete(true, 200, specialInstructions);
                    }
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating special instructions.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                updateSpecialInstructions(specialInstructions);
                            }
                        }
                    });
                }
            }
        });
    }

    public void claimProcessing(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Can you process the <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+"</b> of <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {

                if (choiceName.equalsIgnoreCase("YES")) {

//                    activity.getCurrentLocation(new LocationCallback() {
//                        @Override
//                        public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
//                            if (status) {
                                activity.showLoadingDialog("Updating your processing request, wait...");
                                activity.getBackendAPIs().getOrdersAPI().markAsProcessing(/*location.getLatitude(), location.getLongitude(), */orderItemDetails.getID(), new StatusCallback() {
                                    @Override
                                    public void onComplete(boolean status, int statusCode, String message) {
                                        activity.closeLoadingDialog();
                                        if (status) {
                                            activity.showToastMessage("Your processing request updated successfully");
                                            callback.onComplete(true, 200, new Gson().toJson(localStorageData.getUserDetails()));//"Success");
                                            /*orderItemsAdapter.markAsProcessing(position);
                                            try {
                                                NewOrdersActivity act = (NewOrdersActivity) getActivity();
                                                deleteFromItemsList(orderItemsAdapter.getItem(position).getID());
                                                //activity.onReloadPressed();
                                            }catch (Exception f){}*/
                                        } else {
                                            activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating your processing request.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                                                @Override
                                                public void onSelect(int choiceNo, String choiceName) {
                                                    if (choiceName.equalsIgnoreCase("YES")) {
                                                        claimProcessing(orderItemDetails, callback);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
//                            } else {
//                                activity.showToastMessage(message);
//                            }
//                        }
//                    });
                }
            }
        });
    }

    public void removeProcessingBy(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Are you sure to remove processing info of <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+"</b> of <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {

                if (choiceName.equalsIgnoreCase("YES")) {
                    activity.showLoadingDialog("Removing processing info, wait...");
                    activity.getBackendAPIs().getOrdersAPI().removeProcessingBy(orderItemDetails.getID(), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            activity.closeLoadingDialog();
                            if (status) {
                                activity.showToastMessage("Processing info removoed successfully");
                                callback.onComplete(true, 200, "Success");
                            } else {
                                activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while removing processing info.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                                    @Override
                                    public void onSelect(int choiceNo, String choiceName) {
                                        if (choiceName.equalsIgnoreCase("YES")) {
                                            claimProcessing(orderItemDetails, callback);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });

    }




    private void showSelectVendorDialog(final boolean skipPaymentInformationForCreditVendors, final Vendor selectedVendor, final VendorSelectionCallback callback){

        /*activity.getCurrentLocation(new LocationCallback() {
            @Override
            public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
                currentLocation = location;
                if ( currentLocation == null ){ activity.showToastMessage("Unable to sort vendors by distance");    }*/

                activity.showLoadingDialog("Getting vendors, wait...");
                activity.getBackendAPIs().getVendorsAndButchersAPI().getVendorsForOrderItem(orderItemDetails.getID(), currentLocation, new VendorsAndButchersAPI.VendorsCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, final List<Vendor> vendorsList) {
                        activity.closeLoadingDialog();
                        if (status) {
                            List<String> vendorsStrings = new ArrayList<String>();
                            int mastaanVendorIndex = -1;
                            int selectedVendorIndex = -1;
                            for (int i=0;i<vendorsList.size();i++){
                                vendorsStrings.add(vendorsList.get(i).getName());
                                if ( selectedVendor != null && selectedVendorIndex == -1 && vendorsList.get(i).getID().equals(selectedVendor.getID()) ){
                                    selectedVendorIndex = i;
                                }
                                if ( mastaanVendorIndex < 0 && (vendorsList.get(i).isOwn() /*TODO Removable*/ || vendorsList.get(i).getName().toUpperCase().contains("warehouse")) ){//vendorsList.get(i).getName().trim().equalsIgnoreCase("Mastaan") || vendorsList.get(i).getName().trim().equalsIgnoreCase("Mastaan Warehouse")) ){
                                    mastaanVendorIndex = i;
                                }
                            }

                            ListChooserCallback listChooserCallback = new ListChooserCallback() {
                                @Override
                                public void onSelect(int position) {
                                    final Vendor selectedVendor = vendorsList.get(position);

                                    // If Payment type is credit
                                    if ( skipPaymentInformationForCreditVendors &&
                                            selectedVendor.getPaymentType().equalsIgnoreCase(Constants.CREDIT) ){
                                        if ( callback != null ) {
                                            callback.onComplete(selectedVendor, "cr", orderItemDetails.getVendorPrice(), "");
                                        }
                                    }
                                    // If Payment type is cash
                                    else{
                                        View amountPaidDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_amount_paid, null);
                                        final RadioGroup paymentTypeRadioGroup = (RadioGroup) amountPaidDialogView.findViewById(R.id.paymentTypeRadioGroup);
                                        final RadioButton cashPayment = (RadioButton) amountPaidDialogView.findViewById(R.id.cashPayment);
                                        final RadioButton creditPayment = (RadioButton) amountPaidDialogView.findViewById(R.id.creditPayment);
                                        final EditText amount_paid = (EditText) amountPaidDialogView.findViewById(R.id.amount_paid);
                                        final EditText input_comments = (EditText) amountPaidDialogView.findViewById(R.id.comments);

                                        if ( orderItemDetails.isCashNCarryItem() ){
                                            paymentTypeRadioGroup.setVisibility(View.GONE);
                                        }

                                        amount_paid.setText("" + CommonMethods.getInDecimalFormat(orderItemDetails.getVendorPrice()));
                                        if ( orderItemDetails.getVendorPaidAmount() > 0 ){
                                            amount_paid.setText("" + CommonMethods.getInDecimalFormat(orderItemDetails.getVendorPaidAmount()));
                                        }

                                        amountPaidDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                String paymentType = "";
                                                if ( orderItemDetails.isCashNCarryItem() ){
                                                    paymentType = "ca";
                                                }else {
                                                    if (cashPayment.isChecked()) {
                                                        paymentType = "ca";
                                                    }if (creditPayment.isChecked()) {
                                                        paymentType = "cr";
                                                    }
                                                }
                                                final String amountPaid = amount_paid.getText().toString();
                                                String comments = input_comments.getText().toString();

                                                if (paymentType.length() > 0) {
                                                    if ( amountPaid.length() > 0 ) {
                                                        if ( orderItemDetails.isCashNCarryItem() == false || Double.parseDouble(amountPaid) > 0  ){
                                                            activity.closeCustomDialog();
                                                            //linkVendorAndButcherToOrderItem(position, location, place.getID(), paymentType, Double.parseDouble(amountPaid), comments);
                                                            if ( callback != null ) {
                                                                callback.onComplete(selectedVendor, paymentType, Double.parseDouble(amountPaid), comments);
                                                                //linkVendorToOrderItemAndCompleteProcessing(place.getID(), paymentType, amountPaid, comments);
                                                            }
                                                        }else {
                                                            activity.showToastMessage("* Please enter valid amount");
                                                        }
                                                    } else {
                                                        activity.showToastMessage("* Please enter amount");
                                                    }
                                                } else {
                                                    activity.showToastMessage("* Please select payment type");
                                                }

                                            }
                                        });

                                        amountPaidDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                activity.closeCustomDialog();
                                            }
                                        });

                                        activity.showCustomDialog(amountPaidDialogView, false);
                                    }
                                }
                            };

                            // FOR NON CashNCarry Item directly selecting Mastaan as Vendor
                            if ( orderItemDetails.isCashNCarryItem() == false && mastaanVendorIndex >=0 ){
                                listChooserCallback.onSelect(mastaanVendorIndex);
                            }
                            else{
                                activity.showListChooserDialog("Select vendor", vendorsStrings, selectedVendorIndex, listChooserCallback);
                            }
                        }
                        else {
                            activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while getting vendors list.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if (choiceName.equalsIgnoreCase("YES")) {
                                        showSelectVendorDialog(skipPaymentInformationForCreditVendors, selectedVendor, callback);
                                    }
                                }
                            });
                        }
                    }
                });
            /*}
        });*/
    }

    private void showSelectButcherDialog(final UserDetailsMini selectedButcher, final ButcherSelectionCallback callback){

        activity.showLoadingDialog("Loading butchers, wait...");
        activity.getBackendAPIs().getVendorsAndButchersAPI().getButchersForOrderItem(orderItemDetails.getID(), new VendorsAndButchersAPI.ButchersCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, final List<UserDetailsMini> butchersList) {
                activity.closeLoadingDialog();
                if ( status ){
                    List<String> butchersStrings = new ArrayList<String>();
                    int selectedButcherIndex = -1;
                    for (int i=0;i<butchersList.size();i++){
                        butchersStrings.add(butchersList.get(i).getAvailableName());
                        if ( selectedButcher != null && selectedButcherIndex == -1 && butchersList.get(i).getID().equals(selectedButcher.getID()) ){
                            selectedButcherIndex = i;
                        }
                    }

                    activity.showListChooserDialog("Select butcher", butchersStrings, selectedButcherIndex, new ListChooserCallback() {
                        @Override
                        public void onSelect(int position) {
                            if ( callback != null ){
                                callback.onComplete(butchersList.get(position));
                            }
                        }
                    });
                }
                else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while getting butchers list.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                showSelectButcherDialog(selectedButcher, callback);
                            }
                        }
                    });
                }
            }
        });
    }

    private void showSelectStockDialog(final String selectedStock, final String selectedProcessedStock, final StockSelectionCallback callback){

        activity.showLoadingDialog("Loading stocks, wait...");
        activity.getBackendAPIs().getStockAPI().getWarehouseMeatItemStockFromOrderItem(orderItemDetails.getID(), new StockAPI.WarehouseMeatItemStockCallback() {//TODO ?ilas
            @Override
            public void onComplete(boolean status, int statusCode, String message, WarehouseMeatItemStockDetails warehouseMeatItemStockDetails) {
                if ( status ){
                    if ( warehouseMeatItemStockDetails.getTypeName().equalsIgnoreCase(Constants.UNLIMITED) ){
                        showProcessedStocks(false);
                    }else{
                        showStocks(warehouseMeatItemStockDetails, false);
                    }
                }
                else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while getting stocks list.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                showSelectStockDialog(selectedStock, selectedProcessedStock, callback);
                            }
                        }
                    });
                }
            }
            //-------

            private void showStocks(final WarehouseMeatItemStockDetails warehouseMeatItemStockDetails, boolean showLoadingDialog){
                if ( showLoadingDialog ){ activity.showLoadingDialog("Loading stocks, wait..."); }
                activity.getBackendAPIs().getStockAPI().getStocksForWarehouseMeatItem(orderItemDetails.getWarehouseMeatItemID(), warehouseMeatItemStockDetails.getItemLevelAttributeOptionsIDs(), new StockAPI.StocksListCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, final List<StockDetails> stocksList) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            if ( stocksList.size() == 0 ){
                                activity.showToastMessage("No stock exists for meat item");
                                if ( callback != null ){
                                    callback.onComplete(null, null);
                                }
                            }
                            else{
                                final List<String> stocksStrings = new ArrayList<>();
                                int selectedStockIndex = -1;
                                for (int i=0;i<stocksList.size();i++){
                                    StockDetails stockDetails = stocksList.get(i);
                                    stocksStrings.add("<b><u>"+DateMethods.getReadableDateFromUTC(stockDetails.getDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</u></b>"
                                            +"<br>Quantity: <b>"+CommonMethods.getIndianFormatNumber(stockDetails.getQuantity())+" "+stockDetails.getQuantityUnit()+(stockDetails.getQuantity()==1?"":"s")+"</b>"+(stockDetails.getNoOfUnits()>0?"  (<b>"+stockDetails.getNoOfUnits()+"unit"+(stockDetails.getNoOfUnits()==1?"":"s")+"</b>)":"")
                                            +(stockDetails.getVendorDetails()!=null?", Vendor: <b>"+stockDetails.getVendorDetails().getName()+"</b>":"")
                                    );

                                    if ( selectedStock != null && selectedStockIndex == -1 && stockDetails.getID().equals(selectedStock) ){
                                        selectedStockIndex = i;
                                    }
                                }

                                activity.showListChooserDialog("Select stock", stocksStrings, selectedStockIndex, new ListChooserCallback() {
                                    @Override
                                    public void onSelect(int position) {
                                        if ( callback != null ){
                                            callback.onComplete(stocksList.get(position), null);
                                        }
                                    }
                                });
                            }
                        }
                        else{
                            activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while getting stocks list.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if (choiceName.equalsIgnoreCase("YES")) {
                                        showStocks(warehouseMeatItemStockDetails, true);
                                    }
                                }
                            });
                        }
                    }
                });
            }

            private void showProcessedStocks(boolean showLoadingDialog){
                if ( showLoadingDialog ){ activity.showLoadingDialog("Loading stocks, wait..."); }
                activity.getBackendAPIs().getStockAPI().getProcessedStocksForCategory(orderItemDetails.getMeatItemDetails().getCategoryDetails().getCategoryValue(), new StockAPI.ProcessedStocksListCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, final List<ProcessedStockDetails> stocksList) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            if ( stocksList.size() == 0 ){
                                activity.showToastMessage("No stock exists for category");
                                if ( callback != null ){
                                    callback.onComplete(null, null);
                                }
                            }
                            else{
                                final List<String> stocksStrings = new ArrayList<>();
                                int selectedStockIndex = -1;
                                for (int i=0;i<stocksList.size();i++){
                                    ProcessedStockDetails stockDetails = stocksList.get(i);

                                    String afterProcessingInfo = "";//"After Processing:";
                                    if ( stockDetails.getAfterProcessingWeight() > 0 ){
                                        if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
                                        afterProcessingInfo += "Weight: <b>"+CommonMethods.getIndianFormatNumber(stockDetails.getAfterProcessingWeight())+" kgs</b>";
                                    }
                                    if ( stockDetails.getAfterProcessingDressedWeight() > 0 ){
                                        if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
                                        afterProcessingInfo += "Dressed: <b>"+CommonMethods.getIndianFormatNumber(stockDetails.getAfterProcessingDressedWeight())+" kgs</b>";
                                    }
                                    if ( stockDetails.getAfterProcessingSkinlessWeight() > 0 ){
                                        if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
                                        afterProcessingInfo += "Skinless: <b>"+CommonMethods.getIndianFormatNumber(stockDetails.getAfterProcessingSkinlessWeight())+" kgs</b>";
                                    }
                                    if ( stockDetails.getAfterProcessingLiverWeight() > 0 ){
                                        if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
                                        afterProcessingInfo += "Liver: <b>"+CommonMethods.getIndianFormatNumber(stockDetails.getAfterProcessingLiverWeight())+" kgs</b>";
                                    }
                                    if ( stockDetails.getAfterProcessingGizzardWeight() > 0 ){
                                        if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
                                        afterProcessingInfo += "Gizzard: <b>"+CommonMethods.getIndianFormatNumber(stockDetails.getAfterProcessingGizzardWeight())+" kgs</b>";
                                    }
                                    if ( stockDetails.getAfterProcessingBoteeWeight() > 0 ){
                                        if ( afterProcessingInfo.length() > 0 ){    afterProcessingInfo += ", ";  }
                                        afterProcessingInfo += "Botee: <b>"+CommonMethods.getIndianFormatNumber(stockDetails.getAfterProcessingBoteeWeight())+" kgs</b>";
                                    }

                                    stocksStrings.add("<b><u>"+DateMethods.getReadableDateFromUTC(stockDetails.getDate(), DateConstants.MMM_DD_YYYY_HH_MM_A)+"</u></b>"
                                            +"<br>"+afterProcessingInfo
                                    );

                                    if ( selectedProcessedStock != null && selectedStockIndex == -1 && stockDetails.getID().equals(selectedProcessedStock) ){
                                        selectedStockIndex = i;
                                    }
                                }

                                activity.showListChooserDialog("Select stock", stocksStrings, selectedStockIndex, new ListChooserCallback() {
                                    @Override
                                    public void onSelect(int position) {
                                        if ( callback != null ){
                                            callback.onComplete(null, stocksList.get(position));
                                        }
                                    }
                                });
                            }
                        }
                        else{
                            activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while getting stocks list.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if (choiceName.equalsIgnoreCase("YES")) {
                                        showProcessedStocks(true);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    //---------

    public void completeProcessing(final OrderItemDetails order_item_details, final OrderItemDetailsCallback callback) {
        this.orderItemDetails = order_item_details;

        activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Did you complete the processing of <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+"</b> of <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equalsIgnoreCase("YES")) {
                    // Asking net weight for seafood items
                    if ( orderItemDetails.getMeatItemDetails().getCategoryDetails().getCategoryIdentifier().equalsIgnoreCase("seafood") ){
                        activity.showInputNumberDecimalDialog("Final net weight", "Weight (in kgs)", orderItemDetails.getQuantityUnit().equalsIgnoreCase("kg")?orderItemDetails.getQuantity():0, new TextInputCallback() {
                            @Override
                            public void onComplete(final String inputText) {
                                final double netWeight = CommonMethods.parseDouble(inputText);
                                if ( netWeight > 0 ){
                                    activity.showLoadingDialog("Updating net weight, wait...");
                                    activity.getBackendAPIs().getOrdersAPI().updateNetWeight(orderItemDetails.getID(), netWeight, new StatusCallback() {
                                        @Override
                                        public void onComplete(boolean status, int statusCode, String message) {
                                            activity.closeLoadingDialog();
                                            if ( status ){
                                                orderItemDetails.setFinalNetWeight(netWeight);
                                                continueWithCompleteProcessing();
                                            }else{
                                                activity.showToastMessage("Unable to update net weight, try again!");
                                            }
                                        }
                                    });
                                }else{
                                    activity.showToastMessage("Final net weight must be greater than 0");
                                }
                            }
                        });
                    }
                    else{
                        continueWithCompleteProcessing();
                    }
                }
            }

            private void continueWithCompleteProcessing(){
                // Not checking hub stock availability as order item has already linked to stock & Also showing update stock flow to update stock in case if required
                if ( orderItemDetails.getStock() != null && orderItemDetails.getStock().length() > 0 ){
                    continueWithUpdateStock();
                }
                else{
                    activity.showLoadingDialog("Checking hub stock, wait...");
                    activity.getBackendAPIs().getStockAPI().getHubMeatItemStockFromOrderItem(orderItemDetails.getID(), new StockAPI.HubMeatItemStockCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message, HubMeatItemStockDetails hubMeatItemStockDetails) {
                            activity.closeLoadingDialog();
                            if ( status ){
                                if ( hubMeatItemStockDetails.getQuantity() >= orderItemDetails.getQuantity() ){
                                    continueWithUpdateStock();
                                }
                                else{
                                    if ( userDetails.isSuperUser() ){   // TEMP
                                        activity.showChoiceSelectionDialog("SUPER USER Confirmation", "No stock available at the hub.\n\nDo you still want to complete processing?", "NO", "YES", new ChoiceSelectionCallback() {
                                            @Override
                                            public void onSelect(int choiceNo, String choiceName) {
                                                if ( choiceName.equalsIgnoreCase("YES") ){
                                                    markAsPickupPending();
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        String itemLevelAttributesOptionsNames = "";
                                        for (int i = 0; i < hubMeatItemStockDetails.getItemLevelAttributeOptions().size(); i++) {
                                            String optionName = hubMeatItemStockDetails.getItemLevelAttributeOptions().get(i).getID();
                                            for (int a = 0; a < orderItemDetails.getSelectedAttributes().size(); a++) {
                                                AttributeOptionDetails optionDetails = orderItemDetails.getSelectedAttributes().get(a).getOptionDetails();
                                                if (optionDetails != null && hubMeatItemStockDetails.getItemLevelAttributeOptions().get(i).getID().equals(optionDetails.getID())) {
                                                    optionName = optionDetails.getName();
                                                    break;
                                                }
                                            }
                                            itemLevelAttributesOptionsNames += (itemLevelAttributesOptionsNames.length() > 0 ? ", " : "") + optionName;
                                        }
                                        String formattedItemName = orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize() + (itemLevelAttributesOptionsNames.length() > 0 ? " " + itemLevelAttributesOptionsNames : "");

                                        activity.showNotificationDialog("Alert", CommonMethods.fromHtml("Selected hub (<b>" + orderItemDetails.getHubDetails().getName() + "</b>) don't have enough stock (<b>available: " + CommonMethods.getIndianFormatNumber(hubMeatItemStockDetails.getQuantity()) + " " + orderItemDetails.getQuantityUnit() + "s</b>) to fulfill the order item (<b>" +/*formattedItemName+", "*/"quantity: " + CommonMethods.getIndianFormatNumber(orderItemDetails.getQuantity()) + " " + orderItemDetails.getQuantityUnit() + "s</b>). Please bring <b>" + CommonMethods.getIndianFormatNumber(orderItemDetails.getQuantity()) + " " + orderItemDetails.getQuantityUnit() + "s</b> of <b>" + formattedItemName + "</b> stock from nearest hub/warehouse and then complete processing."));
                                    }
                                }
                            }
                            else{
                                activity.showToastMessage("Something went wrong, try again!");
                            }
                        }


                    });
                }
            }

            private void continueWithUpdateStock(){
                updateStockDetails(orderItemDetails, new OrderItemDetailsCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message, OrderItemDetails order_item_details) {
                        if ( status ){
                            orderItemDetails = order_item_details;
                            markAsPickupPending();
                        }
                        else {
                            if ( statusCode == ResponseConstants.NO_DATA ){
                                if ( userDetails.isSuperUser() ){   // TEMP
                                    activity.showChoiceSelectionDialog("SUPER USER Confirmation", "No hub stock entry available.\n\nDo you still want to complete processing?", "NO", "YES", new ChoiceSelectionCallback() {
                                        @Override
                                        public void onSelect(int choiceNo, String choiceName) {
                                            if ( choiceName.equalsIgnoreCase("YES") ){
                                                markAsPickupPending();
                                            }
                                        }
                                    });
                                }else {
                                    activity.showNotificationDialog("Alert", "Hub stock required to complete processing");
                                }
                            }
                        }
                    }
                });
            }

            private void markAsPickupPending() {

                activity.showLoadingDialog("Completing processing, wait...");
                activity.getBackendAPIs().getOrdersAPI().markAsPickUpPending(orderItemDetails.getID(), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        activity.closeLoadingDialog();
                        if (status) {
                            orderItemDetails.setStatus(2);
                            orderItemDetails.setProcessingCompletedDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));
                            /*Temporarily completing QC*/
                            orderItemDetails.setQualityCheckStatus(Constants.QC_DONE);
                            orderItemDetails.setQualityCheckDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));

                            activity.showToastMessage("Processing completed successfully.");
                            if ( callback != null ) {
                                callback.onComplete(true, 200, "Success", orderItemDetails);      // SENDING CALLBACK
                            }
                        }
                        else {
                            activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while completing processing.\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if (choiceName.equalsIgnoreCase("YES")) {
                                        markAsPickupPending();
                                    }
                                }
                            });
                        }
                    }
                });
            }

        });
    }

    /*public void completeProcessing(final OrderItemDetails orderItemDetails, final OrderItemDetailsCallback callback) {
        this.orderItemDetails = orderItemDetails;

        activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Did you complete the processing of <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+"</b> of <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {

                if (choiceName.equalsIgnoreCase("YES")) {
//                    activity.getCurrentLocation(new LocationCallback() {
//                        @Override
//                        public void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng) {
//                            if ( status ) {
//                                currentLocation = location;
                                showSelectVendorDialog(true, null, new VendorSelectionCallback() {
                                    @Override
                                    public void onComplete(final Vendor vendorDetails, final String paymentType, final double amountPaid, final String comments) {
                                        showSelectButcherDialog(null, new ButcherSelectionCallback() {
                                            @Override
                                            public void onComplete(final UserDetailsMini butcherDetails) {
                                                activity.showInputNumberDecimalDialog("Final net weight", "Weight (in kgs)", orderItemDetails.getQuantityUnit().equalsIgnoreCase("kg")?orderItemDetails.getQuantity():0, new TextInputCallback() {
                                                    @Override
                                                    public void onComplete(final String inputText) {
                                                        if ( CommonMethods.parseDouble(inputText) > 0 ){
                                                            showSelectStockDialog(null, null, new StockSelectionCallback() {
                                                                @Override
                                                                public void onComplete(StockDetails stockDetails, ProcessedStockDetails processedStockDetails) {
                                                                    linkVendorToOrderItemAndCompleteProcessing(vendorDetails, paymentType, amountPaid, comments, butcherDetails, CommonMethods.parseDouble(inputText), stockDetails, processedStockDetails, callback);
                                                                }
                                                            });
                                                        }else{
                                                            activity.showToastMessage("Final net weight must be greater than 0");
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
//                            }else{activity.showToastMessage(message);}
//                        }
//                    });
                }
            }
        });
    }

    private void linkVendorToOrderItemAndCompleteProcessing(final Vendor vendorDetails, final String paymentType, final double amountPaid, final String comments, final UserDetailsMini butcherDetails, final double finalNetWeight, final StockDetails stock, final ProcessedStockDetails processedStock, final OrderItemDetailsCallback callback) {

        activity.showLoadingDialog("Updating vendor details, wait...");
        activity.getBackendAPIs().getVendorsAndButchersAPI().linkVendorToOrderItem(orderItemDetails.getID(), new RequestLinkVendorAndButcherToOrderItem(vendorDetails.getID(), paymentType, amountPaid, comments, butcherDetails.getID(), finalNetWeight, stock!=null?stock.getID():null, processedStock!=null?processedStock.getID():null), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    orderItemDetails.setVendorDetails(vendorDetails);
                    orderItemDetails.setVendorPaymentType(paymentType);
                    orderItemDetails.setVendorAmount(amountPaid);
                    orderItemDetails.setVendorComments(comments);
                    orderItemDetails.setFinalNetWeight(finalNetWeight);

                    orderItemDetails.setButcherDetails(butcherDetails);

                    orderItemDetails.setStock(stock);
                    orderItemDetails.setProcessedStock(processedStock);

                    markAsPickupPendingAndCompleteProcessing(callback);
                }
                else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating vendor details.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                linkVendorToOrderItemAndCompleteProcessing(vendorDetails, paymentType, amountPaid, comments, butcherDetails, finalNetWeight, stock, processedStock, callback);
                            }
                        }
                    });
                }
            }
        });
    }

    private void markAsPickupPendingAndCompleteProcessing(final OrderItemDetailsCallback callback){

        activity.showLoadingDialog("Completing processing, wait...");
        activity.getBackendAPIs().getOrdersAPI().markAsPickUpPending(orderItemDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    orderItemDetails.setStatus(2);
                    orderItemDetails.setProcessingCompletedDate(DateMethods.getDateInUTCFormat(DateMethods.getCurrentDateAndTime()));

                    activity.showToastMessage("Processing completed successfully.");
                    callback.onComplete(true, 200, "Success", orderItemDetails);      // SENDING CALLBACK
                }
                else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while completing processing.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                markAsPickupPendingAndCompleteProcessing(callback);
                            }
                        }
                    });
                }
            }
        });
    }*/

    //---------

    public void updateVendorDetails(final OrderItemDetails orderItemDetails, final OrderItemDetailsCallback callback) {
        this.orderItemDetails = orderItemDetails;

        showSelectVendorDialog(false, orderItemDetails.getVendorDetails(), new VendorSelectionCallback() {
            @Override
            public void onComplete(final Vendor vendorDetails, String paymentType, double amountPaid, String comments) {
                updateVendorForOrderItem(vendorDetails, paymentType, amountPaid, comments, callback);
            }
        });
    }

    private void updateVendorForOrderItem(final Vendor vendorDetails, final String paymentType, final double amountPaid, final String comments, final OrderItemDetailsCallback callback) {

        activity.showLoadingDialog("Updating vendor details, wait...");
        activity.getBackendAPIs().getOrdersAPI().updateVendorDetails(orderItemDetails.getID(), new RequestUpdateOrderItemVendor(vendorDetails.getID(), paymentType, amountPaid, comments), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    orderItemDetails.setVendorDetails(vendorDetails);
                    orderItemDetails.setVendorPaymentType(paymentType);
                    orderItemDetails.setVendorAmount(amountPaid);
                    orderItemDetails.setVendorComments(comments);

                    activity.showToastMessage("Vendor details updated successfully");
                    if ( callback != null ) {
                        callback.onComplete(true, 200, "Success", orderItemDetails);
                    }
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating vendor details.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                updateVendorForOrderItem(vendorDetails, paymentType, amountPaid, comments, callback);
                            }
                        }
                    });
                }
            }
        });
    }

    //---------

    public void updateButcherDetails(final OrderItemDetails orderItemDetails, final OrderItemDetailsCallback callback) {
        this.orderItemDetails = orderItemDetails;

        showSelectButcherDialog(orderItemDetails.getButcherDetails(), new ButcherSelectionCallback() {
            @Override
            public void onComplete(UserDetailsMini butcherDetails) {
                updateButcherDetails(butcherDetails, callback);
            }
        });
    }
    private void updateButcherDetails(final UserDetailsMini butcherDetails, final OrderItemDetailsCallback callback) {

        activity.showLoadingDialog("Updating butcher details, wait...");
        activity.getBackendAPIs().getOrdersAPI().updateButcherDetails(orderItemDetails.getID(), butcherDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    orderItemDetails.setButcherDetails(butcherDetails);

                    activity.showToastMessage("Butcher details updated successfully");
                    if ( callback != null ) {
                        callback.onComplete(true, 200, "Success", orderItemDetails);
                    }
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating butcher details.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                updateButcherDetails(butcherDetails, callback);
                            }
                        }
                    });
                }
            }
        });
    }


    public void updateStockDetails(final OrderItemDetails orderItemDetails, final OrderItemDetailsCallback callback){
        activity.showLoadingDialog("Loading stocks, wait...");
        activity.getBackendAPIs().getStockAPI().getStocksForOrderItem(orderItemDetails.getID(), new StockAPI.StocksListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<StockDetails> stocksList) {
                activity.closeLoadingDialog();
                if ( status ){
                    showStocks(stocksList);
                }else{
                    activity.showToastMessage("Unable to load stocks, try again!");
                }
            }

            private void showStocks(final List<StockDetails> stocksList){
                if ( stocksList.size() > 0 ){
                    View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_list_chooser, null);
                    TextView title = dialogView.findViewById(R.id.title);
                    title.setText("Select stock");
                    vRecyclerView listHolder = dialogView.findViewById(R.id.listHolder);
                    listHolder.setupVerticalOrientation();
                    listHolder.setAdapter(new StocksAdapter(activity, stocksList, new StocksAdapter.Callback() {
                        @Override
                        public void onItemClick(int position) {
                            activity.closeCustomDialog();
                            updateStock(stocksList.get(position));
                        }

                        @Override
                        public void onOrderItemClick(int position) {}

                        @Override
                        public void onStockClick(int position) {}
                    }).setHubDetails(orderItemDetails.getHubDetails()));

                    activity.showCustomDialog(dialogView);
                }
                else{
                    activity.showToastMessage("No stocks to show");
                    if ( callback != null ){
                        callback.onComplete(false, ResponseConstants.NO_DATA, "No stocks", null);
                    }
                }
            }

            private void updateStock(final StockDetails stockDetails){

                activity.showLoadingDialog("Updating stock details, wait...");
                activity.getBackendAPIs().getOrdersAPI().updateStockDetails(orderItemDetails.getID(), new RequestUpdateOrderItemStock(stockDetails.getID(), null), new StatusCallback() {
                    @Override
                    public void onComplete(boolean status, int statusCode, String message) {
                        activity.closeLoadingDialog();
                        if ( status ){
                            orderItemDetails.setStock(stockDetails);

                            activity.showToastMessage("Stock details updated successfully");
                            if ( callback != null ) {
                                callback.onComplete(true, 200, "Success", orderItemDetails);
                            }
                        }
                        else{
                            activity.showChoiceSelectionDialog(false,"Failure", "Something went wrong while updating stock details.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                                @Override
                                public void onSelect(int choiceNo, String choiceName) {
                                    if ( choiceName.equalsIgnoreCase("YES") ){
                                        updateStock(stockDetails);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }
    /*public void updateStockDetails(final OrderItemDetails orderItemDetails, final OrderItemDetailsCallback callback) {
        this.orderItemDetails = orderItemDetails;

        showSelectStockDialog(orderItemDetails.getStock(), orderItemDetails.getProcessedStock(), new StockSelectionCallback() {
            @Override
            public void onComplete(StockDetails stockDetails, ProcessedStockDetails processedStockDetails) {
                updateStockDetails(stockDetails, processedStockDetails, callback);
            }
        });
    }
    private void updateStockDetails(final StockDetails stock, final ProcessedStockDetails processedStock, final OrderItemDetailsCallback callback) {

        activity.showLoadingDialog("Updating stock details, wait...");
        activity.getBackendAPIs().getOrdersAPI().updateStockDetails(orderItemDetails.getID(), new RequestUpdateOrderItemStock(stock!=null?stock.getID():null, processedStock!=null?processedStock.getID():null), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    orderItemDetails.setStock(stock);
                    orderItemDetails.setProcessedStock(processedStock);

                    activity.showToastMessage("Stock details updated successfully");
                    if ( callback != null ) {
                        callback.onComplete(true, 200, "Success", orderItemDetails);
                    }
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating stock details.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                updateStockDetails(stock, processedStock, callback);
                            }
                        }
                    });
                }
            }
        });
    }*/


    //---------

    public void markAsCashNCarryItem(final OrderItemDetails orderItemDetails, final StatusCallback callback){
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showLoadingDialog("Marking as Cash N Carry item, wait...");
        activity.getBackendAPIs().getOrdersAPI().markAsCashNCarryItem(orderItemDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Marked as Cash N Carry item successfully.");
                    callback.onComplete(true, 200, "Success");      // SENDING CALLBACK
                }
                else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while marking as cash n carry item.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                markAsCashNCarryItem(orderItemDetails, callback);
                            }
                        }
                    });
                }
            }
        });
    }

    public void unmarkAsCashNCarryItem(final OrderItemDetails orderItemDetails, final StatusCallback callback){
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showLoadingDialog("Unmarking as Cash N Carry item, wait...");
        activity.getBackendAPIs().getOrdersAPI().unmarkAsCashNCarryItem(orderItemDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("UnmMarked as Cash N Carry item successfully.");
                    callback.onComplete(true, 200, "Success");      // SENDING CALLBACK
                }
                else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while unmarking as cash n carry item.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                unmarkAsCashNCarryItem(orderItemDetails, callback);
                            }
                        }
                    });
                }
            }
        });
    }


}
