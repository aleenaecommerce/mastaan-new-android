package com.mastaan.logistics.flows;

import com.aleena.common.interfaces.ListChooserCallback;
import com.mastaan.logistics.activities.MastaanToolbarActivity;

/**
 * Created by Venkatesh Uppu on 7/1/17.
 */
public class ActionFABsFlow {
    MastaanToolbarActivity activity;

    public ActionFABsFlow(MastaanToolbarActivity activity){
        this.activity = activity;
    }

    public void call(final String primaryNumber, final String alternateNumber){
        if ( alternateNumber != null && alternateNumber.length() > 0 ){
            activity.showListChooserDialog(new String[]{"Call primary", "Call Alternative"}, new ListChooserCallback() {
                @Override
                public void onSelect(int position) {
                    if ( position == 0 ){
                        activity.callToPhoneNumber(primaryNumber);
                    }
                    else if ( position == 1 ){
                        activity.callToPhoneNumber(alternateNumber);
                    }
                }
            });
        }else{
            activity.callToPhoneNumber(primaryNumber);
        }
    }
}
