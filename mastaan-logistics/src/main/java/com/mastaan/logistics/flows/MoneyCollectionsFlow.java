package com.mastaan.logistics.flows;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.aleena.common.constants.SpecialCharacters;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.models.RequestUpdateMoneyCollectionTransactionDetails;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.MoneyCollectionDetails;
import com.mastaan.logistics.models.MoneyCollectionTransactionDetails;
import com.mastaan.logistics.models.UserDetails;
import com.mastaan.logistics.models.UserDetailsMini;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 22/10/16.
 */
public class MoneyCollectionsFlow {
    MastaanToolbarActivity activity;
    UserDetails userDetails;

    String moneyCollectionID;
    MoneyCollectionTransactionDetails transactionDetails;
    MoneyCollectionTransactionCallback moneyCollectionTransactionCallback;

    public interface MoneyCollectionTransactionCallback {
        void onComplete(boolean status, int statusCode, String message, MoneyCollectionTransactionDetails transactionDetails);
    }

    public MoneyCollectionsFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
        this.userDetails = new LocalStorageData(activity).getUserDetails();
    }

    /*public void markMoneyCollectionTransactionAsConsolidated(final String moneyCollectionID, final MoneyCollectionTransactionDetails transactionDetails, final MoneyCollectionTransactionCallback callback){
        this.transactionDetails = transactionDetails;
        this.moneyCollectionTransactionCallback = callback;

        if ( Math.abs(transactionDetails.getOrderDetails().getTotalCashAmount()-transactionDetails.getAmount()) > 1 ){

            String alertString = "Are you sure to mark as consolidated?<br><br>";
            alertString += "<b>"+CommonMethods.capitalizeFirstLetter(transactionDetails.getDeliveryBoyDetails().getAvailableName())+"</b>";
            if ( transactionDetails.getAmount() >= 0 ){    alertString += " collected ";   }
            else{    alertString += " returned ";   }
            alertString += "<b>"+SpecialCharacters.RS+ ""+CommonMethods.getIndianFormatNumber(Math.abs(transactionDetails.getAmount()))+"</b>";
            alertString += " instead of ";
            if ( transactionDetails.getOrderDetails().getTotalCashAmount() >= 0 ){    alertString += " collecting ";   }
            else{    alertString += " returning ";   }
            alertString += "<b>"+SpecialCharacters.RS+ ""+CommonMethods.getIndianFormatNumber(Math.abs(transactionDetails.getOrderDetails().getTotalCashAmount()))+"</b>";
            alertString += " from <b>"+CommonMethods.capitalizeFirstLetter(transactionDetails.getOrderDetails().getCustomerName())+"</b>.";

            activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml(alertString), "CANCEL", "PROCEED", new ChoiceSelectionCallback() {
                @Override
                public void onSelect(int choiceNo, String choiceName) {
                    if ( choiceName.equalsIgnoreCase("PROCEED") ){
                        markMoneyCollectionTransactionAsConsolidated();
                    }
                }
            });
        }else{
            markMoneyCollectionTransactionAsConsolidated();
        }
    }*/

    public void markMoneyCollectionTransactionAsConsolidated(final String moneyCollectionID, final MoneyCollectionTransactionDetails transactionDetails, final MoneyCollectionTransactionCallback callback){
        this.moneyCollectionID = moneyCollectionID;
        this.transactionDetails = transactionDetails;
        this.moneyCollectionTransactionCallback = callback;

        activity.showLoadingDialog("Marking as consolidated, wait...");
        activity.getBackendAPIs().getMoneyCollectionAPI().markMoneyCollectionTransactionAsConsolidated(moneyCollectionID, transactionDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    transactionDetails.setStatus(Constants.WITH_ACCOUNTS);
                    transactionDetails.setConsolidated(true);
                    transactionDetails.setConsolidatorDetails(userDetails);
                    if ( moneyCollectionTransactionCallback != null ){
                        moneyCollectionTransactionCallback.onComplete(true, 200, "Success", transactionDetails);
                    }
                }else{
                    activity.showSnackbarMessage("Unable to mark as with accounts, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            markMoneyCollectionTransactionAsConsolidated(moneyCollectionID, transactionDetails, callback);
                        }
                    });
                }
            }
        });
    }

    // UPDATE

    public void updateMoneyCollectionTransactionDetails(final String moneyCollectionID, final MoneyCollectionTransactionDetails transactionDetails, final MoneyCollectionTransactionCallback callback){
        this.moneyCollectionID = moneyCollectionID;
        this.transactionDetails = transactionDetails;
        this.moneyCollectionTransactionCallback = callback;

        View dialogUpdateMoneyCollectionTransaction = LayoutInflater.from(activity).inflate(R.layout.dialog_update_money_collection_transaction, null);
        final RadioButton cash = (RadioButton) dialogUpdateMoneyCollectionTransaction.findViewById(R.id.cash);
        final RadioButton paytm = (RadioButton) dialogUpdateMoneyCollectionTransaction.findViewById(R.id.paytm);
        final vTextInputLayout amount_collected = (vTextInputLayout) dialogUpdateMoneyCollectionTransaction.findViewById(R.id.amount_collected);
        final vTextInputLayout payment_id = (vTextInputLayout) dialogUpdateMoneyCollectionTransaction.findViewById(R.id.payment_id);

        paytm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if ( isChecked ){ payment_id.setVisibility(View.VISIBLE); }
                else{ payment_id.setVisibility(View.GONE); }
            }
        });

        amount_collected.setText(CommonMethods.getInDecimalFormat(transactionDetails.getAmount()));
        payment_id.setText(transactionDetails.getPaymentID());

        if ( transactionDetails.getPaymentType() == 1 ){ cash.setChecked(true); }
        else if ( transactionDetails.getPaymentType() == 9 ){ paytm.setChecked(true);}

        dialogUpdateMoneyCollectionTransaction.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.closeCustomDialog();
            }
        });

        dialogUpdateMoneyCollectionTransaction.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String eAmount = amount_collected.getText().toString();
                amount_collected.checkError("* Enter amount");
                final String ePaymentID = payment_id.getText().toString();
                payment_id.checkError("* Enter payment id");
                if ( eAmount.length() == 0 ){
                    return;
                }
                if ( paytm.isChecked() && ePaymentID.length() == 0 ){
                    return;
                }

                //----
                activity.closeCustomDialog();
                if ( cash.isChecked() ){
                    updateMoneyCollectionTransactionDetails(new RequestUpdateMoneyCollectionTransactionDetails(Double.parseDouble(eAmount), 1, null));
                }else if ( paytm.isChecked() ){
                    updateMoneyCollectionTransactionDetails(new RequestUpdateMoneyCollectionTransactionDetails(Double.parseDouble(eAmount), 9, ePaymentID));
                }
            }
        });
        activity.showCustomDialog(dialogUpdateMoneyCollectionTransaction);
    }

    private void updateMoneyCollectionTransactionDetails(final RequestUpdateMoneyCollectionTransactionDetails requestUpdateMoneyCollectionTransactionDetails){

        activity.showLoadingDialog("Updating transaction details, wait...");
        activity.getBackendAPIs().getMoneyCollectionAPI().updateMoneyCollectionTransactionDetails(moneyCollectionID, transactionDetails.getID(), requestUpdateMoneyCollectionTransactionDetails, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    transactionDetails.getUpdatedBy(new UserDetailsMini(userDetails.getID(), userDetails.getAvailableName()));
                    transactionDetails.setAmount(requestUpdateMoneyCollectionTransactionDetails.getAmount());
                    transactionDetails.setPaymentType(requestUpdateMoneyCollectionTransactionDetails.getPaymentType());
                    transactionDetails.setPaymetnID(requestUpdateMoneyCollectionTransactionDetails.getPaymentID());
                    if ( moneyCollectionTransactionCallback != null ){
                        moneyCollectionTransactionCallback.onComplete(true, 200, "Success", transactionDetails);
                    }
                }else{
                    activity.showChoiceSelectionDialog("Failure!", "Unable to update transaction details.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("RETRY")){
                                updateMoneyCollectionTransactionDetails(requestUpdateMoneyCollectionTransactionDetails);
                            }
                        }
                    });
                }
            }
        });
    }

    // MARK ALL

    public void markAllMoneyCollectionsAsWithAccounts(final List<MoneyCollectionDetails> moneyCollectionsList, final StatusCallback callback){

        String alertString = "Are you sure to mark the following <b>"+moneyCollectionsList.size()+"</b> items as consolidated?<br>";
        final List<String> pendingIDs = new ArrayList<>();
        for (int i=0;i<moneyCollectionsList.size();i++){
            MoneyCollectionDetails moneyCollectionDetails = moneyCollectionsList.get(i);
            if ( moneyCollectionDetails.isConsolidated() == false ){//transactionDetails.getStatus().equalsIgnoreCase(Constants.WITH_DELIVERBOY)){
                pendingIDs.add(moneyCollectionDetails.getID());
                alertString += "<br><b>"+SpecialCharacters.DOT+" </b>"+CommonMethods.capitalizeFirstLetter(moneyCollectionDetails.getOrderDetails().getCustomerName())+": <b>"+CommonMethods.getIndianFormatNumber(moneyCollectionDetails.getCollectedAmount())+" / "+CommonMethods.getIndianFormatNumber(moneyCollectionDetails.getOrderDetails().getTotalAmount())+"</b>";
            }
        }

        if ( pendingIDs.size() > 0 ) {
            activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml(alertString), "CANCEL", "PROCEED", new ChoiceSelectionCallback() {
                @Override
                public void onSelect(int choiceNo, String choiceName) {
                    if (choiceName.equalsIgnoreCase("PROCEED")) {
                        markAllMoneyCollectionsAsConsolidated(pendingIDs, callback);
                    }
                }
            });
        }else{
            activity.onReloadPressed();
        }

    }

    private void markAllMoneyCollectionsAsConsolidated(final List<String> moneyCollectionsIDs, final StatusCallback callback){
        activity.showLoadingDialog("Marking all as with accounts, wait...");
        activity.getBackendAPIs().getMoneyCollectionAPI().markAllMoneyCollectionsAsConsolidated(moneyCollectionsIDs, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    if ( callback != null ){
                        callback.onComplete(true, 200, "Success");
                    }
                }else{
                    activity.showSnackbarMessage("Unable to mark all as with accounts, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            markAllMoneyCollectionsAsConsolidated(moneyCollectionsIDs, callback);
                        }
                    });
                }
            }
        });
    }


}
