package com.mastaan.logistics.flows;

import android.location.Location;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vAppCompatEditText;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.backend.CategoriesAPI;
import com.mastaan.logistics.backend.OrdersAPI;
import com.mastaan.logistics.backend.models.RequestCreateNewOrderForOrderItem;
import com.mastaan.logistics.backend.models.RequestFailOrderItem;
import com.mastaan.logistics.backend.models.TempOrder;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.fragments.MastaanFragment;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.CustomPlace;
import com.mastaan.logistics.models.DeliverySlotDetails;
import com.mastaan.logistics.models.FailureReasonDetails;
import com.mastaan.logistics.models.OrderItemDetails;
import com.mastaan.logistics.models.UserDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 27/4/16.
 */
public class OrderItemActionsFlow {

    MastaanToolbarActivity activity;
    OrderItemDetails orderItemDetails;
    Location currentLocation;
    StatusCallback callback;

    String serverTime;

    public interface ChangeOrderItemDateCallback{
        void onComplete(boolean status, OrderItemDetails orderItemDetails);//String deliveryDate, OrderDetails2 orderDetails2);
    }

    public OrderItemActionsFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
        this.serverTime = new LocalStorageData(activity).getServerTime();
    }


    public void replaceItem(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        View replaceItemDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_replace_item, null);
        final vAppCompatEditText replace_instructions = (vAppCompatEditText) replaceItemDialogView.findViewById(R.id.replace_instructions);

        replaceItemDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();
            }
        });

        replaceItemDialogView.findViewById(R.id.replace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();

                String replaceInstrctions = replace_instructions.getText().toString();
                if (replaceInstrctions.length() > 0) {
                    completeReplaceItem(replaceInstrctions);
                } else {
                    activity.showToastMessage("* Please enter replace instructions.");
                }
            }
        });
        activity.showCustomDialog(replaceItemDialogView);
    }

    private void completeReplaceItem(final String replaceInstructions){

        activity.showLoadingDialog("Sending item replacement instructions, wait...");
        activity.getBackendAPIs().getOrdersAPI().replaceOrderItem(orderItemDetails.getID(), replaceInstructions, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Item replace instructions sent successfully.");
                    callback.onComplete(true, 200, "Success");
                    /*deleteFromItemsList(orderItemsAdapter.getItem(position).getID());*/
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while sending item replace instructions.\nPlease try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("YES")) {
                                completeReplaceItem(replaceInstructions);
                            }
                        }
                    });
                }
            }
        });
    }

    public void updateQuantity(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        View updateQuantiyDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_update_quantity, null);
        TextView title = updateQuantiyDialogView.findViewById(R.id.title);
        TextView quantity_unit = updateQuantiyDialogView.findViewById(R.id.quantity_unit);
        final EditText new_quantity = updateQuantiyDialogView.findViewById(R.id.new_quantity);

        title.setText("Update quantity");
        new_quantity.setText(CommonMethods.getInDecimalFormat(orderItemDetails.getQuantity())+"");
        quantity_unit.setText(orderItemDetails.getQuantityUnit()+"(s)");

        updateQuantiyDialogView.findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String newQuantity = new_quantity.getText().toString();

                if ( newQuantity.length() > 0 ){
                    if ( orderItemDetails.getQuantity() != Double.parseDouble(newQuantity) ){

                        activity.closeCustomDialog();
                        completeUpdateQuantity(Double.parseDouble(newQuantity));
                    }else{
                        activity.showToastMessage("* New quantity and current quantity are same");
                    }
                }else{
                    activity.showToastMessage("* Please enter new quantity");
                }
            }
        });

        updateQuantiyDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.closeCustomDialog();
            }
        });

        activity.showCustomDialog(updateQuantiyDialogView);
    }

    private void completeUpdateQuantity(final double newQuantity){

        activity.showLoadingDialog("Updating quantity, wait...");
        activity.getBackendAPIs().getOrdersAPI().updateOrderItemQuantity(orderItemDetails.getID(), newQuantity, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Quantity updated successfully.");
                    callback.onComplete(true, 200, newQuantity + "");
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while updating quantity.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                completeUpdateQuantity(newQuantity);
                            }
                        }
                    });
                }
            }
        });
    }

    public void changeDeliveryDate(final OrderItemDetails orderItemDetails, final ChangeOrderItemDateCallback callback) {
        this.orderItemDetails = orderItemDetails;

        activity.showDatePickerDialog("Select Date", DateMethods.getCurrentDate(), null, DateMethods.getOnlyDate(DateMethods.getReadableDateFromUTC(orderItemDetails.getDeliveryDate())), new DatePickerCallback() {
            @Override
            public void onSelect(String fullDate, int day, int month, int year) {
                getBuyerOrdersForDate(day, month, year, callback);
            }
        });
    }

    private void getBuyerOrdersForDate(final int day, final int month, final int year, final ChangeOrderItemDateCallback callback){
        /*final String selectedDate = day + "/" + month + "/" + year;

        activity.showLoadingDialog("Loading buyer orders list on "+DateMethods.getDateInFormat(day+"/"+month+"/"+year, DateConstants.MMM_DD_YYYY)+", wait...");
        activity.getBackendAPIs().getOrdersAPI().getBuyerOrdersForDate(orderItemDetails.getOrderDetails().getBuyerDetails().getID(), day, month, year, new OrdersAPI.BuyerOrdersListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<TempOrder> ordersList) {
                activity.closeLoadingDialog();
                if (status) {
                    for (int i = 0; i < ordersList.size(); i++) {
                        if (ordersList.get(i).getID().equals(orderItemDetails.getOrderID())) {
                            ordersList.remove(i);
                            break;
                        }
                    }
                    if (ordersList.size() > 0) {
                        List<UserDetails> orders = new ArrayList<UserDetails>();
                        for (int i = 0; i < ordersList.size(); i++) {
                            orders.add(new UserDetails(ordersList.get(i).getID(), DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(ordersList.get(i).getDeliveryDate()), DateConstants.HH_MM_A), DateMethods.getDateInFormat(DateMethods.getReadableDateFromUTC(ordersList.get(i).getDeliveryDate()), DateConstants.DD_MM_YYYY_HH_MM_A)));
                        }
                        orders.add(new UserDetails("id", "Create new order", ""));

                        activity.showListChooserDialog("Select order on " + DateMethods.getDateInFormat(selectedDate, DateConstants.MMM_DD_YYYY), orders, new MastaanFragment.PlaceChooserCallback() {
                            @Override
                            public void onSelect(CustomPlace place) {
                                if (place.getName().equalsIgnoreCase("Create new order")) {
//                                    String minimumTime = DateMethods.getTimeIn12HrFormat(serverTime);
//                                    if (DateMethods.compareDates(DateMethods.getOnlyDate(selectedDate), DateMethods.getOnlyDate(serverTime)) != 0) {
//                                        minimumTime = null;
//                                    }
//                                    activity.showTimePickerDialog("Select time on " + DateMethods.getDateInFormat(day + "/" + month + "/" + year, DateConstants.MMM_DD_YYYY), minimumTime, null, new TimePickerCallback() {
//                                        @Override
//                                        public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
//                                            createNewOrderForOrderItem(selectedDate, fullTimein12HrFormat, callback);
//                                        }
//                                    });
                                    getSlotsForDateAndChangeDeliveryDate(day + "/" + month + "/" + year, callback);
                                } else {
                                    activity.showToastMessage("Temporarily merging with other order is disabled");
                                    //mergeOrderItemToOrder(place.getID(), place.getAmount(), callback);
                                }
                            }

                            @Override
                            public void onCancel() {
                            }
                        });

                    } else {*/
//                        String minimumTime = DateMethods.getTimeIn12HrFormat(serverTime);
//                        if (DateMethods.compareDates(DateMethods.getOnlyDate(selectedDate), DateMethods.getOnlyDate(serverTime)) != 0) {
//                            minimumTime = null;
//                        }
//                        activity.showTimePickerDialog("Select time on " + DateMethods.getDateInFormat(day + "/" + month + "/" + year, DateConstants.MMM_DD_YYYY), minimumTime, null, new TimePickerCallback() {
//                            @Override
//                            public void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds) {
//                                createNewOrderForOrderItem(day + "/" + month + "/" + year, fullTimein12HrFormat, callback);
//                            }
//                        });
                        getSlotsForDateAndChangeDeliveryDate(day + "/" + month + "/" + year, callback);
                    /*}
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while getting buyer orders list on " + DateMethods.getDateInFormat(day + "/" + month + "/" + year, DateConstants.MMM_DD_YYYY) + ".\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                getBuyerOrdersForDate(day, month, year, callback);
                            }
                        }
                    });
                }
            }
        });*/
    }

    private void getSlotsForDateAndChangeDeliveryDate(final String date, final ChangeOrderItemDateCallback callback){

        activity.showLoadingDialog("Loading slots for " + date);
        activity.getBackendAPIs().getCategoriesAPI().getSlotsForCategoryForDate(orderItemDetails.getMeatItemDetails().getCategoryDetails().getID(), date, new CategoriesAPI.SlotsForDateCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String loadedDate, final List<DeliverySlotDetails> slotsList) {
                if (DateMethods.compareDates(date, loadedDate) == 0) {
                    activity.closeLoadingDialog();
                    if (status) {
                        final List<String> slotStrings = new ArrayList<>();
                        for (int i = 0; i < slotsList.size(); i++) {
                            slotStrings.add(slotsList.get(i).getDisplayText() + " (" + slotsList.get(i).getOrdersCount() + "/" + slotsList.get(i).getMaximumOrders() + ")");
                        }

                        activity.showListChooserDialog("Select slot on " + DateMethods.getDateInFormat(date, DateConstants.MMM_DD_YYYY), slotStrings, new ListChooserCallback() {
                            @Override
                            public void onSelect(int position) {
                                //String deliveryDateAndTime = DateMethods.getDateInFormat(date, DateConstants.DD_MM_YYYY) + ", " + DateMethods.getTimeIn12HrFormat(slotsList.get(position).getEndTime());
                                //String slotID = slotsList.get(position).getID();
                                createNewOrderForOrderItem(date, slotsList.get(position), callback);
                            }
                        });

                    } else {
                        activity.showSnackbarMessage("Error in loading slots, try again!", "RETRY", new ActionCallback() {
                            @Override
                            public void onAction() {
                                getSlotsForDateAndChangeDeliveryDate(date, callback);
                            }
                        });
                    }
                }
            }
        });
    }

    private void mergeOrderItemToOrder(final String mergeOrderID, final String mergeOrderDate, final ChangeOrderItemDateCallback callback){

        /*activity.showLoadingDialog("Merging to order, wait...");
        activity.getBackendAPIs().getOrdersAPI().mergeOrderItemToOrder(orderItemDetails.getID(), mergeOrderID, new OrdersAPI.MergeOrderItemToOrderCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, String deliveryDate, OrderDetails2 orderDetails2) {
                activity.closeLoadingDialog();
                if (status) {
                    if (callback != null) {
                        callback.onComplete(true, deliveryDate, orderDetails2);
                    }
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while merging order.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                mergeOrderItemToOrder(mergeOrderID, mergeOrderDate, callback);
                            }
                        }
                    });
                }
            }
        });*/
    }

    private void createNewOrderForOrderItem(final String date, final DeliverySlotDetails slotDetails, final ChangeOrderItemDateCallback callback){

        activity.showLoadingDialog("Changing delivery date, wait...");
        activity.getBackendAPIs().getOrdersAPI().createNewOrderForOrderItem(orderItemDetails.getID(), new RequestCreateNewOrderForOrderItem(date, slotDetails.getEndTime(), slotDetails.getID()), new OrdersAPI.CreateNewOrderForOrderItem() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails modifiedOrderItemDetails){//String deliveryDate, OrderDetails2 orderDetails2) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Delivery date changed successfully");
                    if (callback != null) {
                        callback.onComplete(true, modifiedOrderItemDetails);//deliveryDate, orderDetails2);//date+", "+time);
                    }
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while changing delivery date.\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                createNewOrderForOrderItem(date, slotDetails, callback);

                            }
                        }
                    });
                }
            }
        });
    }


    public void deleteItem(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showChoiceSelectionDialog("Alert!", Html.fromHtml("Are you sure to delete <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getName())+"</b> of <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equalsIgnoreCase("YES")) {
                    activity.showLoadingDialog("Deleting item, wait...");
                    activity.getBackendAPIs().getOrdersAPI().deleteOrderItem(orderItemDetails.getID(), new StatusCallback() {
                        @Override
                        public void onComplete(boolean status, int statusCode, String message) {
                            activity.closeLoadingDialog();
                            if (status) {
                                activity.showToastMessage("Item deleted successfully.");
                                callback.onComplete(true, 200, "Success");
                                /*activity.onReloadPressed();*/
                            } else {
                                activity.showToastMessage("Something went wrong while deleting item, try again!");
                            }
                        }
                    });
                }
            }
        });
    }

    public void failItem(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showChoiceSelectionDialog("Confirm", Html.fromHtml("Are you sure to fail <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize())+"</b> of <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName())
                +"?</b>"+(orderItemDetails.getTotalOrderDiscount()>0?"<br><br><b>Note:</b> Coupon/Membership discount will be adjusted/removed based on updated order.":""))
                , "NO", "YES", new ChoiceSelectionCallback() {

            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if (choiceName.equalsIgnoreCase("YES")) {
                    List<CustomPlace> failureReasons = new ArrayList<>();
                    List<String> fReasons = new LocalStorageData(activity).getFailureReasons();
                    for (int i = 0; i < fReasons.size(); i++) {
                        failureReasons.add(new FailureReasonDetails(fReasons.get(i)));
                    }
                    failureReasons.add(new FailureReasonDetails("Other"));

                    activity.showListChooserDialog("Select reason", failureReasons, new MastaanFragment.PlaceChooserCallback() {
                        @Override
                        public void onSelect(CustomPlace place) {
                            if (place.getName().equalsIgnoreCase("Other")) {

                                View failReasonDialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_fail_reason, null);
                                final EditText reason = (EditText) failReasonDialogView.findViewById(R.id.reason);

                                failReasonDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String failureReason = reason.getText().toString();
                                        if (failureReason.length() > 0) {
                                            activity.closeCustomDialog();
                                            checkForCancelOrderItemAndProceedToMarkAsFailed(failureReason);
                                        } else {
                                            activity.showToastMessage("* Enter failure reason");
                                        }
                                    }
                                });
                                failReasonDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        activity.closeCustomDialog();
                                    }
                                });
                                activity.showCustomDialog(failReasonDialogView);
                            } else {
                                checkForCancelOrderItemAndProceedToMarkAsFailed(place.getName());
                            }
                        }

                        @Override
                        public void onCancel() {
                        }
                    });
                }
            }
        });
    }

    private void checkForCancelOrderItemAndProceedToMarkAsFailed(final String reason){

        activity.showLoadingDialog("Cancelling order, wait...");
        activity.getBackendAPIs().getOrdersAPI().checkForCancelOrderItem(orderItemDetails.getID(), orderItemDetails.getOrderDetails().getBuyerID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if ( status ){
                    continueFailingItem(false, reason);
                }
                else{
                    activity.closeLoadingDialog();
                    if ( statusCode == 500 ){
                        activity.showChoiceSelectionDialog(false, "Confirm", message, "NO", "YES", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("YES") ){
                                    continueFailingItem(true, reason);
                                }
                            }
                        });
                    }else{
                        activity.showChoiceSelectionDialog(false, "Failure", "Something went wrong while canceling order item. Please try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                            @Override
                            public void onSelect(int choiceNo, String choiceName) {
                                if ( choiceName.equalsIgnoreCase("RETRY") ){
                                    checkForCancelOrderItemAndProceedToMarkAsFailed(reason);
                                }
                            }
                        });
                    }
                }
            }

            //-----

            private void continueFailingItem(final boolean showDialog, final String reason){
                if ( orderItemDetails.getStatusString().equalsIgnoreCase(Constants.ORDERED) || orderItemDetails.getStatusString().equalsIgnoreCase(Constants.ACKNOWLEDGED) ){
                    markAsFailed(showDialog, reason, true);
                }
                else {
                    activity.showChoiceSelectionDialog("Confirm", CommonMethods.fromHtml("Do you want to <b>reset stock</b> after cancelling item?"), "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES") ){
                                markAsFailed(showDialog, reason, true);
                            }else{
                                markAsFailed(showDialog, reason, false);
                            }
                        }
                    });
                }
            }
        });
    }

    private void markAsFailed(final boolean showLoadingDialog, final String reason, final boolean resetStock) {
        if ( showLoadingDialog ){   activity.showLoadingDialog("Marking as failed, wait...");   }
        activity.getBackendAPIs().getOrdersAPI().failOrderItem(orderItemDetails.getID(), new RequestFailOrderItem(reason, resetStock), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Item marked as failed successfully.");
                    callback.onComplete(true, 200, reason);
                    //activity.onReloadPressed();
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while marking item as failed.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                markAsFailed(showLoadingDialog, reason, resetStock);
                            }
                        }
                    });
                }
            }
        });
    }

    public interface ReProcessOrderItemCallback {
        void onComplete(boolean status, int statusCode, String message, OrderItemDetails orderItemDetails);
    }

    public void reProcessOrderItem(final OrderItemDetails orderItemDetails, final ReProcessOrderItemCallback callback) {
        this.orderItemDetails = orderItemDetails;

        activity.showChoiceSelectionDialog("Reprocess?", Html.fromHtml("Are you sure to reprocess the <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getName())+"</b> of <b>"+CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName())+"?</b>"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if ( choiceName.equalsIgnoreCase("YES")){
                    reProcessOrderItem(callback);
                }
            }
        });
    }

    private void reProcessOrderItem(final ReProcessOrderItemCallback callback) {

        activity.showLoadingDialog("Reprocessing order item, wait...");
        activity.getBackendAPIs().getOrdersAPI().reProcessOrderItem(orderItemDetails.getID(), new OrdersAPI.ReProcessOrderItemCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, OrderItemDetails new_order_item_details) {
                activity.closeLoadingDialog();
                if (status) {
                    activity.showToastMessage("Item reprocessed.New order created successfully.");
                    callback.onComplete(true, 200, "Success", new_order_item_details);
                    //activity.onReloadPressed();
                } else {
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while reprocessing item.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                reProcessOrderItem(orderItemDetails, callback);
                            }
                        }
                    });
                }
            }
        });
    }


    public void markCounterSaleAsDelivered(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;
        this.callback = callback;

        activity.showLoadingDialog("Completing counter sale order, wait...");
        activity.getBackendAPIs().getOrdersAPI().markCounterSaleAsDelivered(orderItemDetails.getOrderID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    activity.showToastMessage("Counter sale order completed successfully.");
                    callback.onComplete(true, 200, "Success");
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while completing counter sale order.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                markCounterSaleAsDelivered(orderItemDetails, callback);
                            }
                        }
                    });
                }
            }
        });
    }


    //----

    public void addDelayedDeliveryComment(final OrderItemDetails orderItemDetails, final StatusCallback callback){
        this.orderItemDetails = orderItemDetails;

        activity.showInputTextDialog("Delayed delivery comment", "Comment", orderItemDetails.getDelayedDeliveryComment(), new TextInputCallback() {
            @Override
            public void onComplete(String inputText) {
                addDelayedDeliveryComment(inputText, callback);
            }
        });
    }

    private void addDelayedDeliveryComment(final String comment, final StatusCallback callback){

        activity.showLoadingDialog("Adding delayed delivery comment, wait...");
        activity.getBackendAPIs().getOrdersAPI().addDelayedDeliveryComment(orderItemDetails.getID(), comment, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    callback.onComplete(true, 200, comment);
                }else{
                    activity.showChoiceSelectionDialog("Failure!", "Something went wrong while adding delayed delivery comment.\nPelase try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES") ){
                                addDelayedDeliveryComment(comment, callback);
                            }
                        }
                    });
                }
            }
        });
    }


}