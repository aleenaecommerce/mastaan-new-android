package com.mastaan.logistics.flows;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;

import com.aleena.common.adapters.ListArrayAdapter;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.widgets.vSpinner;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.gson.Gson;
import com.mastaan.logistics.R;
import com.mastaan.logistics.activities.MastaanToolbarActivity;
import com.mastaan.logistics.constants.Constants;
import com.mastaan.logistics.interfaces.OrderItemsListCallback;
import com.mastaan.logistics.localdata.LocalStorageData;
import com.mastaan.logistics.models.OrderItemDetails;

import java.util.List;

/**
 * Created by venkatesh on 27/4/16.
 */
public class CustomerSupportFlow {

    MastaanToolbarActivity activity;
    OrderItemDetails orderItemDetails;

    public CustomerSupportFlow(MastaanToolbarActivity activity) {
        this.activity = activity;
    }

    public void assignToCustomerService(final Context context, final OrderItemDetails orderItemDetails, final StatusCallback callback){
        this.orderItemDetails = orderItemDetails;

        View assignToCustomerSupportDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_assign_to_customer_service, null);
        final vSpinner reasonsSpinner = (vSpinner) assignToCustomerSupportDialogView.findViewById(R.id.reasonsSpinner);
        final vTextInputLayout comments = (vTextInputLayout) assignToCustomerSupportDialogView.findViewById(R.id.comments);

        final List<String> customerSupportReasons = new LocalStorageData(context).getCustomerSupportReasons();
        if ( customerSupportReasons.size() > 0 ){
            customerSupportReasons.add(0, Constants.SELECT);
        }else{
            customerSupportReasons.add(Constants.SELECT);
        }
        customerSupportReasons.add(Constants.OTHER);

        ListArrayAdapter adapter = new ListArrayAdapter(activity, R.layout.view_spinner, customerSupportReasons);//new ArrayAdapter<String> (this, android.R.layout.simple_expandable_list_item_1, new ArrayList<String>());//new AttributeOptionsAdapter(context, options, weightQuantityUnit);//
        adapter.setPopupLayoutView(R.layout.view_spinner_popup);
        reasonsSpinner.setAdapter(adapter);
        reasonsSpinner.setSelection(0);

        assignToCustomerSupportDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason = customerSupportReasons.get(reasonsSpinner.getSelectedItemPosition());
                if ( reason.equalsIgnoreCase(Constants.SELECT) == false) {
                    activity.closeCustomDialog();
                    if ( comments.getText().toString().length() > 0 ){
                        assignToCustomerService(reason+": "+comments.getText().toString(), callback);
                    }else{
                        assignToCustomerService(reason, callback);
                    }
                }else{
                    activity.showToastMessage("* Select reason");
                }
            }
        });
        activity.showCustomDialog(assignToCustomerSupportDialogView);

    }

    private void assignToCustomerService(final String reason, final StatusCallback callback){

        activity.showLoadingDialog("Assigning to customer support, wait...");
        activity.getBackendAPIs().getCustomerSupportAPI().assignToCustomerSupport(orderItemDetails.getID(), reason, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    callback.onComplete(true, 200, reason);

                    Intent fireBaseReceiver = new Intent(Constants.FIREBASE_RECEIVER)
                            .putExtra("type", Constants.ADD_CS_LISTENER_FOR_ORDER_ITEM)
                            .putExtra(Constants.ORDER_ITEM_DETAILS, new Gson().toJson(orderItemDetails));
                    activity.sendBroadcast(fireBaseReceiver);
                }else{
                    activity.showChoiceSelectionDialog(false, "Failure!", "Something went wrong while assigning to customer support.\n\nPlease try again!", "CANCEL", "RETRY", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if (choiceName.equalsIgnoreCase("RETRY")) {
                                assignToCustomerService(reason, callback);
                            }
                        }
                    });
                }
            }
        });
    }

    //------

    public void claimCustomerSupportItem(final OrderItemDetails orderItemDetails, final OrderItemsListCallback callback) {
        this.orderItemDetails = orderItemDetails;

        activity.showLoadingDialog("Claiming for customer support, wait...");
        activity.getBackendAPIs().getCustomerSupportAPI().claimCustomerSupportItem(orderItemDetails.getID(), new OrderItemsListCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message, List<OrderItemDetails> orderItemsList) {
                if ( status ){
                    callback.onComplete(true, 200, "Success", orderItemsList);
                }else{
                    activity.closeLoadingDialog();
                    activity.showSnackbarMessage("Something went wrong while claiming, try again!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            claimCustomerSupportItem(orderItemDetails, callback);
                        }
                    });
                }
            }
        });
    }

    //-------

    public void addCommentForCustomerSupport(final OrderItemDetails orderItemDetails, final StatusCallback callback){
        this.orderItemDetails = orderItemDetails;

        activity.showInputTextDialog("Add comment", "Comment", orderItemDetails.getCustomerSupportComment(), new TextInputCallback() {
            @Override
            public void onComplete(String inputText) {
                addCommentForCustomerSupport(inputText, callback);
            }
        });
    }

    private void addCommentForCustomerSupport(final String comment, final StatusCallback callback){

        activity.showLoadingDialog("Adding comment, wait...");
        activity.getBackendAPIs().getCustomerSupportAPI().addCommentForCustomerSupport(orderItemDetails.getID(), comment, new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    callback.onComplete(true, 200, comment);
                }else{
                    activity.showChoiceSelectionDialog("Failure!", "Something went wrong while adding comment.\nPelase try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES") ){
                                addCommentForCustomerSupport(comment, callback);
                            }
                        }
                    });
                }
            }
        });
    }

    //------------

    public void removeFromCustomerSupport(final OrderItemDetails orderItemDetails, final StatusCallback callback) {
        this.orderItemDetails = orderItemDetails;

        activity.showChoiceSelectionDialog("Confirm!", Html.fromHtml("Do you want to remove <b>" + CommonMethods.capitalizeFirstLetter(orderItemDetails.getOrderDetails().getCustomerName()) + "'s " + CommonMethods.capitalizeFirstLetter(orderItemDetails.getMeatItemDetails().getNameWithCategoryAndSize()) + "</b> from customer support?"), "NO", "YES", new ChoiceSelectionCallback() {
            @Override
            public void onSelect(int choiceNo, String choiceName) {
                if ( choiceName.equalsIgnoreCase("YES")){
                    removeFromCustomerSupport(callback);
                }
            }
        });
    }

    private void removeFromCustomerSupport(final StatusCallback callback){

        activity.showLoadingDialog("Removing from customer support, wait...");
        activity.getBackendAPIs().getCustomerSupportAPI().removeFromCustomerSupport(orderItemDetails.getID(), new StatusCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                activity.closeLoadingDialog();
                if ( status ){
                    callback.onComplete(true, 200, "Success");

                    Intent firebaseReceiver = new Intent(Constants.FIREBASE_RECEIVER)
                            .putExtra("type", Constants.REMOVE_CS_LISTENER_FOR_ORDER_ITEM)
                            .putExtra(Constants.ORDER_ITEM_ID, orderItemDetails.getID());
                    activity.sendBroadcast(firebaseReceiver);
                }else{
                    activity.showChoiceSelectionDialog("Failure!", "Something went wrong while removing from customer support.\nPelase try again!", "NO", "YES", new ChoiceSelectionCallback() {
                        @Override
                        public void onSelect(int choiceNo, String choiceName) {
                            if ( choiceName.equalsIgnoreCase("YES") ){
                                removeFromCustomerSupport(orderItemDetails, callback);
                            }
                        }
                    });
                }
            }
        });
    }

}