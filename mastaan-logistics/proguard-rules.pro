# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and orderItemDetails by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# =========================

-keepattributes Signature
-keepattributes Exceptions

-keepattributes SourceFile,LineNumberTable
-renamesourcefileattribute SourceFile

-keepattributes *Annotation*

-keepattributes SourceFile,LineNumberTable
-keep class * extends android.app.Application
#-keep class * implements android.os.Parcelable { public static fipernal android.os.Parcelable$Creator *; }

# ======= COMMONLIB =======

-keepclassmembers class com.aleena.common.models.** { *; }
-keepclassmembers class com.aleena.common.location.model.** { *; }
#-keep class com.aleena.common.** { *; }

# === MASTAAN LOGISTICS ====

-keepclassmembers class com.mastaan.logistics.models.** { *; }
-keepclassmembers class com.mastaan.logistics.backend.models.** { *; }
#-keep class com.mastaan.mastaan.** { *; }

# ======= APPCOMPAT =======

-keep class android.support.v7.widget.SearchView { *; }

# ======== RAZORPAY =======

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

-keepattributes JavascriptInterface
-keepattributes *Annotation*

-dontwarn com.razorpay.**
-keep class com.razorpay.** {*;}

-optimizations !method/inlining/*

-keepclasseswithmembers class * {
  public void onPayment*(...);
}

# ===== RETROFIT & PICASSO ======

-keep class retrofit.** { *; }
-keep class retrofit2.** { *; }
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn retrofit.**
-dontwarn retrofit2.**
-dontwarn okio.**
-dontwarn com.squareup.okhttp.**

# ======= EPSON PRINTER ======

-keep class com.epson.** { *; }
-dontwarn com.epson.**

# ======= BROTHER PRINTER ======

-keep class com.brother.** { *; }
-dontwarn com.brother.**

# ========= HYPERTRACK =======

-keep class com.hypertrack.lib.** { *; }
-keep class io.hypertrack.sendeta.model.** { *; }
-keep class io.hypertrack.sendeta.network.** { *; }
-keep class io.hypertrack.sendeta.store.** { *; }
-keep class maps.** { *; }
-keep public class com.google.** {*;}
-keep class com.crashlytics.** { *; }
-keep class com.crashlytics.android.**

-dontwarn com.hypertrack.lib.**

# ========= JACKSON LIB =======

-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}
-keepnames class com.fasterxml.jackson.** { *; }
-dontwarn com.fasterxml.jackson.databind.**

# =====  FIREBASE DATABASE ====

-keep public class com.google.android.gms.* { public *; } # Better
-dontwarn com.google.android.gms.**
#-keep class com.google.android.gms.** { *; }
#-dontwarn com.google.android.gms.**

-keep class com.firebase.** { *; }
-dontwarn com.firebase.**
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }
-dontwarn org.w3c.dom.**
-dontwarn org.joda.time.**
-dontwarn org.shaded.apache.**
-dontwarn org.ietf.jgss.**
-dontnote com.firebase.client.core.GaePlatform
