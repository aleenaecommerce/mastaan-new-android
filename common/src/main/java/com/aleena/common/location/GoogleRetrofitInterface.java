package com.aleena.common.location;

import com.aleena.common.location.model.DirectionsData;
import com.aleena.common.location.model.PlacePredictionsData;
import com.aleena.common.location.model.PlaceSearchDataByLatLng;
import com.aleena.common.location.model.PlaceSearchDataByPlaceID;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by venkatesh on 11/6/15.
 */

public interface GoogleRetrofitInterface {

    @GET("/geocode/json")
    void getPlaceDetailsByLatLng(@Query("latlng") String latlng, @Query("key") String key, Callback<PlaceSearchDataByLatLng> callback);

    @GET("/geocode/json")
    PlaceSearchDataByLatLng getPlaceDetailsByLatLng(@Query("latlng") String latlng, @Query("key") String key);

    @GET("/place/details/json")
    void getPlaceDetailsByPlaceID(@Query("placeid") String placeid, @Query("key") String key, Callback<PlaceSearchDataByPlaceID> callback);

    @GET("/place/autocomplete/json")
    void getPlacePredictions(@Query("input") String queryName, @Query("location") String nearLocationLatLng, @Query("radius") int inRadius, @Query("key") String key, Callback<PlacePredictionsData> callback);

    @GET("/directions/json")
    void getDirections(@Query("origin") String originLatLngString, @Query("destination") String destinationLatLngString, @Query("key") String key, Callback<DirectionsData> callback);

}
