package com.aleena.common.location;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import com.aleena.common.constants.ConstantsCommonLibrary;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class FusedLocationService implements LocationListener,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener {

    Context context;
    boolean listenUpdates;
    CurrentLocationCallback callback;

    private static final long INTERVAL = 1000 * 5;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long ONE_MIN = 1000 * 60;
    private static final float MINIMUM_ACCURACY = 60.0f;

    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;

    private Location location;

    public interface CurrentLocationCallback {
        void onComplete(boolean status, Location location);
    }

    public FusedLocationService(Context context) {
        this.context = context;
        setupdFusedLocationService();
    }

    public FusedLocationService(Context context, CurrentLocationCallback callback) {
        this.context = context;
        this.callback = callback;
        setupdFusedLocationService();
    }

    public FusedLocationService(Context context, boolean listenUpdates, CurrentLocationCallback callback) {
        this.context = context;
        this.listenUpdates = listenUpdates;
        this.callback = callback;

        setupdFusedLocationService();
    }

    public void setupdFusedLocationService(){

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : OnConnect");
        fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        location = fusedLocationProviderApi.getLastLocation(googleApiClient);
        Log.d(ConstantsCommonLibrary.LOG_TAG, "\nLOCATION_SERVICE - InitLocation = "+location);
        /*if ( listenUpdates == false && callback != null) {
            if (location != null) {
                sendCallaback(true, location);
                stop();
            } else {
                getCurrentLocation(callback);
            }
        }*/
        getCurrentLocation(callback);
    }

    @Override
    public void onLocationChanged(Location location) {

        if ( location != null && location.getAccuracy() > 0 ) {
            if( this.location == null || location.getTime() - this.location.getTime() > ONE_MIN || location.getAccuracy() < MINIMUM_ACCURACY ){
                this.location = location;
                if ( listenUpdates ){
                }else{
                    sendCallaback(true, location);
                }
            }
        }
    }

    public Location getCurrentLocation() {
        //Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : CurrentLocation = "+location);
        return this.location;
    }
    public void  getCurrentLocation(final CurrentLocationCallback callback) {

        this.callback = callback;

        if ( location != null ){
            //Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : CurrentLocation , Success, Location = "+location);
            sendCallaback(true, location);
        }else{
            new CountDownTimer(12000, 1000) {
                public void onTick(long millisUntilFinished) {}
                public void onFinish() {
                    if ( location != null ){
                        //Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : CurrentLocation , Success, Location = "+location);
                        sendCallaback(true, location);
                    }else{
                        //Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : CurrentLocation , Failure");
                        sendCallaback(false, location);
                    }
                }
            }.start();
        }
    }

    private void sendCallaback(boolean status, Location locationn){
        if ( callback != null && locationn != null ){
            callback.onComplete(status, locationn);
            if ( listenUpdates == false ) {
                stop();
            }
        }
    }

    public void stop(){
        try {
            fusedLocationProviderApi.removeLocationUpdates(googleApiClient, this);
            Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : onClose");
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}
}