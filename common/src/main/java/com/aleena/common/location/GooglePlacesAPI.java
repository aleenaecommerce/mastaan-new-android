package com.aleena.common.location;

import android.os.AsyncTask;

import androidx.annotation.Nullable;

import android.util.Log;

import com.aleena.common.location.model.DirectionsData;
import com.aleena.common.location.model.PlacePredictionsData;
import com.aleena.common.location.model.PlaceSearchDataByLatLng;
import com.aleena.common.location.model.PlaceSearchDataByPlaceID;
import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by venkatesh on 11/7/15.
 */
public class GooglePlacesAPI {

    String GOOGLE_API_KEY;
    GoogleRetrofitInterface googleRetrofitInterface;

    public interface PlaceDetailsCallback {
        void onComplete(boolean status, int statusCode, LatLng location, String placeID, PlaceDetails placeDetails);
    }

    public interface PlacePredictionsCallback {
        void onComplete(boolean status, int statusCode, String queryName, List<Map<String, String>> placePredictions);
    }

    public interface DirectionsCallback {
        void onComplete(@Nullable List<DirectionsData.Route.Leg> steps, int status_code);
    }

    public GooglePlacesAPI(String GOOGLE_API_KEY, GoogleRetrofitInterface googleRetrofitInterface) {
        this.GOOGLE_API_KEY = GOOGLE_API_KEY;
        this.googleRetrofitInterface = googleRetrofitInterface;
    }

    public void getPlaceDetailsByLatLng(final LatLng location, final PlaceDetailsCallback callback) {

        try {
            new AsyncTask<Void, Void, Void>() {
                com.aleena.common.models.Response<PlaceDetails> response;

                @Override
                protected Void doInBackground(Void... voids) {


                    response = getPlaceDetailsByLatLng(location);
                    return null;


                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (callback != null) {
                        callback.onComplete(response.getStatus(), response.getCode(), location, "", response.getItem());
                    }
                }
            }.execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public com.aleena.common.models.Response<PlaceDetails> getPlaceDetailsByLatLng(final LatLng location) {
        try {


            try {
                PlaceSearchDataByLatLng placeSearchDataByLatLng = googleRetrofitInterface.getPlaceDetailsByLatLng(location.latitude + "," + location.longitude, GOOGLE_API_KEY);

                if (placeSearchDataByLatLng.getStatus().equalsIgnoreCase("OK") || placeSearchDataByLatLng.getStatus().equalsIgnoreCase("ZERO_RESULTS")) {

                    if (placeSearchDataByLatLng.getResults() != null && placeSearchDataByLatLng.getResults().length > 0) {
                        PlaceSearchDataByLatLng.PlaceDet placeDet = placeSearchDataByLatLng.getResults()[0];       // Using only the 1st one.
                        PlaceSearchDataByLatLng.PlaceDet.AddressComponentDetails[] addressComponentDetails = placeDet.getAddress_components();
                        PlaceSearchDataByLatLng.PlaceDet.Geometryy geometryy = placeDet.getGeometry();

                        PlaceDetails placeDetails = new PlaceDetails();         // Object to be Returned
                        placeDetails.setLatLng(location);
                        placeDetails.setFullName(placeDet.getFormatted_address());
                        placeDetails.setLatitude(placeDet.getGeometry().getLocation().getLat());
                        placeDetails.setLongtitude(placeDet.getGeometry().getLocation().getLng());

                        Log.e("lat from g", "" + placeDet.getGeometry().getLocation().getLat());
                        Log.e("lat from gll", "" + placeDet.getGeometry().getLocation().getLng());
                        Log.e("lat from ", "" + geometryy.getLocation().getLat());
                        Log.e("lat from ", "" + geometryy.getLocation().getLng());

                        for (int i = 0; i < addressComponentDetails.length; i++) {
                            String[] type = addressComponentDetails[i].getTypes();
                            for (int j = 0; j < type.length; j++) {
                                if (type[j].equalsIgnoreCase("premise")) {
                                    placeDetails.setPremise(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("route")) {
                                    placeDetails.setRoute(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("sublocality_level_2")) {
                                    placeDetails.setSublocalityLevel_2(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("sublocality_level_1")) {
                                    placeDetails.setSublocalityLevel_1(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("locality")) {
                                    placeDetails.setLocality(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("administrative_area_level_1")) {
                                    placeDetails.setState(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("country")) {
                                    placeDetails.setCountry(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("postal_code")) {
                                    placeDetails.setPostalCode(addressComponentDetails[i].getLong_name());
                                    break;
                                }
                            }
                        }
                        Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Success , Details=" + placeDetails.getFullAddress() + " for (" + location + ")");
                        return new com.aleena.common.models.Response<>(true, 200, "success", placeDetails);
                    } else {
                        Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Failure , Details = NO_RESULTS" + " for (" + location + ")");
                        return new com.aleena.common.models.Response<>(false, 500, "Failure", null);
                    }

                } else if (placeSearchDataByLatLng.getStatus().equalsIgnoreCase("ZERO_RESULTS")) {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Success , Details = ZERO_RESULTS" + " for (" + location + ")");
                    return new com.aleena.common.models.Response<>(true, 200, "Success", null);
                } else {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Failure = " + placeSearchDataByLatLng.getStatus() + " for (" + location + ")");
                    return new com.aleena.common.models.Response<>(false, 500, "Failure", null);
                }
            } catch (RetrofitError error) {
                try {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (" + location + ")");
                    return new com.aleena.common.models.Response<>(false, error.getResponse().getStatus(), "Error");
                } catch (Exception e) {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Error = " + error.getKind() + " for (" + location + ")");
                    return new com.aleena.common.models.Response<>(false, -1, "Error");
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return new com.aleena.common.models.Response<>(false, -1, "Error");

        }
    }

    // PlaceDetails by PlaceID...

    public void getPlaceDetailsByPlaceID(final String placeID, final PlaceDetailsCallback callback) {

        googleRetrofitInterface.getPlaceDetailsByPlaceID(placeID, GOOGLE_API_KEY, new Callback<PlaceSearchDataByPlaceID>() {
            @Override
            public void success(PlaceSearchDataByPlaceID placeSearchDataByPlaceID, Response response) {

                if (placeSearchDataByPlaceID.getStatus().equalsIgnoreCase("OK")) {
                    if (placeSearchDataByPlaceID.getResult() != null) {
                        PlaceSearchDataByPlaceID.PlaceDet placeDet = placeSearchDataByPlaceID.getResult();
                        PlaceSearchDataByPlaceID.PlaceDet.AddressComponentDetails[] addressComponentDetails = placeDet.getAddress_components();

                        PlaceDetails placeDetails = new PlaceDetails();         // Object to be Returned
                        placeDetails.setLatLng(new LatLng(placeDet.getGeometry().getLocation().getLat(), placeDet.getGeometry().getLocation().getLng()));
                        placeDetails.setFullName(placeDet.getFormatted_address());

                        for (int i = 0; i < addressComponentDetails.length; i++) {
                            String[] type = addressComponentDetails[i].getTypes();
                            for (int j = 0; j < type.length; j++) {
                                if (type[j].equalsIgnoreCase("premise")) {
                                    placeDetails.setPremise(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("route")) {
                                    placeDetails.setRoute(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("sublocality_level_2")) {
                                    placeDetails.setSublocalityLevel_2(addressComponentDetails[i].getLong_name());
                                    break;
                                }
                                if (type[j].equalsIgnoreCase("sublocality_level_1")) {
                                    placeDetails.setSublocalityLevel_1(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("locality")) {
                                    placeDetails.setLocality(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("administrative_area_level_1")) {
                                    placeDetails.setState(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("country")) {
                                    placeDetails.setCountry(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("postal_code")) {
                                    placeDetails.setPostalCode(addressComponentDetails[i].getLong_name());
                                    break;
                                }
                            }
                        }
                        Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByPlaceID : Success , Details=" + placeDetails.getId() + " for (place_id = " + placeID + ")");
                        callback.onComplete(true, 200, placeDetails.getLatLng(), placeID, placeDetails);
                    } else {
                        Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByPlaceID : Failure = NO_RESULTS ,  for (place_id = " + placeID + ")");
                        callback.onComplete(false, 500, null, placeID, null);
                    }

                } else {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByPlaceID : Failure = " + placeSearchDataByPlaceID.getStatus() + " for (place_id = " + placeID + ")");
                    callback.onComplete(false, 500, null, placeID, null);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByPlaceID : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (place_id = " + placeID + ")");
                    callback.onComplete(false, error.getResponse().getStatus(), null, placeID, null);
                } catch (Exception e) {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByPlaceID : Error = " + error.getKind() + " for (place_id = " + placeID + ")");
                    callback.onComplete(false, -1, null, placeID, null);
                }
            }
        });

    }


    // Get Place Predictions....

    public void getPlacePredictions(final String queryName, final PlacePredictionsCallback callback) {
        getPlacePredictions(queryName, null, 0, callback);
    }

    public void getPlacePredictions(final String queryName, final String biasLatLng, final int biasRadiusInMeters, final PlacePredictionsCallback callback) {
        /*Hyderabad: 17.3850051,78.4845113, Radius: 600*1000 */

        googleRetrofitInterface.getPlacePredictions(queryName, biasLatLng != null ? biasLatLng : "", biasRadiusInMeters, GOOGLE_API_KEY, new Callback<PlacePredictionsData>() {
            @Override
            public void success(PlacePredictionsData placePredictionsData, Response response) {

                if (placePredictionsData.getStatus().equalsIgnoreCase("OK") || placePredictionsData.getStatus().equalsIgnoreCase("ZERO_RESULTS")) {

                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlacePredictions : Success , Found = " + placePredictionsData.getPredictions().length + " for (place_query = " + queryName + ")");

                    List<Map<String, String>> placePredictions = new ArrayList<Map<String, String>>();

                    PlacePredictionsData.PlaceDet[] placeDet = placePredictionsData.getPredictions();
                    for (int i = 0; i < placeDet.length; i++) {
                        PlacePredictionsData.PlaceDet placeDetails = placeDet[i];

                        Map<String, String> placeData = new HashMap<String, String>();
                        placeData.put("description", placeDetails.getDescription());
                        placeData.put("place_id", placeDetails.getPlace_id());
                        placePredictions.add(placeData);
                    }

                    callback.onComplete(true, 200, queryName, placePredictions);

                } else {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlacePredictions : Failure = " + placePredictionsData.getStatus() + " for (place_query = " + queryName + ")");
                    callback.onComplete(false, 500, queryName, null);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlacePredictions : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (place_query = " + queryName + ")");
                    callback.onComplete(false, error.getResponse().getStatus(), queryName, null);
                } catch (Exception e) {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlacePredictions : Error = " + error.getKind() + " for (place_query = " + queryName + ")");
                    callback.onComplete(false, -1, queryName, null);
                }
            }
        });
    }

    public void getDirections(LatLng origin, LatLng destination, final DirectionsCallback callback) {
        String originLatLngString = "" + origin.latitude + "," + origin.longitude;
        String destinationLatLngString = "" + destination.latitude + "," + destination.longitude;
        googleRetrofitInterface.getDirections(originLatLngString, destinationLatLngString, GOOGLE_API_KEY, new Callback<DirectionsData>() {
            @Override
            public void success(DirectionsData data, Response response) {
                if (data != null && data.routes != null && data.routes.size() > 0 && data.routes.get(0) != null && data.routes.get(0).legs != null /*&& data.routes.get(0).legs.get(0) != null*/)
                    callback.onComplete(data.routes.get(0).legs, response.getStatus());
                else if (data != null && data.routes != null && data.routes.size() <= 0)
                    callback.onComplete(null, 1000);
                else
                    callback.onComplete(null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onComplete(null, -1);
            }
        });
    }
}


/*
public void getPlaceDetailsByLatLng(final LatLng location, final PlaceDetailsCallback callback) {

        String latlng = location.latitude + "," + location.longitude;

        googleRetrofitInterface.getPlaceDetailsByLatLng(latlng, new Callback<PlaceSearchDataByLatLng>() {
            @Override
            public void success(PlaceSearchDataByLatLng placeSearchDataByLatLng, Response response) {

                if (placeSearchDataByLatLng.getStatus().equalsIgnoreCase("OK") || placeSearchDataByLatLng.getStatus().equalsIgnoreCase("ZERO_RESULTS")) {

                    if (placeSearchDataByLatLng.getResults() != null && placeSearchDataByLatLng.getResults().length > 0) {
                        PlaceSearchDataByLatLng.PlaceDet placeDet = placeSearchDataByLatLng.getResults()[0];       // Using only the 1st one.
                        PlaceSearchDataByLatLng.PlaceDet.AddressComponentDetails[] addressComponentDetails = placeDet.getAddress_components();

                        PlaceDetails placeDetails = new PlaceDetails();         // Object to be Returned
                        placeDetails.setLatLng(location);
                        placeDetails.setFullName(placeDet.getFormatted_address());

                        for (int i = 0; i < addressComponentDetails.length; i++) {
                            String[] type = addressComponentDetails[i].getTypes();
                            for (int j = 0; j < type.length; j++) {
                                if (type[j].equalsIgnoreCase("premise")) {
                                    placeDetails.setPremise(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("route")) {
                                    placeDetails.setRoute(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("sublocality_level_2")) {
                                    placeDetails.setSublocalityLevel_2(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("sublocality_level_1")) {
                                    placeDetails.setSublocalityLevel_1(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("locality")) {
                                    placeDetails.setLocality(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("administrative_area_level_1")) {
                                    placeDetails.setState(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("country")) {
                                    placeDetails.setCountry(addressComponentDetails[i].getLong_name());
                                    break;
                                } else if (type[j].equalsIgnoreCase("postal_code")) {
                                    placeDetails.setPostalCode(addressComponentDetails[i].getLong_name());
                                    break;
                                }
                            }
                        }
                        Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Success , Details=" + placeDetails.getFullAddress() + " for (" + location + ")");
                        callback.onComplete(true, 200, location, "", placeDetails);
                    } else {
                        Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Failure , Details = NO_RESULTS" + " for (" + location + ")");
                        callback.onComplete(false, 500, location, "", null);
                    }

                } else if (placeSearchDataByLatLng.getStatus().equalsIgnoreCase("ZERO_RESULTS")) {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Success , Details = ZERO_RESULTS" + " for (" + location + ")");
                    callback.onComplete(true, 200, location, "", null);
                } else {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Failure = " + placeSearchDataByLatLng.getStatus() + " for (" + location + ")");
                    callback.onComplete(false, 500, location, "", null);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (" + location + ")");
                    callback.onComplete(false, error.getResponse().getStatus(), location, "", null);
                } catch (Exception e) {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Error = " + error.getKind() + " for (" + location + ")");
                    callback.onComplete(false, -1, location, "", null);
                }

            }
        });

    }
 */