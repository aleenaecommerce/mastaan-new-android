package com.aleena.common.location;


import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import com.aleena.common.constants.ConstantsCommonLibrary;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class GetCurrentLocation implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final long INTERVAL = 1000 * 5;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long ONE_MIN = 1000 * 60;
    private static final float MINIMUM_ACCURACY = 60.0f;

    public Callback callback;   
    boolean sendCallbak = true;

    Context context;

    private LocationRequest locationRequest;
    private GoogleApiClient  googleApiClient;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;
    private Location location;

    public GetCurrentLocation(Context context, Callback callback) {

        this.callback = callback;
        this.context = context;
        this.sendCallbak = true;

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (googleApiClient != null) {
            googleApiClient.connect();
        }

        new CountDownTimer(15000, 800) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : ON_LOCATION_FETCH_TIMER_END");
                sendCallback();
            }
        }.start();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onConnected(Bundle bundle) {
        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : OnConnect");

        location = fusedLocationProviderApi.getLastLocation(googleApiClient);
        fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequest, this);

    }

    public void sendCallback() {
        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : LOCAION_SEND_CALLBACK , Location = " + location + " , sendCallback = " + sendCallbak);
        if (sendCallbak) {
            if (callback != null) {
                callback.onComplete(location);
            }
            sendCallbak = false;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : onLocationChnage " + location);
        //callback.onComplete(location);
        this.location = location;
        sendCallback();
        fusedLocationProviderApi.removeLocationUpdates(googleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void stop() {
        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SERVICE : OnConnect");
        fusedLocationProviderApi.removeLocationUpdates(googleApiClient, this);
    }

    public interface Callback {
        void onComplete(Location location);
    }


}