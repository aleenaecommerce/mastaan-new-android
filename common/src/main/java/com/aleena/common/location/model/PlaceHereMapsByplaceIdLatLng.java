package com.aleena.common.location.model;

import java.util.List;

public class PlaceHereMapsByplaceIdLatLng {

    private Response Response;
    private String full_Address;

    public String getFull_Address() {
        return full_Address;
    }

    public void setFull_Address(String full_Address) {
        this.full_Address = full_Address;
    }

    public void setResponse(Response Response) {
        this.Response = Response;
    }

    public Response getResponse() {
        return this.Response;
    }

    public class MetaInfo {
        private String Timestamp;

        public void setTimestamp(String Timestamp) {
            this.Timestamp = Timestamp;
        }

        public String getTimestamp() {
            return this.Timestamp;
        }
    }

    public class MatchQuality {
        private int District;

        public void setDistrict(int District) {
            this.District = District;
        }

        public int getDistrict() {
            return this.District;
        }
    }


    public class DisplayPosition {
        private double Latitude;

        private double Longitude;

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public double getLatitude() {
            return this.Latitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }

        public double getLongitude() {
            return this.Longitude;
        }
    }

    public class NavigationPosition {
        private double Latitude;

        private double Longitude;

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public double getLatitude() {
            return this.Latitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }

        public double getLongitude() {
            return this.Longitude;
        }
    }


    public class TopLeft {
        private double Latitude;

        private double Longitude;

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public double getLatitude() {
            return this.Latitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }

        public double getLongitude() {
            return this.Longitude;
        }
    }


    public class BottomRight {
        private double Latitude;

        private double Longitude;

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public double getLatitude() {
            return this.Latitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }

        public double getLongitude() {
            return this.Longitude;
        }
    }

    public class MapView {
        private TopLeft TopLeft;

        private BottomRight BottomRight;

        public void setTopLeft(TopLeft TopLeft) {
            this.TopLeft = TopLeft;
        }

        public TopLeft getTopLeft() {
            return this.TopLeft;
        }

        public void setBottomRight(BottomRight BottomRight) {
            this.BottomRight = BottomRight;
        }

        public BottomRight getBottomRight() {
            return this.BottomRight;
        }
    }


    public class AdditionalData {
        private String value;

        private String key;

        public void setValue(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }
    }


    public class Address {
        private String Label;

        private String Country;

        private String State;

        private String County;

        private String City;

        private String District;

        private String PostalCode;

        private List<AdditionalData> AdditionalData;

        public void setLabel(String Label) {
            this.Label = Label;
        }

        public String getLabel() {
            return this.Label;
        }

        public void setCountry(String Country) {
            this.Country = Country;
        }

        public String getCountry() {
            return this.Country;
        }

        public void setState(String State) {
            this.State = State;
        }

        public String getState() {
            return this.State;
        }

        public void setCounty(String County) {
            this.County = County;
        }

        public String getCounty() {
            return this.County;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public String getCity() {
            return this.City;
        }

        public void setDistrict(String District) {
            this.District = District;
        }

        public String getDistrict() {
            return this.District;
        }

        public void setPostalCode(String PostalCode) {
            this.PostalCode = PostalCode;
        }

        public String getPostalCode() {
            return this.PostalCode;
        }

        public void setAdditionalData(List<AdditionalData> AdditionalData) {
            this.AdditionalData = AdditionalData;
        }

        public List<AdditionalData> getAdditionalData() {
            return this.AdditionalData;
        }
    }


    public class Location {
        private String LocationId;

        private String LocationType;

        private DisplayPosition DisplayPosition;

        private List<NavigationPosition> NavigationPosition;

        private MapView MapView;

        private Address Address;

        public void setLocationId(String LocationId) {
            this.LocationId = LocationId;
        }

        public String getLocationId() {
            return this.LocationId;
        }

        public void setLocationType(String LocationType) {
            this.LocationType = LocationType;
        }

        public String getLocationType() {
            return this.LocationType;
        }

        public void setDisplayPosition(DisplayPosition DisplayPosition) {
            this.DisplayPosition = DisplayPosition;
        }

        public DisplayPosition getDisplayPosition() {
            return this.DisplayPosition;
        }

        public void setNavigationPosition(List<NavigationPosition> NavigationPosition) {
            this.NavigationPosition = NavigationPosition;
        }

        public List<NavigationPosition> getNavigationPosition() {
            return this.NavigationPosition;
        }

        public void setMapView(MapView MapView) {
            this.MapView = MapView;
        }

        public MapView getMapView() {
            return this.MapView;
        }

        public void setAddress(Address Address) {
            this.Address = Address;
        }

        public Address getAddress() {
            return this.Address;
        }
    }

    public class Result {
        private int Relevance;

        private String MatchLevel;

        private MatchQuality MatchQuality;

        private Location Location;

        public void setRelevance(int Relevance) {
            this.Relevance = Relevance;
        }

        public int getRelevance() {
            return this.Relevance;
        }

        public void setMatchLevel(String MatchLevel) {
            this.MatchLevel = MatchLevel;
        }

        public String getMatchLevel() {
            return this.MatchLevel;
        }

        public void setMatchQuality(MatchQuality MatchQuality) {
            this.MatchQuality = MatchQuality;
        }

        public MatchQuality getMatchQuality() {
            return this.MatchQuality;
        }

        public void setLocation(Location Location) {
            this.Location = Location;
        }

        public Location getLocation() {
            return this.Location;
        }
    }


    public class View {
        private String _type;

        private int ViewId;

        private List<Result> Result;

        public void set_type(String _type) {
            this._type = _type;
        }

        public String get_type() {
            return this._type;
        }

        public void setViewId(int ViewId) {
            this.ViewId = ViewId;
        }

        public int getViewId() {
            return this.ViewId;
        }

        public void setResult(List<Result> Result) {
            this.Result = Result;
        }

        public List<Result> getResult() {
            return this.Result;
        }
    }


    public class Response {
        private MetaInfo MetaInfo;

        private List<View> View;

        public void setMetaInfo(MetaInfo MetaInfo) {
            this.MetaInfo = MetaInfo;
        }

        public MetaInfo getMetaInfo() {
            return this.MetaInfo;
        }

        public void setView(List<View> View) {
            this.View = View;
        }

        public List<View> getView() {
            return this.View;
        }
    }



}
