package com.aleena.common.location.model;

/**
 * Created by venkatesh on 11/7/15.
 */

public class PlaceSearchDataByPlaceID {

    PlaceDet result;
    String status;

    public class PlaceDet {

        AddressComponentDetails []address_components;
        String formatted_address;
        Geometry geometry;

        public class AddressComponentDetails{
            String long_name;
            String short_name;
            String []types;

            public String getLong_name() {
                return long_name;
            }
            public String[] getTypes() {
                return types;
            }
        }

        public class Geometry{
            Location location;

            public Location getLocation() {
                return location;
            }
        }

        public class Location{
            double lat;
            double lng;

            public double getLat() {
                return lat;
            }

            public double getLng() {
                return lng;
            }
        }

        public AddressComponentDetails[] getAddress_components() {
            return address_components;
        }

        public String getFormatted_address() {
            return formatted_address;
        }

        public Geometry getGeometry() {
            return geometry;
        }
    }

    public String getStatus() {
        return status;
    }

    public PlaceDet getResult() {
        return result;
    }
}
