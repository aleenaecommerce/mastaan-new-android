package com.aleena.common.location.model;

import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.AddressBookItemDetails;
import com.google.android.gms.maps.model.LatLng;

public class PlaceHereMapsAddressResponse {
    public String Country;
    public String State;
    public String County;
    public String City;
    public String District;
    public String Subdistrict;
    public String PostalCode;
    public String Label;
    public Double longtiude;
    public Double latitude;
    public LatLng latLng;          // Latitude & Longitude
    String id;
    //   public List<AdditionalData> AdditionalData;
    String status;
    //String full_name;       // Full Name
    private String Building;


    public PlaceHereMapsAddressResponse(AddressBookItemDetails addressBookItemDetails) {
        this.latLng = addressBookItemDetails.getLatLng();

        this.id = addressBookItemDetails.getID();
        Label = addressBookItemDetails.getTitle();
        Label = addressBookItemDetails.getLocality();
        this.Building = addressBookItemDetails.getPremise();
        County = addressBookItemDetails.getLandmark();
        this.Building = addressBookItemDetails.getRoute();
        Subdistrict = addressBookItemDetails.getSublocalityLevel_2();
        Subdistrict = addressBookItemDetails.getSublocalityLevel_1();
        District = addressBookItemDetails.getLocality();
        State = addressBookItemDetails.getState();
        Country = addressBookItemDetails.getCountry();
        PostalCode = addressBookItemDetails.getPostalCode();
    }


    public PlaceHereMapsAddressResponse(String latLng, String premise, String sublocality, String label, String locality, String state, String country, String pincode) {
        this.latLng = LatLngMethods.getLatLngFromString(latLng);
        Label = premise;
        Subdistrict = sublocality;
        District = label;
        this.Building = locality;
        State = state;
        Country = country;
        PostalCode = pincode;
    }

    public PlaceHereMapsAddressResponse(LatLng latLng) {
        this.latLng = latLng;
    }

    public PlaceHereMapsAddressResponse() {
    }

//    public void setFullName(String full_name) {
//        this.full_name = full_name;
//    }
//
//
//    public String getFullAddress() {
//        return getLabel();
//
//    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getSubdistrict() {
        return Subdistrict;
    }

    public void setSubdistrict(String subdistrict) {
        Subdistrict = subdistrict;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        latitude = latitude;
    }

    public Double getLongtiude() {
        return longtiude;
    }

    public void setLongtiude(Double longtiude) {
        this.longtiude = longtiude;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String PostalCode) {
        PostalCode = PostalCode;
    }


    public String getBuilding() {
        return this.Building;
    }

    public void setBuilding(String Building) {
        this.Building = Building;
    }


}
