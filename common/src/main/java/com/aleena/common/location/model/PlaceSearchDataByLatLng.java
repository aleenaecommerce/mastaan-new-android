package com.aleena.common.location.model;

/**
 * Created by venkatesh on 11/7/15.
 */

public class PlaceSearchDataByLatLng {

    PlaceDet[] results;
    String status;

    public class PlaceDet {

        AddressComponentDetails[] address_components;
        String formatted_address;
        private Geometryy geometry;

        public void setGeometry(Geometryy geometry) {
            this.geometry = geometry;
        }

        public Geometryy getGeometry() {
            return this.geometry;
        }

        public class AddressComponentDetails {
            String long_name;
            String short_name;
            String[] types;

            public String getLong_name() {
                return long_name;
            }

            public String[] getTypes() {
                return types;
            }
        }

        public AddressComponentDetails[] getAddress_components() {
            return address_components;
        }

        public String getFormatted_address() {
            return formatted_address;
        }

        public class Geometryy {

            private Locationn location;

            private String location_type;


            public void setLocation(Locationn location) {
                this.location = location;
            }

            public Locationn getLocation() {
                return this.location;
            }

            public void setLocation_type(String location_type) {
                this.location_type = location_type;
            }

            public String getLocation_type() {
                return this.location_type;
            }

        }

        public class Locationn {
            private double lat;

            private double lng;

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLat() {
                return this.lat;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }

            public double getLng() {
                return this.lng;
            }
        }
    }


    public String getStatus() {
        return status;
    }

    public PlaceDet[] getResults() {
        return results;
    }
}
