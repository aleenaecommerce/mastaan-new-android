
package com.aleena.common.location.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class DirectionsData {
    public List<GeocodedWaypoint> geocoded_waypoints = new ArrayList<>();
    public List<Route> routes = new ArrayList<>();
    public String status;

    public class Route {
        public Bounds bounds;
        public List<Leg> legs = new ArrayList<>();
        public OverviewPolyline overview_polyline;
        public String summary;

        public class Bounds {
            public Location northeast;
            public Location southwest;

        }

        public class Location {
            public Double lat;
            public Double lng;
        }

        public class Leg {
            public Distance distance;
            public Duration duration;
            public String end_address;
            public Location end_location;
            public String start_address;
            public Location start_location;
            public List<Step> steps = new ArrayList<>();

            public class Distance {
                public String text;
                public Integer value;
            }

            public class Duration {
                public String text;
                public Integer value;
            }

            public class Step {
                public Distance distance;
                public Duration duration;
                public Location end_location;
                public String html_instructions;
                public Polyline polyline;
                public Location start_location;
                public String travel_mode;
                public String maneuver;

                public class Polyline {
                    public String points;

                    public List<LatLng> getPolyLinePoints() {

                        ArrayList<LatLng> poly = new ArrayList<>();
                        int index = 0, len = points.length();
                        int lat = 0, lng = 0;
                        while (index < len) {
                            int b, shift = 0, result = 0;
                            do {
                                b = points.charAt(index++) - 63;
                                result |= (b & 0x1f) << shift;
                                shift += 5;
                            } while (b >= 0x20);
                            int dLat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                            lat += dLat;

                            shift = 0;
                            result = 0;
                            do {
                                b = points.charAt(index++) - 63;
                                result |= (b & 0x1f) << shift;
                                shift += 5;
                            } while (b >= 0x20);
                            int dLng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                            lng += dLng;

                            LatLng p = new LatLng(((double) lat) / 1E5, ((double) lng) / 1E5);
                            poly.add(p);
                        }
                        return poly;

                    }
                }

            }
        }

        public class OverviewPolyline {
            public String points;
        }
    }

    public class GeocodedWaypoint {
        public String geocoder_status;
        public String place_id;
        public List<String> types = new ArrayList<>();

    }
}
