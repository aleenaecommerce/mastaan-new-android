package com.aleena.common.location.model;

/**
 * Created by venkatesh on 11/7/15.
 */

public class PlacePredictionsData {

    PlaceDet []predictions;
    String status;

    public class PlaceDet {
        String description;
        String place_id;

        public String getDescription() {
            return description;
        }

        public String getPlace_id() {
            return place_id;
        }
    }

    public String getStatus() {
        return status;
    }

    public PlaceDet[] getPredictions() {
        return predictions;
    }
}
