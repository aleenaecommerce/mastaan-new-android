package com.aleena.common.location;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.Nullable;

import com.aleena.common.location.model.DirectionsData;
import com.aleena.common.location.model.PlaceHereMapsByplaceIdLatLng;
import com.aleena.common.location.model.PlaceHereMapsSearchDataByLatLng;
import com.aleena.common.models.HereMapsPlaceDetails;
import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by venkatesh on 11/7/15.
 */
public class HereMapsPlacesAPI {
    String type;
    PlaceDetails pplace;
    String HEREMAPS_API_KEY;
    HereMapsRetrofitInterface heremapsRetrofitInterface1;

    public HereMapsPlacesAPI(String GOOGLE_API_KEY, HereMapsRetrofitInterface heremapsRetrofitInterface1) {
        this.HEREMAPS_API_KEY = GOOGLE_API_KEY;
        this.heremapsRetrofitInterface1 = heremapsRetrofitInterface1;
    }


    public void getPlaceDetailsByLatLng(final String apiKey, final String mode, final String prox, final PlaceDetailsCallback callback) {

        new AsyncTask<Void, Void, Void>() {
            com.aleena.common.models.Response<PlaceDetails> response;

            @Override
            protected Void doInBackground(Void... voids) {
                response = getPlaceDetailsByLatLng(apiKey, mode, prox);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (callback != null) {
                    //callback.onComplete(response.getStatus(), response.getCode(), prox, "", response.getItems());
                }
            }
        }.execute();
    }

    public com.aleena.common.models.Response getPlaceDetailsByLatLng(final String apiKey, final String mode, final String prox) {
        try {
            PlaceHereMapsSearchDataByLatLng placeSearchDataByLatLng = heremapsRetrofitInterface1.getPlaceDetailsByLatLngHereMaps(apiKey, mode, prox);


//
            PlaceHereMapsSearchDataByLatLng.Response placeDet = placeSearchDataByLatLng.getResponse();
            List<PlaceHereMapsSearchDataByLatLng.View> addressViews = placeDet.getView();

            // Using only the 1st one.

            pplace = new PlaceDetails();

            for (int i = 0; i < addressViews.size(); i++) {
                List<PlaceHereMapsSearchDataByLatLng.Result> addressResults = addressViews.get(0).getResult();
                for (int j = 0; i < addressResults.size(); i++) {

                    PlaceHereMapsSearchDataByLatLng.Location location = addressResults.get(j).getLocation();
                    PlaceHereMapsSearchDataByLatLng.Address adddd = addressResults.get(j).getLocation().getAddress();

                    // pplace.setLabel(location.getAddress().getLabel());
                    LatLng ll = new LatLng(location.getDisplayPosition().getLatitude(), location.getDisplayPosition().getLongitude());

                    pplace.setLatLng(ll);
                    pplace.setLatitude(location.getDisplayPosition().getLatitude());
                    pplace.setLongtitude(location.getDisplayPosition().getLongitude());
                    pplace.setFullName(addressResults.get(j).getLocation().getAddress().getLabel());

                    Log.e("setFullname", "" + adddd.getLabel());
                    Log.e("fkk", "" + pplace.getFullName());


                }

            }

            Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Success , Details=" + pplace + " for (" + pplace.getFullName() + pplace.getLatLng() + ")");
            return new com.aleena.common.models.Response<>(true, 200, "Sucess", pplace.getFullName());


        } catch (RetrofitError error) {
            try {
                Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (" + ")");
                return new com.aleena.common.models.Response<>(false, error.getResponse().getStatus(), "Error");
            } catch (Exception e) {
                Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Error = " + error.getKind() + " for (" + error + ")");
                return new com.aleena.common.models.Response<>(false, -1, "Error");
            }
        }

    }

    public void getHerePlacePredictions(final String biasLatLng, final String queryName, final int biasRadiusInMeters, final String appid, final String appcode, final String formatt, final String city, final PlacePredictionsCallback callback) {
        /*Hyderabad: 17.3850051,78.4845113, Radius: 600*1000 */
        heremapsRetrofitInterface1.getAutoSuggest(biasLatLng != null ? biasLatLng : "", queryName, biasRadiusInMeters, HEREMAPS_API_KEY, appid, appcode, formatt, city, new Callback<HereMapsPlaceDetails>() {
            @Override
            public void success(HereMapsPlaceDetails placePredictionsData, Response response) {
                //  if (placePredictionsData.getStatus().equalsIgnoreCase("OK") || placePredictionsData.getStatus().equalsIgnoreCase("ZERO_RESULTS")) {

                try {


                    Log.d("heremaps", "\nhereapi : PlacePredictions : Success , Found = " + placePredictionsData.getResults().length + " for (place_query = " + queryName + ")");
                    List<Map<String, String>> placePredictions = new ArrayList<Map<String, String>>();
                    HereMapsPlaceDetails.HerePlacesResults[] placeDet = placePredictionsData.getResults();
                    for (int i = 0; i < placeDet.length; i++) {
                        HereMapsPlaceDetails.HerePlacesResults placeDetails = placeDet[i];
                        final Map<String, String> placeData = new HashMap<String, String>();
                        placeData.put("title", placeDetails.getTitle());
                        placeData.put("vicinity", placeDetails.getVicinity());
                        //  placeData.put("id", placeDetails.getId());
                        Log.e("title map", placeDetails.getTitle());
                        // Log.e("title vicinity", placeDetails.getVicinity());
                        Log.e("placeData", "" + placeData);
                        placePredictions.add(placeData);
//                        Comparator mycomparator = Collections.reverseOrder();
//                        Collections.sort(placePredictions, mycomparator);
//                        Log.e("mycomparator", "" + mycomparator);
                        Log.e("sort", "" + placePredictions);
                        MyComparator comp = new MyComparator(placeData);

                        Map<String, String> newMap = new TreeMap(comp);
                        newMap.putAll(placeData);
                        System.out.println(newMap);
                        Log.e("newMap", ""+newMap);

                    }

                    callback.onComplete(true, 200, queryName, placePredictions);
                    Log.e("query name", queryName);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
//                } else {
//                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlacePredictions : Failure = " + placePredictionsData.getStatus() + " for (place_query = " + queryName + ")");
//                    callback.onComplete(false, 500, queryName, null);
//                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d("heremaps logs", "\nHereAPi : PlacePredictions : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (place_query = " + queryName + ")");
                    callback.onComplete(false, error.getResponse().getStatus(), queryName, null);
                } catch (Exception e) {
                    Log.d("heremaps logs", "\nHERE API : PlacePredictions : Error = " + error.getKind() + " for (place_query = " + queryName + ")");
                    callback.onComplete(false, -1, queryName, null);
                }
            }
        });
    }

    class MyComparator implements Comparator {
        Map map;

        public MyComparator(Map map) {
            this.map = map;
        }

        @Override
        public int compare(Object o1, Object o2) {
            return (o2.toString()).compareTo(o1.toString());
        }
    }


    public void getDirections(LatLng origin, LatLng destination, final DirectionsCallback callback) {
        String originLatLngString = "" + origin.latitude + "," + origin.longitude;
        String destinationLatLngString = "" + destination.latitude + "," + destination.longitude;
        heremapsRetrofitInterface1.getDirections(originLatLngString, destinationLatLngString, HEREMAPS_API_KEY, new Callback<DirectionsData>() {
            @Override
            public void success(DirectionsData data, Response response) {
                if (data != null && data.routes != null && data.routes.size() > 0 && data.routes.get(0) != null && data.routes.get(0).legs != null /*&& data.routes.get(0).legs.get(0) != null*/)
                    callback.onComplete(data.routes.get(0).legs, response.getStatus());
                else if (data != null && data.routes != null && data.routes.size() <= 0)
                    callback.onComplete(null, 1000);
                else
                    callback.onComplete(null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onComplete(null, -1);
            }
        });
    }

    public interface PlaceDetailsCallback {
        void onComplete(boolean status, int statusCode, String location, String placeID, PlaceDetails placeDetails);

    }


    public interface PlacePredictionsCallback {
        void onComplete(boolean status, int statusCode, String queryName, List<Map<String, String>> placePredictions);
    }

    public interface DirectionsCallback {
        void onComplete(@Nullable List<DirectionsData.Route.Leg> steps, int status_code);
    }

    public interface PlacesIDCallback {
        void onComplete(boolean status, int statusCode, String queryName, PlaceHereMapsByplaceIdLatLng placeHereMapsSearchDataByLatLng);
    }

}