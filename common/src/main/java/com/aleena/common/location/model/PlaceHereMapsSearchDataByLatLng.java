package com.aleena.common.location.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class PlaceHereMapsSearchDataByLatLng {
    private LatLng latLng;
    private Response Response;
    private String fullAddress;
    private String id;



    public PlaceHereMapsSearchDataByLatLng() {
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getId() {
        return id;
    }

//    public PlaceHereMapsSearchDataByLatLng(LatLng latLng) {
//        this.latLng = latLng;
//    }

    public void setId(String id) {
        this.id = id;
    }

    public Response getResponse() {
        return this.Response;
    }

    public void setResponse(Response Response) {
        this.Response = Response;
    }

    public class MetaInfo {
        private String Timestamp;

        private String NextPageInformation;

        public String getTimestamp() {
            return this.Timestamp;
        }

        public void setTimestamp(String Timestamp) {
            this.Timestamp = Timestamp;
        }

        public String getNextPageInformation() {
            return this.NextPageInformation;
        }

        public void setNextPageInformation(String NextPageInformation) {
            this.NextPageInformation = NextPageInformation;
        }
    }


    public class MatchQuality {

    }

    public class DisplayPosition {
        private double Latitude;

        private double Longitude;

        public double getLatitude() {
            return this.Latitude;
        }

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public double getLongitude() {
            return this.Longitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }
    }


    public class TopLeft {
        private double Latitude;

        private double Longitude;

        public double getLatitude() {
            return this.Latitude;
        }

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public double getLongitude() {
            return this.Longitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }
    }


    public class BottomRight {
        public double Latitude;

        public double Longitude;

        public double getLatitude() {
            return this.Latitude;
        }

        public void setLatitude(double Latitude) {
            this.Latitude = Latitude;
        }

        public double getLongitude() {
            return this.Longitude;
        }

        public void setLongitude(double Longitude) {
            this.Longitude = Longitude;
        }
    }


    public class MapView {
        private TopLeft TopLeft;

        private BottomRight BottomRight;

        public TopLeft getTopLeft() {
            return this.TopLeft;
        }

        public void setTopLeft(TopLeft TopLeft) {
            this.TopLeft = TopLeft;
        }

        public BottomRight getBottomRight() {
            return this.BottomRight;
        }

        public void setBottomRight(BottomRight BottomRight) {
            this.BottomRight = BottomRight;
        }
    }


    public class AdditionalData {
        private String value;

        private String key;

        public String getValue() {
            return this.value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getKey() {
            return this.key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }


    public final  class Address {

        public   String Country;
        public   String State;
        public  String County;
        public  String City;
        public  String District;
        public  String Subdistrict;
        public  String PostalCode;
        public  String Label;
        public List<AdditionalData> AdditionalData;

        private String Building;
        public String getLabel() {
            return this.Label;
        }

        public void setLabel(String Label) {
            this.Label = Label;
        }

        public String getCountry() {
            return this.Country;
        }

        public void setCountry(String Country) {
            this.Country = Country;
        }

        public String getState() {
            return this.State;
        }

        public void setState(String State) {
            this.State = State;
        }

        public String getCounty() {
            return this.County;
        }

        public void setCounty(String County) {
            this.County = County;
        }

        public String getCity() {
            return this.City;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public String getDistrict() {
            return this.District;
        }

        public void setDistrict(String District) {
            this.District = District;
        }

        public String getSubdistrict() {
            return this.Subdistrict;
        }

        public void setSubdistrict(String Subdistrict) {
            this.Subdistrict = Subdistrict;
        }

        public String getPostalCode() {
            return this.PostalCode;
        }

        public void setPostalCode(String PostalCode) {
            this.PostalCode = PostalCode;
        }

        public List<AdditionalData> getAdditionalData() {
            return this.AdditionalData;
        }

        public void setAdditionalData(List<AdditionalData> AdditionalData) {
            this.AdditionalData = AdditionalData;
        }

        public String getBuilding() {
            return this.Building;
        }

        public void setBuilding(String Building) {
            this.Building = Building;
        }
    }


    public class MapReference {
        private String ReferenceId;

        private double Spot;

        private String SideOfStreet;

        private String CountryId;

        private String StateId;

        private String CountyId;

        private String CityId;

        private String DistrictId;

        private String AddressId;

        private String RoadLinkId;

        public String getReferenceId() {
            return this.ReferenceId;
        }

        public void setReferenceId(String ReferenceId) {
            this.ReferenceId = ReferenceId;
        }

        public double getSpot() {
            return this.Spot;
        }

        public void setSpot(double Spot) {
            this.Spot = Spot;
        }

        public String getSideOfStreet() {
            return this.SideOfStreet;
        }

        public void setSideOfStreet(String SideOfStreet) {
            this.SideOfStreet = SideOfStreet;
        }

        public String getCountryId() {
            return this.CountryId;
        }

        public void setCountryId(String CountryId) {
            this.CountryId = CountryId;
        }

        public String getStateId() {
            return this.StateId;
        }

        public void setStateId(String StateId) {
            this.StateId = StateId;
        }

        public String getCountyId() {
            return this.CountyId;
        }

        public void setCountyId(String CountyId) {
            this.CountyId = CountyId;
        }

        public String getCityId() {
            return this.CityId;
        }

        public void setCityId(String CityId) {
            this.CityId = CityId;
        }

        public String getDistrictId() {
            return this.DistrictId;
        }

        public void setDistrictId(String DistrictId) {
            this.DistrictId = DistrictId;
        }

        public String getAddressId() {
            return this.AddressId;
        }

        public void setAddressId(String AddressId) {
            this.AddressId = AddressId;
        }

        public String getRoadLinkId() {
            return this.RoadLinkId;
        }

        public void setRoadLinkId(String RoadLinkId) {
            this.RoadLinkId = RoadLinkId;
        }
    }

    public class Location {
        private String LocationId;

        private String LocationType;

        private DisplayPosition DisplayPosition;

        private MapView MapView;

        private Address Address;

        private MapReference MapReference;

        public String getLocationId() {
            return this.LocationId;
        }

        public void setLocationId(String LocationId) {
            this.LocationId = LocationId;
        }

        public String getLocationType() {
            return this.LocationType;
        }

        public void setLocationType(String LocationType) {
            this.LocationType = LocationType;
        }

        public DisplayPosition getDisplayPosition() {
            return this.DisplayPosition;
        }

        public void setDisplayPosition(DisplayPosition DisplayPosition) {
            this.DisplayPosition = DisplayPosition;
        }

        public MapView getMapView() {
            return this.MapView;
        }

        public void setMapView(MapView MapView) {
            this.MapView = MapView;
        }

        public Address getAddress() {
            return this.Address;
        }

        public void setAddress(Address Address) {
            this.Address = Address;
        }

        public MapReference getMapReference() {
            return this.MapReference;
        }

        public void setMapReference(MapReference MapReference) {
            this.MapReference = MapReference;
        }
    }



    public class Result {
        private int Relevance;

        private double Distance;

        private String MatchLevel;

        private MatchQuality MatchQuality;

        private String MatchType;

        private Location Location;

        public int getRelevance() {
            return this.Relevance;
        }

        public void setRelevance(int Relevance) {
            this.Relevance = Relevance;
        }

        public double getDistance() {
            return this.Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getMatchLevel() {
            return this.MatchLevel;
        }

        public void setMatchLevel(String MatchLevel) {
            this.MatchLevel = MatchLevel;
        }

        public MatchQuality getMatchQuality() {
            return this.MatchQuality;
        }

        public void setMatchQuality(MatchQuality MatchQuality) {
            this.MatchQuality = MatchQuality;
        }

        public String getMatchType() {
            return this.MatchType;
        }

        public void setMatchType(String MatchType) {
            this.MatchType = MatchType;
        }

        public Location getLocation() {
            return this.Location;
        }

        public void setLocation(Location Location) {
            this.Location = Location;
        }
    }


    public class View {
        private String _type;

        private int ViewId;

        private List<Result> Result;

        public String get_type() {
            return this._type;
        }

        public void set_type(String _type) {
            this._type = _type;
        }

        public int getViewId() {
            return this.ViewId;
        }

        public void setViewId(int ViewId) {
            this.ViewId = ViewId;
        }

        public List<Result> getResult() {
            return this.Result;
        }

        public void setResult(List<Result> Result) {
            this.Result = Result;
        }
    }


    public class Response {
        private MetaInfo MetaInfo;

        private List<View> View;

        public MetaInfo getMetaInfo() {
            return this.MetaInfo;
        }

        public void setMetaInfo(MetaInfo MetaInfo) {
            this.MetaInfo = MetaInfo;
        }

        public List<View> getView() {
            return this.View;
        }

        public void setView(List<View> View) {
            this.View = View;
        }
    }
}

