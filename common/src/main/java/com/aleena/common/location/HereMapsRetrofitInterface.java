package com.aleena.common.location;

import com.aleena.common.location.model.DirectionsData;
import com.aleena.common.location.model.PlaceHereMapsByplaceIdLatLng;
import com.aleena.common.location.model.PlaceHereMapsSearchDataByLatLng;
import com.aleena.common.location.model.PlacePredictionsData;
import com.aleena.common.location.model.PlaceSearchDataByLatLng;
import com.aleena.common.location.model.PlaceSearchDataByPlaceID;
import com.aleena.common.models.HereMapsPlaceDetails;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by venkatesh on 11/6/15.
 */

public interface HereMapsRetrofitInterface {

    @GET("/reversegeocode.json")
    PlaceHereMapsSearchDataByLatLng getPlaceDetailsByLatLngHereMaps(@Query("apiKey") String apiKey, @Query("mode") String mode, @Query("prox") String prox);

    @GET("/reversegeocode.json")
    void getPlaceDetailsByLatLngHereMap(@Query("apiKey") String apiKey, @Query("mode") String mode, @Query("prox") String prox, Callback<PlaceSearchDataByLatLng> callback);


    @GET("/geocode.json")
    void getPlaceDetailsByPlaceID(@Query("apiKey") String apiKey, @Query("searchtext") String searchtext, Callback<PlaceHereMapsByplaceIdLatLng> callback);


    @GET("/places/v1/autosuggest")
    void getAutoSuggest(@Query("at") String nearLocationLatLng, @Query("q") String queryName, @Query("radius") int inRadius, @Query("key") String key, @Query("app_id") String app_id, @Query("app_code") String app_code, @Query("tf") String tf, @Query("addressFilter") String addressFilter, Callback<HereMapsPlaceDetails> callback);

    @GET("/directions/json")
    void getDirections(@Query("origin") String originLatLngString, @Query("destination") String destinationLatLngString, @Query("key") String key, Callback<DirectionsData> callback);

}
