package com.aleena.common.location;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.Nullable;

import com.aleena.common.location.model.DirectionsData;
import com.aleena.common.location.model.PlaceHereMapsByplaceIdLatLng;
import com.aleena.common.location.model.PlaceHereMapsSearchDataByLatLng;
import com.aleena.common.models.PlaceDetails;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by venkatesh on 11/7/15.
 */
public class HereMapsPlacesAPI1 {
    String type;
    PlaceDetails pplace;
    String HEREMAPS_API_KEY;
    HereMapsRetrofitInterface1 heremapsRetrofitInterface1;

    public HereMapsPlacesAPI1(String GOOGLE_API_KEY, HereMapsRetrofitInterface1 heremapsRetrofitInterface1) {
        this.HEREMAPS_API_KEY = GOOGLE_API_KEY;
        this.heremapsRetrofitInterface1 = heremapsRetrofitInterface1;
    }


    public void getPlaceDetailsByLatLng(final String apiKey, final String mode, final String prox, final PlaceDetailsCallback callback) {

        new AsyncTask<Void, Void, Void>() {
            com.aleena.common.models.Response<PlaceDetails> response;

            @Override
            protected Void doInBackground(Void... voids) {
                response = getPlaceDetailsByLatLng(apiKey, mode, prox);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (callback != null) {
                    //callback.onComplete(response.getStatus(), response.getCode(), prox, "", response.getItems());
                }
            }
        }.execute();
    }

    public void getPlaceDetailsByLatLngSearch(final String apiKey, final String mode, final PlaceDetailsCallback callback) {

        new AsyncTask<Void, Void, Void>() {
            com.aleena.common.models.Response<PlaceDetails> response;

            @Override
            protected Void doInBackground(Void... voids) {
                // response = getPlaceDetailsBySearch(apiKey, mode);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (callback != null) {
                    //callback.onComplete(response.getStatus(), response.getCode(), prox, "", response.getItems());
                }
            }
        }.execute();
    }


    public com.aleena.common.models.Response getPlaceDetailsByLatLng(final String apiKey, final String mode, final String prox) {
        try {
            PlaceHereMapsSearchDataByLatLng placeSearchDataByLatLng = heremapsRetrofitInterface1.getPlaceDetailsByLatLngHereMaps(apiKey, mode, prox);


//
            PlaceHereMapsSearchDataByLatLng.Response placeDet = placeSearchDataByLatLng.getResponse();
            List<PlaceHereMapsSearchDataByLatLng.View> addressViews = placeDet.getView();

            // Using only the 1st one.

            pplace = new PlaceDetails();

            for (int i = 0; i < addressViews.size(); i++) {
                List<PlaceHereMapsSearchDataByLatLng.Result> addressResults = addressViews.get(0).getResult();
                for (int j = 0; i < addressResults.size(); i++) {

                    PlaceHereMapsSearchDataByLatLng.Location location = addressResults.get(j).getLocation();
                    PlaceHereMapsSearchDataByLatLng.Address adddd = addressResults.get(j).getLocation().getAddress();

                    // pplace.setLabel(location.getAddress().getLabel());
                    LatLng ll = new LatLng(location.getDisplayPosition().getLatitude(), location.getDisplayPosition().getLongitude());

                    pplace.setLatLng(ll);
                    pplace.setLatitude(location.getDisplayPosition().getLatitude());
                    pplace.setLongtitude(location.getDisplayPosition().getLongitude());
                    pplace.setFullName(adddd.getLabel());

                    Log.e("setFullname", "" + adddd.getLabel());
                    Log.e("fkk", "" + pplace.getLongtitude());


                }

            }

            Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Success , Details=" + pplace + " for (" + pplace.getLongtitude() + pplace.getLatLng() + ")");
            return new com.aleena.common.models.Response<>(true, 200, "Sucess", pplace.getFullName());


        } catch (RetrofitError error) {
            try {
                Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Error = " + error.getKind() + " , StatusCode = " + error.getResponse().getStatus() + " for (" + ")");
                return new com.aleena.common.models.Response<>(false, error.getResponse().getStatus(), "Error");
            } catch (Exception e) {
                Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByLatLng : Error = " + error.getKind() + " for (" + error + ")");
                return new com.aleena.common.models.Response<>(false, -1, "Error");
            }
        }

    }

    public void getPlaceDetailsByPlaceID1(String apikey, final String placeID, final PlaceDetailsCallback callback) {
        heremapsRetrofitInterface1.getPlaceDetailsByPlaceID(apikey, placeID, new Callback<PlaceHereMapsSearchDataByLatLng>() {
            @Override
            public void success(PlaceHereMapsSearchDataByLatLng placeHereMapsByplaceIdLatLng, Response response) {
                try {
                    if (placeHereMapsByplaceIdLatLng != null) {
                        PlaceHereMapsSearchDataByLatLng.Response placeDet = placeHereMapsByplaceIdLatLng.getResponse();
                        List<PlaceHereMapsSearchDataByLatLng.View> addressViews = placeDet.getView();

                        // Using only the 1st one.

                        pplace = new PlaceDetails();

                        for (int i = 0; i < addressViews.size(); i++) {
                            List<PlaceHereMapsSearchDataByLatLng.Result> addressResults = addressViews.get(0).getResult();
//                            for (int j = 0; i < addressResults.size(); i++) {

                            PlaceHereMapsSearchDataByLatLng.Location location = addressResults.get(0).getLocation();
                            PlaceHereMapsSearchDataByLatLng.Address adddd = addressResults.get(0).getLocation().getAddress();

                            // pplace.setLabel(location.getAddress().getLabel());
                            LatLng ll = new LatLng(location.getDisplayPosition().getLatitude(), location.getDisplayPosition().getLongitude());

                            pplace.setLatLng(ll);
                            pplace.setLatitude(location.getDisplayPosition().getLatitude());
                            pplace.setLongtitude(location.getDisplayPosition().getLongitude());
                            pplace.setFullName(adddd.getLabel());

                            Log.e("setFullname", "" + adddd.getLabel());
                            Log.e("fkk", "" + pplace.getFullName());


//                            }

                        }

//
//                        }
                        callback.onComplete(true, 200, "Success", placeID, pplace);
                    } else {
                        // Log.d("GoCiboLogs", "\nGOOGLE_API : search : Failure = " + placeHereMapsByplaceIdLatLng + " for (" + placeHereMapsByplaceIdLatLng + ")");
                        callback.onComplete(false, 500, "Failure", placeID, pplace);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    Log.d("heremaps logs", "\nHERE API : PlacePredictions : Error = " + error.getKind() + " for (place_query =  " + placeID);
                } catch (Exception e) {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByPlaceID : Error = " + error.getKind() + " for (place_id = " + placeID + ")");
                }
            }
        });
    }


    public void getPlaceDetailsByPlaceID(String apikey, final String placeID, final PlaceDetailsCallback callback) {
        heremapsRetrofitInterface1.getPlaceDetailsByPlaceID(apikey, placeID, new Callback<PlaceHereMapsSearchDataByLatLng>() {
            @Override
            public void success(PlaceHereMapsSearchDataByLatLng placeHereMapsByplaceIdLatLng, Response response) {
                try {
                    PlaceHereMapsSearchDataByLatLng.Response placeDet = placeHereMapsByplaceIdLatLng.getResponse();
                    List<PlaceHereMapsSearchDataByLatLng.View> addressViews = placeDet.getView();

                    // Using only the 1st one.

                    pplace = new PlaceDetails();

                    for (int i = 0; i < addressViews.size(); i++) {
                        List<PlaceHereMapsSearchDataByLatLng.Result> addressResults = addressViews.get(0).getResult();
                        for (int j = 0; i < addressResults.size(); i++) {

                            PlaceHereMapsSearchDataByLatLng.Location location = addressResults.get(j).getLocation();
                            PlaceHereMapsSearchDataByLatLng.Address adddd = addressResults.get(j).getLocation().getAddress();

                            // pplace.setLabel(location.getAddress().getLabel());
                            LatLng ll = new LatLng(location.getDisplayPosition().getLatitude(), location.getDisplayPosition().getLongitude());

                            pplace.setLatLng(ll);
                            pplace.setLatitude(location.getDisplayPosition().getLatitude());
                            pplace.setLongtitude(location.getDisplayPosition().getLongitude());
                            pplace.setFullName(adddd.getLabel());

                            Log.e("setFullname", "" + adddd.getLabel());
                            Log.e("search name", "" + pplace.getFullName());


                        }

                    }


                    callback.onComplete(true, 200, "Success", "fgd", pplace);


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                try {
                    Log.d("heremaps logs", "\nHERE API : PlacePredictions : Error = " + error.getKind() + " for (place_query =  ");
                } catch (Exception e) {
                    Log.d("GoCiboLogs", "\nGOOGLE_API : PlaceDetailsByPlaceID : Error = " + error.getKind() + " for (place_id = " + placeID + ")");
                }
            }

        });
    }


    public void getDirections(LatLng origin, LatLng destination, final DirectionsCallback callback) {
        String originLatLngString = "" + origin.latitude + "," + origin.longitude;
        String destinationLatLngString = "" + destination.latitude + "," + destination.longitude;
        heremapsRetrofitInterface1.getDirections(originLatLngString, destinationLatLngString, HEREMAPS_API_KEY, new Callback<DirectionsData>() {
            @Override
            public void success(DirectionsData data, Response response) {
                if (data != null && data.routes != null && data.routes.size() > 0 && data.routes.get(0) != null && data.routes.get(0).legs != null /*&& data.routes.get(0).legs.get(0) != null*/)
                    callback.onComplete(data.routes.get(0).legs, response.getStatus());
                else if (data != null && data.routes != null && data.routes.size() <= 0)
                    callback.onComplete(null, 1000);
                else
                    callback.onComplete(null, response.getStatus());
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onComplete(null, -1);
            }
        });
    }

    public interface PlaceDetailsCallback {
        void onComplete(boolean status, int statusCode, String location, String placeID, PlaceDetails placeDetails);

    }


    public interface PlacePredictionsCallback {
        void onComplete(boolean status, int statusCode, String queryName, List<Map<String, String>> placePredictions);
    }

    public interface DirectionsCallback {
        void onComplete(@Nullable List<DirectionsData.Route.Leg> steps, int status_code);
    }

    public interface PlacesIDCallback {
        void onComplete(boolean status, int statusCode, String queryName, PlaceHereMapsByplaceIdLatLng placeHereMapsSearchDataByLatLng);
    }

}