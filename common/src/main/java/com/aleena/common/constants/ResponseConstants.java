package com.aleena.common.constants;

/**
 * Created in Venkateh Uppu on 31/10/18.
 */

public final class ResponseConstants {
    public static final int SUCCESS = 200;
    public static final int UNKNOWN_ERROR = -1;
    public static final int NOT_FOUND = 404;
    public static final int INTERNAL_ERROR = 500;
    public static final int UNAUTHORIZED = 401;
    public static final int FORBIDDEN = 403;
    public static final int INPUT_ERROR = -2;
    public static final int INBUILT_ITEM = -3;
    public static final int DATA_EXISTS = -4;
    public static final int ALREADY_EXISTS = -5;
    public static final int NO_DATA = -6;
    public static final int EXPIRED = -7;
    public static final int CANCELLED = -8;
}
