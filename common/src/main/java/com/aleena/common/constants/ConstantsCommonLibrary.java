package com.aleena.common.constants;

/**
 * Created by venkatesh on 8/9/15.
 */
public final class ConstantsCommonLibrary {

    public static final String LOG_TAG =  "vCommonLogs";

    public static final int PERMISSIONS_SETTINGS_ACTIVITY_CODE = 908;
    public static final int INTERNET_ENABLER_ACIIVITY_CODE = 910;
    public static final int LOCATION_SETTINGS_ACTIVITY_CODE = 911;
    public static final int FILE_CHOOSE_ACTIVITY_CODE = 912;
    public static final int CAPTURE_IMAGE_ACTIVITY_CODE = 913;
    public static final int CONTACT_PICK_ACTIVITY_CODE = 914;

    public static final int PLACE_SEARCH_ACTIVITY_CODE = 925;

    public static final String PLACE_DETAILS = "PLACE_DETAILS";
    public static final String LATLNG = "LATLNG";


}
