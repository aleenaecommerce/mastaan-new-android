package com.aleena.common.constants;

/**
 * Created by venkatesh on 8/3/16.
 */
public final class DateConstants {

    public static final String []WEEK_DAYS = new String[]{"", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    public static final String[] COMMON_DATE_FORMATS = new String[]{"dd-MM-yyyy, hh:mm:ss a", "dd/MM/yyyy, hh:mm:ss a"
            , "dd-MM-yyyy, hh:mm a", "yyyy-MM-dd, hh:mm a", "dd/MM/yyyy, hh:mm a", "yyyy-MM-dd, hh:mm a"
            , "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy/MM/dd'T'HH:mm:ss.SSS'Z'"
            , "dd-MM-yyyy hh:mm:ss a", "dd/MM/yyyy hh:mm:ss a"
            , "dd-MM-yyyy hh:mm a", "dd/MM/yyyy hh:mm a"
            , "dd-MM-yyyy, hh:mm a", "dd/MM/yyyy, hh:mm a"
            , "dd-MM-yyyy, HH:mm", "dd/MM/yyyy, HH:mm"
            , "dd-MM-yyyy HH:mm", "dd/MM/yyyy HH:mm"
            , "MMM dd, yyyy HH:mm", "MMM dd, yyyy hh:mm a", "MMM dd, yyyy"
            , "dd/MM/yyyy", "dd-MM-yyyy"
            , "yyyy/MM/dd", "yyyy-MM-dd"
            , "MMM dd", "MMM yyyy", "hh:mm:ss a", "hh:mm a", "HH:mm:ss", "HH:mm", "yyyy"
    };
    /*public static final String[] COMMON_DATE_FORMATS = new String[]{
            "dd-MM-yyyy'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            , "HH:mm", "MMM dd", "hh:mm a", "HH:mm:ss", "hh:mm:ss a", "yyyy-MM-dd", "yyyy/MM/dd", "dd-MM-yyyy", "dd/MM/yyyy", "MMM dd, yyyy"
            , "dd-MM-yyyy, HH:mm", "dd/MM/yyyy, HH:mm", "dd-MM-yyyy, hh:mm a", "dd/MM/yyyy, hh:mm a"
            , "dd-MM-yyyy, HH:mm:ss", "dd/MM/yyyy, HH:mm:ss", "dd-MM-yyyy, hh:mm:ss a", "dd/MM/yyyy, hh:mm:ss a"
            , "MMM dd, yyyy HH:mm", "MMM dd, yyyy hh:mm a"
            , "MMM dd, yyyy HH:mm:ss", "MMM dd, yyyy hh:mm:ss a"
            , "yyyy-MM-dd, HH:mm", "yyyy/MM/dd, HH:mm", "yyyy-MM-dd, hh:mm a", "yyyy/MM/dd, hh:mm a"
            , "yyyy-MM-dd, HH:mm:ss", "yyyy/MM/dd, HH:mm:ss", "yyyy-MM-dd, hh:mm:ss a", "yyyy/MM/dd, hh:mm:ss a"
    };*/
    public static final String UTC_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";//"yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    public static final String DD_MM_YYYY_HH_MM_SS_A = "dd/MM/yyyy, hh:mm:ss a";
    public static final String DD_MM_YYYY_HH_MM_A = "dd/MM/yyyy, hh:mm a";
    public static final String DD_MM_YYYY_HH_MM = "dd/MM/yyyy, HH:mm";
    public static final String MMM_DD_YYYY_HH_MM_A = "MMM dd yyyy, hh:mm a";
    public static final String MMM_DD_HH_MM_A = "MMM dd, hh:mm a";

    public static final String DD_MM_YYYY = "dd/MM/yyyy";
    public static final String MMM_DD_YYYY = "MMM dd, yyyy";
    public static final String DD_MMM_YYYY = "dd-MMM-yyyy";

    public static final String MMM_DD = "MMM dd";
    public static final String MMM_YYYY = "MMM yyyy";

    public static final String DD_MMM = "dd MMM";

    public static final String YYYY = "yyyy";

    public static final String HH_MM_SS_A = "hh:mm:ss a";
    public static final String HH_MM_SS = "HH:mm:ss";

    public static final String HH_MM_A = "hh:mm a";
    public static final String HH_MM = "HH:mm";


}
