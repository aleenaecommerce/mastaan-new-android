package com.aleena.common.constants;

/**
 * Created by venkatesh on 14/3/16.
 */

public final class SpecialCharacters {

    public static final String RS = "\u20B9";
    public static final String GREATER_THAN = "\u003E";

    public static final String DOT = "\u2022";

    public static final String CHECK_MARK = "\u2713";
    public static final String X_MARK = "\u2717";
}
