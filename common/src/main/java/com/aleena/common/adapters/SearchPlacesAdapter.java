package com.aleena.common.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.R;

import java.util.List;
import java.util.Map;

public class SearchPlacesAdapter extends RecyclerView.Adapter<SearchPlacesAdapter.ViewHolder> {

    Context context;
    List<Map<String, String>> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public SearchPlacesAdapter(Context context, List<Map<String, String>> itemsList, CallBack callBack) {
        this.context = context;
        this.itemsList = itemsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public SearchPlacesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_search_place, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        try {


            final Map<String, String> placeDetails = itemsList.get(position);

            Log.e("title adapter", "" + placeDetails);
//            String[] splitText1 = placeDetails.get("title").split(",", 2);
//        viewHolder.title.setText(splitText[0]);
//        viewHolder.description.setText(splitText1[1]);

        viewHolder.title.setText(placeDetails.get("title"));
        viewHolder.description.setText(placeDetails.get("vicinity"));

//            if (splitText1.length == 2) {
//                viewHolder.title.setText(splitText1[0].trim());
//                viewHolder.description.setText(splitText1[1].trim());
//            } else {
//                viewHolder.title.setText(splitText1[0].trim());
//
//            }

            if (position == getItemCount() - 1) {
                viewHolder.separator.setVisibility(View.INVISIBLE);
            }

            if (callBack != null && viewHolder.placeSelector != null) {
                viewHolder.placeSelector.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onItemClick(position);
                    }
                });
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public void addItems(List<Map<String, String>> items) {

        if (items != null) {
            for (int i = 0; i < items.size(); i++) {
                itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public void clearItems() {
        itemsList.clear();
        notifyDataSetChanged();
    }

    public List<Map<String, String>> getAllItems() {
        return itemsList;
    }

    public int getItemsCount() {
        return itemsList.size();
    }

    public Map<String, String> getPlaceDetails(int position) {
        if (position < itemsList.size()) {
            return itemsList.get(position);
        }
        return null;
    }

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        Button placeSelector;
        TextView title;
        TextView description;
        View separator;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            placeSelector = (Button) itemLayoutView.findViewById(R.id.placeSelector);
            title = (TextView) itemLayoutView.findViewById(R.id.title);
            description = (TextView) itemLayoutView.findViewById(R.id.subtitle);
            separator = (View) itemLayoutView.findViewById(R.id.separator);
        }
    }

}