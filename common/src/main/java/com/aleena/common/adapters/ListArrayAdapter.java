package com.aleena.common.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aleena.common.R;
import com.aleena.common.methods.CommonMethods;

import java.util.List;

public class ListArrayAdapter extends ArrayAdapter<String>{

    Context context;
    LayoutInflater inflater;
    int layoutView;
    int popupLayoutView = -1;
    List<String> listItems;
    boolean showAlternateColors;

    public void setPopupLayoutView(int popupLayoutView) {
        this.popupLayoutView = popupLayoutView;
    }



    class ViewHolder {          // Food Item View Holder
        TextView name;
    }

    public ListArrayAdapter(Context context, List<String> listItems) {
        super(context, R.layout.view_list_item, listItems);
        layoutView = R.layout.view_list_item;
        this.context = context;
        this.listItems = listItems;

        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ListArrayAdapter(Context context, int layoutView, List<String> listItems) {
        super(context, layoutView, listItems);
        this.context = context;
        this.layoutView = layoutView;
        this.listItems = listItems;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ListArrayAdapter(Context context, String []listItems) {
        super(context, R.layout.view_list_item, listItems);
        layoutView = R.layout.view_list_item;
        this.context = context;
        this.listItems = CommonMethods.getStringListFromStringArray(listItems);

        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public ListArrayAdapter(Context context, int layoutView, String []listItems) {
        super(context, layoutView, listItems);
        this.context = context;
        this.layoutView = layoutView;
        this.listItems = CommonMethods.getStringListFromStringArray(listItems);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if ( convertView == null ) {
            convertView = inflater.inflate(layoutView, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        setupView(position, viewHolder);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if ( convertView == null ) {
            try{
                if ( popupLayoutView != -1 ) {
                    convertView = LayoutInflater.from(context).inflate(popupLayoutView, parent, false);
                }
            }catch (Exception e){}
            if ( convertView == null){
                convertView = LayoutInflater.from(context).inflate(layoutView, parent, false);
            }
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        setupView(position, viewHolder);        // Display Details

        return convertView;
    }

    public void setupView(final int position, ViewHolder viewHolder){
        viewHolder.name.setText(getItem(position));

        if ( showAlternateColors ){
            if ( position % 2 == 0 ) {
                viewHolder.name.setBackgroundColor(Color.parseColor("#d1d1d1"));
            }else{
                viewHolder.name.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        }
    }

    public ListArrayAdapter setShowAlternateColors(boolean showAlternateColors) {
        this.showAlternateColors = showAlternateColors;
        return this;
    }

    public void setItems(List<String> listItems) {
        this.listItems = listItems;
        notifyDataSetChanged();
    }

    public void clearItems(){
        listItems.clear();
        notifyDataSetChanged();
    }

    public void addItem(String item){
        listItems.add(item);
        notifyDataSetChanged();
    }

    public void deleteItem(int position){
        listItems.remove(position);
        notifyDataSetChanged();
    }

    public void deleteItem(String item){
        listItems.remove(item);
        notifyDataSetChanged();
    }

}
