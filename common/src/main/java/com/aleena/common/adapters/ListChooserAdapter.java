package com.aleena.common.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.R;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.methods.CommonMethods;

import java.util.ArrayList;
import java.util.List;

public class ListChooserAdapter extends RecyclerView.Adapter<ListChooserAdapter.ViewHolder> {

    Context context;
    List<String> itemsList;
    int highlightItem = -1;

    ListChooserCallback callback;

    public class ViewHolder extends RecyclerView.ViewHolder{

        //View itemSelector;
        TextView name;
        TextView indicator;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            //itemSelector = itemLayoutView.findViewById(R.id.itemSelector);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
            indicator= (TextView) itemLayoutView.findViewById(R.id.indicator);
        }
    }

    public ListChooserAdapter(Context context, List<String> itemsList, ListChooserCallback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.callback = callback;
    }

    public ListChooserAdapter(Context context, List<String> itemsList, int highlightItem, ListChooserCallback callback) {
        this.context = context;
        this.itemsList = itemsList;
        this.highlightItem = highlightItem;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ListChooserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list_chooser_item, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        String itemDetails = itemsList.get(position);

        if ( itemDetails != null ) {
            if ( itemDetails.contains("<br>") || itemDetails.contains("</br>") || itemDetails.contains("<b>") || itemDetails.contains("</b>") ){
                viewHolder.name.setText(CommonMethods.fromHtml(itemDetails));
            }else {
                viewHolder.name.setText(itemDetails);
            }

            if (viewHolder.indicator != null) {
                if (position == highlightItem) {
                    viewHolder.indicator.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.indicator.setVisibility(View.GONE);
                }
            }

            if (callback != null) {
                viewHolder.name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onSelect(position);
                    }
                });
            }
        }

    }

    //------------

    public void setItems(List<String> items){
        if ( items == null ){ items = new ArrayList<>();    }
        itemsList = items;
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItems(List<String> items){
        if ( items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

//    public String getItem(int position){
//        return itemsList.get(position);
//    }

    public List<String> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

}