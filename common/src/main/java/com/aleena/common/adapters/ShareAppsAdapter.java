package com.aleena.common.adapters;

import android.content.Context;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.aleena.common.R;


public class ShareAppsAdapter extends RecyclerView.Adapter<ShareAppsAdapter.ViewHolder> {

    Context context;
    List<ResolveInfo> itemsList;
    CallBack callBack;

    ViewGroup parent;

    public interface CallBack {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout appSelector;
        ImageView appImage;
        TextView appName;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            appSelector = (LinearLayout) itemLayoutView.findViewById(R.id.appSelector);
            appImage = (ImageView) itemLayoutView.findViewById(R.id.appIcon);
            appName = (TextView) itemLayoutView.findViewById(R.id.appName);
        }
    }

    public ShareAppsAdapter(Context context, List<ResolveInfo> shareAppsList, CallBack callBack) {
        this.context = context;
        this.itemsList = shareAppsList;
        this.callBack = callBack;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ShareAppsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        View appView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_share_app, null);
        return (new ViewHolder(appView));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final ResolveInfo appInfo = itemsList.get(position);

        String appName = appInfo.loadLabel(context.getPackageManager()).toString();//appInfo.activityInfo.loadLabel(getPackageManager()).toString();
        viewHolder.appName.setText(appName);
        viewHolder.appImage.setImageDrawable(appInfo.activityInfo.loadIcon(context.getPackageManager()));

        if ( callBack != null && viewHolder.appSelector != null ){
            viewHolder.appSelector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBack.onItemClick(position);
                }
            });
        }else{
            if ( viewHolder.appSelector != null ){    viewHolder.appSelector.setClickable(false);   }
        }

    }

    public void addItems(List<ResolveInfo> shareAppsList){
        if ( shareAppsList != null && shareAppsList.size() > 0 ){
            for(int i=0;i<shareAppsList.size();i++){
                itemsList.add(shareAppsList.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public int getItemsCount(){
        return itemsList.size();
    }

    public ResolveInfo getAppInfo(int position){
        if ( position < itemsList.size() ) {
            return itemsList.get(position);
        }
        return null;
    }

}