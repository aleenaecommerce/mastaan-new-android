package com.aleena.common.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aleena.common.R;

import java.util.ArrayList;
import java.util.List;

public class ListRecyclerViewAdapter extends RecyclerView.Adapter<ListRecyclerViewAdapter.ViewHolder> {

    Context context;
    int layoutView = R.layout.view_list_item;
    List<String> itemsList;

    Callback callback;

    public interface Callback{
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            name = (TextView) itemLayoutView.findViewById(R.id.name);
        }
    }

    public ListRecyclerViewAdapter(Context context, List<String> itemsList, Callback callback) {
        this(context, R.layout.view_list_item, itemsList, callback);
    }

    public ListRecyclerViewAdapter(Context context, int layoutView, List<String> itemsList, Callback callback) {
        this.context = context;
        this.layoutView = layoutView;
        this.itemsList = itemsList;
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View orderItemHolder = LayoutInflater.from(parent.getContext()).inflate(layoutView, null);
        return (new ViewHolder(orderItemHolder));
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {

        String itemDetails = itemsList.get(position);

        if ( itemDetails != null ) {
            if ( itemDetails.contains("<br>") || itemDetails.contains("</br>") || itemDetails.contains("<b>") || itemDetails.contains("</b>") ){
                viewHolder.name.setText(Html.fromHtml(itemDetails));
            }else {
                viewHolder.name.setText(itemDetails);
            }

            if (callback != null) {
                viewHolder.name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callback.onItemClick(position);
                    }
                });
            }
        }

    }

    //------------

    public void setItems(List<String> items){
        if ( items == null ){ items = new ArrayList<>();    }
        itemsList = items;
        notifyDataSetChanged();
    }

    public void clearItems(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public void addItems(List<String> items){
        if ( items.size() > 0 ) {
            for (int i = 0; i < items.size(); i++) {
                this.itemsList.add(items.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public List<String> getAllItems(){
        return itemsList;
    }

    public int getItemsCount(){
        return itemsList.size();
    }

}