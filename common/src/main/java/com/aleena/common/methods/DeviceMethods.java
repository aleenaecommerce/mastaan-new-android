package com.aleena.common.methods;

import android.content.Context;
import android.location.Location;
import android.telephony.TelephonyManager;

import com.google.android.gms.maps.model.LatLng;

import java.util.UUID;

/**
 * Created by venkatesh on 29/10/15.
 */
public final class DeviceMethods {


    public static final String getDeviceID(Context context) {
        String deviceID = "";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            String tmDevice = "" + telephonyManager.getDeviceId();
            String tmSerial = "";// + telephonyManager.getSimSerialNumber();
            String androidId = "" + android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

            deviceID = deviceUuid.toString();
        }catch (Exception e){}

        return deviceID;
    }

}
