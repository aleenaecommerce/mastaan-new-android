package com.aleena.common.methods;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.text.Html;
import android.util.Log;

import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.constants.DateConstants;

import org.json.JSONArray;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by venkatesh on 23/7/15.
 */

public final class CommonMethods {

    public static final CharSequence fromHtml(String text){
        if ( text != null ){
            return Html.fromHtml(text.trim().replaceAll("\n", "<br>"));
        }
        return text;
    }

    public static final int parseColor(String colorString){
        try{
            if ( colorString.contains("#") == false ){  colorString = "#"+colorString;  }
            return Color.parseColor(colorString);
        }catch (Exception e){}
        return Color.GRAY;
    }

    public static final String addSpacesBetweenCharacters(String text, int noOfSplacesBetweenCharacters){
        if ( text != null ){
            String formattedText = "";
            for (int i=0;i<text.length();i++){
                if ( formattedText.length() > 0 ) {
                    for (int j = 0; j < noOfSplacesBetweenCharacters; j++) {
                        formattedText += " ";
                    }
                }
                formattedText += (""+text.charAt(i));
            }
            return formattedText;
        }
        return text;
    }


    public static final boolean compareStrings(String string1, String string2){
        if ( string1 != null && string2 == null ){
            return false;
        }else if ( string1 == null && string2 != null ){
            return false;
        }else if ( string1 == null && string2 == null ){
            return true;
        }else{
            return string1.equals(string2);
        }
    }
    public static final boolean compareStringsIgnoreCase(String string1, String string2){
        if ( string1 != null && string2 == null ){
            return false;
        }else if ( string1 == null && string2 != null ){
            return false;
        }else if ( string1 == null && string2 == null ){
            return true;
        }else{
            return string1.equalsIgnoreCase(string2);
        }
    }

    public static final float parseFloat(String inputText){
        try{
            return Float.parseFloat(inputText);
        }catch (Exception e){}
        return 0;
    }
    public static final double parseDouble(String inputText){
        try{
            return Double.parseDouble(inputText);
        }catch (Exception e){}
        return 0;
    }
    public static final int parseInteger(String inputText){
        try{
            return Integer.parseInt(inputText);
        }catch (Exception e){}
        return 0;
    }
    public static final long parseLong(String inputText){
        try{
            return Long.parseLong(inputText);
        }catch (Exception e){}
        return 0;
    }

    //========= Validations

    public static final boolean isValidEmailAddress(CharSequence email) {
        if (email != null) {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        } else {
            return false;
        }
    }

    public static final boolean isValidPhoneNumber(String phone) {
        phone = removeCountryCodeFromPhoneNumber(phone);
        if (phone.length() == 10) {
            return true;
        } else {
            return false;
        }
    }

    //========= Phone Number

    public static final String removeCountryCodeFromPhoneNumber(String phoneNumber) {
        String modifiedPhoneNumber = "";
        if (phoneNumber != null && phoneNumber.length() > 0 ) {
            if (phoneNumber.charAt(0) == '+') {
                modifiedPhoneNumber = phoneNumber.substring(3);
            } else {
                modifiedPhoneNumber = phoneNumber;
            }
            try {
                long number = Long.parseLong(modifiedPhoneNumber);
            } catch (Exception e) {
                modifiedPhoneNumber = "";
            }
        }
        return modifiedPhoneNumber;
    }

    public static final String getContactNumberFromURI(Context context, Uri uriContact) {

        String contactNumber = "";

        try {
            String contactID = "";
            Cursor cursor = context.getContentResolver().query(uriContact, new String[]{ContactsContract.Contacts._ID}, null, null, null);
            if (cursor.moveToFirst()) {
                contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            }
            cursor.close();

            // Using the contact ID now we will get contact phone number
            Cursor cursorPhone = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                            ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,
                    new String[]{contactID}, null);

            if (cursorPhone.moveToFirst()) {
                contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                Log.d(ConstantsCommonLibrary.LOG_TAG, "Number = " + contactNumber);
            }
            cursorPhone.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return removeCountryCodeFromNumber(contactNumber);
    }

    public static final String removeCountryCodeFromNumber(String inputNumber) {

        String outputNumber = "";

        if (inputNumber != null && inputNumber.length() > 0) {
            inputNumber = removeSpacesFromString(inputNumber);
            outputNumber = inputNumber.replaceAll("\\s+", "");
            if (inputNumber.charAt(0) == '+') {
                outputNumber = inputNumber.substring(3);
            }
        }
        return outputNumber;
    }

    public static final String getContactNameFromURI(Context context, Uri uriContact) {

        String contactName = "";

        try {
            Cursor cursor = context.getContentResolver().query(uriContact, null, null, null, null);
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            }
            cursor.close();
        } catch (Exception e) {
        }

        return contactName;

    }

    public static final Bitmap getContactPhotoFromURI(Context context, Uri uriContact) {

        Bitmap contactPhoto = null;

        try {
            String contactID = "";
            Cursor cursor = context.getContentResolver().query(uriContact, new String[]{ContactsContract.Contacts._ID}, null, null, null);
            if (cursor.moveToFirst()) {
                contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            }
            cursor.close();

            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(), ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));
            if (inputStream != null) {
                contactPhoto = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
            }
        } catch (Exception e) {
        }

        return contactPhoto;
    }

    //======== JSON

    public static final String getJSONArryString(List<String> stringList) {
        if (stringList != null) {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < stringList.size(); i++) {
                jsonArray.put(stringList.get(i));
            }
            return jsonArray.toString();
        }
        return "";
    }


    //========= Strings, Ints, Doubles, Floats, Lists, Arrays

    public static final String removeSpacesFromString(String inputString) {
        String modifiedString = "";
        if (inputString != null) {
            modifiedString = inputString.trim();
            modifiedString = modifiedString.replaceAll("\\s+", "");
        }
        return modifiedString;
    }

    public static final String getInDecimalFormat(float inputFloat) {
        String formatedString = inputFloat + "";
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        formatedString = decimalFormat.format(inputFloat);
        return formatedString;
    }



    public static final String getInDecimalFormat(double inputDouble) {
        String formatedString = inputDouble + "";
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        formatedString = decimalFormat.format(inputDouble);
        return formatedString;
    }

    public static final String getIndianFormatNumber(double inputDouble) {
        DecimalFormat indinaNumberFormat = new DecimalFormat("#######,##,##,###.##");
        return indinaNumberFormat.format(inputDouble);
    }

    public static final String getStringFromTo(String string, int from, int to){
        if ( string == null ){  string = "";    }
        if ( to >= from && string.length() > to ){
            String formattedString = "";
            for (int i=from;i<=to;i++){
                formattedString += (""+string.charAt(i));
            }
            return formattedString;
        }
        return string;
    }
    public static final String capitalizeFirstLetter(String string) {
        if (string != null) {
            string = string.toLowerCase();
            if (string.length() >= 2) {
                return string.substring(0, 1).toUpperCase() + string.substring(1);
            } else if (string.length() == 1) {
                return string.substring(0, 1).toUpperCase();
            } else {
                return string;
            }
        } else {
            return "";
        }
    }

    public static final String getStringOfMaxSize(String inputTex, int maxSize){
        if ( inputTex != null && inputTex.length() > 0 && maxSize > 0 && maxSize <= inputTex.length()){
            inputTex = inputTex.trim();
            if ( maxSize -2 >= 0 ) {
                return inputTex.substring(0, maxSize - 2) + "..";
            }else{
                return inputTex.substring(0, maxSize);
            }
        }
        return inputTex;
    }

    public static final String getStringOfSize(String inputTex, int size){
        if ( inputTex != null && inputTex.length() > 0 && size > 0){
            inputTex = inputTex.trim();
            if ( inputTex.length() > size ){
                return inputTex.substring(0, size - 2) + "..";
            }else if( inputTex.length() < size ){
                for (int i=inputTex.length();i<size;i++){
                    inputTex += " ";
                }
                return inputTex;
            }
        }
        return inputTex;
    }

    public static final String capitalizeStringWords(String string) {
        if (string != null) {

            String capitalizedWordsString = "";

            String[] splitedWords = string.split("\\s+");
            for (int i = 0; i < splitedWords.length; i++) {
                splitedWords[i] = capitalizeFirstLetter(splitedWords[i]);
            }
            for (int i = 0; i < splitedWords.length; i++) {
                if (i != 0) {
                    capitalizedWordsString += " ";
                }
                capitalizedWordsString += splitedWords[i];
            }

            return capitalizedWordsString;
        } else {
            return "";
        }
    }

    public static final String getStringFromStringArray(String[] stringArray) {
        return getStringFromStringArray(stringArray, ", ");
    }
    public static final String getStringFromStringArray(String[] stringArray, String separator) {

        if (stringArray != null) {
            String string = "";
            for (int i = 0; i < stringArray.length; i++) {
                if ( stringArray[i] != null && stringArray[i].length() > 0 ){
                    if ( string.length() > 0 ) {
                        string += separator;
                    }
                    string += stringArray[i];
                }
            }
            return string;
        }
        return "";
    }

    public static final String[] addStringArrays(String[] array1, String[] array2) {
        if (array1 != null && array2 != null) {
            String[] array = new String[array1.length + array2.length];
            for (int i = 0; i < array1.length; i++) {
                array[i] = array1[i];
            }
            for (int i = 0; i < array2.length; i++) {
                array[array1.length + i] = array2[i];
            }
            return array;
        }
        return new String[0];
    }

    public static final List<String> addStringLists(List<String> list1, List<String> list2) {
        if ( list1 == null ){
            list1 = new ArrayList<>();
        }
        if ( list2 != null && list2.size() > 0 ){
            for (int i=0;i<list2.size();i++){
                list1.add(list2.get(i));
            }
        }
        return list1;
    }

    public static final String[] getStringArrayFromStringList(List<String> stringsList) {
        String[] stringsArray = new String[0];
        if (stringsList != null && stringsList.size() > 0) {
            stringsArray = new String[stringsList.size()];
            for (int i = 0; i < stringsList.size(); i++) {
                stringsArray[i] = stringsList.get(i);
            }
        }
        return stringsArray;
    }

    public static final List<String> getStringListFromString(String inputString, String separtaor){
        if ( inputString != null && inputString.length() > 0 && separtaor != null ){
            String []list = inputString.split(separtaor);
            for (int i=0;i<list.length;i++){
                list[i] = list[i].trim();
            }
            return getStringListFromStringArray(list);
        }
        return new ArrayList<>();
    }
    public static final String[] getStringArrayFromString(String inputString, String separtaor){
        if ( inputString != null && inputString.length() > 0 && separtaor != null ){
            return inputString.split(separtaor);
        }
        return new String[]{};
    }

    public static final List<String> getStringListFromStringArray(String []stringArray) {
        return getStringListFromStringArray(stringArray, false);
    }
    public static final List<String> getStringListFromStringArray(String []stringArray, boolean excludeNulls) {
        List<String> stringList = new ArrayList<>();
        if (stringArray != null && stringArray.length > 0) {
            for (int i = 0; i < stringArray.length; i++) {
                if ( !excludeNulls || stringArray[i] != null ){
                    stringList.add(stringArray[i]);
                }
            }
        }
        return stringList;
    }

    public static final String getStringFromStringList(List<String> stringList) {
        return getStringFromStringList(stringList, ", ");
    }
    public static final String getStringFromStringList(List<String> stringList, String separator) {

        if (stringList != null) {
            String string = "";
            for (int i = 0; i < stringList.size(); i++) {
                if (stringList.get(i) != null) {
                    if (string.length() != 0) {
                        string += separator;
                    }
                    string += stringList.get(i);
                }
            }
            return string;
        } else {
            return "";
        }
    }

    public static final String getStringLinesFromStringArray(String[] stringArray) {

        if (stringArray != null) {
            String string = "";
            for (int i = 0; i < stringArray.length; i++) {
                if (stringArray[i] != null) {
                    if (string.length() != 0) {
                        string += "\n";
                    }
                    string += stringArray[i];
                }
            }
            return string;
        } else {
            return "";
        }
    }

    public static final int getMaxValueFromIntList(List<Integer> inputList) {
        if (inputList != null && inputList.size() > 0) {
            int[] inputArray = new int[inputList.size()];
            for (int i = 0; i < inputList.size(); i++) {
                inputArray[i] = inputList.get(i);
            }
            return getMaxValueFromIntArray(inputArray);
        } else {
            return 0;
        }
    }

    public static final int getMaxValueFromIntArray(int[] inputValues) {
        int maxValue = 0;
        if (inputValues != null) {
            for (int i = 0; i < inputValues.length; i++) {
                if (inputValues[i] > maxValue) {
                    maxValue = inputValues[i];
                }
            }
        }
        return maxValue;
    }

    public static final double getMaxValueFromDoubleList(List<Double> inputList) {
        if (inputList != null && inputList.size() > 0) {
            double[] inputArray = new double[inputList.size()];
            for (int i = 0; i < inputList.size(); i++) {
                inputArray[i] = inputList.get(i);
            }
            return getMaxValueFromDoubleArray(inputArray);
        } else {
            return 0;
        }
    }

    public static final double getMaxValueFromDoubleArray(double[] inputValues) {
        double maxValue = 0;
        if (inputValues != null) {
            for (int i = 0; i < inputValues.length; i++) {
                if (inputValues[i] > maxValue) {
                    maxValue = inputValues[i];
                }
            }
        }
        return maxValue;
    }

    public static final String getStringLinesFromStringsList(List<String> stringsList) {

        if (stringsList != null) {
            String[] stringsArray = new String[stringsList.size()];
            for (int i = 0; i < stringsList.size(); i++) {
                stringsArray[i] = stringsList.get(i);
            }
            return getStringLinesFromStringArray(stringsArray);
        } else {
            return "";
        }
    }

    public static final String[] getArrayFromString(String string) {
        if (string != null) {
            return string.split(",");
        } else {
            return (new String[]{});
        }
    }






//--------------------------------------------------------------------------CUFH







    public static final void getShareAppsList(Context context, final ShareAppsListCallback callback) {

        final List<ResolveInfo> filteredShareApps = new ArrayList<>();

        final PackageManager packageManager = context.getPackageManager();
        final Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");

        new AsyncTask<Void, Void, Void>() {

            List<ResolveInfo> shareApps = packageManager.queryIntentActivities(shareIntent, 0);
            String[] impApps = new String[]{"HipChat", "Messaging", "Email", "Mail", "Twitter", "Hangouts", "Gmail", "Facebook", "WhatsApp", "Lite"};
            String[] notRequiredApps = new String[]{"Add to Dropbox", "Asparagus", "Pocket", "Fake GPS", "Copy to clipboard", "Notes", "Bluetooth", "ColorNote", "MiTalk", "Drive", "RAR", "Add to Evernote", "Add to my yums", "Add to Swipes", "Add to Wunderlist", "Freelancer Messenger", "Keep", "Refind", "Save to drive", "Pepo"};

            @Override
            protected Void doInBackground(Void... voids) {
                for (final ResolveInfo appInfo : shareApps) {
                    String appName = appInfo.activityInfo.loadLabel(packageManager).toString();
                    boolean isRequiredApp = true;
                    for (int i = 0; i < notRequiredApps.length; i++) {
                        if (appName.trim().equalsIgnoreCase(notRequiredApps[i])) {
                            isRequiredApp = false;
                            break;
                        }
                    }

                    if (isRequiredApp == true) {
                        boolean isImpApp = false;
                        for (int i = 0; i < impApps.length; i++) {
                            if (appName.trim().equalsIgnoreCase(impApps[i])) {
                                isImpApp = true;
                                break;
                            }
                        }

                        if (isImpApp) {
                            filteredShareApps.add(0, appInfo);
                        } else {
                            filteredShareApps.add(appInfo);
                        }
                    }
                }

                if (filteredShareApps != null && filteredShareApps.size() > 0) {
                    for (int i = 0; i < filteredShareApps.size(); i++) {
                        for (int j = i; j < filteredShareApps.size(); j++) {
                            if (filteredShareApps.get(i).loadLabel(packageManager).toString().trim().compareTo(filteredShareApps.get(j).loadLabel(packageManager).toString().trim()) > 0) {
                                ResolveInfo temp = filteredShareApps.get(i);
                                filteredShareApps.set(i, filteredShareApps.get(j));
                                filteredShareApps.set(j, temp);
                            }
                        }
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                callback.onComplete(filteredShareApps);
            }
        }.execute();

    }

    public static final int dpFromPx(final Context context, final float px) {
        return (int) (px / context.getResources().getDisplayMetrics().density);
    }

    public static final int pxFromDp(final Context context, final float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    public static final void openInPlaystore(Context context, String packageName) {

        Uri uri = Uri.parse("market://details?id=" + packageName);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    public static final List<String> getRegisteredEmails(Context context) {
        List<String> accountsList = new ArrayList<String>();

        try {
            Account[] accounts = AccountManager.get(context).getAccounts();                        //For all registered accounts;
            //Account[] accounts = AccountManager.get(context).getAccountsByType("com.google");    //Getting all registered Google Accounts;
            for (Account account : accounts) {
                if (isValidEmailAddress(account.name)) {
                    accountsList.add(account.name);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return accountsList;
    }

    public interface ShareAppsListCallback {
        void onComplete(List<ResolveInfo> shareAppsList);
    }











    //-----------

    public static final Date getDateFromStringInLocal(String dateString) {

        Date date = null;

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                date = simpleDateFormat.parse(dateString);
                break;
            } catch (ParseException e) {
            }
        }
        return date;
    }

    public static final String getDateInFormatInLocal(String inputDate, String dateFormat) {

        if (inputDate == null) {
            inputDate = "";
        }
        String formatedDate = inputDate;

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat inputTimeFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                inputTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                SimpleDateFormat requiredDateFormat = new SimpleDateFormat(dateFormat);
                formatedDate = requiredDateFormat.format(inputTimeFormat.parse(inputDate));
                break;
            } catch (ParseException e) {
            }
        }

        return formatedDate;
    }

    public static final String getReadableTimeFromUTC(String utcTime) {

        if (utcTime == null) {
            utcTime = "";
        }
        SimpleDateFormat readableTimeFormat = new SimpleDateFormat("dd-MM-yyyy, hh:mm a");
        String readableTIme = utcTime;

        try {
            SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            readableTIme = readableTimeFormat.format(utcFormat.parse(utcTime));
        } catch (Exception e) {
        }

        return readableTIme.toUpperCase();
    }

    public static final int getOnlyHourInLocal(String fullTime) {

        if (fullTime == null) {
            fullTime = "";
        }
        int onlyHour = -1;

        for (String commonDateFormat : DateConstants.COMMON_DATE_FORMATS) {
            try {
                SimpleDateFormat inputTimeFormat = new SimpleDateFormat(commonDateFormat);
                inputTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(inputTimeFormat.parse(fullTime));
                onlyHour = calendar.get(Calendar.HOUR_OF_DAY);
                break;
            } catch (ParseException e) {
            }
        }

        return onlyHour;
    }

    public static final int getOnlyMinutesInLocal(String fullTime) {

        if (fullTime == null) {
            fullTime = "";
        }
        int onlyMinutes = -1;

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat inputTimeFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                inputTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(inputTimeFormat.parse(fullTime));
                onlyMinutes = calendar.get(Calendar.MINUTE);
                break;
            } catch (ParseException e) {
            }
        }

        return onlyMinutes;
    }

    public static final Date getTomorrowMidNightTime() {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        calendar.add(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public static final String[] getTimeSlotsFromNow(boolean cutOffCurrentSlot, int maxNoOfSlots) {

        ArrayList<String> timeSlots = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");

        /*String currentTime = simpleDateFormat.format(calendar.getTime());
        try {
            if (simpleDateFormat.parse(currentTime).compareTo(simpleDateFormat.parse("10:00:00")) >= 0 && simpleDateFormat.parse(currentTime).compareTo(simpleDateFormat.parse("15:00:00")) <= 0) {
                maxTime = "15:00:00";
            } else if (simpleDateFormat.parse(currentTime).compareTo(simpleDateFormat.parse("16:00:00")) >= 0 && simpleDateFormat.parse(currentTime).compareTo(simpleDateFormat.parse("18:00:00")) <= 0) {
                maxTime = "18:00:00";
            } else if (simpleDateFormat.parse(currentTime).compareTo(simpleDateFormat.parse("19:00:00")) >= 0 && simpleDateFormat.parse(currentTime).compareTo(simpleDateFormat.parse("22:00:00")) <= 0) {
                maxTime = "22:00:00";
            }else{
                maxTime = currentTime;
            }
        } catch (Exception e) {}*/
        calendar.add(Calendar.MINUTE, 60 - calendar.get(Calendar.MINUTE));
        if (cutOffCurrentSlot) {
            if (calendar.get(Calendar.MINUTE) > 30) {
                calendar.add(Calendar.HOUR, 1);
            }
        }

        while (true) {
            try {
                String slot = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.HOUR, 1);

                /*if ( simpleDateFormat.parse(simpleDateFormat.format(calendar.getTime())).compareTo(simpleDateFormat.parse(maxTime)) >= 0 ) {
                    if ( timeSlots.size() == 0 ){   timeSlots.add("No Slots Available");    }
                    break;
//                }*/
//                if (timeSlots.size() >= maxNoOfSlots) {
//                    break;
//                }
                if ( DateMethods.compareDates(slot, "10:00 PM") == 0 ) {
                    break;
//                    calendar.add(Calendar.HOUR, 11);
//                    slot = dateFormat.format(calendar.getTime());
//                    calendar.add(Calendar.HOUR, 1);
                }
                if ( DateMethods.compareDates(slot, "11:00 PM") == 0 ) {
                    break;
//                    calendar.add(Calendar.HOUR, 10);
//                    slot = dateFormat.format(calendar.getTime());
//                    calendar.add(Calendar.HOUR, 1);
                }

                slot = slot + " to " + dateFormat.format(calendar.getTime());
                timeSlots.add(slot);

                Log.d(ConstantsCommonLibrary.LOG_TAG, "slot = " + slot);

            } catch (Exception e) {
                Log.d(ConstantsCommonLibrary.LOG_TAG, "Exception = " + e);
            }
        }

        String[] availSlots = new String[timeSlots.size()];
        for (int i = 0; i < timeSlots.size(); i++) {
            availSlots[i] = timeSlots.get(i);
        }

        return availSlots;
    }

    public static final String[] getTimeSlots(String fromTime, String endTime, boolean cutOffCurrentSlot) {

        ArrayList<String> timeSlots = new ArrayList<>();

        int fromHour = 0;
        int endHour = 0;

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat fromDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                Date fromDate = fromDateFormat.parse(fromTime);
                Calendar fromCalender = Calendar.getInstance();
                fromCalender.setTime(fromDate);

                if (cutOffCurrentSlot) {
                    if (fromCalender.get(Calendar.MINUTE) != 0) {
                        fromCalender.add(Calendar.HOUR, 1);
                    }
                }

                fromHour = fromCalender.get(Calendar.HOUR_OF_DAY);
                break;
            } catch (ParseException e) {
            }
        }

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat endDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                Date endDate = endDateFormat.parse(endTime);
                Calendar endCalendar = Calendar.getInstance();
                endCalendar.setTime(endDate);
                endHour = endCalendar.get(Calendar.HOUR_OF_DAY);
                break;
            } catch (ParseException e) {
            }
        }

        Log.d(ConstantsCommonLibrary.LOG_TAG, "st = " + fromHour + " et=" + endHour);

        if (fromHour != 0 && endHour != 0) {
            if (fromHour != endHour) {
                for (int i = fromHour; i < endHour; i++) {
                    String startSlot = "" + i;
                    if (i < 12) {
                        startSlot = i + ":00 AM";
                    } else if (i == 12) {
                        startSlot = "12:00 PM";
                    } else if (i < 24) {
                        startSlot = (i - 12) + ":00 PM";
                    } else if (i == 24) {
                        startSlot = "12:00 AM";
                    }

                    int j = i + 1;
                    String endSlot = "" + j;
                    if (j < 12) {
                        endSlot = j + ":00 AM";
                    } else if (j == 12) {
                        endSlot = "12:00 PM";
                    } else if (j < 24) {
                        endSlot = (j - 12) + ":00 PM";
                    } else if (j == 24) {
                        endSlot = "12:00 AM";
                    }

                    String slot = startSlot + "  to  " + endSlot;
                    timeSlots.add(slot);
                }
            } else {
                // timeSlots.add("No Slots Available");
            }

        } else {
            // timeSlots.add("Error in finding slots");
        }

        String[] availSlots = new String[timeSlots.size()];
        for (int i = 0; i < timeSlots.size(); i++) {
            availSlots[i] = timeSlots.get(i);
        }
//        if ( availSlots.length == 0 ){
//            availSlots = new String[]{"No Slots Available"};
//        }

        return availSlots;
    }

    public static final boolean isPackageInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
