package com.aleena.common.methods;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;

/**
 * Created in vCommonLib on 03/12/16.
 * @author Venkatesh Uppu (c)
 */

public class CameraMethods {

    Activity activity;
    Context context;

    int CAPTURE_IMAGE_ACTIVITY_CODE = 121;

    CaptureImageCallback captureImageCallback;

    File tempImageFile;

    public interface CaptureImageCallback {
        void onComplete(boolean status, File capturedFile, Bitmap capturedBitmap);
    }

    public CameraMethods(Context context){
        this.context = context;
        this.activity = (Activity) context;
    }

    public CameraMethods captureImage(final CaptureImageCallback captureImageCallback){
        this.captureImageCallback = captureImageCallback;

        tempImageFile = getTempImageFile();
        if ( tempImageFile != null ) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempImageFile()));
            activity.startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_CODE);
        }else{
            Toast.makeText(context, "Unable to capture Image, try again!", Toast.LENGTH_LONG).show();
        }
        return this;
    }

    private File getTempImageFile(){
        final File path = new File( Environment.getExternalStorageDirectory(), context.getPackageName() );
        if(!path.exists()){
            path.mkdir();
        }
        return new File(path, "capture_image.png"); //it will return /sdcard/image.tmp
    }

    //============

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try{
            if (requestCode == CAPTURE_IMAGE_ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap capturedBitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.fromFile(getTempImageFile()));
                    if ( captureImageCallback != null ){
                        if ( capturedBitmap != null ){
                            captureImageCallback.onComplete(true, tempImageFile, capturedBitmap);
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(context, "Unable to capture Image, try again!", Toast.LENGTH_LONG).show();
                    if ( captureImageCallback != null ){
                        captureImageCallback.onComplete(false, null, null);
                    }
                }
            }
        }catch (Exception e){e.printStackTrace();}
    }
}
