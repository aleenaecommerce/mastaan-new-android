package com.aleena.common.methods;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.core.app.NotificationCompat;

import com.aleena.common.R;

import java.util.List;

/**
 * Created in Venkatesh Uppu on 20/08/18.
 * @author Venkatesh Uppu
 */

public class NotificationMethods {

    Context context;
    public static final String NOTIFICATIONS_CHANNEL_ID = "vMT_N_CI";


    public NotificationMethods(Context context){
        this.context = context;
    }

    public void showNotification(int index, String title, String details, Intent clickActivity){
        showNotification(index, title, details, null, clickActivity);
    }
    public void showNotification(int iconResourceID, int iconBackgroundColor, int index, String title, String details, Intent clickActivity){
        showNotification(iconResourceID, iconBackgroundColor, index, title, details, null, clickActivity);
    }
    public void showNotification(int index, String title, String details, List<NotificationAction> actionsList, Intent clickActivity){
        showNotification(R.mipmap.ic_launcher, Color.TRANSPARENT, index, title, details, actionsList, clickActivity);
    }
    public void showNotification(int iconResourceID, int iconBackgroundColor, int index, String title, String details, List<NotificationAction> actionsList, Intent clickActivity){
        try{
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, NOTIFICATIONS_CHANNEL_ID)
                    .setSmallIcon(iconResourceID)
                    .setColor(iconBackgroundColor)
                    .setContentTitle(title)
                    .setContentText(details)
                    .setAutoCancel(true)
                    //.setSound(defaultSoundUri)
                    //.setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_ALL);

            if ( actionsList != null && actionsList.size() > 0 ){
                for (int i=0;i<actionsList.size();i++){
                    NotificationAction notificationAction = actionsList.get(i);
                    if ( notificationAction != null ){
                        notificationBuilder.addAction(new NotificationCompat.Action(notificationAction.getIcon(), notificationAction.getName(), notificationAction.getIntent()));
                    }
                }
            }

            if ( clickActivity != null ) {
                //Intent notificationDetailsAct = new Intent(this, NotificationDetailsActivity.class);
                //notificationDetailsAct.putExtra(Constants.NOTIFICATION_DETAILS, new Gson().toJson(notificationDetails));
                PendingIntent pendingIntent = PendingIntent.getActivity(context, index, clickActivity, PendingIntent.FLAG_UPDATE_CURRENT);
                notificationBuilder.setContentIntent(pendingIntent);
            }
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(index, notificationBuilder.build());
        }catch (Exception e){e.printStackTrace();}
    }


    //-------

    public static class NotificationAction{
        int icon;
        String name;
        PendingIntent intent;

        public NotificationAction(String name){
            this(0, name, null);
        }
        public NotificationAction(String name, PendingIntent intent){
            this(0, name, intent);
        }
        public NotificationAction(int icon, String name){
            this(icon, name, null);
        }
        public NotificationAction(int icon, String name, PendingIntent intent){
            this.icon = icon;
            this.name = name;
            this.intent = intent;
        }

        public int getIcon() {
            return icon;
        }

        public String getName() {
            return name;
        }

        public PendingIntent getIntent() {
            return intent;
        }
    }

}
