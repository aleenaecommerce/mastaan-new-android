package com.aleena.common.methods;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import java.io.File;

/**
 * Created in vCommonLib on 03/12/16.
 * @author Venkatesh Uppu (c)
 */

public final class FileChooserMethods {

    Activity activity;
    Context context;

    int FILE_CHOOSE_ACTIVITY_CODE = 122;

    FileChooserCallback fileChooserCallback;

    public interface FileChooserCallback {
        void onComplete(boolean status, Uri uri, File file, String filePath);
    }


    public FileChooserMethods(Context context){
        this.context = context;
        this.activity = (Activity) context;
    }



    public FileChooserMethods showFileChooser(FileChooserCallback fileChooserCallback) {
        return showFileChooser("*/*", fileChooserCallback);
    }
    public FileChooserMethods showFileChooser(String fileType, FileChooserCallback fileChooserCallback) {
        if ( fileChooserCallback != null ) {
            this.fileChooserCallback = fileChooserCallback;

            Intent intent = new Intent();
            intent.setType(fileType);//intent.setType("image/*");//intent.setType("*/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            //intent.addCategory(Intent.CATEGORY_OPENABLE);

            try {
                activity.startActivityForResult(Intent.createChooser(intent, "Select a Image to upload."), FILE_CHOOSE_ACTIVITY_CODE);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(context, "Please install a File Manager to continue..", Toast.LENGTH_LONG).show();
            }
        }
        return this;
    }


    //========

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if ( requestCode == FILE_CHOOSE_ACTIVITY_CODE && resultCode == android.app.Activity.RESULT_OK ){
                if ( fileChooserCallback != null ){
                    Uri chooseFileURI = data.getData();
                    String filePath = FileMethods.getFilePath(context, chooseFileURI);
                    File file = new File(filePath);
                    if ( file != null ) {
                        if (fileChooserCallback != null){
                            fileChooserCallback.onComplete(true, chooseFileURI, file, filePath);
                        }
                    }else{
                        Toast.makeText(context, "Unable to detect file", Toast.LENGTH_LONG).show();
                        fileChooserCallback.onComplete(false, chooseFileURI, file, filePath);
                    }

                }
            }
        }catch (Exception e){e.printStackTrace();}
    }

}
