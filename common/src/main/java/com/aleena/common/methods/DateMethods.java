package com.aleena.common.methods;

import android.util.Log;

import com.aleena.common.constants.DateConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by venkatesh on 13/1/16.
 */
public final class DateMethods {


    //========== Conversions

    public static final String getDateString(Date date){
        return getDateString(date, DateConstants.DD_MM_YYYY_HH_MM_SS_A);
    }
    public static final String getDateString(Date date, String requiredFormat){
        String dateString = "";
        if ( date != null ){
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(requiredFormat);
                dateString = simpleDateFormat.format(date);
            }catch (Exception e){}
        }
        return dateString;
    }

    public static final Date getDateFromString(String dateString){

        Date date = null;

        if ( dateString != null ) {
            for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                    simpleDateFormat.setLenient(false);
                    date = simpleDateFormat.parse(dateString);
                    break;
                } catch (ParseException e) {}
            }
        }
        return date;
    }

    public static final List<Date> getDatesListFromStringsList(List<String> dateStringsList){

        List<Date> datesList = new ArrayList<>();
        if ( dateStringsList != null && dateStringsList.size() > 0 ) {
            for (int i = 0; i < dateStringsList.size(); i++) {
                Date date = getDateFromString(dateStringsList.get(i));
                if (date != null) {
                    datesList.add(date);
                }
            }
        }
        return datesList;
    }

    public static final Calendar getCalendarFromDate(String dateString){
        return getCalendarFromDate(getDateFromString(dateString));
    }

    public static final Calendar getCalendarFromDate(Date date){
        if ( date != null ){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return  calendar;
        }
        return null;
    }

    public static final String getDateFromCalendar(Date date){
        if ( date != null ){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.DD_MM_YYYY_HH_MM_SS_A);
            return simpleDateFormat.format(calendar.getTime());
        }
        return null;
    }

    public static final String getDateFromCalendar(Calendar calendar) {
        return getDateFromCalendar(calendar, "dd/MM/yyyy, hh:mm a");
    }

    public static final String getDateFromCalendar(Calendar calendar, String dateFormat) {

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
            simpleDateFormat.setLenient(false);
            return simpleDateFormat.format(calendar.getTime());
        } catch (Exception e) {}

        return "";
    }

    public static final long getDateInMilliSeconds(String fullDate){

        Calendar calendar = getCalendarFromDate(fullDate);
        if (calendar != null ){
            return calendar.getTimeInMillis();
        }

        return 0;
    }

    public static  final  String getDateInFormat(String dateString, String requiredDateFormat) {
        if ( dateString != null && dateString.length() > 0 ) {
            String dateInRequiredFormat = dateString;
            for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
                try {
                    SimpleDateFormat inputTimeFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                    inputTimeFormat.setLenient(false);
                    SimpleDateFormat outputDateFormat = new SimpleDateFormat(requiredDateFormat);
                    outputDateFormat.setLenient(false);
                    dateInRequiredFormat = outputDateFormat.format(inputTimeFormat.parse(dateString));
                    //Log.d(vConstants.LOG_TAG, "============> "+DateConstants.COMMON_DATE_FORMATS[i]+"  > "+dateString);
                    break;
                } catch (Exception e) {}
            }
            return dateInRequiredFormat;
        }
        return dateString;
    }

    public static final  String getDateInFormat(String dateString, String inputDateFormat, String requiredDateFormat) {
        if ( dateString != null && dateString.length() > 0 ) {
            String dateInRequiredFormat = dateString;
            try {
                SimpleDateFormat inputTimeFormat = new SimpleDateFormat(inputDateFormat);
                inputTimeFormat.setLenient(false);
                SimpleDateFormat onlyDateFormat = new SimpleDateFormat(requiredDateFormat);
                onlyDateFormat.setLenient(false);
                dateInRequiredFormat = onlyDateFormat.format(inputTimeFormat.parse(dateString));
            } catch (Exception e) {}

            return dateInRequiredFormat;
        }
        return dateString;
    }


    //======== Manipulations

    public static  final  String getOnlyDate(String dateString) {

        if (dateString == null) {dateString="";}
        return getDateInFormat(dateString, "dd-MM-yyyy");
    }

    public static  final  String getOnlyTime(String dateString) {
        if (dateString == null) {dateString="";}
        return getDateInFormat(dateString, "hh:mm a");
    }

    public static final String getTimeIn12HrFormat(String fullDate) {

        if (fullDate != null) {
            SimpleDateFormat monthAndDateFormat = new SimpleDateFormat("hh:mm a");
            String onlyDate = fullDate;

            for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
                try {
                    SimpleDateFormat inputTimeFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                    inputTimeFormat.setLenient(false);
                    onlyDate = monthAndDateFormat.format(inputTimeFormat.parse(fullDate));
                    break;
                } catch (ParseException e) {
                }
            }

            return onlyDate;//.toUpperCase();
        }
        return "";
    }

    public static final String getTimeIn24HrFormat(String fullDate) {

        if (fullDate != null) {
            SimpleDateFormat monthAndDateFormat = new SimpleDateFormat("HH:mm");
            String onlyDate = fullDate;

            for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
                try {
                    SimpleDateFormat inputTimeFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                    inputTimeFormat.setLenient(false);
                    onlyDate = monthAndDateFormat.format(inputTimeFormat.parse(fullDate));
                    break;
                } catch (ParseException e) {
                }
            }
            return onlyDate;//.toUpperCase();
        }

        return "";
    }

    public static  final int getDayFromDate(String dateString) {

        Calendar calendar = getCalendarFromDate(dateString);
        if ( calendar != null ){
            return calendar.get(Calendar.DAY_OF_MONTH);
        }
        return 0;
    }

    public static final int getMonthFromDate(String dateString) {

        Calendar calendar = getCalendarFromDate(dateString);
        if ( calendar != null ){
            return calendar.get(Calendar.MONTH)+1;
        }
        return 0;
    }

    public static final int getYearFromDate(String dateString) {

        Calendar calendar = getCalendarFromDate(dateString);
        if ( calendar != null ){
            return calendar.get(Calendar.YEAR);
        }
        return 0;
    }

    public static final int getHourFromDate(String dateString) {

        Calendar calendar = getCalendarFromDate(dateString);
        if ( calendar != null ){
            return calendar.get(Calendar.HOUR_OF_DAY);
        }
        return 0;
    }

    public static final int getMinutesFromDate(String dateString) {

        Calendar calendar = getCalendarFromDate(dateString);
        if ( calendar != null ){
            return calendar.get(Calendar.MINUTE);
        }
        return 0;
    }

    public static  final String getDayOfWeekFromDate(String dateString) {

        Calendar calendar = getCalendarFromDate(dateString);
        if ( calendar != null ){
            return DateConstants.WEEK_DAYS[calendar.get(Calendar.DAY_OF_WEEK)];
        }
        /*if ( dateString != null ) {
            SimpleDateFormat weekDayFormat = new SimpleDateFormat("EE");
            String onlyDayOfWeek = dateString;

            for (int i = 0; i < vDateConstants.COMMON_DATE_FORMATS.length; i++) {
                try {
                    SimpleDateFormat inputDteFormat = new SimpleDateFormat(vDateConstants.COMMON_DATE_FORMATS[i]);
                    inputTimeFormat.setLenient(false);
                    onlyDayOfWeek = weekDayFormat.format(inputDteFormat.parse(dateString));
                    break;
                } catch (ParseException e) {}
            }
            return onlyDayOfWeek.toUpperCase();
        }*/

        return dateString;
    }

    public static final String addToDateInMonths(String dateString, int additionInMonths) {

        String addedTime = dateString;

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                simpleDateFormat.setLenient(false);
                Date date = simpleDateFormat.parse(dateString);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.MONTH, additionInMonths);
                addedTime = simpleDateFormat.format(calendar.getTime());
                break;
            } catch (ParseException e) {
            }
        }

        return addedTime;
    }

    public static final String addToDateInDays(String dateString, int additionInDays) {

        String addedTime = dateString;

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                simpleDateFormat.setLenient(false);
                Date date = simpleDateFormat.parse(dateString);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.DATE, additionInDays);
                addedTime = simpleDateFormat.format(calendar.getTime());
                break;
            } catch (ParseException e) {
            }
        }

        return addedTime;
    }

    public static final Calendar addToCalendarInDays(Calendar calendar, int additionInDays) {

        if ( calendar != null ){
            calendar.add(Calendar.DATE, additionInDays);
        }

        return calendar;
    }

    public static final Calendar addToCalendarInMonths(Calendar calendar, int additionInMonths) {

        if ( calendar != null ){
            calendar.add(Calendar.MONTH, additionInMonths);
        }

        return calendar;
    }

    public static final String addToDateInHours(String dateString, double additionInHours) {

        String addedTime = dateString;

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                simpleDateFormat.setLenient(false);
                Date date = simpleDateFormat.parse(dateString);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int addMinutes = (int) (additionInHours * 60);
                calendar.add(Calendar.MINUTE, addMinutes);
                addedTime = simpleDateFormat.format(calendar.getTime());
                break;
            } catch (ParseException e) {
            }
        }

        return addedTime;
    }

    public static final String addToDateInMinutes(String time, int additionInMinutes) {

        String addedTime = time;

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                simpleDateFormat.setLenient(false);
                Date date = simpleDateFormat.parse(time);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.MINUTE, additionInMinutes);
                addedTime = simpleDateFormat.format(calendar.getTime());
                break;
            } catch (ParseException e) {
            }
        }

        return addedTime;
    }

    public static final String addToDateInSeconds(String time, int additionInSeconds) {

        String addedTime = time;

        for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                simpleDateFormat.setLenient(false);
                Date date = simpleDateFormat.parse(time);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.SECOND, additionInSeconds);
                addedTime = simpleDateFormat.format(calendar.getTime());
                break;
            } catch (ParseException e) {
            }
        }

        return addedTime;
    }


    //======== Comparisions

    public static final long getDifferenceBetweenDatesInMilliSeconds(String dateString1, String dateString2) {
        return getDifferenceBetweenDatesInMilliSeconds(getDateFromString(dateString1), getDateFromString(dateString2));
    }

    public static final long getDifferenceBetweenDatesInMilliSeconds(Date date1, Date date2) {

        if ( date1 != null && date2 != null ){
            long difference = (date1.getTime() - date2.getTime());
            if (difference < 0) {
                difference = (-1) * difference;
            }
            return difference;
        }

        return -1;
    }

    public static final long getDifferenceBetweenDatesInMilliSecondsWithSign(String dateString1, String dateString2) {
        Date date1 = getDateFromString(dateString1);
        Date date2 = getDateFromString(dateString2);
        if ( date1 != null && date2 != null ){
            return (date1.getTime() - date2.getTime());
        }
        return -1;
    }

    public static final long getDifferenceToCurrentTime(String dateString) {

        long differenceToCurrent = -1;

        if (dateString != null && dateString.length() > 4) {
            for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                    simpleDateFormat.setLenient(false);
                    Date date = simpleDateFormat.parse(dateString);
                    Date currentDate = new Date();
                    differenceToCurrent = (currentDate.getTime() - date.getTime());
                    if (differenceToCurrent < 0) {
                        differenceToCurrent = (-1) * differenceToCurrent;
                    }
                    break;
                } catch (Exception e) {
                }
            }
        }
        return differenceToCurrent;
    }

    public static final int compareDates(String stringDate1, String stringDate2) {
        return compareDates(getDateFromString(stringDate1), getDateFromString(stringDate2));
    }

    public static final int compareDates(Date date1, Date date2) {

        if (date1 != null && date2 != null) {
            if (date1.equals(date2)) {
                return 0;
            } else if (date1.before(date2)) {
                return -1;
            } else if (date1.after(date2)) {
                return 1;
            }
        }
        return -5;
    }

    public static final String getWeekDayFromDate(String dateString){
        return getWeekDayFromDate(getCalendarFromDate(dateString));
    }
    public static final String getWeekDayFromDate(Date date){
        return getWeekDayFromDate(getCalendarFromDate(date));
    }
    public static final String getWeekDayFromDate(Calendar dateCalendar){
        String []weekDays = new String[]{"", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        if ( dateCalendar != null ){
            return weekDays[dateCalendar.get(Calendar.DAY_OF_WEEK)];
        }
        return "";
    }

    public static final boolean compareWeekDays(String weekDay1, String weekDay2){

        if ( weekDay1 != null && weekDay2 != null ){
            if ( getWeekDayCodeFromString(weekDay1).equalsIgnoreCase(getWeekDayCodeFromString(weekDay2)) ){
                return true;
            }
        }
        return false;
    }

    public static final String getWeekDayCodeFromString(String weekDay){

        if ( weekDay != null ) {
            if (weekDay.equalsIgnoreCase("Sun") || weekDay.equalsIgnoreCase("Sunday")) {
                return "Sun";
            } else if (weekDay.equalsIgnoreCase("Mon") || weekDay.equalsIgnoreCase("Monday")) {
                return "Mon";
            } else if (weekDay.equalsIgnoreCase("Tue") || weekDay.equalsIgnoreCase("Tuesday")) {
                return "Tue";
            } else if (weekDay.equalsIgnoreCase("Wed") || weekDay.equalsIgnoreCase("Wednesday")) {
                return "Wed";
            } else if (weekDay.equalsIgnoreCase("Thu") || weekDay.equalsIgnoreCase("Thursday")) {
                return "Thu";
            } else if (weekDay.equalsIgnoreCase("Fri") || weekDay.equalsIgnoreCase("Friday")) {
                return "Fri";
            } else if (weekDay.equalsIgnoreCase("Sat") || weekDay.equalsIgnoreCase("Saturday")) {
                return "Sat";
            }
        }
        return weekDay;
    }

    public static final boolean isDateInRange(String compareDateString, String rangeStartString, String rangeEndString) {
        return isDateInRange(getDateFromString(compareDateString), getDateFromString(rangeStartString), getDateFromString(rangeEndString));
    }

    public static final boolean isDateInRange(Date compareDate, Date rangeStart, Date rangeEnd) {

        if (compareDate != null && rangeStart != null && rangeEnd != null) {
            if ( compareDates(compareDate, rangeStart) >= 0 && compareDates(rangeEnd, compareDate) >= 0 ){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }

    //======== Retrieve

    public static final String getCurrentDate() {
        return getCurrentDate(DateConstants.DD_MM_YYYY);
    }
    public static final String getCurrentTime() {
        return getCurrentDate(DateConstants.HH_MM_SS_A);
    }
    public static final  String getCurrentDateAndTime() {
        return getCurrentDate(DateConstants.DD_MM_YYYY_HH_MM_SS_A);
    }
    public static final String getCurrentDate(String requiredFormat) {
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(requiredFormat);
            return simpleDateFormat.format(calendar.getTime());
        } catch (Exception e) {}
        return "";
    }

    public static final String getCurrentMonthFirstDate(){
        String currentMonthFirstDate = "";
        Calendar calendar = Calendar.getInstance();   // this takes current date
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        try{
            currentMonthFirstDate = new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime());
        }catch (Exception e){}
        return currentMonthFirstDate;
    }

    public static final String getMonthStartDate(String dateString){
        String monthStartDate = "";
        if ( dateString != null && dateString.length() > 0 ){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getDateFromString(dateString));
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            try{
                monthStartDate = new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime());
            }catch (Exception e){}
        }
        return monthStartDate;
    }

    public static final String getMonthEndDate(String dateString){
        String monthEndDate = "";
        if ( dateString != null && dateString.length() > 0 ){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getDateFromString(dateString));
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            try{
                monthEndDate = new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime());
            }catch (Exception e){}
        }
        return monthEndDate;
    }

    public static final String getDateFromTimeStamp(long timeStamp){
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy, hh:mm:ss a");
            String dateString = simpleDateFormat.format(new Date(timeStamp));
            return dateString;
        }catch (Exception e){}
        return timeStamp+"";
    }

    public static final String getReadableDateFromUTC(String utcTime) {
        return getReadableDateFromUTC(utcTime, "dd-MM-yyyy, hh:mm:ss a");
    }
    public static final String getReadableDateFromUTC(String utcTime, String requiredFormat) {

        if (utcTime != null && utcTime.length()>0) {
            //Log.d("======>", requiredFormat+" for "+utcTime);
            SimpleDateFormat readableTimeFormat = new SimpleDateFormat(requiredFormat);
            String readableTIme = utcTime;

            try {
                SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");//"yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                readableTIme = readableTimeFormat.format(utcFormat.parse(utcTime));
            } catch (Exception e) {e.printStackTrace();}

            return readableTIme;//.toUpperCase();
        }
        return "";
    }

    public static final String getDateInUTCFormat(String inputDate) {
        if (inputDate != null) {
            for (int i = 0; i < DateConstants.COMMON_DATE_FORMATS.length; i++) {
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateConstants.COMMON_DATE_FORMATS[i]);
                    simpleDateFormat.setLenient(false);

                    SimpleDateFormat utcFormat = new SimpleDateFormat(DateConstants.UTC_FORMAT);
                    utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    String utcDate  = utcFormat.format(simpleDateFormat.parse(inputDate));;
                    //Log.d("TAG", "===========> Matched: "+DateConstants.COMMON_DATE_FORMATS[i]+ " for "+inputDate);
                    return utcDate;
                } catch (Exception e) {
                }
            }
        }
        return "";
    }

    //=========

    public static final String getTimeFromMilliSeconds(long milliSeconds) {
        return getTimeStringFromMinutes((milliSeconds / 1000) / 60);
    }

    public static final String getTimeStringFromMinutes(long totalTimeInMinutes) {
        return getTimeStringFromMinutes(totalTimeInMinutes, "hour", "minute");
    }

    public static final String getTimeStringFromHours(double totalTimeInHours) {
        return getTimeStringFromMinutes((long)(totalTimeInHours*60), "hour", "minute");
    }

    public static final String getTimeStringFromMinutes(long totalTimeInMinutes, String hourTAG, String minuteTAG) {
        String time = "";
        long hours = totalTimeInMinutes / 60;
        long minutes = totalTimeInMinutes % 60;

        if (hours > 0) {
            time = hours + " " + hourTAG + "s";
            if (hours == 1) {
                time = hours + " " + hourTAG;
            }
            if (minutes > 0) {
                if (minutes == 1) {
                    time = time + " " + minutes + " " + minuteTAG;
                } else {
                    time = time + " " + minutes + " " + minuteTAG + "s";
                }
            }
        } else {
            if (minutes == 1) {
                time = minutes + " " + minuteTAG;
            } else {
                time = minutes + " " + minuteTAG + "s";
            }
        }
        return time;
    }




}
