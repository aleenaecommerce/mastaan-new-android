package com.aleena.common.methods;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 29/10/15.
 */
public final class LatLngMethods {


    public static final LatLng getLatLngFromString(String latlngString) {
        LatLng latLng = new LatLng(0, 0);
        if ( latlngString != null && latlngString.length() > 0 ) {
            try {
                String[] latlong = latlngString.split(",");
                latLng = new LatLng(Double.parseDouble(latlong[0].trim()), Double.parseDouble(latlong[1].trim()));
            } catch (Exception e) {
            }
        }

        return latLng;
    }

    public static final float getDistanceBetweenLatLng(LatLng fromLatLng, LatLng toLatLng) {
        float distance = -1;
        try {
            Location fromLocation = new Location("fromLocation");
            fromLocation.setLatitude(fromLatLng.latitude);
            fromLocation.setLongitude(fromLatLng.longitude);
            Location toLocation = new Location("toLocation");
            toLocation.setLatitude(toLatLng.latitude);
            toLocation.setLongitude(toLatLng.longitude);
            distance = fromLocation.distanceTo(toLocation);
        } catch (Exception e) {}
        return distance;
    }

}
