package com.aleena.common.methods;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

import com.aleena.common.models.WifiDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Venkatesh Uppu on 21/07/17.
 */
public class WifiMethods {

    public interface WifisCallback{
        void onComplete(boolean status, int statusCode, String message, List<WifiDetails> wifisList);
    }

    public static final void getAvailableWifis(final Context context, final WifisCallback callback){

        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if ( wifiManager.isWifiEnabled() ){
            BroadcastReceiver wifiScanReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context c, Intent intent) {
                    if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                        List<ScanResult> scannedResults = wifiManager.getScanResults();

                        if( scannedResults != null ) {
                            List<WifiDetails> wifisList = new ArrayList<>();
                            for(ScanResult scanResult : scannedResults){
                                WifiDetails wifiDetails = new WifiDetails(scanResult.BSSID, scanResult.SSID);
                                wifisList.add(wifiDetails);
                            }
                            if ( callback != null ){
                                callback.onComplete(true, 200, "Success", wifisList);
                            }
                        }else{
                            if ( callback != null ){
                                callback.onComplete(false, -1, "Unknown error", null);
                            }
                        }
                        context.unregisterReceiver(this);

                        //ScanResult bestResult = null;
                        //if(bestResult == null || WifiManager.compareSignalLevel(bestResult.level, results.level) < 0){
                        //    bestResult = results;
                        //}
                    }
                }
            };
            context.registerReceiver(wifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            wifiManager.startScan();
        }
        else{
            if ( callback != null ){
                callback.onComplete(false, -1, "Wifi not enabled", null);
            }
        }
    }

}
