package com.aleena.common.methods;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.telephony.TelephonyManager;

import java.util.UUID;

/**
 * Created by venkatesh on 29/10/15.
 */
public final class AppMethods {


    public static final int getAppVersionCode(Context context) {
        int currentVersion = 0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            currentVersion = pInfo.versionCode;
        } catch (Exception e) {
        }
        return currentVersion;
    }

    public static final String getAppVersionName(Context context) {
        String versionName = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionName = pInfo.versionName;
        } catch (Exception e) {
        }
        return versionName;
    }


}
