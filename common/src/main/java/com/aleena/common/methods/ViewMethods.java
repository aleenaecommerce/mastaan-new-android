package com.aleena.common.methods;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import android.view.View;

/**
 * Created by Venkatesh Uppu on 24/11/18.
 */
public class ViewMethods {

    public static final Bitmap getBitmapFromView(@NonNull View view) {
        if ( view != null ) {
            //Define a bitmap with the same size as the view
            Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
            //Bind a canvas to it
            Canvas canvas = new Canvas(bitmap);
            //Get the view's background
            Drawable bgDrawable = view.getBackground();
            if (bgDrawable != null) {
                bgDrawable.draw(canvas);    //has background drawable, then draw it on the canvas
            } else {
                canvas.drawColor(Color.WHITE);  //does not have background drawable, then draw white background on the canvas
            }
            view.draw(canvas);  // draw the view on the canvas
            return bitmap;
        }
        return null;
    }

}
