package com.aleena.common.methods;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.aleena.common.constants.ConstantsCommonLibrary;

import java.util.Calendar;

/**
 * Created by venkatesh on 23/6/16.
 */
public class AlarmMethods {

    Context context;

    public AlarmMethods(Context context){
        this.context = context;
    }

    public void setAlarmOneTime(long timeInMillSeconds, Intent activityIntent){
        try {
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 12345, activityIntent, PendingIntent.FLAG_CANCEL_CURRENT);
//            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Activity.ALARM_SERVICE);
//            //alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), timeInMillSeconds, pendingIntent);
//            //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), timeInMillSeconds, pendingIntent);
//            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + timeInMillSeconds, pendingIntent);

//            Calendar cal = Calendar.getInstance();
//            cal.set(2016, month, h, min);
            //registering our pending intent with alarmmanager
            AlarmManager am = (AlarmManager) context.getSystemService(Activity.ALARM_SERVICE);
            am.set(AlarmManager.RTC_WAKEUP, DateMethods.getCalendarFromDate("23/06/2016, 05:37:00 PM").getTimeInMillis(), pendingIntent);

            Log.d(ConstantsCommonLibrary.LOG_TAG, "Alarm set @ "+timeInMillSeconds+" msec where currentTime: "+System.currentTimeMillis()+" msec");
        }catch (Exception e){
            Log.d(ConstantsCommonLibrary.LOG_TAG, "Alarm set exception for time @ "+timeInMillSeconds+" msec");
            e.printStackTrace();
        }
    }
}
