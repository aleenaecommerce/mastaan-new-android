package com.aleena.common.utils;


import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by venkatesh on 16/4/16.
 */
public class vFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

    public vFragmentStatePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 0;
    }

    public Fragment getCurrentFragment(ViewPager viewPager) {
        return (Fragment)this.instantiateItem(viewPager, viewPager.getCurrentItem());
    }

    public Fragment getFragment(ViewPager viewPager, int position) {
        return (Fragment)this.instantiateItem(viewPager, position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Yet another bug in FragmentStatePagerAdapter that destroyItem is called on fragment that hasnt been added. Need to catch
        try {
            super.destroyItem(container, position, object);
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
    }

}
