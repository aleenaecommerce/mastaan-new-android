package com.aleena.common.models;

/**
 * Created by venkatesh on 26/7/16.
 */
public class DeviceDetails {
    String deviceID;
    String deviceName;

    public String getDeviceID() {
        return deviceID;
    }

    public String getDeviceName() {
        return deviceName;
    }
}
