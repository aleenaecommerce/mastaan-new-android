package com.aleena.common.models;

/**
 * Created by venkatesh on 11/7/15.
 */
public class HereMapsPlaceDetails {

    String status;


    private HerePlacesResults[] results;

    public HerePlacesResults[] getResults() {
        return results;
    }

    public String getStatus() {
        return status;
    }

    public class HerePlacesResults {
        String title;
        String vicinity;
        String id;


        public String getTitle() {
            return title;
        }

        public String getVicinity() {
            return vicinity;
        }

        public String getId() {
            return id;
        }

    }


}
