package com.aleena.common.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 22/6/16.
 */
public class WifiDetails {
    String _id;

    String bssid;
    String ssid;

    Double[] loc;

    public WifiDetails(String bssid, String ssid){
        this.bssid = bssid;
        this.ssid = ssid;
    }

    public String getID() {
        if ( _id == null ){ return "";  }
        return _id;
    }

    public String getBSSID() {
        if ( bssid == null ){ return "";  }
        return bssid;
    }

    public String getSSID() {
        if ( ssid == null ){ return "";  }
        return ssid;
    }

    public void setLocation(LatLng location) {
        if ( location != null ){
            this.loc = new Double[2];
            this.loc[0] = location.latitude;
            this.loc[1] = location.longitude;
        }
    }

    public LatLng getLocation() {
        if ( loc != null && loc.length == 2 ) {
            return new LatLng(loc[0], loc[1]);
        }
        return new LatLng(0,0);
    }
}
