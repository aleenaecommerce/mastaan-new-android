package com.aleena.common.models;

import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.LatLngMethods;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 11/7/15.
 */
public class PlaceDetails{

    String id;
    Double latitude;
    Double longtitude;

    String title;           // Used for AddressBook

    LatLng latLng;          // Latitude & Longitude
    String full_name;       // Full Name

    String premise;
    String route;
    String sublocality_level_2;     // Area
    String landmark;
    String sublocality_level_1;     // Area
    String locality;        // District
    String state;           // State
    String country;         // Country
    String postal_code;     // PinCode

    public PlaceDetails(){}

    public PlaceDetails(LatLng latLng){
        this.latLng = latLng;
    }

    public PlaceDetails(String latLng, String premise, String sublocality, String landmark, String locality, String state, String country, String pincode){
        this.latLng = LatLngMethods.getLatLngFromString(latLng);
        this.premise = premise;
        this.sublocality_level_2 = sublocality;
        this.landmark = landmark;
        this.locality = locality;
        this.state = state;
        this.country = country;
        this.postal_code = pincode;
    }

    public PlaceDetails(AddressBookItemDetails addressBookItemDetails){
        this.id = addressBookItemDetails.getID();
        this.title = addressBookItemDetails.getTitle();
        this.full_name = addressBookItemDetails.getFullAddress();
        this.latLng = addressBookItemDetails.getLatLng();
        this.premise = addressBookItemDetails.getPremise();
        this.landmark = addressBookItemDetails.getLandmark();
        this.route = addressBookItemDetails.getRoute();
        this.sublocality_level_2 = addressBookItemDetails.getSublocalityLevel_2();
        this.sublocality_level_1 = addressBookItemDetails.getSublocalityLevel_1();
        this.locality = addressBookItemDetails.getLocality();
        this.state = addressBookItemDetails.getState();
        this.country = addressBookItemDetails.getCountry();
        this.postal_code = addressBookItemDetails.getPostalCode();
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(Double longtitude) {
        this.longtitude = longtitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setFullName(String full_name) {
        this.full_name = full_name;
    }

    public String getFullAddress() {
        if ( (route != null && landmark != null) && route.equalsIgnoreCase(landmark) ){
            return new CommonMethods().getStringFromStringArray(new String[]{premise, route, sublocality_level_2, sublocality_level_1, locality, state, country, postal_code});
        }
        return new CommonMethods().getStringFromStringArray(new String[]{premise, route, sublocality_level_2, landmark, sublocality_level_1, locality, state, country, postal_code});
    }

    public String getFullName() {
        return full_name;
    }

    public String getPremise() {
        return premise;
    }

    public void setPremise(String premise) {
        this.premise = premise;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getSublocalityLevel_2() {
        return sublocality_level_2;
    }

    public void setSublocalityLevel_2(String sublocality_level_2) {
        this.sublocality_level_2 = sublocality_level_2;
    }

    public String getSublocalityLevel_1() {
        return sublocality_level_1;
    }

    public void setSublocalityLevel_1(String sublocality_level_1) {
        this.sublocality_level_1 = sublocality_level_1;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getLocality() {
        return locality;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setPostalCode(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
