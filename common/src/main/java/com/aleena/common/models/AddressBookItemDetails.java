package com.aleena.common.models;

import android.location.Location;

import com.aleena.common.methods.CommonMethods;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 11/7/15.
 */
public class AddressBookItemDetails {

    public String _id;

    String ty;          // Favourite or History

    public String t;     //title
    public String fa;    //full name

    public String pos;   //latlng
    public double[] loc;

    public String p;     //premise
    public String s1;    //sub locality level1
    public String lm;    //landmark
    public String s2;    //sub locality level2
    public String r;     //route
    public String l;     //locality
    public String s;     //state
    public String c;     //country
    public String pin;   // PinCode

    public AddressBookItemDetails() {}

    public AddressBookItemDetails(PlaceDetails placeDetails) {
        this._id = placeDetails.getId();
        this.t = placeDetails.getTitle();
        this.fa = placeDetails.getFullAddress();

        this.pos = placeDetails.getLatLng().latitude + "," + placeDetails.getLatLng().longitude;
        this.loc = new double[2];
        loc[0] = placeDetails.getLatLng().latitude;
        loc[1] = placeDetails.getLatLng().longitude;

        this.p = placeDetails.getPremise();
        this.lm = placeDetails.getLandmark();
        this.r = placeDetails.getRoute();
        this.s2 = placeDetails.getSublocalityLevel_2();
        this.s1 = placeDetails.getSublocalityLevel_1();
        this.l = placeDetails.getLocality();
        this.s = placeDetails.getState();
        this.c = placeDetails.getCountry();
        this.pin = placeDetails.getPostal_code();
    }

    public AddressBookItemDetails(Location location) {
        this.pos = location.getLatitude() + "," + location.getLongitude();
        this.loc = new double[2];
        loc[0] = location.getLatitude();
        loc[1] = location.getLongitude();
    }

    public AddressBookItemDetails(LatLng latLng, String premise, String sublocality_level2, String sublocality_level1, String road_no, String landmark, String locality, String state, String country, String pincode){
        this.pos = latLng.latitude + "," + latLng.longitude;
        this.loc = new double[2];
        loc[0] = latLng.latitude;
        loc[1] = latLng.longitude;

        this.p = premise;
        this.s2 = sublocality_level2;
        this.s1 = sublocality_level1;
        this.r = road_no;
        this.lm = landmark;
        this.l = locality;
        this.s = state;
        this.c = country;
        this.pin = pincode;
    }


    public AddressBookItemDetails(String latLng, String premise, String sublocality, String landmark, String locality, String state, String country, String pincode){
        this.pos = latLng;
        this.p = premise;
        this.s2 = sublocality;
        this.lm = landmark;
        this.l = locality;
        this.s = state;
        this.c = country;
        this.pin = pincode;
    }


    public String getID() {
        if ( _id == null ){ return "";  }
        return _id;
    }

    public void setID(String _id) {
        this._id = _id;
    }

    public String getType() {
        if ( ty != null && ty.equalsIgnoreCase("h") ){
            return "history";
        }
        return "favourite";
    }

    public void setType(String type) {
        if ( type == null ){ type = ""; }
        if ( type.equalsIgnoreCase("h") || type.equalsIgnoreCase("his") ||type.equalsIgnoreCase("history") ){
            this.ty = "h";
        }else  {
            this.ty = "f";
        }
    }

    public String getTitle() {
        if ( t == null ){
            return  "";
        }
        return t;
    }

    public void setTitle(String t) {
        this.t = t;
    }

    public LatLng getLatLng() {
        if ( loc != null && loc.length == 2 ){
            return new LatLng(loc[0], loc[1]);
        }
        else {
            try {
                String[] splitText = pos.split(",", 2);
                if (splitText.length == 2) {
                    return new LatLng(Double.parseDouble(splitText[0]), Double.parseDouble(splitText[1]));
                }
            } catch (Exception e) {}
        }

        return new LatLng(0, 0);
    }

    public void setLatLng(LatLng latLng) {
        if ( latLng != null ) {
            this.pos = latLng.latitude + "," + latLng.longitude;
        }
    }

    public String getFullAddress() {
        if ( (r != null && lm != null) && r.equalsIgnoreCase(lm) ){
            return new CommonMethods().getStringFromStringArray(new String[]{p, r, s2, s1, l, s, c, pin});
        }
        return new CommonMethods().getStringFromStringArray(new String[]{p, r, s2, lm, s1, l, s, c, pin});
    }

    public String getFullName() {
        return fa;
    }

    public void setFullName(String fa) {
        this.fa = fa;
    }

    public String getPremise() {
        return p;
    }

    public void setPremise(String premise) {
        this.p = premise;
    }

    public String getRoute() {
        return r;
    }

    public String  getLandmark() {
        return lm;
    }

    public void setLandmark(String lm) {
        this.lm = lm;
    }

    public String getSublocalityLevel_2() {
        return s2;
    }

    public void setSublocalityLevel2(String sublocality_level_2) {
        this.s2 = sublocality_level_2;
    }

    public String getSublocalityLevel_1() {
        return s1;
    }


    public String getLocality() {
        return l;
    }


    public String getState() {
        return s;
    }

    public String getCountry() {
        return c;
    }

    public String getPostalCode() {
        return pin;
    }


}
