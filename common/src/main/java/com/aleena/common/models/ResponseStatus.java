package com.aleena.common.models;

/**
 * Created by venkatesh on 15/7/15.
 */

public class ResponseStatus {

    String code;
    String msg;
    boolean alreadyPresent;
    double latestAvailability;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        if ( code == null ){ code = ""; }
        return code;
    }

    public String getMessage() {
        return msg;
    }

    public boolean isAlreadyPresent(){  return  alreadyPresent; }

    public double getLatestAvailability() {
        return latestAvailability;
    }
}
