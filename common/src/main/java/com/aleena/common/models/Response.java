package com.aleena.common.models;

/**
 * Created by venkatesh on 06/09/17.
 */

public class Response<T> {
    boolean status;
    int code;
    String message;
    T item;

    public Response(boolean status, int code, String message){
        this.status = status;
        this.code = code;
        this.message = message;
    }
    public Response(boolean status, int code, String message, T item){
        this.status = status;
        this.code = code;
        this.message = message;
        this.item = item;
    }

    public boolean getStatus() {
        return status;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        if ( message == null ){ message = "";   }
        if ( message.equalsIgnoreCase("Error") ){   return "Something went wrong";  }
        return message;
    }

    public T getItem() {
        return item;
    }
    public T getItems() {
        return item;
    }
    public T getCount(){    return item;    }
}
