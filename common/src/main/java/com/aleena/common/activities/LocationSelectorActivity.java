package com.aleena.common.activities;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.R;
import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.methods.LatLngMethods;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vMapFragment;
import com.aleena.common.widgets.vMapWrapperLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

/**
 * Created by Venkatesh on 31/03/17.
 */

public class LocationSelectorActivity extends vToolBarActivity implements View.OnClickListener{

    GoogleMap map;
    View locateMe;

    View searchLocation;
    TextView location;

    View done;

    LatLng selectedLocationLatLng;
    PlaceDetails selectedLocationDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selector);

        //----------

        locateMe = findViewById(R.id.locateMe);
        locateMe.setOnClickListener(this);

        searchLocation = findViewById(R.id.searchLocation);
        searchLocation.setOnClickListener(this);
        location = (TextView) findViewById(R.id.location);

        done = findViewById(R.id.done);
        done.setOnClickListener(this);

        //map=((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
        final vMapFragment vMapFragment = ((vMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        vMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                //map = vMapFragment.getMap();
                map.setMyLocationEnabled(true);

                vMapFragment.setOnDragListener(new vMapWrapperLayout.OnDragListener() {
                    @Override
                    public void onDrag(MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                            location.setText("release map to set place..");
                        } else {
                            getPlaceName(map.getCameraPosition().target);       // on Completed Moving Map..
                        }
                    }
                });

                //----

                LatLng prefillLatLng = LatLngMethods.getLatLngFromString(getIntent().getStringExtra(ConstantsCommonLibrary.LATLNG));
                if ( prefillLatLng.latitude != 0 && prefillLatLng.longitude != 0 ){
                    selectedLocationLatLng = prefillLatLng;
                    moveCamera(selectedLocationLatLng, 1200, false);         // Move & Animate Camera..
                    getPlaceName(selectedLocationLatLng);
                }else {
                    locateMe.performClick();
                }
            }
        });

    }

    @Override
    public void onClick(View view) {

        if (view == locateMe) {
            fetchCurrentLocation(2500, true);          // Fetch Location with cameraMoveAnimate Time..
        }

        else if (view == searchLocation) {
            Intent searchActivity = new Intent(context, LocationSearchActivity.class);
            startActivityForResult(searchActivity, ConstantsCommonLibrary.PLACE_SEARCH_ACTIVITY_CODE);
        }

        else if (view == done) {
            if ( selectedLocationDetails != null ){
                Intent resultData = new Intent();
                resultData.putExtra(ConstantsCommonLibrary.PLACE_DETAILS, new Gson().toJson(selectedLocationDetails));
                setResult(RESULT_OK, resultData);
                finish();
            }else{
                showSnackbarMessage("* Select location");
            }
        }
    }

    //-------------

    public void fetchCurrentLocation(final int animateTime, final boolean adjustAnimateTime) {

        checkLocationAccess(new CheckLocatoinAccessCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {

                if (status) {

                    showLoadingDialog(true, "Fetching your current location, please wait..");
                    new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                        @Override
                        public void onComplete(Location location) {

                            closeLoadingDialog();

                            if (location != null) {
                                selectedLocationLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                                moveCamera(selectedLocationLatLng, animateTime, adjustAnimateTime);         // Move & Animate Camera with adjustAnimateTime..
                                getPlaceName(new LatLng(location.getLatitude(), location.getLongitude()));

                            } else {
                                showChoiceSelectionDialog(true, "Failure", "Unable to Fetch your current location.\n\nDo you want to try again?", "NO", "YES", new ChoiceSelectionCallback() {
                                    @Override
                                    public void onSelect(int choiceNo, String choiceName) {
                                        if (choiceName.equalsIgnoreCase("YES")) {
                                            fetchCurrentLocation(animateTime, adjustAnimateTime);          // Fetch Location with cameraMoveAnimate Time with adjustAnimateTime..
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    public void getPlaceName(LatLng locationLatLng) {

        selectedLocationLatLng = locationLatLng;
        selectedLocationDetails = null;

        location.setText("Fetching place name, wait..");
        done.setEnabled(false);
        getGooglePlacesAPI().getPlaceDetailsByLatLng(selectedLocationLatLng, new GooglePlacesAPI.PlaceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, LatLng locationLatLng, String placeID, PlaceDetails placeDetails) {
                done.setEnabled(true);
                if (status == true) {
                    selectedLocationDetails = placeDetails;
                    location.setText(placeDetails.getFullAddress());
                } else {
                    location.setText("Error in Fetching Location, try again!");
                    if (statusCode == -1) {
                        boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
                        if (isInternetAvailable == false) {
                            location.setText("No Internet connection, Unable to load place details , try again!");
                        }
                    }
                }
            }
        });
    }

    //---------

    public void moveCamera(LatLng targetLocationLatLng, int animateTime, boolean adjustAnimateTime) {

        if (adjustAnimateTime == true) {
            boolean contains = map.getProjection().getVisibleRegion().latLngBounds.contains(targetLocationLatLng);      // Check Given Point VIsible in Current View

            if (contains == false) {       // If Not Visible in Current ZoomLevel / View...
                Location currentLocation = new Location("current");
                LatLng curLoc = map.getCameraPosition().target;
                currentLocation.setLatitude(curLoc.latitude);
                currentLocation.setLongitude(curLoc.longitude);

                Location targetLocation = new Location("target");
                targetLocation.setLatitude(targetLocationLatLng.latitude);
                targetLocation.setLongitude(targetLocationLatLng.longitude);

                float distance = currentLocation.distanceTo(targetLocation);
                if (distance < 500) {
                    animateTime = 400;
                } else if (distance < 1000) {
                    animateTime = 800;
                } else if (distance < 5000) {
                    animateTime = 1500;
                } else {
                    animateTime = 2500;
                }
            } else {                       // If Visible in CUrrent ZoomLevel / View....
                animateTime = 200;
            }
        }

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(targetLocationLatLng)      // Sets the center of the map to Mountain View
                .zoom(17)                           // Sets the zoom
                .bearing(0)                         // Sets the orientation of the camera to east
                .tilt(0)                            // Sets the tilt of the camera to 30 degrees
                .build();                           // Creates a CameraPosition from the builder

        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), animateTime, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
            }

            @Override
            public void onCancel() {
            }
        });
    }

    //==========================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == ConstantsCommonLibrary.PLACE_SEARCH_ACTIVITY_CODE && resultCode == RESULT_OK){
                PlaceDetails placeDetails = new Gson().fromJson(data.getStringExtra(ConstantsCommonLibrary.PLACE_DETAILS), PlaceDetails.class);

                if (placeDetails != null) {
                    this.selectedLocationDetails = placeDetails;
                    selectedLocationLatLng = placeDetails.getLatLng();
                    location.setText(placeDetails.getFullAddress());
                    moveCamera(placeDetails.getLatLng(), 1200, false);         // Move & Animate Camera..
                }
            }
        }catch (Exception e){   e.printStackTrace();    }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

}
