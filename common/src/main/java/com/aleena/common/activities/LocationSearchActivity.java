package com.aleena.common.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.aleena.common.R;
import com.aleena.common.adapters.SearchPlacesAdapter;
import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.models.PlaceDetails;
import com.aleena.common.widgets.vEditText;
import com.aleena.common.widgets.vRecyclerView;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Venkatesh on 01/04/17.
 */

public class LocationSearchActivity extends vToolBarActivity implements View.OnClickListener{

    String searchName;

    vEditText location;
    ImageView clearSearch;

    vRecyclerView searchPlacesHolder;
    SearchPlacesAdapter searchPlacesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_search);
        hasLoadingView();
        switchToContentPage();

        //............................................................


        location = (vEditText) findViewById(R.id.location);
        location.setTextChangeListener(new vEditText.TextChangeCallback() {
            @Override
            public void onTextChangeFinish(final String text) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (text != null && text.length() > 0) {
                            getPlacePredictions(text);
                        } else {
                            clearSearch.setVisibility(View.INVISIBLE);
                            searchPlacesAdapter.clearItems();
                        }
                    }
                });
            }
        });
        clearSearch = (ImageView) findViewById(R.id.clearSearch);
        clearSearch.setOnClickListener(this);

        searchPlacesHolder = (vRecyclerView) findViewById(R.id.searchPlacesHolder);
        searchPlacesHolder.setupVerticalOrientation();

        searchPlacesAdapter = new SearchPlacesAdapter(context, new ArrayList<Map<String, String>>(), new SearchPlacesAdapter.CallBack() {
            @Override
            public void onItemClick(int position) {
                fetchPlaceDetailsByIDAndProceed(searchPlacesAdapter.getPlaceDetails(position).get("place_id"), searchPlacesAdapter.getPlaceDetails(position).get("description").toString());
            }
        });
        searchPlacesHolder.setAdapter(searchPlacesAdapter);

    }

    @Override
    public void onClick(View view) {

        if ( view == clearSearch ){
            switchToContentPage();
            location.setText("");
            searchPlacesAdapter.clearItems();
        }
    }

    @Override
    public void onReloadPressed() {
        super.onReloadPressed();
        getPlacePredictions(searchName);
    }

    public void getPlacePredictions(String qName){

        searchName = qName;

        clearSearch.setVisibility(View.VISIBLE);
        searchPlacesAdapter.clearItems();

        showLoadingIndicator("Searching, wait..");
        getGooglePlacesAPI().getPlacePredictions(qName, "17.3850051,78.4845113", 300*1000, new GooglePlacesAPI.PlacePredictionsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String queryName, List<Map<String, String>> placePredictions) {

                if (queryName.equalsIgnoreCase(searchName)) {
                    switchToContentPage();
                    if (status == true) {
                        if (placePredictions.size() > 0) {
                            searchPlacesAdapter.addItems(placePredictions);
                        } else {
                            showNoDataIndicator("Nothing found ");
                        }
                    } else {
                        showReloadIndicator(statusCode, "Unable to retrieve places, try again!");
                    }
                }
            }
        });
    }

    public void fetchPlaceDetailsByIDAndProceed(final String placeID, final String placeFullName){

        showLoadingDialog("Fetching place details, please wait...");
        getGooglePlacesAPI().getPlaceDetailsByPlaceID(placeID, new GooglePlacesAPI.PlaceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, LatLng location, final String placeID, PlaceDetails placeDetails) {
                if (status == true) {
                    if ( placeDetails.getLocality() != null && placeDetails.getState() != null && placeDetails.getCountry() != null && placeDetails.getPostal_code() != null ){
                        closeLoadingDialog();
                        placeDetails.setFullName(placeFullName);
                        onDone(placeDetails);
                    }else{
                        fetchPlaceDetailsByLatLngAndProceed(false, placeDetails.getLatLng(), placeFullName);
                    }
                }
                else {
                    closeLoadingDialog();
                    showSnackbarMessage("Unable to Fetch Place Details!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            fetchPlaceDetailsByIDAndProceed(placeID, placeFullName);
                        }
                    });
                }
            }
        });

    }

    public void fetchPlaceDetailsByLatLngAndProceed(final boolean showLoadingDialog, final LatLng latLng, final String placeFullName){

        if ( showLoadingDialog ){
            showLoadingDialog("Fetching place details, please wait...");
        }
        getGooglePlacesAPI().getPlaceDetailsByLatLng(latLng, new GooglePlacesAPI.PlaceDetailsCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, LatLng location, final String placeID, PlaceDetails placeDetails) {
                if (status == true) {
                    placeDetails.setFullName(placeFullName);
                    onDone(placeDetails);
                } else {
                    closeLoadingDialog();
                    showSnackbarMessage("Unable to Fetch Place Details!", "RETRY", new ActionCallback() {
                        @Override
                        public void onAction() {
                            fetchPlaceDetailsByLatLngAndProceed(true, latLng, placeFullName);
                        }
                    });
                }
            }
        });

    }

    public void onDone(PlaceDetails placeDetails){

        if ( placeDetails != null ) {
            inputMethodManager.hideSoftInputFromWindow(location.getWindowToken(), 0);    // Hides Key Board After Item Select..

            Intent placeData = new Intent();
            placeData.putExtra(ConstantsCommonLibrary.PLACE_DETAILS, new Gson().toJson(placeDetails));
            setResult(RESULT_OK, placeData);
            finish();
        }else{
            showSnackbarMessage("Loaction not available.");
        }
    }

    //==========

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        onBackPressed();
        return true;
    }

}