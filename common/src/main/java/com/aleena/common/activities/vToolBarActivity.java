package com.aleena.common.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aleena.common.R;
import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.dialogs.ChoiceSelectionDialog;
import com.aleena.common.dialogs.CustomDialog;
import com.aleena.common.dialogs.DatePickerDialog;
import com.aleena.common.dialogs.ListChooserDialog;
import com.aleena.common.dialogs.NumberPickerDialog;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.ContactPickerCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.NumberPickerCallback;
import com.aleena.common.interfaces.PermissionsCheckCallback;
import com.aleena.common.interfaces.StatusCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.interfaces.TimePickerCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.location.GoogleRetrofitInterface;
import com.aleena.common.location.HereMapsPlacesAPI;
import com.aleena.common.location.HereMapsPlacesAPI1;
import com.aleena.common.location.HereMapsRetrofitInterface;
import com.aleena.common.location.HereMapsRetrofitInterface1;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.models.WifiDetails;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import retrofit.RestAdapter;


public class vToolBarActivity extends AppCompatActivity {

    public vToolBarActivity activity;
    public Context context;
    public Toolbar toolbar;
    public ActionBar actionBar;
    public Animation fabEnterAnim, fabExitAnim;


    public LayoutInflater inflater;
    public InputMethodManager inputMethodManager;
    public String RS;
    public String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    BroadcastReceiver activityReceiver;
    ViewSwitcher viewSwitcher;
    ProgressBar loading_indicator;
    View reload_indicator;
    TextView loading_message;
    TextView no_data_message;
    SwipeRefreshLayout swipeRefreshLayout;
    SwipeRefreshLayout loadingPageSwipeRefreshLayout;
    HereMapsPlacesAPI hereMapsPlacesAPI;
    HereMapsPlacesAPI1 hereMapsPlacesAPI1;
    AlertDialog loadingDialog;
    AlertDialog notificationDialog;
    ChoiceSelectionDialog choiceSelectionDialog;
    CustomDialog customDialog;
    DatePickerDialog datePickerDialog;
    AlertDialog timePickerDialog;
    NumberPickerDialog numberPickerDialog;
    ListChooserDialog listChooserDialog;
    PermissionsCheckCallback permissionsCheckCallback;
    StatusCallback internetCallback;
    CheckLocatoinAccessCallback locationSettingsCallback;
    ContactPickerCallback contactPickerCallback;
    GoogleApiClient googleApiClient;
    GooglePlacesAPI googlePlacesAPI;
    boolean isAutoRefreshActive;
    Handler autorRefreshHandler;


    //----------

    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    //----------

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        activity = this;
        context = this;
        setupActionBar();

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        // ANIMATIONS

        fabEnterAnim = AnimationUtils.loadAnimation(this, R.anim.fab_enter_anim);
        fabExitAnim = AnimationUtils.loadAnimation(this, R.anim.fab_exit_anim);

        this.RS = getString(R.string.Rs);

        //------

    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    public void startActivityAndFinishPreviousActivities(Intent activityIntentWithoutExtras) {
        ComponentName componentName = activityIntentWithoutExtras.getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
        startActivity(mainIntent);
    }
    //==============  ACTIONBAR METHODS  ==============//

    public GooglePlacesAPI getGooglePlacesAPI() {
        if (googlePlacesAPI == null) {
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("https://maps.googleapis.com/maps/api").setLogLevel(RestAdapter.LogLevel.FULL).build();
            GoogleRetrofitInterface googleRetrofitInterface = restAdapter.create(GoogleRetrofitInterface.class);
            googlePlacesAPI = new GooglePlacesAPI(context.getResources().getString(R.string.google_apikey), googleRetrofitInterface);
        }
        return googlePlacesAPI;
    }

    public HereMapsPlacesAPI getHerePlacesAPI() {
        if (hereMapsPlacesAPI == null) {
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("https://places.cit.api.here.com/").setLogLevel(RestAdapter.LogLevel.FULL).build();
            HereMapsRetrofitInterface googleRetrofitInterface = restAdapter.create(HereMapsRetrofitInterface.class);
            hereMapsPlacesAPI = new HereMapsPlacesAPI(context.getResources().getString(R.string.heremaps_apikey), googleRetrofitInterface);
        }
        return hereMapsPlacesAPI;
    }


    //==============   RECEIVER METHODS    ========//

    public HereMapsPlacesAPI getHereMapsAPI2() {
        if (hereMapsPlacesAPI == null) {
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("https://reverse.geocoder.ls.hereapi.com/6.2/").setLogLevel(RestAdapter.LogLevel.FULL).build();
            HereMapsRetrofitInterface hereMapsRetrofitInterface1 = restAdapter.create(HereMapsRetrofitInterface.class);
            hereMapsPlacesAPI = new HereMapsPlacesAPI(context.getResources().getString(R.string.heremaps_apikey), hereMapsRetrofitInterface1);
        }
        return hereMapsPlacesAPI;
    }

    public HereMapsPlacesAPI1 getHereMapsSearchAPI() {
        if (hereMapsPlacesAPI1 == null) {
            RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("https://geocoder.ls.hereapi.com/6.2/").setLogLevel(RestAdapter.LogLevel.FULL).build();
            HereMapsRetrofitInterface1 hereMapsRetrofitInterface2 = restAdapter.create(HereMapsRetrofitInterface1.class);
            hereMapsPlacesAPI1 = new HereMapsPlacesAPI1(context.getResources().getString(R.string.heremaps_apikey), hereMapsRetrofitInterface2);
        }
        return hereMapsPlacesAPI1;
    }

    protected void setupActionBar() {
        try {
            toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
        }
    }

    //==============  SNACKBAR METHODS  ==============//

    public ActionBar getAppCompactActionBar() {
        return actionBar;
    }

    public void registerActivityReceiver(final String receiverName) {
        try {
            unregisterActivityReceiver();

            IntentFilter intentFilter = new IntentFilter(receiverName);
            activityReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    try {
                        onActivityReceiverMessage(receiverName, intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            this.registerReceiver(activityReceiver, intentFilter);  //registering receiver
        } catch (Exception e) {
        }
    }

    public void unregisterActivityReceiver() {
        try {
            if (activityReceiver != null) {
                this.unregisterReceiver(activityReceiver);
            }
        } catch (Exception e) {
        }
    }

    public void onActivityReceiverMessage(String activityReceiverName, Intent receivedData) {
    }

    public void showSnackbarMessage(String message) {
        showSnackbar(findViewById(R.id.container), message, null, null);
    }

    public void showSnackbarMessage(String message, String actionName, ActionCallback callback) {
        showSnackbar(findViewById(R.id.container), message, actionName, callback);
    }

    //---

    public void showSnackbarMessage(View view, String message, String actionName, ActionCallback callback) {
        showSnackbar(view, message, actionName, callback);
    }

    //----------------------------

    private void showSnackbar(View view, String message, String actionName, final ActionCallback callback) {

        if (view == null) {
            view = findViewById(R.id.container);
        }

        if (view != null) {
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            //TextView snackbar_text = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
            //snackbar_text.setLineSpacing((int)dpFromPx(context, 100), 1);

            if (actionName != null) {
                snackbar.setAction(actionName, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (callback != null) {
                            callback.onAction();
                        }
                    }
                });
            }
            snackbar.show();
        } else {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    //==============  LOADING VIEW METHODS  ==============//

    public void showShortToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void copyToClipBoard(String textToCopy) {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.setText(textToCopy);
    }

    public void hasBackButton() {
        ImageButton backButton = findViewById(R.id.backButton);
        if (backButton != null) {
            backButton.setVisibility(View.VISIBLE);
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
    }

    public void hasLoadingView() {
        hasLoadingView(-1);
    }

    public void hasLoadingView(int textColorResourceID) {
        try {
            viewSwitcher = findViewById(R.id.viewSwitcher);
            loading_indicator = findViewById(R.id.loading_indicator);
            reload_indicator = findViewById(R.id.reload_indicator);
            loading_message = findViewById(R.id.loading_message);
            no_data_message = findViewById(R.id.no_data_message);

            loadingPageSwipeRefreshLayout = findViewById(R.id.loadingPageSwipeRefreshLayout);
            if (loadingPageSwipeRefreshLayout != null) {
                loadingPageSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        loadingPageSwipeRefreshLayout.setRefreshing(false);
                        if (isShowingLoadingPage() == false) {
                            onReloadPressed();
                        }
                    }
                });
            }

            if (textColorResourceID != -1) {
                loading_message.setTextColor(context.getResources().getColor(textColorResourceID));
                no_data_message.setTextColor(context.getResources().getColor(textColorResourceID));
            }
        } catch (Exception e) {
        }
    }

    public void switchToLoadingPage() {
        try {
            viewSwitcher.setDisplayedChild(0);
        } catch (Exception e) {
        }
    }

    public void switchToContentPage() {
        try {
            viewSwitcher.setDisplayedChild(1);
        } catch (Exception e) {
        }
    }

    public void showLoadingIndicator(CharSequence loadiingMessage) {
        try {
            viewSwitcher.setDisplayedChild(0);
            loading_indicator.setVisibility(View.VISIBLE);
            reload_indicator.setVisibility(View.GONE);
            loading_message.setText(loadiingMessage);
            loading_message.setVisibility(View.VISIBLE);
            no_data_message.setVisibility(View.GONE);
        } catch (Exception e) {
        }
    }

    public void showReloadIndicator(int statusCode, String reloadMessage) {
        if (statusCode == -1) {
            boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
            if (isInternetAvailable) {
                reloadMessage = "Not able to connect to Server. \nPlease try again after a while";
            } else {
                reloadMessage = "No Internet connection!";
            }
        }
        showReloadIndicator(reloadMessage);
    }

    public void showReloadIndicator(String reloadMessage) {
        try {
            loading_indicator.setVisibility(View.GONE);
            reload_indicator.setVisibility(View.VISIBLE);
            loading_message.setText(reloadMessage);
            loading_message.setVisibility(View.VISIBLE);
            no_data_message.setVisibility(View.GONE);
            reload_indicator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onReloadPressed();
                }
            });
            viewSwitcher.setDisplayedChild(0);
        } catch (Exception e) {
        }
    }

    public void showNoDataIndicator(CharSequence noDataMessage) {
        try {
            viewSwitcher.setDisplayedChild(0);
            loading_indicator.setVisibility(View.GONE);
            reload_indicator.setVisibility(View.GONE);
            loading_message.setVisibility(View.GONE);
            no_data_message.setVisibility(View.VISIBLE);
            no_data_message.setText(noDataMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isShowingLoadingPage() {
        try {
            if (viewSwitcher.getDisplayedChild() == 0 && loading_indicator.getVisibility() == View.VISIBLE) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean isShowingReloadPage() {
        try {
            if (viewSwitcher.getDisplayedChild() == 0 && reload_indicator.getVisibility() == View.VISIBLE) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean isShowingNoDataPage() {
        try {
            if (viewSwitcher.getDisplayedChild() == 0 && loading_indicator.getVisibility() != View.VISIBLE && reload_indicator.getVisibility() != View.VISIBLE) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    //-------------- Swipe Refresh ----------------//

    public boolean isShowingContentPage() {
        try {
            if (viewSwitcher.getDisplayedChild() == 1) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    //==============  CUSTOM DIALOGS METHODS  ==============//

    public void onReloadPressed() {
    }

    public void hasSwipeRefresh() {
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    swipeRefreshLayout.setRefreshing(false);
                    onReloadPressed();
                }
            });
        }
    }

    // Loading Dialog.
    public void showLoadingDialog(CharSequence title) {
        showLoadingDialog(false, title);
    }

    public void showLoadingDialog(boolean isCancellable, CharSequence title) {
        closeLoadingDialog();
        //if ( loadingDialog == null ){
        loadingDialog = new AlertDialog.Builder(context).create();
        View loadingDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null);
        loadingDialog.setView(loadingDialogView);
        loadingDialog.setCancelable(isCancellable);
        //}

        TextView loadingTitle = loadingDialogView.findViewById(R.id.title);
        loadingTitle.setText(title);

        try {
            loadingDialog.show();
        } catch (Exception e) {
        }
    }

    public void closeLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

    // Custom Dialog..
    public void showCustomDialog(View dialogView) {
        showCustomDialog(dialogView, true);
    }

    public void showCustomDialog(final View dialogView, final boolean isCancellable) {
        showCustomDialog(dialogView, isCancellable, false);
    }

    public void showCustomDialog(final View dialogView, final boolean isCancellable, boolean setTransparentBackground) {
        closeCustomDialog();
        customDialog = new CustomDialog(context);
        customDialog.showDialog(isCancellable, dialogView, setTransparentBackground);
    }

    public void closeCustomDialog() {
        if (customDialog != null) {
            customDialog.closeDialog();
        }
    }

    // Notification Dialog..
    public void showNotificationDialog(String notificationTitle, CharSequence notificationSubtitle) {
        showNotificationDialog(true, notificationTitle, notificationSubtitle, null);
    }

    public void showNotificationDialog(boolean isCancellable, String notificationTitle, CharSequence notificationSubtitle) {
        showNotificationDialog(isCancellable, notificationTitle, notificationSubtitle, null);
    }

    public void showNotificationDialog(String notificationTitle, CharSequence notificationSubtitle, final ActionCallback callback) {
        showNotificationDialog(true, notificationTitle, notificationSubtitle, callback);
    }

    public void showNotificationDialog(boolean isCancellable, String notificationTitle, CharSequence notificationSubtitle, final ActionCallback callback) {
        //if ( notificationDialog == null ){
        notificationDialog = new AlertDialog.Builder(context).create();
        View notificationDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_notifcation, null);
        notificationDialog.setView(notificationDialogView);
        notificationDialog.setCancelable(isCancellable);
        //}
        TextView title = notificationDialogView.findViewById(R.id.title);
        TextView subtitle = notificationDialogView.findViewById(R.id.subtitle);
        title.setText(notificationTitle);
        subtitle.setText(notificationSubtitle);

        notificationDialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notificationDialog != null) {
                    notificationDialog.dismiss();
                    if (callback != null) {
                        callback.onAction();
                    }
                }
            }
        });

        try {
            notificationDialog.show();
        } catch (Exception e) {
        }
    }

    public void closeNotificationDialog() {
        if (notificationDialog != null) {
            notificationDialog.dismiss();
        }
    }

    // Choice Selection Dialog..
    public void showChoiceSelectionDialog(CharSequence title, CharSequence subtitle, String option1, String option2, final ChoiceSelectionCallback choiceSelectionCallback) {
        showChoiceSelectionDialog(true, title, subtitle, option1, option2, choiceSelectionCallback);
    }

    public void showChoiceSelectionDialog(boolean isCancellable, CharSequence title, CharSequence subtitle, String option1, String option2, final ChoiceSelectionCallback choiceSelectionCallback) {
        closeChoiceSelectionDialog();
        choiceSelectionDialog = new ChoiceSelectionDialog(context);
        choiceSelectionDialog.showDialog(isCancellable, title, subtitle, option1, option2, choiceSelectionCallback);
    }

    public void closeChoiceSelectionDialog() {
        if (choiceSelectionDialog != null) {
            choiceSelectionDialog.closeDialog();
        }
    }

    // TextInput Dialog
    public void showInputTextDialog(String dialogTitle, String hintText, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, hintText, "", -1, true, callback);
    }

    public void showInputTextDialog(boolean isCancellable, String dialogTitle, String hintText, boolean hideCancelButton, final TextInputCallback callback) {
        showInputTextDialog(false, dialogTitle, null, hintText, null, -1, true, -1, true, callback);
    }

    public void showInputTextDialog(String dialogTitle, CharSequence dialogMessage, String hintText, String prefillText, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, dialogMessage, hintText, prefillText, -1, true, -1, callback);
    }

    public void showInputNumberDialog(String dialogTitle, String hintText, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, hintText, "", InputType.TYPE_CLASS_NUMBER, true, callback);
    }

    public void showInputNumberDialog(String dialogTitle, String hintText, final String prefillNumber, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, hintText, prefillNumber, InputType.TYPE_CLASS_NUMBER, true, -1, callback);
    }

    public void showInputNumberDialog(String dialogTitle, String hintText, final String prefillNumber, final int limitNoOfCharacters, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, hintText, prefillNumber + "", InputType.TYPE_CLASS_NUMBER, true, limitNoOfCharacters, callback);
    }

    public void showInputNumberDecimalDialog(String dialogTitle, String hintText, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, hintText, "", InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL, true, -1, callback);
    }

    public void showInputNumberDecimalDialog(String dialogTitle, String hintText, final double inputNumber, final TextInputCallback callback) {
        if (inputNumber != 0) {
            showInputTextDialog(dialogTitle, hintText, CommonMethods.getInDecimalFormat(inputNumber), InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED, true, -1, callback);
        } else {
            showInputTextDialog(dialogTitle, hintText, "", InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED, true, -1, callback);
        }
    }

    public void showInputTextDialog(String dialogTitle, String hintText, int inputType, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, hintText, "", inputType, true, -1, callback);
    }

    public void showInputTextDialog(String dialogTitle, final String hintText, String prefillText, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, hintText, prefillText, -1, true, -1, callback);
    }

    public void showInputTextDialog(String dialogTitle, final String hintText, String prefillText, boolean compulsaryInput, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, hintText, prefillText, -1, compulsaryInput, -1, callback);
    }

    public void showInputTextDialog(String dialogTitle, final String hintText, String prefillText, int inputType, final boolean compulsaryInput, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, hintText, prefillText, inputType, compulsaryInput, -1, callback);
    }

    public void showInputTextDialog(String dialogTitle, final String hintText, String prefillText, int inputType, final boolean compulsaryInput, final int limitNoOfCharacters, final TextInputCallback callback) {
        showInputTextDialog(dialogTitle, null, hintText, prefillText, inputType, compulsaryInput, limitNoOfCharacters, callback);
    }

    public void showInputTextDialog(String dialogTitle, final CharSequence dialogMessage, final String hintText, String prefillText, int inputType, final boolean compulsaryInput, final int limitNoOfCharacters, final TextInputCallback callback) {
        showInputTextDialog(true, dialogTitle, dialogMessage, hintText, prefillText, inputType, compulsaryInput, limitNoOfCharacters, false, callback);
    }

    public void showInputTextDialog(boolean isCancellable, String dialogTitle, final CharSequence dialogMessage, final String hintText, String prefillText, int inputType, final boolean compulsaryInput, final int limitNoOfCharacters, final boolean hideCancelButton, final TextInputCallback callback) {
        View inputTextDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_input_text, null);
        TextView title = inputTextDialogView.findViewById(R.id.title);
        TextView message = inputTextDialogView.findViewById(R.id.message);
        final vTextInputLayout input_text = inputTextDialogView.findViewById(R.id.input_text);

        if (dialogTitle != null && dialogTitle.length() > 0) {
            title.setVisibility(View.VISIBLE);
            title.setText(dialogTitle);
        } else {
            title.setVisibility(View.GONE);
        }

        if (dialogMessage != null && dialogMessage.length() > 0) {
            message.setVisibility(View.VISIBLE);
            message.setText(dialogMessage);
        } else {
            message.setVisibility(View.GONE);
        }

        input_text.setHint(hintText);
        input_text.setText(prefillText);
        if (inputType != -1) {
            try {
                input_text.getEditText().setInputType(inputType);
            } catch (Exception e) {
            }
        }
        if (limitNoOfCharacters > 0) {
            input_text.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(limitNoOfCharacters)});
        }
        if (hideCancelButton) {
            inputTextDialogView.findViewById(R.id.cancel).setVisibility(View.GONE);
        }

        inputTextDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input_text.checkError("* Enter " + hintText);
                if (compulsaryInput == false || input_text.getText().length() > 0) {
                    closeCustomDialog();
                    inputMethodManager.hideSoftInputFromWindow(input_text.getWindowToken(), 0);
                    if (callback != null) {
                        callback.onComplete(input_text.getText());
                    }
                }
            }
        });
        inputTextDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeCustomDialog();
            }
        });

        showCustomDialog(inputTextDialogView, isCancellable);
    }

    // Number Picker Dialog..
    public void showNumberPickerDialog(final String displayTitle, final String[] displayValues, final int startValue, final int minValue, final int maxValue, final NumberPickerCallback callback) {
        showNumberPickerDialog(true, displayTitle, displayValues, startValue, minValue, maxValue, callback);
    }

    public void showNumberPickerDialog(final boolean isCancellable, final String displayTitle, final String[] displayValues, final int startValue, final int minValue, final int maxValue, final NumberPickerCallback callback) {
        closeNumberPickerDialog();
        numberPickerDialog = new NumberPickerDialog(context);
        numberPickerDialog.showDialog(isCancellable, displayTitle, displayValues, startValue, minValue, maxValue, callback);
    }

    public void closeNumberPickerDialog() {
        if (numberPickerDialog != null) {
            numberPickerDialog.closeDialog();
        }
    }

    // Date Picker Dialog..
    public void showDatePickerDialog(String dialogTitle, final DatePickerCallback callback) {
        showDatePickerDialog(dialogTitle, null, null, DateMethods.getCurrentDate(), true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String initialDate, final DatePickerCallback callback) {
        showDatePickerDialog(dialogTitle, null, null, initialDate, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String initialDate, boolean showYear, final DatePickerCallback callback) {
        showDatePickerDialog(dialogTitle, null, null, initialDate, showYear, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, final DatePickerCallback callback) {
        showDatePickerDialog(dialogTitle, minimumDate, maximumDate, minimumDate, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, String initialDate, final DatePickerCallback callback) {
        showDatePickerDialog(dialogTitle, minimumDate, maximumDate, initialDate, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, String initialDate, final boolean showYear, final DatePickerCallback callback) {
        closeDatePickerDialog();
        datePickerDialog = new DatePickerDialog(context);
        datePickerDialog.showDatePickerDialog(dialogTitle, minimumDate, maximumDate, initialDate, showYear, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, String initialDate, final boolean showYear, final boolean showDay, final DatePickerCallback callback) {
        closeDatePickerDialog();
        datePickerDialog = new DatePickerDialog(context);
        datePickerDialog.showDatePickerDialog(dialogTitle, minimumDate, maximumDate, initialDate, showYear, showDay, callback);
    }

    public void closeDatePickerDialog() {
        if (datePickerDialog != null) {
            datePickerDialog.closeDialog();
        }
    }

    // Time Picker Dialog..
    public void showTimePickerDialog(String dialogTitle, final TimePickerCallback callback) {
        showTimePickerDialog(dialogTitle, "00:00", "23:59", "00:00", callback);
        //showTimePickerDialog(dialogTitle, 0, 0, 24, 0, 0, 0, callback);
    }

    public void showTimePickerDialog(String dialogTitle, String initialTime, final TimePickerCallback callback) {
        showTimePickerDialog(dialogTitle, "00:00", "23:59", initialTime, callback);
        //showTimePickerDialog(dialogTitle, 0, 0, 24, 0, 0, 0, callback);
    }

    public void showTimePickerDialog(String dialogTitle, String minimumTime, String maximumTime, final TimePickerCallback callback) {
        showTimePickerDialog(dialogTitle, minimumTime, maximumTime, minimumTime, callback);
    }

    public void showTimePickerDialog(String dialogTitle, String minimumTime, String maximumTime, String initialTime, final TimePickerCallback callback) {

        int initHour = 0, initMinutes = 0, minHour = 0, minMinutes = 0, maxHour = 24, maxMinutes = 0;

        Calendar initialTimeCalendar = DateMethods.getCalendarFromDate(initialTime);
        if (initialTimeCalendar != null) {
            initHour = initialTimeCalendar.get(Calendar.HOUR_OF_DAY);
            initMinutes = initialTimeCalendar.get(Calendar.MINUTE);
        }

        Calendar minTimeCalendar = DateMethods.getCalendarFromDate(minimumTime);
        if (minTimeCalendar != null) {
            minHour = minTimeCalendar.get(Calendar.HOUR_OF_DAY);
            minMinutes = minTimeCalendar.get(Calendar.MINUTE);
        }
        Calendar maxTimeCalendar = DateMethods.getCalendarFromDate(maximumTime);
        if (maxTimeCalendar != null) {
            maxHour = maxTimeCalendar.get(Calendar.HOUR_OF_DAY);
            maxMinutes = maxTimeCalendar.get(Calendar.MINUTE);
        }

        showTimePickerDialog(dialogTitle, minHour, minMinutes, maxHour, maxMinutes, initHour, initMinutes, callback);
    }

    //===============================================================

    public void showTimePickerDialog(String dialogTitle, final int minHour, final int minMinutes, final int maxHour, final int maxMinutes, final int initialHour, final int initialMinutes, final TimePickerCallback callback) {

        closeTimePickerDialog();

        Log.d(ConstantsCommonLibrary.LOG_TAG, "minT: " + (minHour + ":" + minMinutes) + " , maxT: " + (maxHour + ":" + maxMinutes) + " , iniT: " + (initialHour + ":" + initialMinutes));

        final String minTime = String.format("%02d", minHour) + ":" + String.format("%02d", minMinutes);
        final String maxTime = String.format("%02d", maxHour) + ":" + String.format("%02d", maxMinutes);
        final String initTime = String.format("%02d", initialHour) + ":" + String.format("%02d", initialMinutes);

        timePickerDialog = new AlertDialog.Builder(context).create();
        View timerPickerDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_time_picker, null);
        timePickerDialog.setView(timerPickerDialogView);
        timePickerDialog.setCancelable(true);

        TextView title = timerPickerDialogView.findViewById(R.id.title);
        if (dialogTitle != null && dialogTitle.length() > 0) {
            title.setText(dialogTitle);
        }

        final TimePicker time_picker = timerPickerDialogView.findViewById(R.id.time_picker);

        time_picker.setCurrentHour(initialHour);
        time_picker.setCurrentMinute(initialMinutes);

        time_picker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                try {
                    final String selectedTime = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute);

                    if (DateMethods.compareDates(minTime, selectedTime) > 0) {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "MIN , min:" + minTime + " with current " + selectedTime);
                        time_picker.setCurrentHour(minHour);
                        time_picker.setCurrentMinute(minMinutes);
                    } else if (DateMethods.compareDates(selectedTime, maxTime) > 0) {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "MAX , max:" + maxTime + " with current" + selectedTime);
                        time_picker.setCurrentHour(maxHour);
                        time_picker.setCurrentMinute(maxMinutes);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        timePickerDialog.show();

        timerPickerDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.dismiss();
                SimpleDateFormat t24HourFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat t12HourFormat = new SimpleDateFormat("hh:mm aa");
                String t24HourTime = String.format("%02d", time_picker.getCurrentHour()) + ":" + String.format("%02d", time_picker.getCurrentMinute());
                String t12HourTime = "";
                try {
                    t12HourTime = t12HourFormat.format(t24HourFormat.parse(t24HourTime));
                } catch (Exception e) {
                    showToastMessage("Ex = " + e);
                }
                callback.onSelect(t24HourTime, t12HourTime, time_picker.getCurrentHour(), time_picker.getCurrentMinute(), 0);
            }
        });

        timerPickerDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.dismiss();
            }
        });
    }

    public void closeTimePickerDialog() {
        if (timePickerDialog != null) {
            timePickerDialog.dismiss();
        }
    }

    public String getDeviceID() {
        String deviceID = "";

        try {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                deviceID = Settings.Secure.getString(
                        context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            } else {
                final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                if (mTelephony.getDeviceId() != null) {
                    deviceID = mTelephony.getDeviceId();
                } else {
                    TelephonyManager telephonyManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
                    String tmDevice = "" + telephonyManager.getDeviceId();
                    String tmSerial = "";// + telephonyManager.getSimSerialNumber();
                    String androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

                    UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

                    deviceID = deviceUuid.toString();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
//        {TODO}    //TODO ?rtp
        return deviceID;
    }

    public String getDeviceName() {
        String deviceName = "";
        String manufacturerName = Build.MANUFACTURER;
        String modelName = Build.MODEL;

        if (manufacturerName != null && manufacturerName.length() > 0) {
            deviceName = manufacturerName.toUpperCase();
            if (modelName != null && modelName.length() > 0) {
                if (modelName.startsWith(manufacturerName)) {
                    deviceName = modelName.toUpperCase();
                } else {
                    deviceName = manufacturerName.toUpperCase() + " " + modelName.toUpperCase();
                }
            }
        }

        return deviceName;
    }

    //==============================================================//

    public int getAppVersionCode() {
        int currentVersion = 0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            currentVersion = pInfo.versionCode;
        } catch (Exception e) {
        }
        return currentVersion;
    }

    public String getAppVersionName() {
        String versionName = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionName = pInfo.versionName;
        } catch (Exception e) {
        }
        return versionName;
    }

    public boolean isWifiConnected() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return wifi.isConnected();
    }

    public WifiDetails getConnectedWifiDetails() {

        WifiDetails wifiDetails = null;

        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();

                wifiDetails = new WifiDetails(wifiInfo.getBSSID(), wifiInfo.getSSID().replace("\"", ""));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return wifiDetails;
    }

    public boolean isMobileDataConnected() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo mobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return mobile.isConnected();
    }

    public boolean isLocationAccessEnabled() {
        boolean status = false;
        LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        try {
            status = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (status == false) {
                status = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    public boolean isGPSEnabled() {
        boolean status = false;
        LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        try {
            status = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    //====

    public void checkLocationAccess(final CheckLocatoinAccessCallback callback) {

        locationSettingsCallback = callback;

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {
                        }
                    })
                    .build();
        }

        if (googleApiClient != null) {

            googleApiClient.connect();

            LocationRequest highAccuracylocationRequest = new LocationRequest();
            highAccuracylocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationRequest balancedPowerlocationRequest = new LocationRequest();
            balancedPowerlocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            LocationSettingsRequest.Builder locationSettingsRequestBuilder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(highAccuracylocationRequest)
                    .addLocationRequest(balancedPowerlocationRequest)
                    .setAlwaysShow(true);

            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationSettingsRequestBuilder.build()).setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {

                    final Status status = result.getStatus();

                    if (status.getStatusCode() == LocationSettingsStatusCodes.SUCCESS) {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : ENALED : All location settings are satisfied. The client can initialize location requests here");
                        locationSettingsCallback.onComplete(true, 200, "Location settings already enabled");
                    } else if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                        try {
                            Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : NOT ENABLED : Showing dialog to enable settings directly.");
                            status.startResolutionForResult(vToolBarActivity.this, ConstantsCommonLibrary.LOCATION_SETTINGS_ACTIVITY_CODE);
                        } catch (Exception e) {
                            Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : NOT ENABLED : Not possible to show dialog, User has to enable manually.");
                            checkLocationAccessManually(callback);
                        }
                    } else if (status.getStatusCode() == LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE) {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : NOT ENABLED : No way to fix the settings so we won't show the dialog.");
                        checkLocationAccessManually(callback);
                    }
                }
            });
        } else {
            checkLocationAccessManually(callback);
        }

    }

    public void checkLocationAccessManually(CheckLocatoinAccessCallback callback) {

        locationSettingsCallback = callback;

        if (isLocationAccessEnabled()) {
            locationSettingsCallback.onComplete(true, 200, "Location settings already enabled");
        } else {
            showChoiceSelectionDialog(true, "Use Location?", "This app wants to change your device settings.\n\nUse GPS, Wi-Fi, and cell networks for location", "CANCEL", "SETTINGS", new ChoiceSelectionCallback() {
                @Override
                public void onSelect(int choiceNo, String choiceName) {
                    closeChoiceSelectionDialog();
                    if (choiceName.equalsIgnoreCase("SETTINGS")) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, ConstantsCommonLibrary.LOCATION_SETTINGS_ACTIVITY_CODE);
                    } else {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : CANCELLED BY USER");
                        locationSettingsCallback.onComplete(false, 500, "Location settings cancelled by user");
                    }
                }
            });
        }

    }

    //---------

    public void getCurrentLocation(final LocationCallback locationCallback) {
        getCurrentLocation(true, locationCallback);
    }


    //============ INTERNET CONNECTION CHECK METHODS  ==============//

    public void getCurrentLocation(final boolean showDialogs, final LocationCallback locationCallback) {

        checkLocationAccess(new CheckLocatoinAccessCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if (status) {
                    if (showDialogs) {
                        showLoadingDialog("Fetching current location, wait...");
                    }
                    new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                        @Override
                        public void onComplete(Location location) {
                            if (showDialogs) {
                                closeLoadingDialog();
                            }
                            if (location != null) {
                                if (locationCallback != null) {
                                    locationCallback.onComplete(true, true, "Location fetched successfully.", location, new LatLng(location.getLatitude(), location.getLongitude()));
                                }
                            } else {
                                if (locationCallback != null) {
                                    locationCallback.onComplete(false, true, "Unable to get current location.", null, null);
                                }
                            }
                        }
                    });
                } else {
                    if (locationCallback != null) {
                        locationCallback.onComplete(false, false, "Location access disabled.", null, null);
                    }
                }
            }
        });
    }

    public void callPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(intent);
    }

    //------ CONTACTS PICKER

    public void checkInternetConnection(final StatusCallback callback) {

        this.internetCallback = callback;

        boolean isInternetAvailable = ((ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;

        if (isInternetAvailable == true) {
            if (internetCallback != null) {
                internetCallback.onComplete(true, 200, "");
            }
        } else {
            if (internetCallback != null) {
                internetCallback.onComplete(false, 500, "");
            }
            showChoiceSelectionDialog(false, "No Internet Connection", "Application needs internet to proceed.", "SETTINGS", "RETRY", new ChoiceSelectionCallback() {
                @Override
                public void onSelect(int choiceNo, String choiceName) {
                    if (choiceName.equalsIgnoreCase("RETRY")) {
                        checkInternetConnection(internetCallback);
                    } else if (choiceName.equalsIgnoreCase("SETTINGS")) {
                        Intent internetSettingsAct = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        startActivityForResult(internetSettingsAct, ConstantsCommonLibrary.INTERNET_ENABLER_ACIIVITY_CODE);
                    }
                }
            });
        }
    }

    //-----

    public boolean isInternetAvailable() {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    public void pickFromContacts(ContactPickerCallback contactPickerCallback) {
        this.contactPickerCallback = contactPickerCallback;

        Intent contactPickerAct = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(contactPickerAct, ConstantsCommonLibrary.CONTACT_PICK_ACTIVITY_CODE);
    }

    //====== PERMISSIONS CHECK

    public void hideSoftKeyBoard(View[] views) {
        if (views != null && views.length > 0) {
            for (int i = 0; i < views.length; i++) {
                hideSoftKeyBoard(views[i]);
            }
        }
    }
    /*public void checkPermissions(boolean isHasAnyNeverAskAgainDeniedPermissions, PermissionsCheckCallback callback) {

        this.permissionsCheckCallback = callback;

        // ONorAfter Android-M
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            try {
                PackageManager packageManager = context.getPackageManager();
                PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS);
                String[] neededPermissions = packageInfo.requestedPermissions;

                List<String> notGrantedPermissions = new ArrayList<>();
                for(int i=0;i<neededPermissions.length;i++){
                    if ( ContextCompat.checkSelfPermission(this, neededPermissions[i]) != PackageManager.PERMISSION_GRANTED ) {
                        notGrantedPermissions.add(neededPermissions[i]);
                    }
                }
                if ( notGrantedPermissions.size() > 0 ) {
                    // user denied atleast on permission flagging """WITH"" NEVER ASK AGAIN
                    if ( isHasAnyNeverAskAgainDeniedPermissions ) {
                        // SHOWING APPLICATION SETTINGS PERMISSIONS PAGE
                        Toast.makeText(context, "Please Enable all permissions to continue.", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, ConstantsCommonLibrary.PERMISSIONS_SETTINGS_ACTIVITY_CODE);
                    }
                    else{
                        requestPermissions(notGrantedPermissions.toArray(new String[notGrantedPermissions.size()]), ConstantsCommonLibrary.PERMISSIONS_SETTINGS_ACTIVITY_CODE);
                    }
                }else{
                    sendPermissionCheckCallback(true, false);
                }

            }catch (Exception e){e.printStackTrace();}
        }
        // On PreAndroid-M all permissions enabled ONINSTALL
        else{
            sendPermissionCheckCallback(true, false);
        }
    }

    public void sendPermissionCheckCallback(boolean status, boolean isHasAnyNeverAskAgainDeniedPermissions){
        if ( permissionsCheckCallback != null ) {
            permissionsCheckCallback.onComplete(status, isHasAnyNeverAskAgainDeniedPermissions);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            int permissionsEnabled = 0;
            for (int i = 0, len = permissions.length; i < len; i++) {
                if ( grantResults[i] ==  PackageManager.PERMISSION_GRANTED ){
                    permissionsEnabled++;
                }
            }

            if ( permissions.length == permissionsEnabled ){
                sendPermissionCheckCallback(true, false);
            }else{
                boolean isHasAnyNeverAskAgainDeniedPermissions = false;
                for ( int i=0;i<permissions.length;i++){
                    // user denied flagging """WITH"" NEVER ASK AGAIN
                    if ( shouldShowRequestPermissionRationale(permissions[i]) == false ) {
                        isHasAnyNeverAskAgainDeniedPermissions = true;
                        break;
                    }
                }
                sendPermissionCheckCallback(false, isHasAnyNeverAskAgainDeniedPermissions);
            }
        }else{
            sendPermissionCheckCallback(true, false);
        }
    }*/

    //---------  AUTO REFRESHER

    public void hideSoftKeyBoard(View view) {
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        }
    }

    public void checkPermissions(PermissionsCheckCallback callback) {
        //checkPermissions(false, callback);
    }

    public void startAutoRefreshTimer(final long autoRefreshIntervalTimeInSeconds) {

        stopAutoRefreshTimer();

        try {
            autorRefreshHandler = new Handler();
            isAutoRefreshActive = true;
            Log.d(ConstantsCommonLibrary.LOG_TAG, "Auto Refresh Timer Started @ " + System.currentTimeMillis() + " in " + context.getClass().getSimpleName());
            autorRefreshHandler.postDelayed(new Runnable() {
                public void run() {
                    if (isAutoRefreshActive) {
                        //Log.d(ConstantsCommonLibrary.LOG_TAG, "Auto Refresh Time Elapsed @ " + System.currentTimeMillis()+" in "+context.getClass().getSimpleName());
                        onAutoRefreshTimeElapsed();
                        autorRefreshHandler.postDelayed(this, (autoRefreshIntervalTimeInSeconds * 1000));
                    }
                }
            }, (autoRefreshIntervalTimeInSeconds * 1000));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopAutoRefreshTimer() {
        if (autorRefreshHandler != null) {
            isAutoRefreshActive = false;
            autorRefreshHandler.removeCallbacksAndMessages(null);
            Log.d(ConstantsCommonLibrary.LOG_TAG, "Auto Refresh Timer Stopped @ " + System.currentTimeMillis() + " in " + context.getClass().getSimpleName());
        }
    }

    public void onAutoRefreshTimeElapsed() {
    }

    //--------

    public void showListChooserDialog(String[] listItems, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(listItems, callback);
    }

    public void showListChooserDialog(List<String> listItems, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(null, listItems, callback);
    }

    public void showListChooserDialog(String dialogTitle, String[] listItems, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(dialogTitle, listItems, callback);
    }

    public void showListChooserDialog(String dialogTitle, List<String> listItems, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(dialogTitle, listItems, callback);
    }

    public void showListChooserDialog(String dialogTitle, List<String> listItems, int highlightItem, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(dialogTitle, listItems, highlightItem, callback);
    }

    public void showListChooserDialog(boolean isCancellable, String dialogTitle, List<String> listItems, int highlightItem, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(isCancellable, dialogTitle, listItems, highlightItem, callback);
    }

    public void closeListChooserDialog() {
        if (listChooserDialog != null) {
            listChooserDialog.close();
        }
    }

    //----

    public void hideSoftKeyboard(View[] views) {
        if (views != null && views.length > 0) {
            for (int i = 0; i < views.length; i++) {
                hideSoftKeyBoard(views[i]);
            }
        }
    }

    public void hideSoftKeyboard(View view) {
        try {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
        } catch (Exception e) {
        }
    }

    //=================================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == ConstantsCommonLibrary.PERMISSIONS_SETTINGS_ACTIVITY_CODE) {
                checkPermissions(permissionsCheckCallback);
            } else if (requestCode == ConstantsCommonLibrary.LOCATION_SETTINGS_ACTIVITY_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : ENALED BY USER");
                    if (locationSettingsCallback != null) {
                        locationSettingsCallback.onComplete(true, 200, "Location settings enabled by user");
                    }
                } else {  //Activity.RESULT_CANCELED
                    Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : CANCELLED BY USER");
                    if (locationSettingsCallback != null) {
                        locationSettingsCallback.onComplete(false, 500, "Location settings cancelled by user");
                    }
                }
            } else if (requestCode == ConstantsCommonLibrary.INTERNET_ENABLER_ACIIVITY_CODE || resultCode == ConstantsCommonLibrary.INTERNET_ENABLER_ACIIVITY_CODE) {
                checkInternetConnection(internetCallback);
            } else if (requestCode == ConstantsCommonLibrary.CONTACT_PICK_ACTIVITY_CODE || resultCode == ConstantsCommonLibrary.CONTACT_PICK_ACTIVITY_CODE) {
                String contactNumber = CommonMethods.getContactNumberFromURI(context, data.getData());
                if ((contactNumber != null && contactNumber.length() > 0) && contactPickerCallback != null) {
                    contactPickerCallback.onComplete(true, "", contactNumber);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onAfterMenuCreated() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterActivityReceiver();

        stopAutoRefreshTimer();

        closeLoadingDialog();
        closeNotificationDialog();
        closeChoiceSelectionDialog();
        closeNumberPickerDialog();
        closeDatePickerDialog();
        closeTimePickerDialog();
        closeCustomDialog();
        closeListChooserDialog();

    }
}
