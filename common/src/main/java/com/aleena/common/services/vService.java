package com.aleena.common.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;

import android.util.Log;

import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.models.WifiDetails;


public class vService extends Service {

    //public vService service;

    BroadcastReceiver serviceReceiver;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //==============   RECEIVER METHODS    ========//

    public void registerServiceReceiver(final String receiverName) {
        try{
            unregisterServiceReceiver();

            IntentFilter intentFilter = new IntentFilter(receiverName);
            serviceReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    try {
                        onServiceReceiverMessage(receiverName, intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            this.registerReceiver(serviceReceiver, intentFilter);  //registering receiver
        }catch (Exception e){}
    }
    public void unregisterServiceReceiver(){
        try{
            if ( serviceReceiver != null) {
                getApplicationContext().unregisterReceiver(serviceReceiver);
            }
        }catch (Exception e){}
    }

    public void onServiceReceiverMessage(String serviceReceiverName, Intent receivedData) {}


    //---------  AUTO REFRESHER

    boolean isAutoRefreshActive;
    Handler autorRefreshHandler;

    public void startAutoRefreshTimer(final long autoRefreshIntervalTimeInSeconds) {

        stopAutoRefreshTimer();

        try {
            autorRefreshHandler = new Handler();
            isAutoRefreshActive = true;
            Log.d(ConstantsCommonLibrary.LOG_TAG, "Auto Refresh Timer Started @ " + System.currentTimeMillis() );//+ " in " + getApplicationContext().getClass().getSimpleName());
            autorRefreshHandler.postDelayed(new Runnable() {
                public void run() {
                    if (isAutoRefreshActive) {
                        //Log.d(ConstantsCommonLibrary.LOG_TAG, "Auto Refresh Time Elapsed @ " + System.currentTimeMillis());//+" in "+context.getClass().getSimpleName());
                        onAutoRefreshTimeElapsed();
                        autorRefreshHandler.postDelayed(this, (autoRefreshIntervalTimeInSeconds * 1000));
                    }
                }
            }, (autoRefreshIntervalTimeInSeconds * 1000));
        }catch (Exception e){e.printStackTrace();}
    }

    public void stopAutoRefreshTimer(){
        if ( autorRefreshHandler != null ){
            isAutoRefreshActive = false;
            autorRefreshHandler.removeCallbacksAndMessages(null);
            try{Log.d(ConstantsCommonLibrary.LOG_TAG, "Auto Refresh Timer Stopped @ " + System.currentTimeMillis() + " in " + getApplicationContext().getClass().getSimpleName());}catch (Exception e){}
        }
    }

    public void onAutoRefreshTimeElapsed(){}

    //-----------

    public WifiDetails getConnectedWifiDetails(){

        WifiDetails wifiDetails = null;

        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();

                wifiDetails = new WifiDetails(wifiInfo.getBSSID(), wifiInfo.getSSID().replace("\"", ""));
            }
        }catch (Exception e){e.printStackTrace();}

        return wifiDetails;
    }

    public boolean isMobileDataConnected(){
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo mobile = connManager .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mobile.isConnected()) {
            return true;
        }
        return false;
    }

    //===============================================================

    public String getDeviceID() {
        String deviceID = "";
        /*try {
            TelephonyManager telephonyManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

            String tmDevice = "" + telephonyManager.getDeviceId();
            String tmSerial = "";// + telephonyManager.getSimSerialNumber();
            String androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

            deviceID = deviceUuid.toString();
        }catch (Exception e){}*/ //TODO ?rtp

        return deviceID;
    }

    public String getDeviceName(){
        String deviceName = "";
        String manufacturerName = Build.MANUFACTURER;
        String modelName = Build.MODEL;

        if ( manufacturerName != null && manufacturerName.length() > 0 ){
            deviceName = manufacturerName.toUpperCase();
            if ( modelName != null && modelName.length() > 0 ){
                if ( modelName.startsWith(manufacturerName) ){
                    deviceName = modelName.toUpperCase();
                }else{
                    deviceName = manufacturerName.toUpperCase()+" "+modelName.toUpperCase();
                }
            }
        }

        return deviceName;
    }

    public int getAppVersionCode() {
        int currentVersion = 0;
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            currentVersion = pInfo.versionCode;
        } catch (Exception e) {
        }
        return currentVersion;
    }

    public String getAppVersionName() {
        String versionName = "";
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0);
            versionName = pInfo.versionName;
        } catch (Exception e) {
        }
        return versionName;
    }

    //=================================

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterServiceReceiver();

        stopAutoRefreshTimer();
    }
}
