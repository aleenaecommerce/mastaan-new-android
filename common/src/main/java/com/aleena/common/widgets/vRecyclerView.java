package com.aleena.common.widgets;

import android.content.Context;

import android.util.AttributeSet;
import android.util.Log;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.aleena.common.constants.ConstantsCommonLibrary;


/**
 * Created by venkatesh on 18/6/15.
 */

public class vRecyclerView extends RecyclerView {

    Context context;
    RecyclerView recyclerView;
    ScrollListener scrollListener;

    public interface ScrollListener {
        void onScrolled(boolean isScrollEnd);
    }

    public vRecyclerView(Context context) {
        super(context);
        this.context = context;
        recyclerView = this;
    }

    public vRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        recyclerView = this;
    }

    public vRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        recyclerView = this;
    }

    public void setScrollListener(ScrollListener scroll_Listener){
        this.scrollListener = scroll_Listener;

        this.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if ( newState == RecyclerView.SCROLL_STATE_SETTLING ){
                    Log.d(ConstantsCommonLibrary.LOG_TAG, "ScrollEND");
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if ( scrollListener != null ) {
                    long thresold = 1;
                    if ( recyclerView.getAdapter().getItemCount() > 2 ){ thresold = 2;  }
                    /*if ( recyclerView.getAdapter().getItemCount() != 0 ) {
                        int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                        if ( lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - thresold ) {
                            scrollListener.onScrollEnd();
                        }
                    }*/
                    long visibleItemCount = recyclerView.getChildCount();
                    long totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    long firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                    if ( totalItemCount - visibleItemCount <= firstVisibleItem + thresold ) {
                        scrollListener.onScrolled(true);
                    }else{
                        scrollListener.onScrolled(false);
                    }
                }
            }
        });

    }

    public boolean isLastItemDisplaying() {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1) {
                return true;
            }
        }
        return false;
    }

    public void setupVerticalOrientation(){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void setupHorizontalOrientation(){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void setupVerticalGridOrientation(int columnsCount){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(context, columnsCount, GridLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void setupHorizontalGridOrientation(int rowsCount){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(context, rowsCount, GridLayoutManager.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void setupVerticalStaggeredGridOrientation(int columnsCount){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(columnsCount, GridLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void setupHorizontalStaggeredGridOrientation(int rowsCount){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(rowsCount, GridLayoutManager.HORIZONTAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public int getFirstCompletelyVisibleItemPosition() {
        try {
            return ((GridLayoutManager) getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        } catch (Exception e) {}
        return 0;
    }

    public int getLastCompletelyVisibleItemPosition() {
        try {
            return ((GridLayoutManager) getLayoutManager()).findLastCompletelyVisibleItemPosition();
        } catch (Exception e) {}
        return 0;
    }

    public void scrollToPosition(int position){
        if ( position >= 0 ) {
            try {
                getLayoutManager().scrollToPosition(position);
            } catch (Exception e) {}
        }
    }
}
