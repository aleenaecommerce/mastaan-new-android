package com.aleena.common.widgets;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.SupportMapFragment;

public class vMapFragment extends SupportMapFragment {

    private View mOriginalView;
    private vMapWrapperLayout mVMapWrapperLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mOriginalView = super.onCreateView(inflater, container, savedInstanceState);

        mVMapWrapperLayout = new vMapWrapperLayout(getActivity());
        mVMapWrapperLayout.addView(mOriginalView);

        return mVMapWrapperLayout;
    }

    @Override
    public View getView() {
        return mOriginalView;
    }

    public void setOnDragListener(vMapWrapperLayout.OnDragListener onDragListener) {
        mVMapWrapperLayout.setOnDragListener(onDragListener);
    }
}
