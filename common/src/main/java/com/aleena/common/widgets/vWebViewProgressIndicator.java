package com.aleena.common.widgets;

import android.webkit.WebChromeClient;
import android.webkit.WebView;

/**
 * Created by venkatesh on 27/8/15.
 */

public class vWebViewProgressIndicator extends WebChromeClient {

    CallBack callback;

    public interface CallBack {
        void onLoading(int progress);
    }

    public vWebViewProgressIndicator(CallBack callback){
        this.callback = callback;
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        super.onProgressChanged(view, newProgress);

        callback.onLoading(newProgress);
    }
}
