package com.aleena.common.widgets.roboto;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by venkatesh on 29/7/15.
 */

public class RobotoCondensedEdittext extends EditText {

    Typeface robotoRegularFont;

    public RobotoCondensedEdittext(Context context) {
        super(context);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoCondensedEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoCondensedEdittext(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");
        this.setTypeface(robotoRegularFont);
    }
}
