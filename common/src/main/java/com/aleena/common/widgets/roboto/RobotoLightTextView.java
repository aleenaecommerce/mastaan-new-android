package com.aleena.common.widgets.roboto;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by venkatesh on 29/7/15.
 */

public class RobotoLightTextView extends TextView {

    Typeface robotoRegularFont;

    public RobotoLightTextView(Context context) {
        super(context);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoLightTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }
}
