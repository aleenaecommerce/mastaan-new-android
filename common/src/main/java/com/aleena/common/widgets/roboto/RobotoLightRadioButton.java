package com.aleena.common.widgets.roboto;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by venkatesh on 29/7/15.
 */

public class RobotoLightRadioButton extends RadioButton {

    Typeface robotoRegularFont;

    public RobotoLightRadioButton(Context context) {
        super(context);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoLightRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }
}
