package com.aleena.common.widgets.roboto;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by venkatesh on 4/11/15.
 */
public class RobotoLightButton extends Button {

    Typeface robotoRegularFont;

    public RobotoLightButton(Context context) {
        super(context);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoLightButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoLightButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }
}
