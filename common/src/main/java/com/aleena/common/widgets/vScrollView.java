package com.aleena.common.widgets;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

/**
 * Created by venkatesh on 18/6/15.
 */
public class vScrollView extends ScrollView {

    ScrollView scrollView;
    private ScrollViewListener scrollViewListener = null;
    boolean isScrollEnded;

    public interface ScrollViewListener {
        void onScrollChange(vScrollView scrollView, boolean isScrollEnd, int x, int y, int oldx, int oldy);
    }

    public vScrollView(Context context) {
        super(context);
        scrollView = this;
    }

    public vScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        scrollView = this;
    }

    public vScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        scrollView = this;
    }

    public void setScrollViewListener(ScrollViewListener scrollViewListener) {
        this.scrollViewListener = scrollViewListener;
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        View view = (View) getChildAt(getChildCount() - 1);
        int diff = (view.getBottom() - (getHeight() + getScrollY()));
        if ( diff == 0 ) { // if diff is zero, then the bottom has been reached
            isScrollEnded = true;
        }else{  isScrollEnded = false;  }

        if (scrollViewListener != null) {
            scrollViewListener.onScrollChange(this, isScrollEnded, x, y, oldx, oldy);
        }
    }

    /*
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        switch (action)
        {
            case MotionEvent.ACTION_DOWN:
                    Log.i("VerticalScrollview", "onInterceptTouchEvent: DOWN super false" );
                    super.onTouchEvent(ev);
                    break;

            case MotionEvent.ACTION_MOVE:
                    return false; // redirect MotionEvents to ourself

            case MotionEvent.ACTION_CANCEL:
                    Log.i("VerticalScrollview", "onInterceptTouchEvent: CANCEL super false" );
                    super.onTouchEvent(ev);
                    break;

            case MotionEvent.ACTION_UP:
                    Log.i("VerticalScrollview", "onInterceptTouchEvent: UP super false" );
                    return false;

            default: Log.i("VerticalScrollview", "onInterceptTouchEvent: " + action ); break;
        }

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        super.onTouchEvent(ev);
        Log.i("VerticalScrollview", "onTouchEvent. action: " + ev.getAction() );
         return true;
    }
     */

    public boolean isScrollEnded() {
        if ( isScrollable() == false || isScrollEnded ){
            return true;
        }
        return false;
    }

    public void focusToViewTop(final View view){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, view.getTop());
            }
        });
    }
    public void focusToViewBottom(final View view){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, view.getBottom());
            }
        });
    }

    public void scrollToEnd(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }
    public void scrollToTop(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_UP);
            }
        });
    }

    public boolean isScrollable(){
        View child = getChildAt(0);
        if (child != null) {
            int childHeight = child.getHeight();
            //Toast.makeText(getContext(), "cC = "+childHeight+" sH="+scrollView.getHeight(), Toast.LENGTH_LONG).show();
            return getHeight() < (childHeight + scrollView.getPaddingTop() + scrollView.getPaddingBottom());
        }
        return false;
    }

}
