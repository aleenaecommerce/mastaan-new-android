package com.aleena.common.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.aleena.common.R;
import com.aleena.common.constants.ConstantsCommonLibrary;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by venkatesh on 23/9/15.
 */

public class vCollageImageView extends vFlowLayout {

    int defaultImageResourceID;

    Context context;
    vCollageImageView vCollageImageView;
    int viewWidth = -1;
    int viewHeight = -1;

    ArrayList<Integer> appImages;
    ArrayList<String> localImages;
    ArrayList<String> imageURLs;

    public vCollageImageView(Context context) {
        super(context);
        this.context = context;
        vCollageImageView = this;
        defaultImageResourceID = R.drawable.image_default;
    }

    public vCollageImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        vCollageImageView = this;
        defaultImageResourceID = R.drawable.image_default;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        if ( viewWidth == -1 && viewHeight == -1 ){
            viewWidth = getWidth();
            viewHeight = getHeight();
            Log.d(ConstantsCommonLibrary.LOG_TAG, "ViewWidth = " + viewWidth + ", viewHeight = " + viewHeight);

            if ( appImages != null ){
                int imagesCount = appImages.size();
                Map<String, Integer> bestRowsColumns = getBestRowsColumns(imagesCount);
                int itemWidth = viewWidth/Integer.parseInt(bestRowsColumns.get("columns").toString());
                int itemHeight = viewHeight/Integer.parseInt(bestRowsColumns.get("rows").toString());
                Log.d(ConstantsCommonLibrary.LOG_TAG, "Items(aIs): " + appImages.size() + "  ==>  BEST itemWidth = " + itemWidth + ", itemHeight = " + itemHeight);

                for (int i=0;i< appImages.size();i++){
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View collageView = inflater.inflate(R.layout.view_collage_image, null, false);
                    //collageView.setRotation(-30);
                    ImageView image = (ImageView) collageView.findViewById(R.id.image);
                    Picasso.get()
                            .load(appImages.get(i))
                            .placeholder(defaultImageResourceID)
                            .error(defaultImageResourceID)
                            .fit()
                            .centerCrop()
                            .into(image);

                    LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(itemWidth-1,itemHeight);
                    if ( imagesCount % 2 == 1 && i == 0 ){
                        parms = new LinearLayout.LayoutParams((itemWidth-1)*2,itemHeight);
                    }
                    image.setLayoutParams(parms);

                    vCollageImageView.addView(collageView);
                }
            }
            else if ( localImages != null ){
                int imagesCount = localImages.size();
                Map<String, Integer> bestRowsColumns = getBestRowsColumns(imagesCount);
                int itemWidth = viewWidth/Integer.parseInt(bestRowsColumns.get("columns").toString());
                int itemHeight = viewHeight/Integer.parseInt(bestRowsColumns.get("rows").toString());
                Log.d(ConstantsCommonLibrary.LOG_TAG, "Items(lIs): " + imagesCount + "  ==>  BEST itemWidth = " + itemWidth + ", itemHeight = " + itemHeight);

                for (int i=0;i< localImages.size();i++){
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View collageView = inflater.inflate(R.layout.view_collage_image, null, false);
                    //collageView.setRotation(-30);
                    ImageView image = (ImageView) collageView.findViewById(R.id.image);
                    Picasso.get()
                            .load(new File(localImages.get(i)))
                            .placeholder(defaultImageResourceID)
                            .error(defaultImageResourceID)
                            .fit()
                            .centerCrop()
                            .into(image);

                    LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(itemWidth-1,itemHeight);
                    if ( imagesCount % 2 == 1 && i == 0 ){
                        parms = new LinearLayout.LayoutParams((itemWidth-1)*2,itemHeight);
                    }
                    image.setLayoutParams(parms);

                    vCollageImageView.addView(collageView);
                }
            }
            else if ( imageURLs != null ){
                int imagesCount = imageURLs.size();
                Map<String, Integer> bestRowsColumns = getBestRowsColumns(imageURLs.size());
                int itemWidth = viewWidth/Integer.parseInt(bestRowsColumns.get("columns").toString());
                int itemHeight = viewHeight/Integer.parseInt(bestRowsColumns.get("rows").toString());
                Log.d(ConstantsCommonLibrary.LOG_TAG, "Items(iUs): " + imagesCount + "  ==>  BEST itemWidth = " + itemWidth + ", itemHeight = " + itemHeight);

                for (int i=0;i<imageURLs.size();i++){
                    Log.d(ConstantsCommonLibrary.LOG_TAG, "Loading Image-"+i+" : "+imageURLs.get(i));
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View collageView = inflater.inflate(R.layout.view_collage_image, null, false);
                    ImageView image = (ImageView) collageView.findViewById(R.id.image);
                    Picasso.get()
                            .load(imageURLs.get(i))
                            .placeholder(defaultImageResourceID)
                            .error(defaultImageResourceID)
                            .fit()
                            .centerCrop()
                            .tag(context)
                            .into(image);

                    LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(itemWidth-1,itemHeight);
                    if ( imagesCount % 2 == 1 && i == 0 ){
                        parms = new LinearLayout.LayoutParams((itemWidth-1)*2,itemHeight);
                    }
                    image.setLayoutParams(parms);

                    vCollageImageView.addView(collageView);
                }
                Log.d(ConstantsCommonLibrary.LOG_TAG, "Added Images "+vCollageImageView.getChildCount());
            }
            else{
                Log.d(ConstantsCommonLibrary.LOG_TAG, "No Images");
            }
        }

    }

    private Map<String, Integer> getItemWidthHeight(int holderWidth, int holderHeight, int rows, int columns){

        int width = 0, height = 0;

        width = holderWidth/rows;
        height = holderHeight/columns;

        Map<String, Integer> bestItemWidhtHeight = new HashMap<>();
        bestItemWidhtHeight.put("width", width);
        bestItemWidhtHeight.put("height", height);
        return bestItemWidhtHeight;
    }

    private Map<String, Integer> getBestRowsColumns(int count){

        if ( count % 2 == 1 ){  count++;    }
        int rows = -1, columns = -1;

        for (int r=1;r<count;r++){
            for (int c=1;c<=count;c++){
                if ( r * c == count ){
                    //Log.d(ConstantsCommonLibrary.LOG_TAG, "items: " + count + "  ==>  rows = " + r + ", col = " + c);
                    if ( (rows == -1 && columns == -1) || ( viewHeight >= viewWidth && r >= c )  || ( viewWidth >= viewHeight && r <= c )){
                        rows = r;
                        columns = c;
                    }
                }
            }
        }
        Log.d(ConstantsCommonLibrary.LOG_TAG,  "Items: " + count + "  ==>  BEST ROWS = " + rows + ", COLUMNS = " + columns);

        Map<String, Integer> bestRowsColumns = new HashMap<>();
        bestRowsColumns.put("rows", rows);
        bestRowsColumns.put("columns", columns);
        return bestRowsColumns;

    }

    public int getViewWidth() {
        return viewWidth;
    }

    public int getViewHeight() {
        return viewHeight;
    }

    public void addAppImages(ArrayList<Integer> appImages){
        if ( appImages == null ){   appImages = new ArrayList<>();  }
        this.appImages = appImages;
    }

    public ArrayList<Integer> getAppImages() {
        return appImages;
    }

    public void addLocalImages(ArrayList<String> localImages) {
        if ( localImages == null ){   localImages = new ArrayList<>();  }
        this.localImages = localImages;
    }

    public ArrayList<String> getLocalImages() {
        return localImages;
    }

    public void setDefaultImage(int defaultImageResourceID){
        this.defaultImageResourceID = defaultImageResourceID;
    }

    public void addImageURLs(ArrayList<String> imageURLs) {
        if ( imageURLs == null ){   imageURLs = new ArrayList<>();  }
        this.imageURLs = imageURLs;
    }

    public ArrayList<String> getImageURLs() {
        return imageURLs;
    }


}
