package com.aleena.common.widgets;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.aleena.common.interfaces.SoftKeyBoardActionsCallback;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class vTextInputLayout extends TextInputLayout {

    TextInputLayout textInputLayout;
    boolean detectTextChange = true;

    boolean showingError;
    String errorMessage;

    Date lastTypedTime;
    Timer textWatchTimer = new Timer();

    SoftKeyBoardActionsCallback softKeyBoardActionsCallback;

    public vTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        textInputLayout = this;
    }

    public vTextInputLayout(Context context) {
        super(context);
        textInputLayout = this;
    }

    public String getText(){
        return textInputLayout.getEditText().getText().toString().trim();
    }

    public void checkError(String errorMessage){
        if ( getText().trim().length() > 0){
            hideError();
        }else{
            this.errorMessage = errorMessage;
            textInputLayout.getEditText().setText("");
            showError(errorMessage);
        }
    }

    public void showError(String errorMessage){
        showingError = true;
        textInputLayout.setErrorEnabled(true);
        textInputLayout.setError(errorMessage);
    }

    public void hideError(){
        showingError = false;
        textInputLayout.setError(null);
        textInputLayout.setErrorEnabled(false);
    }


    public void setTextChangeListener(final vTextInputLayout.Callback callback) {

        textInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                lastTypedTime = new Date();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (textWatchTimer != null) {
                    textWatchTimer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (detectTextChange == true) {
                    textWatchTimer = new Timer();
                    textWatchTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            Date runTime = new Date();
                            if (lastTypedTime.getTime() + 180 <= runTime.getTime()) {   // If Typing Finished..
                                //Log.d("customTextListener", "Typing Finished : " + editText.getText());
                                callback.onTextChangeFinish(getText());
                            }
                        }
                    }, 800);
                }
            }
        });
    }

    public void setSoftKeyBoardListener(SoftKeyBoardActionsCallback callback){
        this.softKeyBoardActionsCallback = callback;

        textInputLayout.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    softKeyBoardActionsCallback.onDonePressed(textInputLayout.getEditText().getText().toString());
                }
                return false;
            }
        });
    }

    public void setText(String text){
        textInputLayout.getEditText().setText(text);
    }

    public void changeText(final String text) {
        detectTextChange = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    textInputLayout.getEditText().setText(text);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    detectTextChange = true;
                }
            }
        }).start();
    }

    public interface Callback {
        void onTextChangeFinish(String text);
    }

}
