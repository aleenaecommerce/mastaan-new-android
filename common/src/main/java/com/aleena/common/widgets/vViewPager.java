package com.aleena.common.widgets;

/**
 * Created by venkatesh on 6/11/15.
 * For Purpose : ViewPager Without Adapter
 */

import android.content.Context;

import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;


public class vViewPager extends ViewPager {

    public vViewPager(final Context context) {
        super(context);
    }

    public vViewPager(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        // Make sure all are loaded at once
        final int childrenCount = getChildCount();
        setOffscreenPageLimit(childrenCount - 1);

        // Attach the adapter
        setAdapter(new PagerAdapter() {

            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {
                return container.getChildAt(position);
            }

            @Override
            public boolean isViewFromObject(final View arg0, final Object arg1) {
                return arg0 == arg1;

            }

            @Override
            public int getCount() {
                return childrenCount;
            }

            @Override
            public void destroyItem(final View container, final int position, final Object object) {
            }
        });
    }

    public Fragment getShowingFragment(FragmentManager fragmentManager){
        Fragment fragment = (Fragment) fragmentManager.findFragmentByTag("android:switcher:" + this.getId() + ":" + this.getCurrentItem());
        return fragment;
    }

}

