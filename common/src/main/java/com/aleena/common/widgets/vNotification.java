package com.aleena.common.widgets;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import android.view.View;
import android.widget.RemoteViews;

import com.aleena.common.R;


/**
 * Created by venkatesh on 29/6/15.
 */
public class vNotification {

    Context context;
    RemoteViews customView;

    NotificationCompat.Builder notification;

    public vNotification(Context context) {
        this.context = context;
        notification = new NotificationCompat.Builder(context);

    }
    public vNotification(Context context, String title, String message){
        this.context = context;
        setUpNotification(-1, title, message);
    }

    public vNotification(Context context, int imageResourceID, String title, String message){
        this.context = context;
        setUpNotification(imageResourceID, title, message);
    }

    private void setUpNotification(int imageResourceID, String title, String message) {
        notification = new NotificationCompat.Builder(context);
        try {
            if (imageResourceID != -1) {
                notification.setSmallIcon(imageResourceID);
            }else {
                notification.setSmallIcon(R.mipmap.ic_launcher);
            }
        }catch (Exception e){}
        notification.setContentTitle(title);
        notification.setContentText(message);
        notification.setAutoCancel(true);
        notification.setDefaults(Notification.DEFAULT_ALL);

        androidx.core.app.NotificationCompat.InboxStyle inboxStyle = new androidx.core.app.NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(title);
        inboxStyle.addLine(message);
        notification.setStyle(inboxStyle);
    }

    public void setStackBuilder(TaskStackBuilder taskStackBuilder){
        PendingIntent resultPendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);    // Gets a PendingIntent containing the entire back stack
        notification.setContentIntent(resultPendingIntent);
    }

    public void makePinnedNotification(){
        notification.setAutoCancel(false);
//        notification.flags |= Notification.FLAG_NO_CLEAR; //Do not clear the notification
//        notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
//        notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
//        notification.defaults |= Notification.DEFAULT_SOUND; // Sound
    }

    public void setCustomView(View view) {
        customView = new RemoteViews(context.getPackageName(), view.getId());
        notification.setContent(customView);
    }

    public void setLaunchActivity(Intent launchActivity){

        // For Back to CUrrent Activity
        launchActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, launchActivity, PendingIntent.FLAG_UPDATE_CURRENT);//FLAG_CANCEL_CURRENT);
        notification.setContentIntent(resultPendingIntent);


        /*TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        //stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(launchActivity);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, Intent.FLAG_ACTIVITY_NEW_TASK);
        notification.setContentIntent(resultPendingIntent);*/
    }

    public void makePinned(){
        notification.build().priority = android.app.Notification.PRIORITY_HIGH;
        notification.build().flags |= Notification.FLAG_NO_CLEAR; //Do not clear the notification
    }

    public void disableDefaults(){
        //notification.setDefaults(Notification.)
    }

    public RemoteViews setBigView(int bigView){
        RemoteViews remoteView = new RemoteViews(context.getPackageName(), bigView);
        return remoteView;
    }

    public void show(){
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, notification.build());
    }
    public void show(int notificationIndex){
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notificationIndex, notification.build());
    }

    /*

    Notification notification = new Notification(R.drawable.ic_launcher, "Pinned Notification", System.currentTimeMillis());
            NotificationManager mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.pinned_notification);
            //        contentView.setImageViewResource(R.id.image, R.drawable.ic_launcher);
            //        contentView.setTextViewText(R.id.title, "Custom notification");
            //        contentView.setTextViewText(R.id.text, "This is a custom layout");


            contentView.setTextViewText(R.id.message, Html.fromHtml(displayMessage));

            notification.contentView = contentView;

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationClickAct, 0);
            notification.contentIntent = contentIntent;

            notification.flags |= Notification.FLAG_NO_CLEAR; //Do not clear the notification
            //notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
            //notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
            //notification.defaults |= Notification.DEFAULT_SOUND; // Sound

            mNotificationManager.notify(pinnedNotificationIndex, notification);

     */
}