package com.aleena.common.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Switch;

/**
 * Created in vCommonLib on 07/11/18.
 * @author Venkatesh Uppu (c)
 */

public class vSwitch extends Switch {

    OnCheckedChangeListener onCheckedChangeListener;

    public vSwitch(Context context) {
        super(context);
    }

    public vSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public vSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
        super.setOnCheckedChangeListener(onCheckedChangeListener);
        this.onCheckedChangeListener = onCheckedChangeListener;
    }

    public void setCheckedWithoutCallback(boolean isChecked){
        setCheckedWithoutCallback(isChecked, onCheckedChangeListener);
    }
    public void setCheckedWithoutCallback(boolean isChecked, OnCheckedChangeListener onCheckedChangeListener){
        this.setOnCheckedChangeListener(null);
        this.setChecked(isChecked);
        this.setOnCheckedChangeListener(onCheckedChangeListener);
    }

}