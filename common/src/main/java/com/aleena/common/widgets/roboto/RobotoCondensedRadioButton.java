package com.aleena.common.widgets.roboto;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by venkatesh on 1/7/15.
 */

public class RobotoCondensedRadioButton extends RadioButton {

    Typeface robotoRegularFont;

    public RobotoCondensedRadioButton(Context context) {
        super(context);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoCondensedRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoCondensedRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");
        this.setTypeface(robotoRegularFont);
    }
}
