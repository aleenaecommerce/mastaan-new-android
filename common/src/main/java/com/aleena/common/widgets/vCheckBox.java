package com.aleena.common.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

/**
 * Created by Venkatesh Uppu on 09/03/19.
 */

public class vCheckBox extends CheckBox {

    OnCheckedChangeListener onCheckedChangeListener;

    public vCheckBox(Context context) {
        super(context);
    }

    public vCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public vCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
        super.setOnCheckedChangeListener(onCheckedChangeListener);
        this.onCheckedChangeListener = onCheckedChangeListener;
    }

    public void setCheckedWithoutCallback(boolean isChecked){
        setCheckedWithoutCallback(isChecked, onCheckedChangeListener);
    }
    public void setCheckedWithoutCallback(boolean isChecked, OnCheckedChangeListener onCheckedChangeListener){
        this.setOnCheckedChangeListener(null);
        this.setChecked(isChecked);
        this.setOnCheckedChangeListener(onCheckedChangeListener);
    }

}