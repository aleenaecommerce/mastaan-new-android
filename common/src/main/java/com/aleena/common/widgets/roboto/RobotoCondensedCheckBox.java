package com.aleena.common.widgets.roboto;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.aleena.common.widgets.vCheckBox;

/**
 * Created by venkatesh on 1/7/15.
 */

public class RobotoCondensedCheckBox extends vCheckBox {

    Typeface robotoRegularFont;

    public RobotoCondensedCheckBox(Context context) {
        super(context);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoCondensedCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoCondensedCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Condensed.ttf");
        this.setTypeface(robotoRegularFont);
    }
}
