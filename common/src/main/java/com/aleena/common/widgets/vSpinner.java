package com.aleena.common.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

/**
 * Created by venkatesh on 2/11/15.
 */
public class vSpinner extends Spinner {

    private OnSpinnerEventsListener mListener;
    private boolean mOpenInitiated = false;

    public vSpinner(Context context) {
        super(context);
    }

    public vSpinner(Context context, int mode) {
        super(context, mode);
    }

    public vSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public interface OnSpinnerEventsListener{
        void onSpinnerOpened();
        void onSpinnerClosed();
    }

    // the Spinner constructors

    @Override
    public boolean performClick() {
        // register that the Spinner was opened so we have a status
        // indicator for the activity(which may lose focus for some other
        // reasons)
        mOpenInitiated = true;
        if (mListener != null) {
            mListener.onSpinnerOpened();
        }
        return super.performClick();
    }

    public void setSpinnerEventsListener(OnSpinnerEventsListener onSpinnerEventsListener) {
        mListener = onSpinnerEventsListener;
    }

     //Propagate the closed Spinner event to the listener from outside.
    public void performClosedEvent() {
        mOpenInitiated = false;
        if (mListener != null) {
            mListener.onSpinnerClosed();
        }
    }

     // A boolean flag indicating that the Spinner triggered an open event.
     // @return true for opened Spinner
    public boolean hasBeenOpened() {
        return mOpenInitiated;
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasBeenOpened() && hasWindowFocus) {
            performClosedEvent();
        }
    }

}