package com.aleena.common.widgets.roboto;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.aleena.common.widgets.vCheckBox;

/**
 * Created by venkatesh on 29/7/15.
 */

public class RobotoLightCheckBox extends vCheckBox {

    Typeface robotoRegularFont;

    public RobotoLightCheckBox(Context context) {
        super(context);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }

    public RobotoLightCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        robotoRegularFont = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        this.setTypeface(robotoRegularFont);
    }
}
