package com.aleena.common.widgets;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class vMapWrapperLayout extends FrameLayout {

    private OnDragListener mOnDragListener;

    public vMapWrapperLayout(Context context) {
        super(context);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mOnDragListener != null) {
            mOnDragListener.onDrag(ev);
        }
        try {
            return super.dispatchTouchEvent(ev);
        }catch (Exception e){ e.printStackTrace();return true;  }
    }

    public void setOnDragListener(OnDragListener mOnDragListener) {
        this.mOnDragListener = mOnDragListener;
    }

    public interface OnDragListener {
        public void onDrag(MotionEvent motionEvent);
    }
}
