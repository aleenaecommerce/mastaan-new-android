package com.aleena.common.widgets;

import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.aleena.common.interfaces.SoftKeyBoardActionsCallback;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by venkatesh on 29/7/15.
 */

public class vAppCompatEditText extends AppCompatEditText {

    AppCompatEditText appCompatEditText;
    boolean detectTextChange = true;

    Date lastTypedTime;
    Timer textWatchTimer = new Timer();

    SoftKeyBoardActionsCallback softKeyBoardActionsCallback;

    public vAppCompatEditText(Context context) {
        super(context);
        appCompatEditText = this;
    }

    public vAppCompatEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        appCompatEditText = this;
    }

    public vAppCompatEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        appCompatEditText = this;
    }

    public void checkError(String errorMessage){
        if ( getText().toString().trim().length() == 0){
            appCompatEditText.setText("");
            appCompatEditText.setError(errorMessage);
        }else{
            hideError();
        }
    }

    public void showError(String errorMessage){
        appCompatEditText.setError(errorMessage);
    }

    public void hideError(){
        appCompatEditText.setError(null);
    }

    public void setTextChangeListener(final TextChangeCallback callback) {

        appCompatEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                lastTypedTime = new Date();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (textWatchTimer != null) {
                    textWatchTimer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (detectTextChange == true) {
                    textWatchTimer = new Timer();
                    textWatchTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            Date runTime = new Date();
                            if (lastTypedTime.getTime() + 180 <= runTime.getTime()) {   // If Typing Finished..
                                //Log.d("customTextListener", "Typing Finished : " + editText.getText());
                                callback.onTextChangeFinish(appCompatEditText.getText().toString());
                            }
                        }
                    }, 800);
                }
            }
        });
    }

    public void setSoftKeyBoardListener(SoftKeyBoardActionsCallback callback){
        this.softKeyBoardActionsCallback = callback;

        appCompatEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    softKeyBoardActionsCallback.onDonePressed(appCompatEditText.getText().toString());
                }
                return false;
            }
        });
    }

    public void changeText(final String text) {
        detectTextChange = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    appCompatEditText.setText(text);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    detectTextChange = true;
                }
            }
        }).start();
    }

    public interface TextChangeCallback {
        void onTextChangeFinish(String text);
    }

    //------

    boolean disablePaste;
    public void disablePaste() {
        this.disablePaste = true;
    }
    @Override
    public boolean onTextContextMenuItem(int id) {
        if ( disablePaste && ( id == android.R.id.paste || id == android.R.id.pasteAsPlainText ) ){
            return false;
        }
        return super.onTextContextMenuItem(id);
    }

}
