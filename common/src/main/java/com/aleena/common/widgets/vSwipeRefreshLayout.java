package com.aleena.common.widgets;

import android.app.Activity;
import android.content.Context;

import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aleena.common.R;
import com.google.android.material.appbar.AppBarLayout;

/**
 * Created by venkatesh on 19/4/16.
 */
public class vSwipeRefreshLayout extends SwipeRefreshLayout implements AppBarLayout.OnOffsetChangedListener {
    private AppBarLayout appBarLayout;

    public vSwipeRefreshLayout(Context context) {
        super(context);
    }

    public vSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (getContext() instanceof Activity) {
            appBarLayout = (AppBarLayout) ((Activity) getContext()).findViewById(R.id.appBarLayout);
            if ( appBarLayout != null ) {
                appBarLayout.addOnOffsetChangedListener(this);
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if(appBarLayout!=null){
            appBarLayout.removeOnOffsetChangedListener(this);
            appBarLayout = null;
            super.onDetachedFromWindow();
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        this.setEnabled(i == 0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        try {
            return super.onTouchEvent(ev);
        } catch (IllegalArgumentException e) {  // Fix for support lib bug, happening when onDestroy is
            return true;
        }
    }
}