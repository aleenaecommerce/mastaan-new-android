package com.aleena.common.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ViewSwitcher;

import com.aleena.common.R;
import com.aleena.common.constants.DateConstants;
import com.aleena.common.interfaces.DateAndTimePickerCallback;
import com.aleena.common.methods.DateMethods;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by venkatesh on 4/12/15.
 */
public class DateAndTimePickerDialog implements View.OnClickListener{

    Context context;
    AlertDialog dateAndTimePickerDialog;

    DateAndTimePickerCallback callback;

    //
    View datePickerDialogView;

    ViewSwitcher viewSwitcher;
    Button dateView;
    Button slotView;
    TextView selectedDateView;
    TextView selectedSlotView;
    DatePicker date_picker;
    TimePicker time_picker;
    Button done;
    Button cancel;

    boolean showYear = true;

    public DateAndTimePickerDialog(Context context){
        this.context = context;
        dateAndTimePickerDialog = new AlertDialog.Builder(context).create();

        datePickerDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_date_and_time_picker, null);

        viewSwitcher = (ViewSwitcher) datePickerDialogView.findViewById(R.id.viewSwitcher);

        dateView = (Button) datePickerDialogView.findViewById(R.id.dateView);
        dateView.setOnClickListener(this);
        slotView = (Button) datePickerDialogView.findViewById(R.id.slotView);
        slotView.setOnClickListener(this);
        selectedDateView = (TextView) datePickerDialogView.findViewById(R.id.selectedDateView);
        selectedSlotView = (TextView) datePickerDialogView.findViewById(R.id.selectedSlotView);

        date_picker = (DatePicker) datePickerDialogView.findViewById(R.id.date_picker);
        time_picker = (TimePicker) datePickerDialogView.findViewById(R.id.time_picker);

        done = (Button) datePickerDialogView.findViewById(R.id.done);
        done.setOnClickListener(this);
        cancel = (Button) datePickerDialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if ( view == dateView ){
            viewSwitcher.setDisplayedChild(0);
            done.setText("SELECT TIME");
            selectedSlotView.setText("SELECT TIME");
        }

        else if ( view == slotView ){
            viewSwitcher.setDisplayedChild(1);
            selectedSlotView.setText(DateMethods.getTimeIn12HrFormat(String.format("%02d",time_picker.getCurrentHour()) + ":" + String.format("%02d", time_picker.getCurrentMinute())));
            done.setText("DONE");
        }

        else if ( view == cancel ){
            dateAndTimePickerDialog.dismiss();
        }

        else if ( view == done ){

            if ( done.getText().toString().equalsIgnoreCase("SELECT TIME") ){
                slotView.performClick();
            }
            else if ( done.getText().toString().equalsIgnoreCase("DONE") ){
                SimpleDateFormat t24HourFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat t12HourFormat = new SimpleDateFormat("hh:mm aa");
                String t24HourTime = String.format("%02d",time_picker.getCurrentHour()) + ":" + String.format("%02d", time_picker.getCurrentMinute());
                String t12HourTime = "";
                try {
                    t12HourTime = t12HourFormat.format(t24HourFormat.parse(t24HourTime));
                } catch (Exception e) {}

                if (showYear==false) {               callback.onSelect(String.format("%02d",date_picker.getDayOfMonth())+"/"+String.format("%02d",(date_picker.getMonth()+1)), date_picker.getDayOfMonth(), date_picker.getMonth()+1, 0, t24HourTime, t12HourTime, time_picker.getCurrentHour(), time_picker.getCurrentMinute(), 0);
                } else {
                    callback.onSelect(String.format("%02d", date_picker.getDayOfMonth()) + "/" + String.format("%02d", (date_picker.getMonth() + 1)) + "/" + date_picker.getYear(), date_picker.getDayOfMonth(), date_picker.getMonth() + 1, date_picker.getYear(), t24HourTime, t12HourTime, time_picker.getCurrentHour(), time_picker.getCurrentMinute(), 0);
                }
                dateAndTimePickerDialog.dismiss();
            }

        }
    }

    public AlertDialog getDialog(){
        return dateAndTimePickerDialog;
    }

    // Date Picker Dialog..

    public void showDatePickerDialog(String dialogTitle, final DateAndTimePickerCallback callback){
        showDatePickerDialog(dialogTitle, null, null, DateMethods.getCurrentDate(), true, "00:00", "23:59", "00:00", callback);
    }

    public void showDatePickerDialog(String dialogTitle, String initialDate, final DateAndTimePickerCallback callback){
        showDatePickerDialog(dialogTitle, null, null, initialDate, true, "00:00", "23:59", "00:00", callback);
    }
    public void showDatePickerDialog(String dialogTitle, String initialDate, String initialTime, final DateAndTimePickerCallback callback){
        showDatePickerDialog(dialogTitle, null, null, initialDate, true, "00:00", "23:59", initialTime, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, String initialDate, final DateAndTimePickerCallback callback){
        showDatePickerDialog(dialogTitle, minimumDate, maximumDate, initialDate, true, "00:00", "23:59", "00:00", callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, String initialDate, final boolean showYear, String minimumTime, String maximumTime, String initialTime, final DateAndTimePickerCallback callback){

        this.showYear = showYear;
        this.callback = callback;

        dateAndTimePickerDialog = new AlertDialog.Builder(context).create();
        dateAndTimePickerDialog.setView(datePickerDialogView);
        dateAndTimePickerDialog.setCancelable(true);

        TextView title = (TextView) datePickerDialogView.findViewById(R.id.title);
        if ( dialogTitle != null && dialogTitle.length() > 0 ){
            title.setText(dialogTitle);
        }

        int initialDay = 0, initialMonth = 0, initialYear = 0;
        Date initDate = DateMethods.getDateFromString(initialDate);
        if ( initDate  != null ){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(initDate);
            initialDay = calendar.get(Calendar.DAY_OF_MONTH);
            initialMonth = calendar.get(Calendar.MONTH);
            initialYear = calendar.get(Calendar.YEAR);
        }

        DatePicker.OnDateChangedListener onDateChangedListener = new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int dayOfMonth) {
                selectedDateView.setText(DateMethods.getDateInFormat(String.format("%02d", date_picker.getDayOfMonth()) + "/" + String.format("%02d", (date_picker.getMonth() + 1)) + "/" + date_picker.getYear(), DateConstants.MMM_DD_YYYY));
            }
        };

        if ( showYear == false ){
            date_picker.setMinDate(DateMethods.getDateInMilliSeconds("01/01/2012"));
            date_picker.setMaxDate(DateMethods.getDateInMilliSeconds("31/12/2012"));
            try{
                date_picker.findViewById(Resources.getSystem().getIdentifier("year", "id", "android")).setVisibility(View.GONE);
            }catch (Exception e) {}

            date_picker.init(2012, initialMonth, initialDay, onDateChangedListener);
        }
        else{
            if (DateMethods.getDateInMilliSeconds(minimumDate) > 0 ){
                date_picker.setMinDate(DateMethods.getDateInMilliSeconds(minimumDate));
            }
            if (DateMethods.getDateInMilliSeconds(maximumDate) > 0 ){
                date_picker.setMaxDate(DateMethods.getDateInMilliSeconds(maximumDate));
            }
            date_picker.init(initialYear, initialMonth, initialDay, onDateChangedListener);
        }

        selectedDateView.setText(DateMethods.getDateInFormat(String.format("%02d", date_picker.getDayOfMonth()) + "/" + String.format("%02d", (date_picker.getMonth() + 1)) + "/" + date_picker.getYear(), DateConstants.MMM_DD_YYYY));

        //-------

        int initHour = 0, initMinutes = 0, minHour = 0, minMinutes = 0, maxHour = 24, maxMinutes = 0;

        Calendar initialTimeCalendar = DateMethods.getCalendarFromDate(initialTime);
        if (  initialTimeCalendar != null ){
            initHour = initialTimeCalendar.get(Calendar.HOUR_OF_DAY);
            initMinutes = initialTimeCalendar.get(Calendar.MINUTE);
        }

        Calendar minTimeCalendar = DateMethods.getCalendarFromDate(minimumTime);
        if (  minTimeCalendar != null ){
            minHour = minTimeCalendar.get(Calendar.HOUR_OF_DAY);
            minMinutes = minTimeCalendar.get(Calendar.MINUTE);
        }
        Calendar maxTimeCalendar = DateMethods.getCalendarFromDate(maximumTime);
        if (  maxTimeCalendar != null ){
            maxHour = maxTimeCalendar.get(Calendar.HOUR_OF_DAY);
            maxMinutes = maxTimeCalendar.get(Calendar.MINUTE);
        }

        final String minTime = String.format("%02d",minHour)+":"+String.format("%02d",minMinutes);
        final String maxTime = String.format("%02d",maxHour)+":"+String.format("%02d",maxMinutes);
        final String initTime = String.format("%02d",initHour)+":"+String.format("%02d",initMinutes);
        final int initialHour = initHour, initialMinutes = initMinutes, mininumMinutes = minMinutes, minimumHour = minHour, maximumMinutes = maxMinutes, maximumHour = maxHour;
        time_picker.setCurrentHour(initialHour);
        time_picker.setCurrentMinute(initialMinutes);

        time_picker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                try {
                    final String selectedTime = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute);

                    if (DateMethods.compareDates(minTime, selectedTime) > 0) {
                        //Log.d(Constants.LOG_TAG, "MIN , min:" + minTime + " with current " + selectedTime);
                        time_picker.setCurrentHour(minimumHour);
                        time_picker.setCurrentMinute(mininumMinutes);
                    } else if (DateMethods.compareDates(selectedTime, maxTime) > 0) {
                        //Log.d(Constants.LOG_TAG, "MAX , max:" + maxTime + " with current" + selectedTime);
                        time_picker.setCurrentHour(maximumHour);
                        time_picker.setCurrentMinute(maximumMinutes);
                    }
                    selectedSlotView.setText(DateMethods.getTimeIn12HrFormat(String.format("%02d",time_picker.getCurrentHour()) + ":" + String.format("%02d", time_picker.getCurrentMinute())));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //-------

        try{    dateAndTimePickerDialog.show(); }catch (Exception e){}
    }

    public void closeDialog(){
        if ( dateAndTimePickerDialog != null ){
            try{    dateAndTimePickerDialog.dismiss();  }catch (Exception e){}
        }
    }

}
