package com.aleena.common.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.R;
import com.aleena.common.interfaces.ActionCallback;

/**
 * Created by venkatesh on 4/12/15.
 */
public class NotificationDialog {

    Context context;
    AlertDialog notificationDialog;

    public NotificationDialog(Context context){
        this.context = context;
        notificationDialog = new AlertDialog.Builder(context).create();
    }

    public AlertDialog getDialog(){
        return notificationDialog;
    }

    public void showDialog(CharSequence notificationTitle, CharSequence notificationMessage) {
        showDialog(true, notificationTitle, notificationMessage, null);
    }
    public void showDialog(CharSequence notificationTitle, CharSequence notificationMessage, final ActionCallback callback) {
        showDialog(true, notificationTitle, notificationMessage, callback);
    }
    public void showDialog(boolean isCancellable, CharSequence notificationTitle, CharSequence notificationMessage, final ActionCallback callback) {
        closeDialog();

        View notificationDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_notifcation, null);
        notificationDialog.setView(notificationDialogView);
        notificationDialog.setCancelable(isCancellable);

        TextView title = (TextView) notificationDialogView.findViewById(R.id.title);
        TextView subtitle = (TextView) notificationDialogView.findViewById(R.id.subtitle);
        title.setText(notificationTitle);
        subtitle.setText(notificationMessage);

        notificationDialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialog();
                if (callback != null) {
                    callback.onAction();
                }
            }
        });

        //---------

        try{    notificationDialog.show();  }catch (Exception e){}
    }

    public void closeDialog(){
        if ( notificationDialog != null ){
            try{    notificationDialog.dismiss();   }catch (Exception e){}
        }
    }

}
