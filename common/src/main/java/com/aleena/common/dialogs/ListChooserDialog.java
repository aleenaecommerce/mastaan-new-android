package com.aleena.common.dialogs;

import android.app.AlertDialog;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;

import com.aleena.common.R;
import com.aleena.common.adapters.ListChooserAdapter;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.widgets.vRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkatesh on 19/02/17.
 */
public class ListChooserDialog {

    Context context;
    AlertDialog listChooserDialog;

    public ListChooserDialog(Context context){
        this.context = context;
        listChooserDialog = new AlertDialog.Builder(context).create();
    }

    public AlertDialog getDialog(){
        return listChooserDialog;
    }

    public void show(String []listItems, final ListChooserCallback callback) {
        show(null, listItems, callback);
    }
    public void show(List<String> listItems, final ListChooserCallback callback) {
        show(null, listItems, callback);
    }
    public void show(String dialogTitle, String []listItems, final ListChooserCallback callback) {
        List<String> list_items = new ArrayList<>();
        if ( listItems != null && listItems.length > 0 ){
            for (int i=0;i<listItems.length;i++){
                if ( listItems[i] != null ) {
                    list_items.add(listItems[i]);
                }
            }
        }
        show(dialogTitle, list_items, -1, callback);
    }
    public void show(String dialogTitle, List<String> listItems, final ListChooserCallback callback) {
        show(dialogTitle, listItems, -1, callback);
    }
    public void show(String dialogTitle, List<String> listItems, int highlightItem, final ListChooserCallback callback) {
        show(true, dialogTitle, listItems, highlightItem, callback);
    }
    public void show(boolean isCancellable, String dialogTitle, List<String> listItems, int highlightItem, final ListChooserCallback callback) {
        View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_list_chooser, null);
        listChooserDialog.setView(dialogView);
        listChooserDialog.setCancelable(isCancellable);

        TextView titleView = (TextView) dialogView.findViewById(R.id.title);
        vRecyclerView listHolder = (vRecyclerView) dialogView.findViewById(R.id.listHolder);
        listHolder.setHasFixedSize(true);
        listHolder.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.VERTICAL, false));
        listHolder.setItemAnimator(new DefaultItemAnimator());

        titleView.setText(dialogTitle);
        if ( dialogTitle == null || dialogTitle.length() == 0 ){
            titleView.setVisibility(View.GONE);
        }else{
            titleView.setVisibility(View.VISIBLE);
        }
        listHolder.setAdapter(new ListChooserAdapter(context, listItems, highlightItem, new ListChooserCallback() {
            @Override
            public void onSelect(int position) {
                if ( callback != null ){
                    close();
                    callback.onSelect(position);
                }
            }
        }));

        try {
            listChooserDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close(){
        if ( listChooserDialog != null ){
            try{
                listChooserDialog.dismiss();
            } catch (Exception e){

            }
        }
    }

}
