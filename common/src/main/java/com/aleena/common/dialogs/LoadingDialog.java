package com.aleena.common.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.aleena.common.R;

/**
 * Created by venkatesh on 4/12/15.
 */
public class LoadingDialog {

    Context context;
    AlertDialog loadingDialog;

    public LoadingDialog(Context context){
        this.context = context;
        loadingDialog = new AlertDialog.Builder(context).create();
    }

    public AlertDialog getDialog(){
        return loadingDialog;
    }

    public void showDialog(CharSequence title) {
        showDialog(false, title);
    }
    public void showDialog(boolean isCancellable, CharSequence title) {
        closeDialog();

        View loadingDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null);
        loadingDialog.setView(loadingDialogView);
        loadingDialog.setCancelable(isCancellable);

        TextView loadingTitle = (TextView) loadingDialogView.findViewById(R.id.title);
        loadingTitle.setText(title);

        //-------

        try{    loadingDialog.show();   }catch (Exception e){}
    }

    public void closeDialog(){
        if ( loadingDialog != null ){
            try{    loadingDialog.dismiss();    }catch (Exception e){}
        }
    }

}
