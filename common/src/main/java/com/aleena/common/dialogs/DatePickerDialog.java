package com.aleena.common.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.aleena.common.R;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by venkatesh on 4/12/15.
 */
public class DatePickerDialog {

    Context context;
    AlertDialog datePickerDialog;

    CommonMethods commonMethods;

    public DatePickerDialog(Context context){
        this.context = context;
        datePickerDialog = new AlertDialog.Builder(context).create();
        commonMethods = new CommonMethods();
    }

    public AlertDialog getDialog(){
        return datePickerDialog;
    }

    // Date Picker Dialog..

    public void showDatePickerDialog(String dialogTitle, final DatePickerCallback callback){
        showDatePickerDialog(dialogTitle, null, null, DateMethods.getCurrentDate(), true, true,callback);
    }

    public void showDatePickerDialog(String dialogTitle, String initialDate, final DatePickerCallback callback){
        showDatePickerDialog(dialogTitle, null, null, initialDate, true, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, final DatePickerCallback callback){
        showDatePickerDialog(dialogTitle, minimumDate, maximumDate, minimumDate, true, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, String initialDate, final DatePickerCallback callback){
        showDatePickerDialog(dialogTitle, minimumDate, maximumDate, initialDate, true, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, String initialDate, final boolean showYear, final  boolean showDay, final DatePickerCallback callback){

        datePickerDialog = new AlertDialog.Builder(context).create();
        View datePickerDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_date_picker, null);
        datePickerDialog.setView(datePickerDialogView);
        datePickerDialog.setCancelable(true);

        TextView title = (TextView) datePickerDialogView.findViewById(R.id.title);
        if ( dialogTitle != null && dialogTitle.length() > 0 ){
            title.setText(dialogTitle);
        }

        final DatePicker date_picker = (DatePicker) datePickerDialogView.findViewById(R.id.date_picker);

        int initialDay = 0, initialMonth = 0, initialYear = 0;
        Date initDate = DateMethods.getDateFromString(initialDate);
        if ( initDate  != null ){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(initDate);
            initialDay = calendar.get(Calendar.DAY_OF_MONTH);
            initialMonth = calendar.get(Calendar.MONTH);
            initialYear = calendar.get(Calendar.YEAR);
        }

        if ( showYear == false ){
            date_picker.setMinDate(DateMethods.getDateInMilliSeconds("01/01/2012"));
            date_picker.setMaxDate(DateMethods.getDateInMilliSeconds("31/12/2012"));
            try{
                date_picker.findViewById(Resources.getSystem().getIdentifier("year", "id", "android")).setVisibility(View.GONE);
            }catch (Exception e) {}

            date_picker.init(2012, initialMonth, initialDay, null);
        }
        else if ( showDay == false ){
            if (DateMethods.getDateInMilliSeconds(minimumDate) > 0 ){
                date_picker.setMinDate(DateMethods.getDateInMilliSeconds(minimumDate));
            }
            if (DateMethods.getDateInMilliSeconds(maximumDate) > 0 ){
                date_picker.setMaxDate(DateMethods.getDateInMilliSeconds(maximumDate));
            }
            try{
                date_picker.findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
            }catch (Exception e) {}

            date_picker.init(initialYear, initialMonth, 1, null);
        }
        else{
            if (DateMethods.getDateInMilliSeconds(minimumDate) > 0 ){
                date_picker.setMinDate(DateMethods.getDateInMilliSeconds(minimumDate));
            }
            if (DateMethods.getDateInMilliSeconds(maximumDate) > 0 ){
                date_picker.setMaxDate(DateMethods.getDateInMilliSeconds(maximumDate));
            }
            date_picker.init(initialYear, initialMonth, initialDay, null);
        }

        datePickerDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showYear==false) {
                    callback.onSelect(String.format("%02d",date_picker.getDayOfMonth())+"/"+String.format("%02d",(date_picker.getMonth()+1)), date_picker.getDayOfMonth(), date_picker.getMonth()+1, 0);
                } else {
                    callback.onSelect(String.format("%02d", date_picker.getDayOfMonth()) + "/" + String.format("%02d", (date_picker.getMonth() + 1)) + "/" + date_picker.getYear(), date_picker.getDayOfMonth(), date_picker.getMonth()+1, date_picker.getYear());
                }
                datePickerDialog.dismiss();
            }
        });

        datePickerDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.dismiss();
            }
        });

        //------

        try{    datePickerDialog.show();    }catch (Exception e){}

    }

    public void closeDialog(){
        if ( datePickerDialog != null ){
            try{    datePickerDialog.dismiss(); }catch (Exception e){}
        }
    }

}
