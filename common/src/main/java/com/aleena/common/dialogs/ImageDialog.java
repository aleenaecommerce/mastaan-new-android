package com.aleena.common.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.aleena.common.R;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Created by Venkatesh Uppu on 23/6/17.
 */

public class ImageDialog {
    Context context;
    AlertDialog alertDialog;
    View dialogView;

    String imageURL;
    File imageFile;

    public ImageDialog(Context context){
        this.context = context;
    }

    public void show(String imageURL){
        this.imageURL = imageURL;
        show();
    }
    public void show(File imageFile){
        this.imageFile = imageFile;
        show();
    }

    private void show(){
        try{
            alertDialog = new AlertDialog.Builder(context).create();
            dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_image, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(true);
            alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

            ImageView image = (ImageView) dialogView.findViewById(R.id.image);
            if ( imageURL != null ) {
                Picasso.get()
                        .load(imageURL)
                        .placeholder(R.drawable.image_default)
                        .error(R.drawable.image_default)
                        .into(image);
            }
            else if ( imageFile != null ) {
                Picasso.get()
                        .load(imageFile)
                        .placeholder(R.drawable.image_default)
                        .error(R.drawable.image_default)
                        .into(image);
            }

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            dialogView.findViewById(R.id.container).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
        }catch (Exception e){}
    }

}
