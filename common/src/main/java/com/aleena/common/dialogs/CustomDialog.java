package com.aleena.common.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by venkatesh on 4/12/15.
 */
public class CustomDialog {

    Context context;
    AlertDialog customDialog;

    public CustomDialog(Context context){
        this.context = context;
        customDialog = new AlertDialog.Builder(context).create();
    }

    public AlertDialog getDialog(){
        return customDialog;
    }

    public void showDialog(View dialogView){
        showDialog(true, dialogView);
    }

    public void showDialog(boolean isCancellable, View dialogView) {
        showDialog(isCancellable, dialogView, false);
    }
    public void showDialog(boolean isCancellable, View dialogView, boolean setTransaprentBackground) {
        customDialog = new AlertDialog.Builder(context).create();
        customDialog.setView(dialogView);
        customDialog.setCancelable(isCancellable);

        if ( setTransaprentBackground ){
            try{
                customDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            }catch (Exception e){}
        }

        try {
            ViewGroup grp = (ViewGroup) dialogView.getParent();
            if (grp != null) {
                grp.removeView(dialogView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            customDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeDialog(){
        if ( customDialog != null ){
            try{
                customDialog.dismiss();
            } catch (Exception e){

            }
        }
    }

}
