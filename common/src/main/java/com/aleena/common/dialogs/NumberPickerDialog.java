package com.aleena.common.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.aleena.common.R;
import com.aleena.common.interfaces.NumberPickerCallback;
import com.aleena.common.methods.CommonMethods;

import java.util.List;

/**
 * Created by venkatesh on 4/12/15.
 */
public class NumberPickerDialog {

    Context context;
    AlertDialog numberPickerDialog;
    NumberPickerCallback callback;

    public NumberPickerDialog(Context context){
        this.context = context;
        numberPickerDialog = new AlertDialog.Builder(context).create();
    }

    public AlertDialog getDialog(){
        return numberPickerDialog;
    }

    public void showDialog(final String displayTitle, List<String> displayValues, final int startValue, final int minValue, final int maxValue, final NumberPickerCallback callback) {
        showDialog(true, displayTitle, new CommonMethods().getStringArrayFromStringList(displayValues), startValue, minValue, maxValue, callback);
    }

    public void showDialog(boolean isCancellable, final String displayTitle, List<String> displayValues, final int startValue, final int minValue, final int maxValue, final NumberPickerCallback callback) {
        showDialog(isCancellable, displayTitle, new CommonMethods().getStringArrayFromStringList(displayValues), startValue, minValue, maxValue, callback);
    }

    public void showDialog(final String displayTitle, final String[] displayValues, final int startValue, final int minValue, final int maxValue, final NumberPickerCallback callback) {
        showDialog(true, displayTitle, displayValues, startValue, minValue, maxValue, callback);
    }

    public void showDialog(boolean isCancellable, final String displayTitle, final String[] displayValues, final int startValue, final int minValue, final int maxValue, final NumberPickerCallback callback) {

        View numberPickerDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_number_picker, null);
        numberPickerDialog.setView(numberPickerDialogView);
        numberPickerDialog.setCancelable(isCancellable);
        this.callback = callback;

        final TextView title = (TextView) numberPickerDialogView.findViewById(R.id.title);
        final NumberPicker numberPicker = (NumberPicker) numberPickerDialogView.findViewById(R.id.numberPicker);

        if (displayTitle != null && displayTitle.length() > 0) {
            title.setText(displayTitle);
        }
        if (minValue > 0) {
            numberPicker.setMinValue(minValue);
        }
        if (maxValue > 0) {
            numberPicker.setMaxValue(maxValue);
        }
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setValue(startValue);

        if (displayValues != null && displayValues.length > 0) {
            numberPicker.setDisplayedValues(displayValues);
        }

        numberPickerDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( callback != null ) {
                    callback.onSelect(numberPicker.getValue());
                }
                numberPickerDialog.dismiss();
            }
        });
        numberPickerDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberPickerDialog.dismiss();
            }
        });

        //------

        try{    numberPickerDialog.show();  }catch (Exception e){}
    }

    public void closeDialog(){
        if ( numberPickerDialog != null ){
            try{    numberPickerDialog.dismiss();   }catch (Exception e){}
        }
    }

}
