package com.aleena.common.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.aleena.common.R;
import com.aleena.common.interfaces.ChoiceSelectionCallback;

/**
 * Created by venkatesh on 4/12/15.
 */
public class ChoiceSelectionDialog {

    Context context;
    AlertDialog choiceSelectionDialog;
    ChoiceSelectionCallback callback;

    public ChoiceSelectionDialog(Context context){
        this.context = context;
        choiceSelectionDialog = new AlertDialog.Builder(context).create();
    }

    public AlertDialog getDialog(){
        return choiceSelectionDialog;
    }

    public void showDialog(CharSequence title, CharSequence subtitle, String option1, String option2, final ChoiceSelectionCallback callback) {
        showDialog(false, title, subtitle, option1, option2, callback);
    }

    public void showDialog(boolean isCancellable, CharSequence title, CharSequence subtitle, String option1, String option2, final ChoiceSelectionCallback callback) {

        this.callback = callback;

        View choiceSelectionDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_choice_selection, null);
        choiceSelectionDialog.setView(choiceSelectionDialogView);
        //}
        choiceSelectionDialog.setCancelable(isCancellable);

        TextView dialog_title = (TextView) choiceSelectionDialogView.findViewById(R.id.choice_dialog_title);
        dialog_title.setText(title);
        TextView dialog_subtitle = (TextView) choiceSelectionDialogView.findViewById(R.id.choice_dialog_subtitle);
        dialog_subtitle.setText(subtitle);
        //dialog_subtitle.setText(Html.fromHtml("<b>"+subtitle+"</b>"));
        final Button dialog_option1 = (Button) choiceSelectionDialogView.findViewById(R.id.choice_dialog_option1);
        dialog_option1.setText(option1);
        final Button dialog_option2 = (Button) choiceSelectionDialogView.findViewById(R.id.choice_dialog_option2);
        dialog_option2.setText(option2);

        dialog_option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();
                if (callback != null) {
                    callback.onSelect(1, dialog_option1.getText().toString());
                }
            }
        });

        dialog_option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();
                if (callback != null) {
                    callback.onSelect(2, dialog_option2.getText().toString());
                }
            }
        });

        try{    choiceSelectionDialog.show();   }catch (Exception e){}
    }

    public void closeDialog(){
        if ( choiceSelectionDialog != null ){
            try{    choiceSelectionDialog.dismiss();    }catch (Exception e){}
        }
    }

}
