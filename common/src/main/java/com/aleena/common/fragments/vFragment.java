package com.aleena.common.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.ClipboardManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.aleena.common.R;
import com.aleena.common.constants.ConstantsCommonLibrary;
import com.aleena.common.dialogs.ChoiceSelectionDialog;
import com.aleena.common.dialogs.CustomDialog;
import com.aleena.common.dialogs.DatePickerDialog;
import com.aleena.common.dialogs.ListChooserDialog;
import com.aleena.common.dialogs.NumberPickerDialog;
import com.aleena.common.interfaces.ActionCallback;
import com.aleena.common.interfaces.CheckLocatoinAccessCallback;
import com.aleena.common.interfaces.ChoiceSelectionCallback;
import com.aleena.common.interfaces.DatePickerCallback;
import com.aleena.common.interfaces.ListChooserCallback;
import com.aleena.common.interfaces.LocationCallback;
import com.aleena.common.interfaces.NumberPickerCallback;
import com.aleena.common.interfaces.TextInputCallback;
import com.aleena.common.interfaces.TimePickerCallback;
import com.aleena.common.location.GetCurrentLocation;
import com.aleena.common.location.GooglePlacesAPI;
import com.aleena.common.location.GoogleRetrofitInterface;
import com.aleena.common.location.HereMapsPlacesAPI;
import com.aleena.common.location.HereMapsRetrofitInterface;
import com.aleena.common.methods.CommonMethods;
import com.aleena.common.methods.DateMethods;
import com.aleena.common.widgets.vTextInputLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import retrofit.RestAdapter;

public class vFragment extends Fragment {

    BroadcastReceiver fragmentReceiver;

    Context context;
    private View layoutView;

    public LayoutInflater inflater;
    public InputMethodManager inputMethodManager;

    private ViewSwitcher viewSwitcher;
    ProgressBar loading_indicator;
    View reload_indicator;
    TextView loading_message;
    TextView no_data_message;

    SwipeRefreshLayout swipeRefreshLayout;
    SwipeRefreshLayout loadingPageSwipeRefreshLayout;

    public Animation fabEnterAnim, fabExitAnim;

    AlertDialog loadingDialog;
    AlertDialog notificationDialog;
    ChoiceSelectionDialog choiceSelectionDialog;
    CustomDialog customDialog;
    DatePickerDialog datePickerDialog;
    AlertDialog timePickerDialog;
    NumberPickerDialog numberPickerDialog;
    ListChooserDialog listChooserDialog;

    public int REGISTER_ACTIVITY_CODE;

    CheckLocatoinAccessCallback locationSettingsCallback;

    GoogleApiClient googleApiClient;
    GooglePlacesAPI googlePlacesAPI;

    public vFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutView = view;
        context = getActivity();

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        // ANIMATIONS

        fabEnterAnim = AnimationUtils.loadAnimation(context, R.anim.fab_enter_anim);
        fabExitAnim = AnimationUtils.loadAnimation(context, R.anim.fab_exit_anim);

        // ACTIVITY CODES

        REGISTER_ACTIVITY_CODE = getResources().getInteger(R.integer.register_activity_code);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public GooglePlacesAPI getGooglePlacesAPI(){
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://maps.googleapis.com/maps/api").setLogLevel(RestAdapter.LogLevel.FULL).build();
        GoogleRetrofitInterface googleRetrofitInterface = restAdapter.create(GoogleRetrofitInterface.class);
        return new GooglePlacesAPI(context.getResources().getString(R.string.google_apikey), googleRetrofitInterface);
    }
    public HereMapsPlacesAPI getHereMapsAPI() {
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("https://reverse.geocoder.ls.hereapi.com/6.2/").setLogLevel(RestAdapter.LogLevel.FULL).build();
        HereMapsRetrofitInterface hereMapsRetrofitInterface = restAdapter.create(HereMapsRetrofitInterface.class);
        return new HereMapsPlacesAPI(context.getResources().getString(R.string.heremaps_apikey), hereMapsRetrofitInterface);

    }


    //==============   RECEIVER METHODS    ========//

    public void registerFragmentReceiver(final String receiverName) {
        try{
            unregisterFragmentReceiver();

            IntentFilter intentFilter = new IntentFilter(receiverName);
            fragmentReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    try {
                        onFragmentReceiverMessage(receiverName, intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            context.registerReceiver(fragmentReceiver, intentFilter);  //registering receiver
        }catch (Exception e){}
    }
    public void unregisterFragmentReceiver(){
        try{
            if ( fragmentReceiver != null) {
                context.unregisterReceiver(fragmentReceiver);
            }
        }catch (Exception e){}
    }

    public void onFragmentReceiverMessage(String activityReceiverName, Intent receivedData){}

    //================= SNACK BAR METHODS    ============//

    public void showSnackbarMessage(String message) {
        showSnackbar(layoutView.findViewById(R.id.container), message, null, null);
    }

    public void showSnackbarMessage(String message, String actionName, ActionCallback callback){
        showSnackbar(layoutView, message, actionName, callback);
    }

    public void showSnackbarMessage(View view, String message, String actionName, ActionCallback callback){
        showSnackbar(view, message, actionName, callback);
    }

    private void showSnackbar(View view, String message, String actionName, final ActionCallback callback){
        if ( view == null ){    view = layoutView;    }

        if ( view != null ){
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            if ( callback != null && actionName != null ){
                snackbar.setAction(actionName, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.onAction();
                    }
                });
            }
            snackbar.show();
        }else{
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public void showToastMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    //---

    public void copyToClipBoard(String textToCopy){
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.setText(textToCopy);
    }

    //================== LOADING VIEW METHODS  ==================//

    public void hasLoadingView() {
        hasLoadingView(-1);
    }
    public void hasLoadingView(int textColorResourceID) {
        try{
            viewSwitcher = (ViewSwitcher) layoutView.findViewById(R.id.viewSwitcher);
            loading_indicator = (ProgressBar) layoutView.findViewById(R.id.loading_indicator);
            reload_indicator = layoutView.findViewById(R.id.reload_indicator);
            loading_message = (TextView) layoutView.findViewById(R.id.loading_message);
            no_data_message = (TextView) layoutView.findViewById(R.id.no_data_message);

            loadingPageSwipeRefreshLayout = (SwipeRefreshLayout) layoutView.findViewById(R.id.loadingPageSwipeRefreshLayout);
            if (loadingPageSwipeRefreshLayout != null) {
                loadingPageSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        loadingPageSwipeRefreshLayout.setRefreshing(false);
                        if ( isShowingLoadingPage() == false ) {
                            onReloadPressed();
                        }
                    }
                });
            }

            if ( textColorResourceID != -1 ){
                loading_message.setTextColor(context.getResources().getColor(textColorResourceID));
                no_data_message.setTextColor(context.getResources().getColor(textColorResourceID));
            }
        }catch (Exception e){}
    }

    public void switchToLoadingPage(){
        try {
            viewSwitcher.setDisplayedChild(0);
        }catch (Exception e){}
    }
    public void switchToContentPage(){
        try {
            viewSwitcher.setDisplayedChild(1);
        }catch (Exception e){}
    }
    public void showLoadingIndicator(String loadiingMessage){
        try {
            viewSwitcher.setDisplayedChild(0);
            loading_indicator.setVisibility(View.VISIBLE);
            reload_indicator.setVisibility(View.GONE);
            loading_message.setText(loadiingMessage);
            loading_message.setVisibility(View.VISIBLE);
            no_data_message.setVisibility(View.GONE);
        }catch (Exception e){}
    }

    public void showReloadIndicator(int statusCode, String reloadMessage){
        if ( statusCode == -1 ){
            boolean isInternetAvailable = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
            if ( isInternetAvailable ){
                reloadMessage = "Not able to connect to Server. \nPlease try again after a while";
            }else{
                reloadMessage = "No Internet connection!";
            }
        }
        showReloadIndicator(reloadMessage);
    }
    public void showReloadIndicator(String reloadMessage){
        try {
            viewSwitcher.setDisplayedChild(0);
            loading_indicator.setVisibility(View.GONE);
            reload_indicator.setVisibility(View.VISIBLE);
            loading_message.setText(reloadMessage);
            loading_message.setVisibility(View.VISIBLE);
            no_data_message.setVisibility(View.GONE);

            reload_indicator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onReloadPressed();
                }
            });
        }catch (Exception e){}
    }

    public void showNoDataIndicator(String noDataMessage){
        try {
            viewSwitcher.setDisplayedChild(0);
            loading_indicator.setVisibility(View.GONE);
            reload_indicator.setVisibility(View.GONE);
            loading_message.setVisibility(View.GONE);
            no_data_message.setVisibility(View.VISIBLE);
            no_data_message.setText(noDataMessage);
        }catch (Exception e){}
    }

    public boolean isShowingLoadingPage(){
        try{
            if ( viewSwitcher.getDisplayedChild() == 0 && loading_indicator.getVisibility() == View.VISIBLE){
                return true;
            }
        }catch (Exception e){}
        return false;
    }

    public boolean isShowingReloadPage(){
        try{
            if ( viewSwitcher.getDisplayedChild() == 0 && reload_indicator.getVisibility() == View.VISIBLE){
                return true;
            }
        }catch (Exception e){}
        return false;
    }

    public boolean isShowingNoDataPage(){
        try{
            if ( viewSwitcher.getDisplayedChild() == 0 && loading_indicator.getVisibility() != View.VISIBLE && reload_indicator.getVisibility() != View.VISIBLE){
                return true;
            }
        }catch (Exception e){}
        return false;
    }

    public boolean isShowingContentPage(){
        try{
            if ( viewSwitcher.getDisplayedChild() == 1 ){
                return true;
            }
        }catch (Exception e){}
        return false;
    }

    public void onReloadPressed(){}

    public void onFABPressed(){}


    //-------------- Swipe Refresh ----------------//

    public void hasSwipeRefresh() {
        swipeRefreshLayout = (SwipeRefreshLayout) layoutView.findViewById(R.id.swipeRefreshLayout);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    swipeRefreshLayout.setRefreshing(false);
                    onReloadPressed();
                }
            });
        }
    }


    //==============  CUSTOM DIALOGS METHODS  ==============//

    // Loading Dialog.
    public void showLoadingDialog(CharSequence title) {
        showLoadingDialog(false, title);
    }

    public void showLoadingDialog(boolean isCancellable, CharSequence title) {
        closeLoadingDialog();
        //if ( loadingDialog == null ){
        loadingDialog = new AlertDialog.Builder(context).create();
        View loadingDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null);
        loadingDialog.setView(loadingDialogView);
        loadingDialog.setCancelable(isCancellable);
        //}
        TextView loadingTitle = (TextView) loadingDialogView.findViewById(R.id.title);
        loadingTitle.setText(title);

        try {
            loadingDialog.show();
        }catch (Exception e){}
    }

    public void closeLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }

    // Custom Dialog..
    public void showCustomDialog(View dialogView) {
        showCustomDialog(dialogView, true);
    }
    public void showCustomDialog(View dialogView, boolean isCancellable) {
        closeCustomDialog();
        customDialog = new CustomDialog(context);
        customDialog.showDialog(isCancellable, dialogView);
    }

    public void closeCustomDialog() {
        if (customDialog != null) {
            customDialog.closeDialog();
        }
    }

    // Notification Dialog..
    public void showNotificationDialog(String notificationTitle, CharSequence notificationSubtitle) {
        showNotificationDialog(true, notificationTitle, notificationSubtitle, null);
    }
    public void showNotificationDialog(boolean isCancellable, String notificationTitle, CharSequence notificationSubtitle) {
        showNotificationDialog(isCancellable, notificationTitle, notificationSubtitle, null);
    }
    public void showNotificationDialog(String notificationTitle, CharSequence notificationSubtitle, final ActionCallback callback) {
        showNotificationDialog(true, notificationTitle, notificationSubtitle, callback);
    }
    public void showNotificationDialog(boolean isCancellable, CharSequence notificationTitle, CharSequence notificationSubtitle, final ActionCallback callback) {
        //if ( notificationDialog == null ){
        notificationDialog = new AlertDialog.Builder(context).create();
        View notificationDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_notifcation, null);
        notificationDialog.setView(notificationDialogView);
        notificationDialog.setCancelable(isCancellable);
        //}
        TextView title = (TextView) notificationDialogView.findViewById(R.id.title);
        TextView subtitle = (TextView) notificationDialogView.findViewById(R.id.subtitle);
        title.setText(notificationTitle);
        subtitle.setText(notificationSubtitle);

        notificationDialogView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notificationDialog != null) {
                    notificationDialog.dismiss();
                    if (callback != null) {
                        callback.onAction();
                    }
                }
            }
        });

        try {
            notificationDialog.show();
        }catch (Exception e){}
    }

    public void closeNotificationDialog() {
        if (notificationDialog != null) {
            notificationDialog.dismiss();
        }
    }

    // Choice Selection Dialog..
    public void showChoiceSelectionDialog(CharSequence title, CharSequence subtitle, String option1, String option2, final ChoiceSelectionCallback choiceSelectionCallback) {
        showChoiceSelectionDialog(true, title, subtitle, option1, option2, choiceSelectionCallback);
    }
    public void showChoiceSelectionDialog(boolean isCancellable, CharSequence title, CharSequence subtitle, String option1, String option2, final ChoiceSelectionCallback choiceSelectionCallback) {
        closeChoiceSelectionDialog();
        choiceSelectionDialog = new ChoiceSelectionDialog(context);
        choiceSelectionDialog.showDialog(isCancellable, title, subtitle, option1, option2, choiceSelectionCallback);
    }

    public void closeChoiceSelectionDialog() {
        if (choiceSelectionDialog != null) {
            choiceSelectionDialog.closeDialog();
        }
    }

    // TextInput Dialog
    public void showInputTextDialog(String dialogTitle, String hintText, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, hintText, "", -1, true, callback);
    }
    public void showInputTextDialog(String dialogTitle, CharSequence dialogMessage, String hintText, String prefillText, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, dialogMessage, hintText, prefillText, -1, true, -1, callback);
    }
    public void showInputNumberDialog(String dialogTitle, String hintText, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, hintText, "", InputType.TYPE_CLASS_NUMBER, true, callback);
    }
    public void showInputNumberDialog(String dialogTitle, String hintText, final String prefillNumber, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, hintText, prefillNumber, InputType.TYPE_CLASS_NUMBER, true, -1, callback);
    }
    public void showInputNumberDialog(String dialogTitle, String hintText, final String prefillNumber, final int limitNoOfCharacters, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, hintText, prefillNumber, InputType.TYPE_CLASS_NUMBER, true, limitNoOfCharacters, callback);
    }
    public void showInputNumberDecimalDialog(String dialogTitle, String hintText, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, hintText, "", InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL, true, -1, callback);
    }
    public void showInputNumberDecimalDialog(String dialogTitle, String hintText, final double inputNumber, final TextInputCallback callback){
        if ( inputNumber != 0 ) {
            showInputTextDialog(dialogTitle, hintText, CommonMethods.getInDecimalFormat(inputNumber), InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED, true, -1, callback);
        }else{
            showInputTextDialog(dialogTitle, hintText, "", InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED, true, -1, callback);
        }
    }
    public void showInputTextDialog(String dialogTitle, String hintText, int inputType, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, hintText, "", inputType, true, -1, callback);
    }
    public void showInputTextDialog(String dialogTitle, final String hintText, String prefillText, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, hintText, prefillText, -1, true, -1, callback);
    }
    public void showInputTextDialog(String dialogTitle, final String hintText, String prefillText, boolean compulsaryInput, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, hintText, prefillText, -1, compulsaryInput, -1, callback);
    }
    public void showInputTextDialog(String dialogTitle, final String hintText, String prefillText, int inputType, final boolean compulsaryInput, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, hintText, prefillText, inputType, compulsaryInput, -1, callback);
    }
    public void showInputTextDialog(String dialogTitle, final String hintText, String prefillText, int inputType, final boolean compulsaryInput, final int limitNoOfCharacters, final TextInputCallback callback){
        showInputTextDialog(dialogTitle, null, hintText, prefillText, inputType, compulsaryInput, limitNoOfCharacters, callback);
    }
    public void showInputTextDialog(String dialogTitle, final CharSequence dialogMessage, final String hintText, String prefillText, int inputType, final boolean compulsaryInput, final int limitNoOfCharacters, final TextInputCallback callback){
        View inputTextDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_input_text, null);
        TextView title = (TextView) inputTextDialogView.findViewById(R.id.title);
        TextView message = (TextView) inputTextDialogView.findViewById(R.id.message);
        final vTextInputLayout input_text = (vTextInputLayout) inputTextDialogView.findViewById(R.id.input_text);

        if ( dialogTitle != null && dialogTitle.length() > 0 ) {
            title.setVisibility(View.VISIBLE);
            title.setText(dialogTitle);
        }else { title.setVisibility(View.GONE); }

        if ( dialogMessage != null && dialogMessage.length() > 0 ) {
            message.setVisibility(View.VISIBLE);
            message.setText(dialogMessage);
        }else { message.setVisibility(View.GONE); }

        input_text.setHint(hintText);
        input_text.setText(prefillText);
        if ( inputType != -1 ){
            try{   input_text.getEditText().setInputType(inputType);    }catch (Exception e){}
        }
        if ( limitNoOfCharacters > 0 ){
            input_text.getEditText().setFilters(new InputFilter[] { new InputFilter.LengthFilter(limitNoOfCharacters)});
        }

        inputTextDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input_text.checkError("* Enter "+hintText);
                if ( compulsaryInput == false || input_text.getText().length() > 0 ) {
                    closeCustomDialog();
                    inputMethodManager.hideSoftInputFromWindow(input_text.getWindowToken(), 0);    // Hiding Keyboard on AutoVerification Starts..
                    if (callback != null) {
                        callback.onComplete(input_text.getText());
                    }
                }
            }
        });
        inputTextDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeCustomDialog();
            }
        });

        showCustomDialog(inputTextDialogView);
    }

    // Number Picker Dialog..
    public void showNumberPickerDialog(final String displayTitle, final String[] displayValues, final int startValue, final int minValue, final int maxValue, final NumberPickerCallback callback) {
        showNumberPickerDialog(true, displayTitle, displayValues, startValue, minValue, maxValue, callback);
    }
    public void showNumberPickerDialog(final boolean isCancellable, final String displayTitle, final String[] displayValues, final int startValue, final int minValue, final int maxValue, final NumberPickerCallback callback) {
        closeNumberPickerDialog();
        numberPickerDialog = new NumberPickerDialog(context);
        numberPickerDialog.showDialog(isCancellable, displayTitle, displayValues, startValue, minValue, maxValue, callback);
    }

    public void closeNumberPickerDialog() {
        if (numberPickerDialog != null) {
            numberPickerDialog.closeDialog();
        }
    }

    // Date Picker Dialog..
    public void showDatePickerDialog(String dialogTitle, final DatePickerCallback callback){
        showDatePickerDialog(dialogTitle, null, null, DateMethods.getCurrentDate(), true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String initialDate, final DatePickerCallback callback){
        showDatePickerDialog(dialogTitle, null, null, initialDate, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String initialDate, boolean showYear, final DatePickerCallback callback){
        showDatePickerDialog(dialogTitle, null, null, initialDate, showYear, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, final DatePickerCallback callback){
        showDatePickerDialog(dialogTitle, minimumDate, maximumDate, minimumDate, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, String initialDate, final DatePickerCallback callback){
        showDatePickerDialog(dialogTitle, minimumDate, maximumDate, initialDate, true, callback);
    }

    public void showDatePickerDialog(String dialogTitle, String minimumDate, String maximumDate, String initialDate, final boolean showYear, final DatePickerCallback callback){
        closeDatePickerDialog();
        datePickerDialog = new DatePickerDialog(context);
        datePickerDialog.showDatePickerDialog(dialogTitle, minimumDate, maximumDate, initialDate, showYear, true, callback);
    }

    public void closeDatePickerDialog(){
        if ( datePickerDialog != null ){
            datePickerDialog.closeDialog();
        }
    }

    // Time Picker Dialog..
    public void showTimePickerDialog(String dialogTitle, final TimePickerCallback callback){
        showTimePickerDialog(dialogTitle, "00:00", "23:59", "00:00", callback);
        //showTimePickerDialog(dialogTitle, 0, 0, 24, 0, 0, 0, callback);
    }
    public void showTimePickerDialog(String dialogTitle, String initialTime, final TimePickerCallback callback){
        showTimePickerDialog(dialogTitle, "00:00", "23:59", initialTime, callback);
        //showTimePickerDialog(dialogTitle, 0, 0, 24, 0, 0, 0, callback);
    }
    public void showTimePickerDialog(String dialogTitle, String minimumTime, String maximumTime, final TimePickerCallback callback){
        showTimePickerDialog(dialogTitle, minimumTime, maximumTime, minimumTime, callback);
    }
    public void showTimePickerDialog(String dialogTitle, String minimumTime, String maximumTime, String initialTime, final TimePickerCallback callback){

        int initHour = 0, initMinutes = 0, minHour = 0, minMinutes = 0, maxHour = 24, maxMinutes = 0;

        Calendar initialTimeCalendar = DateMethods.getCalendarFromDate(initialTime);
        if (  initialTimeCalendar != null ){
            initHour = initialTimeCalendar.get(Calendar.HOUR_OF_DAY);
            initMinutes = initialTimeCalendar.get(Calendar.MINUTE);
        }

        Calendar minTimeCalendar = DateMethods.getCalendarFromDate(minimumTime);
        if (  minTimeCalendar != null ){
            minHour = minTimeCalendar.get(Calendar.HOUR_OF_DAY);
            minMinutes = minTimeCalendar.get(Calendar.MINUTE);
        }
        Calendar maxTimeCalendar = DateMethods.getCalendarFromDate(maximumTime);
        if (  maxTimeCalendar != null ){
            maxHour = maxTimeCalendar.get(Calendar.HOUR_OF_DAY);
            maxMinutes = maxTimeCalendar.get(Calendar.MINUTE);
        }

        showTimePickerDialog(dialogTitle, minHour, minMinutes, maxHour, maxMinutes, initHour, initMinutes, callback);
    }

    //--------

    public void showListChooserDialog(String []listItems, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(listItems, callback);
    }
    public void showListChooserDialog(List<String> listItems, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(null, listItems, callback);
    }
    public void showListChooserDialog(String dialogTitle, String []listItems, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(dialogTitle, listItems, callback);
    }
    public void showListChooserDialog(String dialogTitle, List<String> listItems, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(dialogTitle, listItems, callback);
    }
    public void showListChooserDialog(String dialogTitle, List<String> listItems, int highlightItem, final ListChooserCallback callback) {
        closeListChooserDialog();
        listChooserDialog = new ListChooserDialog(context);
        listChooserDialog.show(dialogTitle, listItems, highlightItem, callback);
    }

    public void closeListChooserDialog() {
        if (listChooserDialog != null) {
            listChooserDialog.close();
        }
    }

    //===============================================================

    public String getDeviceID() {
        String deviceID = "";
        /*try {
            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

            String tmDevice = "" + telephonyManager.getDeviceId();
            String tmSerial = "";// + telephonyManager.getSimSerialNumber();
            String androidId = "" + android.provider.Settings.Secure.getString(getActivity().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

            UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

            deviceID = deviceUuid.toString();
        }catch (Exception e){}*/    //TODO ?rtp

        return deviceID;
    }

    public String getDeviceName(){
        String deviceName = "";
        String manufacturerName = Build.MANUFACTURER;
        String modelName = Build.MODEL;

        if ( manufacturerName != null && manufacturerName.length() > 0 ){
            deviceName = manufacturerName.toUpperCase();
            if ( modelName != null && modelName.length() > 0 ){
                if ( modelName.startsWith(manufacturerName) ){
                    deviceName = modelName.toUpperCase();
                }else{
                    deviceName = manufacturerName.toUpperCase()+" "+modelName.toUpperCase();
                }
            }
        }

        return deviceName;
    }

    public int getAppVersionCode(){
        int currentVersion=0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            currentVersion = pInfo.versionCode;
        }catch (Exception e){}
        return currentVersion;
    }

    public String getAppVersionName(){
        String versionName="";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionName = pInfo.versionName;
        }catch (Exception e){}
        return versionName;
    }

    //======================

    public boolean isLocationAccessEnabled() {
        boolean status = false;
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(context.LOCATION_SERVICE);

        try {
            status = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (status == false) {
                status = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    public boolean isGPSEnabled() {
        boolean status = false;
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(context.LOCATION_SERVICE);
        try {
            status = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    public void checkLocationAccess(final CheckLocatoinAccessCallback callback) {

        locationSettingsCallback = callback;

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {
                        }
                    })
                    .build();
        }

        if (googleApiClient != null) {

            googleApiClient.connect();

            LocationRequest highAccuracylocationRequest = new LocationRequest();
            highAccuracylocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationRequest balancedPowerlocationRequest = new LocationRequest();
            balancedPowerlocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            LocationSettingsRequest.Builder locationSettingsRequestBuilder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(highAccuracylocationRequest)
                    .addLocationRequest(balancedPowerlocationRequest)
                    .setAlwaysShow(true);

            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationSettingsRequestBuilder.build()).setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {

                    final Status status = result.getStatus();

                    if (status.getStatusCode() == LocationSettingsStatusCodes.SUCCESS) {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : ENALED : All location settings are satisfied. The client can initialize location requests here");
                        locationSettingsCallback.onComplete(true, 200, "Location settings already enabled");
                    } else if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                        try {
                            Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : NOT ENABLED : Showing dialog to enable settings directly.");
                            status.startResolutionForResult(getActivity(), ConstantsCommonLibrary.LOCATION_SETTINGS_ACTIVITY_CODE);
                        } catch (Exception e) {
                            Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : NOT ENABLED : Not possible to show dialog, User has to enable manually.");
                            checkLocationAccessManually(callback);
                        }
                    } else if (status.getStatusCode() == LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE) {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : NOT ENABLED : No way to fix the settings so we won't show the dialog.");
                        checkLocationAccessManually(callback);
                    }
                }
            });
        } else {
            checkLocationAccessManually(callback);
        }

    }

    public void checkLocationAccessManually(CheckLocatoinAccessCallback callback) {

        locationSettingsCallback = callback;

        if (isLocationAccessEnabled()) {
            locationSettingsCallback.onComplete(true, 200, "Location settings already enabled");
        } else {
            showChoiceSelectionDialog(true, "Use Location?", "This app wants to change your device settings.\n\nUse GPS, Wi-Fi, and cell networks for location", "CANCEL", "SETTINGS", new ChoiceSelectionCallback() {
                @Override
                public void onSelect(int choiceNo, String choiceName) {
                    closeChoiceSelectionDialog();
                    if (choiceName.equalsIgnoreCase("SETTINGS")) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, ConstantsCommonLibrary.LOCATION_SETTINGS_ACTIVITY_CODE);
                    } else {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : CANCELLED BY USER");
                        locationSettingsCallback.onComplete(false, 500, "Location settings cancelled by user");
                    }
                }
            });
        }

    }

    //====

    public void getCurrentLocation(final LocationCallback locationCallback){
        getCurrentLocation(true, locationCallback);
    }

    public void getCurrentLocation(final boolean showDialogs, final LocationCallback locationCallback){

        checkLocationAccess(new CheckLocatoinAccessCallback() {
            @Override
            public void onComplete(boolean status, int statusCode, String message) {
                if ( status ){
                    if ( showDialogs ){ showLoadingDialog("Fetching current location, wait..."); }
                    new GetCurrentLocation(context, new GetCurrentLocation.Callback() {
                        @Override
                        public void onComplete(Location location) {
                            if ( showDialogs ){ closeLoadingDialog(); }
                            if ( location != null ){
                                if ( locationCallback != null ){
                                    locationCallback.onComplete(true, true, "* Location fetched successfully.", location, new LatLng(location.getLatitude(), location.getLongitude()));
                                }
                            } else {
                                if ( locationCallback != null ){
                                    locationCallback.onComplete(false, true, "* Unable to get current location.", null, null);
                                }
                            }
                        }
                    });
                } else {
                    if ( locationCallback != null ){
                        locationCallback.onComplete(false, false, "* Location access disabled.", null, null);
                    }
                }
            }
        });
    }

    public void showTimePickerDialog(String dialogTitle, final int minHour, final int minMinutes, final int maxHour, final  int maxMinutes, final int initialHour, final  int initialMinutes, final TimePickerCallback callback){

        closeTimePickerDialog();

        Log.d(ConstantsCommonLibrary.LOG_TAG, "minT: "+(minHour+":"+minMinutes)+" , maxT: "+(maxHour+":"+maxMinutes)+" , iniT: "+(initialHour+":"+initialMinutes));

        final String minTime = String.format("%02d",minHour)+":"+String.format("%02d",minMinutes);
        final String maxTime = String.format("%02d",maxHour)+":"+String.format("%02d",maxMinutes);
        final String initTime = String.format("%02d",initialHour)+":"+String.format("%02d",initialMinutes);

        timePickerDialog = new AlertDialog.Builder(context).create();
        View timerPickerDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_time_picker, null);
        timePickerDialog.setView(timerPickerDialogView);
        timePickerDialog.setCancelable(true);

        TextView title = (TextView) timerPickerDialogView.findViewById(R.id.title);
        if ( dialogTitle != null && dialogTitle.length() > 0 ){
            title.setText(dialogTitle);
        }

        final TimePicker time_picker = (TimePicker) timerPickerDialogView.findViewById(R.id.time_picker);

        time_picker.setCurrentHour(initialHour);
        time_picker.setCurrentMinute(initialMinutes);

        time_picker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                try {
                    final String selectedTime = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute);

                    if (DateMethods.compareDates(minTime, selectedTime) > 0) {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "MIN , min:" + minTime + " with current " + selectedTime);
                        time_picker.setCurrentHour(minHour);
                        time_picker.setCurrentMinute(minMinutes);
                    } else if (DateMethods.compareDates(selectedTime, maxTime) > 0) {
                        Log.d(ConstantsCommonLibrary.LOG_TAG, "MAX , max:" + maxTime + " with current" + selectedTime);
                        time_picker.setCurrentHour(maxHour);
                        time_picker.setCurrentMinute(maxMinutes);
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        });

        timePickerDialog.show();

        timerPickerDialogView.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.dismiss();
                SimpleDateFormat t24HourFormat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat t12HourFormat = new SimpleDateFormat("hh:mm aa");
                String t24HourTime = String.format("%02d",time_picker.getCurrentHour()) + ":" + String.format("%02d", time_picker.getCurrentMinute());
                String t12HourTime = "";
                try {
                    t12HourTime = t12HourFormat.format(t24HourFormat.parse(t24HourTime));
                } catch (Exception e) {
                    showToastMessage("Ex = " + e);
                }
                callback.onSelect(t24HourTime.toUpperCase(), t12HourTime.toUpperCase(), time_picker.getCurrentHour(), time_picker.getCurrentMinute(), 0);
            }
        });

        timerPickerDialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.dismiss();
            }
        });
    }

    public void closeTimePickerDialog(){
        if ( timePickerDialog != null ){
            timePickerDialog.dismiss();
        }
    }

//---------  AUTO REFRESHER

    boolean isAutoRefreshActive;
    Handler autorRefreshHandler;

    public void startAutoRefreshTimer(final long autoRefreshIntervalTimeInSeconds) {

        stopAutoRefreshTimer();

        try {
            autorRefreshHandler = new Handler();
            isAutoRefreshActive = true;
            Log.d(ConstantsCommonLibrary.LOG_TAG, "Auto Refresh Timer Started @ " + System.currentTimeMillis() + " in " + context.getClass().getSimpleName());
            autorRefreshHandler.postDelayed(new Runnable() {
                public void run() {
                    if ( isAutoRefreshActive ) {
                        //Log.d(ConstantsCommonLibrary.LOG_TAG, "Auto Refresh Time Elapsed @ " + System.currentTimeMillis()+" in "+context.getClass().getSimpleName());
                        onAutoRefreshTimeElapsed();
                        autorRefreshHandler.postDelayed(this, (autoRefreshIntervalTimeInSeconds * 1000));
                    }
                }
            }, (autoRefreshIntervalTimeInSeconds * 1000));
        }catch (Exception e){e.printStackTrace();}
    }

    public void stopAutoRefreshTimer(){
        if ( autorRefreshHandler != null ){
            isAutoRefreshActive = false;
            autorRefreshHandler.removeCallbacksAndMessages(null);
            Log.d(ConstantsCommonLibrary.LOG_TAG, "Auto Refresh Timer Stopped @ " + System.currentTimeMillis() + " in " + context.getClass().getSimpleName());
        }
    }

    public void onAutoRefreshTimeElapsed(){}

    //========================

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == ConstantsCommonLibrary.LOCATION_SETTINGS_ACTIVITY_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : ENALED BY USER");
                    if (locationSettingsCallback != null) {
                        locationSettingsCallback.onComplete(true, 200, "Location settings enabled by user");
                    }
                } else {  //Activity.RESULT_CANCELED
                    Log.d(ConstantsCommonLibrary.LOG_TAG, "LOCATION_SETTINGS : CANCELLED BY USER");
                    if (locationSettingsCallback != null) {
                        locationSettingsCallback.onComplete(false, 500, "Location settings cancelled by user");
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterFragmentReceiver();

        stopAutoRefreshTimer();

        closeLoadingDialog();
        closeNotificationDialog();
        closeChoiceSelectionDialog();
        closeNumberPickerDialog();
        closeDatePickerDialog();
        closeTimePickerDialog();
        closeCustomDialog();
        closeListChooserDialog();
    }

}
