package com.aleena.common.interfaces;

/**
 * Created by venkatesh on 17/3/16.
 */
public interface SoftKeyBoardActionsCallback {
    void onDonePressed(String text);
}
