package com.aleena.common.interfaces;

/**
 * Created by venkatesh on 27/9/15.
 */

public interface DateAndTimePickerCallback {
    void onSelect(String fullDate, int day, int month, int year, String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds);
}