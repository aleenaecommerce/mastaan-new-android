package com.aleena.common.interfaces;

/**
 * Created by venkatesh on 20/1/16.
 */
public interface PermissionsCheckCallback {
    void onComplete(boolean status, boolean isHasAnyNeverAskAgainDeniedPermissions);
}
