package com.aleena.common.interfaces;

/**
 * Created by venkatesh on 18/1/16.
 */
public interface ContactPickerCallback {
    void onComplete(boolean status, String contactName, String contactNumber);
}
