package com.aleena.common.interfaces;

/**
 * Created by Naresh-Crypsis on 27-10-2015.
 */
public interface DayAndTimePickerCallback {
    void onSelect(int selectedDayPosition, int selectedSlotPosition);
}
