package com.aleena.common.interfaces;

/**
 * Created by venkatesh on 23/9/15.
 */
public interface StatusCallback {
    void onComplete(boolean status, int statusCode, String message);
}
