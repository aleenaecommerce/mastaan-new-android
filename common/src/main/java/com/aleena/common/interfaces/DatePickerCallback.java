package com.aleena.common.interfaces;

/**
 * Created by aleena on 27/9/15.
 */

public interface DatePickerCallback{
    void onSelect(String fullDate, int day, int month, int year);
}