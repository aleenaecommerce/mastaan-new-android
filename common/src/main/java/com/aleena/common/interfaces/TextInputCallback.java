package com.aleena.common.interfaces;

/**
 * Created by Venkatesh Uppu on 20/9/16.
 */
public interface TextInputCallback {
    void onComplete(String inputText);
}
