package com.aleena.common.interfaces;

/**
 * Created by aleena on 27/9/15.
 */

public interface ActionCallback {
    void onAction();
}
