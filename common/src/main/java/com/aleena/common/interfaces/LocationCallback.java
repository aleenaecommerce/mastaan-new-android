package com.aleena.common.interfaces;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by venkatesh on 23/9/15.
 */
public interface LocationCallback {
    void onComplete(boolean status, boolean isLocationAccessEnabled, String message, Location location, LatLng latLng);
}
