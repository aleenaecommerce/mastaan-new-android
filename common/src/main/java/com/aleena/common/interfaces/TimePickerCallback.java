package com.aleena.common.interfaces;

/**
 * Created by Venkatesh Uppu on 27/9/15.
 */

public interface TimePickerCallback {
    void onSelect(String fullTimein24HrFormat, String fullTimein12HrFormat, int hour, int minutes, int seconds);
}