package com.aleena.common.interfaces;

/**
 * Created by Venaktesh Uppu on 29/07/16.
 */

public interface ListChooserCallback {
    void onSelect(int position);
}
